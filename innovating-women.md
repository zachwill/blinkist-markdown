---
id: 5463024c63356600085d0000
slug: innovating-women-en
published_date: 2014-11-13T00:00:00.000+00:00
author: Vivek Wadhwa and Farai Chideya
title: Innovating Women
subtitle: The Changing Face of Technology
main_color: 39C2ED
text_color: 247994
---

# Innovating Women

_The Changing Face of Technology_

**Vivek Wadhwa and Farai Chideya**

_Innovating Women_ takes a critical look at today's technology industry, which, for all its success, remains incredibly old fashioned in its gender imbalance. Statistics and case studies help us scrutinize major players in the technology industry, while personal stories give insights into the lives of talented female innovators working hard against the odds.

---
### 1. What’s in it for me? Learn about the women who are changing technology against all the odds! 

The women who are driving technological innovations and pioneering business today are the ones you've never heard of. These blinks shine a light on the breakthroughs being made by female innovators today, from their academic success to their innate knack for business, and instigation of vital social change. It also reveals the incredible challenges that women face, due to gender discrimination that is deeply — but not irredeemably — rooted in the industry. Astonishing statistics and inspiring personal tales make these blinks a fascinating read and a reality check.

In the following blinks, you'll also learn

  * how _2001: A Space Odyssey_ and a brilliant female innovator brought live sports to your living room;

  * the euphemistic technical term for only hiring white male nerds; and

  * how sexual harassment killed a multi-million-dollar deal.

### 2. Women are vital innovators in the technology industry – yet we often only hear about men. 

Think about some of the great innovators in the technology industry. Who comes to mind first? Founder of _Apple_, Steve Jobs, or Mark Zuckerberg of _Facebook_ fame? Or perhaps, Kay Koplovitz? If you haven't heard of Koplovitz's groundbreaking work, here's the reason why: She's a woman.

Why is it that we usually associate the technological advances of recent decades with only the men behind them?

It's not the case that women don't have what it takes to innovate, in fact, quite the opposite. A study conducted by the _National Science Foundation_ found that females matched males in mathematical achievement. Moreover, for every 100 men that enrol in higher education, there are 140 women.

Not only do women excel academically, they are also no less business-savvy than men, if not more so. Another study completed by _Babson's Global Entrepreneurship_ revealed that female-led high-tech start-ups have noticeably lower failure rates than those that are male leaders.

Despite this, the technology industry itself often praises the achievements of men only. In fact, even over the past few years, the _TechCrunch Crunchies Awards_ (the Oscars of the tech industry), have often been exclusively awarded to men. As a result, we're often left unaware of the achievements of female innovators like Kay Klopovitz, who you should thank if you've ever watched a live sports game on TV.

In 1977, Klopovitz singlehandedly brought professional sport to cable television in the United States. Inspired by a talk given by the author of _2001: A Space Odyssey_, Arthur C. Clarke, she brought the future to life by developing the novel idea of using satellites for commercial rather than military purposes.

If only we heard more about success stories like Klopovitz, we'd see that women might even have an edge over men in their abilities to innovate. This is exactly what we'll be exploring in the next blink, as we take a closer look at the tech businesswomen we rarely hear about.

### 3. Studies have shown that women achieve greater success as entrepreneurs than men. 

Did you know that women actually have a knack for success in entrepreneurial ventures? 

In 2009, a _Kauffman Foundation_ study showed that women were more _capital-efficient_ than men. Or, in plain English, women in business spend the money they start off with more wisely. They also see more stable or greater returns on this money.

Not only do women in business have a knack for managing money, they also demonstrate sophisticated people skills. Women generally place a higher value on personal and professional networks than men, and spend more time cultivating good business partnerships.

Another distinctive way in which women make great business leaders is what Whitney Johnson, cofounder of _Rose Park Advisors_, calls _disruptive innovation._ How exactly can something be disruptive and innovative at the same time?   

Disruptive innovations outperform existing and outdated ideas or businesses. For example, email is a disruptive innovation, when we consider how it blew the almost-obsolete snail mail out of the water.   

Women are disruptive innovators in the way that they build and maintain networks _outside_ of their organizations. A professor of the Harvard Business School has demonstrated this in his research, showing the way women work "outside the box" to bring new ideas and people together in a novel way.   

Of course, anyone can dream up novel ideas, but it seems women in particular are also highly successful at turning these ideas into profitable companies.

This can be seen when we look at companies backed by _venture capitalists:_ highly selective and demanding investors who invest in small businesses so they can grow. One study showed that on average, the successful venture capital-backed companies have _double_ the amount of women in their highest ranks, indicating the value of their contribution.

> _"Women instinctively know how to play where no one else is playing."_

### 4. Female-led technology businesses enjoy huge success, and still don’t receive the funding they deserve. 

The CEO characters that you see in TV shows and movies all tend to look the same — middle-aged, suited-up and male. But this pop culture stereotype is far from reality — while we don't often see powerful female CEOs depicted in the media, female-led and -owned businesses shouldn't be overlooked.

Not only are women beginning to lead their own companies, they're also enjoying large amounts of success. And they're no minority either — the _Center for Women's Business Research_ says that as many as 41 percent of private businesses in the United States are owned by women.

These businesses also tend to be highly successful and consistently outperform their male-owned counterparts. After surveying highly profitable firms in 2013, _American Express_ found that among businesses with revenue greater than $10 million, those owned by women enjoyed a 47 percent higher rate of growth. Moreover, from 2002 to 2012, female-owned businesses grew 28.6 percent, while other businesses only grew 24.4 percent. Once again, women-owned companies came out on top!

Despite their success, businesses led by women often struggle to acquire funding. Another study, also by the Center for Women's Business Research, discovered that in spite of the large portion of private businesses owned by women, as little as three to five percent of these actually receive venture capital-based funding and support.

A significant reason for this can be directly tied to gender bias. Venture capitalists often talk about their skill in _pattern recognition_, or their aptitude for spotting bright entrepreneurs with profitable ideas.

What this often means, however, is that these venture capitalists look for people that remind them of themselves. Of 89 venture capitalists on _TheFunded.com's_ top 100 list, only _one_ is a woman. So "pattern recognition" is really just a euphemism for investing in white, male nerds.

### 5. Women in the technology industry have brought about enormous social change. 

Not only are women making waves in technology, they're also making a difference in the world. Many female innovators have utilized technology to address worldwide poverty and inequality. As entrepreneurs, women have a vested interest in overcoming an oppressive status quo.

Women all over the world are socially and economically marginalized, and this is particularly clear in developing countries. For example, in Mexico, the prevailing macho culture forces women into traditional gender roles and often prevents girls, particularly in rural towns, from completing their education.

However, there is now a growing current of social change in Mexico, as women begin to set up _microenterprises –_ very small businesses, usually started from a low- or no-interest loan. 36 percent of microenterprises are run by women, which amounts to around two million small companies. These new developments are highly promising, but unfortunately up to 90 percent of microenterprises fail in their first year.

This is something that female entrepreneurs like Leticia Casanueva are working to change. Her company provides training and tools like simple inventory tracking software to help rural women start and run financially stable businesses.

Not only do entrepreneurial women help to boost the rural economy, they also work hard to provide a much-needed source of female employment in these areas.

A leading example is the work of Resha Jazrawi as the co-founder of _Acumen_ — a firm that makes investments specifically aimed at creating jobs for those who need them most, i.e., women. Acumen's investments are called _patient investments_, as they allow a business to develop slowly without the expectation of fast returns.

One example of Acumen's impact can be seen in the A to Z textile mills in Tanzania, which manufacture anti-malaria mosquito nets. Aside from providing a helpful product, this investment has created as many as 80,000 jobs, 90 percent of which are designed for women.

### 6. Gender bias exists at all levels of the technology industry. 

Right from the outset, women in technology struggle against the industry's gender bias. In fact, this sort of discrimination begins before they can even launch their careers.

Many women and minorities are actively discouraged from pursuing a career in science, technology, engineering or mathematics. These disciplines, known as the STEM fields, are rife with gender stereotypes — countless women talk even of high school teachers who deemed them not good enough to make a career in STEM, despite their exceptional grades.

Female students are often judged using unfounded assumptions, such as that women are "too sociable" to work in science. A 2010 _Beyer Corporation_ survey found that as many as 40 percent of female and/or minority chemists and chemical engineers had been discouraged from pursuing their careers.

If, despite this discouragement, a young female student makes her way through her studies with excellent marks and begins to search for a job, she will yet again be met with discrimination. In tech, hiring policies are so skewed that certain jobs are effectively "men only."_Bright Labs_ found these gender discrepancies were overwhelming in the United States: 92.7 percent of network engineers, 91.5 percent of PC technicians, 90.4 percent of IT support workers and 90 percent of software engineers were men _._

Despite this obvious imbalance, high-earning companies have yet to show any initiative to rectify things. A 2012 survey by _McKinsey & Company_ found that only eight percent of larger companies had women in more than 25 percent of the top jobs. This is a reality we can see statistically and socially.

For example, when attending a tech conference in Uruguay, female telematics engineer Agustina was briskly told by a key investor: "I don't have much to say to you because I'm a techie nerd." The investor assumed that women weren't interested or knowledgeable, nor had anything to say about technology.

### 7. Motherhood and family pose unique challenges for the female entrepreneur. 

It's easy to admire the success of a high-powered male professional. But you should remember that these men enjoy an advantage: they have been free to pursue their career, unlike women, who are forced to decide between career and family in order to achieve their goals.

This is a great challenge for female professionals, especially as workplace culture makes technology industries notoriously unfriendly places for single mothers. The collective mind-sets of many start-ups are geared toward the single male rather than a woman with family responsibilities.

For example, the simple act of leaving work at 5 p.m. is considered a faux pas in the industry. Female employees frequently report being stigmatized by their male colleagues for working "mommy hours" to accommodate school and child care, even if these women come to work as early as 5 a.m. and work just as many hours!

The entrepreneurial environment also involves networking during "happy hours" — late nights and weekends. As a result, many women are forced to pay a "female tax" by not participating in these extracurricular activities, because they look after their children instead.

Thankfully, formal support systems are starting to assist career-oriented women with families. Some progressive countries have legislation to support women during maternity leave and ensure companies don't discriminate against them when hiring. Germany, for example, has developed the _Elterngeld_ system, which pays either parent 67 percent of their earnings during leave.

In many cases, however, men do not take up their parental leave. So mothers are frequently called upon to be the primary caregiver. This was highlighted by a study conducted by Stanford University, which found that mid-career men were four times as likely as their female counterparts to have a partner who took responsibility for household and children. And yet our cultural norms still dictate that only "bad mothers" would place the same importance on their career as their family.

### 8. Workplace sexism is unfortunately still rife in the technology industry. 

Although we wish it were a thing of the past, sexual discrimination is still alive and well. In fact, it's something women have to deal with on a daily basis to varying degrees.

Remember the "female tax" we learned about earlier? Well, it also extends to what female professionals wear — make-up, style and sex appeal are all things women have to worry about before they even start the day. Some businesswomen have even internalized this, instructing younger employees to pay attention to how they dress as a critical factor in their career success.

Unsurprisingly, many women, such as Catherine Rose, project manager for interactive teaching tool _LightAide_, are dismayed that they are not valued by their PhD or MBA, but for their outward appearance. This trend is also seen in the mainstream press.

For example, when interviewing entrepreneur and technology executive Kim Polese, _Fortune_ magazine chose to run the story with a close-up of her face, and used the angle that Polese was a tech industry "femme fatale." Polese had explicitly asked beforehand that the story focus on her software management company Marimba.

Worse still, women in technology also unfortunately suffer outright sexual harassment. Heidi Roizen, the former worldwide developer for Apple relations, experienced this first hand.

While working for a multi-million-dollar software company, Roizen was on the verge of signing a company-defining deal with large computer manufacturer. She was invited to sign the deal over supper in a luxurious restaurant and during the meal, the vice president of the manufacturer lewdly tried to coax her hand onto his crotch. Naturally, she stormed out of the building, and the deal fell through. Millions in revenue were lost.

These are just some of the manifestations of the sexism still sadly prevalent in the high tech industry.

> _"I do recognize that I should put on make-up and get dressed up, but I would rather spend my time moving projects forward." — Catherine Rose._

### 9. If we recognize the issues that women face, we can change hiring policies to empower them. 

From what we've seen of the workplace culture in the technology industry, the challenges for women are deeply entrenched. Which makes one wonder — is change even possible? 

Not if we deny the problem, which only reduces accountability and maintains the harmful status quo. Instead, gender imbalances need to be discussed in a public forum, though such dialogue may not be received warmly.   

For instance, in 2010, co-author Vivek Wadwha published a widely read piece called _Silicon Valley: You and Some of Your VCs have a Gender Problem_ that exposed the shocking gender imbalances in high tech companies. It was met with a huge uproar, and many senior figures publicly attacked the author on social media. Some colleagues even emailed him, saying "there are easier ways to get laid."  

The article also brought Twitter under fire, by exposing the imbalanced demographics in the company: all of Twitter's board members and executives were male. Its leaders vociferously attacked those that attempted to bring the gender issue to light. Twitter CEO Dick Costolo actively _denied_ the existence of the issue.   

However, in 2014, Twitter added an additional female board member, and a flurry of other tech companies followed suit. Clearly, it took a large-scale public relations embarrassment for Twitter to bring positive change to their company and others.   

Positive change in the technology industry should begin with reform in a crucial area: hiring policies. If recruitment pools are broadened, there is a chance to bring about gender equality. Universities have equal numbers, if not greater numbers, of female STEM graduates than male. If firms recruit directly from graduate programs, a higher percentage of female applicants are instantly available.   

Another way to increase diversity is through a _quota system_. This has been successful in other fields such as sports management. For example, the "Rooney rule" employed in National Football League ensures that minorities are interviewed for management positions. This could be an effective approach in the technology industry, ensuring that a certain percentage of positions are filled by women.

> _"To create the next Google, Facebook, and Intel, we need to boost entrepreneurship — particularly among women."_

### 10. Final summary 

The key message in this book:

**The technology industry is a boys club that needs to be exposed. Firstly, for the sake of the talented women, who are challenged daily despite being instinctively business-savvy high academic performers, and arbiters of social change, not to mention mothers. And secondly, for the progression of the technology industry today and in the future.**

Actionable advice:

**Girls make great geeks!**

If you are turning away from a career in technology, perhaps because you feel it's only for geeky guys, then you'd better think again! There are many innovative, powerful and inspirational women in the technology industry today, all proving that it's not only possible for women to be tech leaders, but vital.

**Suggested** **further** **reading:** ** _The Innovators_** **by Walter Isaacson**

_The Innovators_ explores the social and cultural forces that inspired technological innovation through the history of computers and the internet. By weaving together the personal stories of technology's greatest minds, _The Innovators_ gives you an inside look at how the best and the brightest innovate and collaborate.
---

### Vivek Wadhwa and Farai Chideya

Vivek Wadhwa is a fellow of the Arthur and Toni Rembe Rock Center for Corporate Governance at Stanford University. _Foreign Policy_ magazine named him one of 2012's Top 100 Global Thinkers.  

  

Farai Chideya is a multi-award-winning journalist and a Distinguished Writer in Residence at New York University.

