---
id: 57af79449400350003a45f15
slug: innumeracy-en
published_date: 2016-08-16T00:00:00.000+00:00
author: John Allen Paulos
title: Innumeracy
subtitle: Mathematical Illiteracy and Its Consequences
main_color: FED133
text_color: 7A6518
---

# Innumeracy

_Mathematical Illiteracy and Its Consequences_

**John Allen Paulos**

_Innumeracy_ (1988) explains how an aversion to math and numbers pervades both our private and public lives. By examining various real-life examples of innumeracy and its consequences, the book offers helpful solutions to combat this irrational and misguided fear of math.

---
### 1. What’s in it for me? Lose your fear of numbers. 

Mathematics can be quite a difficult subject, and anyone who struggled for years with sine functions, logarithms and probability distributions in school knows this all too well. Math is often seen as such a challenging discipline that many people, even the highly-educated, have developed a fear of it and shy away from anything involving numbers.

In our society, you can openly boast that mathematics was always your worst subject and that you're not a "numbers person." But people who fail to grasp the basic concepts of numbers and probability are innumerate — and innumeracy is not a minor issue.

In these blinks, you'll learn how common innumeracy is and why it is a major risk. You'll see how innumerate people struggle to handle many real-life situations and how a basic understanding of mathematical concepts can be of great benefit to you.

You'll also learn

  * how many choices of outfit there are if you own five shirts and three pairs of pants;

  * why you should expect to spot a blond woman with a ponytail in a yellow car driven by a bearded, mustachioed black man in Los Angeles; and

  * why astrology is pseudoscience.

### 2. Innumerate people have trouble grasping basic mathematical principles and often fail to react appropriately to everyday events. 

It's rare to hear people openly admit to illiteracy; yet it's quite common to hear someone acknowledge that math was his worst subject, or shrugging and saying that he's simply not a numbers person.

But innumeracy — that is, lacking the basic notions of math — is nothing to boast about.

Being innumerate has a host of negative consequences, one of them being an inability to react appropriately and make accurate judgments in circumstances involving numbers and probability.

An incapacity to determine whether a figure in a given context is big or small makes innumerates prone to _personalizing_, when their numerical intuition is prejudiced by their own experience.

For instance, the probability of being eaten by an alligator is quite low, although it does sometimes happen. But an innumerate person might read a news story about such an event and develop an irrational fear of alligators, ignoring any statistical evidence demonstrating that gator attacks are a very rare occurrence.

Another negative effect of innumeracy is the inability to grasp the implications of simple mathematical principles.

Let's take the _multiplication principle_, which holds that if we have _m_ different ways of making a choice and _n_ different ways of making a subsequent choice, then we have _m_ x _n_ different ways to make these choices in succession.

To apply this elementary concept to an everyday situation, we would be able to conclude that a woman with five shirts and three pairs of pants would have 15 (5 x 3) outfit choices for any given day.

Going even further, the multiplication principle would yield the conclusion that if the woman were planning her outfits for an entire week, she would have 15 choices the first day, 15 the next day, and so on, or a total of 15⁷ choices **–** that's 170,859,375 options!

Innumerate people, however, might reject the truth of this number and believe that it's ridiculous that a few shirts and pants could result in such an incomprehensibly large number.

### 3. Innumerate people tend to misunderstand the concept of coincidences, which are improbable, yet common, events. 

In 1492, Christopher Columbus discovered the New World. In 1942, Enrico Fermi discovered the atom. Coincidence? Sigmund Freud might say, "I think not!" The famous founder of psychoanalysis claimed that there was no such thing as a coincidence.

But what is a coincidence in the first place? Coincidences are improbable events, but they actually happen quite often **–** so much so, in fact, that we should sometimes _expect_ them to happen.

The high occurrence of coincidences is a point so well established that it has even been used in court. In 1964, for instance, a ponytailed blond woman in Los Angeles stole a purse and sped off in a yellow car driven by a black man with a beard and mustache.

Two suspects who fit the description were quickly brought into the California Supreme Court. Interestingly, the court used math to argue that in a city as large as LA, it was probable that there were many such couples, most of whom were innocent. As a result, the suspects were set free.

Statistics and probability explain the sometimes astonishing likelihood of coincidence. But, because innumerates don't understand this, or fail realize the importance of statistical evidence, they often confuse simple nuances.

For instance, they are often perplexed by the fact that while it's probable that _some_ coincidence will occur, it's much less likely that a _specific_ coincidence will occur.

Say there are 23 people gathered at a party. There is a good 50 percent chance that at least two of them will share a birthday. However, it is much less likely that two of them will share a birthday on a specifically selected day — in other words, the likelihood that two people will have both been born on, say, September 21st.

The party would need to have 253 people present to make the chances of two people sharing a specific birthday reach the 50 percent level.

> _"It would be very unlikely for unlikely events not to occur."_

### 4. Pseudoscience preys on people’s innumeracy. 

Math is a field built on absolute truths **–** but that doesn't mean its applications are always absolutely correct.

In fact, there are entire industries built on the pseudoscientific, exploitative misuse of math.

If you've overheard people saying things like "Mercury in retrograde," or "We didn't click because I'm a Libra, our stars just didn't align," you've been privy to an all-too-prevalent form of pseudoscience: astrology.

Astrology claims that our personality and moods are affected by the way the planets were being pulled by gravity at the time of our birth.

As such, the field is loosely based on mathematical and physical laws, which state that the pull of gravity on an object or body corresponds to, or is proportional to, its mass. Concurrently, the gravitational pull between two bodies decreases in proportion to the square of the distance between them.

Astrology is a bastardization of these laws, however, and a quick look at the facts reveal the field as a total sham. The gravitational pull of the doctor or nurse who brought you into the world would entirely outweigh the pull of planets millions of miles away.

The idea behind astrology is evidently alluring, however: A 1986 Gallup poll revealed that 52 percent of American teenagers believe in it.

And it's not just ignorant young folk who are targeted by pseudoscience.

Despite Freud's strengths as a psychoanalyst, to say that math was not his forte would be an understatement. His friend Wilhelm Fliess once convinced him that the numbers 23 and 28 had special properties, since if you add and subtract certain multiples of them, you can attain any number. To get the number six, for instance, you could calculate 6 = (23 x 10) + (28 x -8).

This calculation is correct, but it's not because 23 and 28 are special. In fact, any two numbers without common factors have this property **–** so you could do the same with 2 and 3, for instance, or 11 and 24.

Even a man as brilliant as Freud was vulnerable to the throes of "math"-based pseudoscience. Unfortunately, because math lends an air of universal truth, it's an easy tool for tricksters to utilize to manipulate the innumerate.

### 5. Innumeracy is a result of poor education, psychological blocks and misconceptions about math. 

So what exactly causes the prevalence of innumeracy?

One contributing factor is the way math is taught in schools. Students are taught the basic mechanics of adding and subtracting, but the way those skills are applicable in real life is not emphasized nearly enough.

Let's say you were given the math problem (1-¼) x ⅕ = ? For a student, this problem might feel a lot more relevant if she were asked instead to solve the following problem:

A quarter of the global population is Chinese, and one-fifth of the remaining population is Indian; what is the percentage of the Indian population?

If your answer is 15 percent, you nailed it!

Introducing more examples of math's real-world applications in school would help students understand why it's a useful skill to have, instead of just mechanically poring over abstract problems.

A second issue is the paralyzing psychological blocks that many people develop toward math.

This is also known as _math anxiety_, a condition people develop due to intimidation and emotional scarring from negative experiences with math. Maybe an aggressive teacher used harsh words to convince someone that they were incapable of grasping math, or instilled a belief that there are some inherently gifted students with "math minds."

Like Freud, many highly intelligent and capable people might have math anxiety — but struggling with math doesn't make them incapable. If you have a fear of math, regaining your confidence might actually be the first step to combating these blocks.

Even if it seems silly, you can ease yourself into math by solving numerous simple problems that you're already fairly comfortable with, and slowly progressing from there!

Also problematic are the misunderstandings about math that perpetuate people's psychological aversion to it.

One such misconception is that math is a cold, mechanical subject that numbs its students to fully comprehending and appreciating subjects in the humanities. But this is like saying that understanding the intricacies of molecular biology prevents us from appreciating life's big questions, which, of course, is untrue!

### 6. We face trade-offs on a regular basis, but mathematical understanding can help us approach decisions in daily life 

Math isn't just a world of numbers disconnected from our day-to-day lives; it can actually shed light on why people in society act as they do.

Consider the concept of _trade-offs_. When faced with decisions, we often have to make a trade-off between competing concerns, such as trying to determine whether your donation to one charity will be used more wisely by another. Mathematics can help us understand how we tend to approach these trade-offs.

When we are forced to choose, we have to take into account two kinds of statistical errors, _type-1_ and _type-2 errors._ Type-1 errors happen when one rejects a hypothesis that is true; an example would be when people repudiated the hypothesis that smoking is directly related to cancer.

On the other hand, a _type-2 error_ occurs when one accepts a false hypothesis, such as the medieval belief that the earth was flat.

When confronted with a question or decision, different people will judge the importance of each error in different ways. For example, when it comes to capital punishment, a liberal will focus more on avoiding the type-2 error; they wouldn't want the innocent to suffer unfairly. However, a conservative would be more likely to focus directly on avoiding the type-1 error — in this case, a criminal not getting his just desserts.

Not only can math provide interesting insights about society, you can even use it to make better decisions in your daily life. For example, just a basic understanding of probability and statistics can prevent you from squandering your money.

Say you find a dress at a 40-percent discount, and the store announces that it's been reduced by an additional 40 percent. You might think, "Score, an 80-percent discount! Time to get it in all three colors!"

But in reality, it's only a 64-percent discount from the original price, since the 40-percent reduction on the first 40-percent discount only subtracts another 24 percent off the original price.

> _"Probability, like logic, is not just for mathematicians anymore. It permeates our lives."_

### 7. Final summary 

The key message in this book:

**An awareness of numbers is an important tool in our everyday lives. Whether it's putting the news and statistics we hear into perspective, or seeing through the nonsense of pseudoscience, math is essential to helping us navigate real-life situations.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _How Not to Be Wrong_** **by Jordan Ellenberg**

_How Not to Be Wrong_ gives us an intimate glimpse into how mathematicians think and how we can benefit from their way of thinking. It also explains how easily we can be mistaken when we apply mathematical tools incorrectly, and gives advice on how we can instead find correct solutions.
---

### John Allen Paulos

John Allen Paulos is a Professor of Mathematics at Temple University in Philadelphia. He is the author of many other books, including _A Mathematician Reads the Newspaper_, and has written for publications such as _The New York Times_ and ABCNEWS.com.

