---
id: 53aad60a37303200072a0000
slug: insanely-simple-en
published_date: 2014-06-24T00:00:00.000+00:00
author: Ken Segall
title: Insanely Simple
subtitle: The Obsession that Drives Apple's Success
main_color: A4C0DB
text_color: 586775
---

# Insanely Simple

_The Obsession that Drives Apple's Success_

**Ken Segall**

_Insanely_ _Simple_ talks about the business strategy of keeping things simple. The author hones in on Apple's inner processes to show us how we can apply simplicity to our work and why complexity can hinder a company's development. The reader will learn about how Steve Jobs implemented this structure at Apple and upheld it even in the face of critics.

---
### 1. What’s in it for me? Learn how to simplify your business. 

These days it's normal to get bogged down with all the complexity surrounding us. This rings especially true in the workplace, where we often have to deal with conflicting ideas and drawn-out meetings. Simplicity, the author argues, can help solve these problems.

In these blinks, you'll discover

  * how Steve Jobs turned Apple into the most successful computer company in the world,

  * why iPads and iPhones only have one button and

  * how to make your projects and teams effective, streamlined and focused on designing beautiful products.

### 2. People prefer simplicity over complexity. 

Have you ever bought a new computer with such complicated new features that you couldn't get it to work? This type of complexity is a problem in our modern world because humans, just like all other life on the planet, prefer simplicity.

We know that nature doesn't choose the most complicated way to solve a problem. Rather, it finds the simplest way. In fact, it's often the simplest species that are the most successful.

Take earthworms for example: they're incredibly simple organisms, yet they've outlived thousands of more complex species.

Simplicity seems to be natural, and as humans, we prefer it when our lives are made as simple as possible.

This can be seen in the way we choose the products we use in our everyday lives: we tend to prefer simple ones over complex.

Product complexity became an issue for Microsoft when they created the Zune Store. Instead of allowing customers to simply pay for a song with their credit cards (as Apple does), customers first had to convert their money into Microsoft Points.

Microsoft's designers didn't think about simplicity and, consequently, the Zune Store never caught on.

This shows that, given a choice between a complex or simple way to reach a goal, people will most likely choose the simplest option. Companies that understand this know to make their products as simple as possible.

Apple's stores are a great example. As soon as you step inside, you're greeted by a minimalist decor and your options (to buy or repair something) are clearly demarcated. That's why the stores are so popular, especially in comparison to Microsoft's stores, which are over-packed with products and offer too many choices.

By understanding the human need for simplicity, you can build enduring business-customer relationships.

> _"The simpler path is the far more attractive one."_

### 3. Keeping teams small and feedback honest enables companies to reach effective solutions. 

A lot of us believe the best way to come up with a solution is having many minds working on it together. Yet this isn't always the best approach. Sometimes a small team is better.

Often, the larger and more complex a company's hierarchy, the longer it takes to make progress. Some companies have giant, complex structures, with staff working together across many departments. However, this can seriously complicate the decision-making process.

Take Dell: it has many different departments, such as customer support and sales, and each has a different CEO. Due to this set-up, it takes a long time for anything to be accomplished.

This problem was made concrete when Dell attempted to create a new branding strategy. The project ran for several months across different departments, but didn't make any progress. Eventually, because the different teams couldn't find a satisfying approach, the project was discarded.

Apple, however, has enjoyed success by doing the opposite as Dell. At Apple, teams working on projects are kept as small as possible. Steve Jobs was even known to kick people out of meetings if he thought their skills weren't vital to its success!

Aside from small teams, another way to run an efficient company is to ensure that everybody receives direct and honest feedback straight from their project managers.

Unfortunately, in many companies, feedback isn't given directly, but passed down from manager to supervisor to employee. This drawn-out process not only eats up precious time but can result in inaccurate feedback.

At Apple, employees usually receive their feedback personally after presenting their work to their boss. This streamlined process prevents misunderstandings and misconstrued information.

> _"The quality of work from a project is inversely proportional to the number of people involved in the project."_

### 4. Create small teams of highly skilled people and give them tight deadlines. 

Have you ever sat at an insufferable meeting full of people talking for hours with absolutely no progress being made? Many of us have experienced this scenario and are desperate to avoid it. In order to prevent such inefficiency, you need to streamline your work.

One way you can do this is by creating small groups of smart people to keep projects running smoothly.

Instead of having large teams made up of good people, build small teams of _excellent_, highly-skilled people. That way everyone in the team will contribute and no one will hold the rest back.

This proved effective at Apple, where the core design and marketing teams never had more than a hundred people. Steve Jobs came up with this number, as this was the maximum number of names he could remember!

This rule was so strict that if somebody new wanted to join the team, another person would have to leave it. This meant the new person had to be better suited for the role than the person they replaced and, consequently, teams grew increasingly efficient and their skill levels rose.

Once you've developed a highly-skilled team, have them work as efficiently as possible by giving them tight deadlines. People actually tend to work better and more efficiently if they're given a limited amount of time to come up with a high-quality result.

Apple, for example, allows a total of three months for marketing projects, from the brainstorming process to the launch of the ad.

This is enough time to choose and develop the best ideas, and to prepare a launch strategy without it becoming overly complicated or allowing too many conflicting ideas to arise.

> _"Apple encourages big thinking but small everything else."_

### 5. Think creatively and differently to stay ahead of your rivals. 

It is sometimes thought that the best way to get ahead in business is to emulate successful companies. However, in truth, business success relies on innovation and creativity.

If you're the first to come out with a new product, you'll most likely be the market leader.

Being the first to introduce a product to a market gives you an edge over your rivals. While they struggle to innovate a product that competes with yours, you'll already be working on better, next-generation models.

For example, Apple was the first company to successfully come out with an MP3 player — the iPod. The company went on to dominate over 70 percent of the market, constantly introducing newer and even more profitable generations of iPods, leaving other companies to play catch up.

Aside from being innovative in product creation, you also need to be innovative in your marketing if you want to stand out from the competition.

For example, Apple has launched some of the most successful marketing strategies in history by exploring new ways to create ads.

This is evident in Apple's "Think Different" campaign, which didn't even advertise a specific product. Instead, it highlighted the values that were most important to the company. The campaign was successful because it made people associate the company with innovation and creativity.

The way Apple markets its products is also unique. Whereas many companies will list all of a product's different features, Apple only uses one picture of the product and lets the design speak for itself.

This innovative marketing approach sets Apple apart from its competitors.

### 6. Apple applies the concept of simplicity to every step of its work process. 

Apple is renowned for its simple approach to everything. As we've seen, simplicity influences the company's marketing and product design, and even the size of its meetings. But when was this approach adopted?

It started when Steve Jobs returned to Apple in 1997 and made simplicity his main goal.

One of the first things he did was introduce a chart made of four quadrants to illustrate the new and simple direction Apple would take. Instead of producing many different models, he wanted to have only four computer models: laptops for professionals and consumers, and desktops for professionals and consumers.

Ever since, nearly every Apple product has been the result of a simplification process.

In fact, all Apple products undergo a process where unnecessary features that complicate the design are removed.

Just take a look at an iPhone or iPad. You'll notice they only have one button. Why? Because, apart from zero, one is the simplest number imaginable. The idea behind this is that even if the product has a vast array of features, we feel secure that with one single click, we can return to the familiar home screen.

In addition to simplifying products, Apple also simplifies its customer experience. It understands that it creates a sense of insecurity if it gives customers too wide a range of choices, which can make them question whether they made the right choice in the first place.

Therefore, in Apple stores, customers have limited choices and a simple way to recognize which product is best for them. This leaves them satisfied with their choice, instead of regretting not purchasing a slightly different model.

This simplicity is the trademark of Apple and one of the main reasons why it has become so immensely successful.

> _"Simplicity not only allows Apple to revolutionize — it allows Apple to revolutionize repeatedly."_

### 7. Apple employees love what they do, so they work hard to create the best products. 

Life is too short to do something that makes you miserable. Let's be honest; if you aren't passionate about something, you won't pour all your energy into it.

Passion is vital, and Apple's success can be largely attributed to its passionate team.

After all, Apple founders Steve Jobs and Steve Wozniak built the first Apple computer out of sheer passion to produce simple, user-friendly computers. This devotion led to Apple becoming the most successful technological company of all time.

Even after Jobs left in 1985, he still regarded the company as a beloved child and, when it ran into difficulties in 1997, he decided to return.

Although he sacrificed his own personal life to turn the company around, Jobs made it his policy to hire people who shared his vision to make Apple one of the world's greatest companies. Soon after, he had assembled a passionate team.

At Apple, staff members _have_ to be passionate about creating change in the world and technology.

The company also knows when to wield its famous "simple stick" to ensure they're creating the best products. When a project becomes too complex, it must be simplified (i.e., hit with the simple stick!).

If a product was overly complicated, it was not sold until it satisfied everyone, including Jobs, who was known for his incredibly high standards.

For instance, in 1997, the marketing department spent weeks on designing a new iMac ad. But Jobs hated it because he thought that it didn't fit with Apple's simple image.

As a result, the department worked long nights to satisfy Jobs. This eventually led to the creation of one of the most iconic advertisements ever made: the award winning campaign "Think Different".

This never would've been possible if the team hadn't been passionate about its work.

### 8. Steve Jobs was the main reason why Apple managed to regain its glory in the 1990s. 

One of the most iconic public figures of the twenty-first century, Steve Jobs is celebrated for having revolutionized the entire computer industry. But how did he get to where he did?

His popularity started gaining momentum when he directed Apple to focus on simplicity after its near-collapse in the 1990s.

In 1997, Apple was only a few months away from going bankrupt, the company having lost its former image of producing simple, high-quality products.

When Jobs returned, he took it upon himself to restore the company's reputation, reminding both employees and customers of the core values of simplicity and innovation that Apple was founded upon.

He also abolished the different departments that produced their own Mac models and software products, and refocused Apple on producing only a select few products.

This approach quickly took effect with the success of the first iMac in 1998: Apple's former glory had been reinstated.

Jobs created an image of Apple that would shape the company's success for decades to come.

This came in the form of his vastly successful "Think Different" campaign, which was launched even before there was an actual product to sell. Instead, it sold the values of creativity and innovation, which increased Apple's sales even before the introduction of the new iMac.

In fact, it was the first campaign in the history of selling computers that profited before there was a new product to sell.

The campaign worked because it focused on how Apple should be perceived and what its customers should value. Furthermore, it restored the image of Apple as a cutting-edge, innovative brand.

Jobs made people feel that if they invested in Apple, they could help keep innovation and creativity alive in an environment that had a very traditional approach to marketing and product development.

### 9. Final Summary 

The key message in this book:

**Apple** **is** **so** **successful** **because,** **thanks** **to** **Steve** **Jobs,** **it** **adheres** **to** **the** **concept** **of** **simplicity.** **Streamlining** **and** **simplifying** **processes,** **such** **as** **keeping** **teams** **small** **and** **having** **team** **leaders** **directly** **interact** **with** **projects** **in** **progress,** **enables** **the** **company** **to** **reap** **the** **benefits** **of** **keeping** **ahead** **of** **its** **competitors.**

Actionable advice:

**Stop** **inviting** **everyone** **to** **meetings.**

The next time you plan a meeting, don't invite everyone just to be polite. Instead, make sure only the people who are essential to the meeting are present. This will keep it running smoothly and effectively without it being weighed down by those who don't have the relevant skills or input for the project or problem at hand.

**Make** **shorter** **deadlines.**

Allowing a long stretch of time to complete a project can be a breeding ground for conflicting and unresolved ideas. By keeping to a shorter (yet still reasonable) deadline, you will increase the chances of more streamlined and effective projects and problem-solving.
---

### Ken Segall

Ken Segall was an advertising agency creative director for NeXT and Apple, and worked closely with Steve Jobs for many years. He also served as a creative director for Dell, BMW and Intel. He is the man responsible for the "i" in many of Apple's most popular products and was part of revolutionary campaigns, such as the "Think Different" ad campaign.

