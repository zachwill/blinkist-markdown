---
id: 557d9d3565333800071d0000
slug: insight-selling-en
published_date: 2015-06-15T00:00:00.000+00:00
author: Michael Harris
title: Insight Selling
subtitle: Sell Value & Differentiate Your Product With Insight Scenarios
main_color: E57D37
text_color: 995425
---

# Insight Selling

_Sell Value & Differentiate Your Product With Insight Scenarios_

**Michael Harris**

_Insight Selling_ (2014) sheds light on new techniques for salespeople to win even more sales by setting up scenarios that enlighten the buyer and build trust. By using "insight scenarios," a sales team can not only solidify sales relationships but also significantly increase sales volumes!

---
### 1. What’s in it for me? Discover how to become a better seller by using insight scenarios. 

We've all been there: as you contemplate a big-ticket purchase, like a new flat-screen television, a salesperson approaches you and starts his well-rehearsed spiel.

Suddenly you're drowning in display ratios, refresh rates and connectivity questions, things that set your mind spinning and sap your confidence.

This is not the way to seal a deal. As you'll learn in these blinks, dumping tons of information on a potential buyer isn't the most effective sales method.

Instead, you need to listen to your client and weave a simple narrative that leads to a single conclusion: that her life will be easier with your product in hand. These blinks will show you how.

In these blinks, you'll learn

  * how to use and construct effective insight scenarios;

  * why personalized stories are better than tons of facts; and

  * why listening to your customer's story is so important.

### 2. Insights help a potential customer refine her thinking; they show what the client doesn’t yet see. 

A salesperson is often compared to an actor, with a scripted pitch and a talent for playing to the audience. Yet really, salespeople are more like teachers, standing in front of a class of students eager to learn.

Straightforward selling, such as offering ice cream to vacationers on the seashore, is just that: straightforward. Yet complex selling requires you to challenge a buyer's preconceptions.

Today a buyer can find all the information she needs online to make a decision. So a customer really only seeks out a salesperson when she's already done her research about a desired product.

Granted, this might be a smart approach when buying new shoes, but not when you're looking for a complex solution to a company problem. The truth is, most buyers lack the time and competence to really understand all the possibilities of how a product could benefit them.

Compare a situation like this with visiting a doctor. You might think you have the flu after looking up your symptoms on the internet, yet the doctor's diagnosis — considering that he has more insight into the science of health overall — could be something completely different.

So how do you challenge a potential customer's preconceptions? By providing _insights_.

Salespeople offering complex solutions should slowly guide a buyer to the solution's ultimate value proposition. This usually involves asking a buyer questions about her company's needs and concerns, yet also assumes that the buyer knows the answers to these questions.

But this isn't always the case, especially if your product is completely a new service that the buyer has no experience with, or is complex. Your customer then should instead be allowed to discover your product's value through insights. Insights are ideas that refine the way a buyer thinks. The objective of an insight scenario is to shine the light of insight on unrecognized value so that salespeople can sell value and differentiate their product. Value is unrecognized when either the customer does not recognize the true root cause of the problem, or when the customer underappreciates the cost of the status quo or the benefits of the change.

You can find insights from determining what you think the buyer doesn't see, or create insights by providing a dramatized version of how life might be with or without your product — somewhat like the "before" and "after" pictures in weight-loss advertising!

> _"Complex selling has changed, because buying has changed."_

### 3. Insight scenarios engage a prospect, giving them an emotional, personal story to relate to. 

So you've found an effective insight to present to a customer. But what if she doesn't listen to you?

This is where a _scenario_ can help.

_Insight scenarios_ are short stories that help a buyer picture your value proposition and how it could function in the real world. Your scenario shouldn't be longer than 350 words or take more than two minutes to deliver, as its purpose is not to drown a buyer in facts but to plant the seed of doubt about her current circumstances.

Insight scenarios are especially effective as they engage rather than question a customer's ideas or beliefs.

An insight scenario emotionally draws a buyer in, giving her a personalized story to which she can relate as opposed to a list of dry facts and figures. A well-told scenario allows a buyer to picture herself in the position of the actual user of the product and to explore a _virtual test drive_ of its features.

As the salesperson, you could for example tell a story of a customer named Jane, who similar to your current prospect, had issues with team coordination and missed company goals. You could then explain how you assisted Jane with your amazing team-building program. Now Jane's company is meeting its goals because of her intelligent purchase.

So what's the difference between an insight scenario and a case study, then? In general, case studies are far more complex than insight scenarios, offering research-like profiles of relationships with other customers.

Case studies only work as a convincing proof in a pitch in which a buyer has already displayed interest in your value proposition. Flooding your prospect with facts when she's not yet ready will actually work against you!

> _"Because stories are facts wrapped up in context and delivered with emotion, they are more memorable."_

### 4. Creating a good insight scenario might be simple, but it still must be constructed carefully. 

A good insight scenario must challenge a buyer's thinking with the ultimate goal of highlighting your value proposition. To do this, your insight scenario needs to be carefully crafted.

Here are seven steps to creating an insight scenario.

The first step is to figure out how your prospect thinks, and how she will need to think differently to agree to a purchase. The scenario you create should highlight the gap between these two points.

Let's say you work for a public relations company looking to sell services to a hotel chain suffering from weak bookings. The hotel's management thinks that they're losing out to other hotels; but through your scenario, you want to show instead that customers are actually flocking to Airbnb.

The second step is to work your buyer's goals into your scenario, so that the buyer can relate personally to your story. Here you might want to talk about improving customer satisfaction, for example, as that's one of the hotel chain's primary concerns.

The third step involves being specific about the buyer's particular problem, or _complication_. This is where you dish out facts, such as how many other hotels went under as Airbnb grew more popular.

In the fourth step, describe a situation in which an external force plays the "bad guy," such as a competitor entering the market or a new set of laws that makes business more difficult.

The fifth step captures your buyer's "aha" moment, where she realizes the need for change.

The sixth step reveals how your service is the answer to the hotel chain's problems — but remember not to go overboard with details. Here you could explain simply how a PR campaign focusing on customer service can draw in more high-end customers to offset the loss of guests to Airbnb.

In the seventh and final step, ask the buyer questions to help her discover and tell her own story. In doing so, your buyer will connect the dots and realize how your scenario addresses her issues.

> _"The salesperson cannot lead with value, but must instead lead the buyer to the value."_

### 5. Insight selling means figuring out your prospect’s story and leading it to your value proposition. 

The telling of your insight scenario will only take about two minutes. So what should you do with the rest of your time with your potential buyer?

First, ask questions that highlight the value of your product.

In your scenario, you paint a picture for the customer to reveal how much better life could be with your product. After this, you should ask your prospect questions to gradually guide them to revealing the true value of your product for her company.

Keep in mind this is not an interrogation! Keep it simple and let every question uncover a new piece of the value proposition puzzle for the buyer.

Stick with simple questions that make the story clear. In our previous example, you as the PR sales rep could ask the buyer exactly what happens when clients choose Airbnb, or what the concrete consequences of continued weak bookings would be.

As well as demonstrating the value of your product, you need to listen carefully to a buyer to figure out her story as well.

The crux of insight scenarios and questions is to steer a buyer's vision into one that leads to the value of your product. To do so, you need to identify your starting point. That is, you need to listen carefully to determine a buyer's current situation.

So if a buyer is primarily concerned with hygiene issues and you've gone on for minutes about the market clash of hotels and Airbnb, you've clearly not listened to your prospect.

Be aware that listening isn't as easy as it "sounds." Few people know how to truly listen.

As a prospect chatters on, most of the time you might just be waiting for your turn to offer your own thoughts. Avoid this! The buyer will notice straight away that you're not fully engaged.

> _"You only tell a story to hear the buyer's story."_

### 6. With a little coaching, any sales team can learn to develop effective insight scenarios. 

Insight selling can be integrated into a company's sales model with a little practice. Although the strategy might initially be challenging for salespeople, the rewards are worth the work!

The first, and obvious, goal is that insight sellers need to offer well-crafted insights.

To do so, salespeople need to have access to a great deal of information, from customer knowledge to a company's unique selling points. Gathering this information can sometimes be tricky.

For example, the author recalls how he asked a managing director for more information on how a customer could use a certain product, only to be pushed off and then informed that, as a salesperson, he wouldn't be smart enough to understand it!

Also, sellers need to learn to change from using a model in which they simply spit out a stream of information to using one in which they're so familiar with the facts that they can effortlessly craft effective insight scenarios.

When a salesperson offers an insight scenario to a prospect, he needs to know not only the product intimately but also how business is done at the prospect's company. A salesperson also needs a good grasp of the prospect's general situation, and be able to relate these facts to the scenario, rather than asking the prospect random questions.

Coaching can help salespeople come up with effective insight selling strategies.

This works best in flat-hierarchy environments, in which sellers can hone their skills by writing their own scenarios and sharing them with colleagues.

For instance, consider organizing a meeting where your sales team determines the biggest issue the company's customers are facing. One salesperson could write a scenario that addresses this issue and present it to the group. The team then could discuss the scenario and provide feedback.

> _"Without a helicopter's view of the customer's world, salespeople are selling blind."_

### 7. Final summary 

The key message in this book:

**To sell effectively, salespeople need to provide buyers with insights. The best way to do this is through insight scenarios, or short stories that highlight the value of a product and engage the customer on an emotional level.**

Actionable advice:

**A follow-up letter might just seal the deal.**

After a meeting with a buyer, send a follow-up letter in which you outline the new insights you presented and the way they relate to the buyer's vision for her company. This is a great chance to reconfirm the new buying vision as well as to access other decision-makers in the company who might also read the letter.

**Suggested** **further** **reading:** ** _Agile Selling_** **by Jill Konrath**

In _Agile Selling,_ you'll discover how to boost your productivity and learn to thrive in any kind of sales environment. The book explores the strategies of great salespeople and how they adapt to new circumstances, and offers practical advice on how you too can become more adaptive in your business.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Michael Harris

A veteran of Wall Street, Michael Harris is the founder and CEO of Insight Demand, a sales training company.

