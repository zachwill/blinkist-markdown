---
id: 516bc3f3e4b01b686deb9b24
slug: inspired-en
published_date: 2013-05-13T00:00:00.000+00:00
author: Marty Cagan
title: Inspired
subtitle: How To Create Products Customers Love
main_color: F4AD31
text_color: 8F651D
---

# Inspired

_How To Create Products Customers Love_

**Marty Cagan**

_Inspired_ describes the best practices of creating successful software products and explains the most common pitfalls and how to avoid them. The lessons are applicable in a range of product environments, from fledgling start-ups to large corporations.

---
### 1. Great product managers are intelligent, focused and “bilingual” in technology and business. 

Nine out of ten product releases are failures: they fail to meet their objectives. Mostly this is because the product manager's role has been poorly defined or the person is incapable of filling that role. Hence, many CEOs wonder what characteristics to look for when hiring product managers.

In fact, a wide array of attributes and skills is needed. Essentially, product managers solve customers' problems, and this requires them to understand their customers and empathize with their problems as well as be intelligent and insightful enough to come up with solutions.

Product managers must also be engaging and versatile communicators. They must interact with lots of different stakeholder groups from engineers to executives, and hence need to be "bilingual" in the sense that they understand both the technology and business sides of the product.

Finally, product managers bear full responsibility for delivering products as promised. They face long hours at work and a constant overload of tasks, so they need to have a strong work ethic and good time-management skills. Above all else, they must know how to prioritize. Their mantra is: "The main thing is to keep the main thing the main thing."

So where can you find such people?

A great and obvious source is within your own company, where it is easier to find and assess candidates just by asking around. Look everywhere: Great future product managers can be waiting in virtually any department, from engineering to customer service to marketing. Typically, they will have already expressed some interest in playing a stronger role in the product. Once you find them, train and mentor them to fill the role successfully.

**Great product managers are intelligent, focused and "bilingual" in technology and business.**

### 2. Product managers need product teams around them with clearly defined roles. 

A product manager has two main responsibilities: evaluating product opportunities and defining the products to be built to address those opportunities. To allow her to focus on these tasks, the product manager needs a product team around her, much like a CEO needs a management team to run a company.

This team comprises several well-defined roles:

_User experience designers_ create the product's interface toward the customer. This crucial role means they must work closely with the product manager.

_Engineers,_ also a vital component of the team, build the product that the product manager has defined. This makes them the product manager's peers, not subordinates. To help engineers understand what the product needs to do, the product manager must involve them early in the process and have them actually meet and observe real customers.

A dedicated _project manager_ is also recommended to oversee the work of engineers in the execution phase. This allows the product manager to focus on discovering and defining new products while the project manager schedules and tracks the projects required to build and launch them.

A separate _product marketing person_ is needed to tell the world about the product. Naturally, both she and the product manager should give each other input to help formulate marketing messages and product requirements respectively.

Finally, every product team can benefit from seeking out the smartest people in the company, regardless of function or position, and making them unofficial "deputy product managers." If asked for ideas and feedback, they can deliver a surprising amount of value just based on their smarts.

**Product managers need product teams around them with clearly defined roles.**

### 3. Make user experience design a priority: Build a complete UX team and help them contribute. 

Good user experience (UX) is a vital component of good products. The iPod, for example, owed much of its success to the pleasant and effortless UX.

To create a good UX, the product manager must work intensively with a UX design team, which ideally comprises four essential roles:

An _interaction designer_ works to understand the target users' requirements and thinking, and then creates a wireframe design of the product based on this understanding. After this, _visual designers_ contribute the look and feel of the user interface on top of the wireframe. Essentially, these two roles create the user experience, making them a key part of the product team. They should be involved early on, taking part in everything from deciding on the product strategy to participating in customer visits.

Note that the importance of visual designers is often sadly underestimated. Their work has the power to evoke emotions in the customers, and if Apple has shown us anything, it is that products that evoke strong emotions, like love and craving, tend to be smash hits.

In addition to these two roles, you need a _rapid prototyper_ who can quickly create prototypes of the product and a _usability tester_ who then has users test the product, thus generating input for another iteration of the design process.

To allow her UX design team to fully contribute, a product manager must let them complete their work _before_ tasking engineers with building the product. This is because good designers want to experiment with multiple designs, but the engineering process is not agile enough to facilitate this.

**Make user experience design a priority: Build a complete UX team and help them contribute**.

### 4. A product manager must constantly seek out and assess product opportunities. 

Opportunities for new products are everywhere around us, and new ideas, innovative technologies and shifting competitive landscapes create fresh opportunities every day. They exist even in seemingly saturated markets. Remember AltaVista and Infoseek? Their dominance of the search engine market seemed unassailable until Google came along.

In this endlessly shifting landscape, a product manager must constantly and quickly evaluate product opportunities and decide which ones to pursue. To help achieve this, she should answer a list of questions used to help define and communicate such opportunities, called a _Product Opportunity Assessment_ (POA):

  * What problem will this solve?

  * Who are we solving a problem for and how big is that market?

  * What alternatives do competitors provide and why are we the ones who can succeed in this?

  * What factors are critical to success (e.g. partnerships with distributors)?

  * What metrics will we use to measure our success?

  * Why is this the right time to enter the market and what is our go-to-market strategy (i.e. how will we sell the product)?

  * Based on the above questions, is this an opportunity we should pursue?

This light and easy-to-understand tool can greatly help analyze and communicate product opportunities without resorting to rigid and overly long documentation methods. Its results can then be discussed with senior management and a clear go/no-go decision can be reached, thus helping the product manager focus her efforts.

Note that all questions in the POA are geared toward understanding the opportunity, not presupposing a solution. Only after finding a product opportunity to pursue does a product manager begin the _product discovery_ phase: defining the right product to fit that opportunity.

**A product manager must constantly seek out and assess product opportunities.**

### 5. Product discovery: Validate that your product is valuable, usable and feasible by defining and testing a minimal product. 

To be successful, a product must be:

  * _feasible_ : your engineers must be capable of building it.

  * _usable_ : customers must be able to use it.

  * _valuable_ : the product itself must deliver value to customers, so they want to buy it.

All three criteria need to be tested and validated _before_ building the product. To facilitate this, the product manager and her UX interaction designer should first come up with a _minimal product_ prototype: one that has the bare minimum functionality necessary to be valuable, and yet delivers a realistic enough user experience for real users to test.

Involve an engineer in defining the minimal product so they can gauge its feasibility and help the product manager decide which features to cut and which to leave in. This ensures the engineers are confident that they can implement the features that remain, and hence no more cutting is required later on. In short, the product should now be feasible. Once a minimal product is arrived at, a prototype can be built and tested with real users to validate whether it is usable and valuable.

Once you've validated a product and delivered the specifications to engineering, you must make a fundamental mindset shift from _product discovery_ to _execution_. There should be no further changes to the product specifications after this.

If management requests changes after this point, don't lose focus on execution but rather start a new discovery process for version 2.0 of your product to run in parallel with building version 1.0.

**Product discovery: Validate that your product is valuable, usable and feasible by defining and testing a minimal product.**

### 6. Use high-fidelity prototypes to convey product specs effectively and to test your product on real users. 

A key responsibility of a product manager is to deliver accurate product specifications to the engineering team. The most effective way to do this is to build a _high-fidelity prototype_ : a prototype with minimal functionality, but a realistic user experience. In the case of simple websites, for example, the prototype may just comprise clickable buttons that lead to different pages.

The end-goal is a prototype that anyone can easily interact with to understand the product, without necessarily having to look at any product specs.

In addition to being a handy tool for communicating product specs, a hi-fi prototype allows product managers to immediately test the product with real users. The sooner this happens, the easier it is to adjust the product according to the feedback.

So how does this testing happen?

When setting up a prototype test, first of all you'll need test subjects. Gather them from whatever sources you can: friends, family or Craigslist recruits. All are fair game.

Second, be sure to prepare thoroughly beforehand. Define what tasks you want the users to complete, focusing on the tasks they are expected to spend the most time on.

During the test itself, the less you say, the better. Don't taint the test subjects' experiences with your own expectations. Your goal is to observe and understand where your model might be inconsistent with how a user thinks. Just watch: Can they complete the tasks easily? Do they find what they're looking for?

If you identify obvious problems after the first test subjects, fix them immediately. This way, the next users can already test your new solution.

**Use high-fidelity prototypes to convey product specs effectively and to test your product on real users.**

### 7. Use a charter user program to understand your customers and gain references for your launch. 

If you're a product manager, understanding customers is a critical part of your job. In fact, it's so important that you should attend every site visit, customer interview and usability test as well as use whatever market-research tools are available to really gain a deep understanding of customers.

Note, though, that customers are notoriously poor at expressing what exactly they want from a product. Therefore, don't let their input directly steer the course you take with your product. Instead, stay focused on identifying customer needs, and think for yourself how to address those needs.

A great way to gain deep customer insights is a _charter user program (CUP)_. Basically, this is a program where you find and recruit 8–10 customers from your target market who suffer from the problem your product aims to solve. You then work with these customers as partners to develop and test a product to address this issue. Your end goal is to develop a product that works for your whole target market, so don't fall into the trap of making a specialized product for just one or two of your charter users.

Using a CUP is beneficial for the participating customers because they gain early — usually free — access to the product. Remember, it solves a painful problem for them, so the earlier they can use it, the better for them.

You, on the other hand, gain extensive access to users from your target market, allowing you to rapidly test prototypes of your product and refine it accordingly. Eventually, when you launch the product, you can realize the second big advantage of a CUP: use the participants as satisfied customer references, which are essential for any successful product launch.

**Use a charter user program to understand your customers and gain references for your launch.**

### 8. Product management is about making choices: Use personas and product principles to help you with this. 

Effective product management means constantly prioritizing and making choices: Which product opportunities should be pursued? Which features should be included in the product? Which tasks are most important? These decisions are difficult because, typically, every stakeholder in the company will feel strongly and differently about them.

To help resolve these tradeoffs, you should define a set of _product principles_ : your beliefs of what's truly important to the strategy of this entire product line and the company. In the case of a start-up, the company's mission statement often embodies these principles.

Despite their strategic nature, the principles need to be specific enough to help guide any decisions regarding features, target customers, etc. For example, if an online auction provider has decided that its primary product principle is safety, then it should not hesitate to increase security measures for customers, even at the cost of usability.

Another excellent tool for making difficult product choices is the use of _personas_ : fictional user profiles of (imaginary) typical customers. These personas are created by the product manager and interaction designer based on what they know about their potential customers.

For example, you could say that your potential customers are "Marys" (professional, tech-savvy women in their 30s) and "Freddys" (male students with limited income). Depending on whether you prioritize "Marys" or "Freddys," you will likely choose very different features to include in your product. Such simple archetypes help align the whole product team on who the customer is and what is important for them. Just make sure that at some stage, you also talk to real users and verify your personas are accurate!

**Product management is about making choices: Use personas and product principles to help you with this.**

### 9. When you make changes to your product, think carefully about what you want to achieve and deploy them gently. 

When improving existing products rather than launching new ones, many product managers fall into a modus of just fixing bugs and adding features as requested. But bolting on features indiscriminately to please specific customers often makes the product less appealing to the rest of the market, hurting its success in the long run.

Just as with new products, product improvement should start with an understanding of what your goals are. You must understand the product's most important business metrics and gear any improvements toward affecting those metrics. Say your product is a website where a key metric is what percentage of visitors become customers — currently 5%. You may then introduce a new feature to raise that percentage, like an easier sign-up process. If the percentage rises to 10%, you've just added a very valuable feature!

An especially valuable time to make changes to a product comes _right after_ the product launch. Lots of lessons can be learned in the first week when the product is actually live. During this time, consider yourself in "rapid response" mode, addressing issues or lagging key metrics quickly.

When improving products, be careful that you don't unwittingly _abuse your users_, i.e. release changes that they don't appreciate. To avoid this, ensure you deploy changes _gently_. This means you inform customers of changes well in advance and make the transition as painless as possible. Redouble your quality assurance efforts, and for major changes, consider running the old and new versions of your product in parallel, at least initially, allowing users to opt in as they please.

**When you make changes to your product, think carefully about what you want to achieve and deploy them gently.**

### 10. Final summary 

The key message in this book:

**A product manager's main responsibilities are assessing product opportunities and defining products to address those opportunities. The product manager and her team must ensure the products launched are feasible, usable and valuable, by creating a prototype of the product and testing it with real users as soon as possible.**

The questions this book answered:

**What are great product teams made of?**

  * Great product managers are intelligent, focused and "bilingual" in technology and business.

  * Product managers need product teams around them with clearly defined roles.

  * Make user experience design a priority: Build a complete UX team and help them contribute.

**What are the best practices of building great products?**

  * A product manager must constantly seek out and assess product opportunities.

  * Product discovery: Validate that your product is valuable, usable and feasible by defining and testing a minimal product.

  * Use high-fidelity prototypes to convey product specs effectively and to test your product on real users.

  * Use a charter user program to understand your customers and gain references for your launch.

  * Product management is about making choices: Use personas and product principles to help you with this.

**How can you go about improving existing products?**

  * When you make changes to your product, think carefully about what you want to achieve and deploy them gently.
---

### Marty Cagan

Marty Cagan has been involved in defining and building some of the most successful products of our time at eBay, AOL and Netscape Communications. He is a founding partner at Silicon Valley Product Group, where he helps companies develop their product organizations and strategies to create great products.

