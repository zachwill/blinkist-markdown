---
id: 5623b6013264660007110000
slug: intelligent-disobedience-en
published_date: 2015-10-19T00:00:00.000+00:00
author: Ira Chaleff
title: Intelligent Disobedience
subtitle: Doing Right When What You're Told to Do Is Wrong
main_color: E55F2E
text_color: B24A24
---

# Intelligent Disobedience

_Doing Right When What You're Told to Do Is Wrong_

**Ira Chaleff**

_Intelligent Disobedience_ (2015) offers insight into why we're so quick to follow orders — even when we know we shouldn't. It gives you all the tools you need to effectively resist the rules, regulations and orders that you know are wrong or harmful — without putting yourself at risk.

---
### 1. What’s in it for me? Learn when disobedience is the right choice to make. 

When a policeman tells you what to do, do you do it? Most likely yes, you jump to it. What about an air steward who tells you to switch off your cellphone just before takeoff — do you? Yes, of course. In both cases you obey their commands. 

And society expects you to. In order to facilitate its smooth running, we are encouraged to do what people, especially those in positions of authority, tell us. But should we?

These blinks show you that, whatever society might suggest, it is often necessary or even right to disobey an order, especially when that order could lead to harm and disaster. By understanding the right conditions for disobedience, we can make the world a better place.

In these blinks you'll discover

  * why you shouldn't take medical instructions from the hospital janitor;

  * why Intelligent Disobedience can help stop plane crashes; and

  * why you should teach your children to question rather than obey.

### 2. We should follow orders, but only when they are reasonable, constructive and based on legitimate authority. 

Would you consider yourself to be an obedient person? Most people will say: "No! I make decisions for myself." But imagine that you're working in a nuclear plant, and your boss hurriedly tells you to shut off the reactor. Would you do it? Most likely you would, without any hesitation. 

But why would we be so quick to follow orders in this particular case? Well, certain situations require obedience, while others don't. For a system to demand your obedience, it must meet particular conditions. 

First, systems that require obedience should have rules and orders which are "reasonably fair," meaning both that the system is based on moral principles and that it can function properly.

In contrast, systems whose rules are arbitrary or lead to negative outcomes (such as the suffering of others) should not be obeyed.

Second, the person who gives the orders must hold their position of authority over others legitimately, and act competently.

For example, a senior doctor who gives a junior colleague an instruction during a brain operation has both competence and legitimate authority. If the junior doctor ignores her expertise, the consequences could be severe. But if the hospital janitor gives the junior doctor instructions on brain surgery (or if a senior doctor gave the janitor instructions on how to best clean the floors), then their orders aren't based on competency or legitimate authority.

Finally, the order should be constructive.

If obeying an order would cause more harm than good, then it shouldn't be followed. Complying with immoral orders is wrong, and the person acting upon them can be held responsible for any harm caused by their actions.

For example, those who carried out Nazi war crimes tried to hide behind the fact they were "just following orders." This plea was ignored, and many were convicted for their terrible deeds.

### 3. You have the right to disobey orders that will lead to disaster. 

Obedience to laws, orders and regulations is a cornerstone of our society. We are expected to stop at red lights, pay taxes and keep the music down at 2 a.m., and normally we adhere to these rules. 

However, as the last blink showed, there are situations in which obedience is dangerous, or even immoral. 

While most cultures operate under the assumption that obedience is good and disobedience is bad, there are plenty of occasions when this kind of binary thinking is insufficient. 

Imagine, for example, that a nurse receives an order from a doctor to give a drug to a cardiac patient, but the nurse knows that the drug will cause the patient to die. The nurse informs the doctor about his concerns, but the doctor stubbornly insists. Is the nurse obligated to administer the drug anyway? 

Of course not. It would kill the patient! This sensible refusal to comply with an instruction is called _Intelligent Disobedience_.

But the nature of our social organization causes many of us not to exercise this right. Instead, most people find it simpler to obey bad orders than to face the wrath of an authority figure. 

In practical terms, it's often better to comply with a bad order, as saying "no" could cause us to lose our job altogether. 

For example, if you're a marketing director and the CEO orders you to increase all product prices by 25 percent, you're likely to comply even though you know that this move will hurt the relationships with existing customers and cost the firm loads of money.

But saying no could mean losing projects or, even worse, your job! Saying yes to this terrible idea is simply easier — in the short term, at least.

It's times like these when Intelligent Disobedience shines. Our remaining blinks will reveal how.

### 4. Intelligent Disobedience is about evaluating the source, aim and consequences of an order before obeying it. 

So far, we've established that people not only have the right but also the obligation to disagree with and refuse bad orders. But how do you know which orders are ill-thought but harmless and which are genuinely harmful?

If you want to know whether applying Intelligent Disobedience is justified, ask yourself:

Does the order come from a legitimate source? Or is the legitimate source missing important information that is relevant to the rule or order? Think back to the janitor and the junior doctor. Did the janitor have legitimate authority in matters concerning brain surgery?

Is the aim of the order itself wrong? Or if the goal is just, will the order actually achieve that goal? If, for instance, your friend tells you that you should turn off your TV to make it stop raining, you can safely apply Intelligent Disobedience. 

Finally, will the order have severe moral consequences? Is it likely to cause serious harm? For instance, if the employee of a fumigator firm is ordered to fumigate an inhabited house, she should probably say no. That's dangerous!

As you can see, Intelligent Disobedience is very narrow, and is not the same as _civil disobedience._

Civil disobedience involves the intentional disruption of the system as a whole and aims to incite similar acts of disobedience. Intelligent Disobedience challenges only the particular order in question.

However, in cases of truly unfair systems, civil disobedience may actually constitute Intelligent Disobedience. 

For example, in pre-civil rights America, when it was against the law for some doctors to treat black patients, those who ignored the order were committing civil disobedience that was also Intelligent Disobedience.

### 5. Leaders should value Intelligent Disobedience and encourage others to express themselves. 

Have you ever worked at a job where the manager took any attempt at constructive criticism as a personal attack? This type of leader, as well as being a nightmare to work with, dissuades Intelligent Disobedience. Teams with no opportunity to disagree intelligently are very unhealthy.

Intelligent Disobedience helps the team as a whole perform to the best of their ability. Consider the nurse who refused the doctor's orders to give a potentially harmful drug to a patient. If he hadn't refused or at least questioned the order, there is a chance that the patient would have died, and it is the doctor who would have been held responsible for medical malpractice. 

But in order for Intelligent Disobedience to work in a way that means the whole team benefits, it must be communicated in a constructive way.

The authority figure has to understand _why_ you're disobeying and trust that you are acting in everyone's best interests. If you can't get this across, then you'll only breed distrust, which weakens the team.

In order to keep your relationship with your leader healthy, you need to effectively communicate why you are questioning or disobeying the order that you have been given.

You may be completely right: writing a sales report the week before Christmas (when the company makes half of its annual sales) may make no sense. 

But if you don't communicate that to your superiors, and instead ignore or simply "forget" the order, then they will not understand why you disobeyed them, and may instead simply think of you as disloyal or rude.

If you have to apply Intelligent Disobedience, _always_ make it clear that you won't be carrying out the order and explain the reasons why. It's the only way your superiors will understand that something is amiss.

### 6. Be aware of your situation so that you can better judge how to express your concerns. 

Now that you know _why_ you should apply Intelligent Disobedience, it's important to be familiar with the different strategies available to you.

But before you start, you should build up your _situational awareness_ so that you know _when_ to be intelligently disobedient _._ The more you know about your situation and your surroundings, the better you can assess whether the order you have been given is worthy of being followed — or not.

To illustrate this, imagine that a soldier has been ordered to fire on a specific target. However, because of his keen awareness of his situation, he knows that the "enemy" is in fact one of his fellow soldiers! Naturally, he applies Intelligent Disobedience and disobeys the order. 

If he hadn't been paying attention to the world around him, then he could have committed a terrible mistake!

But what if _you've_ been given a bad order? How should you make your intention of intelligently disobeying known?

There are two ways of communicating this to your superior.

If the situation isn't urgent, then you should use _mitigating language_, which is "soft" or "mild," when communicating your disobedience.

This communication style is helpful in managing social and professional relations. For example, if your spouse is driving and you're pretty sure they're going the wrong way, you don't want to just grab the wheel to correct them. Instead, you could ask "Are you sure this is the right way?" This gentle, non-challenging question is perfect for correcting small mistakes. 

_Assertive language_, on the other hand, is useful when the situation is potentially or immediately harmful. In these cases, you have to be much more clear and take decisive action to avert disaster.

If a plane is about to leave the runway, for example, any safety concerns that the first officer has must be expressed assertively and immediately, in a way that demands action. One pre-flight mistake could spell disaster for hundreds of people.

### 7. We should learn Intelligent Disobedience at an early age. 

Learning to obey authority and social norms is a crucial part of children's education. However, children should also be aware that there are some situations in which the rules shouldn't be followed. 

Unfortunately, the institutions where children spend a lot of their time (like church and school) require strict obedience and deference to authority figures. But if children _only_ learn to obey, then the consequences can be drastic.

We can see this in a 2004 abuse case surrounding an 18-year-old McDonald's employee named Louise Ogborn. Her assistant manager received a phone call from someone claiming to be a police officer, who made allegations that Louise has stolen a purse. Under this pretence, Louise was submitted to all kinds of abuse, such as being shut in a storage room naked and ultimately being sexually assaulted by the assistant manager's husband, who was issued instructions by the "police officer." 

The actions of the assistant manager and her husband show the dangers of not questioning orders critically. Why would a police officer ask someone to look after, and interrogate a suspect themselves? Why did the husband simply follow the dreadful instructions from someone on the other end of a phone? 

Society will be much better off when people are able to think critically and enact Intelligent Disobedience when presented with orders that are harmful or make no sense. This has to be taught at an early age.

Asking children questions like "Why do you think I'm telling you to do that?" or "Can you think of another way for us to do X?" will replace subjection with understanding.

Guide dogs are a good way to think about this. Guide dogs have to follow orders from their owners, _unless_ they know the order would put them both in danger. If the human tells the dog to go forward when there's a bottomless sinkhole right in front of him, the dog won't budge.

Kids too need to know when to follow, and when to push back.

### 8. Final summary 

The key message in this book:

**While we have been socially programed to respect authority and follow orders, not all orders should be obeyed. Sometimes you have to apply Intelligent Disobedience in order to help leaders stay consistent with everyone's goals and values.**

**Suggested** **further** **reading:** ** _Drive_** **by Daniel Pink**

In _Drive_, Daniel Pink describes the characteristics of extrinsic and intrinsic motivation. He reveals that many companies rely on extrinsic motivation, even though this is often counterproductive. The book explains clearly how we can best motivate ourselves and others by understanding intrinsic motivation.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Ira Chaleff

Ira Chaleff is an author, executive coach and consultant who has been named one of the 100 "Best Minds on Leadership." His other works include _The Courageous Follower_ and _The Art of Followership._

