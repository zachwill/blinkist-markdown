---
id: 5a516061b238e10006337079
slug: into-the-magic-shop-en
published_date: 2018-01-08T00:00:00.000+00:00
author: James R. Doty, MD
title: Into the Magic Shop
subtitle: A Neurosurgeon's Quest to Discover the Mysteries of the Brain and the Secrets of the Heart
main_color: 2EE5E3
text_color: 135E5E
---

# Into the Magic Shop

_A Neurosurgeon's Quest to Discover the Mysteries of the Brain and the Secrets of the Heart_

**James R. Doty, MD**

_Into the Magic Shop_ (2016) reveals details of Dr. James R. Doty's research into the symbiotic relationship between the heart and brain, and how the practice of meditation can strengthen this bond. Dr. Doty uses both scientific and personal insight to illuminate his studies and demonstrate how important it is to have a compassionate heart as well as a curious mind.

---
### 1. What’s in it for me? Learn the benefits of thinking with your heart as well as your mind. 

You may have read the words of spiritual gurus who've described the benefits of meditation, and you may have read about some of its medical advantages as well. With Dr. James R. Doty, you get the best of both worlds: a medical expert and a seasoned meditation practitioner who's been experiencing the transformative effects of meditation since he was 12 years old.

Dr. Doty's experiences have led him to the unique understanding that the human heart does more than just pump blood; it also shows its own "intelligence" in its communications with the brain. This connection can be strengthened by meditation to ensure that you're always making the best decisions in every aspect of your life.

In these blinks, you'll learn

  * how meditation can help you reach your goals;

  * which part of your body connects your brain and heart; and

  * why the compassionate will inherit the world.

### 2. Meditation relaxes the body, calms the mind and brings you closer to your goals in life. 

Those unfamiliar with meditation often have the misconception that it's about stopping your thoughts. Meditation doesn't stop you from thinking, but it can free you from unwanted thoughts that have too much control over you and cause tension.

Meditation has the power to change your life and make you into a more efficient human being. It does this through its two main components: relaxing the body and quieting the mind.

This process starts each time you sit down, either in a chair or on the floor, and relax your body one part at a time, from the toes on your feet to the hairs on your head. To keep your focus on your body and the present moment, and not be carried away by distracting thoughts, you can concentrate on slowly inhaling and exhaling. Some people prefer to focus on an object, like a candle's flame, or to repeat a special mantra.

Like any other skill, meditation takes time and practice, so don't be discouraged if it's a while before you're sitting peacefully in the moment and not engaging with those disruptive thoughts.

The more regularly you meditate, the more effective it will be, so it's wise to start out twice a day. And with constant practice, you'll soon start to notice the effects, even when you're simply going about your day. Eventually, the time will come when a negative or stressful thought will appear and just as quickly vanish while you remain calm and clear-headed.

Once you achieve this clear mind and relaxed body, you can begin to reap the rewards of _visualization_.

This is another facet of meditation; it involves picturing your ideal future self as a way to increase the likelihood of it coming to pass.

This too will take practice. At first, picturing your future self might feel like trying to see through a fogged-up window. But the more you work at it, the stronger your intentions will become until the image is imprinted in your brain. This clarity will make it feel more like a reality and therefore attainable.

### 3. For meditation to have a truly positive effect, you must follow your heart. 

While meditation can have life-changing benefits, it will be most effective when it's part of an overall lifestyle that emphasizes good intentions.

Since meditation strengthens the mind and its ability to focus on and realize your objectives, it can just as easily be used for bad as it can for good.

If you intend to make money at the expense of others and even through harming them, then meditation will mean your selfish desires are more likely to come true. This is why many stock brokers on Wall Street use meditation. And while their increased focus and reduced stress earn them more money, their selfish motives aren't making the world a better place.

If your goals are determined solely by your mind and not by your heart, bad intentions can easily be formed.

For years, the author used meditation and visualization to be a rich, yet unfulfilled, doctor.

Since he was 12 years old, Dr. James Doty had been meditating on the goal of being rich, and even though he'd earned enough to be worth $75 million, he found little satisfaction. Eventually, he began to understand his mistake: he'd been leaving his heart out of the decision-making.

The truth is, wisdom isn't solely a matter of brains. True wisdom and intelligence come from the heart, just as much as it does from the mind. And in the next blink, we'll take a closer look at this unique relationship.

> _"We can create anything we want, but it is only the intelligence of the heart that can tell us what's worth creating."_

### 4. The heart informs the brain and can change the way it functions. 

It's hard to underestimate the importance of the brain since it's responsible for keeping the body functioning in harmony. But, of course, without a beating heart, it wouldn't do much good.

While both of these organs are important, research has revealed that the heart actually sends more information to the brain than the other way around.

Likewise, most people find that their emotions are more in control of their thinking than their thoughts are on top of their emotions — an imbalance that meditation is extremely helpful in fixing.

The author has raised some eyebrows by suggesting that the signals being sent from the heart to the brain are the emotions that inform the brain's thoughts. After all, as he points out, strong emotions can lead to frantic, ceaseless thinking, and, while rationalizing can make us feel a bit better, it doesn't dismiss those feelings altogether.

But here's the thing: meditation can change the way you feel by altering the interactions of your heart and brain.

The _vagus nerve_ is an important part of the body's nervous system that links heart and mind via a connection at the brainstem. One of the functions of the vagus nerve is to carry messages to and from the heart when a threat arises. When blood pressure increases in response to the threat, the heart sends signals to the brain through the vagus nerve to trigger our fight-or-flight response.

Now, if we use meditation to train ourselves to keep our breathing under control in any circumstance, our heart rate will not increase as dramatically, and no message will be sent to the brain to enter fight-or-flight mode, allowing us to remain relaxed. Over time, the brain learns new patterns of response and we'll be calmer and less stressed in our daily lives.

Ultimately, you have more influence than you think over the form and function of your brain, and by practicing meditation you'll be making sure the brain gets the best messages the heart has to offer.

### 5. Let compassion inform your decisions and make your life more rewarding. 

Humans are social beings with an inherent desire to connect with other people. At the core of this desire is compassion, the driving force behind our social instincts.

Compassion can also inform the decisions you make, whether those decisions are the result of an emotional response or rational consideration.

We think of compassion as being tied to emotions, but we can also see it as the motivation behind the analytical work in science and medicine.

Surgeons are taught to remain calm and focused and reduce errors by keeping their emotions away from the operating table. But this doesn't prevent doctors such as the author from remaining compassionate when they're performing an operation. Dr. Doty will still feel an intense desire to help, even when he's being coldly analytical, and it makes his work even more precise and focused.

Compassion is another benefit of meditation, as the practice will open your heart and your natural desire to connect with others.

Certainly, you could try to meditate without opening your heart, but this will lead to an unpleasant experience of over-analyzing yourself. You'll find meditation much more enjoyable with an open heart because then your brain will tap into your natural desire to be compassionate and to relate to others.

For further evidence of our natural inclination toward compassion, neuroscience shows that when someone gives a gift, the pleasure center of the giver is more active than that of the receiver.

While it's true that being open-hearted has its risks, since it makes us vulnerable to the cruelty in the world, it's still a risk worth taking. It's far more rewarding to live a life open to the joys that come with relationships than to close yourself off to the world.

In the next blink, we'll explore some of the very practical benefits of compassion.

> _"When we go inward, and our heart is open, we will connect with the heart, and the heart will compel us to go outward and connect with others."_

### 6. It is better and healthier to live with compassion for others. 

Maybe you've heard the saying "nice guys finish last," but a more accurate statement would be that the compassionate people will be the last ones standing.

A lot of people base their decisions on the mantra "survival of the fittest," which they interpret as requiring selfish and ruthless behavior. But human survival is all about compassion.

Forming communities and being social has led humanity to succeed over the years, not isolationism and self-centered behavior. It's not difficult to understand that people live longer lives when they're part of a group of people who look after one another and are there to help in emergencies.

Another common belief is that "survival of the fittest" means working around the clock and putting yourself through endless stress. Yet this kind of stress takes a deadly toll on the heart. Never-ending stress is just another way of saying the body is in constant fight-or-flight mode, and this is one of the leading causes of cardiac arrest.

A healthy heart is one that is part of a relaxed body, with a heartbeat that varies and isn't under relentless stress. And the way to achieve that healthy state is through a combination of compassion, meditation and visualization.

This combination is really an unstoppable kind of magic, so it makes perfect sense that the author discovered it by walking into a magic shop when he was just 12 years old.

The woman who worked in the shop could see that he was a troubled little boy, so she taught him the secrets of relaxing your body, quieting your mind and visualizing your goals — all while having an open, empathetic heart. Since then, Dr. Doty has found these core principles to be a magical combination that can unlock endless doors.

Now it's time for you to learn this magic and pass it on to someone else. Imagine the harmony and peace that could be achieved if everyone taught at least one other person!

> _"We get sick alone, and we get well together."_

### 7. How to meditate. 

We've learned a lot about the benefits of meditation, but how exactly do you meditate?

There are four parts to meditating successfully: _freeing your body of tension_, _gaining control over your mind_, _allowing your heart to open_ and _setting a clear intention for your practice_.

Before you begin, remember that it's not worth trying meditation if you are in a state of stress, are under the influence of any substances, or if you're tired.

With that in mind, the first thing you should do is sit comfortably somewhere quiet where you won't be interrupted. Then relax.

Think of your intention — something you want to achieve in your personal or professional life. Once you have a clear intention, relax your body by taking three deep breaths in through your nose and out through your mouth. Then imagine you are looking at your seated self, and begin to relax the muscles in your toes and feet, gradually moving on to relaxing the whole body. Do not be angry with yourself if you feel so relaxed that you fall asleep — it takes practice to maintain a wakeful state of relaxation. After you've relaxed your whole body right up to your facial muscles and scalp, imagine relaxing your heart muscle as you breathe.

Naturally, it is hard to stop your mind from wandering during all of this. Thoughts will constantly pop up as you're trying to relax. When they do, don't cling to them, but gently let them go and return to focusing on your body.

Once your body is relaxed and your thoughts are still, you can call to mind someone you love unconditionally, and someone who has unconditionally loved you, and enjoy that feeling of warmth. Now think of someone with whom you have a more complicated relationship and try to see yourself in them — open your heart to them and bathe them in that warmth.

Perhaps during the same meditation, clear your mind again and call up your intention, see yourself achieving it and try to add more details to the picture. Feel the emotions that come with that feeling of accomplishment. Try to spend ten to 30 minutes a couple of times a day with your intention, and you will find that the picture becomes clearer and clearer.

When you have finished meditating, open your eyes and sit present and free of thought for a few minutes to ease yourself back into the world, letting what you have just experienced resonate with you.

### 8. Final summary 

The key message in this book:

**Through the practice of meditation, we can let more compassion into our lives and begin to think more with our heart and not just our mind. This will not only lead to a healthier and calmer life, but also to a more rewarding existence for all of us, making the world a better place.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Why Zebras Don't Get Ulcers_** **by Robert M. Sapolsky**

_Why Zebras Don't Get Ulcers_ (1994) vividly explains the biology behind stress and its impact on our lives, functioning as an effective way to deal with immediate problems, while also posing serious health risks in the long run. The author also offers plenty of practical tips on how to keep stress under control.
---

### James R. Doty, MD

James R. Doty, MD is a neurosurgeon and professor at Stanford University's Department of Neurosurgery. He is also a philanthropist and the director and founder of the Center for Compassion and Altruism Research and Education, which proudly includes the Dalai Lama among its benefactors.

