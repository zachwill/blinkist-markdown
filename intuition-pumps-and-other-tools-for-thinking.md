---
id: 53eb45043634330007490000
slug: intuition-pumps-and-other-tools-for-thinking-en
published_date: 2014-08-12T00:00:00.000+00:00
author: Daniel C. Dennett
title: Intuition Pumps and Other Tools for Thinking
subtitle: None
main_color: None
text_color: None
---

# Intuition Pumps and Other Tools for Thinking

_None_

**Daniel C. Dennett**

This book presents several complex philosophical issues, and offers "thinking tools" that make them easier to understand. These thinking tools have a wide range of applications, from improving your debate skills to understanding the concepts of consciousness and free will.

---
### 1. What’s in it for me? Learn to think like a philosopher. 

Sometimes philosophical topics are so complex or abstract they can make your head spin. When it's difficult to grasp a topic, it's even harder to form a well-rounded, substantiated opinion about it. For instance, can you come up with a definition of "consciousness"? What about "free will"? What's your opinion on these issues?

Thankfully, there are "thinking tools" that can break down these abstract issues and jumpstart your thinking. These blinks explore some of the best thinking tools for a better understanding of complicated topics, and gaining new and deeper insights into the world around you.

In these blinks, you'll discover:

  * what a technical device has in common with a human, 

  * how to identify and respond to illogical statements,

  * why creatures as simple as termites can build complex castles,

  * why different people can interpret the same word in vastly different ways, and

  * what the game "rock, paper, scissors" can teach you about free will.

### 2. An intuition pump is a thinking tool that breaks down complex concepts. 

Thinking about complex problems can often be quite exhausting. Fortunately, philosophers have developed certain "thinking tools" to make this easier. Thinking tools come in many forms, such as illustrative examples, metaphors or special thought experiments called _intuition_ _pumps_.

So how do intuition pumps work? Well, they clarify abstract problems by making them easier to understand. For example, consider the question "What is freedom?" "Freedom" can be a rather difficult idea to grasp. Is it being able to make whatever decision you want? Is it knowing or fulfilling your capabilities?

We can use an intuition pump here, by imagining a concrete situation that makes these questions more tangible. Imagine a group of prisoners trapped in a jail. Every night, the prison guard unlocks the prisoners' cells for a few hours while they sleep, but they aren't aware of this.

Are the inmates "free" during those hours? They're physically able to leave their cells, but they don't know they have that opportunity, so they can't devise an escape plan. This intuition pump illuminates the complex nature of "freedom" to us, and helps us gain new perspectives.

You can tweak the intuition pump to emphasize different aspects of a problem. Let's say, for instance, that the guard _doesn't_ wait for the prisoners to fall asleep, and instead, he drugs them and unlocks their cells while they're knocked out. Now, not only are they unaware that their cells are unlocked, they aren't even able to realize that by waking up. This illuminates another aspect of freedom: you aren't fully free if someone else is actively limiting your freedom.

So an intuition pump can help us understand a concept as intangible as freedom. You might even gain new insights you hadn't previously considered.

### 3. A philosopher works with thinking tools, whereas a scientist works with facts. 

There are many similarities between scientists and philosophers — they're both known for asking challenging questions. Their purpose in asking these questions, however, differs quite a bit.

Generally, scientists aim to discover truth, whereas philosophers aim to make others reevaluate their beliefs. Many scientists dream of winning the Nobel Prize, because it's the highest recognition in their field. There's always competition between researchers who study the same topics, because everyone would like to be the first to discover a scientific truth.

By contrast, philosophers don't necessarily hope to find "truth." They compare and evaluate the ideas and attitudes of other thinkers, then try to develop new ideas to be evaluated. The ultimate goal for most philosophers is to make other people reflect on their own mindsets, even if those other people don't agree with their ideas or try to refute them.

Scientists and philosophers also have very different roles within their own field. Scientific discoveries are based on facts, which means they exist independently of their discoverers. For example, Nicolaus Copernicus was the first to prove mathematically that the earth revolves around the sun, which was an incredible discovery at the time. Today this fact seems obvious, and we can observe it easily with modern technology. Even if Copernicus hadn't existed, another scientist would've discovered this fact eventually. A scientific truth exists _before_ it is discovered.

On the contrary, philosophers, like artists, create new and individual works. The _Mona_ _Lisa_ would never have come into existence without Da Vinci himself. In the same way, philosophical ideas also depend on their creators. Philosophers take inspiration from others, but their ideas, attitudes and styles are still uniquely individual.

> _"Nobody said the truth had to be fun."_

### 4. Thinking tools can help you improve yourself. 

It goes without saying that all humans make mistakes — even the most brilliant thinkers. Most people view mistakes negatively, but actually, mistakes can serve as very useful tools for self-improvement.

Though everyone makes mistakes, almost no one likes to admit to them, and most people prefer to hide them. Instead of trying to hide your mistakes, get the best out of them by facing them.

For example, consider a child who chooses to play and neglect her homework one night. If her teacher unexpectedly gives the class a test the next day, she'll learn that in the future it's better to study each night just in case.

This ability to learn from our mistakes is part of our evolution. When a creature can learn from its mistakes, it's more likely to survive and pass on its genes, whereas those who can't are more likely to be weeded out by natural selection. So learning from your mistakes is a critical thinking tool that's part of your biology.

Another useful thinking tool is explaining your ideas to non-experts. This can help you improve the clarity and delivery of your thoughts.

Have you ever listened to two experts from the same field speaking to one another? Most non-experts can barely understand a word. Sometimes the ideas become so complex that the experts themselves can't fully understand each other.

So when you have expert knowledge of something, test yourself by trying to explain it to a non-expert. You'll be forced to find simpler and more direct ways of making your points, which will help you in expressing your ideas overall.

These thinking tools — learning from your mistakes and expressing yourself to non-experts — are very beneficial, so be sure to use them often.

> _"The chief trick to making good mistakes is not to hide them — especially not from yourself."_

### 5. Thinking tools can help you identify illogical statements. 

Humans are extremely social creatures: we spend much of our time communicating. It's vital to be respectful and logical when we communicate, as this is such a fundamental part of who we are.

Certain thinking tools can help you identify illogical statements. Before you criticize someone's thinking, however, make sure you're being respectful. A thinking tool called "Rapoport's Rules" outlines some steps for dealing with someone who has a different mindset to you:

First, listen carefully to their position and make sure you understand it. Next, point out the parts you agree with, and what you've learned from them. Finally, you can start to criticize their logic.

This ensures that your criticism is constructive, and it'll make the person more likely to listen to your position too.

One useful thinking tool for exposing illogical statements is the "_reductio_ _ad_ _absurdum_," which means taking a precarious assertion to its extreme conclusion. For example, Sir John Eccles, a neuroscientist, once asserted that the neurotransmitter glutamate contains "the mind." So would it be murder to throw out a bottle of glutamate? Clearly Eccles' statement overreaches itself.

Certain words, like "rather," can also signal illogical statements, so get used to recognizing them. Consider the assertion, "Pigs needn't be treated as living beings. Rather, they're food for humans." The "rather" makes it seem like there are only two options, but actually, other options exist. We can use pigs for food _and_ treat them as living things, for instance.

The word "surely" also often indicates that something is illogical. It's often used as assurance that the following statement is so obviously true that it doesn't need any evidence. So make sure that "surely" isn't covering up something unsubstantiated.

With the appropriate thinking tools, you can train yourself to catch illogical assertions, and debunk them in a way that's respectful and effective.

### 6. Thinking tools can illuminate how meaning develops, and how it depends on our interpretations. 

Look around the room you're in. Can you give a name and meaning to most things you see? Assigning meaning to things is an essential part of our nature, and thinking tools can help us understand and develop that sense of meaning.

Words and their meanings aren't inherent, and thinking tools can make this clear. Have you ever studied a second language? If so, you know this well: it's necessary to practice new words over and over again in order to drill in their meaning.

A baby learning language goes through a similar process. The first thousand or so times it hears the word "mother," it won't connect this with its mother — the word is only a meaningless sound, just like a word in a language you don't know. But gradually, the baby will learn the meaning of "mother," as it starts to associate it with seeing her.

The fact that children sometimes parrot words or phrases they don't understand also illustrates this. For example, a young child might say, "My father is a doctor." Do they really understand what a "doctor" is? Probably not. But is it necessary for the child's development to understand it immediately? No, because the child's knowledge will continue to grow, and we know it will eventually encompass the word "doctor."

Meaning changes not only with words, but with the situation. Consider the wide range of meaning the sentence "Say that again and I'll kill you" could have. If a stranger said that while pointing a gun at you in a dark alley, you'd take it very seriously. If you overheard two children say that while laughing, you probably wouldn't be concerned, because of the playful situation.

The development of "meaning" is complex and it can differ greatly with context and interpretation, but thinking tools can untangle this.

### 7. The meaning of a word depends on its context, and we can examine this with thinking tools. 

Our understanding of a word, sentence or idea depends on the context we hear it in. A philosopher, Quine, developed a few thinking tools to clarify this.

His first tool, the "Quinian crossword puzzle," shows us how different words can express the same concept to different people. It's like a regular crossword, except that each question has at least two correct solutions. As a result, each person who completes it produces a different final product that depends on their own interpretation.

For example, think of a four-letter-word for a vehicle that travels on water. Did you think of ship? Or boat? Maybe raft? These are all valid answers, but they differ depending on your interpretation of "water vehicle." Likewise, if several people complete the same Quinian crossword, they'll produce different valid results that reflect their own understandings of the concepts presented.

Another one of Quine's intuition pumps shows us that our interpretations can differ not only on the same concept, but on the same word.

To understand this, imagine a remote island where the inhabitants only speak their own unique language. Two foreigners who don't know the language are stranded there. There are no translators, so the foreigners will have to learn the language only by observing and interacting with the native inhabitants.

The castaways are rescued, and when they return home, each decides to write a manual describing this new, exotic language. How would these two manuals compare? It's very unlikely that the two castaways would have translated each word into exactly the same word in their own native language. Again, each manual will be a valid interpretation of the same words, and they'll both be useful for understanding the island language, but they'll both also reflect their writers' unique interpretations.

### 8. Technical devices can be used as intuition pumps for understanding the concept of meaning. 

What do a human and a technical device have in common? Well, just like a technical device, humans need to process the information they take in, then respond to it.

A technical device takes in an external signal, then translates it into its own code so it can "understand" and respond. A human goes through similar processes — for example, if a person hears their second language, they'll take it in, translate it into their language in their head, derive the meaning and respond accordingly.

Imagine two separate electronic boxes connected by a wire. One box has two buttons, a blue and green, and the other has three lightbulbs: blue, green and red. The blue or the green light will flash depending on which of the colored buttons you press.

Next, imagine you change the code of one box. Now only the red light flashes when either button is pressed. Why? Well, each box has its own code. It's as though they speak different "languages," and the wire translates between them. If you change the code on one side, the translator won't be meaningful anymore, so the red light will flash. The same goes with language: you have to translate a foreign language or it's not meaningful.

Humans are also like technical devices because their actions are predictable. Imagine a chess-playing computer. We can predict what move it will make because it was programmed to analyze the pieces in a certain way, and will always use the same process. Though human decision making is more complex, we also learn to recognize patterns in peoples' thought processes, and then try to predict their actions in the future.

The similarities between humans and technical devices can give us a better understanding of peoples' actions, and how we ourselves interpret those actions.

### 9. Illustrative examples demonstrate that evolution follows a “design” without having an intelligent designer. 

Have you ever seen something in nature that looked almost man-made? Termite castles, for instance, sometimes look like incredible buildings created by humans. But while humans spend a lot of time planning those buildings, the termites just start building, seemingly without knowing the advantages of the shapes they create.

This example from nature is a thinking tool. It shows evolution following an intelligent design, even though its "designers" — the termites — don't understand this. For example, some gazelles have developed a leaping movement called "stotting" that allows them to escape their predators. If a predator is near, they'll start jumping, because the predators are less likely to hunt the gazelles that exhibit this strange behavior. It's not clear why the gazelles are safer when they stot, but it has a clear evolutionary advantage.

So just as the termites don't understand why they build in their specific way, the gazelles intuitively know they should stot without understanding the reason. Their evolution is "intelligent," although the individual termites and gazelles themselves aren't intelligent.

We can better understand this evolution, and also the role of genes, by using an illustrative example about books. If you read two different novels in the same language, you'll find that the vocabulary they use is very similar. The words themselves don't define the novel — what matters is what order they're in. Genes work in a similar way. All mammals have the same basic set of genes, just as all fluent speakers of a language have the same basic vocabulary. Instead of word order, genes vary in their interaction with each other, and that produces the differences between species, and individuals within a species.

Even if individual creatures aren't very intelligent, they're part of an evolutionary trajectory that's intelligent in itself, and thinking tools can show us this.

> _"There are reasons for the shapes created by the termites, but the termites don't have those reasons."_

### 10. The concept of “consciousness” is hard to grasp, but thinking tools can help us. 

If you're reading this, you have consciousness. Consciousness — or the awareness of our existence and environment — may seem simple or self-explanatory, but what exactly is it? Intuition pumps, such as thought experiments and examples, can shed some light on this elusive concept.

An illuminating example comes from people with _prosopagnosia,_ or the inability to recognize faces. In one study, prosopagnosia sufferers were shown pictures of the faces of people they knew, and given a list of names to choose from to identify the person. Because they couldn't recognize them, they could only guess. Interestingly, when they guessed correctly, researchers observed a response in their skin. They didn't consciously know that their guess was correct, but their body was somehow aware, and produced a response.

This example indicates that people have two systems for recognizing faces: a _conscious_ and _unconscious_ system. Clearly, the concept of consciousness _isn't_ self-explanatory, as some conscious and unconscious processes are linked.

A thought experiment can reveal another interesting characteristic of consciousness. Imagine that a scientist named Mary has lived her whole life in a room without colors, with only a black and white television connecting her to the world. She's researched vision extensively, and knows, for instance, what wavelength stimulates our retina when we see a tomato. She knows we call that color "red," but she's never seen a red tomato. When Mary leaves the room and sees a tomato for the first time, does she learn something new? Well, of course, because even if she knew all the facts, she didn't understand what the concept of "redness" actually looked like.

This shows that consciousness is about more than just knowing facts. Even if you're conscious of the color red, you can't fully understand it until you _experience_ it. Consciousness requires us to actively engage with the world around us.

### 11. Thought experiments show that even though our free will isn’t absolute, we do have some control of our actions. 

Are we truly free? Do we really make our own decisions, or does something else determine them? These are questions philosophers have struggled with for ages. Fortunately, thought experiments help with this.

One important thought experiment highlights the impact our environment has on our decisions. Imagine that scientists manage to create a human. He has his own thoughts, but the scientists can influence them. If the scientists influence him to commit a murder, is he responsible? It's unfair to blame him, because his environment — in which the scientists control him — has influenced him so much. However, _everyone's_ environment influences them. If we say that his environment was partly to blame, then surely we must apply that to _everyone_, so our free will can't be absolute.

We do have _some_ degree of free will, and the "rock, paper, scissors" game is a great thinking tool to explore it. At first glance, this game appears to be based completely on chance. Many philosophers argue that if a person has absolute free will, they should be able to make their actions totally unpredictable — so in a game like rock, paper, scissors, it shouldn't be possible to predict your opponent's choice. No one should really be "good" at rock, paper, scissors.

And yet some people _are_ good at it — there are even rock, paper, scissors tournaments. Some people are better at predicting their opponent's moves. Absolute unpredictability can't be achieved, so it seems we don't have free will. However, _practical_ unpredictability can be achieved. That means trying to choose arbitrarily and have no obvious pattern.

Since there is only _practical_ unpredictability, there must also be only _practical_ free will. In the end, it's a mixture of both: everyone is influenced by their environment, but we do have some practical free will over our choices.

### 12. Final summary 

The key message in this book:

**Even** **if** **you** **aren't** **a** **philosopher,** **you** **can** **reflect** **on** **complex** **concepts** **with** **the** **appropriate** **thinking** **tools.** **They** **can** **help** **you** **better** **understand** **the** **mindset** **of** **others,** **while** **also** **motivating** **you** **to** **create** **your** **own,** **well-founded** **opinions.** **They** **can** **offer** **you** **new** **insight** **into** **topics** **you** **might** **not** **have** **considered** **before,** **and** **help** **you** **form** **opinions** **you** **didn't** **know** **you** **had.**

Actionable advice:

**Use** **your** **mistakes** **to** **your** **advantage.**

Instead of trying to hide your mistakes, practice some self-scrutiny. View your mistakes as valuable learning experiences rather than something to be ashamed of.

**Don't** **believe** **everything** **you** **hear.**

Instead, try to detect and uncover illogical statements, by looking for rhetorical warning signs like _rather_ and _surely,_ which might conceal untenable reasoning.
---

### Daniel C. Dennett

Daniel C. Dennett is a philosopher, cognitive scientist and professor at Tufts University. His work ranges from philosophical topics such as consciousness, to scientific topics like evolution. He's written several successful non-fiction books, including _Darwin's_ _Dangerous_ _Idea_ , _Consciousness_ _Explained_ and _Sweet_ _Dreams._

