---
id: 5977314bb238e10005703355
slug: inventing-the-future-en
published_date: 2017-07-27T00:00:00.000+00:00
author: Nick Srnicek, Alex Williams
title: Inventing the Future
subtitle: Postcapitalism and a World Without Work
main_color: F43544
text_color: C22A36
---

# Inventing the Future

_Postcapitalism and a World Without Work_

**Nick Srnicek, Alex Williams**

_Inventing the Future_ (2015) is a radical manifesto for the political left. These blinks describe why the current political tactics of the left are failing, explain how neoliberalism has become today's dominant global ideology and propose a future based on full automation and a universal basic income.

---
### 1. What’s in it for me? Look at the current limits and future prospects of the political left. 

Have you ever participated in a demonstration or a protest rally? Or maybe you've bought ethically produced clothes? You may have felt good afterward — but how much lasting change did your actions bring about?

Probably very little. This is the underlying problem with modern-day participatory left-wing politics. While such actions may offer the feeling that you are doing something at the moment, the long-term effects are harder to judge.

In a world where capitalism and neoliberalism are the ruling ideologies of the day, any will to change must imply something more than buying the "right" banana or coffee. So let's look at how we got to where we are and how to move forward.

In these blinks, you'll find out

  * why complex systems like the economy show the limits of folk politics;

  * how a fringe group of academics and intellectuals would come to dominate mainstream politics; and

  * why the automating work will be a blessing for workers around the world.

### 2. The majority of current leftist politics is limited in both scope and impact. 

What do street protests, shopping at local stores and teach-ins all have in common?

Well, every one of these standard leftist political actions is an example of _folk politics_, a term that refers to ideas and attitudes that emphasize local, direct-action and small-scale approaches to politics.

Good examples of folk politics are movements like Occupy Wall Street, ethical consumption or any of the many student occupations throughout history. While each of these examples might have received its moment in the media spotlight and a burst of temporary momentum, they often fail to make structural change in the long run. These tactics are no longer sufficient in the contemporary political climate and, if applied now, would fail to produce structural change.

How come?

Because at its core, folk-politics is about everyday manifestations and not structural problems. For instance, it promotes personal action like sign-making and the occasional protest over systematic thinking like making changes to legal structures or running candidates for office. In other words, it puts feelings — like anger, frustration and outrage — above critical thinking and strategy.

But the issue here isn't that folk politics is morally bad or incorrect. Rather, the problem is that it has no sustainable long-term strategy or vision. Instead of guiding the course of history, folk politics simply reacts to the actions initiated by corporations and governments.

Not only that, but by bringing people together around single issues, it loses sight of the bigger picture. A classic example is the organization Live Aid. In 1985, this group raised loads of money to provide famine relief in Ethiopia. To do so, they hosted tear-jerking, celebrity-led events.

The problem was that their approach solely appealed to people's emotions, rather than their rational minds. Beyond that, a lot of the money raised ended up in the hands of rebel militias, thereby extending the civil war and worsening the famine.

### 3. Folk politics falsely simplifies the complex modern world. 

So, folk politics' reliance on local, individual actions and personal emotions severely limit its efficacy, but another problem is its focus on immediate, self-gratifying goals at the expense of lasting, long-term change. For instance, folk politics might celebrate a corporation delaying the construction of an oil pipeline, failing to recognize that the company is just waiting until people look away so that it can move forward with the project.

In this case, the issue isn't that folk politics is local, but that it sees local actions as sufficient in and of themselves, rather than as necessary elements within a longer-term vision.

But if it's so ineffective, why are people so enamored with folk politics?

A major reason is that it simplifies complex global issues. After all, global society is a _complex system_, loaded with interlocking arenas like world politics, economics, climate change and globalization. For most people, such ideas are difficult to grasp.

For instance, the global economy isn't something that can be directly perceived. Instead, it's a collection of different actions, players and outcomes, distributed across various systems. In other words, you'll never meet "the economy" in person. It's a convoluted web of elements from property laws to natural resource distribution, technological infrastructure and much more.

Such a separation between everyday experiences and the systems within which we live alienates people, as the extensive and complicated effects of these systems make it difficult for us to find our own experience within them.

Just take the cultural theorist Fredric Jameson. He says conspiracy theories are popular because people feel so alienated from the systems in which they live. Conspiracy theories narrow perspective into a single, culpable party, whether it be the Freemasons, the Bilderberg Group or any other convenient scapegoat.

Conspiracy theories are popular because they provide a simple answer to the question, "Who's responsible?" The problem is that these theories obscure our own role in the situation at hand and simplify cause-and-effect relationships.

Similarly, folk politics is appealing because it simplifies the complex world by directing energy at concrete, immediate actions. In so doing, it bypasses the complex, elusive reality of global relations.

### 4. Neoliberalism has grown from a fringe theory into the dominant socioeconomic approach of our age. 

To better understand the limits of folk politics — and the contemporary left in general — just think about what the other side has been able to accomplish. While folk politics is failing to make lasting change, the political opposition has been able to transform society through _neoliberalism._

This political ideology can be summed up by a steadfast belief in unimposed, free-market capitalism. However, since classic liberalism is about free markets and opposing government regulation, neoliberalism ironically relies heavily on state influence to defend property rights, impose antitrust laws and generally sustain the neoliberal world order.

Since its inception, neoliberalism has covered the globe, infiltrating media outlets, public policy circles, education, labor and even the self-perception and identities of ordinary people. Neoliberal hegemony peaked in the mid-1990s with the collapse of the Soviet Union, the rise of the Clinton administration in the United States and the ubiquitous penetration of this ideology in academic economics circles.

Nowadays, it can be seen in the social expectation that people will constantly update their skill sets to remain employable, a reflection of a continual push toward self-branding and the incessant drive to network, with all the stress, anxiety and depression this produces. In fact, neoliberalism has become so entrenched in our worldview that it's practically impossible to see any alternative.

However, before it became the behemoth it is today, neoliberalism was a fringe theory. It emerged in Vienna in the 1920s and in Chicago, London and Germany in the 1930s. Then, in 1938, all of these previously independent movements formed a transnational organization at the Walter Lippmann Colloquium in Paris.

This gathering brought together classical liberal theorists and economists like Friedrich Hayek and Ludwig von Mises to form the _Mont Pelerin Society_ or _MPS_. The explicit aim of this group was to develop and spread a new type of liberalism.

The MPS was a closed intellectual circle that included most of the central figures in the postwar creation of neoliberalism. Hayek and von Mises played key roles, as did the American economist Milton Friedman and many others.

But how did such a small group of thinkers bring their neoliberal project to dominate the global stage? We'll learn all about it in the next blink.

> _"Neoliberalism is 'the form of our existence — the way in which we are led to conduct ourselves, relate to others and to ourselves.'"_ \- Pierre Dardot, philosopher, and Christian Laval, sociologist

### 5. The left can learn from the neoliberal turn. 

While we can complain about neoliberalism being the dominant political and cultural idea of our time, it's impossible to dispute how effectively it carried out its aims. In fact, the hegemonic position neoliberalism achieved is the result of a carefully considered, long-term, top-down strategy.

This strategy had neoliberals put together policy arguments that were then systematically pushed through think tanks, like the Heritage Foundation and the Hoover Institute, as well as universities. These institutions then trained the global elite, further spreading neoliberalism and cementing its ideological influence across the globe.

For instance, in Chicago, Milton Friedman wrote detailed op-eds and news columns, and regularly appeared on TV, an unprecedented move for an academic at the time. Newspapers like the _Wall Street Journal_, the _Daily Telegraph_ and the _Financial Times_ worked in sync to promote his ideas, while businesses funded a popular TV show in the 1980s based on his work called _Free to Choose._

Not only that, but Friedman's 1980 book, also entitled _Free to Choose,_ sold better than any other nonfiction title that year. The combined effect of all these efforts assembled a massive audience for Friedman's economic and social ideas.

As you already know, this strategy worked. By the 1980s, neoliberalism was the commonly accepted political ideology across the board. Politicians rose to prominence under its free-market flag, including Ronald Reagan in the United States and Margaret Thatcher in the United Kingdom.

So, what can be learned from this absolute neoliberal onslaught?

The most important takeaway is that the left needs to build a similarly long-term, strategic vision. To do so, the left will need to abandon its fear of organizational secrecy, hierarchy and rationality. Such changes are essential if we are to build and sustain a comparably hegemonic position.

This means that, instead of just reacting to the political moves of the right and the now-accepted neoliberal policies of our world, the left must actively build an alternative. We must construct a positive vision for society by increasing the sophistication of our organization.

Next up, we'll take a closer look at what that vision entails.

### 6. Automation and unemployment are increasingly likely. 

Now that we know what type of approach is called for on the left, what should we be fighting for? Well, to answer that question, we first need to begin with an examination of the current state of labor.

This is a key factor, since full automation and unemployment grow more likely by the day, even as people work more than ever. Take industry as an example: while the industrial sector employed just 1,000 robots in 1970, today it relies on over 1.6 million.

Meanwhile, unemployment is on the rise, with job growth predominantly confined to the service sector. But even in service jobs, robotization is creeping in. Pretty soon, every person, whether they're a chef, a construction worker or even a stock analyst, will be vulnerable to replacement by machines.

In fact, the most detailed estimates of the labor market's trajectory suggest that between 47 percent and 80 percent of today's jobs could be automated. As a result, people who do have jobs are desperate to keep them and are therefore working longer hours than ever before.

It's interesting to note that economists and thinkers in the past didn't believe this would happen. The economist John Maynard Keynes famously argued that nobody would work longer than a 15-hour week by 2030. Karl Marx also assumed that people would be living very differently by now, with far greater personal freedom from monotonous jobs.

But none of these predictions have come true. Shortly after World War II, the workweek stabilized at 40 hours, and since then there have been few attempts to change that.

Moreover, technology has eroded the separation between work and life. As a result, the average full-time worker in the United States works closer to 47 hours per week.

That all being said, full automation should be our goal — and we'll learn why in the final blink.

### 7. Universal basic income is the key to creating a world after capitalism. 

How can we deal with the impending reality of full automation?

By embracing it! After all, the loss of jobs prompted by automation will lead to the need for _universal basic income_ or _UBI_, the idea of giving every citizen enough money to live on.

It might sound like a utopian impossibility, but UBI isn't as radical or new an idea as you might think. For instance, in the 1960s and 70s, basic income was a core proposal of the US welfare system. Economists, NGOs and policymakers were all exploring the idea in great detail. In fact, 1,300 economists signed a petition urging the US Congress to pass UBI, and two presidents, Richard Nixon and Jimmy Carter, attempted to pass it as legislation.

UBI is an idea that has been gaining momentum in the modern era as well. Support has come from writers like Paul Krugman and Martin Wolf, and news outlets like the _New York Times_ and the _Economist_.

So, UBI might not be as far-fetched as it sounds and it has a great deal to offer. For instance, an automated workforce paired with UBI would liberate humanity from work itself, allowing each of us to do whatever we wanted with our limited time on earth.

In this way, it would free workers from the shackles of capitalism. For most people, working isn't a fulfilling part of life — it's simply a means of survival. As a result, just 13 percent of workers worldwide say their jobs are engaging. For most of them, work is a degrading, draining, downright stressful necessity.

This problem would be solved by a leftist UBI that is sufficient to live off of and provided to all citizens, universally and unconditionally.

But UBI wouldn't just free us from work, it would also challenge the very notion of work itself. This is actually one of the major barriers to UBI, since most of us identify ourselves solely in terms of our work. As such, to successfully enact UBI, we must first consider how our thinking will change when we no longer need to work.

> _"The goal of the future is full unemployment."_ \- Arthur C. Clarke

### 8. Final summary 

The key message in this book:

**The current political tactics of the left are failing for working people. To succeed in transforming society, the left must take a page out of the right-wing playbook and build a long-term, strategic vision that offers an alternative to the current neoliberal, capitalist world order.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _PostCapitalism_** **by Paul Mason**

_Postcapitalism_ (2015) offers a close examination of the failures of current economic systems. The 2008 financial crisis showed us that neoliberal capitalism is falling apart, and these blinks outline the reasons why we're at the start of capitalism's downfall, while giving an idea of what our transition into _postcapitalism_ will be like.
---

### Nick Srnicek, Alex Williams

Nick Srnicek is a lecturer at the City University of London. He is also the author of _Platform Capitalism_.

Alex Williams is a lecturer of sociology at the City University of London.

