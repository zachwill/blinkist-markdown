---
id: 572eec87a35db30003e6bdbc
slug: inventology-en
published_date: 2016-05-12T00:00:00.000+00:00
author: Pagan Kennedy
title: Inventology
subtitle: How We Dream Up Things That Change the World
main_color: 55B0F7
text_color: 326891
---

# Inventology

_How We Dream Up Things That Change the World_

**Pagan Kennedy**

_Inventology_ (2016) shows us how the world's brightest minds identify problems and come up with brilliant solutions. Find out how you need to think in order to discern future trends and create the next great invention!

---
### 1. What’s in it for me? Learn about the genesis of great inventions – and how you might come up with one yourself. 

The monomaniacal and madcap genius, with wild hair and a personal lab, working feverishly into the night: this is the inventor of popular imagination. In reality, however, most great inventors are more likely to wear sneakers and jeans than a lab coat. Indeed, some of the greatest success stories of the modern world — Twitter, Facebook and Pantone — were all created by fairly "normal" individuals.

So how did they go about it? And could you do the same? Those are the questions that these blinks seek to answer. They contain both a historical account of many great inventions and a list of factors that any aspiring inventor would do well to consider.

In these blinks, you'll discover

  * how the wheeled suitcase came to be;

  * why Adam Smith believed that every factory worker was a great inventor at heart; and

  * whether you have what it takes to be a Super-Encounterer.

### 2. Every great invention is borne of a problem in need of a solution. 

Do you own a wheeled suitcase? If you do, you know how much easier it makes traveling; instead of lugging your luggage through train station and airport, you can simply and swiftly wheel it along. But did you know that these suitcases are a relatively recent invention?

Before the 1970s, everyone had to carry luggage by hand, no matter how heavy it was.

It took Bernard D. Sadow, the vice president of a luggage company, to recognize this problem and come up with a solution.

Inspiration struck while Sadow was walking through an airport and struggling with two heavy suitcases.

All too familiar with the problem, Sadow hit upon the solution when he saw an airport employee pushing a wheeled platform, atop which rested a heavy machine. "Why shouldn't luggage have wheels, too?" Sadow thought. Soon, he'd developed and patented a prototype.

His first design sold well, but it wasn't perfect: The wheels were placed on the long side of the suitcase and if you tugged too hard it would bump your legs, not to mention that turning corners caused it to topple.

Since Sadow only used his product while on vacation, he saw no need to spend time improving it. But an airline pilot named Robert Plath, in need of more than a mere holiday suitcase, decided to tinker with the trundling travel bag.

Eventually, after much work in his own private workshop, Plath developing the suitcase we know today: He put the wheels on the correct side and added a solid handle so the suitcase wouldn't tip over or roll into your legs.

Plath's design went on to become the universally loved product that people use whenever they need to travel.

So, why was Robert Plath able to build a better suitcase than Sadow? Well, since Plath had to travel every day, he had a better understanding of the problem, which, in turn, enabled him to hit upon a better solution. As we'll see in the next blink, such familiarity is key to success.

### 3. To find a good solution, you need a deep understanding of the problem. 

Have you ever spent months or even years with a nagging problem — and then, one random day, hit upon an ingenious solution and exclaimed, "Why didn't I think of this before?!"

Well, don't feel bad. Complete understanding of a problem usually only comes with repetition.

Adam Smith explained this in 1776: He wrote that one benefit of having factory workers is that they can become inventors through confronting the same task, or problem, on a daily basis.

One such factory worker was a boy in charge of pulling a lever every day. The constant repetition prompted the boy to think inventively: He automated the lever by tying a string around it and connecting it to another part of the machine. Now he could stand back and watch as the machine's lever pulled itself.

Sure, encountering the same problem over and over again can lead to frustration, but it can also lead to great inventions.

For this to happen, however, three components must be present:

First, the frustration has to come from a hidden problem, one that's difficult to detect.

Second, you should be able to tell how solving this frustrating problem will affect a lot of people in the future, not just you.

Third, you have to accept that it might take ages for the invention to become successful.

Let's look at the example of Jack Dorsey, the founder of Twitter: In 2000, he came up with an idea for _text-based broadcasts_ that send messages about location and activities to friends. He found it frustrating that this simple and useful idea wasn't available.

The solution to his problem was difficult to detect because few people at the time had the right kind of phone to send these messages. But sales of smartphones were on the rise, so he was able to predict that his idea could become successful — it just might take a while.

And his patience paid off. Six years after Dorsey identified a problem and developed a solution, the rest of the world caught up and Twitter took off.

### 4. Feeling lucky will make you more observant, and can make discoveries easier. 

Do you ever have the feeling that you're blessed with good luck? If that's the case, you might be great at finding solutions.

Psychology professor Richard Wiseman suggests that people who feel lucky are more observant and better at identifying useful solutions.

In one experiment, Wiseman brought together a group of people, all of whom considered themselves either lucky or unlucky. He gave everyone a newspaper and asked them to count the photographs in it. On the second page of the paper, a message was written: "Stop counting — there are 43 photographs in this newspaper."

It turned out that the lucky people actually did have a higher likelihood of noticing the message. They found the solution in only a few seconds, while the unlucky ones took around two minutes.

The author has a term for these fantastically observant people: _Super-Encounterers_.

Super-Encounterers make great inventors because they don't narrowly focus on a problem. Instead, they explore, especially when they are unsure of the answer. This widens their outlook, providing more chances to discover and create.

This behavior also presents itself in an experiment conducted by psychologist Mihaly Csikszentmihalyi.

He set up a room, opened it up to art students and told them to pick out and draw objects within it.

Two groups evolved: One group chose objects and started drawing immediately with one concept in mind. One such student simply picked up a book and a hat and drew them.

The other group took their time observing different objects in different ways. These students looked at things through a glass prism and opened books to see the individual pages. As a result, their drawings were based on improvisation, exploration and discovery.

Seven years later, Csikszentmihalyi checked in with the students and found that most of the first group couldn't support themselves and had stopped painting. But many people from the second group went on to become professional artists or teachers.

As you'll learn in the next blink, however, luck is no longer enough. Today, data is what drives discovery.

### 5. Today, discoveries are not based on luck but on data. 

Did you know that Viagra was actually invented by accident? In fact, many medical discoveries are stumbled upon on paths that scientists thought would lead to something else. And these days discovering something by accident has never been easier.

This is because we live in the digital era.

"Big Data" has become such a common theme that a new class of scientist has emerged: _the bioinformatics people_.

Instead of waiting for a lucky break, bioinformatics people create their own luck by using computers and checking the results of thousands of past experiments in order to reveal hidden connections.

A McKinsey report from 2013 shows that this form of research actually has value. It's called data-mining, and they estimate it to be worth around $100 billion per year.

One of the major advantages of making discoveries through data-mining is the time it saves; by letting computers search through the huge piles of data, you save the time and money it would cost for teams of scientists to do it. This way you can analyze thousands of medical trials in a single afternoon!

As you might assume, the amount of medical-industry data is enormous. In fact, it's already measured in petabytes. And one petabyte equals one million billion bytes!

Today's data-mining algorithms can check this otherwise unused information in record time, leading to surprising discoveries, like new uses for old medications.

For example, consider _imipramine_. It was developed as an antidepressant, but research discovered that it could also be used against small-cell lung cancer.

Before data-mining, making this kind of discovery would have taken decades. Today it can happen incredibly quickly, and in the case of imipramine, researchers went from analyzing the data to clinical trials in around two years.

But data-mining isn't the only advantage we have these days, as we'll see in the next blink; it's also easier to create something out of nothing.

### 6. Creating something out of nothing is possible and can even save lives. 

Did you know that there's a color code for the brownies in Ben & Jerry's ice cream? It's true, and it was generated using a system that was created out of nothing.

This invention dates back to the 1960s, when Lawrence Herbert was co-owner of a printing company called Pantone.

During this time, Herbert was in charge of ordering inks, and he noticed a problem: every supplier used different color pigments. So, depending on who you ordered from, every color would be different.

Herbert had a brilliant idea: A color-coding system called Pantone that would work like a universal language, something everyone in the printing industry could use.

Each color has a corresponding number. For example, Daffodil Yellow = Pantone 123.

He created a sample page and went to work convincing the industry of its potential. Though introducing Pantone wasn't easy, Herbert never gave up, and it eventually caught on. By the 1970s Pantone was making more than $1 million a year in licensing fees.

Herbert managed to create a valuable invention out of nothing; all he had was an idea and a piece of paper.

And we still find this kind of inventive magic today. Just consider an important discovery about the human gut.

Modern research shows that the microbes in our gut are crucial to our health. But if the levels of these microbes become unbalanced, the consequences can be deadly, resulting in ailments like the stomach infection _Clostridium difficile_, or "C-diff."

Here's where the out-of-nothing invention comes in. Scientists discovered a unique way of curing people with C-diff: with healthy human feces.

Doctors use microbes from the feces of a healthy donor and "transplant" them into the sick person's gut through infusion. With this method, what would otherwise be a fatal infection can be cured within hours.

This shows us that brilliant new ideas can come from anywhere, and it's another example of an amazing invention being created from nothing.

Furthermore, you don't need big funding or a team of scientists to create an amazing invention. As the next blink makes clear, all you need is imagination.

> _"To transform the world, you can start small, with whatever is sitting on your desk right now."_

### 7. Trying to forecast the future will help you to come up with great inventions. 

Wayne Gretzky was perhaps the greatest ice-hockey player ever. And there was a secret to his success: he always went where the puck was heading, not where it had been.

This is exactly what you need to do when inventing. It's called _forecasting_, and it's no easy task.

To help with this, you can play the _Wayne Gretzky Game_ : Picture the world a few decades from now and try to imagine what technology will exist. Base your thoughts on how you believe human desire and technology will continue to evolve.

To see this game in action we can look at Xerox: In the 1970s, Xerox forecasted the future and figured out what tech-savvy people would want — a personal workstation for the consumer market, with a mouse, a word processor and a graphical user interface.

But forecasting accurately isn't always so easy.

In the mid-1960s, strategist Herman Kahn tried to determine just how good we are at it. He asked the leading minds of the time to give 100 technological predictions for the year 2000.

It turned out that the predictions for communications and computing were about 80 percent accurate. These included accurate forecasting for the internet, VCRs and cell phones.

The worst predictions had to do with transportation, medicine and architecture. These included misguided predictions about hibernating spaceships and appetite-control pills.

But these predictions show that an accurate one can catalyze change.

Take Moore's Law, for example. It states that computing power doubles every 18 months. Gordon E. Moore himself later said that after he proposed his theory in the 1960s, he noticed that it launched certain behaviors.

People in the technology industry began to realize that if they weren't moving fast enough they would fall behind Moore's Law. In this way, the prediction became a self-fulfilling prophecy, and a driving force for new inventions.

Coming up with a great new invention isn't easy, but, as we'll learn in the next blink, everybody has the power to do it.

### 8. Using your mind as a test lab will help you come up with great inventions. 

The mind is a powerful tool and you can use it to conjure up your ideas and inventions. Many of our greatest inventors use their imagination to envision their ideas before they sketch them out.

Someone famous for taking this approach: Nikola Tesla.

As a child, he would spend sleepless nights imagining new cities and structures. At the age of 17, he began turning these thoughts into inventions.

Tesla could see his inventions so clearly in his mind's eye that he didn't need any models or sketches to allow him to start creating. For him, the visions were so clear that he dreamed of inventing a camera that could capture them from his mind.

Recently, inventor Elon Musk continued this dream by building a system that allows people to design an object by moving their hands through the air.

So how do you make these thoughts a reality? Start by asking questions and getting your mind going.

As a writing teacher, the author has watched many students explore their creative minds. One student told him that he had the plot for an entire movie in his head; it was just a matter of putting it on paper.

When the author met with him a week later, the student still wasn't able to write it all down. He understood the story, he just didn't know how to get started.

To help, the author asked the student all the questions about the story he would ask himself: How does the movie start? Whose point of view is being used? Even, what odors do you imagine there to be?

With every invention you can imagine, there are hundreds of details that can be turned into questions like these. Ask yourself as many questions as you can to help design and realize your idea.

Imagination is the precursor of invention. And as everyone has an imagination, anyone can come up with a great invention.

> _"Our brains are designed to simulate reality, allowing us to test out wild ideas."_

### 9. Everybody can come up with great ideas. You don’t need to be an expert. 

Chances are you probably haven't heard of John Harrison. He was an English carpenter and clockmaker who solved a problem for the British Parliament in 1714 and won £20,000 — the equivalent of about £2 million in today's money.

Harrison's story shows us that it's often people outside of a specific industry that find the best solutions for its problems.

Back in the 1700s, sailors weren't able to calculate their longitudinal position, and this left them in danger of crashing into rocks or other vessels, and sinking down to the watery depths.

For example, in 1707, 2,000 British sailors died when an English fleet crashed in the ocean.

So, in 1714 the British parliament offered a reward to anyone who could find an accurate method for calculating the longitude while aboard a ship. People assumed the solution would come from an astronomer or someone from the Royal Observatory, but it was John Harrison, a simple carpenter and clockmaker, who took home the prize.

He created the _marine chronometer_, a clock that was accurate to the second and allowed sailors to tell the time back home and compare it to the time where they currently were, giving them a fixed position.

Another example is Adam Rivers: He's a postdoctoral researcher at the University of Georgia in the marine sciences department who, in 2012, solved a problem for the food industry.

A company was having trouble getting the right food coloration for their health shake. While the problem was proving hard to solve for a food scientist, Rivers quickly connected the dots and solved the problem over a single weekend.

Using his knowledge of marine biology, along with some tools he bought at Walmart, he managed to chemically alter the mix of metals in the water to create a more pleasingly colored beverage.

So it really doesn't matter what industry you currently work in. To become a successful inventor, all you need to possess is initiative and perseverance.

> _"Outsiders, rather than those within a field, tended to hit on the best answers."_

### 10. Final summary 

The key message in this book:

**Everyone has the potential to come up with a great idea. The next amazing invention can come from anyone, anywhere, especially if that person works outside the main field of action. What helps is a deep understanding of the problem and the ability to foresee where society is headed and what people will need.**

Actionable advice:

**Launch a crowdfunding campaign.**

If you think you have a great idea, launch a campaign on a crowdfunding platform and seek the feedback of real people. This will help you evaluate your idea without seeking investors or spending tons of money.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Where Good Ideas Come From_** **by Steven Johnson**

_Where Good Ideas Come From_ examines the evolution of life on earth and the history of science. This _New York Times_ bestseller highlights many parallels between the two, ranging from carbon atoms forming the very first building blocks of life to cities and the World Wide Web fostering great innovations and discoveries.

In addition to presenting this extensive analysis, replete with anecdotes and scientific evidence, Johnson also considers how individual and organizational creativity can be cultivated.
---

### Pagan Kennedy

Pagan Kennedy, a journalist, writer and former MIT Knight Science Journalism Fellow, specializes in microbiology and neuro-engineering. She has written several books, both fictional and non-fictional, and her journalism has appeared in such publications as the _New York Times Book Review_ and the _Boston Globe Magazine_.

