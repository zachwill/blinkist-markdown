---
id: 55b76af73235390007020000
slug: investing-in-people-en
published_date: 2015-07-28T00:00:00.000+00:00
author: Wayne Cascio and John Boudreau
title: Investing In People
subtitle: Financial Impact of Human Resource Initiatives
main_color: E52E3F
text_color: CC2938
---

# Investing In People

_Financial Impact of Human Resource Initiatives_

**Wayne Cascio and John Boudreau**

_Investing in People_ (2011) shines a light on human resources, a crucial department in a successful company that too often is undervalued and underappreciated. A savvy personnel strategy can fine tune a company's performance and boost employee well-being. The book provides an easy-to-follow, four-step process to improving your human resources strategies.

---
### 1. What’s in it for me? Learn why effective HR is crucial to success and how you can achieve it. 

Are you passionate about human resources? Probably not. In all the areas of business, personnel management is considered the least sexy of roles. This attitude often leads to HR becoming the "forgotten zone," with managers apathetic about its activities, as long as they're not too expensive.

Yet HR is one the most important elements in the profitability of a business. The bottom line: companies that constantly improve their personnel strategies do better than those companies that ignore them. 

These blinks show you just how important human resources can be and how you can make your company's HR department as strong as possible.

In these blinks, you'll discover

  * why you need a LAMP to shine a light on human resources;

  * how you can make your employees healthier people; and

  * how one visionary manager doubled employee pay _and_ increased profits.

### 2. Great HR and your company shines. Poor HR and your company suffers. Ignore HR at your peril! 

The importance of human resources as a key driver of a company's success is acknowledged but rarely appreciated.

Any manager knows it's important to have and maintain good employees. But could you, if pressed, articulate what's good and what's not about your own personnel management strategies?

Many managers can't, and continue to fumble with HR, for better or often, worse. The fact is that human resources is a crucial element in determining whether your company will grow and thrive.

Why then, do we so often value HR not on its effectiveness but solely on whether it's efficient?

You might easily see the connection between effective marketing and revenues, but teasing out the link between HR and financial success is trickier. Because of this, business leaders are largely indifferent to human resources issues, and only assess strategies with regard to cost efficiency.

But the advantages of good HR are plain to see. Companies depend on both the quality and commitment of employees. The Hay Group's "World's Most Admired Companies" list even shows that companies that treat their people as _valuable assets_ are as a rule more successful.

It's paramount to make the connection between decisions regarding human resources and a company's financial performance visible and understandable.

But what's the best method for doing so?

One powerful option is to use the _LAMP_ framework, an acronym that stands for _logic_, _analytics_, _measure_ and _process_.

LAMP is about understanding the logic behind a problem, finding the correct ways to measure it, analyzing the data and creating a process to respond to the issues you've discovered.

Want to learn how to shine your own LAMP on your HR processes? Read on for more.

### 3. Establish a logical framework to get behind the numbers to figure out the real HR problem. 

When a company wants to measure success, what does it do? Most companies will simply look to data. But if all you do is crunch numbers, you'll miss out on some deeper issues.

To start making better decisions, you need to look at the issues behind the numbers.

First establish a clear, _logical_ framework — the "L" in LAMP — starting with identifying the causes and the consequences of certain problems at your company.

Consider absenteeism. By just looking at numbers, all you'll discover is that often, employees get sick. But studies have shown that in the United States, only 35 percent of absenteeism can be explained by personal illness. So you'll need to look a little deeper.

Family issues, stress, personal issues or even a mentality of entitlement are just some of the factors that can contribute to a high absentee rate. But how these elements are significant will remain obscured if you don't establish a logical framework to help you understand the gravity of the problem.

For example, a logical framework for absenteeism could look like this: employees aren't at work to complete their jobs; other employees have to fill in, which either reduces overall productivity or results in work not getting done at all.

Less work completed of course means higher costs for you! So by reducing absenteeism, you'll reduce costs. Isn't it logical now that you need to get to the bottom of absenteeism?

By using the LAMP framework, you ensure that you're shining a light on the right problem in your overall human resources strategy. With your problem now in view, it's time to start measuring.

### 4. Only a sound analysis of data will lead you to relevant insights and help you sniff out solutions. 

If you ever tackled a college thesis paper, you know the challenges of sifting through tons of data. But only once you have all the necessary data, can you start the real work of analysis and writing.

Data without _analysis –_ "A" in LAMP — doesn't make a good thesis, nor does it make a good HR strategy.

Be wary of jumping to conclusions too soon, as a quick look at the data might mislead you into making a false insight. Such mistakes can cost you!

Say you've been given data that shows that stores with happy, cheerful employees correlate to higher sales. You might decide to try to reproduce this correlation in other stores by starting a training program to improve employee attitudes.

The program will be expensive, but if it boosts sales, the effort is worth it. Right?

Not quite. A proper analysis might have shown that employee morale is high in certain stores only because customers there belong to a demographic that spends more. If this was the case, your expensive training program wouldn't impact sales at all — it would just be expensive.

So do a proper analysis before you jump into action. But don't be fooled — detailed analysis is often a complex process and can only be conducted by specialists. It's nevertheless essential that all HR leaders have a basic understanding of analytical methods. 

HR managers need to stay up-to-date on best practices by reading relevant publications regularly.

Your HR chief should also ensure that any specialists you hire know all the necessary background information for your company, such as why structures are the way they are, why certain staff members work like they do, and perhaps why any changes have not been enacted before.

> _"Analytics transforms HR logic and measure into rigorous, relevant insights."_

### 5. Choosing appropriate measurement techniques helps put your HR problems in context. 

From punctuality to turnover, there is a whole range of metrics for human resources. But as a manager, you'll learn nothing without a good grasp of your problem's context.

Decreasing employee turnover, for example, seems like a worthy goal. Your employees will be overall more experienced, and you'll spend less time and money finding and training replacements.

Yet measuring turnover rates alone won't give you much insight.

If you want to decrease the rate of turnover, the simplest way would be to lower the required skill level for company positions. More candidates will apply for jobs, and those hired will be more desperate to stay in the job, given the competition. But is this good strategy? Of course not!

You can't build a better strategy on one simple data point. If you want to understand the issue at hand, you'll have to _measure_ — the "M" in LAMP — meticulously. To tackle a turnover problem more effectively, consider calculating turnover rates by department and by position.

By measuring in a targeted manner, you'll soon find far more interesting results.

You might conclude that a high turnover rate hurts the company only when it applies to higher-level positions. But rapid turnover might actually be beneficial lower in your company's hierarchy, as after all, new employees earn less money and can take fewer holidays.

Knowing this, you could improve the conditions for those more valuable positions and dedicate fewer resources to the rest. But will this keep employees happy? That's a separate question.

But with a good grasp of logical frameworks, analysis and measurement tools, you're well on your way to solving your HR problems. Now it's time to examine the _process_ of how to make these problems go away.

### 6. To put your insights into practice, it's important to get non-HR managers on board. 

After all that hard work in researching HR problems, you need to make sure the company benefits from your new insights. In other words, company managers need to get on board to make your new HR _processes_ a success.

If you want your insights to shape decisions and conduct, you or your HR managers have to keep in mind that each company branch has its own priorities and focus.

For a sales team, sales numbers obviously are of paramount importance. The production department, on the other hand, is more interested in efficiency and reducing waste.

Team managers therefore have different _knowledge frameworks_, with which they process and understand information in light of their job priorities.

A salesman will be glued to his seat if you're introducing a new program guaranteed to increase sales. Yet if you focus on how the program is equally important for customer welfare, you might lose your sales team's attention.

When you want your managers to actively engage, you need to communicate with them in a manner that best suits their knowledge framework.

Build on what your team already knows. For example, managers focus a lot of their energies on profitability and costs. Why not start your HR pitch using an analysis that focuses on the costs of poor procedure?

In doing so, you'll grab their attention. After they're hooked, you can introduce some of your more complex analytical tools that might be less familiar.

As an example, start by outlining a basic turnover-cost analysis to your audience. This will help those managers not in HR to understand the direct link between decisions made in HR and the company's bottom line.

Once you've made this breakthrough, you can get into the finer details to explain the logical, measured analysis you've performed and in doing so, help each team better grasp why the new strategy is a win for them!

### 7. Eliminate unnecessary expenses through absenteeism by investing in your employees’ health. 

It's a worker's nightmare. A coworker falls ill, and suddenly, all her work is on your plate, too. It's high time that companies pay more attention to health at work!

The largest share of health-care costs — some 75 percent — actually stems from a small number of diseases strongly related to lifestyle choices: cardiovascular disease, diabetes and obesity.

Promoting healthy behavior can help prevent many of these ailments, from exercising regularly to help fend off cardiovascular disease to maintaining a healthy diet to avoid diabetes and obesity.

A company can reduce both direct and indirect health-care costs by supporting efforts to encourage employees to choose healthier lifestyles.

Take Pitney Bowes, for example. This large software company did its research and came to the conclusion that employees suffering from high blood-pressure issues were having a big impact on the company's overall health-care costs.

The company thus decided to cover the costs of blood pressure medication for employees that needed it. And in doing so, Pitney Bowes saw a marked reduction in health-related emergencies and absenteeism, not to mention an increase in productivity.

Pitney Bowes isn't an isolated case. A Towers-Watson study in 2009 revealed that high-performing companies invested significantly more in the health of their employees than did less-successful companies.

Aside from cutting costs, there's another benefit to looking after your company's health: employees are far more likely to feel loyal to their employer when they feel cared for.

In this way, strengthening the health of employees will strengthen a company itself!

### 8. Employee happiness and financial success are linked. Make it your priority to boost morale. 

In January 1914, Henry Ford cut the work day for his employees to eight hours and increased the daily minimum wage to $5, almost double its previous level.

People thought Ford had gone mad. But his pro-employee decision proved successful, as workers performed better and worked harder as a result.

Ford knew just how much employee attitude affected productivity, as it still does today.

Studies done between 1998 and 2000 revealed that companies with positive employee attitudes have long-term returns of 82 percent, while companies with negative employee attitudes had 37 percent.

Food retailer SYSCO's in-house consultants found a high correlation between employee attitude and pre-tax earnings. This is because happy, motivated employees are far more likely to accept and adapt to innovation, meaning a company can constantly improve its offerings.

Happy employees can reduce costs, too. _Fortune_ magazine's list of the "100 Best Companies to Work For" all receive twice as many job applications than do other companies, while turnover rates are half that of the competition. This saves companies a lot of money in both recruitment and training.

SYSCO also considers employee quality-of-life issues, by offering services such as child care.

Additionally, employees with SYSCO's subsidiaries do a self-assessment once a year. The top-performing companies always score higher in a few key measurements, such as "upper management spends time talking with employees about our business direction" or "my supervisor treats me with dignity and respect."

This demonstrates SYSCO's ability to ensure that a strong company culture is a priority at all levels.

### 9. Work-life programs benefit both employees and companies. Today, more employees expect them. 

Some employees are "married" to their jobs. But for many, life is more than just a career. Importantly, work-life programs are on the rise. So how exactly can such programs help your company?

The aim of work-life programs is to help employees find a balance between employment responsibilities and personal life. As the structure of work has changed radically in recent decades, so too have the needs of employees.

Almost as many women as men are now in the workforce, with some 80 percent of couples dual earners. Without a stay-at-home parent, child care has become a more difficult business.

Studies have shown that the generation born between 1979 and 1994 places great importance on flexible work schedules, a work-life balance and altruistic principles. They're not just fixated on a higher pay package. This means that companies can no longer rely on pay alone as an incentive.

More and more college-educated employees wait longer to have children, worrying that parenthood will interrupt or even damage their careers. And because professionals are often working autonomously and not just from 9-to-5, their need for flexible arrangements is significant.

Work-life programs are a win-win solution for companies and employees, leading to better financial results and decreased costs.

Research has shown that the best predictor for employee health and satisfaction is a good work-life balance. Work-life programs, therefore, lead to decreased turnover and absenteeism rates.

Scandinavian Airlines (SAS), for example, has made significant investments in work-life programs _and_ consequently has the lowest voluntary turnover rate in the whole industry, at just two percent!

SAS has grown each year since its founding 34 years ago, and in 2009, it achieved revenues of $2.31 billion. So we can see how a better quality of life for employees can lead to better business!

### 10. Final summary 

The key message in this book:

**It's time to end the underappreciation of human resources. Evaluate your HR processes step-by-step, using logical frameworks and considered metrics to get the data you want and powerful analytics to discover the solutions you need. By engaging each branch of your company individually, your HR strategy will make a difference to your company's productivity and profit levels while keeping your employees healthy and happy, too.**

Actionable advice:

**Follow the LAMP framework!**

The next time you have to solve a complex business problem, try using the LAMP framework. Create a _logical_ framework that connects the different processes that make up your problem. Then try to _analyze_ where exactly the problem in your business strategy is found by _measuring_ carefully the data you've accumulated. Then create an effective _process_ to solve the problem.

**Suggested** **further** **reading:** ** _Work Rules!_** **by Laszlo Bock**

_Work Rules!_ (2015) takes us through the inner workings of Google, one of the most powerful and successful companies in the world. Bock tells us precisely how Google pulls off this feat while consistently being ranked as the best employer in the world.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Wayne Cascio and John Boudreau

Wayne F. Cascio is a distinguished professor at the University of Colorado, and holds the Robert H. Reynolds Chair in Global Leadership at the University of Colorado Denver. 

John Boudreau is research director at the Center for Effective Organizations and professor of management and organization in the Marshall School of Business at the University of Southern California.

