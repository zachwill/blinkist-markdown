---
id: 599304fcb238e100058f78b6
slug: investing-with-impact-en
published_date: 2017-08-17T00:00:00.000+00:00
author: Jeremy K. Balkin
title: Investing With Impact
subtitle: Why Finance is a Force for Good
main_color: 2FB3EA
text_color: 1B6685
---

# Investing With Impact

_Why Finance is a Force for Good_

**Jeremy K. Balkin**

_Investing With Impact_ (2015) explores how people have harnessed the power of capitalism to do good and improve society at large. As a number of ethical and philanthropic investors have shown, finance needn't be a system of pure greed. Find out why the frequently demonized capitalist system may well be the secret to saving the world.

---
### 1. What’s in it for me? Learn how to invest for both high returns and society as a whole. 

Capitalism is often blamed for causing many of the world's problems. Its detractors claim that it encourages an endless hunt for higher returns, which leads to ethically problematic investments and a general disregard for humanity. At the same time, we all know money can do good in the world. Few complain when money is used for philanthropy or invested in new infrastructure or education.

So what if capitalist interests could be used to benefit society?

These blinks explore that possibility, and show how, in many cases, businesses and investments can help further humanity better than states and governments.

You'll also find out

  * why people — not the capitalist system — are to blame for the 2008 financial crisis;

  * that companies are better than states at providing services to society; and

  * why the E6 investment model is the way to make investments good for all.

### 2. The capitalist system isn’t inherently evil; it’s people that make it bad. 

No tool is inherently good or bad. Morality enters the picture when tools are put to use. For instance, an axe can save lives when it's used to chop wood to build a fire in winter; in the hands of a murderer, however, that same axe could be a weapon. Capitalism is a tool like any other.

Since human beings ultimately control the capitalist system, it's up to us to decide whether capitalism is used for good or bad.

The capitalist system works by establishing a free market that is based on empowered individuals who are free to make their own decisions. Over the past 50 years, this system has brought prosperity and made the world a better place for many.

Thanks to free trade, the wealth of nations has risen, as have levels of employment and self-sustainability — all of which has improved living standards.

According to a 2014 World Bank report, approximately 80 percent of poverty reduction is due to a nation's economic growth through free trade and free markets. Free trade and free markets are also what halved global poverty between 1981 and 2005.

Capitalism's ameliorative effects have led most people to view capitalism in a positive light. In fact, a 2014 study by the Pew Research Center showed that 4.5 billion people in the world think that free-market capitalism is the best economic system.

Now, you might be thinking, "What about the 2008 financial crisis? Wasn't this the result of a broken system?"

As a matter of fact, the crisis was the result of moral and societal issues. The people who made the financial decisions failed — not the system itself.

The crisis stemmed from the greedy and selfish actions of bankers who favored short-term earnings over long-term benefits and disregarded what was best for their communities.

In retrospect, the immoral actions of US energy company Enron foreshadowed the crisis that followed.

The company collapsed into bankruptcy after they tried to hide debts by exploiting loopholes and posting false numbers. Rather than trying to provide a helpful service, the company strove to improve the stock price and earn big returns and dividends.

The motives of bankers were similarly greed-driven. Rather than helping clients, they based their actions on self-interest alone.

### 3. Companies are more effective than governments at instigating social change. 

Education, health care and welfare are public services that, generally, the state offers to help its citizens. But debt has recently put these services in jeopardy.

These days, governments are accruing such huge debts that they're forced to raise taxes and cut spending in an effort to cover them. These methods, which only hurt a country's economic growth, are called _austerity_ measures.

After the 2008 crisis, many governments imposed austerity measures, which failed to generate wealth or improve society. In some US states, raising taxes and cutting spending couldn't even prevent the health-care system from collapsing.

Governments, bogged down by bureaucracy and with limited authority, routinely have trouble meeting the needs of citizens. Taking action takes a great deal of time.

Companies, on the other hand, don't have these problems. They don't have to pass a bill through Congress in order to take action, which allows them to deliver effective health-care, educational and environmental policies more efficiently than any government.

Indeed, Walmart provides its employees with better health care than the US government provides its citizens.

As of 2014, a 30-year-old smoker, earning $30,000 per year, would pay $70 a month to be covered by the Walmart plan. To be covered by an average US health-care plan, that same person would have to pay $352 a month.

As for environmental policies, companies are also ahead of the curve. In 2008, Starbucks decided it would reduce water consumptions by 25 percent before 2015. There was no lobbying or amendments to be made; they simply began working toward that goal.

Businesses can also benefit from being multinational, which allows them to easily allocate local resources across borders. This is how Coca-Cola is able to run effective educational programs all over the world.

Whether it's health-care, educational or environmental concerns, companies also have more financial power than governments to execute these programs. Apple, Microsoft and Google all have far bigger budgets than many nations, including the United Kingdom.

True, we haven't seen many businesses taking these actions — but there's a new generation eager to effect positive change.

### 4. Millennials have the right values to put the capitalist system back on track. 

Traditionally, the standard career advice has been: don't change jobs every year; it'll make you look unreliable and overly eager to jump ship.

But today, the new generation — the millennials — has a different set of career values, and people such as start-up developers are expected to have a varied résumé.

Millennials place a great deal of importance on having a career that improves and benefits society.

For the baby-boomer generation, the most important career values were stability and individual prosperity. But for the millennials, who were born between 1984 and 2000, there's a completely new set of ideals.

A big reason for this shift is that millennials aren't fooled by the classic idea of the American Dream, which suggested that if you worked hard you'd be properly rewarded. The 2008 financial crisis was proof of the hollowness of this ethos.

In the traditional scenario, the millennials would be on the losing end of things. They would earn less money than the baby boomers due to seniority, and due to the aging population they would be expected to work harder and pay more taxes.

So instead of focusing on money, millennials prioritize business practices that make society a better place. They also perform more volunteer services than any other age group, which is another sign that they are truly interested in philanthropy.

Millennials are the leaders of tomorrow, and they will bring their own set of values to the companies they run and use the capitalist system in a way that will benefit society.

When we look at the statistics, we can see how impressive millennials are demographically.

By 2020, millennials will comprise 40 percent of all eligible voters in America and 75 percent of its workforce. This adds up to around 103 million millennials, which is about 36 percent of the overall US population.

The sheer size of this demographic means they'll be able to successfully push their philanthropic agenda forward through their businesses.

### 5. Impact investing allows profits to be used in ways beneficial to society. 

Finance and investing have the potential to do far more than simply make savvy bankers rich.

Take _social impact bonds_, for instance. They free up taxpayer dollars so the government can use that money to get private investors to fund social programs.

For example, Nebraska has a literacy issue. Seventy-seven percent of students can read well, but only 55 percent of black children are literate.

Fixing this problem would benefit society as a whole, so the government could issue social impact bonds. Now, private companies can finance a program to help marginalized children improve their reading — by, for instance, opening a new school or paying for more teachers.

If the statistics improved, the private financiers would get a return on their investment in the form of tax benefits, which is money that normally would go to finance these programs. So, when a social impact bond works, everyone wins!

This is also known as _impact investing_, and the challenge is to make these opportunities financially rewarding so that they're attractive to investors.

One way to do this is to emphasize the future benefits — for instance, the positive impact the investment may have on the lives of students.

When students do well in school, they grow up to be more confident and earn more money in their careers, leading to higher profits, which creates more jobs and more tax revenue for society as a whole.

Such societal payoff needs to be brought to the forefront because if the returns are not clear, investors will simply put their money toward other companies that _do_ offer clear returns. But these companies don't always have society's best interests at heart. Enron, for example, offered clear returns on investment by being corrupt.

This is why it's important to consider the societal benefit of our investments; otherwise, we'll only end up in another crisis. So a good investment is one that strikes a balance between profit and positive social impact.

In the final blink, let's take a closer look at how we can find that perfect balance.

### 6. The 6E investment model makes sure a company has society’s best interests at heart. 

It's not always easy to find the right investment with a positive social impact.

Thankfully, there's the _6E model_ to help guide the way. It takes into consideration six criteria that clearly and effectively measure the social impact of an investment.

The better the score, the more value the investment carries.

The first criterion is _economics_. This simply calculates the price of a company's stock. Riskier investments will have a higher future stock price, and a higher return rate.

The second criterion is _employment_. Companies with a better score will provide a greater social benefit by employing more people and creating more jobs.

Apple has 12,000 employees at its headquarters in Cupertino, California, and claims to have created 60,000 jobs. Consider all the other businesses, such as restaurants, gyms and grocery stores, that have popped up around Cupertino. You can usually bet on companies that have the potential for job growth.

The third criterion is _empowerment_. Consider how diverse a company is. Are there people of different genders, ages and ethnicities on both the employee and executive levels?

According to the renowned consulting firm McKinsey, a company with a diverse board will bring higher rewards since their strategies will be better aligned with the global business world.

It also makes the company more trustworthy. After all, would you believe a company that says it's looking out for the best interests of everyone in a community if their executives are all white men?

The fourth criterion is _education_. How strong are the company's professional development programs? Better education leads to more skills and higher earning, all of which enables people to better care for themselves.

The fifth criterion is _ethics_. High-scoring companies always have clear ethical guidelines for their executives.

Finally, there's the _environment_. Find out what the company is doing to reduce pollution. This is of great importance since it has a huge impact on society and general health worldwide.

With these six factors in mind, you can see how a business — and your investment — contribute to making the world a better place.

### 7. Final summary 

The key message in this book:

**Finance can be a positive force when used correctly. Impact investing, which generates profitable returns while simultaneously benefiting society, is just one way to invest ethically. Socially conscious investors should look at social bonds and closely investigate potential companies to make sure they have society's best interests at heart.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Business of Good_** **by Jason Haber**

_The Business of Good_ (2016) is your guide to social entrepreneurship and earning a living while making a difference in the world. These blinks explain how communities benefit from this new way of doing business and how you gain, too, in becoming a successful social entrepreneur.
---

### Jeremy K. Balkin

Jeremy K. Balkin, an esteemed leader on the subject of ethical banking and finance, is often described as the "Anti-Wolf of Wall Street." A valued speaker, Balkin has given presentations at the United Nations and the World Economic Forum. His TED talk on ethical banking has been viewed over a quarter of a million times.

