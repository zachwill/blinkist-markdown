---
id: 5818682d9c08690003cbdc72
slug: invisible-influence-en
published_date: 2016-11-04T00:00:00.000+00:00
author: Jonah Berger
title: Invisible Influence
subtitle: The Hidden Forces That Shape Behavior
main_color: 3BC0DA
text_color: 1F6573
---

# Invisible Influence

_The Hidden Forces That Shape Behavior_

**Jonah Berger**

_Invisible Influence_ (2016) is all about the effect other people have on the clothes you wear, the music you like and the decisions you make. These blinks explain how your actions, thoughts and preferences are shaped by others, and how by understanding this process, you can have greater control over these influences.

---
### 1. What’s in it for me? Find out about the forces that shape your decisions. 

It often seems as if some of those around us are influenced far too easily. People appear willing to change their opinions because of just one tweet that comes up on their feeds, or will follow some foolish fashion trend that clashes not only with their body types, but their country's climate. But of course, _you_ would never let yourself be influenced in such obvious ways — or would you?

As these blinks will demonstrate, our thoughts and behaviors are influenced by external forces and factors far more than we might like to admit. Luckily, you can learn how to spot this invisible influence, and even use it to your advantage.

In these blinks, you'll learn

  * about the unexpected benefits of attending every class session in your college course;

  * why some people get paid for _not_ wearing something — without being nude models; and

  * how a big hurricane might influence the name you give to your baby.

### 2. Every decision you make is affected by others, and identifying this influence can help you overcome it. 

Have you ever thought of the reasons behind the clothes you wear or the car you drive?

Most people would chalk these things up to objective features like price, mileage or design, but this doesn't tell the whole story. In reality, we're quite easily influenced by social pressure. While many of us are reluctant to admit that others have power over us, they most certainly do; they affect our actions, preferences and even our self-perceptions.

Consider the neighborhood you grew up in: it has a profound impact on you because of things like the quality of nearby schools and the peers that surround you. If every kid in your neighborhood teased bookworms, think about how that could cause you to develop negative feelings toward reading.

But what is it about humans that allows us to be so easily influenced by others?

One reason is that we're vulnerable to all manner of subtle factors. Take one study by psychologist Richard Moreland of the University of Pittsburgh: he instructed four women who were considered equally attractive to attend a large college class and act like normal students. The four women were to attend zero, five, 10 and 15 sessions of the class, respectively.

At the end of the semester, Moreland gave the students photographs of all four women and asked the students to rate the women based on their looks. The majority of the class preferred the woman who'd attended 15 sessions, but weren't aware of ever having seen her!

Simply put, the study suggests that we prefer familiar people, and social influence is a powerful force in this regard. However, if you are aware of it, it will have less of an effect on you.

A child may not help but be influenced by the bookworm-hating kids in your neighborhood — that is, until she realizes how they're influencing her. After that, the other kids won't be able to discourage her so easily.

When faced with such an issue, she might ask herself whether people who read a lot are, in fact, losers. She might then consider all the prolific, illustrious scientists and writers who immersed themselves in books, and be encouraged by them instead.

> _"99.9 percent of all decisions are shaped by others."_

### 3. We’re inclined to imitate others and adopt their opinions. 

Knowing what you now know about social influence, do you still think you're a totally independent person?

Well, if you do, you'd be wrong; we're highly prone to adopting the behavior and opinions of those around us.

One reason for doing so is simply that it saves time, because we don't have to figure things out for ourselves. But we also do it because of social pressure.

For example, in 1951, psychologist Solomon Asch did an experiment in which groups of people were asked to match the length of a line shown to them on a card with one of three other lines drawn on another card. Each participant was then called on individually to match them.

The correct answer should have been obvious — except six of the seven participants were actors, all instructed to give the same, incorrect answer. The one real participant was called on last and, in one-third of the cases, he or she conformed to the group opinion and gave a clearly incorrect answer.

But imitation doesn't just affect logic. Another such type of conformity is called _emotional mimicry_, wherein, when we see another person expressing an emotion — say, smiling joyfully — cells in our brain, called _mirror neurons_, begin to fire, making us smile as well.

Imitation can even explain various forms of media hype. While publishers and producers would love to know how popular a book, song or movie will be, it's nearly impossible to tell because of the power of social influence.

Just take a study in which teens were asked to listen to songs by unknown artists and download the ones they liked. One group of participants undertook the process without outside influence, while the second group could see the number of previous downloads within their group.

The second group gravitated toward songs that had been frequently downloaded by previous participants, showing that participants affected one another's choices. However, the songs that were popular in the first, independent group, did also experience some popularity within the second, indicating that quality also played a role.

So, what others think can influence us tremendously. But there are also instances when we don't imitate others — that is to say, we don't _always_ want to be like everyone else. Let's take a look at this important factor next.

### 4. Many people want to stand out in some way – but not everyone. 

Imagine you're in love with a new band that only a select few people know exists. Then, all of a sudden, the group goes mainstream and everyone else is obsessed with them too. Would you be annoyed by this?

If so, you might be a bit of a snob — but you're definitely not alone.

Traditional economic theory argues that we make choices based on quality and price. In this system of thought, if outside influences have any effect, it's that we follow others' lead. But there's actually another force at play: the _snob effect_, which refers to the basic principle that the more the general population likes something, the less interested many of us become.

People like this don't just want to appear _different_ ; they want to appear better, smarter, richer or more sophisticated. This yearning to differentiate oneself may even explain why so many well-known athletes have at least one older sibling. The first born often strives for academic achievement and the second, in an attempt to differentiate himself, gets into sports.

While some people do yearn to distinguish themselves, not all of us do. The truth is, the extent to which such distinction matters to you is a result of your background.

For instance, in the United States, people feel a strong need to differentiate themselves, whereas the average East Asian cares more about societal harmony and connectedness. But there are also differences within the United States, especially along class lines. So, while a middle-class person will be more eager to set herself apart, say by owning different material objects, working-class people are more likely to imitate one another's choices.

In fact, people from lower-income backgrounds will often like something more if many others do too. This might be because, in working-class communities, familiarity and cooperation are valued more than uniqueness.

> _"You can't be a non-conformist if you don't drink coffee."_ South Park

### 5. Brands and people work hard to show what they’re not, sometimes with negative results. 

Can you imagine a brand paying you to _not_ wear their clothes? Well, for celebrities like Mike Sorrentino, a character on MTV's reality show _Jersey Shore_, this is a reality.

In fact, brands and people alike go to great lengths to ensure that they're not confused with something or someone else — especially if that other entity is undesirable. Brand-name clothing is one way to communicate something about the identity of the wearer, and brands work hard to distance themselves, and their customers, from unfitting identities.

For instance, Abercrombie & Fitch paid Sorrentino to not wear their clothes because he didn't conform to the brand's preppy image.

Or take the luxury brand Burberry, whose unique Burberry pattern used to mark its wearer as a distinguished gentleman. Nowadays, however, this upscale image has been overtaken by a very different one: certain groups of soccer hooligans have made the pattern integral to their uniform. As a result, Burberry removed the pattern from the majority of their product line.

So, proving what you're _not_ can be just as important as proving what you _are_, and sometimes people harm themselves trying to do the former. For instance, studies have found that many young African-Americans, especially those with lighter skin tone, intentionally underperform at school to avoid being misidentified as white, or as "acting white," and end up damaging their career prospects in the process.

Taking this into account, if you want to distinguish yourself, it's safer to stick with costly and disposable goods. After all, you can't stick out by using something that everyone needs and can afford, like bread or toilet paper. Expensive products and activities, on the other hand, are great for distinguishing oneself, as long as others see you using or performing them.

In the end, the fact that not everyone can afford these inessential things makes them inherently exclusive. Such costs could be monetary, like purchasing a yacht; time costs, like the time required to learn Esperanto; or an opportunity cost, like risking unemployability by sporting lots of piercings.

### 6. People crave familiarity, difference and a balance of the two. 

Imagine an evil witch has cursed you and, from now on, every single breakfast you eat for the rest of your life will consist solely of chocolate pudding. That doesn't sound so terrible, does it? After all, who doesn't love familiarity — or chocolate for that matter?

Humans tend to prefer familiar people and things, and with good evolutionary reasons: being attracted to familiar people increases our chances of forming close-knit groups. Likewise, familiar foods, even chocolate pudding, are a better bet for survival than those interesting-looking mushrooms in the garden.

In fact, this pull toward the familiar goes so far that, in the wake of big hurricanes, parents often give their children names that begin with the same letter as that of the storm, like a family in New Orleans naming their daughter Kathy after Hurricane Katrina. These parents prefer such names simply because they've been exposed to the sound of the letter so much.

But, while familiarity is a favored attribute that we seek out, humans also crave novelty. Just like all other mammals, we are always searching for new experiences. For instance, the _Coolidge effect_ describes a mammal, a male hamster for instance, losing all interest in a female of the same species after copulating with her several times.

Then, when a new female is introduced, the hamster in this case becomes frisky, excited by the potential for a new experience. The same effect takes place in females, although it's not as powerful.

But regardless of how much we desire a new person or thing, we'll often remain on guard until we know that it's safe. In this sense, what we're really seeking is the middle ground — and that goes for our identity as well: we want to be familiar _and_ different.

For example, we all want to be unique in our social circles. But to be a part of such a group in the first place, you also need to be familiar. So, you might diverge from your peers by driving a white car instead of a silver one, but won't go so far as to insist on driving a tractor.

### 7. Other people can be a source of great motivation or a major distraction, depending on the task at hand. 

Why do so many people join groups when working toward goals like losing weight or qualifying for competitions?

Well, it's often because others can help us perform better. But they can also distract, and whether or not social influences will help or hinder us depends on how complex a task is.

When a task is relatively simple, we perform better under the gaze of spectators, competitors or companions, since such people keep us motivated. For instance, you're more likely to stick with a diet if other people are holding you accountable; similarly, you'll run faster when in competition with others.

However, if the task is quite difficult and involves learning, the presence of others can hamper your performance for a variety of reasons. First off, other people can be distracting, either because they make us focus on looking good instead of completing the task at hand, or because they involve you in activities unrelated to your goal.

But the presence of others will also make you more physically alert. Your body will be ready to fight, flee or play, none of which are much help when solving a complex mathematical equation.

At the same time, you can use social influence to motivate others to act differently, but this tool has its limits. For example, you can make people consume less energy in their households by appealing to their competitive spirit. In one study, when it came to energy consumption, the thing people cared most about was how much energy they saved relative to their neighbors. This turned out to be a much bigger motivating factor than, say, protecting the environment.

However, this strategy only works if there's hope that you can beat out the competition. So, if a student is so far behind in his schoolwork that he has no chance of catching up, comparing him to his classmates will only be of further detriment to his morale.

### 8. Final summary 

The key message in this book:

**We're influenced by others in everything we do, even if we're not aware of it. From imitating someone word for word, to doing the exact opposite of someone else just to be different, other people are always guiding our actions. The good news is, the more we understand this influence, the better we can handle it.**

Actionable Advice

**Mimic your negotiation partner.**

The next time you're in a negotiation and things aren't looking good, try using mimicry to win the other person's trust. By simply leaning back when your interlocutor does, scratching your head when he scratches his and otherwise subtly copying his behavior, you'll make the other party feel more connected and trusting. Negotiators who mirror their counterpart's behavior are five times more likely to reach an acceptable agreement.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Contagious_** **by Jonah Berger**

_Contagious_ examines what makes a product, idea or behavior more likely to be shared among many people. The book explores the question of whether contagious things are accidents or the results of good marketing, or whether contagiousness is an inherent feature of a product, idea or behavior. It argues that, far from being merely a matter of luck, the majority of very popular products and ideas are the result of a combination of savvy planning and execution.
---

### Jonah Berger

Jonah Berger is a professor of marketing at the University of Pennsylvania's Wharton School. He is the recipient of several academic awards for his research and teaching, and is a contributor to the _New York Times_, _Wired_ and various other magazines. Berger is the author of the _New York Times_ best seller _Contagious: Why Things Catch On_.

