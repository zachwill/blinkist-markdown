---
id: 542927266665620008460000
slug: invisibles-en
published_date: 2014-09-30T00:15:00.000+00:00
author: David Zweig
title: Invisibles
subtitle: The Power of Anonymous Work in an Age of Relentless Self-Promotion
main_color: F7E934
text_color: 787119
---

# Invisibles

_The Power of Anonymous Work in an Age of Relentless Self-Promotion_

**David Zweig**

These blinks are about the _invisible_ people of our society: those who work hard because of their own intrinsic motivation rather than the prospect of reward. Even though we can't see them, they're the backbone of society. However, current cultural attitudes are diminishing their role, and if we want to improve our world, we need to rekindle our appreciation for _Invisibles_.

---
### 1. What’s in it for me? Learn about society’s invisible people. 

If the signs in an airport are confusing and unhelpful for passengers trying to find their flights, people will likely complain. If the signs are perfect, no one will notice. That's the nature of the work done by Invisibles: it's done so well that we can't see it. Invisibles work hard because they want to feel satisfied that they've done a good job, not because they want money or fame. Even though we often can't see these people, our society rests on them.

Unfortunately, we're in the midst of some very negative societal changes. In our digital age, people are encouraged to seek fame and attention. They want external recognition for their work — the exact opposite of what Invisibles want. In these blinks, you'll learn about the critical role Invisibles play, and what we need to do to give them the value they deserve.

You'll also learn

  * why people who get less attention for their work are happier;

  * why the creator of one of the most famous perfumes in the world didn't want recognition;

  * how attention and praise can negatively affect your performance;

  * why Invisibles thrive more in Asian and African cultures; and

  * how we can improve society by drawing inspiration from Invisibles.

### 2. The world is getting “noisier,” and people are becoming more self-centered. 

In our modern world, we're constantly surrounded by "noise." Whether it's the crushing soundtrack of a film, the ever-present blaring of cellphones, or the millions of opinions posted online each day, our world is getting noisier by the minute.

To stand out in this intense and chaotic environment, we perpetually have to shout — both literally and figuratively. People use online social networks to broadcast their thoughts to everyone. Anyone seeking media exposure now views their own life and thoughts as a brand to promote: nothing is too personal to share with the world.

Facebook and Twitter, for example, are designed for people to share personal information. Apps like Foursquare even encourage us to share our current locations, and many people do so without thinking.

This ever-present noise is tipping our society dangerously out of balance.

In the last few centuries, successful nations have always had a healthy balance of working people.

On one side, there are the silent and determined people who drive the economy and other elements of society. They're satisfied with doing good work, even if they don't get public praise for it.

On the other side, there are people in the spotlight. They might be celebrities in Hollywood, the entertainment industry or the media. These are the people who are known for what they do, and they're equally important for keeping the balance.

However, modern society now encourages us to be more like the latter group. Everyone's compelled to stand out as a great individual, and everyone wants recognition. We want to be the lead singer, not the technician behind the scenes. The people who work "quietly" to make things better for everyone are becoming fewer and fewer, and this has some very unfortunate implications.

> _"A world of front men is just a lot of noise."_

### 3. Invisibles are people who disappear from society when they do their work well. 

_Invisibles_ are the exact opposite of what our noisy world is pushing us toward. They're the quiet backbone of society. Invisibles are people who do exceptional work, but try to avoid getting too much praise for it.

We can't function properly without Invisibles, but we don't pay much attention to them. In fact, we usually don't notice them at all, or even realize that their jobs exist.

Invisibles are people who are highly skilled and meticulous. They can be found in any field. They might be the fact checker for the _New York Times_, or the traffic controller at the airport.

Invisibles are the opposite of what many people feel compelled to become today. They focus on the grunt work that's needed for finishing projects, rather than the fame they could earn afterward. Actually, they usually feel uncomfortable if they receive any kind of public praise.

The famous perfumer, David Apel, is a good example of an Invisible. He created some of the best-known smells in the world. He worked for months on the perfume Unforgivable by P. Diddy, and when it was released and sold out immediately, he was pleased to not receive public attention for it. Instead, he basked in the feeling of have done such a great job.

> _"Invisibility is a mark of honor."_

### 4. The desire to be proud of your work is a stronger motivating force than fame or wealth. 

When a person does something well, they should be showered with praise and reward, shouldn't they? Well, perhaps not.

Although we have a tendency to commend someone when they do something well, reward systems and other external factors actually _decrease_ a person's performance.

Even in school, we assume that children are motivated to work hard because they want As. But in fact, research has shown that children do better in school when they aren't pressured by external measurements like grades.

A psychologist named Sam Glucksberg led one of the most famous studies on this topic. He conducted various experiments where groups of participants worked on small projects, either for money or for no reward at all. He studied the ways that the potential for reward affected the participants' ability to do their tasks.

Interestingly, he found that there was a tremendous difference between the two groups. The prospect of monetary reward actually _decreased_ the participants' performance. When they knew they could receive money for their work, the participants narrowed their focus on their projects. They were less creative and took fewer risks, wanting to stick to the most reliable way of earning the money. Creativity, however, is absolutely essential when looking for innovative ways to solve new problems.

Invisibles aren't always _completely_ indifferent to external reward, but that reward is just a side benefit to them. They'll still get some outside recognition, like respect from their co-workers, but their primary motivation is always their internal desire to see their jobs well done.

Dennis Poon, the lead structural engineer for the Shanghai Tower, is another example of an Invisible. When the tower is complete, he won't get nearly as much attention as the architect. Instead, he'll feel rewarded when the skyscraper withstands strong winds and earthquakes, because he isn't in it for the fame.

### 5. Successful Invisibles are highly meticulous perfectionists. 

So what's the most important trait of Invisibles? Well, they're very meticulous, meaning that they're extremely careful and precise about their work. They want to complete their tasks perfectly. After all, this is part of what leads to their invisibility: if you do a job perfectly, no one will notice.

Although you can find Invisibles in vastly different fields of work, one thing they all have in common is that they're very proud of their attention to detail. You can see this commonality in the people managing the technology at a rock concert, the engineers of a suspension bridge, or the anesthesiologist in an operation: they're all paying close attention to every detail of their work. They're all very conscientious.

Invisibles need every detail to be perfect, because they can't stand it if the quality of their work is compromised in any way. Dennis Poon, for example, never skimps on the details of his structures, even when he's pressured to cut costs or have his buildings finished earlier. Instead, he needs every detail to be exactly what he wants. If anything is out of place, he won't be satisfied.

Meticulousness is what helps Invisibles stay so hidden. They know that their projects _need_ to be perfect, or else people will notice them, and they aren't hoping to be noticed. If they produce anything with a flaw, it will draw attention to them and their work. If their project is flawless, they can stay in the background.

There's a good way to test if someone has been painstaking. If you don't stop to wonder who was behind a certain job, that means they did their work very well.

### 6. Invisibles enjoy having responsibility, and often seek it out. 

In addition to meticulousness, there's another common trait all Invisibles have. They enjoy having responsibility. In fact, they thrive on it, and will often seek out high positions in their field.

Even when Invisibles are well-trained, experienced, and respected for what they do, they still strive to be better.

Many qualified workers have mundane and practically "invisible" jobs, but they hate them with a passion. This is not the case for Invisibles. They're often highly trained and revered experts in their fields.

Radiohead's guitar technician, Plank, is a good example of an Invisible. He's been with the band since they started over 20 years ago, and he's the most trusted member of the group. He's responsible for Thom Yorke's guitars, and still helps other crew members set up the concerts so that no mishaps occur.

Because they're so qualified, Invisibles often feel the need to take on more responsibility. Just because they don't want public recognition doesn't mean they're fine with always playing the underdog.

Invisibles usually end up with leading positions in their area of expertise, although we might not notice them.

In TV, for example, we see shows like _Grey's Anatomy_ that focus on surgeons or superstar doctors who single-handedly control the operating theaters and save the patients. In reality, this is far from the truth.

Anesthesiologists are actually the ones who direct the operation, and the surgeons go to them if they need any help. Anesthesiologists must monitor all the surgical procedures, although they won't be the ones to get credit in the end. The surgeons typically receive the most praise, although the invisible anesthesiologists don't mind, because they don't work for praise. Their reward is being the leader, and fulfilling their responsibilities in a way they can be proud of.

### 7. Invisibles are valued differently in different communities. 

Every culture in the world has its own strengths and weaknesses. Each also has it's own values, so naturally, invisibles are valued quite differently depending on their cultural environment.

One of the most prominent differences between Western and Asian societies is the emphasis placed on individuality. This has big implications for Invisibles in either culture.

In Western societies, people are encouraged to be individuals. From a very young age, Westerners are taught to stick up for themselves, stand out, and be independent.

This kind of behavior is looked down upon in many Asian or African societies. In a place like Japan, the role of the individual is more to act like a supporting "column" holding up the community as a whole.

This means that in Western countries, people are often competitive in their workplaces. They want to stand out to the boss and secure promotions, even if that means putting down their colleagues.

In non-Western countries, people are typically less competitive at work. They're more focused on helping the company advance as a whole, even if that means sacrificing their own personal advancement.

So, it shouldn't come as a surprise that Invisibles are much more common in Asian cultures. Conscientiousness, responsibility, and other common traits of Invisibles are highly valued in Japanese culture, for instance.

There are many examples of invisible jobs in Japan. Some are traditional, like the artisans who make calligraphy brushes, and some are modern, like the creators of manga and anime. Most people with jobs like this will never be famous, but they still find quiet happiness in the quality and beauty of their work.

Japanese companies are also more supportive of Invisibles. They often try to acknowledge and support people who aren't in leadership positions. Some companies even have days where high-level leaders take the time to recognize and appreciate their workers.

### 8. We need to place more emphasis on quality work if we want to improve our society. 

So now that we've seen how Invisibles work, what does this mean for us? What does it mean for our future?

We're in need of a change. Our society is drifting off track, and we need to direct it back on course.

The financial crisis has already been a painful sign that something is terribly wrong. Banks gamble away our money, and everyone's in competition with each other. A new kind of dog-eat-dog society is emerging.

In response to this, we spend most of our time trying to brand ourselves by sharing our lives on social media networks. Even our social media usage has become competitive: we want to show we're more interesting than anyone else.

We used to value high-quality work, but we now prefer quick fame and wealth. Our current obsession with micro-celebrities will only harm us in the long run. More and more people will stop seeking a good education or job. Instead, they'll strive to be YouTube celebrities or get-rich-quick bankers.

What we truly need is inspiration from Invisibles. More of us must take on their qualities, especially by putting aside our own personal advancement. If we focus less on getting attention, and more on improving society, we might be able to make significant changes.

The focus on individuality has been an important part of American history, and it certainly has it's own place and time. However, it's starting to become too much. We need to distance ourselves from our individualism, and work to be more humble and reserved. If we place less emphasis on ourselves, we can get back on track with improving our society.

If we can rekindle our appreciation of high-quality jobs that give workers more fulfillment, we can steer through our noisy world and make it out safely in the end.

### 9. Final summary 

The key message in this book:

**Society is held together by Invisible people who pull the strings without us noticing, and we need them to function. Unfortunately, we're in the midst of a crisis: these days, everyone is being pushed away from this valuable "Invisibility." We need to recalibrate our support for Invisibles if we want to improve our noisy world.**

Actionable advice:

**Looking for a new job? Ask yourself what's more important: happiness or attention?**

It might seem like you'll work harder if you know a promotion might be on the horizon, but actually, internal motivations are more important. Do something you have passion for, and work to please yourself — not others. Not only will you feel more satisfied, the quality of your work will be higher.

**Suggested** **further** **reading:** ** _Leaders_** **_Eat_** **_Last_** **by Simon** **Sinek**

_Leaders_ _Eat_ _Last_ explores the influence that neurochemicals have on the way people feel and consequently act, and examines the discrepancies between how our bodies were designed to function and how they function today. Ultimately, we need true leaders to direct us back on the right path.
---

### David Zweig

David Zweig is a prolific writer whose work has been featured in the _Atlantic Monthly,_ the _New York Times_ and the _Wall Street Journal_. He's also a singer, guitarist and producer, and has released two albums. His first novel was called _Swimming Inside the Sun_.

