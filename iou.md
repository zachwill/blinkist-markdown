---
id: 54d89b8b383963000a780000
slug: iou-en
published_date: 2015-02-11T00:00:00.000+00:00
author: John Lanchester
title: I.O.U.
subtitle: Why Everyone Owes Everyone and No One Can Pay
main_color: F43150
text_color: C22740
---

# I.O.U.

_Why Everyone Owes Everyone and No One Can Pay_

**John Lanchester**

In _I.O.U.,_ author John Lanchester explains how the 2008 economic collapse occurred, and critically, why nobody saw it coming. With an analysis that delves into the workings of our global financial system, the book shows how greed, confusion and irresponsibility brought the economy to its knees.

---
### 1. What’s in it for me? Learn how bankers were allowed to nearly destroy global finances. 

Even though the events of the 2008 financial crisis seem distant, we continue to live with its consequences.

From Greeks suffering under austerity measures to Americans unable to get a housing loan to rising unemployment figures all around the globe, the crisis is still with us.

How did we get here? Why was such risk allowed to build up in the financial system, and importantly, why didn't anyone do something to stop it?

These blinks help to explain the big mistakes and base greed which paved the way for the crash, and crucially, show why little was done to prevent us from flying over a financial cliff.

In the following blinks, you'll discover

  * why communism actually raised living standards in capitalist economies;

  * why it's not wise to lend money to people who have no money at all; and

  * why it's perfectly acceptable to blame the bankers for the crash, after all!

### 2. Following the fall of the Berlin Wall, inequality grew exponentially in capitalist democracies. 

What caused the global financial crash in 2008?

Although things like poor government regulations and financial greed played a role, to truly understand why the crash happened, we need to look at the bigger picture.

The excesses of our modern financial system were born from a certain political _climate_ that emerged after the fall of the Berlin Wall in 1989.

Let's look at the climate before the fall, to better understand what came afterward.

During the Cold War, the United States and the Soviet Union represented two competing political and economic systems, capitalism and communism, respectively. And as it happens, the capitalist West actually benefitted from its conflict with the communist world.

That's because the United States and its allies were essentially competing in an ideological beauty contest. Capitalists and communists alike strove to persuade the world that their system produced a better way of life.

Thus, capitalist systems supported by democratic governments did everything possible to show the advantages of their side. This led to unprecedented benefits for the middle and lower classes (groups not naturally favored by the free market), such as free education, steadily growing incomes, a strong welfare system and so on.

But when the Berlin Wall fell, the communist system fell too and the beauty contest came to an abrupt end. And thus victorious capitalists no longer had to act as if they cared for the poor and dispossessed.

In other words, there was no longer a political incentive to limit inequality.

And so, the wealth of the people at the very top — the 0.1 percent — _exploded_. Since 1980, the average income for this group has jumped by more than 700 percent. And meanwhile, the income of everyone else stayed more or less the same.

### 3. Once free-market capitalism caught on and regulations waned, the financial industry boomed. 

As we've learned, our capitalist economies have engendered increasing wealth inequality. But how did we come to adopt and champion such an imperfect economic system?

Well, our form of capitalism — called _Anglo-Saxon capitalism_ — was first implemented in the United States and the United Kingdom, where it produced such staggering wealth that it was soon adopted by countries all over world.

Let's dig a little deeper to understand what Anglo-Saxon capitalism is all about. Ultimately, this form of capitalism is based on the assumption that a pure and free market is resilient and self-correcting.

Thus, proponents of Anglo-Saxon capitalism believe that there should be minimal government regulation, taxation or spending by the state — all to ensure that private firms (especially banks) are unconstrained in their pursuit of profit.

The underlying idea: when it comes to making money, companies know best.

And to a certain extent, this kind of capitalism _works_. The gross domestic product (GDP) of both the United States and the United Kingdom grew massively, and other countries sought to emulate such growth. Russia, Ireland, Poland and Australia all worked to deregulate their own markets and adopt the Anglo-Saxon capitalist system.

As a consequence, the financial system too started to boom. Consider that from 1973 to 1985, financial services made up 16 percent of all U.S. corporate profits; yet in the past decade alone, financial services totaled 41 percent.

How can we account for this? Without government regulation, bankers were free to invent new money-making mechanisms — and they did.

Today, the financial industry no longer revolves solely around selling stocks and shares; rather, bankers deal with much more complicated and risky financial products.

And we'll find out all about these products — and what makes them so risky — in the following blinks.

### 4. Risky financial products called derivatives have transformed – and crippled – the banking system. 

In the late 1970s, the banking system underwent a crucial change with the development of new, highly profitable financial products called _derivatives_.

What's a derivative? It's basically _the right to buy something_ — and you can sell or trade this right to someone else.

For example, a farmer agrees to sell to you next year's harvest for a certain price. Congratulations, you've purchased a derivative! Now, if the harvest is more profitable than expected, you've earned some money, since you agreed to buy the harvest below its actual value. However, if your contract to buy is higher than the actual value, you end up essentially losing money.

This example is straightforward, but financial derivatives tend to be far more complex and thus, incredibly risky — potentially leading to huge losses.

The _credit default swap (CDS)_ — an insurance on debt — was one of these highly dangerous derivatives, and it was widespread in the years leading up to the financial crisis.

Here's how a CDS works. Let's say Joe takes out a mortgage, and then the bank sells the debt to a trader. The trader buying the debt is taking on risk, because if Joe defaults, the trader's investment will be worthless.

However, if the trader takes out a CDS for an annual fee, then the loss will be protected in the event that Joe defaults. At this point, the CDS can also be traded; here, the buyer of the CDS would get the annual fee but still have the obligation to pay out.

Ultimately, CDS contracts were incredibly risky as they were often packaged with other forms of derivatives, creating very complex bonds.

Furthermore, these bonds were sold all over the world, spreading the hidden risks. After all, a buyer on the other side of the world would have no way of knowing whether the original mortgage borrower was likely to default!

> _"No wonder Warren Buffet called CDSs 'weapons of mass destruction!'"_

### 5. A booming financial sector needed more fuel for its fire, so risky mortgages became a big thing. 

Derivatives changed the rules of banking by hiding risk and spreading it globally.

While this was a risky strategy, if the original mortgage borrower pays back his loan with no problem, the risk for a credit default swap (CDS) derivative holder is minimal.

Unfortunately, another risky practice succeeded in undermining this already risky system.

Banks began to issue _subprime mortgages,_ or mortgages for low-income individuals or people with poor credit scores.

Let's back up a bit. The financial system at the time was booming, yet it needed new fuel to keep growing at the rates that the equally booming financial sector was demanding. And that's where subprime mortgages come in.

In the past, risky mortgages like subprime mortgages were rare. But this changed for two reasons. One, the financial system was hungry for cash; and two, the proliferation of derivatives made it easy to sell debt.

Thus people for whom it was near impossible to get a mortgage previously were now offered huge loans to purchase houses they really couldn't afford.

And to make matters worse, as subprime lending increased, another derivative came into existence in order to trade all this newly created debt: _the CDO (collateralized debt obligation)_.

CDOs work like this. First, debt from subprime mortgages was collected together into huge pools. Then the pools were spliced and mortgages with similar risk levels were collected together, creating packages which were then sold globally.

So, what's the problem with this practice? Well no one could possibly know whether the borrowers would actually pay back their loans. Since subprime mortgages were new, there was zero data on the level of risk.

Yet bankers didn't hesitate. They thought by trading CDOs, they were passing the risk to someone else.

### 6. Irresponsible mortgage brokers used predatory tactics to issue loans to those who couldn’t repay. 

Thanks to a boom in subprime mortgages and CDOs, profits were flowing into the financial sector.

Yet the good times led to more greed. Bankers became increasingly willing to try anything to keep the cash coming in, leading to _predatory lending_ practices.

Mortgage brokers would do anything to sign up new borrowers, whether they were capable of paying back the debt or not — and often at unusually high interest rates. These debts were then bundled into new CDOs, and sold.

To understand just how underhanded predatory lending became, consider that Wells Fargo, America's fourth-largest bank, systematically targeted African-American church members for subprime mortgages — even though many could qualify for better loans.

Here's the truly villainous part: in an effort to push these terrible mortgages, the bank even persuaded church leaders to encourage their congregations to take out subprime loans.

But how could these practices be allowed? Subprime lending was like the Wild West: there were no rules.

Before the crash, there were 250,000 mortgage brokers in the United States. And these people didn't need a license or any qualifications at all to make contact with potential borrowers.

Even worse, borrowers weren't required to give any proof of income, and could apply for a mortgage using "stated income" — that is, a borrower could say anything and the broker would believe it.

Unsurprisingly, in a 2007 survey some 60 percent of subprime borrowers said that they had lied about their income.

And from this scenario, you can guess what happened. People defaulted on mortgages, and the housing bubble burst. And as all this risk was compounded and sold across the globe, no one was immune once it all came tumbling down.

And yet, looking back, the folly of subprime mortgages and derivatives seems obvious. Why didn't anyone realize what was happening and try to put the brakes on?

> _"Banking is all about the management of risk; and the bankers massively failed."_

### 7. Bad math and irrational behavior facilitated an enormous build-up of risk in the banking system. 

So why didn't anyone see the crash coming?

There are two broad reasons. One lies with the mathematical models used in the banking system, and the other has to do with human nature.

Starting with the models, it's important to realize that banking is all about assessing risk. The financial sector hires thousands of mathematicians (called "quants") solely tasked with building complex mathematical models to calculate risk.

And although the predictive powers of quants seemed unimpeachable at first, their complex models and formulas failed to predict the eventual crash.

To understand how badly they failed, consider that according to financial models created by analysts at investment bank Goldman Sachs, a crash could only occur if there were "25-standard deviation moves, several days in a row."

That's math-speak for, "It's about as likely as winning the lottery. Twenty-five times. In a row."

However, the crash happened. So obviously the math was wrong!

But where was common sense in all of this? Bankers aren't stupid; everyone knew they were participating in risky practices. So why did they choose to ignore the risks?

It comes down to biology. When we take risks, our cognitive understanding is limited. In other words, humans are not capable of acting completely rationally. In fact, as psychologist-economists Daniel Kahneman and Amos Tversky have shown, our brains are hardwired to make certain mistakes when it comes to assessing risk.

And it doesn't help that the banking system is especially tolerant of risk, since risk is linked to higher profits.

So while you may try to avoid risk at all costs, bankers see it as a path to potential profit. They actively seek it out and spread it through the system.

And the faulty math? That just meant they could take even more risks.

### 8. Ratings agencies too had strong incentives to ignore risk and support subprime-based derivatives. 

You may wonder, where were the rating agencies, and why weren't they working to downgrade toxic debt, to keep it from spreading through the system?

The unbelievable answer is that many of them simply didn't want to.

There's a reason for this. The rating agencies were paid by the very same banks and investors whose products they were supposed to grade.

Therefore, they had a strong incentive to ignore risk and hand out good grades, or firms and investors would simply go to a competing rating agency. So ultimately, it was simply more profitable for rating agencies to ignore the warning signs.

And of course at this time too the market for subprime mortgage-backed securities took off, so there was a lot of money involved for rating agencies, too.

In fact, an obscene amount of money, as the fee for rating CDOs was three times higher than what was paid for more traditional financial products, like corporate bonds.

So business boomed for everyone. Moody's, a leading rating agency, saw revenues _double_ from 2002 to 2006.

And so rating agencies gave AAA grades — the highest grade possible, one which was intended only for the most secure investments — to 90 percent of mortgage-backed CDOs.

Did you miss that? We'll repeat: Rating agencies claimed that financial products that were based on mortgages nefariously issued to uncreditworthy borrowers were _some of the safest investments you could make_.

### 9. Blind faith in free-market economics led to limited government oversight of the financial sector. 

Many people blame the government for the financial crisis. While the government didn't cause the crisis, it definitely could have prevented it by regulating the banking system.

So why wasn't there more government oversight of the banking industry?

Part of the answer lies with the prevalence of Anglo-Saxon capitalism. After the fall of the Berlin Wall, nearly every Western government committed itself to the idea of free markets, and thus, the financial crisis was preluded by years of deregulation.

In other words, the government let banks write their own rules.

This was true even in the case of new, risky derivatives (like CDOs and CDSs) that might have been subject to government oversight. The banks preempted regulation by creating the ISDA — the International Swaps and Derivatives Association — which fought battles with governments all over the world. And eventually, governments gave up and industry won the right to regulate itself.

Of course, the ISDA had an advantage over the government: legislators had almost no clue what derivatives were or how they worked. Financial lobbyists could tell the government anything, and officials had little choice but to believe them.

In the end, banking lobbyists not only managed to bypass government regulations but also actually persuaded the U.S. Congress to pass a law _forbidding_ any regulation on derivatives.

It's easy to blame the government, but to be fair — legislators simply didn't understand the risks of these new financial practices.

However, a few governments did manage to adequately regulate derivatives, and those governments were much better off in the period following the financial collapse.

Canada, for instance, had far more financial sector oversight than most countries. And as a result, it was the only G8 country that didn't have to bail out its banks.

And in fact, Canada is a great example of a thriving economy without an unconstrained financial sector. Since 2004, Canada's average income has risen by 11 percent; meanwhile, incomes in the United States rose by only 5 percent in the same period.

### 10. Final summary 

The key message in this book:

**Following the fall of the Berlin Wall, free-market capitalism became the dominant economic system and the financial industry grew to massive proportions. At the same time, unregulated financial products transformed the banking system. Ultimately, questionable practices and irresponsible behavior facilitated an enormous build-up of risk, ultimately leading to financial collapse.**

**Suggested further reading:** ** _After the Music Stopped_** **by Alan S. Blinder**

_After the Music Stopped_ explains and analyzes the causes of the last decade's great financial crisis. It details the mechanics of the underlying problems as well as the sequence of events as panic began to set in. Finally, it also explains how the US government managed to halt the chaos and rescue the economy.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### John Lanchester

John Lanchester is an award-winning journalist and novelist. His work has appeared in _The New York Times_, _The New Yorker_, _The_ _Guardian_ and other publications. In 2012, he published _Capital_, a novel about the financial collapse.

