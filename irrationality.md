---
id: 57925909172bb2000304560d
slug: irrationality-en
published_date: 2016-07-28T00:00:00.000+00:00
author: Stuart Sutherland
title: Irrationality
subtitle: The Enemy Within
main_color: 2CA5DA
text_color: 1C6A8C
---

# Irrationality

_The Enemy Within_

**Stuart Sutherland**

_Irrationality_ (1991) is a guide to illogical decisions, unreasonable actions and irrational behavior as a whole. These blinks reveal how people tend to be more irrational than rational, examines several reasons why and offers solutions as to how we can become a little more logical in our decision making.

---
### 1. What’s in it for me? Find out about your own irrationality. 

Human beings are rational beings. Since the Enlightenment in eighteenth-century Europe, this simple fact has been a central assumption of scholars and thinkers and continues to be something we are taught from an early age.

But are we really as rational as we think?

Well, we're actually far less rational in our daily behavior than we might suspect. Whether it's when we judge other people by their looks, when we feel we should follow our intuition or when we refuse to change our minds even though it flies in the face of reason, irrationality is part of our daily routine, and it often goes completely unnoticed.

These blinks will show you what irrationality entails, how we are irrational in many situations and what we can do to become more rational.

You'll also learn

  * why we think good-looking people are intelligent;

  * how peer pressure can derail rational thinking; and

  * why eighteenth-century people thought that turmeric cured jaundice.

### 2. Irrationality is more common than people think, and it’s the result of ignoring knowledge. 

We tend to think that human beings are deeply rational beings, and the basis for this idea can be traced back thousands of years. From Aristotle's claim that man is a "rational animal," to Descartes' famous statement, "I think, therefore I am," to Kant's "have the courage to use your own reason," a common belief throughout history has been that humans are fundamentally rational.

But this might not be true after all.

When we meet a new person, we often judge them in an instant, based solely on their looks. This is both exceedingly common and totally irrational, as people often turn out to behave very differently than they appear.

So, clearly we're prone to _irrationality_ — but what does that word actually mean?

In a nutshell, irrationality means deliberately forming conclusions that aren't based on knowledge. So, it might be rational for a young boy to climb a tree to try to touch the moon. But the same action would be absolutely irrational if performed by an adult astronomer who knows how far away the moon is.

Basically, the breadth of our knowledge plays a role in determining the rationality of our actions. But, while rational thinking is based on knowledge, rational conclusions can also be false. For example, people long believed that all swans were white; this was a completely rational assumption until the Australian continent was discovered, where black swans are common.

This is an example of how insufficient or false knowledge can produce false conclusions, despite rational thinking. But false rationality is not to be confused with irrationality. Irrational thinking differs in the sense that it's deliberate.

Inadvertently forgetting to carry the one when doing math will produce an error — but it wouldn't be the result of irrationality. Irrationality would be to ignore knowledge despite its ample presence, such as by thinking someone is unsuited for a job despite her résumé being a perfect fit for the requirements.

So, making an irrational decision requires deliberate action. But why do humans do this?

> _"Everybody is irrational some of the time, and the more complex the decisions to be taken, the more irrational they are."_

### 3. Irrationality affects individuals and organizations alike. 

People make irrational decisions for a variety of reasons, and this tendency plagues both individuals and major organizations. In the case of individuals, people tend to distort their sense of reality in order to maintain irrational behavior and thoughts.

After making a decision, we often glorify our choice because we don't want to admit the possibility of being wrong. Likewise, we tend to lower our opinion of the option we didn't choose.

In fact, studies have found that when teenage girls are asked to rate a series of albums and subsequently choose between two albums that they gave a similar rating, their opinion of the record chosen in the second half of the exercise was much higher than the one they chose it over.

But distorting our sense of reality isn't the only way we act irrationally; we also like to hold on to irrational behaviors and thoughts.

This happens all the time in the stock market, where people feel an irrational need to keep an investment that has dropped in value and shows no signs of rising again. The clear rational choice in this scenario would be to hold on to the stock if it was beneficial and let go of it if not.

So, these are ways individuals are affected by irrationality — but what about bigger groups?

Well, a common way that irrationality manifests in organizations is through structures that conflict with organizational goals. In fact, organizational structures often encourage selfish behavior that benefits individuals at the expense of the whole.

At investment banks, bonus payments tend to encourage bankers to place risky bets that could bring major profits and, therefore, large bonuses. But bankers are rarely held accountable for bad bets they make on risky assets; it is the organization as a whole which has to bear the brunt of their mistakes.

Another example can be seen with the financing of departments in an organization. The amount of money allocated to a given department is often based on how much they were given the year before, without any regard for how that money was spent. This type of policy encourages overspending, not organizational well-being.

### 4. When and how we receive information is a major factor in our irrationality. 

As you might know, you're much more likely to be killed in a car accident than by a shark attack while swimming in the ocean. Even so, the 1975 movie premiere of _Jaws_, a film about a man-eating shark, caused a sharp drop in sea swimmers in California.

The frightened Californians were succumbing to something known as the _availability error_, wherein people irrationally ignore known facts and pay attention to information that makes the biggest impression on them, or that comes to mind most easily. In other words, what's most directly "available."

Our tendency toward the availability error, and therefore irrationality, increases when we're confronted with emotionally charged information. For instance, the annual number of deaths from strokes is 40 times higher than those caused by accidents; however, many people still believe that they're more likely to die in an accident because events like plane crashes are constantly featured in the media and are highly emotionally charged.

In fact, emotionally powerful information to which we've recently been exposed comes into our minds much faster than other information. As a result, people are more likely to watch a movie for which they've recently seen a trailer, than one they've merely read about.

Emotionally charged images are more "available" to our minds than simple text.

The availability error can also cause all sorts of other irrational behaviors. A notable example is what's known as the _primacy error_, a situation in which we form irrational beliefs based on a first impression. We do this because our first impression is what comes immediately to our minds.

Yet another example is called the _halo effect_ : when someone has a noticeable good trait, we tend to believe they have other good traits as well. For example, we tend to think that people with good looks are also intelligent.

### 5. Doing what is socially expected and publicly announcing decisions can lead to irrational behavior. 

Conforming to social norms is often one of the most rational things you can do. For instance, it's rational to conform to driving on the correct side of the street to avoid a car accident. But conforming to social norms can also produce irrational behavior.

For example, the psychologist Solomon Asch demonstrated that subjects make irrational choices simply to conform to what society expects. He did so by showing his test subjects two cards: the first one had one line on it and the second had the same line as well as two others, one considerably longer and the other shorter than the first line.

The participants sat in a circle and were asked to a choose a line on the second card that was the same length as that on the first. Interestingly enough, many participants conformed by copying the answer given by an accomplice planted by the psychologist, which turned out to be wrong.

And in the real world, it happens all the time that people stick to publicly announced decisions even if they're irrational. In fact, people are more likely to stick with decisions that they announce publicly than those made in private.

In the end, it's socially normative to do what we say we will. So, when we make an incorrect decision, we irrationally scramble to save face, instead of admitting that we changed our minds.

Just take the several studies done by advertising companies, which found that strangers who publicly declared that they'd buy a product were more likely to actually do so than those who privately wrote down their intention.

This inclination can produce something called the _boomerang effect_, which holds that once a strong view is challenged, it becomes even stronger. For instance, if someone strongly commits to an irrational belief by publicly announcing it, challenging their assertion can make it even stronger because the person in question will dig in their heels, wanting to save face.

> _"Obedience is behaving in the way dictated by a figure of authority, conformity, is behaving in the same way as one's equals."_

### 6. Giving rewards is irrational and being deprived of choice spurs irrational behavior. 

We all love rewards, and it might seem like giving them is a rational form of encouragement. But the exact opposite is true: while verbal praise might be a good motivator, since it can be internalized by the recipient and keep encouraging them for times to come, physical rewards are actually quite _dis_ couraging. In fact, rewards don't actually make people perform better at all, and giving them is thus totally irrational.

In one experiment with students, the participants were asked to write headlines for their college newspaper. One group was paid while the other was not. In the end, the unpaid students wrote more headlines, showing that the incentive of a monetary reward produces worse performances.

A far better motivator than simple rewards is to foster a desire for self-improvement. It has been shown that students learn best when they view their teacher's comments as helping them improve. Likewise, the more specific the comments, the more likely the students are to be encouraged in their learning.

Another factor that promotes irrational behavior is to be deprived of the opportunity to choose. One experiment demonstrated this by giving ten-year-old children a toy — but the way they were given their toys differed:

For both sample groups, two toys were presented to each individual child, with one of them being the known favorite of the given child. In one group, the experimenter would then give the child their favorite of the two, but also mention that the two toys looked the same to him and that he had chosen this toy for the child for no reason in particular.

This group of children enjoyed their favorite toy much less than they had before and liked it less than the children in the other group, who were allowed to choose their favorite toy freely, without comment from the experimenter.

In other words, the children irrationally lowered their opinion of their favorite toy simply because someone else chose it for them — and did so, apparently, at random.

> _"It is important to remember that both rewards and punishments may be damaging and that the opportunity to choose freely where possible is beneficial."_

### 7. Denying information to maintain beliefs, as well as our emotions, can make us act irrationally. 

It's common for logic to be juxtaposed with emotions, and it's thus of little surprise that the latter are strongly tied to irrational behavior. For instance, we might love our partner so much that we believe she's the greatest person on the planet, or might avoid doctors because we're terrified of finding out we're sick. In short, emotions make us behave irrationally because they distort our worldview.

But emotions are different than feelings. We might _feel_ cold or tired; on the other hand, emotions are a combination of certain feelings with our tendency to act and think in certain ways.

Stress, depression and jealousy are emotions that, like many others, make us act irrationally because they muddy our perspective. Depressed people tend to see the world in a much more negative light, while optimistic people can often see the positive side of things.

In addition, strong emotions of any type prevent us from focusing on rational decision making and make it difficult to consider alternatives, thereby producing irrational behavior. One experiment presented participants with a word and asked them to choose its anagram from a list of six others.

Some of the people were put under stress by the threat of electric shock and they performed much worse than those who weren't threatened. They scanned the alternatives less carefully, made fewer correct choices and were four times more likely not to examine their choice before making a decision.

So, emotions cause irrational thinking — but so does failing to seek contradictory evidence in order to maintain our beliefs. In fact, people seek confirmation of their beliefs much more often, despite the fact that it would be more rational to critique them and see if they actually hold up.

One study showed that we seek confirmation of our opinions even if they are self-antagonizing: students who thought negatively of themselves preferred to share rooms with people who also had low opinions of them.

> _"Everyone under strong emotion can be driven to irrational thought and action."_

### 8. Irrationality prevents us from properly assessing correlations and cause-and-effect relationships. 

It wasn't long ago that scientists couldn't show a correlation between smoking and lung cancer. In fact, the connection between the two went unnoticed for centuries before the scientists Richard Doll and Richard Peto released certain statistics in the 1970s.

So, why did people fail to make this connection?

Well, the inability to see connections between events — or the inclination to see connections that don't exist — is often a result of irrationally ignoring numbers and statistics. But despite understanding the numbers, we still make mistakes regarding associations between events every single day.

One reason for this is the tendency to confuse the causes and effects of events. For instance, questionnaires have shown that people often think it's more likely for a mother with blue eyes to have a daughter with blue eyes, than for a daughter with blue eyes to have a mother with blue eyes. Obviously, this is irrational because the likelihood is exactly the same, the information is just phrased differently.

But seeing a cause-and-effect relationship where none exists is also a major issue. This type of irrationality is known as an _illusory correlation_. For example, we often see false cause-and-effect relationships by connecting like events or ideas. Just take the bright yellow herb turmeric, which, until the late eighteenth century, was thought to cure jaundice, a yellowing of the skin and eyes.

Another good example of such irrational connections is psychoanalysis. This field is prone to false connections, such as the supposed link between humans breastfeeding during our infancy and a later adult preoccupation with the mouth as expressed through kissing, smoking or talking too much.

And that's not the only way psychoanalysis falls victim to false cause-and-effect relationships. Psychoanalysts also tend to incorrectly attribute their patients' improvement to their treatment; studies have found that placebo treatments that have a practitioner see a patient, listen and make supportive noises without offering any proper therapy, work just as well or better than true psychoanalysis.

The bottom line is that the simultaneous occurrence of two events doesn't necessarily mean that one caused the other.

> _"Mathematics is a tool to rational thinking: it is impossible to be rational without it whether you are going to the horse races, designing an aeroplane or … selecting an employee."_

### 9. Overconfidence and intuition lead to irrational decisions. 

Whether the task at hand is properly assessing the size of a parking space, the time needed to boil an egg or the strengths of a prospective hire, our intuition can often prove incorrect. We tend to be overly confident in our judgments and our own gut feelings.

For instance, one study found that people who were 100 percent sure of their ability to spell certain words, actually only spelled 80 percent of them correctly. Another survey found that 95 percent of British drivers believed they were better than the average motorist, while most people think they'll live longer than the average person.

Naturally, such feelings of overconfidence are irrational, because it's fundamentally impossible that the vast majority of anything is better than the average. And this is a problem because overconfidence can mean big trouble. As an example, a fire at the Browns Ferry nuclear reactor in Alabama broke out when a technician, who was overconfident in his abilities, checked for an air leak with a candle.

And having too much faith in our intuition can breed similar problems, because it causes us to make irrational decisions. Job interviewers, for instance, often trust their intuition to judge the cognitive abilities of the candidate in a face-to-face interview; this approach is totally irrational.

It would be much more rational to have the candidate perform a series of verbal and spatial tests. The inclination to rely on intuition instead goes back to the halo effect, the tendency to view someone as inherently good based on one good feature, like self-confidence or good looks.

So, instead of trusting our intuition to make good choices, we should use _actuarial prediction_. This basically means using data and formal mathematical analysis to form judgments.

### 10. Knowledge of statistics improves rationality. 

So, you've seen how people are irrational in a variety of ways; but is rationality really so desirable?

Well, the truth is, being rational isn't as important in the personal realm since your personal choices, like whether to have pasta or rice for dinner, aren't especially important. And when they are more important, like when choosing a career, there are so many unknown factors that rationality only marginally increases the likelihood of a successful outcome.

However, in society at large, whether in engineering or medicine, irrationality has a serious impact.

So, how can you become more rational?

Through the power of statistics. It's no coincidence that economics, psychology and medical students all learn basic statistics: a knowledge of this field can drastically diminish your chances of making incorrect, irrational choices.

For instance, one study asked subjects if a large hospital that averaged 45 births per day or a smaller one with an average of 15 births per day would see more days within a period of a year on which 60 percent of the babies born were boys. The subjects assumed that the number of days would be the same for both hospitals, but some knowledge of statistics quickly reveals that the smaller hospital is more likely to have days with 60 percent male births. Why?

The more often you repeat a random experiment in which different events occur with known likelihoods, the more closely the observed frequency of the events occurring will resemble their true frequency. This is called the _law of large numbers_ because the larger the sample, the more likely a true frequency of events is to occur within it. Therefore, a 50-50 split between male and female births is more likely in the larger hospital.

And that's not all statistics are good for — they can also improve rationality when you're trying to evaluate the results of a decision as a product of your desire for it and its likelihood of occurring. This is known as _utility theory_ and it's the best way to make rational decisions since it accounts for both the probability of an outcome and its desirability.

### 11. Avoid irrationality by paying attention to the way questions are phrased and writing out pros and cons when making decisions. 

While an understanding of statistics might help you make more rational decisions, especially when it comes to serious and complicated issues, it's impractical to spend loads of time making calculations. So, here are some simpler ways to avoid irrationality.

One way is to know that the way a question is phrased can irrationally influence your response. For example, in one study, certain subjects were asked whether they'd rather save 200 people from an imaginary lethal disease, or attempt to save 600 people with a 33 percent likelihood of success. Another group was asked to choose between the risk of letting 600 people die with a probability of 67 percent that they would die, or letting 400 people die for sure.

The two groups generated entirely different choices — but the two questions actually ask the exact same thing, they're just formatted differently.

A similar bias can be seen in questions involving scales. When asked to indicate the number of headaches one has per week from 1-5, 6-10 and so on, or to rate our satisfaction with a product or service on a scale that includes "dissatisfied", "satisfied", "very satisfied" and "extremely satisfied", people will tend to choose one of the two middle options regardless of whether it reflects their real feelings.

Understanding question bias is key to avoiding irrationality. But another strategy is simply to list pros and cons when making complicated decisions. After all, people can only hold a certain number of ideas in their minds at once; writing such a list will help you make more rational decisions and avoid overlooking important information.

Writing down ideas protects your mind against information overload and, therefore, the risk of bad decisions. Even Charles Darwin was said to have used this method when trying to decide whether or not to get married!

### 12. Final summary 

The key message in this book:

**Humans tend to be irrational much of the time, and this can affect us negatively. We can reach better outcomes by improving our rational decision-making abilities, which depends on trading intuition for statistical methods, weighing the pros and cons of a situation and taking all available information into account.**

Actionable advice:

**Take your time when it comes to big decisions.**

With so many decisions to make, it might be a good idea to save hard thinking for really important situations. But when you _are_ confronted with a serious decision, be sure to take the time to think it through and act rationally. After all, an irrational slip could have serious consequences!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Predictably Irrational_** **by Dan Ariely**

**_Predictably Irrational_** **explains the fundamentally irrational ways we behave every day. Why do we decide to diet and then give it up as soon as we see a tasty dessert? Why would your mother be offended if you tried to pay her for a Sunday meal she lovingly prepared? Why is pain medication more effective when the patient thinks it is more expensive? The reasons and remedies for these and other irrationalities are explored and explained with studies and anecdotes.**
---

### Stuart Sutherland

Stuart Sutherland was a renowned psychologist and writer who taught at Oxford University and the University of Sussex. He is best known for his book _Irrationality_ and a personal account of his struggle with manic depression, titled _Breakdown_.

