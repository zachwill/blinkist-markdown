---
id: 58bd5a220d93aa00045b76bf
slug: irresistible-en
published_date: 2017-03-08T00:00:00.000+00:00
author: Adam Alter
title: Irresistible
subtitle: The Rise of Addictive Technology and the Business of Keeping Us Hooked
main_color: FEF038
text_color: 706A19
---

# Irresistible

_The Rise of Addictive Technology and the Business of Keeping Us Hooked_

**Adam Alter**

_Irresistible_ (2017) shows how dangerously dependent we've become on the smartphones, tablets, video games and social platforms that we've surrounded ourselves with. Is our attachment to these devices strictly related to the convenience they provide? Or have we actually grown addicted to the psychological rewards they offer?

---
### 1. What’s in it for me? Understand exactly why you should limit your time in front of screens and devices. 

If you are reading this you are almost certainly looking at a screen of a smartphone, tablet or computer. And chances are this isn't the only time you've looked at a screen today, whether you were catching up on social media, reading your favorite blog or pinning some inspirational pictures. Okay, so what's wrong with that?

Well, in an age where smart devices and computers have become an essential part of life, there's a heightened risk of getting addicted to these new toys. We simply find them irresistible. In these blinks, we'll explore exactly why that is. We'll get down to the roots of this irresistibility and offer some advice to obviate the problem — because, as we all know, sometimes we really can get too much of a good thing.

In these blinks, you'll find out

  * that you probably spend twice as much time online as you think you do;

  * why answering emails quickly isn't productive; and

  * why you get so much pleasure from all those likes on Facebook.

### 2. You really can be addicted to your smartphone or video games. 

How would you feel if you couldn't use your smartphone for a day? How about an entire week? It's not a scenario most of us enjoy contemplating; indeed, most of us start to get anxious if we're separated from our device for just a few hours.

Unsettlingly, this modern relationship with phones has revealed a new type of addiction.

Perhaps you've asked yourself whether you're spending too much time on your phone and not enough time with your friends and loved ones. If you have, you're not alone. This is exactly what Kevin Holesh was thinking in 2014 when he developed a new app called Moment.

Moment's purpose is to collect user data and determine exactly how much time people are spending glued to their mobile devices. While Moment's users generally believe they spend around 90 minutes a day staring at their phone, the app reveals that people pick up their device roughly 40 times throughout the day and spend, on average, a total of _three hours_ staring at it.

Guidelines for healthy use suggest spending no more than an hour a day on a phone, but 88 percent of Moment's users far exceeded this limit.

Video games are another majorly addictive technology.

_World of Warcraft_, a massively multiplayer online role-playing game, has created an especially immersive and interactive virtual world that allows people to create their own avatar and embark on quests where they encounter other online users in real time.

Millions of users log in to this world every day, and, according to video-game specialist Jeremy Reimer, up to 40 percent of them become addicted.

As a result, treatment centers are popping up worldwide — such as ReStart, a facility near Seattle that was created as a joint venture between a psychologist and a computer programmer.

In the blinks that follow, we'll take a closer look at the science behind addiction and what you can do to help keep yourself from getting hooked.

### 3. Certain situations and neurological stimulations can make you susceptible to addiction. 

For a long time, there was a popular misconception that addicts were immoral and weak-willed. But as we've grown to understand how addiction works, it's become clear that this isn't the case.

For instance, the situations we find ourselves in often contribute a great deal to the development of an addiction.

During the Vietnam War, for instance, many soldiers had access to an extremely pure form of heroin called "heroin number 4," which was introduced in Vietnam in 1971.

As a result, 19 percent of US soldiers were addicts by the end of the war, a situation that prompted Washington University psychiatrist Lee Robbins to take an interest in their plight.

Robbins knew that only 3 to 5 percent of heroin addicts are able to prevent a relapse after treatment, so it was surprising that 95 percent of the soldiers, after being treated by Robbins, remained clean. She believed that the reason for this was contextual; her patients were in the United States, removed from the stressful situations and conditions in Vietnam — and, of course, they didn't have such easy access to the drug.

This prompted many people to question previously established beliefs about addiction.

Addiction was now beginning to seem less like an affliction only suffered by the weak of will and more like something that could happen to anyone, given the right circumstances.

This new understanding was supported by evidence uncovered by psychologist James Olds in his experiments on rats in the early 1950s.

Having accidentally discovered the pleasure center in a rat's brain, Olds implanted an electrode and made it possible for his rats to stimulate this area by pressing a button. Under these circumstances, the rats simply pressed the button over and over again, ignoring food and water, until they eventually died.

When this experiment was later conducted with monkeys, the results were the same, leading researchers to conclude that all animals, including humans, can become addicted under the right conditions.

Many substances provide feedback that hits the brain's pleasure centers, and when this is combined with a situation from which we might want to escape — the horrors of war, for instance — you get the perfect recipe for addiction.

As you'll learn, technology, just like addictive substances, also has the power to stimulate our pleasure centers.

### 4. Behavioral addiction is similar to drug addiction and can cause serious health problems. 

You might have noticed that there are a number of support groups these days for people who are addicted to certain behaviors — things like sex or gambling or even obsessive cleaning.

Traditionally, we've thought that serious addictions involve substances such as drugs and alcohol, but, in recent years, it's been shown that many behaviors can affect our brain in the same ways that substances do.

According to neuroscientist Claire Gillian of Cambridge University, behaviors can stimulate the same areas of the brain that drugs like heroin and cocaine stimulate — and this includes many behaviors that take place online, such as playing video games, engaging in sex chats and gambling.

In all of these circumstances, the neurotransmitter dopamine is released in the brain, which in turn sets off a feeling of intense pleasure.

But this initial pleasure gradually decreases when the behavior is repeated. This causes people to make the addiction worse by spending more and more time online, in a futile attempt to recapture that first great dopamine high.

But here's the good news: since behavioral addictions are less intense than drug addictions, they're also easier to kick.

And considering the number of health problems that technology addictions can cause, there are plenty of reasons to break the cycle of addiction.

One of the biggest drawbacks of staring at a screen all day is sleeplessness.

Our inability to stray far from our screens means that many people will even take their devices to bed with them.

According to Arianna Huffington's book, _The Sleep Revolution_, 60 percent of US adults keep a device within reach of their bed.

Yet our bodies interpret the blue light from a glowing screen as a signal to stay awake. In chemical terms, exposure to blue light prevents our bodies from producing melatonin, the hormone that regulates our sleep and which is stimulated by things like darkness and candlelight.

By getting less sleep, we're increasing the risk of heart disease and depression. It also harms our immune system, making us susceptible to many other ailments.

> _"In the long run, our technology compulsions are directly damaging our health."_

### 5. Email can be addictive, unhealthy and hurt productivity, so disable your notification systems. 

Another trick our brain can play is to make us feel productive by responding to an email within a few seconds of receiving it.

Yet this kind of immediate reaction is just another addictive behavior and it actually reduces the quality of our work.

According to some studies, 70 percent of emails that are sent to office workers get read within six seconds. This might sound like fantastic efficiency, but it's the exact opposite.

To answer an email, you need to stop what you're doing, thereby breaking your concentration. And it's estimated that, on average, it takes us around 25 minutes to return to a place of deep concentration after checking email.

Since studies show that the average office worker checks her email 25 times per day, this means she'll spend the entire day without ever reaching full concentration.

In a _New York Times_ article, the Journalist Chuck Klosterman suggested that the act of answering an email provides a rewarding feeling; it's like achieving a small goal. This feeling can become addictive, however, and lead people to focus only on small, insignificant tasks.

So, to avoid this behavior, disable your email notifications and set infrequent times to check your email.

In 2012, a group of psychologists observed office workers after they were not allowed to check their email for several days at a time.

While people initially found it difficult to communicate, they soon began to use phones or simply walk to each other's offices to speak face-to-face. They also began stepping outside for their breaks more often, rather than continuing to sit in front of their screens.

Perhaps most surprising was the health and productivity results. Since they were able to stay focused for longer periods of time, more high-quality work was being done. Their heart rates even revealed lower levels of stress. And all this just because they were no longer being bombarded with constant email alerts.

### 6. Unpredictable rewards – like those offered by social media and gambling – increase the chances of addiction. 

If you've spent any time with babies, you've probably noticed how they love to repeat actions. They'll happily press the same button to switch on the same light or make the same noises over and over again. Babies do this because they enjoy the positive feedback — the look of that pretty light or the sensation of making that strange sound.

As adults, we also have a desire for positive feedback, and when we're rewarded for carrying out a simple action, we may start to develop an addiction. This is especially true if we don't know _when_ that simple action will be rewarded.

In the 1970s, psychologist Michael Zeiler conducted an experiment with pigeons. He set up a cage with a button that the pigeons could peck with their beaks to be rewarded with food.

But Zeiler made things interesting by changing the regularity of the reward. He found that if the pigeons got a reward every time the button was pressed, they would peck at regular but rather infrequent intervals; but if the reward came only 50 to 70 percent of the time, the birds would peck more frequently and persistently.

Because Zeiler made the reward more unpredictable, the rush of dopamine to the brain became bigger, which is exactly what makes gambling so appealing to people. When we roll the dice and come up with the winning number, that rush of pleasure is especially addictive.

This same addictive feedback system can be found in social media. The "like" button is a perfect example.

The like button dates back to 2008, when Facebook introduced it as a novelty to provide users with a quick and easy way to give friends feedback about their photos or messages. But this feedback is unreliable, so every time we post something we begin feverishly wondering whether it will be liked or not.

This makes every post on social media a gamble with pretty high stakes, since people can interpret getting no likes as a sign that their friends have abandoned them or that their post is subpar.

So it's no surprise that just about every social media platform out there, including sites like LinkedIn and YouTube, feature those addictive feedback buttons.

> _"Like pigeons, we're more driven to seek feedback when it isn't guaranteed."_

### 7. People have difficulty relaxing, which can lead to a dangerous habit: workaholism. 

Today, there are over 30,000 offerings on Google Books for those who want to improve their life. Considering the supply of self-help literature, it would seem that there's a great demand for this help, that people genuinely want to have a better life. But it isn't that simple.

In truth, humans are addicted to making things difficult for themselves.

Even if someone has managed to arrange a pretty cushy life for themselves, it's not uncommon for that person to continue being unhappy.

In 2014, psychologist Timothy D. Wilson and his associates developed an experiment that highlights the human need to seek hardships that we are then forced to overcome.

Undergraduate students were asked to sit calmly for 20 minutes and try to have a pleasant experience while avoiding negative thoughts.

The twist was that the students were also given a relatively painful electric shock before the clock started, and they were told that they could shock themselves during the experiment if they wanted to.

Oddly enough, two-thirds of the male students and one-third of the women decided to shock themselves at least once, while one man shocked himself nearly two hundred times!

For reasons we don't fully understand, it seems to be in our nature to seek out some kind of hardship or challenge to overcome, even when we're given the opportunity to enjoy life and relax.

In fact, we try so hard to prevent relaxation that our need for activity and constant achievement has its own name: workaholism.

This is especially apparent in societies that have a strong work culture, like Japan, where it's not uncommon for people to literally work themselves to death. Over the past twenty years, a new Japanese term has emerged: _karoshi_, which roughly translates to "death from too much work."

In 2011, an employee at the computer company Nanya died of heart failure as he was sitting at his desk. He'd put in one too many 19-hour workdays, and his system simply collapsed.

What makes karoshi death particularly strange is that these people are usually rich and successful. They don't need to work as hard as they do. But they're _addicted_ — in this case to the meaning and achievement that work can provide.

### 8. Children need to be protected from the dangers of technology addiction. 

When you were a child, you probably hated it when your parents put a limit on your television or computer time.

But you should be thanking them, since too much time in front of a screen can actually affect a child's empathy.

It's been proven that children who spend more time communicating with screens than with people can suffer some pretty negative side effects. And yet, each year, more and more children are glued to the screen.

Over the past 20 years, the amount of time children spend with electronic devices has risen by 20 percent.

In 2012, child psychologist Yalda T. Uhls wanted to determine just how much this was affecting children by taking a group of kids on a one-week technology-free nature retreat.

To help measure the impact that camping had on the children, they were given two empathy tests, one before and one afterward. This test, called DANVA2, uses facial expressions and tone of voice to gauge a person's feelings.

Remarkably, after just one week away from technology, the children scored 33 percent higher on the empathy test, further demonstrating the negative impact devices have on people skills.

It's up to adults to protect children from technology, since children are particularly susceptible to addiction and are unable to monitor their own behavior.

We do a commendable job of keeping kids away from tobacco and alcohol, but we should also be keeping them from spending hours on the computer or playing video games.

We don't know the full extent of how digital media will affect the lives of today's schoolchildren, but we've already seen some of the negative consequences.

It's important for children to learn that boredom, as well as physical and emotional challenges, are a fact of life that will require patience and hard work. The effortless entertainment of computers and mobile devices can undermine this important lesson. Those who never learn it may struggle with apathy later in life.

### 9. Substitution is a better method of curbing addiction than repression. 

If you've ever tried quitting something cold turkey — eating junk food, for instance, or smoking cigarettes — then there's a good chance that you've also experienced a relapse.

Our most common reaction to an undesired behavior is to repress or suppress it, but repression often only makes an addiction worse.

We can see this in the behavior of people living in the especially conservative and religious states of the United States, where sexuality and sexual urges are considered taboo. Despite the repressive attitudes, the computer usage data on Google Trends revealed that more people were searching for pornography in these states than anywhere else in the United States during 2015.

Relying on willpower alone to change your behavior is a risky proposition; substitution, on the other hand, is a much more reliable method.

Charles Duhigg, the author of _The Power of Habit_, breaks addiction down into three parts: a cue, a routine and a reward.

For mobile technology, the cue is taking out your smartphone; the routine is opening up your social media app and scrolling through your feed; the reward is feeling connected and seeing how many likes your last post got.

With this in mind, in 2014, the Company of Others launched a new device called Realism. Designed to help break smartphone addiction, Realism looks and feels like a smartphone but, instead of a screen, it has a transparent frame that offers a view of the world around you.

So the cue is still there — but when you feel the need to grab your device you can reach for Realism, which offers a new routine: looking at the real world around you instead of the virtual world inside your phone.

Like any other addiction, technology addictions can be overcome. There are creative and practical solutions to help break the cycle — but before you can move forward, you have to take the first step and acknowledge that the problem is there. So take a moment, turn off your devices and ask yourself: How irresistible has technology become in your life?

### 10. Final summary 

The key message in this book:

**Anyone can develop an addiction given the right circumstances, and this is just what today's environment has provided. By bringing mobile devices into nearly every aspect of our lives, we're surrounded by addictive triggers that can lead to stress, depression and insomnia. Thankfully, there are practical solutions to fight these problems and help you regain control of your life.**

Actionable advice:

**Avoid opening more than two tabs on your internet browser.**

Many people work with five to ten tabs running simultaneously on their internet browser. Jumping from one page to the next can create a false sense of constructive activity. This is an addictive feeling — but it's much more efficient to just stick to one thing at a time.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Hooked_** **by Nir Eyal**

_Hooked_ explains, through anecdotes and scientific studies, how and why we integrate certain products into our daily routines, and why such products are the Holy Grail for any consumer-oriented company. _Hooked_ gives concrete advice on how companies can make their products habit-forming, while simultaneously exploring the moral issues that entails.
---

### Adam Alter

Adam Alter, PhD, teaches psychology at New York University's Stern School of Business. His ideas and research on social psychology have made him a sought-after consultant for a number of businesses, including Google and Microsoft. He's written for such publications as the _New York Times_ and _WIRED_. His previous book is the bestseller _Drunk Tank Pink_.

