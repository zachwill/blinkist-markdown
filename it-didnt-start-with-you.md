---
id: 57ad907899b53c000385541c
slug: it-didnt-start-with-you-en
published_date: 2016-08-15T00:00:00.000+00:00
author: Mark Wolynn
title: It Didn't Start With You
subtitle: How Inherited Family Trauma Shapes Who We Are and How to End The Cycle
main_color: FA326C
text_color: BF2653
---

# It Didn't Start With You

_How Inherited Family Trauma Shapes Who We Are and How to End The Cycle_

**Mark Wolynn**

_It Didn't Start With You_ (2016) sheds light on a common thread in family relationships. These blinks explain how the source of your emotional or mental problems isn't necessarily _you_ but instead, your family history. You'll learn how trauma can be passed from generation to generation, and what you can do to break the cycle.

---
### 1. What’s in it for me? Overcome the long-standing traumas that have shaped your life. 

Were you ever uncomfortable when you visited grandparents or other relatives? Or did you have a fraught relationship with your parents? Chances are that hidden traumas in your family are to blame.

Traumatic events can affect the way we feel and behave, and when they run deep, sometimes trauma requires years of therapy to resolve. But if left unresolved, traumas, just like genes, can be passed on from generation to generation. This can have far worse consequences than just suffering a parent's sour looks over the family Christmas dinner table!

How do you overcome the crippling hold of family trauma to ensure that you don't pass on such feelings to your children? As these blinks will show you, language is the key to unlocking the past.

In these blinks, you'll also learn

  * how the pain suffered by your great-aunt could haunt you personally today;

  * why a few simple questions are all you need to get to the root of pain; and

  * how repeating positive statements can rewire your brain.

### 2. Severe trauma can lead to negative behavior and feelings, even when the trauma isn’t your own. 

Many people struggle with emotional problems. Some hold irrational fears, exhibit destructive habits or feel trapped in unhealthy relationships.

If you've ever faced problems such as these, you might think that you're to blame, that you're the source of your emotional turmoil.

But often such unhealthy behavior stems from trauma that you instead experienced, directly or indirectly, through your family.

Traumatic events, such as an early separation from a mother or an act of violence, can affect a person deeply. These events not only cause stress and fear but also can change a person's behavior.

Importantly, if a person isn't able to resolve the symptoms of such trauma, that person might instead suppress them and as a result, develop unhealthy behavioral patterns.

Worse, such people can even pass the effects of trauma to the next generation.

In short, you can carry symptoms of trauma in your life that _you_ didn't experience. You might have recurrent feelings or exhibit behaviors that aren't the result of any particular event in your own life.

One of the author's patients, for example, was overwhelmingly afraid of dying. She was severely claustrophobic and feared being unable to escape from a life-or-death situation.

She described the feeling as "I can't breathe, I can't get out; I'm going to die."

The patient wasn't reacting to trauma from her life but to the experience of her mother's relatives, who she later learned were murdered in a gas chamber during World War II.

Severe trauma like this can be so powerful that echoes of it can affect people who personally didn't experience it. But how exactly can this be the case?

### 3. Personal traumas can be passed on to successive generations via genes as well as through behavior. 

Even if you don't know much about your parents' or grandparents' past, the lives they lived and traumas they experienced can affect you, too.

In fact, past traumas can lead to behavioral patterns that can deeply affect the family dynamic.

We know that people often hide the effects of painful or traumatic events. However, it's important that we find a way to resolve the effects of trauma. If we don't, families can find themselves in a vicious cycle of inherited traumatic feelings.

The death of a child is a traumatic event for a couple, for example. Yet if the couple refuses to deal with the reality of the situation, suppressing their painful emotions, they'll likely project this trauma on to a second child.

Our biology too can be affected by traumatic events. Research has shown that thoughts and emotions can alter a person's genetic code, or DNA. This means that a person who suffers from trauma might pass "traumatized" genes on to children.

According to Stanford University cell biologist Bruce Lipton, emotions like fear or anger can "biochemically alter the genetic expression of…offspring."

Trauma also alters stress hormones, and parents can pass on these changes to children as well.

Rachel Yehuda, a researcher at Mount Sinai School of Medicine in New York, studied Holocaust survivors with post-traumatic stress disorder, or PTSD. She found that levels of the hormone cortisol were atypically low in the bodies of Holocaust survivors and war veterans.

In general, the body increases cortisol levels following a traumatic event in an attempt to "normalize" the body's systems. Yet people with PTSD often have chronically low levels of cortisol and, as a result, they can potentially pass this trait on to offspring.

All in all, trauma doesn't just impact one person, but potentially an entire family. So when it comes to identifying and overcoming trauma, we must examine the family, too.

### 4. Resolving parent-child relationships is key in breaking the cycle of trauma. 

Even if you had a favorite teacher, a close friend or perhaps someone in your life you felt was your hero, these individuals probably don't affect who you are and how you feel about yourself as much as your parents.

Simply, parents have the strongest influence on a child's life.

Why is this the case? Your relationship with your parents shapes who you are. Your parents bring you into this world, and the _life force_ they give you carries on afterward.

There are four ways a parent-child relationship can be disrupted, called the _Four Unconscious Themes_. These are an overly dependent child-parent relationship; the rejection of a parent; a break in a relationship with the mother; and trauma inherited from a family member.

If you suffer from trauma or emotional problems, you should examine your family history and the events of your early childhood. Doing so might help explain what's happening in your emotional life today.

There are questions to help you figure out if your experience fits one or more of the Four Unconscious Themes.

If you wonder whether you experienced an _interrupted bond_, for instance, look to the events of your mother's pregnancy; whether you might have been adopted; or if you were separated from your mother before you were three years old.

The _Core Language Approach_, a therapeutic process for solving psychological problems through language, can also help uncover suppressed trauma.

_Core Descriptors_ are a key component of the Core Language Approach. You can identify them with exercises, such as describing your mother or father to learn better how you feel about them.

If you say something like "My mother was abusive," you can move to the next step, which is writing down an event you blame her for.

Exercises such as these can help you pinpoint specific problems in your relationships, so you can start to address them and ultimately resolve them.

### 5. Discover underlying fears or trauma by finding the right language to shine light on their true nature. 

It's common to be afraid of spiders or snakes, but there are also many less common fears. And less common fears are often rooted in trauma from your past or your family's past.

We have to be particular with the language we use for describing hidden fears because we often don't have simple words for them, especially when the fear stems from early childhood trauma or inherited trauma.

Sigmund Freud wrote about _repetition compulsion_, or when a person unconsciously tries to bring her suppressed trauma to the surface through repetitive behavior.

Our memory or even an absence of memory can alter past experiences, so it's important to use Core Language to pinpoint the source of the trouble.

One of the author's patients felt that "The world isn't a safe place. You have to hide who you are. People can hurt you." Those thoughts, her Core Language, were rooted in her great aunt's experience in the Holocaust. Her inherited trauma was controlling her.

You can't overcome your fears until you know specifically what they are. So uncover your fears by first finding your _Core Complaint_.

Your Core Complaint is a phrase that describes your current fear or phobia, such as "I'm out of control right now and I'm afraid of what I might do."

Next, identify your _Core Sentence_, or the outcome that could result if your fear comes true. It might be "I'll hurt my child" or "My partner will abandon me," for instance.

Once you've found your Core Complaint and Core Sentence, you can start to work on the link between your fears and your family's history.

> _"Of all the core language tools you learn … the sentence that describes your worst fear, your Core Sentence, is the most direct path to uncovering unresolved family trauma."_

### 6. Your deepest fears can help point you toward the source of trauma in your family history. 

Fears are like signposts. They direct us back through events, guiding us to the source of our pain.

So how do we find these signposts and follow them down the path to better mental health?

You can uncover the root of a problem by paying attention to your symptoms. Use the Core Language Approach to ask questions to help you create your _Core Language Map_.

A Core Language Map is made up of four components: the Core Complaint, the Core Sentence, the Core Descriptors and the Core Trauma. Each component serves as a signpost, pointing to events in your family history. Those events tell us what we fear, and why we fear them.

Find your Core Trauma by examining your deepest fears and anxieties as well as your relationship with your parents. The grandchild of a Holocaust survivor, for instance, felt that she was going to "vaporize" and that her body would "incinerate in seconds."

Such language indicated that the individual was affected by a deeply traumatic event in her family's past. When she looked into her family history, she found that her Core Trauma was that her relatives were targeted for extermination as part of the Holocaust in World War II.

You can use the insights you gain from your Core Language Map in two ways. The first is through _Bridging Questions_, or questions that use your fear as a tool to discover patterns in your family history.

If a person is afraid of hurting a child, for example, a Bridging Question might be, "Did someone in your family blame themselves for hurting a child?" Or perhaps, "Did anyone in your family feel responsible for a child's death?"

The second is to create a family tree. Trace your family history back three or four generations, and place any traumatic events next to the family member who experienced them.

A traumatic event might be the early death of a loved one, exclusion from the family due to a dispute or a major sociopolitical event, such as a genocide.

### 7. Free yourself from inherited trauma by making peace with your past and your family’s past. 

A problem is a lot easier to solve once it's been identified. Identifying trauma is the first step toward overcoming it.

Creating new visual and verbal language that allows you to communicate with yourself is an important part of the healing process. When you recognize that a relative's trauma is holding you back, you can work on breaking the cycle, so the trauma stops with your generation.

To do so, you should use written exercises, visualizations, breathing exercises, your personal language and _healing sentences._ Healing sentences help you acknowledge your pain and the pain of people who suffered the original trauma.

Sentences are powerful. If you repeat a negative sentence like "I'm a failure" over and over, you'll start to believe it. Healing sentences instead work to instill positive feelings.

A healing sentence might be, "Instead of reliving what happened to _you_, I promise to live my life fully." Another might be, "These are not my feelings. I've inherited them from my family."

You can also try a healing action, such as lighting a candle in memory of someone in your family with whom you've become estranged. A ceremonial act could help you find a safe, emotional connection with that person, one step toward finding forgiveness or acceptance.

Healing sentences can also help you heal your relationship with your parents. This relationship plays a key role in your healing process, especially if your trauma stems from early childhood.

Using a healing sentence such as "I'll take your love as you give it, not as I expect it" is helpful, even if your parents are deceased. This sentence allows you to accept and forgive your parents' shortcomings.

Coming to terms with your struggle, communicating your struggle using the right language and accepting other people's struggles, too, are important parts of healing a relationship.

### 8. Final summary 

The key message in this book:

**Don't assume that you are the source of your trauma. Your emotional struggles might have been passed down from relatives, through genes or historical relationships. Use the Core Language Approach to identify your trauma, find its source and ultimately overcome it. Break the cycle! Defeat your trauma so you don't pass it on to the next generation.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Childhood Disrupted_** **by Donna Jackson Nakazawa**

What happens when a child who's experienced trauma grows up, and yet never outgrows that trauma? _Childhood Disrupted_ (2015) reveals the deep physiological and emotional consequences of the stress that shapes us both as children and as adults, and explains how we can recover from our childhood experiences and help our own children.
---

### Mark Wolynn

Mark Wolynn is the founder of the Family Constellation Institute, which specializes in the Core Language Approach in addressing family trauma.

