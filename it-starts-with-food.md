---
id: 56ebe0f770f3820007000001
slug: it-starts-with-food-en
published_date: 2016-03-22T00:00:00.000+00:00
author: Dallas Hartwig and Melissa Hartwig
title: It Starts With Food
subtitle: Discover the WHOLE30 and Change Your Life in Unexpected Ways
main_color: EE3D3B
text_color: BA302E
---

# It Starts With Food

_Discover the WHOLE30 and Change Your Life in Unexpected Ways_

**Dallas Hartwig and Melissa Hartwig**

_It Starts With Food_ (2012) gives you the inside scoop on the profound effects food can have on your body and well-being. Importantly, these blinks explain how you can alter your diet to both lose weight and feel better, body and soul.

---
### 1. What’s in it for me? Eat right to reset your body and become healthier in 30 days. 

While you might think that bag of salty chips after a long day is a treat you've earned, really you're just hurting yourself! 

Modern food producers have spent decades researching the science of human craving, designing snacks and other products to hijack your brain and make you want foods that are not just nutritionally poor but also physically dangerous. 

These blinks take you on a journey to show you what the food you eat does to your body, changing it in sometimes surprising and even scary ways. Importantly, you'll learn how to reset your body's natural balance and become healthier and happier by making better choices about what appears on your plate. 

In these blinks, you'll learn

  * how that afternoon bar of chocolate messes with your brain;

  * how to make life easier for your immune system; and

  * why you should always listen to your gut.

### 2. The food you eat has a big impact on your mental state and the health of your immune system. 

Have you noticed that certain foods make you feel weak or stressed out? Food can have a profound impact on how people think and feel. 

This is why diets don't work unless they fundamentally change your food cravings. If you find yourself returning to junk food after a phase of dieting, it's not necessarily because you lack willpower — it's because certain foods really mess with your brain!

Humans have evolved to appreciate three basic flavor categories: sweet, fatty and salty. Sweet foods give you energy; fatty foods give you calories; and finally, salty foods keep you hydrated. 

The fact is that humans are hardwired to like these flavors. Food producers know this and have used this fact to their market advantage. 

Processed foods such as the hamburgers and french fries from your favorite fast food outlet, chocolate bars and even frozen meals offer a _supernormal stimulus_, meaning these foods are either sweeter, saltier or fattier (or all three) than non-processed foods. This extra boost makes us crave such foods even more.

When you eat a chocolate bar, for instance, your brain releases _dopamine_, a neurotransmitter that gives you feelings of pleasure. The more chocolate you eat, the more your body craves the effect of dopamine; and this feedback loop makes you want to eat more chocolate in the future. 

Yet food doesn't just scramble your brain, convincing you that another cheeseburger can't be all that bad. It also affects your immune system, potentially causing _inflammation_. 

Inflammation in the body usually happens as a normal response to an injury or illness, but _chronic inflammation_ is different. Chronic inflammation occurs when your immune system gets confused and your body starts attacking itself, as it can't differentiate between "you" and a foreign invader, such as a virus. 

A lot of processed food today contains chemical compounds that can be difficult for your immune system to process. Such chemicals can stress your body, leading to chronic inflammation and even serious illness.

> _"They've turned real food into Frankenfood."_

### 3. What you eat can make your body’s hormones misfire, confusing your brain and damaging your body. 

Food can actually change how your hormones work. Your body's hormones are supposed to keep your system in balance, but certain foods can throw your body's system off balance. 

_Insulin_, which controls the body's blood sugar levels, is the most important hormone associated with food. Chronically high levels of blood sugar are harmful, and can cause diabetes. Insulin is supposed to prevent this by telling your body to store extra sugar as _glycogen_, to be used later when your body needs it.

If you eat too much sugar, however, your glycogen reserves fill up and your body stores the sugar instead as _triglycerides,_ or fat. When this happens, a hormone called _leptin_ is thrown off balance. 

Leptin tells your brain to stop eating when you're full. When you consume too much sugar, your brain can't properly read your leptin levels. You could develop _leptin resistance_ and overeat, because you won't feel full when you should. 

What's more, if your brain can't properly detect leptin levels, it may think you're too skinny, further pushing your body to produce more fat.

It is actually harmful for your cells to be stuffed with too much energy from sugars, so the cells protect themselves by becoming _insulin resistant_. Your brain responds to this event however by releasing _even more_ insulin. This excess causes you to feel dizzy (as if you haven't eaten), as your body thinks it needs more energy!

The food you eat also affects the health of your gut. Comprising some 80 percent of your immune system, the gut is where food is broken down into its component nutrients.

The gut protects the body by acting as a barrier, catching harmful substances you ingest before they make it "inside" to affect your immune system. Once bad stuff gets inside, however, it is much harder for your immune system to cope.

### 4. To maintain the body’s optimal health, try to avoid grains, legumes, seed oils and dairy products. 

Most people know it's unhealthy to eat too much sugar or drink too much alcohol. But other foods that may seem more benign can also be bad for your health.

Try to keep all grains and legumes out of your diet, even such "healthy" options as quinoa or whole-grain (or brown) rice. While grains are nutritious, they are not as nutritious as fruits and vegetables. They have a higher calorie content, too. When you eat grains, you may eat fewer fruits and vegetables because grains can fill you up so easily! 

Your body can't use all the nutrients in grains, either. Grains contain a lot of _phytate_, an acid that helps plants store phosphorous (to help them grow). But phytate can also bind with essential minerals such as _zinc_, which keeps your body from absorbing them — potentially leading to deficiencies. 

Legumes such as beans, peas, lentils and peanuts contain a lot of phytate as well. 

Even soy products such as tofu aren't great for human consumption. Soy contains _isoflavones_, which are a type of _phytoestrogen_. Your body treats phytoestrogens as it does female reproductive hormones, which in excess can be harmful. For men, it can lead to a testosterone imbalance; for women, it can speed up the growth of breast cancer cells. 

It's also good to avoid dairy products and seed oils. Milk doesn't just include useful calcium and calories, but also may include naturally occurring growth hormones — good for baby calves, but unhealthy for humans. 

Seed oils, such as sunflower oil or sesame oil, have high levels of _omega-6 fatty acids_, which can cause inflammation if you consume too much of them. Seed oils are present in processed foods, like tortillas. 

Most of the soy we consume actually comes from soy oil, which accounts for 10 percent of the total calories produced by the American food industry.

> _"We are not saying you can never eat any sugar or drink any alcohol ever again. We simply want you to make educated decisions about food."_

### 5. Eat plenty of meat, good fat, fruits and vegetables. 

So we've listed foods to avoid, but which foods then _should_ you eat?

A healthy diet should contain a lot of meat, seafood and eggs, as well as foods that contain the "right" kinds of fat. Such choices are protein-rich, and protein helps the body grow, recover from an injury or a strenuous workout, and in general is one of the more satiating nutrients.

Eggs are packed with essential acids and vitamins. Even the cholesterol in eggs can be good for you, if you maintain a well-balanced diet. 

Fat is also important because it gives you energy. It's important to remember that the energy from fat is much better for your body than the energy you get when you eat carbohydrates. 

So seek out _monounsaturated_ fats, found in olives, and _saturated_ fats, found in eggs and butter. Don't be misled by the undeserved bad rep that saturated fat has had in recent years. The myth that all saturated fat clogs your arteries is based on outdated evidence that has since been disproved. 

Avoid _trans fats_, or artificial fats found in processed foods, and "bad" saturated fat, found in refined carbohydrates, such as breakfast cereals. The saturated fat in refined carbohydrates can promote insulin resistance and inflammation — but this is not the case with saturated fats from meat. 

And of course, fruits and vegetables are very important. Vegetables are packed with nutrients and healthy carbohydrates, and also offer anti-inflammatory properties.

If you don't like vegetables, it's probably because you haven't had them prepared correctly, or you've simply become too accustomed to sugary or fatty processed foods. Bottom line: You're an adult now. Eat your vegetables!

While fruits are high in vitamins, they're not quite as nutritious as vegetables. Fruits also contain fructose, a sugar that can be harmful if you consume too much of it. Fructose metabolizes like alcohol, and can cause general inflammation or even liver damage. 

However, most people who consume too much fructose actually do so through eating foods with high-fructose corn syrup — not by eating fresh fruit. So don't skimp on the fruit in your diet!

### 6. Improve your health by changing the way you eat. Turn off the TV and pay attention to your plate! 

Most people eat without thinking much about how they are putting food in their bodies, whether in front of the television or on the run. If this is you, it's time for a change!

Eating right starts with _listening_ to your body. Your body tells you what you need. If you listen, you don't even need to count calories! Try the following tips.

First, eat your meals in a relaxed fashion, ideally while sitting at a table. Turn the television and your smartphone off, and chew slowly. When you gulp your food, your body's hormones don't have time to effectively process what's landing in your gut. A meal should take _at least_ 15 minutes to eat! 

Be sure to eat three meals a day. Don't snack! Space out your meals four to five hours apart; doing so helps keep your leptin levels in balance, allowing your body to adequately determine whether you're full or not. 

Don't drink caffeinated beverages after midday, either. Caffeine can suppress your appetite and keeps you awake. 

You may get cravings when you first try to adjust your diet, but do what you can to resist them. It'll get easier over time!

Here are some rules to follow for each meal.

Base each meal around a protein source, with one to two palm-sized servings of protein. This means if your protein source is eggs, you can hold more than one egg in your hand — so eat two eggs!

The rest of your meal should be made up of vegetables. It's okay to add spices and salt; you won't consume too much salt anyway if you avoid processed foods altogether. 

Eat fruits too, but they shouldn't replace your vegetables. Each meal should also contain one or more sources of fat, such as olives or avocados. In a well-balanced, healthy diet, these kinds of fats won't make you gain weight. 

Don't reduce the amount you eat, either! You won't lose weight simply by eating less. Healthy weight loss takes time. You'll lose weight as your body adjusts to your new eating routine.

### 7. Use the Whole30 program to make permanent changes to your diet and lifestyle. 

Baby steps don't work when you're trying to make a big change in your lifestyle. You need a radical change to break bad habits, or you'll just fall back on them.

That's why the _Whole30_ plan is so effective. The Whole30 plan aims to reset your metabolism in just 30 days and stop your unhealthy food cravings. 

The first phase is the _process of elimination_, during which you only eat healthy foods. Be strict. Don't just try to recreate your favorite unhealthy foods with healthier ingredients, as this only makes you dwell on what you can't really have. 

You can allow yourself _some_ processed foods, such as canned olives, but don't eat anything containing _monosodium glutamate_, _sulfites_ or _additive carrageenan_. Monosodium glutamate has deadly neurotoxins; sulfites can cause pulmonary problems; and additive carrageenan can cause inflammation. 

This phase isn't easy, and you might feel yourself going through withdrawal as you cut out excess salt and sugar from your diet. Stick with it! These tough cravings will soon pass.

After the first 30 days, the second phase of the Whole30 is _reintroduction_. It's in this phase where you can start, slowly, eating some unhealthy foods again. During this phase you can explore legumes, non-gluten grains and dairy products, while at the end reintroducing grains with gluten. 

After you reintroduce a certain food, go back to healthy food for a day and see how your body feels. And only reintroduce the foods you truly miss — remember, listening to what your body is telling you is crucial. If you reintroduce something that makes you feel bad, get rid of it and stick to your healthy choices. 

Remember: this change is permanent. If you feel yourself slipping back into old habits, restart yourself on the Whole30. It's a marathon, not a sprint! Even if you occasionally mess up, you'll never go back to square one once you've started. You will learn how to best tune into your body and its needs!

> _"Maybe ice cream really makes your stomach hurt, but you really love ice cream, so you decide it's worth it for you. That is entirely your call."_

### 8. Final Summary 

The key message in this book:

**The food you eat has a big effect on your mood, psychology, immune system and strength. So give your diet the attention it deserves! Avoid grains, dairy products and unhealthy fats, and make sure you get plenty of protein, vegetables and fruits. Kick start your metabolism and suppress your cravings with the Whole30 plan, but don't lose hope if you slip up now and then. The Whole30 plan makes you healthier and stronger for life!**

Actionable advice:

**Avoid smoothies.**

Fruit is important in a healthy diet, but don't make your main fruit consumption come from smoothies. Liquid calories just don't fill you up as much as solid calories can. So it's best to eat fresh fruit in small servings throughout the day. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _The Fast Diet_** **by Michael Mosley and Mimi Spencer**

_The Fast Diet_ (2013) is a guide to intermittent fasting and its health benefits. The 2015 edition includes a revised and expanded theory behind intermittent fasting, along with advice for physical exercise.
---

### Dallas Hartwig and Melissa Hartwig

Melissa Hartwig is a sports nutritionist who has been featured in several publications, including _The_ _Wall Street Journal_. Dallas Hartwig is a physical therapist and sports nutritionist. _It Starts With Food_ is their second bestselling book. **  

**

