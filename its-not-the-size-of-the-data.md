---
id: 5799ebd9e0bc1b000321bcce
slug: its-not-the-size-of-the-data-en
published_date: 2016-08-04T00:00:00.000+00:00
author: Koen Pauwels
title: It's Not the Size of the Data
subtitle: It's How You Use It
main_color: 54ABC0
text_color: 326673
---

# It's Not the Size of the Data

_It's How You Use It_

**Koen Pauwels**

_It's Not the Size of the Data_ (2014) is a beginner's guide to designing, creating and adopting your own marketing dashboard, helping you uncover the links between campaigns and performance, and monitor progress with long-term goals in mind.

---
### 1. What’s in it for me? Learn how to use big data to make better marketing decisions. 

Marketing has traditionally been seen as a "soft science," something that's best performed using one's gut rather than one's head. But in many cases, this intuition-based marketing has lacked direction, or has failed to resonate with the wider goals and desires of companies as a whole. As a result, many marketing departments have become outsiders within their own companies, separated from other departments entirely.

But with new and readily available technologies, all this can change.

These blinks show you how to build a marketing dashboard, enabling you to collect and utilize data that will offer actionable insights on how to make better marketing decisions.

You'll also learn

  * how a marketing dashboard differs from a regular dashboard;

  * how to design one; and

  * how you can motivate your employees to use it.

### 2. Use a dashboard to link marketing to performance. 

Imagine you're driving on the highway. A quick glimpse at your car's dashboard tells you that you're driving too fast, and that you're almost out of fuel. Without having to consider all the complex processes going on underneath the hood of your car, you're able to make the right decision: slow down and refuel.

To do the same for their business, marketers also need their own dashboard. A _marketing dashboard_ helps link marketing to performance by making data understandable and actionable.

Any company with huge amounts of data about their sales, for instance, would only be able to glean insights from those raw numbers by spending quite a long time examining them. This is rather like taking a stalled car and staring at the parts under the hood, hoping that the problem jumps out at you.

In much the same way that a car dashboard shows a driver the performance indicators she needs to know to get from point A to point B safely and efficiently, with a marketing dashboard, raw information can be summarized clearly using visuals that help you comprehend and make use of the patterns your data reveals.

An informative and visually pleasing dashboard will also help you use your data to keep your marketing decisions accountable and within budget. Studies have shown a long-standing disconnect between marketing operations and a company's executive agenda; this is likely tied to the way we tend to make marketing decisions instinctively, sticking to what feels right without questioning why.

This lack of reflection often leads to lavish spending, as controlling costs isn't usually a top priority for marketing departments. It's here that your marketing dashboard can really make a difference, connecting marketing spending to a company's financial and strategic plan, helping you monitor campaigns and stay on top of expenses.

If you're starting to think that a marketing dashboard is rather distinct from a car's dashboard in its potential and capabilities, well, you're right! Find out why in the next blink.

### 3. Marketing dashboards allow you to monitor performance in the long term. 

Dashboards have long been a part of interfaces on our tech devices and services; just think of your Macbook or Tumblr homepage! A dashboard is there to give you a clear overview of what's going on. In business, your dashboard includes key stats and performance indicators, and the same goes for marketing dashboards.

However, there are two key factors that set marketing dashboards apart.

First off, a marketing dashboard consists of _connected data_. While your typical dashboard will report on individual metrics to help you assess a specific performance, such as website page views, a marketing dashboard takes things to the next level. How? By showing how metrics are _linked to each other_. This helps you tease out which factors influence each other, and performance overall.

Consider the two metrics of online advertising and sales; a marketing dashboard will show you how, for instance, tripling the online advertising budget will cause sales to rise around 13 percent.

Second, a marketing dashboard is built to help you track data in the long term; conventional dashboards, in contrast, are designed to help you monitor day-to-day operations in order to support your data efforts in the short term.

Take a dashboard used by an e-commerce startup, for instance; it will display the web traffic for a client's online store. If there's zero traffic, a team member can quickly check whether the client's site is down, work out where the issue lies and solve the problem.

However, the marketing dashboard isn't an instant monitoring tool. Instead, it connects performance with targets set by the marketing team or the business as a whole. By looking at long-term trends, marketers can track how sales numbers respond to different marketing campaigns or PR efforts.

If you're liking the idea of the dashboard so far, you're probably curious about how you can make one of your own. In the next blink, you'll learn how you can create and customize a dashboard to match the metrics you're most interested in.

### 4. Design your marketing dashboard to suit your employees and stakeholders. 

When you start designing your marketing dashboard, keep in mind who you're designing it for: your employees!

Get talking to your team members and find out what information they'd like to know to do their jobs better. These are needs that you can translate into _Key Performance Indicators (KPIs)_ — that is, the metrics that'll help you measure how effectively your business is progressing toward its goals.

The next step is to consider your KPIs in light of their role in employee decision making. Which KPIs do certain team members need to make a particular decision? Take market share, for example. If this KPI falls, employees know they need to take action to improve the situation. They might decide to start a new marketing campaign with the goal of winning back customers from rival companies.

Finally, it's time to get all your stakeholders involved, too. This means anyone involved in your business, from suppliers to customers. The goal of this is to find as many relevant KPIs as possible and ensure they're all covered on your dashboard. You don't want any blind spots!

Say your brand's performance is easily influenced by policy makers in government or regulatory agencies. You'd need to develop KPIs to track your reputation on key issues, like corporate social responsibility and transparent hiring practices. Keeping an eye on these classic blind spots is also a great way to prevent PR disasters down the line!

Now that you know which KPIs matter to your team, it's time to experiment with how you display them. Next, we'll look at why visuals are your friend when it comes to building dashboards.

### 5. Use colors, timestamps and symbols to make your dashboard legible at a glance. 

Imagine if your car dashboard only featured numbers illuminated in red. It'd be pretty hard to discern your fuel status from your indicators while driving! Car dashboards are designed to be easy to read at a glance, and the same should be true for great marketing dashboards. So how can you visualize KPIs with clarity?

For starters, colors are key. By allocating each KPI category its own distinctive color, you can seek out and separate different kinds of information much faster. It helps to use colors in a logical way, too. Dark green might, for instance, be used to display your monetary values; dark red might also be assigned to monetary values, but only those that are negative.

Another vital visual element for your dashboard is the _timestamp_. After all, your dashboard's purpose is to help you monitor changes over a specific period of time. So, make sure that it's easy to find the dates and times at which information was updated!

Finally, make use of symbols to communicate contextual information in an intuitive way. Warning signs or exclamation points are universal indicators for negative or close-to-negative values, for example. It's best to avoid complicated sets of strange symbols — it'll only irritate employees! Keep symbols clear and simple, and they'll be far more useful for everyone.

### 6. Motivate your employees by communicating and leveraging the benefits of a dashboard. 

We all know how tough it can be to get teams to adopt a newly introduced tool. Unfortunately, marketing dashboards, for all their benefits, aren't immune to this. If you want your employees to really make use of them, you've got to motivate them!

With this in mind, the way you introduce them to the dashboard is crucial. Explain to your team why the dashboard is a necessary part of their work, and make clear that the dashboard will help them both monitor the performance of their choices _and_ help them improve their choices later on.

You can illustrate this in terms of an example KPI, such as increasing user friendliness by ten percent. Show your team how their dashboard can help them track the extent to which a particular marketing campaign contributed to increasing user friendliness, and track a corresponding uptick in sales.

Dashboards help employees learn that their decisions really do matter, and result in direct, visible impacts on sales. This, in turn, motivates them to improve their performance much more than simply reminding them that they should be more productive.

You can leverage this further by using dashboard results to create incentive programs. With the clear metrics provided by the dashboard, managers can easily see when goals are exceeded, work out whose excellent performance was responsible and give out bonuses accordingly.

### 7. Final summary 

The key message in this book:

**Marketing dashboards help you navigate the tricky waters of competitive business by making raw data readable, connecting marketing with performance, keeping campaigns accountable and meeting the decision-making needs of your team members.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Marketing Above the Noise_** **by Linda J. Popky**

_Marketing Above The Noise_ (2015) guides you through today's marketing world, helping you separate useful advice from useless noise. Advising against jumping on the bandwagon and following all those hot new marketing trends, these blinks demonstrate that tried and true approaches to marketing are the best way to win over — and hold onto — customers.
---

### Koen Pauwels

Koen Pauwels is an educator, consultant and specialist in marketing performance. He currently holds a professorship at Ozyegin University in Turkey and is the recipient of multiple awards, including the 2010 Google WPP Research Award.

