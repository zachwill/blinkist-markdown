---
id: 59256927b238e10007b6afd4
slug: its-not-you-en
published_date: 2017-05-25T00:00:00.000+00:00
author: Sara Eckel
title: It's Not You
subtitle: 27 (Wrong) Reasons You're Single
main_color: 5E95C4
text_color: 466E91
---

# It's Not You

_27 (Wrong) Reasons You're Single_

**Sara Eckel**

_It's Not You_ (2014) is an antidote to the stereotypical dating guides that offer advice that isn't relevant to today's modern lifestyles. Here you'll discover why you no longer need to panic about being single after you turn 30 and why marriage needn't be regarded as key to your happiness. This is a guide to help today's single women navigate a dating world that is quite different than it was just a generation ago.

---
### 1. What’s in it for me? Appreciate the single life. 

How many times have you pitied yourself? How terrible does it feel to have been alone for such a long time? How many awful dates have you gone on, just so you could feel a little bit of tenderness?

As a woman in your thirties or older, single life is laden with hardship. It's an ongoing struggle — against the expectations of society, against your own self-conception — and you're under constant pressure to find happiness in a world where happiness means finding a partner, getting married and having children.

Well, it doesn't have to be this way. These blinks teach you to appreciate your single life again. You'll learn what it takes to be happy as a single woman as well as how to escape the pressures of society. There are some simple techniques that will help you oppose the prejudices of married women and allow you to approach dating with ease.

You'll also learn

  * why you shouldn't erase feelings of sadness;

  * what Fox News thinks about single women; and

  * what you should do about your dissipated lifestyle.

### 2. It’s okay to be yourself and to feel lonely from time to time. 

If you're unhappily single, you may have heard that, before you can find a partner, you need to fix things about yourself. This, in fact, is the advice of many dating experts.

But this advice puts the blame for your singledom on you and your needy and off-putting personality. It also leads people to believe that they need to resolve their own personal and self-esteem issues before they can even hope to start a relationship, which simply isn't true.

Psychologist John Gottman, who has been researching newlywed couples, is determined that there isn't some ideal personality that is required for a successful relationship. Rather, the challenge is to find a person who accepts you for the person you are, neuroses and all.

So don't beat yourself up thinking that you need to fix your relationship with your parents or overcome your low self-esteem in order to be someone's loving partner or spouse. According to psychology professor Kristin Neff, people with low self-esteem are generally just as lovable as those with high self-esteem.

That said, it's absolutely normal for feelings of loneliness to be accompanied by feelings of shame and a desire to fix yourself.

These feelings are wired into our brains because, for our ancestors, living alone was a deadly proposition; long ago, we _needed_ company to help protect us from dangerous predators. Neuroscientist John Cacioppo tells us that the shame we feel about loneliness is still part of our biological wiring, and it can kick in even when we're living safely in a studio apartment.

So, if you want to ease these feelings, the best thing to do is simply accept them as part of the human experience.

> _"If everyone had to "get right with themselves" before finding a partner, the population would have died off long ago."_

### 3. Sadness is an important part of being alive, and online dating has its advantages. 

What is your idea of a happy life? Does it include marriage and kids as well as a satisfying job? Research does show that individuals who are married tend to be happier than single people. However, it's important to keep in mind that 40 percent of marriages end in divorce, and that there are still plenty of women who are happily single.

It's also important not to ignore or suppress your feelings of unhappiness if they arise.

Of course, this is exactly what we may _want_ to do with our sadness. Most people are eager to do whatever it takes to erase sadness altogether. But unhappiness is an essential part of being alive — just as essential as happiness, even though a higher value tends to be placed on happiness.

Try to approach sadness as a Buddhist would: know that happiness and sadness are two parts of a full life, and that life can't be experienced to its fullest without accepting both of these emotions.

So, if you're single and feeling sad, don't blame your lack of a partner; rather, accept the feeling as part of being alive.

Now, with this frame of mind, you may look to online dating as a great opportunity to find a partner.

Many people have turned to online dating, since it has the advantage of allowing people to conduct sincere conversations that might otherwise never take place.

Generally, on a first date, people will go on about how happy they are and come across as having an optimistic personality, even if it's far from the truth. When people are able to get to know one another through a conversation over the internet, however, they're more likely to feel safe enough to speak truthfully about topics that first dates often avoid.

Thus, dating sites are often ideal for getting to know someone.

A study conducted by the University of Chicago even supports this theory. The study found that the marriages of couples who initially dated online are substantially more likely to last.

### 4. Successful and confident women can intimidate men, but they have great chances of getting married. 

If you're a single woman who has struggled to find a partner, here's a question: Are you a confident woman who has full control of your fast-paced professional life? If so, this is a good thing, right?

Well, if you are a self-confident woman, know that this confidence may intimidate men who feel that you might not have the need, or the time, for a meaningful relationship with them.

Confidence is a great thing to have, but if you hope to appear more approachable, it's wise to slow down and give yourself the time and space to experience stronger emotional connections.

When you wear your independence and self-sufficiency on your sleeve, men may also find it difficult to see what kind of purpose they could have in your life. It's always good to make sure they're aware of how their skills are appreciated, whether it be cooking or helping you out with a work dilemma.

In truth, successful and educated women have a better chance of getting married — but this wasn't always the case. In the past, women without career ambitions were thought to make better spouses.

That seems to have changed. In 2006, sociologist Christine Whelan found that educated women earning $100,000 or more per year were more likely to marry than women with lower incomes.

Furthermore, according to a 2011 study by Harvard economist Dana Rotz, women who postpone getting married until their late thirties are 46 percent less likely to have that marriage end in divorce.

This is pretty solid proof that the times have changed and women no longer have to decide between love and a career. And don't panic if you haven't found the right person yet. A stabler relationship may be right around the corner.

### 5. Misguided relationship advice includes opinions about desperate and over-affectionate women. 

According to dating experts, many women end up single because they're not confident enough — because they're _needy_. Such women are criticized for seeming too desperate to get married. This desperation, assert the experts, is off-putting and a little sad.

But this label of "desperate" is often utterly inaccurate and is often applied to women who are far from desperate.

To say a woman is "being desperate" used to imply behavior such as marrying an older man, even though you might find him repulsive.

Despite the fact that this kind of behavior has nothing to do with the desire to be in a close and caring relationship, dating experts still have misguided ideas about how to avoid appearing desperate.

The most common suggestion is this: act like someone you're not and hide your affection and how much you really care about the other person. If you don't, it'll seem as though you're trying too hard to please them. Such affected indifference might mean not cooking for them, even though you want to, or generally trying to come across as less interested than you really are.

This is awful advice. Being strong enough to expose your true feelings and reveal your love, without being sure that the other person feels the same way, is exactly the kind of behavior that can lead to a devoted relationship.

Professor Brené Brown has studied the difference between those with healthy self-esteem and those without it, and she found that the healthy ones embrace their vulnerability as a part of what makes them beautiful.

### 6. Sometimes over sharing with friends can increase your chances of bad dates. 

If you've been on a number of unsuccessful dates, you may be familiar with a popular post-date activity: getting together with your friends to recount how badly it went.

This activity may seem innocent enough, but these friendly chats could end up hurting your chances of forging a successful relationship.

Let's say you're used to complaining about how uninteresting the guys you date are. And then you go on a great date and you really like the guy — but he doesn't call you back. It might be hard, in light of the narrative your friends are familiar with, to admit this disappointment to them, so, instead of the truth, you come up with a different story and say that you didn't call him back because of your own emotional baggage. Maybe you even have a recurring excuse about how it all goes back to you having a bad relationship with your dad.

Whatever the excuse, it might be obscuring the real reason that a date didn't work out.

Perhaps you were at dinner with a gentleman who was friendly, attentive and charming, but you just never connected. Later, you tell your friends that he kept tapping his fork against the table, which ruined the evening, which makes you look rather petty and over-concerned about insignificant things.

In situations like this, it can simply be best to keep your experiences to yourself and not feel like you have to share a story or justify why a date didn't turn out perfectly.

This way, you can learn to rely on your own judgment, not what your friends think may or may not be true. This is an important step toward self-reliance, which can be liberating and empowering.

Friends _can_ be a good source of support, but they don't need to know or comment on every detail of your life.

### 7. Don’t feel forced into relationships or going out every night in order to find Mr. Right. 

Here's another common yet misguided piece of advice: Before you can have a happy marriage, you must have enough prior experience with long-term relationships.

There's never been any proof to suggest that experience in long-term relationships is required in order to enjoy a happy marriage. Rather, the opposite would seem to be the case, since there are plenty of people who found their first love in their late thirties, forties or fifties and went on to have a happy marriage.

Yet, despite the statistics, women over thirty who have yet to be in a long-term relationship tend to think they have to hurry up, meet more people and get noticed, even if they'd rather not.

Sure, it's healthy to go out and meet new people, but the goal should be to enjoy yourself, not to keep your eyes peeled for Mr. Right. By constantly thinking about finding a partner, you may be setting yourself up for repeated disappointment — and that's exactly the opposite of what you should be doing.

The real reason behind these concerns is the unfortunate belief that marriage is a goal and that, once reached, it will allow you to finally live happily ever after.

Often we regard a spouse, and a ring on our finger, as a reward, as proof that our efforts have paid off. We also tend to believe that, once we're married, we'll be with that person forever, no matter what.

But the truth of the matter is that our lives are constantly changing and nothing lasts forever, including our marital status.

### 8. Avoid getting trapped by negative emotions and wrongheaded opinions about being single. 

One of the toughest parts of being alone is being stuck with your own thoughts, and asking yourself questions like, "Why am I still single?" Or, "Why did he abandon me?" It can be easy to get stuck under a cloud of negativity.

Unfortunately, many single women become trapped in what psychologist Mihaly Csikszentmihalyi calls _psychic entropy_, which is when the mind can only focus on negative things.

During these negative periods, we can only bring up bad memories and we forget any positive ones, which may be due to the way our brains were wired back in cave-dwelling times. Back then, it was more important for our survival to remember threats, and less important to remember the good times.

It's easy to find skeletons when you're constantly sitting in this dismal closet. Perhaps you'll come to the conclusion that you shouldn't have broken up with your ex, and that this break up gave you bad karma, and _that's_ why you're alone.

To make matters worse, these negative thoughts can be compounded by the negative opinions of others.

It's not uncommon for married couples to regard older single people as children who refuse to grow up. These couples tend to believe that unmarried people would rather spend their money on going out and traveling, among other frivolous things that are supposedly less important than buying a house and settling down.

This has even found its way into political discussion: Fox News commentators have suggested that single women vote for Democrats rather than Republicans because, unlike married women, they don't care about the future of their country or the safety of their children.

This is unfair, of course. And it also leaves out some positive statistics. For instance, single people are more likely to visit their parents and help their friends.

### 9. Love is all around, as long as you keep your eyes and heart open. 

When you think about love, you probably think about romance. But romantic love isn't the only form of love or the only way to find happiness.

Love can be found in every moment of your day-to-day life, not only in romantic relationships.

When we have a significant, warm experience with a friend, family member, or even with a stranger, this is also love. All it takes is making a human connection with another person — and there is love.

An effective way to help yourself fully experience this love is to perform a loving-kindness meditation.

This involves thinking of someone dear and wishing them happiness by saying a mantra. One popular mantra goes like this: "_May you be happy. May you be free from suffering."_

You then spend some time repeating this mantra to yourself while thinking about that person. Once you've done this, repeat it again with someone else in mind, someone you're not as close to. This will help you expand your circle of love and appreciate the people who are meaningful to you. It will also make you a calmer and more compassionate person.

We're often surrounded by love yet unable to recognize happiness. This happens because we're generally misguided. We're encouraged to look at other people and aspire to their way of life in order to find happiness, which is exactly how the myths surrounding marriage emerged.

This happens with romantic love, too: we're told that this is the best way to be happy. So we feel that, without it, we're somehow incomplete. But it doesn't have to be this way.

Being single might make it harder for you to cuddle up to someone on a cold Friday night, but that doesn't mean you're alone. Cherish the moments you spend with friends and loved ones — and, of course, don't forget to appreciate the privacy and solitude you get on your own.

> _"I was so specific about the type of happiness I wanted that I far too often ruined a good thing."_

### 10. Final summary 

The key message in this book:

**Dating advice has remained stuck in the past while the modern female has become more independent and less focused on marriage and kids. More women have found that singledom can provide an opportunity to get to know themselves better and experience other forms of love. This doesn't mean loneliness has vanished, but when we move away from old-fashioned dating advice, we can learn to love ourselves more and not rely on others for our happiness.**

Actionable advice:

**Talk to yourself as you would to a cherished friend.**

Self-compassion is an immensely valuable trait. By treating yourself kindly, you'll greatly improve your relationship with yourself and, thus, with others, including a potential partner. How can you be self-compassionate? Well, the next time you feel sad, lonely or left out, simply imagine that your best friend is feeling as you do and comfort yourself as you would this friend.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _All The Single Ladies_** **by Rebecca Traister**

_All The Single Ladies_ (2016) chronicles the history and changing circumstances of single women in America. These blinks explain how cultivating female friendships and seeking alternative strategies to balancing personal and professional obligations help women achieve greater success and independence.
---

### Sara Eckel

Sara Eckel is a freelance writer whose essays and criticism have appeared in a number of publications, including the _Washington Post_ and _Glamour_. _It's Not You_, based on her popular Modern Love column for the _New York Times_, is her first book.

