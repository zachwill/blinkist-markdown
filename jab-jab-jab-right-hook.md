---
id: 547df29a61393200081b0000
slug: jab-jab-jab-right-hook-en
published_date: 2014-12-05T00:00:00.000+00:00
author: Gary Vaynerchuk
title: Jab, Jab, Jab, Right Hook
subtitle: How to Tell Your Story in a Noisy Social World
main_color: DF3156
text_color: C42B4C
---

# Jab, Jab, Jab, Right Hook

_How to Tell Your Story in a Noisy Social World_

**Gary Vaynerchuk**

_Jab, Jab, Jab, Right Hook_ explains how managers, marketers and small businesses can capitalize on social media platforms like Facebook to increase their public profile. A great social media marketing campaign can deliver that fatal blow — the "right hook" that knocks consumers into buying their product. The author teaches you social media moves that'll have your product floating like a butterfly and stinging like a bee.

---
### 1. What‘s in it for me: Finally achieve social media domination. 

Nowadays people scramble all over each other to scream as loudly as possible on social media. They must think it's the only way to get heard and spread the word about their product. They're wrong.

While social media can be a chaotic place with billions of voices and brands all vying for each others' attention, there's an easy way to cut through the noise: You have to know what your story is, and how to present it on each social network in the best way.

Just a few tricks and tips will help your story reach more people than you ever thought possible. After reading these blinks, you'll have learned

  * how to market products on social media without annoying people;

  * which social media network is the most underused, despite its huge potential; and

  * why _where_ you put the story is as important as _the story itself_.

### 2. Social media is essential for successful product marketing. 

Take a look around you. How many people do you see fiddling with their phones?

The rapid spread of cell phones has completely changed the way we consume media. In the United States alone, there are nearly _325 million_ mobile phone subscriptions. To put that into perspective, the total population in 2013 was 316 million!

It's safe to assume that nearly everyone has a phone now, and nearly all those users are on _social media_ — the websites or applications that allow people to connect with each other and network.

Market research by eMarketer has found that people spend nearly half their phone time on social media. In fact, _71 percent_ of Americans are on Facebook, and there are more than half a billion Twitter users worldwide.

Social media has changed our consumption habits. It directly influences the way we spend our money: One in four people use social media sites to help them decide what they want to buy. They find out about a product through a viral Twitter campaign, or their friend's Facebook post.

And social media isn't just for the young — Baby Boomers recently increased their social media consumption by 42 percent in just one year. This fact is especially important to advertisers, because Baby Boomers account for 70 percent of US spending.

It's normal for new media outlets to overtake old ones — once upon a time radio overtook print, and TV replaced radio in turn. But social media's takeover has been significantly faster. It took radio _38 years_ to reach 50 million people, and TV 13. Instagram? Only a year and half.

Clearly, marketers need to understand how to utilize these powerful new tools. Social media is simply vital in our modern age.

> _"Social media is like crack — immediately gratifying and hugely addictive."_

### 3. A great marketing campaign relies on its content. 

So what do these social changes mean for marketers? Well, your marketing needs great content, and you've got to tell people enticing stories.

The only way you can stand out in the noisy world of social media is through outstanding content. We no longer see individual ad campaigns that launch for six months or so, as used to be the case for TV. Nowadays, strong marketing means year-round campaigns that provide fresh content daily.

These new campaigns engross the customers in a story that provides brand exposure, and ideally leads to the climax: a sale.

The Nike+ campaign is a great example. Nike+ is an app that lets people compare their exercise accomplishments, and even compete with each other. This means the users themselves actually create daily content and promote the company.

Naturally, different companies will approach this task differently, but outstanding content does adhere to a few rules.

Firstly, the content shouldn't be annoying or intrusive. It should blend seamlessly into the consumer's life. Anyone can avoid ads though DVRs, Adblock, mute buttons or simply fast scrolling. Marketing content has to be genuinely entertaining to catch the eye.

It also can't be too demanding of the audience. Your advertisements are for the consumers, not you. Informative and interactive ads usually work better, as consumers enjoy them more.

Finally, remember that even great content is meaningless in the wrong context. Effective content has to be _native_.

That means it should fit the format, demographics and general trends of the platform it's on. Don't waste your content by choosing the wrong platform.

For example, an unclear or low-quality photo on Pinterest might flop, but be a hit on Twitter if it's posted with a witty caption. Marketers should understand how different social networks function, so they can use them as effectively as possible.

### 4. If you want to maximize exposure on Facebook, your content has to be outstanding. 

What role does Facebook play in all this?

Facebook is really the dream space for interacting with consumers. It's a constant hive of activity — in 2012, there were over a billion active users each month. Shockingly, Facebook accounts for _one-fifth_ of total webpage views in the United States.

Advertising on Facebook is relatively inexpensive. Ad campaigns on television can cost between $7,000 and $13,000, and the companies who pay for them can't even be sure how many people watch them. Facebook ads, on the other hand, cost between $0.50 and $1.50 per "like." They're also interactive, as consumers can share them with each other.

Advertising on Facebook does pose certain challenges, however. After all, it's designed to give the consumers the best experience — not the advertisers.

Just consider how the newsfeed works. It's been designed to filter out certain content, so it doesn't get clogged up with unwanted information (from advertisers _or_ people).

Facebook developed _EdgeRank_ to address this problem. Every time a user does something on Facebook, it's recorded as a piece of information called an "edge." EdgeRank keeps track of these, and uses them to determine what should go into the newsfeed. So if you regularly like a friend's photos, but not their status, you'll see more of their photos in your newsfeed.

This means marketers have to create content that can generate more interaction — or edges. Moreover, EdgeRank doesn't factor in purchased likes, or click-throughs to other sites. So even if a link to your page could generate millions of sales, it'll stay out of the newsfeed unless it's liked, shared or commented on.

In short, your content has to be great for Facebook advertising to be worth it.

> _"Facebook reported 41 percent of its ad revenue came from mobile, equaling $1.6 billions in the second quarter of 2013."_

### 5. Twitter can provide great brand exposure if used #properly. 

What about tweets? They're everywhere, but can they bring in real revenue?

Well, Twitter provides great opportunities for businesses, but it's often inefficiently used. It can be another dream for marketers, as it allows them to connect directly with consumers.

In 2012, Twitter had more than 100 million users in the United States, and 500 million worldwide. Unlike Facebook pages, Twitter profiles are almost entirely public, which makes it easier for strangers to connect.

However, it can be hard to reach prospective customers through Twitter, as their feeds are constantly inundated with tweets, many of which are spontaneous conversations. There is an enormous amount of information on Twitter — approximately 750 tweets per second. How can marketers make their content stand out?

One good strategy here is _trendjacking,_ which means latching on to a popular issue people are discussing. The most discussed topics on Twitter are called _trending topics_. A mathematical algorithm identifies them and lists them on the site.

Trendjacking is underused. One example of a missed opportunity for trendjacking was when the TV series _30 Rock_ aired it's final episode in January 2013 after seven years as one of America's favorite shows.

The day after the final episode, _30 Rock_ was one of the top-ten trending topics in the United States. The jeans company 7 For All Mankind, nicknamed "Sevens," could've jumped on the "seven" trend, but they missed the opportunity and didn't draw any _30 Rock_ fans to their brand.

_Hashtags,_ which mark keywords and categorize tweets, also provide good marketing opportunities. However, #they're #often #overused or #misused.

It's very important that your hashtags hit the right tone — use them indiscriminately or without any sense of humor or irony, and they'll backfire, making you look out of touch.

### 6. Businesses still haven’t caught on to the marketing power of Pinterest. 

Any marketers targeting a female demographic would be crazy not to use Pinterest.

It's the perfect social media platform for sales, especially for women. Pinterest provides virtual "pin boards" that people can "pin" images to. So companies can create virtual store fronts where each image links directly to their home site.

Brands can also diversify by having several pin boards, with some only tangentially related to the brand. For example, a tea company could draw in travelers by hosting a pin board for teacups spotted in strange places in the world!

Pinterest is massively underused by marketers. A 2012 survey found that Pinterest users were _79 percent_ more likely to buy a product on Pinterest than Facebook. It's a great platform, and it caters to a rapidly growing market. By 2012 the number of users had grown by 379 percent, and by 2013 it had _48.7 million users_.

It also has a large gender bias toward women. Females on the site outnumber males five to one, and about half are mothers. So it could be an ideal market for home goods, or any other products targeted largely at women.

So why aren't businesses all over Pinterest? Well, legal issues and business dogmatism are the main reasons. Companies are cautious about risking copyright infringement by sharing photos they don't own.

No lawsuits concerning copyright on Pinterest have actually been filed, and Pinterest has done its best to make its terms of use more business-friendly. So copyright issues don't seem to be a serious danger — businesses just don't want to put in the effort of expanding to a new platform, in case it's nothing but a passing fad.

### 7. Final summary 

The key message in this book:

**As more and more of our lives take place online, it is essential for businesses to use social media campaigns. So figure out what platforms are best for your brand, and tailor your marketing to them. Remember there's no "one size fits all" for social media marketing, and don't be afraid of new** **platforms like Pinterest. Any business can find success when it gets social media right.**

Actionable advice:

**Take advantage of trends.**

Find out what issues people are already excited about, then tie in your product or brand. Trending topics on Twitter are a great way to do this. It's easier for you and more meaningful to your customers if you can transfer their enthusiasm for other things onto your company.

**Suggested further reading:** ** _Crush It!_** **by Gary Vaynerchuk**

_Crush It!_ is a motivational text, a blueprint and guide for those who want to translate their passion into a business. Using the author's life as an example, this book details how everyone can "crush it," i.e., realize the possibility of living their passion, determining their livelihood and making a living off of what they love to do.
---

### Gary Vaynerchuk

Gary Vaynerchuk is a _New York Times_ bestselling author and storytelling entrepreneur. He also runs his own digital consulting agency, VaynerMedia, which helps Fortune 500 companies develop effective social media strategies. He was voted one of the top 20 people every entrepreneur should follow by _BusinessWeek_.

