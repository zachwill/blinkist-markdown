---
id: 5e3bf4e66cee07000682b9cf
slug: jay-z-en
published_date: 2020-02-07T00:00:00.000+00:00
author: Michael Eric Dyson
title: JAY-Z
subtitle: Made in America
main_color: None
text_color: None
---

# JAY-Z

_Made in America_

**Michael Eric Dyson**

_JAY-Z: Made in America_ (2019) explores the enormous political and artistic contributions of one of the most influential hip-hop artists of our time. JAY-Z is the only rapper to become a billionaire. His career spans three decades and 17 platinum albums. However, he is much more than just a mega earner. These blinks reveal that he is also an adventurous, experimental artist and a principled activist, using his considerable influence to fight against social inequality.

---
### 1. What’s in it for me? Be inspired by the remarkable career of one of the most influential hip-hop artists of our time. 

JAY-Z is a quintessential American hustler who's used his enormous talent and political savvy to leave an impoverished childhood behind and become the immensely successful music mogul he is today. He has achieved the rare feat of combining commercial success with artistic experimentation, drawing not only on poetic influences but also on visual and performance art. As such, he has expanded the possibilities for hip-hop music and its representation in music videos.

JAY-Z is far more than just a successful musician. In these blinks, you will learn about his work as an influential public intellectual, vocal in his fight against oppression. You will also discover that he "puts his money where his mouth is," and has used his great wealth to produce documentaries, found organizations and support political campaigns in which he believes.

In these blinks, you'll discover

  * how JAY-Z and Beyoncé released a trio of albums to resolve a crisis in their marriage;

  * why JAY-Z should be considered a poet as much as a hip-hop star; and

  * what inspires JAY-Z's activism on prison reform.

### 2. Through his enormous talent and determination, JAY-Z managed to transform himself from a blight hustler to a bright hustler. 

What exactly is a _hustler_? Hustling can be seen as intrinsically American, a key attribute of anyone trying to secure that longed-for American dream. Hustling is being proactive, strategizing, working toward achieving your ambitions. 

However, there are different kinds of hustling. _Blight hustling_ takes place in the context of desperation. African Americans have a long history of blight hustling, which started in the time of slavery. As slaves, African Americans were denied basic human rights and economic opportunities. This meant that they were forced to live and work on the margins of society. The systematic oppression of African Americans during slavery and beyond had far-reaching consequences that can still be seen today in the high levels of poverty in African American communities.

JAY-Z experienced poverty first-hand. He grew up with a single mom who struggled to support a household of four children on one income. To make money and get ahead, JAY-Z became a blight hustler, dealing crack cocaine on street corners. Many of his peers who were dealing at the same time ended up in jail.

However, JAY-Z escaped that fate thanks to his obsession with music. He tapped into his enormous musical talent to create his first album. He then determinedly tried to secure a deal with a record company. When he couldn't find anyone who would support him, he founded his own company and released his first album by himself.

JAY-Z had turned into a _bright hustler_ — someone who uses his tenacity and drive in a creative way to effect positive change. As JAY-Z has become more and more successful, he has been able to pull off ever brighter and more magnificent hustles, transforming himself into a music mogul, social activist, and philanthropist. 

However, JAY-Z never forgot what it was like to come from the very difficult circumstances of the blight hustler. He repeatedly mentions it in his songs and keeps up strong connections to the communities from which he came. 

When he became established as a rapper, JAY-Z took emerging rappers like Drake and Lil Wayne under his wing. He became a mentor to them, making space for them, too, to become masters of bright hustle — using their creativity to forge successful careers for themselves.

### 3. JAY-Z managed to make his music accessible to a broad base of fans even while releasing experimental political songs. 

Countless rappers have made their money by making music with shallow lyrics that appeal to the broadest possible base of hip-hop fans, from fraternities to block parties. Others, like Talib Kweli and Common, have chosen to prioritize the message they want to communicate, making music that matters but doesn't necessarily reach the charts. JAY-Z's career is remarkable because he has managed to do both. 

He makes a lot of music on themes which are run-of-the-mill in hip-hop albums, like fast cars and booty and parties. But then he slips in songs that offer him the space to reflect on deeper social issues.

In his well-known song "Minority Report," he talks about the way that African Americans were abandoned by the government in the aftermath of Hurricane Katrina and protests the fact that certain lives are seen as being more valuable — and more worthy of saving — than others. In the impactful song "Young, Gifted, Black," JAY-Z reflects on white privilege and the structural inequalities that make it hard for African American youth to succeed in America.

Writing about topics such as these is commercially risky. They can be perceived as being too serious, and alienate fans. JAY-Z realized this following the release of his first album, _Reasonable Doubt_, in 1996, which received critical acclaim but didn't sell many records. The music wasn't accessible enough, and only appealed to a niche group of fans. 

JAY-Z decided that he wanted to be able to reach as broad a fanbase as possible. That way, he would be able to communicate his message to a very wide audience and have more political clout and the opportunity to effect change. But how could he write about serious political themes and still make contact with his fans?

He decided to focus on making music that was great to dance to and would be welcome at any party. With a solid base of party hits, he would have more room to experiment and be playful, slipping in some more thoughtful numbers on each album. He also makes use of the "B-sides" on his albums to experiment and publish tracks that might befuddle his party hip-hop fans.

### 4. JAY-Z is an unappreciated poet who should be recognized as a master of his craft. 

Hip-hop artists are not usually thought of as poets. JAY-Z's name is not placed alongside Wordsworth's or Shakespeare's on lists of the greatest bards in our history. However, that is due to shortcomings in the way that most people listen to his work, rather than a shortage of talent on his part. 

JAY-Z is a master of language with considerable intellectual gifts. If you listen closely to his lyrics, you'll hear that he uses complex metaphors and wordplay to communicate politically astute messages and make insightful social commentary. 

Although he never had formal training apart from English classes in high school, JAY-Z has dedicated himself to practicing his craft. He has spent thousands of hours perfecting and editing his lyrics in his head. Startlingly, he never writes down his complex rhymes. 

In this sense, JAY-Z can be seen as continuing the African American oral tradition, where folk stories were passed from generation to generation without being mediated through the printed page. His lyrics, therefore, sound natural and conversational. That might lead the listener to believe they were easy to compose. But that assumption would miss the lengths JAY-Z goes to in polishing his craft. 

A deceptively simple line such as "flyer than a piece of paper bearin' my name" from the song "Public Service Announcement," is actually laden with metaphorical significance. Paper fliers advertising shows are a very important part of the material culture of hip-hop. So JAY-Z's line alludes to that but can also be taken as a reference to flying — a significant concept in African America culture as it evokes the idea of escaping from captivity in slavery. In modern use, to look _fly_ is a reference to being smartly dressed, to _looking good_. 

JAY-Z's apparently throwaway line plays with this cacophony of meanings and resonances for African American culture. As in any powerful poetry, there is more and more meaning to be found the deeper you dig.

### 5. JAY-Z’s artistic practice breaks down the barriers between “high” and “low” art. 

In the song "Guns and Roses," Jay-Z describes himself as being like Michaelangelo, with his lyrics "paint[ing] pictures with poems." That line is representative of how JAY-Z repeatedly compares himself to other artists and seeks inspiration in their work.

Including references to visual artists and artworks in his songs is an unusual and subversive move for a hip-hop artist. He is explicitly laying claim to the title of _fine_ artist. Furthermore, he is challenging the idea of hip-hop as being something that belongs "on the streets," and showing that it can have a home inside an art institution as well. 

Perhaps nowhere is this more visible than in the "Performance Art Film" (as JAY-Z described his music video) for the song "Picasso Baby." In this long video, JAY-Z performs inside an art institution, experimenting with the artistic potential of the music video. The song is filmed in the Chelsea Pace Gallery and features references to artists like Picasso, artworks such as the _Mona Lisa_, and hallowed institutions such as the Museum of Modern Art (MoMA) in New York. 

JAY-Z performs from behind a cordon as if representing a living artwork. The music video took a grueling six hours to film, with JAY-Z's performance paying homage to legendary performance artist Marina Abromovic, whose work _The Artist is Present_ saw her sit for 700 hours in the MoMA. 

Institutional "fine art" spaces are usually inaccessible or even hostile to African Americans. There is a violent erasure of black artists and subjects, who very rarely appear on the gallery walls. JAY-Z laments this fact, singing "Marilyn Monroe, she's quite nice. But why all the pretty icons always white? Put some colored girls in the MoMA."

JAY-Z doesn't only lament the whiteness of art spaces. By inserting himself onto the walls of the gallery and labeling his music video a piece of performance art, he shows that his work, too, belongs in those spaces and that the categories of "high" and "low" culture are arbitrarily constructed for the purposes of exclusion.

### 6. JAY-Z is deeply committed to social justice and puts his money where his mouth is. 

By now, we're used to seeing celebrities make impassioned statements about social justice. It's less usual to see them actually backing up those statements with concrete actions.

JAY-Z is notable for fighting for social justice through a variety of channels. 

He uses his public platform on social media to help mobilize his fans to action over issues in which he believes. He has also published opinion pieces in the _New York Times_ about issues relating to criminal justice reform and social inequality. 

However, JAY-Z doesn't only affect change by disseminating his ideas. He also takes concrete actions to fight for his principles. 

For example, JAY-Z has founded a number of important initiatives to tackle criminal justice reform. He created the Shawn Carter Scholarship for young people who have previously spent time in prison and want to attend college but would usually be excluded for financial reasons. He also cofounded an organization that more directly campaigns against shortcomings in the criminal justice system and its systematic mistreatment of African American youth. 

As a way of calling broader public attention to this issue, he produced a documentary miniseries exploring the abuse and eventual tragic suicide of Kalief Browder, a man who was arrested on baseless charges but ended up being held for three years without trial at Rikers Island prison. The treatment he received during that time was so bad that his mental health never recovered, and he died by suicide after he was released. 

Drawing on his enormous wealth, connections, creativity, and artistic abilities, JAY-Z is thus able to make impactful interventions into an issue that is very close to his heart. 

A look at his philanthropic record shows that JAY-Z has also provided financial assistance to some of the people who needed it the most, like victims of the 9/11 terrorist attacks or Hurricane Katrina. He has also provided funds to bail out members of the Movement for Black Lives and to offer legal counsel to black victims of police brutality. 

JAY-Z has first-hand experience of growing up in poverty and being deprived of opportunities to prosper. That intimate knowledge has motivated him to use his wealth and influence to help people who are in the same position to get opportunities that he never had.

### 7. JAY-Z has fought for political change from within institutions. 

There are many ways to fight for social justice. Public protest is a very important way to call attention to social ills. For example, American footballer Colin Kaepernick's now-famous protest involved "taking the knee" during the singing of the national anthem at football games run by the NFL in order to protest police brutality. 

Though outspokenly supportive of Kaepernick, JAY-Z seemed to backtrack on that support by accepting an offer to advise the NFL on their live music programming and engagement with issues of social justice. 

This alliance with the NFL was widely criticized and seen as a betrayal of his commitment to social justice. Hadn't JAY-Z long campaigned against police brutality and the criminal justice system himself? Why was he throwing Kaepernick under the bus?

While these criticisms are valid, it's simplistic to frame JAY-Z as being simply a sell-out. A more nuanced way to look at it would be to see him as a pragmatic activist prepared to fight for change from _within_ institutions. Just as JAY-Z managed to package conscious hip-hop in a way that the masses wanted to consume, he's also able to work with an organization like the NFL to further the causes he supports. 

For a hip-hop artist, JAY-Z has unprecedented influence in mainstream politics. For example, he had a very close alliance with former president Barack Obama, prominently contributing to his election campaigns and mobilizing his base of millions of fans to support him as a presidential candidate. For his part, Obama made it clear that he was very inspired by JAY-Z, going so far as to quote him in political speeches.

There are many critics who will be frustrated that JAY-Z is not more _radical_ in his politics. He is, after all, a billionaire, complicit in the capitalist systems that produce inequality. And he is seemingly very cozy with the political elite. However, the positive effects of his pragmatic activism are undeniable, and he shows no signs of slowing down. The wealthier and more established he becomes, the more vocal he is in fighting against injustice.

### 8. The personal became political for JAY-Z and Beyoncé. 

In 2016, Beyoncé released her seminal album, _Lemonade._ It is a searing account of what it was like for her to experience the uncertainty and paranoia of being cheated on by her partner. In one video, she triumphantly marches along the street, smashing everything she can see with a baseball bat, seemingly reveling in the destruction. 

Beyoncé doesn't explicitly name JAY-Z as the culprit or claim the album as autobiographical, but there is enough innuendo for the listener to put the pieces together. The couple have always kept their relationship private, so this amounted to a shockingly public revelation. It also had broader political implications, forming a war cry against the mistreatment of women in a misogynistic culture. 

JAY-Z was himself responsible for helping to reinforce that culture in hip-hop, rapping about women in disrespectful terms, and fronting a macho public personality. Perhaps for that reason, his response to _Lemonade_ was a big surprise. 

He could have maintained a stony silence and refused to acknowledge the album publicly. Instead, he responded with an album of his own expressing his deep remorse. In 2017, JAY-Z released the album _4:44._ On it, he explicitly takes responsibility for what he's done and acknowledges how badly he betrayed someone he loves. He expresses shame and his fears of how his children will think differently of him when they find out.

By showing himself as weak and fallible, J-ZAY displays a rare and subversive vulnerability. He undercuts the macho culture he helped to create in hip-hop and in toxic masculinity more broadly. 

In 2018, Beyoncé and JAY-Z released the album _Everything is Love_ together, completing the trilogy of albums delving into the vulnerable crisis in their relationship. This album is subversive in a different way. It's not rage, or shame, but love that it celebrates. There are very few representations of loving black relationships in popular culture. In this album, it takes center stage.

A couple as high profile as Beyoncé and JAY-Z walks a thin line between the public and private. In choosing to publicize the crisis in their relationship through exquisitely crafted songs and videos, they made not only an artistic contribution but a political one too. They changed the narrative of how men and women can address each other in the context of macho hip-hop culture.

### 9. Final summary 

The key message in these blinks:

**Academics don't usually take the time to engage closely with the work of hip-hop artists like JAY-Z. Michael Dyson shows us that this is a big mistake. Analyzing JAY-Z's albums as a body of poetic political commentary provides startling insights into what it means to grow up amid racial inequality and police brutality in America. Studying the trajectory of JAY-Z's career provides important lessons into how entrepreneurship and the pursuit of material success can be combined with a strong commitment to social justice and meaningful activism.**

Actionable advice: 

**Embrace the spirit of DIY (Do-It-Yourself).**

JAY-Z is one of the most commercially successful hip-hop artists in the world. However, when he started out in the music business, no one wanted to sign him on to release his first album. Frustrated by all the rejection from record labels, JAY-Z decided to start his own. He released his own album to critical acclaim — and the rest is history. Don't allow others to stifle your creative passions. Today there are so many avenues for publishing music and writing online. If no one else wants to help you share your work, then figure out how to do it yourself.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Let Love Have the Last Word_** **by Common**

Now that you've been inspired by JAY-Z's career, why not deepen your knowledge of hip-hop by learning about the life of another remarkable rapper: Common? 

Common is one of JAY-Z's contemporaries, and like JAY-Z, he grew up in poverty and had to hustle to realize his musical ambitions and become successful. He is also notable for embracing a conscious approach to hip-hop, crafting lyrics to make you think and reflect on political issues. His efforts have resulted in him receiving the honor of an Academy Award for Best Original Song. 

In these blinks, you'll learn about Common's own take on his childhood and how he managed to overcome a difficult youth by embracing religion and practicing self-care. You'll also learn why his relationship with his mother is so important, and how Common had to grow into being a good father himself. If you want to delve deeper into the life of another great hip-hop artist of our time, then have a look at the blinks to _Let Love Have the Last Word_.
---

### Michael Eric Dyson

Michael Eric Dyson is a widely respected public intellectual and the author of the _New York Times_ best sellers _Tears We Cannot Stop_ and _What Truth Sounds Like_. He is also an opinion writer for the _New York Times_ and is a contributing editor at the _New Republic_ and ESPN's _The Undefeated_. Dyson holds the distinguished position of University Professor of Sociology at Georgetown University, where he teaches an acclaimed course on JAY-Z's life and work.

