---
id: 5950c336b238e10005bb4951
slug: jerusalem-en
published_date: 2017-06-29T00:00:00.000+00:00
author: Simon Sebag Montefiore
title: Jerusalem
subtitle: The Biography
main_color: D9B22B
text_color: 7D6619
---

# Jerusalem

_The Biography_

**Simon Sebag Montefiore**

_Jerusalem_ (2011) tells the story of a city considered holy by three of the world's major religions, and which is central to some of the greatest conflicts in human history. These blinks detail the history of Jerusalem, the near-constant battles it has inspired and the fundamental role it has played in shaping humankind over the course of millennia.

---
### 1. What’s in it for me? Understand how Jerusalem’s long and complex history affects us to the present day. 

It is said that one man can change history, but can the same be said for a city? Well, if there was one city that could claim such a legacy, it would definitely be Jerusalem. Serving as a center for the three Abrahamic religions — Judaism, Christianity and Islam — it's known as the Holy City, and continues to be a site of conflict to this very day.

So how did Jerusalem take on such significance to these three religions, as well as human history in general? To answer that question, these blinks journey through the millennia, beginning some 5,000 years ago, to understand the remarkable city of Jerusalem.

In these blinks, you'll find out

  * how the Temple of Solomon took on such importance for Jews and Judaism;

  * why Christians and Muslims also see Jerusalem as central to their religions; and

  * how foreign powers have influenced the Holy City throughout history.

### 2. Jerusalem was inhabited as early as 5000 BC, and the Israelites arrived 4000 years later. 

The Bible, in addition to being a holy text, also offers an early history of Jerusalem. However, since the Bible contains a great deal of contradictory information, it's not a reliable factual account in and of itself.

That's why we have to use archaeology and other ancient texts to verify certain biblical events.

For instance, the origins of the name "Jerusalem" date back to ancient Egyptian texts that refer to "URSALIM," a variation on the word "Salem," meaning "god of the evening star." And while widespread archaeological research in the area is restricted by the land's holiness, excavations conducted in surrounding areas have found that people inhabited the region as early as 5000 BC.

From there, we know that the first texts attributed to a Jerusalemite date back to 1458 BC, when the Egyptian empire included the entirety of modern-day Palestine. Those early texts show that King Abdi-Heba requested that Egyptian archers defend the city.

Then, about a century later, the first Hebrews, or _Israelites_, arrived. These newcomers proved considerably different from the Egyptians as they worshiped a single God.

Moses would later help the Israelites escape from Egypt, but it was his successor, Joshua, who brought the Israelites to Jerusalem. They struggled for more than a century after their arrival there, from 1200 to 1050 BC. That's when the Philistines defeated them in the Battle of Ebenezer and stole the Ark of the Covenant, their holiest object, which contained the remains of the Ten Commandments.

David was born after this battle, though, and he would go on to fight and defeat Goliath, the Philistines' best warrior. David became a great leader, uniting the tribes of Israel to beat the Philistines and retake the Ark.

In the wake of this victory, Jerusalem grew — but not by too much.

The settlement called City of David was built just outside the old town, along with a palace, but Jerusalem remained quite small, especially when compared to the neighboring city of Babylon. It was also during this time that David planned to build the city's first temple, although it wouldn't be completed until after his death, by his son, Solomon.

> Some have doubted the existence of David, but in 1993, an ancient inscription providing historical verification of his life was found in northern Israel.

### 3. Jerusalem’s first two temples offered a connection to God and played a central role in the development of Judaism. 

The first of Jerusalem's temples was built on a hill in the city known as the Temple Mount, sometime around 900 BC. A room was constructed in this sacred edifice called the "holy of holies," which was solely intended for the Ark of the Covenant and God.

In fact, once the Ark was placed in the room, it's said that God's divine presence filled the room, speaking to Solomon and saying, "I will establish the throne for thy kingdom upon Israel for ever." Because of this, Jerusalem, and Temple Mount in particular, was henceforth known as a site for divine communication.

However, since the temple was also a safehouse for Jerusalem's gold, it became a tempting prize for the many warlords in the area and would eventually be razed to the ground around 587 BC, when the Babylonian warlord Nebuchadnezzar leveled the temple, setting the city ablaze and doing away with the Ark forever.

But from this destruction also came new life: The destruction of the first temple can be seen as the start of the modern Jewish tradition. After all, alienated from their city and their temple, the Jews now lived in Babylon, ashamed that they had failed to strictly adhere to their religion. Because of this guilt, it was in Babylon that the Israelites truly became practicing Jews.

It was also a time when they began to define themselves by writing the first books of what would become the Bible. So, when a new Babylonian ruler, Cyrus the Great, emerged some 50 years later and offered the Jews a chance to return to Jerusalem and build a new temple on the same spot, some hailed him as a messiah.

The offer was accepted. Jerusalem's second temple was completed in March of 515 BC, and shortly afterward, the first Passover was celebrated since the Israelites' exile from Egypt. Thus began a new era of strict Judaism and reconstruction.

### 4. Jesus was born in a tumultuous time, which led to his crucifixion and contentious resurrection. 

The Israelites had returned to Jerusalem, but their sovereignty wouldn't last. Around 66 BC, the Romans seized control of the region. After the death of the city's longtime ruler Herod the Great, the resulting turmoil was inevitable.

Many people took Herod's death as an indication of the coming apocalypse. They ranted and raved in the streets, calling themselves prophets or kings and were crucified by the Roman governor for their troublesome behavior.

This was the political climate in which Jesus of Nazareth was born. While Jesus mostly steered clear of Jerusalem, he entered the city a few times to warn others about the imminent apocalypse, telling them to repent or be denied entry into heaven.

In response, people hurled stones at him, especially because he referred to himself as the prophesized Messiah and said, "I and my father are one." He even rode a donkey into Jerusalem on Passover, likely in the year 33 AD, preaching to a large and rambunctious crowd that the fall of the temple was coming and the apocalypse was near.

The high priests of the time debated how to deal with Jesus, and their choice was simplified when Judas helped Roman guards locate Jesus, leading to a rapid trial and crucifixion. While the Gospels say the Romans didn't carry out Jesus's punishment, the author believes they did. After all, historians know that the Romans used public crucifixion and whipping to dissuade opposition to their rule.

What came next is definitely up for debate: guards were stationed near Jesus's tomb to prevent his followers from retrieving his body and spreading rumors that he had been resurrected. However, many archaeologists believe that this is precisely what occurred, with a bribe paid to the guards.

It's impossible to know one way or another. What is for certain is that Jesus's followers went on to form the Nazarenes sect, led by Jesus's brother James. They even continued to practice Judaism, praying at the temple for the next 30 years.

### 5. The Roman emperor ordered an attack on Jerusalem, leading to the fall of the second temple. 

James went on to lead his brother's followers, but the transformation of this sect into the religion now known as Christianity would largely be led by the apostle Paul, a man who claimed that the resurrected Christ had told him to preach his message to the Gentiles.

Paul's work to convert people into the first "Christians" meant he was eventually taken to the ruler of Rome, Nero, for judgment and beheading. But Paul's fate wasn't the worst of it.

Things were much more frightening in Jerusalem, as Nero began to crack down on areas of rebellion within the Empire. Between 66 and 70 AD, Jerusalemites grew increasingly disgusted by the murderous ways of Roman generals, and the high priests eventually voted to stop offering sacrifices to Nero.

In response, Nero sent Titus Flavius Vespasianus to deal with the rebellion. But as soon as he began his march toward the city, Nero died and Vespasianus himself became Emperor.

By the time Roman soldiers finally arrived in Jerusalem, thousands of people had already died because of an ongoing power struggle. Despite this turmoil, the inhabitants of the city still came together, putting up a four-month fight before being overpowered by the Romans.

Titus's uncontrollable soldiers crucified hundreds of Jews every day. They cut off food supplies, forcing at least one mother to resort to eating her own baby. As the stench of the city became unbearable, the Temple was finally toppled, ending a resistance that had cost between 600,000 and 1 million lives.

But just like the fall of the first Temple, this act of destruction would only strengthen the Jewish faith. Take, for example, Rabbi Yohanan ben Zakkai, who looked upon the ruins of the holy site with a student, telling him, "be not grieved." His teachings would help form the foundation of modern Judaism.

Over a half a millennium later, the prophet Muhammad would point to the destruction of the temple as a sign that the Jewish people were no longer in the good graces of God.

### 6. The Romans converted to Christianity and began building their empire around it. 

Following the fall of the second temple, Jerusalem was known as Aelia Capitolina and Jews were banned from the city. Some of the rubble from the temple was used to build the Damascus Gate in the wall of the Holy City, and a statue of Aphrodite was erected on the stone upon which Jesus was crucified.

Around 193 AD, as the Roman Empire was becoming less concerned with the Jews in the city, and increasingly concerned with the threat posed by the spread of Christianity, a small group of practicing Jews were finally allowed to hold services in Jerusalem on the Mount of Olives.

In 306 AD, Emperor Constantine had a vision that inspired his religious awakening. He converted to Christianity and Jerusalem was renamed the holiest place on Earth. Constantine even banned all non-Christian services and began forming the church's hierarchy.

The Emperor's mother, Helena, a convert herself, became obsessed with locating the "true cross" used in Jesus's crucifixion. Having found one marked "Jesus King of Jews" and after placing it next to a dying woman who miraculously recovered from her illness, Helena and Constantine believed themselves to have discovered the true cross.

Helena would play another key role in Christian history by erecting the Church of the Holy Sepulchre, probably on the same land that held Jesus's tomb. It became the holiest Christian church and the apparent home of the true cross.

But as Rome rapidly embraced Christian monotheism, there was little room left for other religions. Many Jews were burned for refusing to convert and the Jewish holy group that had been praying on the Mount of Olives was only allowed entrance into the city once a year.

Anti-Jewish policies continued to be enforced in the following centuries. Beginning around 438 AD, though, Empress Eudocia did allow Jews more access to the Temple Mount on holy days. She ordered the reconstruction of Jerusalem's defensive walls, including those that can still be seen today surrounding Mount Zion and the City of David.

Nevertheless, even later, in the sixth century, Justinian and Theodora ruled with the hope of establishing a global, dominant Christian Empire. They repressed pagans, homosexuals and Samaritans, and forced the baptism of Jews.

### 7. Muhammad led his followers on a spiritual journey to Jerusalem. 

The Roman Christian Empire was huge and ambitious — but succumbed to the classic mistake of spreading itself too thin, which left Jerusalem vulnerable to conquests.

The first of these conquests was led by the Persians in 614 AD, who were joined by 20,000 Jews to take the city. Following the battle, the Persian Shah Khusrau II granted Jews control over Jerusalem.

As always, however, it wouldn't last.

Just three years later, the Shah exiled all the Jews. From there, the Byzantine Christians, now ruled by Emperor Heraclius, retook control and brought the true cross back to the Holy Sepulchre.

It was around the same time, in 610 AD, that a man by the name of Muhammad was meditating when he was visited by the Archangel Gabriel, who told him that he was the chosen messenger and prophet of God. In a later vision, Muhammad was taken at night by Gabriel and a winged horse to the "Furthest Sanctuary," as the Koran puts it, which was later interpreted to be the Temple Mount of Jerusalem.

Muhammad overcame an initial reluctance to accept his role as a prophet and began preaching. His initial group of followers eventually established Islam's first Mosque in the city of Medina.

At first, the early Muslims had Jewish allies and even incorporated Jewish traditions, like banning pork and observing the Sabbath as beginning at sundown on Friday. But as Muhammad experienced further revelations of a rapidly approaching judgment day, the pressure increased to convert people and save them from this judgment day.

To deal with the non-muslims who were unwilling to submit to Muhammad's growing empire, a Jihad, or struggle, began. This led to more militant action as the conquest spread throughout the Arab world, with women and children taken into slavery and people forced to convert to Islam.

Muhammad died in 623 AD, before reaching Jerusalem. But he was succeeded by Omar, who knew the holy city was important to the Byzantine Christians. So he was able to negotiate a peaceful takeover of Jerusalem that granted the Christians access in exchange for their surrender.

### 8. Attempts at Muslim unity were disrupted when the First Crusade ravaged Jerusalem. 

The first Muslim dynasty brought an entirely new sense of freedom to Jerusalem.

Jews were once again allowed to pray on the Temple Mount and the early Muslim leader, Abd al-Malik, brought a sense of unity to both Shia and Sunni Muslims by having the Dome of the Rock and the al-Aqsa Mosque built.

Finished around 692, the beautiful golden Dome transformed the skyline of Jerusalem forever. And it was Abd al-Malik's son, Walid, who played a crucial role in finishing the construction of the al-Aqsa Mosque, Islam's holiest house of worship.

During the construction of these monuments, the Koran was also being compiled. This holy text of Islam placed particular emphasis on the role Muhammad played, giving precise details on the religion's rituals and bestowing the title of _Caliph_ upon Jerusalem's new rulers.

But any sense of peace in Jerusalem would come to a bloody end at the hands of Crusaders.

The first Crusade was a violent campaign called by Pope Urban II in 1095. He wanted to liberate the Church of the Holy Sepulchre. His orders were for the soldiers of God to cleanse the land of infidels, atoning for their own sins in the process.

So, thousands of drinking, gambling and fornicating men put down their bottles and picked up their swords, answering the call of God. As they marched on the Holy City, thousands of Jews were killed or forced to convert.

While the Christian soldiers weren't the best trained, they were united, which is more than can be said of their Muslim opponents. In-fighting and personal disputes between Arabs and Turkish leaders kept them from joining forces and provided the Crusaders with a major advantage. Eventually, the Crusaders would scale the walls of Jerusalem, open the doors and massacre a population now trapped within the defenses of their own city.

In the end, piles of heads, hands and feet would litter the city's streets as blood-drenched Crusaders tore their way through Jerusalem.

### 9. Muslim rebellion led to future crusades and a continual battle over Jerusalem. 

The crusades may have dismantled Muslim power in Jerusalem, but by 1187 the city had changed hands once again, as a remarkable Muslim leader by the name of Saladin brought the city under his control. Starting out as the talented young nephew of a general, he rose to become Sultan and united an area that now comprises the territory of modern-day Egypt, Syria, Yemen and most of Iraq.

After defeating the Crusader armies in a desert battle outside of Jerusalem, Saladin captured Guy, the king of the city. He took him to Damascus and left the city under the protection of two knights.

Preferring to forgo a massacre, Saladin then negotiated for Jerusalem's capture and the imprisonment of the inhabitants, who would be ransomed or forced into slavery. Upon receiving a total of 220,000 dinars of ransom money, Saladin released the majority of the Jerusalemites. He would later release an additional 1,000 prisoners before finally occupying the city.

Some people wished for Saladin to destroy the Church of the Holy Sepulchre entirely, but he remained tolerant of the Christian pilgrims — at least within the walls of the city. That being said, with Saladin in control of Jerusalem, future Crusades were practically inevitable.

Following the generally ineffective Second Crusade, the Third Crusade was led by Richard the Lionheart of England and Philip II of France. This campaign ended with Richard nearly single-handedly capturing the city of Jaffa.

From there, a treaty was signed in 1193 that gave Christians access to the Church of the Holy Sepulchre. It was similar to a deal that would be negotiated a few decades later, in 1229, after the Roman Emperor Frederick II launched the Sixth Crusade.

Following this later campaign, Frederick sat down with Sultan Kamil to negotiate a minimum ten years of peace that would give Frederick Jerusalem and Bethlehem, along with clear access to the sea, as long as the Muslims remained in control of the Temple Mount.

While Jews were entirely left out of this deal, it was a remarkable bid to share sovereignty over the city — although it invariably left some people unsatisfied. Muslims mourned the loss of their city, while the Church wished Frederick had indeed taken the Temple Mount as well.

### 10. An army of slave-soldiers retook Jerusalem, leading to the city’s reconstruction. 

On July 11, 1244, 10,000 saber-rattling Tatar horsemen invaded Jerusalem and set the Holy Sepulchre ablaze, killing the monks and nuns who lived there and disemboweling the priests. This onslaught cleared the way for a new empire to rule the land: the Mamluks.

Their rise to power is quite extraordinary since the name "Mamluk" refers to a slave soldier — not someone you would necessarily expect to rule over an empire. Nevertheless, their leader Baibars the Panther did just that.

Baibars was a huge blond and blue-eyed Turk who had been sold from person to person in Syria and Egypt before converting to Islam. In 1260, he led a Mamluk army in what was one of the first and most important victories against the Mongols. Baibars naturally expected a substantial reward for his service, but when Mamluk Sultan Qutuz refused, Baibars killed him and claimed the crown for himself.

His first order of business as the new ruler was to do away with any remaining Crusaders and retake Jerusalem. And that's precisely what he did. Baibars soon created a new caste system that would remain in place in the city for the next 300 years. It required Jews to wear yellow turbans, Christians to wear blue ones and forbid both groups from wearing armor or fur and from riding horses.

Baibars then led his army through one Crusader city after another, decimating the populations and using their heads as decorations.

However, his influential rule didn't last very long. He died in 1277 after accidentally drinking from a glass of poisoned milk that he had intended for another person.

At the time of his passing, Jerusalem was in terrible shape and only home to about 2,000 inhabitants. The rebuilding of the city would fall to the new Mamluk sultan, Nasir Muhammad, who made a number of repairs in 1317 that can still be seen today.

For instance, he ordered the Temple Mount to be refortified, with new roofs installed on the Dome and al-Aqsa Mosque. He also had the city's gates beautifully adorned, restoring some of Jerusalem's long-held prestige and its status as a desirable destination.

### 11. After the Mamluks, Jerusalem was taken over by the Ottomans and the city grew. 

Before Mamluk rule came to an end, Jews were finally allowed to rebuild their synagogues, and the Jewish Quarter of Jerusalem grew to host a well-established population of about 1,000 people. In addition, the city was bringing in considerable revenue by placing exorbitant taxes on European Christian pilgrims.

By now, however, you've surely noticed that Jerusalem was rarely stable. Indeed, by 1516, the city had changed hands once again. This time, the conqueror was the Ottoman Empire.

Led by the Ottoman Sultan Selim the Grim, the Ottomans took Jerusalem with relative ease, adding it to their growing empire, which would eventually span most of the Middle East. While Selim would pass away a few years later, he left his 25-year-old son Suleiman in charge to become, as Suleiman's wife called him, the "Solomon of his age."

Suleiman dreamt that he was visited by Muhammad with a message to rebuild the city, and he followed these divine orders. He spent his 50 years in power making Jerusalem as beautiful as possible. In fact, much of the beautiful adornments still visible in the Old City district today were created during Suleiman's reign.

Due in large part to this impressive beautification, the city's population rose to 16,000, including both Jews and Protestants. Suleiman welcomed Sephardic Jews who came as refugees from Spain, where another Crusade was ravaging the land and Jews were being targeted by the Spanish Inquisition.

He even gave the city's Jewish population a nine-foot wall on the western side of King Herod's temple, not far from the site of the city's first synagogue.

They called this place "ha-Kotel." Known today as "the Western Wall" or "the Wailing Wall," for many Jews it symbolizes the holiness of the city and remains one of the holiest sites in Judaism.

It was around the same time that the German monk and theologian Martin Luther began to challenge the authority of the Catholic Church, setting in motion the Reformation and signaling the launch of the Protestant movement. This new religious sect held the Bible as its highest guiding principle, which naturally increased the importance of Jerusalem.

### 12. Around the turn of the twentieth century, hopes for Jewish and Arab independence flourished. 

The Ottomans held Jerusalem for centuries, but their grip loosened in the nineteenth century as unforeseen immigration changed the existing dynamics of the city.

Jews had been wrongly blamed for the assassination of Emperor Alexander III of Russia and, as a result, 2 million of them fled Russia between 1888 and 1914. Thousands of them went to Jerusalem.

This massive wave of refugees launched the Zionist movement for Jewish independence. The first Zionist Congress in 1897 called for a Jewish homeland in Jerusalem, an idea previously supported by the likes of Napoleon and US President John Adams. A comrade of Karl Marx's named Moses Hess even predicted an increase in anti-Semitism and suggested that Palestine become a socialist Jewish nation.

But at the same time, Arab nationalists were also searching for a homeland, posing a major dilemma for the Ottomans as World War I began in 1914. After all, the Ottomans wanted the Arabs to fight on their side, but couldn't stomach Arab demands, which included their own sovereign land following the war.

By contrast, the British were willing to offer Arab independence if the Allies defeated the Ottomans and Germans, and the Arabs were convinced that a British alliance was their best bet. The Arab Revolt, aimed at forging an Arab nation free from the Turkish Ottomans, was launched in 1916.

During this period, the Zionist movement became a political tool of war. Since Russia wanted to be rid of their Jews, the British and French agreed that embracing Zionism was a smart way to maintain a Russian alliance.

Germany, too, issued pro-Zionist propaganda. After all, one of the most dangerous anti-Semitic hoaxes ever propagated was being circulated at this time: a book called _The Protocols of the Elders of Zion_. It suggested that a small group of Jews controlled all global affairs, including the outcomes of massive conflicts like World War I. Germany wanted this supposedly powerful force of Jewish influence on their side.

Ultimately, the United Kingdom committed to the Zionist cause by issuing the Balfour Declaration in 1917, which called for a Jewish homeland in Palestine.

### 13. Following World War I, a number of international treaties increased tensions in Jerusalem. 

The Allies of World War I had made a strategic decision to support Zionism through the Balfour Declaration. Ironically though, if this resolution had been delayed by just a couple of days, it probably would have never been released.

The declaration was primarily designed to maintain the alliance with Russia, but the night before it was signed, the Bolshevik Revolution succeeded and Russia withdrew from the war entirely.

What made the situation even more complicated was a third wartime agreement between Britain and France called the Sykes-Picot Treaty. The agreement stated that France would receive control over Syria and Lebanon, the United Kingdom would get Iraq as well as part of Palestine, and Jerusalem would be split between France, Britain and Russia.

This tangled mess, which took years to settle, came to a head at the 1919 Paris Peace Conference. There, US President Woodrow Wilson was blamed for his failure to rein in British and French imperialism.

In the end, Palestine came under British rule and Syria under French rule in what became known as the "mandate swindle."

Tensions only rose from there. As consolation for this compromise, Arab noble Emir Faisal was crowned King of Greater Syria, which included Lebanon and Palestine.

While this appointment may have been intended as a largely symbolic one, it emboldened Arabs throughout the region to protest the Balfour Declaration, which was still honored by the new mandate. As a result, protests erupted with Arabs bearing signs that read, "Palestine is our land, Jews are our dogs!" and "Slaughter the Jews."

In an attempt to quell the chaos, a British officer, Sir Ronald Storrs, was made governor of Jerusalem. But with only 188 policemen to deal with an onslaught of rioting Arabs, and a Jewish Quarter that was rapidly degenerating into a war zone with Jewish girls being gang raped in the streets, he could hardly keep control over the worsening situation.

As 88,000 more Jews entered Palestine between 1921 and 1929, it was clear that the Balfour Declaration wouldn't go over smoothly and Arab tensions would only worsen.

### 14. The decades after WWI were filled with escalating violence and increased extremism. 

Tensions in Jerusalem were steadily rising, and in 1921, certain changes were made with the goal of rapid de-escalation. Winston Churchill, then UK Secretary of State for the Colonies, put Emir Faisal in charge of the Kingdom of Iraq and the notion of a "Greater Syria" was dropped.

Then, as Churchill tried to convince the residents of Jerusalem that the Jews had an ancient claim to the land, he went on to establish a two-tiered system of city leadership involving a mayor at one level, and a mufti — the highest Muslim legal authority — on the other. The position of mayor was filled by Ragheb Nashashibi and that of mufti by Haj Amin Husseini.

Unfortunately, Husseini was a megalomaniac and anti-Semite who would later embrace the views of Adolf Hitler, and things rapidly worsened under his authority. For instance, after a Jewish boy was murdered while attempting to retrieve a ball from an Arab garden, some young Jews retaliated by attacking the Arab Quarter. Their vengeance led the mufti to call for another attack on the Jewish Quarter, resulting in the Thawrat al-buraq riots of 1929 that claimed 131 lives.

Extremism was rapidly increasing on both sides. It was in this climate that future Prime Minister of Israel David Ben-Gurion replaced the more placid Chaim Weizmann as Zionist leader, primarily in response to the mufti and his calls in 1933 for Jerusalem to adopt a "fascist anti-democratic leadership."

From there, in the 1930s, British and Zionist leaders attempted to negotiate a "two-state solution," or an Arab-Jewish confederation. However, the mufti scoffed at the idea and more violence ensued.

Husseini used the ongoing violence to have his rivals killed, including members of his own family and any Arabs suspected of cooperating with authorities. All of this was far too much for Mayor Nashashibi, whose opposition to the mufti placed him on Husseini's kill list.

In the end, two opposing groups of Arabs emerged: those in favor of Husseini showed their support by wearing a checkered keffiyeh scarf, and those in favor of Nashashibi wore a tarbous, or fez, as a symbol of their willingness to negotiate.

### 15. Escalating violence led to the Battle of Jerusalem in 1948. 

While there was once a will to find a compromise in Jerusalem, this mindset rapidly vanished in the 1940s as both sides became more violent, and even the British became a target.

For example, take the King David Hotel, Jerusalem's most luxurious accommodation at the time and the home of British military intelligence in the region. It was bombed by Israeli extremists on July 22, 1946. The incident claimed 91 lives and demolished the entire wing that housed British intelligence offices.

Then, in 1947, the United Nations became involved, once again pushing a two-state solution that would have Jerusalem overseen by a governor of the United Nations' choosing. However, the Arab Higher Committee rejected the proposal and yet another wave of violence overtook the city. Over the six weeks following the refusal, 1,060 Arabs, 769 Jews and 123 Brits lost their lives.

On April 9, 1948, despite being under orders not to hurt women or children, Israeli extremists launched their most heinous attack yet. They entered an Arab town west of Jerusalem and tossed grenades into family homes, killing between 100 and 254 people. Just a week later, Arab forces killed 77 doctors and nurses of the Jewish Hadassah Hospital, later selling postcards depicting the mutilated victims.

When the British finally left on May 14, 1948, Jews were in control of Western Jerusalem, while Arabs held the Old City and the East. These battle lines would set the stage for the Battle of Jerusalem, which officially started just four days later, on May 18.

At the time, although both US President Truman and Soviet leader Joseph Stalin both recognized Israel, its status remained fluid and few expected the dramatically outnumbered Jewish soldiers to last very long. Nonetheless, months of combat ravaged the narrow streets, ancient homes and dark basements of the Old City.

The Israelis lost the first round of the battle and access to the Western Wall along with it. But in November 1948, they pushed the fight outside the city's walls and beyond, capturing the Negev desert, forcing a ceasefire and reaching an armistice with all five Arab states in the region.

### 16. An angered Egyptian President spurred on a brief conflict that secured a decisive victory for Israel. 

Even though the city was now split in half between Arabs and Jews, on December 11, 1949, Jerusalem was officially declared the capital of Israel. And for the first time since the Crusades, it had a king: Abdullah of Jordan.

It wasn't long before coup attempts began, leading to Abdullah's assassination in the al-Aqsa Mosque in 1951 and the crowning of his grandson Hussein. King Hussein was more willing to work with the Israelis and was better able to cope with the tyrants of the region, like Saddam Hussein.

That being said, there were always ongoing threats. These now came in the form of President Gamal Abdel Nasser of Egypt, who considered the new arrangement a wound to Arab pride, and Yasser Arafat, the leader of the Palestinian Liberation Organization, or PLO, which led a raid on Israel in January of 1965. While this attack failed, it left Palestinians feeling emboldened and prompted even more Arab resentment of Israel.

While King Hussein sought peace, his hand was forced by Nasser, who told him he could either support the fight against Israel or be seen as a traitor to the Arabs. He chose the former and air raids began in 1967.

At the same time, Syria, which also supported Egypt, began mobilizing its forces. The thought of the impending violent conflict was so nerve-wracking for Israelis that Yitzhak Rabin, then Chief of General Staff of the Israeli Defense Forces, couldn't sleep or eat for days in advance of June 5, 1967, when 6,000 shells were launched into Israel from Jordan. This attack marked the start of what became known as the Six-Day War.

Israel was prepared for war. They decimated Jordan's air force in one fell swoop and their ground troops only needed four hours to take the United Nations Government House, which the Jordanians were using as a base of operations.

On the night of this attack, with the sky ablaze with artillery fire, Israeli paratroopers crossed into the West and, on June 6, seized the northern territory outside the city walls known as the American Colony.

The next day, they continued on to the Mount of Olives and the fight was all but over. An Israeli tank destroyed a bus blocking the Lions' Gate and, after a brief firefight, Israel took the Temple Mount.

> The Lions' Gate is the eastern gate that leads to the Mount of Olives and Temple Mount.

### 17. The situation in Jerusalem remains a complicated standoff. 

Upon breaking through the Lions' Gate, Israeli soldiers secured access to the Western Wall for the first time in roughly 20 years. It was a poignant moment and many of them wept upon gazing at this holy site that most had only seen in photos.

The Minister of Defense, Moshe Dayan, then issued a statement saying, "to our Arab neighbors, Israel extends the hand of peace and to all peoples of all faiths we guarantee full freedom of worship." It had taken 2,000 years for Jews to once again be able to visit the Temple Mount, but Dayan made it clear that the site would be controlled by the Islamic charitable fund, _waqf_, not by Israel.

However, this statement did little to untangle the situation. In fact, since the end of the Six-Day War, Jerusalem has been stuck in a complicated position. For instance, some Christians still believe that Jerusalem is the place where Armageddon will begin and that Jewish occupation of the city is just another step in that direction. In fact, Australian Christians actually set fire to the al-Aqsa Mosque in 1969, in an attempt to speed up the process.

Or consider an incident from 1982, when two Arabs were shot on the Temple Mount. Allegations followed that this was the work of Israelis who want to see the construction of a third temple, but in reality, only a tiny minority of Jews have any desire for such a thing to happen.

And peace is still a long way off. Some people still hope for a two-state solution, despite the Palestinian Intifada, or uprising, of 1987, which sparked the launch of the nationalist group Hamas and struck a blow against the prospect of a unified Israel.

Then again, a treaty was signed in 1993 and private talks have occurred as recently as 2008. Such developments suggest that the democratic Palestinian government is still willing to negotiate.

But Israel isn't making things any better. If there are to be any realistic prospects for peace in the future, Israel needs to stop displacing Arabs by constructing settlements in the name of Israel's "mission."

### 18. Final summary 

The key message in this book:

**The history of Jerusalem offers a fascinating insight into the powerful role that religion has played in shaping humanity. The story of Jerusalem indicates that while it may seem that violent conflict is forever doomed to repeat itself, there is always hope that people of different beliefs will learn to live together in peace.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _A Peace to End All Peace_** **by David Fromkin**

The Middle East today is a hotbed of violence and war. Whether the civil war in Syria or the intractable Arab-Israeli conflict, peace in the region seems a far-off dream. Yet how did the Middle East become so unstable? In _A Peace to End All Peace_ (1989), you'll learn that European colonial ambitions during World War I were the catalyst that led to today's modern crises.
---

### Simon Sebag Montefiore

Simon Sebag-Montefiore is an award-winning British author of historical nonfiction. His titles, including _Young Stalin_, _Catherine the Great and_ _Potemkin_ and _Stalin: The Court of the Red Tsar_ have received multiple awards and have attracted worldwide critical acclaim.

