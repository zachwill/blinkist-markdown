---
id: 552bde5d6466340007780000
slug: job-u-en
published_date: 2015-04-15T00:00:00.000+00:00
author: Nicholas Wyman
title: Job U
subtitle: How to Find Wealth and Success by Developing the Skills Companies Actually Need
main_color: 2A9BD2
text_color: 1B6285
---

# Job U

_How to Find Wealth and Success by Developing the Skills Companies Actually Need_

**Nicholas Wyman**

_Job U_ (2015) reveals how the idea that college is for everyone will disadvantage both the individual and the workforce as a whole — and even the economy itself. These blinks explore alternative approaches to education that will help us find fulfilling _and_ well-paying careers, proving that college isn't all it's cracked up to be.

---
### 1. What’s in it for me? Discover why there is more to learning than college. 

Everybody knows that if you want to get ahead in life, you need to go to college. No employer in their right mind would employ someone without a college degree — someone like that must be an idiot!

If you think like this, you might be in line for a shock.

Employers in many of the fastest growing industries in the United States aren't looking for college graduates. What they are looking for are the specific skills needed for specific roles, and for people with good experience. For them, a college degree is worse than useless.

So what should you do instead? These blinks show you the alternatives to college that don't cost the Earth and which give you the skills you need to find the job you want.

In these blinks you'll discover

  * why you don't even need to leave your home to get an education; and

  * why student debt could bring down the US economy.

### 2. A college education is not the best path into the future world of work. 

Have you got kids? If you do, it's a fair bet that once they've finished high school, you'd like them to go to college. A college degree will give them the best chance of a good job with a high salary, and that's a fact. Or is it?

You might be surprised to hear that every day, jobs are created that do _not_ require a college education. In fact, according to a forecast by the Labour Department of the United States, a mere 27 percent of new jobs that will be generated from 2014 to 2024 require college education. Moreover, the US Bureau of Labor Statistics predicts that of the 20 occupations expected to create the most new jobs from 2012 to 2022, only _one_ of these — general and operations management — requires a bachelor's degree.

So if we continue to force the norm of college education upon our children, they may later find themselves fighting for jobs where their degree is no longer relevant. And it's not just graduates in a decade's time who'll find themselves unemployed — the problems have already begun. The jobless rate for college graduates under the age of 25 was 8.2 percent in 2013 compared to 5.4 percent in 2007.

So if a college education can no longer secure a job, what can?

There are two keys to finding a job today: technical skills and work experience. Rapidly growing industries, such as the tech world, require you to have both the technical know-how and, where possible, real world experience. Most college degrees can give you neither.

In this way, workers who did not go to traditional colleges to study are increasingly better off than those that did. For example, in Florida and Virginia, graduates with technical or occupational associate degrees currently outearn the state's bachelor's degree recipients by almost $2,500 per year.

So statistics show loud and clear that a college degree may not benefit you as an individual. But did you know that they are harming society too? Read on to the next blink to find out why.

### 3. Forcing everyone to go to college will create an unbalanced, debt-ridden society. 

With such a rapidly changing workforce, our society's "college for everybody" mentality is not only misleading for many graduates seeking employment — this mind-set will also damage society as a whole. But how?

Because we promote a college degree above all else, there has been a marked decrease in vocational and technical courses. For example, faced with a budget deficit in 2013, the Los Angeles Unified School System slated nearly all its shop classes for elimination by the year's end. That's around 600,000 students being denied vocational training.

Though such a decision might save money in the short term, it will nevertheless create complex problems for our future, as jobs requiring technical skills find fewer and fewer potential employees to fill them. In fact, the increasingly unproductive workforce we'll be left with will cost society far more.

Another alarming cost on the horizon is our skyrocketing student debt. Today, most students attending college will take out a student loan and hope they'll be able to pay it back once they're earning higher wages.

However, these higher wages are far from guaranteed. In fact, it's highly unlikely that many students will be able to pay off their loans. This has huge consequences, not just for the students, but for our economy.

At the beginning of 2014, US student loan debt already exceeded $1 trillion _and_ was rising. That put it well above the total US credit card debt, making student loans the _largest_ category of unsecured credit. If a whole generation of students are unable to pay off these loans, we would see an economic crisis worse than the subprime mortgage collapse that lead to the 2008 financial crisis.

Today, life after high school looks somewhat like a minefield. If college is not the best route for young individuals, or the society they live in, what is? Read on to explore the alternatives in the following blinks.

### 4. Apprenticeships will support you on your way to a high-earning job. 

Want to get an education that's proven practical for thousands of years? Consider an apprenticeship. Back in the classical world, youngsters would learn their trade on the job alongside experts. Yet, as "college for everybody" took over, apprenticeships took a back seat.

However, the benefits of apprenticeship are plenty, and with our increasingly precarious world of work, it might be time to bring the apprentice back to the fore.

First, have a guess which group yields more dropouts: apprentices or college students? If you thought college students were less likely to quit, you're mistaken!

The professional relationship between apprentice and mentor is incredibly personal, challenging and inspiring. Apprentices find themselves engaged in a constant back-and-forth exchange that involves solving problems and receiving feedback from the mentor. And, if an apprentice ever feels lost or unable to handle the workload, they'll always have someone to talk to.

Compare this situation to a college student going it alone with no one to turn to in a crisis, and it's clear why an apprentice has a better shot of completing their education!

It's not just high school leavers that can benefit from an apprenticeship. Countries which encourage young people to take apprenticeships are _significantly_ more productive than countries where college is promoted blindly.

When Britain's former prime minister Tony Blair asked German Chancellor Angela Merkel why her country's economy was so strong, her answer was short and simple: "We still make things." And behind this confident answer? Apprenticeships. 

Germany's apprenticeships and wider vocational programs provide employees suited to the actual workforces. It's no surprise that three out of every four German secondary school students complete apprenticeships. As a result, youth unemployment in Germany was 7.7 percent in 2014, compared to 14.4 percent in the United States and an even higher 19.7 percent in the United Kingdom.

### 5. Associate degrees can cut down your debt and increase your job opportunities. 

Wouldn't it be great to get a huge slice of the teaching you get from a college degree, but for a fraction of price plus a better chance of finding a job? Nope, it's not just a fantasy — it's a very real thing called an _associate degree_!

Associate degrees are two year courses, usually taken at a community or a technical college, that combine elements of traditional academic learning with specific professional skills, such as those required by an engineer, lab technician or midwife.

So why do an associate degree? First of all, it's _cost-effective._ According to research by the College Board, the cost of a two-year associate degree in the United States averages about $6,300. But tuition and fees for four-year bachelor's degree programs at in-state public institutions cost about $34,000 and an unbelievable $116,000 at private institutions!

Associate degrees aren't just good for your wallet — they're also good for your future. Research shows that many associate degree holders enjoy better employment prospects than their collegiate counterparts, and a full one-third of them start their careers at higher pay levels.

This is because associate degrees are offered in a wide range of skill areas, including aviation, computer technology, biomedical design, paralegal design and dozens more. Associate degree graduates with vocational skills in a specific area are far more attractive to employers, compared to college degree graduates with more abstract skills and purely academic abilities.

So if you want a better job and less debt, an associate degree is a smart option. However, they do require you to study full time. If you're looking for a part-time option, you're in luck, as the following blink outlines an alternative path.

### 6. Study flexibly, for free and even from home with a Massive Open Online Course. 

If associate degrees sound like a great option to you, but you're unable to complete one because of work commitments, don't despair! There is a way of studying for a degree without giving up your job, and there's even a charming acronym for it — meet the _MOOC_.

MOOCs, or _massive open online courses_, offer you a broad range of study paths ranging from theory to technical skills and hands-on subjects. Wait, hands-on subjects? Can you really teach that online?

You sure can! One of the most popular skills-based subjects is a course called "Science and Cooking: From Haute Cuisine to Soft Matter Science." It's a powerful example of how even the most hands-on skills can be taught virtually. After all, what could be more tangible than the smells and tastes of food?

A MOOC does more than give you a great range of study options — these degrees democratize the entire system of elite education. Think about it: An incredibly expensive college degree seems "worth it" because of the prestige that comes from studying at a particular university.

But with MOOCs, this myth is shattered. The quality of teaching is superb, it's free and you don't even have to leave your own home. Through MOOCs, a great education is available to all, no matter your financial position.

And if you're still not convinced that you should swap your college dream for a MOOC, just think of the flexibility. As a MOOC student and employee at your job, you can undertake your studies at any place, any time.

And, you're able to learn any course you want, regardless of your academic background. In this way, MOOCs can even help employees to skill up in a different profession in order to change their current vocations without having to leave their job first.

### 7. Final summary 

The key message in this book:

**Despite what you've heard, college is** ** _not_** **for everyone. This attitude will not only disadvantage you, but the broader workforce. However, high unemployment, underemployment and drop-out rates can be solved by exploring many paths to a fulfilling and successful work life that will take you further than a college degree ever could.**

Actionable advice:

**Decide for yourself!**

So it's time for you to decide what you want to do with your life and all you hear from your friends, family and the media is college, college, college? Take a step back to consider what you want. If you're not sure about college, take a look at your options — you've got lots of them! Then decide what's best for you.

**Suggested** **further** **reading:** ** _Startup of You_** **by Reid Hoffman and Ben Casnocha**

_The Start-Up of You_ is a guide to how you can leverage strategies used by start-ups in your own career: being adaptable, building relationships and pursuing breakout opportunities.

In a world where entire industries are being ravaged by global competition and traditional career paths are fast becoming dead-ends, everyone needs to hustle like an entrepreneur.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Nicholas Wyman

Nicholas Wyman is a Harvard Business School graduate who discovered his true strengths in a kitchen service apprenticeship. As CEO of the Institute for Workplace Skills and Innovation, he assists thousands of people in finding their strengths and the jobs to suit them.

