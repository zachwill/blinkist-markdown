---
id: 5e5f901e6cee07000655e2a6
slug: jog-on-en
published_date: 2020-03-05T00:00:00.000+00:00
author: Bella Mackie
title: Jog On
subtitle: How running saved my life
main_color: None
text_color: None
---

# Jog On

_How running saved my life_

**Bella Mackie**

_Jog on_ (2019) examines the devastating effects of anxiety disorders and reveals how exercise can help to treat them. These blinks also explore author Bella Mackie's own mental health struggles and chart her journey to recovery.

---
### 1. What’s in it for me? Keep your mind and your body running smoothly. 

Sweaty palms, a racing heart, and your mind stuck on a constant loop of negative thoughts — too many of us know the hallmarks of anxiety. But what do you do when your anxiety becomes so bad that your whole life falls apart? And how can you help yourself get better? 

As you'll learn in these blinks, the answer can be as simple as going for a jog. 

Both a searingly personal memoir and a primer on mental health, these blinks explore the health problems that drove journalist Bella Mackie to the brink of despair — and how a simple exercise regime pulled her back from the edge. Packed with devastating insights, scientific research, and inspirational stories, these blinks are a call to action to get fit and start living again. 

A short warning: Some of the content in these blinks contain sensitive or potentially triggering content pertaining to self-harming.

In these blinks, you'll discover

  * what running does to your brain;

  * the invisible barriers that prevent us from exercising; and

  * how nature affects your mood.

### 2. Bella Mackie struggled with mental health problems for most of her life. 

Bella Mackie had only been married for eight months when her husband walked out on her. After he left, Bella lay on the floor, utterly heartbroken. She knew she would have to get up eventually and deal with this trauma — just like anyone after a failed relationship. But the problem was, Bella wasn't like everyone else.

Even as a young child, Bella was incredibly anxious. Everything frightened her — from surreal pieces of art to certain pieces of music to the noises that cars made. Her stomach and her chest often ached with worry. When she went to school parties, she would get a powerful sense of dread; something would just feel wrong. 

When she went to high school, Bella developed strange behaviors. Whenever she had a scary thought, she would swallow, blink, or spit on the ground to try and get rid of it. She hurt herself, too. Not only did she pick at her skin until she drew blood, she also pulled her hair out — a condition known as _trichotillomania._ Even today as an adult, she has scars on her legs from pulling out her hair. 

She did all of this to try and cope with her intense anxiety. But, unsurprisingly, these coping strategies didn't work. Instead, things got worse. She started experiencing _disassociation_ — a terrifying symptom of anxiety that makes sufferers feel detached from their surroundings. 

When Bella experienced disassociation, the world around her looked horribly distorted. Colors seemed garish, and it felt like there was a layer of bubble wrap between her and reality. Sometimes, when the disassociation was really bad, she couldn't recognize her own face in the mirror. The condition made everything seem staged and fake — as if her friends and family were actors in a play. 

By the time Bella was a teenager, she was having panic attacks, too. 

In an attempt to feel safe, she started avoiding places that made her panic. But as the years went by, this meant she couldn't visit most of the city where she lived, or go to her local stores, or even visit her nearest park. By the time she got married, Bella was avoiding everything — from planes to freeways to elevators to the subway. 

In the next blink, we'll take a closer look at the illnesses that were driving Bella's isolation.

### 3. As Bella found out, anxiety disorders are serious illnesses with both mental and physical symptoms. 

What does anxiety feel like? If you haven't experienced it yourself, you might think you have a pretty good idea. When Bella told people about her mental health issues, they would often say, "I totally get it, I'm a worrier too!" These people meant well, but they actually had little understanding of how debilitating anxiety can be. 

**The key message here is: a** **s Bella found out, anxiety disorders are serious illnesses with both mental and physical symptoms.**

So what does it really mean to suffer from anxiety? 

Let's start by taking a look at _Obsessive Compulsive Disorder,_ otherwise known as OCD. This anxiety disorder is often associated with keeping things overly neat. But this devastating condition actually has nothing to do with making sure your belongings are perfectly aligned. 

In fact, OCD starts with distressing thoughts. A mother suffering from OCD might suddenly think, "What if I killed my daughter?" Now, although thoughts like these are disturbing, they are not abnormal. In fact, studies show that all of us experience random negative thoughts like this from time to time. 

The difference is how we deal with them. Someone without OCD will just think, "Wow, that was a weird thought" and move on to thinking about something else. But for the OCD sufferer, the disturbing thought becomes stuck in her mind. She will spend hours worrying about whether she really could hurt her child. 

Sometimes, the sufferer develops compulsive behaviors, too __ — believing that these acts are the only way to stop bad thoughts from coming true. Bella used to believe that the only way to prevent her mom from dying was to turn off light switches in a particular way. This led to Bella spending hours switching lights on and off until she felt she had done it right. 

_Panic disord_ er is another anxiety condition that has terrifying consequences for the sufferer. This disorder relates to the _fight or flight response_ — triggered when your body senses an immediate threat. When this happens, adrenaline is released into your bloodstream, which speeds up your reaction time and makes you stronger and quicker. 

But panic disorder triggers this adrenaline response at inappropriate times. Bella could be in a supermarket, or on a bus, when her body would start sending signals that she was under severe threat. When this happens, sufferers start feeling breathless and shaky, which leads to more feelings of dread and panic. In Bella's case, she sometimes felt like she was about to die during an especially bad panic attack.

### 4. Bella decided to stop running away from her problems, and just run instead. 

Let's get back to Bella's story. By the time her marriage ended, Bella was suffocating in her narrow cage of anxiety. She had no confidence, no independence, and now she had no husband. It seemed like things couldn't get much worse. But there was a silver lining: the breakup was painful, but it also made her reevaluate her life. 

**The key message in this blink is: Bella decided to stop running away from her problems, and just run instead.**

After a few days of crying and drinking too much wine, she decided to do something different. Instead of spending the evening slumped in front of the television, she went for a jog. She still doesn't really know how she came to this decision — it just felt like the right thing to do. 

She was too nervous to go to a gym or a park, so she found a secluded alleyway close to her home instead. She only managed to run for three minutes that first evening, and she did a lot of walking in between. But afterwards, she noticed something remarkable: she hadn't cried for a whole quarter of an hour. 

Bella started returning to that alleyway every night to practice jogging. In the beginning, her runs were short, slow, and utterly exhausting. Yet, she carried on. Two weeks later, she found the confidence to leave the alley and started running on the streets around her home. 

During those early jogs, she had a realization. When she ran, she felt less sad, and her mind became quieter. For those few minutes of physical exertion, she wasn't thinking about her divorce or her husband dating other people. In fact, she wasn't thinking about very much at all. After years of her brain tying itself up in knots with scary, intrusive thoughts, this quietude was a huge relief. 

And it wasn't just her bad thoughts that were disappearing — her panic attacks were receding, too. 

Soon, Bella was jogging through busy markets and crowded streets — places she'd spent years avoiding. If she'd been walking, these areas would have triggered the familiar feelings of doom and dread. But miraculously, when she ran through them she felt ok. There was no room in her mind for panic anymore. Instead, she focused on her feet hitting the sidewalk, on the people she had to avoid hitting, and the ache in her legs.

Bella's anxiety was no longer in charge. And it felt great.

> _"My feet were in control and I was running purposefully, not running away."_

### 5. Running helped Bella feel better – and it can do the same for you. 

Now, six years after her divorce, Bella runs for at least an hour every morning. This might sound like a chore, but Bella firmly believes that this simple exercise routine has saved her life. Not only have her panic attacks disappeared, but she's also happier and more confident. She's even gotten married again. 

When she talks about how jogging changed her life, some people are cynical. Some say she might have gotten better regardless. But science suggests that exercise _does_ have a real and beneficial impact on mental health. 

**The key message here is: running helped Bella feel better — and it can do the same for you.**

So why _is_ exercise so good for the mind? 

Well, it has a lot to do with a hormone called _cortisol_. Cortisol is a stress hormone that gets released when your body's fight or flight response is triggered. Scientists have found that when you exercise, your cortisol levels drop, leaving you feeling less stressed afterward. 

Exercise also encourages anxiety sufferers to think about their symptoms differently. This works because the effects of exercise on the body are actually very similar to the physical symptoms of anxiety. In both cases, you experience a racing heart, intense sweating, and bursts of adrenaline. This means that, when an anxious person adopts an exercise regime, he begins to have _positive_ associations with these sensations. Later on, when he experiences anxiety symptoms, he'll be less scared of them and won't panic as much. 

Bella has seen this benefit for herself. Before she started exercising, she would interpret a pounding heart and breathlessness as a sign that she was having a heart attack. And within a few seconds, she'd be in the midst of a full-blown panic attack. But after experiencing these same sensations when jogging, they no longer fill her with dread. 

Along with the hormonal and cognitive benefits, there's also evidence that exercise changes us on a much deeper, neurological level. One recent study looked at mice that lived in a stressful environment. Some of these mice were then allowed to exercise frequently, whereas the other mice were permitted no exercise at all. Afterward, the researchers discovered that the active, stressed mice had created new connections in their hippocampus — the area of the brain that deals with emotions. But the sedentary group hadn't. This suggests that, at a neurological level, exercise equips us to handle stress better. It certainly helped Bella.

### 6. Exercising in nature gives your mental health an extra boost – something Bella personally experienced. 

Any amount of exercise is good for your health, whether you're putting in miles on a treadmill or running around the block. But to maximize the benefits of working up a sweat, try to step outside your everyday surroundings, and head to a more natural setting instead. 

**The key message here is: exercising in nature gives your mental health an extra boost — something Bella personally experienced.**

Consider a recent study from Stanford University, in which some participants took a long nature walk, while others strolled through an urban environment. The researchers found that those who walked in nature spent less time dwelling on sad or negative thoughts. Not only that, when the nature group underwent brain scans, they were found to have less activity in the _subgenual prefrontal cortex –_ the part of the brain that's linked to poor mental health. However, these effects weren't seen in the group who walked in an urban setting. 

These findings are backed up by other recent research, which has discovered that physical activities carried out in nature _–_ like horseback riding and cycling _–_ leave people feeling less depressed and angry. 

But what can you do if you live in a town or city? Don't worry — studies have shown that you don't always need to exercise in nature to get the benefits of it. Remarkably, research by the University of Essex has found that simply looking __ at _photographs_ of lush, natural landscapes as you exercise is enough to boost your self-esteem and reduce your blood pressure! 

Now that she's aware of all the benefits, Bella tries to run in nature as much as she can. Once, after a terrible week in which a close friend had died, Bella jogged along the beautiful Irish coastline. Battered by the wind and with seagulls for company, her morbid thoughts of bereavement vanished from her mind. Suddenly, she not only saw, but _felt,_ the beauty of her surroundings — the sea, the waves, a mountain. During that run, she felt small but not insignificant. She realized that she was connected to the natural world, even if her place in it was miniscule. As she stopped to listen to the waves and feel the sun on her face, she was not dwelling on the past or worrying about the future. Instead, she was finally living in the here and now.

### 7. Many of us don’t get enough exercise, and the reasons why are complex. 

Since beginning her exercise journey, Bella has spoken to many people for whom running has made a big difference _–_ like Sara, a young mother who found that jogging eased her postnatal depression. Sara had been self-harming to try to _feel_ something again after giving birth. But running gave Sara a healthier way to do this. These days, the only pain Sara inflicts on herself involves jogging on an icy winter's day or embracing the burn after an intense exercise session. To her, running is worth it. 

Bella and Sara's stories are inspirational, but they also raise a troubling question: If running is so great, then why aren't more of us doing it? 

**The key message here is: many of us don't get enough exercise, and the reasons why are complex.**

It's a sad fact that 26 percent of all adults in England do fewer than 30 minutes of exercise a week. The stats also show that women exercise less than men. The reason for this gender imbalance may lie in the preconceived ideas we have about exercise. Research has shown that many women and girls view sports as aggressive, competitive, and incompatible with being feminine. 

Often, this female aversion to sports starts young; many teenage girls report feeling uncomfortable during mixed gender gym classes. One reason is because of the comments that boys make during these sessions. Unfortunately, these feelings of unease follow women into adulthood. When _Cosmopolitan_ magazine did a survey, they found that a majority of women felt intimidated by gyms and that some were scared of being judged by men. 

These fears around exercise are often intensified for women from ethnic minority backgrounds. **** In the UK, black and Asian women do less sports compared to their white counterparts. One study found that South Asian women worried about experiencing racism in group exercise classes, while Muslim women were anxious that they would have to exercise in groups with men present. 

As we can see, not all of the barriers that stop people from running can be broken down by willpower alone. Some of them require institutions and policy makers to think about how exercise can be made available to all. 

But if you're someone who's always shied away from jogging, remember that it's never too late to start. In fact, the oldest marathon runner in the world, Fauja Singh, only began training in his eighties. So whenever you're willing and able, jogging will still be there for you. Good luck!

### 8. Final summary 

The key message in these blinks:

**There is no magical cure for anxiety. There's no pill you can take or exercise you can do that will make sure you never feel worried or sad again. But, a running regime can help you manage your symptoms and give you the tools to lead a more fulfilling life. So lace up your sneakers, and leave your anxiety behind by making your body fly down a** **– preferably nature-filled — road.**

Actionable advice: 

**Don't do too much too soon.**

When you start running, it can be easy to overexert yourself. You may feel full of enthusiasm and push yourself to run faster and further, right out of the gate. But going hard early on can lead to injury or exhaustion — and can stop you from running altogether. A better approach is to start slowly, perhaps by using a 5k running app that helps you build up to longer distances over a period of weeks. However you do it, just remember that to go long distances in the future, you probably need to start small today. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Spark_** **, by Angie Morgan and Courtney Lynch**

Now that you've learned what jogging can do for your mental health, why not discover what it can do for the rest of your brain by checking out the blinks to _Spark_. 

You'll take a deep dive into the myriad ways that exercise boosts your performance — from your capacity to learn to your immune system to your ability to cope under pressure. Packed with real-life examples and scientific research, these blinks reveal the fundamental connections between body and mind. So for even more reasons to strap on your running shoes, head over to the blinks to _Spark,_ by Angie Morgan and Courtney Lynch.
---

### Bella Mackie

Bella Mackie is an author and journalist. Before writing her debut book _Jog On_, she wrote for the _Guardian_ newspaper, _Vogue_, and _Vice_ magazine.

