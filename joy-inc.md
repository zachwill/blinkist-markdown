---
id: 54fdc7f6626661000a670000
slug: joy-inc-en
published_date: 2015-03-11T00:00:00.000+00:00
author: Rich Sheridan
title: Joy Inc.
subtitle: How We Built a Workplace People Love
main_color: FBEE34
text_color: 706A17
---

# Joy Inc.

_How We Built a Workplace People Love_

**Rich Sheridan**

In _Joy Inc._, author Rich Sheridan shows you how a culture of joy can be the basis for any successful company. Using examples from his experience as CEO of software company Menlo Innovations, the author shows how fostering open communication, transparency and positivity can transform a workplace.

---
### 1. What’s in it for me? Learn how to use the power of joy to unleash the power of your staff. 

Who doesn't want more joy in life? Unless you're the Grinch, or prefer gloom to glee, joy is an emotion that lifts our spirits and fuels our inspiration.

So imagine just how great it would be if you could be joyful in every aspect of your life; not just on weekends but at work, too. You might enjoy your daily commute, and find working hard feels instead like play time.

Such a situation doesn't have to exist only in your imagination. Based on the practices of the highly innovative and successful software company Menlo Innovations, these blinks will show you how to turn any workplace into a joyful center where employees thrive.

In the following blinks, you'll discover

  * why a software company would support bringing babies to work;

  * what the advantages are of high-speed voice technology; and

  * how wearing a Viking helmet can help you promote openness.

### 2. Joy motivates employees. It makes them work harder and longer, and work together. 

Unless you're the grumpy Scrooge of Dickensian fame, joy is an emotion you crave and enjoy.

Being happy and full of life is better than being miserable and gloomy, isn't it? So why don't we incorporate more positive feelings — such as joy — into our workplaces?

Joy drives people to seek a higher purpose and leave a lasting impact on the world. And joy can be a valuable motivator in the business world, where it manifests as a desire to work on something larger than yourself and belong to a supportive community.

You'll find that joyful people work longer and harder than ever before to achieve such goals.

Consider the difference between the Wright brothers and Samuel Pierpont Langley. While both Langley and the Wrights were striving to create the first-ever airplane, their motivations for doing so diverged. Langley wanted fame and fortune; the Wright brothers wanted to experience the joy of flight.

And it was the pursuit of joy that inspired the Wright brothers not only to win but to soar!

Another benefit of bringing joy to the business world is that joy serves as a base for your company's culture, which helps unite and shape your team of employees.

When the author invites clients to his software company, Menlo Innovations, he asks: If half of his employees express joy for their work and the other half doesn't, which employees would the client prefer to work with? Of course, the answer is always, _the joyful ones_.

As it turns out, Menlo's clients implicitly understand that joyful employees are more productive and engaged in their work, which leads to better results.

So we've learned that joy is a way to reach new, higher goals and that joyful employees create a joy-driven culture. But what does a joyful company actually look like? Keep reading to find out.

> _"Joy is building something that sees the light of day and is used and widely adopted by the people for whom it was created."_

### 3. A flexible and fun office space means more joy overall. Nix gray cubicles; open up your space. 

Would working in a dank, windowless office make you feel joyful? Probably not.

The design and layout of your office can have a huge effect on how your employees feel when they're working.

The author observed this firsthand when he redesigned his company offices, creating a large, open space where employees could see each other and work together. This open environment is chock-full of employees, talking and laughing. In fact, it feels more like a lively restaurant than an actual office!

Menlo has made sure that its office space would be easy to redesign, too. Electrical wiring is adapted for this purpose and furniture is lightweight, making any move hassle-free.

For the author, this flexibility distinguishes the company from typical corporate spaces, where work is fixed and routine. Allowing employees to play around and have fun creates a relaxed but creatively charged environment.

Even more so, the ability to rearrange the space helps employees be more productive. At Menlo, employees have total freedom in this regard. Sometimes they move things around to ensure a team is seated together; other times, moving things around is just a way to create change for its own sake.

When you change where you sit, you literally change your point of view. Think about it: after spending ages sitting at your desk, mulling a difficult problem, say you get up and go to the kitchen. And then, when you open the fridge, ta-da! The solution comes to you.

And that's exactly why, by allowing staff the freedom to move around, Menlo creates a more productive workplace.

> _"First we shape our buildings, then they shape us." — Winston Churchill, 1943_

### 4. Open, direct communication is a crucial component of a joyful business culture. 

Open workplaces also have another benefit, in that they help facilitate communication; which, in turn, promotes joy.

This is important, as being able to communicate more directly is a great way of avoiding misunderstandings and unnecessary conflicts in the workplace.

Menlo uses a technique called _high-speed voice technology_ (or in non-Menlo speak, "direct communication").

This system provides many benefits. For one, it supports active listening. Since employees can hear many conversations around them in an open workspace, anyone can jump into any discussion if they have something to add, or just want to ask a question.

This approach does run against common practice, however. At most companies, arguments are often handled via proxy, such as through another person or by email. In conversations via proxy, it's easy to become passive-aggressive or to misread the tone of a conversation, which can lead to conflict.

Direct conversations help you avoid conflict. Being able to speak directly, and in person, promotes conviviality. Also, since you can see the other person's body language, it's easier to understand their true message.

Additionally, having conversations allows employees to build relationships, which in turn leads to improved productivity.

Employees at Menlo work together in pairs. Each pair is assigned a single computer, which makes clear communication a necessity.

This pairing approach also has a learning component. Each member has something to teach their counterpart, and also something to learn.

But it doesn't end there! To ensure relationships are built across the entire team, every week the pairings are rearranged, giving employees the opportunity to form new relationships.

> _"A noisy culture is a joyful culture."_

### 5. Shared habits and time together, walks in the park or informal gatherings, help create collective joy. 

Most families have rituals or traditions, such as Sunday dinner and holiday celebrations. We also like to keep artifacts, like photographs, that tell stories about our past. These rituals and artifacts constitute our family culture.

The same holds true for business. If you want to promote joy in your company, you have to choose your artifacts and rituals carefully.

That's why Menlo's leadership created rituals geared to promote an open, joyful culture. Instead of holding ordinary meetings, they have a _daily standup_ at 10 a.m., during which each employee team tells the others what they're working on.

To make the atmosphere more joyful, they pass around a horned Viking helmet. When it's time for a pair to present, they each grab one horn and hold the helmet between them while sharing.

Later in the day, at 3 p.m., the entire staff takes a walk together, a ritual called _walkies_. This allows employees to stretch their legs and get some air while conversing and building relationships with team members.

Additionally, the company relies on a _show & tell _ritual, which is basically a version of the classic game with a twist. A team that has completed a project watches as a client attempts to explain exactly what the team has done. This ritual keeps clients and Menlo employees on the same wavelength.

One more important aspect of Menlo's culture is the work authorization board. This workboard displays the tasks for which each pair is responsible. This visual artifact plays a major role in reducing conflict by removing ambiguity about who's doing what.

So just as you can get to know a person based on the photos he hangs on his walls, rituals and artifacts in the workplace can reveal and reproduce company values.

### 6. Get your team involved in hiring candidates that are joyful; skills are good, but attitude is better. 

A joyful company culture is made up of strong, almost familial bonds. This of course begs the question: how should you recruit new members to join your family?

The most important thing is to ensure that your interview process aligns with your core values.

In other words, it's far more important to hire based on personal traits and openness to joy than on actual skills.

When candidates apply to work at Menlo, they take a tour of the office as part of the interview process. If they appreciate the communal workplace, there's a stronger chance of a match between employee and company.

Of course, that might mean that the company has to pass over some solitude-loving geniuses, but that's okay. Because for a joyful culture, the whole must be greater than the sum of its parts.

_Kindergarten skills_, or the ability to play well with others, are another important factor of Menlo's personality-driven interview process.

To discover whether candidates have good kindergarten skills, Menlo invites all candidates to a mass interview (dubbed an _extreme interview_ ) filled with various interactions with Menlo staff and each other. This process tests the candidates' ability to joyfully interact with others.

This gets to another core component of the hiring process: Let your team take part.

At Menlo's extreme interviews, two candidates work together while one Menlo employee observes. In total, each candidate works in three different pairs with three different employee-observers. And then, at the end of the interview, employees collectively evaluate the candidates.

First impressions count, so don't forget that as you're hiring! If you aspire to a joyful company culture, that spirit should be the first thing a candidate experiences when walking through the door.

> _"Hire humans, not polished resumes."_

### 7. Creating a safe working environment is crucial in supporting experimentation and innovation. 

Could you experience joy in a pitch-black room if you're afraid of the dark? Not really.

And that's exactly why creating a safe, comfortable working environment is critical if you want employees to try new things, voice their opinions and experiment freely.

If the fear of failing is taken away, employees will take more risks, which will lead to greater innovation.

Here's where _feeling safe_ (as opposed to _being safe_ ) becomes a necessity. When people are focused on being safe, they're likely to be cautious; yet those who already feel safe will have more courage to experiment.

At Menlo, employees feel safe because they know failures won't be criticized. This allows them the freedom to take calculated risks.

This practice has led to some interesting innovations, such as _Menlo babies_. The company decided to see what would happen if an employee who had just had a child brought her into work every day.

Having a baby in the office turned out to be a perfect match for the company's joyful culture. And as an unexpected result, Menlo's "baby-friendly" culture became a unique selling point when pitching to potential clients.

But although experimenting can sometimes lead to noteworthy innovation, it can of course also lead to failure. That's why it's important to allow ideas to fail faster, as opposed to letting a failing project drag on, wasting time and money.

For instance, Menlo's Ford Everest project aimed to combine 30 different platforms in a single Web-based system. Because of the project's scale, no one wanted it to fail; so the company ploughed in more and more money ($400 million in total) to make it work.

Yet the project was doomed from the start. In the end, Menlo had to even spend an additional $200 million just to shut the project down. If only the company had simply allowed the endeavor to fail faster, it would have saved considerable time and money.

> _"Fear is the biggest killer of joy."_

### 8. Final summary 

The key message in this book:

**In business, promoting a joyful company culture, one based on openness and transparency, drives innovation and productivity. In fact, aligning everything you do with joy will make employees happier, thus motivating them to work harder.**

Actionable advice:

**Open up your workspace, open up minds.**

Tearing down physical walls effectively tears down the walls that can lock in new ideas. So get rid of gray cubicles or unnecessary offices, and let employees design their collective space as to encourage collaboration and open conversation.

**Suggested further reading:** ** _Creativity,_** **_Inc_** **by Ed Catmull and Amy Wallace**

_Creativity,_ _Inc._ explores the peaks and troughs in the history of Pixar and Disney Animation Studios along with Ed Catmull's personal journey towards becoming the successful manager he is today. In doing so, he explains the management beliefs he has acquired along the way, and offers actionable advice on how to turn your team members into creative superstars.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Rich Sheridan

Rich Sheridan is the CEO of Menlo Innovations in Ann Arbor, Michigan.

