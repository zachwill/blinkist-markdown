---
id: 5a098a54b238e1000655ceef
slug: joy-on-demand-en
published_date: 2017-11-17T00:00:00.000+00:00
author: Chade-Meng Tan
title: Joy on Demand
subtitle: The Art of Discovering the Happiness Within
main_color: EB8C32
text_color: 9E5E22
---

# Joy on Demand

_The Art of Discovering the Happiness Within_

**Chade-Meng Tan**

_Joy on Demand_ (2016) is a guide to finding inner peace, calm and joy through the ancient practice of meditation. These blinks walk you through a simple, entry-level program that will transform moments of misery into windows of hope, eventually guiding you to true, sustained happiness.

---
### 1. What’s in it for me? Learn how meditation can make you happier. 

Are you a happy, joyful person? It's perhaps not the easiest question to answer. And indeed, your answer might even change over time. But whatever your current status, there is a foolproof way to feel happier and more joyful, and it doesn't require any money or expensive tools.

You probably already know that meditation can help you manage stress and boost your performance, but in fact, it goes much deeper and can have a profound impact on your well-being and happiness, too.

In these blinks, you'll find out 

  * why most adults feel much the same as they did as teenagers;

  * that you need to familiarize your mind with feeling joy; and

  * why feeling love for a stranger will make you happier.

### 2. A person’s level of happiness tends to remain static unless improved through regular meditation practice. 

There was once a Chinese man who visited a palm reader. After examining the man's hands, the palm reader told him he was miserable and would remain that way until he turned forty. The man was thrilled by this information, assuming that he simply had to hold out till he turned forty, at which point he would finally be happy. However, he had misunderstood; the palm reader clarified that, at forty, the man would finally become accustomed to his misery.

For the man in the example, and most people worldwide, happiness remains stable over their lifetimes. Studies have even shown that humans are highly adaptable to both negative and positive life circumstances; we return to our base level of happiness, or unhappiness, with relative ease.

Just take a 1978 study by the psychologist Philip Brickman, which found that neither winning the lottery nor being paralyzed in an accident had a long-term impact on a person's happiness level. Following the initial elation or shock, a person will adapt to the momentous change, going back to feeling the way he did before the event.

Or consider a 1996 study of twins done by the psychologist David Lykken, which concluded that at least 50 percent of the human ability to remain happy depends on genetics. Surprisingly enough, external factors like money and education only account for about 3 percent of this ability.

The implication here is that, if a person is born unhappy, she simply has to suck it up. But, actually, there is something you can do about it; mental strengths, like joy and resilience, can be increased through practice.

In fact, just as working out can strengthen one's muscles, mental exercises can increase one's happiness. The author made this incredible discovery when he began meditating. He soon found that this practice is for the mind what doing sit-ups or running laps are for the body.

How come?

Well, when the mind is strengthened, it becomes more capable of achieving certain results — say, the experience of joy.

But this requires regular meditation, which can be difficult to commit to. But in the following blinks, you'll learn why it's worth it and what you stand to gain from establishing a practice of your own.

> _"I started meditating because I was miserable enough to try anything."_

### 3. Meditation has much to offer, from a renewed sense of calm to the development of emotional strength. 

If you've ever exercised, you know how difficult it can be to get started and how fulfilling it usually is once you do. Well, the same goes for meditation. This mental practice is initially challenging, but it comes with innumerable benefits, the first of which is an experience of profound calm.

In fact, through regular meditation, you can learn how to actively calm your mind. Naturally, this doesn't happen overnight. At first, your mind will be susceptible to the same incessant thoughts that it's prone to now. But as you practice, you'll be able to reliably calm your thoughts during meditation.

Soon after attaining this ability, you'll be able to put it into practice in real-life situations. For instance, after just a few weeks of meditating, one of the author's students gained the discipline to prevent himself from making snide remarks to his in-laws, dramatically improving his life.

But that's not the only benefit of meditation. It can also develop _emotional resilience_, or the ability to recover from setbacks. This skill can be especially useful when you face emotional strife.

For example, imagine you get fired. For many people, this would cause a great deal of emotional suffering that could lead to anxiety, worry and maybe some binge drinking, none of which are great coping strategies. Meditation, in contrast, will teach you to respond to distress by calming your mind. And this will allow you to pay attention to emotions as they arise and help you realize that they are all fleeting.

With this new skill, you'll soon be able to befriend those emotions, accepting them for what they are.

Once you've done this, your emotions won't yank you around as much, and you'll be left free to consider constructive next steps, like using your new free time to find another job or change careers.

So the benefits of meditation are obvious. But won't it take a long time to experience them? You'll learn more about that in the next blink.

### 4. Meditation can bring you rapid joy if you commit to your practice. 

Before we get into the mechanics of exactly how meditation works, it's important to understand how you can make it a daily practice. It may be tricky, but it's likely not as difficult as you think.

In fact, you can easily make meditation more accessible by harnessing the power of joy. Here's how:

There comes a clear turning point in every meditation practice that the author refers to as the _Joy Point_. Once this point is reached, the meditation student can easily access joy during practice, basically at will. The nice thing about the Joy Point is that, once you're there, your practice will develop its own momentum.

After all, being able to access joy through your practice will incentivize more meditation, ever increasing your ability to attain joy and launching an upward spiral of success. Because of this incredible benefit, the key is to get to this point quickly, before you get discouraged.

The Joy Point may sound far off, but the truth is, it takes much less time to reap the joyful benefits of meditation than you think. For instance, many people who participate in mindfulness-based meditation courses see positive changes within just a few weeks.

The exact figures vary from person to person, but it's clear that benefits can be attained in a relatively short period of time. Just take the author. Based on his experiences with students, he estimates that it takes one hundred hours of meditation for a practitioner to first feel benefits. An even more generous assessment is offered by the Dalai Lama himself, who says just fifty hours are sufficient to see positive changes!

And, finally, a 2007 study done by the Chinese psychologist Y.Y. Tan found that a mere one hundred minutes of meditation were sufficient to increase concentration, calm and everyday happiness.

To put it simply, meditation can make you happier — and it can do this quickly. In the next blink, you'll learn how to get started on the road to joy today!

### 5. In one breath, you can feel the life-changing benefits of meditation. 

If you're like most people, the last blink probably made you wonder how little effort you can put in while still deriving benefits from your meditation practice. It's a natural question, and the answer is sure to take your breath away.

Quite literally, a single inhalation and exhalation are enough to experience some of the benefits meditation has to offer. Don't buy it? Just try this exercise out for yourself:

With your eyes either closed or open, take a slow, deep breath. While inhaling and exhaling, pay close, yet gentle attention to the flow of your breath.

Many people find that, after just this one breath, they feel much calmer and more relaxed. But how's that possible?

It's simple, really. First, mindful breathing tends to be longer and deeper than your normal respiration, thereby activating your parasympathetic nervous system, which is responsible for regeneration and rest.

Second, when you pay attention to your breath, you bring yourself into the present moment. As a result, for a few seconds you stop worrying about what happened before or what's to come, experiencing tremendous relief.

Such simple benefits of meditation can in fact have truly life-altering effects. The ability to trigger regeneration and rejuvenation can be especially impactful.

For instance, the greatest tennis players on Earth depend on the ability to recover their bodies in the short period of time between each point, which amounts to just ten or fifteen seconds. Novak Djokovic, one of the best players of the past few decades, says that through mindful meditation during these brief interludes he can dramatically boost his performance on the court. It might just be one of the best-kept secrets on the road to becoming a Grand Slam champion.

A single breath is enough to notice results. But how can you take it further?

### 6. A meditative state can be achieved by focusing on a particular object or sensation or even by simply taking a moment for rest. 

Imagine walking into a room full of cash and being told you could take home as much money as you could carry. You probably wouldn't settle for some spare change, right? Well, the same thing goes for meditation.

The breath you learned about in the last blink is the equivalent of spare change — and you can easily have much more than that. But to advance beyond this basic point, you first need to establish the foundation of meditation: settling the mind through _anchoring_.

This describes the practice of gently returning the attention of the mind to a given object or action. Your breath is perhaps the best anchor at your disposal. After all, it's ever-present and offers a continuous, calming point on which to focus. If your thoughts begin to wonder, bring them back to your breath — the inhalation and exhalation.

That being said, you can choose any number of other anchors, like your body or something you hear, see or touch.

The idea is to get your thoughts to settle, like snowflakes in a snow globe. Once they have, you'll get a clear view of the entire scene. _This_ — clarity, perspective — is what meditation has to offer.

So anchoring is the goal, but if it's initially too challenging you can take it more slowly. Just practice resting instead.

That's right; you can rest and call it practice. However, don't be fooled. For many people, truly resting isn't actually that easy; most people aren't used to doing nothing. Resting can be a neat challenge.

When attempting this, simply allow your mind to relax. Think of soothing imagery like waves on a beach, or the fluttering wings of a butterfly. Or, if you prefer, repeat a _mantra_, a short meditative sentence, like, "I don't need to go anywhere or do anything. In this moment, all I need to do is rest."

Whichever method you initially adopt, anchoring or resting, you'll find yourself on the path to calm. In the next blink, you'll learn how to go even further, moving toward true joy.

### 7. Joy is all around you and, by noticing it in its small instances, you can take steps toward happiness. 

Pour water out onto a downward slope, and it will effortlessly flow down it, following its natural inclination. In the same way, for you to flow toward joy, you must become naturally inclined to feel happiness.

The first step is to make your mind more familiar with joy. This is a simple principle but a logical one as well. After all, when the mind becomes acquainted with the experience of joy, it simultaneously learns to notice it more easily. From there, the mind even begins to seek out joy and works to create the conditions under which it will occur.

In fact, the word "familiar" comes from the same root as the word "family." This is no coincidence, and by making joy as close and intimate to you as a family member, you can come to truly know this experience.

To create familiarity, you must first notice _when_ you're happy. As you attempt to do this, remember that feelings of joy can at first be quite fleeting and easily overlooked. For instance, when you're driving on the highway, do you notice every blue car that passes you? More likely, the blue cars blend in with those of every other color. Well, the same is true for small moments of joy, which initially are hard to pick out from the others.

Because of this, it's essential to practice noticing such thin slices of joy. Just take a moment when you get into a warm shower to feel the soothing water hit your skin. This experience is often one of pure joy, but rarely do people take the time to let it sink in. Instead, they quickly get used to it, and the feeling passes.

Cherish these small moments and keep in mind that noticing all the joy you're already experiencing is the first step toward happiness.

But this isn't the only tool at your disposal. Next, you'll learn about another practice to help increase your experience of joy.

### 8. Meditation can soothe negative emotions through love and compassion. 

Just about everybody has experienced a bad day at work. But did you know that you can turn such an experience around by employing a simple mental exercise?

Well, you can. Just by thinking thoughts of loving-kindness, you can transform your mood into one of great happiness. Here's how:

In a public space, with other people present, choose two people from the crowd and wish upon them the experience of real happiness. It sounds simple, but you'll be amazed at the results.

For instance, the author once got a group of people to perform this exercise at least once every hour during a normal workday. After the experiment, he received an email from one participant who said that, while she normally hates her work and her job, doing this exercise made that day one of her happiest in the past seven years.

This is one way that a meditative practice can make you happy. Yet another can help deter negative emotions.

The next time you feel upset, begin to calm your mind using the basic mindfulness exercises you learned in the previous blinks. Just keep bringing your attention back to an anchor, like say your breath. Once you feel calm, you can begin working to heal the emotional pain.

To do so, recognize what emotion you're feeling and then feed it its opposite. For instance, if you're feeling anger, you might imagine your rage as a monster inside of you. The more angry thoughts you have, the stronger the monster gets and, if you try to push it away, it only grows bigger.

The only thing to do in this situation is to befriend the monster and nourish it with kindness and gentle thoughts. As you do so, imagine the monster, and your anger, shrinking away and becoming your friend.

With this last piece in place, you have a comprehensive set of simple tools to maximize joy in your life. Just stay mindful, keep yourself anchored and approach bad feelings with soothing compassion.

### 9. Final summary 

The key message in this book:

**For most people, joy is a distant blip on the horizon, but it doesn't need to be. In fact, true happiness is probably closer than you believe. By developing a meditation practice, based on a consistent commitment to being present and grounded, you can find joy on Earth.**

Actionable advice:

**Don't overstrain yourself during meditation.**

It's common for people to think they should approach meditation with iron discipline, exerting tremendous effort to keep themselves on task. However, such an approach can actually be quite harmful. While some discipline is useful — say, a commitment to daily practice — it can also easily become counterproductive. So, when meditating, prevent yourself from working to attain anything. Such exertion is just another activity of the mind that will distract you from your practice. Instead, gently notice your thoughts, relax and let them go. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Book of Joy_** **by Dalai Lama & Desmond Tutu**

_The Book of Joy_ (2016) is an insightful guide to living a life free of sadness, stress and suffering. These blinks are full of actionable ways to cultivate joy for yourself and others while overcoming the obstacles that so often prevent people from finding happiness on earth.
---

### Chade-Meng Tan

Chade-Meng Tan began his career as an engineer before gradually discovering his spiritual side. As a best-selling author and the creator of a number of wildly popular mindfulness courses, he now shares his insights about joy with people around the world.

