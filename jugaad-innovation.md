---
id: 559bcba03966610007d10000
slug: jugaad-innovation-en
published_date: 2015-07-09T00:00:00.000+00:00
author: Navi Radjou, Jaideep Prabhu, Simone Ahuja
title: Jugaad Innovation
subtitle: Think Frugal, Be Flexible, Generate Breakthrough Growth
main_color: DF322C
text_color: C42C27
---

# Jugaad Innovation

_Think Frugal, Be Flexible, Generate Breakthrough Growth_

**Navi Radjou, Jaideep Prabhu, Simone Ahuja**

_Jugaad Innovation_ (2012) provides some much-needed guidance for business leaders who want to drive innovation in today's increasingly unpredictable global marketplace. These blinks look to innovators in India, China and throughout Africa for fresh, bottom-up approaches, frugal techniques and flexible management strategies to give the reader a strong understanding of jugaad principles.

---
### 1. What’s in it for me? Discover the approach to innovation that will make your company truly competitive. 

When Border Books filed for bankruptcy protection in 2011, many people wondered what had gone so terribly wrong; the once-ubiquitous bookstore chain was suddenly gone. So what had gone wrong? The answer is simple: they failed to adapt to the needs of a changing world.

Even though traditional, Western innovation practices have proven successful, many Western companies still have a long way to go when it comes to creating truly adaptable innovation processes. These strategies are vital to growth and survival in a global, and rapidly changing, market.

So how do you do it?

Enter _jugaad innovation_. Jugaad is a Hindi word meaning an ingenious and clever improvisation.

As these blinks will show you, jugaad innovators are highly adaptable, good at doing more with less, and great at challenging entrenched notions and assumptions about business.

In these blinks, you'll learn

  * why Western innovation can act as a straitjacket;

  * why so many CEOs of Western companies feel threatened by social media; and

  * why you should never forget the margins.

### 2. Outdated R&D methods and top-down approaches hinder innovation in many Western firms. 

As North American and European economies expanded during the 20th century, Western corporations became characterized by ever-increasing institutionalization. With dedicated research and development (R&D) departments and standardized business processes, emphasis was consistently placed on managing innovation.

While this structured approach has indeed proven successful, it's not particularly sustainable. Not only are institutionalized methods expensive and a drain on resources, they also lack flexibility and are insular to the point of elitism.

Consider "Six Sigma," the leading dogma for major Fortune 500 firms such as Boeing and General Electric. An integrated set of management techniques, Six Sigma is designed to reduce defects and increase efficiency in production.

Six Sigma was pioneered by Motorola in 1986, and although it works wonders in limiting production mistakes — ensuring 99.99966 percent of products will be manufactured defect-free — it hardly allows any room for change or development. In an age of diversifying customer groups and rapid technological shifts, this is a significant drawback.

Six Sigma and other standardized approaches also serve to stifle novel strategies put forth by a company's more enterprising employees, even when these unconventional methods can solve business problems in ways that traditional approaches cannot.

The reality is that many Western firms are ill-equipped to survive, let alone innovate, in the complex market environment of the future. Scarcity, diversity, hyperconnectivity and breakneck globalization all present never-before-seen challenges to businesses.

Hierarchical communication, linear planning and insular approaches to innovation suggest that these companies will struggle to adapt to major trends, such as increasing social media use.

Facebook, for example, has become a brainstorming center for many young employees, as one firm revealed to the authors. However, the company admitted that they simply couldn't manage to funnel these ideas back into its corporate R&D system.

While many organizations are at a loss as to how to incorporate future challenges into their structure, there is a solution. It's called jugaad innovation, and it's simple, yet effective. Learn more in the following blinks!

### 3. Jugaad thinking finds opportunities in limitations. 

So what is jugaad innovation? At the heart of this method is awareness: recognizing constraints and making them work for you. A jugaad innovator will respond quickly to what he sees not as obstacles, but _opportunities._

One early jugaad innovator was Tulsi Tanti. After founding a textile unit, he, like many other Indian entrepreneurs, struggled with an expensive and unreliable energy supply.

After a period of trial and error, Tanti discovered a dependable, sustainable _and_ affordable power source: wind turbines. After the initial costs of setting up the turbines were recovered, Tanti was able to fuel his business for free.

And he didn't stop there. Tanti realized that it wasn't just his company that could benefit from affordable alternative energy — there was a growing global demand for sustainable power sources, so he founded a company to meet it: Suzlon Energy.

Today, Suzlon Energy, which was established in 1995, is the world's fifth-largest wind energy supplier. The company is active in over 30 countries across six continents, and employs 13,000 people.

A proactive problem-solving attitude like Tanti's would serve many Western firms well as they face new challenges.

For example, many Fortune 500 CEOs felt threatened by the explosion of social media networks and their potential influence — they are a powerful tool that can spread rumors, damage brands and ruin, in a matter of hours, reputations that took years to build. In short, social media is seen as a threat to a stable reputation.

Procter & Gamble, however, saw the opportunities provided by social media. As early as 2000, the company embraced emerging tools as a new way to engage with customers.

Instead of using their traditional R&D teams to test new products on expensive, time-consuming focus groups, they began using social media tools to test dozens of new product ideas with customers online, and even utilized social media to turn satisfied customers into word-of-mouth marketers.

### 4. Jugaad innovators don’t just think outside of the box – they create whole new boxes. 

Every company should learn to never stand still, and always be ready to try new things. Of course, that's easier said than done!

The downfall of many Western firms is unfortunately their inability to be flexible. They tend to have a profound aversion to risk, and become complacent in their business practices. Companies take past successes and older structures as "tried and true" models and, in doing so, close themselves off to the possibility of new and innovative solutions.

One example of this is Kodak. Although Kodak had developed digital photography technology earlier than some of its competitors, it saw the new world of digital cameras as a threat to its highly profitable main business: film. Particularly because Kodak enjoyed the lion's share of the film market, the company hesitated; as a result, it was easily overtaken by its competitors in the digital photography sector.

As Malcolm Gladwell points out in _Outliers_, invention is by its very nature a disorderly process, filled with deviant behavior and ideas that drive game-changing innovation. Acting flexibly entails working through conditions of diversity, volatility and unpredictability by questioning widely held industry assumptions and improvising solutions.

Dr. V. Mohan, a globally renowned diabetes expert, runs a mobile telemedicine clinic in India that services some of the country's most remote villages. Dr. Mohan and other doctors assist their patients from their offices in Chennai using video monitors, while direct care is provided by a network of primarily urban doctors who travel in a van equipped with telemedicine technologies, which allow for diagnostic tests.

Mohan challenged the accepted notion of patients going to doctors. Instead of a doctor's office, he thought of using a van and "commuting" remotely through video feed. To access his staff through cost-effective communication capabilities, he partnered with the Indian Space Research Organisation.

This jugaad innovator was effectively able to turn the tables, and bring doctors to their patients.

> _**"** One cannot alter a condition with the same mindset that created it in the first place." - Albert Einstein_

### 5. Jugaad innovators don’t just focus on mainstream demographics – they embrace the margins too. 

Westerners tend to view products and services for marginalized segments as a social contribution, and not a profitable venture. But in fact, marginalized segments can represent a business opportunity in themselves!

Unfortunately, deeply entrenched Western business models can only adapt half-heartedly to serve those at the margins. This is also a result of Western companies' short-term outlook: their fixation on quarterly performance makes them less likely to invest the time and resources needed to design business models that can target marginal segments. They feel that the return on such investments is unlikely to materialize for a number of years. 

But focusing on marginalized target groups makes sense from both an ethical and a business standpoint.

Underdeveloped infrastructure, ineffective government and accelerated population growth have hampered access to basic services like healthcare, education and energy for millions of people throughout Africa, India and Latin America. By the same token, however, such widespread scarcity implies that millions of excluded citizens are also millions of potential customers. 

Dr. Rana Kapoor recognized this, and quit his job at a multinational company in 2004 to start an inclusive bank serving the financial needs of a broad range of consumers. His driving force was the belief that banks should serve the 600 million Indians who at the time had no access to a bank. He thus established YES BANK.

By including marginalized groups into his business model, Kapoor was able to give back to the community, and not without making a financial profit. YES BANK earns two percent over its cost of lending, whereas most banks earn between one and one and a half percent.

Today YES BANK is India's fifth-largest private bank.

### 6. Jugaad methods should complement existing Western practices. 

We've seen that jugaad innovators are good at thinking on their feet, always willing to try new things and challenge the status quo. But being a jugaad innovator doesn't necessarily mean tearing down the entire system of Western innovation. Instead, it's a matter of balance.

Jugaad innovation is most effective when integrated into traditional innovation models within complex environments that face rapid changes. So, companies must discern when jugaad innovation can best be put to use.

Jugaad works best in highly volatile settings, where product lifecycles are short, demographic patterns are shifting and where competition, as well as government regulations, are unpredictable.

Fast-paced industries that are in constant flux require flexible thinking and agility, and it is here that jugaad can deliver. 

Jugaad methods also excel in settings where industries are hungry for resources, and in sectors that are in their early stages of development. In these situations, market mechanisms and industry standards have to be established. Such contexts give innovators the chance to make the most out of very little, and seek opportunity in the face of many obstacles.

So how can you get your company started with jugaad methods? As a leader, you'll need to be able to prioritize which jugaad principles are the most applicable to your firm.

For instance, a premium retailer selling luxury items might not consider the jugaad principle of "achieving more with less," that is, seeing opportunity in adversity, or "including the margin" to be very relevant; however, "keeping it simple" could be crucial to streamlining service experience for high-end customers.

On the other hand, consumer product suppliers like Procter & Gamble or Whirlpool might choose to do more with less by creating new frugal products for customers with diminishing purchasing power.

### 7. Final summary 

The key message in this book:

**Jugaad thinking is driven by flexibility, seeing opportunity in adversity and embracing marginalized consumer bases**. **When these principles are combined with traditional Western methods, companies will discover newfound agility, resourcefulness and resilience, allowing their business to stay competitive in an ever-shifting market.**

**Suggested further reading:** ** _The_** **_Innovator's_** **_Dilemma_** **by Clayton M. Christensen**

_The_ _Innovator's_ _Dilemma_ explains why so many well-managed and well-established companies fail dismally when faced with disruptive technologies and the emerging markets they create. Through historical examples, Christensen explains why it is precisely these "good" management companies that leave big players so vulnerable.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Navi Radjou, Jaideep Prabhu, Simone Ahuja

Navi Radjou is an independent consultant on innovation and leadership based in Palo Alto, California, and is also a fellow at the University of Cambridge's Business School.

Jaideep Prabhu is a professor of Indian business at the University of Cambridge.

Simone Ahuja is the founder of a marketing and strategy consulting firm Blood Orange, based in Minneapolis and Mumbai.

© Navi Radjou, Jaideep Prabhu, Simone Ahuja: Jugaad Innovation copyright 2012, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

