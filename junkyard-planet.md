---
id: 543395f93437360008360000
slug: junkyard-planet-en
published_date: 2014-10-09T00:00:00.000+00:00
author: Adam Minter
title: Junkyard Planet
subtitle: Travels in the Billion-Dollar Trash Trade
main_color: 2D8DC8
text_color: 216894
---

# Junkyard Planet

_Travels in the Billion-Dollar Trash Trade_

**Adam Minter**

Full of visceral details and fascinating personal narratives, _Junkyard Planet_ digs into the history and current state of the waste management industry. Through a riveting tour of the sites that take care of our trash, Minter argues that the recycling and reclamation industry, despite its well-publicized environmental hazards, represents the most logical and sustainable solution to offset the insatiable consumption of the developed world.

---
### 1. What’s in it for me? Gain a deeper understanding of the recycling industry. 

Amid the hype surrounding green technology and sustainable practices, it's easy to lose sight of the sometimes gritty realities behind the flowery rhetoric.

These blinks will take you behind the scenes of today's global recycling industry. You'll learn all about the system you participate in whenever you throw a soda can into that blue recycling bin and begin to see waste management in an entirely new light.

In these blinks, you'll also discover

  * why it's cheaper for Los Angeles to ship its used paper to China for recycling than to Chicago;

  * how the recycling industry came to be the second largest employer in the world;

  * why Chinese recycling plants are at the vanguard of sustainability; and

  * why panhandlers fishing cans out of trash bins are a crucial part of a multi-billion dollar industry.

### 2. There’s a hidden, global network of recycling and reclamation sites that processes the waste we produce. 

Most of us recycle. We separate our bottles from our cans, paper from cardboard, and we set it out on the curb to be collected.

But this is just one small step in the recycling process. The role we play in the recycling industry is small at best.

In fact, the recycling industry is dependent on a chain of actors, both large and small, who collect, sort and sell scrap and trash to each other.

The bottom rung is the individual scavenger, such as the panhandler who collects Coke cans from public bins. The products of this labor are then sold, for a profit, to processing and packing institutions such as junkyards and recycling plants. These then sell the bulk objects to companies that melt and transform scrap into new metal, paper and plastic.

The volume of accumulated material increases at each stage in the process — from the panhandler's few cans to the millions of them sold to manufacturers.

In recent decades, this chain of supply has expanded beyond national borders to the point that, today, waste management takes place within a _global_ logistical network that rivals that of the manufacturing industry: there are scrap metal processors in Southern China that specialize in extracting copper from American Christmas tree lights; there are entire towns in India that manufacture belt buckles from European scrap brass; and there are Chinese scrap dealers who spend half the year touring junkyards in the American South.

But what caused this shift? On the one hand, the migration of manufacturing from the main centers of consumption and, on the other, the advent of cheap transportation options.

Just look at the way in which the waste management industry took advantage of the trade imbalance between China and the United States. Because the US imports more from China than it exports, shipping companies began offering discounts to dealers wanting to ship scrap back to China on container ships that would have otherwise been empty.

> _"Placing a box or a can or a bottle in a recycling bin doesn't mean you've recycled anything, and it doesn't make you a better, greener person: it just means you've outsourced your problem."_

### 3. Recycling is not just a matter of virtue – it also makes money. 

Have you ever wondered when and how the recycling industry came to be?

If you thought it had to do with the general population choosing to do the environmentally sound thing, think again.

The actual reason is a simple and familiar one: there was a great deal of money to be made.

Recycling is one of the simplest forms of entrepreneurship, and a recycling business can be run with little to no overhead costs.

The central principle of recycling is always the same regardless of the scale of the business.

First, to identify an in-demand resource. Second, to acquire that resource for a low price (or, in the case of scavengers, for no price at all). Third, sell it for as high a price as you can.

In short, recycling only happens because someone can profit from it.

A case in point is the American scrapyard, which was traditionally responsible for breaking apart and recycling old automobiles. This was a profitable business as long as labor was cheap and the extracted metal could be sold.

Yet, in the mid-1950s, the price of labor began to rise and steel mills began to lose interest in melting down old automobiles.

Operating at a loss, scrapyards decided to stop buying old cars from Americans. The result was that consumers began abandoning their cars en masse. In 1969, 70,000 cars and trucks were abandoned in New York City alone.

Nowadays, however, we are producing so much waste that, although the potential profit for each individual item is very small, scrap dealers are still able to make significant amounts of money because of the volume.

For example, the profit to be made from extracting copper from old wires at OmniSource (in Fort Wayne, Indiana) is just a few cents per pound. However, OmniSource processes approximately _10 million_ pounds of copper each year.

> _"The global recycling industry turns over as much as $500 billion annually — roughly equal to the GDP of Norway — and employs more people than any other industry on the planet except agriculture."_

### 4. In the recycling industry, the more profitable option is often the more sustainable one. 

There's a common misconception that recycling only benefits the planet. In fact, the more sustainable the recycling practice, the more profitable it tends to be.

First, the more thoroughly an item is recycled — i.e., the more stripped down it is to its individual components — the more profit the recycling company makes.

In Chinese cable-recycling factories, such as Qingyuan Jintian, employees are hired to snip computer mice from their cables, steel plugs from printer cables and USB plugs from USB cables before smelting the cables into copper ingots.

With the thorough processing of cables into their individual components, the factory is able to reduce the amount of contamination in its copper. This allows them to sell the metal at a higher price, and to sell the other steel components separately to their respective markets.

While this is a purely financial decision on the company's part, it also results in a more sustainable recycling procedure.

Second, the use of recycled raw materials in manufacturing is not only the cheaper option but also spares the environmental costs involved in mining those materials directly.

Recycled aluminum, produced from scrap metal by one steel company in Michigan, is made using just 8 percent of the energy required in the production of new aluminum. This process also removes the step of mining for bauxite, the raw mineral used in the production of aluminum, making it a much cheaper option.

And so, proper recycling brings together the best of both worlds: both buyers and sellers of recycled aluminum make a profit while the savings gained from using recycled metals also end up sparing the environment the negative effects of mining.

### 5. Developing countries can make use of old materials more effectively than developed ones. 

Another general rule of the global recycling industry is that developing countries can make use of old objects more effectively than developed countries.

After all, if you can afford very little, you'll tend to reuse a lot.

In San Francisco, a glass jelly jar is probably destined for the recycling bin. But in the Mumbai slums, that very same jar could well become a kitchen utensil.

The economic value of reusing an object makes a larger difference for poorer people than for the relatively wealthy. Wealthy people are more likely to divert that inherent economic value to the local recycling company instead.

The low cost of labor in developing countries, like China, India and Thailand, means that recycling factories can process objects more comprehensively. This is because workers sort through and separate recyclables by hand — a simpler and more precise method than the automated mechanical techniques used in the recycling plants of developed countries.

In addition, in developing countries, the growing manufacturing industries and accompanying demand for raw materials ensures that more parts of an object are reused.

For example, since the mid-1980s, scrap dealers in the US have been exporting old computer mainframes and telephones to China.

Why?

Although the US has a market for the copper and steel components of such electronics, it doesn't have an equivalent market for the plastic produced. China, on the other hand, does have a market for plastics — a large one, in fact.

So, forced to choose between landfilling the plastic for zero profit or exporting it to China for sale, scrappers are bound to choose the latter.

### 6. Recycling is inextricably linked to raw material demand and the consumption that fuels it. 

We often think of recycling as a way of compensating for our rampant consumption — atoning for our sins, so to speak.

But the reality is more complicated since our insatiable desire to consume and the growing recycling industry go hand in hand.

Indeed, there's a proven correlation between income, consumption and recycling rates.

Let's take a look at the recycling rates of two communities in Hennepin County, Minnesota as an example.

The first, Minneapolis, had a median household income of $45,838 in 2010, and a recycling rate that ranked 36 out of 41 communities in the county. In contrast, the affluent community of Minnetonka Beach, with a far higher median household income of $168,868, had the county's best recycling rate.

While income is not the sole determinant of recycling rates, it's hard to refute the argument that richer, higher-consuming people have a greater opportunity to generate recyclable waste than lower-consuming members.

Moreover, having the option to recycle our waste can actually cause an _increase_ in the rate of consumption.

Two experiments conducted in recent years demonstrated that people tended to use more paper when there was a recycling bin present than when there was only a trash bin.

The authors of the study hypothesized that the environmentalist associations of recycling serve as a sort of get out of jail free card, which help to assuage feelings of guilt that arise as a result of unnecessary consumption.

Despite its "green" connotations, the process of recycling is inextricable from that of consumption. The only reason you can recycle is because you've consumed, and the only reason you can consume certain products is because someone else recycled.

### 7. A better understanding of waste management allows us to demand products that are more easily recycled. 

What can we do to reduce waste?

The obvious way is to reduce our consumption. But since that's not always as easy as it sounds, the next best thing is to make the products we consume simpler to recycle.

This requires a better understanding of the way recycling works, as the following two points illustrate.

First, the most attractive, compact products are not necessarily the most easy to reuse or recycle.

One example is Apple's Macbook Air. This laptop computer flaunts a minimalist design, but its wafer-thin profile and compact construction actually make it harder to repair or dismantle into individual, recyclable components.

While customers can demand that companies design their products to be more easily recycled, they would first have to grasp that recycling works primarily through refurbishment or the sale of individual components.

Second, the more materials used in a product, the more difficult that product is to recycle.

Take a classic notebook: its various components — paper, cardboard, plastic cover, steel coil — are indeed recyclable. However, recycling this requires the extra labor of meticulously separating the components for it to be recycled thoroughly.

Producing a notebook made entirely of recyclable paper and cardboard is a far more sustainable option but that isn't immediately obvious without a comprehensive understanding of the complexities of the recycling process.

### 8. The most sustainable recycling solution for the US is to let its waste flow to countries that need it most. 

As we've seen, there are many problems with the way recycling is handled globally, especially in developing countries.

Take, for example, the town of Guiyu, China, a global recycling site for discarded electronic devices. Poisonous fumes and caustic acids from the electronic waste management process have presented significant environmental and health hazards in the town. The result is that it has become a symbol of the ills that the globalization of waste can bring.

Yet it's also true that there's no way to domestically recycle everything that is consumed in the United States while maintaining financial viability.

For the moment, the US will be able to recycle about two-thirds of the material harvested through recycling. But even then, millions of tons of material would end up in landfills because US manufacturers simply don't need additional raw materials.

Moreover, prohibiting US exports of scrap to developing nations would do nothing to improve labor conditions there. Such conditions are not the direct result of the recycling and manufacturing processes, but from the more pressing issues that face every developing country: food safety, nutrition and clean water.

If the goal is to maximize the recycling rates of Americans' waste, the best way to do this would be to let American recyclables go to wherever the demand for them is greatest.

For example, US consumers want Christmas tree lights. But the wires for those lights are manufactured in China and the manufacturer requires copper to make them. As long as there's a US demand for Christmas tree lights, Chinese manufacturers should be allowed to import used lights from the US that would otherwise be disposed of in a landfill.

### 9. The world is a better, cleaner and more interesting place for its junkyards and recycling plants. 

Despite the many problems that plague the current recycling industry, including concerns over unsafe working conditions in Chinese recycling plants, the world would be a far worse place without recycling.

Recycling on a massive scale is responsible for significant savings in electricity, labor costs and environmental damage.

According to the Institute of Scrap Recycling Industries (ISRI), in 2012, 1.53 billion cubic yards of landfill space were saved thanks to the recycling of 46 million tons of paper and cardboard; 188 billion pounds of coal and iron ore were saved by the recycling of iron and steel; and over 76 million megawatt hours of electricity were saved by the recycling of more than 5 million tons of aluminum.

Due to its profit-driven nature, the globalized recycling industry can direct consumer waste to wherever it's needed most, resulting in the most efficient reuse of resources.

For an illustrative example of how global recycling networks allowed for an efficient distribution of resources, let's return to the case of the mass abandonment of cars in the United States in the 1960s.

The solution to that problem eventually came from Thailand.

Since it had undergone a construction boom prior to the economic crisis of 2008, it became a major importer of US scrap metal as a source of cheap yet reliable material for buildings.

Due to this increased demand, prices for steel spiked in scrap yards across the US, and people brought in whatever steel they could cash in. It was thanks to this phenomenon that the US was able to finally clear out its backlog of abandoned cars.

### 10. Final summary 

The key message in this book:

**Recycling isn't just something we do to give back to the planet. It's a multi-billion dollar industry that involves countless workers across the globe. The reality of this hidden network of recycling and reclamation sites contains no ethical certainties, but it does represent the most efficient and sustainable use of the waste that we produce every day.**

Actionable advice:

**Whenever possible, reuse rather than recycle.**

The next time you think about recycling something, consider whether you can't somehow reuse it instead. This is the most sustainable path for the things we consume. For example, you could reuse a plastic water bottle by refilling it with clean tap water. You could also get more creative and turn your empty glass jars and bottles into flower vases.

**Suggested** **further** **reading:** ** _Behind_** **_the_** **_Beautiful_** **_Forevers_** **by Katherine** **Boo**

_Behind_ _the_ _Beautiful_ _Forevers_ describes life in the Annawadi slum in India, close to Mumbai's international airport. These blinks tell the story of families who live in squalid conditions making a living from collecting garbage, but still dream of a better life, even though the odds are overwhelmingly against them.
---

### Adam Minter

Adam Minter is a journalist and author based in Shanghai. He grew up in a family of scrap dealers and wrote extensively as a correspondent for various scrap industry publications before publishing _Junkyard Planet_.

