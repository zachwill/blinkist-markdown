---
id: 53e214eb6339360007490000
slug: just-babies-en
published_date: 2014-08-05T00:00:00.000+00:00
author: Paul Bloom
title: Just Babies
subtitle: The Origins of Good and Evil
main_color: F89B3C
text_color: 915B23
---

# Just Babies

_The Origins of Good and Evil_

**Paul Bloom**

_Just_ _Babies_ is about the development of morality in humans. It explores the emotions that help us to be moral, the influence family and kinship have on our moral judgements, and how society as a whole fosters morality. The book also shows what happens when we lack the emotions crucial to acting morally.

---
### 1. What’s in it for me? Change the way you think about the universality of human morality. 

Are you a good person? If your answer is yes, what makes you think so? Could it just be that your moral beliefs differ from other people's? Most of us believe that what we think is moral is a universal truth. But moral standards differ greatly throughout the world, even though important prerequisites are already hard-wired into the brains of infants.

In this book, Paul Bloom shows us how even babies possess the capacity for moral reasoning and prefer fair to unfair people, just like adults. Even though a lot of moral behavior is developed through customs and imitation, our basic emotions play a key role in moral reasoning.

Our moral feelings toward our in-group differ radically from those we have toward strangers, and our society and religion influence our moral behavior greatly.

In these blinks, you'll learn:

  * why babies prefer people of their own race,

  * how sociopaths are able to commit such gruesome crimes,

  * that being religious doesn't necessarily make you a more moral person,

  * why we don't eat our deceased, and

  * why children sometimes prefer to throw away a treat.

### 2. Some form of moral judgement seems to be innate. 

Have you ever looked at a baby and wondered what on earth was going through its mind? Although it might not seem like much, there's probably a lot more going on in there than you'd think. After all, babies already possess some innate feelings about what's right and wrong.

Even though we can't ask babies about their judgments, researchers have found ways to make educated guesses about their moral feelings.

In one experiment, researchers showed nine- and 12-month-old infants images of a ball trying to get up a hill with either a helpful character (a square gently pushing the ball uphill) or a hindering character (a triangle pushing the ball back down). Afterward, they showed a sequence where the ball would approach either the square or the triangle. Both the nine- and 12-month-olds spent a longer amount of time looking at the images when the ball approached the hindering character than when it approached the helpful character.

According to the researchers, this indicated that the children had developed expectations of how the ball would react to the hindering character and therefore looked longer when the ball acted differently from how they'd expected.

In a second experiment, the researchers added eyes to the shapes, making them look more like people, which they used to deduce that the children were making a social judgement about the hindering character.

Critics of the experiment think it only shows that babies developed expectations of what would happen, not which shape they _morally_ preferred.

Therefore the original researchers conducted a third experiment in which they used three-dimensional puppets instead of animated geometric shapes. Again, there was a ball either being helped or hindered by a puppet, and instead of simply using "looking measures," the researchers adopted "reaching measures" to determine which puppet the kids preferred.

Almost all the babies reached for the helpful puppet, demonstrating their opinion of "good" and "bad" and, in doing so, an innate form of moral judgement.

### 3. Even babies and young children respond to the pain of others. 

If you've tried caring for a young child and found it challenging, you might be surprised to know that young children are capable of supporting others.

Babies can respond to the pain of others just a few days after they're born. When one baby hears another crying, it will also start to cry — and it's not merely a reaction to loud noise.

Experiments have shown that babies cry significantly less when they hear a random computer-generated noise at the same volume as another child crying. They also cry less when they hear a recording of their own crying, showing that it's another person's suffering that elicits their response.

Young children also try to soothe others — though it's not always effective. Developmental psychologist Carolyn Zahn-Waxler found that young children who saw others acting as if they were in pain — e.g., a mother banging her knee — reacted by soothing them.

Researchers argue that this kind of soothing behavior seems to be an evolved innate ability because we also see it in our primitive ancestors. According to primatologist Frans de Waal, chimpanzees put their arm around a chimpanzee that's been attacked and pat or groom them.

Needless to say, toddlers haven't quite perfected the art of soothing others. Sometimes their instinctive reaction to others' pain is to soothe themselves — because seeing others in pain makes them feel sad.

Finally, young children try to be genuinely helpful even without being prompted.

Consider the following experiment conducted by psychologists Felix Warneken and Michael Tomasello: a toddler is in a room with his or her mother; an adult walks in with his arms full and tries to open a closet door; nobody looks at the child or asks for help. Despite that, about half of the children spontaneously stood up and opened the door for the adult, indicating that even very young children want to help out.

We can thus conclude that even babies and young children demonstrate some kind of moral behavior.

### 4. Children favor equal sharing of resources. 

When it comes to sharing resources, such as food or money, adults have many cleverly devised ways of calculating what a "fair" and moral share is for everyone. And it appears that this ability also exists in children.

Young children have an _equality_ _bias_. In other words, they like to allocate the same resources to everyone, regardless of who they are.

In one experiment, psychologists Kristina Olson and Elizabeth Spelke gave children a puppet. They had to help her to share stickers between two other puppets. In one scenario, for example, the two puppets were friends of the first puppet; in another, one puppet was a friend and the other a stranger; in a third situation, one puppet was a sibling and the other a stranger.

The researchers found that if the three year olds were given an even number of stickers, they almost always allocated an equal number of them between the two characters. This indicates that it didn't matter to the children whether the puppet was closer to the person allocating resources — they still acted upon their equality bias and shared equally among everyone.

The equality bias is so strong that children would rather throw away resources than give them out unfairly.

This was shown in another experiment in which children between the ages of six and eight were told a story about two boys who cleaned their room and could thus be rewarded with erasers, which the children are asked to give out. When there was an uneven number of erasers (five), children would give two erasers to each boy, but almost always wanted to throw the fifth eraser away.

This remained the case even when the kids were assured that neither of the boys would know about the extra erasers, and therefore the recipients couldn't show any gloating or envy. This indicates that children strongly favor equality.

### 5. Compassion is a prerequisite for moral judgements but it alone doesn’t make us moral. 

Have you ever read a newspaper and shuddered to think about the gruesome crimes some people are capable of committing? Actually, what sets these people apart might be their lack of one key human trait: compassion, or concern for the suffering of others. Without compassion, it's very hard to act morally.

One group of people who generally lack compassion are psychopaths. Psychologist Abigail Marsh recounts one anecdote about a female psychopath who was being shown pictures of faces showing different emotions and failed over and over again to recognize a fearful expression.

After a while, she finally figured it out and said, "That's the look people get right before I stab them." In conclusion, psychopaths have a very distinct core: indifference toward the suffering of others, or a lack of compassion.

Healthy people feel compassion and it's very hard for them to act against it.

In the 1930s, researchers conducted an experiment where they asked people how much money it would take for them to strangle a cat with their bare hands. The average answer was $10,000, which translates to about $155,000 in today's currency. The same people said it would only take half that amount to persuade them to have one of their front teeth pulled out.

The conclusion? Compassion carries more weight for most of us than our own pain and vanity.

But compassion alone isn't sufficient to make moral judgments. Experiments by psychologist Daniel Batson and his colleagues found that we can act compassionately and immorally at the same time.

Imagine you had in front of you a long list of people waiting for a heart transplant and you could decide who'd be the next person to get the operation. And imagine you'd heard the story of one young girl on the list in particular and you felt compassion for her — so much so that you'd prefer her to have the heart transplant than the others on the waiting list.

This is understandable in terms of compassion, but it wouldn't be moral because it would be unfair to the other patients.

> **F** **act:  

** Psychopaths cannot recognize fear in people's faces.

### 6. Empathy helps us to be moral, but it takes more than empathy to elicit moral behavior. 

Even though the words "empathy" and "compassion" are often used interchangeably, they actually have different meanings. You only show _empathy_ if you actually put yourself in the shoes of another person.

Most of us feel empathy toward others. Imagine seeing a comedian giving an awful performance on stage: nobody is laughing, the poor guy is starting to sweat, some people are getting up to leave. Are you already squirming in your seat?

Emotions are "infectious." If others feel bad, we feel bad as well: empathy is a powerful, often irresistible impulse.

Moral philosopher Adam Smith describes empathy as a process in which "we enter as it were into [another's] body, and become in some measure the same person with him." Novelist John Updike recalls his childhood memories of his sick grandmother as perfect empathy: "My grandmother would have choking fits at the kitchen table, and my own throat would feel narrow in sympathy."

These automatic impulses of empathy can actually make us kinder because they allow us to understand the situations of others and in turn help us to soothe them.

But, as with compassion, empathy isn't enough to make us moral human beings.

Take the real-world case described by philosopher Jonathan Glover, of a woman who lived near the death camps in Nazi Germany. The woman bore witness to prisoners taking several hours to die after being shot and it upset her so much that she wrote a letter stating, "I request that it be arranged that such inhumane deeds be discontinued, or else be done where one does not see it."

She had empathy enough to be moved at these inhumane acts. Yet she could still live with these murders taking place as long as she didn't have to see them, indicating that her empathy alone wasn't enough to elicit moral behavior and make her actually oppose the inhumane deeds.

### 7. Revenge and the desire for punishment are also part of our moral feelings. 

If someone cheats in a game, would you try to punish them for it? Would you spend money to punish them? One experiment shows that most people actually would.

Two economists set up the following experiment: four people play a money game in which each person gets $20. They then play several rounds and people put money in the middle at the start of each round. If everyone puts their $20 in the middle ($80), it gets doubled ($160) and split four ways so everyone gets back $40.

But if you hold back while everyone else puts their money in ($60), it gets doubled ($120) and split four ways. The others now each have $30 and you have $50. Plus, a participant can spend their own money in order to take away money from another person.

80 percent of the participants spent money to punish the people that played "unfairly" — i.e., those witholding their cash — even though the money they spent to punish another person wouldn't help them or the other players directly. This shows that people have such a strong preference for punishing unfair behavior that they'd diminish their own funds to punish those not behaving fairly.

Even very young children show the desire to punish others as part of their moral feeling. In one study with 21-month-old toddlers, they see a puppet is struggling to open a box, another puppet that comes to help it open the lid and a third puppet that comes and jumps on the box, slamming it shut. Afterward, the children are asked which puppet to reward with a treat and which puppet to punish by taking away a treat.

As predicted, the kids gave the helpful character a treat and, when asked to take away a treat, they would take it from the "bad" one.

When children already have this desire to punish at such a young age, we can assume that this is an important aspect of our moral social feelings as human beings.

> **F** **act:  

** Most people would sacrifice their own resources to punish people behaving asocially.

### 8. Disgust and an increased focus on purity make our moral judgements harsher. 

Imagine seeing people squatting on the train or in the middle of the tracks to relieve themselves. Would you be disgusted?

This was exactly what SS escorts made Jewish prisoners do by denying them access to toilets on trains. As a result, German passengers openly expressed their disgust at the Jewish prisoners, even going as far as to say they deserved their fate because they were mere animals.

Disgust can be a powerful force behind evil because it affects our compassion and normal moral judgements and makes us meaner.

In an experiment by Thalia Whatley and Jonathan Haidt, subjects were hypnotized to feel a flash of disgust whenever they saw an arbitrary word. Then, when the subjects later read stories of a mild moral transgression, the subjects who had seen that particular word judged the transgressions more harshly than those who hadn't seen it, thus indicating that disgust made their moral judgments harsher.

Likewise, an increased focus on purity tends to influence our moral judgement. In one experiment, psychologists Erik Helzer and David Pizarro interviewed students in a public hallway about their political orientation. Students who were approached while standing next to a hand sanitizer dispenser tended to state that they were generally more conservative and more disapproving of actions that could be seen as "sexually impure" than students who weren't standing close to the dispenser.

Helzer and Pizarro concluded that an increased focus on purity influences individuals' assessment of other peoples' actions by making their moral judgements harsher. This could be due to the connection between physical and spiritual cleanliness. If you see a cleanliness cue, you might be reminded of purity (both physical purity and purity of thought) and therefore react much more harshly towards transgressions of this purity.

### 9. We prefer our kin for evolutionary, moral, and unifying reasons. 

If you're a parent or have friends who are parents, you know how much parents sacrifice for their children. They feed, clothe and provide for them, they lose sleep for them, they put up with their crying and screaming — and most parents would say without hesitation that they'd die for their children.

But would we do the same for strangers?

It's evolutionarily logical that humans favor members of their family over others. Just picture our ancestors, who lived in small, hunter-gatherer societies in Savanna-like habitats. For them, behaving altruistically and morally toward family meant helping them — and indirectly their own genes — to survive and thereby increase their own evolutionary chances.

According to the author, given that relatives have the same genes as one another, "kindness to kin is, in a very real way, kindness to oneself."

It's normal to perceive emotional bonds between family members from a moral perspective. Imagine how you'd judge parents that were behaving indifferently toward their child, e.g., treating him or her like a stranger. If you're like most people, you'd judge the parents as being immoral.

Even the mere metaphor of kinship is very powerful in bringing people together. Many groups that want to strengthen their social bonds describe themselves as a family, a sisterhood or a brotherhood. Many societies also have "fictive kin," i.e., unrelated individuals are considered blood relatives.

Writer Rachel Aviv reports on the lives of homeless gay teenagers on the streets of New York who form elaborate fictive families. They have roles like mother and father, and describe their willingness to be mentors to each other. One of these boys named Ryan said the following about this fictional kinship, "A lot of us lost our biological families, so the gay family fills the void."

### 10. We prefer people in our in-group over others, which in severe cases can lead to racism. 

Even in today's society, segregation and racism are big problems. This may be because the feeling of belonging to an in-group is so important that we can easily be incited to fight for it _._

Consider Sherif and Tajfel's 1954 experiment in which they wanted to determine the bare minimum it takes to divide people.

They invited 22 fifth graders from white, middle-class families to a summer camp and split the boys into two groups that didn't know about each other's existence.

During the first week, group cohesion was fostered by playing games, and afterward the researchers brought the groups into contact and arranged tournaments between them. Each group reinforced difference from the other group by making their own flags, stealing and destroying things from the other group, and using racial epithets even though everyone was white.

In short, their relationship went from animosity to full-blown enmity. This experiment is famous for showing how easily warring communities can be created and how important it is for us to feel as though we belong to an in-group.

Even babies already have a bias towards the familiar. If you show babies pictures of people of different ethnicities, they look longer at the pictures of the individuals who are similar to the people who are nurturing them, indicating that they prefer them. This shows that children prefer what they're familiar with even before they realize whether they're "white" or "black" themselves.

But this doesn't mean that babies are born racists. These differences only become relevant if they're being mirrored by the social environment.

For example, if children go to racially mixed schools, they tend to develop less racist attitudes in general because race doesn't present a major difference. However, if in the school cafeteria all the white kids sit at one table and all the black kids sit at another one, race can become a salient factor. Only then is there a risk of children learning to be racist.

> **Fact:**  

If there's pressure against your in-group, you tend to become less tolerant.

### 11. Society has great influence through its customs on what we think is moral or fair. 

Do you think it's OK for a husband to rape his wife? Or for parents to beat their children? While we might unhesitatingly say those things are morally wrong today, just 100 years ago most people considered them normal and lawful behavior.

So what's changed since then? The customs of our society, which greatly influence our moral behavior.

For example, the custom of tipping is purely altruistic in the sense that you don't expect to be repaid for this kindness, especially if you're tipping in a city where you don't live. Still, it's considered normal to tip for good service. In some countries, it's even customary to tip if the service wasn't very good at all.

Yet for most people, tipping isn't a thoughtful act of kindness but something automatic: we calculate the tip and leave because it's what's expected of us, i.e., it's the custom.

Logically, then, different customs create different feelings about morality.

Herodotus, an ancient Greek historian, explains how the Greeks, who burned the bodies of their dead fathers, were brought together with Indians, who ate the bodies of their dead fathers. Each group was horrified by the actions of the other group because they thought their custom was the only acceptable way to treat the deceased. Neither group had ever thought of treating the dead any other way!

Herodotus calls custom "king of all," arguing that this example demonstrates how "one can see what custom can do."

That's because customs not only influence our behavior, but perpetuate themselves through imitation, passed down from adults to children.

The author cites one experiment in which children played a bowling game where they could win tokens that they could then trade in for prizes. Before playing, they watched an adult play and donate some of his tokens into a charity jar. The more the adult donated, the more the kids donated later.

### 12. Religious belief doesn’t necessarily make us more moral. 

Many people, especially in the United States, don't think it's possible to be a good and moral person without believing in God. Others, like Christopher Hitchens, think that religion is "violent, irrational, intolerant, allied to racism and tribalism and bigotry."

In actuality, however, it's going to church, not religiosity itself, that's linked to moral behavior. What's the difference? The social aspect: people involved in a religious community tend to donate more money and time to charity.

In other words, it's possible to not believe in a god but be active in a religious community and display more moral behavior through charity than a highly religious person who spends all day praying by him- or herself.

But religious attendance can also have moral downsides.

Psychologist Jeremy Ginges found a strong relationship between religiousness and support for suicide bombing among Palestinian Muslims. Again, the key factor here was a religious community, not religious beliefs.

Also, among groups like Indonesian Muslims, Mexican Catholics, British Protestants, Russian Orthodox, Israeli Jews and Indian Hindus, the frequency of religious attendance (but not of prayer!) is linked to responses such as, "I blame people of other religions for much of the trouble in this world."

This means that the more frequently people attend religious services, the more likely it is that they will be hostile and judgemental in their attitudes toward other religions.

Finally, religious belief doesn't cause moral belief: it merely reflects it.

In his book _The_ _Evolution_ _of_ _God_, Robert Wright states that most people who adhere to monotheistic religions become less accepting of people of other faiths in the face of an external threat, e.g., a war. In these cases, they feel they don't need to behave morally towards outsiders and tend to justify their bad moral behavior through their faith.

But if there are no threats, the moral circle expands and they're more likely to find the understanding and tolerant sides of their beliefs. Therefore people's interpretation of their religious belief changes if the conditions of the community change.

### 13. Final summary 

The key message in this book:

**Even** **though** **a** **lot** **of** **our** **moral** **behavior** **is** **learned** **during** **childhood,** **babies** **already** **possess** **innate** **traits** **that** **are** **key** **for** **moral** **development.** **And** **although** **we're** **hard-wired** **for** **certain** **moral** **judgements,** **what's** **considered** **moral** **and** **what** **isn't** **differs** **all** **over** **the** **world** **and** **changes** **over** **time.**

Actionable advice:

**Broaden** **your** **horizons.**

Next time you unquestioningly adhere to a cultural norm, instead of thinking, "That's just the way we do things," challenge yourself to think, "That's just what our tribe happens to do now." This will make you more open toward other societies' morals, and fosters an understanding between cultures.

**Get** **to** **know** **the** **people** **who** **aren't** **in** **your** **immediate** **in-group.**

Instead of always hanging with the same social circles, try to spend time with different people every once in a while. When you learn how they see the world, it might make you a more tolerant and open person.
---

### Paul Bloom

Paul Bloom is a professor of psychology and cognitive science at Yale University. In 2003 he won the Stanton Prize from the Society for Philosophy and Psychology for outstanding, early-career contributions to interdisciplinary research. His other bestsellers are _How_ _Pleasure_ _Works_ (2010) and _Descartes'_ _Baby_ (2004).

