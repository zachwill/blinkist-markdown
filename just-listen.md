---
id: 54198b156439610008530000
slug: just-listen-en
published_date: 2014-09-18T08:00:00.000+00:00
author: Mark Goulston
title: Just Listen
subtitle: Discover the Secret to Getting Through to Absolutely Anyone
main_color: DD2C48
text_color: C42740
---

# Just Listen

_Discover the Secret to Getting Through to Absolutely Anyone_

**Mark Goulston**

_Just_ _Listen_ (2009) combines time-tested persuasion and listening techniques with new methods to help you get your message across to anybody. By learning how to be a better listener, how the brain works and how people think, you'll be able to motivate people to do what you want because you'll better understand their needs.

---
### 1. What’s in it for me? Learn how to become an effective communicator. 

In nearly every kind of relationship and across all professions, we come into situations where we need to motivate people to do something, act a certain way or even just get them to listen to us. Unfortunately, in spite of how often we're in these situations, we're generally not very good at persuading people to do what we want.

What we want is for people to "buy in," i.e., become invested in us and thus be open to what we have to say. However, because we're often too caught up in ourselves and our own problems, we lose the ability to communicate well — and effectively shoot ourselves in the foot.

So how can you get people to "buy in"? You have to stop _talking_ and start _listening_.

In these blinks, you'll find out

  * how asking simple questions can get a man to step back from the ledge;

  * why the Mr. Spock in you is one of your greatest allies; and

  * why the way Colin Powell answered a sensitive question earned him so much respect.

### 2. Listening is a key to overcoming resistance and initiating progress. 

Have you ever stopped to consider the rhythm of your everyday conversations? If so, you might have found that you approach them as if they were all rational arguments, and this can often be counterproductive.

In fact, using arguments or pressure to influence or convince others often creates resistance. This is especially true when people come to you in times of stress: they don't want advice to improve their situation — they just want to share what's going on with them.

Imagine a man named Steve is standing on the edge of a seventh-story roof, threatening to jump and kill himself. As authorities surround the building and prepare themselves for the cleanup, a negotiator, Lieutenant Williams, approaches Steve. Williams tries to convince Steve that there are other options available to him other than hurting himself, and tells him that he's there to help him get out of this terrible situation.

Unfortunately, Steve doesn't feel understood and responds angrily, rejecting Lt. Williams's help.

The problem? Williams didn't listen. Listening gives others the chance to share their feelings and concerns, which creates a space for taking the next steps and making arguments. When we feel like our concerns are being heard, it engenders a certain level of trust between us and our conversation partners.

Now imagine that another negotiator, Lieutenant Brown, arrives at the scene to talk to Steve. After listening to Steve, Brown tells him: "I bet you feel this is your only way out."

"Yes," Steve replies.

By listening, Brown is able to show Steve how he empathizes with his situation.

He asks Steve about how he lost his job, why his wife left him, and so on. Steve then starts to calm down and begins to explain his situation, while becoming more open to solutions other than killing himself.

If you want people to be open to your arguments, you have to listen first. And, as you'll discover in the next few blinks, we're biologically programmed to do just that.

> _"When you really get where people are coming from ... they're more likely to let you take them where you want them to go."_

### 3. We feel positive emotions when our emotions are mirrored back to us. 

Surely, you've heard the adage "Monkey see, monkey do," right? As it turns out, it's true!

Indeed, we are constantly _mirroring_ those around us, i.e., recognizing, acknowledging and reciprocating the feelings and emotions of the individuals we're engaged with.

Interestingly, mirroring is actually programmed into our brains: brain cells called _mirror_ _neurons_ allow us to experience what we perceive that others are feeling. Mirror neurons used to be called "monkey see, monkey do" neurons for good reason: if you've ever cringed when you saw a friend or co-worker get a paper cut — almost feeling the pain yourself — or started to tear up when you saw someone else cry, then you've experienced your mirror neurons in action.

Some scientists believe these neurons could even be the basis for human empathy. In fact, researcher V.S. Ramachandran has called mirror neurons "empathy neurons" because of the way they bring people closer together.

Mirror neurons are also what cause us to constantly try to appease those around us, satisfy others' wishes and expectations, and seek their approval.

For example, when a speaker notices that his audience has become unresponsive, constantly looking at their watches or staring off into space, his mirror neurons cause him to respond to their desires by saying: "Okay, it's time for a break."

However, when our feelings aren't met with empathy, but instead with apathy, hostility or other negative responses, we're less likely to feel connected to others.

Studies have shown that if we mirror others but aren't mirrored in return, we develop a deficit in our mirror neuron receptors. When these _mirror_ _neuron_ _receptor_ _deficits_ occur, we feel alone and disconnected.

Whether it's because we engage in so much impersonal communication via email or mobile phones or because we have less time to form connections, we no longer mirror each other the way we used to.

### 4. Listening relies on the rational – not the emotional or instinctual – part of our brains. 

Have you ever felt like you were having an argument with yourself? Like your consciousness was divided into different "yous"? Well, your brain really _is_ divided into three different thinking parts, or layers, and they all experience and react to the world differently.

Our _reptilian_ layer is very primitive, and is all about reacting to the immediate situation. This layer is responsible for our fight-or-flight reactions. It doesn't take the time to ponder or analyze situations: it acts.

Or: it doesn't. Sometimes the reptilian layer causes a deer-in-the-headlights reaction, i.e., we freeze up and can't act at all.

The next layer, the _mammalian_ layer, is a bit more evolved than the reptilian layer: it's in charge of our emotions and is the home of your inner drama queen. The mammalian layer is where powerful feelings arise, including emotions like anger, jealousy, love, grief, pleasure, joy and sadness.

Lastly, there's the rational, _reasoning_ layer. This layer of the brain is responsible for collecting and analyzing the data from the reptilian and mammalian layers of the brain, and developing logical next steps.

Think of this last layer as your inner Mr. Spock, i.e., the _Star_ _Trek_ character who was always careful to weigh the pros and cons of a given situation and then decide on a course of action.

Just how you have different thinking layers that influence how you react to the world, so do your conversation partners. If you want them to be receptive to what you have to say, you'll have to make sure that they're thinking with the right layer. The remaining blinks will show you just how to make sure your conversation partners have the right mindset to communicate effectively.

### 5. Make sure that you and your conversation partner are using your rational brains. 

If you want to be able to listen to and reach others, you have to get your own emotions under control first. Emotions like fear, anger or panic will hinder your ability to reason and develop nuanced strategies.

For example, former US Secretary of State Colin Powell needed to exert serious control over his emotions when he was asked in front of 8,000 people to comment on his wife's admission to a mental hospital.

Instead of getting angry, Powell took a moment to gain control of his emotions and said, "Excuse me — the person you love more than anyone is living in hell, and you don't do whatever you can to get her out? Do you have a problem with that, sir?"

His ability to remain calm added to his reputation as a leader.

Unfortunately, we won't always be able to stay in control. Luckily, when we lose our cool, we can regain control over and access to the rational layer by simply acknowledging threats or panic.

In threatening situations, the brain's Mr. Spock shuts down and passes control over to the emotional side of the brain, controlled by the _amygdala_. Feeling threatened can also trigger the fight-or-flight mechanisms that cause logical reasoning to freeze and allow your immediate emotions and instincts to take over.

However, by expressing your feelings of fear or panic aloud, you give yourself the opportunity to calm down and look for solutions. In fact, studies have shown that simply naming threats and fears cools down the amygdala, causing our reptilian brain to cede control, passing it back to our rational brain.

Knowing this, you should also give others the space they need to address their fears when things start getting out of control. This way, they can listen to your arguments again with a clear and rational mind.

### 6. Showing vulnerability is empowering and gives others the chance to listen to us. 

One of the most difficult parts of becoming a good communicator is accepting your vulnerability. However, your vulnerability is a tool: when you show vulnerable emotions, like helplessness or fear, you give others the chance to connect and respond.

As we've seen, mirroring is a critical part of identifying with others. However, others can only mirror what you show them, so if you hide your emotions, you won't be truly understood.

Imagine you're nervous about a big presentation, but you also feel ashamed of being so nervous. Your colleague makes an insensitive remark to you and you respond with anger. Because you're showing anger, you're more likely to get anger in return.

However, if you had instead shown your genuine emotions, i.e., your nervousness, your colleague probably would have been able to empathize with you and boost your confidence before the meeting.

Furthermore, giving others the opportunity to show their vulnerability enables you both to explore what's behind the emotions.

Picture the following scenario: one day at a hectic law firm, an associate breaks out into tears because that morning, as he was leaving for work, his daughter started crying and said she hated him. His boss walks by and sees him in distress. Although she would have ignored breakdowns like these in the past, she instead decides to go into his office.

She tells him that she understands how difficult it is to balance work and family, and that she's working to make the firm more family friendly. In the past, the firm's attorneys had worked so much that they hardly saw their kids — an especially difficult situation for parents with young children.

Hearing this, the associate continues to cry, and tells her that he's started smoking again and has gained weight. By allowing her employees to freely express their vulnerability, the boss showed them that she cares for their well-being, and thus earned more trust and openness from her employees.

> _"If you want to open the lines of communication, open your own mind first."_

### 7. When you level with others, they’ll be more relaxed and open for dialogue. 

If there's one thing we all love talking about, it's ourselves. And you can use this to your advantage when you want people to open up and become invested in your interaction.

Using questions to establish an atmosphere of equality with your conversation partner will create a stronger connection. One way to do this is with the _Side-by-Side_ approach, in which you ask questions during a shared moment and then follow up with more questions to deepen the connection.

Imagine a father and son in a car driving somewhere together. On what would usually be a mundane drive, this time the father surprises his son by asking which of his friends he thinks will get into trouble later in life. The son is intrigued, and answers that he thinks it'll be his friend Michael.

"Why?" his father asks. Because, his son replies, Michael's parents have split up and he's already been in trouble. The father then asks what he'd do if his friend got into trouble, and the conversation continues from there.

By asking questions that demonstrate interest rather than boring his son with questions about grades, the father got his son to open up to him about deeper issues, like friendship and loyalty.

Demonstrating real interest in others will make them feel valuable, and thus bring the dialogue to a deeper level. For example, which of the following questions do you think is a better pitch for a drug rep:

"Hi Dr X. Do you have a minute to discuss the benefits of a new drug?"

or

"Excuse me Dr. X, do you mind if I ask you something personal? I just wanted to know if you still have fun as a doctor?"

The first pitch follows a familiar pattern, while the second breaks that pattern with something unexpected: the drug rep demonstrates interest in the hard-working doctor. In summary, since the majority of our interactions follow familiar patterns, it's precisely by breaking these patterns that we can open up and deepen our connections.

### 8. The best way to make others feel understood and valued is through empathy. 

Although we've seen examples of how powerful empathy can be, showing empathy isn't always intuitive.

Here's a script you can follow that will guide you towards empathy:

First, attach an emotion to what your conversation partner is feeling. Let's use anger as an example.

Next, ask him or her if your perceptions are accurate by saying something like: "I'm trying to get a sense of what you're feeling and I think it's anger. Is that correct? If not, what _are_ you feeling?"

Then, once you've established which emotion they're feeling, ask: "How angry are you?" Be prepared for an emotional response, and allow plenty of time for their answer. Remember: this isn't about you, so don't get defensive.

Next, find out _why_ they're angry by saying something like: "And the reason you're so angry is because …?"

After they've answered, say: "Tell me — what needs to happen for that feeling to feel better?"

Finally, find out how the two of you can work together to move forward; ask: "What can I do to make that happen? What can you do to make that happen?"

By demonstrating empathy, your conversation partner will feel "felt," thus allowing you both to connect and move forward together.

Imagine two agents whose persistent bickering hurts their firm. The younger of the two is extremely gifted in acquiring new clients and brags about rubbing elbows with celebrities. The older one is great at handling these clients, and finds the other agent conceited and distracting.

The two go to a mediator, who hears them out and determines that the younger agent isn't being felt. In fact, he's just looking for some acknowledgment — he wants support, not attention.

Sure enough, it's true, and as the young agent weeps, the older one soothes him by telling him how much respect he has for him. That's why he's his partner! They both promise not to be so loud and patronizing to one another, and the firm recovers.

### 9. Final summary 

The key message in this book:

**Although** **it** **might** **feel** **counterintuitive,** **the** **best** **way** **to** **get** **people** **to** **listen** **and** **become** **receptive** **to** **your** **ideas** **is** **to** **listen** **to** **them** **first.** **Listening** **allows** **you** **to** **create** **a** **shared** **connection** **between** **you** **and** **your** **conversation** **partner** **that** **opens** **them** **up** **to** **your** **ideas.**

Actionable advice:

**Don't** **be** **afraid** **to** **be** **vulnerable.**

If you ever feel afraid, nervous or distressed about something, don't pretend you're not and try to hide it. By being honest with your emotions, you can create stronger bonds with your conversation partners and open up lines of communication, thus making everyone feel better understood. What's more, they'll respect your courage and integrity, and even find you more trustworthy.

**Suggested** **further** **reading:** **_Influence_** **by** **Robert** **Cialdini**

_Influence_ explains in detail the fundamental principles of persuasion that get us to say yes, including how they are used against us by compliance professionals like salesmen, advertisers and con artists. Knowing these principles will allow you both to become a skilled persuader yourself and to defend yourself against manipulation attempts.
---

### Mark Goulston

Mark Goulston is a psychiatrist, consultant, business coach and writer whose columns have appeared in _Fast_ _Company_ and Tribune Media Services. In addition, the Consumers' Research Council of America has named him one of America's top psychiatrists.

