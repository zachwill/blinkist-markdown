---
id: 561c1de436356400077e0000
slug: just-mercy-en
published_date: 2015-10-15T00:00:00.000+00:00
author: Bryan Stevenson
title: Just Mercy
subtitle: A Story of Justice and Redemption
main_color: 499844
text_color: 377333
---

# Just Mercy

_A Story of Justice and Redemption_

**Bryan Stevenson**

_Just Mercy_ (2014) is a walk through the American criminal justice system of the 1980s. These blinks explain how a system that is supposed to safeguard the rights of the nation's citizens became an unjust tool to mistreat and abuse the most vulnerable members of society through mass incarceration and excessive sentencing.

---
### 1. What’s in it for me? Learn about the history and development of the American criminal justice system. 

What do you know about the American criminal justice system? Have you ever visited one of its prisons? How about some hands-on experience and knowledge from a lawyer who has worked inside the American prison-industrial complex and played his part in the scenes unfurling in many of the country's courtrooms?

In _Just Mercy_, the author explains how the system worked back in the 1980s, how it has developed up until today, and especially how different societal groups, including women, children and mentally disabled people, have been subject to an unpleasant type of "special treatment."

In these blinks, you'll discover

  * how high the prison population has soared since the 1980s;

  * how the system's treatment of mentally disabled criminals has developed over the years; and

  * why being an underage criminal in 1980s Alabama was a pretty grim combination.

### 2. Since the 1980s, America’s criminal justice system has been marked by excessive punishment and mass incarceration. 

Criminal justice in America has long been a subject of public interest, prompting countless movies and TV series that detail the lives as well as work of lawyers, judges and even prisoners. But when you peel back the glossy Hollywood facade there's a stark reality with nothing entertaining about it. 

Why's that?

Well, since the 1980s, the American criminal justice system has been one of excessive punishment. That's because in the 1980s the country's courts started doling out extreme sentences for even the most minor offenses. This was especially true if the person on trial had any kind of criminal record. The result was that even committing a petty crime could land you in jail for life. 

So, while in the early years of the decade, 41,000 people in America were incarcerated at a given time for drug-related offenses, today that number stands at 500,000. This fact is especially shocking since drugs use exploded in the 1980s. It's clear that a fundamental change took place in both the sentences which were doled out and public opinion about what constituted fair punishment. 

For instance, in the 1980s the author met a woman serving a long prison sentence. Her crime?

Writing five bad checks, each for less than $150, to buy Christmas presents for her kids.

But extreme punishments bred another extreme: mass incarceration. It's pretty simple: the more people you jail for small crimes, the more crowded prisons become. And this is why America is now facing a nationwide crisis of mass incarceration. 

For example, America's prison population has risen from 300,000 in the early 1970s to 2.3 million people today. That's not even counting the additional six million who are currently on probation or parole. Statistically speaking, that means that one out of every 15 people born in 2001 will be in prison sometime during his life. 

So where did all of these _new_ criminals come from?

> _"Each of us is more than the worst thing we've ever done."_

### 3. African-Americans have been disproportionately mistreated by the country’s criminal justice system. 

Mass incarceration and excessive punishment already constitute a frightening reality, but it gets worse when you look at who's being targeted. That's because African-Americans in particular have fallen victim to the flawed criminal justice system. 

How?

Racial bias, woven deep into the fabric of American society, means that African-Americans are treated with constant suspicion. Therefore black people are much more likely to be criminal suspects than their white counterparts. So, while it's shocking that one in 15 Americans will go to prison during their lifetime, it's even more appalling that for African-Americans this ratio is one in three!

The author, an African-American, recalls an encounter he had with the Atlanta police. Here's what happened.

One night he parked in front of his house and decided to stay in his car for 15 minutes listening to his favorite band on the radio. Before he knew it a SWAT car had pulled up and he found himself staring down the muzzle of an officer's pistol. The police illegally searched his car and told him that he should be thankful for being set free. 

But the author's experience, an all too regular reality for black Americans, isn't the only way black people are mistreated by the criminal justice system: African-Americans often also receive unfair trials. So, while the author had done nothing wrong and should have had no reason to be scared, there were plenty of other people in exactly his position who had been hit with serious consequences. 

But why would African-Americans be convicted of crimes they didn't commit?

Because the American criminal justice system was making it difficult for black people to prove their innocence.

For instance, although a ruling by the Supreme Court in the 1880s found excluding jurors on the basis of race unconstitutional, juries remained all or almost entirely white well into the 1980s, a full century later. That's because courts always found a reason to deny black jurors the right to serve. This fact meant that African-Americans faced juries composed entirely of whites even in counties with majority black populations.

### 4. America’s broken criminal justice system has had serious repercussions for children. 

If what you've learned so far isn't bad enough, you've yet to hear one of the most deplorable facts about America's prison-industrial complex: it has swept up children as young as 13 years old. Although children rarely comprehend the nature and consequences of the crimes they commit, during the 1980s they were often tried as adults. 

In the 1980s, Alabama had the highest instance globally of minors sentenced to death. And even today, prosecutors in Florida are personally responsible for deciding whether a child should be tried in adult court or not, with no minimum age requirement whatsoever. 

Being tried as an adult means being punished as an adult, a fact that can have brutal consequences for child offenders. Instead of going to juvenile correctional facilities, kids as young as 13 who are convicted as adults go to real prisons where they face physical and sexual abuse. In fact, the likelihood of suffering sexual abuse in prison is five times higher for underage inmates, and the only way to avoid it is by going into solitary confinement. 

The author had a client who received a life sentence for an armed robbery and attempted homicide he committed when only 13 years old. He spent 18 years in isolation. 

And, as if life in prison isn't horrific enough for a child, the courts also readily sentenced children to death. In fact, it wasn't until 1989 that the US Supreme Court took the death penalty off the table for children under 15, and not until 2005 that they banned the death penalty for children altogether.

### 5. Women were another major casualty of the unfair criminal justice system. 

It's clear by now that the American criminal justice system, based on mass incarceration and unfair sentencing, preys on the most weak and helpless members of society. But African-Americans and children aren't the only ones being mistreated. In fact, the incarceration of women has also been rapidly escalating.

Between 1980 and 2010 the rate of incarceration for women in America increased by a whopping 646 percent. That's one and a half times higher than the rate for men. 

But this statistic isn't due to marauding bands of women terrorizing America's streets. In fact, about 60 percent of the 200,000 women serving time are doing so for drug or property-related offenses. 

Furthermore, the conditions in which female prisoners are kept tend to be quite extreme. So, while it's obvious that going to prison will never be a luxurious experience, the treatment that female inmates face is absolutely heinous. Many of them live in cramped quarters and are subjected to abuse from male guards. 

For instance, Tutwiler Prison in Alabama holds about twice the number of women it was built to house in the 1940s. Not just that, but up until the 1990s, male guards were granted access to the showers while female prisoners were bathing. As a result, countless prisoners suffered rape and sexual abuse at the hands of the guards who were meant to ensure their safety. 

Some prisoners even became pregnant, and help for them was sorely lacking, because even if a guard was accused of improper conduct multiple times, the worst punishment he was likely to face was temporary reassignment. 

But in addition to the frightening realities of sexual abuse, female prisoners were treated in ways that anyone would find grossly humiliating. For example, until 2008, many state prisons handcuffed female inmates _while they were giving birth_.

> _"Approximately 75 to 80 percent of incarcerated women are mothers with minor children."_

### 6. America’s mentally ill got caught in the catch-all net of mass incarceration. 

Another group unjustly trapped by America's criminal justice system is the mentally ill, and a major reason for their mass imprisonment was the recent closure of many mental health facilities. That's because people with mental illnesses have always been institutionalized in America, whether in hospitals or prisons. 

At the end of the nineteenth century, the mentally ill were largely incarcerated after committing crimes while sick, but the harsh conditions they faced resulted in many of them being relocated to mental health facilities. 

However, at the same time, large numbers of people were locked up in those mental institutions for noncriminal reasons, like, for example, being homosexual. As a result, between the 1970s and 1990s countless American mental institutions were closed because they had partly become prisons for the innocent.

But many of the people in these facilities were there for good reason, and when the hospitals closed, those who were genuinely mentally ill found themselves behind bars after committing crimes upon release. So today, half of all prisoners in America are mentally ill, and prisons hold three times more people with serious mental diseases than American mental health facilities do. 

But the closure of mental institutions wasn't the only catalyst for the incarceration of the mentally ill. Another issue was the criminal justice system's mishandling of them in the 1980s.

In the 1980s courts didn't give adequate weight to the fact that mentally ill defendants had impaired judgment. As a result, courts sentenced the mentally ill in the same way they sentenced everyone else, and it wasn't until 2002 that the Supreme Court banned the death penalty for the mentally ill.

Furthermore, once in prison, mentally ill inmates didn't receive the treatment that their special needs demanded. For instance, in Louisiana's Angola Prison, inmates used to be required to put their hands through their cell's bars for handcuffing before an officer could enter.

When an inmate had an epileptic convulsion and needed assistance, he was in no state to put his hands through the cell bars, and officers subdued him by spraying him with fire extinguishers.

It's clear that mass incarceration and harsh sentencing have taken their toll, but just how scarring can these practices be?

> _"Nearly one in five prison and jail inmates has a serious mental illness."_

### 7. The consequences of mass incarceration go beyond the individual prisoner and often affect their entire community. 

Given the state of mass incarceration in America, you might casually refer to a defendant who was _only_ sentenced to 15 years. But before you do so you should consider the effects that prison time can have on inmates. 

That's because simply being in prison can be a traumatic experience that changes a person for life. So, while ten years behind bars might sound reasonable for some crimes, the experience will likely be deeply damaging for the person convicted. 

Take Joe Sullivan, who was given a life-sentence without parole for a non-homicide crime committed when he was just 12 years old. In prison, Joe suffered sexual abuse that led him to attempt suicide multiple times. Eventually he developped multiple sclerosis, which confined him to a wheelchair. 

In fact, many inmates suffer treatment that's so brutal they often can't understand how they themselves once committed acts of violence. 

But prisoners aren't the only ones suffering under mass incarceration, the practice also has a catastrophic and sometimes hidden effect on their families and communities. That's because when someone is accused of a crime it affects his entire family. 

For instance, Walter McMillian was put on death row for a murder he didn't commit. When the author went to Walter's home in Monroe County to meet his wife and daughter he was greeted by over 30 family members, all of whom had been affected by Walter's conviction. 

Not just that, but harsh sentences also drastically affect communities, especially when the defendant is from a small, dense neighborhood, which is often true for African-Americans living in rural areas. While defending Walter, the author was contacted by countless people who offered him help and encouragement. They ranged from Walter's old business partners to his closest friends. In short, everyone in the community took an interest in the case. 

OK, at this point you're likely shocked by the barbarism of the criminal justice system in the 1980s, but reading on you'll see some positive changes that have occurred.

> _"An absence of compassion can corrupt the decency of a community, a state, a nation."_

### 8. The early 2000s have witnessed reforms in the American criminal justice system. 

It may seem like there's no hope for America and its failed criminal justice system, but in truth, some meaningful changes have been made. In fact, during the early 2000s, the instances of severe punishments like the death penalty and life sentences began to decline. 

From 1999 through to 2010, the number of executions carried out annually decreased by nearly 50 percent. What's more, states like New York and Maryland did away with the death penalty entirely.

But the list of reforms goes on. In 2010 the Supreme Court banned life imprisonment without parole for children convicted of non-homicide crimes, and in 2012 the same court eliminated life sentences for children without parole — even for murder. By doing so, they effectively ended the possibility of a child being sentenced to die behind bars. 

The effect?

The drop in severe sentencing has also caused a drop in overall imprisonment rates. In fact, in 2012, the number of people incarcerated in America dropped for the first time in 40 years!

But the situation is still bleak and the system will need to show even more mercy. That's because despite the decline in hard punishments, America's criminal justice system remains undeniably unjust for certain populations. 

Many people don't have the money to get the legal advice necessary for a fair trial. As a result, they fall victim to a highly prejudicial system. Despite what some might think, the fact that African-Americans, children, women and the mentally ill are disproportionately incarcerated does not prove that these groups commit more crimes than those capable of paying their way to freedom. It just proves that the justice system is predisposed to view such people as guilty unless they can retain counsels who prove otherwise.

> _"The power of mercy is that it belongs to the undeserving."_

### 9. Final summary 

The key message in this book:

**America's criminal justice system has been marred by two abhorrent practices: mass incarceration and extreme punishment. Over the past few decades, the most historically vulnerable groups in society from African-Americans to poor single mothers have faced extraordinarily excessive sentences for small crimes and sometimes for offenses they didn't even commit.**

**Suggested** **further** **reading:** ** _The New Jim Crow_** **by Michelle Alexander**

_The New Jim Crow_ (2010) unveils an appalling system of discrimination in the United States that has led to the unprecedented mass incarceration of African-Americans. The so-called War on Drugs, under the jurisdiction of an ostensibly colorblind justice system, has only perpetuated the problem through unconscious racial bias in judgments and sentencing.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Bryan Stevenson

Bryan Stevenson is a death row attorney who founded and serves as executive director of the Equal Justice Initiative, an Alabama-based nonprofit organization that represents and advocates for subjugated people. In addition to his work at EJI, Stevenson is a professor of law at the New York University Law School.

