---
id: 534ed5a365316600077d0000
slug: justice-en
published_date: 2014-04-23T13:09:43.273+00:00
author: Michael J. Sandel
title: Justice
subtitle: What's the Right Thing to Do?
main_color: B12830
text_color: B12830
---

# Justice

_What's the Right Thing to Do?_

**Michael J. Sandel**

What is justice? How can we act in a just and morally correct way? Drawing on various examples from everyday life, Michael J. Sandel illustrates how differently the idea of justice can be interpreted, for example, by philosophers like Aristotle and Kant. Over the course of _Justice_ (2009), he urges us to critically question our own convictions and societal conventions.

---
### 1. Our understanding of justice is subjective and changing constantly. 

Justice is both one of the most frequently debated and most difficult topics to grasp in philosophy, because our understanding of it is subjective, changing constantly over the course of history.

Is it right to sacrifice one person's life to prevent the death of many others? Is it fair to tax rich people to help the poor? Is abortion a human right — or murder?

The answers to these questions differ depending on the individual. Everyone views them from a unique perspective that's made up of different norms, values, experiences, and unfortunately also prejudices and resentments, all of which play a crucial role in determining our judgments.

Moreover, the history of philosophy shows us that the answers to questions of justice are always bound to the era in which they were asked.

In ancient theories such as Aristotle's, justice was closely linked to virtue and the "good life": a society is only just when it fosters and rewards the virtues of its citizens. So before we ask what is just, we have to know what constitutes a good life.

According to a more modern philosophy like Utilitarianism, justice always revolves around general well-being: justice is what increases the sense of happiness among the majority.

Other modern theories like the Libertarian philosophy see the most important part of a just society as the guarantee of freedom to every individual to live their lives according to their own rules.

### 2. We sharpen our sense of justice by exploring different philosophical perspectives. 

Although it's not possible to come up with a universal definition of justice, it makes sense to deal with different theories of justice over the history of philosophy, compare them, and weigh their strengths and weaknesses against one another.

We therefore shouldn't think of the great philosophers as antiquated thinkers, but as engaging interlocutors and advisers on practical questions of our modern, everyday situation: How should we judge progressive taxes? Am I allowed to make promises I can't keep? How can we make a solid argument for same-sex marriage? Philosophers like Kant, Aristotle and John Rawls give us answers that in turn help us find our own answers.

Bearing various theories in mind can help us sharpen our sense of justice, driving us to question our rigid positions, explore new ways of approaching complex problems and, in doing so, see them in a new light.

By asking the right questions, comparing possible answers and judging them by the different standards promoted by each of these schools of philosophy, we can develop an idea of what we think is just.

### 3. The Utilitarians: Actions are just when they promote the common good. 

The founder and most famous exponent of Utilitarianism was Jeremy Bentham (1748–1832), an English moral philosopher and social reformer. His utilitarian philosophy assumes that all people want to pursue pleasure and well-being, and to avoid pain and unhappiness. This assumption is the foundation of the Utilitarian moral standard.

According to this philosophy, actions that cause well-being or happiness are moral, whereas actions that trigger unhappiness or suffering are wrong. An important postscript: it is never one person's own happiness that counts, but the happiness of the others. A truly just action promotes the happiness of many people, not only that of an individual.

Although a sex murder, for example, might promote the well-being of the murderer — it's the end of all happiness for the victim and causes unspeakable pain and suffering for the victim's family and friends. By Utilitarian logic, the murder would be morally wrong because it didn't promote the greatest possible degree of happiness.

But things are different when we talk about a terrible dictator being murdered: according to the principles of Utilitarianism, had the assassination attempt on Hitler on July 20, 1944 been successful, it would have been moral because Hitler's death would have made it possible to save many other people's lives.

One of the biggest criticisms of Utilitarianism concerns its practical applicability. The more people are influenced by my actions, the harder it is for me to judge their future sense of happiness and pain. Decisions that only influence a small group of people ("Should I help an old man cross the street or not?") are relatively simple to judge with Utilitarianism. However, decisions that affect millions of people ("Which education policy would promote the greatest possible happiness?") have highly complex consequences. Because making a reliable prediction about how the decision will affect the final balance between happiness and pain is virtually impossible.

### 4. The Libertarians: Justice means being able to live and act however you see fit. 

The philosophy of the Libertarians is based on the principle that freedom is the greatest good we have. All other rights and obligations are either subordinate to or are derived from that principle. Our freedom may only be limited if it curtails other people's freedom.

And thus Libertarians see justice as respecting and preserving people's freedom. The consequences arising from this have a wide-reaching political and economic significance: for example, people cannot be stopped from managing their money freely. Laws that interfere with the free market violate the freedom of the individual and are thus unjust. Consequently, Libertarians speak out against taxes, social security contributions and public health insurance — which they consider an inhibition or theft.

However, they also stand for several more progressive positions: for example, they're in favor of same-sex marriage, abortion and the separation of church and state as these are all in line with the freedom of the individual.

All of these Libertarian stances are united by the thought that, as long as I don't hurt somebody else by doing something, nobody can tell me what to believe, who to love or how to do business. A society is only just when individuals are guaranteed complete freedom to live and act according to their ideas.

Economists Friedrich A. von Hayek (1899–1992) and Milton Friedman (1912–2006) are the most famous theorists of Libertarianism. Their ideas enjoyed great popularity in the 1980s, as seen, for example, under the pro-market, laissez-faire policies of the Reagan and Thatcher administrations.

### 5. Kant I: We have to do the right thing for the right reason. 

German philosopher Immanuel Kant (1724–1804) argues that an action's moral worth is based on the motives preceding it. He considers an action just when somebody does the right thing for the right reason.

Kant uses the following example to explain the question of just and moral actions.

A child walks into a grocery store and wants to buy a loaf of bread. The grocer could overcharge the child without the child noticing. But he doesn't do it — for the sole reason that it could be bad for his business if people found out that he'd swindled a child.

Is the grocer acting in a morally correct way?

Kant would say no, because the grocer is only acting out of self-interest. As a result, his actions lack moral worth. A moral action isn't only what's useful or typical.

In this way, Kant rejects the philosophy of Utilitarianism because it's based on people's calculation. In Utilitarianism it's not important that actions are made with the right intentions, but that they have the right consequences. According to Kant, this teaches us to try to always get the best out of a situation for ourselves. However, the ability to tell right from wrong only plays a minor role.

So what does Kant's objection mean for our grocer?

Simply put, his actions would be moral if he decided to sell the loaf of bread to the child for the normal price simply because it's wrong to rip people off.

### 6. Kant II: Just actions mean acting in accordance with the categorical imperative. 

What's the difference between a billiard ball and a human being?

Of course there are a million different answers to this question. But according to Kant, the most important is this: the billiard ball obeys solely the laws of physics. When we hit it, it rolls forward; if we let it fall, it falls down.

Naturally, we're subject to the laws of nature: if somebody pushes us hard, we react — for example, by falling to the ground. But unlike the billiard balls, we can make our own voluntary decisions and choose our own way — regardless of all external influences. The billiard ball only knows the laws of physics, but we human beings can make our own laws and act according to these.

Like Kant's famous categorical imperative: "Act only according to that maxim whereby you can, at the same time, will that it should become a universal law." In other words: act only on principles that you think all people should follow.

Here's one example: Should I make a promise I can't keep? Or take out a loan even if I know I can't pay the money back?

According to the categorical imperative, no. Because if everybody who needed money made empty promises, nobody would be able to trust others to keep a promise.

For Kant, the categorical imperative is a test that helps us to act in a moral and, consequently, just way. If we can't reconcile our actions with the categorical imperative, they're morally wrong.

### 7. Rawls I: Only when an original position of equality lies behind a veil of ignorance can we know what’s just. 

In order to understand the universal pillars of justice, American philosopher John Rawls (1921–2002) suggests that we perform the following thought experiment: We should ask ourselves which social principles we would live by if we existed in a hypothetical state of pure equality, behind a "veil of ignorance."

By this veil of ignorance and state of natural equality, Rawls means there would be no social classes, genders, ethnicities, political views or religious beliefs. We would all be equal: nobody would know where they're from or what role they'd have in an as-yet undiscovered social order.

What social principles would we choose under these circumstances?

To begin with, we'd rule out Utilitarianism because it could lead to oppression. Based on its logic, it would be permissible to throw certain people to the lions as long as the majority got pleasure from it.

In order to protect ourselves from discrimination or persecution in our thought experiment, we'd favor basic universal liberties, such as freedom of speech and religion.

We'd also toss out Libertarianism. Because the fear of ending up as beggars on the street in a system with unlimited freedoms where nobody was obliged to look out for us would be far more vivid than the hope of getting as rich as Bill Gates.

Instead, we'd tend to favor an equal distribution of income and prosperity. With one limitation: if inequality is beneficial to the community, it must also be permitted. For example, it makes sense that a doctor earns more money than a bus driver. Even Bill Gate's billion-dollar fortune could be compatible in this scheme — for example, by integrating it into a progressive tax system that pays for other people's health, education and welfare.

### 8. Rawls II: Justice means not leaving opportunities and prosperity to chance. 

The core of John Rawls' philosophy of justice builds up an argument that the distribution of income and opportunities may not be based solely on random factors.

Let's contemplate the image of a race to illustrate this thought concretely.

In a _feudal or caste system_, only noblemen can take part in the race, while everybody else is a priori excluded. Performance, talent, ability and ambition don't matter: the only deciding factor is the random luck of having been born into a noble family.

However, a _free-market economy_, as supported by the Libertarians, contains a few improvements: here, everybody is allowed to participate in the race, though runners from wealthy families who received a good education have a virtually unassailable competitive advantage over uneducated, socially disadvantaged people who are denied such privileges.

By contrast, a _fair meritocracy_ can offer other improvements: it opens up, for example, educational possibilities to everybody and makes it possible for runners from poorer families — even if they have less preparation and lower quality "equipment" — to start at the same line.

Despite the drastically fairer conditions offered by a meritocracy, we can, even in this race, confidently predict who will win: the fastest runner. The factors that make it possible for them to win — talent, luck, the day — are, in turn, determined by chance. Thus they are equally as arbitrary as being born into the noble class or a rich family.

So should we just get rid of the race? Or do we strap lead shoes onto the fastest runner?

No and no: Rawls offers an alternative that corrects the unequal distribution of talents and requirements without limiting the most talented people. He calls this alternative the _difference principle_. It means that the fastest runners are encouraged and fostered, but they have to share their winnings with the people who didn't fare as well as them.

### 9. Aristotle: In order to know what is just, we have to know its aim and purpose. 

Justice, wrote Greek philosopher Aristotle, is the highest aim we can strive towards: "Neither the evening star nor the morning star is so marvelous."

But how can we achieve this good and worthy goal?

Aristotle believed that there can be no principles set in stone that define what is just and unjust for the rest of eternity. He instead suggested that we approach the concrete questions of justice _teleologically_ (Greek: _télos_ — "purpose" or "aim"). That means that before we judge whether something is right or wrong, we first have to question the _télos_ behind the subject of a moral problem.

How do you think Aristotles would judge the following "cheerleader dilemma"?

Callie, although wheelchair bound, was a popular cheerleader who energized the crowd. But one day she was thrown out of the troupe. The reason? All cheerleaders were required to take gymnastics — and Callie couldn't because of her handicap.

Most of us don't have trouble forming an opinion here: Callie's exclusion from the troupe was unjust. Although she couldn't take gymnastics, she performed well as a cheerleader.

Aristotle wouldn't agree or disagree with us, but instead urge us to consider a deeper question: What's the _télos_ of the cheerleading institution? Is the purpose of cheerleading to arouse people's excitement? To acknowledge certain virtues like team spirit? Or does it revolve around abilities like organization, synchronicity and physicality?

We can only judge what is just and unjust in this case once we've answered these questions clearly and without bias.

### 10. Justice is built upon a politics of the common good. 

Is it unjust when same-sex couples are prohibited from getting married?

Aristotle teaches us that we would do best to answer such a question by first considering the _telos_ of marriage as a social institution.

So is marriage there for the purpose of procreation or is it instead an exclusive and loving commitment between two people?

However, there is still one problem with this method: even if we could convincingly explain that its purpose is to secure the commitment between two people regardless of their gender, we involve our own moral views: we believe that homosexual relationships are just as worthy as heterosexual relationships.

But what about people who represent a different moral position and do not believe that homosexual relationships have the same worth?

Ultimately, questions about justice are always questions about morals, evaluative norms and the individual ideas of a "virtuous life." Since there are always competing beliefs about how to live virtuously in a pluralistic, democratic society, it's impossible to find a single, universal answer.

How can we deal with these differences in opinion? And what can we do to make fewer people act on their prejudices and fears?

The answer is: with a politics of the common good that can establish an intellectually, morally and spiritually richer life in our society. It must make an effort to help citizens better understand the commitment to the common good; it must protect social practices, like teaching, learning and immigration, from market-oriented thinking; it must keep financial inequality within bounds and tax the wealthy at higher rates; and, last but not least, it must shift the public's focus towards difficult moral questions and debate them in an educated and unbiased manner.

### 11. Final Summary 

The key message of this book is:

**People have been thinking about the question of justice for millennia, and the answers have been as varied as the thinkers who came up with them. The reason? Justice means more than judging right from wrong. When we talk about justice, we have to discuss how we want to live, what worth we assign to freedom and whether certain virtues are more worthy than others.**

### 12. Questions 

This book answered the following questions:

**What is just and how do we create morals?**

  * The understanding of justice is subjective and changing constantly

  * We sharpen our sense of justice by exploring different philosophical perspectives.

**What do the great philosophers say about justice and morality?**

  * The Utilitarians: Actions are just when they promote the collective happiness.

  * The Libertarians: Justice means being able to live and act however you see fit.

  * Kant I: We have to do the right thing for the right reason.

  * Kant II: Just actions mean acting in accordance with the categorical imperative.

  * Rawls I: It is only from an original position of equality behind a veil of ignorance that we can know what's just.

  * Rawls II: Justice means not leaving opportunities and prosperity to chance.

  * Aristotle: In order to know what is just, we have to know its aim and purpose.

**What consensus can we arrive at from these different perspectives?**

  * Justice is built upon a politics of the common good.
---

### Michael J. Sandel

Michael J. Sandel (b. 1953) is an American philosopher. He studied at Oxford and has been teaching political philosophy at Harvard for three decades. His lectures on justice have become so popular that tickets for seats in his lecture hall have to be raffled off. In 2009 his lectures were documented for American television and can now be viewed online at www.justiceharvard.org.

