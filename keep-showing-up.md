---
id: 5e338d016cee070007b1e537
slug: keep-showing-up-en
published_date: 2020-01-31T00:00:00.000+00:00
author: Karen Ehman
title: Keep Showing Up
subtitle: How To Stay Crazy In Love When Your Love Drives You Crazy
main_color: None
text_color: None
---

# Keep Showing Up

_How To Stay Crazy In Love When Your Love Drives You Crazy_

**Karen Ehman**

_Keep Showing Up_ (2019) examines the differences that can divide married couples and provides inspirational suggestions for overcoming these obstacles. These techniques bring spouses closer together as well as help reaffirm their faith in Christianity.

---
### 1. What’s in it for me? Learn tips and advice for maintaining a happy and healthy Christian marriage. 

"Why is marriage so hard? What happened to the man I married? Why can't my husband do anything right?" Whether you've been married for six months or sixteen years, odds are you've had one or more of these thoughts race through your head. Why? Because no matter how much we want to believe in the romantic happily-ever-after fairy tale of love and marriage, the reality is that a successful marriage is hard work. 

Fortunately, there are strategies you can put in place right now that will help you get through the tough times and appreciate the good ones. These coping mechanisms will help you embrace each other's strengths and fundamental differences. They'll help you see your hubby through fresh eyes and remind you of why you got together in the first place. And most importantly, they can help strengthen your personal relationship with God through prayer. 

While these blinks are designed for wives in a heterosexual partnership, anyone seeking tips on keeping their marriage happy and healthy can learn from them. And please note: this advice is geared toward the average married couple experiencing the typical ups and downs of everyday life. If you're experiencing more serious issues like infidelity or physical or verbal abuse, please seek help! Reach out to a pastor, a friend, or a domestic abuse center in your community.

In these blinks, you'll learn

  * what the four different forms of love are;

  * how to focus on the positives in your marriage; and

  * why it's important to know your spouse's love language.

### 2. We don’t have realistic expectations for how hard marriage really is on a day-to-day basis. 

They say that opposites attract. But over time, those same personality quirks and different ways of looking at life can be the exact same things about our husbands that drive us up the wall! When that happens, it's natural to look at our partner and wonder, "What happened to him?" But really, we should be taking a look in the mirror and asking ourselves if our expectations for marriage were realistic in the first place.

**The key message here is: We don't have realistic expectations for how hard marriage really is on a day-to-day basis.**

When we first get engaged, we have a very sunny picture of what our lives are going to be like. We see our married friends posting pictures of domestic bliss on Instagram and Facebook. Nobody ever posts pictures of themselves arguing over dinner or struggling to get the kids ready for school. Hollywood's idea of marriage is even worse. We see bickering couples on sitcoms all the time. But those fights are played for laughs and resolved in less than half an hour. And sometimes it seems like half an hour is about how long most celebrity marriages last. Divorce is made to look like a quick fix to all of life's problems. None of these things encourage couples to stick together and work it out. This all builds up and reinforces the false ideas about marriage we had in the first place.

Another common issue is that we look at our marriage from only our own point of view. But, of course, your marriage can't just be about you. Remember in the Bible, God created Eve from Adam's rib. Not his leg or his arm. She was made from his side, showing us that they were meant to be equal partners. Both of them were necessary to make their partnership work. It's the same with you and your husband. You both have to work at having a successful marriage.

To help reframe your point of view, ask yourself this: Why did you get married? The book of Ephesians tells us what the Biblical purpose of marriage is when it says, "The two will become one flesh…I am talking about Christ and the church." This means husband and wife aren't just one physically. They're one spiritually. Marriage is meant to be an illustration of the relationship between Jesus and the church, one of pure selflessness. Your selfless, monogamous, complementary relationship with your husband demonstrates God's divine plan to the world.

Once we accept the fact that marriage is a difficult, long-term project requiring two people who are willing to work together, we can start looking at things a little differently.

### 3. "Different" is not the same thing as "wrong." 

When we first fall in love with the man we'll eventually marry, there's a rush of discovery, as we learn all about their different experiences and perspectives. But for some reason, we expect those differences to lessen over time as we become one unit. That's a mistake. We should embrace those differences and use them to examine our own preconceived notions. Just because his ideas are different, it doesn't make them wrong.

**The key message here is: "Different" is not the same thing as "wrong."**

There are all sorts of differences that can add up to misunderstandings and hurt feelings. But a lot of times, we're reading things into a situation that just aren't there. Let's say you and your husband are driving somewhere. You're chatting away about your day or a story you heard, but he doesn't seem all that engaged. You might get your feelings hurt because you think he's not interested in you. But you're really just making an assumption. Maybe he's worried about missing a turn and is focused on the road. Maybe he's got something weighing heavily on his mind. Or maybe he's just not that talkative anyway, and you shouldn't expect him to be. You need to communicate and find out what's really going on. Don't just assume the worst.

Or let's say you're having an issue with one of your kids, and you and your husband can't agree on how to handle it. This is an important area of your life, and it's natural for both of you to feel strongly that you're right and the other is wrong. But don't forget: "different" is not the same as "wrong." If you want your husband to agree with you all of the time, what do you really need him for? Your marriage should be a strong partnership between two unique individuals. The two of you together are stronger than either of you individually. 

We all know conflict can be uncomfortable. But civil disagreement can also make you stronger in the long term. When you do disagree, don't attack each other. One of the Biblical verses most often used in weddings comes from 1 Corinthians. It says, "Love is patient," and "Love is not easily angered." You may have used it in your own wedding. Listen and try to understand where your partner is coming from. Conflict can be like sandpaper, filing down your rough edges and allowing the two of you to fit together comfortably. 

Remember, marriage takes work. But if you lose the negative attitude and make your differences work for you, not against you, you'll have the foundation for a strong, healthy relationship.

### 4. Love takes different forms. Its first form is temporary and must evolve, or it will die. 

When we first get together with our boyfriend-then-husband, every day feels like an adventure. We're excited by each new discovery. Whether we're learning about each other's past or making new memories together, we're happy just to spend time with the other person. But that early infatuation is just one of the forms love takes, and it doesn't last forever. A successful marriage is built on a foundation of love that evolves over time.

**The key message here is: Love takes different forms. Its first form is temporary and must evolve, or it will die.**

Did you know that in its original Greek, the New Testament has four different words to describe love? The first one describes erotic love: passion. That's what we're feeling when we first fall in love with someone. It's exciting, but it's selfish. It's all about how the other person makes us feel, and it fades over time. 

The second one describes friendship, or what we call "brotherly love." It takes time and should be an important component of marriage. But it's also conditional. We love someone because they love us. 

The third form is familial love. It's the love that just _is_. Of course we love our parents and our kids. But there's a danger in allowing your marriage to stall out here. You can't just love your husband because you're expected to. 

The love that originates with God is the fourth form: unconditional love. God always loves us, no matter what we do. It's a purely selfless act, and that's the love we should aspire to in our marriages. Love your husband despite his faults, not because of what he's done for you lately. 

Marriage is a long-term proposition. Growth and change can be hard to measure when you're right in the thick of it. When things get hard, think back to the beginning of your relationship…and think ahead to the end. 

Remember your wedding day? You were kind and loving to your husband. You didn't avoid his touch. Remind yourself of that feeling and model your actions on it every day. Then, think ahead to your last day on Earth. What can you do to make sure you and your husband love each other unconditionally? Make decisions based on whether or not your actions will cause regret in the future. Ask yourself, "Is this fight really worth sacrificing everything we've worked so hard to create?" Will you choose to fight? Or will you choose love?

### 5. Focus on your husband's positive attributes and the many day-to-day blessings we take for granted. 

Every marriage is a unique blend of the fantastic and the frustrating. Sure, it's great that he took you out to dinner at a fancy restaurant and you both had a wonderful time. But it doesn't mean you've forgotten that argument you had over breakfast about picking up the kids from school. When things get hard, it's all too easy to dwell on the bad times. 

Instead, we need to intentionally focus on — and be truly thankful for — the good times we've had. Then, we can learn to reframe the bad times in a fresh and different light. Sound impossible? It really isn't.

**The key message here is: Focus on your husband's positive attributes and the many day-to-day blessings we take for granted.**

To begin with, take a look at your prayers. When you talk to God, are you thanking him for the blessings in your life? Or are you so busy asking for the things you haven't got that you forget to give thanks for your home? Your health. The health of your children and your family. Your friends. The food you eat — all those little things we take for granted each and every day. 

It's tempting to look at our lives and compare them to those of our friends or, even worse, those we see depicted in the media. Have you ever thought, "I wish my husband could be more like that guy?" Don't ask God to change your husband. Instead, be thankful for the husband you have. Remember the tenth commandment: "Thou shalt not covet." If you're wishing for something you haven't got, whether it's a new house or a magically transformed husband, you're guilty of coveting. 

Take a moment to really think about your husband. Think back to when you were first falling in love. Think about all those times he was there for you, for your kids, for your friends. Then, sit down and make a list of some of the things you admire most about your husband. Maybe he's got a great sense of humor, or he's always there for a friend. But don't keep it to yourself. Share that list with him. He might not know that's how you see him. Simply telling him all the things you love most about him can open the doors to intimacy and communication.

Focusing on the positives allows us to put the harder times in perspective.

### 6. We can't go back in time and recreate that first excitement of falling in love, but we can keep discovering new things about our spouse. 

Have you ever been afraid that maybe you and your husband just aren't in love anymore? It's a very common fear. But often when you're thinking that way, you're comparing your current situation to that exciting honeymoon period. Like we just learned, those early days of passion are not going to last forever. You can't turn back the clock and experience it all over again. But you can learn to recognize the signs of love and romance that have been there all along.

**The key message here is: We can't go back in time and recreate that first excitement of falling in love, but we can keep discovering new things about our spouse.**

In his book _The Five Love Languages_, Dr. Gary Chapman identified five specific love languages we use to demonstrate affection. These are: words of affirmation, acts of service, receiving gifts, quality time, and physical touch. 

When we don't speak each other's love language, we can't recognize when someone is demonstrating affection toward us. For example, let's say your dominant love language is receiving gifts. Nothing big, just tokens that show you've been thinking about him. So you're always picking up little presents for your husband but never getting any in return. But your husband's love language is acts of service. He's always doing chores without being asked, keeping your car filled with gas, little things like that. You might notice, but you don't see it as a sign of affection — the same way that those little gifts you're giving your husband don't mean as much to him.

Once we can learn to interpret each other's love language, we can see the things our partners have been doing all along to show us how much they love us.

Another problem that can bring boredom to your marriage is routine. Any marriage can fall into a routine, thanks to the daily grind of work, kids, and just plain life. It's important to break your routine once in a while, but that doesn't have to mean expensive gifts and extravagant gestures. Remember, your marriage isn't a show being performed for the benefit of others on Instagram. It's about you and your husband really connecting. 

One great way to break your routine is by embracing the thrill of discovery. Make a point to continue discovering new things about each other, to talk to and really listen to each other. Ask questions about his childhood, his favorite movie, or his favorite memories — and share your own answers to the same questions. You'd be surprised at how much you can still learn about each other.

### 7. Our difficulties are opportunities to seek strength and guidance from God. 

Sometimes it can be hard to focus on the good times and all of our husband's positive attributes. No matter how hard we try to avoid it, trouble always comes knocking at our doors. The good news is that God is in our corner at all times — especially when we're at our lowest point.

**The key message here is: Our difficulties are opportunities to seek strength and guidance from God.**

Too often, we allow our troubles to get the better of us. This happens because they seem to come out of nowhere. They knock us for a loop, and we get overwhelmed. But in the Book of John, Jesus says, "You will face trouble…but fear not, for I have overcome." He doesn't say you "might" face trouble. He guarantees that you will! It's inevitable. So stop being surprised when it arrives. 

Jesus also says there's nothing to be afraid of because he has already overcome. Instead of asking "Why is this happening to me?" ask "What do I do now?" Allow Jesus to teach you how to communicate, to listen, and to forgive.

When times are hard, it's more important than ever to allow God to enter your heart. As we discovered earlier, we shouldn't be asking God to change our husbands. No, your husband isn't perfect. He never was, but you chose to love him anyway. So, your prayers should focus on your reactions and your behavior. Ask God for the wisdom to recognize that imperfections exist and embrace your imperfect spouse, flaws and all. Think about it this way: if you had a husband who was perfect in every way and fulfilled your every need, why would you ever need to turn to God? Those flaws that make us human and drive us crazy are the exact same things that allow us to get closer to God.

When you ask God for help, he will answer. But don't expect your troubles to disappear overnight. Developing a meaningful relationship with God is a gradual, lifelong process. You'll need to be prepared for disappointment along the way, but don't give up! Pray for patience and strength and, above all, give God space to work.

### 8. The marriage covenant can be a powerful tool to make positive changes in your community. 

Remember earlier we talked about marriage being designed to be an illustration of the relationship between Christ and the church? That means that you and your husband are constantly preaching a sermon to the people around you, just by going about your lives! But to truly live that life to its fullest, why not take it a step further? 

**The key message in this blink is: The marriage covenant can be a powerful tool to make positive changes in your community.**

As we've discussed, your marriage is an equal partnership between two unique individuals. You have chosen to love each other despite your flaws. But the flip side of embracing each other's differences is celebrating each other's strengths. The two of you are a powerful team with very specific individual skill sets. You've probably already identified the areas you excel in just by divvying up your duties around the house. 

You can take it a step further by looking around and seeing where you can put those talents to use in your community. For example, let's say that you're a great cook, your husband's an avid reader, and you both love getting in the car on the weekend and exploring new places. A good fit for you might be a senior outreach center. You can help prepare meals, you can both deliver them to the homes where they're needed, and your husband can sit and read with them for a while every week. 

The point is that every community has needs that aren't being met, so be the change you want to see. Sit down with your husband and identify the areas you both feel passionately about. Wherever you see people in pain — whether it's physical, emotional, spiritual, or financial — there's a need. When you start to think about it, you'll be able to identify with people who are going through situations similar to ones you've faced yourself. That kind of personal connection will help lead you to your calling.

In your search to find something you can do together, don't forget to be supportive of each other's individual interests. You don't sacrifice your own identity just because you're married. Pursue the things that speak to you personally and encourage your husband to do the same. 

We don't always think of our marriage as a ministry. But accepting that our marriage is a powerful tool to perform God's work can be one of the most fulfilling aspects of a husband and wife relationship. Not only will it bring you closer together as a couple, it will bring you both closer to God and have a lasting impact on the lives of those around you.

### 9. Final summary 

The key message in these blinks: 

**There's no two ways about it: marriage is hard. But your differences don't need to divide you. When we decide to do the work and keep showing up, we find that our relationships with our husbands becomes stronger, more loving, and more supportive. And our relationship with God deepens right along with it.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Eight Dates_** **, by John Gottman, Julie Schwartz Gottman, Doug Abrams & Rachel Carlton Abrams**

You've just learned some techniques to strengthen your relationship with your spouse, and God, but do you want some more tips for your marriage? Learn how to further navigate the ups and downs of your relationship with _Eight Dates,_ a practical guide that tells you how you can save your relationship by going on eight specific kinds of dates. For more insight on how to deepen your bond and discuss important issues like money, sex, and trust, head on over to the blinks to _Eight Dates_.
---

### Karen Ehman

Karen Ehman is the author of 13 inspirational books, including the _New York Times_ bestseller _Keep It Shut: What To Say, How To Say It And When To Say Nothing At All_. She is a speaker for Proverbs 31 Ministries and writer for the popular bible study app First 5.

