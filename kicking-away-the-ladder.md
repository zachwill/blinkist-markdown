---
id: 548c608733343200091d0000
slug: kicking-away-the-ladder-en
published_date: 2014-12-18T00:00:00.000+00:00
author: Ha-Joon Chang
title: Kicking Away the Ladder
subtitle: Development Strategy in Historical Perspective
main_color: 5732A6
text_color: 5732A6
---

# Kicking Away the Ladder

_Development Strategy in Historical Perspective_

**Ha-Joon Chang**

_Kicking Away The Ladder_ takes a historical look at how the Western powers have grown economically and politically. It shows that the principles and tactics that have made them rich and powerful are counter to those they propose developing countries should live by.

---
### 1. What’s in it for me? Learn how developed nations gained their prosperity by acting completely against what they tell developing nations to do. 

If you switch on the news or read the newspaper, you'll find a Western politician telling those in the developing world what they need to do to become rich and peaceful. Developing nations face international pressure to establish free-market economies, democratize their governments and improve workers' rights.

These blinks show that Western leaders tend to preach one message of economic development, and practice another. Far from being the bastions of free trade and liberty, Western nations protected their economies from foreign competition and exploited their labor force. In short they built their economies by doing the "wrong" things.

High employment levels and just political rights are ubiquitous aims, though arriving at those standards can have a detrimental economic cost on developing nations. In these blinks, we will examine why fair economic and political standards can come at the cost of wealth and power, and how some countries have attempted to close the gap between fairness and prosperity.

These blinks will show:

  * why Britain employed child labor much later than you think;

  * why America wasn't always a free-trade advocate; and

  * what prompted women's right to vote in Switzerland.

### 2. The USA achieved dominant economic standing by protecting its industries. 

What does the flag of the United States stand for?

Along with liberty and democracy, one of the most common answers to this question is _free trade_ and the _free market_. The United States often represents the image of free economic practices, unfettered by controlling government interference or high tariffs.

Yet, despite this motto of free trade, the early history of the US shows it was actually governmental regulations that led to its prosperity and power.

In the late eighteenth century, after a long struggle, the United States threw off British rule to become an independent nation. The new country faced many problems: its industry was undeveloped and weak in comparison to the European powers.

The newly independent United States turned to the opposite of free trade: _protectionism._ This meant that any foreign product entering the country would be subject to a five percent customs duty. The policy kept the price of US products comparably cheaper, harboring domestic economic growth.

Protectionism continued as domestic industry grew; in fact, the customs tariff climbed. In 1812, to pay for another war with Britain, it was increased to 25 percent. When the war ended a few years later, the tariff continued to climb — to a whopping 40 percent by 1820.

Incredibly, protectionism policy continued well into the twentieth century. Before the Second World War, the US was considered to have the most protected economy in the world, second only to Russia.

Although long-term protectionism may sound counter to modern economic ideals, it was undoubtedly successful. Protected behind the customs barriers, US industry grew to become the strongest in the world.

Only when the US achieved this position of dominance did it open its borders to free trade.

### 3. Britain established economic dominance starting during the industrial revolution by protecting its industries. 

Britain's naval fleet ruled the waters in the eighteenth and nineteenth centuries, marking the small island's status in world trade.

How did this little, cold and wet island achieve such economic standing? As with the United States, the answer lies in the country's adoption of protectionism.

We have to revisit fifteenth century Britain to gain a deeper understanding of its protectionist strategy. At that time, England was a rather impoverished nation. Its main export was raw wool which it sold to the sophisticated textile industry of The Low Countries (modern day Belgium and Holland). England had very little industry of its own.

One English king, Henry VII resolved to change this. He wanted to build up England's own textile industry and he realized that protectionism was the best way. So, beginning in 1489, he initiated laws protecting domestic industry.

First, England poached skillful workers from the Low Countries who had years of textile experience. Then the export of raw wool (needed to turn into cloth) was banned, providing English manufacturing with the materials to succeed, simultaneously depriving foreign competitors of the raw material. Henry VII's new laws effectively spurred the growth of English textiles.

In fact, protectionism worked so well that Britain used it again in the eighteenth century to build its manufacturing industries.

Through trade tariffs, the government encouraged the import of raw materials like cotton and iron ore and the export of manufactured goods, such as cloth and steam engines. Its policies effectively capitalized on the low cost of abundant imported materials and the huge global demand of manufactured goods, turning the small nation into a world-leading economy.

### 4. Britain’s historical dominance over colonial interests shows how rich countries exploit poor ones. 

Why do you think some countries are rich and others poor?

The reason, according to many people, is because certain wealthy and more powerful countries are able to use their position to exploit and impoverish the others.

Is this true? Well, if you look at the historical evidence it certainly appears this way.

Let's look at one particular example from the past which displays this unfair relationship between rich and poor.

In the seventeenth and eighteenth centuries, Britain acquired colonies on the Eastern coast of North America. The British government resolved to use the resources of these colonies to make itself wealthy. The raw materials, abundant in North America, such as furs, tobacco and timber, were stripped domestically and sent for British manufacturers to use.

Then at the same time, the colonists themselves were prevented from growing their own industries. For example, Britain outlawed the construction of rolling and steel mills in America. This act forced the colonists to specialize in more primitive, less profitable products like pig and bar iron, instead of high value steel production. Of course this benefited British steel manufacturers as it limited the competition.

Thus, the colonies of North America were deliberately kept poor in order to grow the British economy.

In addition to exploiting its own colonies, Britain used its power to also dominate other nation-states, forcing them to sign humiliating, unfair treaties.

For example, by using its military might to defeat China in the Opium War (1839-1842), Britain forced the Chinese to acquiesce control of their customs duties. The British set the duties at cheap rates, allowing British goods to flood China, stifling domestic industry.

We have now seen how Western nations grew rich and powerful through exploitation and protectionism. The next blinks show how those countries coach developing nations to grow through entirely differing methods.

### 5. It’s false that democracy is responsible for Western economic power. 

What do you think makes a country prosperous?

Many will argue, _democracy_. It seems obvious: A huge proportion of the world's most successful economies are democratic. Therefore, democracy must be a key factor, right? However, looking back into the past you can see that the link between democracy and economic development is unclear.

For a start, when most _NDCs (Now Developed Countries)_ such as the USA and the UK became economically powerful, they weren't expressly democratic. 

For example, in France between 1815 and 1830, only males over 30, who paid at least 300 francs in taxes, could vote. This limited the franchise to just 0.25 percent of the total population. Then, when voting rights were expanded in 1848, only males were given the vote.

In most countries, this lack of women's suffrage was common until relatively recently. It wasn't until 1903 that Women could vote in Australia, 1928 in the UK and in Switzerland women were barred from voting until 1971.

Yet, even when the right to vote was slowly expanded in the NDCs, the democratic process was hardly open or transparent.

Take secret balloting. In order to make elections fair, who you vote for should be kept secret. Most of us take this for granted now, but in the past it wasn't so. For example in Prussia until 1919, a lack of a secret ballot left voters at risk of bribery and intimidation. In France, it was only in 1913 that a voting envelope was used to protect one's precious vote.

So what does this tell us? That developing countries should be less democratic to get better economic results? No, of course not, but we must be aware that rich nations achieved success using methods we would frown upon. This way we can judge developing nation's growth more fairly.

The following blinks offer more examples of how western nations used undemocratic methods to build their wealth and power.

### 6. The history of Western economic development is marked by property rights violations. 

How would you feel if, after spending your entire life building up a prosperous business, the government claimed it?

It's very unlikely that you would make the mistake of building a business again and society would lose its entrepreneurial spirit.

In order to avoid this situation, Western politicians and economists argue for strong _property rights._ These rights legally protect someone's property from seizure by the state. Property rights are meant to promote an entrepreneurial spirit: as entrepreneurs know that the rewards for their enterprise will never be stolen from them, they are incentivized to work hard. 

However, if we look at how the NDCs developed their economies, we see that economic development doesn't always require strong property rights.

For example, take Britain in the eighteenth century. Over this century there was a huge advance in the sophistication and size of the British textiles industry. Behind this development was actually the government's violation of property rights. Britain's leaders took the common land, considered public domain, and gave it to private landowners, who then filled it with sheep. It was this act of government "theft" that led to an increase in the supply of wool which fuelled the textile boom.

And it is not only the violation of physical property rights that can fuel economic development; stealing peoples ideas, violating their _intellectual property rights,_ can have the same effect.

When the NDCs grew economically, the theft of ideas and inventions was liberal. For example, before 1852 the British government allowed its citizens to patent foreign ideas for their own use. This meant that if a German had a great invention, a British person could steal, manufacture and sell it in Britain, under the full protection of the law.

So, although we might think property rights are crucial to economic development, the opposite has also been true.

### 7. Abuse of workers’ rights was rife in the NDCs, before regulations were established. 

Certain working practices seem diabolically inhumane: forced 15-hour work days and child labor are two, for example.

As a result, governments, charities and NGOs (non-governmental organizations) work to ensure a world free of such inhumane practices. However, although exploiting workers is condemned now, in the past it was not the case.

In fact, the NDCs actually took advantage of such practices as they were developing.

Take Britain, for example, the world's first industrial power. In the late 1700s and early 1800s, British industry was reliant on the employment of children, both in the textile mills and factories, and in coal mines.

Children working in the coal mines could be expected to work in horrific conditions for very long hours. In the 1820s, for example, it was common for children to work underground for 12 to 16 hours every day.

The early history of the United States shows another example of how child labor was central to economic life. In 1820s half the labor in the cotton industry were children under 16; and there were still 1.7 million people working in the industry by the end of the nineteenth century. 

Even well into the twentieth century, was child labor still prevalent in many Western societies. Denmark took until 1925 to establish stringent laws for worker rights.

But, why are these historic examples relevant?

If we want to see developing nations reach the heights of the NDCs, then we must realize that they are limited in their options compared to how the West built their economic wealth. They cannot lie, cheat and exploit their way to the top in such an unregulated fashion as the West did.

### 8. Contemporary developing countries are developing faster than the now developed countries at  similar stages. 

So we should now realize that the currently developing nations have to approach economic development in different ways than the NDCs did.

However, rather than something which we bemoan, we can view this challenge as a positive change. In fact, the majority of developing nations are currently far more "developed" than their Western counterparts were at the same stage in their development.

Take the institutions vital for a fair and prosperous society: the institutions of bureaucracy, welfare or democracy. In currently developing nations they are much stronger than they were in the historical NDCs.

For example, the UK income per capita in 1820 may have been twice that of modern India (adjusted for inflation), but India is far more developed in its institutions: India has universal suffrage, a central bank and strong labor regulations, none of which existed in nineteenth century Britain.

Why was institutional development so poor in the developing NDCs?

Partly it was because those who had control of the government, were also those who benefitted from the unfair system.

That's precisely why, for example, it took well into the twentieth century for Britain to ban child labor. The House of Lords, which decided policy in the nineteenth century, was dominated by rich men who owned the mines and factories that the children worked in. To ban child labor would mean that they lost a ready supply of incredibly cheap and docile labor.

Yet in developing countries, at least those with universal suffrage, such powerful forces have less control, therefore development is required for solutions — spurring greater development.

Seeing that developing nations are growing within the bounds of modern regulations makes them far more advanced than the NDCs that developed at the hands of unjust practices. Many people in these countries have rights that were unavailable to the people of eighteenth century Britain or America.

### 9. Final summary 

The key message in this book:

**When advising developing nations what to do, NDCs often conveniently forget to tell them how they got there. Yet, partially** ** _because_** **developing nations cannot grow at the hands of unregulated exploitation, they have been building their economies in more advanced ways.**

**Suggested further reading:** ** _23 Things They Don't Tell You About Capitalism_** **by Ha-Joon Chang**

In _23_ _Things_ _They_ _Don't_ _Tell_ _You_ _About_ _Capitalism_ Ha-Joon Chang destroys the biggest myths of our current economic approach. He explains how, despite what most economists believe, there are many things wrong with free market capitalism. As well as explaining the problems, Chang also offers possible solutions which could help us build a better, fairer world.
---

### Ha-Joon Chang

Ha-Joon Chang is a leading economist, ranked by _Prospect Magazine_ as one of the top World Thinkers in 2013. He has worked as a consultant for the United Nations, the World Bank and several U.N. agencies.

He is the author of many books including the bestselling _23 Things they Don't Tell You About Capitalism_ (also available in blinks).

