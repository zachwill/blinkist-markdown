---
id: 59ba1b07b238e100052dbce7
slug: killing-the-rising-sun-en
published_date: 2017-09-14T09:00:00.000+00:00
author: Bill O’Reilly and Martin Dugard
title: Killing the Rising Sun
subtitle: How America Vanquished World War II Japan
main_color: BF3637
text_color: BF3637
---

# Killing the Rising Sun

_How America Vanquished World War II Japan_

**Bill O’Reilly and Martin Dugard**

_Killing the Rising Sun_ (2016) tells the story of the Pacific War, which took place between 1941 and 1945, and its main belligerents, the United States and Japan. From the attack on Pearl Harbor to bloody invasions of Japan to the development of the world's first atomic bomb, the book portrays the brutality of World War II from a US perspective, and describes how the war was eventually won.

---
### 1. What’s in it for me? Delve into the Pacific War’s atomic brutality. 

It has been over 70 years since the United States dropped the first — and thus far, only — atomic weapons ever used in warfare on the land of the rising sun, Japan. The bombings of Hiroshima and Nagasaki have since become emblematic of the devastating brutality of World War II.

These blinks give context to this brutality, detailing how the United States was forced into the war after the Japanese attacked Pearl Harbor, and what it took to finally make Japan surrender, thus marking the end of the war.

You'll discover the grim and brutal history of what went on in the Pacific War between the United States and Japan from 1941 to 1945, and how the world's first atomic bombs were developed and later dropped on Hiroshima and Nagasaki.

You'll also learn

  * What happened on A-day;

  * how a bomb with the explosive force of 19,000 tons of dynamite came to be; and

  * why, even when Hiroshima lay in ashes, Japan did not surrender.

### 2. Toward the end of World War II, Japanese soldiers were still fighting tooth and nail against American invaders. 

On December 7, 1941, the United States was left without a choice.

The Japanese military had been eager to show their strength and dominance of the Pacific region, so they unleashed a horrific surprise attack on the American naval fleet at Pearl Harbor, Hawaii, killing nearly 2,500 men.

The United States had never experienced an attack like this, so there was little doubt about what would come next: the country declared war on Japan and joined the Allied forces in World War II.

Fast forward to the autumn of 1944, and the Allies were on the verge of winning the war; only the incredibly resilient and fierce Japanese army stood in their way.

General Douglas MacArthur was ready to apply the crushing blow, and he planned to deliver it on October 20, 1944.

It was called _A-Day_, short for Attack Day, and over 100,000 US Army troops under MacArthur's command were sent onto the beaches and into the jungles of the Philippines.

Given the proximity of the Philippines to Japan, this invasion was a strategic necessity if they were to have any hope of successfully invading Japan.

MacArthur had reason to be optimistic: intelligence reports had predicted minimal enemy resistance. Yet weeks of bloody combat and fierce resistance followed, and it soon became clear that finishing off the Pacific campaign was going to be anything but easy.

It was the same story approximately 700 miles to the east, on the small island of Peleliu.

The Americans were desperate to secure an airstrip on this little speck in the Pacific Ocean, and once again the intelligence reports gave reason to be optimistic. But the Japanese forces were deeply entrenched and weren't giving up an inch of land without inflicting horrific casualties on their American invaders.

Little did the US forces know that the Imperial Japanese Army was fighting under the code of Bushido, one of the strongest beliefs of the samurai, which views surrender as an appalling dishonor.

So, a battle that was supposed to last four days stretched on for 12 blood-soaked weeks, resulting in more than 6,500 American casualties before the island of Peleliu finally came under American control.

> _General Douglas MacArthur received the Medal of Honor for his service in the Philippines, making him and his father, General Arthur MacArthur Jr., the first father and son to be awarded the medal._

### 3. During the war, Japanese forces committed barbarous crimes and Japan refused to surrender even after heavy US bombardment. 

After the bloody fighting on Peleliu, Americans began to realize that, since Japanese soldiers considered their Emperor to be a divine being, they were willing to fight to the death in his honor.

Japanese soldiers were also fighting for _hakkō_ _ichiu_, which translates to "eight crown cords under one roof." This referred to their ultimate goal: to unite all of Asia under Japanese Emperor Hirohito.

To make this a reality, and to supply their war effort with the extra oil and steel they needed, Japan had invaded every neighboring island and nation and plundered its natural resources.

These invasions resulted in some extremely barbarous crimes by the Japanese soldiers.

After the battle for the city of Shanghai in 1937, the Japanese executed 90,000 Chinese soldiers who had been taken as prisoners of war. They then moved on to the civilians, many of whom were randomly shot, while others became part of a grisly contest between Japanese soldiers, who placed bets on who could chop off the most heads.

The _Japan Advertiser_ newspaper actually covered one of these beheading contests, keeping a daily tally between two prominent soldiers.

Things weren't any better in the city of Nanking, where an estimated 80,000 women were systematically raped by Japanese soldiers.

Even the attack on Pearl Harbor showed signs of the particularly cruel practices of the Japanese military.

The bombing was a tactical strike against a military target. But the surprise attack was executed early on a Sunday morning, when the majority of the US sailors and personnel were sure to be asleep — and thus would have no hope of surviving.

By the spring of 1945, things had turned around, but the Japanese continued to fight.

In an attempt to crush the will of the Japanese people, US Air Force General Curtis LeMay had decided it was time to move beyond attacking military bases and factories, and began a campaign of mass aerial bombardment.

This new strategy began on March 10, 1945 when B-29 bombers dropped 2,000 tons worth of napalm bombs on Tokyo, killing 100,000 people and badly burning 40,000 more.

But even though similar strikes would target the cities of Nagoya, Yokohama, Osaka, Kobe and Kawasaki, the Japanese still showed no signs of surrender.

> _"If the Japanese fight this fiercely over a small, remote island, what does this say about the success of an invasion of Japan?"_ — Franklin D. Roosevelt

### 4. The United States secretly developed the first atomic bomb while marines continued their bloody battles in the Pacific. 

After more than 12 years as president and commander in chief, Franklin D. Roosevelt passed away on April 12, 1945. Under extreme pressure to end the war, Roosevelt had been smoking at least one pack of unfiltered Camel cigarettes a day, which likely contributed to the cerebral hemorrhage that killed him.

The task of achieving victory in the Pacific now fell to his vice president, Harry S. Truman. But Truman would soon have a critical tool at his disposal that had remained in its developmental stages during his predecessor's time: the atomic bomb.

Scientists had discovered a way to split the nucleus of an atom just prior to the start of World War II. A tremendous amount of energy is released from a nucleus when it splits, and both the Germans and the Japanese were racing to beat the United States and become the first to weaponize this energy.

The United States had a team of researchers led by physicist J. Robert Oppenheimer, whose work to build an atomic bomb was called the Manhattan Project. By the spring of 1945, after years of nonstop research and testing, they were ready for a full-scale test.

They took their atomic bomb to a remote part of the desert in New Mexico, where the world's first weapon of mass destruction was about to be put on display.

Meanwhile, even though Nazi Germany had officially surrendered on May 8, 1945, the war in the Pacific was still raging.

On the southern Japanese island of Okinawa, US Marines were entangled in a bloody fight. Allied forces were getting closer to reaching the Japanese mainland, but the battles on Okinawa would prove to be some of the war's most gruesome.

After 82 days of combat and 20,000 US casualties, Okinawa was finally taken on June 23, 1945. But if anyone thought the Japanese were any closer to surrender, they were sorely mistaken.

While his cities may have been in ruins and any hope of victory long since crushed, the thought of surrender had yet to cross Emperor Hirohito's mind.

### 5. The United States successfully tested the atomic bomb before shipping it across the Pacific Ocean and toward Japan. 

In the early morning hours of July 16, 1945, President Truman was in Potsdam, a small town outside Berlin, Germany. He was there to attend a summit with British Prime Minister Winston Churchill and Soviet leader Joseph Stalin. Negotiations were underway to bring order to Europe after the chaos of war, but Truman was about to get some important news from back home.

At precisely 5:30 a.m., in a flat stretch of the New Mexico desert known as Jornada del Muerto, the Trinity test was scheduled to begin. J. Robert Oppenheimer had christened the Manhattan Project's first atomic bomb with the codename "Trinity."

While Oppenheimer and his fellow scientists were certain that the bomb would release deadly radioactive particles into the atmosphere, no one could have been exactly sure of what else Trinity had in store.

The world's first atomic bomb detonated as scheduled, and it filled the sky with a blinding light while sending a devastating blast wave that was felt miles away. When the results came back, the scientists discovered that their new bomb was equivalent to 19,000 tons of TNT.

When Truman heard this news, he became determined to use the bomb against Japan. In his mind, the atomic bomb was simply to become another conventional weapon of war, albeit far more powerful. But he would soon realize how dramatically this device could tip the scales of war.

Even though US forces were starting to see a few Japanese soldiers surrender, the fact remained that there were still 2 million others ready to defend their homeland to the death. And despite the increasing number of homeless and hungry Japanese citizens, the Emperor was still calling for war.

In response, two new bombs were quickly prepared and shipped across the Pacific Ocean toward Japan.

> _Thanks to his spies, Soviet leader Joseph Stalin knew about the atom bomb and the US research facility in Los Alamos before Truman shared the news._

### 6. The bombing of Hiroshima caused incredible destruction and misery. 

Prior to August 6, 1945, Hiroshima had remained untouched by US bombing raids. But since many of the city's 350,000 residents were soldiers, and the city's port was one of the Japan's largest military supply depots, Hiroshima remained a target.

Three days before the scheduled bombing of Hiroshima, B-29 bombers had flown over the city, dropping pamphlets that read, "Civilians, evacuate at once."

This followed a prior warning from President Truman, who spoke from the Potsdam Conference to warn Emperor Hirohito that, if he didn't surrender soon, Japan would face "prompt and utter destruction." But again, the Emperor remained determined in his refusal to submit.

So the plan proceeded. On August 6, Colonel Paul Tibbets, accompanied by his handpicked crew, boarded their B-29 bomber, which had its nickname "Enola Gay" painted along its side. The name was a reference to Tibbets's beloved mother, who had long supported Paul's desire to be an Air Force pilot.

The Enola Gay took off from Tinian, one of the Northern Mariana Islands, carrying its lethal payload, a bomb that had been christened "Little Boy."

They reached Hiroshima without incident, and at 8:15 a.m., Little Boy was dropped, causing unprecedented destruction and human misery.

The detonation occurred during the city's morning commute. Within seconds, all life within a one-mile radius of ground zero was wiped away; in an instant, 70,000 lives vanished.

Further out beyond the radius, survivors covered in burns screamed in agony, and in the months and years to come, many of these people would eventually die, succumbing to radiation poisoning.

As the dust from Little Boy settled, President Truman once again called upon Emperor Hirohito to surrender unconditionally. But all he received in return was utter silence.

> _"Thousands of men, women, and children within a half-mile radius . . . are simultaneously reduced to lumps of charcoal."_

### 7. The Japanese Imperial Army was clearly beaten, but the United States dropped a second atomic bomb on Japan. 

After Hiroshima, the atomic bomb was no longer a secret, and an uneasy mix of fear and celebration began to set in across the Allied nations.

_The New York Times_ expressed what many people were at once worried about: the atomic bomb would become a justifiable new weapon of war, perhaps used by other nations in the future.

Other US newspapers celebrated the new weapon, while many American soldiers expressed their thanks that plans for further ground combat had come to an end. Some also saw it as an appropriate payback for Pearl Harbor.

By this point, Japan was completely encircled by US forces, Hiroshima was covered in the ashes of the dead and it was clear that the Japanese Imperial Army had no chance of victory. Yet Emperor Hirohito still refused to surrender.

On top of everything else, the Soviet Union had just violated the nonaggression pact that they had agreed with Japan back in 1941.

On August 9, 1945, the Soviets invaded Manchuria in northeastern China. This was a province that Japan had conquered early in the war, but it was now in no position to defend itself against the Russian forces.

That same day, a mere three days after the Hiroshima bombing, the US Air Force dropped their second atomic bomb over Japan.

This five-ton bomb was given the name "Fat Man" due to its rounder design, and it was delivered by a B-29 known as "Bockscar." The chosen target on this day was Nagasaki, home to a Japanese torpedo plant.

Fat Man proved to be even more powerful than Little Boy, killing an estimated 45,000 men, women and children, and injuring 60,000 more. Again, many of the survivors experienced the horror of radiation poisoning over the weeks, months and years that followed.

> _"It seems to be the most terrible thing ever discovered, but it can be made the most useful."_ — President Harry S. Truman

### 8. After much-continued resistance, Japan finally surrendered on August 15, 1945, ending years of war. 

Truman didn't want any more people to die, but a thought hung in his mind: would he have to drop a third atomic bomb on Tokyo before this would end?

While the Japanese culture saw surrender as dishonorable, there was also a fear of criminal punishment among the country's military leaders, which was also preventing the end of the war.

Tribunals to hold Nazi war criminals accountable for their crimes were already underway in the German city of Nuremberg. Many of Japan's high-ranking military leaders, including the divine Hirohito, were surely uneasy about ending up with the same fate as the Nazi leadership.

This would certainly be the case for General Hideki Tojo, often seen as the "Hitler of Japan." He ordered the attack on Pearl Harbor and was surely at the top of the United States' most wanted list.

There was no shortage of crimes that the trials could seek justice for, including the murder of prisoners of war and allowing troops to turn women from conquered territories into prostitutes.

The tribunal would also seek retribution for the 150,000 British and Dutch soldiers that were forced into slave labor, while being denied food and medical treatment by the Japanese.

So it was on August 10, 1945, the day after the Nagasaki bombing, that Truman received Japan's letter of surrender, though it came with one major condition: that Emperor Hirohito remain in power.

Truman was unwilling to appear submissive to Japan's demands — but he was willing to negotiate. He sent word back to Japan that Hirohito wouldn't be granted immunity from prosecution for his war crimes, but that he could remain in power.

It would be five tense days of waiting before Japan made their surrender official on August 15th. Finally, World War II had come to an end.

When word reached the streets of Washington, mass celebration broke out.

In the end, several Japanese generals and ministers were sentenced to death, including General Tojo and former prime minister Koki Hirota. But Emperor Hirohito was not tried; instead, he came to be seen by the allies as an important figure in the peaceful rebuilding of Japan.

### 9. Final summary 

The key message in this book:

**The United States was dragged into World War II after the Japanese attack on Pearl Harbor in 1941. The secret development of the first atomic bomb, and the subsequent dropping of two bombs on Hiroshima and Nagasaki in August of 1945, forced Japan to surrender and finally ended the war. However, the question remains as to whether dropping the atomic bomb was the right choice.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading: _Command and Control_** ** __****by** **Eric Schlosser**

_Command and Control_ (2013) uncovers the disturbing truth behind the troubled and accident-prone US nuclear weapons program. Find out what's really been going on since World War II, when the first nuclear bomb was invented, and how lucky we are to still be here despite numerous accidents and close calls that could have kicked off Armageddon. If you think the stockpile of nuclear weapons in the United States has always been safely stored under lock and key — think again!
---

### Bill O’Reilly and Martin Dugard

Bill O'Reilly is a journalist and television personality best known for his conservative political commentary show, _The O'Reilly Factor_, broadcast on Fox News. He is also the author of numerous books, including _Killing Kennedy_, _Killing Reagan_ and _Killing Jesus_.

Martin Dugard is an American author and journalist. His books include _The Last Voyage of Columbus_, _Killing Kennedy_, _Killing Reagan_ and _Killing Jesus_.

