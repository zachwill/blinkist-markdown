---
id: 58a046e8cb5c2800040315e2
slug: labor-of-love-en
published_date: 2017-02-14T00:00:00.000+00:00
author: Moira Weigel
title: Labor of Love
subtitle: The Invention of Dating
main_color: FC6A69
text_color: 963F3F
---

# Labor of Love

_The Invention of Dating_

**Moira Weigel**

_Labor of Love_ (2016) is your guide to the history of dating. These blinks walk you through the social, cultural and economic shifts that have shaped modern rituals of courtship and explain the curious fads and fashions of flirtation that have come and gone through the ages.

---
### 1. What’s in it for me? Learn what dating was like for your parents’ and grandparents’ generations. 

Dating isn't like it used to be. Not so long ago, looking for a date required courage at the local bar or a friend's party. Now, dozens of eager partners are just a swipe away, on OkCupid, Tinder or Match.com.

There's no question that the internet has changed our relationship with dating. Historically, however, this isn't the first time that the path to finding love has taken a sharp turn. In fact, how we date has always been driven by socioeconomic and cultural developments.

In these blinks, you'll see how the industrial revolution, the rise of capitalist societies and even the 1960s hippie culture transformed the world of dating. Interestingly, you'll learn what your parents and grandparents had to do to get a date — and compare this with your experiences with dating today.

You'll also learn

  * how female shop employees influenced the dating world;

  * why dating apps are really about consumption; and

  * why the word "gay" has evolved to mean homosexual.

### 2. The influx of women into cities during the Industrial Revolution transformed courtship in America. 

The world of dating is much different than the one in which your parents met. Do you know why?

Well, economics certainly played a part. To understand this better, let's wander back briefly in history to the Industrial Revolution, an event that transformed courtship among the working and middle classes.

Before the late nineteenth century, "dating" as we know it now wasn't really a thing. Instead, matchmaking was controlled by one's parents or other relatives. In the 1880s, however, the industrial boom led to an increased need for labor, and many young women moved to cities to work in factories, in shops as salespeople, or as servants. This influx of female workers transformed life in the city. Suddenly, city dwellers could witness a previously rare sight — men and women spending time together in public spaces, even kissing in parks and secluded alleyways.

Couples showed affection in public mainly because they had nowhere else to go. Apartments were small and crowded, leaving little room for privacy; workers on limited incomes couldn't afford an evening at the theater or other places of entertainment.

In contrast, society's expanding middle class, built with the new wealth from industrialization, could "call" on each other. This popular method of courtship basically had men competing for the favors of women by visiting them at home.

If a man was interested in a particular woman, he'd show up at her home and knock on her front door. A servant would usually answer and take the man's name and inform the woman of the "caller." If the woman was interested in the caller, he'd be invited inside. The pair could then talk, sing and enjoy each other's company for a time, yet always under supervision.

Such a private ritual reflected the wealth of the growing middle classes. Unlike workers who had to steal time in the shadows in public, wealthier individuals could meet suitors in private.

### 3. In the 1900s, a thriving American economy made dating a question of consumption and taste. 

There was a time when people looking for a relationship would roam the local bars for a partner. But today, most people just log on and swipe right or left, depending on the particular dating app.

From patient, personal searching to digital courtship — how did we get here?

_Shopgirls_ played a key role in this transformation. These young women moved to American cities in the early twentieth century for jobs in department stores, selling luxury goods to affluent customers.

At the time, working as a shopgirl was thought of as a great opportunity to meet a rich man — ideally one who could be turned into a rich husband.

Shopgirls observed the well-heeled women who shopped in their stores and made a point to mimic the way these rich ladies dressed and spoke.

In this boom time, the shopgirls became excellent salespeople, and their clients bought more and more. Soon it was tough to tell the difference between the customers and the shopgirls, who mirrored their exclusive taste in all things.

By 1925, consumerism was the engine that fueled the American economy and society at large. But what does all this have to do with online dating?

It explains why we often judge a potential mate online based on what the person consumes; that is, on what the person likes and wears, and not on who they are as a person.

If you visit online dating services such as Match.com or OkCupid, for example, you're asked first about consumer preferences, like your favorite bands, books and food. Not whether you're a quiet or shy person, for example, or about other characteristics that define your personality.

Yet in the early twentieth century, shopgirls weren't the only people looking for love. There were plenty of people for whom dating was not an option. We'll explore who these people were in the next blink.

### 4. Gay people and black women in the 1900s devised ways to overcome society’s obstacles to dating. 

Even today, people who don't fit the "traditionalist" view that a couple should consist of a man and a woman have to fight for basic rights. A century ago, things were even more challenging — even dangerous. 

In the 1900s, society and the law were openly hostile to the practice of homosexuality. Gay people in America faced sanctions, if not jail time, when they were outed. Indeed, many gay people found themselves behind bars or the targets of public shaming in the papers.

As a response, gay people created a secret language to communicate their sexual preference to potential mates without fear of oppression.

For instance, tilting your head in a particular way or mentioning the word "gay" in conversation, which at the time was a word that meant "fun" or "bright," could indicate your sexual preference to the person with whom you were speaking, if they understood the code.

Gay people also used clothing as a form of secret communication. Ralph Werther lived in New York in the 1900s and was one of the first transgender people to publish an autobiography. In the book, Werther describes how he would wear white gloves and a red bow tie to signal his sexual preference to others.

In addition to challenges in the gay community, many black women working as domestic servants in white households also faced difficulties when it came to finding a partner.

These girls mostly stayed in the home and thus didn't have the opportunity to meet men. They also earned little and couldn't afford entertainment, like that at the popular but expensive Harlem clubs.

So these women organized "rent parties" — social functions in private homes in Harlem hosted by people who were struggling to pay rent. At these gatherings, musicians would play for money, with some of the proceeds going to the host. These parties also allowed black women to meet single men.

### 5. The more liberal dating norms of the 1950s rattled the nerves and morals of the previous generation. 

Today it's not uncommon for people to jump from romantic partner to partner or "hook up" with a person for a short period or even one night, without any expectation of a relationship.

The practice of trying out different partners before committing comes from the 1950s when a new trend in dating became popular. It was called "going steady."

While "going steady" might seem just like dating as we know it today, in practice it was something different. Going steady was a ritual that signaled a coming of age, and had little to do with finding someone to marry.

"Going steady" essentially became a fad; by the mid-1950s, one in every ten children had tried to "go steady" with a romantic partner before the age of 11!

This fad was worrisome for "old-fashioned" parents. Dating for their generation was done specifically to find a spouse; now, their children were dating with greater freedom, "going steady" with partner after partner.

Although premarital sex wasn't a new concept, society wasn't ready for this and parents feared their children would have sex with people they had no intention of settling down with.

Studies from the 1950s show that young people were comfortable with "heavy petting," or sexual stimulation stopping just short of intercourse, with a "steady" romantic partner.

The Kelly Longitudinal study, for example, revealed in one poll that of participants who had married in the 1950s, just under ten percent had done nothing more than kiss and hug a partner before marriage. Which means the other 90 percent, or a clear majority, had done a whole lot more.

### 6. The sexual revolution gave both men and women free rein to enjoy dating as well as premarital sex. 

While "going steady" was all the rage in the 1950s, after the 1960s sexual revolution, rules regarding dating and sexual relationships became far less restrictive.

At the time, young people argued that they deserved the freedom to have sex with whomever they chose. Intercourse wasn't an act to be enjoyed only once people were married. They felt that sex and sexual desire were natural things and should be pursued however one desired.

This younger generation also felt that rules or laws that dictated sexual mores, such as those condemning premarital sex or homosexuality, should be repealed.

In short, marriage and monogamy were out; free love was in. This laissez-faire approach to sex and relationships made dating far easier for men and women alike.

One example from this new uninhibited era was the publication of the adult magazine Playboy by Hugh Hefner in 1953. And in 1965, the magazine Cosmopolitan rebranded, taking a more consumerist tack. Articles instructed female readers on how to make themselves more like the women that men wanted.

Even though these publications' strategies varied, both magazines thrived given the new atmosphere of freedom in sexuality and sexual exploration.

By the early 1970s, one out of every four American male students had a subscription to Playboy; each issue sold millions of copies.

### 7. The AIDS epidemic put the brakes on casual sex, first for gay men and then for the public at large. 

In the early 1980s, many people, especially gay men, were being diagnosed with and dying from a series of uncommon diseases. Doctors were perplexed; the one connection among a majority of patients, however, was sexual orientation.

Doctors began referring to this growing epidemic as _gay-related immuno-deficiency,_ or GRID. We now know this illness as AIDS, or Acquired Immune Deficiency Syndrome.

This epidemic hit the gay community particularly hard; and in the process, AIDS also transformed how society viewed casual sex and dating.

Importantly, AIDS forced people to talk about sex. Sleeping with someone was now a matter of life and death — the stakes of casual relationships had been raised dramatically.

Sexual freedom wasn't as free as it used to be, and new rules had to be followed to stay safe.

During the AIDS epidemic, some writers and doctors advocated for abstinence, or avoiding casual sexual contact altogether. But this was an impossible proposition. People then began to devise ways to have safe sex to lower the risk of infection.

South African doctor Joseph Sonnabend, practicing in New York at the time, supervised the writing of a booklet in collaboration with two gay men called _How to Have Sex in an Epidemic_. It proposed that men be aware of the potential health risks of gay sex, explaining how to practice safer sex through _creative penetration_ that involved the use of condoms, sex toys and/or fingers.

Honesty about sexual relations in the gay community eventually spilled over to the general public. The new focus on safe sexual practices prompted communities of all stripes to talk about sex openly.

US Surgeon General C. Everett Koop, against the wishes of fellow conservative Christians, encouraged Americans to openly discuss matters of sex.

With assistance from the Centers for Disease Control (CDC), Koop published a brochure that found its way into millions of American households. This brochure tackled difficult issues such as the need to be cautious when choosing sexual partners, as well as how to ask questions about a mate's previous sexual contact and history of sexually transmitted diseases.

### 8. Final summary 

What is the key message of this book:

**Dating has changed drastically through the ages. A variety of factors have influenced how individuals choose a mate, but the economy has greatly influenced trends in the world of dating as we know it today.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Modern Romance_** **by Aziz Ansari**

The internet and modern technology have revolutionized the way we communicate, learn, work and live. They've even revolutionized our concept of love. _Modern Romance_ (2015) explains how our idea of "love" has changed in recent generations, and how you can make the most of today's technology in your quest to find it.
---

### Moira Weigel

Moira Weigel is an American writer and PhD candidate at Yale University. Her work has appeared in _The_ _New York Times_ and _The_ _Guardian_, among other publications.

