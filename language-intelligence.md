---
id: 59887313b238e100062900e1
slug: language-intelligence-en
published_date: 2017-08-10T00:00:00.000+00:00
author: Joseph J Romm
title: Language Intelligence
subtitle: Lessons on Persuasion from Jesus, Shakespeare, Lincoln, and Lady Gaga
main_color: 7AB0CC
text_color: 466575
---

# Language Intelligence

_Lessons on Persuasion from Jesus, Shakespeare, Lincoln, and Lady Gaga_

**Joseph J Romm**

_Language Intelligence_ (2012) focuses on an aspect of language that is often overlooked or dismissed: the art of rhetoric. From the King James Bible to Shakespeare, from modern-day political campaigns to the lyrics of pop songs, rhetoric is a widely used tool — one that we all should learn to use and understand. After all, in words there is power and strength.

**This is a Blinkist staff pick**

_"These blinks take you through history and around the world in search of answers to the age-old question of how the languages we speak affect the way we think and act. Really fascinating stuff!"_

– Erik, Editorial Production Manager at Blinkist

---
### 1. What’s in it for me? Learn the art of rhetoric. 

In classical education, one subject reigned supreme: the art of rhetoric. Cicero was a gifted speaker, and so were Aristotle and Socrates. Rhetoric long retained its status, and can be found in the poetic language of Shakespeare and Goethe.

But today the high art of rhetoric has fallen by the wayside. And this is dangerous. Not only do we neglect the beauty of language; we lose our ability to listen, making us susceptible to all sorts of chicanery and doublespeak.

Rhetoric is both beauty and beast. It contains colorful figures of speech, such as descriptive language, metaphors and irony. Yet rhetoric is also a form of persuasion, and can be used to manipulate.

These blinks walk you through the beauty of language and reveal the tools and tricks that are used in persuasion. You will learn to appreciate the language arts and to see past their seductive techniques.

You'll also learn

  * why you need to master rhetoric if you want to sell something;

  * why George W. Bush is a gifted speaker; and

  * why Lady Gaga is the queen of pop rhetoric.

### 2. Even a cursory knowledge of rhetoric will make you less manipulable. 

Modern society is awash with reason and empirical evidence; information abounds — and can be easily shared or double-checked online. In this flood of fact, it's all too easy to forget the art of rhetoric and the power of language. Today, there's hardly any place for rhetoric in the school curriculum, and academia is hardly more accommodating.

For instance, 2011 marked the four hundredth anniversary of the King James Bible. Scholars, of course, had plenty to say. What's amazing is that although this translation ranks among the finest compositions in English literature, not one of these scholars deigned to discuss figures of speech.

In fact, the historical theologian Alister McGrath even insinuated that the translation's expressiveness was mere accident.

Such willful disregard of rhetoric's centrality in the Bible is more than a mere academic quibble. It's representative of a cultural trend — a trend that should be resisted. After all, when we understand rhetoric, we can begin to resist its charms.

Rhetoric hasn't vanished, after all. Just think about how politicians and advertisers speak. An understanding of how rhetoric works makes it easier to recognize both a speaker's motivation and his manipulative techniques.

Advertising wouldn't be the same without rhetoric.

In 1992, Edward McQuarrie and David Glen Mick researched advertising spending among corporations. They found that billions were spent on language studies and that figures of speech were at the center of advertising strategies. Rhetoric played a central role in making ads as memorable as possible.

If an ad uses a pun or a metaphor, the target audience is more like to remember the ad in question. For instance, Deere & Company, which, among other products, manufacturers diesel engines, uses the tagline, "Nothing runs like a Deere." Not the cleverest pun — but it certainly sticks!

Once you see through advertising tricks and recognize the crafty art of rhetoric, you'll be able to make more rational purchasing decisions.

### 3. Short words are an effective rhetorical tool, especially in politics. 

It's tempting to think that people who use ornate, florid phrases and pepper their sentences with recherché locutions are smarter than those who use short and simple words to communicate.

But that's not necessarily true. In fact, short words usually get the point across much more effectively than long ones. Winston Churchill, orator extraordinaire, firmly held this belief. What's more, the finest and most memorable sentences in our culture are far from complex. Just consider Hamlet's "to be or not to be" soliloquy or Martin Luther King, Jr's "I have a dream" speech.

The example par excellence is of course Shakespeare, a master of the short word if ever there was one. Even at Lady Macbeth's most frenzied, when she is beset by ghastly hallucinations and attempting to wash her hands of phantom blood, her speech is repetitive and staccato: "Out, damned spot! Out, I say!"

No surprise, then, that short words are very effective in the political sphere.

George W. Bush may have been mocked by the literati for his elementary vocabulary, but it was precisely the seeming genuineness of his unpolished speech that found favor with the voters.

It's actually a gift to be able to use language in this way. Just consider his spontaneous responses to the 9/11 terrorist attacks.

Two days after the attacks, he addressed a crowd with a bullhorn at Ground Zero. When someone in the crowd shouted that they couldn't hear, Bush rapidly replied. He said, "The rest of the world can hear you. And the attackers will be hearing from us very soon"

It's this ability to craft a resonant response with just a few short words that can make a politician truly successful.

Bush isn't the only one to employ this technique, either. Just think of Obama's winning slogan in 2008: "Yes, we can."

### 4. Repetition is a good way of getting your point across – as the Bible illustrates. 

If you've ever had to deal with a grumpy toddler, you'll know that one no is never enough. You've got to say it again and again and again, until the negation finally sinks in.

It's exactly the same with adults. Just like children, we learn and remember through repetition.

A great real-world example of this is how pop music and jingles get stuck in our heads. It's partly because we love re-listening to simple tunes. But it's also because they're designed to be so catchy.

Simplicity and repetition are well known tricks for pop musicians. The queen of this is Lady Gaga — even her stage name works this way.

Consider her 2009 superhit, "Poker Face." In the song, the title is repeated _30 times_. On top of that, she really drives it home by drawing "poker" out into "p-p-poker" several times.

Nor is the phenomenon of repetition restricted to the pop world. There's a lot of it in the Bible, too. It's a technique that goes all the way back.

In fact, that may be one of the reasons why religious Republican politicians are often crowd-pleasing orators. George W. Bush, for instance, is a born-again Christian, and the Bible may well have inspired him to hone the art of repetition.

The sentence that opens the Gospel of John contains some of the most famous repetitions of all time: "In the beginning was the Word, and the Word was with God, and the Word was God."

Perhaps that's why Democratic presidential candidates have also put repetition to use.

American politicians adore the word "America" and Barack Obama is no exception. He even managed to say it six times in two sentences at the 2004 Democratic National Convention.

Obviously, being repetitive isn't the stylistic faux pas your middle-school teacher may have said it was.

### 5. Irony can be used to deflect the truth or to inconspicuously communicate a point. 

You've probably seen this classic defense strategy in action. Someone's losing an argument, and so, instead of pushing logically forward, he takes a different tack and starts making fun of his opponent. First, he sarcastically praises his opponent's intelligence and then mockingly agrees with his opponent's arguments.

Climate change deniers often use irony in this way to attack scientists.

Novelist Michael Crichton ridiculed the climate-related concerns of researchers by saying that, in the 1970s, scientists thought an ice age was imminent. Conservative journalist George Will took up this wisecrack and ran with it, repeatedly re-stating it for years after.

Of course, it was all just a smoke screen to make the scientists look foolish. They had never been concerned about global cooling. In 2008, when climatologist Thomas C. Peterson conducted a study of climate science articles from the 1960s to the 1980s, he exposed the myth. Climate scientists never believed in the threat of an ice age during this period.

Irony also serves a second function. It can help to convey an argument without seeming to — by strengthening the message through intimation. If the truth is hinted at, rather than proclaimed from the rooftops, a speaker can give an audience the satisfaction of working out what's being implied.

For instance, in Shakespeare's _Julius Caesar_, Brutus attempts to persuade a crowd that Caesar's assassination was justified. But when Mark Antony takes the stage he uses a different approach. Instead of accusing Brutus, he praises his honesty. He ironically and repeatedly calls Brutus and his friends honorable. The dripping sarcasm and Mark Antony's mockery of Brutus's behavior is plain for all to see.

### 6. Foreshadowing is a great rhetorical trick perfect for political speeches. 

There's a dramatic principle known as Chekhov's gun. If a play begins with a pistol on stage you can be certain that it'll be fired before the final act. It's not a spoiler — in fact, it heightens the suspense.

The same rule holds just as true in speeches: foreshadowing serves to strengthen the main message.

It's a strategy with a long pedigree. Even the Bible uses it.

Let's consider the story of Joseph. Joseph's father, Jacob, gave him a colorful coat as a present. This gift riled Joseph's brothers, as it made clear that Joseph was Jacob's favorite child.

At this point, Joseph dreamed a foreshadowing dream.

He dreamed that he and his brothers were gathering sheaves of grain. Suddenly, his own bundle stood up and his brothers' bundles bowed down before his.

Needless to say, Joseph's brothers weren't pleased when Joseph related the thinly-disguised symbolic dream.

They first considered killing Joseph, but, after some debate, decided to sell him into slavery. Joseph found himself in Egypt and eventually became a powerful counselor to Pharaoh. Years later, a famine in Canaan drove the brothers to Egypt to search for grain. Joseph put them through a variety of tests, but ultimately, he saved them from the famine.

The dynamics of the dream foreshadowed what eventually happened.

Foreshadowing certainly works well in stories. But it works even better in politics.

When Martin Luther King, Jr. spoke from the steps of the Lincoln Memorial in August 1963 to demand equal civil rights for African-Americans, he confidently opened with some foreshadowing.

He foretold that the March on Washington would be the most memorable march for freedom in US history. And he was right. This short introduction to the "I had a dream" speech is often forgotten. But in concentrating on freedom and history, it contained within it the main themes that Doctor King was to hammer home in the speech.

### 7. Metaphors are powerful figures of speech that mirror structures in the human brain. 

Metaphors aren't part of our everyday speech. We're unlikely to use them when asking someone to pass the salt, for example. We tend only to use metaphors in elevated or emotional situations. When we tell someone she's our sunshine, it's not literal — but it's more powerful than merely saying they're nice.

To be sure, metaphors trump similes.

When we tell someone that she's sweet as honey, it may make an impression, but saying that she _is_ our honey is stronger still.

Similes may be more factually accurate, but metaphors really go straight to our emotions because they're capable of conjuring images that don't exist in real life.

But it's precisely this power that makes metaphors dangerous.

Imagine that your beloved's father was a beekeeper and being called "your honey" calls up traumatic memories of being stung by bees. Your sweet metaphor may turn out to have some unexpectedly bitter overtones.

Metaphors are also interesting because they mirror the ways in which we think.

Cognitive scientist Edward O. Wilson makes this point in his 1984 book, _Biophilia_. The brain uses metaphors to structure, compress and file information.

The psychologists Kahneman and Frederick backed this up in a University of California study conducted in 2001.

Students were asked to estimate the number of murders in Detroit that year, as well as for the state of Michigan. The two numbers were almost equal, but the students believed that Detroit was twice as deadly. This was because the students were accustomed to using Detroit as shorthand — a metaphor — for dangerous criminal activity.

### 8. Extended metaphors create such strong messages that political campaigns often revolve around them. 

If a simple metaphor can create one vivid picture, an extended metaphor can generate a cascade of images.

Once again, the Bible provides a good example of this rhetorical technique.

In the book of John, Chapter 10, Jesus is depicted as a good shepherd willing to sacrifice himself for his flock — a man who will lead his flock to calming uplands.

This is a powerful metaphor for salvation through Christ. And the metaphor continues.

Like a good shepherd who protects his flock from the wolf, Jesus will protect you from life's perils.

The sheep, the shepherd, the uplands and the wolf are all images that create an extended metaphor for Jesus's devotion to humankind.

So powerful are extended metaphors that they are common currency in political campaigns.

Often, two metaphors will be put to use — a positive for one candidate and a negative one for the opponent.

But these metaphors should be coherent. The Democrats learned this lesson in the 2004 presidential campaign against George W. Bush.

They made the mistake of attempting to portray Bush as both stupid and cunning.

On the one hand, they claimed Bush lied about the presence of weapons of mass destruction in Iraq and about links between Saddam Hussein and Osama Bin Laden. On the other, they caricatured Bush as incompetent and stupid. The Michael Moore film _Fahrenheit 9/11_, for example, includes footage of Bush dumbly failing to react after hearing about the terrorist attacks on 9/11.

These portrayals actually canceled each other out rather than creating a powerful extended metaphor.

### 9. Rhetoric can be used for malicious purposes, to influence and manipulate. 

The art of rhetoric, fine as it is, can also be used for evil.

Indeed, the dark side of rhetoric was already well known in classical Athens. The Sophists were a group who prided themselves on their ability to make weak arguments seem strong by using rhetorical techniques.

The philosopher Plato admired them for their skills, but also heaped criticism upon them. He famously claimed that if a Sophist and a doctor had to convince a crowd of the better physician, the Sophist would always prevail, even though the resulting treatment would have dire consequences.

Rhetoric can also be used to influence and manipulate people. We see this everywhere, most commonly in advertising and marketing.

In his 1996 seminal book _The Psychology of Persuasion_, Kevin Hogan explained how to make a reluctant customer buy a product through indirect insinuation. Even making negative suggestions works!

Hogan found that if a salesperson told people not to feel pressured to buy anything, they'd end up purchasing items. Even telling them that they needn't make a decision, or cautioning them not to make up their minds too fast, had a positive effect.

There are basic brain functions that lead to this. We simply don't notice negations such as "don't" as easily as positive expressions like "buy something today."

The principle was also illustrated by George Lakoff in his 2004 book, _Don't Think of an Elephant_! There's no way around it; that sentence will cause an elephant to pop into your head. Therefore, negative imperatives like "don't feel you have to buy this product" actually encourage the opposite of what they ostensibly command.

It makes no difference whether you're using rhetoric in a debate, in a love poem or for selling a product; what's important is staying attuned to its manipulative powers. If you spot someone using rhetoric for harm, be prepared to question his ulterior motives.

### 10. Final summary 

The key message in this book:

**Rhetoric isn't a forgotten art that's confined to old dusty historical and literary books. It's still here today, even if it often goes unnoticed. Whether you want to sing a pop song, tell people about climate change or convince a person of your deep love, you can put rhetoric to use.**

Actionable advice:

**Analyse the information you receive**

Next time you hear a politician speak, don't just buy the message. Instead, analyze the speech. What rhetorical tricks did the politician use? Why? Once you've seen through what the politician wanted you to believe, you'll hopefully have a clearer idea of what is actually at stake

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Through the Language Glass_** **by Guy Deutscher**

_Through the Language Glass_ (2010) explores the many ways in which language both reflects and influences our culture. By exploring the different ways that languages deal with space, gender and color, the book demonstrates just how fundamentally the language you speak alters your perception of the world.
---

### Joseph J Romm

Joseph J. Romm is a communication expert who works on raising awareness for climate-change issues in the United States. He runs a blog called _Climate Progress_ and is the author of several books on ecological concerns, including _Hell and High Water_.

