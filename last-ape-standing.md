---
id: 53319de93862370007e60000
slug: last-ape-standing-en
published_date: 2014-03-25T14:00:00.000+00:00
author: Chip Walter
title: Last Ape Standing
subtitle: The Seven-Million-Year Story of How and Why We Survived
main_color: 2165A6
text_color: 2165A6
---

# Last Ape Standing

_The Seven-Million-Year Story of How and Why We Survived_

**Chip Walter**

_Last Ape Standing_ (2013) tracks the journey of the evolution of human beings. It starts seven million years ago when the jungle habitat of our early ancestors began to recede. This began a process which saw our forbears start walking upright, develop large brains and use tools for the first time.

The process continued over millions of years, and eventually humanoids with large brains left Africa to migrate across the world. The last of these many migrations was our species, Homo sapiens, the first species we know to have the capacity for culture. And this capacity, along with the ability to learn, enabled us to become the last apes standing.

---
### 1. What’s in it for me? Learn how modern humans evolved over the last seven million years. 

No species in the history of the planet has reached anywhere near the levels of dominance that _Homo_ _sapiens_ have. From the Arctic, through the Americas, from Asia to Australia, humans are masters of their environments. How did that happen? 

These blinks will transport you back in time seven million years to show you how a group of apes evolved to achieve this level of success.

You'll discover that we weren't the only group of humanoids to evolve. You'll read about other waves of human migration out of Africa that occurred before our own, and how those humanoid species colonized the globe.

You'll also find out how a mutation in our big toe gave us a crucial advantage for avoiding predators and catching prey. And, finally, you'll see how the birth of art, about 70,000 years ago, was actually a critical development in human evolution.

### 2. Mutations in the big toe made it possible for our ancestors to walk upright. 

Seven million years ago, the world was in the midst of a massive transformation. Changes in the climate were transforming vast areas of the African rainforest into less densely wooded grasslands known as _savannas_. 

This development hugely impacted the apes living in the changing environment, and presented them with a completely new set of problems. 

Like the modern Chimpanzee, the rainforest apes gathered their food from the trees — berries, nuts and fruits — and, as they developed, their big toes would curve so that they could grasp the trees' trunks and branches. But in the sparsely wooded environment of the savannas, the apes' nimble climbing ability was no longer essential, as hanging fruit was less prevalent and there were fewer trees to climb. 

Another problem was that the threat of predators was much greater in the savannas than it was in the rainforest. 

Then evolution intervened. 

A genetic mutation occurred in some of the rainforest apes which stopped the big toe from curling during their development. This was a crucial stage in the apes' evolution. Because a _straight_ big toe can support much more weight than a curled one (our big toes support 30% of our weight) having straight big toes was a vital trait for apes: it enabled our ancestors to walk upright. 

But the benefits of having straight big toes don't end there. It also gave apes the ability to run, jump and change direction swiftly, and to stand tall and survey their surroundings, allowing for a better view of both predators and prey. All of these are crucial attributes for surviving the harsh and challenging savanna environment.

But the mutation of the big toe was just the first step in a series of evolutionary adaptations that enable us humans to live the upright, dynamic lives that we do. Later developments occurred in the neck and pelvic area, the latter of which led to much narrower hips.

### 3. The change to a meat-based diet helped our early ancestors develop larger brains. 

Because primates live in large communities, they need to keep track of all the direct and indirect relationships in their group. For that reason, apes have long had the biggest brains of any animal group. It was this relatively large brain capacity that gave our ancestors their evolutionary advantage.

For the ape, savanna life was challenging enough to force them to use their relatively large brains to their fullest. Indeed, large brains are essential to the acquisition of survival strategies and for effective communication between individuals — and better communication means better collaboration to accomplish shared goals. 

Possessing such abilities was crucial to the ape's survival because, compared with the rainforest, savannas contained many more predators, less food and water, and fewer places to hide. Moreover, since large brains were so important to our ancestors' evolution, they continued to develop and grow.

Of course, a larger brain requires a greater amount of energy, and although the savanna contained little traditional food (fruits and nuts), it did provide more potential prey. It was this that gave our ancestors their great advantage, because a meat-based diet contains many more calories than a diet based on fruits and nuts, providing the extra energy which would allow them to physically support larger brains. 

Fortuitously, this caused a positive-feedback loop, in which having a larger brain meant a greater ability to source meat — which, in turn, meant more energy to grow even bigger brains. For example, consider the fact that the brain capacity of a chimpanzee is approximately 350cc, while that of the savanna primate was 450-500cc. That's a 25-40% larger brain.

So, the conditions of the savanna impelled our ape ancestors to grow much larger brains, and the savanna supplied them with enough meat to support those brains, which gave them the ability to adapt to their difficult new environment. 

It's interesting to note that another primate, _australopithecines,_ maintained a foliage-based diet. As you might expect, the brains of this species did not display the same capacity increase, and they eventually became extinct 1.2 million years ago.

### 4. Being born sooner and developing later enabled our ancestors to become masters of their environment. 

While developing larger brains and the ability to walk upright was clearly advantageous to our ancestors, it also complicated the childbirth process. 

As the apes became bipeds, their bodies changed radically. One of the biggest changes was in the pelvic region: in order to increase adeptness at walking and standing upright on two legs, they developed narrower hips.

Furthermore, as the primates' brains became larger, so did their heads. Coupled with narrower hips, this meant that childbirth became hugely problematic, as the size of the birth canal shrank while the size of babies' heads increased. 

Once again, evolution stepped in. Our ancestor primates began to be born earlier in their development, while the child's head was still small enough to fit through the birth canal. 

And this adaptation is still in effect today. For a human baby to be born at the same level of development as a baby gorilla, it would have to gestate in the womb for 20 months. 

But instead, as human babies develop for the first year or so outside the womb, they are basically _fetal apes_, unable to defend themselves.

But developing outside of the womb also gave us a crucial advantage. While it takes just 11-12 years for chimps to reach physical adulthood, the human brain and body requires almost two decades. Not only does this longer period of development allow our brains to grow larger, but it also provides us with the opportunity to learn as we grow.

As our brains develop, they're malleable. Because of this, we're able to learn from our environment and acquire the skills that will enable us to manage it, such as the ability to create and produce tools and other cultural objects. In contrast, the brains of other primates are fully formed after just one year; too little time for it to learn and develop as we do.

In a compromise between walking upright and having large brains, evolution provided one of the strongest reasons for our success: a long childhood spent outside the womb.

### 5. There were many other species of humanoid in addition to Homo sapiens. 

1.9 million years ago, once certain primates had developed the attributes crucial to surviving savanna life, they began to leave Africa and spread across the globe.

But these early travellers were not our direct ancestors. Rather, they were another species of humanoid: _Homo_ _erectus_. 

In adapting to their new environments, _Homo_ _erectus_ developed stone tools and mastered fire. They wandered far and wide, dispersing over a vast distance, reaching Arabia, Central Asia, Indonesia and Europe.

What this shows is that, by this stage in their development, early humans were adapting to various habitats. As they dispersed, they encountered new environments — mountains, marshlands, tropical climates — and with each one they encountered they were further from the habitat that had defined their genetics. The result was that these humanoids were forced to be creative. 

But it wasn't only _Homo_ _erectus_ that existed outside of Africa. Evidence shows that other humanoid species migrated successfully too. 

One example is the Denisovans, evidence of whom was discovered in Siberia. It's been estimated that the common ancestor of humans and Denisovans lived approximately 1 million years ago. There's also strong evidence of interbreeding among _Homo_ _sapiens_ and Denisovans, in the discovery that 4-6% of the genomes of the people of Papua New Guinea and the nearby Bougainville Island contain Denisovan DNA. 

Another example is _Homo_ _floresiensis_, and evidence of this species has been found on an Indonesian island. Members of this species were sophisticated hunters and had mastered fire, yet they were under four feet tall with small brains. How could their tiny brains lead to such sophisticated behavior? 

Some argue that it's possible the group's stature was the result of _island_ _dwarfing_, where a lack of available food means that a species shrinks in size in order to survive. In the case of _Homo_ _floresiensis_, despite shrinking, they managed to retain their brain power.

It is believed that this species died out around 17,000 years ago. Some suggest, however, that they might live on in the depths of an unexplored Indonesian jungle.

### 6. Modern humans’ closest ancestor was the Neanderthal, and our common ancestor lived 250,000 years ago. 

In addition to the other early humanoid species was _Homo_ _heidelbergensis_ — the most likely common ancestor of both _Homo_ _sapiens_ and _Homo_ _neanderthalis_ (i.e. Neanderthals). 

Those _Homo_ _heidelbergensis_ that migrated from Africa to Europe evolved into Neanderthals. Stockier and stronger than other humanoids — like _Homo_ _erectus_ — their more compact bodies meant that they were less exposed to the cold, harsh European climate, and thus better suited than other types. 

The Neanderthals had large brains — 1100-1400cc, approximately the same size as ours — and fairly sophisticated lifestyles. For instance, their hunting techniques were relatively complex, involving them working together to hunt large game. One of their tactics, for example, was to surround an animal, such as a Mammoth, and drive it towards the edge of a high cliff. To achieve this, the Neanderthals needed sophisticated communication, and it's known that their brains were powerful enough to develop the use of language (although they did have a limited vocal range). 

Further evidence of the Neanderthals' relatively advanced lifestyle is found in the fact that they built shelters, performed burial rituals for their dead and cared for the sick among them. 

However, despite this sophistication, Neanderthals had a fatal flaw: they grew up too soon.

By the time a Neanderthal turned 15, he'd reached adulthood. This is a full five years earlier than the modern human, meaning that the Neanderthal had less time to learn in childhood. 

One probable cause of this quicker development was the demands of their harsh environment. The cold climate, large prey and small communities meant that it was simply more advantageous for Neanderthals to reach adulthood sooner. Of course, this growth speed unfortunately limited the Neanderthals' ability to evolve as modern humans did, which is partly why they weren't able to spread and survive.

### 7. The ability to think symbolically allowed Homo sapiens to become the dominant species on earth. 

Approximately 70,000 years ago, _Homo_ _sapiens_ were on the brink of extinction. 

An ice age and a "volcanic winter" (caused by the ash from a huge volcano eruption blocking the sun) made life incredibly hard. As a result, the population of _Homo_ _sapiens_ dwindled to an estimated 10,000 individuals. 

And yet, over the following centuries they did not die out. Rather, they multiplied and dispersed to become the most dominant species in our planet's history. 

How? 

From around the same time that Neanderthals were edging towards extinction, the first evidence of art and culture has been discovered. In Blombos cave, South Africa, scientists have found tiny rocks with geometric patterns etched into them, and the first evidence of jewelry, in the form of small perforated shell beads. 

Although it's impossible to decipher such patterns, we can assume that their creators knew what they symbolized. This ability to make and "read" symbols constitutes a massive evolutionary step.

Indeed, the ability to _think_ _symbolically_ enabled _Homo_ _sapiens_ to develop a highly complex language, and — crucially — to become self-aware. 

Using symbols we can communicate and discuss ideas and theories among a large number of people. Just look at our written language, for example, which depends entirely on our ability to read and interpret symbols in order to make meaning. 

And this symbolic thinking brought with it another great advantage: it allowed us to become self-aware. For instance, when we think to ourselves, we're actually splitting our minds into two. One part does the "talking" while the other "listens," somewhat like speaking to ourselves in a mirror. 

In doing this, you are, in fact, creating a symbol of yourself, and discussing things with that symbol. As a result of this process, we become conscious, self-aware and able to plan and reflect on our behavior.

The development of art is evidence that our ancestors could think symbolically — a vital evolutionary advantage that allowed _Homo_ _sapiens_ to evade extinction and grow at a fantastic rate.

### 8. In a relatively short period, Homo sapiens spread across the whole globe as other humanoids died out. 

From the moment that _Homo_ _sapiens_ first journeyed out of Africa, it didn't take them long to reach all four corners of the globe. 

67,000 years ago they reached China. 40,000 years ago they reached Europe one way and Australia the other. Finally, 20,000 years ago our ancestors crossed a land bridge between Russia and Alaska, and, 13,000 years ago, reached South America. 

Along their way, _Homo_ _sapiens_ came into contact with the various, different humanoid subspecies who'd left Africa before them. In many cases, it's probable that _Homo_ _sapiens_ interbred with the other species. Indeed, as we've already seen, humans and Denisovans did interbreed; and they bred with the Neanderthals, too. In fact, scientists have found that 1-4% of human DNA is Neanderthal. 

As well as interbreeding, it's also likely that our ancestors fought against the other human groups. For example, evidence has been found in Europe suggesting that Neanderthals and _Homo_ _sapiens_ did indeed battle each other. And not only that: they even _ate_ one another too. 

Some of these humanoid subspecies (Neanderthals, in particular) simply couldn't contend with the more advanced _Homo_ _sapiens_. But that doesn't mean _Homo_ _sapiens_ wiped the others out directly. Rather, it's more likely that _Homo_ _sapiens_ were more able to master their environment effectively. For example, their more sophisticated technologies, their ability to think and communicate symbolically, enabled _Homo_ _sapiens_ to steal the habitat, hunting grounds and prey of the other groups.

As a result, the other humanoid species were steadily squeezed out, retreating into smaller and smaller pockets as our ancestors took over the environment. 

Eventually, as the other humanoids died out, our ancestors were the last apes standing. But this was merely the start of their journey: 10,000 years ago, our sophistication reached another level with the invention of farming, which in turn led to the beginning of civilization, 5,000 years ago.

### 9. Final summary 

The main message of this book:

**As _Homo_** **_sapiens_, we were just one of many humanoids to crawl out of the rainforests of Africa, walk upright and develop sophisticated tools. Yet, we're the only ones to survive to this day — and not just survive, but _flourish_. One reason for this was our long childhood. Having a childhood which spans two decades (as it still does) allowed our brains to grow and develop in the open environment, giving us a huge advantage over rival groups. Another reason was our ability to think symbolically, which not only gave us written language but also allowed us to become self aware, making us capable of planning what our future actions and behaviors will be.**

Actionable advice:

**Don't take childhood for granted.**

Our lengthy childhood provides us with the opportunity to learn from our environment, and helps make us inquisitive and creative. We should not take this for granted, whether we are still young or whether we have children of our own we should ensure that it is a time of learning and creativity. We should not encourage our young to grow up too soon. 

**Stay creative!**

Our earliest ancestors to become self-aware displayed this through art — a legacy we should continue. Through art we can express our views and we can communicate these views to others. Art truly enriches our society and enables communication to traverse the barriers of language and culture.
---

### Chip Walter

Chip Walter is a filmmaker, author and former Bureau Chief of CNN. In addition to his books, including _Thumbs, Toes and Tears — And Other Traits that make us Human_, he has written numerous articles for _Slate_, _The Economist_ and _The Wall Street Journal._

