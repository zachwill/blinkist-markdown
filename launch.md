---
id: 5457a0133365660008af0000
slug: launch-en
published_date: 2014-11-06T00:00:00.000+00:00
author: Jeff Walker
title: Launch
subtitle: An Internet Millionaire's Secret Formula to Sell Almost Anything Online, Build a Business You Love and Live the Life of Your Dreams
main_color: 2CA3DC
text_color: 1D6A8F
---

# Launch

_An Internet Millionaire's Secret Formula to Sell Almost Anything Online, Build a Business You Love and Live the Life of Your Dreams_

**Jeff Walker**

With his _Product Launch Formula,_ Jeff Walker has changed the way we sell. In his book _Launch_, he outlines his tried-and-tested strategy for selling products online and building a business that's almost guaranteed to bring success. Walker shows you how he's succeeded by outlining a step-by-step plan for launching your product — even before you know exactly what it is.

---
### 1. What’s in it for me? Harness the power of the internet to launch your product and business successfully. 

Think about it: the power of the internet is awesome. The technology enables you to reach out to billions of people, at little or no cost. Just imagine the implications for starting a business!

However, there's one drawback: billions of other people can do the same thing. This makes the internet a competitive place, and it can be tricky to make your business rise above the "noise."

This is where the _Product Launch Formula_ comes in to play. Created by the author and based on his personal experience as an entrepreneur, this formula can make your product or business stand out. It focuses on the period immediately before you share your idea with the world, pre-launch.

It is in this period when you create and build excitement around your upcoming product. How do you do this? These blinks will explain; but if you need even more encouragement to read on, consider this: the author used his formula to make $1 million _in just a single hour_.

In the following blinks, you'll also discover:

  * why a mailing list is the most essential business tool you can own;

  * why there's no longer a need for hope-based marketing; and

  * how scarcity can help your product sell in record time.

### 2. The instant communicative power of the internet puts to rest marketing based merely on hope. 

You've spent years thinking about a fantastic business idea, mulling over its possibilities and dreaming of success. Then, one day, you think: "What the hell, I'll go for it!"

So, you borrow all the money you can to get your dream business off the ground and you present it to the world. Then you sit back and wait for the money to start rolling in.

The only problem is that no one seems to think your idea is as good as you do.

This is exactly how many new businesses fail: they succumb to what's known as _hope marketing_ — making a service or product available and simply _hoping_ that it will be popular.

Yet hope is no foundation on which to build a successful enterprise. If you want to succeed, you cannot merely _hope_ that things will work out; rather, you have to control, as much as possible, your chances of succeeding.

Easier said than done, you're probably thinking. Luckily, our modern world provides us with the perfect tools with which to escape our ill-advised reliance on hope marketing.

The key to building a successful business is to engage with your prospects — your potential customers — and the instant communicative power of the internet can help you accomplish this.

In the past, if you'd wanted to find out what these potential customers thought of your idea for a service or product, you'd have to reach out to them and then wait days, weeks or even months for a reply.

Using email, this can be achieved in minutes.

The power of the internet enables you to discover, in record speed, precisely how attractive your idea is to your prospects. And the sooner you find out which ideas don't work, the sooner you can discover what does.

Furthermore, with this incredible increase in speed comes a massive reduction in costs: whereas in the relatively recent past, sending an email or publishing a marketing article could cost you a small fortune, today it doesn't have to cost you a single cent.

Yet this doesn't mean that using the internet means instant profits. You have to use your available tools wisely, as you'll see in the following blinks.

### 3. Direct people to your products by using mental triggers, such as authority or scarcity. 

All this talk about using online technologies to capture new markets might lead you to think that such ideas are thoroughly modern.

Yet this isn't entirely true. At the heart of an effective launch are many time-tested strategies.

One such strategy is to use a _mental trigger_ in the marketing of your idea, service or product. Mental triggers exploit the brain's programming to direct us toward making certain decisions rather than others.

To function efficiently, the brain tends to take shortcuts when making a judgment. Subconsciously, the mind scans for clues in the environment, which it uses to influence our actions.

These are mental triggers; and because they're so powerful, by incorporating them into your marketing you stand a great chance of grabbing people's attention.

But how do you do this?

There are many examples of mental triggers that you can use to generate interest in your product. Two particularly effective ones are _authority_ and _scarcity._

_Authority_ is a great trigger, because we're predisposed to accept the viewpoints of authoritative people. That's why we're so willing to believe in the health advice we get from doctors, or in the accuracy of street directions we get from police.

By incorporating factors that give your viewpoint an air of authority — for instance, listing any impressive and relevant qualifications you have or any famous clients you represent — you increase the odds of people trusting what you have to say **.**

Another mental trigger is _scarcity_. When people think that a particular product is scarce (like diamonds or Ferraris, for example) they automatically assign it a greater value, and want to get their hands on it as soon as possible.

A simple way to exploit this tendency in marketing a product or service is to suggest that its price will rise imminently. For example, offering a "special pre-launch price for a limited time only" is a surefire way to generate interest in in your product.

### 4. The secret to creating a successful product is to market it using sequences and stories. 

Let's say that you've created what you humbly consider to be the greatest product of all time.

But after a brief online search, you encounter an obstacle: although they're not quite as good as yours, other similar products are already on the market.

In this scenario, how do you reach out to prospects and show that your product is really the best?

In a world saturated with similar companies and products, cutting through the noise of the competition is all about having a good marketing strategy.

To that end, an effective way to build excitement around your product or service is to use _sequences –_ that is, to deploy a variety of marketing tactics over a number of days.

The benefits of generating interest in this way can be illustrated by asking yourself: which was the most anticipated Harry Potter book — the first one or the last?

The correct answer is: the last book. The sequence of books that led to that final publication had established and built interest and excitement in the story. (You'll learn more about the most effective sequences in later blinks.)

Another proven strategy to ensure that you appeal to potential customers is to use _stories_.

Put yourself in the customer's shoes, and ask yourself: which would excite me more — a gripping story or a dry theory based on empirical data?

It's the former, obviously. Everyone loves a good story. After all, every religion in the world derives its power from stories, and the same is true of our traditions and cultures.

So, when designing your marketing strategy, make sure that you tell a good story. Stories are also especially useful while using sequences: with each installment, you can go a bit deeper into the story, developing your message in an engaging way.

### 5. The secret to getting a business off the ground is to build a high-quality mailing list. 

So, how exactly can you use the internet — and your ability to communicate to billions of potential customers — to make your businesses successful?

Learn how to use the Product Launch Formula! What this formula entails will be explained over the next few blinks, but it begins with "the list."

Every successful business starts with a _mailing list_ : a list of the people that subscribe to your emails. These are the prospects that will become your most valuable, loyal and responsive customers, and they'll determine how successful you become.

To illustrate the power of the list, consider that when the author needed to get his hands on some fast cash to make a down payment on a house, he turned to his list and offered his subscribers a new product. Within just one week, he had raised more than enough money.

Yet, it's vital to remember that the quality of your list depends not only on its size, but on how many people actually open your mails. A list of 100 where 60 percent of the people open emails is better than a list of 1,000 where only 1 percent do.

So, how do you build up a good list? One tactic is to create what's known as a _squeeze page_.

This is a type of website "landing page" which presents a prospect with two options: enter an email address to receive something of value, or leave the page.

The advantage of the squeeze page is obvious: if a prospect wants to enter the website, he has to enter his contact details.

But not every squeeze page is a successful one. To encourage people to sign up you have to offer them a free gift that you know they'll want, and one that is relevant to your business.

For instance, you wouldn't offer a free video tutorial titled "Improving Your Golf Swing!" if you're trying to develop a business consultancy.

### 6. The process of launching a successful product starts with a "pre-prelaunch." 

Imagine you're captain of a navy ship in the eighteenth century.

In the distance, you see an unfamiliar vessel. Squinting through the fog, you can't quite make out whether the ship is a friend or foe. What should you do?

While you might not be savvy to the rules of eighteenth-century naval warfare, you might consider firing a "shot across the bow" of the other ship. The purpose of this shot is not to damage the other ship; it's merely to get its attention.

And what worked for sailors in the past also works for marketing today. Before you even think about launching your product, you should grab your prospects' attention by _pre-prelaunching_ it.

The _pre-prelaunch_ of an upcoming product or service is designed to pique the interest of those on your list. It's also the perfect opportunity to get prospects' feedback on the product or service you're planning to offer, to see whether it will interest people or whether the idea requires further work.

To create an effective pre-prelaunch, there are various criteria you must follow.

The most important one is that you do not appear to be selling anything _directly_. If you make your pre-prelaunch too much like a sales pitch, it'll put people off. 

There are many strategies you could follow to achieve this, but the author favors one in particular: a short, 80-word email with the subject line "Quick announcement and a favor."

This email is brief and to the point. It informs the reader that a new product is likely to drop very soon, before asking them to complete a brief survey which asks for feedback on the idea.

That's it. No promises of a life-changing product, just a short notification and an appeal for help. And, according to the author, this strategy works 95 percent of the time!

### 7. Ensure your product connects with the desires of prospects in the “prelaunch stage.” 

So you've got your mailing list, and you've piqued the interest of your prospects with the pre-prelaunch. Now it's time to embark on the _prelaunch stage_.

This is the stage where you make sure that prospects find value in your product. This is accomplished by breaking your prelaunch stage into three separate stages.

At each of these stages, you should release content brimming with mental triggers that will increase the chances that your prospects will connect with your product. This content can take any form: PDF reports, a series of emails, blog posts or — the most popular type of content — videos.

To begin a successful prelaunch, follow the following steps:

First, release the initial installment of your prelaunch content. The function of this content is to answer the implicit question of _why_ your prospects should care about your product. Most of the time this is achieved by showing them how the product can transform their lives.

So, if your business is selling golf lessons, you should tell potential customers how you'll transform their game. If you're in the management consultancy game, tell them how you'll transform their business.

Two or three days later, you move to the second stage: telling prospects _what_ your project is about. In this stage, you provide a little more information about the transformation you're promising them.

Here it's a good idea to use case studies. Tell the story of a person who has improved their golf game through your golf swing tutorials; the story of their transformation is a powerful sales tool.

After another two or three days comes the third stage, in which you answer the question of _how_ they'll be transformed by your product.

This stage should end with you revealing how a prospect can get hold of your product; how long they can expect to wait for the actual launch; and what special deal you'll be offering to the very first subscribers — creating that important scarcity trigger.

### 8. A sense of scarcity and time pressure makes prospects eager to buy your product quickly. 

After creating all of this excitement and interest in your product, you're ready to launch.

But before you start, it's worth repeating a crucial aspect of the prelaunch stage. Toward the end of your third prelaunch content release, you should create a sense of scarcity concerning your offer.

In doing this, you're aiming to let prospects know that the release of your product is imminent, and that, once released, it'll be available for a _limited time only_. Scarcity causes excitement.

If this is handled correctly, then the excitement you generate will make the actual launch a much smoother process, since people will already be eagerly waiting for it.

When it comes to preparing for the launch day itself — also known as _open cart day_ — you should follow a few simple instructions:

First, ensure that you have an easy-to-use, well-designed sales page where customers can sign up, and then send a brief email informing them that your product has been released.

If you've done the prelaunch correctly, you can sit back, relax and watch the sales roll in!

Of course, since the aim is to generate excitement by using the scarcity trigger, your launch period should not be open ended. It should have a definite endpoint that will serve to threaten your prospects with the possibility that they'll miss out entirely if they don't subscribe quickly.

For instance, perhaps after a time you'll raise the price; or maybe prospects will lose the chance to receive bonuses and extras. Perhaps you'll even make the product _truly_ scarce, and stop selling it after the launch period.

In between the open cart day and the end of your launch, you need to keep busy, contacting your list and reminding them of the day that the good times will come to an end!

You've now learned all about the stages of the _Product Launch Process_. However, there is still more for you to learn — starting with how to launch, even if you don't have a product!

### 9. Using a seed launch, you can create a successful business even before you have a product or market. 

The formula outlined in the previous blinks isn't only useful for getting your product out there in the most effective way possible.

It can also be used before you have a list or a market — or even a _product_ to sell!

If you are just starting out, you can create a _seed launch._

The aim of this approach is to start with nothing and build your success, launch after launch after launch, each one much bigger than the last. As the saying goes: Out of tiny acorns, mighty oaks grow.

Take Tara Marino. She wanted to help people live more fulfilled lives but she didn't know exactly how. Even though she hadn't yet come up with a product, she decided to launch. She used the launch formula to appeal to a small handful of people and managed to rack up $3,000 worth of sales!

Based on the feedback she got, Marino settled on and designed her product — teleseminars. Then, after delivering that product, she used the feedback to create a more professional, attractive product, which she launched (to a greater number of people), this time gaining $12,000.

Then the next launch brought $90,000. And the one after that, $190,000.

It's very likely that the list for your seed launch will be tiny, comprising only those people you're able to attract through social media.

But this is not a bad thing. Smaller lists tend to be relatively more responsive than larger ones. Small lists will also contain a few _hyper responsives –_ your most excitable and loyal fans. So, even with a tiny list, you'll be able to generate a number of sales from it.

As soon as you start the process, it's crucial that you get as much feedback as possible. So keep asking for opinions from your prospects, and keep sending out surveys.

Every opportunity to mold your product to the specific desires and needs of your potential customers should be taken, as that's how you can turn a small launch into a business empire.

### 10. If you want to maximize the potential of your launches, use the mailing lists of your affiliates. 

So you now know that it's possible to create a successful launch with just a tiny list.

But let's not get carried away. The reality is that maximizing the power of your launch requires you to access the biggest list you can.

A great way to get access to a huge list is to use one created by another company. This is known as a _Joint Venture (JV) Launch_.

Chances are that, in your particular market, there'll be businesses you can team up with that have their own lists. A quick Google search will confirm this.

If your business idea is to sell online golf tutorials, simply search for "improve your golf game" and see what comes up. Sign up to the sites, read their content and see if it's similar to yours.

If it is, and the company appears to be successful (that is, they have a big mailing list), reach out to them and find out if they want to work with you.

Of course, if you find a company who could help you, they aren't going to agree to do so out of the goodness of their hearts. They're going to need some kind of incentive. So, offer them a commission on every sale they bring to you.

You'll also need to demonstrate to them that you'll sell enough units of your product to make it worth their while.

One way to prove this is to try initially an _internal launch_ — taking your product and launching it to your own list first. If it's a hit, this will show other companies that giving you access to their list will be to their advantage.

Once they agree to work with you, they will drive the people on their list to you, usually with a link to your squeeze page.

> Fact: One JV launch helped the author make $12,000 in just one second and $1 million in under an hour!

### 11. Final summary 

The key message in this book:

**You cannot merely hope that your product will succeed;** ** _planning_** **is the key to the success of your business idea. Luckily, the Product Launch Formula offers you concrete steps to organize your strategy and launch a successful product.**

Actionable advice:

**Keep in touch with your customers.**

Just because you've launched your product doesn't mean that the work is over. After a potential customer has signed up to buy your product, make sure that you keep in contact with them to get their feedback. Also, if you try and overshoot their expectations, giving them more than they anticipated, this will make them more likely to sign up for your _next_ product!

**Suggested** **further** **reading:** ** _The $100 Startup_** **by Chris Guillebeau**

The _$100 Startup_ is a guide for people who want to leave their nine-to-five jobs to start their own business. Drawing from case studies of 50 entrepreneurs who have started microbusinesses with $100 or less, Guillebeau gives advice and tools on how to successfully define and sell a product, as well as how to grow your business from there.
---

### Jeff Walker

Jeff Walker is an entrepreneur who started his first online business in 1996, with little money to support himself and his family. Since then, he has founded a number of businesses that have made him a multimillionaire. As the founder and CEO of _Internet Alchemy_, Walker teaches others the secrets to his incredible success.

