---
id: 5741c2992edd1b00032e6c0e
slug: lawrence-in-arabia-en
published_date: 2016-05-27T00:00:00.000+00:00
author: Scott Anderson
title: Lawrence in Arabia
subtitle: War, Deceit, Imperial Folly and the Making of the Modern Middle East
main_color: DE3E30
text_color: AB3025
---

# Lawrence in Arabia

_War, Deceit, Imperial Folly and the Making of the Modern Middle East_

**Scott Anderson**

_Lawrence in Arabia_ (2013) reveals how a small cast of characters forever changed the Middle East during World War I and the Arab Revolt. At its center was T. E. Lawrence, a brash and untrained young military officer who was torn between two nations and experienced firsthand the broken promises of politics and the horrors of war.

---
### 1. What’s in it for me? Experience the drama of World War I that made the Middle East what it is today. 

They say that one man can change the course of history — so imagine what happens when you multiply that by four.

During and after World War I, imperial powers divided the Middle East into the countries we know today, and the region became host to a number of lingering conflicts. In the midst of this turbulent period, the actions of four men — T. E. Lawrence, Curt Prüfer, Aaron Aaronsohn and William Yale, each on different sides of the war, each with his own agendas and sympathies — shaped the history of the region and the world.

The following blinks tell the stories of these four men and their endeavors in the Middle East during World War I — stories that read like a real-life John LeCarré thriller, replete with spies, deception and malicious intentions. It is also the story of how the Middle East turned into one of the world's most politically volatile regions.

In these blinks, you'll find out

  * how Prüfer's connections in the Ottoman Empire swayed the Turks' allegiances;

  * why Lawrence's capture of Aqaba turned out to be a bittersweet victory; and

  * why the repercussions of the _Sykes-Picot Agreement_ are still being felt today.

### 2. Thomas Edward Lawrence’s deep fascination with history led him to the Middle East. 

There's a chance you may know of T. E. Lawrence, the man made famous in the classic movie _Lawrence of Arabia_. But you probably know less about the formative years of this man who played such a crucial role in the Middle East during World War I.

Thomas Edward Lawrence was born in 1888. As a middle-class teenager in Oxford, England, he developed an early curiosity in European and Egyptian history, spending most of his free time at the University of Oxford's Ashmolean Museum.

In 1909, while working on his senior thesis at Oxford, he embarked on a wildly ambitious project: Lawrence walked throughout Syria, studying every castle built during the Crusades to determine whether the Christians learned about architecture from the Muslims or vice versa.

During this trip, Lawrence was met with great hospitality and quickly fell in love with the Middle East.

The staff at Oxford were so impressed by his thesis that he was invited to stay in the Middle East to help out at an archeological dig for his beloved Ashmolean Museum.

By 1911, he was working in _Carchemish_, an ancient city located on the border of Turkey and Syria.

He got to know Middle Eastern culture very well and, as one of the heads of the excavation site, he proved himself to be a natural leader.

He frequently wrote home to his family, telling them how he felt right at home in Carchemish: he was earning the respect of the locals through his impressive understanding of their history as well as his ability to work long hours under the blistering desert sun.

Lawrence spent the next three years working in Carchemish, during which time Europe and the Middle East wound up on the brink of World War I.

### 3. Three other men from different walks of life were also hard at work in the Middle East prior to WWI. 

One of them was Curt Prüfer, a 33-year-old German spy.

Prüfer had an early love of foreign languages that led to him becoming an interpreter at the German embassy in Cairo. But Prüfer had greater ambitions and, in 1913, he met some influential people in Egypt who had an interest in disrupting the country's politics.

Egypt had been under British control since the 1880s and Prüfer, along with other German spies, began stoking the fires of revolution among Egyptian Muslims so they would rise up against British Christians.

However, by November of 1913, the British government took notice and managed to get Prüfer fired from his embassy job. But as we'll find out later on, he wasn't about to leave the Middle East just yet.

Another prominent figure in the area at the time was Aaron Aaronsohn, who was working just across the Suez Canal, in what was then Palestine.

Aaronsohn was a Jewish _agronomist_, or agricultural expert, who was internationally renowned for building a large research facility dedicated to turning Palestine back into the lush and fertile land it once had been.

But Aaronsohn was also a passionate Zionist, meaning he dedicated much of his work to creating a self-sufficient area the Jewish population of the world could return to and call home. So, when Aaronsohn wasn't planting seeds, he was appealing to Western governments to help the Zionist cause.

Which brings us to our third man of note: William Yale.

Yale's family was once one of the wealthiest in America: you probably know the name because of Yale University in Connecticut, which dates back to the 1700s. By 1913, however, the family's fortune was all but gone and William had to look for work.

He found it at Standard Oil, one of the biggest oil companies in the world. And, in 1913, Yale joined Standard Oil to help it find new sources of oil in the Middle East.

Little did he know how interesting his life was about to become.

> "History is often the tale of small moments — chance encounters or casual decisions or sheer coincidence …"

### 4. “History is often the tale of small moments – chance encounters or casual decisions or sheer coincidence …” 

In 1913, the four men — Lawrence, Prüfer, Aaronsohn and Yale — all looked around and noticed the same thing: The Ottoman Empire was in decline.

Once a mighty force, by the start of World War I, the Ottoman Empire was coming apart at the seams.

The vast state dated back centuries: in the 1600s, it extended from Algeria and much of northern Africa across the Middle East to the Caspian Sea and as far north as Budapest.

But since the _Crimean War_, in which the Ottoman, French and British empires fought together against the Russian Empire, and the other _Russo-Turkish Wars_ throughout the 1800s, the Ottoman Empire had lost territories.

By 1913, Egypt had fallen under British control and northern territories such as Bulgaria, Serbia and Romania had all achieved independence. And now the areas that were still under the control of the Turkish monarchy in Constantinople (present-day Istanbul), including Syria, Turkey and Palestine, were starting to become unstable.

Part of the reason for this was that these lands were filled with a diverse population of Muslims, Christians and Jews. Adding to this friction was the rise of a political party called the _Young Turks_, who by 1913 had revolutionized the Empire to form a new government that was struggling to maintain control.

Three high-ranking Turkish officers, known as the _Three Pashas_, led this new government: Interior Minister Talaat Pasha, War Minister Enver Pasha, and Naval Minister Djemal Pasha.

And so, the Ottoman Empire entered into World War I in a weakened state.

It all began on Monday, June 29, 1914, when the heir to the Austro-Hungarian throne, Archduke Franz Ferdinand, was assassinated in Yugoslavia.

At first, it wasn't clear which side the Ottoman Empire would take in the war.

But Curt Prüfer was still in the area exploiting the growing instability and trying to convince the Turks to turn against the British and Egypt, ultimately becoming Djemal Pasha's German liaison.

### 5. The horrors of war quickly escalated once the Ottoman Empire chose a side. 

As war broke out, lines were quickly drawn between the _Allied Powers_, led by the British Empire, France and Russia, and the _Central Powers_, led by Germany and Austria-Hungary. And on November 2, 1914, the Ottoman Empire finally sided with the Central Powers.

Aaronsohn immediately felt the repercussions of this decision.

He saw the military plunder everything in sight for use in the war effort: clothes, food, vehicles and even parts of plumbing systems were seized from homes to be repurposed for the war. He saw Jews and Christians being stripped of weapons and taken into Turkish labor camps.

Many Jews begin to flee the area, but Aaronsohn was determined to remain in Palestine.

Meanwhile, Lawrence, with his knowledge of the region and its culture, was quickly put to work in the British military intelligence office in Cairo.

At first, it was a waiting game, but eventually the British were spurred to launch their own offensive after Curt Prüfer led a Turkish attack on Egypt's Suez Canal that ended in an embarrassing failure.

As this offensive was being planned, Lawrence advised the British to strike at the _Gulf of Alexandretta_. The area had many advantages, including a deep harbor, which allowed boats and soldiers to approach easily, and a landscape that provided cover. Plus, he knew the Arabs and Armenians in the area would likely support their fight against the Turks.

But, against Lawrence's advice, the British and the French decided upon what is now known as the _Gallipoli Campaign_. The battles there went on to show how unprepared military forces were for modern warfare and the use of machine guns.

A massacre ensued when the forces of the British and French empires landed on Turkey's Gallipoli Peninsula in April of 1915: soldiers struggling to get off the boats were effortlessly gunned down. Four thousand fell on the first day alone. People said the water was so filled with blood that the red shore of Gallipoli could be seen hundreds of miles away.

### 6. Lawrence and Yale both cringed at incompetence and deceit they saw around them. 

As the war began, William Yale found himself faced with a tricky situation.

After finding some promising areas for Standard Oil to exploit, he was then tasked with securing the land from the Turkish government in a deceitful way.

Yale had spent most of 1914 wandering around the Middle East in search of areas that might benefit his employer. But once war had broken out, he knew that any drilling plans Standard Oil might have had were indefinitely on pause.

So his employer came up with a cunning plan. Yale was to visit Djemal Pasha, secure the rights to drill on half a million acres of land in Palestine, and simply not mention the fact that Standard Oil wouldn't drill until the war was over.

Yale wasn't a ruthless businessman, but he remained loyal to his employers. In the end, he could only work up the nerve to ask for a quarter million acres of land. Since Djemal assumed the oil would be quickly made available and help the war effort, he agreed.

Yale didn't feel good about the deal, but, by the summer of 1916, he was already on his way home to the United States. And it was on his way back that he began to realize that all of his travels and dealings in the area could be put to good use for the Allied powers.

In the meantime, Lawrence was increasingly frustrated by his superiors' incompetence.

He despised the inefficiency of the Gallipoli Campaign, and he came to realize how deeply politics influenced the British government's military tactics.

For instance, he knew that the decision to land in Gallipoli rather than Alexandretta was largely due to the French government's interest in securing post-war Syria for its own political ends.

Military intelligence was taking a toll on Lawrence, and he was eager to get out of the Cairo offices and try to do some actual good.

### 7. The Arab Uprising began with negotiations that were rooted in deception. 

Lawrence's opportunity to leave his desk job behind came when an Arab lieutenant in the Ottoman forces surrendered to the British. When he was brought in for questioning, he revealed that there was a large population of Arabs, especially in Mecca, willing to fight against the Turks.

But the Arabs would only promise an alliance with the British under certain conditions, and so negotiations ensued.

The British opened up communications with the leader of the Arabs, King Emir Hussein, and his son, Prince Faisal ibn Hussein, and negotiations were held throughout 1915 and into 1916.

King Hussein wanted a guarantee that the Arab territories would be granted independence after the war. While the British agreed to this, they kept the details of these negotiations secret from the French, knowing they would object due to their interest in laying claim to Syria.

Lawrence was sent into the field as a British liaison to work with Prince Faisal and his army of Arab soldiers.

And finally, on June 5, 1916, Emir Hussein fired his rifle in the direction of a Turkish fort in Mecca, and the Arab Uprising began.

Meanwhile, in Europe, the Allied Powers were conducting their own negotiations: in early 1916, they drafted the _Sykes-Picot Agreement_, which, in the event of an Allied win, would grant France control of Syria, British control of Iraq and joint control by the French, British and Russians of Palestine.

As he worked in military intelligence, Lawrence knew of this agreement and it would haunt him throughout the war.

But, in October of 1916, Lawrence was settling into his role as advisor to Prince Faisal and the two immediately took a liking to each other.

Lawrence saw Faisal's unique ability to unite the various Arab tribes of the region and Faisal likely appreciated Lawrence's intelligence and honesty.

Indeed, Lawrence was honest, even telling Faisal about the Sykes-Picot Agreement and constantly doing what he could to try to undermine the desires of the British and French to betray the Arabs.

> "We needed to win the war, and their inspiration had proved the best tool out here." — T. E. Lawrence

### 8. The Jewish population of the Middle East also played an influential role in the war. 

Meanwhile, Aaron Aaronsohn was growing increasingly concerned with what was going on around him.

In Syria, Aaronsohn witnessed the genocide of the Ottoman Empire's Armenian population. And he felt it was only a matter of time before the same happened to the Jewish population.

So, in an effort to help the Allied forces, Aaronsohn decided to take drastic measures: he began keeping a record of the location and size of Turkish armies, depots and supplies, and of undefended coastlines.

Now, if only he could get this information into the hands of the British!

In the summer of 1916, Aaronsohn snuck out of the Ottoman Empire and made his way to neutral Denmark. There, he was picked up by British authorities who greeted his wish to become a wartime spy with a healthy amount of skepticism.

Aaronsohn was detained in Scotland Yard, pleading his case to become a British military intelligence operative.

Finally, by March of 1917, the British were convinced that Aaronsohn was sincere and that his information was valuable. Quickly, Aaronsohn organized a group of 24 Jewish operatives who would act as spies for the Allied forces. Aaronsohn's sister, Sarah, was in charge of this operation.

The spy ring managed to stay active and pass along valuable information about Turkish troops for a while, but it came to a crashing halt on September 17, 1917 when one of Aaronsohn's spies was caught.

Then, on October 2, 1917, Turkish forces seized the group in Palestine, beating and torturing them. Sarah managed to escape and tried to commit suicide, only to have the bullet miss her brain. It took her another four days to die.

As his spy ring disintegrated, Aaronsohn was busy making political gains for his Zionist movement.

In November 1917, still unaware of his sister's death, he celebrated the British government's issue of the _Balfour Declaration_, which confirmed support for the creation of a Jewish homeland in Palestine.

### 9. Lawrence and Prüfer found themselves on opposing paths in the thick of the war. 

By April of 1917, Lawrence was fully immersed in the world of the Arab Uprising. To avoid sticking out like a sore thumb in the middle of Prince Faisal's camp, he shed his British uniform and clothed himself from head to toe in Arab garments.

With the war now in its second year, Lawrence knew that the Arabs would have to take decisive action.

Since he was well aware of the Sykes-Picot Agreement, Lawrence understood that if the Arabs were going to have any chance at claiming independent land, they would have to capture it themselves.

So Lawrence began developing a daring plan to capture the Turkish fort in Aqaba, an area along the northern edge of the Red Sea that was a great place to reinforce additional advances into the heart of Syria.

Also, Lawrence knew that France was plotting its own strike on Aqaba, so it was important for the Arabs to get there first. But rather than opt for a traditional attack by boat, Lawrence gathered a small group of Arab soldiers to move inland through the Wadi Sirhan desert and capture the fort from behind.

Meanwhile, as Lawrence prepared for action, Prüfer was on the sidelines. Instead of talking to contacts in the field, Prüfer was stuck behind a desk in Constantinople staring at maps and trying to come up with new strategies.

This geographical isolation made Prüfer blind to the realities of the war. Even in 1917, as the Arab Uprising progressed and a Jewish spy ring was operating out of Palestine, Prüfer refused to admit there was any trouble. In his reports, he still considered the Jewish population of Palestine "docile and obedient" and the Arabs "too cowardly to rise up against the Turks."

He couldn't have been more wrong.

### 10. Lawrence led a raid on Aqaba that changed the course of the war. 

After four days of nearly intolerable desert conditions on the way to Aqaba, Lawrence began doubting his own efforts and became overwhelmed with hopelessness. Arab soldiers around him were dying, some even from snake bites as the venomous reptiles slithered into their warm blankets at night.

He was carrying the bitter knowledge of the Sykes-Picot Agreement, knowing that even if they did take Aqaba, it was all too likely that Arabs would end up being betrayed by the British at the end of the war.

But his hope was restored when he took a lonely journey through the Wadi Sirhan desert and met another Arab tribal leader, Emir Nuri Shalaan. Lawrence was impressed with the man's determination to defeat the Turks and his spirit convinced Lawrence to carry on and try to take as much land as possible, hoping that no one else would die in vain.

Lawrence and his troops soldiered on toward Aqaba and, as they approached the town of Aba el Lissan, they realized they'd caught up to 550 Turkish soldiers that had been called in for reinforcements. It was clear: they could either try to outrun them to Aqaba or fight.

Lawrence's first major conflict was a swift victory: his small group only lost two men as the Arabs skillfully encircled the Turks, killing 300 and taking 160 captive.

From Aba el Lissan, they reached Aqaba by nightfall.

As they approached the fort, Lawrence saw that his strategy had been sound: all they found was empty blockhouses and trench lines.

Their arrival was met with little resistance: after a two-day standoff, with barely a shot fired from either side, the Turkish commander surrendered on July 6, 1917. Aqaba was theirs.

The British military promoted him to major and awarded him the Victoria Cross, the highest military honor a soldier can receive.

Now the British military could stock Aqaba with supplies and reinforcements, creating an important new base of operations from which to launch future attacks.

> "It might be fraud or it might be farce, [but] no one should say that I could not play it." — T. E. Lawrence

### 11. As the war continued, Lawrence’s experiences became more troubling. 

But Lawrence cared little for awards and medals. As far as he was concerned, the only benefit his success had was that his superiors were now taking his military strategies more seriously.

Lawrence used this new respect to propose an even more ambitious plan: a synchronized attack by the forces of the British Empire and Arab troops to take the key cities of Damascus and Jerusalem.

Now holding Major Lawrence in high regard, his superiors agreed to prepare for his bold strategy.

However, Lawrence and his fellow soldiers grew restless. Although supplies were being stocked in Aqaba, months went by without any further movements made to the north.

And this restlessness resulted in Lawrence's army taking more lives.

On September 19, 1917, in an effort to disable an important Turkish train line, Lawrence took a hundred men on a mission to blow up a bridge and destroy a train.

The result was a gruesome display: after the explosion, Lawrence's army opened fire, killing dozens of Turkish soldiers who tried to escape the train, which left behind many injured civilians, including dozens of women.

Lawrence was clearly becoming psychologically detached from the violence around him, and it was about to get worse.

While trying to inspect a major railway complex in the Syrian town of Deraa, Lawrence was captured and held prisoner by the district governor.

In Lawrence's own description of the events in Deraa, he managed to prevent the governor from raping him, only to be beaten and tortured before later escaping from a hospital.

The author believes there is ample evidence to suggest that Lawrence was indeed both raped and tortured: those who knew Lawrence and saw him after he returned from Deraa saw no signs of the beatings he would later describe, but they did notice a change in his demeanor. He was colder and even more detached from the horrors of war around him than before.

### 12. Aaronsohn and Yale discovered that the United States had limited interest in the Middle East. 

In April of 1917, the United States officially entered World War I by declaring war on Germany. Although it didn't have an official position on the Middle East, the United States was still interested in what was happening in the region.

This is why when William Yale returned to the United States, he found that the _US Bureau of Secret Intelligence_ was very interested in his information. At the time, this bureau was a small organization run by Assistant Secretary of State Leland Harrison.

Based on Yale's experience in the area, they decided to send him to Cairo, thereby making him the only US intelligence agent in the Middle East.

Despite his new title, Yale was far from an expert in military intelligence. So, when he arrived in Cairo in October of 1917, he was granted access to some British documents to help get him up to speed. After reading the papers, Yale saw that there was only one man who seemed to know what was going on in the Middle East: T. E. Lawrence.

But there was yet another man busy trying to get the United States more interested in Middle Eastern affairs: Aaron Aaronsohn.

He traveled to the United States in late 1917 to drum up American support for his Zionist movement in Palestine, and possibly even get the country to declare war on Turkey.

It was in New York that Aaronsohn finally received the news that his spy ring had collapsed, and that his sister and many of his friends were now dead.

To make matters worse his trip was a bust: Aaronsohn was unable to convince his contacts in the United States, such as Supreme Court Justice Louis Brandeis, to interfere with British affairs in Palestine.

At the time, US President Woodrow Wilson was publicly voicing a policy of anti-imperialism. And, perhaps foreseeing all the political landmines in the region, Brandeis and other politicians did not share Britain's interest in Palestine.

### 13. As the war continued into 1918, the outcome wasn’t clear for either side. 

By the start of 1918, our protagonists had different views of what would happen in the war, especially in the Middle East.

Curt Prüfer believed things were looking up for Germany: the Russian Revolution of 1917 had caused Russia to exit the conflict altogether, which helped Germany gain strength in Eastern Europe.

In April of 1918, Prüfer was even scheming to win the allegiance of Abbas Hilmi II, the former monarch of Egypt, whom Prüfer hoped would regain his power following the war.

Alas, Prüfer's efforts would prove to be quite misguided, as within a matter of months, Germany's fortunes would be rapidly dwindling.

On the other hand, Yale's hunch that Lawrence was a Middle East expert proved correct. He finally met Lawrence in March of 1918, and the two men sat down for a talk.

Lawrence had quite a lot to tell Yale: that the Arabs didn't trust the British or the French, and that he and his men were working under the impression that the Arabs could lay claim to any land they took by force.

Lawrence added that he didn't have much hope for the Zionist movement. He felt that a Jewish state in Palestine would never be peaceful, and that it would have to be created and maintained by force.

Soon after this conversation, Lawrence was led to believe that his efforts had not been in vain. Having spoken out vehemently against the Sykes-Picot Agreement, Lawrence eventually pressed for an official answer from the imperial powers: Would the Arabs be able to achieve independence?

Finally, in June of 1918, he got the answer he'd been hoping for: that Britain and France would recognize the independence of lands that Arabs had freed themselves.

Lawrence was now eager to lead his Arab army to Damascus, and he told his men to prepare for victory.

### 14. Fifteen months of war came to a climactic end in Damascus. 

In September of 1918, things in Europe and the Middle East were suddenly coming to a close. The United States had arrived in France to push back the Germans and, on September 19, a British Empire offensive began that successfully forced the Turkish troops into a retreat.

At this point, T. E. Lawrence was about to embark on his final outing — but things got off to a grim start.

Lawrence mobilized his troops and, as soon as they entered the town of Tafa, they witnessed a horrible sight: dead bodies lay everywhere, including the raped and mutilated corpses of women and children.

Lawrence, enraged, reportedly told his men, "The best of you brings me the most Turkish dead."

In the days that followed, Arab soldiers would kill an estimated 4,000 German and Turkish soldiers, leaving a bloody trail to Damascus.

When they did arrive at Damascus, Lawrence and the Arab rebels were greeted with celebrations, but there were more pressing matters at hand.

Lawrence quickly tried to establish an Arab government in Damascus to secure the city's independence.

But this also meant dealing with the _Turkish Hospital_ : an abandoned tent filled with 800 wounded and untreated Turkish troops who had been left to die. The nightmarish scene of the hospital would remain etched in the minds of all the people who witnessed it.

And the situation was not going to get better for the Arabs: as Lawrence had feared, the French and British were still planning to execute the terms the Sykes-Picot Agreement, despite what they'd told him just a few months prior.

On October 3, 1918, a meeting was held at the Victoria Hotel in Damascus, and two British Generals informed Lawrence and Faisal that borders were being redrawn. Lebanon and Palestine were to be separate lands, making Syria landlocked. And Prince Faisal would act as Syria's administrator under the guidance of the French.

The following afternoon, a stunned Lawrence, who had spent the past two years of his life working to reach the city, would leave Damascus, never to return.

### 15. Despite their best efforts, Yale and Lawrence couldn’t prevent the mistakes of the Paris Peace Conference. 

Not long after the fall of Damascus, on October 31, 1918, fighting in the Middle East ended. Then, on November 11, Germany signed an armistice to end the war. But things weren't settled for everyone.

Since the proposed changes to the Middle East wouldn't be made official until the Paris Peace Conference in January of 1919, there was still a small chance for Arab independence.

So Lawrence and Yale lobbied for this cause as best they could.

Lawrence spent the months before the Paris Peace Conference pleading for Faisal Hussein to be able to lead his people without interference from the French, but eventually Lawrence was barred from the proceedings.

William Yale had also witnessed the final bloody days of the war — he, too, had been at Damascus to see the horrors of the Turkish Hospital and the broken promises of the British and French.

Yale tried to lobby the US representatives with his so-called _Yale Plan_, a proposal for sustainable peace and independence in the area, but it fell on deaf ears. Yale could see that the United States was already reverting back to its isolationist attitude toward world affairs.

Instead, at Paris, the Middle East was carved up: the British got Iraq and Palestine, and the French gained control of Syria.

William Yale would end his own memoirs by calling the Paris Peace Conference "the prologue of the twentieth century tragedy."

And indeed, the decisions made in Paris didn't end well: in 1946, the French abandoned Syria, leaving in its wake an ongoing civil war. In 1952, the British were ousted first from Egypt and then, in 1958, from Iraq. These once imperialized lands would continue to harbor deep resentment toward Britain for years to come.

> "Ever since, Arab society has tended to define itself less by what it aspires to become than by what it is opposed to…"

### 16. Lawrence spent the rest of his life trying to cope with the betrayals and horrors of WWI. 

Lawrence was disgusted with the results of the Paris Peace Conference. His mother said that he would spend entire mornings sitting in the same position, staring into space.

In fact, Lawrence spent much of the remainder of his life trying to distance himself from his past.

When he returned to civilian life, he changed his name to John Hume Ross and later Thomas Edward Shaw. And he joined the Royal Air Force, working in a number of low-level positions, never again wanting to carry any major responsibilities.

In 1922, Lawrence hand-printed eight copies of his memoirs, entitled _Seven Pillars_, for a small circle of friends.

Word of his book got out, and he printed 200 more copies. Eventually, the book became so popular that Lawrence allowed the book to be printed for the mass market. However, with each new version, he made heavy edits, removing and altering certain details about the violence and troubles he endured.

Those who knew him recognized his bouts of suicidal depression and refusal to speak of certain aspects of the war as signs of _shell shock_, or what we'd call _post-traumatic stress disorder_ (PTSD) today.

Perhaps the most telling example of Lawrence's deep regret about the outcome of the Arab Revolt was his trip to Buckingham Palace.

On October 30, 1918, he was summoned to Buckingham Palace to be made a Knight Commander of the Order of the British Empire.

But when it came time to kneel before King George and be honored with knighthood, Lawrence informed the king that he was only there to personally refuse the honor.

Lawrence then turned and walked away. It was an unprecedented gesture that left the King stunned, holding on to the medal he was to give Lawrence.

Sadly, Thomas Edward Lawrence died in a motorcycle accident on May 13, 1935, at the age of 46, after losing control of his motorcycle while trying to avoid two young boys on bicycles.

### 17. Final summary 

The key message in this book:

**Amid all the tragedies of World War I, the events in the Middle East have arguably had the most enduring impact. When T. E. Lawrence helped start the Arab Revolt, he tried to prevent the imperialist machinations of France and Great Britain from laying claim to foreign soil. Three other men also played their parts on the Middle Eastern stage during the War, but in the end, neither them nor Lawrence could prevent what happened after the fighting stopped.**

**Got feedback?**

We'd love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Scott Anderson

Scott Anderson is a journalist who has specialized in reporting from war-torn areas such as Lebanon, Israel and Bosnia. His work has appeared in _Esquire_, _Vanity Fair_ and _The New York Times Magazine_. His other books include _The Man Who Tried to Save the World_ and _The 4 O'Clock Murders_.

