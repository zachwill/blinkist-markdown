---
id: 599c34f5b238e10006a126bd
slug: lawyers-liars-and-the-art-of-storytelling-en
published_date: 2017-08-24T00:00:00.000+00:00
author: Jonathan Shapiro
title: Lawyers, Liars and the Art of Storytelling
subtitle: Using Stories to Advocate, Influence, and Persuade
main_color: 8AC6AE
text_color: 446155
---

# Lawyers, Liars and the Art of Storytelling

_Using Stories to Advocate, Influence, and Persuade_

**Jonathan Shapiro**

_Lawyers, Liars and the Art of Storytelling_ (2016) reveals the unexpected relationship between screenwriting and the art of presenting a legal case. These are two disciplines that both rely on great storytelling skills and effective rhetoric. So with the aid of real-life examples and tips people in television and film, you'll find out how you can use the art of storytelling to craft compelling cases that will keep the courtroom riveted.

**This is a Blinkist staff pick**

_"I love all the quirks and oddities of human behavior showcased in these blinks, they make for great conversation!"_

– Ben H, Head of Editorial at Blinkist

---
### 1. What’s in it for me? Get the tricks and tools for telling winning stories. 

If you've ever watched one of the numerous TV shows about lawyers and trial cases, like _Law & Order_, _Boston Practice_ or _Ally McBeal_, you'll know that the law is a wellspring of plotlines. But have you ever paid attention to the stories the lawyers _in_ those shows tell to win their cases?

Writing a successful TV series and winning a case in court have more in common than one might think. They are both about winning over an audience, be it a jury or TV viewers.

Here we will gain insights from a lawyer who is also a TV writer. We will get the tricks of both trades and learn how to tell stories to both laypeople and law-people.

You'll also learn

  * what the rhetoric triangle is;

  * why just telling a story with emotion isn't enough; and

  * how to use logic in three steps to make a winning argument.

### 2. Storytelling, though not taught in law school, is one of the most important skills a lawyer can have. 

To be an effective lawyer, you need to be able to convince a judge or jury to see things the way you see them. The best lawyers do this by crafting a compelling narrative, thereby winning over jurors and judges and helping to establish trust with their clients.

But crafting a stellar narrative requires good storytelling skills. So, let's look at how a lawyer can transform their case into a great story.

Let's start at the beginning: When a lawyer first takes a case, it is her responsibility to study, analyze and understand every piece of evidence she can get her hands on. These pieces are the building blocks of the narrative.

The details of the evidence supply the characters, motivations, actions and conflict that form the plot and lead to a resolution.

Furthermore, having a solid understanding of storytelling won't just benefit the delivery of your argument in court; it can also help you acquire clients and even convince your boss to give you a promotion.

Despite its manifest benefits, the art of storytelling gets short shrift in law schools. In fact, there isn't one course in today's curriculum that teaches anything about storytelling.

Instead, students learn about legal writing and research skills — dry, technical tasks that require no creativity.

In short, it's usually up to the lawyers themselves to supplement their law school education and learn about the skills that will allow them to weave compelling stories.

The blinks ahead will explore these skills, starting with a look at the genesis of the ancient art of storytelling.

### 3. By incorporating a few techniques from the ancient Greeks, any lawyer can be a great storyteller. 

The roots of modern storytelling reach all the way back to ancient Greece and the works of the seminal philosopher, Aristotle.

Aristotle was the first to identify the three key elements of convincing storytelling: _ethos_, _logos_ and _pathos_, which, in English, translate to _credibility_, _logic_ and _emotion_.

Each of these elements make up one side of the _rhetorical triangle_, though how much emphasis you give to each individual element will change based on the material you're dealing with.

So, as a lawyer, you might be putting together an argument that has a particularly strong emotional component; however, if you want that argument to be successful, you can't overlook the other two.

For example, let's say you present the events in your case with rigorous logic, and you include emotionality by describing the passionate motives of your suspect. Well, you still need credible sources, such as a witness who can confirm the story's verity.

In fact, credibility is really at the heart of the legal profession.

It's why lawyers have to pass professional tests, such as _the bar exam_ before they can practice. These preliminaries exist so that clients and jurors can rest assured that the person arguing a case has an expert understanding of the law.

The effectiveness of witnesses also hinges on credibility.

As a lawyer, it's your job to show that the witnesses for the opposing side aren't credible and to make sure that your expert witnesses are credible beyond reproach, so that the jury is confident in your story.

Appearances also play a big role in establishing credibility and giving a good first impression.

If you want to erase any doubt about your credibility, you should maintain a sharp appearance and a tidy office, both of which are reassuring to clients. So stay away from any comparisons to Lionel Hutz by making sure both you and your office aren't disheveled and unorganized.

### 4. There are three ways you can infuse your story with logic and truthfulness. 

Any lawyer can find an expert witness able to provide fake credibility; logic, on the other hand, is impossible to fake.

You can think of logic as the bedrock of truth — a foundation on which you can build an unimpeachable narrative. There are three steps to building this foundation.

First of all, identify the parts of the event in question that you and the opposing side can already agree on.

Secondly, go back to before the event and find more undeniable facts that everyone can agree upon.

Finally, the third step to finding compelling logic for your story is to pay close attention to your opponent's story and identify any vulnerable parts that can be reasonably flipped around to your advantage.

This approach was used well in a Supreme Court case about whether or not strands of DNA could be patented.

In making his argument, the prosecutor made a slight mistake when he suggested, DNA, like gold, is something that occurs naturally in the world, so we shouldn't be allowed to patent it.

The defendant spotted this opening and flipped this flawed statement on its head. He explained how DNA is also like a prescription drug; both are a collection of molecules and prescription drugs have already been patented.

By taking advantage of his opponent's flawed logic, he won the case, and the strand of DNA in question was allowed to be patented.

Furthermore, the first two steps aren't only great legal techniques but fantastic writing techniques as well.

For instance, in one script dealing with legal procedures in the aftermath of 9/11, the author began with what everyone agreed upon about the event: that the United States was deeply shocked and eager for justice.

He then took the second step, going back to the events that occurred well before the attack. After studying Japan's attack on Pearl Harbor and the subsequent internment of Japanese-Americans, he developed a similar story about post-9/11 America and the frightening idea of "CIA black sites" to house suspected terrorists.

### 5. Emotion is the final ingredient to a compelling story. 

While emotion is a crucial element to every story, today's public figures have shown that it can easily be overused.

This is especially the case for politicians, who have grown to rely too much on the emotional factor when trying to connect with their constituents. As a result, their messages are often a jumbled mess of feelings unsupported by logic.

Nevertheless, emotion is still a vital part of a storyteller's toolbox, as long as it is balanced by the other elements.

A good use of emotion can be found in the case of Kerry Kennedy, a member of the Kennedy political dynasty who was charged with illegally driving under the influence of a sleeping pill.

She was being defended by the lawyer Gerald B. Lefcourt, who found an effective way to make use of the jury's emotions.

After Kennedy had been called to the stand, Lefcourt asked her to provide information about her family background. She explained that she was raised by a single mother after her father had died. Lefcourt then asked her how her father had died, opening the door for her to explain how her father, Bobby Kennedy, had been murdered during his run for the presidency.

Suddenly, the whole courtroom was aware of Kerry's role in the tragic story of the Kennedy family. While it was completely irrelevant to the case at hand, the information subtly put the jury in her shoes without making a direct plea for sympathy. Four days later, Kennedy was acquitted of all charges.

It's important to remember that emotion, more than logic and credibility, can be the most dangerous to your story.

The author knows this from first-hand experience: As a young prosecutor, the author once dealt with a defendant who had beaten his daughter in public, and he was sure that he'd be able to use the jury's emotions to his benefit.

His strategy was to get the defendant to reenact the incident while the author stood in for the daughter.

But the scenario got out of hand when the author was accidentally struck in the face and his glasses went flying across the room. It was such an embarrassing mistake that a few of the jurors couldn't help but laugh — which was the exact opposite of what he'd wanted them to do.

Luckily, the incident didn't cost the author the case, even though, at the time, he was convinced he'd blown it. In the end, the jury found the defendant guilty.

Emotions are a powerful force, and therefore require a delicate touch.

### 6. Once a lawyer has a persuasive story, it’s time to convert it into a performance. 

Thinking of your case as a movie can be helpful in many ways.

First, you want to come up with the kind of convincing story that audiences will identify with. Then, you want to turn that story into a script. After that, you can find the actors to bring your story to life.

Though there are no strict rules for how a lawyer should script the story, there are some great examples to learn from.

Real-world examples abound, but there are also some fantastic films that offer incredible insight into how the story of a case can be scripted and delivered in a court of law.

A classic example is _To Kill A Mockingbird_, but you could also benefit from finding films that deal with a case that is similar to your own.

For instance, if you were trying to present a case that dealt with employment and labor law, you could watch a lawyer deliver a perfect script to a jury in the film _Bread and Roses_, by British director Ken Loach.

After all, you're giving a performance in court, so it makes sense to learn the techniques of the best performers out there.

There are also plenty of books that will tell you the secrets of successful lawyers, including how to hold your body and use your voice. But reading about these things doesn't compare to seeing an actor deliver a great performance.

The actors in both film and television know how to get the most out of a script.

Actor Michael Badalucco was a regular on the legal drama _The Practice_, and he made countless annotations in his scripts so that he could get the most out of every line. In dealing with a case involving police misconduct, he made a note, in all-caps, that read "IT'S DESPICABLE."

Notes like these weren't meant to be spoken, but they helped him stay authentic and reminded him of the message his character was trying to get across to the jury, which is a habit that you could benefit from developing as well.

Techniques like these are bound to help a lawyer give the most convincing performance possible.

### 7. The Final Summary 

The key message in this book:

**The best way to persuade audiences is not with legalistic mumbo-jumbo. It's far better to tell a story that adheres to the principles laid out in the Aristotelian rhetorical triangle. By combining, ethos, logos and pathos — credibility, logic and emotion — lawyers can create compelling narratives that are sure to convince both jurors and prospective clients.**

Actionable advice:

**Don't forget that the most convincing stories have both a hero and a villain.**

Although this is obvious in some criminal cases — murder trials, for instance — it's possible for even the dullest bureaucratic case to have a good guy and a bad guy, too.

For example, in a speech before a lowly California state commission on climate change, a citrus industry lobbyist made an extremely compelling argument for the use of industrial pesticides. How? By making mandarins out to be the hero and the government to be the villain. If the government blocked the use of pesticides, the noble mandarin would die out and people would blame the government!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Influence_** **by Robert B. Cialdini**

_Influence: The Psychology of Persuasion_ (1984) explains in detail the fundamental principles of persuasion that get us to say yes, including how they are used against us by compliance professionals like salespeople, advertisers and con artists. Knowing these principles will allow you both to become a skilled persuader yourself and to defend yourself against manipulation attempts.

**This is a Blinkist staff pick**

_"I love all the quirks and oddities of human behavior showcased in these blinks, they make for great conversation!"_

– Ben H, Head of Editorial at Blinkist
---

### Jonathan Shapiro

Jonathan Shapiro, Hollywood screenwriter, journalist and practicing attorney, is a jack of many trades. His writing credits for television include many popular legal shows such as _The Blacklist, Boston Legal_ and _The Practice._

