---
id: 5463378966346600083e0000
slug: lead-with-a-story-en
published_date: 2014-11-14T00:00:00.000+00:00
author: Paul Smith
title: Lead with a Story
subtitle: A Guide to Crafting Business Narratives that Captivate, Convince and Inspire
main_color: 4EAADA
text_color: 2B5D78
---

# Lead with a Story

_A Guide to Crafting Business Narratives that Captivate, Convince and Inspire_

**Paul Smith**

_Lead with a Story_ (2012) teaches you how to enhance your skills as a great leader by harnessing the power of storytelling. By taking examples from one of the most successful companies in the world, you'll learn how to craft a great story that motivates people and modifies their behavior.

---
### 1. What’s in it for me? Learn how to motivate and inspire people through the art of storytelling. 

The art of storytelling predates nearly every other human invention. It's no surprise either, considering the amazing power that stories have to totally capture our attention and excite our emotions, transporting us into strange and captivating worlds.

But stories aren't just pleasant ways to pass the time or get a thrill; they're also valuable tools. In fact, storytelling was among the first and best ways to impart valuable information and motivate behavior. And that's no less true today than it was during human prehistory.

These blinks are about the power of the story as well as the components that make them compelling. Here you'll learn why and how so many businesses are already using the power of storytelling to motivate your behavior, as well as what you can do to incorporate storytelling into your own life and career.

In these blinks, you'll find out:

  * why some speakers just put you to sleep;

  * why one runner's injury didn't stop him from finishing the race; and

  * how to tell a story that appeals to anyone.

### 2. Storytelling is an important aspect of any successful business. 

Question: Why is it that some of the longest-running shows on televisions are soap operas?

Answer: People just love a good story. Whether it's the drama in a soap or a riveting page-turner, we become completely captured by good stories.

But it's not only authors and screenwriters who have profited from the art of storytelling. In fact, a great deal of businesses have as well: Nike, Microsoft, FedEx and Costco each have their own _corporate storyteller._

Who exactly are these corporate storytellers and why do these companies hire them?

Stories are critical components of corporate messaging towards customers and employees alike.

For most of human history, storytelling has been the primary method for imparting knowledge. Before the printing press made possible the mass distribution of written materials, most information was shared via oral tradition in the form of stories.

Indeed, storytelling offers some unique advantages over other types of communication:

First, anyone can tell and learn from a story. A good story can transfix anyone, regardless of age or education.

In addition, stories are memorable. It's quite difficult to remember an isolated fact or a statistic. However, according to psychologist Jerome Bruner, if these facts are put into a story, we're 20 times more likely to remember them.

Finally, stories can appeal to every type of learner.

There are three types of learners: 40 percent of us are _visual_ learners, 40 percent are _auditory_ learners and the remaining 20 percent are _kinetic_ learners. Stories attract all three — a story's imagery influences visual learners, the vocabulary appeals to auditory learners and the emotions and feelings connect with kinetic learners.

As you can see, stories are a great way to impart knowledge, so much so that businesses have incorporated them into their strategies. In the next few blinks, we'll go into detail about the specific areas of business that can be improved with a story.

### 3. Capturing customer stories can greatly improve your customer service. 

When was the last time that you had a remarkable — or horrible — customer service experience? Did it motivate you to leave kudos for the customer service representative in the "how are we doing" box? Or maybe leave a scathing review on _Yelp_?

If you've ever left a review, then you've done that company a favor. In fact, these stories are a great way for consumers to share insights on how to get customer service right. Consider the story of Ray Brook, who was visiting Portland, Oregon for two busy days filled with meetings:

Not living in Portland, he needed a car and decided to hire one from _National Car Rental_. Once he got to the counter he was shocked to discover that his driver's license had expired a mere few days prior, meaning the company couldn't legally lend him a vehicle.

He was in trouble: how on earth was he going to make it to his meetings?

The next day, as he waited on his new license, National Car Rental employees agreed to drive him around — from his meeting to his hotel and then to another meeting. And they even drove him to the DMV to renew his license!

Naturally, Brook was astounded by the quality of service they had provided him and so he wrote a letter to the CEO of National, commending their actions.

Impressed, the CEO began using this story during speeches to his staff all across America. Brook's story of staff going the extra mile became the new standard expected of National employees.

Indeed, a good customer-service story is useless if no one knows about it. As a leader, you can capture a great opportunity to learn, simply by ensuring that customers have a place to record their experiences.

For example, you could create a "story box" on your website, give customers self-addressed envelopes to encourage them to share their stories or even just scour customer review sites to gleam stories about your company.

### 4. Often the best way to spread a company’s values and culture is through stories. 

How many businesses promise to "put the customer first" or declare that its employees are "the most important part of the business?" These hollow messages are a dime-a-dozen, and customers and employees alike are well within their minds to consider them utterly meaningless.

A company's values and culture are best spread through compelling stories, not vague slogans or hollow promises.

We can see a prime example of this from 2011, when the revolution in Egypt took a violent turn and foreigners living in the country began fleeing to the airports. However, due to the instability there, most flights from the country were being cancelled. Amongst those attempting to flee was Rasoul Madadi, an employee of _Procter & Gamble_ ( _P &G_), and his family.

Struggling to get out, Madidi called P&G. The company promised to do whatever they could, buying him and his family tickets for _five_ flights in order to ensure that flight cancellations wouldn't prevent him from leaving.

When one of the flights finally took off, the company ensured that Madadi would receive accommodation and supplies upon landing. They really _did_ put him first.

Stories like these are worth a million slogans like, "We value our employees." It _shows_ people a company's true values with actions, not just words.

Stories also help employees to understand what is _really_ expected of them. This is crucial, since these expectations sometimes differ from the actual rules. For example, companies may _say_ that they offer flexibility for employees who are expecting children, but people nonetheless often worry that they will end up on the losing side of the deal.

Stories are a great way to close the gap and increase employees' confidence in their company. For instance, P&G shares stories on their websites about new mothers who've used and benefitted from the company's flexible working policies as a way to clearly demonstrate to others that they have nothing to fear from taking advantage of this flexibility.

### 5. Use stories to forge strong relationships between diverse team members. 

Most of us who work for large companies don't know our colleagues very well. We might become true friends with one or two coworkers, but we likely won't get past such shallow topics as "Nasty weather today, huh?" or "Did you see the game last night?" with most of the team.

However, as a leader, you want your employees to build relationships. So how can you overcome this obstacle? One way is through stories.

Motivating people to share stories with one another can be the best way to build up strong relationships within a team.

Take the story of Jamie, for example, who, despite being the head of his team, couldn't seem to make real friends with anyone at work.

Then one day the team had a bonding session, in which they each had to share a story. Jamie used the opportunity to open up about his life, speaking about how his brother who had suffered from bipolar disorder and killed himself.

The story left most of his group in tears, and it brought them closer to him. They realized that he was more than just a co-worker: he is a human being, with depth and complexity.

His personal story helped transform him from a stranger with whom everyone shared an office into a relatable person. It created a strong bond between him and his team, meaning that he was happier at work and that his team was more willing to work hard for him.

Furthermore, stories can also help you build a diverse team. There's no debate in the business world that diverse teams outperform homogeneous ones, so it's important to diversify your team's skills and experiences.

Yet, building such a team can be difficult, as some people behave negatively towards others without realizing it. Stories, however, offer members of the team a means to share their discomfort in a way that can help others understand and change their behavior accordingly.

### 6. Rule books are no substitute for a good story when it comes to making and spreading company policy. 

Be honest: have you ever read through all the documents that outline the rules and regulations of your company? Chances are, even if you are in C-level management, that you haven't. So, how do you learn all the rules?

Perhaps the most obvious way is trial and error, simply doing what you feel is appropriate and then correcting your behavior after being reprimanded. However, while this may help a few people learn a few things, this method can only provide limited knowledge. After all, how many rules does the average employee actually break?

In order to bridge this gap between personal experience and company policy, leaders can use stories as a way to inform employees of what they should and shouldn't do.

For instance, when new P&G employees begin their employment, they are told the story of two workers who abused the company's free cafeteria service for trainees. Even after having worked for the company for a while, they continued sneaking in for a free lunch.

They got away with it for some time; security in the canteen wasn't very strict and no one had to show identification or prove that they were in training in order to enter. The company simply trusts that only those who are entitled to the service would use it.

Yet, in the end their desire to get something for nothing got the better of them. They visited the cafeteria so often that staff eventually became suspicious and called management, who caught the cheaters and had them fired.

So what lessons does this story teach? Employees learn, whether they've read the rule book or not, that they won't get away with taking advantage of the company's generosity.

All it takes is a simple, compelling story to make policy become clear and understandable.

### 7. Stories can be used to inspire employees, even when times are tough. 

Imagine that you are bringing a long, exhausting project to a close, and you want to start slowing down your efforts and moving on to a new, exciting project. Your boss, however, disagrees. What can she do to motivate you to continue and keep giving it your all?

She could spout nonsensical motivational phrases about "giving it 110 percent" — or she could tell you an inspiring story. We can draw some inspiration from history by looking at the famous story of the Tanzanian runner John Stephen Akhwari:

As Akhwari was running in an Olympic marathon in 1968, he suffered a fall and dislocated his knee. He could have easily given up right then, accepted medical treatment and left for home. But he didn't. He got back on his feet and, in agony, kept going.

An entire hour after the winner had crossed the finish line, Akhwari finally entered the stadium to the cheers of those who had stayed to the end.

After finishing, he was asked why he'd kept going. He replied: "My country didn't send me 5,000 miles to start this race, they sent me 5,000 miles to finish it."

Leaders can use his story, or similar stories, to inspire people to keep going until the work is finished, knowing that people will respect them and admire them for it.

Such stories can also help prevent people from jumping ship at the first sign of trouble. Managers can inspire staff to continue onward with stories of companies that faced misfortune, but that persevered and reached success.

For example, when P&G launched _Pringles_, sales were initially good but soon started to slip. While they could have scrapped the project in search of easier profits, they instead kept going. They made some improvements to their recipe based on customer feedback and relaunched the product, making Pringles one of the best-selling chip brands today.

Now that you've seen just how powerful stories can be, our final blinks will show you what constitutes a good story.

### 8. A successful story is comprised of only three ingredients: context, action and a result. 

At school, we learn that a story needs a beginning, middle and end. In more scientific jargon, you need _context_, _action_ and the _result_ (CAR). And what's true for schoolchildren is true for business as well.

Let's start with context:

The context stage of a story is crucial — without it, the audience doesn't know what's going on.

But what do you need to provide context for? A good place to start is _where_ and _when_ your story takes place. This helps the audience discern whether the story is true or hypothetical.

You also need to establish who your protagonist is as well as what she wants, and who or what the antagonist is.

When you're establishing context, it's important that you try your best to ensure that your audience can relate to your story as much as possible. The more the audience members can relate, the more they'll take notice, which means that you should probably leave out fanciful worlds with superheroes and kings.

With everything set up, you can then move on to the action. This is where your hero battles against her enemies who stand in the way of her goals. In your story, this might be an employee battling his boss in order to try out his revolutionary business strategy.

Here, you don't have to overburden your audience with details. You just need to grip them with a good mixture of success and failure for your hero.

Finally you're left with the result of your hero's actions: the conclusion to your story.

Who triumphs in the battle between hero and villain? This is where the audience members will learn the moral of your story: should they follow the hero's path to victory, or use his story as a precautionary tale?

### 9. The most effective stories play on people’s emotions. 

After all of these blinks we still haven't actually talked about what constitutes a story. No matter what the content of the story actually is, a good story hinges on its ability to evoke the emotions of the audience.

However, you don't want to evoke just any emotions. If you want your audience to become _motivated_ by your story, you'll have to appeal to the right emotions. For instance, it's easy to get people teary-eyed when you tell them a heartwarming story about puppies, but this won't be that useful to you unless you own a pet store.

We can take an example from Texas, which in the 1980s was ridden with so-called "litterbugs." The government tried its best to combat littering by appealing to emotions, publishing advertisements that showcased a Native American weeping at the sight of environmental destruction due to litter.

While touching, these ads had no effect. The worst litterers were people with little concern for the environment or minority groups such as Native Americans.

So, the government changed directions, this time producing an ad featuring Texas's greatest sports stars and musicians and connecting littering with damaging Texas itself. This time, they evoked the right emotions from their audience: the litterers were very proud of their state and their heritage, and hearing the anti-littering message from their heroes hit home.

Littering in Texas dropped 72 percent in the time immediately after running these ads.

But how do you know which emotional connections to make for your stories. Most stories will come from customer feedback, and the easiest place to find this feedback is in customer surveys.

P&G used this tactic in 2008 when they asked customers how the economic downturn affected them. They received many replies detailing people's fears about not being able to pay the bills or maintain their standard of living.

So, if they wanted to launch a product that would save people money, for instance, then these stories would have been very effective at evoking the right emotions.

### 10. Fill your story with surprises to pique your audience's interest and memory. 

You're at a conference and you feel your eyelids growing heavier and heavier as you listen to a speaker drone for hours. When he finally finishes, how much of the story do you expect to remember? Probably next to nothing.

For more memorable storytelling, inject your stories with a bit of _surprise._

If there is any sort of shocking action in your story, do your best to squeeze it into the beginning. For example, if your story takes place in a turbulent environment — perhaps during a revolution or on Wall Street during a stock-market crash — then use this information when you establish the context. This way, your audience will be transfixed from the very beginning.

You can also help your audience remember your story by adding surprise to the end. Surprise at the end sticks because of a brain phenomenon called _memory consolidation_ : whenever we experience something, our memories aren't formed immediately, but in the moments following.

During memory consolidation, it's possible to influence a memory's stickiness by attaching memories to certain stimuli. One such stimuli is adrenaline, which is released when the body experiences a rush, often brought on by shock or surprise.

Thus, ending your story with a surprise will help your audience members retain their memory of the story.

Sometimes putting surprises in the right places requires a little creativity regarding how you structure the story. Take this story about never giving up, for example:

At 22 he lost his job. At 25 he ran for state parliament, only to be defeated. Then, at 34 he aimed for a seat in congress, and failed again. At 45 he ran for the senate, was defeated and then he tried again at 49 but lost again. But two years later all this failures were forgotten when he became immortalized as our sixteenth President, Abraham Lincoln.

In this case, by withholding the name, we can create a straightforward story with a memorable surprise twist.

### 11. Final summary 

The key message in this book:

**Storytelling is one of the best ways to impart knowledge and motivate behavior, and as such is a key instrument to successful leadership. From more efficient customer service to defining your company's culture and setting strong values, storytelling is a versatile tool that can improve every aspect of your business.**

**Suggested** **further** **reading:** ** _The_** **_Story_** **_Factor_** **by Annette** **Simmons**

_The_ _Story_ _Factor_ explains what a powerful tool narratives can be, and unravels the art of telling your own stories. Stories are far more effective than mere facts or figures when you're making a presentation or trying to inspire people to take action.
---

### Paul Smith

Paul Smith is a popular keynote speaker as well as a corporate trainer in leadership and storytelling techniques. He is formerly an executive and 20-year veteran of the _Procter & Gamble Company_ and has authored two books, _Lead with a Story_ and _Parenting with a Story_.

