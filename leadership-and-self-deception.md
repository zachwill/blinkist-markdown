---
id: 530c53d334623100081b0000
slug: leadership-and-self-deception-en
published_date: 2014-02-25T08:00:00.000+00:00
author: The Arbinger Institute
title: Leadership and Self-Deception
subtitle: Getting Out of the Box
main_color: 2CA7DE
text_color: 1D6D91
---

# Leadership and Self-Deception

_Getting Out of the Box_

**The Arbinger Institute**

This book will show you how and why most people are in a state of self-deception where they view their needs as more important than those of people around them. It demonstrates the negative impact this self-deception has on our lives, but also shows a way out of this state, benefiting both our private and professional lives.

---
### 1. What’s in it for me? 

In many ways, today's society seems to reward selfishness and self-aggrandizement. Perhaps because of this, more and more people are starting to feel that they are superior to others and their needs are more important than those of the people around them.

_Leadership_ _and_ _Self-Deception_ explains that this mentality is in fact a form of self-deception, and very harmful to our private and professional lives. What's worse, it spreads like a virus and results in nothing but ineffective work and negative feelings.

The book explains how innocently we get infected with this virus of self-deception by simply ignoring our most natural instincts.

Finally, you will also find out how you can stop the vicious cycle of self-deception to become a better leader, a more effective worker as well as a kinder, more helpful person.

### 2. We deceive ourselves by seeing other people’s needs as less important, so we treat them like objects. 

All people want to be treated with respect and dignity by others. This idea is so fundamental that it is even manifested in our constitutions, laws and philosophies. But when it comes to everyday life, many of us forget this ideal.

In our daily interactions, we often feel that our needs and wishes are more important than those of other people. An example of this would be when we are sitting on a bus or plane, and instead of offering the empty seat next to us to others, we hope no one takes it, so we might enjoy more space. In effect, we value our own comfort above the need of others to find a seat.

Because we judge the needs of others as less real and important than our own, it is easy for us to start to think of them as mere objects. This is because our sense of superiority prevents us from seeing others as equals, so we no longer see a reflection of ourselves in others. In short, we begin to lack empathy, so we no longer see others as "real" people anymore.

This means that as we sit there on the bus or plane, we will probably see the other passengers as mere threats to us and our comfort, rather than as other human beings with their own needs.

This phenomenon is known as _self-deception_ or "_being_ _in_ _the_ _box._ " In this context, self-deception means that we do not see ourselves and the people around us as they really are. Hence we are deceiving ourselves.

You might say that when we are self-deceived, we are trapped inside a box, the limits of which distort our world view so we see other people as objects of little importance. To break free of self-deception, we must break free of the box.

### 3. Self-deception means we constantly seek justification for a worldview where others’ faults and our own virtues are inflated. 

In today´s selfish world, it might seem harmless to inflate one's own virtues and importance. But in fact, this act of self-deception is harmful and what's worse, it is a neverending vicious circle where one act of self-deception leads to another.

How?

We deceive ourselves by exaggerating our own needs and virtues while simultaneously magnifying the flaws and faults of others. This distorted view makes us more prone to blame others in times of friction.

For example, imagine you are arguing with your spouse about where to spend your vacation. If you are self-deceived, you will consider your own wishes as more important than your spouse's. If an argument ensues, you will also see your own behavior as more reasonable and measured than your spouse's.

The result?

You will blame your spouse for escalating the argument and for not respecting your wishes. Since you are self-deceived, you will probably not even be able to spot these flaws in reasoning.

But this kind of distorted worldview cannot stand on its own, as it will be inevitably challenged by reality. In order to stop it collapsing, we need to construct _self-justification_ for it. This means we actively look for and come up with excuses and reasons to bolster our worldview.

For example, when you blame your spouse for not caring enough about your wishes regarding your holiday destination, you need to find a justification for this view. You may then inflate the importance of your own needs. For example, you might think to yourself: "_I_ have worked so hard this year that I really need some rest and relaxation." This kind of self-aggrandizement prevents you from seeing that your spouse has every right to express their opinion as well.

### 4. You don’t have to behave badly toward others to be self-deceived; it is enough to have bad feelings toward them. 

We all want to be liked, respected and loved. Typically, one would think that the key to getting these things lies in our behavior: how we act toward others.

But in fact, the way others see us depends on something that runs much deeper than our behavior: our underlying feelings. This is because we are often able to sense how people feel about us, even if they do not show it.

For example, there have probably been times when someone has treated you nicely, but you've been able to sense that really, they were doing it just to extract some favor from you. We intuitively know if the behavior of the other person is not a true reflection of their feelings toward us.

In fact, we tend not to respond to what people are doing per se, but rather to how we perceive their feelings toward us while they are doing it.

For example, imagine you are having a ferocious argument with your spouse when you realize that you will soon be late for work. You quickly end the argument and give him or her a kiss. Though there is nothing hostile in the kiss itself, your spouse will surely sense your true underlying feelings, and probably respond negatively to your kiss.

Just as others respond to our feelings, not our actions, it is not our actions that determine whether or not we are self-deceived. You can act very kindly toward someone, but still deep down feel that their needs are inferior to yours. This means that despite your apparent kindness, you are in fact self-deceived.

If you think about it, any behavior can be manifested either from inside the box of self-deception, when it is driven by feelings of superiority, or from outside the box of self-deception, driven by feelings of equality. It is this difference in feelings that determines how you view others, not their behavior per se.

### 5. The need for self-justification of our distorted worldview is ineffective and even destructive. 

We have seen how self-deception affects our perception of others. But it also negatively affects our own motivation and priorities, leading us to lose sight of what is really important.

This is because when we are self-deceived, we must constantly look for self-justification to support our distorted worldview. This means we are not focused on what we want to achieve, be it at work or at home.

For example, at work you often need to work together with other people to get good results. If you are self-deceived, you will try to undermine your co-workers' ideas and achievements in order to justify your view that they are inferior to you. This means that the real goal — generating good ideas for your employer company — will not be your main concern anymore. Therefore self-deception can stand in the way of being productive and achieving results.

Another way in which self-deception hurts us is that we actively seek out and even provoke faults in others so as to justify our own low view of them.

For example, a mother who feels that her son stays out too late might set an unreasonably early curfew for him. In reality, she fully expects him to break it and thus justify her mistrust and the negative feelings about him. So in fact she is trying to provoke the very behavior she is upset about.

Finally, our search for self-justification also has an impact on our own personality, by diminishing the very virtues we feel make us superior to others.

For example, if we feel we are more knowledgeable than everyone else, we will probably not respond well to others trying to teach us something new, because of our self-justification for our inflated sense of wisdom. Therefore we actually hinder ourselves from learning new things and become less knowledgeable.

So in conclusion, if we are self-deceived, our search for self-justification harms our relationship with others, as well as diminishes our effectiveness at our work.

### 6. Self-deception is contagious and reinforced by the self-deception of others. 

In our families and at work we seldom act autonomously. Interacting with other people is an inextricable part of life.

This means that our self-deception has an impact on those around us as well, and can in fact spread like a virus. This is because when things go awry, if we are self-deceived we see others as inferior to ourselves, so we tend to naturally blame them. This of course makes them feel like they are being treated unfairly, which makes them defensive. They naturally begin to emphasize our faults, while inflating their own virtues in order to feel better about themselves. This way they too become self-deceived.

When two people are in the box of self-deception, they both blame each other for mistreatment, and react by further mistreating the other person. This leads to a vicious cycle of mutual mistreatment.

For example, if you are in a relationship and always blame your partner in every fight and disagreement, you will begin to focus on your partner's faults so intently that you completely ignore your own mistakes and shortcomings. Your stubbornness and inability to see your own faults will eventually lead to your partner blaming you in disagreements, leading him or her to be blinded to their own faults as well.

As you can see, self-deception is like a virulent infection that is passed along to others through contact.

Due to the damage self-deception does to us and those around us, it is crucial to understand how we become infected ourselves and what the underlying causes for our infection are. In the following blinks, we will examine how and why this infection takes place.

### 7. When we stop ourselves from doing something that we wanted to do for another person, we betray ourselves. 

Humans are social beings, relying on one another and each other`s empathy to get through the trials and tribulations of life.

If we are mentally healthy and balanced, it is easy for us to feel empathy, because we see a reflection of ourselves in others around us.

Nevertheless, self-deception can easily creep into our mentality. The first step in this process is _self-betrayal_, which happens when we ignore our natural desire to help other people.

Every human being instinctively wants to help others. For example, imagine you wake up one night because your baby is crying loudly in the other room. Probably your first, natural instinct will be to get up and calm your baby quickly before your spouse wakes up, because you care about your spouse and don't want to disturb their sleep too.

If you do not follow this natural instinct to be kind and helpful, you betray yourself. You may for example start wondering why it should be you who gets up, when you also need your sleep. Whereas before you considered your spouse's need for sleep as valid and important as your own, you now begin to devalue it. You disregard their needs, and thereby betray your own natural instincts. This is the gateway to self-deception.

### 8. We must justify our self-betrayal, which leads us to self-deception and having negative feelings towards others. 

We all know that nagging feeling that arises when we feel we should do something for another person. By now you know that if you don't act on that feeling, you betray yourself. So how does this self-betrayal put you in the box of fully fledged self-deception?

Well, in order to justify your self-betrayal, you need to change your world view.

Coming back to the example of the baby crying at night: if you do not act on your desire to be helpful and kind to your spouse, you will feel a strong need to justify this inaction. Probably, you will start coming up with reasons and excuses for why you shouldn't be the one to get up and why it should be your spouse. You might think for example, that it is always you who gets up, or that you have something important in the morning so you should be allowed to sleep.

Searching for self-justification like this puts you on a path that leads to the box of self-deception. You are inflating your own needs and wishes above the needs of others, and this will lead you to blame them. In this example, you would probably start feeling angry at your spouse, blaming him or her for not getting up.

This connection between self-betrayal and self-connection can be seen in the fact that we do not develop negative emotions toward others because of the way the act, but because of our own self-betrayal:

When you first woke up to the sound of your baby crying, you had no negative feelings toward your spouse, you just wanted to get up and help. It was only after you betrayed yourself and made up excuses for your betrayal — thus becoming self-deceived — that your feelings for him or her took a turn for the worse. In this interval your spouse did not have the chance to do anything, hence your self-deception was purely about your own self-betrayal.

So self-betrayal leads to self-deception, and self-deception, as we have seen, is harmful both privately and professionally. Next, you will find out how you can stop self-betrayal in order to prevent yourself from succumbing to self-deception.

### 9. We can stay out of the box of self-deception by always acting on our instinct to help others. 

We know that when we betray ourselves by ignoring our wish to help others, it leads to self-deception. Therefore, if we can stop betraying ourselves, we can stop being self-deceived.

First of all, it's important to understand that we cannot get out of the box of self-deception by merely changing our behavior — for example, by trying to avoid others or finding ways to cope with our self-deception.

This is because changing our behavior or avoiding tricky situations does not actually change our state of mind. Remember, self-deception is not defined by what we do, but by what the underlying emotions are, so simply changing behavior will not solve the problem.

For example, if you're in a relationship and you're in the box of self-deception, because you don't consider your partner's feelings and needs as equal to yours, you might think it's a good idea to start avoiding certain topics of conversation that have led to arguments in the past.

But because your worldview is distorted, the quality of the relationship will not improve through this change in behavior. You will still feel your partner's needs are inferior to yours, which will result in mistreatment, leading to arguments again.

Instead, you must stop betraying yourself, and this happens the moment you start questioning whether you're any better than the people around you. When you do this, you will no longer resist your instinct to help them.

Without self-betrayal, you will see others as equal human beings with valid needs and desires, instead of as mere objects.

To keep this up, you must simply keep honoring that first instinct you have to help and be kind to others. You need to constantly monitor your feelings and instincts, especially when dealing with different people, as it is possible to be self-deceived toward some people, but not toward others.

To fully free yourself of self-deception, you need a long-term commitment.

### 10. Freeing ourselves of self-betrayal and self-deception benefits our professional and private lives. 

We have all experienced the liberating feeling of being outside the box of self-deception when interacting with some people. Be it in your private life or at work, the feeling of dealing with someone on a basis of mutual respect is a positive and inspiring one, and also a key to success.

To be a successful leader, you must be free of self-deception. Great leaders treat people as equals and with underlying respect, thereby gaining willing and loyal followers. Even if the leadership methods are strict or demanding, those being led will not resent the leaders as they do not feel that they are devalued.

Therefore as a leader, you have the responsibility to get out of the box of self-deception yourself, as well as helping others get out in turn.

This is because the more people are out of the box at a workplace, the more a culture of responsibility will emerge, instead of a culture where people blame each other. Without the need to seek self-justification for their self-deception, people accept their responsibilities and focus on working more efficiently and achieving results.

Also, in our private lives, refraining from self-betrayal and honoring our desire to help others makes life easier. When families and friends treat each other helpfully and as equals, they do not waste energy blaming others or justifying themselves, and are happier as a result

Getting out of the box of self-deception has so many positive effects that clearly we should all strive to achieve it. By bringing this culture of equality and respect to our families and to our workplaces, we can hopefully inspire others to get out of the box as well, thereby spreading positive effects further.

### 11. Final Summary 

The key message in this book:

**Many of us are in the box of self-deception: we consider the needs and wishes of others as less important than our own. This has all kinds of negative effects, harming our relationships, work attitude and ability to lead others. To get out of the box we must simply start following our natural instincts to be helpful to others.**

Actionable advice:

**Try to shift your mental focus away from others and onto yourself.** Instead of focusing on what others are doing wrong, try to think about what you can do right to help them. Also, don't worry about whether others are helping you enough, instead worry about whether you are helping others enough.
---

### The Arbinger Institute

The Arbinger Institute is a global consulting company comprising international and multidisciplinary scholars. It is recognized as leader in organizational culture, and its other books include _The Anatomy of Peace_ and _The Choice_.

