---
id: 550fed62666564000a020000
slug: leadership-blindspots-en
published_date: 2015-03-23T00:00:00.000+00:00
author: Robert Bruce Shaw
title: Leadership Blindspots
subtitle: How Successful Leaders Identify and Overcome the Weaknesses that Matter
main_color: E1512F
text_color: AD3E24
---

# Leadership Blindspots

_How Successful Leaders Identify and Overcome the Weaknesses that Matter_

**Robert Bruce Shaw**

_Leadership Blindspots_ is all about the unknowns that set us back — the gaps in our skills, knowledge and understanding that cause our business efficiency and problem-solving skills to falter. By following the techniques outlined in this book, you can effectively identify and eliminate your own blind spots.

---
### 1. What’s in it for me? Discover the potential weaknesses that can undermine any leader. 

Do you know what your biggest weaknesses are? Most of us have an idea of the areas in which we lack strength, and a great many of us will take steps to overcome this.

Yet the weaknesses we know about are not the ones that will derail our careers; it's the weaknesses we have no idea about — our blind spots — that do that.

These blinks highlight the common blind spots that blight business leaders of all types, including Steve Jobs, whose arrogance led him to be fired from the company he created. But is there anything we can do to avoid the shortsightedness caused by these blind spots? Luckily there is, and these blinks will explain all.

In these blinks you'll discover

  * how to stop a crisis from sinking your company; and

  * why we all need to behave like Undercover Bosses.

### 2. Blind spots are unrecognized weaknesses or threats that can undermine your success. 

They say that "pride comes before a fall." When you are at your most confident you are also at your greatest risk of a defeat or set back. Why is this the case?

Well, even when we feel we're at our most powerful, there are sure to be hidden _blind spots_. Indeed, even the most experienced leaders have blind spots that can cause them huge problems if they aren't careful.

Even a great visionary like Steve Jobs suffered from blind spots, despite his reputation for success.

Although effective, Jobs' leadership style angered many of his colleagues. For example, he would refuse to see any information that didn't support his views, and would simply ignore those who disagreed with him.

Worse still, Jobs had zero awareness that people harbored negative feelings about him. His belief in his own greatness shrouded his perception.

This blind spot eventually had massive consequences when he got into a quarrel with Apple CEO John Sculley. Jobs demanded that the board remove his rival, yet faced with the ultimatum of keeping either Jobs or Sculley, the board warned that it would be the divisive Jobs who would be packing his bags.

Leadership blind spots can have catastrophic consequences, and the more power a leader has within a company, the greater the consequences of her blind spot.

Take Ron Johnson, for example, who did wonders as a retail leader at Target and Apple before being snatched up by JCPenney as their new CEO. JCPenney hoped he would transform the company, and gave him plenty of power to do so. This was a huge mistake.

In his arrogance, he made sweeping changes, including deep price cuts. His confidence hid his lack of knowledge about the company's values and image, and ultimately resulted in a loss of $1 billion in the first year as JCPenney lost its traditional customers.

Clearly, blind spots can be severely damaging for your company and career. In the following blinks, we'll examine how you can identify your own personal blind spots.

### 3. Audit your mistakes and look for recurring weaknesses. 

The first step in correcting your blind spots is figuring out what and where they are. Luckily, there is one thing in particular that throws them into sharp relief: _mistakes._

Mistakes show us the areas in which we lack knowledge, competence and understanding, and thus represent areas which might be our blind spots.

It is therefore crucial that you analyze your mistakes, especially the ones you keep making, to uncover the blind spots that caused them in the first place.

The author, for example, has had a long and successful career as a consultant, yet his career is nonetheless riddled with mistakes. One such misjudgment was concentrating only on improving the results for his clients, i.e., executives, without considering how his services would affect the teams they led.

One team in particular was severely affected by his suggestions and later complained about their new workload, thus highlighting his error.

After learning about this mistake, he realized he had a blind spot: he wasn't considering the bigger picture when giving advice to business leaders. However, once he was aware of his blind spot, he was able to change his strategy and avoid problems in the future.

But your blind spots aren't overcome by the simple awareness that they exist. Often leaders will ignore blind spots, thinking they're trivial. Sometimes, fixing a blind spot requires a considerable amount of effort and change, and it's so much easier to stick with the status quo, which requires less effort.

But this attitude will inevitably lead to huge problems.

Consider Xerox's R&D group PARC, for example, which played an important role at the start of the computer industry. However, they tended to concentrate on short-term profits, failed to keep up with industry innovations, and never lived up to their early promise.

The company's senior management knew they were falling behind, but didn't try hard enough to change course, and were consequently overtaken by competitors.

### 4. Feedback is key to avoiding blind spots, so make sure you ask for it. 

Most of us don't like feedback, especially if it's critical. It's hard not to take it personally when someone trashes our ideas or strategies. Yet much as we loathe it, there is nothing more important to working well and avoiding blind spots than getting regular feedback from others.

The people close to you often see the weakness and threats to your career or business that you yourself, busy living your career and running your business, can't see.

This concept was codified in 1955 by two American psychologists, Joseph Luft and Harrington Ingham, who developed the _Johari Window_, a model which shows the differences between how others see you and how you see yourself **.**

According to the model, the fact that people have different vantage points from which to examine their environment makes it possible for employees to see weaknesses in their leaders that the leaders themselves are unable to recognize.

History is full of examples of employees helping their bosses find their weak spots.

Meg Whitman, who is now CEO of Hewlett-Packard, is a case study. Earlier in her career, she gave her boss feedback about his leadership style, telling him that he pushed his opinion on the team and that the team therefore had no confidence in or ownership of their work.

He'd had no idea! He was able to use this feedback to change his management style, become a better listener and a more understanding leader.

But you don't want just _any_ feedback. You want _honest_ feedback. That means you have to actually ask for it.

People don't often volunteer criticism _of_ their boss _to_ their boss. So leaders have a responsibility to ask for it. That way, even the most timid of employees will feel comfortable telling you what they think.

Another way is to consult a third party, such as an external consultant or an HR manager. Because the feedback is anonymous, it is therefore unbiased.

### 5. Blind spots aren’t equally bad, and some might not be that bad at all. 

Not all blind spots are equal: some might destroy you while others will merely set you back.

Blind spots aren't equally important in determining leaders' success. They exist in various areas of your responsibility, and depending on your role, some of these areas will be more important than others.

Having a blind spot in your market knowledge could be catastrophic if you're a marketing manager, but it could be virtually meaningless if you're the head of catering.

In determining whether a particular blind spot is actually worth addressing, it's helpful to reflect on the following analogy from Bill Gore, the founder of W. L. Gore & Associates, Inc.

Imagine you're engaged in a naval battle and your ship is being shelled. Suddenly, a shot strikes your bow. What do you do?

Well, if the shot has struck above the waterline, then there's little chance of sinking and thus little to be concerned about. If it struck _below_ the waterline, then you're in deep trouble.

Leaders should encourage their teams to analyze where their possible blind spots lie and do the same for themselves. If they're in a place that could sink the company, then it's time for swift action.

However, it is also important to recognize that not all blind spots are destructive. In the right situation, blind spots can even be helpful for those who are prone to indecision and fear.

Blind spots can give leaders a boost of confidence: without seeing the dangers ahead, they can plow forward with their idea without fear of failure.

Take Sara Blakely, who started with only $5,000 and became the youngest self-made female billionaire in history. Her blind spots — not thinking about risks and being overly tenacious — helped her overcome her inexperience and become wildly successful.

### 6. There’s no better way to learn about your own business than by using your own two eyes. 

The popular perception of the political class is one of a clueless elite, oblivious to the concerns of the rest of us.

This is true, but not only for politicians. Any leader, in fact, will lose touch with their team as they begin their climb up the slippery pole.

As leaders climb higher up the ranks, their knowledge about what happens below them dries up.

Toward the top, managers have to think more about things that affect the entire company, like overall business strategy, customer organization, public relations, and so on, and less about what actually goes on _in_ the company.

Leaders, no matter what level they find themselves at, can avoid this by spending time engrossing themselves in the business of the workers "below" them. It is vital that they are proactive, so that they never completely lose perspective.

This is the idea behind the popular TV show _Undercover Boss_, in which company leaders work the "lower" jobs in their own company in order to correct their perception of the comings and goings of everyone who isn't in the upper echelon.

As the show demonstrates, it's not enough for leaders to get this information second hand. They must witness with their own eyes how things really work.

Often, leaders get their information from concise, filtered reports. Yet the staff who edit these reports can't know exactly what is important and what isn't. What's more, they're more likely to modify or neglect information that could be damaging to their team.

It is for these reasons that Andrew Gould, former CEO of Schlumberger, established a successful new rule in his company: whenever he needed information, he never asked his leadership team, but instead asked the people who were directly responsible. This way, he ensured that he got the information he needed.

### 7. Look for contradictory data and subtle warning signs. 

One of our biggest weaknesses is our tendency to see only the things which back up our own point of view, and to ignore everything that contradicts it. This _willful blindness_ causes serious problems for leaders.

It is absolutely critical that you consider _all_ relevant data, and not just the information that backs up your viewpoint. You want to strive for objectivity, not bias. This means going beyond your comfort zone in terms of where and how you collect data.

Consider, for instance, that most companies measure their performance by their budget. However, this indicator is exceedingly narrow, and doesn't give you any information about the competition. Even if a company is raking in huge profits, it's entirely possible that its competitors have an even greater market share and even higher gains.

Examining budget performance alone could give you a false impression of the company's risks and opportunities. The solution would be to expand your sources of information in order to expand your perspective.

Good leaders have such a familiarity with diverse data that they've become experts at identifying the subtle warning signs that show that blind spots are developing. They know how to read between the lines, and understand gaps in the data, thus allowing them to act before problems arise.

When one of the author's clients made a proposal in a team meeting, one of his peers was unusually quiet. The leader became aware of her silence, and asked her about it. As it turns out, she had reasonable concerns about the proposal, but didn't dare express them out of uncertainty about how her criticism would be received.

Had the leader not noticed these subtle cues, then his peer's concerns might never have been raised!

### 8. Surround yourself with a group of trusted advisers and promote a culture of productive debates. 

A strong team can produce better work in greater amounts than one person alone. It's therefore in every leader's interest to select his team members carefully, and create the kinds of working conditions that maximize performance.

Crucially, having the right team can help leaders to overcome their blind spots in the long term.

Remember: the better you know someone, the better they will be at seeing your blind spots. For this reason, leaders should carefully select a team of trusted advisers, who regularly give specific and actionable feedback, and aren't afraid to be constructively critical.

After leaving Apple under a cloud in 1985, Steve Jobs worked for years with the same leadership team at Pixar. It included people like Ed Catmull and John Lasseter who challenged Jobs whenever a weakness in his leadership style emerged, helping him to make the best decisions possible.

Jobs later attributed Pixar's huge success to the carefully considered composition of the team.

Furthermore, a good leader encourages debates to productively explore differences and maximize his strengths.

By trying to avoid conflict, team members guarantee that they won't be able to critically examine business challenges. Thus it's important to have a diverse team and a culture of respect and constructive criticism, where important issues are discussed in detail.

The one-time head of Xerox's R&D group, Bob Taylor, established a culture of debate in his team. All activities within the group were presented in team meetings and discussed afterward. Taylor wanted his team members to openly criticize one another's ideas, but always be respectful.

Strengthened by this culture, the group was able to identify nearly all Xerox's technical weaknesses, and the company developed many outstanding innovations, such as the first pull-down menus, the mouse as a navigation device and laser printers.

As a leader, it's up to you to ensure that your team is diverse and willing to debate in order to help you identify and overcome blind spots.

### 9. Final summary 

The key message in this book:

**For one reason or another, there will always be gaps in our knowledge and performance — blind spots — that prevent us from doing our best work. Maximizing your leadership performance means identifying and overcoming these blind spots with the help of a solid team.**

**Suggested** **further** **reading:** ** _Leaders_** **_Eat_** **_Last_** **by Simon Sinek**

_Leaders_ _Eat_ _Last_ explores the influence that neurochemicals have on the way people feel and consequently act, and examines the discrepancies between how our bodies were designed to function and how they function today. Ultimately, we need true leaders to direct us back on the right path.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Robert Bruce Shaw

Robert Bruce Shaw is a management consultant who helps senior executives to improve leadership performance and manage strategic organizational change. He has written numerous articles on management as well as another book, _Trust in the Balance._

© Robert Bruce Shaw: Leadership Blindspots copyright 2014, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

