---
id: 59bf946cb238e100052dbd64
slug: leadership-bs-en
published_date: 2017-09-18T13:00:00.000+00:00
author: Jeffrey Pfeffer
title: Leadership BS
subtitle: Fixing Workplaces and Careers One Truth at a Time
main_color: FDEF35
text_color: 6E6817
---

# Leadership BS

_Fixing Workplaces and Careers One Truth at a Time_

**Jeffrey Pfeffer**

_Leadership BS_ (2015) looks at the dirty world of business executives to see what life's really like at the top of the corporate ladder. What we find is something quite different than the squeaky-clean image most motivational leadership gurus and CEO biographies will try to sell you. Discover what a nasty business you'll really have to get into if you want to become a successful leader in today's cutthroat business world.

---
### 1. What’s in it for me? Gain a new perspective on leadership. 

Twenty years ago, no one would have raised an eyebrow if you said that, to become the CEO of a successful company, you'd have to be tough and not always Mr. Nice Guy. That was then. Pick up a random business magazine today, and you'll learn that being a successful leader means being authentic, caring and empathic.

But is this really a good leadership style? Do you really reach the top by being a modest, authentic and generally nice person all the time?

These blinks take an alternative look at leadership and challenge the common assumptions of today's leadership literature.

In these blinks, you'll learn

  * what a "GE jerk" is;

  * why successful leaders aren't afraid to lie; and

  * why the number of administrators is on the rise in many organizations.

### 2. We’ve built myths around popular business leaders that prevent new leaders from succeeding. 

Business gurus are a lot like preachers. But instead of delivering comforting myths about a divine, benevolent being, they spread myths about heroic business leaders and the secrets of their ways.

These heroic stories have manufactured appealing images, reputations and legacies, but they fall far short of giving a realistic portrait.

Take the story of former General Electric CEO Jack Welch. The stories have painted him as a man who valued every employee, always instilled confidence in his subordinates and promoted long-term strategies and vision for GE. What the stories tend to leave out is the term "GE jerks," which former employees used to describe the kind of worker that existed under Welch's leadership.

Welch had what was referred to as a "rank and yank" policy: every year the bottom 10 percent of GE's managers would be fired regardless of their overall career performance. This made GE managers ruthless and unpleasant as they desperately chased results.

Other details that business gurus also tend to leave out include the pollution lawsuits filed against GE, the corporation's price-fixing schemes and the cases of fraud that went on during his reign.

When we create flawless biographies of figures like Jack Welch, it's really a detriment since people will invariably come to the conclusion that they couldn't possibly live up to their legacy. As a result, people don't even bother trying.

When author Michael Dyson wrote a biography of Martin Luther King, Jr., he didn't shy away from mentioning his adultery because he felt it was important not to mythologize our leaders. King's imperfections are what make King more relatable and human, and therefore more inspirational as a historical figure.

To realize that our heroes also have flaws is vital to our ability to feel as though we're able to follow in their footsteps and bring about social change in our community. We need to understand that great things can indeed be achieved by imperfect people.

On the other hand, presenting inspirational figures as flawless keeps real change from ever occurring — the problems persist and the gurus stay in business.

****

### 3. Self-confidence and narcissism can help leaders rise to the top and stay there. 

No one will accuse Donald Trump of false modesty. His businesses are all plastered with his name: Trump Casino, Trump Steaks, Trump Tower and so on. While this makes him the butt of many jokes, it doesn't change the fact that he's the 417th richest man in the world, worth an estimated $4 billion.

So it's safe to say Trump might know a thing or two about business leadership, especially when it comes to self-promotion.

Effective self-promotion is a way to draw attention to your positive qualities and speak highly of yourself, which is a skill that can get you hired and move you up the corporate ladder.

Research has shown that interviewers rate job applicants higher and recommend candidates who show an ability for self-promotion. This is because self-promotion is just another form of self-confidence, which is a quality that rubs off on other people and makes them feel better about themselves.

A 2012 study by the Berkeley Business School shows that both confident and overconfident people received more respect and achieved higher social status and influence within their groups. This was the case even when the members of these groups were aware that the person's overconfidence was unjustified.

Similar benefits have been shown to hold true for leaders with narcissistic tendencies, such as self-importance, arrogance and the belief that they should receive special or unique treatment.

A study of CEOs during the 2007 financial crisis discovered that while companies led by narcissistic CEOs suffered more at the start of the crisis, their personality led to increased chances of survival. In part due to their outsized self-confidence, narcissists have a stronger desire for action and risk-taking. This helped narcissistic CEOs steer their companies to recover from the crisis with greater success.

These characteristics have also been shown to benefit the biggest leaders: US presidents. After reviewing the performance of 41 former presidents, those with _fearless dominance_, which is a mixture of narcissism, glibness and guiltlessness, received the best evaluations of presidential leadership. This included their overall persuasiveness and ability to manage a crisis.

### 4. Worry less about authenticity and more about displaying confidence and poise, even if it’s phony. 

When athletes are exhausted, with cramping muscles and sore bones, they have to convince themselves to "play through the pain." Leaders are often expected to do a similar thing and block out their emotions in order to make the best business decisions.

This is another fact that gets glossed over in books about leadership, which often suggest the best leaders are always the most authentic ones. But in reality, successful leaders are generally putting on a show and displaying different versions of themselves.

Helen Rubin has published a number of biographies of legendary leaders and even gotten to know some of them. She believes that leaders gain their most successful qualities by acting. They rehearse the desired quality until it eventually sinks in and becomes natural; it would be hard to call such a quality "authentic."

This was the case for Andy Grove, former CEO of Intel, who would toughen up his shy managers by putting them through a workshop called "wolf school." Here, managers got comfortable with behavior like leaning in and shouting in someone's face.

All of this was designed to give managers a strong sense of confidence and the ability to convince one of Intel's hard-nosed directors of their brilliant ideas. Even if they didn't feel particularly confident, they were taught to act as if they were. The message was: fake it until you believe it.

This is the same sort of principle that was the subject of a 1979 sociological study. It looked at a number of jobs where employees had to display positive emotions, regardless of how they actually felt. The results showed that this can be psychologically demanding and stressful — but that it's also good for business.

If you're the CEO of a company that employs thousands of people, you'll probably be expected to come off as a smart and strong individual when you interact with employees. And the same would be true for an entrepreneur who's hoping to become that robust CEO, since the goal is to attract talented employees, customers and investors who see you as a confident and trustworthy leader.

### 5. Successful leaders use lies to reach their goals and make employees happy. 

As children, we're told that lying is bad, a sinful act. Many American kids are told to emulate the young George Washington, who was caught cutting down a cherry tree when he was six years old and made a famous confession: "I can't tell a lie. I did it with my hatchet."

Yet not only is this story most likely a fictional one; lying has also proven to be highly useful for successful business leaders, especially for those trying to gain and hold onto power.

In 2014, a study showed that people in positions of power lie more often than others and with greater ease. Researchers concluded that having power made lying less stressful since powerful people are less likely to be subjected to the consequences of dishonesty than those without power.

When describing Steve Jobs, an Apple employee used the phrase "reality distortion field" to describe Jobs's habit of inventing his own version of reality about Apple, its devices and success. When Jobs died, it was revealed that the FBI had files on him with quotes from several people who'd questioned his honesty. They felt that Jobs had a tendency to twist the truth and distort reality in an effort to reach his goals.

Another reason leaders use lying to their advantage is to smooth over difficult situations where conflicting interests might otherwise stand in the way.

Another 2014 study looked at 200 talent-management companies, and they found that 73 percent of them had decided that the best policy was to mislead their employees into thinking they had a chance of being promoted. Management found this to be a good way of establishing much friendlier relationships with employees.

Since people tend to see themselves as above average, this method takes advantage of a person's natural predisposition for wishful thinking.

Mark Twain was probably onto something when he wrote: "Truth is such a precious commodity, it should be used sparingly."

> The story about young George Washington and the cherry tree was likely an invention by one of Washington's biographers to promote his book.

### 6. Trust is an inessential trait in leadership as deals often need to be broken. 

One of the many perks of being wealthy and powerful would appear to be the ability to break your commitments if you no longer feel like doing something. If you've ever had to deal with keeping track of a powerful person's schedule, you'll be familiar with how quickly things can change.

Nevertheless, books on leadership will often promote the idea that establishing trust is an essential part of success. Yet the truth is, many businesses perform fantastically even when people have a very low opinion of the leader's trustworthiness.

In 2011, a survey of American workers found that only 14 percent considered their business leaders to be ethical and honest. Meanwhile, only 10 percent trusted their managers to make the right decisions in difficult times, and 7 percent believed senior management was telling the truth about their actions.

is a worldwide survey that gauges the trust people have in CEOs, and in 2014 that trust stood at less than 50 percent.

When leaders decide to change their mind and their plans, it's not necessarily a sign of evildoing. As any CEO will tell you, circumstances and conditions can change quickly and a CEO has to react accordingly.

It's common for organizations to break alliances with one company and start doing deals with its competitors.

In 2005, Jason Calacanis sold his company, Weblogs, to its rival AOL — and for good reason. In his experience, it's unwise to enter into an alliance work with a big corporation like LinkedIn or Facebook when to developing your ideas. In all likelihood, they'll steal your innovative idea and create their own version of a similar product, without so much as a thank you.

A recent management study took a closer look at contract breaching and found that actions by people aren't regarded in the same light as actions by companies. When an individual breaks the terms of a contract, it's generally seen as reprehensible, but when an organization does it, it's seen as an unavoidable fact of doing business — no big deal.

### 7. Successful leaders place more importance on staying in power and shifting the blame. 

There are more than a few differences between military leadership and business leadership.

In the military, officers care so much about their personnel that they often won't eat until everyone else has had their fill. In business, only a fool believes that their boss has any concern about how hungry they might be.

Business books love to suggest that the best leaders really care about their employees, but when it comes down to it, leaders use their power primarily for their own benefit — not their employee's.

In 1975, a sociological study was done to determine why the number of school administrators increases disproportionately to other positions. And it was found that, once administrators gain a certain amount of power, they use it to protect their job when times get tough.

So, in prosperous times, more administrative jobs become available, and in tough times, job losses will affect the more vulnerable bottom tier positions, with the administrators using their power to stay safe. As a result, the administrator jobs stack up while the others even out.

Expecting a leader to look after employees is naïve. It's wiser to expect leaders to use their employees as scapegoats for their mistakes in order to stay confident and self-assured.

The leaders of General Motors have spent years blaming their financial woes on the money they need to spend to keep union workers from striking. This would include health insurance, retirement and other agreed-upon benefits.

But the real reason for GM's problem was that consumers prefer their competition's cars — especially Toyota, which sold about 30 percent more than GM in 2004. But to admit this would require executives to accept the blame, which is something they'll work diligently to avoid.

### 8. There’s no reason for workers and employers to feel loyal to one another. 

There are different ways businesses will try to protect their prosperity and power, many of which may adversely affect their employees. A round of layoffs is probably the most distressing option, but others include relocating to cheaper offices and cutting back on supplies.

There's a message here that every employee can learn from: protect your own neck!

There's no reason to think you have to work hard and stay loyal to a company that doesn't care for you. In fact, it's pretty foolish to do so.

There was a recent study that checked in with students from a management school shortly after graduation, and then checked in again two years later. The students were asked how well they felt their jobs had lived up to the reciprocal expectations they had when first hired. Around 55 percent felt that the specific promises their job had made when they were recruited had been violated.

Reciprocity is a social and moral obligation between two parties to honor and repay one party's efforts. But this is a concept that means very little in the workplace.

A 2015 study highlighted the difference between workplace and personal reciprocity. When people were asked to imagine being picked up from an airport, either by a coworker or by a personal friend, they felt far more obligated to repay the friend than the colleague.

Psychologists have a theory as to why this is, and it comes down to the payment one receives for effort. And this works both ways: leaders don't assume employees are performing a favor by being loyal and working hard. They believe these efforts are being repaid with their paycheck, and no further reciprocity is required.

It's also worth noting that business transactions are generally forward thinking and not concerned with the past. So if someone is doing a favor, it's because they hope to get special consideration in the future.

### 9. You can learn a lot about the ugly truths of leadership by watching leaders in action. 

If you watch sports, especially soccer or basketball, you're probably familiar with _playacting_, which is when one player dramatically falls after receiving the slightest touch from the opposing player. This is a strategic move that the best players will perfect, since it costs them nothing and, if it works, it may earn them a free kick or a couple of free throws.

The best leaders are also familiar with the benefits of playacting. So if you want to know what's really going on, it's best to keep an eye on their performance and what they're doing, rather than what they're saying.

IDEO is one of the best product-design firms and they've done a good job of holding onto their talented employees, in large part because they promote a culture of _design thinking_. In order to come up with better designs, employees are encouraged to _watch_ how others solve problems instead of just listening to them talk.

This same principle has found its way to leadership training: aspiring managers and executives now pay attention to behavior traits as well as everything else. Thus, socializing and dealing with people outside of the office are now recognized as two of the most important skills for a successful leader.

So while the leadership industry might _say_ they adhere to religious and moral virtues, if you watch what they're really doing, you may see the immoral truth.

The classic book _The Prince_, by Machiavelli, remains a relevant depiction of someone using immoral actions in the name of the greater good and the protection of a virtuous society. This happens a lot in politics and leadership, where security and prosperity are routinely achieved through dubious means — so much so that the job is often about finding the least dubious path to success.

By most accounts, Steve Jobs was a bully, and being threatened by him was so commonplace it had its own name: "You've been Steved." But who knows whether, without plenty of Steving, Apple would have become the paragon of success it is today.

### 10. Final summary 

The key message in this book:

**The myths we've created about our most celebrated leaders are costing people their jobs and creating unhappy work environments. The inspiring recommendations in popular leadership literature are by and large bogus, because they don't take into account the reality behind social-science research, human behavior and what really goes on in the day-to-day functions of a CEO. It may not be pretty, but being a successful leader involves lying, breaking promises, rewriting contracts and misleading your employees so they'll stay motivated.**

Actionable advice:

**Use distrust to your advantage.**

You can predict a colleague's future behavior by considering how they have behaved in the past. Colleagues who have previously reneged on commitments or broken promises are bound to act this way again. So be distrustful of them if the data shows that their previous behavior reveals a certain trend. This can be more accurate than what they tell you or how they try to present themselves. So before you place your trust and future well-being in the hands of someone else, do your homework and see what they've been up to in the past.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading: _Power_** ** __****by Jeffrey Pfeffer**

Some people have it, and others don't—Jeffrey Pfeffer explores why in _Power._ One of the greatest minds in management theory and author or co-author of thirteen books, including the seminal business school text _Managing With Power_, Pfeffer shows readers how to succeed and wield power in the real world.
---

### Jeffrey Pfeffer

Jeffrey Pfeffer is a professor at the Stanford Graduate School of Business and has taught many classes on effective human-resource management. Today, he is considered one of the world's leading management experts, with published research on organizational power that goes back nearly 30 years. He is also the author of _Power_, which details how readers can successfully wield power in their everyday lives.

