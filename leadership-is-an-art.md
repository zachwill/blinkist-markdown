---
id: 57dbf49384aa58000368e7d0
slug: leadership-is-an-art-en
published_date: 2016-09-21T00:00:00.000+00:00
author: Max De Pree
title: Leadership Is an Art
subtitle: None
main_color: 29B0CC
text_color: 196E80
---

# Leadership Is an Art

_None_

**Max De Pree**

_Leadership Is An Art_ (1987) teaches you how to lead your employees in a kind, humane way. The best leaders know that empathy and elegance are the keys to workplace success, not tough talk and harsh discipline. When you look at leadership as an art form, you won't just improve your company but also make life better for the people you lead, too.

---
### 1. What’s in it for me? Learn how to become an exemplary leader. 

Bookshelves are full of leadership manuals. Yet _Leadership Is an Art_ stands out; here's why.

This book refuses to present leadership as a single-minded quest for short-term profits. It doesn't frame a company leader as someone who's main job is to manipulate employees aggressively into working as much and as hard as they can.

As these blinks show, leadership should be in the business of establishing and fostering fair, caring relationships as well as meaningful collaboration among employees. You'll learn how to motivate your employees to act as if _they_ own the company — not you — and why you should make this a goal.

In these blinks, you'll also learn

  * what it takes to be an _elegant_ leader;

  * what great benefits you earn when you listen to your employees; and

  * why it's a big problem if people in your company swear a lot.

### 2. A good leader guides employees toward pursuing a company’s shared mission. 

In the movies, the "evil boss" is a classic character. He's intimidating and rude, barking orders at subordinates from behind a big, heavy desk.

Good leadership, however, is the direct opposite of this caricature. Being a good leader is about being helpful, leading employees in a team with the ultimate goal of performing better.

A leader provides employees with _direction,_ which has three components: _values,_ a _vision_ and _goals_.

A leader should communicate a company's _values_. Is the firm family-friendly? Client-oriented? An employee needs to know a company's core principles, to stay on track with work and goals.

Leaders also need to be clear about what a company is working toward: the company's _vision_.

Which aspects of the company need to change to keep up with the times? Which traditions should be preserved? Is the company targeting a wide range of customers, or catering to a specific group?

Finally, leaders have to outline a plan for achieving the company's vision. What are the specific _goals_ in doing so? Employees need to know what they're aiming for.

Good leadership doesn't stop here. A good leader also makes sure employees are personally invested in a company's mission, and actively seeks out ideas on how to improve the company.

One effective strategy for engaging employees is to offer them ownership shares in the company. Giving an employee stock in the company creates a win-win situation; employees are more motivated to generate profits, and in turn, the company thrives.

Strong leaders create a climate in which employees have the freedom to develop skills and ideas. The furniture company Herman Miller achieved success based on this concept.

Herman Miller has been encouraging employees to share ideas for enhancing company productivity since 1950. The staff earns a cut of any gains they generate, too. From 1987 to 1988, employee suggestions helped the company save some $12 million.

This strategy pays off, too: Herman Miller is regularly cited in _Fortune_ magazine's list of "most admired companies."

> _"A friend of mine characterizes leaders simply like this: Leaders don't inflict pain; they bear the pain."_

### 3. Healthy leader-employee relationships are based on respect and good communication. 

Leadership is about developing healthy, positive relationships with employees. So what's the best way to do this?

The first step is to remember that employees are humans with weaknesses and strengths, just like everyone else.

Remembering your employees' humanity is an important part of treating them with respect, a fundamental part of any healthy relationship. It also reminds you to show employees the same kindness, no matter their position in the company hierarchy.

Every employee has the right to be treated well, feel included and have a _covenantal relationship_ — a relationship in which the employee is appreciated for who he or she is.

Focus on the unique strengths and skills that each employee contributes, and you'll find it much easier to manage workers and get them to collaborate well together. Don't fall into the trap of comparing employees to some ideal you think they _should_ live up to. Accept them for who they are.

Doing so bolsters your employees' confidence and makes them more comfortable with sharing ideas to make the company better. Employees will be more open about their skills and shortcomings as a result, so it'll be easier for you to assign them to teams and give them certain tasks.

Another key component of strong employee relationships is communication.

To maintain good lines of communication, a leader needs to listen carefully to what employees have to say. What's more, make sure employees have all the information they need about the projects they're working on. It's better for employees to have too much information rather than too little.

Finally, be sure to word your ideas and thoughts clearly, so employees can always follow you. Remember to be compassionate and use kind words whenever you can.

### 4. Change can be beneficial in a company, but employees need to be involved in the process. 

Change can make many people uncomfortable, but we all know that it's an inevitable part of life. The business world is constantly in flux, and a company can keep up only if it adapts to changing circumstances.

Change isn't just about survival, however; it fosters creativity and can make work more fun, too!

Everyone is forced to work in new ways when a company undergoes a big change. This event can stimulate employee creativity, providing people with new opportunities. If your company is expanding its product range, for example, this might be your chance to work on that idea you've been mulling over for years!

In short, change isn't something that you or your employees should fear. In fact, you should consider changing your company's hierarchies and roles in general.

In many companies, some people work only on developing product ideas while others are responsible for _implementing_ them, that is, turning those ideas into something a customer can buy.

However, it's better when employees have the freedom to switch between roles. They're more productive if they can work on whichever task they're best suited for at that time. If they're in a creative phase, for instance, they'll contribute more if they're allowed time to refine their ideas.

But a leader can't just switch to becoming an inventor, especially if he's unwilling to lose his position in the company hierarchy. Such a leader has to accept that someone else might be the person to implement his ideas.

When it comes to changing roles, it's important that employees feel valued. They have to feel like they're contributing to a project because of their specific skills. If you treat employees as if they're interchangeable, they'll lose interest in the work. Again, good communication helps here.

So make sure employees understand that you value them, no matter what role they're in. After all, your business wouldn't be a success without them!

> "_...we cannot become what we need to be by remaining what we are."_

### 5. Intimacy and inclusivity are an important part of leadership, even in a capitalist system. 

Good leaders don't just make employees happier. They have the potential to reform the profit-focused, impersonal capitalist world from the inside out.

Since their conception, capitalist systems have been _exclusive_ systems. Most employees are excluded from a company's production process; they take orders from the top. Many are poorly paid and don't benefit when profits increase. Thus, workers are often frustrated and unsatisfied.

Good leaders know that people thrive best in _inclusive_ environments, not exclusive ones. People are happier, healthier and more creative when they feel involved and appreciated.

That's why the best leaders favor _inclusive capitalism_, a system in which everyone plays a role in developing and maintaining the system itself.

Inclusive capitalism is based on _intimacy_. Relationships aren't just about contracts. Workplace intimacy means that you care about each of your employees and the work each person does.

Capitalist systems have traditionally been based on _contractual relationships_, meaning that everyone simply follows the rules set out in a contract. Contractual relationships are based on material interests, not a person's well-being.

_Covenantal relationships,_ on the other hand, are intimate pacts based on care. A waiter has a covenantal relationship with a customer when he truly wants that person to enjoy her meal, regardless of the restaurant's bottom line. Coworkers have a covenantal relationship when they genuinely enjoy working together in a team.

At its core, a covenantal relationship is about cooperation and not coercion. When you incorporate such relationships into your business, everyone wins. Employees are happier and more motivated, and customers are more satisfied, too.

> _"Intimacy is at the heart of competence. It has to do with understanding, with believing, and with practice."_

### 6. Listening attentively to employee concerns gives you a head’s up when things start to go south. 

Sometimes you just know things are going well at work; but other times, you can't shake the feeling that perhaps something is not quite right in the office.

Let's look at strategies for confirming your intuition in such situations.

If your company goes into a slow period, plenty of warning signs should be visible. You'll save time and money if you learn how to recognize these signs.

It's a bad sign if you find yourself spending more energy trying to control your employees, rather than giving them more freedom. Being over-controlling is counterproductive. You'll only stress yourself out, and diminish your employees' creativity and motivation.

So don't put the clamps on; talk to and listen to your employees instead. Work with them to figure out what the real problems are, and how those problems can be solved together.

You can also use this strategy to address other warning signs, such as excessive swearing or office tension. Remember employees need to be involved in problem-solving; you can't just give orders.

Another important pointer for keeping your team on track is to monitor everyone's performance, including your own!

The author does this by sending employees lists of requests and topics in advance before they discuss them at a meeting. That allows employees to prepare questions and consider issues ahead of time, so the meeting runs efficiently.

Such topics might include productivity, upcoming projects or even an employee's own goals for advancing her career or education.

It's important to know the personal goals of your employees so that you can match each person with a meaningful project. In doing so, you help make the employee's personal goals align with the company's overall goals!

> _"Both the people and the process should be directed toward reaching human potential."_

### 7. Great leaders lead elegantly, making decisions carefully and thoughtfully. 

What exactly is an "elegant" leader? A CEO decked out in a bespoke suit or new shoes?

No, those are just superficial things. Elegance in leadership goes deeper.

Elegant leaders know how to lead _smoothly_. They're not rushed or impulsive when it comes to making important decisions, such as agreeing to a new hire or developing a product.

Instead, an elegant leader makes decisions carefully and thoughtfully. She observes and analyzes a situation before taking action. She listens carefully to employees and develops a clear plan that takes employee interests into account.

An elegant leader even helps the company with the transition when she retires, by picking a good successor.

The leader will sort through candidates and choose one with great communication skills, a person the leader trusts will stay devoted to staff and the company vision.

When you pay this kind of careful attention to detail, you'll rarely make mistakes or grapple with the messy process of setting a company back on course. Caring about details is an important part of running things efficiently, and elegantly.

Great leaders also refrain from blaming employees when things go wrong. Such a leader never forgets that employees are human; the leader respects them for who they are and doesn't treat them like cogs in a machine.

Great leaders don't abuse their power, either. All employees deserve to be treated well, regardless of their place in the hierarchy. We're human, after all — workers and leaders alike.

The power you have as a leader comes with a lot of responsibility. Being a good leader is ultimately about using that power in a kind, effective and helpful way. Elegance in leadership isn't just about leading the company to success — it's about leading benevolently, as well.

> _"Elegant leaders always reach for completeness."_

### 8. Final summary 

The key message in this book:

**Great leadership is about being compassionate and humane. It's certainly not about barking orders at people who have less power than you do. So respect your employees and listen to what they have to say. Engage them in the company's progress as much as possible and assign them tasks they truly find meaningful. Leading in a humane way is better for everyone: you, the people with whom you work and the company as a whole.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Leaders Eat Last_** **by Simon Sinek**

_Leaders_ _Eat_ _Last_ explores the influence that neurochemicals have on the way people feel and consequently act, and examines the discrepancies between how our bodies were designed to function and how they function today. Ultimately, we need true leaders to direct us back on the right path.
---

### Max De Pree

Max De Pree is the bestselling author of _Leadership Jazz_ and _Leading Without Power._ For most of his career, he's headed _Herman Miller,_ a furniture company that his father founded.

