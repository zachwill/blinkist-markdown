---
id: 59d11246b238e10006d636c7
slug: leadership-is-half-the-story-en
published_date: 2017-10-01T23:00:00.000+00:00
author: Marc Hurwitz and Samantha Hurwitz
title: Leadership is Half the Story
subtitle: A Fresh Look at Followership, Leadership, and Collaboration
main_color: E6712E
text_color: 994B1F
---

# Leadership is Half the Story

_A Fresh Look at Followership, Leadership, and Collaboration_

**Marc Hurwitz and Samantha Hurwitz**

_Leadership is Half the Story_ (2015) presents leadership as a fluid state that people can and should move in and out of as circumstances change. By adopting such an approach, you can increase collaboration and boost success.

---
### 1. What’s in it for me? Discover a new approach to leadership. 

Traditionally, leadership is a top-down affair and those who follow are considered not only less skilled but unworthy. Though this way of thinking is still common, it's incompatible with the modern workplace. Work these days is fluid; employees constantly switch jobs and roles, while businesses face challenges that are too vast and technical for any one person to overcome.

That's why we need to adopt a new, flexible mindset that recognizes the need to shift leadership as the context changes. In these blinks, you'll find out how to do just that by empowering followers, fostering collaboration and being open to the best ideas, regardless of who proposes them.

In these blinks, you'll learn

  * why social skills are integral to the modern workplace;

  * how leaders depend on their followers; and

  * how you can increase innovation by embracing flexibility.

### 2. In the modern, team-oriented workplace, social skills and adaptability are key. 

It was just a few decades ago that leadership gurus like Jack Welch rose to prominence by peddling the idea that success is built on individualism, personal drive and self-reliance. But today, such a notion is almost inconceivable; the world of business has shifted and workplaces are more team-oriented than ever before.

In fact, a study conducted in 2012 found that, in the 1980s, just 20 percent of work happened in teams, compared to 80 percent in the 2010s. Thus, people entering the workforce in the twenty-first century are sometimes referred to as the "we generation." This generation knows that working together is the best way to produce good work.

This new focus on teamwork has made interpersonal skills absolutely crucial for modern employees. People who lack such skills find it practically impossible to effectively contribute to a team. After all, if you can't communicate well, listen to others or see beyond your own ideas, how can you contribute to a group effort?

But sociability isn't the only necessary ingredient in the changing workplace. Agility is also invaluable. Modern firms are notorious for rapid-fire corporate restructuring, and one must be able roll with the punches.

According to a study done by the labor statisticians Hipple and Sok, the average employee stays with the same company for about 4.6 years. Jumping from job to job has become the new standard. Indeed, 51 percent of people stay at a given job for less than two years.

That's why _interpersonal agility_, or the ability to adapt to new social contexts, is essential. If you're always changing jobs, you'll always be working with new colleagues, bosses and CEOs.

The ability to rapidly integrate yourself into a new culture is crucial.

### 3. By following a different model of work, you can reach new innovative heights. 

Now you know that the modern workplace moves faster than ever before and that success in this new climate requires juggling the responsibilities of teamwork as well as those of personal relationships.

Okay, so how do you effectively navigate this crazy tangle?

Through a new approach called _generative partnership_. This term describes a relationship that's designed for you and your teammates to attain results that go above and beyond the creativity and innovation that any of you could achieve on your own. Here's how it works:

Generative partnerships are founded on _leadership_ and _followership_. The difference between the two is pretty self-explanatory: a leader guides the team by framing tasks and setting the context within which the team works; a follower takes his cue from the leader, executing the tasks that the leader assigns.

Unlike traditional approaches, however, a generative partnership considers both of these roles equal and dynamic. A person can be both a leader and a follower, depending on the group's needs.

For instance, take the Beatles and their hit album, _Sgt. Pepper's Lonely Hearts Club Band_. While recording the album, each band member contributed his own ideas and songs. Whoever had the best ones would become the leader and the other band members would follow him.

Naturally, these roles shifted from song to song and resulted in a collaborative effort that produced an incredible, innovative album, making the already famous band immortal. Their success depended on throwing out any hard rules about who was in charge, and this allowed them to adopt a fluid dynamic — going with the best ideas and doing away with hierarchy almost completely.

> "If leadership is about setting goals for the team then followership is the complementary action: the pursuit of team goals."

### 4. Followership improves companies and develops leadership. 

For some people, the word "followership" is bound to have negative connotations. This should come as no surprise since we usually associate following with being underrated and underdeveloped. In organizational contexts, the word is barely used at all. But the concept of followership can add tremendous value to the work of any team, and it's also bound to have a positive impact on corporations.

Just take the work of the American researchers Philip Podsakoff and Scott MacKenzie, who studied the impact of followership on the performance of teams in various industries, from insurance to paper, pharmaceuticals to food service.

Their results show that, regardless of industry, there is a correlation between adherence to followership — that is, a state in which followers easily abide by the instructions and advice of their leaders — and significant increases in performance metrics. Customer satisfaction increases, as do sales and product quality. The study even found that followership increases the majority of performance indicators by 17 to 34 percent.

So followership is good for companies. It's also great at developing leadership. After all, you can't lead without followers.

Just take Derek Sivers, an American entrepreneur and the founder of CDBaby. During a TED talk he delivered, he spoke about starting a movement. To illustrate his point, he screened a video of a young man dancing on his own, while surrounded by onlookers. In the video, another bold person eventually stands up, joining the lone dancer. Soon enough, everyone is busting a move.

The point is, sometimes even a single follower can produce a leader. In the case of the dancing man, the courage of a sole follower was all it took to make all the others feel comfortable enough to get down, too. Without this follower, the lone dancer would never have been a leader at all.

### 5. Great followers get promoted faster, earn higher salaries and do more for their companies. 

The successful businessman and former General Electric CEO Lawrence A. Bossidy says that, over time, great followers do more for an organization than great leaders. Why is that?

Well, if the employees on the frontlines — the salespeople and customer-service representatives — are great at following, they'll have more successful careers. After all, success on the job is usually measured by rapid promotions and big bonuses, rewards that great followers easily obtain.

Just take a study done by Thomas Ng, Lillian Eby, Kelly Sorensen and Daniel Feldman, in 2005. It found that powerful followers rapidly climb the career ladder, earn higher salaries and receive more freedom to take initiative at work. They accomplish all this by demonstrating to their leaders that they can carry out the tasks they are assigned and by supporting, as well as advocating for, the decisions that leaders make.

Because of this reliability and loyalty, leaders seek out followership skills in their employees. For instance, a 2007 article called "Surviving Your New CEO," published in the _Harvard Business Review_ by Kevin Coyne and Edward Coyne, pointed out that traditional hiring criteria like technical expertise and leadership skills are out of date. Leaders today look for employees who are self-motivated, good at working in teams and supportive of the vision put forth by their boss.

Unsurprisingly, all these traits are characteristic of great followers.

And great followers are what employers want. For instance, before taking over as CEO of a subsidiary of Bell, the largest telecommunications company in Canada, Ramesh Lakshmi-Ratan spoke with each of his former colleagues, hoping to learn who would commit to his vision as head of the company. After these discussions, he selectively retained the people on his team who believed in, and were thus willing to follow, his chosen path.

### 6. Good leaders know when to step back and let others take control. 

Today, the primary role of leaders is to facilitate collaboration. And, as you've already learned, this job isn't necessarily done by the senior manager, but can actually be taken on by other, lower-level team members.

But knowing exactly when to transfer leadership isn't easy. In order to help you make this call, think of a basketball game:

In basketball, even the most talented players can't have the ball all the time. Situations inevitably arise where their path to the basket is blocked and a pass is the smartest choice. Just as star players need to learn when to pass the ball, the best leaders need to know when to hand off responsibility to other teammates who may be in a better position to score.

In other words, leaders need to know when they should lead and when it's best to pass that leadership to someone else. However, _when_ you pass responsibility to another person depends on the situation.

For instance, consider Kevin McKenna, the general manager of the Canadian multinational, Phoenix Contact. In meetings, McKenna is always an active participant, ready to learn new things and generate ideas for his team. But when the time comes to make a decision about which ideas to run with, McKenna always leaves the room, knowing that a manager's presence can disrupt the team's creativity. To put it differently, McKenna knows when he should step back and let others lead.

But how do you know when you or other leaders should take a backseat role?

Well, there is no sure answer to this question; it all depends on the situation. However, adequate training is an essential part of helping leaders learn when to stand aside. That's why it's essential for companies to provide their leaders and potential leaders with the highest quality coaching available. Only with training will a leader have a chance of knowing when to pass the ball.

### 7. Final summary 

The key message in this book:

**Followership tends to get a bad name, but actually it's an essential and valuable trait. Followers enable and produce leaders, and good leaders know when to let their followers take control. By adapting your vision of leadership, you can increase innovation and produce better results.**

Actionable advice

**Share knowledge with your colleagues.**

The next time you participate in a training or seminar, take a moment to share the knowledge you've acquired with your colleagues. Spreading this information will enable followers to do their jobs better, not to mention produce a growing set of leaders to guide the group.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Leadership BS_** **by Jeffrey Pfeffer**

_Leadership BS_ (2015) looks at the dirty world of business executives to see what life's really like at the top of the corporate ladder. What we find is something quite different than the squeaky-clean image most motivational leadership gurus and CEO biographies will try to sell you. Discover what a nasty business you'll really have to get into if you want to become a successful leader in today's cutthroat business world.
---

### Marc Hurwitz and Samantha Hurwitz

Marc Hurwitz is the cofounder of and Chief Insight Officer for FlipSkills. He has a PhD in cognitive neuroscience and lectures at the University of Waterloo.

Samantha Hurwitz is the cofounder of and Chief Encouragement Officer for FlipSkills. She is a leadership and followership coach, as well as a consultant and trainer.

