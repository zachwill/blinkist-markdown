---
id: 5e590d886cee07000673bbd7
slug: leadership-strategy-and-tactics-en
published_date: 2020-03-02T00:00:00.000+00:00
author: Jocko Willink
title: Leadership Strategy and Tactics
subtitle: Field Manual
main_color: None
text_color: None
---

# Leadership Strategy and Tactics

_Field Manual_

**Jocko Willink**

_Leadership Strategy and Tactics_ (2020) teaches you how to take the skills of a high-functioning Navy SEAL team and apply them to your workplace. You'll learn about practices such as Extreme Ownership, and find out why humility is better than arrogance. These tips will help you to leave your ego at the door and to remember that your team's success should always come before personal success.

---
### 1. What’s in it for me? Learn effective leadership techniques from an experienced Navy SEAL. 

Being a team leader can be fraught with challenges. You're often called upon to juggle multiple responsibilities, make tough decisions, and many must overcome their own insecurities. But fortunately, for most of us, being a team leader isn't a matter of life or death — and that's because we're not leading Navy SEAL platoons on high-risk special ops missions. But this is precisely what author Jocko Willink did.

In his experiences leading a Navy SEAL platoon, Willink used a full range of leadership skills. And he noticed that these skills could also be applied to other, less dangerous, professions, such as leading a business.

If you think the military is all about blindly following orders, think again. These are lessons in practicing humility, ditching your ego, and building relationships based on mutual trust and respect — they are lessons we can all learn.

In these blinks, you'll also find out

  * why doing the dirty work with subordinates can be a smart move;

  * why it's smart to let others come up with a winning plan; and

  * why too much praise can be a bad thing.

### 2. Sometimes you need to step back from the situation to see things clearly. 

Picture the scene. A team of Navy SEALs is on a training mission. They're tasked with storming an offshore oil rig. But things aren't going to plan.

As they approach the rig, the team finds themselves frozen in position, guns out, waiting for someone to make a call on how best to move forward. The problem is, no call is coming. There are too many places for possible enemies to hide on this rig and too few places for the SEAL team to find protective cover.

Time is ticking away, and all anyone can do is what they're trained to do in this situation — scan for targets. But when you scan for targets, all you see is what's in the sights of your gun. You miss out on the big picture. In order to move forward, someone from the team needs to detach.

**The key message here is: Sometimes you need to step back from the situation to see things clearly.**

Luckily for this SEAL team, they had someone who could step up and take charge: the author, Jocko Willink. He knew exactly what to do.

To get a grip on the situation, Willink stepped back, pointed his weapon up to "high-port" position, and had a good look at their surroundings. This move allowed him to see the full picture. He could see the obstacles that lay ahead and the path which the team should take. He could then take the initiative and give the order, "Hold left! Move right!"

Outside of his life as a Navy SEAL, the author has found that detaching from a situation is one of a leader's best tools. Whenever you feel like you're becoming overwhelmed by all there is to do, take a step back. You can even do this literally — take a step back from your desk or a conversation and take a deep breath. Lift your head up, look in both directions, allow yourself to let go of whatever emotions are going on and try to see and hear what's really going on around you. This will help you to become grounded with the present moment and will put you in a better place to make a more rational decision — one that is less influenced by highly charged emotions.

Later on in his military career, the author learned plenty of other lessons that can be applied to leadership roles outside of the armed forces. And in the blinks ahead, we'll look at more of these valuable tactics and strategies.

### 3. Good leadership stems from two fundamental components: the Dichotomy of Leadership and Extreme Ownership. 

So how can business leaders learn from the military?

You may have heard aggressive, militaristic terms like "take no prisoners" used in the business world. But when it comes to leadership, the best policy isn't one of aggression; it's one of _balance._

In his career as a Navy SEAL, the author had first-hand experience with bad leadership. In his second platoon, one team commander refused to consider any ideas that weren't his own. This kind of arrogance severely damaged team morale. It was only a matter of time before a new commander had to be brought in.

**The key message: Good leadership stems from two fundamental components: the Dichotomy of Leadership and Extreme Ownership.**

When the author talks about leadership to people, they're often expecting him to share a very straightforward approach to leadership. They expect an approach based on strict hierarchy, where people simply do what the boss says, no questions asked. But the reality of what makes a good boss — in the military or the business world — isn't as simple as that. There needs to be a _dichotomy,_ or balance, to good leadership. You can't be too aggressive or too hands-off, too talkative or too quiet, too much of a disciplinarian or too much of a pushover. The list goes on and on.

At its core, successful leadership is about creating strong relationships based on mutual trust and respect. Only by doing this can you lead a motivated and dedicated team to victory. It won't happen if you're inflexible and won't listen to your team's ideas. A successful leader is a respected leader, and for that to happen, the people in the team need to feel respected and protected as well.

When it comes to gaining this respect, perhaps more than any other strategy, the author believes in _Extreme Ownership_. Extreme Ownership means owning up to all problems, mistakes, and failed plans. Put very simply, this means taking responsibility for the bad things — without exception — that's why it's called extreme.

If a team member makes a mistake, shows up late or doesn't do their job, _you_ own it. After all, her failing reflects the fact that you haven't trained her properly. Or perhaps it means you haven't effectively communicated the importance of her role and how essential it is to the overall team's success.

Ultimately, the failure of a team project rests on the leader's shoulders alone. Plus, playing the blame game is never ever a good look for anyone.

### 4. The core tenets to leadership include humility and a willingness to pick up brass. 

There's a popular concept that suggests people are either born with certain skills and talents, or they're not. This belief is far from accurate. The truth is, anyone can get better at whatever job they find themselves in.

This is an important thing to keep in mind, especially if you suddenly find yourself leading a team, and feel overwhelmed with the responsibility. Or worse, you suffer from _imposter syndrome_, the feeling that you don't deserve to be in such a position.

If this sounds all too familiar, don't worry. It's far better to feel like you need to improve than to feel arrogant — like everything you do and say is untouchably perfect.

**The key message in this blink: The core tenets to leadership include humility and a willingness to pick up brass.**

First of all, what does it mean to pick up brass? **** This phrase comes from the largely unrewarding job of picking up all the bullet shells that are left on the ground after target practice. Often, leaders may feel like they're too good to take part in such a menial task. But few things are as effective in building respect as a leader who's willing to chip in on a mindless job that nevertheless needs to be done.

This doesn't mean you should always be volunteering to do the dirty work, but it is a good practice to get in the trenches every once in a while. It's both a sign of solidarity and respect. It also offers a chance to interact and bond with your team. A leader can learn a lot about the personalities and dynamics within the team by chipping in on the unpleasant tasks.

Always remember, being promoted to team leader generally means you have a higher rank than others, but that doesn't mean you're actually superior to anyone else. It certainly doesn't mean you should start acting that way. Leadership is about building relationships, and the best way to get the respect of your team is to stay humble and not pretend like you know everything.

People are far more likely to respect the person who displays a willingness to learn, asks questions, and asks for help, than the person who pretends like they already know everything there is to know.

### 5. Lead by empowering your teams and allowing for plan ownership. 

Within most team structures, there are job titles and positions that come with a higher or lower designation than others. This is especially true in the military. But that doesn't mean any one job is more important than another.

Part of a leader's job is to make sure that _everyone_ on the team knows how essential their job is to the success of the operation. There is a reason their position was created, just like there's a reason for hierarchies and rules within an organization. When someone fails to meet expectations or doesn't respect orders, it's often because no one has taken the time to explain how essential each role is within the hierarchy.

**The key message: Lead by empowering your teams and allowing for plan ownership.**

The Navy SEALs practice what is called _decentralized command_. This means that every team has its own leader, and within teams of eight to ten people, everyone is expected to step up and lead on occasion. Since all team members know their goal, they should be capable of making decisions that get the team closer to achieving that goal. In practice, decentralized command should be a consistent source of empowerment and motivation.

So, how does the team leader fit into this? Well, even though leaders give the orders, that doesn't mean they need to come up with the plans themselves. It's the leader's job to clearly explain the objectives of the current mission, but the team should be allowed to come up with the plan themselves. This way, the team has ownership of the plan, which is a powerful motivator for getting the job done.

Of course, teams can come up with less-than-ideal plans. This still doesn't mean you should impose your own ideas. Use your best judgement: **** If the proposed idea is 70 to 80 percent as effective as yours, then let them go ahead and make the best of it. If their plan is 50 to 60 percent ideal, ask some pointed questions that will allow the team to see the problem and make the right adjustments.

If the plan your team comes up with has no redeeming value, then it's a matter of telling them to go back to the drawing board and come up with something better.

### 6. Lead by using iterative decision-making, and refrain from solving every problem yourself. 

It's not easy being in charge. At any given moment there can be multiple decisions waiting to be made. Many leaders can feel the pressure to either go all in on a strategy, or to call it off altogether.

But leaders needn't operate with an all-or-nothing mindset.

**The key message here is: Lead by using iterative decision-making, and refrain from solving every problem yourself.**

For instance, let's say you received some intel. Your enemy target could be spending the night in an abandoned building about five hours away from your base. This intel isn't definite, there's no guarantee the target will be there, and there are dangerous threats in the area. But on the flip side, the reward for capturing this target is huge.

In any risky proposition like this, you may feel the need to be decisive, but that isn't always the best strategy. Instead, you can use _iterative decision-making_. This allows you to make a series of small decisions to explore the situation further. It allows you to slowly gain more information, before committing to anything.

If you received uncertain intel like the enemy hiding in the warehouse, you could make a series of small maneuvers that get your platoon closer to the target while minimizing the risk. After each step, you could set up a forward operating base and recheck the intel to see if there's been any changes. If nothing's changed, you can continue moving closer and checking in. If, along the way, the intel turns out to be bad, you can safely turn back. But by getting incrementally closer you'll lessen the chance of something going horribly wrong.

As is often the case, there are more than just two extreme choices. And when practicing a balanced, humble and empowering approach to leadership, you should also be mindful of your impulse to be the go-to problem solver for your team.

Being a problem solver is a great skill for any leader, but if you become too eager to step in, you'll end up taking away valuable growth opportunities for your team. So, if you know the solution to a problem, don't blurt it out! Instead, ask a leading question that can steer someone else to solve the problem on their own. In the end, this will be far more helpful than being the person everyone goes to whenever a problem arises.

### 7. Always put your ego in check, and learn when to dole out punishment. 

One of the most common problems in leadership is the ego. When someone steps into a leadership role, it's all too common for their ego to start getting in the way. Rather than leading their team to glory, their only concern becomes personal success.

The ego can become particularly troublesome when leaders need to manage a peer — or someone of equal status in the organizational hierarchy.

**The key message in this blink is: Always put your ego in check, and learn when to dole out punishment.**

Leading a peer can be challenging. After all, you'll want to ensure you're seen as being just as good as him. It's all too easy to jump in and start showing him that you're the one leading. But don't. Put away your ego and let him come up with the plan. It's the same tactics you'd use for leading subordinates. Whenever you're in doubt, it's always best to take the high road and think, what would I want to see from my boss in this situation? Remember, if you want trust and respect as a leader, you have to give trust and respect back to your peers as well.

In general, it's never good to be a micromanager. Micromanaging sends out the message that you really don't trust your team to execute its job. One of the only appropriate times to constantly check in is when you're dealing with someone who has repeatedly failed to improve their performance. In this case, it may be time to set a series of strict goals. Then frequently check in with the person to make sure they're being met.

This brings us to the subject of when and how much punishment should be given. As always, you should practice Extreme Ownership, when something goes wrong it should be seen as a reflection of your leadership first and foremost. But there are times when someone may be purposefully ignoring direction or disobeying the rules, even after you've clearly explained the reason for those directions or rules.

Ideally, there will be a policy in place that clearly states the type of punishment that comes when someone engages in willful disobedience. However, even with a clear policy, it's up to the leader to use her discretion. Ask yourself if there are extenuating circumstances and whether leniency should be used. If not, then follow the appropriate policy to the letter.

### 8. Good communication provides balanced praise, clear guidance and limits rumors. 

Okay, so we've looked at a lot of Navy SEAL leadership strategies — advice anyone can benefit from. In this final blink, we'll look at one more all-important skill: communication.

**The key message here is: Good communication provides balanced praise, clear guidance and limits rumors.**

Let's start with praise. How should a leader deliver praise? First of all, it's best to be specific rather than broad, and be wary of going overboard. Too much praise can actually cause people to ease up on their efforts.

So instead of telling the whole team it did a great job, be more specific and say something like: "Susan, great job on clearing out that room with all the obstacles."

When delivering praise, it's also a good time to restate the goals that still lie ahead. It's easy for people to rest on their laurels, so to keep motivation levels up, remind everyone that this is just one success on the road to bigger results.

It's also important to make sure no one gets left behind when you're providing updates. Imagine a long line of soldiers marching single file. The people at the front may know what's going on and be able to respond perfectly, while the people at the back can end up panicking if they're left in the dark.

Don't let this happen. Establish clear lines of communication with absolutely everyone. When people are left in the dark, that's when rumors start. Rumors can kill motivation levels and cause all kinds of problems with morale. So get ahead of the problem and keep people up to date at all times. And be truthful, even when something's gone wrong.

And be truthful and clear when you're giving orders as well. Tell the team why it's important that they do exactly what you say. If you don't have a good reason, this is often a sign that there may in fact be a better way to do things.

Finally, if someone doesn't follow orders, you can practice Extreme Ownership while trying to correct the situation by asking, "What could I have done to help you better understand your role in this project?"

In good times and in bad, your leadership role is all about the success and well-being of your team, and not about exercising your power. Always listen to what your team members have to say and choose your words carefully. By remaining calm and collected, you'll be setting the example for everyone else.

### 9. Final summary 

The key message in these blinks:

**Strong leadership starts with Extreme Ownership, which is accepting that any mistakes your team makes are a reflection on your ability to effectively lead — no exceptions. Leadership is ultimately about forging strong relationships based on mutual trust and respect. This means you need to show trust and respect to everyone on your team. You can do this by letting them take the lead on their plans to reach the goals you set for them, and by practicing clear and honest communication in good times and bad.**

Actionable advice:

**Don't be afraid to apologize.**

Some leaders may tell you that it's a sign of weakness to apologize. This isn't the case. In fact, refusing to apologize is more likely to be seen as a sign of insecurity. Offering an apology is part of Extreme Ownership and it can go a long way to earning the respect of your team when it's the obvious and right thing to do. We all make mistakes from time to time and refusing to own them will impress nobody.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Extreme Ownership_** **, by Jocko Willink and Leif Babin**

One of the most important strategies offered in _Leadership Strategy and Tactics_ is the concept of Extreme Ownership. In fact, it's so important that the author devoted a whole separate book on the subject, which is why we recommend checking out our blinks to _Extreme Ownership_.

You may be thinking that taking responsibility for every one of your team's mistakes sounds impractical? Well now's the time to find out why Extreme Ownership is so effective at building strong working relationships, motivating devoted employees and boosting team morale.
---

### Jocko Willink

Jocko Willink ended his twenty-year career with the US Navy SEALs as a special operations unit commander. Following his retirement, he went on to be the co-founder of the leadership and management consulting company, Echelon Front. His other books include _Extreme Ownership_ (2015) and _Discipline Equals Freedom_ (2017).

