---
id: 555b06d16135330007370000
slug: lean-analytics-en
published_date: 2015-05-19T00:00:00.000+00:00
author: Alistair Croll and Benjamin Yoskovitz
title: Lean Analytics
subtitle: Use Data to Build a Better Startup Faster
main_color: FBA643
text_color: 946227
---

# Lean Analytics

_Use Data to Build a Better Startup Faster_

**Alistair Croll and Benjamin Yoskovitz**

_Lean Analytics_ (2013) offers key advice on how to successfully build your own start-up. It follows a data-based approach to explain how you can use effective metrics to help your organization grow.

---
### 1. What’s in it for me? Get the necessary tools to analyze your start-up. 

You have the brains, you have the drive, you even have a supportive network to get your start-up going. So what more do you need? The right kind of analytics, of course! No matter how smart or motivated, an aspiring entrepreneur should always be aware of that deadly pitfall: building something that nobody wants. By showing you how to use good metrics to assess your success, this book will ensure that your start-up is heading in the right direction.

So, what are the basic concepts you need to analyze your start-up? And how can you apply these concepts to make your start-up grow? These are some of the questions that _Lean Analytics_ sets out to answer.

You'll also discover

  * that some data can be good, but too much can be fatal;

  * how stickiness can drive the growth of your start-up; and

  * which number should matter most.

### 2. Start-up founders should be data-informed – not data-driven. 

What is a _start-up_, exactly? It's an organization that aims to build a sustainable and replicable business model. Perhaps you've dreamed of founding one yourself?

If so, you'll need to stay informed about _data_. Data, in addition to guiding you along your journey, makes it hard to delude yourself.

So what is data?

It's the numerical information that's vital to your business. If you run a media site, for example, you'll need data about ad-click numbers. If you're an investor, you'll need to know all the figures about the returns on your investments.

One reason that data is so crucial is that entrepreneurs often lie to themselves a bit when assessing their success. After all, they often need to convince other people (like investors!) of their ideas without having any hard evidence that these ideas will actually work.

However, if you believe _too much_ in your dreams, your start-up probably won't survive. You need to stay grounded in reality — and that's where data comes in.

Data is the antidote to self-delusion. By allowing you to soberly measure your success, it keeps you on track: you'll know exactly where you stand as you work toward your goal.

You shouldn't become a robot that just follows the numbers, however. Your personal judgment is important too! You don't want to be _data-driven_ ; instead, _s_ tay _data-informed_.

Though data is undeniably important, collecting and analyzing it can become addictive. Moreover, you'll run into trouble if you use data to optimize just one part of your business.

Imagine, for example, that you run a website, and your data shows that pictures of scantily clad women increase your click-through rate. If you just blindly follow that by filling your page with models in bikinis, you might undermine your business's image or integrity.

So don't become a slave to your data. Remember: data is ultimately just another tool.

> _"Instincts are experiments. Data is proof."_

### 3. Good metrics are ratios that are both comparable and understandable. 

So what's the goal of staying data-informed? Ultimately, your data should guide you to the right product, and the right market, before your money runs out. But you also need to find an effective way to measure your success. That means finding good metrics that provide you with relevant and meaningful data.

Good metrics have three important characteristics: they're comparable and understandable, and they're most effective as ratios.

A comparable metric tells you how things are developing. You should be able to compare a metric to different time periods, groups of consumers, or competitors. The phrase "increased revenue from last week," for example, provides more meaningful information than just "2 percent revenue."

A good metric is also understandable. Your data should keep you moving in the right direction, but if nobody can comprehend or remember it, it'll just turn into a burden. It won't lead to any positive changes in your organization. So keep your metrics simple, like "revenue per week."

The most useful metrics are also ratios. There are a number of reasons for this.

First off, ratios are easier to act on. If you're running a media site, for instance, you'll have information about your ad-click numbers. Ad click per day, however, is much more useful: it tells you if you're achieving your ad-click goals. You'll know if you need to improve on this or not.

Secondly, ratios are inherently comparable. They allow you to compare short-term metrics over a longer period of time. "Ad clicks per month," for example, is one metric. But the ratio of ad clicks per day over average clicks _within_ a month will tell you if your site is more popular at a certain time, or if viewers are starting to taper off.

> _"You want your data to inform, to guide, to improve your business model, to help you decide on a course of action."_

### 4. Start-up founders should concentrate on something that they’re good at, that they enjoy and that they can make money with. 

The key to thriving as a start-up founder is finding where the demand for your product and your ability and desire to answer that demand intersect.

So first, find a business you're truly passionate about. Building a start-up isn't just about creating something that works. Ask yourself: What do you really _want_ to do?

Don't start a business you'll end up hating, because if you don't enjoy what you're doing, you almost certainly won't succeed. Furthermore, investors typically look for founders who truly care about solving a specific problem. You won't attract investment if you're unenthusiastic and disengaged.

Second, make sure you're doing something you're actually good at. If you identify a real niche in the market, you won't be the only person trying to fill it. You need to be able to satisfy your market's demand better than your competitors.

And remember: Never start a business where everyone can compete with you equally. You need some sort of advantage, like a network of friends and contacts that will improve your chances of success.

Finally, be sure you can earn money doing what you do. After all, this is what starting a business is all about: getting people to pay you to do what you want.

That means you need to earn enough money from your customers for the product or service you're offering. You also need to do this without spending too much time or money acquiring them.

So be careful — don't waste your time building something you're uninterested in or that nobody wants.

### 5. A start-up goes through distinct stages: Empathy, Stickiness, Virality, Revenue and Scale. 

There are several ways to understand start-up development. The _Lean Analytics_ framework says that start-ups go through five distinct stages: _Empathy, Stickiness, Virality, Revenue_ and _Scale_.

In the first stage, Empathy, you identify a need that people have. This points you in the right direction: you'll know where your niche in the market is.

Next, in the Stickiness stage, you figure out how to answer your customers' need in an effective way that they're willing to pay for.

Then you enter the Virality stage. That's where you build the product, features and functionality that'll attract your customers.

Once you have a base of loyal customers, you start the Revenue stage, during which your business really grows and expands.

The Scale stage comes last. In the Scale stage, you'll start trying to break into new markets or expand, and your start-up will begin looking more like a larger company.

The Stickiness, Virality and Revenue stages all drive your start-up's growth. How?

Stickiness ensures that your customers keep using your product. You can assess your stickiness by measuring _engagement_, a key metric for predicting success.

Facebook, for instance, had low engagement in the beginning. They had few users, but they got them to keep coming back. Facebook soon became enormously sticky.

There's a metric for Virality as well. Its key metric is the number of new users each established user brings in. The more users your current users add, the faster your business will grow.

Facebook was designed to function better as it gained more users, so, as more people encouraged their friends to join, it went viral rather quickly.

Revenue, of course, is the ultimate metric for identifying a sustainable business model. Facebook now has a good one: it generates revenue by placing highly personalized ads on its users' pages.

### 6. Start-up founders should always focus on one metric above all the others. 

One of the keys to achieving success in your start-up is staying focused. That means you need to concentrate on the single metric that's most critical to whatever stage you're going through.

You should always know what your most important metric is at any given time. As a start-up founder, you'll have to keep track of multiple figures, like revenue per customer or customer satisfaction. Some of these numbers will matter immediately, and some you'll store for future use — when you present your company's history to an investor, for example.

Don't lose yourself in your business's data, however. Concentrate on the right thing at the right time. If you're running a media page, for example, don't stress out about your ad-click rates before you've attracted a sizable base of customers. Your ad-click rates won't matter before then.

The number you should be focusing on at the moment is your _One Metric That Matters,_ or _OMTM_. Your OMTM helps you set clear goals and measure your success along the way.

In the restaurant industry, for example, the ratio of staff costs to gross revenue is a great OMTM. It's simple, immediate, actionable and comparable: it's a single number you can generate every night; you can adapt your costs to it quickly; and you can easily track it over time and compare it with other restaurants.

You could set a clear goal by aiming for a ratio of 0.25, for instance. That would mean that each of your staff costs should produce four times the gross revenue. If you're below, maybe you're under-serving your customers. If you're close to this figure, you probably have a good balance between customer service and customer profitability.

> _"If there's any secret to success for a startup, it's focus."_

### 7. Start-up founders have to outline their business model and find the right customers. 

A lot of start-ups are based on truly creative ideas and some manage to attract large groups of people. Few start-ups actually make money, however. Even big organizations like Facebook and Twitter have struggled to make money from their user bases.

That's why your business model is so important. As a start-up founder, you need to define yours.

So what's the simplest business model? It would probably be a lemonade stand where you sell your lemonade for more than it cost to make it.

A real business model, on the other hand, is the description of your business's core concepts. It outlines the ways you'll get people to use your product. And keep in mind that your product is more than just the thing you're selling — it's also a mix of other factors, like your service, brand, support or packaging.

When you buy an iPhone, for instance, you aren't just getting a phone. You're buying a piece of Apple's character, too.

Business models work in a similar way. They're a mash-up of several aspects of the business — sales tactics, revenue sources, product types and delivery models. You'll have to carefully consider these different elements when you construct your business model.

Also, a good business model helps you separate your valuable customers from those that might be harmful.

Unfortunately, not all users are good for you. Some might only be good in the long term, like people who use a free, basic service and only sign up for a paid account after a few years.

Other users might never become paying customers, but could still provide free marketing or bring in other users who _will_ pay.

Some users, however, might only harm your business. They could distract you, consume your resources or spam your site. So make sure your start-up prioritizes and meets the needs of your most important users.

### 8. The best metric for an e-commerce start-up is revenue per customer. 

If you're like most people, you've probably bought several things online during the course of your life. E-commerce companies are very common nowadays. In fact, they're probably the most common kind of online business there is.

E-commerce companies focus on customer loyalty and acquisition. They usually make money by selling products they can deliver electronically, like music downloads on iTunes, or physically, like shoes from Zappos.

Some e-commerce companies are loyalty-focused, like Amazon. Amazon builds a strong relationship with its customers, and ensures they'll keep coming back, by offering them a wide range of products and keeping its services simple.

There are other e-commerce companies, however, that focus on acquiring new customers and making big, single sales instead of building loyalty. A website that sells second-hand cars and charges a fee for its services would be an example of this. That site wouldn't need its customers to come back — it would focus on one-time sales.

There are several metrics to use in e-commerce, but the most important is _revenue per customer_.

Revenue per customer is the combination of a few other figures. It includes your _conversion rate_, the percentage of visitors to your site who actually buy something. It also factors in the rate at which your customers return to the site to buy your products again. This particular figure tells you if you should focus more on building loyalty with current customers or on acquiring new ones.

Your _shopping cart size_ is also critical. That figure tells you how much your customers are actually spending.

Revenue per customer is the combination of these two figures. It's important for all kinds of e-commerce companies, regardless of whether they're focused on loyalty or acquisition or a mix of the two. Revenue per customer lets you know how efficiently your business is running.

### 9. Media sites make money through advertisements, so click-through rates are the most important metric for them. 

Google's search engine and CNN's website are both examples of media sites, and much of these sites' revenue comes from advertising. So if your business model is a media site, you'll probably be focused on earning money from advertisements, too.

Inserting ads into online content is fairly easy, and a lot of websites rely on the money from advertising to pay the bills.

Ad-based monetization can be a fallback revenue source, which subsidizes, for instance, a cheaply priced game, or it can pay for the operating costs of providing free content.

You can earn revenue from ads in a number of ways. Some sites make money from displaying banners, while others have sponsorship agreements or affiliate relationships.

For example, imagine a sporting news site that has a partnership with a local sports team, and a standing contract to display banners for it. The site might also sell sports books through an affiliate relationship with an online bookstore.

Media sites focus on their click-through rates because ad revenue is usually tied to the number of clicks an ad gets. That's why click-through rates or display rates are so important — they represent real money.

However, media sites have other concerns, too. They need to make sure their visitors stay as long as possible and visit a lot of pages. They also have to know how many of their visitors are returning and how many are new.

You can tailor your ads to suit these different figures. For media sites, advertising is everything.

### 10. Final summary 

The key message in this book:

**Start-up founders need to focus on what they're good at and enjoy doing. If you follow your passion and keep a data-informed mindset, you can effectively use metrics to move through the** ** _Lean Analytics_** **stages, reach your goals and bring your organization great success.**

Actionable advice:

**Question yourself.**

Before you start, ask yourself: Have you identified a problem that's worth solving? Have you got a good solution to it? Do you actually want to solve this problem? Is this something you're good at? Make sure you're in the right field and you have the right mindset before you start down your start-up path.

**Suggested further reading:** ** _The Lean Startup_** **by Eric Ries**

The _Lean Startup method_ helps start-ups and tech companies develop sustainable business models. It advocates continuous rapid prototyping and focusing on customer-feedback data.

The method is based on the concepts of _lean manufacturing_ and _agile development,_ and its efficacy is backed up by case studies from the last few decades.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Alistair Croll and Benjamin Yoskovitz

Alistair Croll is an entrepreneur, author and public speaker who focuses on web performance, big data, cloud computing and start-ups. He's also the chair of O'Reilly's Strata conference, TechWeb's Cloud Connect and Interop's Enterprise Cloud Summit. Ben Yoskovitz is an entrepreneur who serves as mentor to several start-ups. He also speaks at conferences and events like the Lean Startup Conference and the Internet Marketing Conference.

