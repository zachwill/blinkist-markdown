---
id: 516bc2c7e4b01b686deb9b20
slug: lean-in-en
published_date: 2013-05-13T00:00:00.000+00:00
author: Sheryl Sandberg
title: Lean In
subtitle: Women, Work, and the Will to Lead
main_color: DF2F40
text_color: C42938
---

# Lean In

_Women, Work, and the Will to Lead_

**Sheryl Sandberg**

Through a combination of entertaining anecdotes, solid data and practical advice, _Lean In_ (2013) examines the prevalence of and reasons for gender inequality both at home and at work. It encourages women to _lean into_ their careers by seizing opportunities and aspiring to leadership positions, as well calling on both men and women to acknowledge and remedy the current gender inequalities.

---
### 1. Despite tremendous strides, we are still far from gender equality. 

In today's developed world, women are better off than ever before, thanks largely to the women's movement in the past century. But though at first glance it may seem like the battle against inequality has been won, there is still much to do.

Consider compensation: In 1970, American women made 59 cents for each dollar men made in similar jobs. While that figure has risen, progress has been slow: in 2010, it was still only _77_ cents. As one activist noted wryly, "Forty years and eighteen cents. A dozen eggs have gone up ten times that amount." Nor is this problem unique to the U.S.: in Europe, the current figure is little better at 84 cents.

In addition to being monetarily undervalued, studies show that women's performance is also unfairly denigrated. When asked to assess the performance and growth potential of otherwise equal employees, both men _and_ women discriminate against women.

But surely this applies only to the ignorant and misogynistic, whereas we enlightened individuals would be fair?

Surprisingly, the same studies show that the more impartial the evaluator claimed to be, the more they actually discriminated against women.

This kind of "benevolent sexism" is far more dangerous than the overtly hostile kind, for the perpetrator usually has no idea how his or her attitudes hurt female colleagues and thus feels no compunction to reassess them.

At home too, inequality lingers. For example, it is broadly assumed that it is a woman's job to raise children. When asked whether they expected their spouse to step off their career track to raise children, 46% of the men surveyed said yes, compared to only 5% of the women.

**Despite tremendous strides, we are still far from gender equality.**

### 2. Women are still conspicuously absent from leadership positions, partially due to the leadership ambition gap. 

Nowhere is gender inequality more evident than in leadership positions: Just 20% of parliament seats globally are held by women, and only 4% of Fortune 500 CEOs are women.

These figures are striking because in the realm of academic achievement, women, on average, fare better than men, earning 57% of all undergraduate degrees and 60% of master's degrees in the U.S. Yet, somehow this flood of competent women entering the workforce becomes a trickle by the time they reach the leadership level.

Many factors contribute to this phenomenon, but one of the most important is the _leadership ambition gap_. Studies show that men are more ambitious and more likely to _want_ to become executives than women. Why?

Gender stereotypes are one driver: Women are not expected to be ambitious and career-oriented, and those who violate these expectations can be labeled as "bossy" or worse. These stereotypes, enforced since childhood, can pressure women to temper their career goals.

Similarly, whereas most men automatically assume they can have both fulfilling personal lives and successful careers, women are constantly told by society and the media that eventually, they will have to make compromises between career and family. This often results in women being less committed to their careers and leaving the workforce to care for their children. Surveys of Yale and Harvard Business School alumni found that some 20 years after graduating, only half the women were employed full-time compared to 90% of the men. With such a mass exodus of highly-educated women from the workforce, it is little wonder that a leadership gap exists.

**Women are still conspicuously absent from leadership positions, partially due to the leadership ambition gap.**

### 3. Let’s talk openly about inequality and work toward correcting it, together. 

We must be able to speak openly about gender and the disadvantages women face without this being seen as complaining or as demanding special treatment.

An open discussion raises awareness and encourages more people to come forth and address the issues. This should in turn inspire more women to lead and more men to want to be part of the solution and support women to lead.

Increased awareness can bring about small but critical changes that will help level the playing field. For example, a professor who becomes aware that women tend to be reluctant about raising their hands when the class is asked a question can start calling on students directly, thus evening out the chances of each gender to answer.

Women must also support each other. Discouragingly, this has not always been the case. Consider the "queen bee" phenomenon: Historically, only one woman per company could rise to senior status in the male-dominated corporate environment; hence, she felt threatened by other women and often actively _hindered_ their advancement.

Similarly, stay-at-home mothers may make working mothers feel guilty and insecure about their career choices, and vice versa, leading the two groups to needlessly criticize and discourage each other. For example, the first female officer to join a U.S. Navy submarine noted that while her male crewmates respected her, their _wives_ resented her intensely.

The push for gender equality must continue. Not only does it help society as a whole take advantage of the competence and leadership skills of half the population, but, as a study of Harvard students showed, equality actually raises the satisfaction of all parties involved, not just the direct beneficiaries.

**Let's talk openly about inequality and work toward correcting it, together.**

### 4. Women’s lack of confidence can hold back their careers. 

In addition to the many external obstacles that impede women at work, they often also face a battle from within: self-doubt.

Even the most competent professionals, including the author herself, can be plagued by _the impostor syndrome_ : feeling like your skills and success are fraudulent — and soon to be uncovered. Women tend to experience impostor syndrome more intensely than men and in general underestimate their own abilities.

Studies across a multitude of industries such as medicine, law and politics show that women tend to judge their own qualifications and performance as worse than they actually are, while men do the opposite and tend to be overly confident.

Similarly, men tend to attribute their successes to their own innate skills and blame external factors for their failures, whereas women credit their successes to external factors and blame their innate abilities for their failures.

These misperceptions breed further insecurity in women, and insecurity can hurt your career: It takes confidence to promote yourself at a senior job interview or to pull up a chair at an executive meeting.

Self-doubt can also cause women to forgo great career opportunities, because they consider themselves unqualified. However, in a fast-moving world, you cannot wait for perfectly tailored positions to open up; instead, you must seize the initiative, grab opportunities, and make them work for you. In short, you must _lean in_ to your career, not lean back or stand aside.

So what to do?

Though you can't will yourself to become confident, sometimes faking it can help. Acting and carrying yourself as if you're confident can often transform into genuine confidence.

We should also acknowledge that women are less likely to feel confident enough to reach for opportunities and therefore, we must correct for this through encouragement and support.

**Women's lack of confidence can hold back their careers.**

### 5. Careers are more like jungle gyms than ladders; aim for the top but be flexible in your route. 

Today, the _career ladder_ concept is inaccurate. People no longer advance linearly from entry level to executive in one company or industry. A more accurate image is a _jungle gym_ with multiple routes to the top.

This concept is comforting for people who, like the author, have no specific career plan after graduating college. On a jungle gym, you don't need an exact goal; you can try out the various routes available and see which ones lead you in the right direction.

To help you on this journey, you should plan for both the long term and short term.

A long-term dream doesn't have to be anything specific or even realistic, but it should help you decide what kind of work you care about. The author, for example, wanted to do _meaningful_ work, so she used this conviction to guide her throughout her career.

In addition, you should evaluate career opportunities based on the single most important thing they offer: _potential for growth_. When the author deliberated on whether to take a position at a then quite unknown Google, the CEO told her that she should only care about personal growth potential, which is largest in fast-growing companies: "If you're offered a seat on a rocket ship, you don't ask what seat. You just get on."

He was right, and you too should look for teams, projects and companies with great growth potential.

Along with the long-term goals, you should also set short-term (e.g. 18-months) goals. Include work goals _and_ personal learning aspirations. Ask yourself, "Where can I improve?"

**Careers are more like jungle gyms than ladders; aim for the top but be flexible in your route.**

### 6. Women must carefully navigate the razor’s edge of ambition and likeability. 

Even today, gender stereotypes color our perceptions of others: men are expected to be decisive and driven, women sensitive and communal.

A woman with a successful career violates her gender stereotype, which is why likeability and career success are positively correlated for men but negatively correlated for women. Competent, ambitious men are praised, whereas such women are described as "pushy" or "not team players." This is incredibly unfair, especially as likeability is an important factor for career success.

However, trying to fit into her expected gender role can also impede a woman's career, as it means being less ambitious and less prone to seizing career opportunities. This creates a "damned if you do, damned if you don't" dynamic.

Nowhere is this more evident than in the difficulties women have in negotiating for promotions or higher compensation. Such negotiations are absolutely vital for career advancement, but a woman advocating for herself is reacted to unfavorably — by both men and women.

Researchers have highlighted some special considerations for women navigating this minefield: You must try to come across as "appropriately feminine," i.e. nice and communal. Hence, try to soften your message by speaking on behalf of a group rather than yourself, e.g. "_Our department_ has had a great year" or "_Women_ usually get paid less than men."

Unfortunately, to overcome gender biases, women must also legitimize _the very act_ of negotiating, for example, by quoting industry compensation benchmarks or by mentioning that someone more senior, like a manager, suggested they negotiate.

One can hope that as powerful women become less of an exception, such acrobatics will no longer be required.

**Women must carefully navigate the razor's edge of ambition and likeability.**

### 7. To foster effective communication, practice and encourage authenticity and appropriateness. 

Authentic, honest communication is essential in the workplace. It strengthens relationships, allows unwise decisions to be challenged and helps people broach uncomfortable topics. Yet, many people, especially women, fear that speaking honestly at work can make them seem negative or unduly critical. Hence, they bite their tongue when in fact, their input is sorely needed.

This is why leaders must do all they can to encourage authenticity by asking people for feedback and suggestions, as well as by publicly thanking those who have been honest.

The key to effective communication in any environment is to mix authenticity with the appropriate consideration for other people's feelings: be delicately honest, not brutally honest. This is easier said than done, however.

Don't confuse appropriateness with beating around the bush. For example, don't say, "Though I have faith in your analysis, at this time, I feel uncertain about the possible downsides of your proposal," when you really mean, "I disagree with this idea."

Sometimes humor can be an effective way of broaching a difficult subject. For example, one Google executive was having trouble initiating an honest discussion with a seemingly hostile colleague until he asked her jokingly, "Why do you hate me?"

Rarely is there an absolute truth in any situation; hence, to communicate effectively, you must first try to see things from the other person's perspective. Try, for example, to reflect aloud on their position: "I understand you're upset about this because you feel..."

Also, when you make statements, try to start them with "I" rather than making them sound like absolute truths: "I feel we should…" rather than "You're wrong." The former helps start discussions, the latter disagreements.

**To foster effective communication, practice and encourage authenticity and appropriateness.**

### 8. Attract rather than accost mentors, and build a natural, reciprocal relationship with them. 

Many young professional women today seem near-obsessed with finding themselves a mentor — and for good reason. Senior executives who advise you and use their influence on your behalf are crucial to career advancement, regardless of gender.

Unfortunately, it is far more difficult for women than men to find such relationships. One reason is that most senior corporate leaders are men, and they feel uncomfortable mentoring young women due to the possible misinterpretations of such a relationship.

But another reason is that almost every seminar, blog post and article related to career development in the past decade has told professional women to find a mentor so they can excel, when in fact, they should excel so they can find a mentor. Research shows mentors choose their protégés based on performance and future potential; hence, a flat out "Will you be my mentor?" to a total stranger is unlikely to work.

Delivering an outstanding performance can definitely catch a would-be-mentor's attention, but it is not the only way.

Approaching a senior executive with a specific, well-prepared inquiry every now and then can spawn an ongoing relationship, too. Even occasional brief chats or email exchanges can constitute a relationship that is just as beneficial as "formal" mentorship. After all, it's the relationship and degree of investment that counts, not the label.

Whatever you do, remember that mentoring is a reciprocal relationship in which the mentor also gains useful information as well as a sense of pride from seeing the mentee grow. Respect your mentor's time and expertise; don't just meet to "catch up" or to complain.

Finally, consider that your peers can also be valuable mentors, for they often understand your situation better than any executive could.

**Attract rather than accost mentors, and build a natural, reciprocal relationship with them.**

### 9. Equality means a truly equal partnership at home, too. 

For women to successfully combine a fulfilling career with raising a family, a supportive partner who is committed to equality at home is vital. A 2007 study of well-educated women who left the workforce showed that 60% cited their husbands as a critical factor in that decision, referring specifically to their husband's lack of participation in child care and other domestic activities.

So how equal are homes today? According to recent data, in U.S. households where both parents are employed full-time, the mother still spends 40% more time on childcare and 30% more time on housework than the father does.

Sometimes it is the mother herself who pushes the father away from childrearing duties by criticizing him whenever he cares for the baby: "That's not the way to put on a diaper. Stand aside and let me show you!" The end result is that the father becomes less and less involved, leaving the majority of the work for the mother.

For true equality, mothers must treat fathers as equally capable partners and must share responsibilities so that both parents have their part to play.

Institutional policies also discourage the father from playing an equal role at home. Both in the U.S. and Europe, periods of maternity leave offered by companies or mandated by law tend to be longer than paternity leave. Also, men who violate stereotypical expectations by prioritizing their family over their careers tend to be penalized more for it in salary and promotions than women.

Not only is equality at home important for women pursuing careers, but it also leads to happier relationships and sets an important example for children. Hence, it is _always_ worth challenging an unequal status quo at home, even if it creates a few conflicts in the short term.

**Equality means a truly equal partnership at home, too.**

### 10. Before you go on maternity leave, lean in to your job as much as possible. 

From an early age, girls are taught that one day they will have to choose between a successful career and being a good mother.

This image is not only misleading and disheartening, it also has a nasty side effect: Women damage their own careers by preemptively making room for what they assume will be an impossible balancing act between career and family.

Consider an ambitious young lawyer who is offered an exciting new role at work — a real career maker. Because she plans to start a family in "just" a few years, she begins to wonder if she can really take on this new responsibility. After all, her children will also demand time. After consideration, she declines the opportunity.

Choices like this mean that by the time the baby arrives, the mother is in a drastically different position careerwise than if she had ambitiously pursued every opportunity. Instead of being a rising star, she may find her career stagnating.

This means that when she has to make choices regarding things like her career and child care, her job will be considerably less rewarding than it could have been. And after maternity leave, her career may seem so unappealing that she chooses to leave the workforce completely. Thus, the very steps she took to help fit her job and family together actually ended her career.

The years and months preceding motherhood are not a time to lean back but a critical time to lean in as much as possible. Don't hit the brakes at work until you really need to.

**Before you go on maternity leave, lean in to your job as much as possible.**

### 11. Don’t try to do everything perfectly; focus on what’s important. 

The myth of "having it all" is one of the most dangerous traps ever set for women. No one can have it all because life is about tradeoffs. No one can do _everything_ at home and at work _perfectly_.

In high-stress jobs, people often do everything the company asks of them — and then suddenly quit because of burnout. This is senseless.

Instead, draw boundaries and try to work the job on your own terms. Companies and leaders, for their part, should shift to a culture that is less face-time oriented and focus on results rather than on time spent at the office.

There is pressure at home too, where mothers are expected to spend increasingly large amounts of time with their children. This relatively new _intensive mothering_ phenomenon can create guilt in working mothers, even though research shows that having others care for your child while you work has no negative effect on any aspect of the child's development whatsoever.

For mothers, effective _guilt management_ can be as important as time management. As a rule of thumb, do not focus on the things you are _not_ doing but rather on completing and enjoying the task at hand.

Because no one can do it all, prioritize and focus on what's most important. For example, make time for your daughter's dance recital but don't worry about folding the linens perfectly. Don't shoot for perfection; instead, try to find solutions that are sustainable in the long run and fulfilling in the moment, both at home and at work.

There is no "perfect" way to have both a fulfilling personal life and a successful career, so find whatever way works best for you.

**Don't try to do everything perfectly; focus on what's important.**

### 12. Final summary 

The key message in this book:

**Despite tremendous strides in gender equality in the past decade, women are still conspicuously absent from leadership positions. This is due to external factors such as lingering gender biases and stereotypes, as well as internal pressures such as lack of confidence and anxiety over the challenges of combining a career with raising a family. Working to address and correct these issues benefits not only women, but society as a whole.**

The questions this book answered:

**Does gender inequality exist today and what can we do about it?**

  * Despite tremendous strides, we are still far from gender equality.

  * Women are still conspicuously absent from leadership positions, partially due to the leadership ambition gap.

  * Let's talk openly about inequality and work toward correcting it, together.

**What are the factors that can help or hinder women's careers?**

  * Women's lack of confidence can hold back their careers.

  * Careers are more like jungle gyms than ladders; aim for the top but be flexible in your route.

  * Women must carefully navigate the razor's edge of ambition and likeability.

  * To foster effective communication, practice and encourage authenticity and appropriateness.

  * Attract rather than accost mentors, and build a natural, reciprocal relationship with them.

**What factors help combine a successful career with a fulfilling personal life?**

  * Equality means a truly equal partnership at home, too.

  * Before you go on maternity leave, lean in to your job as much as possible.

  * Don't try to do everything perfectly; focus on what's important.
---

### Sheryl Sandberg

Sheryl Sandberg is the chief operating officer of Facebook and formerly a vice president at Google as well as the chief of staff of US Secretary of the Treasury, Larry Summers. In 2011, she was ranked the fifth most powerful woman in the world by _Forbes_.

