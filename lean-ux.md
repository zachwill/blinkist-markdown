---
id: 55e45c212852cc0009000010
slug: lean-ux-en
published_date: 2015-08-31T00:00:00.000+00:00
author: Jeff Gothelf
title: Lean UX
subtitle: Applying Lean Principles to Improve User Experience
main_color: 2B9CD7
text_color: 1D6991
---

# Lean UX

_Applying Lean Principles to Improve User Experience_

**Jeff Gothelf**

_Lean UX_ (2013) is a guide to applying lean principles to interactive design workspaces. These blinks explain the techniques of Lean UX and outline how you can best integrate them into your company's design process. You'll learn the importance of close collaboration and customer feedback, as well as how to constantly improve your designs.

---
### 1. What’s in it for me? Apply the principles of lean to design. 

If you have an interest in business strategy, you'll probably have heard of lean. Lean is a business approach, most famously used by Toyota, designed to cut waste and inefficiency, while at the same time allowing a company to be flexible, innovative and customer focused. 

Lean has been applied to many businesses, from start-ups to established companies, and in many areas, from meetings to product creation. These blinks take the principles of lean and apply them to the world of design. If you want to be more efficient _and_ more creative in your approach to design, read on!

In these blinks, you'll discover

  * why you should draw your design on paper first;

  * why the best design teams have more than just designers in them; and

  * why, if you want to succeed, it's better to test, test and test again.

### 2. The three foundational principles of Lean UX are design thinking, agile software development and lean startup. 

Do you ever see the designers that work for your company? If your business is like most, it likely keeps the design team separate from everyone else, which means that they work in their own little bubble. 

Luckily there's a way to overcome this.

It's called Lean UX, and it connects designers to a greater collaborative process in which every team member contributes to design. Basically it's a mix of _design thinking, agile software development_ and _lean start-up_.

But what do these terms all mean?

First, design thinking is the idea that every aspect of a business can be approached with design in mind. For instance, when a company encounters an issue, it can solve it like a designer would. One key to this strategy is to involve many people when brainstorming, thereby producing more potential solutions. 

Second, agile software development allows designers to deliver greater value to the customer while cutting product cycle times, which it accomplishes by involving everyone in a collaborative product development process. So, instead of a traditional approach, which would see work divided into departments, agile development means everyone working together from the start!

This strategy's main benefits are twofold, in that many hands make light work, and since collaboration builds team spirit while fostering creativity. 

The third aspect of Lean UX is the application of the lean start-up method to product design, a strategy that implies fast-paced experimentation and validation. 

It works like this:

Prototypes are turned out as fast as possible to test market assumptions early on. This early testing then generates feedback almost instantly, telling you what works and what doesn't. This way, inaccurate assumptions and weak ideas can be scrapped with little effect, freeing up the resources for your best ideas to flourish. 

Now that you've learned the basics of Lean UX, it's time to explore the four steps of the Lean UX cycle.

### 3. Convey ideas about your business and test them by defining your desired results, ideal customers and the features on offer. 

So, how do you put Lean UX into practice? The process has four simple aspects, and the first is to create assumptions.

But what does this entail?

Assumptions are written expressions of your unwritten ideas and beliefs. Take a recruiting company, for instance; an assumption of theirs might be that customers, in their case employers, will utilize their service to interact with potential employees. But while most of the company's employees might assume this, assuming isn't enough. This assumption now needs to be tested, and that's where the rest of the process comes in.

Once you have an assumption, your next task is to turn it into a testable hypothesis statement using the three remaining aspects: _outcomes_, _personas_ and _features._

Start by generating an outcome, that is, the result you want your product or service to achieve for users. For instance, the recruiting firm's outcome might be to get more job seekers to sign up for their service. 

Once you've decided upon an outcome, design your personas — sketches of your model users. Start by making a small drawing with a name and age, say, Kathleen, 32. Then, list behavioral and demographic information about your persona. In Kathleen's case, this might include being married with three kids and working as a consultant. 

Next, write down your persona's difficulties and needs — perhaps she's struggling to balance her kids' educations with her job, and might be interested in a work-from-home position. You can now finalize your persona by adding potential solutions to their problems. Maybe Kathleen needs a platform that showcases her skills to potential employers and helps her land a new job. 

The last step is to add features, actual products and services that might achieve the outcomes you desire for your personas, such as a service that helps employers and potential employees connect easily. 

Once you've done this, you can bring all four aspects together to write a hypothesis statement that explains the features you intend to offer to your personas, while highlighting your desired outcomes.

### 4. Open the design process to your whole product team from the start. 

It's common for companies to leave designing to the designers. After all, why should others be involved when it's not their job? 

Lean UX turns this practice on its head, and for good reason: bringing every member of the product team into the design process from the get-go makes for faster development, and design that is better suited to your company.

This is because design processes normally involve a design team being briefed by someone else, and subsequently creating a product based on this secondhand information. If the design doesn't work, it is then sent back for reworking, a process that can go on forever. 

Lean UX gets around this problem by putting designers to work with other employees right away, allowing the team to fix problems immediately and move the process along. 

Consider a designer and a developer going back and forth in an informal dialogue to design a dashboard. It takes them a few sketches and adjustments but they soon agree on a design. The designer is then free to iron out the specifics, while the developer writes the infrastructural code.

But maybe you want a more formal approach to implementing collaborative design. Look no further than _design studio_, a strategy that simply brings different team members together in the same room to develop design solutions to any problem. 

Design studio works because collaboration between product managers, business analysts, software developers and designers on a single problem provides a diversity of skills, outlooks and expertise that make the process run smoothly and efficiently. 

So, your assumptions and hypothesis statements are ready and your team is prepared to collaborate. Now you're ready to dive in by developing experiments to test your hypothesis.

### 5. Test your products before committing to them to determine which ideas work. 

You might be wondering what actually makes the Lean UX process "lean"? In fact, it all relies on testing product ideas in the most efficient way possible, in order to decide which ones are worth your time. Doing so means trimming the fat to ensure more resources go to the products that _do_ work. 

The best strategy for testing product ideas is with the _Minimum Viable Product_, or _MVP_. Here's how it works:

The MVP is essentially the smallest thing you can make or smallest action you can take to test the validity of your product. For instance, say you want to start a newsletter, a venture that is both time consuming and costly. You start by questioning the very idea of the newsletter, asking yourself, "is a newsletter necessary and will it be worth my while to make it?" 

In order to test this, you need an MVP — in this instance, it could be a signup bar on your website that can measure customer demand. If too few people sign up, you'll know that the interest isn't there, allowing you to drop your idea without wasting any additional resources. 

But an MVP could also be a prototype that you experiment with before fully committing to a product or service. There are two approaches you can use to design such a prototype:

First there's _paper prototyping_, a low-fidelity process that can be completed in as little as an hour by using flaps to hide and reveal different information on a page, thereby mimicking a user's on-screen experience. The drawback of this technique is that it's highly artificial and can produce a simulation that sometimes inadequately mimics the real experience of the final product. 

Medium- or high-fidelity options would be the right choice for products in the more advanced stages of design. While these options boast a visual and interactive quality, and can come close to perfectly matching the final product, the downside is that they are time consuming to produce and maintain.

### 6. Continuously seek customer feedback through regular and collaborative research. 

So, an MVP is a great tool to test your hypothesis, but how can you conduct this testing effectively?

First, it's important to know that a commitment to continuous product testing through customers is central to Lean UX. Doing so provides constant market feedback that helps you prove or revise your hypothesis quickly. 

For instance, a common Lean UX strategy is to test your MVP once a week with three different customers. A typical week practicing this strategy could look something like this:

On Monday, you decide what you're going to test and which customers to approach.

On Tuesday, you fine-tune your MVP to make sure the design clearly communicates exactly what you want to test.

On Wednesday, you put the finishing touches on your MVP and write a test script for your moderator to administer to participants. 

On Thursday, you actually test the MVP with customers for no longer than an hour and then review the results.

On Friday, you use the information you've gathered to either validate your hypothesis and move on to the next phase of design, or to revise it and retest the following week. 

But Lean UX is all about collaboration, and the research phase is no exception. So don't put the task in the hands of one expert outsider; instead, bring your whole team into the process. 

Outsourcing your research is actually a poor strategy because it adds external bias. It's better to include one specialized researcher on your team, but to conduct the research as a group. For instance, when deciding the aspects you're going to test, make sure to involve both developers and designers to ensure you produce a well-rounded test. 

Now that you know what Lean UX is and how it works, it's time to learn how to best integrate this practice into _your_ organization.

### 7. Simple changes can boost the collaborative potential of your workspace, ensuring effective implementation of Lean UX. 

As you now know, Lean UX isn't just a series of processes — it's a mindset and management ethos that requires some organizational shifts to ensure successful integration. It's essential to push your team members to look beyond their individual roles, because doing so will help foster a collaborative environment. 

For instance, in "normal" companies, employees tend to conform to their job titles and descriptions. As such, they're often discouraged from overstepping the limits of their responsibilities. The problem with this is that it leads to their full range of skills going untapped. 

So, while each person has a primary ability, such as design, research or development, every team member can also contribute to other disciplines using their secondary skills and interests. For example, a software developer with a creative mind could also be involved in design. 

The result is a more engaged and friendly atmosphere that comes with increased collaboration, creating the right climate for avoiding silo thinking and making your whole team accountable for their results. 

But how can you better promote this type of environment? Create an open workspace that fosters collaboration and dialogue between your team. There are a few ways to accomplish this:

For one, it's essential to remove any physical obstacles to collaboration. Perhaps cubicles isolate your employees from one another, or the floor plan of your office forces the separation of departments. 

Another productive change is to ensure enough space for whiteboards that allow people to sketch things like personas and prototypes, and where team members can leave feedback and support. 

But sometimes it's just not possible for your whole team to share a single space. In cases like this, your best option is to use tools like skype or regular get-togethers to foster collaboration.

### 8. Final summary 

The key message in this book:

**The web-driven economies of today, dominated by software-based products, have led to ever-decreasing product life cycles, as the continuous nature of software has made rapid product updates essential. To keep ahead of the curve, it's key to experiment constantly while seeking user feedback, tasks that Lean UX is perfectly suited to tackle through its collaborative dialogic approach.**

**Suggested** **further** **reading:** ** _Inspired_** **by Marty Cagan**

_Inspired_ describes the best practices of creating successful software products and explains the most common pitfalls and how to avoid them. The lessons are applicable in a range of product environments, from fledgling start-ups to large corporations.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Jeff Gothelf

Jeff Gothelf is a principal at the innovation consulting company Neo. His diverse career has included work in interactive design, a position as leader of a user experience team and even blogging. He is a highly sought-after public speaker and has led interdisciplinary teams at Publicis Modem, TheLadders and AOL.

