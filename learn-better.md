---
id: 5a2ffe45b238e10007a0de7b
slug: learn-better-en
published_date: 2017-12-14T00:00:00.000+00:00
author: Ulrich Boser
title: Learn Better
subtitle: Mastering the Skills for Success in Life, Business, and School, or, How to Become an Expert in Just About Anything
main_color: FC3238
text_color: C2272B
---

# Learn Better

_Mastering the Skills for Success in Life, Business, and School, or, How to Become an Expert in Just About Anything_

**Ulrich Boser**

_Learn Better_ (2017) upends traditional approaches to learning skills and obtaining knowledge. Learning was once thought to depend entirely on the innate ability and intelligence of the learner. Rote learning was the order of the day. We now know there are much more effective ways to learn. In fact, there are six simple steps to better learning.

---
### 1. What’s in it for me? Learn to learn better. 

To become a master in your field, you either have to be a born genius, have an outstanding memory or, at the very least, be willing to bury your head in the books for years. Right? Well, not quite. Actually, everyone can become efficient learners. You don't have to be the next Newton or have a photographic memory; all you need is a specific set of learning methods.

Apply these methods and you'll be on your way to becoming an expert in any subject.

In these blinks you'll be walked through the six steps you need to take to master the art of learning. As well as helping you enjoy the learning process, they'll show you how to excel in areas where you usually perform poorly.

You'll also learn

  * how you can build the desire to learn any topic, even statistics;

  * how Craigslist can make you learn better; and

  * what "a giant, throbbing, thirty-foot-wide buttocks" has to do with learning.

### 2. Learning is easy. You just need the right tools and strategies. 

For years it was assumed that learning ability was dependent on innate intelligence. But now experts have begun to show that this isn't the case. With just a few strategies and tools, you can drastically improve your learning.

The biggest finding is that, by employing learning strategies, you can dramatically improve your outcomes.

In the 1980s, Anastasia Kitsantas conducted an experiment at an all-girls school. She split the girls into three teams and taught them to play darts. The members of "Team Performance" were told that they would win by hitting the bull's eye; those of "Team Learning Method" were taught throwing strategies, such as keeping their arms close to their bodies; and the girls in "Team Conventional Wisdom" were simply told to do their best. In the end, not only did "Team Learning Method" outperform the others; its members also had the most fun!

There's another useful learning tool called _self-quizzing._ This learning strategy involves repeatedly recalling and testing yourself on what you've been taught _._ It's a technique designed to help new ideas stick in your long-term memory.

In fact, research has shown that self-quizzing is 50 percent more effective than some other learning strategies.

A 2006 Washington University study demonstrated just this. Researchers Jeffrey Karpicke and Henry Roediger gave a text to two groups of participants. The first group read it four times. The second group only read it once, but they practiced recalling it three times over.

When Karpicke and Roediger tested them all a few days later, they found that the passage had stuck significantly more in the minds of the group members who'd performed self-quizzing.

A final method for improving learning is using earplugs to block out external noise.

The author used this technique as an 11-year-old. He'd been having trouble focusing on his math problems, and found that judicious use of earplugs really helped him concentrate.

So those are some great practical tips to get you going. Now let's dive in a little deeper with the six steps to better learning.

### 3. If you can find meaning in your study, your learning is going to get better, fast. 

We've seen how effective some learning strategies can be, as well as how they can make learning more fun. But learning can be made more effective still by using the six steps of learning.

The first step is all about _value_. If you can make the skills or knowledge you're learning valuable and meaningful, you'll be much more driven to take initiative and persevere.

Just consider the case of Jason Wolfson, an engineer in his forties. His basement is jam-packed with his artsy Lego creations. It might seem a little strange for a grown man to be tinkering with toys, but there's a story behind each construction. The blue police phone booth was inspired by _Dr Who_, and his replica of Gonzo is a tribute to his wife, who's a big fan of the Muppet character.

Wolfson spent so much time and effort on these constructions because they're truly meaningful to him. And that's how he became a master!

But meaning and significance don't just appear out of nowhere. It takes time and a great deal of hard work to find them.

For instance, few students harbor a burning passion for statistics. And so to get a group of them motivated, Chris Hulleman, a psychology professor at the University of Virginia, developed a little strategy.

He made the students write essays on how statistics could enhance them and their interests. They put pen to paper and came up with some real gems — for instance, that statistics could help their prospects as nurses, managers or marketers. Consequently, they quickly became much more interested in the subject. In fact, some really got the bug and ended up jumping a whole grade level.

### 4. You’ll need to set small, specific goals to master a skill, but first you’ll want to be sure of the basics. 

The story goes that Isaac Newton suddenly grasped the law of universal gravitation after an apple fell on his head. But you can't rely on such eureka moments in real life.

Learning breakthroughs come from setting _targets_. And that's what step two is all about: breaking up larger goals into smaller goals, or targets, that you can focus on one at a time.

Say you want to improve your marathon running time. Instead of just trying to run faster, target smaller and more manageable aspects of the run. Maybe you'd be better this week training on hilly terrain, for example? Not only are such smaller targets less daunting; they also make it easier to track improvement.

But there's more to mastering a skill than breaking up your goals. You also need to build up some background knowledge before you begin. That's because it's difficult to really grasp a new subject or field before you've gotten a handle on the basics.

For instance, imagine you have no knowledge of German and are suddenly confronted with the question, "_Hast du heute gefrühstückt?"_ Without a few fundamental German words and grammar rules under your belt, you'd have no way of knowing that you were being asked whether you'd eaten breakfast.

You should apply the same concept elsewhere in your life. After all, you wouldn't start trying to fix the heating pipes without a little plumbing knowledge.

And once you have the essentials down, you'll be that much better at setting yourself small and achievable goals.

### 5. When it comes to learning, the importance of feedback can’t be overemphasized. 

Have you ever practiced a presentation in front of a friend before the big day? It's a common thing to do, and with good reason. Being evaluated by an audience is a fantastic way to improve.

That's why the third step is to _develop_ — that is, to hone your skills by getting feedback. After all, when you're learning, you often don't know in what areas you need to improve. Someone else's perspective is bound to help in the long run, even if it feels like it's slowing you down at the time.

The author actually personally benefited from this step. He'd always loved playing basketball, but he'd never been very good; in fact, he was pretty much the worst on his team. Even though he shot hoops for half an hour at his local court each day, his moves and footwork remained dismal.

But that's because his efforts were unfocused, and he wasn't receiving any feedback. Then, one day while browsing Craigslist, he found basketball instructor Dwane Samuels.

With Samuels's help, the author worked on perfecting specific moves like the one-dribble pull-up jump shot. The trainer showed him how his middle finger should roll off the ball and how his feet should be positioned. Within a few weeks, the author's jump shots began finding the hoop; he even started hitting three-pointers.

Another way to improve is to decrease error rate by monitoring and recording your mistakes.

If you're monitoring what you do, you'll be more aware of your performance and where you're going wrong. In short, you'll stop repeating mistakes.

Even brain surgeons benefit from this technique. Mark Bernstein recorded every mistake that occurred in his Toronto operating room over a 10-year period. Miscommunication, a poorly positioned sponge or a delay in anesthesia: it all went in his database.

Thanks to this feedback system, his team's surgical error rate went from three mistakes a month to just one and a half.

> _"Tracking outcomes can be embarrassing. Yet, this type of focused awareness boosts outcomes. Increased awareness about our performance makes us better at just about everything."_

### 6. You can improve your learning by immersing yourself in your field and making things visual. 

You might think that renowned writers, artists or scientists were born to succeed in their respective fields. But, in reality, geniuses are forged in the crucible of continuous learning.

That's step four in a nutshell — you've got to _extend_ the knowledge you already possess. In other words, if you aspire to reach the highest echelons, you'll have to constantly improve your understanding of a given topic.

The painter Jackson Pollock knew this well. At the age of 23, he joined Mexican muralist David Alfaro Siqueiros's workshop. Siqueiros encouraged Pollock to experiment. It was there that Pollock learned the drip-and-pour technique that brought him world renown, but it took him a long time to master it. He learned to work with fractal structures — geometric patterns that repeat on smaller and smaller scales, such as you'll find on Romanesco broccoli or in spiral galaxies.

At first, Pollock's initial drip paintings displayed little fractal complexity. But, over time, the fractals grew increasingly complex and detailed. It fact, they were so complex that it was a physicist, Richard Taylor, not an art historian, who first spotted them.

A further technique you can use for deepening your skills and knowledge is to visualize images with your mind's eye.

Stand-up comedian Bob Harris is a great fan of this method. Once he wanted to memorize the titles of E. M. Forster's novels. So he imagined himself in a room, looking out the window, through which he could see "a giant, throbbing thirty-foot-wide buttocks." This image ensured that Harris would forever remember the book titles _A Room with a View_ and _Howard's End_. Presumably, in Harris's mind, the gargantuan buttocks belonged to a giant named Howard.

Through the simple act of visualizing, you too can benefit and learn the concepts and facts you need in your chosen field.

### 7. Understanding relationships between concepts and practicing different approaches to a skill makes for better learning. 

It's said that the best way to master a new skill is to practice it over and over again. But the problem is that practice alone isn't actually all that efficient.

This is where step five comes in — you've got to _relate_. In other words, you have to develop an understanding of the relationship between concepts.

Psychologist Charles Judd proved this in 1908 when he conducted an experiment at the University of Chicago. He had two groups of children throwing darts at a target four inches underwater.

The first group simply practiced throwing darts underwater. But the second group learned about the physics of refraction beforehand. They now understood that the target's real position wasn't where it appeared to be, due to the way light moves underwater.

Next, Judd moved the target deeper under water, to a depth of 12 inches, where refraction was more pronounced. This made conditions even more difficult as the target appeared even further away from its real position.

Of course, the second group, which had a grasp on the physics, could now relate the information they'd learned to the new situation. Predictably, they outperformed the first group.

Varying how you practice also works wonders for learning.

A 1993 study compared two groups of women training to shoot free throws in basketball. One group practiced free throws and nothing else. The second mixed things up a bit. As well as free throws, they tried shooting from marks eight and fifteen feet away from the basket.

In the end, the group which had practiced a variety of shots ended up performing much better at the free-throw line.

### 8. We can eliminate overconfidence by reviewing our knowledge. 

Have you ever found yourself driving around in circles for hours, just because you're convinced you know how to get somewhere and are unwilling to ask for directions?

The sixth and final step encourages you to review your knowledge — to _rethink_ — and thus prevent exactly this sort of mistake.

Overconfidence leads to mistakes on a fairly regular basis. People usually overestimate their past performance and familiarity: they think they've learned more than they really have, and so expect to perform well in the future.

The military even has a name for it. They call it _victory disease_. A general may overestimate his own abilities because he's won many times before. There's no better example of this than General George Armstrong Custer, the Union general who, during the Civil War, defeated the enemy time after time. But it all went wrong in 1876 at the Battle of the Little Bighorn. He led his 200 men against over 1,000 Native Americans. Only one of Custer's men is thought to have survived.

Which is all to say, reviewing your knowledge and understanding helps you to deal with overconfidence.

For instance, psychologist Art Markman was convinced he knew how a toilet worked. After all, he used it several times a day and as a child he'd even opened up the cistern a few times to check the stopper and wiggle the handle.

But one day he realized that he couldn't actually describe how a toilet was constructed, and that he certainly wouldn't be able to take it apart and put it back together again.

Only after admitting his lack of knowledge could he ask himself the necessary questions: "How does the water flow into the toilet? Where does it go after flushing?"

He was ready for better learning.

You too can become a better learner if you abide by these six steps. No matter if you'd like to fix toilets, improve at sports or learn a language, a few simple practices will soon mean you'll become master of your knowledge domain.

### 9. Final summary 

The key message in this book:

**By putting in the effort and focusing on learning, almost everyone can become an expert or master in almost any field. But to do this you'll need to invest the time and use some tried-and-true strategies. Just remember these six steps:** **value, target, develop, extend, relate and rethink** **. With these methods, you'll be well on your way to better learning.**

Actionable advice:

**Space out your learning**

The next time you're studying for an important exam, make sure to split your learning into several sessions. You certainly know the frustration of spending an entire day studying only to find, just a few hours later, that you've already forgotten almost everything. To spare yourself this frustration and learn more efficiently, you need to break up your learning into small achievable goals and spread it out over time, instead of trying to learn everything in one sitting. Your brain needs breaks to store new information, so go ahead — take one!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Peak_** **by Chip Conley**

_Peak_ (2007) reveals how hotelier Chip Conley became inspired by a book on psychology and applied it to his own successful business philosophy. These blinks show us how we can find lasting success and happiness by shifting away from fixating on profit and instead focusing on creating positive relationships with customers and investors.
---

### Ulrich Boser

Ulrich Boser has worked as an editor, writer, reporter and education researcher. His work has appeared in the _New York Times,_ the _Wall Street Journal, Slate, Wired_ and _Newsweek._ As a child, he was categorized as a "slow learner" by his teachers, which sparked his later interest in the science of learning. His other books include _The Gardner Heist, Learn Better_ and _The Leap_.

