---
id: 557599f739356100074e0000
slug: learn-or-die-en
published_date: 2015-06-10T00:00:00.000+00:00
author: Edward D. Hess
title: Learn or Die
subtitle: Using Science to Build a Leading-Edge Learning Organization
main_color: 15683F
text_color: 15683F
---

# Learn or Die

_Using Science to Build a Leading-Edge Learning Organization_

**Edward D. Hess**

_Learn or Die_ (2014) makes a strong case for personal and organizational learning as a survival tool in today's fast-paced business world. Full of practical tips, the book presents a framework for how individuals and organizations can create an environment that fosters life-long learning.

---
### 1. What’s in it for me? Reevaluate how your company deals with learning and improve performance. 

Established business leaders know that being able to learn is a key to success. But what does this mean? Being able to learn means improving and growing your skills — and in doing so, staying ahead of the pack.

An individual can learn; yet the bigger challenge is getting an entire organization to keep learning.

In today's fast-paced business world, we either learn, or die. These blinks give you insight into the latest research that explains how our brains work — that is, exactly how we learn — and how you can then use this knowledge to reevaluate how your company operates.

You'll learn how to build a successful, dynamic organization based on how the human mind works. That's bad news for your competitors, and great news for your employees and bottom line!

In these blinks, you'll discover

  * how Toyota deals with employee mistakes;

  * why you need a comfortable chair to learn effectively; and

  * how to think as quickly as a firefighter.

### 2. System 1 thinking says keep the status quo. System 2 thinking says shake it up; we can do better. 

We learn as we observe the results of cause and effect in our environment. When dark clouds gather overhead, for example, you know through past observation that it may rain.

Over time, such observations become a way of interpreting the world. Our learning systems are somewhat like a computer operating system — that takes more work to rewrite. Yet it's precisely our ability to rewrite what we've learned that is the key to _productive learning_.

The human learning machine consumes lots of energy, and because of this, tends to operate more often than not on autopilot — relying on instinct — to conserve fuel.

We call this mode _System 1_. It's in this mode that you, for example, keep your voice down inside rather than yell and scream, based on the hard lessons you learned as a child.

Sometimes this mode can blind you, however. When you really need to think out of the box, such as when you're trying to figure out why a competitor's product is crushing yours in the market, you need to switch your thinking to _System 2_.

The ultimate goal of a learning organization is to overcome the preference for System 1 thinking. To do so, learning organizations must help employees to process information, especially information that challenges automatic assumptions.

System 1 thinking isn't a problem with basic activities. Yet in business, it can cause you to miss trends or opportunities. A company stuck in System 1 thinking often can't see alternatives to the status quo.

In contrast, System 2 thinking helps you reject automatic responses; instead, you're able to consider new options and temper any bias.

Observing and managing the way you think is essential to actual learning. One way to get in the habit of using System 2 thinking is to simply reflect on what you've done during the day. This will help you to identify critical moments in which System 2 thinking could have helped.

Throughout the process, remember to keep an open mind so you can turn mistakes into learning opportunities.

### 3. Don’t deny your emotions, as they’re necessary for thinking. Spock should not be your hero. 

We often — yet erroneously — separate our rational, thinking mind from our emotional mind. The truth, however, is that thinking is _impossible_ without emotion. The areas of the brain responsible for emotions and thinking are closely networked, and even overlap.

Star Trek's highly logical Spock is, after all, a fictional character. In reality, there's no such thing as completely logical thought. Your emotions will always affect your thoughts, and vice versa.

Consider how your emotions can color your memories. Or your "gut" intuition, which isn't really a thought or a feeling, but more a mixture of the two.

Your mind, brain and body all communicate, and what is communicated affects each organ in turn. That's why when you hurt yourself or are stressed, for example, both your thinking and your ability to learn is compromised.

But that doesn't mean that the quality of your thinking is completely out of your control.

Not all emotions _disrupt_ thinking, as we know from our evolutionary past. While negative emotions activate self-preserving behaviors, such as our fight-or-flight instinct, positive emotions enhance cognitive processing through thinking and learning, increasing awareness, exploration and creativity.

So rather than rejecting your emotions, you should embrace the ones that enhance how you learn. Smiling more, expressing gratitude and reflecting on the good things in life give you a positive outlook that can ease the learning process.

But managing emotions isn't easy. Negative emotions can limit your comprehension and trigger a fear response, hijacking other functions of the brain and thus undermining your ability to change perspectives or behaviors.

When we're afraid, we become obsessed with that fear, and in turn can't use our minds fully.

You can combat fear by reframing a situation into one that isn't so scary. For instance, if you're afraid to deliver a presentation to the board of directors, just view the talk as a learning opportunity — and you'll be fine.

So now you know how the process of learning works. The following blinks will examine tools and methods that foster learning.

### 4. Improve your organization’s learning curve by hiring the right people – internally motivated learners. 

So now that you know how the human mind thinks and learns, it's time to apply these lessons to your organization.

Your goal is to make your team a _High Performance Learning Organization_ (HPLO), a place where learning is praised as a key element for success.

HPLOs need to achieve three key goals: finding the _right people_, creating the _right environment_ and establishing the _right processes_. Let's look first at how to find the right people.

A learning organization needs people who have a learning mind-set, which is characterized by a genuine motivation to learn.

Our basic need to survive led to the development of behavior centered around a decision to approach or avoid things. If a thing was dangerous, we avoided it. If not, we approached it.

Whether or not we take risks and learn is likewise predicated on whether we've learned to approach or avoid the process of learning.

For example, if you've been humiliated by a manager after trying something new, you probably will avoid taking risks in the future. An HPLO, in contrast, aims to bring people into the organization who are motivated to learn and to help others learn.

Similarly, employees should have _self-efficacy_, or a belief in their ability to achieve. They should see problems as a challenge and an opportunity to learn, not as something to fear. A manager with self-efficacy can cope with a major product recall, for example, knowing that he can learn from the situation to avoid it in the future.

There are many reasons to learn — some are internal, and others are external. People who want to master or expand their skills and welcome feedback are fueled by _intrinsic motivations_. That is, learning makes them _feel_ good!

_Extrinsically motivated_ learners, on the other hand, seek the approval of others. They want to learn to get good grades, high pay or respect. Generally more competitive and egoistic, they don't welcome challenges and try to avoid failure, really only wanting to confirm how smart they really are.

It's clear that the sort of learners HPLOs want and need are internally motivated learners!

### 5. Employees need to feel safe to speak up and think creatively, so create a positive environment. 

Once you've found the right people, you need to cultivate the kind of environment that helps them thrive — one that fosters learning.

A positive learning environment recognizes learners as individuals and provides them with autonomy and control over their learning.

Role models should send employees on a journey of discovery, and challenge them. They should inspire a learning mind-set and help people cope with the fear of failure, any stress or other inhibitors to learning.

A positive learning environment is one in which team leaders are evaluated on their ability to care and nurture employees' growth. Learning organizations use things like "360" reviews (in which employees rate managers) to set financial compensation for top management.

A positive environment deconstructs hierarchies. Learned deference to parents, elders or teachers can affect our relationship with authority in the workplace. Employees are often afraid to speak up, and fear the threat of a bad performance review or being passed over for promotion.

But a _High Performance Learning Organization_ (HPLO) requires an environment in which employees are invited to share thoughts, and this is only possible when employees feel safe.

For example, managers can praise courageous workers who speak up or show humility by admitting their failures. Toyota, for instance, encourages its employees to be honest regarding their mistakes, which can only happen if an employee knows that she won't be punished.

Employers should be motivated to create a positive learning environment, as a space that facilitates learning also leads to high employee engagement.

Typically, companies promote high engagement by taking employees' opinions into account, giving them the opportunity to do what they do best and recognizing them for doing good work. Together, this results in more respect, self-efficacy and a positive atmosphere, all of which encourage learning.

### 6. Great learning requires great communication. Turn on System 2 to be a present, active listener. 

Once you've found the right people and have created the right environment, it's time for the third goal in creating a _High Performance Learning Organization_ (HPLO): establishing the right processes.

The right processes are built upon effective communication. After all, learning is a team effort!

Remember System 1 and System 2 thinking? The same concept applies to conversation. Often, we allow System 1 thinking to take over during a chat. We simply look to validate our beliefs and self-image, and don't actively seek to challenge or expand our perspective.

You can see clearly that System 1 thinking isn't a recipe for learning.

If you approach a conversation with a System 2 mind-set, you'll ensure that your talk is open and honest. You should question _everything_, and learn from your conversation partner. In the workplace, we learn when we admit we're wrong, or when we suspend judgment regarding others' opinions.

Unfortunately we live in a culture that values telling over asking, in that we'd much prefer to offer our opinion than ask other people about their own thoughts.

"Tellers" often assume that they are imparting valuable knowledge. Here is the message they're actually delivering: "I'm smarter and know more than you."

Instead, engage in humble inquiry and switch to "asking." By asking questions, you demonstrate to your colleagues that you care about what they think and that you're willing to listen and learn.

To do this, you need to admit to yourself that you know much less than you think you know!

Good conversations also require both a speaker and a listener to be engaged, and this means fighting distractions. Humans can cognitively process 600 words per minute, yet we speak at a rate of about 100 to 150 words per minute. As a result, we can get bored quickly.

But clearly our conversation partners want us to pay attention! Give them peace of mind by letting them know that you're present and are listening. Paraphrase their words back to them or ask for further explanation, for example.

### 7. Consider different critical thinking strategies to find the optimal solution. 

To readjust your view based on new facts — as is crucial for learning — you have to be able to conduct a critical analysis of new situations and information.

Here are a few critical thinking tools that will help you improve your problem solving.

First is the _recognition-primed decision model_. With this model, you compare your situation with situations from the past, identify which situations are most relevant to your current event and then apply the solution that worked then. This method is great when time is of the essence.

For example, when a firefighter arrives at the scene of a fire, she has no time to weigh the pros and the cons of a multitude of actions. Instead, she'll think back to a few fires she successfully fought to see what she could apply to her current situation. She'll consider the characteristics of the fire, the layout of the building and the surrounding environment to determine quickly what to do.

But some situations will be completely new, meaning you can't rely on past solutions to tackle them. In such cases, use a _pre-mortem analysis._ Just as a doctor uses a post-mortem (after death) examination to determine how a patient died, using a _pre-mortem_ approach gives you a chance to anticipate future events before they happen.

When facing a situation, imagine a possible solution and then imagine that the solution was incorrect. Ask yourself, "What went wrong?" You can then examine the details that led to your hypothetical failure, and use that information to mitigate risks you might have initially overlooked.

In the 1970s, planning teams from Shell Oil used a similar strategy to reduce overconfidence and prevent employees from getting tunnel vision when considering solutions.

And finally, an _insight process_ can help you discover new ideas. Slow down, suspend your judgment and ask yourself: Is there any data that contradicts my beliefs? Would a different perspective bring different answers? If I reframe my question, will I end up with a different set of answers?

### 8. Find your own path and apply learning “best practices” to your organization. 

Are you ready to apply these lessons to your own organization? Let's look at a couple of real-life examples and see how others have succeeded.

Hedge fund Bridgewater uses _drill downs_ to improve its learning environment. Essentially all conversations, meetings and interviews are recorded, and everyone has access. Sounds scary, right?

Privacy implications aside, this strategy allows employee thinking and personal weaknesses to be easily reviewed. Colleagues can thus be brutally honest with each other and help each other improve.

Sometimes business leaders may need to modify a business model or even company culture. Doing so requires a leader to establish new learning processes, and to think long and hard about how and at what level decisions are made.

Accounting software solutions developer Intuit, when faced with a business slowdown, radically modified its approach by adopting _design thinking_ — a methodology used by creatives to explore, discover and make new products. The company's goal: to produce genuine innovation.

To spread design thinking, Intuit empowered employees by giving them time to develop personal projects and by encouraging experimentation and rewarding the most promising ideas.

Let's say your company is already a market leader. You don't need to change, but how do you preserve your position? Staying ahead means constantly reinventing your business while maintaining high levels of employee engagement.

United Parcel Service (UPS) started with $100 and a bike. Each time the company expanded its services — from worldwide delivery, overnight shipping or air service — it faced hurdles that it turned into valuable lessons.

UPS also transformed employee satisfaction into a science, spreading the company's core values throughout its culture and rewarding employees with stock plans. As of 2012, UPS had a retention rate of some 90 percent!

You'll need to follow a path that's right for your business. But whatever you decide, learning will be the engine that propels your organization toward success.

### 9. Final summary 

The key message in this book:

**Individuals and organizations need to find a way to continuously learn and adapt or face professional obsolescence. But to learn well, individuals and organizations need to learn intelligently, and that means creating the conditions in which learning is easy and rewarded.**

**Suggested further reading:**

_How We Learn_ explains the fascinating mechanisms in our minds that form and hold memories, and shows how with this information, we can better absorb and retain information. You'll explore the many functions of the brain and gain practical advice on how to better study and learn.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Edward D. Hess

Edward D. Hess is a professor of business administration at the University of Virginia Darden School of Business. He has written a number of books on personal growth, leadership and entrepreneurship, including _The Road to Organic Growth_ and _Grow to Greatness_.

