---
id: 534ec41a6531660007650000
slug: learned-optimism-en
published_date: 2014-04-29T11:00:00.000+00:00
author: Martin Seligman
title: Learned Optimism
subtitle: How to Change Your Mind and Your Life
main_color: B0D5F0
text_color: 526470
---

# Learned Optimism

_How to Change Your Mind and Your Life_

**Martin Seligman**

_Learned Optimism_ explains why so many people grow up to be pessimistic and what the negative implications of this habit are. Furthermore, it shows how our habitual optimism or pessimism influences us for better or for worse in all areas of life. Finally, it shows how pessimists can learn how to become optimists, thus greatly improving their health and happiness, and presents several techniques for learning this new way of thinking.

---
### 1. What‘s in it for me: acquire the right mindset and a positive view on your world. 

Have you ever wondered why some people seem to find it easier to get through life than others? Or, why some people are far healthier and enjoy more success?

If so, you've probably thought at least once that such people were just "born under a lucky star." But have you ever considered that perhaps their good fortune is the result of their optimistic outlook on life?

_Learned_ _Optimism_ is about exactly this phenomenon. Its author, Martin Seligman, is considered the father of the _positive_ _psychology_ movement, a movement which began with Seligman's studies of, what he called, "learned helplessness."

In perhaps his most famous experiment, Seligman administered electric shocks to dogs. Some of these canine subjects had the ability to put an end to the shocks by touching a button with their nose, while others couldn't stop the shocks no matter what they did.

What fascinated Seligman was that the dogs who couldn't change their fate in this experiment would later also not even attempt to do anything about their situation when they actually could. Instead, they would simply lay there, apparently defeated.

While it is quite normal for people to feel helpless in a situation of defeat, one thing makes the helplessness stick, or enables us to "shrug off" the situation and move on: our so-called _explanatory_ _style._

Explanatory style refers to the way in which we explain the negative events of our lives to ourselves : optimistically or pessimistically. Both optimists and pessimists tend to use very distinct explanatory styles.

As you'll see in the following blinks, this style does not only pertain to individuals, but also to entire teams. For example, all other things being equal, explanatory style is a predictor of the success and failure of sports teams, especially when under pressure.

Also in these blinks, you'll discover the reason why optimists tend to be healthier than pessimists.

You'll find out why talking to yourself in a pessimistic way can lead to depression.

Finally, and most importantly, you'll discover that our explanatory style is _learned_, and that it's possible to change your outlook on life for the better.

### 2. Pessimists have explanatory styles that are universal, permanent and internal; vice versa for optimists. 

Whenever we experience a negative event in our lives, we always explain it to ourselves in one of two ways: optimistically or pessimistically.

But what exactly characterizes our explanatory style?

First, pessimists consider problems to be permanent, while optimists consider them only temporary.

For example, if you lose an important client, you might think, "I _always_ lose the most important clients." By using the word "always," you make the explanation permanent: you've always lost important clients, and always will — so what's the point in trying?

In contrast, if you use an optimistic explanatory style, then you consider negative events to be fleeting. For instance, you might think, "I lost this _one_ important client, but I'll do well with other ones."

Second, where optimists think of problems as being specific to a certain situation, pessimists tend to generalize.

For example, if a pessimistic student believes that the bad grade they received is unjustified, they might go on to think of grades as unfair _in_ _general_. Consequently, they might find it a lot harder to study for their next exam.

However, if they instead thought about the problem they encountered in a _specific_ manner, then they'd focus only on the event itself. For instance, they might think: "Ok, this one professor is unfair, but perhaps the others will better appreciate my work."

Third, while optimists tend to consider negative events as being externally caused, and positive events as internally caused, pessimists usually think of these the other way around. For example, if your spouse leaves you, you could think: "He left because I'm not good enough, or because I didn't laugh at his jokes."

On the other hand, if you consider the source of negative events as external, you might think instead: "He simply wasn't ready for a commitment, so actually the relationship was a waste of my time."

Luckily, the ways in which optimists and pessimists make sense of bad events are not set in stone: all three behavioral patterns can be changed.

### 3. Pessimism and optimism are both habits of thinking, i.e., self-talk, which can be learned. 

Our explanatory style derives from our individual experience. Depending on our life experiences, we become either pessimists, believing we have no control over our fate, or optimists, feeling a sense of control over our destinies.

This can, in fact, be seen even in dogs, as seen in the experiment about learned helplessness mentioned earlier. The experiment turned the dogs into victims of an unpleasant situation, and in doing so they were trained to become passive and pessimistic.

Similarly, human thinking habits also are learned, mostly during childhood and mainly from parents and schoolteachers.

Children usually imitate their parents' behavior, so if those parents tend to explain negative events pessimistically, their child will be more likely to employ the same explanatory style.

Teachers are influential, too. For instance, they often evaluate the poor results of boys and girls differently, telling boys that their poor performance is due to them not paying attention in class, and telling girls that they're "just not that great with numbers."

As a result, girls develop a rather "internal" explanatory style, where they blame themselves for their performance, in contrast to boys, who more often have an "external" style. Because an internal explanatory style can promote depression, this could be one cause of the higher frequency of depression in females.

Moreover, life crises are particularly influential in terms of learning explanatory style. If children learn that crises can be overcome, and thus that adversity is impermanent, they're more likely to develop a relatively optimistic style.

Therefore, childhood crises need not have a negative effect on children. It will all depend on how those crises are handled by the child's primary caretakers.

The most important takeaway is that, since our explanatory style is learned, we can change the way we "talk" to ourselves: even if you've acquired a pessimistic style in childhood, you're not condemned to use it forever.

### 4. Optimists have a better immune system and are generally healthier. 

The positive effects of an optimistic outlook are far greater than most of us assume. For instance, compared with their pessimistic peers, optimists are generally healthier.

Why is this?

Firstly, on a cellular level, optimists often have a stronger immune system. For example, studies that induced a state of inescapable helplessness in rats have shown that their immune systems produce fewer T-cells — cells that are crucial to immune system response.

Other studies have shown that changing our explanatory style, and the relief from the feeling of helplessness that this provides, can even enhance the immune system of cancer patients.

Secondly, because optimists tend to be more active than pessimists, they're more likely to take good care of themselves.

This is because optimists believe that their actions have a positive effect, so they're more likely to adhere to a health care regimen. Pessimists, on the other hand, tend to think that nothing they do matters, so they have no reason to even try to change their lifestyles, however unhealthy they may be.

Also, optimists encounter fewer negative life events than pessimists do, a phenomenon that researchers explain in terms of a pessimist's passivity due to their conviction that they can't change anything. So if you're a pessimist, encountering an abundance of negative events, your body will have to suffer a lot of stress, which in turn can lead to disease.

Thirdly, optimistic people find it easier to sustain friendships, and friendship is beneficial to our health. This is because having a friend that you can confide in and discuss anything and everything with actually _eases_ the stress generated by negative life events.

So when you're going through a rough patch, confiding in someone who is close to you can help immensely. Often, because such people know us so well, they'll have insightful, useful ideas about what we can do to improve our situation.

### 5. Pessimism promotes depression. 

At this point in history, the Western world is witnessing an epidemic of depression: at any given moment, approximately 25 percent of the population is experiencing some form of depression.

But what are the causes of depression?

While negative events and biological factors seem to play a role, these alone can't explain the whole phenomenon.

For instance, if one of your siblings is depressed, there's a higher probability that you'll become depressed, too. However, these genetic influences are not particularly strong.

Negative life events also cannot be the main cause of depression. While it's normal to respond to negative events in our lives with mild depressive symptoms — like a bad mood, or lethargy — not everybody responds to such events by becoming seriously depressed.

So, if genetic influences and negative events themselves aren't the cause of depression, what is?

The answer, in short, is our explanatory style: how we think about those events is a determining factor in whether or not we become depressed.

As one study has shown, learned helplessness can be produced by giving people a task that's impossible to fulfill, no matter what they do.

In the human study, subjects were placed in a room, and given a panel with several buttons. The room was then filled with noise, and the subjects were given the task of stopping the noise by pressing the panel's buttons.

However, the experiment was rigged for some of the subjects: no matter which buttons they pressed, the noise wouldn't stop.

The result?

These particular subjects learned to be helpless in this situation, and after the experiment was over they showed symptoms of depression.

This study supports Seligman's model of depression, which in his words is "the belief that your actions will be futile." So, while loss, defeat and failure can cause depression, it will only happen if you believe that nothing you can do will change the situation.

### 6. You are far more likely to win at sports competitions if you employ an optimistic style of thinking. 

As we've seen, optimistic and pessimistic explanatory styles have a huge influence on performance. One area where this is particularly true is competitive sports. Given two teams that are equal in every other respect, the optimistic team will always outperform the pessimistic team, especially after a prior defeat.

Consider, for example, a study by Seligman and his colleagues, in which they rated the optimism of baseball teams across two different seasons. Keep in mind that, in terms of players, both teams were equally talented.

An especially optimistic team explains their losses like so: "We lost against this one team, just this one time, and it's not really our fault."

The study found that, in 1985 — the first season — the New York Mets had the most optimistic style of all the National League teams. On the other hand, the St. Louis Cardinals had a very pessimistic way of explaining their failures.

On this basis, Seligman and his colleagues predicted that the Mets would enjoy an even more successful season in 1986, and that the Cardinals would lose ground. As it turned out, this is precisely what happened.

Another example of the powerful influence of optimism on performance can be seen in the 1987 Berkeley swim team. The optimistic swimmers in the team performed better _after_ failure than the pessimistic swimmers.

In an experiment, the explanatory style of each swimmer was evaluated. Then, after the swimmers had actually performed at their best, they were told by their coach that they'd performed significantly worse than they had. This was extremely frustrating to the athletes.

When asked to swim again, the pessimists swam a lot slower (up to two seconds slower) than their first trial, whereas the optimists swam just as well, or even better!

### 7. Optimists are much more successful; talent is overrated if it’s not matched by optimism. 

Even if you're not a big-shot athlete, you're more likely to overcome the challenges in your life if you use an optimistic explanatory style.

This benefit can be seen even in children, as those with an optimistic style do better in the classroom than their pessimistic peers.

In one experiment, two groups of children — one optimistic, the other pessimistic — were given solvable math problems. In this first trial, both groups perform equally well.

In a second trial, however, the math problems are switched for _unsolvable_ ones.

How did each group respond?

The pessimistic children gave up, refusing to continue with the task. In contrast, the optimistic children, while aware that they were making mistakes, continued in their efforts to solve the problems by using their existing strategies.

Finally, in a third trial, both groups were presented again with solvable problems, and subsequently asked to rate how well they will do when solving similar problems in the future. The pessimistic children responded that they would be able to solve only about 50 percent of the problems, while the optimists believed that they would be able to solve around 90 percent.

The same effect can be observed later in life, too, as optimistic students achieve better grades in college than their SAT scores predict.

This was revealed in an experiment at the University of Pennsylvania. Usually, the admissions office chooses students according to their high school grades, their college board scores and the results of their achievement tests. These are calculated into a predictive index: if the score is high enough, the student gets admitted to the university.

But there is a problem with this system: certain freshmen do worse than expected, and some do much better.

To find out why, Seligman assessed the freshmen students for their optimism, then waited to see how they would perform in their first semester.

As you've probably guessed, the optimistic students performed better than predicted, and the pessimists did worse than expected.

### 8. Optimism is also very useful in your professional life. 

Why do some people seem to thrive at every job they do, while others barely scrape by?

Again, the factor that often differentiates these people is their explanatory style.

People with an optimistic explanatory style are better suited for occupations that involve a lot of rejection. This is especially true in sales jobs that involve cold calling: in this environment, having an optimistic explanatory style will determine whether you will thrive or quit.

For example, the selection process of the insurance company Metropolitan — which hires 5000 new agents every year — is extremely thorough, since it costs them more than $30,000 to select and train a new agent. Nevertheless, after a four-year period, 80 percent of them will have quit.

An important part of a sales agent's work is cold calling, which typically ends in a lot of refusals. Only those agents who continued to call at least ten leads a day, no matter how many rejections they received, became successful. The others usually quit.

In addition to the already extensive selection process, Seligman measured the optimism of prospective sales agents. He proceeded to hire agents that underperformed slightly on the standard industry tests (which measured the applicant's "aptitude" to become a sales agent) but scored very highly in optimism.

As Seligman monitored the careers of these optimists, he found that they actually performed better than the agents who were selected based solely on their skills.

As Seligman's study showed, aptitude or talent is not sufficient to predict professional success, particularly in fields where one encounters a lot of setbacks. Therefore, he suggests selecting personnel for three main characteristics: motivation, aptitude and, of course, optimism.

### 9. You can change your self-talk by using the ABC technique. 

Now that you've seen the devastating effects a pessimistic explanatory style can have on your life, you might reasonably wonder: "How can I change from a pessimist into an optimist?"

One particularly effective way of handling negative self-talk is the so-called ABC technique, developed by the psychologist Albert Ellis. This technique involves three steps: adversity, belief and consequence.

First, it's important to monitor yourself in order to observe the link between adversity, belief and consequence in your life.

Consider this everyday example:

ADVERSITY: A love interest doesn't return your phone calls.

BELIEF: He or she doesn't like me. My jokes are not funny. I'm ugly.

CONSEQUENCE: You feel depressed all day.

But it's not always easy to recognize these ABCs in your own life, as most of our self-talk is unconscious. Nevertheless, you should try to listen to your self talk and find at least five ABCs, so you can observe their negative effect on your life.

To do this, try to record all three ABC components when examining your negative self talk.

_Adversity_ can describe any challenging event: an argument with your partner, a speeding ticket, or forgetting to buy groceries on your way home from work.

_Belief_ concerns how you interpret such situations. Here it is important to distinguish thoughts from feelings (as feelings are consequences). For example, a belief can be: I'm a bad parent; I'm incompetent; I did a good job; My memory is terrible.

When it comes to _consequences_, you should consider what you did as a result of A and B, and how you felt. For example, did you cry? Were you miserable? Did you shout and get mad? Were you embarrassed?

Once you have found a few ABCs in your life, you are then in a position to change them. It's crucial to realize at this point that the beliefs you've recorded will largely determine the consequences.

### 10. Try to understand the connection between A, B and C, and change where appropriate. 

Our beliefs about a situation will determine both its consequences and how we feel about the situation. So, by changing our beliefs, we also change these consequences and our feelings.

While it's difficult to ascertain which of our beliefs are "true," it's clear that how we think about negative events greatly influences how they affect us.

For example, imagine you're faced with an adverse situation at work — say you're calling a customer repeatedly and they don't pick up the phone. You believe that the customer is probably too busy to answer. The consequence is that you decide to simply leave a message and try again tomorrow.

However, with a different belief, the same adverse situation can have a very different result. For example, after calling for the fifth time, you believe that the customer is probably screening your calls because they don't want to do business with you. The consequence of this belief is that you feel defeated and unworthy, and might decide to quit trying to reach them altogether.

Situations like the above pervade our entire lives, and can even influence how our children perceive and act in similar situations. But by discussing these ABCs with your children and helping them to develop alternative beliefs, you might actually help them lead happier lives.

For example, consider the ABCs in these two scenarios:

Your child's friend decides to sit with the new kid during lunch. Your child believes that his friend doesn't like him anymore, and that he's trying to make a new best friend. The consequence is that your child becomes sad, feels rejected and, because he's preoccupied by this situation, stops paying attention in class.

Now consider this alternative reaction: Your child believes that it's nice of his friend to try to make friends with the kid at school; he decides to join them and introduce himself. The consequence? Your child feels secure in himself, and might even make a new friend as a result.

### 11. You can become an optimist by questioning and analyzing your beliefs. 

As we've seen, optimists interpret stressful situations as being specific, temporary and externally caused. Is this how _you_ tend to respond to such situations? If not, you're in luck, as there are several ways that you can begin to change your beliefs.

The first way is _disputation_, which works on a deep level to transform your negative beliefs. Disputing your beliefs involves testing every belief in terms of the following four questions:

_Is_ _the_ _belief_ _actually_ _true?_ If so, what evidence is there? For example, if one client doesn't buy from you, does this really mean that you're bad at your job? If you answer "yes," then how can you explain the successful sales you've made this week?

_Is_ _there_ _an_ _alternative_ _explanation?_ Here, you should focus on the specific, changeable and impersonal causes. For example, if your colleagues suddenly stop talking the moment you entered the office, is it actually because they were talking about you behind your back? Is it possible they were talking about something else entirely?

_What_ _are_ _the_ _implications_ _of_ _your_ _belief,_ _if_ _it_ _were_ _true?_ How probable are these implications, and are they really that bad?

Finally, ask yourself: _is_ _what_ _I'm_ _thinking_ _useful_ _to_ _me?_ If a thought isn't useful, can you simply let it go and focus instead on how to change the situation next time around?

Once you have practiced this method of disputing your beliefs, you can then move on to the second way: _externalizing_ the voices.

For this, you need to get a close friend to do the exercise with you and to attack you as viciously as you do yourself, using all your own negative self-beliefs. Your task is to defend yourself against these attacks by verbalizing your defense out loud.

You can also do this exercise with your child. However, you must ensure that your child actually disputes the attacks sufficiently.

By practicing these methods regularly, you'll be on your way to changing your negative beliefs for good, and becoming a full-fledged optimist!

### 12. Final summary 

The key message in this book:

**Optimism is healthier than pessimism, both mentally and physically, and optimists tend to be happier, more successful people. But both optimism and pessimism are learned responses to challenging negative situations, which means that — by way of a variety of methods — it's possible for you to become more optimistic.**

Actionable advice:

**In adverse situations, think _ABC_.**

Next time when you encounter adversity in your life, you can use the ABC technique to dispute your negative self talk. For instance, if your spouse is short with you on the phone, rather than thinking "maybe he doesn't love me anymore" or "I'm always interrupting her, making it impossible for her to work," you can use the ABC technique to disrupt your negative self-talk. This will make you much happier in the long run and prevent you from snapping back at your partner, who might simply be in a rush.

**Use _ABC_ instead of thinking of a situation as permanent.**

The next time you ruin your diet by eating a piece of cake, don't tell yourself "I'll never be able to stick to my diet and will always be chubby" and then proceed to eat the entire cake. Instead, try using the ABC technique to change your thoughts: "Okay, I just ate a piece of cake, which was delicious. Tomorrow, I'll get back to my diet again, and I can already see great success on the scale."
---

### Martin Seligman

Martin Seligman is a professor of psychology and chairman of the American Psychological Association. He is one of the leading figures in positive psychology, which focuses on developing the positive aspects of the human psyche (in contrast to "traditional psychology," which deals mainly with mental illness). His other bestselling books include _The Optimistic Child_, _Authentic Happiness_ and _Flourish_.

