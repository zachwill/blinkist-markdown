---
id: 596c9f05b238e10005430c4d
slug: learning-leadership-en
published_date: 2017-07-20T00:00:00.000+00:00
author: James Kouzes and Barry Posner
title: Learning Leadership
subtitle: The Five Fundamentals of Becoming an Exemplary Leader
main_color: C0262D
text_color: C0262D
---

# Learning Leadership

_The Five Fundamentals of Becoming an Exemplary Leader_

**James Kouzes and Barry Posner**

_Learning Leadership_ (2016) proves that leaders don't have to be born with a talent for great leadership. Like any other skill, leadership is something that can be learned and improved upon. And with the help of just five fundamental strategies, you too can learn what it takes to be an effective leader. These are great tips for those who are just stepping into a leadership position or for seasoned pros looking for a fresh perspective.

---
### 1. What’s in it for me? Learn what it takes to become a good, successful leader. 

What makes for a good leader? A charming character, iron will, an inspiring personality? Is there such thing as a "natural leader" or is good leadership a skill that can and, for some, must be learned?

The good news is that anyone can be a leader. But stellar leadership requires constant effort and repeated learning. In fact, learning, challenging yourself and also failure along the way are at the very foundation of good leadership.

These blinks lay out five fundamentals that form the basis of good, exemplary leadership. You will be guided through these steps and be given all the tools at hand to eventually learn and master this invaluable skill.

In these blinks, you'll learn

  * that good leadership starts with believing in your potential;

  * why you should imagine yourself as an exemplary leader in ten years; and

  * why baseball players are very good at learning from past failures.

### 2. Fundamental #1: Believe in yourself and never stop learning. 

You've probably known someone who's been referred to as "a born leader," and maybe that appellative gave you the impression that every successful leader must have an inherent gift.

Well, let's start by debunking this myth. The truth is this: everyone has the potential to become a leader.

But before you become a great leader, you need to understand that this ability is already inside you and that it can be built upon.

Look within and find out what you consider important in your life. Ask yourself, "What challenges have I faced?" And, "What kind of encouragement did I use?"

Chances are, you have overcome significant challenges in life, and you can use these past events as ways to motivate and lead others.

To really bring that into action, let's go over the first self-coaching exercise:

Before you start your day, stop and ask yourself four questions: Who am I? What do I do? How do I make a difference? What will I do today that really matters?

Write down your answers to these questions on your phone or on a piece of paper, and carry those answers with you. This way, you can always refer to it if things get difficult and remind yourself of the day's goal.

This is a simple and effective way to stay motivated, accomplish your goals and gain confidence in yourself as a leader.

It's also important to remember that one of the best skills you can have is a willingness to learn.

All great leaders are constantly looking for new knowledge. They work hard and remain focused on growing and gaining more skills.

Take David Maister, a sought-after management consultant. He is well aware that his clients wouldn't hire him a second time if he wasn't constantly learning and keeping up with the newest trends.

You should have a similar mind-set and know that if you want to be seen as a valuable leader, you need to always be learning.

### 3. Fundamental #2: Stay focused on the future and how you can help your team succeed. 

The world is always evolving and it's necessary that you keep up with these changes. This is a challenge every leader faces, and the best of them employ two techniques to stay on the ball.

One simple motto every leader should know goes like this: "Observe the present to prepare for the future."

A good leader is expected to keep an eye on the future so that they can help their team prepare for whatever lays ahead.

Seeing into the future doesn't involve gazing into a magical crystal ball. It means paying attention to the present and recognizing how today's events will affect tomorrow.

One exercise for staying prepared is to imagine what the world will be like in ten years. If you're going to remain an effective leader, how big will your team need to be and what tools are they going to need? Will they need more diversity and better tools? And what skills are you going to need to learn to continue guiding them?

Taking this imaginary journey into the future shouldn't be an ego trip; it's about realizing the predictions you can already make based on current trends, and finding out what you need to learn.

As always, your attention should be on how you can best take care of the people around you.

Remember, if you're only looking out for yourself, not many people will jump to your assistance when the time comes — and it will come! — that you need help.

As a leader, it's your job to set the values and goals of your team so that everyone is helping each other reach those common goals.

Alan Daddow used to help run the Australian agriculture business Elders, and he still remembers when the nature of his job as leader became crystal clear to him.

One day, it dawned on him: His only job was to do whatever it takes to maximize his team's effectiveness. Nowadays, when he looks back at his best leadership experiences, none of them involve doing something for himself. They're all about doing something for a team member.

> _"It's not just in making the decisions to act; it's about acting on the decisions you make."_

### 4. Fundamental #3: Seek out challenges and don’t be afraid to learn from your mistakes. 

Have you ever missed out on a career opportunity because you were too scared? Most of us probably have, which is why it's never too late to push yourself further and overcome those fears.

If you're going to develop the skills necessary to great leadership, you will have to challenge yourself.

After all, leaders face challenges every day, challenges that require them to step outside their comfort zone. So it's best to seek out these challenges now and test yourself so that you can grow more comfortable.

This is a good time for a baseball analogy:

A baseball player will play around 162 games every season, and it will be considered a successful season if the team wins just over half of those games. Likewise, a decent batting average for a player is considered to be around 25 percent, which means they're expected to strike out around 75 percent of the time!

Leading, like playing ball, is challenging, and everyone knows it. So part of being a success is handling failures and disappointments well by using them as opportunities to learn, grow and get better.

For the baseball player, this means trying to get a 30 percent batting average next year instead of 25. For you as a leader, it means continuing to challenge yourself and showing that you have the courage and determination to continue growing.

It takes courage to do something that scares you or puts you in an unfamiliar or uncomfortable situation. Fear of the unknown usually keeps people from taking the plunge. But it's a willingness to face that fear that distinguishes the great leaders from the average ones, and you can teach yourself how to take first, frightening steps.

To begin with, think about how you would end the sentence, "It took courage for me to…"

You've probably already done something that took courage, and reminding yourself of all the instances when you overcame your fears will allow you to develop and grow your courage.

### 5. Fundamental #4: Be open and receptive to the help and feedback of others. 

It's common for people to think of leaders as being out there on their own with no one to cover their back. But this is another misconception that you should get rid of right now.

If you hope to become a successful leader and reach your goals, you need to learn how to ask for help and seek support.

For this skill, it's good to develop strong relationships with your peers and those who are in a position to support you in your goals.

And remember, no success story happened to a leader who didn't have both a mentor and supporters, even if this help only came from family members and former teachers.

If you're still doubtful, think about the people who receive awards. When they get to the podium, each one has a list of dozens of people who helped make the achievement possible — whether that person is getting an Academy Award or a Nobel Peace Prize.

Or how's this for evidence: George Vaillant is a Harvard psychology professor who conducted the world's longest continuous study of mental and physical health. He found that the most important thing in life for good health is your relationships with others.

Along with maintaining good relationships, it's also good to pay attention to the feedback you get from others.

Part of creating a good working environment for your team is making it safe for them to provide feedback, and for you to be receptive and allow that information to influence your actions and behavior. Remember: this information will help you improve your skills.

So after your next team meeting, find someone you trust and ask them to provide honest feedback on how your behavior influenced both the discussion and the decisions that were made. Once you receive the feedback, make sure you thank them; and once you take in the information, don't try to defend your actions. Instead, simply use it as a way to grow and improve.

### 6. Fundamental #5: Have a daily practice in place to ensure continued learning. 

It's understandable if you want to become an extraordinary leader in as little time as possible. But if you're serious about this goal, it's best to take your time and remember that there are three steps to mastering anything: practice, practice and more practice.

No matter how good you are, there will always be some skill in need of development. And, of course, the most important things to work on are your weaknesses.

If you're open to feedback, you might find that your presentations can be improved. For example, maybe you'll be informed that you don't make eye contact with audience members, which is causing people to disengage from what you're trying to say.

So be thankful for feedback received and take the time to practice making eye contact. During the next presentation, you might want to try looking up at least three times; in the following presentation, make it six times, and so on until it becomes natural.

Practice and improvement take time. It'll be a while before you get the results you're after — but don't give up and don't rush things.

Leadership-development programs are also a great way to continue learning and create mutually beneficial relationships.

At one such program at the University of Delaware, Nick Martin and Georgia DiMatteo discovered that Nick was excellent at setting challenges for himself, though he had problems with motivation. Meanwhile, Georgia was great at motivation, but not so great at setting challenges.

With this knowledge, they agreed to help one another and exchange feedback after every lesson.

Okay, so now it's time to give yourself a daily habit to ensure that you continue growing no matter how many successes you encounter.

Let's look at what Harry Kraemer does. He's the CEO of the health-care company Baxter International, and for the past 35 years he has set aside 15 to 30 minutes to reflect on his day and determine what kind of impact he made and what has impacted him.

This way, he's sure to learn something about himself, every single day.

> _"Becoming exemplary in any endeavor is hard work. Leadership is no exception. Making extraordinary things happen requires extraordinary effort."_

### 7. Final summary 

The key message in this book:

**The day a great leader stops learning is the day they stop being a great leader. So make sure you stay open and curious and continue to grow and increase your skills. If you want to be someone who can offer strong and reliable guidance, it's essential that you're aware of the changes being made in your field. Stay informed and make sure your team doesn't fall behind.**

Actionable advice:

**Find a learning buddy.**

Next time you want to learn or improve a skill, choose a learning buddy and tell them about your goal. Set up regular times over the next few months to check in with one another so that you can share your progress and get feedback. Simply having someone who expects you to meet certain goals will increase your chances of success.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Leadership Challenge_** **by James Kouzes and Barry Posner**

In _The_ _Leadership_ _Challenge_, James Kouzes and Barry Posner explain how anyone can become a better leader. Citing various examples from their 25 years of experience and extensive research, the authors present their theories on what makes a successful leader, and give practical advice on how to learn good leadership behavior.
---

### James Kouzes and Barry Posner

James M. Kouzes is the Dean's Executive Fellow of Leadership at Santa Clara University's Leavey School of Business. He is a popular speaker on the subject of leadership for both corporations and government organizations.

Barry Z. Posner also teaches at the Leavey School of Business, where he's an Accolti Endowed Professor of Leadership. He is also an in-demand speaker for leadership seminars and workshops.

© James Kouzes and Barry Posner: Learning Leadership copyright 2016, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

