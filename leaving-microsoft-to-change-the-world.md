---
id: 5563c5666461640007100100
slug: leaving-microsoft-to-change-the-world-en
published_date: 2015-05-29T00:00:00.000+00:00
author: John Wood
title: Leaving Microsoft to Change the World
subtitle: An Entrepreneur's Odyssey to Educate the World's Children
main_color: 96C1E5
text_color: 4F6578
---

# Leaving Microsoft to Change the World

_An Entrepreneur's Odyssey to Educate the World's Children_

**John Wood**

_Leaving Microsoft to Change the World_ (2006) is the story of John Wood, a former top-level Microsoft employee who left his career to found Room to Read, a non-profit charity organization. Room to Read aims to help stamp out global illiteracy by focusing on children's education and gender-equality.

---
### 1. What’s in it for me? Find out how to go from corporate to nonprofit. 

Most people go on vacation and come back with a clear mind and rejuvenated spirits. But John Wood, a former Microsoft executive, came back from Nepal completely changed: his worldview had been challenged and he now had a strong desire to change his career completely.

In _Leaving Microsoft to Change the World_, Wood details how, after poverty-stricken Nepal made him realize how many uneducated children needed help, he felt compelled to leave his corporate job and dedicate his whole life to opening schools and libraries in poor and rural communities around the world.

These blinks reveal how Wood's nonprofit charity organization Room to Read got started, and how he managed to make it continually expand and grow.

You'll also discover

  * why books in Nepal have to be locked up;

  * how a dedicated 17-year-old made Wood expand his charity to Vietnam; and

  * how educating women will always benefit the greater community.

### 2. John Wood's life changed during a trip to Nepal, where he realized the value and necessity of literacy and children's education. 

Meet John Wood: a top executive who worked for Microsoft between 1991 and 1998. Wood enjoyed great success at Microsoft, until an eye-opening vacation in Nepal forced him to scrutinize the value of something he'd always taken for granted: education.

Wood was on a hike in rural Nepal when he decided to take a tour of the local schools — and his life changed forever. The condition of the schools absolutely shocked him.

Wood met with an education resource guide named Pasupathi. Pasupathi told Wood that the literacy rate in Nepal was only 30% and that he dreamed of improving the country's education system.

Pasupathi then took John on a tour of a local school. The room marked "School Library" was empty. When Wood asked where the books were, he was shown a locked cabinet. The books were so precious they had to be kept under lock and key.

The few books they did have weren't really fit for schoolchildren, either. One of them was an Italian edition of a book by Umberto Eco.

So Wood asked if the teachers would be interested in English books, and when they said yes, he promised to send two to three hundred books along.

Wood remembered his promise. When he got back to Kathmandu he emailed several of his contacts, informing them of the school's poor conditions. He asked for books and monetary donations, promising to use 100 percent of the donations to buy more children's books. He also asked people to forward the email to their own contacts.

The results exceeded his expectations. Wood quickly gathered about 3,000 books to send back to Nepal.

> _"What struck me first was the UN's estimate that 850 million people in the world lacked basic literacy."_

### 3. Wood quit his job and left his old life behind so he could devote himself to improving children's education. 

Wood felt a deep satisfaction when he sent the books to Nepal — a greater satisfaction than he'd ever felt working at Microsoft. He knew his actions had had a profound impact, so he eventually decided to leave his company and fully devote himself to education.

After a second trip to Nepal, Wood decided to pursue a nonprofit career in funding schools and libraries. There was certainly a lot of work to do, as there were so many Nepalese schools in bad condition. Wood knew there were plenty of people who wanted to work for Microsoft, but few people who had the resources to help schools in Nepal.

It was clear to him where he was needed. When he got back to his office, he told his boss he was leaving to pursue his newfound dream: helping to end illiteracy.

His decision turned his life upside down. He gave up the security of his high-level job at Microsoft to focus on his new nonprofit, Room to Read _,_ even though there wasn't a guarantee he'd be successful. (John had originally called his organization Books for Nepal, but he changed it when he decided to expand to other countries.)

Wood's job at Microsoft had given him stock options, health benefits, a travel expense account, a car and a driver. When he decided to give all that up, he relocated to San Francisco. He figured that was the best place for him to work because it put him near potential donors — his old contacts at Microsoft.

The radical changes in Wood's life also affected his relationship. He and his girlfriend now had very different aims in life, so they decided to separate.

> _"The children of Nepal obviously needed me more than my employer did. It was time to jump out of the plane and run my own show."_

### 4. Wood worked hard crafting his pitch because nonprofits can't survive without investment and donations. 

What determines whether nonprofits and charities are successful or not? Well, money, of course!

Nonprofits need people who can aggressively secure investments. They need team members who won't take "no" for an answer.

Fortunately, Wood had the right attitude. Whenever an investor rejected him, he'd try to change their "no" into a "not yet." That way he might be able to get donations from them in the future.

This is a critical part of building any nonprofit: Never give up on getting more investment.

Wood had a good strategy for securing more funding. He came up with five principles he'd stress each time he gave a pitch.

The first was connecting with the donors. Investors are usually highly educated, so Wood would personalize his pitch by reminding them of the value of their own educations.

Wood would also show the clear link between donations and the positive effects they generated. He took care to show people that their money would make a meaningful difference in his organization.

He also stressed the _overhead_. An organization's overhead is the costs required to sustain it, like employee salaries. Room to Read's overhead was only 10 percent, meaning that 90 cents of each dollar went directly to the schools themselves. The low overhead made it easier to get donations.

Wood's fourth principle was passion. He emphasized how meaningful the project was to him personally, since he left a cushy job at Microsoft for it. Passion made his pitch resonate more with his audiences.

Finally, Wood stressed meaningfulness. He did this by showing the long-term effects of his project and people's donations, to show them their money made a serious difference in other people's lives.

> _"This love of reading, learning, and exploring new worlds so predominates my memory of youth that I simply could not imagine a childhood without books."_

### 5. Vietnam became the next country for Room to Read's expansion, thanks to two chance events. 

Wood first became interested in Vietnam back when he worked for Microsoft, after meeting a 17-year-old named Vu on a business trip.

Vu worked the night shift at the hotel where Wood was staying and wanted to practice his English. He also had a book with him: _Learning Microsoft Excel_.

When Wood asked him about the book, Vu said he was studying computers because they allowed him and his country to connect more with the rest of the world. He believed computers were the future of Vietnam.

Vu was clearly passionate, but he didn't have access to that many resources for studying computer science or English. Wood's memory of the young boy inspired him to return to Vietnam after founding Room to Read.

While he was trying to figure out ways to expand to Vietnam, Wood got a call from a woman named Erin. He didn't know it then, but Erin would end up becoming an indispensable helper.

Erin had gotten Wood's number from a mutual friend, after hearing about what Wood was doing in Nepal. She'd been to Vietnam for her own community work and realized how much she loved the country after she left. She was looking for a way to return.

Erin wanted to go back to Vietnam so badly that she volunteered to work for Wood for free. She was confident her connections and network would help her expand Room to Read and that she'd eventually be hired full-time.

And she was right! After spending a few weeks in Vietnam, she met with the Ministry of Education and set up some important contacts. John hired her right away.

> _"One of the things I learned at Microsoft is that if you find a good person, you should hire them, and they will more than pay for themselves."_

### 6. At Microsoft, Wood learned lessons indispensable to Room to Read. 

When Wood left his life as a successful businessman, he didn't leave everything from Microsoft behind. In fact, the skills he learned at Microsoft proved immensely valuable in his new life.

Wood learned four important lessons from Microsoft that helped him with Room to Read.

The first was about getting results. Microsoft had always emphasized the importance of getting tangible results, and Wood brought that mentality to Room to Read. He focused on the schools and libraries themselves and kept careful track of the exact number of books donated, the number of students enrolled and the work they were doing.

These figures told Wood exactly how effectively his organization was increasing literacy. They also provided important details for attracting more investors and donors.

Wood's next lesson from Microsoft was about treating employees with respect. He knew that you won't get the most out of your team if you don't treat them well.

So Wood encouraged everyone to speak their minds during meetings. He'd give serious consideration to their opinions regarding Room to Read's plans, which made his employees more passionate about the project and also helped the organization itself, as it helped foster both critics and advisors.

The third lesson was to be data-driven. At Microsoft, Wood learned the importance of numbers — things like year-on-year growth rate, revenue from various products and comparisons with competitors. Numbers were so important to Wood that he tested his applicants on them in interviews. He even checked if they knew the numbers posted on Room to Read's website!

The final lesson was about loyalty. Wood learned what he knew about loyalty from Steve Ballmer, who'd been Microsoft's second-in-command. Ballmer treated everyone equally, no matter what level they worked at. Wood implemented this philosophy in Room to Read.

### 7. Wood had to create a strong and sustainable local network before he could expand Room to Read to new territories. 

How can you expand an organization into new parts of the world without losing sight of its original purpose and character? That was the challenge Wood faced as Room to Read grew. And even if you're filthy rich, it doesn't help to just throw money at such a problem.

Wood realized he had to create a local network of donors and other key players before he could expand his organization. So he held fundraisers to bring in new people, like volunteers and employees. The fundraisers also helped introduce the organization to new donors and investors.

These local volunteers and employees were essential to the organization. One of them was Pasupathi, the education resource guide Wood had met in Nepal. Pasupathi now delivers books and helps organize the opening of new schools.

And Wood's team didn't just deliver books and organize things — they literally helped build new schools and libraries, too! People came together for construction projects, as more buildings were needed.

Also, Room to Read's strong local base helped the project gain international support, which made it easier to expand.

Wood had to maintain his established contacts as he expanded, however. He knew if he focused too much on the organization's new areas, the older parts might start to suffer.

So he took care to ensure that Room to Read's first branches stayed intact and kept receiving appropriate funding. He carefully monitored the original Nepalese schools to make sure they were still getting a steady stream of books, funding and other key resources, too.

> _"The Andrew Carnegie of the 21st century will not be a rich white male. It will be a network of concerned global citizens, and we will create it."_

### 8. Wood understood that illiteracy was an even greater problem for women, so he founded Room to Grow, a scholarship program for girls. 

Did you know that two-thirds of the world's illiterates are women? That's why Wood made it a priority to create a new program specifically for them.

Room to Grow is a scholarship program solely for girls.

It offers scholarships to girls from underprivileged families who wouldn't otherwise be able to pay for their education. Anita, a 15-year-old from New Delhi, is one such girl.

Anita's parents decided to marry her off because they couldn't afford to keep her in school. She protested, insisting that she'd bring more money to the family if she finished her education, but, unable to convince them, she had to look for another solution. With the help of a teacher, she reached out to Room to Grow.

Room to Grow approved her petition and granted her a scholarship. Anita is still studying now, and she serves as a role model to her younger sister, who wants to follow her lead.

That's what Room to Grow is all about: using education to improve the lives of women and foster equality around the world.

And educating women isn't just about helping _them_ ; it benefits the community, too. As more women become literate, society improves.

For instance, the education that girls receive directly affects their families: it's been shown that when women are kept up to date on issues in maternal health, infant mortality rates decrease.

Education also helps people prevent the spread of sexually transmitted diseases, like HIV/AIDs and other serious and preventable ailments.

Finally, continued education leads to more opportunities for employment, which decreases hunger and poverty.

All in all, it's essential for girls to be educated. Education for girls doesn't just help them — it improves society as a whole.

> _"The United Nations estimates that two-thirds of the 850 million illiterate people in the world are female."_

### 9. The 2005 tsunami tragedy taught Wood that entrepreneurs should think big. 

Near the fifth anniversary of Room to Read, Wood took a week-long holiday. Though he'd planned to relax, he got a disturbing phone call while he was away: a devastating tsunami had just hit Asia. So Wood got back to work trying to figure out ways to rebuild schools and libraries in the affected areas.

Wood knew he could turn the 2005 tsunami tragedy into an opportunity to rebuild. Fortunately, he was able to secure an interview on CNN mere days after it occurred, which afforded Room to Read the opportunity to help in the tsunami's immediate aftermath.

A few days before the interview, Wood told his team to pledge to building schools in the villages ravaged by the disaster. He didn't know how the funding would arrive, but he knew he wouldn't rest until he'd helped as much as he could.

Then his PR consultant got him a four-minute interview on CNN. In the interview, Wood recounted the story of Room to Read, and how it had helped libraries and schools in the developing world.

Room to Read received tremendous support after the interview. Wood was stormed with phone calls congratulating him, and his organization received tons of donations in both money and books to help rebuild the region's schools.

Wood learned an important lesson from this experience: entrepreneurs should think big. Even though he hadn't initially known how he'd get enough support for his goals, he announced them anyway, trusting that some sort of funding would come through.

In the end, it did. His passionate vision of helping the region truly inspired people. They wanted to help, too, and they allowed Wood to reach his goal.

### 10. Final summary 

The key message in this book:

**John Wood never looked back after he left his cushy job at Microsoft to devote himself to ending global illiteracy. He stayed committed to his dream, and used his passion and skills from his previous job to build an international network of people equally devoted to bringing more education to the world. Room to Read and Room to Grow have changed thousands of lives. One person really can make a difference in the world.**

**Suggested** **further** **reading:** ** _The Promise of a Pencil_** **by Adam Braun**

This book tells the inspiring story of Adam Braun and Pencils of Promise, a charity he founded on just $25, and which has built more than 200 schools in developing countries all over the world. The book is divided into simple lessons that show how everybody can find their passion, make the best use of their potential and live a life full of meaning, joy and inspiration.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### John Wood

John Wood is a philanthropist, entrepreneur, best-selling author and the founder of Room to Read and Room to Grow, nonprofits devoted to ending childhood illiteracy.

