---
id: 5518a2ea3961320007ba0000
slug: legendary-service-en
published_date: 2015-04-02T00:00:00.000+00:00
author: Ken Blanchard, Kathy Cuff and Vicki Halsey
title: Legendary Service
subtitle: The Key Is To Care
main_color: BE2638
text_color: BE2638
---

# Legendary Service

_The Key Is To Care_

**Ken Blanchard, Kathy Cuff and Vicki Halsey**

_Legendary Service_ (2014) outlines the principles of great service and describes just how you can implement them in your company. As interacting with customers is a key element in almost any business, following this model is a surefire way to improve your company's performance overall.

---
### 1. What’s in it for me? Discover what you need to do to make your customer service legendary. 

They say the customer is always right, but really, that's just a start.

Customer service isn't just about getting the customer what they want. It's about creating an experience so good, with customers that are so happy, that your competitors are left in the dust.

This strategy only works when your customer service is better than great; that is, when it's _legendary_.

These blinks follow a student named Kelsey as she moves between her university classes and her workplace. Through her experiences, you'll learn exactly what legendary customer service is and discover ways to implement it in your own company.

In these blinks, you'll discover

  * why you should know if your customer prefers pretzels or popcorn;

  * how to empower employees to be accountable for customer service; and

  * how a waving bear made a grandmother's day.

### 2. Legendary service is all about building relationships that promote business success. 

We all value good customer service. Yet simply instructing your staff to "be friendly" to customers isn't enough.

The truth is, you have to genuinely care about your customers' needs to keep them coming back — and for that, you need a real service strategy. In other words, you need _legendary service_, which at its core is about building relationships that promote business success.

There are two kinds of relationships involved in legendary service. The first is the relationship you have with your employees, or your _internal customers_.

This is important! If your people feel valued, they'll enjoy coming to work and they'll share this positive attitude with customers. Thus managers need to create an environment that motivates.

The second relationship is with your _external customers_. This is where legendary service is crucial. You want to deliver the kind of service that's so consistent and smooth that customers will return to you, instead of seeking out a competitor.

Neglecting legendary service and letting bad service be the norm can have dire consequences for a business. Kelsey, a university student working in a discount store, learned this firsthand.

One day at work, a customer asked to return a defective coffeemaker. The customer unfortunately couldn't find the original receipt; and although Kelsey wanted to help the customer and accept the broken item, her manager refused to accept a return on a used item without a receipt.

The customer protested and then left angrily, never to return. Kelsey was frustrated that she couldn't help the customer, and ultimately, this experience diminished Kelsey's work satisfaction. She had to drag herself to her next shift, the previous day's unpleasantness still in her mind.

To avoid scenarios such as these, managers and business owners should follow the _ICARE model_. We'll explore this model in the following blinks.

> _"Simply put, companies can achieve Legendary Service by showing their customers they care about them."_

### 3. “I” stands for ideal service – begin by asking yourself, how you can meet your customers’ needs? 

How would you rate your last customer experience, whether shopping at the supermarket, getting your hair done at a salon or tuning up your car at the garage?

Unfortunately, the vast majority of customer experiences are either just fine or mediocre.

But when was the last time you experienced extraordinary customer service? Probably it's been a long time, if ever. This is because although most companies want to be extraordinary in the eyes of a customer, few know how to achieve it.

And that brings us to the first step of the ICARE model: "I" stands for _ideal service_. Begin by defining what _ideal service_ would be at your company **–** that is, what you need to do to successfully meet each customer's need, every day.

Getting close to ideal service requires a ton of work. Your customers have to be convinced that they were treated in a special way. In other words, you have to make people feel just how much you care about their needs and concerns.

This can have immediate results. Kelsey, our student-worker, learned this when she tried ideal service at work. Kelsey noticed a customer standing in front of a back-to-school display, and went to offer her assistance.

The woman explained that her son was going off to college and needed "everything." And although it appeared to be an emotional moment, the customer was still cheered to have someone assist her as she came to grips with her son's extensive shopping list.

Kelsey first introduced herself by name, chatted with the woman briefly and then started systematically working down the woman's shopping list, assisting her with bedding, school supplies and a microwave. Kelsey even recommended a book about how to succeed in college, that the woman's son might find useful.

At the next department meeting, Kelsey's manager read to the staff a letter he received from a happy customer, explaining how Kelsey helped turn a challenging shopping trip into a pleasant experience. And the woman promised to recommend the store to her friends.

This story epitomizes ideal service. The ideal service experience is all about making the customer's experience special and memorable, so much so that they can't wait to return.

### 4. “C” stands for culture of service. Work to foster an environment focused on customer needs. 

It's important to note that if only one of your employees is providing ideal service, that doesn't really help your business. You need to communicate ideal service to _everyone_ in your company.

To do so, create a _culture of service_. This is the "C" in ICARE! In other words, foster an environment focused on your customers' needs.

And since every culture of service is unique, yours should have its own vision and values, ones that fit perfectly to your company.

A vision can be generalized, something like, "We always want to meet the needs of our customers." On the other hand, values should be more specific, and could relate to trust, quality or continuous improvement.

Taken together, your vision and your values will lay the foundation for your service culture. The clearer your vision and values are, the better your service will be! That's why you should define what service means to you and make sure everyone in your company understands it, too.

Of course, to really make the message stick, you'll need to train your employees on how to provide great service. You'll also need to create a sustainability plan which includes follow-up activities to maintain a high level of service. This may include setting up concrete service goals, such as a target satisfaction rate, and measuring progress.

When Kelsey started thinking about how to implement a culture of service at her own company, she reached out to one of her professor's former students, a woman who now owns a clinic.

Kelsey visited this clinic with her grandmother and immediately noticed the clinic's warm, friendly atmosphere. Everyone seemed to care about Kelsey's grandmother, even having just met her!

The owner explained that she shared her _service vision_ with her team from the moment she started at the clinic, which was expressed as: "To treat our patients as family and nurture them back to health." Kelsey even saw the service vision text on a poster, hanging in the clinic's hallway.

### 5. “A” stands for attentiveness. Figure out who your customers are and be attentive to what they want. 

Once you've established a culture of service, you have to practice _attentiveness_ by thinking about who your customers are and what they want.

In other words, get to know your customer base!

Start by dividing your customer base into smaller groups, based on common needs or preferences. This will allow you to create specific customer profiles.

Next, collect some information. Ask yourself: What are my customers' preferences, what do they expect from me and how can I fulfil their needs? Once you've gathered the answers to these questions, try to figure out how you deliver better customer service in a measurable way.

This process is important, because when you know your customers, you can treat them the way they expect to be treated. Studies show that it's actually not just the first impression that's important; the last impression is equally critical for the total customer experience.

For example, if a regular customer is treated badly just one time, this event can destroy the entire relationship, even if for 99.9 percent of the time before the customer was treated well.

Kelsey better understood the value of attentiveness after speaking with a friend who works at a local bar. To deliver legendary service, he makes sure to pay particular attention to regular customers as well as new visitors.

There's one man who visits two or three times a week, and likes to drink a beer at the bar. The bartender knows the man's regular beer and often listens to his complaints about work. Once the man asked for popcorn; and since then, the bartender made sure to always put a bowl of popcorn on the bar when he sees the man walking in.

In another example, on Saturdays a young couple visits the bar and prefers a table in the corner. They like pretzels, so the bartender never forgets to give them a bowl with their drink order.

Kelsey realized during their chat that her bartender friend's attentiveness keeps his regular customers coming back.

### 6. “R” stands for responsiveness. Show your customers how much you care about their needs. 

Although it's important to know your customers' needs, that alone isn't enough to achieve legendary service. You also have to show a willingness to _meet_ their needs.

If you practice _responsiveness_ — which extends the concept of attentiveness — you'll likely hear customers say things like, "She made me feel cared for, like she was _on my side_."

To achieve this, start by listening to your customers. And in addition, find a way to ask the right questions to keep them talking.

While you're asking questions and listening, show empathy by using verbal and nonverbal communication. One way to show empathy is by paraphrasing the other person's remarks and mirroring their feelings, which communicates the fact that you're really listening.

This step is especially important when customers are unhappy, because you won't be able to help solve their problems if you don't understand what's going wrong. (On that note, always remember: the customer isn't the problem — the situation is the problem.)

When Kelsey and her grandmother visited the zoo, their tour guide perfectly demonstrated responsive customer service. During their bus tour, Kelsey noted the guide's sense of humor and friendly delivery, so she decided to ask him to do something special for her grandmother's birthday.

Saying it was no problem at all, he stopped the bus in front of the bear compound. He then asked the bear via the bus microphone to greet a special birthday guest. The whole bus began to wave at the bear and finally, the bear waved back.

Afterwards, the guide told Kelsey that after 13 years on the job, the best thing about his job was listening to people and meeting their needs to keep his job's focus on the fun. And it certainly works! Kelsey's grandmother was delighted to be treated in this special way.

> _"Being responsive to your customers goes hand in hand with being attentive to their needs and preferences."_

### 7. “E” stands for empowerment! Empower your employees to deliver great service to customers. 

The crucial last element in the ICARE model is to _empower_ your employees.

This is a critical because, as we saw earlier, frustrated employees won't deliver great service to external customers.

Employees should be empowered to do what's necessary to provide great service, according to the company's broader service vision.

Sometimes managers are reluctant to empower their staff, but you have to remember that your service employees are the ones who make a difference to your customers — not top-level managers. And because of this, empowering the very people who interact with customers has a huge impact on customer satisfaction.

One way to empower employees is by offering more training. By attending workshops where they can learn new skills and more about your company's products, your employees can then give more qualified service to customers.

Additionally, make sure your salespeople feel comfortable standing up for what they want and offering suggestions to help improve your business. Being empowered in this way will make employees feel valued, which will in turn make them feel more motivated. And that will result in better service for your customers, leading to better results companywide.

When Kelsey tried to change the service policies at her workplace, management listened and rewarded her efforts. When she brought up the legendary service model to her boss, he liked it but said he couldn't do anything about it, as the managers above him were busy with other matters.

But when personnel changes at the top created new opportunities, Kelsey's boss made sure to recognize Kelsey for her great work by offering her a position as department manager.

Then her plan for achieving great service was implemented across the entire organization, which led to more customers — and of course, happier ones! — than ever before.

### 8. Final summary 

The key message in this book:

**Delivering great service starts with understanding your customers' needs. Once you've figured out what your customers want, you can focus on showing them how much you care about their needs. Additionally, you have to empower employees to provide great service, according to the company's broader service vision.**

Actionable advice:

**Take the initiative! The path to great service might start with you.**

Start by making whatever service you personally deliver to customers as good as it can possibly be. And then, if you get positive feedback, talk to your manager and share your ideas on how to achieve better service. If you're convincing and passionate about your ideas, you might actually be able to change your whole company for the better!

**Suggested** **further** **reading:** ** _Selling the Invisible_** **by Harry Beckwith**

Services represent a significant and growing portion of the modern economy, yet marketing them remains a mystery. _Selling the Invisible_ serves as a guide for promoting the intangible. It outlines how to set up a marketable service company, and how that business can then be advertised and promoted.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Ken Blanchard, Kathy Cuff and Vicki Halsey

Ken Blanchard is an entrepreneur and author specializing in management issues. His bestselling book, _The One Minute Manager,_ sold more than 15 million copies. His company, Ken Blanchard Companies, offers international management training and consulting services.

