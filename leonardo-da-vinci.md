---
id: 5a64b06bb238e10007b0c9ba
slug: leonardo-da-vinci-en
published_date: 2018-01-24T00:00:00.000+00:00
author: Walter Isaacson
title: Leonardo Da Vinci
subtitle: The Biography
main_color: E35132
text_color: BF442A
---

# Leonardo Da Vinci

_The Biography_

**Walter Isaacson**

_Leonardo da Vinci_ (2017) is an illuminating and thoughtful account of one of history's most renowned individuals. Isaacson has studied previous biographies and accounts of Leonardo's life — including the very first one, from the sixteenth century — as well as Leonardo's wealth of notes. Isaacson paints a very human portrait of the legendary Renaissance artist and engineer. Though one of the few people in history who may truly be called a genius, Leonardo was only human and there is much that can be learned from his curiosity and approach to life.

---
### 1. What’s in it for me? Understand the life of an icon of innovation and discovery. 

Leonardo da Vinci was no ordinary artist. He was the prototypical Renaissance man, unashamed to dabble in many fields. In fact, part of what made him such an incredible innovator was his broad range of interests and his mastery of multiple disciplines — all of which were driven by a deep curiosity in the place occupied by humankind in nature.

Leonardo is a myth in his own right. He has inspired artists and creative minds from the painter Raphael to the novelist Dan Brown, from surrealist artist Salvador Dali to musician Bradford Cox, lead man of the band Deerhunter. But if you really want to understand Leonardo, you have to go beyond the myth.

Leonardo's was no ordinary life, and these blinks break it down and put everything in context. It's a lot easier to appreciate Leonardo's talent and insights if you really know what he was reacting against and what his ideas and works foreshadowed.

Above all, this is a story of inspiration. It's the story of a man who spent much of his life working on commission, failing to finish projects or publish his greatest works. It's a story that shows that, in the end, talent will always prevail, no matter the distractions or difficulties along the way.

In these blinks, you'll learn

  * some fifteenth-century German slang;

  * what Leonardo's to-do list looked like; and

  * what you can learn by pouring wax into a bull's heart.

### 2. Leonardo was an illegitimate child, but this worked in his favor. 

Even in the best of times, being labeled "illegitimate" would hardly be considered a great plus. But for Leonardo da Vinci it turned out to be a distinct benefit.

Leonardo was born on April 15, 1452, to Piero da Vinci and Caterina Lippi, a 16-year-old peasant girl. They were not married.

Piero was a notary in the town of Vinci. In other words, he worked for merchants and upper-crust princes, resolving disputes and handling business transactions. It was a pretty respectable job as these things go — it even meant that Piero could use the honorific title "Ser."

But because Leonardo had been born out of wedlock, he was under no compulsion to join Piero's business.

Ser Piero's father, grandfather and great-grandfather had all been notaries and, in normal circumstances, Leonardo would have become one, too. However, to be a notary you had to be a member of the guild. Needless to say, there were rules about who they let in. Certainly no children born out of wedlock, which is why the door was closed to Leonardo.

However, this exclusion gave Leonardo the opportunity to flourish. Nor was he the only one who benefited from being an illegitimate child. In fact, some of the most famous cardinals, princes and generals of the day had been born out of wedlock. The same was true of the Renaissance poets Giovanni Boccaccio and Petrarch, as well as the artist Leon Battista Alberti, who himself greatly influenced Leonardo.

Theoretically, Piero would have been able to fill out the paperwork to "legitimize" Leonardo at a later date. But there was little point — it was clear from early on that Leonardo was destined for life as an artist.

It made sense. He hadn't been sent to any of the foremost Latin schools, and this further spurred Leonardo to learn through his unique personal curiosity and from experience. Another sort of life was in store.

### 3. Leonardo excelled as an apprentice and eventually surpassed his teacher. 

In 1464, Piero moved with little Leonardo from Vinci to Florence, a city renowned for its arts and commerce. At the time, there were 83 silk workers, 84 wood carvers and 30 painters living there. On top of that, the city had the highest literacy rate in all of Europe.

It was in Florence that Leonardo really began to flourish. At 14, he started an apprenticeship in the workshop of Andrea del Verrocchio, an artist and engineer who was also a client of Piero's.

In those days, becoming a painter and artist was very much like becoming a carpenter or blacksmith. It was a very formal artisanal job.

Verrocchio's workshop was no different. Clients ordered paintings from Verrocchio's studio much as they did vases or brass doorknobs, which were also available to order there.

Actually, it's partly thanks to Leonardo's achievements that artists are not seen as mere craftspeople these days, but rather as creative, highly-respected individuals who can produce thought-provoking works. But Leonardo the artist was shaped in Verrocchio's workshop. The training was rigorous, and Leonardo really developed his skills: he gained an understanding of perspective, geometry and how to paint light and shadow.

By the time he was 20, Leonardo had not only earned the title of "master painter," but surpassed the talents of his teacher as well.

You can compare the work of the two painters most clearly in _The Baptism of Christ_, on which Verrocchio and Leonardo collaborated.

The painting shows a pair of angels beside Christ. The one painted by Verrocchio is rather flat and lifeless. There's a sharpness of feature that makes the angel's hair seem to sit atop its head like a poorly placed wig.

Leonardo's angel is quite different. It has a lively face and eyes, and there are no sharp edges. He uses a technique known as _sfumato_ — which can be translated as "smokiness" — to soften sharp lines and make the transition from hair to forehead look natural.

Leonardo used _sfumato_ because he thought it more accurately represented how the human eye perceives objects. In centuries to come, he would become renowned throughout the world for this very technique.

### 4. Before he left Florence, Leonardo was already famous for pioneering bold new painting techniques. 

During the 1470s, Leonardo continued to work in Verrocchio's studio as a master painter.

While there, he painted a remarkable portrait: _Ginevra de Benci_. It was his first painting not devoted to a religious theme, and he even managed to demonstrate a whole new style of portraiture.

Up until that point, most portraits in that style tended to be side profiles. But Leonardo chose a three-quarter pose for the sitting Ginevra. And instead of a straight portrait, Leonardo attempted to capture something of her mysterious inner-self. Her eyes and smile show her to be an intense and thoughtful young woman.

But it wasn't just Leonardo's reputation as a painter that grew around this time. He also became known to the local authorities.

Anonymous allegations were twice made to the police. These claimed that Leonardo had illegally engaged in sodomy. However, as one of the allegations also mentioned a member of the powerful Medici family, strings were most likely pulled to ensure the accusations — and any possible charges — had no chance of going anywhere.

Not that what he was doing was particularly unusual. Florence was a city known for homosexuality. In fact, "Florenzer" was the slang term for "homosexual" in Germany at the time.

In 1477, Leonardo opened his own workshop in Florence and began work on what would be the first of many unfinished projects. This painting, known as the _Adoration of the Magi_, took an extremely novel approach to the subject matter.

It was a common enough scene to paint. And, normally, the artist showed the three kings bringing gifts to the baby Jesus.

But Leonardo planned it differently. Mary and Jesus would sit in the middle of a swirling tempest of emotion. Even the animals depicted would be overcome with spiritual awe.

Externalizing psychology through gesture and expression was precisely the same technique he had used in his portrait of Ginevra. But this time it was even more radical.

The painting was never finished, with the sky and a few finishing elements remaining untouched. Leonardo left both his painting and the city of Florence itself behind entirely.

### 5. In Milan, Leonardo began to explore his many passions while entertaining the royal court. 

Though it only took a week to travel from Florence to Milan in 1482, the two cities may as well have been worlds apart. While Florence was a republic where wealthy bankers were highly influential, Milan was ruled over by the Sforza family.

Leonardo had made his way there in the hope of finding a willing patron in Ludovico Sforza, the future Duke of Milan. Leonardo supplied Ludovico with an impressive list of his abilities. He claimed he was proficient in military engineering and so would be able design portable bridges, mobile cannons and armored chariots. Otherwise — Leonardo added tangentially — sculptures and paintings would be his pursuits in times of peace.

It was clear even then that Leonardo was determined not to limit himself to painting alone.

During his years in Milan, Leonardo sought to explore his primary interest, namely, the analogous relationship between nature, humans, architecture and engineering.

For instance, in the early 1480s, Leonardo suggested that a healthy city would best operate like a healthy human. Canals would circulate clean water as veins circulate blood, while an underground sewage system would dispose of waste as the bowel system disposes of feces. This idea was far ahead of its time. However, Ludovico didn't take Leonardo up on his offer to redesign Milan's waterways.

Leonardo did find his niche elsewhere, though: the Duke loved having Leonardo on hand to assist with entertaining at court.

Even before he arrived in Milan, Leonardo had gained experience in the theater. He'd designed props and painted backgrounds for productions in Florence. In Milan, he went into overdrive. His pageants, plays and scientific demonstrations were all hosted at the Sforza court.

Leonardo's _The Masque of the Planets_ was an especially elaborate performance. Its mechanical props and painted sceneries enchanted the audience. And with this success, Leonardo's stock rose even higher. In fact, he was achieving more fame as an entertainer than as a painter!

### 6. Leonardo took advantage of his access to great minds by collaborating and exchanging ideas. 

In his way, Leonardo was a charmer. Those who knew him described him as radiant, handsome, graceful and a gifted storyteller. According to his first biographer, Vasari, Leonardo's mere presence could comfort the most troubled soul.

The Sforza court in Milan attracted brilliant minds from all fields. Leonardo set about using his social skills to make connections and create opportunities. Today, we might call it networking.

For instance, though Leonardo was pretty adept at geometry, his math skills in other areas weren't so developed. Luckily, he was close to the mathematician Luca Pacioli, who helped him improve his arithmetic. In fact, Leonardo's notebooks contain many to-do lists, one of which reads, "learn the multiplication of roots from Maestro Luca."

Leonardo was also an enthusiastic collaborator. For instance, he provided the illustrations for Pacioli's 1498 mathematics book, _On Divine Proportion_. They were the only drawings of his to be published in his lifetime, and they're surprisingly beautiful.

He depicted polyhedrons as wooden skeletal structures and used clever shading and transparent faces to help the readers understand their geometric characteristics.

As well as mathematicians, Leonardo loved spending time with architects, especially Donato Bramante and Francesco di Giorgio, both of whom also had artistic tendencies.

They often found themselves discussing Vitruvius, the Roman military engineer from the first century BC who wrote a very famous treatise on architecture.

Vitruvius thought buildings should follow the principles of the human body. So, a temple should be designed with a central point that is equidistant to its front, back and sides, just as the human navel is equidistant from the top of the head and the tips of the outstretched hands and feet.

It was an idea that found favor with Leonardo. He was becoming increasingly obsessed with the proportions of human anatomy and analogies to it that could be found in nature.

It was these discussions that led directly to the creation of Leonardo's most famous drawing, _Vitruvian Man_, which depicts a man — not unlike Leonardo himself — set proportionally in the middle of both a circle and a square.

> _"Because of his love for animals, Leonardo was a vegetarian for much of his life…"_

### 7. Leonardo’s Milan paintings show his revolutionary approach to narrative and psychology. 

_Vitruvian Man_ wasn't the only incredible piece of art Leonardo produced during his time in Milan.

The Duke also commissioned him to produce two portraits.

The first and most well known is _Lady with an Ermine_. It's a portrait of Cecilia Gallerani, one of Sforza's mistresses, with whom Ludovico had a child in 1490 when she was 16.

It's been suggested that the commission was given to commemorate the pregnancy.

Once again, Leonardo's portrait broke with tradition. It's an image brimming with mystery. It seems to depict a woman who is obviously thinking about something.

Her posture and the picture's symbolism tell a story. Her inner turmoil can be felt even after all these years. For instance, by the way her hand is tenderly placed on an ermine — a kind of short-tailed weasel — the preciousness of her relationship with Sforza is made clear. The clue is in the animal: the Duke had been awarded the Order of the Ermine by the king of Naples.

When we compare the portrait to that of Ginevra de Benci, it's clear that Leonardo had been working on his sfumato technique. The play of light and shadow gives the image an extraordinary depth. Additionally, modern analysis has revealed that Leonardo used extremely thin, nearly transparent layers of glaze to help him control the colors' brilliance.

The second portrait, _La Belle Ferronnière_, is also thought to depict one of the Duke's mistresses who had become pregnant, Lucrezia Crivelli. She was a lady-in-waiting to the Duke's wife, Beatrice D'Este.

It's an odd picture. The interplay of light and shadow is lively, but the hair is a bit flat, there's a strange glare on the cheekbone, and her body doesn't quite sit right. Only the ribbon on Lucrezia's dress has Leonardo's distinctive touch to it.

This raises the possibility that parts of the picture may actually be the work of one of Leonardo's students. But Leonardo's hand and influence can still be detected.

### 8. The Last Supper is a masterpiece, as Leonardo captures an impossible moment. 

Like every great artist, Leonardo knew that observation was a prized skill. Humans, nature and animals were all subjects of detailed survey in his notebooks. Horses especially crop up repeatedly as he spent years working on a monument of one that was sadly never completed due to the arrival of the invading French forces in 1494.

Otherwise, humans were his main interest. And much of what he witnessed through observation was digested and put into practice in the world-famous _The Last Supper_.

The Duke had commissioned Leonardo to paint a fresco in the dining room of the Santa Maria delle Grazie monastery. Leonardo started in 1495, but in typical fashion, he wasn't exactly speedy, much to Ludovico's immense frustration.

But Leonardo did have a lot to think about, so we can't blame him too much.

Creative approaches to optics, perspective, as well as to human posture and gesture, were all used as Leonardo attempted to capture the moment when Christ revealed that Judas had betrayed him.

It wasn't just a single moment that Leonardo wanted to depict. He was also trying to explain to the viewer what had just occurred and what would happen next. And Leonardo did it 12 times over, once for each apostle. If you look from one to the next, it's possible to get a real sense of the range of emotions: they're angry, startled, resigned to fate or, in the case of Judas, greedily clutching a bag of silver.

Leonardo also uses a few tricks to really impress the moment upon the viewer. The table is impossibly narrow and he plays with the lines of perspective to create a sense of depth that draws the eye to the figure of Christ himself. But he did this deliberately. He was playing with the rules of perspective he'd been so obsessively studying. It was a sign that he had brought his work in geometry and science to his art.

### 9. Under the employ of Cesare Borgia, Leonardo would reach his limit as a military engineer. 

Around 20 years after Leonardo had boasted to Ludovico Sforza of his skills as a military engineer, he finally faced the true horrors of war.

The year was 1499 and Louis XII's French troops took Milan and broke up the Duke's court. The French didn't object to Leonardo himself; in fact, plans were in place for Leonard to meet the French king. However, the ongoing military conflict meant that Leonardo had to make his way back to Florence.

Leonardo found himself under the wing of one Cesare Borgia, an ally of Louis's who was setting about conquering territories all over Italy. Most likely it was Niccolo Machiavelli, the author of _The Prince_, who, while negotiating Florence's surrender to the all-conquering Borgia, also arranged the deal to secure Leonardo's services.

With Borgia's approval, Leonardo toured war-torn Italy and suggested how bridge and fortress designs could be improved. He calculated, for instance, that if fortresses had curved walls instead of flat ones, cannonballs could be partially deflected and so cause less damage.

What Borgia really loved though was Leonardo's amazing, detailed maps. To take an example, Leonardo's 1502 map of Imola melded art and science. Unusually, it was in color and represented the territory from a bird's eye perspective. And it was to scale, too, mapping out distances between houses and towns in every direction.

It was thanks to this sort of precision and detail that Borgia was able to launch lightning attacks before the independent princes who were his enemies had time to prepare a defense.

After eight months, however, Leonardo had had enough. Though Borgia was known for his charisma, he was also a corrupt and vicious murderer. After all, it's well known that he was the man that Machiavelli based _The Prince_ upon. And even Machiavelli was troubled by the horrors inflicted by Borgia's army.

Becoming a military engineer had been a lifelong dream for Leonardo, but it had turned into a nightmare.

> _"'Save me from strife and battle, a most beastly madness,' Leonardo once wrote."_

### 10. Even Leonardo’s unfinished works proved to be influential. 

When Leonardo returned to Florence he met the new kid on the block — Michelangelo, whose statue, _David_, was a sight to behold.

Presented with such talent, the authorities devised a challenge and commissioned both Leonardo and Michelangelo to paint different sections of the Florence Council Hall.

But competition appealed to neither artist.

Leonardo's planned fresco could potentially have been as powerful as _The Last Supper_. It would have been even larger, covering around one-third of a 174-foot wall. The proposed subject was the Battle of Anghiari, a Florentine victory from 1440. Leonardo envisaged tremendous swirls of blood, dust, grinding teeth, wailing horses and panic-stricken soldiers.

Michelangelo was meant to paint another wall, using a different battle as inspiration.

The two artists did not get along. For starters, the moody Michelangelo was rude to Leonardo at every opportunity he got. And besides, the competitive atmosphere really didn't suit Leonardo's artistic temperament.

Just as he had done earlier in Florence with _Adoration of the Magi_, Leonardo abandoned the _Battle of Anghiari._

But this time the stakes were higher: it would have been a huge work. But his abandoning it is understandable. _The Last Supper_ had shown Leonardo that fresco work requires a different set of skills than painting. He wasn't a trained fresco artist and in his initial work on the _Battle of Anghiari,_ Leonardo struggled to find the right paint mixtures to help the fresco adhere to the plaster.

Despite their incompleteness, Leonardo's efforts were celebrated. When the preparatory sketches for both works were displayed to the public in Florence, young artists traveled across Europe to admire them.

The famed Raphael himself drew his own versions of them both, while Cellini called the display "the school of the world." Today, the drawings are still considered exemplary specimens of the High Renaissance.

### 11. Leonardo’s insightful work on human anatomy was lost for centuries. 

Before leaving Florence for good, Leonardo took the opportunity to resume his studies of human anatomy.

In many ways, the results of Leonardo's close examinations of the human body can be counted among his most impressive works. However, they lay undiscovered for many years.

An anatomy professor at the University of Pavia was able to provide around 20 fresh corpses for Leonardo to dissect. While engaged in this macabre work, Leonardo made beautiful drawings of each and every bone, muscle and human organ.

Clearly, an intimate knowledge of anatomy is immensely useful if you want to become a better painter of the human body. But Leonardo's interest wasn't merely artistic. His learning ran deeper that visual perception alone.

He wrote 13,000 words of text to accompany the 240 drawings, which detailed his thoughts on the functioning of the eyes, the heart, the brain and even the human fetus.

These drawings and observations would have massively contributed to the sum of human knowledge had they been published. Leonardo was far ahead of his time. For instance, his notebooks contain the very earliest description of the effects of arteriosclerosis upon the body.

But perhaps his most impressive study is that of the heart.

At the time, the liver was the organ most associated with blood. Leonardo not only recognized the importance of the heart; he was even able to understand the workings of the aortic valve, thanks to his prior understanding of the laws of fluid dynamics and a bit of artistic trickery with wax and a bull's heart: he filled the heart with wax and used the resulting mold to create a glass replica so he could observe the motion of fluid within.

Amazingly, it would be 450 years before modern science really got to grips with fluid dynamics in the heart. When it did, it of course showed Leonardo's hypothesizing was right all along.

Leonardo's observations remain useful to this day today in part because, like all great teachers, he really knew how to make things more understandable. He used fantastic analogies, like describing the heart as the "central seed" from which all "roots and branches emanate."

### 12. King Francis I’s patronage allowed Leonardo to put the finishing touches on his final three paintings. 

In 1507, Leonardo returned to Milan. Now, though, he found himself far more inclined to work on scientific studies of air, water and geology. These dovetailed nicely with his observations on birds, fish and flying machines.

After a misguided sojourn in Rome between 1513 and 1516, Leonardo suddenly found himself with the patron to end all patrons: the charismatic — like so many of Leonardo's patrons — King Francis I of France.

At 64 years of age, Leonardo traveled out of Italy for the first time in his life. He was never to return. He was to become the King's official artist and engineer.

King Francis went so far as to call Leonardo the "most eminent painter of our time." Leonardo was also given the opportunity to design a new royal village out in the country, but it was never built. On top of that, Francis even gave Leonardo a proper salary. He would be paid by individual commission no longer.

In 1517, Leonardo received a visit from Cardinal Luigi of Aragon. The Cardinal's secretary, Antonio de Beatis, recorded the meeting. We're told that Leonardo admitted that a recent stroke had deprived him of the use of his right hand, and he felt his days as an artist were behind him. But he also showed his guests three paintings he was in the process of completing, each one a masterpiece:

One was _Saint John the Baptist._ In it, a seductive and androgynous long-haired figure emerges from the shadows, a raised hand pointing cryptically toward the heavens.

The second was _Virgin and Child with Saint Anne._ It's still a masterpiece, but the author personally thinks the expressions of the subjects pale in comparison to Leonard's depiction of John the Baptist.

The third, which he'd been carrying around for years, would go on to become one of the most famous paintings of all time.

### 13. In the Mona Lisa, we see the culmination of Leonardo’s interests and abilities. 

Throughout his life, Leonardo strove to understand and unravel the mysteries of the world. It wasn't because he desired fame or glory, but because he was driven by a deep curiosity.

Specifically, his paintings were exercises in motion and emotion. A simple gesture for Leonardo could reveal a person's mindset and motives. This approach made him one of history's greatest painters. Leonardo had breathed life into what was formerly still and static.

The _Mona Lisa_ is often thought of as Leonardo's absolute masterpiece. It is not just a painting of a woman but also points to his interests outside painting, and is a testament to the fact that some human mysteries are insoluble.

The portrait is best known for its subject's smile, which seems to express different emotions depending on your point of focus.

If you look at her eyes, the smile is out of focus. This makes the upturned corners of her mouth look as though she's smiling. But when you focus on the mouth itself, the smile disappears.

In this image, too, Leonardo used his studies of optics and perception to blur the landscape. This contrasted with the standard method of the day, which was to keep all aspects of the canvas in focus, no matter their respective distances.

His studies of light and reflection gave him the idea of using a lead white undercoat. This catches the light through thin layers of translucent glaze. The effect is especially evident around the cheeks. They seem almost to shimmer with life.

Books have been written on the _Mona Lisa_ 's composition, but here it is enough to point out that the background is also revolutionary. The river in the background flows into the sitter's silk scarf, and the fluid landscape becomes one with her hair, as though she's merged with nature.

By applying nature's patterns to the human form, Leonardo encapsulated his research into nature and the relationship between it and humanity. This message continues to captivate audiences far and wide.

Leonardo would possess _Mona Lisa_ for the rest of his life, passing away eight days after his 67th birthday, on April 23, 1519.

### 14. Final summary 

The key message in this book:

**Leonardo da Vinci is perhaps best known for how his bold new style of painting pushed the medium forward and helped usher in the High Renaissance. But he was much more than a painter. If Leonardo's notebooks had been published after his death in the sixteenth century, the world would likely have taken a different course. His studies in anatomy, architecture, engineering and fluid dynamics were all far ahead of his time. There are few people who can be called genuine geniuses, but Leonardo is one of them. Unlike Albert Einstein and Stephen Hawking, however, Leonardo intrigues not because of his superhuman intellect, but because he was an ordinary person with a curious mind.**

Actionable advice:

**Get out your pencils!**

We can't all be as talented as Leonardo, but there's a lot to be said for engaging with and enjoying the nature around us. Why not take a walk in the park with a sketchbook and a few pencils? Colored pencils aren't even necessary for this. Observe, imbibe, enjoy.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Da Vinci's Ghost_** **by Toby Lester**

_Da Vinci's Ghost_ (2012) takes you back in time to the Renaissance, when Leonardo da Vinci created his iconic drawing, the _Vitruvian Man_ — a naked figure framed in a square and circle. Today, this drawing can be found everywhere — it's even featured on coins in some countries. Find out what was going on at the time and what Leonardo might have been thinking when he embedded art, science and philosophy all in one image.
---

### Walter Isaacson

Walter Isaacson is the author of acclaimed biographies of Albert Einstein, Steve Jobs and Benjamin Franklin. He is the former managing editor of _Time_ magazine and the current CEO of the Aspen Institute, a nonprofit organization striving to promote innovative ideas and leadership.

