---
id: 534f7d9b6531660007f50000
slug: less-doing-more-living-en
published_date: 2014-04-23T09:22:36.000+00:00
author: Ari Meisel
title: Less Doing, More Living
subtitle: Make Everything in Life Easier
main_color: DC2C48
text_color: C2273F
---

# Less Doing, More Living

_Make Everything in Life Easier_

**Ari Meisel**

_Less Doing, More Living_ (2014) guides you through nine fundamental steps on your journey toward becoming more effective. In these blinks, the author shares his favorite tools and techniques for optimizing, automating and outsourcing everything on that pesky to-do list, thus giving you time for the things that are most important in your life.

---
### 1. What’s in it for me? Learn how to free up more time for yourself by becoming super efficient. 

We all wish we had more time to spend on the things we love doing, whether that means being around our family or working on our hobbies. Yet, despite these wishes, time is something of which we never seem to have enough.

This is where these blinks come in. They show you how you can find time in your schedule and space in your brain to spend most of your time doing what you want to do. Based on three central aims — _Optimize, Automate_ and _Outsource_ — they inform you how to unleash waves of efficiency in everything you do, from organizing your finances to maintaining your health.

In these blinks, you'll discover

  * why you'll never have to personally book a table in a restaurant ever again;

  * why running errands is a thing of the past; and

  * how to turn your smartphone into a portable bank manager.

### 2. Track your life to learn where you can start saving time. 

Maybe you've heard of the 80-20 rule? It states that 80 percent of what you accomplish is the result of a mere 20 percent of all the time and energy you invest. And this isn't only applicable to work or study. It's the case for _everything_ you do. So how can we figure out what time isn't worth spending?

One way is to start tracking.

But who's got time to live their busy schedule _and_ observe it? Luckily, there are resources out there to make tracking your time much easier.

Enter _RescueTime_, a program that monitors what you're actually doing on your computer all day. If you find that a particular website or application is stealing your day away, RescueTime will help you block access to it. Problem solved!

But what if it's not just your productivity you want to track, but your health?

Take it back to basics with an old-school paper journal to record everything you eat during the day. You could also get your hands on a pedometer to count all the steps you take, or simply make an effort to weigh yourself each day. Because the more you know about your day, the sooner you'll be able to improve it.

But what does it mean to improve something as broad as our daily routine? Think of it as _optimizing your processes_. For a role model, look to IKEA. Each IKEA product comes with highly optimized instruction manuals for assembling each product, whether it's a coffee table or geometric lamp.

These instructions have been boiled down to their very essence — a few language-independent, easy-to-follow steps. It's time to approach your life like IKEA approaches their manuals. Whatever the task, whether it's writing emails or organizing meetings, you should minimize fuss and get it done in as few steps as possible.

### 3. Create an external brain to save valuable space in your own. 

We all like to think of ourselves as great multi-taskers with brilliant memories. But often we don't actually have enough room to juggle everything in our minds. Life would be so much simpler if we just had an extra brain.

Well, guess what? You can have one!

Several new tools have emerged to help us remember all those dates and details. Evernote, for example, is an app for taking notes of all kinds: textual or pictorial, hand-drawn or web-based. It's also free and has no storage limit. The author himself has about 1800 notes in his Evernote at the moment!

You can consider Evernote simply as a _backup_ of your brain. Neuroscientists have even shown that Evernote organizes information in a very similar way to the human brain, linking similar thoughts together into complex, interconnected networks.

But what if you know how to organize but want to spend less time organizing?

Say hello to your very own virtual assistant. Today, you can hire a person or several people and delegate tasks to them _online_. Additionally, there are two kinds of virtual assistants: _on demand_ and _dedicated_.

An on-demand assistant service consists of a pool of assistants. Say you've got a task that needs doing, like proofreading a document or improving a PowerPoint presentation. Whoever's available in the pool will do it. For $25 a month, Fancy Hands allows you to get assistance with as many as five tasks. For $95, you can request assistance with an unlimited amount of tasks per month.

Dedicated assistants, on the other hand, will do all your tasks personally. This means there's a closer relationship; they'll get to know you and eventually take on more difficult tasks. For $197 a month, Zirtual gives you a dedicated assistant for ten hours every month.

### 4. Customize to save time – and money! 

Say you've got a great idea for the perfect bedroom shelving unit to suit your needs. You know you'll have to build it yourself, which means buying the materials, borrowing tools and spending hours preparing and cleaning up. But if only there were an easier and less expensive way!

Well, there is: 3D printing.

3D printing offers a world of possibilities for creating your own customized products. Take Shapeways, a site where you simply upload a 3D model, and they print it for you.

The author, for example, wanted to create a custom bracket to mount his new MacMini to the wall. He made a simple sketch of his idea and used the online outsourcing platform Fiverr to find someone who could make a complete 3D model of it. He then sent this model to Shapeways and, a week later, the completed bracket was waiting for him in his mailbox.

If that sounds like a success to you, then get this: the author then used the Shapeways ecommerce platform to sell his MacMini bracket, and made $400 by selling six of them.

Customization won't just save and earn you money, however. It will also give you more free time.

Consider _Vitamins on Demand_, a service that collects all the medications and supplements you require and then organizes them into separate packs for each day. Say goodbye to loading up your holiday suitcase with all your pill bottles while worrying whether you've got them all. Instead, just grab the number of packs you need and head out the door!

Want another example? Think about the arduous task of finding a suit that fits. If you want to cut that time in half, sign up to _Indochino_. This company produces tailored suits and will guide you through your measuring process, ensuring you've got measurements you can use again and again for a suit that fits just right.

### 5. Take control of your workweek. 

When Tim Ferriss published _The 4-Hour Workweek_, there was obviously some exaggeration involved — you simply can't get a book written if you only work four hours each week! Nevertheless, the book made an important point: you need to make your workweek _yours,_ and decide what to do, and when.

So what's the first step?

Begin by determining which hours of the day are your best working hours. This will automatically make you more effective. Some people prefer working in the night; others, in the morning. Some like working a bit every day, while others like to squeeze a lot into shorter periods of time.

Everybody works differently, so figure out what works best for you!

It's also vital to figure out what is slowing you down. Often it's external factors — a supplier that's always late, or those indecisive clients.

The solution?

Make the window for interaction with your clients smaller. This won't only make your own day more effective, but force your clients to be more efficient, too. They can thank you for it later!

And there's also another part of your workday that's sapping your time: the scheduling of meetings. Did you know that, on average, the normal procedure for organizing a meeting requires an exchange of _seven_ emails? Luckily, applications such as ScheduleOnce will, as the name suggests, help you get that meeting scheduled the first time around.

The app allows you to make your calendar public, so that you and your colleagues can find meeting times that work for all involved parties.

### 6. Errands add up, so eliminate them! 

If you had a clear conception of how much time you spend running errands each month, you'd be shocked. But come on, a dash to the corner store for toilet paper or dish soap? No big deal, right? Well, the fact is, these errands add up. If you want more productivity, you've got to kill those errands!

But how to get started?

One great way is by signing up for an online delivery service. Amazon's Subscribe & Save, for instance, delivers necessities like toilet paper, razors, shampoo and laundry detergent to your door at certain intervals.

Perhaps you go through two tubes of toothpaste every month; so arrange to have those two tubes delivered to you at the end of each month and never waste time buying toothpaste again.

Though toilet paper is an obvious necessity, there are plenty of other important items that you can have delivered. For instance, by arranging via Subscribe & Save for an order of nine-volt batteries to arrive every six months, the author ensures that his smoke detectors always work.

There are some errands that you can't automate, however. You know, those ones that catch you by surprise, forcing you to rush out and do them before it's too late. Well, thanks to outsourcing, you don't have to waste time on these, either.

TaskRabbit is a great example of an online outsourcing marketplace for those small, unforeseeable tasks — a quick run for groceries, for instance, or a sprint to get your computer or car fixed.

Here's how the author put this service to use:

His nephew, who lives in Los Angeles, wanted a slide for his second birthday. The author, who lives in New York, used TaskRabbit to contract a person in Los Angeles to purchase the slide at an IKEA in Long Beach, take it to his nephew's parents' house and assemble it. A truly unforgettable gift — all for $47.

### 7. Stay on top of your finances with the latest tools. 

In the first blink, we learned about the value of tracking things like your time and dietary habits. However, it's not always beneficial to spend loads of time tracking everything. Trying to track your finances, for example, often results in strain and stress.

Keeping tabs on what and where you spend is a challenge, especially when you have several bank accounts and credit cards, not to mention things like loans, mortgages and stocks and bonds.

So why not take advantage of some new tools and cut the time spent agonizing over your budget?

Mint.com is a great tool for organizing your finances and keeping track of your financial health.

Just provide Mint.com with your details and logins for bank accounts, loans and credit cards, and they'll let you know via email when a balance is dipping toward the red, or if there's been a big deposit or withdrawal from an account, as well as making sure that you're getting paid.

Mint.com can also provide a categorized and comprehensive overview of your financing that allows you to see how you spend your money and time. Mint.com can even use your previous spending patterns to suggest budgets for you!

Another innovative new tool to help you stop stressing and start saving is BillShrink. BillShrink simply asks for a few personal details: your checking account information, your driving habits, how often you use your cellphone and how much you spend on other services, such as movie streaming and magazine subscriptions.

Based on this information, Billshrink outlines cost-effective alternatives and outlines how much these alternatives would help you save over a two-year period; they also alert you to new deals and offers.

But don't be shocked if you find you could be saving more than you realized: when the author used BillShrink for the first time, he realized he could save around $14,000 in two years!

### 8. Give yourself limits and boost your quality of life. 

We've all been there: a desk seething with piles of paper; a floor only traversable via a narrow path through a chaos of _stuff_. And surrounded by all that stuff, you can never find exactly what you need when you need it.

But there's no need for this headache! You can overcome it simply by setting limits for yourself.

Start off by setting an _upper limit_. This will help you locate and keep track of your possessions.

Take the author's electronic gear stash, which once occupied an entire closet. Over time, though, the closet became overcrowded, and finding a specific cable meant dragging the whole mess out of the closet and onto the floor. The author realized things had gotten out of hand, and decided to take action.

So he bid his electronics goodbye and sold off most of his gear on Ebay. What was left fit into one box, which became the upper limit. If he buys a new piece of electronic gear, some other piece has got to go. This way, his stash is manageable, tidy and he can always find what he needs.

Just as you can use upper limits to control things that you might be overindulging in (like perusing Facebook, perhaps), you can also make sure you're doing more of the things that lift your quality of life, such as exercising, meditating and traveling.

How?

By setting _lower limits_. The author, for example, has set a lower limit of at least one trip every month, at least 30 miles of running per week and cooking meals at home at least three days a week. By setting lower limits for your leisure time in this way, you'll make sure your time doesn't always get gobbled up by work.

### 9. Batch your tasks to stay focused. 

Have you ever had the frustrating experience of _just_ getting into something — and then being interrupted by a distracting text or email? It ruins your focus and now you're irritated to boot.

Well, there's a way to keep your focus from being destroyed like this: _batching._

Batching is the simple process of grouping similar tasks together and completing them in batches, instead of one at a time, here and there. This way, it's easier to focus on what you're doing _right now._

Some people use batching by deciding that they'll only answer emails during the first 10 minutes of every hour, instead of replying as they arrive. The author pays all his bills for the week on Fridays, instead of paying them as they come in.

Of course, when you start batching, you learn exactly how much time certain things take up. Take note when things seem to be taking longer than they should, as this tells you that you might need to optimize.

A great place to start is by getting rid of unnecessary paperwork. Sure, there's a lot of talk about living in a paper-free society, but everyday we still find ourselves dealing with pesky papers. Which is why online tools are around: to help us eliminate paperwork even further.

If you ever have to send a document via snail mail, Postal Methods can help you out. All you have to do is upload your document. The company prints it for you, puts it in a stamped envelope and then sends it on its way in the mailbox. This saves you a lot of time, and who likes licking stamps, anyway?

### 10. Health is the ultimate productivity secret. 

It's as simple as this: getting enough shut-eye and eating healthy determines how much energy you will have to do the things you want to do. No amount of productivity apps in the world can change this!

When you deprive yourself of sleep, your body starts producing a rather unpleasantly named hormone called _ghrelin_. Ghrelin increases hunger and lowers levels of another hormone, _leptin,_ which regulates metabolism. When our metabolism doesn't function properly, we will inevitably get tired.

If you want to improve your sleep, there are two simple steps that will help. Firstly, take D-vitamins with your breakfast each day. Secondly, avoid devices emitting blue light, such as TVs, iPads and computers in the hour before you go to bed.

Starting to live more healthily is also simple. Just eliminate fats, right? Wrong. A lot of people still see fat as the enemy, but sugar is the real villain here. Our brains can't function _without_ fat, whereas too much sugar prevents us from absorbing vitamins A, D, E and K. So, no more wasting your time on processed foods and sweets! Start seeking out natural options and increasing your intake of the "good" fats found in olive oil and avocados.

Once you've made sure you're sleeping enough and eating well, it's time to start thinking about getting your body moving. Claiming that you're too stressed to exercise is simply no excuse! The truth is that exercise reduces stress, and you'll find that a good fitness program will allow you to cope better and better each day.

Design your own exercise regimen to include three types of movement.

First, there should be a strength and skill component — something like rock climbing or parkour will do the trick.

Second, include high-intensity interval training — a short interval of exerting yourself to the maximum, followed by an interval of active rest.

And, finally, you should engage in some kind of mobility training — something like yoga or just a good old-fashioned stretching session.

### 11. Final summary 

The key message in this book:

**Though life today seems to be stress-filled, there are ways to cut through the clutter and own your hours. With simple techniques and innovative new tools and services to guide you, you'll be saving time for the things you love in no time at all.**

Actionable advice:

**De-clutter your inbox!**

Use your e-mail service's filter to automatically put all e-mails that have the term "Unsubscribe" and don't have "FW" or "RE" in them in a folder called Optional. This will weed out a lot of non-essential and distracting e-mails from your inbox that you can look at later once you've got time.

**Suggested** **further** **reading:** ** _Scrum_** **by Jeff Sutherland**

Learn all about Scrum, the project management system that has revolutionized the technology industry. This is a uniquely adaptive, flexible system that allows teams to plan realistically and adjust their goals through constant feedback. By using Scrum, your team can improve their productivity (and the final result) without creating needless stress or working longer hours. To find these blinks, press "I'm done" at the bottom of the screen.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Ari Meisel

Ari Meisel is an author and entrepreneur. He advises businesses, leaders and others on how to add value to their endeavors and lives by employing effective time-management strategies.

