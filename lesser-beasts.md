---
id: 577fbc83c088090003fe9f03
slug: lesser-beasts-en
published_date: 2016-07-14T00:00:00.000+00:00
author: Mark Essig
title: Lesser Beasts
subtitle: A Snout-to-Tail History of the Humble Pig
main_color: CC5D47
text_color: A14A38
---

# Lesser Beasts

_A Snout-to-Tail History of the Humble Pig_

**Mark Essig**

_Lesser Beasts_ (2015) relates the long and fascinating history of the pig. Often considered an inferior creature, the pig is actually the sole animal that has stayed connected to us since the beginning of our existence. The pig's reputation has taken plenty of blows over the years — and today, due to modern farming practices, its welfare may be suffering more than ever.

---
### 1. What’s in it for me? Pig out on hog history. 

If you've read a recent food blog or visited a trendy restaurant, you'll have heard it before: everything is better with bacon. Pigs are popular in many parts of the world. For many, ham is a traditional feast dish; no barbecue or Balinese feast, for instance, would be complete without some part of a hog. Much of humanity is simply enamored of the pig. In other cultures, however, whether Islamic or Jewish, pork is a forbidden food. So, what is it that's so polarizing about the pig?

To find out, we need to take a look at a very long relationship. Throughout the ages, different people came to view the pig in different ways. Some cultures see it as essential; others have come to view it as dirty and unfit for human consumption. Let's go on a journey through the history of the pig and find out how this lesser beast has been with us since the birth of humanity.

In these blinks, you'll find out

  * how the early Greeks used pork as a way to assimilate and punish conquered subjects;

  * what vital function pigs played to keep cities clean; and

  * that the way we treat pigs today differs greatly from how we used to treat them.

### 2. Of all domesticated animals, pigs are the most similar to us and we share a long history. 

At first glance, pigs and humans don't seem to have a lot in common. But take a closer look and the similarities are hard to miss.

For starters, let's look at our similar digestive systems.

Pigs and humans are both omnivores, meaning we can both basically eat anything. We share a stomach that breaks down proteins, a small intestine that absorbs sugar and a colon that absorbs all the water. But the similarities don't end there.

Just consider our teeth.

In 1922, a fossil hunter dug up a 10-million-year-old tooth in Nebraska. It eventually found its way to Henry Fairfield Osborn, former director of New York's Natural History Museum. Upon analysis, Osborn concluded that it was a human tooth from the first human-like ape, which he dubbed the _Nebraska Man_. But Osborn was mistaken: the tooth actually came from an ancient and extinct pig-like creature.

And when we look back to ancient times, we find that, as humans started settling down in around 10,000 BC, they brought domesticated pigs along with them.

Humans and pigs have always had a strong relationship and settling into communities made it even stronger. At the sites of several ancient villages around the world, remains of both human and pig bones have been found.

At Hallan Cemi, a famous site in Turkey, researchers discovered that the bones all came from pigs less than one year old, suggesting that they were slaughtered for food.

But a closer look at early domesticated pigs suggests that they weren't just a source of meat but were also used to keep the villages clean by eating leftovers and garbage.

Evidence shows that early humans would normally leave a site when the garbage levels reached a critical mass. With pigs functioning as ambulant garbage-disposal units, however, settlements became more permanent.

As we'll see, though, pigs were both valued and avoided for the same reason.

> _"Pigs became domestic through their relationship not with humans as hunters but rather with humans as villagers."_

### 3. Many rich people started to avoid pork, for reasons of status and religion. 

Despite the fact that domesticated pigs both provided food and enabled us to settle down, they were never exactly a celebrated animal.

As Middle Eastern civilizations began to develop, they found that the climate wasn't ideal for raising pigs; meanwhile, the elite members of society began rejecting them.

Long ago, animals in this area had to travel great distances in dry, hot weather — conditions that didn't suit pigs, which need shade and a variety of food.

Meanwhile, the rich people in this society, such as bureaucrats and priests, were happy to dine on the beef and lamb that poor people couldn't afford. Despite the disdain of the rich, however, pigs didn't disappear; they did what they've always done — they scavenged.

The poor were happy about this. It made their meat cheap and gave them something to eat.

But, due to their all-inclusive diet, the pigs reputation continued to decline. An omnivore, the pig is happy to eat anything, including human corpses and feces, if these happen to be laying around.

This was a serious taboo in the civilizations of Egypt and Mesopotamia, and so people who ate pork were deemed impure.

Cows ate grass; pigs ate cow feces: it's easy enough to guess which animal got more respect.

If that wasn't enough for people to cut down on pork consumption, the Jewish people also introduced a formal ban on pigs in the Torah, which had far-reaching consequences.

Not only did Jews teach this law wherever they migrated, but it also influenced another religion that can be traced back to Abraham: Islam.

Consequently, there are 14 million Jews and 1.6 billion Muslims today that don't eat pork.

But regardless of this early bad reputation, the pig would see a revival among the Romans and Greeks.

### 4. The ancient Greeks and Romans gave pigs a second chance. 

You might have heard of the Greek father of medicine, Hippocrates. But did you know that he declared pork to be the finest meat of all?

Ancient Greece had its own culture and its own eating habits — habits and culture that Alexander the Great spread far and wide as he and his successors imposed their beliefs on the countries they conquered.

Just consider the aftermath of the invasion of Jerusalem by Alexander's successor, Antiochus IV. He demanded that all people act as equals, and forced the Jews to give up their custom of not eating pork.

These events are documented in the second book of the Maccabees, which describes how the Grecian invaders killed the Jewish scribe Eleazar after he refused to eat pork.

Following the Greeks were the Romans, who loved to eat, and to eat pork especially.

In fact, there are more Latin words for pork than for any other meat. For example, the s _uarii_ was a seller of live pigs; a _porcinarius_ sold fresh pork; a _confectorarius_ sold dried pork; and a ham seller was known as a _pernarius._

Food was not only important for the Roman elite; it was an important factor for keeping the general public happy.

Emperor Augustus supported the initiative to provide people with free grain and bread — one half of the Bread and Circuses policy designed to keep people content — and, eventually, in 270 AD, Emperor Aurelian added free pork to go along with the free bread. This led to the Romans building the best agricultural trade system the world had ever seen.

While Roman agriculture was originally a local practice, the growing number of hungry citizens needed more. So, they imported grain, olive oil and cured meats from places such as Egypt, Spain and Syria. During this period, 75 percent of Rome's food was imported.

The pig was an import in more recent history as well. Indeed, if Christopher Columbus hadn't brought pigs to the New World, we might never have settled there.

### 5. Without pigs, the discovery and settlement of the New World might not have been possible. 

The story of Christopher Columbus discovering America is quite famous. Less famous are the pigs that played an important role in his success.

Columbus, and other explorers working for Spain, brought all kinds of animals to the New World. None, however, was as prosperous as the pig.

Cows needed time to adjust to the new climate, even taking as long as a few generations before they fully settled in. But, in a way, pigs are like rats: they can live almost anywhere, and it took them no time to adapt to their new environment.

From the moment they landed on the new land, the pigs were ready to eat and breed. In fact, they multiplied so fast that settlers reported an endless supply of pigs.

A common tactic at this time was to drop off a few pigs on desolate offshore islands and let them breed. Explorers were generally told not to kill them. If there were enough pigs, though, the explorers were free to take what pigs they needed, as long as they left a breeding pair behind.

Thus, pigs contributed to the discoveries of the Spanish: they ensured explorers would have a steady food supply during their journeys.

But it wasn't just the Spanish who got help from the pig; English colonists relied on them as well.

Pigs were perfect for early colonists. Pigs, unlike most animals, thrived on their own, a perfect arrangement for settlers who didn't have many people to help them on the farm. These weren't just any old pigs, however, but the wily forest pigs that the Spanish had brought over from the Caribbean. And it proved to be a great fit in colonial America as well.

Free to roam in the New World, pigs were able to multiply in great numbers and provide settlers with sufficient meat during harsh times.

But pigs seem to be suited to most environments: when these colonies turned into cities, pigs proved just as helpful.

### 6. When cities began developing, pigs were a profitable solution for garbage disposal. 

So, we've seen how the indiscriminate eating habits of the pig both helped keep villages clean and earned pigs a bad reputation in some cultures. Well, near the beginning of the twentieth century, this characteristic would once again come in handy.

As cities began growing rapidly, pigs functioned as four-legged garbage disposals.

Garbage removal was a big problem for cities in the early 1900s, but, luckily, pigs are not only excellent at this job, but they're also cheap workers.

In 1920, the city of Worcester, Massachusetts, used 2000 pigs for getting rid of garbage. As an added bonus, they later sold the pork from these waste-disposal pigs, an arrangement that earned the city $59,000 in two years.

As time went on, many people discovered how profitable pigs could be.

Farmers figured out that feeding pigs corn worked wonders, because pigs are between two and three times more efficient than cows in converting corn to meat.

An important factor in this efficiency is the length of the pig's intestines: longer intestines mean more nutrients can be extracted before the feed leaves the body.

Wild hogs have an intestine-to-body-length ratio of 10:1, but more modern breeds of pig, like the Poland China or the Berkshire _,_ have a ratio of 18:1, which makes them much more effective at extracting nutrients.

While this was an important discovery for farmers, another key factor in modern pig farming was just around the corner.

> _"Hogs don't always carry the prestige of cattle, but you can't live on prestige."_

### 7. Giving antibiotics to pigs had two beneficial side effects: faster weight gain and protection against illnesses. 

Pigs can grow pretty quickly by simply eating garbage or corn. Nonetheless, for some farmers, this wasn't good enough.

A key discovery came in the mid-twentieth century when farmers discovered that, without altering its diet, they could make any pig gain more weight — by giving it antibiotics.

Originally, farmers and pharmaceutical companies believed that the weight gain was due to vitamin B12 supplements, but they quickly realized it was a by-product of the antibiotics contained within these supplements.

So, with antibiotics, pigs reached their slaughter weight, without requiring extra feed, more than two weeks earlier. This, of course, saved farmers tons of money.

Soon enough, without extensive testing, the US Food and Drug Administration approved antibiotics as a feed supplement. This led to 1.2 million pounds of antibiotics being consumed by livestock in the 1960s — a number that eventually rose to 25 million pounds per year.

Another benefit of antibiotics, naturally, was illness prevention.

This played an important role in hog farming as rising land prices caused farmers to stop sending pigs out to pasture. Pigs were instead crowded together in barns, the perfect environment for the spread of illness. Luckily, antibiotics were there to keep the pigs healthy.

Antibiotics, however, aren't all good news for farmers. Today, the continued use of antibiotics poses the risk of creating antibiotic-resistant superbugs that can spread to people.

Superbugs are harmful bacteria — such as the deadly bacterial pathogen Campylobacter — that bring on horrible symptoms like bloody diarrhea. These bacteria can't be killed with antibiotics.

Therefore, some countries, like Denmark, have put an end to the use of antibiotics, and it should be noted that their pork industry still thrives.

But antibiotics and overcrowded barns were only the beginning of modern hog farming. In the next blink, we'll take a closer look at the negative effects of today's practices.

### 8. Modern hog farming is bad for the environment and bad for the pigs. 

Many people still like pork products because they continue to be inexpensive. But this small cost comes at a big price.

To put it bluntly, pig farms are extremely bad for the environment.

Today's pig farms are vast, and so are the _manure lagoons_ that collect all their waste.

In 1995, 60 million US pigs produced the same amount of waste as 266 million people. And despite strict regulations on how to handle human feces, there are very few on how to manage pig waste.

This discrepancy has had dramatic consequences: in June of 1960, heavy rain in North Carolina made an eight-acre pond, filled with 25 million gallons of pig waste, overflow into neighboring soybean and tobacco fields. Millions of fish were later killed when it eventually flowed into the local river.

But the more common consequence of all this waste is that it gets into the groundwater and contaminates nearby wells and streams.

Furthermore, pigs naturally produce methane, ammonia and other gasses, and the overcrowded conditions of pigs on today's farms make living nearby impossible. Sometimes, the highly flammable methane can cause an explosion, like the one that leveled an Iowa farm in 2011, killing 1500 pigs.

But it's not only the environment that's being harmed; the pigs living in these conditions are unhappy.

Despite the hog industry's claims that these conditions are better and more humane than keeping pigs on the field, the opposite is actually true.

Pigs suffer from a list of ailments. They get respiratory tract infections due to poor air quality and they get frustrated being pent up, causing them to act out, bite other pigs or chew on metal bars until their mouths bleed.

To solve some of these problems, most pigs are now kept in cages where they can only stand up or lie down, unable to even turn around.

These shockingly terrible conditions and practices need to change, and, for that to happen, we need to let the farming industry know that consumers care.

### 9. Final summary 

The key message in this book:

**Our connection with pigs is deeper than most of us think. They've been with us since the dawn of civilization and they were there to help us discover the New World and clean up our cities. Even if their main job has always been to provide us with food, it's been a long, enduring relationship.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Project Animal Farm_** **by Sonia Faruqi**

_Project Animal Farm_ (2015) is all about one of the most harmful industries of our time: the modern mass production of meat, eggs and milk. It delves into the horrible conditions farm animals are kept in and the dangerous effects of factory farming on humans, animals and the environment.
---

### Mark Essig

Mark Essig has a PhD in history and is the author of _Edison and the Electric Chair_. He has also written for _The_ _New York Times,_ the _San Francisco Chronicle_ and the _Los Angeles Times_.

