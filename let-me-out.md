---
id: 597bcaa9b238e1000570353d
slug: let-me-out-en
published_date: 2017-08-01T00:00:00.000+00:00
author: Peter Himmelman
title: Let Me Out
subtitle: Unlock Your Creative Mind and Bring Your Ideas to Life
main_color: 39C3D4
text_color: 1D656E
---

# Let Me Out

_Unlock Your Creative Mind and Bring Your Ideas to Life_

**Peter Himmelman**

_Let Me Out_ (2016) offers techniques for overcoming the fears that delay or hamper new projects. The book advocates a series of useful methods designed to nurture creativity and boost the imagination. It's about opening the floodgates to new ideas and making them a reality.

---
### 1. What’s in it for me? Let go of your fear of failure. 

If you had no fear, what would you do? Would you start the novel that's been a half-scribbled idea in your journal for years? Would you take singing lessons? Would you start training for a marathon?

Many of us have dreams that we're afraid to say out loud or even admit to ourselves. Why? Because of our fear of failure, and the related fears of embarrassment and shame. These stifle our dreams and stall them as fantasy instead of transforming them into reality.

But what if you could do something about it?

Every creative has had to face these fears, and they exist only in the mind. The artists and achievers we see celebrated in the media are not necessarily more talented than us, but they were brave enough to confront their fears and learn to quiet them.

In these blinks, you'll learn

  * who Marv is, and why you have to learn to shut him up;

  * why rediscovering your inner child is great for unlocking creativity; and

  * how you might be similar to a baby elephant.

### 2. Got an idea? Confront your fears and challenge negative mind-sets with small concrete actions. 

Have you got an idea for a book, song, film script or business stashed away? Why are you sitting on it? Get moving!

The first step to realizing your dreams is to overcome fear.

It's normal to fear failure. Worry inevitably becomes the biggest obstacle when we really want to succeed. We start talking ourselves down. Maybe we tell ourselves that we aren't talented enough or that someone has beaten us to the punch, or that we're not qualified.

Repeating negative thoughts gets us nowhere. It leads to procrastination at best and self-sabotage at worst. Especially when that pessimism starts piling up.

Such negative thinking is almost always rooted in past experience.

Perhaps, for instance, you've never had a freelancer in your family. You might have internalized the belief that running a business is difficult. It needn't be. There's nothing stopping you from starting that dream travel agency if you really want to!

Equally, if you don't take that first step, negativity and fear can escalate. But the moment you get the ball rolling, even if it's just the smallest nudge, things won't seem so insurmountable.

A good way to unblock negative beliefs is by using a _Brain Bottle Opener_ (BBO for short). These are simple time-limited creative exercises designed to unlock ideas.

A BBO works something like this. In a 15-minute slot, you could scribble down possible options for your new venture's name. In ten minutes you could doodle some logo ideas. In a couple of minutes, you could buy a domain name.

By working to a fixed time and on a defined task, you can quickly snap out of sluggish and negative mind-sets. Every concrete action takes you that little bit closer to realizing your goal.

So that's the first step. Where do you go from here?

### 3. Understand your inner critic and learn to free yourself from doubt. 

It's easy for people who know no better to ask, "Why don't you just get started and do it?" But the truth is, getting going can be pretty hard work. Just why is that?

That little inner voice that fills our heads with doubt certainly plays a part. The author has a name for it: _Marv,_ short for "Majorly Afraid of Revealing Vulnerability."

Marv tells you that taking risks can lead just as much to failure as to success. It's a very primal defense mechanism: Marv likes to keep you out of harm's way if at all possible.

However, if you want to achieve anything, taking risks is part and parcel of the whole process. Therefore, it's critical to subdue Marv's bleating. Use three steps to make your dreams achievable.

First, make them _specific_. Break them down into smaller pieces. Want to run an animal shelter? Start by volunteering.

Second, make your goals _present_. Take action in the here and now. There's no point postponing.

Third, make them _true._ The dream has to be yours and yours alone. You have to find motivation from within.

To mitigate Marv, revisit these three principles as often as possible. If you take things step by step, you will conquer that inner critic.

Another good way to moderate Marv is to examine your behavior and think about what's held you back in the past. We all have our metaphorical _elephant ropes._ Think about it. Many tame elephants are kept shackled from a young age. Fully grown, they are strong enough to escape but don't because as babies they learned it was impossible.

Here's another BBO: Spend five minutes describing your own personal elephant rope, big or small. Maybe your family told you that you couldn't sing, so you stopped singing around people from a young age, even though you loved it. Repeat this five-minute task and understand what's holding you back.

### 4. Reach your goal by confronting fears of abandonment and building trust. 

It's a wonderful thing when you find your sense of purpose and live by it. So why is it so tricky to put that inner critic to rest?

Well, Marv's job is made easier by playing on feelings of abandonment. So it's critical to understand them if you want to confront him. He thinks he's a self-protection mechanism, but he's really not.

We have to step back and work out the source of those fears of abandonment. As children, we were fully dependent on our parents. If we'd been abandoned, we'd have died. As adults, we don't need that level of protection.

Confront the overprotective Marv by writing him a personal letter.

Take three minutes to explain to Marv that he should ease off. You understand his motives, but you're not actually in danger. You're moving on with life. Tell him to save his energy for when you're really in a fix.

Another way to ensure future endeavors succeed is to strengthen relationships. In fact, it's been shown that long-term relationships are far more rewarding than winning the lottery or becoming famous.

An American professor of psychology, Barbara L. Fredrickson, conducted research into the importance of human interconnectedness. She demonstrated that the feelings of positivity we get from relationships don't deplete over time, like most other things.

Consequently, be generous and kind. Let others know how much they mean to you.

Why not start today? Send a message to somebody you admire — a partner, parent, teacher or mentor. Write from the heart. Overcome your inhibitions and try to be genuine and complementary. There's no need to revise. Just send.

These exercises will increase confidence and trust. They'll help you feel more connected. They'll mean you make better choices as you won't be held back by negative emotions.

### 5. Let your inner child lead your creative process. 

Once you've got going, you'll be better equipped to face down residual fears. But doubts can still bubble up and slow you down. The trick is to embrace creative methods that keep you pushing forward.

Embracing your inner child is a sure way to galvanize your project.

Learn to utilize what's called _Kid-Thinking._ It's the powerful creative mind-set that cultivates curiosity and helps you overcome potential obstacles.

According to cognitive psychologist Anthony McCaffrey, creativity can be fostered by confronting what's called _functional fixedness._ That's the phenomenon by which we tend to observe the function of an object as fixed.

What does this mean in practice? To jumpstart Kid-Thinking and use it against functional fixedness, think of an everyday object and try assigning it a variety of non-fixed roles.

Say it's a cup. Maybe it could become a teddy bear's beanie, a paperweight or a cake tin.

Kid-Thinking emphasizes the importance of play, which is a process sure to stimulate our minds. The other benefit to Kid-Thinking is that it forces us to stay in the present moment and so remain positive in our search for creativity and solutions.

A Kid-Thinker, therefore, will keep pressing on and ignore the doubts that impede creative processes. In contrast, a _Stuck-Thinker_ can't look beyond temporary setbacks or negative thoughts.

Have a go at practicing this element of Kid-Thinking. Learn to detect the negative emotions that might otherwise hinder you.

Once you get used to seeing things a different way, you can really free your mind to see things from other points of view as well. So the next time an employee at the coffee shop gets your order wrong, maybe you can recognize it wasn't personal and there's no need to be angry.

Or maybe you go to see a movie and it's sold out. What then? Stay in the present. There's no need to get upset. Remain zen and don't get caught up in negative thoughts.

If you're able to practice keeping your composure and staying in the present, you'll find it easier to make conscious and confident decisions. You'll be able to focus your energy on positivity and use it for further creative endeavors.

### 6. To stay motivated, visualize your dream future and learn from past mistakes. 

You should now have a good idea of what you want to achieve. You know what creative tools to use and how to assuage any doubts. But there's always room for improvement when it comes to motivation strategies.

Try imagining how your life will be once you've achieved your dream. Positive thoughts will contribute to a positive outcome.

It's called a _FutureVision_. Lots of people who begin songs, books or businesses start this way. Begin with the broad brushstrokes of a vision. After that, you can fill in the details. And once the details are in order you can start taking the little steps to make that dream a reality.

Marv will certainly have little to complain about if you have a clear goal and process in mind.

Take five minutes and write your own FutureVision. Dream big. Where will you be in three years' time? Where in the world will you live? Who will stay closest to you? What project or job will make you happy? Be specific and get down to brass tacks. You might well be surprised how much of it comes to fruition.

Another method for improving motivation is understanding that making mistakes is just part of the process. We all make them; there's no shame in that. The trick is to learn from them.

Have a think about past events that have made you unhappy, and why they affected you. That way you'll be able to work out what your core values are and what keeps you motivated.

Say you're a jazz musician and you accepted a job in a circus band to make ends meet. You had to play music you didn't like, and you were morally opposed to their treatment of animals. By reflecting on exactly what made you unhappy, you won't compromise your values in the future.

Meanwhile, forgive yourself and let yourself move on.

### 7. Accept that change happens and learn to face the future with a clear mind. 

Change is part of life. It's scary. Sometimes it leads to good, sometimes to bad. It's essential to learn to embrace both sides of the coin. If you can deal with change, you'll be better equipped for the future.

Change is just part of the creative journey when you're pursuing a dream.

It's easy to cope with positive change, like the arrival of new clients or a new publisher. You might take the credit for it, even if it is just plain old luck.

The real difficulty is handling unwanted change that's completely out of your control. But even then, there are lessons to be learned, and that process can itself be liberating. Identifying your own role and responsibility is critical.

Find five minutes to describe a negative change you experienced. Jot down everyone you had originally held culpable for it. It takes courage to admit that you were in some way at fault. Think what you could have done differently, so you don't make similar errors in the future.

Perhaps you were fired. At first, it's easiest to blame your boss. But it doesn't take much reflection to realize you may have had a role too. Perhaps you were often late delivering tasks or didn't work well in a team. After a little consideration, you'll know where to improve next time.

Once you've gleaned positive lessons from a bad experience, dwelling on it does no good. It paralyzes you. Accept what happened. Once you've let blame, pain or anger drift away, you'll be in a better headspace for the adventure ahead.

### 8. Keep your dream alive by identifying support networks. 

Planning your first solo concert? Then you should call a mate who's played a few gigs and discuss his experiences.

A group of supportive friends is an invaluable asset if you want to fulfill your dreams.

If you're sharing your journey with understanding people, you'll stay passionate and inspired.

A group will support you. You needn't fear making mistakes as you'll have a safe environment where you can be genuine and test ideas.

It's about exchange and keeping the joy alive. Nobody will expect perfection at the first attempt. And when you hit a wall you can always pick up the phone and work through it to get those creative juices flowing again.

The Posse Foundation is a good example. It was founded in 1989 as a nation-wide American organization to help public school students into college. Through supportive, one-to-one mentoring, underprivileged students have been able to land scholarships and finish school.

Posse Foundation students have a 90-percent college graduation rate. In comparison, the graduation rate for non-Posse students from major American cities is sometimes as low as 10 percent.

So how do you create your own support network?

Grab a pen and for two minutes collect names of people you're certain will encourage your dream.

Don't wait; contact one of them immediately and organize a meeting in person, by Skype or phone. If you don't hear back, follow up with them not fewer than three times over the next three months.

What if you can't think of anyone? Well, you've got to find them. Sign up for local interest groups, workshops or initiatives that are sympathetic to your mission. Put their meeting dates in your diary and be sure to attend.

Once you've created a list of names, dates and resources, don't hide it away somewhere in a drawer. Display it prominently and use it!

### 9. Final summary 

The key message in this book:

**It's normal to be afraid of starting something new, but instead of letting worries run endlessly through your head, get to work. There are ways to unblock your creativity, gain courage and try new things. If you feel stuck, try different approaches in your life or businesses. Actions engender change, and every little step brings you closer to realizing your creative dreams.**

Actionable advice:

**Regularly disconnect from technology.**

Try not using modern technology for at least four hours this week. This technology detox will let you reconnect with your thoughts and those closest to you. You'll be surprised how much more focused you'll become. You'll be able to concentrate on creating the life you dream of having. And you might even feel your energy levels increase.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Making Ideas Happen_** **by Scott Belsky**

_Making Ideas Happen_ deals with the obstacles that lie between your ideas and their implementation. It offers insight into the ways in which successful individuals and creative departments overcome these obstacles, by offering real-life examples from some of the world's leading brands and creative minds.
---

### Peter Himmelman

Peter Himmelman is a successful Grammy- and Emmy-nominated musician, author and visual artist. He created the company Big Muse to share communication and leadership skills with organizations and individuals all over the world. He runs seminars on how to use creativity and unlock paths to new solutions.

