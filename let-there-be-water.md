---
id: 570957528b5d6e0003000002
slug: let-there-be-water-en
published_date: 2016-04-11T00:00:00.000+00:00
author: Seth M. Siegel
title: Let There Be Water
subtitle: Israel's Solution for a Water-Starved World
main_color: 8ABBE5
text_color: 3D5366
---

# Let There Be Water

_Israel's Solution for a Water-Starved World_

**Seth M. Siegel**

_Let There Be Water_ (2015) sheds light on Israel's solutions to water problems. Thanks to technological innovation, clever engineering and political foresight, Israel stands out as one country that can teach other nations how to provide water in abundance for its citizens.

---
### 1. What’s in it for me? Learn how Israel is leading the world in developing water conservation solutions. 

How can a land that is 60 percent desert have sufficient water to support a growing population and a booming agricultural sector? Israel's experience proves that necessity plus innovation equals success.

In these blinks, you'll see how a government that dedicates resources to a pressing problem can lead to smart environmental policies and a sustainable water supply even from a very dry land.

As we all head toward a warmer world with the potential for serious water shortages, the time is ripe to start dealing with this looming water crisis. These blinks will explain how Israeli expertise in water technology can help the world keep the taps running.

In these blinks, you'll also learn

  * how Israeli currency and stamps give pride of place to water;

  * how a small pipe leak revolutionized agricultural water use; and

  * why removing salt from seawater changed the face of Israel forever.

### 2. Water mindfulness and long-term planning set the foundations for water self-sufficiency. 

Desert covers 60 percent of Israel, a nation where annual rainfall is far from plentiful. And yet, Israel does not suffer water shortages. With enough surplus water to export to neighboring nations, Israel could teach other countries a thing or two about wise water usage.

It all starts with Israel's appreciation of the need to respect water. Droughts, a common occurrence when living in the desert, make it hard for any Israeli to take water for granted. On top of this, the mindful use of water is taught in schools, where students learn strategies to minimize water usage.

But water mindfulness in Israel goes even further back. Jewish prayers for rain at certain times of the year — said three times each day — have been recited for centuries.

Water has even featured on the country's currency and postage stamps. The five-shekel note celebrates Israel's National Water Carrier, the country's largest water project, while stamps commemorate ancient water systems.

Water mindfulness has been a crucial aspect of managing water shortages and provided the foundations for the nation's long-term plan for self-sufficiency in water usage. In the country's early days, Israel's greatest water resources were located in the far north, along the border with Lebanon and Syria.

However, this wasn't where there was most need for water. The heavily populated area of Tel Aviv, as well as the southern Negev desert, didn't have enough water to maintain a growing population or support profitable agriculture — a big problem.

So what did Israel do? Through the development of a massive infrastructure project, surplus water from the north of Israel was transported to the central and southern regions.

Israel's National Water Carrier project was completed in 1964 and represented a giant leap forward toward water self-sufficiency. But even more innovation was still to come.

> _"You can tell a lot about a country by the way it manages its water."_ –Shimon Tal, former head of the Israeli Water Commission

### 3. Israeli scientists came up with innovations such as drip irrigation and sewage water treatment. 

Jewish water engineer Simcha Blass visited a farm in the 1930s when he noticed something peculiar.

Examining a row of planted trees, he saw that one tree was much taller than the others. Blass discovered a small leak in an irrigation pipe near the base of the taller tree. He suspected that the small but steady drops of water had helped the tree to flourish.

And so the idea of _drip irrigation_ was born, which in time revolutionized agricultural water use in Israel and worldwide. Before Blass's discovery, flood irrigation was the accepted method of watering crops. With this, agriculture commanded more than 70 percent of Israel's overall water consumption.

Drip irrigation, in contrast, saves water while doubling crop output. This revolutionary technique allowed more food to be produced while making more water available for household use, raising living standards overall.

Israeli scientists also pioneered the treatment and reuse of sewage water. Sewage water is the water that goes down our sinks, showers, bathtubs and toilets. Israel used to dump its sewage without treating it, as many nations still do. But today, it reuses over 85 percent of its sewage water. How?

The country uses a process called sand aquifer treatment, or SAT. Israeli hydrologists discovered that fine sand was an incredibly effective filter for cleaning waste water. Using SAT systems, sewage can be used to supply a third of the water needed for agriculture.

Every year in Israel, more than one hundred billion gallons of water are saved using SAT systems.

### 4. Desalination took Israeli water solutions another step further to cement the country’s self-sufficiency. 

Since the state of Israel was founded in 1948, scientists have pondered the problem of how to make salty seawater potable. The large-scale desalination of Mediterranean seawater was seen as an ideal solution to persistent water shortages.

But was such a project feasible?

Chemist Alexander Yarchin was responsible for the first attempts to desalinate salt water in the late 1940s. He proposed spraying water into a vacuum, then freezing it, to push the salt out. But this method proved too expensive and impractical to be a useful solution.

It wasn't until 1966 that Israel made a breakthrough in desalination technology. Sidney Loeb, a Jewish-American chemical engineer working in Israel, developed a technique called _reverse osmosis_.

With reverse osmosis, water is pushed through a membrane that causes pure water to move one way while salt molecules move in the opposite direction. This technology was the practical desalination solution for which Israel had been waiting.

Desalination has certainly shaped the water profile of modern Israel. The country can withstand the driest weather conditions, conserve its groundwater sources as well as facilitate peaceful relations with Jordan and the Palestinian National Authority based on fair water distribution.

> _"The global water crisis is unlikely to be solved without the widespread use of desalinated water."_

### 5. Israeli water expertise has become a booming export industry. 

Whether it's desalination or drip irrigation, Israeli water science has excelled. In light of their success domestically, Israeli entrepreneurs are exploring global opportunities for water conservation.

The result of this has been the transformation of Israel's water expertise into a lucrative export industry.

Israeli company Bermad, for example, has created a device to cut off water flow when a precise amount has passed through a meter. This ensures only an allowed amount of water is used, with not a drop wasted. Bermad has sold its solution in 80 countries, and today employs some 600 people.

Israeli water-technology companies also share expertise with neighboring Arab countries, Jordan and the Palestinian National Authority. The water situation in Gaza, for example, is dire. More water is being drawn from the ground than rainfall can replenish. Groundwater, in turn, has become more saline, or salty and thus undrinkable.

Israel has stepped in to provide the region with clean, potable water, as well as training, technology and assistance to the Palestinian National Authority as they work toward new solutions.

Israel and the Palestinian National Authority have also teamed up with Jordan to develop an ambitious project to desalinate and redistribute Red Sea water among the three nations.

> _"There isn't a scarcity of water. There is a scarcity of innovation."_ –Amir Peleg, Israeli entrepreneur

### 6. Water plays a central role in Israel’s diplomatic relations with China and other nations. 

Surrounded by hostile Arab nations, Israel has been subject to intense diplomatic isolation. But through the sharing of water technology, the country has successfully formed new international bonds.

Israel's relationship with China is one good example. It wasn't until the 1990s that the Chinese government renewed diplomatic relations with Israel. Israel's affiliation with the United States and China's reliance on Arab oil prevented the two nations from forming an alliance.

China, however, is also struggling with water issues. In the mid-1980s, the Chinese government invited Israeli water engineers to assist with an irrigation plan for the Wuwei district, south of the Gobi desert.

So it started out with Israel having something China needed — water expertise. It wasn't long after this initial project that the two nations began to build an official diplomatic relationship.

Water plays a central role in Israeli diplomacy, with both developing and wealthy nations. From the late 1950s onward, Israel shared irrigation techniques with developing nations, including countries in Africa.

Today, over 100 African nations and countries around the world reap the benefits of Israeli training programs in water management and irrigation.

Israeli company Tahal specializes in water-technology solutions for the developing world, designing supply systems for urban areas and irrigation plans for agricultural land.

Even developed countries and regions have something to learn from Israeli technology. Los Angeles, a large city in southern California often plagued by drought, sought help from Israeli companies in addressing polluted groundwater sources.

### 7. Public ownership, real-cost pricing and innovation keep the taps running at full capacity in Israel. 

Having once lived in fear of shortages, Israel today enjoys both independence from climate conditions and confidence in its abundant reserves of water.

There are three key factors that got Israel to where it is today. What are they?

_Public ownership_ of water is a cornerstone of Israel's success. Since 1959, the Israel Water Law has stipulated that water be managed centrally by government agencies, rather than by private corporations. This law allowed the government to have an accurate picture of the resources available while planning for the country's water needs.

_Real-cost pricing of water_ is the second key aspect of Israeli water policy. In other nations, subsidies that prevent people from paying the real cost of the water they need and use are the norm.

This is not the case in Israel. Those who use water are expected to cover the full costs, without a penny in government subsidies.

Why? Well, real pricing, unsubsidized and essentially more expensive, encourages consumers to use the water they need, but not waste — a simple but effective conservation strategy.

Finally, Israel is a major supporter of _innovation in water technology_. The government has acted as a catalyst in many new water ventures by offering financial incentives.

On top of this, a nationwide enthusiasm for new ideas has inspired over 200 water-innovation start-ups to launch in the past decade. Municipal water utilities also receive financial incentives from the government for testing and implementing new technologies.

> _"Israel's water system may be the most successful example of socialism in practice anywhere in the world today."_

### 8. Final summary 

The key message in this book:

**With global water problems looming, Israeli water solutions can serve as a model for the world. With technological innovation, political foresight and powerful public mindfulness, Israel went from struggling with scarcity to water abundance. Today, Israel shares its expertise with developing and developed countries around the world.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Price of Thirst_** **by Karen Piper**

In _The Price of Thirst_ (2014), author Karen Piper reveals how private water companies have not only failed to offer universal access to clean water but also contributed to environmental degradation and political conflict amid a quest for profit.
---

### Seth M. Siegel

Seth M. Siegel is an entrepreneur, lawyer and writer. His work discussing water and environmental policy has appeared in the _New York Times_, the _Wall Street Journal_ and the _Los Angeles Times_. He's also an in-demand public speaker and gives talks on a range of issues including politics in the Middle East, water policy and national security.

