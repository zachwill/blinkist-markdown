---
id: 5b01ada8b238e100074e378a
slug: letters-from-a-stoic-en
published_date: 2018-05-23T00:00:00.000+00:00
author: Seneca
title: Letters from a Stoic
subtitle: None
main_color: 2B98D9
text_color: 1E6996
---

# Letters from a Stoic

_None_

**Seneca**

Written around 65 CE and addressed to a Roman official stationed in Sicily by the name of Lucilius, Seneca's _Letters from a Stoic_ are an exploration of the good life. Drawing on the rich tradition of stoic philosophical thought, Seneca advocates simple living in harmony with nature, avoidance of temptations and vice and the continuous honing of the mind through the study of philosophy. That, Seneca argued, was the path to true happiness.

---
### 1. What’s in it for me? Learn about the good life from a true Roman statesman. 

It's often said that the past is a foreign country — but sometimes it's as familiar as can be. Written almost 2,000 years ago, _Letters from a Stoic_ remain as vital and insightful today as when they were first composed. In fact, the book bears a striking resemblance to that most modern of literary genres, the self-help book!

Seneca's concern is to help his readers through the maze of perennial problems that confront us in all times and places. What is the good life? And how do we achieve it when we're beset by frailty, subject to unpredictable fates and face the inevitability of death?

Seneca recommends balanced and simple living in harmony with nature, and his letters set out a series of actionable insights that can help us onto the right path of reflection, inner peace and the avoidance of temptation.

In these blinks, you'll find out

  * why philosophy is the key to wisdom;

  * the importance of choosing your friends wisely; and

  * how to remain calm in the face of disaster.

### 2. True wisdom is knowing how to live a simple life in accordance with nature. 

Imagine wandering through a forest beneath a high canopy of towering trees blotting out every last ray of light, or climbing into a jagged cave that no human hand could have carved. The feeling of awe we experience in moments like these reflects our appreciation of the work of the divine.

But divinity isn't just something out there in the world — it's also something that's within every one of us. The divine resides in our souls. It's part of us, but it isn't truly _ours_. Like the rays of sun that touch the earth but belong to a star in the sky, our souls belong to divinity itself.

Because they're not ours, we can't work on our souls or take credit when they're praised. But there is something we _can_ work on: the mind.

Our minds are unique. They differentiate us from one another. They also set us apart from the animal world, since what defines man is that he can think rationally.

Rational thought is a product of the mind. That means we can, and should, devote ourselves to honing and perfecting it. The more effort we make to perfect our minds, the more unique we become. Unlike the trappings of everyday life like houses, furniture and artwork, which are simply backgrounds to our lives, the mind is a true one-off. That's why it deserves our attention and labor.

The fruit of working on our minds is _wisdom_, and attaining wisdom is the primary aim of our lives.

So what does it mean to be wise? Wisdom is knowing how to live as nature intended us to and constantly striving to stay true to that intention.

As the author sees it, Nature's plan for us is to live simply, to abstain from worldly pleasures and the desires that drive us to accumulate vast fortunes, gorge ourselves on food and desperately seek fame and fortune. Wisdom allows us to see that. Living in accordance with nature also means confronting the fears that emerge when we look upon the natural cycle of life, in which loss and death are inevitable. 

But that doesn't mean we need to lock ourselves away in a library and devour as many books as we can lay our hands on. All it takes is a little work every day. After all, the royal road to wisdom is study.

The payoff of that effort is insight. We achieve wisdom and come to see that living in harmony with nature is the only truly rational decision.

But that begs the question: what exactly is it that we're supposed to be studying? In the following blinks, we'll take a look at the topics you'll need to study to attain wisdom.

> "_Without wisdom the mind is sick. . ."_

### 3. Philosophy is the key that unlocks wisdom and happiness. 

What is philosophy? Simply put, it's the study of truth in the world that surrounds us and the world of the divine; in other words, philosophy is the key to the good life and virtuous living.

But wait, how do you get from truth to good living?

Philosophy is the light that illuminates the truth. And once we can see clearly, we can make better decisions about how we should live.

Take the fear of death. When we think philosophically, we see that it's just as much part of the natural order of things as life itself. Why should we fear something that belongs to nature? Or think of the desires that animate our attempts to accumulate riches or seek out the pleasures of intoxicants like wine. Philosophy shows us that both drunkenness and wealth are fleeting moments.

So, once we look at the world rationally, we begin to attune ourselves to nature. In turn, this provides us with true happiness.

You can see why Seneca thought that was the most important subject of all!

Compare philosophy to technology, an area in which so much of human ingenuity is on display. Engineers and architects help us build beautiful dwellings, which marble quarriers can later adorn with stunning floors.

But what does all that really add up to? Sure, you'll have a nice enough banquet hall in which to wine and dine your guests. But do you truly _need_ that?

After all, we know of tribes who find shelter in simple pits dug directly into the ground. They don't seem unhappy that they're not sleeping in rooms outfitted with finest marble from Carrara; they appreciate what they have, however little that might seem when you compare it to a palace.

Just as we don't need luxury to find shelter, there are many other things we don't need to be happy. This is another lesson that philosophy teaches us.

So how does philosophy stack up against other subjects?

Music might help us harmonize notes and chords, but does it help us achieve inner harmony with ourselves?

Or consider geometry. It lets us measure everything from the distance between two stars to the size of our estates. But it offers no insight into why a man who has lost every acre of his estate can still smile in the face of the world. If you want to measure a soul, you'll need to turn to philosophy.

So that's the first subject you'll need if you want to become wise.

Philosophy teaches us what nature intended for us. It helps us learn how to face our fears and achieve a sense of balance while doing away with pointless distractions and focusing on true happiness.

> "_For men in a state of freedom had thatch for their shelter, while slavery dwells beneath marble and gold."_

### 4. Don’t just study any old way, do it properly! 

Okay, you've decided to study philosophy to find out how nature intended us to live. How do you go about it?

Surely the great philosophers of Greek antiquity can help. Why not dip into the writings of Zeno, the first stoic, or Aristotle or Plato?

But there's a catch: studying has to be taken seriously. You can't just skim the surface, you have to plumb the depths.

Casually perusing lots of different authors will only give you a superficial understanding of their ideas. Think of it like dashing around the world and never staying very long in any one place. You'll return home with hundreds of fleeting impressions and a ton of new acquaintances — but very few true friends.

That's why the best course of action is to take an established and trusted author and stick to him. It's in the deeper end of the pool that you'll really start learning how to swim!

Sticking to an author means studying his work in detail, but it doesn't mean ignoring everyone else. No single philosopher, no matter how great, ever divined the whole truth. To get at truth, you need to understand philosophy as a whole.

Think of a beautiful woman. She might have wonderful ankles or wrists, but it'd be strange to say that she was beautiful _because_ she had beautiful wrists, right? Her beauty resides in the sum of her many parts. You can't reduce the quality of beauty to any one feature.

But you don't just need to read widely and deeply; you also need to read actively.

Simply repeating what Plato or Zeno have to say on a given topic might be a good way for children to learn, but it's not enough for adults. Mature learning is active — you gain your own knowledge. Great philosophers can help you with this, but in the end, you'll have to take leave of them as well, as you forge your own path.

This means comparing their insights and drawing your own conclusions. Once you start doing that, you're already contributing to philosophical knowledge about the world.

Chances are you won't be the next Socrates — but remember, there's no monopoly on truth. In fact, there's plenty for all of us!

### 5. Your mind is your most valuable possession; it’s your home and the guarantor of peace and health. 

Some itches just can't be scratched. You can travel the whole world and see countless new cities, scenes and people, but still wind up feeling restless.

That's because travel can be a form of escapism. What you neglect is what really matters: developing your mind.

If you take care of your mind, you'll be at home anywhere in the world.

Imagine a heavily laden ship. As long as the cargo is properly secured, it can sail the stormiest of seas with confidence. But if the goods are poorly fastened, they'll start slipping and sliding around the hold and, soon enough, end up taking the whole ship down.

A healthy mind is a bit like the well-stowed ship. Everything is fastened in its proper place. It can travel anywhere and, no matter how rough or serene the ocean, the view from the bridge will always be a pleasure.

But if your mental cargo is badly stored, maintaining an even keel is next to impossible. You'll have an unbalanced mind and be beset by restlessness.

So, you should focus on securing your mental freight. After all, peace of mind comes from inside — you can't find it in the outside world.

Think of a spacious house in the countryside, far from the hustle and bustle of the city. You buy it in the hope of creating an oasis of calm and you even go as far as establishing rules that stipulate that everyone inside the house has to tiptoe around at night to preserve the absolute quiet.

Yet, even after all that, you still can't sleep. You lie awake at night and begin imagining strange noises. No matter how peaceful your surroundings are, you're still plagued by a restless mind. 

What do you do?

True serenity is achieved by looking inwards and quieting your mind. Philosophy, which teaches us how to live in accordance with nature, is the handmaiden of tranquility.

A properly working mind is a wondrous thing. It can even heal the body.

Health is a necessary but insufficient condition of the good life. Exercise is important and useful for that reason, but only when it doesn't become an obsession. A heavy-duty weightlifting routine might get you a washboard stomach and guns of steel, but it can also be a distraction. All those hours working out will amount to time you could have spent cultivating your mind.

Just think what happens if illness strikes. No amount of muscle will support you or restore your health. A sound mind, by contrast, can make all the difference. It can provide the mental strength you need to get through trying times and ultimately help you recover.

### 6. Steer clear of temptation and vice by staying focused on the truth. 

Vice and temptation are everywhere. Think of the Romans: they enjoyed nothing more than seeing men being thrown to the lions, fighting bears and being put to the sword at their local gladiator shows. It was all part of a normal day.

There are countless vices, and some are commonplace. Who hasn't regretted acting a fool after drinking too much? Another vice is idleness. How on earth are you supposed to figure out how to live properly if you get out of bed as the sun is setting?

In the end, there are pretty much as many vices as there are people. Some squander fortunes, others keep mistresses and still others are slaves to their desire for fame and fortune.

So if vice is everywhere, how do you steer clear of it? By living in harmony with nature.

Vice is the fruit of a wish to be better than others. It thrives among those who spurn a normal, simple life. Wanting to be different is what drives people to spend their time and money on fripperies like stylish clothes, fancy carriages and extravagant banquets.

Vice is also a cunning master of disguise. Those who _think_ they have the least work to do are usually those who have the most to do.

Think of dreams. When we're sound asleep and unconscious to ourselves, we rarely notice our dreams. Living well is similar. When we're busy pursuing petty pleasures and reacting to fleeting pains, we are unconscious to ourselves and to what we really need. That's why those of us who have the most work to do on ourselves are usually unaware of what we really ought to be doing.

### 7. Master your fears and anticipate the worst. 

What's your deepest fear? Losing your status, comfort and everything you own? Whatever it is, you'll need to confront it if you want to achieve wisdom.

A good place to start is by becoming comfortable with the idea of poverty. Take a few days every now and again to live as the poorest do. Wear drab, simple clothes and eat stale bread or barley porridge.

Once you've learned to take pleasure from a few paltry scraps of bread, you'll begin to realize that there's nothing to be afraid of when it comes to poverty. This, in turn, will help you cultivate a sound mind.

Because nothing lasts forever, it's a good idea to anticipate other disasters, too. The world is full of examples of sudden falls from grace, unexpected hardships and bad luck. Great empires crumble and honest men who've led righteous lives are cast into exile by vengeful masters. Loved relatives and friends die and whole cities are consumed by fire — that's just the way it is.

The world is a play of contrasts. Night follows day and clouds obscure clear skies. There's an uncanny calm before the most ferocious of storms, and the worst day you can imagine can easily come on the heels of the best you've ever had.

Fairness and unfairness don't come into play. Every living being and thing is subject to the same unpredictable workings of fate.

Consider soldiers. Even in times of peace, they spend their days participating in drills to prepare for the worst-case scenario — war. That's how you should think about the world. Prepare for the direst outcomes, even when things are at their best. Anticipate the eventuality that everything you have might be taken from you.

This way, when fate appears to become vindictive, you'll be prepared and come through it all with a smile on your face. Most importantly of all, you'll have a clear and calm mind.

> _"In the midst of pleasures there are found the springs of suffering."_

### 8. Choose your friends wisely. 

Some people seek to unburden themselves by sharing their troubles with everyone they come across. Others don't even tell their closest friends what's on their minds. Neither of these options is in accordance with nature, as any wise Stoic will have learned.

So what should you do? Two things. Choosing your friends wisely is vital. But once they're in, you should _really_ let them in.

Your best bet when it comes to choosing friends is to base your decision on trust. Befriend those who you're sure you can rely on. Even better is a trustworthy fellow traveler — someone on the same path of discovery as you.

The best friends we make don't just reflect who we already are, they also change us for the better. Friendship is about learning and improving yourself.

That's why it's important to avoid people mired in vice. Adulterers, cheats and torturers corrupt those who are close to them. Make an unwise choice, and chances are you'll soon pick up the vices of your new friends.

If you're pretty certain you've made the right decision, share everything with your friends. Open the door and let them into your life. Best of all, assume that they'll be loyal to you — that's often the best way to make sure that they are loyal!

True friendship is built on a solid foundation, and becoming friends with someone for the right reasons is its cornerstone. After all, a wise man is self-sufficient and has learned to be happy by himself. That means he doesn't _need_ friends. When he does enter into a friendship, he's not out for himself and isn't looking for a personal advantage.

And that makes him a good friend. After all, fair-weather friends aren't really friends at all. No sooner has he got what he wanted, and your so-called friend is heading for the hills.

What the wise appreciate is that friendship is valuable in itself. Creating and sustaining friendships is part of the good life.

> _"After friendship is formed you must trust, but before that you must judge."_

### 9. Face your fear of death. 

Think of eating strawberries. They're at their succulent, sweet best at the end of summer, just before fall, right? Old age is like that too. It's not all about illness, frailty and decline — in fact, aging has its own unique pleasures.

But to appreciate them you have to come to terms with your fear of death.

Like the strawberries that mark the impending turn of the seasons, each period in a life is part of the inescapable cycle of nature. And life itself is hardly a great achievement. Everyone and everything, from slaves to masters, insects to animals, has life.

The key to developing a healthy attitude is to celebrate what you've already had — the years you've lived — and to be grateful for everything that might be given to you tomorrow.

Pacuvius, the governor of Roman Syria, had a memorable way of doing this. Every night, he celebrated his own death by throwing a dinner party with fine wines and food. At the end of the evening, he was carried to his bed to chants of "He has lived, he has lived" in Greek.

While the ostentatiousness of Pacuvius's ceremony might be off-putting, we can learn a lot from his attitude toward death. Once we have conquered our fear, we can begin celebrating what we've already had of life, and each new day becomes a special blessing.

But what about death? It can't be avoided and comes for us all. That means the best we can make of it is to die honorably.

Clinging onto life merely for the sake of living is pointless. What's the most you can hope for? To eat another oyster or drink another glass of wine, things you've had countless times before? Or to carry on fulfilling your duties? We often forget that death is also a duty we have to fulfill.

The best end to an honorable life is an honorable death. But the author, like many of his contemporaries, also believed that suicide was more praiseworthy than continuing a dishonorable life.

Tullius Marcellius put this idea into practice. Struck down with a debilitating illness, he decided to kill himself rather than live with his condition. After dividing his possessions amongst his slaves, he fasted for three days before dying peacefully in a hot bath. He was much admired for his honorable, relaxed and contented death.

> _"As it is with a play, so it is with life — what matters is not how long the acting lasts, but how good it is."_

### 10. Final summary 

The key message in this book:

**The good life is a life lived in accordance with nature. That means leading a simple and honorable life free of vice and temptation — but that's harder to achieve than it sounds. Even so, studying philosophy can help us in this pursuit. By illuminating the truth, it shows us the path through life's many challenges and allows us to face danger, setbacks and the inevitability of death, with serenity. Once we have internalized the lessons philosophy offers, our minds can become oases of calm in a tempestuous world.**

Actionable advice:

**Keep your mind in good shape, just as you would with your body.**

We all know the importance of exercising and eating plenty of vegetables and cutting down on intoxicants like caffeine and alcohol. But health isn't just a matter of hitting the gym and maintaining a healthy diet — it's also about training your mind and cultivating a healthy attitude. So don't forget to complement your fitness regimen with some mental gymnastics.

Studying and learning don't just keep your mind agile, they also open the door to the great truths of the world and mentally prepare you to cope in trying times. Once you've developed the right attitude, you'll be able to face anything!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _On the Shortness of Life_** **by Seneca**

_On the Shortness of Life_ (49 AD) is an essay on how to appreciate life — and how to use it. These blinks show you what is truly valuable in this world and how to avoid getting distracted by unimportant matters. They'll show you where genuine happiness comes from and why working hard will not lead to a tranquil and satisfied mind.
---

### Seneca

The Roman stoic Seneca (ca. 4 BCE — 65 CE) was a philosopher, dramatist and statesman known for his many essays and letters on philosophical topics, as well as a number of dramatic tragedies. Seneca was sentenced to death by his own hand after he was accused of participating in a plot to assassinate the Roman emperor Nero, the most well known of his former pupils.

