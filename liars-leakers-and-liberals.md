---
id: 5b98da39862c430008dbba7e
slug: liars-leakers-and-liberals-en
published_date: 2018-09-13T00:00:00.000+00:00
author: Judge Jeanine Pirro
title: Liars, Leakers, and Liberals
subtitle: The Case Against the Anti-Trump Conspiracy
main_color: 2473B2
text_color: 2473B2
---

# Liars, Leakers, and Liberals

_The Case Against the Anti-Trump Conspiracy_

**Judge Jeanine Pirro**

_Liars, Leakers, and Liberals_ (2018) examines the dark forces that have not accepted Donald Trump's victory. From the dishonest media to Hollywood hypocrites, weak-willed republicans to the "deep state" of the intelligence community, these forces are now attempting to destroy America's president — a patriotic, family-oriented winner whose no-nonsense approach is exactly what the United States needs.

---
### 1. What’s in it for me? Get the story the liberal media won’t tell about Trump, his character and his presidency. 

If you rely entirely on the fake-news mainstream media, you may well think that the Trump presidency is a disaster and that the man himself is an idiot. According to the author, however, this is a far cry from the truth. President Trump is fighting, day in, day out, to make America great again.

And he's done this while fighting a liberal conspiracy against him. The author argues that liberals, as well as the political, law-enforcement and intelligence establishment, are working against Trump. They seek nothing less than the destruction of his presidency, and care nothing for the votes, not to mention the feelings, of millions of decent, ordinary Americans who support Trump with pride.

In the blinks that follow, you'll learn how the lying liberal media consistently and unfairly criticizes Trump. You'll see how, according to the author, the deep state of the intelligence and law-enforcement community is now pursuing a relentless witch hunt against Trump and his administration. And you'll see the fundamental decency and determination of the forty-fifth president of the United States.

In these blinks, you'll discover

  * that Hollywood stars, though silent in the face of Obama's atrocities, constantly call out Trump;

  * how Trump has stood up to liberals who want to protect criminal illegal immigrants; and

  * why it's Hillary Clinton, not Donald Trump, who should really be investigated for collusion with Russia.

### 2. The media today is unashamed to lie in an attempt to damage Donald Trump’s presidency. 

Incredible as it may seem today, there was a time when the media loved Donald Trump.

Back in the 1980s and 1990s, you could barely stroll past a newsstand without seeing Trump's face smiling back at you. The media couldn't get enough of Trump, from his business deals to his charitable donations. Coverage of Trump wasn't always positive. The media never held back, for instance, when Trump's casinos had problems or when his wealth fell in the 1990s. But it was always fair, running well-sourced stories and including Trump's version of events.

How things have changed. Today, the liberal news media hates Trump so passionately that, according to the author, it lies about him relentlessly.

Consider the winners of Trump's Fake News Awards, which are awarded to the liars who oppose him. ABC's Brian Ross was one winner. Ross ran a story alleging that Michael Flynn, Trump's national security advisor, was preparing to testify against the president; he claimed that Trump had ordered him to contact Russia before the election. This, according to the author, was a lie. The story was based on poor sources, and completely false — more akin to tabloid gossip than to investigative journalism.

Ross is not a stupid man. But, according to the author, he, like so many others, allowed his blind, liberal hatred of Trump to cloud his judgment.

If you don't believe the media has an anti-Trump bias, look at the statistics. A Pew Research Center study found that of all stories about Trump, 62 percent were negative. This compares with only 20 percent about Obama, and 28 percent about George W. Bush.

In the author's view, the situation has been exacerbated by the realization, by station chiefs and newspaper bosses, that anti-Trump lies _sell_.

During the election campaign, the _New York Times_ hit an online readership of 1 million for the first time. In the first month of Trump's time in office, the _Times_ signed up 132,000 new subscribers. Two months after the election, the _Washington Post_ recruited 60 new newsroom staff. Magazines like the _New Yorker_ and the _Atlantic_ set new subscription records. These news organizations even have a term for this — the "Trump Bump." They know that anti-Trump lies sell more papers. And so, the author claims, they keep on lying.

> _"We know what the liberal media think of Trump voters: They're deplorables, idiots, rednecks."_

### 3. Hollywood is stuffed full of lying, hypocritical liberals who think they are better than the rest of us. 

The journalists and executives of the liberal media aren't the only ones who despise Trump and everything he stands for. The smug millionaires of the Hollywood set can't stop criticizing Trump, either.

But shouldn't Hollywood take a break from liberal indignation for just one minute, and consider how blinded and hypocritical it has become?

TV personalities like Sarah Silverman and Chelsea Handler have said that, since Trump's election, they are giving up showbiz to focus on activism full time. Now, there's nothing wrong with activism. But it should be _fair_. And, according to the author, Hollywood reserves its outrage for Trump alone.

The celebrities who railed against the injustices of George W. Bush's foreign policy, and who decry Trump today, happily fundraised for Barack Obama. Where was their outrage when Obama ordered drone strikes that killed American citizens? Where was the outrage when Obama sent $221 million to the Palestinian Liberation Organization, a group that is happy to look the other way as women are murdered in so-called honor killings?

No, Hollywood turns a blind eye to the outrages of a Democratic president.

Maybe, says the author, that's because Hollywood and the Democratic establishment are closely intertwined.

This was seen with the Harvey Weinstein scandal. Weinstein, a moviemaking tycoon and a major Democratic donor, was accused of rampant sexual assault, and several rapes. Isn't it odd that it took so long for the Democratic elite to speak out against Weinstein?

Barack Obama received $700,000 in donations from Weinstein, and took days to condemn the filmmaker after allegations were aired. Weinstein donated hundreds of thousands of dollars to the Clinton foundation. Hillary Clinton hid for five days, before she could bring herself to say she was shocked.

Perhaps we shouldn't be surprised. Decades earlier, when women accused Bill Clinton of abuse, harassment and even rape, we didn't hear much outrage from liberals, the author points out. Hillary Clinton threatened the victims, while the Hollywood elite not only kept silent on the allegations but continued hosting fundraisers for the Clintons.

Hollywood liberals think they're better than the rest of us, smarter than the rest of us. But you know what? Movie theater attendances are dropping through the floor. Hollywood is dying. So maybe they aren't so smart, after all.

### 4. Many Republicans are getting in the way of Trump’s agenda, rather than standing squarely behind it. 

At least in Hollywood, people don't pretend to be anything but liberal. Unfortunately, says the author, the same can't be said for the Republicans in Washington. In 2016, the Democrats lost the Oval Office, the Senate and the House, but sometimes you wouldn't know it because, the author contends, Washington is full of RINOs — Republicans in name only.

Trump couldn't have been clearer about his policy promise to build a wall on the United States' southern border with Mexico. And in 2017, Republican Speaker of the House Paul Ryan and Republican Senate Majority Leader Mitch McConnell promised to allocate over $12 billion to fund construction. So far, so good.

But when push came to shove, the amount the two Republican leaders allocated for the wall was just $1.6 billion. To make it even worse, the budget bill that announced the money actually prohibited this tiny sum from being spent on the type of wall Trump was considering. Ryan and McConnell should be ashamed of their failure to get behind Trump's agenda, says the author.

And the border wall isn't the only example. When Trump moved fast to meet his campaign promise of a travel ban, to stop the flow of radical Islamists entering the United States, where did McConnell stand? Certainly not with Trump. Instead, he lined up with the liberals opposing the ban, saying he hoped the courts would rule on whether it went too far. McConnell did nothing when Obama tightened visa requirements for the _exact_ _same_ seven countries a year before. He only started his sanctimonious pleading about religious liberty when it was Trump taking action.

According to the author, here's the reality: too many Republicans on the Hill embody everything that Trump opposes. They're creatures of the swamp, parasites who came to Washington as modest men but leave as millionaires. Rather than working hard to pass legislation to advance Trump's agenda — the Senate has only passed five of the four hundred bills passed to it by the House. Instead, they spend their time wining and dining in fancy restaurants, doing nothing for the hard working Americans who elected them.

To the author's mind, the real divide in Washington isn't between Republicans and Democrats. It's between Trump and the swamp — the lying, self-serving politicians who care more about their personal interests than those of America.

### 5. President Trump tells the truth about illegal immigrants while liberals are busy protecting them. 

Liberals claim that President Trump hates immigrants. But that's just plain wrong, says the author. Donald Trump is married to an immigrant. And the immigrants in his employ at Trump Tower say what a great boss he is.

What Trump does understand is that the American people have legitimate frustrations about illegal immigration.

When Donald Trump announced his candidacy in 2015, he didn't shy away from the question of illegal immigrants. In a tough but, according to the author, balanced statement, he said, "They're bringing drugs, they're bringing crime; they're rapists. And some, I assume, are good people." The liberal media were not just outraged; they thought that this meant the end for Trump, before his presidential run had even begun.

Well, guess what? Not everyone in America is as liberal as the editorial boards of our "great" newspapers. When the rest of the country heard Trump's speech, they thought, says the author, "Well, it's good that someone is finally saying what we are thinking." This speech was the start of a movement that led Trump to the White House.

There is no clearer indication of the divide between Trump and establishment liberals, according to the author, than sanctuary cities. There are over 300 sanctuary cities in the country today, in which government personnel and law-enforcers are prevented from sharing information on illegal immigrants. So even when an illegal immigrant commits a crime, they might not be turned over to Immigration and Customs Enforcement (ICE.)

In 2015, Kathryn Katie Steinle, an attractive 32-year-old woman with blonde hair and a wide smile, was walking by the San Francisco waterfront with her dad. She was happy. She'd just met a young lawyer, whom she hoped to marry. She had her whole life ahead of her. But in that moment it was taken from her by José Ines Garcia Zarate, who shot Katie Steinle to death.

Garcia Zarate is an illegal immigrant with multiple felony convictions. He'd recently completed a prison sentence, and ICE had requested he be detained upon release, so they could collect him for deportation. But the city sheriff, Ross Mirkarimi, ignored this. If he hadn't, Zarate would have been on his way back to Mexico, and Steinle would still be alive.

But Mirkarimi simply let Zarate walk free because San Francisco is a sanctuary city.

It wasn't much of a sanctuary for Steinle, was it?

As President Trump has said, the priority for Democrats is to protect criminals, not to do what's right for America. The contrast with his own agenda, says the author, is clear.

### 6. The deep state spied on Trump’s campaign and has tried to sabotage his presidency with the Russia investigation. 

Trump is determined to put America, and Americans, first. But opposing him is the powerful _deep state_ : long-term government employees of the law-enforcement, intelligence and defense communities, supported by lobbyists and corporations, as well as the fake-news media and the rest of the globalist elite.

According to the author, the deep state has tried to sabotage Trump from the start, leading to the investigation — or "witch hunt," as she calls it — led by Special Counsel Robert Mueller.

It all started with the Christopher Steele dossier, a collection of gossip, says the author, masquerading as intelligence on Trump and Trump's links to Russia. The dossier, she argues, was so outlandish that no major media outlet apart from CNN or Buzzfeed would publish it. It was produced by the British deep-state former spy Christopher Steele, a man who, according to the author, spent his career getting as friendly as possible with Kremlin officials.

But this dubious dossier was the crucial evidence that the FBI used to secure wiretaps to spy on the Trump campaign. When the FBI applied in 2016 to the secret Foreign Intelligence Surveillance Court for a wiretap on Carter Page, a Trump campaign official, it submitted the dossier as evidence. But, outrageously, it withheld the fact that the dossier was paid for by the Hillary Clinton campaign and the Democratic National Committee. That was important information the author believes the court should have had when determining the dossier's credibility — or total lack thereof. The FBI, she says, deliberately withheld it, because it was determined to pursue Trump.

Just think about that. The FBI obtained a warrant from a secret court, to spy on a private US citizen, based on gossip put together by a foreign agent that was funded by the Democrats and Hillary Clinton.

But the situation gets worse. The author's own sources have told her that the Trump campaign wasn't just subject to electronic surveillance; there was also an FBI informant inside the campaign. The presence of such a person would have required the approval of the attorney general. So, the author says, either Obama's attorney general, Loretta Lynch, was directly involved in this conspiracy, or the FBI went rogue, spying on a perfectly legitimate presidential candidate. Either way, it would constitute an abuse of power the likes of which we have never seen before.

> _"The anti-Trump movement is a conspiracy by the powerful and connected to overturn the will of the American people."_

### 7. The witch hunt against Trump continues, though it’s turned up nothing. 

Hundreds of years ago, people used to hunt and burn witches. The witches were, of course, usually innocent, simply the victims of score settling, or attempts to eliminate competitors. But the fact that their crimes were imaginary didn't help them. Once the idea that someone was a witch took hold in the mind of the mob, it would get its blood, regardless of the victim's innocence. Sound familiar?

Let's take a look at the reality behind what the author calls Mueller's "witch hunt."

The hunt, she says, has made a fall guy of a military hero, Michael Flynn. When he served in the military, Flynn put his life on the line for all Americans. Now he finds himself a convicted felon. Why? Flynn faced what the author calls wild accusations of treason and an allegation that he held inappropriate conversations with the Russian Ambassador Sergey Kislyak, on behalf of Trump, before Trump's inauguration.

But Flynn didn't get prosecuted for that because it didn't happen! Instead, he was prosecuted

because he initially lied to the FBI about the conversations. But, according to the author, it's not even clear that he did lie. Flynn is a lifelong government employee and probably can't afford crazy attorney fees for a long trial. It's possible his legal representation simply advised him to plead guilty to lying, simply to end the process.

Just like Michael Flynn, Trump's former campaign chairman, Paul Manafort, has been investigated by Mueller, but for reasons _unrelated_ to collusion with Russia on behalf of the Trump campaign, says the author. Manafort is instead under investigation for allegations related to his consulting work for foreign governments, which the author claims has nothing to do with Trump at all. The judge in Manafort's case has noted this openly, saying to the prosecutor, "You don't really care about Mr. Manafort's bank fraud. You really care about what information Mr. Manafort can give you that would reflect on Mr. Trump or lead to his prosecution or impeachment or whatever."

The Mueller investigation interviewed all the key players from the Trump campaign in 2017. It has the investigative power of the federal government behind it. And still, today, it has found nothing because, says the author, there is nothing to find.

### 8. The story of the Uranium One deal shows the messed-up priorities of the FBI. 

Maybe you still think that the FBI is unbiased and simply pursuing the truth. Ask yourself, then, why did it ignore the story of "crooked" Hillary, the Clinton Foundation and the Uranium One deal?

Obama and the Clintons, according to the author, sold the United States' uranium, and the country's security along with it. Here are the facts to back up this claim.

In 2010, a Russian state-owned company, ARMZ Uranium, bought a 51-percent share of a Canadian company, Uranium One. That matters because Uranium One has the rights to mine 20 percent of the United States' uranium. In order for ARMZ to secure the deal, it needed approval from the Committee on Foreign Investment in the United States. One of Hillary Clinton's representatives, who, at the time, was serving as secretary of state, sat on the committee and approved the deal, as did others from the Obama administration.

Now, here's the catch. While the Russians were acquiring Uranium One, the chairman of the business made no fewer than four donations to the Clinton Foundation, amounting to $2.35 million, which Secretary Clinton _did not_ disclose to Obama. And right after ARMZ announced its investment, a Russian bank with Kremlin links paid Bill Clinton $500,000 to give a single speech in Moscow.

Sound underhanded? It's all the more suspicious, says the author, because the FBI knew, before the deal even happened, that the Russians were bribing and extorting to expand Vladimir Putin's control of US uranium. US defense attorney Victoria Toensing told the author that a secret informer in Russia spoke to the US government of hearing Russians brag that the sale was possible because of their connections to the Clintons.

Now, based on how hard the FBI and Department of Justice are investigating alleged links between Trump and Russia, you might think they'd have been all over this. But, from what the author knows, when the informer tried to reveal this information publicly, the Obama administration threatened to prosecute him for breaching a confidentiality agreement.

So ask yourself — if there isn't enough evidence to investigate Hillary Clinton for collusion with Russia, how can you justify the witch hunt against Trump? The only conclusion, according to the author, is that the FBI and the DOJ will oppose everything Trump does and stands for.

### 9. Look past the lying, liberal propaganda and you’ll see that there is no better man to be president than Donald Trump. 

To hear the "lying" liberal media tell it, you'd think that Donald Trump is a woman-hating, insensitive and hateful man who cares only for himself. Nothing, according to the author, could be further from the truth.

In fact, she says, Trump is a caring family man.

The author has been a personal friend of Trump for many years. She recalls one incident, flying back to New York from Mar-a-Lago on Trump's private jet. That weekend, Trump had been subject to a particularly nasty attack in the _New York Times_ magazine. The author watched as Trump read it. He didn't flinch, didn't shout or even sigh in frustration. In fact, after a minute, he'd forgotten all about it. He called out to the kids. Write down a list of all the things you need for school, he said. We're going to Kmart later to stock up! That's the kind of man he is — a down-to-earth family guy who cares more about his kids than personal attacks on him in the media.

She also knows Trump as a sensitive and respectful man. The president's current chief of staff, General John Kelly, told the author about a Memorial Day visit with Trump to Arlington National Cemetery, in 2017. That day, the cemetery was filled with families of fallen heroes. Many of them, often holding photos of their lost brother, sister or friend, approached Trump to talk to him. They simply wanted to share their memories, and Trump listened to them all.

One of Kelly's duties is to inform Trump whenever a service member dies in the field. When he does this, he says, Trump falls quiet. He frequently mutters the same question: Why do they do this? Now, hateful liberals will twist that quote in a bad way. But stop and think. Trump is so in awe of the courage of someone who dies for his country that he finds it hard to understand.

The media, according to the author, won't stop in its attempts to blacken the character of Trump and his family. But the truth, she says, is that Trump is a decent man, a man who cares deeply for his family, and lives and breathes doing right by America, and by Americans.

### 10. President Trump is making America great again, boosting the economy and achieving foreign-policy miracles. 

Ask yourself a question. When you read about the Trump administration in the papers or watch CNN or NBC, how often do you hear about the Mueller investigation, or the latest "lies" about Trump? And how often do you hear serious, detailed analysis of the economic accomplishments Trump has delivered?

The media fixates on lies and gossip, but, says the author, in the real world, the economy is doing great. Since President Trump's election, three million jobs have been created. Today, she says, there are more available jobs than people unemployed, and unemployment is the lowest it's been for seventeen years.

What's more, she says, despite the best efforts of the Federal Reserve, which has raised interest rates since Trump's election, the stock market has soared to new heights under Trump, rising 25 percent.

Not only have Trump's economic policies created wealth; they have also helped out ordinary working Americans. Take his much criticized tax cuts. Despite all the "lying" talk of them being "cuts for the rich," the tax-reform package will, according to the author, save a typical American family — two parents and two kids — over $2,000 a year.

Now, Trump is an extremely accomplished businessman, so the author isn't surprised that he has been an economic success. But he has also achieved what the author calls "miracles" in the area of foreign policy.

Do you even remember ISIS? We barely hear about the terrorist group nowadays, says the author, because Trump has achieved in months what Obama couldn't do in years. Working with allies and lifting pointless restrictions on the US military, ISIS has been decimated. As Trump himself has said, almost 100 percent of the terrorist group's territory has been retaken since he took office. How often do you hear about that on CNN?

Or compare US policy on North Korea under Obama with that under Trump. It was under Obama that the American student Otto Warmbier was held hostage and tortured beyond recovery, according to the author. Warmbier was finally released under Trump, though unfortunately it was too late for him, and he passed away. Three other American hostages have been released by North Korea under President Trump's high-pressure campaign. He's also started peace talks with North Korea and suspended their weapons testing.

The media likes to say that Trump is reckless, dangerous. But ask yourself, who has really achieved results? According to the author, it's Trump, every time.

### 11. Final summary 

The key message in these blinks:

**Picture a United States with jobs for everyone who wants one. Where businesses can grow and families live in safe communities. Imagine a strong and secure military, and a country that trades fairly across the world. This, according to the author, is the United States that Donald Trump is working for. But, she says, it is under threat from the deep state, which is supported by liberals, the lying media and Republicans too weak to make America great again. A great deal is at stake. Donald Trump's enemies, says the author, must be stopped.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Making of Donald Trump_** **by David Cay Johnston**

_The Making of Donald Trump_ (2016) examines the man behind the highly polished public figure presented to the media — and now the voting public — of America. His thousands of court cases and shady business dealings give a clear picture of the deception and dishonesty that Donald Trump would rather keep out of public view. Now more than ever before, it's crucial that people know whom they're dealing with.
---

### Judge Jeanine Pirro

Judge Jeanine Pirro is a prosecutor turned TV personality. She hosts the Fox News show _Justice with Judge Jeanine_. In 1993, she was elected district attorney of Westchester County, New York, becoming the first woman to hold the position.

