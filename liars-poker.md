---
id: 52fa3d196662610008070000
slug: liars-poker-en
published_date: 2014-02-12T09:14:41.000+00:00
author: Michael Lewis
title: Liar's Poker
subtitle: Rising Through the Wreckage on Wall Street
main_color: None
text_color: None
---

# Liar's Poker

_Rising Through the Wreckage on Wall Street_

**Michael Lewis**

_Liar's_ _Poker_ tells the story of Salomon Brothers, a leader in the bond market in the 1980s. This tell-all account of the author's experiences at Salomon Brothers explains how the firm became one of the most profitable investment banks on Wall Street through its role in establishing the mortgage bond market, and what it did once it reached the top.

---
### 1. A series of government regulations in the 1970s made firms in the bond market more profitable. 

When a government or company wants to raise money by borrowing, they can issue _bonds._

In return for receiving a loan, the government or company gives the lender a promise to pay interest on the loan for a fixed period. This promise — rather like an I.O.U. note — is the bond, and it can be traded; whoever owns the bond receives the interest.

Before the late 1970s, bonds were not prestigious or profitable products on Wall Street. _Stocks_, which give holders actual shares in companies, were much more profitable and sought after.

However, changes in government regulation began to make bonds a more profitable area of business.

The first of these changes happened in 1975, when the US government made regulations that reduced the profitability of stocks.

Before this change, stock trading was a highly lucrative business. Those who sold stocks could make huge amounts in fees or commissions from doing very little work, so naturally they concentrated their efforts on trading stocks rather than bonds. The regulation changed the way that these fees were determined, and the amount that one could charge for trading stocks massively declined.

Although this regulation lessened the amount of money you could make from stocks, they were still more profitable than bonds. But four years later, in 1979, another change in government regulation brought bonds closer to stocks in profitability.

In this year, the chairman of the US Federal Reserve, Paul Volcker, allowed interest rates — which had previously been fixed — to fluctuate. Bond prices have a strong relationship to interest rates. When interest rates go down, bond prices go up. This regulation, therefore, provided possibilities for bond prices to rise rapidly. This made them attractive as financial investments.

### 2. The Wall Street firm Salomon Brothers saw an opportunity in the much-neglected mortgage market. 

Prior to the late 1970s, the driving force in the US financial sector, Wall Street, had absolutely no interest in trading or issuing mortgages.

This was partly because mortgages were too small to bother with. It was also because the loans had no fixed term; borrowers could pay them back at any time. This limited the potential amount of money that could be earned by investors through interest.

However, in the late 1970s and 1980s, changes in the housing market were to transform Wall Street's approach to mortgages forever.

During this period, the US economy grew, and many people became richer. Single-family homes, second homes and retirement properties were in high demand from this affluent population. There was a huge spike in the demand for housing, overwhelming the traditional mortgage providers — small savings and loan banks.

In this changing environment, Salomon Brothers — a relatively small Wall Street firm — began to see the potential of trading mortgages.

Salomon Brothers began trading bonds issued by the publicly owned Government National Mortgage Association (Ginnie Mae), which was created to guarantee mortgages for less attractive low-income buyers. Ginnie Mae raised the money for this by pooling mortgages to create bonds that could be sold to investors. Although a single mortgage was too small to garner investor interest, several mortgages grouped together could form a viable financial product. Whoever held the bonds would collect the interest paid on the mortgages.

Most firms ignored these mortgage bonds, but not Salomon Brothers. They saw an opportunity in this new mortgage market and decided to dedicate an entire department to trading the new product. In partnership with Bank of America, they created the first private mortgage bonds.

### 3. To take advantage of the new mortgage market, Salomon Brothers created a small but strong mortgage department. 

By 1978, Salomon Brothers' new mortgage department was up and running.

To lead it, they appointed a talented trader named Lewis Ranieri. Ranieri was a go-getter who had worked his way up from the bottom of the company. He had first worked in the mailroom and then in the back office before ending up on the trading floor. In the years following, he was to become an instrumental force in the mortgage department's development and success.

First, Ranieri started by assembling a _trading_ _desk_, which was responsible for acquiring bonds and setting their prices and for creating a strong sales force to sell them to customers.

He plucked many of the hires from the back office, the department in charge of running the administration at Salomon. As bond trading grew prestigious, many new hires at Salomon were educated at top business schools, but these first traders closely resembled Ranieri; they were not formally educated and had tough, dynamic personalities.

The department was very much the underdog of the firm, as mortgages were a new and underdeveloped market at that point. Its staff were used to getting things done on their own without much support from the firm and so created unique behaviors. They often had to put extra effort into trade and sales and therefore developed a brash, domineering attitude.

Secondly, Ranieri put together a very strong legal team. This was vital because mortgage bonds, like those developed with Bank of America, were illegal in many states. The job of the legal team was to ensure that this situation changed.

In this role, they were incredibly successful. They managed to successfully lobby the federal government for legal changes that would make it possible to trade mortgage bonds in every state. By 1979, they had successfully created a nationwide market for their product.

### 4. Salomon's mortgage department was catapulted into profitability by federal intervention and Ranieri's leadership skills. 

Entering the 1980s, the recently deregulated bond market was growing and the mortgage department established by Salomon Brothers was up and running. In the beginning, however, the department did not produce desirable results. Investors were still wary of mortgage bonds due to the risk of prepayment, and there was also few banks willing to follow Bank of America's lead of pooling their mortgages into bonds.

But this situation was soon to change when two elements helped Salomon Brothers' mortgage department become very profitable. The first was a couple of important government actions, and the second was the dynamic and highly innovative leadership of Lewis Ranieri.

First, in 1981, US Congress allowed struggling mortgage providers to get rid of their unprofitable mortgages on favorable terms. Salomon Brothers were very happy to buy up these mortgages for rock-bottom prices and turn them into bonds.

Then the Federal Reserve let interest rates drop. Since low interest rates make bonds more valuable, the mortgage bonds were suddenly worth more than they had been when Salomon bought them.

Although Salomon was then left with a healthy supply of valuable bonds, investors were still skeptical about this relatively new product. Lewis Ranieri recognized that the success of his mortgage department rested on persuading investors that the bonds were potentially highly profitable.

He first got the government to guarantee the mortgages against default, which made them secure investments. Then he convinced investors that the risk of prepayment meant higher returns, as riskier investments tend to have bigger payoffs. This, combined with the exciting ups and downs of the bond market, helped dress them up and make them more attractive.

This exceptional combination of outside forces working in the mortgage department's favor and Ranieri's take-charge attitude caused revenues in the department, and thus the firm, to soar.

### 5. The Salomon Brothers' training program strongly resembled a high school classroom. 

American high schools tend to be places of social hierarchy and subcultures. Prestige and status are often awarded to the "cool" kids, who assert themselves over their peers.

The same was true in Salomon Brothers' training program, which was set up to transition graduates into successful trading and sales positions within the firm.

Although this program comprised some of America's best graduates, it often quickly degenerated into something like a school classroom — for example, the "good kids" sat in the front row and the "bad kids" threw balls of paper at them from the back of the room.

These "back row" trainees were particularly juvenile. They had little respect for authority and often heckled managers who came in to give lectures. However, Salomon Brothers didn't discourage this, as they wanted to foster a "survival of the fittest" mentality within the company. If the strong prevailed and the weak got left behind, the firm would be left with aggressive traders who would push for strong profits.

As a result of this mentality, the "back row" trainees often got prominent jobs within the company.

Even after the program ended, the similarity to high school continued due to Salomon's hiring process.

The managing directors of the company got to choose which trainees they wanted. This led to a situation rather like sports-team selections at school. The "best" trainees were the most popular and were keenly fought over by the different departments, while the weaker ones were left until the end.

To be regarded as the "best," trainees could take one of two directions to gain popularity: they could try to get on the good side of specific managing directors to win the positions they wanted, or they could display incredible levels of confidence to show that they were assertive and strong candidates, much like high school students using boisterous behavior to appear popular to others.

### 6. The Salomon Brothers’ mortgage department often behaved like a university fraternity house. 

In the same way that the training program was like a high school classroom, the mortgage department was like an American university fraternity house. It had a less than professional attitude and was characterized by childlike pranks and the bullying of employees.

This behavior came to the mortgage department partly because of the department's unique origins and partly because of its enormous success.

The department had struggled to establish itself before 1981. It started off largely as an experiment to see if mortgage bonds were a profitable product. Therefore management didn't interfere much in its operations. Lewis Ranieri was allowed to create the department in his own image, which meant plucking traders from the back office and choosing training-program graduates with diverse backgrounds, as he felt they had more in common with him.

This meant the department took on many of Ranieri's characteristics, becoming loud, boisterous and aggressive. The "back row" mentality of the training program flourished.

As the experiment worked and the mortgage department became highly successful and profitable, its traders began to feel that they were better than employees in other departments. They didn't really feel that they worked for Salomon Brothers — they felt they worked for the mortgage department.

All of these factors combined to make the department a stereotypical fraternity and, like a fraternity, its members began to partake in some very juvenile antics.

It was not uncommon for traders to play pranks on each other. For example, one trader who went on a business trip discovered his suitcase had been filled with wet toilet paper. Ranieri himself came in to work one day to find his desk completely covered with stacks of paper.

Members of the mortgage department also heckled and harassed employees from other departments, which greatly affected the mood within the trading floor.

### 7. Tactics such as bluffing and taking risks made Salmon Brothers traders resemble gamblers. 

In their spare time, traders at Salomon Brothers gambled their wages in a game called _Liar's_ _Poker_.

The game involved making statements about the serial numbers on dollar bills. It went like this: The first player would make a statement based on all of the serial numbers in the game; for example, he might say, "The serial numbers contain three sixes." His opponents' aim was to determine whether he was bluffing about the number of sixes on his own bill. If they believed he was, then they could challenge his statement. If he was lying, they won; if he wasn't, they lost.

Traders often gambled away hundreds or even thousands of dollars playing Liar's Poker.

The game had a lot in common with trading, as this also involved making bets, taking risks and bluffing.

To succeed in the bond market in the 1980s, traders had to be prepared to make substantial _bets_. The bond market was often volatile, as the prices of bonds fluctuated wildly. The goal of traders was to bet on bonds that could be profitable, acquire them cheaply, and then sell them when prices went up.

Making these kinds of bets was not without _risk_. There was always a chance that bond prices would drop or crash before they could be sold again.

To avoid this, Salomon's traders often _bluffed_. As customers were relatively clueless about the new mortgage market, Salomon could exploit them by lying about the true value or risk of bonds. Traders had no scruples about passing on the risk to customers by getting rid of bonds before the price dropped. They just wanted to increase revenues.

In fact, some traders even bluffed their fellow employees. Experienced traders would often lie about the potential of bonds to less experienced salesmen in order to get rid of the bonds, keep their own revenues high and preserve their bonuses.

### 8. Salomon Brothers compromised customer relations and balanced books in its pursuit of money. 

During the 1980s, money reigned as king of Salomon Brothers' culture, and the company's management, traders and salesmen did whatever they could to bring in more of it. Revenues were pursued above all else, even if they came with significant costs such as angry customers or books left unbalanced.

The way the company dealt with its own customers reflected its single-minded pursuit of revenue.

Salomon's interest in making money didn't always match the customers' best interests. In order to make the most from each transaction, the firm sold bonds to customers at inflated prices. Because customers knew very little about mortgage bonds, Salomon was able to exploit their ignorance.

The company also exploited customers by offloading unwanted bonds to them. When Salomon thought that the price of a bond was about to crash — perhaps because of a spike in interest rates or a rumor in the financial community — they would try to sell that poisonous bond to unsuspecting customers. This was common practice and was known within the firm as _blowing_ _up_ _the_ _customer_.

Salomon Brothers' pursuit of revenue also caused them to neglect their own costs.

Management chased higher revenues at any cost, which meant that it wasn't too concerned with its _bottom_ _line_ or the money it cost to operate the business. The belief was that as long as revenues were high, the firm must be profitable. Little thought was given to how much those revenues cost the company.

Salomon brought in unprecedented revenues during the early 1980s, and therefore neither its treatment of customers nor its ignorance of its own costs were problematic. However, that was soon to change, with disastrous results for the company.

### 9. Salomon Brothers' dominant position was eroded when many talented traders and salesmen were lured away to other firms. 

By the mid-1980s, Salomon had the strongest and most dominant mortgage department on Wall Street. It also invested a lot of time and money in its training program, thereby turning trainees into talented traders. This didn't go unnoticed on Wall Street.

Not wanting to be left behind, other firms started to poach Salomon's top talent, who were easily lured away by attractive compensation packages.

This was partly connected to the conditions at Salomon itself. Although Salomon revered traders who brought in a lot of money, its compensation didn't usually reflect the revenues they brought in. Salary was based on only experience and time, while performance was reflected in limited year-end bonuses. In addition, salaries were similar across the board, and the firm treated employees in all departments the same way.

Talented mortgage traders resented this because they brought in more money than traders from other departments.

So when other Wall Street banks came headhunting for the best of Salomon's staff, many employees simply found these deals too good to pass up. Particularly talented traders like Howie Rubin — a high performer who was lured away by Merrill Lynch for a million-dollar-per-year salary — weren't prepared to turn down seven-figure paychecks.

These defections had some major consequences.

Firstly, the employees who left often took their customers with them, leaving Salomon with less business.

Secondly, they helped break down Salomon's "poker face." This is because they let their customers in on the real value of bonds in order to lure them away from Salomon with lower prices. Because of this, Salomon lost its unique position of being able to bluff its customers, and since it relied on leaving its customers in the dark about the true value of mortgage bonds, these revelations ate away at their margins.

### 10. Salomon Brothers' ruthless pursuit of success helped to create the conditions for its downfall. 

Sometimes, great success can cause oversights that lead to great weakness.

This was definitely the case for Salomon Brothers by the mid-1980s. Many of the policies and practices that had initially brought in high revenues later ushered in its downfall.

One of these major weaknesses came when the mortgage department achieved what it had always wanted: a standardized bond product that effectively solved the problem of prepayment.

This product, called the _Collateralized_ _Mortgage_ _Obligation_ (CMO), grouped mortgage bonds together and then sliced them up into _tranches,_ which were ordered into shorter and longer terms. This gave each tranche a definite _maturity_, or a period over which an investment is repaid. When early payments came in, the first tranches were paid off first, and so on. CMOs made investors happy because even if the mortgage was paid off early, they still got a fixed period of interest.

This innovation made mortgage bonds less risky, and it was a huge success. CMOs brought the firm $60 billion in revenue between 1983 and 1988.

However, more buyers made mortgage bonds more expensive because prices naturally increase with demand, and this cut into Salomon's profit margins. It also meant that the company could no longer control the price of mortgage bonds, as it was becoming increasingly common for other firms to trade these bonds. In short, Wall Street and not Salomon dictated bond prices — meaning Salomon weren't able to bluff clients anymore.

These shrinking margins were compounded by the firm's poor customer relations in earlier years, which now came back to haunt it.

Many customers lost faith in the company and began to turn to other firms. They simply didn't want to work with Salomon anymore as they found their business practices dishonest. Those who had lost money through bad bond deals blamed the firm for dumping the bad bonds on their laps, and they no longer wanted anything to do with it.

### 11. Salomon's top management played a significant role in the company's decline. 

Although Salomon attracted many highly talented employees, its upper management was less than savvy when it came to running the business. It was often clueless about new trends in the bond industry, and about the inner workings of the firm itself.

This was partly because top traders often got promoted to management positions, but these employees did not always make good managers. They were often competitive and ruthless and had no idea how to run a company or manage people.

Ranieri himself was a very ambitious and aggressive trader and was, as head of the mortgage department, good at raising investor confidence — therefore he was eventually promoted to a top position just below the CEO. But taken away from the trading floor and placed in a management role, he wasn't able to remain in touch with current trends in the market, meaning it was usually up to younger and less experienced traders to keep up to date.

Upper management often exhibited poor decision-making skills, which increased the strain. For example, it opposed getting into the junk bond market, which ended up being wildly profitable.

On top of that, they made outright poor decisions. For example, they spent massive amounts of money expanding internationally without doing much research on international markets. They didn't try to understand the different ways in which people in other countries did business, and failed to adapt Salomon Brother's business model to foreign markets. This meant that these expansions were largely failures.

Eventually, the fact that the firm pushed so hard for revenue without accounting for costs took its toll: margins in the mortgage department started to drop. The firm as a whole had high costs, and when the mortgage department's revenue began to fall, these costs became a major problem.

### 12. Final Summary 

The main message of this book:

**The bond market became a huge star on Wall Street during the 1980s. One of the biggest players in this market was Salomon Brothers, which gained its prominence by almost single-handedly creating mortgage bonds. However, Salomon was the creator of its own demise, and its single-minded pursuit of revenue and inability to keep up with the market brought on its downturn.**

Actionable ideas from this book in blinks

**Make sure your firm covers its own costs.**

Salomon Brothers brought in huge revenues, but it also had massive overhead costs. When the mortgage department became less profitable, this spelled disaster for the firm.

Just because you're making money doesn't mean you're earning a profit. Revenue is the overall income of your company, but profit is the difference between revenues and costs. If your costs are higher than your revenues, you will have a loss instead of a profit. If your costs are high, manage them before it becomes a problem.

**Customer relationships are just as important as making money.**

Salomon Brothers pursued revenue over everything else, and it wasn't afraid to let its customers take a hit if it meant money for the firm. This eroded investor confidence, and many customers left to do business with other firms.

Reputation is a large asset. No business is too big to compromise its reputation. If your customers are unhappy, they will leave and give their business to your rivals. Make sure that everyone profits from your business dealings. If you're the only one making money, you could run into trouble later. 

**Don't confuse luck for good business sense.**

Luck played a large role in the success of Salomon Brothers' mortgage department. However, the firm failed to recognize this and continued to manage the firm poorly.

When forces such as regulation and a booming economy work in your favour, by all means reap the benefits. But don't forget that all good things can come to an end, and plan ahead accordingly. Make sure that your management team has a vision, is well organized, and has ideas for how to advance in the future.
---

### Michael Lewis

Michael Lewis started his career as a bond salesman at the London branch of Salomon Brothers but later left investment banking to pursue a career as a financial journalist. He published his first book, _Liar's_ _Poker_ , in 1989. Lewis has since written many bestselling titles, including _Moneyball_ , _The_ _Blind_ _Side_ and _Boomerang_.

