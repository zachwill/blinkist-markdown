---
id: 564052163866330007000000
slug: life-at-the-speed-of-light-en
published_date: 2015-11-09T00:00:00.000+00:00
author: J. Craig Venter
title: Life at the Speed of Light
subtitle: From the Double Helix to the Dawn of Digital Life
main_color: 64B442
text_color: 41752B
---

# Life at the Speed of Light

_From the Double Helix to the Dawn of Digital Life_

**J. Craig Venter**

_Life at the Speed of Light_ (2013) chronicles the pioneering work of the author and his team in creating the world's first synthetic life form. You'll experience the thrill of discovery as you follow the team's groundbreaking work in synthesizing the world's first genome and exploring the teleportation of living organisms.

---
### 1. What’s in it for me? Get ready for a world of artificially created life forms. 

Does the idea of a "Frankenstein" monster freak you out? Or do you shudder at the thought of eating a genetically modified tomato?

If so, then the following blinks may not be for you, as they reveal a whole new world of genetic engineering — the future of science and perhaps even of humanity itself.

You'll learn about one scientific team's fascinating quest to create artificial life from scratch. No, we're not talking about monsters or modified animals; this team, hoping to discover the blueprint for all life on earth, tackled the intricate task of decoding DNA itself.

Reading these blinks, you'll also discover

  * how you can leave a personal note on a string of DNA;

  * why a scientist would want to synthesize a nasty virus; and

  * how in the near future we might be teleporting medicine to Mars.

### 2. Modern biology is based on a new understanding of what life is and what makes it “run.” 

The study of biology asks one profound, powerful question: "What is life?"

In fact, it was exactly this profound question that led physicist Erwin Schrödinger to hold a series of groundbreaking lectures that provoked not just the gathered scientific minds but also a veritable revolution in science as well.

How did he achieve this? Schrödinger in essence presented a new way of interpreting biological life. (Schrödinger, a Nobel laureate, is also notable for his pioneering work in quantum physics.)

In 1944, Schrödinger published _What is Life?_, a book based on his lectures. In it, he examines what exactly makes humans "tick" — and essentially set the stage for modern genetics research.

Schrödinger was one of the first thinkers to suggest that everything that happens in a cell can be explained _solely through physical and chemical processes_. 

Importantly, Schrödinger's ideas inspired scientists James Watson and Francis Crick in their own work, which led to the discovery of what we now know as the genetic code for human life. 

In 1953, Watson and Crick took a closer look at DNA and discovered its now well-known double-helix form. They also identified DNA (deoxyribonucleic acid) as the crucial carrier of the code that determines an organism's genetic information. 

These men also discovered how DNA reproduces and passes its information from generation to generation, work that earned them a Nobel Prize in 1962. Before their efforts, it was commonly believed that proteins — and not DNA — were the carriers of genetic information. 

Ever since, scientists have worked to further unravel DNA's many mysteries. And since 1970, when Crick firmly established the process by which genetic information is transmitted via DNA, scientists have worked diligently to deconstruct and comprehend the entire genetic code. 

Latent in this work is the assumption that all biological life can be reduced to the cellular level — an echo of Schrödinger's hypothesis from all those decades ago.

> _"DNA is the software of life."_

### 3. The possibility of producing life artificially has raised both practical and ethical questions. 

Since the nineteenth century, the scientific community has debated the concept of artificial life and the potential for its creation. 

The debate began in earnest in 1828, when German chemist Friedrich Wöhler challenged the idea that organic and inorganic materials differed in some fundamental way. To show that this notion was false, he chemically synthesized _urea_, the primary component of urine. 

Such a feat might seem inconsequential today, but at the time it was shocking. Society held fast to the idea that human life was precious and special; if a scientist could artificially reproduce something a human does naturally, what does that say about humanity's uniqueness, then? 

Since this discovery, a rift has opened. On one side stands those who believe that "life" is determined exclusively by cellular processes or physical and chemical reactions — nothing more. On the other are individuals who adhere to the idea of _vitalism_, which postulates that life is dependant on a "soul" or some vital function that animates it. 

But the question of whether we _can_ produce life artificially is no longer as pressing as it once was. Today, the question is whether we _should_. Plenty of people fear the potential dangers involved in "playing god." 

Those more morally minded fear that creating life artificially will bring down horrific punishment from on high; or even that intelligent artificial life might develop beyond our control. Popular literature even took up this theme; the frightening and vengeful creature created by the mad scientist Frankenstein captured the imaginations of laypeople who feared what science might do. 

Even in the modern era, the idea of artificial intelligence also raises moralistic questions. Depictions of killer cyborgs in the movie _Terminator_, as well as an AI system gone bad in _2001: A Space Odyssey_, indicate society's concern about artificial "life."

But popular fears haven't stopped the train of exploratory science. Today, the fields of chemistry, biology and computing have come together to give rise to modern _genomics_ and _genetic science_.

> _"Belief is the enemy of scientific advancement."_

### 4. Scientists' discovery of how to manipulate DNA transformed the field of genetics. 

Have you ever wondered how DNA — even from different organisms — is combined? You can't exactly throw genes in a test tube and shake them up, but scientists have come up with a surprisingly straightforward way to slip genetic code into foreign DNA. 

Scientists discovered that by using certain proteins, one can essentially "cut" and then "paste" together bits of DNA. These unique proteins, called _restriction enzymes,_ were first discovered in bacteria in the 1960s.

The enzymes serve as the chemical "scissors" to slice a piece of DNA out of a strand, creating a gap that can then be filled with a new piece of DNA.

This process is called _gene splicing._ Since its discovery, scientists have gained a better understanding of DNA's role in biological life. 

In the 1970s, gene splicing made a huge leap forward. While early experiments involved only simple viruses, scientists in 1972 performed the first gene splice using more complex bacteria. 

Since then, progress has been rapid, and gene splicing experiments quickly became more complex, using mammals such as mice. With each advance, the study of genetics expanded and incorporated ever newer discoveries, such as the role of RNA (ribonucleic acid) in gene replication. 

As DNA is the code of life, RNA is its delivery boy, transporting code from DNA to the _ribosomes,_ or the cellular protein factories that put amino acids into the correct order to produce proteins. 

Gene splicing also helped scientists discover more about how genetic defects can result in hereditary diseases such as cystic fibrosis and other conditions.

### 5. Computer technology enabled faster DNA processing, which aided the search for life’s “secret recipe.” 

Reading and sequencing a DNA strand is far from simple, but the ambitions of geneticists were brought closer to reality with each advance in computer technology. 

In fact, it was the introduction of automated DNA-sequencing machines that propelled the science of genomics into a whole new phase. 

Before powerful computers, sequencing the code of just a single gene could take as long as a year. In the 1990s, however, the advent of fluorescent dyes and advanced lasers made it possible for scientists to record genetic code sequentially into a computer. What's more, the fact that huge amounts of decoded information could be stored digitally made research _a lot_ easier. 

It was at this time that the author founded The Institute for Genomic Research, the world's biggest DNA-sequencing laboratory.

In 1995, his team became the first to completely sequence the DNA of a living organism, laying the groundwork for the author's quest to produce a synthetic cell. 

Creating a database of sequenced genomes essentially brought scientists closer to discovering the constituent parts that are crucial to all life. Once scientists could sequence a variety of genomes, they were able to compare them — an important step in identifying differences and similarities that also increased our overall understanding of the diversity of life. 

From this, the team set out to discover the minimum number of cells necessary for the creation of life. 

The team did this by comparing two different species groups, identifying a total of 480 common genes — some of these genes potentially essential to all living organisms. 

To test the theory, the team chemically synthesized an entire chromosome, composed solely of these essential genes. While work is still ongoing, the team has narrowed the pool by eliminating more and more nonessential genes.

With technical advances in both splicing and storage, as well as the ability to differentiate between essential and nonessential genes, the stage was set for a radically new undertaking: synthesizing DNA itself.

### 6. A crucial experiment led to the creation of chemical DNA and paved the way for synthetic life. 

The author's team then embarked on a journey to synthetically produce a complete chromosome, based solely on computer code. 

But they first needed to develop some new procedures. Eventually, they discovered a promising method for achieving their goal. 

For the experiment, the team chose a "simple" virus called Phi X 174. This virus infects bacteria, and is called a _bacteriophage._ Phi X 174 had been used in various experiments for more than 40 years, and so was well-known in the genetics community. 

Phi X 174's simple structure (with only 11 genes) resulted in the virus being the first to be genetically sequenced, as well as the first to have its genome copied. All this made Phi X 174 a perfect candidate virus for the team's attempts to synthesize a complete chromosome. 

Using the virus, the team was able to use computer code to produce viable genetic information; but, more importantly, they used the information to show that biological life could be produced using nothing but chemical DNA.

As part of the 2003 experiment, the team fed their painstakingly sequenced DNA of the Phi X 174 virus into a computer. The automated DNA synthesizers then reproduced the code chemically. 

Next, the team assembled the carefully selected, ready-made DNA building blocks in the correct order. When everything was in place, they stitched the DNA together using enzymes.

The synthetic DNA produced was then injected into a host bacteria. Following an incubation period, the team confirmed that their artificial bacteriophage had in fact successfully infected the bacteria. 

In a mere two weeks, the team was able to prove that synthetic DNA, chemically built from computer code, contained the information necessary to produce a virus!

### 7. In 2007, the author’s team produced the first synthetic genome of a living organism. 

The team achieved an astonishing feat — they'd synthesized a virus, confirming that synthetic DNA could be activated. 

But viruses aren't really "alive," but are simply genetic information that's coated in a protein. So would it be possible then to synthesize a much more complicated genome, for example from a "living" bacterium? 

This challenge was exactly what the author's team tackled next.

The team sought out the tiniest-known genome that is part of a living, self-replicating cell, called _Mycoplasma genitalium._ This tiny bacterium causes urinary tract infections in humans. 

But this project was much larger than the team's last. To succeed, they would need to accurately synthesize 582,970 base pairs of DNA — 20 times more information than had been synthesized up to that point.

The team's experience showed that they could maintain accuracy while synthesizing a total of 5,000 base pairs of genetic code — so they divided the genome of _M. genitalium_ into 101 segments, called _cassettes_. 

Each cassette would be independently synthesized and then reassembled. The team even ensured proprietary rights to their work by inserting _watermarks_ in the code, or specific gene sequences that marked the genome's lab of origin.

But how did they accurately connect the different sections? Each cassette contained an overlapping sequence at its start and finish, making it easier for the scientists to note where each segment should be attached. 

The final condition was to find a stable environment for the code. They decided on yeast cells, and then injected the cells with the new DNA. 

After painstakingly examining the DNA sequence and identifying the team's watermark, they announced the successful, synthetic production of a bacterial genome.

For the first time ever, the genome of a living organism had been synthesized! The team's next step would be its most formidable challenge yet: transplanting a synthetic genome into a cell to create a living, synthetic _organism_.

### 8. The team’s claim to fame is their transplantation of the first synthetic genome into another cell. 

Despite its wonders, the synthesizing of a living organism's genome just wasn't enough for the scientific community. 

Skeptics could deride chemically created DNA as merely a synthetic molecule — nothing special. The real test was in proving that the team's synthetic DNA was actually viable, and to prove this it would have to be put into the body of a cell to become the cell's new genetic information. 

This was the ultimate goal — because by transplanting a synthetic genome into a cell, they would be basically transforming one species into another in a way that had never before been achieved. 

The first step was to start the experiment again from the beginning, and with a new bacterium. But why did the team give up on _M. genitalium_? Well, the bacterium basically replicated too slowly, forcing the team to wait for up to six weeks to find out whether a test was successful. 

The team wanted to be first, so speed as well as accuracy was at issue. 

The team abandoned _M. genitalium_ in favor of a newly acquired synthetic genome from _M. mycoides._ This rapidly reproducing bacterium allowed them to review results within days.

Finding the correct recipient cell would prove a bit more difficult. The team realized that some cells have a special defensive surface coating that chews up any DNA on contact, making them unsuitable as recipient cells. 

Yet the team discovered that the chemical polyethylene glycol could help in making a recipient cell's membrane more permeable, and even protect DNA as it was transplanted. With this knowledge and a suitable host bacterium in hand, they were ready.

### 9. By transplanting synthetic DNA into a cell, the first organism with a computer “parent” was created. 

Using the polyethylene glycol method, the team succeeded in their initial transplantation tests. They then prepared for one final sequencing, synthesis and transplantation of the _M. mycoides_ genome.

However, things didn't go as planned. With DNA sequencing, even the smallest errors can be fatal. 

Here's what happened. During the final run, the team began turning up negative results — the bacterium wasn't growing. A closer look revealed the culprit: a miniscule, one-letter deletion in the base pair DNA sequencing. This seemingly small mistake threw off everything that followed it. 

The team caught the error and corrected the sequence. The subsequent transplants went off without a hitch, and made genetics history in the process: the first living, self-replicating species to have a computer for a parent! 

As the newly inserted DNA was activated, it began creating cell colonies that were controlled only by the synthetic genome. And because the team remembered to watermark their work, they even went as far as to encode the lab's email address in the synthetic DNA sequence! 

They'd digitized biology by turning DNA's chemical analog code into the digital code of a computer; and from this, rebuilt the chemical information into a molecule of DNA to make original, living cells. 

The team called the new cell _M. mycoides JCVI-syn 1.0_, in honor of the J. Craig Venter Institute. 

With this groundbreaking experiment, synthetic life became a reality. But how did people react to this breakthrough? And more importantly, what impact will artificial life have on the future?

### 10. The public met the first synthetic organism with both cheers and jeers. 

The author's goal in creating the first instance of synthetic life was to prove that DNA is the software and basis for all life. In other words, he wanted to put to rest all arguments for vitalism. 

However, getting people to agree on what constitutes "life" is no easy task. 

Responses to the breakthrough were mixed. Some journalists said it was a profound moment; others called it a haphazard but important discovery. Some even said that no "synthetic life" actually had been created, as a natural cell was used for the transplant as opposed to a synthetic one. 

This last group held that the term "synthetic life" should be used only to refer to organisms made entirely from scratch. Yet it's a virtual impossibility to create something without using an ingredient that's been pre-processed in one way or another. For instance, when you bake a cake from scratch, seldomly if ever do you grind the flour or mill the sugar yourself.

But more importantly, many people were concerned that this process could end up in the wrong hands. Technological advances have enabled homemade versions of lab tools, and open-source information might make it possible for nearly anybody to mess with the "software" of life. 

For example, bioterrorists could learn to produce potentially lethal germs, such as the bacteria that causes the bubonic plague, which killed tens of millions of people in the Middle Ages.

So regulations are necessary, but it's also important to maintain a perspective that balances remote yet potential threats with biotechnology's incredible benefits. 

For example, biology students now have unprecedented access to technology that used to be reserved for top scientists, helping everyone benefit from increased knowledge and discovery. 

And yet there's still more to come. Imagine the benefits of teleporting DNA or even entire organisms, a possibility that could be unlocked by digitizing and synthesizing DNA!

### 11. Teleportation isn’t just for Star Trek; soon we’ll send DNA code from planet to planet. 

You are one of the first colonists on Mars. Yet the machine that regulates your shelter's atmospheric pressure springs a leak, exposing you to an alien germ. You need an antibiotic, but only a lab on earth can help you. So what are your options? 

Sure, there are no Mars colonies — yet. But earth-based scientists are already thinking about situations like these and coming up with solutions for the challenges of a life among the stars. 

Biological teleportation is one potential solution. Building on their work using computer code to generate living organisms, the author's team has been exploring ways to turn genetic information into electromagnetic waves capable of traveling great distances. 

If these experiments are successful, we'll be able to send at lightspeed information required for building life — essentially _biological teleportation._

For instance, you could teleport the DNA of the Martian bacteria to a lab on earth, where scientists could devise and then teleport back an antibiotic. 

The health applications of this technology are countless. It could be used to rapidly diagnose new diseases or design medications and instantly deliver them to hospitals via their molecular structures. 

Biological teleportation can also help us understand life on other planets. If we were to discover an alien life form, we would naturally want to analyze it in a laboratory. The only problem is transportation. A specimen might die during the long trip to earth, or could contain something harmful, like a flesh-eating bacteria. 

But soon we may have robotically controlled genome sequencers that can read the DNA of any microbe and send the information straight back to laboratories on earth, where the organism can be safely reconstructed in a controlled setting.

> _"Humankind is about to enter a new phase of evolution."_

### 12. Final summary 

The key message in this book:

**Modern biology has discovered life's secrets, and they're found in our DNA. Geneticists have in recent years unlocked the tools to manipulate, copy and even digitize genetic code, thereby opening up futuristic possibilities, such as transmitting DNA online or teleporting genetic code from Mars.**

**Suggested further reading:** ** _A Life Decoded_** **by J. Craig Venter**

_A Life Decoded_ (2007) is the autobiography of the prominent American biochemist and geneticist Craig Venter, who played a key role in one of the greatest scientific achievements of our time — the deciphering of the human genetic code. These blinks describe the personal experiences that drove his scientific research, even at times when his methods were attacked by the scientific community.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### J. Craig Venter

Craig Venter is a world-famous biochemist and geneticist. Author of his own autobiography, _A Life Decoded,_ Venter has also been included twice on the "TIME 100," a list of the world's most influential people as chosen by Time magazine editors.

