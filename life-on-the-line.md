---
id: 5846eb0e714e5f00047cdce7
slug: life-on-the-line-en
published_date: 2016-12-26T00:00:00.000+00:00
author: Grant Achatz & Nick Kokonas
title: Life, on the Line
subtitle: A Chef's Story of Chasing Greatness, Facing Death, and Redefining the Way We Eat
main_color: D6362E
text_color: BD3029
---

# Life, on the Line

_A Chef's Story of Chasing Greatness, Facing Death, and Redefining the Way We Eat_

**Grant Achatz & Nick Kokonas**

_Life, on the Line_ (2011) tells the remarkable story of Grant Achatz, a fiercely determined chef whose drive to become the best found him redefining American cuisine before he was even 30 years old. His dreams came true when he opened his own restaurant, Alinea, in Chicago, but just as the awards and accolades came pouring in, a bigger challenge presented itself: he was diagnosed with advanced mouth cancer and faced the possibility of either dying or losing his tongue.

---
### 1. What’s in it for me? Get to know one of America’s greatest chefs. 

Chefs are the new rockstars. They live large; they have television shows; and their fans travel across the globe just to taste what they've cooked. One of these superstars is Grant Achatz. Pulitzer Prize-winning journalist David Shaw has described Achatz's food as an intellectual adventure as well as a great culinary experience, and his restaurants have been crowned the best in the United States. How does one become such a great chef?

From starting as a line cook, responsible for setting up a station and cooking dishes according to the head chef's recipes, to becoming a head chef himself, Achatz has worked his way through the ranks of some of the greatest kitchens. Everything changed, however, when it appeared he'd have to choose between cooking and his own life.

In these blinks, you'll learn

  * how one chef almost made Achatz give up on cooking;

  * what influence chef Thomas Keller had on Achatz's career; and

  * how Achatz created one the world's best restaurants without being able to taste.

### 2. Grant grew up working at his family’s restaurant before moving on to culinary school. 

At just five years of age, Grant Achatz was already cooking. Well, perhaps stirring a bowl of cherry Jell-O isn't exactly "cooking," but food was a big part of Grant's life for about as long as he can remember.

In fact, Grant grew up in a family that owned a series of successful small-town restaurants.

The first one was the Achatz Cafe, a tiny place in Marine City, Michigan, where, from an early age, Grant was given various jobs.

His responsibilities increased as he got older. He started out peeling vegetables and washing dishes; at the age of ten, he was already happily cooking eggs for regular customers.

In March of 1983, the family upgraded to the Achatz Family Restaurant. A much bigger place, with a capacity of 165, it was an even bigger success. Grant worked there as well, and by the time he was fourteen years old he'd moved up to the position of line cook and was given the responsibility of the opening shift and preparing the restaurant for the day ahead.

So, when Grant left Michigan to attend the Culinary Institute of America (CIA) in New York City, he was more experienced than most students.

Grant took CIA's eighteen-month associate degree program, part of which required students to perform an internship at an accredited restaurant. Grant sent out a bunch of letters to different restaurants, but the only response he got landed him at Cygnus, a restaurant at the Amway Hotel in Cedar Rapids, Michigan.

Luckily, Cygnus had a friendly head chef named Jeff Kerr who was willing to show Grant some fundamental techniques, such as how to break down various animals: game birds, fish, and entire pig. On one occasion he even taught him how to make prosciutto.

After three months of preparing soups and salads and taking care of _mise en place_ (the putting together of ingredients for a line cook's workstation), Grant was already showing great promise.

Chef Kerr took notice and introduced Grant to something that would change his life forever.

> _"I want people to be excited, happy, curious, surprised, intrigued, and even bewildered during a meal."_

### 3. Grant’s first experience working for someone he greatly admired was terrible. 

One day at Cygnus, Chef Kerr told Grant about a cookbook by the Chicago chef Charlie Trotter. It was a revelation: the book was an intoxicating mix of simple yet complex and abstract modern cuisine, and Grant spent every night studying the book until he knew it by heart.

After graduating with honors from culinary school, Grant sent Trotter his resume and a cover letter; weeks went by, and just when he was about to give up hope, he got a call. Trotter offered him a two-day trial.

But, unfortunately, working at Charlie Trotter's was not the opportunity he was hoping for.

After loudly berating one chef while strutting around the kitchen, Trotter introduced himself to Grant: "I am Charlie Trotter. If you give a shit."

As a young chef, Grant had hoped to enter an environment that offered mentorship and opportunity to learn, but this kitchen was the polar opposite of that. Instead, he spent sixteen-hour days in an unfriendly kitchen full of stressed-out people. And, as if that wasn't unpleasant enough, Grant had to stick around afterward and remove grease from the inside of the stove hoods with a toxic cleaning spray.

But perhaps the biggest disappointment was that Trotter never even cooked!

As the days went by, Grant was plagued by the feeling that the experience was making him a worse chef, not a better one, and he gradually became convinced that he needed to move on.

He worked up his confidence to give Trotter his notice. When he approached him, he apologized and said that it just wasn't working out.

Trotter was not happy: he told Grant that if he didn't stay for at least a year, he would mean nothing to him and couldn't even put his restaurant on his resume. Grant understood, but he left anyway.

The experience at Charlie Trotter's left Grant feeling depressed. He began to have doubts. Was this what all kitchens were like?

### 4. On a culinary tour of Europe, Grant found inspiration in an unexpected place. 

To reinvigorate himself and get some inspiration, Grant and his girlfriend Cindy decided to take a culinary tour of Europe. The trip was centered around visiting some of the restaurants that had received the prestigious three-star award in the Michelin Guide, arguably the highest honor a restaurant can get.

The first stop was in Paris, where they dined at chef Gerard Boyer's Les Crayères.

It was a nice experience, but a little disappointing. The service was condescending and, for a three-star restaurant, it just wasn't what Grant had hoped for.

This happened again in Vonnas, France, when they dined at chef Georges Blanc's famous restaurant.

While the food was fine, the service was again patronizing and unpleasant — and when the main course arrived, the squab was overcooked.

The next stop was Florence, Italy, and the restaurant Enoteca Pinchiorri.

While it was better than the French restaurants, Grant was still disillusioned. He'd hoped to be inspired and reinvigorated by exciting food, but it simply wasn't happening.

But just when he thought the trip was going to be a complete disappointment, inspiration unexpectedly came during a bike tour of the Tuscan countryside.

As the tour was winding down, the guide had everyone stop for a meal at what looked like an old abandoned stone structure.

When Grant approached, the smell of garlic, herbs and grilled meats hit his nose and an elderly Italian woman came out to give them a warm and friendly greeting. Then she served an incredible meal: chicken that was grilled under bricks; homemade gnocchi; a slow cooked, creamy white bean dish called _fagioli al fiasco_ ; and, of course, plenty of wine.

This was it! This was the friendly service and the amazing, lovingly cooked food that Grant was looking for but hadn't found at any of the three-star restaurants. And it allowed Grant to return to the states with the boost of energy and inspiration he needed.

### 5. With renewed passion, Grant took a culinary leap forward by cooking with Thomas Keller. 

When Grant returned to the United States, he wanted to get his career back on track. And it all began when he was looking through a 1995 issue of _Wine Spectator_ on the best US restaurants. On one page, separate from the main list, he found what he was looking for: a small Napa Valley restaurant called The French Laundry.

Grant was determined to work with its chef, Thomas Keller, who was using unusual ingredients to make exciting food that was starting to get a lot of attention.

Grant sent a total of fourteen letters to Keller. And finally, after two weeks of keeping his fingers crossed, he got an offer for a two-day trial.

When he arrived at 11:30 a.m., Grant was surprised to see Keller himself sweeping the floor of the small, unassuming restaurant. Keller also had a friendly laugh that put him at ease and Grant still remembers the warm handshake he gave him before putting him to work.

It was all very much unlike Charlie Trotter's, including the fact that Keller actually cooked.

It was the perfect environment for Grant to excel in. There was no yelling in the kitchen and it was calm and full of talented chefs who had already worked at amazing restaurants. There were guys in the kitchen who had come from some of the best restaurants in the world, including Jean Georges and Ferran Adrià's Catalan restaurant, elBulli.

Keller also mentored his chefs, showing them how to clean and prepare _foie gras_, and how to cook sophisticated food by combining unexpected flavors. Soon enough, Grant was helping prepare dishes that far surpassed the ones he'd had at the three-star restaurants in Europe.

It was also a great opportunity for Grant to work with unusual ingredients like sea urchin, duck tongue, veal brains and pig ears — and to learn how to prepare dishes like oyster infused tapioca; salmon with a ragout of lentils, truffles and lardoon; and the five-day long procedure of properly preparing tripe.

### 6. Eventually, Grant needed to follow his own vision, which led him to Trio. 

After eleven months working as commis at The French Laundry, Grant moved up to line cook, where he was responsible for six to ten dishes every night. But Grant always knew that this was just a stepping stone for him to eventually do his own thing.

So, after two years of working the line, Grant was eager to strike out in a new direction. For a year he worked at a nearby winery and learned how to bottle wines the old-fashioned way, without any machinery.

But then, in June of 1999, he got an offer from Thomas Keller that he couldn't refuse: the position of sous chef at The French Laundry.

At this point, the restaurant was already considered one of the most exciting places to eat in the United States. Reservations were nearly impossible to get. And now, Grant was working side-by-side with Keller.

Grant even got one of his own dishes onto the menu: it consisted of caviar, cantaloupe melon made into a mousse and a thin gelée of champagne. Keller was quite impressed.

As sous chef, Grant also got the chance to go to Spain and visit the already legendary elBulli restaurant, where he saw how to prepare dishes such as uncooked trout roe in a perfectly fried tempura batter. It boggled Grant's mind: How could something be fried and remain uncooked?

Then there was the braised rabbit with hot apple gelatin, which presented another paradox: How can gelatin be hot when it needs to be cool in order to settle?

When Grant returned from elBulli, he was more eager than ever to strike out on his own, and fortunately there was a restaurant outside of Chicago called Trio that was looking for a new head chef.

After a month of email exchanges with owner Henry Adaniya, who was greatly impressed with Grant's seven-course test dinner, Grant was offered complete control to transform Trio into his vision of American fine dining.

### 7. At Trio, Grant Achatz began to make a name for himself by making wildly original and creative food. 

Grant left The French Laundry in June of 2001, and by the first of July he'd put together a small team of five cooks who bonded over Grant's ambitious plan and then cleaned and remodeled Trio's kitchen.

Grant was excited to create his own version of modern cuisine, and he hoped to make Trio the same kind of food destination for the Midwest as The French Laundry was for the West Coast.

Grant put together three menus: a modern four-course prix fixe menu, a vegetarian menu and an eight-course avant garde tasting menu for the more daring customers.

Grant had to teach the kitchen staff all the techniques he'd learned and how to prepare dishes such as an early signature dish called the Black Truffle Explosion.

After a couple months of the restaurant barely breaking even, the reviews they were hoping for finally came in: the _Chicago Tribune_ and the _Chicago Magazine_ both gave Trio their highest rating of four stars. They called it dynamic cuisine. They implored people to go.

The new popularity that followed gave Grant the freedom he was looking for. He wanted to experiment with more ways of incorporating all the senses in his cuisine.

For example, after testing and tinkering, he created a rosemary vapor to add more flavor to a lobster dish.

Grant also began a long collaboration with Czech designer Martin Kastner to develop handcrafted, experimental new serviceware.

One of Kastner's first designs was "the Tripod" for Grant's frozen lavender desert. It held the frozen orb in place with three legs, which the diner could then squeeze together and lift up to form a popsicle stick.

In 2004, Trio was in peak form. The kitchen was in perfect harmony and Grant came out with his most ambitious menu to date, a twenty-six course, four-hour dinner.

It featured dishes where apple puree was served in the stems of an Angelica plant, caviar was paired with kola nut ice and steamed milk and raspberry tapioca parfait was accompanied by the aroma of freshly cut red roses.

### 8. Alinea became the first restaurant that Grant owned; it was a success from the start. 

Another guest at Trio was Nick Kokonas, a businessman so in love with the food there that he asked Grant if he'd like to partner up and open his own restaurant.

This wasn't the first offer Grant had received, but when Nick told him that the restaurant would be 100-percent in accord with his vision, Grant accepted.

Grant named his first restaurant Alinea, a name for the symbol that marks the start of a new paragraph (¶). It was an ambitious restaurant right from the start. Grant knew he wanted to create a unique atmosphere that would match his unique food, and he was totally adamant about certain things. For instance, no tablecloths!

Nick and Grant worked tirelessly throughout 2004, paying close attention to every detail. Grant's vision of Alinea finally came to life in April of 2005, just a week after his thirtieth birthday.

Alinea was a smashing success.

Grant told his staff he wanted nothing less than for Alinea to be the best in the country, and he knew that there was a tremendous amount of buzz surrounding their opening.

In fact, there was a line outside the door on opening night, with people arriving hours before their scheduled reservation, which made for some challenging service.

There was also an unexpected person in attendance that night: the _New York Times_ food writer Frank Bruni, who followed his visit with a snarky article that lumped Alinea in with the so-called "molecular gastronomy" movement in American cuisine.

Even though the article called his food "spectacular," Grant was fuming over being categorized alongside restaurants that were nothing like Alinea.

But the reviews they were after soon followed: The _Chicago Tribune's_ four-star review was entitled "Alinea Plays for Perfection," and _Gourmet_ magazine immediately named it the number-one restaurant in America.

Nick was actually worried that they'd reached their goal _too_ quickly, but, as we'll see in the next blink, the real concern came after Grant's visit to an oral surgeon.

> _In 2010, Alinea was voted #7 at the London World's Best Restaurant Awards, making it the top ranking U.S. restaurant._

### 9. Diagnosed with cancer, Grant continued to transform Alinea, even after losing his sense of taste. 

A painfully inflamed white spot on Grant's tongue made him decide to visit the doctor. After a biopsy, the doctor came back with frightening results: _advanced squamous cell carcinoma of the tongue_, or stage four mouth cancer.

After visiting the best doctors in the country, the only course of treatment being recommended was the removal of the cancerous parts, which included his tongue, jaw and part of his neck, and the immediate commencement of chemotherapy.

But even this treatment only came with a 50-percent chance of survival.

Food was Grant's life, and he was facing a life without his tongue.

Just as Grant was starting to accept his grim fate, he met Dr. Everette Vokes at the University of Chicago. Dr. Vokes believed he could save Grant's tongue with a relatively new drug called _cetuximab._

Combined with a precise and aggressively targeted radiation treatment, he hoped the drug would reduce and hopefully eradicate the cancer.

It was life-saving news for Grant, but the treatment was indeed aggressive, causing Grant to lose 50 pounds, shed his hair and break out in acne rashes. It also caused the skin of his tongue to peel off, which led to his losing his sense of taste.

Remarkably, he continued to work despite all this, and his food continued to evolve.

Even though he could place salt directly on his tongue and taste absolutely nothing, and barely talk or swallow, Grant still went to work every day after treatment to put together Alinea's fall menu. It was a measurement of how well he'd trained and trusted the chefs under his guidance that he could allow them to taste for him.

And even more remarkably, it was at this point that Nick Kokonas sat down to a meal at Alinea and discovered that, somehow, the food was getting even better.

### 10. Recovery was difficult, but along with his health, Grant’s inspiration and passion also returned. 

On December 13, 2007, Grant was finishing his treatment via surgery to remove affected lymph nodes in his neck. And shortly afterward, as he was still recovering, Grant got the news: he'd won the James Beard Award for Best Chef in America.

The recovery took months of readjusting to the effects of the treatment and to the success of Alinea.

Five months after his surgery, he was still cancer free, and his taste was just starting to come back, but he found himself temperamental and despondent after the ordeal. He wasn't talking to his coworkers and even began to contemplate quitting.

To try and re-spark his ambition, Grant considered opening something in New York or Las Vegas, but Nick was quick to provide Grant with a reality check.

He reminded Grant that it made no sense to open up a restaurant just because they could. After all, he was still recovering and a new restaurant now would be a no-win situation: if it outperformed Alinea, it would only harm their flagship restaurant.

Nick was right. And so Grant took the time he needed to recover and reacquaint himself with what was important in his life and allow inspiration to return naturally.

If there was an upside to his cancer and recovery, it was that he finally slowed down and spent more time with his two kids.

And as his weight and strength came back, he began to enjoy life again. He realized that when you stop worrying about the future, you can start appreciating the fact that you have one.

This is when inspiration did strike: Nick and Grant began work on Next, a restaurant that would remain exciting and challenging by focusing on a new style of cuisine each season. Depending on the season, it would strive to become the next best Italian restaurant, Mexican restaurant, French restaurant — a dynamic, future-facing project indeed.

### 11. Final summary 

The key message in this book:

**Even in the face of life-threatening adversity, it's possible to continue to find inspiration and keep your dream alive. That's what Grant Achatz discovered as he managed to reach the pinnacle of being named the greatest chef in America — with the best restaurant in America –** ** _and_** **beat a cancer that threatened to take everything away from him.**

Actionable Advice

**Inspiration can strike when you have the right perspective.**

One day at Alinea, nothing was going right. Right as a shipment of raspberries was getting delivered, a tray of wine glasses fell, smashing glasses on the ground. But instead of getting upset, Grant remembered the two competing sensations — the smell of fresh berries and the shattering of glass — and got to work creating a dish that would combine them by transforming raspberries into a fragile, glass-like state.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Fast Food Nation_** **by Eric Schlosser**

_Fast Food Nation_ shows how the fast food industry has massive consequences on many other aspects of our lives, including our education, health and working conditions. The book reveals the terrible methods and working conditions — caused in great part by the fast food industry's focus on profit — that are used to create our food.
---

### Grant Achatz & Nick Kokonas

Grant Achatz has won multiple awards for his cooking, including being named the best American chef and owning the best American restaurant. He owns three Chicago restaurants: Alinea, Next and The Aviary.

Nick Kokonas is Grant Achatz's business partner since 2005. He's active in the marketing and planning of Achatz's restaurants.

