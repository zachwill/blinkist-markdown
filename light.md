---
id: 577f7651b7b7eb000373a169
slug: light-en
published_date: 2016-07-11T00:00:00.000+00:00
author: Bruce Watson
title: Light
subtitle: A Radiant History from Creation to the Quantum Age
main_color: CE6336
text_color: B0552E
---

# Light

_A Radiant History from Creation to the Quantum Age_

**Bruce Watson**

_Light_ (2016) is about illumination in all its forms. These blinks go back to the earliest days of humanity to show how, for millennia, light has served as divine, artistic and scientific inspiration.

---
### 1. What’s in it for me? Be enlightened by the wondrous world of light. 

From the dawn of humankind, light has been a source of wonder, appearing in stories of creation and even being worshipped like a god. Light also developed into a source of philosophical inquiry, creative fuel, and scientific struggle — resulting in brilliant art and significant discoveries.

In these blinks you'll be guided through the fascinating story of figuring out what light is and what light can be used for, from illuminating cathedrals to forming one of the centerpieces of the scientific revolution.

You'll also learn

  * what vomit, egg and the Book of Genesis have to do with light;

  * why ordinary mirrors have been viewed as divine; and

  * how light is at the center of the most influential experiment in modern physics.

### 2. Light has been an essential and influential phenomenon since the beginnings of humanity. 

The early humans didn't study light — they worshiped it. In fact, as light crept across a continent each day, it was invariably met with reverence and awe.

So, it's not surprising that light is a key player in lots of creation myths. In the Finnish creation story, _The Kalevala_, an egg cracks open, its yolk becoming the sun and its white the moon.

Or consider the Zuni Native American myth in which earth's first people emerged from a dark underworld into glorious light. Then there are the Bushongo tribesmen of the Congo, who describe the god Bumba, a deity who vomited up the sun. As the sunlight spread, the earth's primordial waters dried up and land began to surface.

And finally, there's the Book of Genesis, in which God says "let there be light."

So, light is essential to people all over the world and it was one of the first topics studied by ancient philosophers. Greek philosophers like Empedocles asked whether light came from an object or the eye that was seeing it. And in the fifth century BC, the philosopher Leucippus asserted that all objects emitted razor-thin light particles.

Following this claim, Euclid and Ptolemy were some of the first people to study light in a laboratory setting, observing its reflection in flat and bent mirrors. From these studies, Euclid discovered that the angle at which light entered a mirrored plane was equal to the angle at which it was reflected. And Ptolemy figured out how curved mirrors interacted with light.

But despite these experiments, nobody knew what light actually _was_.

Because light remained a complete mystery, it became for many religions a symbol of all that is holy. For example, the Old Testament used light as a metaphor for God in a couple of ways:

On Mount Sinai, Moses encountered the brightly burning bush from which God spoke to him. Not only that, but every sacred person in the biblical story is depicted with an aura of light that makes them glow.

### 3. Light became a divine religious force, as well as the site of artistic mastery. 

If you know some Arabic, you might know that the word _manara_ means lighthouse or "place of light." It's from this word that the _minaret_, the tower from which a muezzin calls Muslims to prayer, gets its name.

That's because light is a divine force in Islam. The Qur'an says that God will lead the faithful "from darkness to the light." It also says that "He will bestow on you a light to walk in," and "the man from whom God withholds His light shall find no light at all."

In fact, one of the _suras_, or chapters, of the Qur'an is devoted entirely to light. It's called _al-nur_ or _The Light_ and says that "God is the light of the heavens and earth."

But Islam isn't the only religion to place light on a holy pedestal. Christianity does, too.

During the Middle Ages, Thomas Aquinas was the predominant thinker in Catholic theology. He believed that beyond the darkness of the night sky lay a radiantly blazing paradise. He called it "brightness of glory."

As a result, jewels, gold and even normal mirrors were seen as holy because they reflected the light of God. Not just that, but churches were constructed with massive windows to let in the light of heaven.

So, light became a centerpiece of religious thought, but during the Renaissance it was also key to artistic mastery. Around the 1500s, as the Renaissance spread from Italy to the Netherlands, artists began giving light a new life on canvas.

Artists like Brunelleschi, da Vinci and Rembrandt studied perspective and the traits of shadows to paint light in all of its subtlety. It was this focus on light that made the paintings of this time appear so realistic, and which formed the basis of their genius.

> _"Light was Rembrandt's Holy Grail."_

### 4. Light was explored in the laboratory and then served as inspiration for artists of all kinds. 

As the Renaissance came to a close, European countries began making significant developments in the natural sciences. It was during this period that followed in the seventeenth and eighteenth centuries, now known as the _scientific revolution_, that the empirical investigation of light began in earnest.

One of the first people to study light scientifically at this time was Johannes Kepler. In 1604 he published a treatise on optics in which he described a fundamental law of light. He said that the intensity of light weakened with the square of the distance from which it was observed. So if you move a candle ten feet away, you perceive its light to be one-hundredth as bright as it was when you were standing right next to it.

It was also at this time that René Descartes began to think about light and went on to describe several of its properties. He found that light travels in an instant and generally in straight lines. He likened these characteristics to a tennis ball because, when a beam of light travels through space, it hits an object that reflects it back at a precise angle that corresponds to its angle of entry — just like a ball bouncing.

Then, in the 1670s, while conducting experiments with prisms, Isaac Newton discovered that light is composed of colors and that red and blue light make purple light.

Around the same time, the Italian scientist Francesco Grimaldi proposed that light might be a wave rather than a beam. These new scientific ideas about light began to inspire artists.

During the eighteenth and nineteenth centuries — the Romantic era — light was the inspiration for musicians and writers alike. This fascination led to musical performances in which light could be _heard_. Joseph Haydn, one of Europe's most adored composers, wrote an oratorio called _The Creation_, which turned God's words "let there be light" into melody.

And writers were quick to join the party. Just take the German poet Goethe, who published his _Theory of Colors_ in 1810. In this work, he suggests that we both see and _feel_ light. His theory was that each light color produces a mood. For instance, yellow conveyed serenity and red dignity.

> "Sunrise is Beethoven, sunset Mozart."

### 5. The wave theory of light was widely adopted in the nineteenth century and the first electric light was generated. 

So, light was garnering scientific attention and inspiring great works of art, but at the beginning of the nineteenth century, it was still poorly understood. In fact, all that was really known was how to calculate the angle at which light deflected as well as its approximate speed, that it broke down into colors and was composed of particles — or was it waves?

Luckily, a new generation of students emerged to investigate further. In the early nineteenth century, evidence was found to support the wave theory of light. It was all thanks to the English scientist Thomas Young, who, in 1802, devised what's now known as "Young's experiment," a test that's been called the single most influential experiment in modern physics.

Here's how it worked.

Young would beam light through two slits, one next to the other. If the light was composed of waves that crisscrossed and collided, then he hypothesized that this "interference" should present itself in predictable patterns.

That's because, if two waves are in sync, their crests paralleling and boosting one another, the light should shine brighter. Not just that, but where the crest of one wave met the trough of another, the waves should negate each other, producing darkness, or at least dim light.

And that's exactly the pattern Young observed in his experiment.

Then, in the middle of the century, light was found to be electromagnetic — that is, both electrical and magnetic — by the Scottish scientist James Clerk Maxwell. This led to the creation of the first electric light a few years later. This was a considerable advance for humanity. Prior to this, lighting involved stripping an animal or plant of its fat to produce candles or oil and then burning them.

> _"Descartes never investigated light again, preferring the simpler task of proving the existence of God."_

### 6. Einstein transformed the theory of light and used it to make a profound conclusion. 

At the end of the nineteenth century, the German physicist Albert A. Michelson made the foolhardy claim that the potential for new discoveries in the physical sciences was extremely slim. But we all know that great discoveries continued to be made and many of them led to Einstein's description of light as _quanta_.

In the early twentieth century, scientists were still trying to confirm if light was made of particles or waves, when, in 1905, Albert Einstein offered another description — _quanta_, or packets of energy.

It started three years earlier when another German physicist named Philipp Lenard discovered that when ultraviolet light hits a metal plate, it knocks off electrons. The plate emits electrons in turn, something now known as the _photoelectric effect_.

But if light is made out of waves, how could it knock off individual particles? And if it's made of particles, how do you explain the fact that, no matter how bright the light, the freed electrons always contain the same amount of energy?

Well, Einstein's explanation was that an entirely different category existed where light consisted of discrete wave packets — quanta — and could behave as both particle and wave.

From there, the scientific revolution kept rolling. Basing his reasoning on the fact that light's speed is constant, Einstein proposed that time must be relative. How does that work? Well, when you walk onto a moving walkway in an airport, your speed is added to the speed of the walkway. But that's not how light works; it always travels at the same speed.

With this information in hand, Einstein concluded that time is capable of changing. Light might move at a constant speed, but it can travel different distances in the exact same time. So, if light is constant, time cannot be.

> _"Light simply refuses to behave like anything else in the universe."_

### 7. Final summary 

The key message in this book:

**Human beings have been fascinated by light since our evolution on earth. We have worshipped and investigated it, and it's formed the inspirational basis for painters, musicians and poets alike. Nowadays, light is just a click of a switch away, but it took hundreds of years to find out what exactly it is.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Where Good Ideas Come From_** **by Steven Johnson**

_Where Good Ideas Come From_ examines the evolution of life on Earth and the history of science. The book highlights many parallels between the two, ranging from carbon atoms forming the very first building blocks of life to cities and the World Wide Web fostering great innovations and discoveries.

In addition to presenting this extensive analysis, replete with anecdotes and scientific evidence, Johnson also considers how individual and organizational creativity can be cultivated.
---

### Bruce Watson

Bruce Watson is a frequent contributor to _Smithsonian_ magazine, where he writes about everything from eels to pi, artists and writers. His other books include _Bread and Roses_, _Sacco and Vanzetti_ and _Freedom Summer_.

