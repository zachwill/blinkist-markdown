---
id: 52bec84b3933330008010000
slug: lincoln-on-leadership-en
published_date: 2014-01-01T13:51:43.000+00:00
author: Donald T. Phillips
title: Lincoln on Leadership
subtitle: Executive Strategies for Tough Times
main_color: D93933
text_color: BF322D
---

# Lincoln on Leadership

_Executive Strategies for Tough Times_

**Donald T. Phillips**

_Lincoln on Leadership_ is a detailed examination of the strategies that enabled Abraham Lincoln to lead so effectively before and during the American Civil War. The book gets past the myths of the legendary president and illustrates specific facets of his leadership ability to compare Lincoln's skills to successful strategies employed by modern leaders today.

---
### 1. Modern leaders should learn from Abraham Lincoln and go out and engage with staff from every level of their organization. 

In the American Civil War, many state regiments passed through Washington D.C. on their way to the front. At every opportunity, Abraham Lincoln was there to personally inspect them. Despite the obvious pressures of being president, he took the time to see his soldiers and instill in them his vision of what they were fighting for.

This example is typical of Lincoln's hands-on approach in leading his administration. He maintained close involvement with every division, from his cabinet members to the ordinary soldiers in the Union Army.

Lincoln's desire to keep in touch with his colleagues led him to actively seek them out. For example, not happy with just meeting with his cabinet colleagues at their official bi-weekly meetings, he would chase them down and hold impromptu conferences. In fact, Lincoln spent about 75% of his time meeting people from all aspects of his administration.

Lincoln's method of engaging at every level of his organization is similar to the modern management technique: Managing by Wandering Around (MBWA). MBWA teaches managers to spend as much time as possible with their staff from all levels and departments. They should use this time to stay in touch with employees, gather their opinions on policy and instill in them the organization's values.

MBWA suggests that it is crucial for managers to get out of the office and meet with people on the front line of their business. Once again, Lincoln knew the importance of this. He would often leave his presidential surroundings to meet colleagues and supporters. He even went to the battlefield to comfort wounded soldiers and, as he had done during his Washington inspections, instill the courage and vision to keep fighting.

**Modern leaders should learn from Abraham Lincoln and go out and engage with staff from every level of their organization.**

### 2. Great leaders like Lincoln preach, persuade and reinforce rather than coerce. 

There has always been a debate about which is the better strategy: should leaders use the carrot or the stick to best motivate their staff?

Abraham Lincoln proved that the best leaders use the carrot. From the fierce devotion of the Union Army to kinship of the common citizen, Lincoln inspired a loyal following through his ability to persuade, preach and encourage.

A great example of Lincoln's powers of preaching and persuasion can be found in a famous speech in which he attacked the views of pro-slavery politicians. At the time, the issue of slavery divided the American public, and many wanted to maintain it. Yet, through his words, Lincoln was able to bring people around to his abolitionist outlook. He matched rhetorical flair with a well-crafted argument, suggesting that most of the United States' founding fathers had at one time been against slavery. By doing this, he was appealing to the audience's shared faith in their nation's founding principles. Lincoln won them over and, in fact, this speech greatly helped him win the presidency.

Such examples show Lincoln was able to persuade the American people to side with him through the most divisive time in US history. Where others would have looked to coercion and strict discipline, Lincoln used persuasion.

Yet, it wasn't just in preaching to the public that Lincoln used his skills of persuasion. In his personal dealings with individual members of his administration, he used persuasion as a method of motivation. For example, he empowered his subordinates by making his orders sound like suggestions. This gave them the helpful impression that they were taking part in the process. Instead of being inferiors, they were made to feel like equal partners; he provided them with the confidence to continue working hard.

**Great leaders like Lincoln preach, persuade and reinforce rather than coerce.**

### 3. By mastering speaking both in public and in private, and knowing when to be silent, Lincoln became an expert communicator. 

What's the secret of great leadership? Is it strict discipline? Or maybe luck?

No, the secret of great leadership is communication. The ability to clearly convey your message to people is vital to being a successful leader.

Abraham Lincoln was well aware of this. He mastered the art of communication in three crucial areas. He became adept at speaking to large audiences, communicating with individuals, and, crucially, knowing when tokeep silent.

Lincoln was a superb public speaker, and many of his speeches, such as the Gettysburg Address, have become legendary moments in American history. The secret to his success in this area was preparation. Lincoln prepared exhaustively for his public speeches by writing the full texts in advance and continuously editing them right up until the moment he delivered his speech, ensuring they were the best quality.

Lincoln was also aware that great leaders need to communicate beyond the public stage, so he became equally adept at interpersonal communication. His method was to use stories and imagery to illustrate his points. This meant he could easily communicate complex information with people from a range of backgrounds. This was vital in helping both civilians and military subordinates quickly understand his ideas without time-consuming discussion or explanation.

Lastly, Lincoln understood that, in communication, silence was as effective as speech. He knew that he could save himself the chance of being misunderstood by not speaking at times. For example, in his presidential reelection campaign, he limited his public speaking engagements, which saved him from the risk of alienating voters who already knew his record from his first term. This helped him retain support and win a huge victory over his rivals.

**By mastering speaking both in public and in private, and knowing when to be silent, Lincoln became an expert communicator.**

### 4. Lincoln always carefully considered his options, meaning he was able to successfully take intelligent and bold action. 

When the United States was on the verge of the American Civil War, Abraham Lincoln was faced with a dilemma: he felt he needed to resupply the crucial military post of Fort Sumter, but he knew that in doing so he would likely initiate the conflict.

So what did Lincoln do?

First, he carefully considered his position.

Lincoln knew that he couldn't be seen as starting the war as this would weaken his popularity and, therefore, his political position. Yet he couldn't afford to leave a key position under-resourced, either. So he carefully weighed the pros and cons of each decision and made sure he would be ready for every possible scenario. Only by carefully considering his position could he determine the best strategy.

Then, after ensuring he had the information he needed at his disposal, Lincoln acted boldly. He decided to resupply the fort.

This strategy turned out to be the best one possible. The Confederate army did indeed react by attacking the fort, just as Lincoln had predicted. But because they attacked first, they — and not Lincoln — were viewed as the aggressors. By acting in this careful but bold manner, Lincoln had both resupplied the fort and not only saved his own political position but strengthened it.

This example shows one of Lincoln's greatest strengths as a leader. Rather than rush into making decisions, he ensured all his options were first carefully considered, allowing him to formulate the most intelligent and decisive strategies. This was especially vital during wartime, when a leader can be faced with many difficult decisions. In no small part this ability helped Lincoln and his administration to gain victory in the Civil War.

**Lincoln always carefully considered his options, meaning he was able to successfully take intelligent and bold action.**

### 5. Trial periods give new personnel the chance to display their initiative – and those found wanting can be quickly removed. 

The demands faced by modern leaders don't allow them to be everywhere at once. Business leaders, for example, need to attend meetings, see shareholders and work on strategic planning. They don't have the time to manage every detail of their business. They need help, and the best form of help is independent-minded staff who can use their own initiative, leaving leaders free to do their own jobs.

Yet, how do leaders find these vital members of their staff?

The best leaders can employ a modern technique known as a _honeymoon period_ to gauge their staff's effectiveness and initiative. Staff are given a period, usually between three to six months, in which to display desirable qualities such as initiative. If they don't demonstrate these qualities, then they are moved on, and new staff are awarded the chance to fill the role. This gives personnel the chance to prove themselves, while ensuring that ineffective staff aren't allowed to block the path of better ones.

Once again, Abraham Lincoln provides us with both an example of using independent-minded staff and also just how to find them.

He knew he needed staff who were comfortable using their own initiative, and he discovered such a person in General Ulysses S. Grant. Lincoln found Grant after using the principles of the honeymoon period to discard several ineffective generals. In Grant, he found an aggressive and self-motivated fighter, and someone whose initiative even propelled Grant to find his own like-minded generals. His decisive victories were critical to Lincoln's success. Had Lincoln not used the honeymoon period to find Grant, he could have been stuck with ineffectual generals, making victory that much harder.

**Trial periods give new personnel the chance to display their initiative — and those found wanting can be quickly removed.**

### 6. To exploit technological breakthroughs, Lincoln sought out innovations, implemented them and encouraged new ones. 

Throughout his presidency, Abraham Lincoln was captivated with technological innovation. In fact, he is the only president of the United States to hold his own patent: a method of lifting boats over obstructions in the water. His fascination in this area was born from the fact that he knew the success of an organization depends on its ability to seek out technological breakthroughs.

In order to find and exploit these breakthroughs, Lincoln ensured that his administration remained at the forefront of innovation and technological advances.

For example, Lincoln was methodical in his search for new technologies and innovations, organizing and even personally attending demonstrations of new technology around Washington. He stopped at nothing to find advancements that could give the Union the advantage in the Civil War. This led him to evaluate diverse technologies, from hot air balloons — for use in spying — to flame throwers and even rockets.

He also saw to it that new technologies were quickly implemented, even going so far as to override his generals in the introduction of crucial new weaponry. For example, he personally tested and ordered the introduction of a new type of rifle, despite the concerns of military commanders. In the end, this made an enormous contribution to his army's eventual victory.

In addition to his direct approach to innovation, Lincoln made sure to provide a platform for others to come to him with new ideas. He held open office-hours to allow businessmen and inventors to visit him with new proposals, such as new weapons, to help the war effort.

By entertaining proposals, researching, and developing cutting-edge technology, Lincoln fostered technological development, which helped him lead his administration to ultimate victory.

**To exploit technological breakthroughs, Lincoln sought out innovations, implemented them and encouraged new ones.**

### 7. Managers must carefully balance the need for change and flexibility with stability and consistency. 

Modern business leaders are confronted by a difficult dilemma. Elements of their business, such as technology and customer tastes, constantly evolve, so businesses must react and adapt if they hope to succeed. However, making too many changes too quickly can overwhelm the employees and damage productivity. Therefore, there needs to be a careful balance between being flexible and maintaining consistency.

Great managers are aware of this need for a careful balance and the ways to maintain it. The key to doing this is to first ensure a level of consistency in the organization, such as consistency in what is expected of each member of staff. A member in the sales team, for example, will feel more comfortable when they know what level of sales they should aim for, and that this level won't be raised suddenly.

Putting this consistency into action builds a strong and stable base in the organization, which in turn provides a launch pad for radical change. Leaders can be more flexible to change knowing that their staff are comfortable with their roles and workloads.

Abraham Lincoln provides a great example of how a leader can achieve this balance between consistency and flexibility. He took the US government through one of the most traumatic periods in its history. Throughout the long and bitter Civil War, Lincoln had to constantly make radical changes, like abolishing slavery, to transform a civilian administration into a war-winning machine. He did this by being consistent in his actions. For example, when he promoted his staff, it was always based on the same criteria and levels of performance, and they always knew what was expected of them, whatever task they were doing.

By acting this way, Lincoln managed to successfully make the changes in his government that helped him win the war.

**Managers must carefully balance the need for change and flexibility with stability and consistency.**

### 8. By forgiving the mistakes of both his staff and his enemies, Lincoln built up levels of trust in his administration. 

In the American Civil War, the punishment for desertion from the Union Army was death. Yet, many young men who ran away because they could no longer bring themselves to fight were saved from execution by Abraham Lincoln. In pardoning their desertion, he set an example to his followers and enemies alike that he was prepared to forgive their mistakes. In fact, over the course of his presidency, Lincoln granted more pardons than any other president in US history.

Lincoln's forgiving and lenient image built trust and honesty in his administration. He hoped this would not only encourage his supporters to keep taking risks but also send the message to his enemies that it was safe to surrender.

An example of how Lincoln motivated his staff in this way can be seen in how he was prepared to forgive his generals. He would frequently take personal blame for unsuccessful military operations, thus taking the pressure off his generals. This encouraged them to continue pursuing bold and aggressive strategies, safe in the knowledge that they would be forgiven and protected from adverse criticism if they failed.

Using this approach to his enemies also brought rewards. For example, toward the end of the Civil War, a group of confederates approached Lincoln to ask if they would be executed for treason. True to his reputation, Lincoln reassured them they would remain unharmed as long as they promised to stay faithful in the future. In acting in this way, Lincoln was able to steer his confederate adversaries back toward his leadership and help heal the biggest rift in US history.

**By forgiving the mistakes of both** **his staff and his enemies, Lincoln built up levels of trust in his administration.**

### 9. Final summary 

The key message in this book is:

**Abraham Lincoln embodied leadership by quietly guiding the direction of every division of his administration on a daily basis. Through the use of persuasion, excellent communication and full assumption of his powers, Lincoln spread his vision throughout his organization. Lincoln created the strongest staff possible through trial and error, emphasizing positive reinforcement and integrity, resolving conflicts creatively, and continually encouraging innovation. In doing so, he ensured that his organization had the strength to carry out his vision.**

Actionable ideas from this book in blinks:

**Get Involved**

The best way to manage an organization is to get out of the office and get personally involved. Spend time with your employees, get feedback, reinforce your vision in person and convene random meetings. As a leader, interact with every department of your organization. This way, your vision will be carried out under your supervision.

**Try Out Employees for Positions**

Give new employees a trial period to prove they can excel on their own. If they can't, transfer them somewhere they can be more effective, and find someone better for the job. If, however, they can, then you've found a dependable employee you can rely on while giving yourself more time to focus on your own responsibilities.

**Master Communication**

Perfect your communication skills. A great speech or letter is the best way to remind employees of your company's mission, inspire them to carry it out and let them know how to do so. Silence, or the art of knowing when not to speak, is an equally important form of communication; sometimes speaking doesn't achieve anything.
---

### Donald T. Phillips

Donald T. Phillips is a best-selling non-fiction author who has written over 20 books. As part of the trilogy that includes _The Founding Fathers on Leadership_ and _Martin Luther King, Jr. on Leadership_, _Lincoln on Leadership_ helped establish the historical leadership genre. Phillips' leadership experience includes three terms as mayor of Fairview, Texas.

