---
id: 5489dc7933633700094d0000
slug: live-it-en
published_date: 2014-12-19T00:00:00.000+00:00
author: Jairek Robbins
title: Live It!
subtitle: Achieve Success by Living With Purpose
main_color: 459CA2
text_color: 35787D
---

# Live It!

_Achieve Success by Living With Purpose_

**Jairek Robbins**

_Live It!_ (2014) offers a step-by-step guide to helping you discover what your ideal life looks like and how to make it a reality. All too often we turn away from our dreams, never taking the first step toward changing our lives for the better. This book focuses on what you need to get what you want, and how to avoid obstacles and challenges along the way.

---
### 1. What’s in it for me? Learn how to look back on a rich, fulfilled life with no regrets. 

At the end of life, we look back and think about the decisions we've made, the paths we've chosen and the people we've loved. 

Yet only a few of us, when that time comes, will be able to say honestly that we've lived our lives fully and made a positive difference. Regrets, and opportunities lost, will weigh on the rest of us.

You can avoid regret by living life now. With a few simple tricks, you can become the person you've always wanted to be and live the life of your dreams.

These blinks will show you how to create a vision for your life that empowers you and keeps you focused on your path. Importantly, you'll learn how to deal with doubt, criticism and setbacks along the way.

In the following blinks, you'll also discover:

  * how you can improve your life through showing gratitude daily;

  * how your life can be compared to a marathon foot race; and

how an experiment with cups of rice showed the power of emotions.

### 2. Start building your vision. Imagine what a perfect day in your ideal life would look like! 

Ever tried finding a restaurant but ending up baffled and frustrated by too many choices, mainly because you couldn't pinpoint exactly what you felt like eating?

It's common to feel this way about life in general. To live the life you really want, you can't be fuzzy about your aspirations; you need a clear vision of your future.

Those who can picture their future don't need to wait to see what life deals them; they proactively create their lives in accordance with their desires.

Look to Malala Yousafzai for an amazing example. Even though she was shot by Taliban members for her beliefs, she refused to give up the fight for educational rights for girls in Pakistan. Her vision of changing women's lives for the better was too compelling for her to just stop.

It's essential that you find a clear vision for your life. To do this, think about what a perfect day would look like to you.

When we think of a perfect day, relaxing images of vacation resorts might spring to mind. But what this really is about is crafting a day that contains everything that is truly important to you — things that'll help you reach your goals.

Picture a whole day, from sunup to sundown. Where are you? Who are you with? What are you doing? Are you waking up next to your partner, having breakfast with your children and then going to work at your own company? Or are you in a house next to the ocean, settling in to spend the day writing your novel?

Include all your senses while imagining your day: what do you smell, hear, see and feel?

This exercise not only gives you a better idea of what you truly want, but also gets you so enthused and motivated that you'll want to start moving toward your goal immediately.

Now that you have a better sense about your ideal life, let's start taking steps to achieve it.

### 3. Instead of being distracted by frivolous details, focus on the things that bring lasting happiness. 

Did you know the average American watches over five hours of television every day? This is a huge chunk of time that could be spent on more fulfilling activities.

The trouble is, there seems to be a massive discrepancy between what we think is important and how we actually use our time.

We tend to spend a lot of time on senseless things, like excessive TV watching and shopping, that only serve to distract us — they don't contribute to our happiness.

It's the significant events in our life that are responsible for long-term happiness, but often they just don't crop up in our daily thinking.

Imagine your life is a house: the majestic granite terrace and the well-manicured garden (minor things) are all very lovely, but if the plumbing and electricity (major things) don't work, it would be pretty unpleasant to live there.

If you want to live fully, pay attention to the major aspects in your life. There are several primary areas to consider:

Your number-one priority should be health. Health determines how you feel and the activities you can participate in and enjoy.

Emotional intelligence is also essential. Well-developed emotional intelligence helps you handle what happens to you every day. Rather than being a slave to your reactions and emotions, you can start proactively guiding them.

Other major areas to prioritize are family, friends, profession, finances and spirituality.

Try the following exercise: rate the current state of the above aspects in your life from 0 (none or very poor) to 10 (perfect) and compare these numbers to your vision of a perfect day.

Through this, you'll discover which areas in your life need the most care and attention.

> _"Have you ever noticed that people often sacrifice their health to make money, and then sacrifice their money to try to improve their health?"_

### 4. Identify what drives you forward and channel your emotions to strengthen your motivations. 

If so many of us know how to eat healthily, why are obesity and diet-related illnesses increasing?

It's because there's a difference between knowing what is right, and actually doing it.

Why do we have trouble with this?

When we don't do what we know is good for us, it's because we're lacking intrinsic motivation. The first thing you need to do, therefore, is discover your motivations — your reasons for your actions.

Take work, for example. Some of us work because we want to be able to afford a good education for our children, while others choose to migrate to another country for a more peaceful lifestyle.

Recall your ideal day: who was with you and what did you do? Your answers will help you define your motivations.

Once you recognize your motivations, you need to turn them into action. A great way to do this is to connect these ideas with your emotions.

Emotions are central to all the decisions we make. We decide for or against something to either experience pleasure or to escape pain.

Let's say your alarm wakes you up early. The temptation to stay in bed is strong, because it's warm and cosy. On the other hand, you remember you have an important meeting and if you're late, it will negatively impact your job.

In this situation, the desire to avoid the negative outcome is emotionally more potent, so you decide to get out of bed.

The good thing is, you can use these emotional drives to guide you toward your goals.

Do this by imagining how you'll feel at the moment you reach your goals. Feel the pleasure it will give you and notice what discomfort you'll avoid. Keep these emotions in mind as much as possible. They can be a powerful impetus for your plans.

Having motivation is crucial to living your dream life. But understanding your motivation isn't enough. Your motivations need to manifest into action.

> _"Every person you meet in life is either a warning or an example: an example of what to do or a warning of what not to do. Which one are you being right now in your life?"_

### 5. Don’t let others tell you how to live your life. Only you know what will make you truly happy. 

We are social beings, and interacting with others and living in line with our communities' expectations is a basic value we all share.

So it's a given that people around us will try to influence our decisions, and will often disagree with what we choose to do with our lives.

Imagine you want to take some time out from college to spend a few months volunteering in Africa. You're so passionate about your idea that you share it with others.

Then the doubts and criticism start. Your family tells you to stay and finish your education, while others suggest that it's time to start thinking about having a family.

In these moments, remember that other people's perspectives are always going to be different than yours. People might question your plans because your ideas conflict with their own views, or because they simply are worried about what could happen to you.

It's normal to question yourself and consider a change of plans when you are faced with such reactions. Therefore, it's essential you find ways of coping with doubt.

Always bear in mind that a successful life doesn't have a single definition. While staying in your home town and getting married at 21 might sound terrible to you, this path might make another person extremely happy.

A common reaction is for others to say that you don't have the time or ability to follow your dreams, but know that you can always find a way to learn the necessary skills.

Yet commitment and persistence will play a far more important role in reaching your goals than hard skills. When you persevere, you'll be more likely to succeed — and you can show others that you are in fact capable of achieving your goals.

The author J.K. Rowling is a great example of perseverance. Desperate for money and on the brink of losing her apartment, Rowling was rejected by 12 publishers before one finally agreed to publish her first "Harry Potter" novel.

> _"Everyone likes to be acknowledged, so we become programmed to live in a way that pleases others."_

### 6. Live your dreams by writing down what you want to do, and the steps you’ll take to get there. 

Guess what? You're not going to live forever. So why do you keep postponing your dreams to play out at a vague, uncertain time in the future?

To avoid regrets when it comes to the end of your life's road, you need to set your priorities while you still can.

Start by striking a balance among your tasks. Too often we prioritize urgency over importance.

For example, think of a father regularly missing family dinners to attend last-minute business meetings. The meetings deal with pressing matters, sure, but hardly provide long-term satisfaction. Yet the dinners, although not urgent, are essential for long-term happiness.

Making the most of your life also involves being grateful for what you have. You may be the picture of health right now, for example, but this could change.

Research has shown that being grateful actually improves your health: those who appreciate the good things in life every day are healthier and less likely to suffer from anxiety and depression than others who dwell on things that give them no happiness.

A concrete way to organize your priorities is by writing a _Live it List_.

Like a bucket list, it's a list of all the things you want to do and places you want to visit in your life. There is one difference, though. Unlike a bucket list, the Live it List takes lofty goals and makes them bite-sized, easily achievable pieces.

Say that your dream is to visit Barcelona. You would not only write "Barcelona" on your Live it List, but you would also write down the small steps you might take to make this trip a reality.

These steps could include learning basic Spanish, renewing your passport and putting aside a specific amount of money every month.

By focusing on one small step at a time, you'll steadily move closer and closer to your goal.

> _"Over the years, it's easy for people to slip into autopilot and get stuck doing things they think they need to do versus things that they have always wanted to do."_

### 7. The power of positive thinking and mental preparation can help you reach your every goal. 

Now that you have a clear vision for the future and strong motivation, you'll still need some other tools before everything can fall into place.

It's likely you'll face moments when nothing seems to go right, and progress stalls. During these times, it's vital you remember to think positively.

It may seem somewhat absurd, but how we think about an event or a situation has been shown to affect its outcome.

One famous study was carried out by researcher Masaru Emoto. He filled three cups with an equal measure of cooked rice and water, and directed different emotions to each cup for 30 days. He gave positive thoughts to the first cup, negative emotions to the second and ignored the third completely.

Emoto noticed that after the period was over, the rice in the first cup had started to ferment and smelled sweetly, while the rice in the other cups had started rotting.

Your thoughts function like a self-fulfilling prophecy. If you stand by yourself and your dream, you can make it a reality.

Staying focused isn't always easy, though. To strengthen your positive thinking, you need to practice.

One technique involves living something in your mind before you actually do it, and imagining all the details, emotions and feelings that you'll experience once you achieve your goal.

Say you're nervous about a business meeting. Take some time the night before the meeting to mentally relax and visualize the situation in as much detail as you can.

How do you enter the room and greet your colleagues? What do you say, hear and see? How do you feel once the meeting is over?

This mental rehearsal enables you to practice different scenarios and helps you feel more confident when the actual situation comes about.

Never underestimate the power of your thoughts. What you think can have a huge effect on your success!

### 8. Identify the beliefs that hold you back from your dreams and turn them into empowering thoughts. 

Now you know how thinking positively can help you get the results you want — so what's stopping you from getting out there and living your dreams?

The trouble is, many of us find it hard to shake off our negative, limiting beliefs.

One exercise to pin down what you really believe is to picture your eightieth birthday. How will you feel, and what will you have accomplished by then?

Whatever you see is the result of the path you're taking right now. If you're sluggish and weak, than this is probably your future. If you're energetic and active, then chances are that's how you'll be.

If you're holding limiting beliefs about your future, it's likely you're doing it in your current life, too. For example, how often have you not taken action because you thought you were too old or not talented enough?

To enable focused, consistent action, you need to exchange your limiting beliefs for _empowering beliefs._

Rather than focusing on what you think you can't do or have, focus on what you can do! Use positive thoughts as a mantra, by telling yourself "I have done that once, I can do it again" or "I know how to do it."

These mantras hold remarkable power. Think back to President Obama's campaign in 2008, and how his slogan, "Yes, we can" motivated and inspired people all over America.

The best way to use this technique is to concentrate on one thing at a time, instead of using it for everything at once.

Despite what everyone says, we're really not made to multitask (when we work like this, we're actually just switching quickly from one task to another).

So stay focused and work on one task at a time!

### 9. Finalize your future vision. Draw the roadmap for your life today and 20 years down the road. 

If you want to drive from Los Angeles to San Francisco, the smartest thing to do would be to look up the highway to San Francisco on a map before you leave, and refer to the map during the trip.

The same applies to your life. You can only achieve your goals if you have a clear, detailed roadmap of your future.

Think back again to your vision of an ideal day. This time, stretch your vision from just one day to the next 20 years.

Where will you be, and who is with you? What are you doing? How do you feel? Consider each of the major areas of life; your health, your finances and your relationships.

To see this clearly, it's best to break each aspect into a few subgoals.

See your life's goal as the finishing line of a marathon, and your subgoals as the milestones along the way.

For each major goal, think about what steps you can take this week, this year and in the next five years to move closer to achieving the goal.

If you picture living in a tropical country in 20 years, you could spend a little time this month researching which country interests you the most, and sign up for a language course perhaps later in the year.

Then your goal in the next five years could be to visit the country to see if it's really where you'd love to live.

Along the way, it's always important to measure your progress and adjust and update your vision.

Think of it like an out-of-tune guitar; you don't have to throw it away! All you need to do is change or tune the strings to make it work again.

### 10. Final summary 

The key message in this book:

**With a clear vision for your future, strong motivation and an understanding of your priorities, you'll see that what you believed to be impossible — living your ideal life — can absolutely come true. It just takes some persistence, commitment, positive thinking and a strong belief in yourself to get there.**

Actionable advice:

**Practise gratitude.**

Note down at least three things every day that you are thankful for. Gratitude not only helps us recognize all the good in our life and lifts our mood, but it also helps us stay physically healthy.

**You** ** _can_** **find the time.**

There's no excuse for why you don't have the time to begin building your dreams; you can always carve out time _somewhere_ in your day. Say you need to pick your kids up from sports practise twice a week. Why not split this responsibility with another parent, so that you can grab a couple hours to work toward a personal goal?

**Don't change to please others.**

It's a cliche, but so many of us fall prey to constantly changing our behavior in different social contexts. In the extreme, it can cause unhappiness and anxiety when we're pretending to be someone we're not. Be proud of the person you are and the desires and preferences you have.

**Suggested further reading:** ** _Awaken The Giant Within_** **by Anthony Robbins**

_Awaken The Giant Within_ argues that, ultimately, we're all in control of our own lives, and that by changing our habits, controlling our emotions and believing in those things we want to believe, we can make our ideal life a reality.
---

### Jairek Robbins

Jairek Robbins is a performance and lifestyle coach. The son of internationally renowned coaching pioneer Tony Robbins, Jairek was given the Gold Congressional Award, awarded to young Americans to recognize service and initiative, from the US Congress. He's a regular speaker at seminars and conferences, and _Live It!_ is his first book.

