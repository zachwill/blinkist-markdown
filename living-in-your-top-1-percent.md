---
id: 53c637e23839340007bc0000
slug: living-in-your-top-1-percent-en
published_date: 2014-07-15T00:00:00.000+00:00
author: Alissa Finerman
title: Living In Your Top 1%
subtitle: Nine Essential Rituals to Achieve Your Ultimate Life Goals
main_color: 5FB8D7
text_color: 3D768A
---

# Living In Your Top 1%

_Nine Essential Rituals to Achieve Your Ultimate Life Goals_

**Alissa Finerman**

In _Living_ _In_ _Your_ _Top_ _1%_, author Alissa Finerman outlines nine key lessons for reaching your full potential and becoming your best possible self. Through scientific studies and real-life examples, you'll find out how you can overcome obstacles and attain your goals.

---
### 1. What’s in it for me? Unlock the best possible you. 

Probably almost everyone feels like they have the potential for greatness within them.

And you know what? They're right.

Sadly though, many people never reach their full potential since they lack the tools to do so. Instead, they lead their lives as mere mediocre versions of who they could be.

These blinks will outline how to realize your own inherent potential and become your best possible self. You'll discover concrete tools that will help you identify your priorities, set goals in line with those priorities and then attain your goals through consistent work.

In these blinks, you'll also discover:

  * why you shouldn't worry about your weaknesses but rather leverage your strengths,

  * why Walt Disney was once fired from his job at a newspaper and

  * how Arnold Schwarzenegger succeeded in not one but four very competitive areas.

### 2. Take responsibility for your life. 

You're probably eager to start your journey to becoming your best possible self, but first, let's take a breath and look at _you_.

Start by identifying the _core_ _areas_ of your life. Look at where you are and what you've done thus far; and with this in mind, think about which things in life are important to you.

Your core areas depend mostly on your personal preferences. They can include things such as your career, your health, your financial situation or your relationships. The main thing is that you trust your gut and be honest with yourself about what those preferences are. Don't, for example, fool yourself into chasing ambitious career goals if you really only care about having a stable family life with lots of time for your stamp collection.

Once you have your list, rate each core area from one to ten, based on how happy you are with each area right now.

The ranking will show you what your priorities are, and which areas are most in need of intervention. But before you start working on them, it's important to prioritize _yourself_.

Today, many people are willing to bend over backwards to help others, but continuously ignore their own needs. But sadly, if you don't make yourself a priority, no one else will either.

Just as flight attendants tell you to put on your own oxygen mask before helping others, you should make sure to first help yourself, so that you can thereafter properly help others.

So how can you prioritize yourself? Make a list of seven actions — that take less than fifteen minutes to complete — where you make yourself a priority (i.e., reading a magazine). Over the next week, complete one action a day.

Both these factors — knowing what is important to you and making yourself a priority — are key prerequisites for both personal and professional fulfillment.

> _"Many people define success by being in the top 1 percent, when in reality it's about living in your top 1 percent."_

### 3. Make your mindset a successful one. 

When you try your hand at something new and fail, how do you react?

This question is explored in the book _Mindset_ by Carol Dweck, a psychologist at Stanford University. According to Dweck, there are two distinct types of mindsets.

People with a _growth_ _mindset_ think of life as a learning process, in which they can improve their abilities. They believe that their skills are not fixed, so they seek out new things to learn and thus grow as individuals. They say things such as, "If at first you don't succeed, try, try again."

Yet people with a _fixed_ _mindset_ believe they are stuck with whatever abilities they already have, and no amount of practice will change this. They say things such as, "Basketball is just not the right game for me."

Why is this important?

Because people with a growth mindset are far more likely to succeed in life than those with a fixed mindset. Therefore, you to should try to actively adopt a growth mindset, by staying positive and optimistic.

How?

Focus on positivity, for example, with a simple exercise that encourages you to use more positive words.

Set up a jar in which you place a dollar every time you say something negative, whether a word or statement, such as "I can't" or "Today was a disaster." At the end of each month, donate the money to a charity of your choice.

This should help you think more positive thoughts, which in turn trigger more positive emotions.

But overcoming negativity is no easy task. Psychologist Barbara Fredrickson discovered that for a person to "flourish," or have high levels of mental well-being, that person needed to experience roughly three positive emotions to neutralize just one negative emotion.

Thus you cannot simply be _slightly_ more positive than negative — you really have to tilt the tables.

> _"Change your words, change your life."_

### 4. Focus on your strengths and ignore weaknesses – you’ll feel great! 

We all have our weaknesses, and many of us think we should devote a lot of effort to fixing them. But in fact, it would be more productive and much less of a chore to focus on our strengths, working with what we're already good at — for example, at work.

This strength-based approach is an important prerequisite for personal excellence. This was shown through decades of research by the Gallup Organization on how people can excel and be their best every day.

Gallup found that people who use their strengths daily are six times more likely to feel engaged in their jobs and three times more likely to report an excellent quality of life than people who don't.

Using your strengths regularly is also advocated by the positive psychology movement, founded in 1998 by psychologist Martin Seligman. His research indicates that people tend to be happier when they use their strengths on a regular basis, rather than focusing on fixing their weaknesses and improving what they're not so great at.

The amazing thing about using your strengths is that it can cause a virtuous cycle. The positive emotions you get from using your strengths encourage you to be creative, which in turn helps you build new skills and relationships — yet more strengths for you to use!

So you might be wondering: What exactly are my strengths?

Getting a comprehensive view of your strengths is important, so consider using these online tools to map them properly.

The _VIA_ _Survey_ _of_ _Character_ _Strengths_ is a free online assessment tool, with 240 questions to help you identify your top-five strengths. It can be found here:[ www.authentichappiness.com](<http://www.authentichappiness.com/>).

Gallup uses _Strengthsfinder,_ another assessment tool to help identify a person's strengths. To access this tool, you need to purchase the book _StrengthsFinder_ _2.0_ by author Tom Rath for a registration code.

### 5. Go outside your comfort zone, but don’t stray too far. 

Every mentor, life coach and self-help author says the same thing: you have to leave your comfort zone to challenge yourself and develop as an individual.

But what may be surprising is that you shouldn't stray too far.

Why?

Because there are three different zones of comfort to consider:

The _comfort_ _zone_ refers to actions you are the most comfortable taking. Here, both the level of stress and mental stimulation are low.

In the _stretch_ _zone,_ things are a little less comfortable as you feel increasingly challenged. Stress levels are still manageable, however, and actually keep you sharp and focused. As a result, you learn and grow.

In the _stress_ _zone,_ your challenges constantly overwhelm you, and can't do your best as you're simply too stressed. Because you're in survival mode, you don't learn and soon become exhausted.

So which zone do you think is the right one if you want to keep growing as an individual?

You guessed it: the _stretch_ _zone_.

You should try to figure out exactly where your stretch zone lies and then get in there as often as possible. But how do you do this?

Try scheduling your priorities: If getting in shape is a priority for you, adhere to a workout schedule. Ideally, get some friends to join you, so peer pressure will keep you from skipping workouts.

But you also need to mind your stress levels. One way to do this is a simple productivity mantra: "Do it, dump it or delegate it." If something is important, do it yourself. If not, forget about it. And finally, if someone else can do it, delegate the task. This will help you avoid taking on so much that you slide into the stress zone.

### 6. Set the right goals and begin with small steps. 

If you don't yet have clearly defined goals in life, it's time to make them.

Why?

Having a goal to strive toward is good for you. Studies show that people who have a goal that is personally significant are happier than people who don't. This is because goals motivate, give hope and once they're reached, boost confidence as you realize: "Hey, I did it!"

Arnold Schwarzenegger, for example, always set extremely ambitious goals for himself, and as a result was successful in four highly competitive areas: acting, bodybuilding, politics and real estate.

So how do you set goals the right way?

One common approach is to set goals that are SMART: Specific, Measurable, Attainable, Realistic and Timely. For example, "Every Tuesday at 2 p.m., I am going to do thirty push-ups."

Just make sure your goals are in line with your personal priorities so that you'll be motivated in the long run. For example, if a sense of adventure in life is a priority for you, then you could set a goal to organize every year four long hikes with your family.

Once you've set your goals, start taking _small_ _steps_ toward attaining them.

Why small? Because each step will be easier to complete and thus help you get started as well as build confidence. You won't feel overwhelmed if you take it one small step at a time.

The Japanese refer to this approach as "kaizen," meaning making continuous, small improvements that have a big cumulative impact.

So define the right goals and plot small, manageable steps to attain them. After all, you wouldn't try to read a 400-page book in a single sitting. You'd probably read about twenty pages a night, and in this way, you'd be finished in under a month without once feeling overwhelmed.

> _"Quantum leaps occur by breaking down your big ideas into small steps."_

### 7. To make progress, choose a course of action and pursue it consistently. 

Once you've set your goals, it's time to take action to attain them. If you want to become healthier, for example, you need to start exercising!

You'll probably quickly realize that you have lots of paths toward achieving your health goal. You could start running, hiking, biking, swimming, shooting hoops, lifting weights and so on. So how do you choose the right course of action for you?

Some options have a bigger impact on your health and fitness than others, so naturally it's important to be selective about where you invest your time and energy.

To properly evaluate your options, look at the goals you've set and ask yourself: Would this course of action really help me reach my goals? Is there an even better way? This kind of critical examination should help you decide which action to take.

Of course, whatever choice you make, it will bring some results. The only way not to get results is to sit back and do nothing.

In addition to choosing the right actions, another key determinant of reaching your goals is _consistency_.

Arnold Schwarzenegger didn't become a world-famous bodybuilder by working out every now and then. He adhered to a consistent and rigorous workout regimen for years to achieve his goals!

So how can you become consistent in your actions so that you reach your own goals?

One way is to tell a friend or your partner about your goals over the next thirty days, and the specific actions you'll take to reach them. Ask them to hold you accountable by asking for status updates and punishing you if you fail. This way their watchful eye will keep you moving forward!

### 8. Obstacles are a part of life, and you’ll have to overcome them to reach your goals. 

Nothing meaningful in life can be achieved easily. So don't delude yourself into thinking that you can reach your goals and become your best possible self without putting in lots of hard work and overcoming a few obstacles on the way.

What's important is that you shouldn't let a few bumps discourage you from your quest. Adversity is just an inevitable part of your journey to success.

Consider Thomas Edison: He ran over 2,000 failed experiments before he succeeded and invented the light bulb. Imagine if he had given up after the first 1,000 failures!

Obstacles are just a part of life, which means that success can be yours only if you have the perseverance to overcome them.

So how can you do this best?

By _redefining_ your obstacles. Understand that you're not the only person facing roadblocks — everyone encounters them, so you just have to deal with them as effectively as possible.

One method is to start looking at solutions, instead of just seeing the obstacle. Whenever you encounter an obstacle, write down three possible solutions for overcoming it. For example, if your boss doesn't give you that promotion this time, write down three things you could do better next time around.

This approach prevents you from being discouraged, as while you can identify the obstacle, you're already taking the first steps to overcoming it.

### 9. Become resilient in the face of setbacks and keep going in spite of them. 

The Chinese philosopher Confucius once said, "Our greatest glory is not in never falling, but in rising every time we fall."

He was right. _Resilience_ — the ability to bounce back from setbacks and keep going — is a key determinant of success.

Consider that even Michael Jordan was once cut from his high school basketball team, only to later become the greatest basketball player of all time. Or that Walt Disney was once fired by his newspaper editor for his supposed lack of imagination and good ideas.

The importance of resilience can also be seen in a study by psychologists Emily Werner and Ruth Smith, in which for over 30 years, they followed the lives of 689 high-risk children born in Hawaii. They wanted to understand why some of them became successful despite facing challenges such as poverty, abuse, lack of parenting and alcoholism. And the result? The children who succeeded did so thanks to having built resilience.

Luckily for you, resilience is a quality that can be trained and strengthened.

One useful exercise is composing a list of important life lessons learned as a result of mistakes you've made, and then thinking about how those lessons have helped you. This will make you understand that failure is an important and beneficial part of life, and you'll find yourself bouncing back from future failures far more quickly.

But the most important way to train resilience is to invest time in building and strengthening your relationships with your community, friends and family. Research shows that people are more resilient to stress and get sick less often if they have a supportive social circle.

### 10. Maintain a conscious balance of all the things that are important to you. 

We spend our lives juggling our time and attention among things that are important to us, such as work, family, friends and hobbies. We strive for balance, hoping that one or two areas don't overwhelm everything else to the point of neglect.

You probably know some people who are particularly adept at this balancing act. They are the ones who are always busy but still find time to help you move or meet for lunch. But others are obviously completely out of balance: they may have little going on but never manage to return a phone call.

Learning to control the balance in your life is the final factor in becoming your best possible self.

To achieve this control, you have three tools at your disposal.

_Look_ _at_ _the_ _bigger_ _picture_. Understand which actions will lead to which outcomes, so that you never blindly act without considering how it will affect you in the long run. For instance, if you feel like taking on a new project at work, ask yourself, at what cost? Will this new responsibility prevent you from seeing your family, or force you to drop a hobby that's important to you?

_Identify_ _your_ _non-negotiables_. These are things in life — beliefs, commitments and values — that you're not willing to compromise for any reason. One example could be watching every one of your daughter's basketball games. Once you know your non-negotiables, you'll have an easier time defining how you should balance your life.

_Make_ _conscious_ _trade-offs_. There's only 24 hours in a day, and you can't be in three places at once, so you have to make conscious trade-offs. Do you go to your cousin's wedding or work overtime to finish that important presentation? The right answer depends on you, but by making a proactive, conscious choice, you're empowering yourself.

> _"There is a big difference between knowing about a concept and regularly practicing it."_

### 11. Final summary 

The key message in this book:

**To** **become** **your** **best** **possible** **self,** **you** **need** **to** **start** **by** **looking** **at** **yourself.** **What's** **important** **to** **you?** **Then** **it's** **just** **a** **matter** **of** **setting** **meaningful** **goals** **and** **taking** **small** **steps** **toward** **them,** **no** **matter** **what** **obstacles** **or** **setbacks** **you** **encounter.**

Actionable advice:

**Read** **_Mindset_** **by** **Carol** **Dweck** **and** **_Positivity_** **by** **Barbara** **Fredrickson.**

You just learned the nine essential rituals that will help you become your best possible self. To dive deeper into two of the most important and fascinating concepts introduced here, we highly recommend you read the following subject-related blinks:

In _Mindset_ by Carol Dweck, you'll find out why someone with a _growth_ _mindset_ will become more successful than someone with a _fixed_ _mindset_, and how you can instill the former in yourself.

And in _Positivity_ by Barbara Fredrickson, you'll find out how simple tools such as keeping a _gratitude_ _diary_ can help you lead a happier life.
---

### Alissa Finerman

Alissa Finerman started her career in finance on Wall Street before making the bold move to become a professional life coach.

