---
id: 54e3d412303764000a820000
slug: living-the-80-slash-20-way-en
published_date: 2015-02-19T00:00:00.000+00:00
author: Richard Koch
title: Living the 80/20 Way
subtitle: Work Less, Worry Less, Succeed More, Enjoy More
main_color: C52F45
text_color: C52F45
---

# Living the 80/20 Way

_Work Less, Worry Less, Succeed More, Enjoy More_

**Richard Koch**

_Living the 80/20 Way_ shows you how to apply the 80/20 principle, an economic concept which states that the vast majority of results come from a small proportion of effort, to your personal life. With pragmatic, easily applicable advice about how to create more with less, the author encourages the reader to focus on what's important and to think outside the box.

---
### 1. What’s in it for me? Harness the power of the 80/20 principle to create more with less. 

Are you making the most out of your life? Or do you spend your days running around, scrambling to get work done?

What if you could spend your time more productively?

You can, if you follow the _80/20 principle_. It states that 20 percent of your time and resources goes into 80 percent of the results. Thus 20 percent of a record label's artists generate 80 percent of sales; and the world's top 20 percent of people generate 80 percent of its wealth.

These blinks will show you how to use this principle to transform your own life. You'll learn to identify all the places, from finance to productivity, in which living the 80/20 way can simplify how you live and make you happier, too.

In these blinks, you'll discover

  * how one woman turned $5,000 into $22 million by doing next to nothing;

  * why married people are happier than unmarried singles; and

  * why living the simple life is the best way to live.

### 2. Working all day, every day makes you less productive. To achieve more, you need to do less. 

Some 50 years ago, computers could do little in terms of processing, yet were massive machines. But today we carry incredibly powerful computers around in our pockets!

The story of how computers evolved matches a wider trend in human activity: doing more with less. And in fact, the history of agriculture followed a similar path.

More than 300 years ago, close to 98 percent of the labor force worked in the fields, harvesting food. Due to technological advances, today in developed countries only 2 to 3 percent of the working population is involved with agriculture. And yet we grow and harvest more food than ever!

This is the driving force behind _the 80/20 principle_ : the best 20 percent of our efforts produce 80 percent of the results. And by applying this principle everywhere, we can transform our lives.

We tend to believe that if we spend as much time as possible working, we'll be the most productive. Yet if you apply the 80/20 principle, you would realize that the best way to achieve more is to work less!

Say you're a freelancer and you need to get a project done in a week. If you give yourself all week to do it, you'll procrastinate, spending your time fiddling around and not working on what matters.

But if you give yourself only 20 percent of the week to complete the project (just one day), then you'll work diligently and efficiently. The end results will be better, and you'll have more time to spend on other things.

Interested in how the 80/20 way can work for you? Next we'll show you exactly how to apply this principle to the most important areas of your life.

### 3. To follow the 80/20 principle, set a destination, chart a route to get there, and then go for it! 

Let's say you want to take a vacation. First, you choose a destination, and then you figure out how to get there. And then finally, you go!

It's simple, right? Luckily it's exactly the same process you can use to change your life and start living by the 80/20 principle.

So first, pick a _destination_ : Where do you want the 80/20 principle to take you?

Reflect upon your dreams, goals and objectives. Ask yourself, "What's the best 20 percent of my life on which I'd like to focus?"

For example, when Steve, a restaurant owner in Cape Town, decided to start living an 80/20 life, he identified some key professional and personal areas on which to focus.

He decided that the most important things to him were starting a business, focusing on hospitality, and developing his rock music and teaching.

Once you've followed Steve's example and selected a destination, you'll need to find the best _route_, or how to get there.

To do so, take time to figure out how can you concentrate on these areas with the least amount of effort, but with the best results. Remember, there's never just one path: think outside the box!

For instance, if you want to become a better salesperson, you could take lessons, find a mentor, get an entry-level job, and so on. All you need to do is figure out which path will get you where you want to go in the fastest and easiest way possible.

And lastly, remember to actually go!

You need to take action and start moving. But keep the 80/20 principle in mind: You're trying to get the maximum result from a minimum amount of effort.

### 4. Focus your attention on your happiness islands: what is it that gives you the most pleasure? 

Most of us are in awe of time. It is an extremely valuable and scarce resource, and thus we live our lives feeling that we don't have enough time to do what we want.

It may come as a surprise that _time_ is one of the areas that stands to benefit from the 80/20 principle.

Once you commit to producing the best results with the least amount of effort, you need to figure out how to make the most of the time you have.

So whether you're a freelance designer or a postman, if you dedicate yourself to working harder for a shorter period of time, you'll find your work improved and your free time expanded.

But how can you actually achieve this effective use of your time?

You can start by identifying your _happiness_ _islands_ (times when you feel happiest) and _achievement islands_ (times when you are the most creative or productive). And once you've identified these islands in your own life, you can maximize your output by focusing on them.

That's exactly what Richard Adams did. Adams was a mid-level bureaucrat in the British civil service, and his job was boring and hours long. But he did experience periods of happiness outside of work, especially when he told stories to his daughters.

He started to focus on these small islands of happiness. He then wrote down the stories, and they eventually became his best-selling book, _Watership Down_. From small happiness islands in a sea of boredom came success!

So ultimately, when you let go of your notions about the scarcity of time and start working intelligently, you can achieve miracles.

We'll find out more about the concept of intelligence (and how it connects to laziness) in the next blink.

### 5. Use the 80/20 principle to invest in the long term to earn big, or save incrementally to reach a goal. 

As you may know, 20 percent of Americans own over 80 percent of the entire country's wealth.

And many of these wealthy individuals followed the same 80/20 logic to earn their fortunes, in that they realized you can use a small amount of money to produce unbelievable returns.

In short, they relied on the effect of _compounding_ (when profits are put to the task of generating more profit) to grow relatively tiny amounts of money into large fortunes.

You can also take advantage of this financial strategy by investing your money wisely (say, in the stock market) and thinking in the long term. In other words, even though there might be periods in which your investment doesn't increase much, in the long term, it will increase.

For example, at the end of World War II, Anne Schreiber put $5,000 into the stock market and simply left it there. By 1995, some 50 years later, her small investment had turned into $22 million!

Of course, to invest, you need some savings first. And saving needn't be difficult if you do it incrementally. So for instance, you might set up a bank transfer that automatically sends a small portion of your income (consider 10 percent) into a savings account.

That's what Steve and Helen did to realize their 80/20 goal of buying an apartment that cost $60,000. Together, the couple had an annual income of $78,000. At the end of every month, they hardly had anything left, meaning they weren't putting any money aside.

To avoid spending everything, they channeled 10 percent of their pay directly into a savings account. After six years, they had saved $66,000, which allowed them to buy the apartment (and also fund necessary repairs).

### 6. Instead of trying to please everyone, concentrate on the relationships that matter the most. 

In the modern world, relationships matter. But many of us overextend ourselves. In our desire to build strong relationships with everyone, we end up pleasing no one.

Here the 80/20 principle can help, too.

Since 80 percent of our relationship satisfaction stems from just 20 percent of our actual relationships, to lead a happy life, we need to concentrate on the few relationships that actually matter, such as the ones we have with our romantic partners.

After all, this is one of the most important relationships we have!

A study showed that people who scored in the top percentile for happiness were also involved in a romantic relationship. Nearly 40 percent of married Americans say they are extremely happy; meanwhile, only 23 percent of unmarried Americans would describe themselves the same way.

But to have a strong relationship that supports your happiness rather than undermining it, it's important to ensure that you and your partner agree on a few basic values.

Some 50 percent of marriages end in divorce! The best way to avoid this fate is to choose your partner wisely. If you don't agree on the basics from the outset, it doesn't matter how much effort you put into the relationship later on.

For example, when Steve met Helen, he tried to figure out what made her happy. He soon realized what really mattered to her. She wanted him to be home on time; she wanted to be able to rely on him; and she wanted him to support her projects. What's more, she loved surprises.

And so Steve focused on meeting these basic needs to make Helen happy and strengthen their relationship.

### 7. Things will not make you happy. Stress certainly won’t, either. Simplicity is the key. 

You should now have a good idea of how to apply the 80/20 principle to the main areas of your life.

To finish, let's spend a few moments focusing on the principle's most basic aspect:

_When it comes to living a good life, simplicity is key._

But what is a good life anyway? Philosophers have spent centuries debating this question.

One Greek philosopher, Epicurus, came up with an answer that aligns with the 80/20 principle. The basic components of a good life are food, shelter, clothes, friends, freedom and thought.

And that's it! Ultimately, he unknowingly followed the less-is-more strategy by focusing his energy on the most important tasks.

And Epicurus put this philosophy into practice by living a life ordered by the principle of simplicity. He lived with seven friends in a commune. They grew all their food themselves and didn't worry about accumulating wealth. Instead, they spent their time writing books and sharing ideas.

If you want to follow a similar path, cut out everything in your life that doesn't make you happy.

Consider the story of Ann, who was a successful, well-paid account executive in advertising. Stressed out by her job and exhausted, she quit and gave up her huge apartment for a one-room studio.

She started spending all of her time painting, an activity she loved. Although her parents didn't approve of her new life and criticized her decision to quit a secure job, Ann stuck to her path. And eventually, she started to make good money selling her paintings.

So by simplifying her life, Ann managed to find more happiness and success than she could have ever imagined.

### 8. Final summary 

The key message in this book:

**The 80/20 principle says that 80 percent of what we want is generated by 20 percent of what we actually do. In other words, if you figure out what you want and focus on what makes you happy, you'll be able to create more with less.**

Actionable advice:

**Make a list of things that actually matter to you and then focus your energy on them.**

Doing so will prioritize your happiness and fulfillment. And it has an added benefit: When you focus on yourself and what you really want, you stop wasting as much energy worrying about what other people think of you.

**What to read next:** ** _The 80/20 Principle_** **,** **by Richard Koch**

These blinks have shown you how to use the 80/20 principle in various areas of your life, from your hobbies to your finances. But the technique can be used for far more than just developing your personal life — which is what Richard Koch's first book is all about.

In _The 80/20 Principle_, you'll learn how to apply the Pareto principle to a range of different fields, from linguistics to business management. To gain a deeper understanding of this society-defining metric, and to find out where it comes from and what it has to do with the size of goldfish, check out the blinks to _The 80/20 Principle_.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Richard Koch

Richard Koch, a former partner at consulting firm Bain & Co, is highly regarded author, entrepreneur and investor.

