---
id: 5c3d0d4b6cee07000791ad56
slug: locking-up-our-own-en
published_date: 2019-01-18T00:00:00.000+00:00
author: James Forman Jr.
title: Locking Up Our Own
subtitle: Crime and Punishment in Black America
main_color: E58452
text_color: 995837
---

# Locking Up Our Own

_Crime and Punishment in Black America_

**James Forman Jr.**

_Locking Up Our Own_ (2017) takes a look at the US war on drugs in Washington, DC, and its impact on black Americans. It draws on significant drug and gun legislation from the 1970s through to the late 1990s, which shaped policing methods and influenced the targeting of crime in black communities.

---
### 1. What’s in it for me? Learn about the marginalization of black people in the 1970s–1990s. 

The Black Lives Matter movement began on social media in 2013 when the public expressed their outrage over the acquittal of George Zimmerman for killing black teenager Trayvon Martin. But reports of police brutality against black Americans continue to feature heavily in media outlets.

This kind of fight against racial inequality in the justice system reaches back to a time before Twitter and Instagram existed.

Starting from the 1970s, the author takes us through the history of judicial decisions that have had a significant impact on the black community in Washington, DC. Specifically, we will learn how the introduction of and amendments to gun and drug laws assisted in the incarceration of black men. By exploring the flaws and limitations of past actions, we hope to learn and, in the future, adopt methods that will end the disparity between the criminalization of white people and of black people.

In these blinks, you'll learn

  * when black people started joining the Washington, DC, police force;

  * about Operation Ceasefire; and

  * what "the worst thing to hit since slavery" is.

### 2. Washington, DC’s black community helped halt a proposal to ease marijuana laws. 

In 1975, Washington, DC, had a black mayor and a city council primarily made up of black people. Seventy percent of the citizens were black too. That same year, the city made a decision that would stigmatize young black males for decades to follow.

During this period, proposals were made to soften marijuana-related legislation due to concerns about racial injustice.

In 1975, 80 percent of those arrested for possession of marijuana were black. These arrests held a lifelong burden as they had to be reported on housing, schooling and employment applications. Therefore, on March 18 of that year, David Clarke, a member of the city council, proposed the Marijuana Reform Act, which sought to lower the penalties for possession of marijuana to a fine and a citation.

However, the black community, headed by fellow council member Doug Moore, opposed the proposal, arguing that easing penalties would make it easier for black people to succumb to crime and addiction. The opposition was successful, and on October 21, 1975, the Reform Act was tabled.

To help understand why the reform was opposed, we need to look at the heroin epidemic in the 1960s.

In the early to mid-1960s, less than 3 percent of new prisoners at the Central Detention Facility in Washington were heroin addicts. Then came a huge spike in usage, and by June 1969, this figure had grown to 45 percent, the majority of addicts being young black men.

There was a strong link between heroin addiction and crime. To be able to afford their drugs, addicts would resort to criminal means to make money. According to one study, heroin addicts in DC and three other US cities committed an annual average of more than 300 crimes.

The spike in criminal activity led to outrage across black communities, and black drug dealers were deemed to be betraying their race. Some even believed that black heroin addicts and their passive dependence benefited the white community. In May 1969, posters that likened heroin addiction to slavery appeared across DC, printed by the antidrug organization Blackman's Development Center.

### 3. The gun control laws passed in 1975 had mixed consequences. 

While the city council was discussing penalties for marijuana possession in 1975, gun violence was rising. In 1974, it was the number one cause of death for males in Washington under the age of 40.

This spike led to the city council discussing tougher gun-control measures.

Councilman John Wilson suggested that the sale, purchase and possession of all shotguns and handguns be banned and that the maximum sentencing guidelines for people convicted of gun crimes be raised. Furthermore, he proposed that minimum sentencing times be introduced so that more offenders would go to jail.

Wilson's proposals were supported by victims of gun-related crime and citizens who were angered by the fact that, at the time, 85 percent of those killed by guns were black.

Fellow councilman Moore, however, opposed Wilson's suggestions, claiming that guns were crucial for the black community to arm themselves against street criminals and racism-fueled violence.

In 1976, the council passed stricter gun-control laws, with everyone in favor except Moore. Though the new laws weren't as strict as those Wilson had proposed, they nonetheless banned further sales of guns and mandated the registration of existing ones.

Many black citizens were in favor of the new laws, in part due to the perception that black-on-black street crime had become a greater threat to the community than racist violence.

It can be argued that the stricter gun laws were a huge victory for civil rights; black policymakers passed legislation that aimed to protect the lives of black citizens.

Unfortunately, however, the policy change wasn't as effective as many people had hoped.

The new laws and penalties mostly punished poorly educated black men from low-income households, while still failing to protect the larger black community against gun violence. This was likely because of the overemphasis on punishment, rather than addressing the root causes of gun crimes, such as racial inequality in the provision of health care, education and employment.

### 4. Hiring more black police officers didn’t necessarily curb police violence against black people. 

Throughout most of US history, racism has prevented black people from being considered for positions of authority, especially in police departments.

This meant that significant efforts were required to increase the number of black people on the police force.

In the late 1940s, black civilians started to join the police force. However, segregation existed in patrol cars, and black officers didn't have equal career tracks. To apply for a promotion, you needed to have good written test scores and be assigned a high "suitability for promotion" rating by your supervisors. But racism prevented black officers from receiving good suitability ratings, which made it near impossible to get a promotion.

Two black officers, Burtell Jefferson and Tilmon O'Bryant, couldn't obtain a promotion due to their low suitability ratings. So, in 1958, they formed a covert class for black officers, since very high test scores were the only way to compensate for low suitability ratings. After six months, 12 out of the 15 officers in the class scored high enough on the test to receive a promotion.

Though this was an important step forward for black policing, having more black officers didn't really reduce police violence.

For example, in October 1968, Elijah Bennett was shot by a white police officer after he was verbally reprimanded for jaywalking in a predominantly black neighborhood in DC.

Furthermore, black officers weren't necessarily more sympathetic toward black citizens. A 1966 study by the University of Michigan looked at both white and black officers on duty, and though black officers weren't as prejudiced as white ones, 28 percent of them were still classified as "prejudiced" or "highly prejudiced."

One reason for the high percentage was class divisions. Officers tended to perceive poor black people as posing a risk to law and order in black communities. In some instances, that concern turned into overzealousness, which resulted in the use of excessive force for minor offenses, such as loitering and drunkenness.

### 5. Sentences for drug crimes in DC underwent serious changes. 

From the late 1930s onward, all drug crimes in Washington received a minimum one-year sentence, and a repeat offense would get you ten years behind bars. But by the 1970s, drug dealing was rampant in the city anyway.

So the city council passed new drug-related legislation in March 1981.

David Clarke, the chairman of the council's Judiciary Committee, proposed the categorization of drugs into groups with specific penalties. For instance, marijuana dealers would receive a maximum one-year sentence, while cocaine and heroin dealers would get sentences of up to five and ten years, respectively. Clarke's suggestion was a response to contemporary public perception, which viewed the legal system as a revolving door that returned criminals to the streets.

African-American councilmember John Ray opposed Clarke's proposal with a stricter one, which sought to raise the maximum sentences even more, to three, ten and 15 years for marijuana, cocaine and heroin dealing respectively. Ray also proposed mandatory minimum sentences for gun and drug offenses. The decision regarding maximum sentencing fell in Ray's favor; however, the council rejected his minimum sentencing proposal.

Regardless, a ballot in January 1982 established mandatory minimum sentences for drug dealing. _Initiative 9_ mandated that anyone convicted of dealing heroin would get a minimum of four years in prison, while selling cocaine or marijuana would result in two years and one year in prison respectively.

This initiative was strongly supported by Ray, along with police chief Burtell Jefferson, but opposed by a wide range of groups concerned with civil liberties. So Ray and Jefferson cleverly capitalized on the rage of DC citizens over the booming drug market. Some weeks prior to the vote for Initiative 9, Ray and Jefferson campaigned by visiting one murder scene after another, demonstrating to the public that crime was a huge problem in DC. On September 14, 1982, Initiative 9 won by a landslide.

Though the policy had no impact on crime reduction, drug-related prosecutions increased by nearly 300 percent between 1982 and 1984.

### 6. The late-1980s crack epidemic resulted in “warrior policing.” 

By the late 1980s, the war on drugs was well and truly underway, and police were trained as though they were soldiers going into battle. Thus, they began to instinctively see young people from neighborhoods with high incidences of crime as potential enemies who could attack at a moment's notice.

This new and highly aggressive form of law enforcement, referred to as _warrior policing_, was the antithesis of the US Constitution's "innocent until proven guilty" amendment.

To make themselves appear "not guilty," the author's students had to constantly be mindful of their clothing, speech and behavior. Once, when the author advised a student to go over his schoolwork during the commute to school, the student said it would be dangerous if he didn't remain vigilant to harassment or attacks.

Warrior policing came about as a result of the crack cocaine epidemic. Crack cocaine is made by heating cocaine, baking soda and water, and it creates an immediate and intense high that can be highly addictive. In 1984, 15 percent of people taken in by DC cops tested positive for cocaine. Three years later, that percentage had grown to 60 percent, with nearly all of them having smoked crack.

The black community, too, experienced the destructiveness of crack, with a representative from the National Association for the Advancement of Colored People claiming it as "the worst thing to hit us since slavery."

Due to a growing fear of the drug, many members of the black community were in favor of heavy-handed policing. Drug-related violence was up, and drug cartels and heavily armed street thugs were increasing in numbers. And the violence was far from evenly distributed — in 1989, 90 percent of homicide victims in Washington, DC, were black.

### 7. The “stop and search” tactic was more commonly used in black communities. 

By 1995, incidences of violence in DC had started to drop after peaking during the crack epidemic. However, homicide rates were still three times higher than in 1985. On January 13 of that year, Eric Holder, African-American US attorney for Washington, DC, pointed out that 94 percent of black victims were murdered by another black person.

As a solution to rising homicide rates, Holder came up with Operation Ceasefire, which made "stop and search" into official police policy.

The program leveraged the city's numerous traffic regulations, which meant that the police had an excuse to stop almost any vehicle. This allowed them to search cars for illegal weapons to be confiscated. But most studies show that only 1-5 percent of the searches resulted in weapons being seized, meaning that a large proportion of innocent citizens were stopped for no justifiable reason.

Another problem with Operation Ceasefire was that it disproportionately affected black neighborhoods.

For example, the program did not apply to the city's Second District, one of DC's more prosperous, white neighborhoods, due to its low rate of gun violence.

And because police officers would frequent black neighborhoods, black drivers were significantly more likely to be stopped by the police. These stops often resulted in arrests for crimes not even linked to guns, such as drug possession. This despite the fact that black drivers weren't more likely to possess drugs than white drivers.

This variance didn't surprise Holder. He knew from the very beginning that young black men would be stopped more frequently, especially those in poor areas. And this is exactly what happened. Between 1996 and 1997, sociologist Ronald Weitzer found that people living in lower-class black areas were four to seven times more likely to report pretextual stops and police abuse than those living in middle-class black areas.

The "stop and search" tactic was just another component of the war on drugs that only increased the imprisonment of black people.

### 8. Final summary 

The key message in these blinks:

**Legislation enacted as part of the war on drugs in Washington, DC, such as warrior policing and "stop and search" tactics, resulted in higher rates of police violence and incarceration rates that disproportionately affected black people. Even more surprising: the black community itself appealed for stricter penalties for many crimes. But policy changes and new laws since the mid-1970s have not only failed to reduce crime rates; they've also helped to marginalize black people further.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Stamped from the Beginning_** **by Ibram X. Kendi**

_Stamped from the Beginning_ (2016) offers a powerful examination of the modern history of racism in the United States, including where racist ideas originate and how they spread. In particular, the author looks closely at how the presidential campaigns and administrations of Richard Nixon, Ronald Reagan and Bill Clinton have helped propagate racist thought and had a detrimental impact on America's black communities.
---

### James Forman Jr.

James Forman Jr. is an author, professor of law at Yale Law School and the cofounder of the Maya Angelou Public Charter School in Washington, DC. He has written for the _New York Times_, the _Atlantic_ and many law periodicals.

