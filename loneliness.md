---
id: 542050066365640008550000
slug: loneliness-en
published_date: 2014-09-23T00:00:00.000+00:00
author: John Cacioppo and William Patrick
title: Loneliness
subtitle: Human Nature and the Need for Social Connection
main_color: None
text_color: None
---

# Loneliness

_Human Nature and the Need for Social Connection_

**John Cacioppo and William Patrick**

_Loneliness_ explores our basic human need to connect socially with others. The book explains why having a healthy social life is so critical to our happiness, and outlines some serious consequences that can result from prolonged loneliness. It also offers advice on how to better connect with others.

---
### 1. What’s in it for me? Learn why we seek out relationships with others for a more fulfilling life. 

Feeling lonely is familiar to most everyone. Yet did you know that our desire to have a connection with others isn't just about happiness, but that it's actually vital to our survival?

Humans have evolved to feel lonely when we are craving more social interaction, just like we have evolved to feel hungry when we need more nourishment. Like food, meaningful social relationships are critical to both our mental and physical health.

But why exactly are human relationships so important? In the following blinks, you'll learn why we seek to connect with others and what happens when those connections fall apart. You'll also explore some techniques that you can use to break out of a cycle of loneliness.

In these blinks, you'll also discover:

  * why you often want to gobble a lot of chocolate after a relationship breakup;

  * why nurses who work in hospice care are often happy; and

  * how you can defeat long-term loneliness through a series of small steps.

### 2. Being lonely is an evolutionary trait, as our need for companionship kept us safe from predators. 

Did you know that most people prefer to have a circle of good friends rather than lots of money? Sure, we all feel that social connections are important — but why are they exactly?

We strive to connect with others because we do not want to be _lonely_ _–_ that bleak, desolate feeling we have when we are without social connections.

_Loneliness_ is actually a legacy of our evolutionary history. Just like hunger or thirst, we've developed these intense feelings to inspire actions that are necessary for survival. Feeling hungry tells us we need to eat; thirst signals a need to drink.

Loneliness, in turn, tells us we need to reach out and connect with other people.

Being isolated from a tribe or circle of familiar people was dangerous for our early human ancestors. "Safety in numbers" was the rule of the day, to keep predators at bay and ensure children could be raised safely.

Humans aren't the only creatures who need social connections to survive. Wolves form social groups as they can hunt more effectively in packs. They even bring back food for injured pack members that couldn't participate in the hunt, making sure the whole pack thrives.

Nowadays the situation is different. Being alone isn't life-threatening, and many people prefer living on their own. Yet loneliness can still be harmful.

Prolonged loneliness might seem insignificant when compared to the threat of predation, but being lonely can still lead to health problems, such as depression. Social connections thus are just as important now as they were to hunter-gatherer societies in the past.

The following blinks will take a look at the consequences of long-term loneliness and address ways you can prevent yourself from feeling lonely.

### 3. Our need for social connections is partly genetic and partly shaped by our life experiences. 

Why do some people get lonely easier than others? If social connections are so critical for our survival, shouldn't we all feel lonely with the same intensity?

Some people are perfectly happy living on their own and socializing rarely. Others prefer a big family and a hectic social calendar.

The basic need for social connections is the same for all of us, but the differences lie in our individual gene makeup. We _do_ all need social connections, but our genes determine _how much_ we need them.

Thus some people feel lonely only after a short period of time alone, while others may be able to go for long periods of time without feeling lonely. Thus loneliness is a subjective state.

The ways other people see us can also influence our perception or experience of loneliness.

In one study, participants were introduced to a stranger and asked to interact with them. Before the introduction, however, some participants were told that the stranger was "lonely."

Interestingly, participants who were told the stranger was lonely were less social with the stranger. They essentially expected the stranger to be in general less social, so they didn't bother to put in the effort.

We tend to adapt our behavior toward others based on how much we know about them. If we think someone is lonely, we'll probably treat them differently.

Yet if we view a person negatively because they are lonely, and in turn don't want to socialize with them, this creates a vicious cycle. People often turn away from lonely people, only making them feel more lonely.

### 4. Lonely people struggle when dealing with stressful situations; and stress makes them more lonely. 

So you come home one evening and realize you've forgotten your house key, and are locked out. An annoying predicament, to be sure, but not the end of the world — right?

Well, it might be the end of the world if you are a seriously lonely person. Lonely people tend to see a stressful situation as much worse than it actually is.

Most of us can handle a small crisis like getting locked out of the house. In fact, we might even see it as something positive — a chance to visit a friend who has a spare key, for instance.

For people who are lonely, it's not that easy. They might find it difficult to stay calm; they might even resign themselves to spending the night out in the cold.

Lonely people react strongly to stressful situations because they don't have adequate social support in their daily lives. To make matters worse, they also get less joy out of positive experiences as they often can't share their joy with others.

The stress of a lonely person will also increase with time if they remain lonely.

Loneliness affects the way young people perceive their levels of stress. A young lonely person often views her life as more stressful than that of her peers, even if those happier peers have roughly the same amount of stress.

However, if a lonely person continues to believe this as she grows older, her life actually _will_ become more stressful. If loneliness persists over time, older lonely people will suffer from significantly greater stress levels.

Studies have shown that lonely people have a higher divorce rate. They also argue more frequently with friends and neighbors, as it's harder for them to stay calm when they are upset. These tendencies may lead to more loneliness, which leads to more stress.

> _"Loneliness diminishes the feeling of reward we get from interacting with other people."_

### 5. Prolonged loneliness can result in higher stress levels, which in turn can trigger illness. 

Loneliness and depression often go hand in hand. It's important, however, to remember that even though the two conditions are often associated, loneliness and depression are still two very different states.

Feeling lonely should signal to us that we need to change our behavior. It's supposed to motivate us to reach out to other people, to be social. By contrast, feeling depressed makes us passive and keeps us from changing anything.

When suffering from depression, the trigger of loneliness doesn't motivate us to change our behavior. Instead, depressed and lonely people shy away from what actually might make them feel better; they are trapped inside their loneliness.

Loneliness doesn't only lead to mental and emotional problems but can also make us physically ill.

Loneliness accompanied by sleep problems is common. When you're stressed, it's doubly important that you get a lot of rest to help your body handle the stress. Yet studies have shown that even though lonely people may sleep as much as non-lonely people, the quality of their sleep is poorer.

Loneliness also makes us more prone to infection. One study found that students were more likely to get sick during exams because of higher stress levels. However, the study also discovered a difference between those students who were considered lonely and those who had healthy social lives. Interestingly, the lonely students were much more likely to become sick.

Thus maintaining good relationships with people around us isn't just about having something to do at the weekend, it's a crucial part of living a healthy life.

### 6. Feeling lonely can make us lose self-control, which is why cakes and cookies are so tempting. 

Have you ever wondered why you reach for candy bars and cookies immediately after a nasty breakup? Curiously, feeling lonely changes our ability to control our impulses.

Loneliness can lead us to making unhealthy choices in our lives. In one study, participants were asked their opinion on a certain brand of cookie. Yet some participants were made to feel "lonely," as researchers told them that no one wanted to work with them in a paired exercise. In contrast, the other participants got partners and were socially connected.

Participants then were told to eat as many cookies as needed to make an accurate assessment of their taste. The paired participants ate an average of five cookies; while the "lonely" participants ate an average of nine cookies.

What exactly happened? Our brain pushes us toward foods rich in fat and sugar that give us a pleasure boost to balance the pain of feeling lonely. These sorts of foods essentially act as a quick — yet temporary — fix for an unhappy situation.

Loneliness also affects our self-control. So even if we know that eating a whole bag of double-fudge cookies is bad for us, we find it very hard to stop eating.

Our loss of self-control also means that it's difficult to concentrate when we feel lonely.

In another study, researchers briefly assessed participants' social lives and then placed them in either a lonely or non-lonely group. Each participant then was given the task of listening to different sounds through a pair of headphones.

The sounds played were alternated between the right ear and the left ear; and researchers were aware that in general, people often focus their attention on sounds heard on their right. When participants were asked to focus their attention on sounds heard on their left, it was the lonely participants who had a much more difficult time doing so. They were less capable of concentrating on something challenging, compared to the more socially connected people.

Now that we've looked at loneliness and its effects, the next blinks will explore how we can avoid feeling lonely and reconnect socially.

### 7. Kindness makes us feel connected; yet if we’re too kind, we can be unfair to ourselves. 

Did you know that if you help a stranger pick up coins they've dropped, that brief moment can make you feel good for the rest of the day?

Helping others makes us feel connected. Even if the gesture is small, like picking up some coins off the sidewalk, the effect is the same.

So if you want to feel happier and less isolated from people, make an effort to be kind!

The happiness we feel when we help others selflessly is called _helper's high_. It's a powerful feeling.

Hospice nurses, for example, have been identified as some of the happiest people in the world. Although their work environment is often taxing and sad, nurses build deep connections to their patients, a process that is extremely rewarding.

Even small gestures can give you helper's high. If you give some change to a homeless person, for example, you may experience a mood lift afterwards.

Too much kindness can have a negative effect, however. Lonely people sometimes ignore their desire for things to be fair in order to appear kind, or to make friends.

Fairness is a concept that is important in human interactions and with animals, too. If two monkeys are given the same task, yet one is rewarded with a grape and the other a cucumber, the monkey who got the cucumber will be upset. It knows that a grape is tastier, and feels it deserves the same.

Humans have a similar sense of fairness, but we often ignore it when we are feeling lonely.

Lonely people often crave social connections so badly that they might let others treat them unfairly. Unfortunately, this also can put a lonely person at risk of being disappointed, if he pins his hopes on a person who doesn't truly care for him. This then can lead to even more loneliness.

> _"When we're lonely … we need love, even to the point of persuading us to betray ourselves."_

### 8. Connecting to people in everyday situations can help you feel less isolated and lonely. 

Now that we've seen the negative effects that loneliness can have, how do we prevent loneliness and become more socially connected?

The best way to break the cycle of loneliness is to practice connecting to other people in simple situations. It's not possible to be "cured" overnight, but it's something that anyone who may feel lonely can work on. If you're shy or have difficulty making friends, you need to practice!

It's good to practice in a situation where you have nothing to lose. You'll probably feel less shy with a stranger, and if they don't respond well, you won't have to see them again.

Imagine you're in a supermarket, and you observe someone comparing two brands of yogurt. This is a great opportunity to reach out to a stranger with no strings attached. You could, for example, tell the person which brand you prefer. They'll have another opinion to go on, and you'll feel less isolated.

As you keep reaching out to people, don't get discouraged if you sometimes get an unfriendly reaction. You can't expect to feel connected to everyone. If you set your expectations too high, you'll end up getting disappointed by any failure, and you'll feel lonely again.

So if someone doesn't welcome your attention, don't take it personally. Maybe that person is having a bad day, or perhaps they're just an unfriendly person.

In most cases, this sort of negative behavior doesn't have anything to do with you. So do your best to move on and keep trying. The more you reach out to people, the more you'll build worthwhile connections that will have a very positive effect on your happiness.

### 9. You can break the cycle of loneliness by thinking positively about yourself and others. 

We've seen how important it is to think positively and not get discouraged when trying to connect with others. Positive thinking can also help keep loneliness at bay.

When you have a positive attitude toward life, you'll feel less lonely. Thinking positively will help you stay calm in many situations, especially during an argument.

Although we can't always avoid arguing, it's important to argue in a way that's effective for both people. If you push people away from you through arguments, you may end up being more lonely in the end.

This is especially important in romantic relationships, as fights between romantic partners can sometimes get out of control. There's a good way to defang your arguments: remember to think of your partner in a positive light!

This helps you from taking anything they say in the heat of the moment too personally, and will ensure that your relationship recovers after the argument. Always try to understand your partner's perspective, and remember the positive things about them.

When you stay optimistic, you also influence the way other people perceive you.

We saw in an earlier blink how we often shy away from lonely people, which only increases their loneliness. But the good news is this works in reverse as well!

If you act positively, people will behave more positively toward you in turn. They'll be much more likely to accept you for who you are, and less likely to look down on you. They'll also be more likely to include you in their social activities.

Social isolation is uncommon for people who think positively, which is why optimism is so important for escaping the trap of loneliness. If you want to change the way others relate to you, you first have to change yourself.

### 10. Final summary 

The key message in this book:

**Having meaningful social connections is a crucial part of living a healthy, happy life. Loneliness can have serious consequences, both mentally and physically. If you're caught in a cycle of loneliness, the best way to break out of it is to stay positive and keep reaching out to people, even in small ways.**

Actionable advice:

**Feeling lonely? Don't give up!**

If you're caught in a cycle of loneliness, you can break out of it if you stay dedicated to making changes in your life. Change won't happen overnight, however, but if you stay positive and keep reaching out to others, you'll make the meaningful connections you need to live a more fulfilling life.

**Suggested** **further** **reading:** ** _Happiness_** **by Richard Layard**

In _Happiness_, economist Richard Layard examines what it is that makes us happy and how anyone can achieve greater happiness. Basing his studies on insights from such diverse fields as psychology, philosophy and neuroscience, Layard presents compelling arguments that are great food for thought, encouraging readers to question their daily habits and practices.
---

### John Cacioppo and William Patrick

John Cacioppo is the Tiffany & Margaret Blake Distinguished Service Professor at the University of Chicago, and earned his doctorate at Ohio State University. William Patrick is a writer who previously worked for the Harvard University Press.

