---
id: 58a8d9be95f5390004fb0fbc
slug: long-term-thinking-for-a-short-sighted-world-en
published_date: 2017-02-21T00:00:00.000+00:00
author: Jim Brumm
title: Long-Term Thinking for a Short-Sighted World
subtitle: Restoring happiness, balance, and sanity to our lives and our planet
main_color: 7AA0CC
text_color: 4C6480
---

# Long-Term Thinking for a Short-Sighted World

_Restoring happiness, balance, and sanity to our lives and our planet_

**Jim Brumm**

_Long-Term Thinking for a Short-Sighted World_ (2012) reveals the root of many of the world's problems: our own short-sightedness. From climate change to rampant consumerism and oil depletion, find out how many of the challenges we face today are the result of our inability or unwillingness to see the big picture. These blinks will set you on the path to thinking about the long-term consequences of the actions we take.

---
### 1. What’s in it for me? Open your mind to a long-term view. 

If we hadn't invented glasses some time ago, shortsighted people would have a serious problem. Only able to see things in their immediate surroundings, they would be blind to the larger picture.

The sad truth is, it's not only our eyes but our _minds_ that are shortsighted. For the most part, people seem unable to understand how human actions will affect the planet — and its population — in the long term.

Today, we're depleting what is left of the natural resources and destroying local communities for short-term profits. For the good of future societies, it's high time we got our eyes checked and opened our minds to a long-term view.

In these blinks, you'll learn why humans are shortsighted by nature and how we fail to grasp extended time horizons. You'll see how our shortsightedness may be about to cause our downfall. But this doesn't mean our every effort is foredoomed. You'll also learn that we _can_ create change for a more sustainable world.

You'll also learn

  * why the Royal Air Force had to airdrop cats in Borneo;

  * how many text messages teenagers receive per day; and

  * how many burgers McDonald's sell per minute.

### 2. History shows that people are shortsighted by nature. 

Humans can be pretty good at devising specific solutions for specific problems, but we're not very good at thinking about the long-term consequences of those solutions. To put it another way: we're shortsighted.

Once upon a time, our shortsightedness was beneficial. It kept our prehistoric ancestors focused on the important things — finding food, for instance, and avoiding predators. Our myopia ensured our survival.

Today, however, it's beginning to usher us toward our own downfall.

Between 10,000 and 12,000 years ago, humans invented agriculture. This allowed us to sustain ourselves in the long term, but, despite this revolutionary technology, we remained, by nature, shortsighted.

And when we look back, we can clearly see how this is at the root of our destructive behavior today: humans settle down somewhere and deplete the area's natural resources, thereby causing their own downfall.

This is exactly what happened on Easter Island. Once home to a thriving community, the island was decimated by its inhabitants, who made the fatal error of cutting down all the trees. Most of the wood they used for fuel; the rest they shaped into logs, which they used to roll about their giant statues. This deforestation is believed to be a central reason for the eventual dying out of Easter Island's population.

But instead of learning from our mistakes, we continue in our shortsightedness, to devastating effect.

In the 1950s, when faced with the problem of malaria-carrying mosquitoes on the Asian island of Borneo, the World Health Organization (WHO) responded by spraying vast areas of the island with DDT, a toxic pesticide.

They succeeded in obliterating the mosquitoes, but this short-term solution failed to take into account any of the long-term problems it created.

Geckos ate the contaminated insects and subsequently died of DDT poisoning. Cats then fed on the contaminated geckos and they began dying as well. This left rats with a reduced number of predators, and as their numbers skyrocketed, so too did cases of typhus and plague.

Due to WHO's shortsightedness, the Royal Air Force was forced to airdrop cats into Borneo's affected areas to bring the rat population back down.

> _"We are the only [species] that has the capacity to alter our environment so profoundly as to render it unlivable."_

### 3. Our shortsightedness is reinforced by our focus on the clock. 

We place a huge importance on time and getting things done efficiently, so why is it so difficult for us to consider the long-term consequences of our actions?

It stems from our inability to think beyond our own lifetime and understand _deep time_, which can span millions or billions of years.

While we often read about scientific facts and numbers that deal with long time periods, such as the Earth being 4.6 billion years old, we can't fathom such enormous stretches of time.

We can only truly understand what we're able to experience, so the longest stretch of time we can really comprehend is around 80 years or so, which is next to nothing relative to 4.6 billion years.

Though we struggle to deal with deep time, we excel at living by the seconds and minutes of the clock.

From the moment our alarm goes off in the morning, we have an eye on the clock. And everything we do afterward is scheduled and organized down to the minute. At work, it's not uncommon to have no more than five minutes between meetings — just enough time to walk from one office to the next.

This emphasis on the clock has made our society move increasingly fast.

There is a constant stream of text messages and emails for which we're expected to drop everything and immediately respond to.

In the last three months of 2008 alone, the average US teenager dealt with 2,272 text messages per month. This is equal to sending or receiving a message every 20 minutes, throughout the day and night.

So what are the long-term effects of running our lives by the clock?

This pressure to make snap decisions and provide fast results often adversely affects the quality of our responses.

We also tend to eschew long-term solutions like dieting and exercise, instead favoring drugs that relieve our immediate symptoms. This mentality can lead to returning symptoms — often worse than before — and deteriorating health.

> If the entire time the Earth has existed was compressed into 24 hours, then the history of the human race would be only 1 second long.

### 4. Cars have had long-term negative consequences on nature and our behavior. 

There are numerous ways our shortsightedness can be seen in daily life, but perhaps the most apt manifestations are the cars we drive.

Automobiles are a prime example of short-term thinking. They're everywhere and play a critical role in our lives, but originally almost no thought was given to the long-term impact they would have on human society.

Cars were immensely popular when they were first introduced in the late 1800s. They not only gave people a newfound freedom; they were an important status symbol. Back then, no one could have envisioned the horrendous impact cars would end up having on the world.

In 2012, there were approximately 270 million cars in the United States alone, accounting for nearly half of all automobiles worldwide. And since cars were first introduced, our towns and cities have all been designed and built with them in mind.

Civic planners assume that everyone owns a car and that everyone wants their trips to work and the grocery store to be as convenient as possible. As a result, we've paved millions and millions of acres of arable land to build roads and highways.

Worse still, wars have been waged and thousands of lives lost in the name of that automotive necessity, oil. And perhaps still worse, if you take the number of US lives lost in every war the nation has participated in, it falls far short of the number of US lives lost in car accidents.

And there are other, more existential issues that cars cause; each driver, for instance, exists within his own personal bubble, separated by glass and steel from his fellow man. This disconnect is what induces road rage. Behind the wheel, we scream and swear at people in a way that would seem unthinkable were we face to face.

Though this may seem less serious than traffic-related fatalities, it's exactly this kind of isolation and dissociation from one's neighbor that can lead to the erosion of local communities.

### 5. Local businesses have essential long-term benefits and need to be supported. 

Unlike cars, your neighborhood's small businesses are great at bringing people together. And they're one part of our society that does keep long-term benefits in mind.

By their very nature, local businesses play an essential role in society by keeping communities strong and focused on the future.

When a small business opens up in a neighborhood, they're hoping to achieve long-lasting success by helping neighborhood customers. So, unlike a chain store, they can't just pack up and relocate if customers are unhappy, which gives them an incentive to try extra hard.

Local businesses also help their area's economy by keeping on average 55 cents of every dollar spent within their community. That's a significant amount when you compare it to the 15 cents that a typical chain store will keep local.

And with a strong local economy, there's less need to use resources for long-distance shipping and transportation, both of which damage the environment.

Yet, despite these advantages, corporations still dominate the retail industry.

Corporations like Walmart, Costco and Target have been steadily replacing endangered local businesses all over the United States. As of 2012, 30 percent of all customer spending went to the top ten mega retailers.

In the wake of this corporate takeover, generic shopping malls have replaced what were once vibrant community districts made up of small businesses with different personalities.

These superstores have left many neighborhoods without any small businesses at all, which leaves people with no choice but to use their cars to drive out to the mall to do any and all of their shopping.

So what can we do to save local businesses and keep our communities strong and forward-thinking?

The simple answer is to support your neighborhood's small businesses and buy local goods, such as produce, whenever you can, and to encourage others to shop locally as well.

By helping create a strong local economy, new local businesses will emerge and your community will be in good shape for future prosperity.

### 6. Our debt crisis is also the result of our shortsighted nature. 

There's a good chance you or someone you know has had to deal with the stressful problem of getting out of debt.

Getting _in_ debt in the first place is often the result of short-term thinking.

Credit cards are an easy way to rack up debt, and they're a shining example of costly shortsightedness.

In 2008, the average US credit-card holder owned 3.5 cards, and in 2010 there were over 609 million credit cards being used in the United States alone.

The logic behind using a credit card is the very definition of short-term thinking. You're essentially saying, "I'll buy this now and worry about paying for it later."

By promoting credit cards, our financial system is actually encouraging us to use short-term thinking and get ourselves weighed down with debt. This system is only concerned with stimulating the economy, and since people who've accumulated debt are people who've spent money, the individual details don't matter. The economy is being stimulated.

As a result, the United States is experiencing a debt epidemic. Even the country itself is in debt. As of 2016, the national debt stood at about $3.4 trillion.

But this doesn't change the fact that it's in the best interest of the US economy to give people credit cards and promote mindless consumerism.

If people don't spend, companies lose money and employees get laid off, which leads to even less spending and even more people losing their jobs.

On the other hand, our economy can't withstand all this debt either, and there will eventually be a breaking point.

What needs to change is personal. People need to realign their values with the things that bring true, long-term happiness. The answer is not the accumulation of material objects; it's things like love, friendship, civil engagement and doing what's healthy for your body and mind.

### 7. We need to return to small farming and move away from food production driven by short-term profits. 

If you were to ask a child where food comes from, she might tell you, "The supermarket!" Though cute, such an answer is just one more sign of how corporations are disconnecting us from what's important.

We used to feel a strong connection to our food; knowing where the ingredients came from and taking time to prepare them and cook them ourselves.

These days, this connection has become transactional. We hand someone money and take our meal, without a care for what's going on behind the scenes. Few people ask exactly how McDonald's is able to sell 75 burgers every single second around the world.

Again, it comes down to the shortsightedness of the big food corporations and their failure to consider the long-term impact they're having on our planet and our lives.

A big corporation like McDonald's has corporate needs: things need to be fast, consistent and cheap. So farming methods were changed to satisfy these needs, and over the years, more and more small farms were turned into mono-crop and factory farms. As a result, between 1950 and 2003, the overall number of independent farms in the United States decreased by 60 percent.

This shift took place in the name of short-term profit, with little concern for long-term benefit.

It may be efficient to keep animals cooped up in extremely close quarters on factory farms, but there are a number of horrible side effects as well. To take but one example: massive amounts of compressed manure amass on the farms, only to be dumped into ponds and rivers, poisoning countless fragile ecosystems.

In order for us to produce healthy food, we need to have a healthy and sustainable connection to our Earth. Our current methods, which are hurting both us and our planet, simply aren't cutting it.

A return to long-term sustainability means a return to small farming, which produces food in a way that protects the land. Small farms produce relatively little manure, which, in small amounts, can be reused the way it once was: to fertilize the land for growing crops.

One way to support small farms is by buying your meat and produce from farmer's markets.

> The average American meal has traveled 1,500 to 2,000 miles from source to plate.

### 8. We also need to find renewable energy resources. 

When it comes to using unsustainable methods, the food industry isn't the only culprit. Our reliance on fossil fuels for energy production is also very shortsighted.

Fossil fuels include coal, oil and natural gas, and even though we've known for quite some time that these resources are dwindling, we continue to use them for just about everything: power, transport, heating, lighting, building and agriculture, just to name a few.

Naturally, every day our reserves are further depleted.

Some estimates say that our current rates of consumption will completely drain our oil reserves in just 40 years.

This might lead you to wonder why all our attention isn't focused on finding new energy sources. The answer is simply that this would require us to take a longer view of things.

Shortsighted creatures that we are, we continue to go to extreme lengths for oil, fighting wars and using extraction methods like fracking, which damages the environment severely.

Once you start thinking about long-term prosperity, it becomes clear that new energy sources must be developed.

One option is nuclear power. But while this would allow us to produce a great amount of energy, it would come with serious risks; an accident could be catastrophic. And then there's the long-term problem of nuclear waste staying dangerously radioactive for tens of thousands of years.

The best option we have now is probably solar energy.

After all, the sun provides energy that is both clean and unlimited.

In fact, if each state built just five square miles of solar paneling and connected them to form a giant interconnected grid, we could easily power the entire United States.

While such a proposal would cost around $6 trillion, we would spend the same amount anyway in just 8 years if the price of oil were $100 per barrel.

### 9. The planet’s health is our responsibility, so we must begin thinking long-term. 

As we've seen in the previous blinks, our shortsightedness has left us with many problems. And it raises the question of how we start adopting a long-term mindset.

The first step is to start paying attention to what nature is telling us.

Nature provides plenty of examples proving the impossibility of infinite growth.

Nature is able to thrive by living in cycles: the cycle of day and night; the cycle of the seasons, with leaves growing on trees and then falling off; the cycles of mating and migration.

All of this shows us that humans, which are part of nature, can't just consume and grow indefinitely.

There are basic rules we have to follow. We can't consume more food than we are able to sustainably produce, and we can't simply use and rely on finite resources.

The next step is to remind ourselves of our potential to change.

The press often paints a grim picture of our society's future, but this doesn't mean we're doomed. There are people discovering and teaching ways to live sustainably and fighting to conserve valuable ecosystems, but their stories typically don't get reported very enthusiastically.

Connecticut's Terra Firma Farm is just one example of a community that is teaching the next generation about sustainable farming.

But everyone needs to be proactive. We can't rely on politicians to pursue sustainable long-term policies, since their priority is always going to be the next election. For big changes to be made, we must work together.

Even though we tend to focus on our individual problems, we're also responsible for the world we live in. Each of us must work to keep our planet healthy.

Remember, our own individual welfare and prosperity depends on our planet's health. We as humans have become powerful enough to affect the fate of the Earth. Let us embrace this power and pass a healthy planet on to future generations.

### 10. Final summary 

The key message in this book:

**Our shortsightedness has resulted in a wide array of problems. It's time to start thinking about long-term solutions and start making a positive impact for the well-being of future generations.**

Actionable advice:

**Bring your own coffee cup!**

In 2010, Americans used 23 billion paper cups, each of which ended up in a landfill. When you buy your next coffee, bring your own cup. After just 24 uses, a reusable to-go cup results in real energy savings. Plus, you might get a discount for your trouble!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Carrots and Sticks_** **by Ian Ayres**

_Carrots and Sticks_ (2010) is the bible of behavior, incentives and self-control. These blinks will explain how you can swap out bad habits with rewards, punishments and formal commitments to yourself. You'll gain the skills necessary to tackle challenges such as losing weight, quitting smoking and saving for retirement.
---

### Jim Brumm

Jim Brumm has over 25 years of experience as a writer and editor at magazines, newspapers, a web design firm and an environmental restoration business. He currently lives in Santa Rosa, California, where he works as a freelancer and musician.

