---
id: 556c641f32366300074c0000
slug: long-walk-to-freedom-en
published_date: 2015-06-03T00:00:00.000+00:00
author: Nelson Mandela
title: Long Walk to Freedom
subtitle: The Autobiography of Nelson Mandela
main_color: E19542
text_color: 94622B
---

# Long Walk to Freedom

_The Autobiography of Nelson Mandela_

**Nelson Mandela**

Nelson Mandela's _A Long Walk to Freedom_ (1994) is one of the most famous autobiographies of recent times. It tells the story of his life, from his humble beginnings in the South African countryside to his work as an iconic anti-apartheid freedom fighter, and ends, after chronicling his twenty-year prison sentence, with his final victory and release.

---
### 1. What’s in it for me? Familiarize yourself with the life story of one of the 20th century’s most revered individuals. 

If asked to name the greatest icon of recent times, who would we pick? It's a fair bet that many would choose Nelson Mandela. A man who suffered at the hands of an extremely unjust society, he refused to break, and instead kept fighting, kept pushing for justice and, after decades of punishment, won.

But what led Mandela to develop such strength and conviction? These blinks take you on a journey through Mandela's life, showing the events that made the man.

In these blinks, you'll discover

  * why Mandela had to turn to violence in the face of state brutality;

  * why Mandela turned against many of his supporters and talked to his arch enemy; and

  * how Mandela got his prison guards to support him against the government.

Note: The following blinks contain strong, offensive language. A racist term is used in a translation of a popular political party during Apartheid and reflects Mandela's experience of racism and discrimination in South Africa at the time.

### 2. Nelson Mandela's interest in social justice began during his childhood in rural South Africa. 

Nelson Mandela hardly needs an introduction. His life story is a classic tale of one man's struggle against oppression, and we'll certainly be telling it for years to come.

Mandela was born in 1918, in Mvezo, a small village in the South African countryside. He belonged to the _Xhosa_ tribe, a proud ethnic group that highly valued law, courtesy and education. At birth, he was named Rolihlahla, which means "trouble maker" in the Xhosa language.

Mandela's father, Gadla Henry Mphakanyiswa, was a chief in the tribe, a distinction that traditionally would have endowed him with high social status in the community. The British influence, however, had weakened the authority of tribal chiefs, so the position carried little political clout at the time.

Additionally, the British could oust anyone who threatened their authority, because each chief had to be ratified by the government. Mandela's father was very headstrong and often challenged them, and it wasn't long before the British revoked his status as chief.

When Mandela's father died, another regent of the tribe, Jongintaba, offered to become Mandela's guardian. This would end up having a huge impact on his life.

As a child, Mandela often attended tribal meetings at the regent's court, where he learned about the plight of his people. One of the most prominent figures there was Chief Joyi, an elderly chief with royal lineage who railed against white supremacy.

Chief Joyi taught that the surrounding tribes had lived peacefully until white Europeans arrived and sowed the seeds of conflict. The white man, he said, was greedy and stole land that should've been shared, shattering the tribes' unity.

Later in his life, Mandela would learn that Chief Joyi's history lessons hadn't always been correct. Nonetheless, they influenced his life dramatically: they opened his eyes to social injustice.

### 3. Mandela first began challenging authority as a student at Fort Hare. 

The young Mandela was fond of physical activities like stick fighting, but he was an introvert as well. He was also the first person in his family to go to school, where his lifelong commitment to learning and education began.

The village school Mandela attended was entirely British. The students learned exclusively about British history, culture and institutions; African culture was simply never mentioned in the classroom. Mandela therefore learned the history of his own people from the elders in the regent's court.

At the time, it was standard for Africans to have an anglicized name in addition to their regular one. Miss Mdigane, Mandela's teacher, chose Mandela's for him: Nelson. Mandela never learned the reason for her choice, but suspected it could've been connected to the great British sea captain Lord Nelson.

Mandela studied very diligently. So much so that he ended up completing his junior certificate at Healdtown College in two years rather than three. Then, in 1937, he moved to Fort Hare College, where he studied English, anthropology, politics, native administration and law.

It was also at Fort Hare that he began challenging authority. One night, Mandela and some of his fellow students started discussing the lack of freshmen representation in the House Committee, and they decided to elect their own House Committee that better addressed their interests.

Mandela and his friends caucused among the freshmen and garnered massive support. They then told the warden that if he overruled them, they'd resign, which would cause their freshmen supporters great displeasure.

In the end, they succeeded: the warden allowed the Committee to stand. The following year, however, things didn't go so well.

In his second year, Mandela strongly supported a student boycott and ended up getting expelled for it. Finished with his time at Fort Hare, he decided to move to Johannesburg and get a job.

### 4. Mandela's political work began in Johannesburg. 

Johannesburg was a bustling city when Mandela arrived in 1941. He didn't know it at the time, but in Johannesburg he'd make lifelong friends who'd fight against oppression with him.

Mandela first took a job as a night watchman in a gold mine. To him, the gold mine was a powerful symbol of white oppression — thousands of Africans slaving away every day, in a massive capitalist enterprise that only benefited the white owners.

His true goal was to become a lawyer, however. One day, one of Mandela's cousins offered to take him to someone who could help, Walter Sisulu.

Sisulu ran a real estate agency that specialized in providing housing for Africans. He and Mandela would go on to become very close and the pair would face many hardships together.

Sisulu managed to get Mandela a position as a clerk in one of Johannesburg's largest law firms, where he worked while studying for a BA in law at the University of South Africa.

One of Mandela's colleagues, Gaur Radebe, the only other black employee at the firm, was a prominent member of the _African National Congress_, or _ANC_.

The ANC was founded in 1912, making it the oldest African national organization in the country. It aimed to secure full citizenship for all Africans in South Africa.

Gaur believed the ANC was the best hope for change in the country, and, soon enough, Mandela began attending ANC meetings with him.

Mandela got his first taste of real political activism in 1943, during a bus boycott that protested the rising bus fares. Mandela became an active participant in the boycott — not just an observer. Marching alongside his people was exhilarating and empowering.

In his nascent political life, Mandela also befriended a number of other activists, like Tony O'Dowd, Harold Wolpe and various members of the Communist Party. These connections would prove vital in his later struggle against apartheid.

### 5. The National Party's rise to power saw the beginning of apartheid. 

Walter Sisulu's home in Johannesburg became a hotspot for ANC members and African intellectuals. One of the people who frequented the house was Anton Lembebe, a prominent lawyer who'd have a big impact on Mandela.

Lembebe argued that Africa rightfully belonged to black people. He called on African men from all tribes to unite and assert their right to the land.

Reclaiming the land would do away with the Western standards and ideals that had caused many Africans to internalize deep feelings of shame about their culture — essentially curing a culture-wide inferiority complex.

Mandela, Sisulu, Lembede and a few others eventually went to see Dr. Xuma, the head of the ANC at the time. They suggested the ANC build a _Youth League_ to gather support, as the organization was still somewhat small. Dr. Xuma was initially hesitant because he thought the African masses couldn't be organized, but, in 1944, he agreed to form the Youth League.

And then, in 1948, something shocking happened: Dr. Daniel Malan's _National Party_ won the general election.

The National Party had run on a political campaign called a _partheid_, which means "apartness" in Afrikaans. They'd won the election using extremely racist slogans like _Die kaffer op sy plek_ — "The nigger in his place." As soon as Malan came to power, he began implementing a series of acts that put apartheid into practice.

One of the first was the _Group Areas Act_, which stipulated that different racial groups had to stay in strictly separated areas. The Youth League fought back, organizing a _National Day of Protest_, where they urged all African workers to stay home.

The National Day of Protest took place on June 26th, 1950. It was a success, strengthening both the movement and Mandela's commitment to the struggle.

Thanks to the protest and the _Defiance Campaign_, a similar political move, the number of ANC members swelled to 100,000 in just one year.

### 6. As the National Party's tactics became harsher, Mandela saw the necessity of violence. 

The National Day of Protest strengthened the ANC, but it also illustrated the power of the National Party, which only intensified its efforts to squash resistance.

After the protest, the National Party passed the _Suppression of Communism Act._ They then used it to go after Mandela.

On June 30th, 1950, Mandela was arrested for violating the act. Because of the role he'd played in planning and executing the previous year's protests, the government had been after him for a while.

Massive demonstrations took place on the streets of Johannesburg when Mandela and the others accused along with him first appeared in court. On December 2nd of the same year, they were all found guilty of "statutory communism" and sentenced to nine months in prison. The sentence was suspended for two years, however, allowing Mandela to continue his work.

In August of 1952, Mandela started his own law firm. It focused on helping Africans, many of whom were now in desperate need of legal help. It had become illegal for Africans to ride on Whites Only buses, drink from Whites Only fountains or even walk through Whites Only doors.

When he appeared in court, Mandela made a point of being defiant. In one trial, for example, he managed to free a client by embarrassing her white employer.

The employer had accused her black maid of stealing her "madam's" clothes. So Mandela picked up a piece of the evidence — a pair of her panties. He held them up to the court then asked if they were hers. Embarrassed, she said no, and the case was later dismissed.

As the situation worsened, Mandela and Sisulu came to believe that the National Party's increasingly harsher laws could only be met with violence. Sisulu tried to secretly travel to China to ask the government if they'd provide them with weapons, but the ANC leadership soon found out, which led to a heated debate on the use of violence in the ANC.

### 7. The government went after Mandela and the other ANC leaders as the situation grew worse. 

Mandela was arrested at his house on December 5th, 1956. The warrant for his arrest accused him of _hoogverraad_, the Afrikaans term for High Treason. He'd long expected the government to make a bigger move against the ANC, and now it had finally arrived.

The government claimed they had evidence that Mandela had planned violent acts in the Defiance Campaign. They also arrested nearly all of the Campaign's other leaders.

It was clear from the start that the prosecution's case was weak. The star witness was Solomon Ngubase, a man who was serving a sentence for fraud. He claimed he'd attended an ANC meeting where the leaders had decided to send Walter Sisulu to the Soviet Union to acquire weapons for an armed struggle.

During Ngubase's cross-examination, the defense established that he was neither a member of the ANC nor a university graduate, as he'd claimed. This was a serious blow to the prosecution.

As the court case dragged on, the struggle raged outside. The severity of the situation really hit home on March 26th, 1960, when a tragedy occurred in the town of Sharpeville.

There, thousands of Africans had gathered around a local police station, demonstrating against the "pass laws," which required all Africans to carry pass books when they left their designated neighborhood. The police panicked and opened fire on the crowd without warning. At least 69 people were killed, most of whom were shot in the back as they tried to flee.

Over 50,000 people gathered in Cape Town to protest the shootings. Riots broke out and the government declared a State of Emergency, suspending _habeas corpus_.

The court case took a better turn, however. Although the state had provided thousands of pages of material, the judge ruled that the evidence of a violent plot was insufficient and all the accused were acquitted.

### 8. After the trial, the ANC's struggle moved underground and Mandela created the MK. 

While Mandela and his friends were in prison awaiting trial, they decided it was time to move things underground.

Mandela knew there was no time for celebration after his release; the ANC had to fight back quickly and they needed to change their tactics.

The violence debate within the ANC had already been going on for a few years. In a secret executive meeting, in 1961, Mandela argued that the state had left the ANC no other option.

But the ANC leadership decided the party would maintain an official policy of non-violence; Mandela, however, could create a militant organization within it. The new, militant wing of the ANC was dubbed _Umkhonto we Siswe_, meaning "The Spear of the Nation." It was called the _MK_ for short.

The MK started by using sabotage. Mandela had never fired a gun at someone in his life, but began studying all he could about guerrilla warfare, sabotage and revolution.

He also moved to the _Liliesleaf Farm_ in Rivonia, a small suburb of Johannesburg that had been purchased by the movement. Liliesleaf Farm served as a safe house and training ground for the MK, and it was there that Mandela practiced his shooting and learned to use explosives.

He and the other MK members agreed to use sabotage first, as it had the lowest chance of injury and required less manpower. So in December of 1960, they detonated homemade bombs at a number of government buildings and power stations in Johannesburg. They also began circulating a manifesto declaring the MK's arrival.

The explosions came as a surprise to the government, which, plotting reprisal, redoubled its efforts as well.

### 9. The government persecuted Mandela as the struggle grew more intense. 

By this point the government was willing to do anything to catch Mandela, who'd become an iconic figure in the movement.

They finally captured him on August 5th, 1962, when he was on his way back to Liliesleaf Farm after a secret MK meeting. He was then taken to prison, where he was joined by Sisulu, who'd also been arrested.

On Mandela's first day in court, he, his wife and many of the spectators wore leopard-skin _karosses_, traditional Xhosa garb. In his first address, he stated that he intended to put the government on trial and he didn't feel morally bound by the laws, as they were passed by a parliament he couldn't vote for.

He then recounted several instances where the government had rejected the ANC's attempts to settle their issues through official means. The ANC saw no other option but violence.

The prosecution's main piece of evidence was a six-page action plan taken from Liliesleaf Farm that implicated Mandela and the others for their planning of the MK. The document made it clear they'd be found guilty.

The trial garnered a great deal of international attention and excerpts from the speech Mandela gave on the day of the verdict were published in many newspapers. Vigils were held in cities all over the world.

On June 12th, 1964, Mandela was found guilty of all charges, but international pressure on South Africa helped save his life. A group of UN experts, for example, advised that amnesty be granted to everyone who opposed apartheid. The charges against Mandela would've usually carried the death penalty, but, instead, his final sentence was life in prison.

> _"It is a struggle of the African people, inspired by their own suffering and their own experience. It is a struggle for the right to live."_

### 10. Mandela and his fellow prisoners kept up their resistance in prison. 

After the trial, Mandela was taken to Robben Island, where he'd spend the next 20 years of his life.

Everyday life was very grim on Robben Island. Volleyball-sized stones were dumped into the prison courtyard each day and the prisoners had to crush them into gravel with small hammers. The weather was scorching hot.

Mandela belonged to the class of prisoners that were kept under the strictest control. He was only allowed one visitor and one letter every six months. His correspondence was heavily censored, too; he could barely read the letters he received from Winnie, his wife.

The worst part of prison was solitary confinement, which prisoners could be given for the smallest infractions. Just failing to stand up in your cell when a guard entered was enough.

The prison was designed to break them emotionally, so they kept up the spirit of resistance to make it through their days. When all prisoners except Indians were given shorts to wear, Mandela demanded to see the warden of the prison, as he felt it was indecent for an African man to wear shorts.

After two weeks of protest, the guards gave in. The victory was small but important nonetheless.

The prisoners faced many other challenges as well. It was difficult for them to get books and magazines, and anything related to politics or news was strictly forbidden.

Fortunately, the guards weren't especially bright. One prisoner managed to get a copy of _The Economist_ because the guards assumed it was about economics.

Then, in 1966, the prisoners decided to go on a hunger strike to protest the prison's living conditions. Eventually, the guards joined them. The prison authorities realized the strike was too much for the prison, so they agreed to the prisoners' demands. Rebellion had proved contagious.

### 11. Mandela and his fellow African freedom fighters had wide support from the international community, which pressured the South African government. 

The guards at Robben Island gradually became less strict with the prisoners as time went on, but the situation outside only worsened. There were also signs of hope, however. The 1970s saw an increase in mass protests around Africa and a new, more militant generation of freedom fighters began to emerge.

Mandela and the other prisoners had limited access to the news, but they managed to get word of an uprising in 1976.

In June of that year, fifteen thousand schoolchildren in Soweto, an urban area of Johannesburg, had gathered to protest legislation requiring schools to teach half of their courses in Afrikaans, a language most African children didn't want to learn.

Once again, the police opened fire on the crowd without warning, killing Hector Pieterson, a 13-year-old, along with many others. Two white men were also stoned to death. The events triggered riots and protests throughout the country.

Many in the new generation of South African freedom fighters were more aggressive and militant. Those who were convicted and sent to Robben Island viewed Mandela and the other Rivonia prisoners as moderates.

Many of the young freedom fighters were part of the _Black Consciousness Movement._ They believed the black man had to free himself from his sense of inferiority to whites in order to free himself from oppression. Mandela admired their militancy but thought their exclusive focus on blackness was immature.

The South African uprisings in the late 1970s were covered extensively by the international media, and people worldwide grew more enraged about apartheid. "Free Mandela" campaigns and events were popping up all over the world.

In 1980, the Johannesburg Saturday Post ran a story with the headline _FREE MANDELA,_ including a petition the readers could sign. The article sparked a national debate on Mandela's release.

### 12. The South African government and freedom fighters finally began to negotiate when they both accepted that the violence was too much. 

By the early 1980s, the struggle was only getting bloodier. Where would it end? The violence seemed to be spiraling out of control, pulling society down with it. Something had to be done.

In 1981, the South African Defense Force raided the ANC's offices in Maputo, Mozambique, killing thirteen people. The MK, who'd become more violent by that point, retaliated. In May, 1983, they detonated a car bomb outside a military facility in Pretoria as revenge, killing nineteen people.

Mandela realized that, without negotiations, the situation would only get more chaotic. The ANC had maintained that they wouldn't negotiate with the racist government, but Mandela started to realize it was necessary.

After the government again declared a State of Emergency, in 1986, Mandela requested a meeting with Kobie Coetsee, the Minister of Defense. Surprisingly, the request was granted and he was taken to the minister's private home in Cape Town.

Coetsee asked Mandela what it would take to keep the ANC from using violent tactics. This was the first step in negotiations.

Then, in May of 1988, Mandela and a committee of state officials began holding a series of secret meetings. In December of the following year, Mandela met with the new president, F. W. de Klerk. de Klerk was committed to fostering peace and listened carefully to what Mandela had to say.

In February, 1990, de Klerk announced that he would lift the ban on the ANC, which was still technically an illegal organization (that had widespread support through the country). He also agreed to release all political prisoners that had been put in jail for nonviolent activities. The same day, de Klerk met with Mandela and told him he'd be released.

### 13. Mandela was released in 1990, received the Nobel Peace Prize and continued his political work. 

Nelson Mandela was released on February 11th, 1990. For South African people, however, freedom was still a long way off.

Mandela had been held in a low-security prison outside Cape Town since 1988. He had his own living space there, which served as a kind of halfway house between freedom and prison.

On the day of his release, Mandela was supposed to be taken from the house to the front gate by car, but a television presenter asked him to walk the last part of it. As he made his way toward the gate with his wife by his side, he raised his fist and the crowd shouted.

Later that day, he gave a speech from a City Hall balcony, before a massive crowd. He shouted out "_Amandla_," which is Xhosa for "power," and the crowd shouted back "_Ngawethu_," meaning "to us."

The following afternoon, Mandela told reporters that he'd do anything the ANC saw fit. He saw no conflict of interest between supporting ANC's militant struggle and moving forward with negotiations. The ANC would respond to peace with peace.

The relationship between the government and ANC was still tense, however. In December of 1992, the ANC executives decided to engage in a series of secret bilateral talks with the government. In the first, it was decided that all parties that earned over five percent in the general election should have proportional representation in the cabinet. That meant the ANC would have to work alongside the National Party, which provoked controversy within the ANC.

On April 27th, 1994, the first non-racialized election took place in South Africa. The ANC earned 62.6 percent of the votes. Shortly before that, Mandela was awarded the Nobel Peace Prize.

> _**"** The sight of freedom looming on the horizon should encourage us to redouble our efforts."_

### 14. Final summary 

Nelson Mandela devoted his life to the ideals he believed in. Though he and his people faced great challenges, persecution and violence, Mandela remained committed, even when he was held in prison. His work and dedication led to his becoming the torchbearer of the anti-apartheid movement, and the man who paved the way for a free and democratic South Africa.

**Suggested** **further** **reading:** ** _Mandela's Way_** **by Richard Stengel**

This book is about the inspiring personality traits of Nelson Mandela. It shows us how to develop a similar strength of character, so that, no matter what obstacles life throws in our path, we can overcome the challenges, forgive our oppressors, understand the complexity of human nature, fight for our core principles and thereby succeed in changing society for the better.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Nelson Mandela

Nelson Mandela is among the most eminent political figures of the 20th century. He was the icon of the anti-apartheid struggle in South Africa, devoting his entire life to the cause. For his commitment to justice, he was awarded the Nobel Peace Prize in 1993.

