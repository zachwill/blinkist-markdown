---
id: 56e6a62c9854a5000700002d
slug: losing-the-signal-en
published_date: 2016-03-14T00:00:00.000+00:00
author: Jacquie McNish and Sean Silcoff
title: Losing The Signal
subtitle: The Untold Story Behind the Extraordinary Rise and Spectacular Fall of BlackBerry
main_color: D4302B
text_color: D4302B
---

# Losing The Signal

_The Untold Story Behind the Extraordinary Rise and Spectacular Fall of BlackBerry_

**Jacquie McNish and Sean Silcoff**

_Losing the Signal_ (2015) tells the story of Research in Motion, the company that created the BlackBerry. Beginning with RIM's time as a minor contender in wireless communication in the mid-1990s, it takes us through its triumph as the most influential player in the wireless communication devices market in the mid-2000s, and reveals why RIM sank as rapidly as it rose.

---
### 1. What’s in it for me? Learn from the rise and fall of BlackBerry. 

The story of how Kodak, once the biggest company in photography, went bankrupt after failing to make the transition to digital photography in the early 2000s is a textbook case: companies must adapt to a rapidly changing market or go under.

But Kodak is not alone. The story of the BlackBerry and RIM, the company behind it, is similar to the Kodak story. In just a few years, the Blackberry, once ubiquitous among businessmen and -women all around the world, completely disappeared.

How did something so successful go so wrong? 

In these blinks, you'll learn

  * how BlackBerry got its name;

  * how the iPhone changed everything; and

why you should always keep your accounting in order.

### 2. Jim Balsillie and Mike Lazaridis got to know each other in Canada. 

In the 1970s, Steve Wozniak and Steve Jobs created the Apple I, which essentially laid the foundation for personal computing. In the 1990s, another tech-savvy duo invented another landmark device: the BlackBerry.

One half of the duo — Jim Balsillie — started out as an ambitious student and employee. He grew up in Canada, and was inspired by books such as _The Canadian Establishment_, by Peter Newman, which was about the Canadian business elite, and the classic _The Art of War_, by Sun Tzu.

Balsillie's first job was at Sutherland-Schultz, a small firm in Canada, where he was in charge of selling products and services, as well as negotiating business deals with partners. While at Sutherland-Schultz, he was introduced to Mike Lazaridis — the CEO of Research in Motion Ltd. (RIM). RIM was a Sutherland-Schultz supplier and one of the first companies to work on wireless data technology devices.

After meeting Lazaridis and learning about his ambitions for RIM, Balsillie became interested in working with him.

Mike Lazaridis had a keen, lifelong interest in technology. He was born in Istanbul and, in 1966, when he was five years old, his family moved to Canada, where he was raised. His passion for engineering was already obvious in high school. He would often ask his teachers if he could experiment with the equipment at the school; he and his friends were fascinated by science and elected to get after-school tutoring in applied physics so that they could build on this passion.

Later, during his time at the University of Waterloo, Lazaridis and his friends created a device called Budgie, which could be wirelessly connected to a TV and display information. Lazaridis saw a great business opportunity in Budgie, and it was for this reason that he founded RIM in 1984.

After Mike Lazaridis and Jim Balsillie began working together, it became clear that they were an ideal pair: their different character traits resulted in a perfect symbiosis, with Lazaridis's technological know-how complementing Balsillie's aptitude for business management.

### 3. Balsillie and Lazaridis decided to work in the field of wireless communication and built their own wireless device. 

Remember having to connect your PC to the internet using a cord? Seems like yesterday, right? So it might come as a surprise that wireless communication has been around for a while.

Balsillie and Lazaridis began working on wireless communication in 1996. Before developing their own device, RIM worked on radio modems that could connect laptops and mobile data users to a network called Mobitex. Initially established to allow cars and service trucks to communicate with one another wirelessly, Mobitex was a wireless data network based on radio frequencies.

But Balsillie wanted to diversify the customer portfolio of RIM. At the time, RIM's only major customer was a US modem maker called U.S. Robotics. However, U.S Robotics turned out to be unreliable, at one point cancelling a deal that nearly brought RIM down. After the deal fell through, Balsillie realized that he had to acquire more customers if RIM was to survive in the long run.

Lazaridis saw things slightly differently, though, and advocated for the diversification of RIM's product portfolio. Eventually, he persuaded Balsillie to do exactly that. 

In 1996, Motorola, Nokia and U.S. Robotics were all in the same race to see who could sell the perfect palm-sized, two-way communicator — that is, a portable device that could both send and receive emails. Lazaridis was eager to take part in that race and began working on RIM's prototype: the Inter@ctive 900. Balsillie backed his idea and the two of them decided the Inter@ctive 900 would be the core of RIM's business model.

And thus began RIM's new focus on wireless communication devices.

### 4. A cooperation with BellSouth helped establish the BlackBerry brand. 

In business, a good deal means a win-win agreement between two parties — and RIM was able to come to just such an agreement with BellSouth.

BellSouth owned the Mobitex network and had been planning to shut it down. That is, until RIM approached them with their new wireless device. If not for RIM's device, Mobitex would no longer have been profitable.

Luckily, Balsillie and Lazaridis were able to convince the BellSouth executive team of their device's potential and, as a result, BellSouth resolved to not only keep the Mobitex network alive but to expand it, making it capable of reaching approximately 90 percent of the US population. Because BellSouth and RIM were reliant on each other — and because BellSouth was a reliable partner — RIM was able to generate consistent revenue, which made the endeavor even more secure. 

With a view to making better offers for their customers, and because Balsillie and Lazaridis also wanted more autonomy, RIM purchased unlimited two-year access to the Mobitex network for five million dollars. This enabled RIM to sell cheaper airtime to their customers, which made them particularly attractive to consumers.

By this point, Balsillie and Lazaridis had realized the massive potential of their wireless two-way communication device. But it still needed a name.

RIM had hired a marketing team that advised against names that might be associated with work. Better to choose a name from the natural world, they said. 

Both Balsillie and Lazaridis had also noticed a trend among executives: the constant stream of e-mails was stressing them out.

In an attempt to quell some of this stress, they named their device after a fruit that lowers the blood pressure of those who eat it. Furthermore, their device sort of resembled its organic namesake. 

And so the BlackBerry was born. But how did it become such a hit?

### 5. The BlackBerry success story began with winning over specific executives. 

If you want to implement a new idea, you've got to convince others of its soundness. This was exactly the challenge faced by the people at RIM.

To get the BlackBerry off the ground, RIM had to persuade executives of the concept. Balsillie and Lazaridis quickly saw the potential of the BlackBerry in the business world, but they also knew that selling it to executives would be a challenge. Decisions regarding the purchase of new technological devices are usually made by Chief Information Officers, a group of people notorious for being highly critical of new devices, especially when these devices are designed to send sensitive information via an external network. 

Taking this into account, Lazaridis and Balsillie knew that the only way they could make the BlackBerry a hit was to convince the executives of its utility. If these executives directed other managers to adopt this new technology, the BlackBerry was sure to be a success.

Fortunately, RIM was able to convince John McKinley, then Chief Technology Officer of Merrill Lynch, of the advantages of the BlackBerry and, in 1999, the company ordered the first available BlackBerrys. This was great news — and it also sparked a chain reaction that RIM couldn't possibly have foreseen.

All of a sudden, more and more people from Merrill Lynch needed a BlackBerry and other corporate giants followed suit. RIM's customer base skyrocketed. In 1999, the number of people who owned a BlackBerry sat at 25,000. By 2000, this number had grown to 165,000, and in 2004, RIM's user base boasted roughly two million people.

Indeed, the greater the number of BlackBerry owners, the greater the demand for the device. RIM's selling strategy had helped establish the BlackBerry on the market — but it wasn't strategy alone that brought success.

### 6. By delaying and misleading RIM’s competitors, Balsillie secured a strong market position for RIM. 

What's the most fundamental market principle? If demand is higher than supply, other players will creep into the market to satisfy that demand.

So, naturally, the burgeoning wireless communication market bred competition. Additionally, since the market for wireless communication devices seemed extremely lucrative, other companies were also pouring money into the technology. 

One company that had Balsillie concerned was a Californian start-up called Good Technology. Good Technology developed e-mail software that could be downloaded on various mobile devices and was compatible with multiple carriers — not just the Mobitex network. Balsillie feared that strong competitors such as Nokia would pair up with suppliers like Good Technology and produce cheaper, more flexible devices that would dominate the market. This pressure led Balsillie to a decision: he'd make RIM's software available to giant hardware players such as Nokia. 

This project was named BlackBerry Connect; hardware manufacturers would create the devices and BlackBerry would be responsible for the software and the connection to the data network.

But there was something Balsillie didn't share with his prospective partners: he never intended BlackBerry Connect to be a success.

For him, cooperating with Nokia was just a means of gaining insight into Nokia's long-term development plans — insight that would allow RIM to adapt its strategy accordingly. More importantly, the competition didn't develop their own e-mail service because they were using RIM's. Had, say, Nokia developed its own software and data network, it could have gained both more autonomy and a majority of the market.

Balsillie was essentially trying to gain time to establish the BlackBerry brand. And that's exactly what he did. Unsurprisingly for Balsillie, and frustratingly for the competition, BlackBerry Connect didn't run smoothly. 

Balsillie's strategy wasted the competition's time and helped RIM secure a strong market position. But almost as quickly as it had soared to success, the BlackBerry began plummeting toward oblivion.

> _"The key was stealthily leveraging and launching, then asking for forgiveness."_

### 7. The iPhone forced RIM to quickly develop a competitive product, but it didn’t work out. 

Remember when the first iPhone came out in 2007? It was a total game changer in the world of mobile devices.

The iPhone single-handedly revolutionized the wireless-communication device market. In January 2007, Steve Jobs announced that Apple's new device would shift the existing paradigm, connecting internet communication with a mobile phone and a touch-based iPod. And that wasn't the only trick Apple had up its sleeve: they'd made a deal with AT&T. Working with this giant US network carrier meant that the iPhone could transmit far more data than BlackBerry.

Furthermore, Apple managed to modify consumer preference. It was no longer enough for a phone to be merely functional; it now had to be beautiful, too.

The iPhone was aesthetically pleasing, but it was also powerful. The hardware inside it was so sophisticated that Lazaridis referred to it as a little Mac. It also had more to offer than the BlackBerry in terms of hardware. All in all, the iPhone made such an impression on the market that Lazaridis and Balsillie were forced to come up with an action plan: develop a competitive smartphone within nine months.

The new BlackBerry was called Storm. But since Storm had to be released on the market so quickly, it was plagued by bugs. It would sometimes simply shut down and the touch screen was far inferior to that of the slick iPhone. In short, it was a huge flop.

Unsurprisingly, Storm got terrible reviews. Thus RIM began losing its grip on the mobile-communication device market. Storm had disappointed its customers — and it wouldn't be the only RIM device to do so.

### 8. The iPad, technological difficulties and another failed product ended RIM’s strong market position. 

The iPhone changed the face of mobile technology. But Apple didn't get complacent. Rather, they scored again with the iPad.

In 2010, Apple released the iPad, which again revolutionized the market. Skeptics didn't think it would be as successful as the iPhone, but, nonetheless, the iPad set the standard for the tablet market.

All these changes and innovations had RIM on the ropes.

RIM's customer base was changing significantly. The company's revenue no longer came from corporate customers; instead, their customer base now mainly comprised non-business consumers about whom RIM knew relatively little. This shift put pressure on RIM to bring out a tablet that would keep their customers happy — and it had to happen fast.

RIM's new tablet device was called the PlayBook, and it had several problems. Instead of installing BlackBerry e-mail onto it, Lazaridis chose to install a bridge, which meant users had to install software that would connect the PlayBook to a smartphone, from which they could then download e-mails. This clunky procedure made receiving messages on the PlayBook complicated, and it frustrated people who'd been fans of the BlackBerry. Efficient e-mail communication was what the BlackBerry was all about. In comparison, the PlayBook was sloppy and impractical.

In addition to creating another shoddy device, RIM experienced some technological difficulties, which further compromised RIM's market position.

The industry standard for writing software at the time had shifted toward C++, which was more efficient than Java, the language that RIM used for its software. So Lazaridis decided to change the code of RIM's software to C++. But this move ended up draining so much of the company's time that Lazaridis had to compromise in other areas.

This combination of product failures resulted in RIM's demise. But is a couple missteps all it takes for a successful company to completely bite the dust?

### 9. Personal difficulties between Balsillie and Lazaridis hastened RIM’s decline. 

Have you ever tried to work on a project with someone you've had a falling out with? How did it influence the results? Well, Lazaridis and Balsillie eventually butted heads — and, adding insult to injury, there was a major accounting problem at RIM.

In 2007, an accounting executive held a meeting with Balsillie and Lazaridis to make them aware of an accounting problem at the company. They were engaging in a practice known as backdating — that is, allowing shareholders to purchase stocks at prices that the shares had had at one point in the past.

As long as every shareholder was informed of this practice, it was legal. But sharing this information with shareholders was Balsillie's responsibility, and he'd neglected to fill everyone in. This was the first time Lazaridis witnessed Balsillie fail and it jeopardized their relationship.

The mounting difficulties between Lazaridis and Balsillie wasn't just bad news for the duo, it also created mistrust within RIM.

Balsillie and Lazaridis continued working together, united by their shared passion for the BlackBerry, but their personality differences widened. While Balsillie was interested in sports events and hunting, Lazaridis was more interested in abstract intellectual pursuits, such as studying physics. The expansion of RIM meant that the CEOs were so busy with their tasks that they weren't able to resolve their difference and this conflict soon spread to their employees.

Lazaridis's engineers blamed the problem of Storm and PlayBook on a failed marketing campaign; Balsillie's marketing employees pointed the finger at Lazaridis's engineers. A lack of trust and responsibility was apparent in the company and this, perhaps, was the main reason for RIM's problems and eventual downfall.

### 10. Final summary 

The key message in this book:

**There is a difference between establishing a company and running a company. Customers and competitors are ever-changing; if you can't adapt, you'll die out. Failing to realize this, coupled with mistrust within the company, was central to RIM's demise.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _How the Mighty Fall_** **by Jim Collins**

In _How_ _the_ _Mighty_ _Fall,_ influential business expert Jim Collins explores how even successful companies can suddenly collapse, especially if they make the wrong decisions. He also offers leaders advice to prevent them from making the same mistakes.
---

### Jacquie McNish and Sean Silcoff

Jacquie McNish is a best-selling author and senior correspondent for _The Wall Street Journal_. Sean Silcoff, a reporter for _The Globe and Mail_, is an award-winning business writer.

