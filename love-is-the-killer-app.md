---
id: 5623e13832646600073e0000
slug: love-is-the-killer-app-en
published_date: 2015-10-21T00:00:00.000+00:00
author: Tim Sanders
title: Love is the Killer App
subtitle: How to Win Business and Influence Friends
main_color: CB352D
text_color: CB352D
---

# Love is the Killer App

_How to Win Business and Influence Friends_

**Tim Sanders**

_Love Is the Killer App_ (2003) is a guide to career success in changing times. The secret? Love and compassion. These blinks explain how love — in the business sense — can help you be smart, generous and compassionate while achieving your goals in both life and work.

---
### 1. What’s in it for me? Discover how love and compassion are the key to unlocking success in today’s business world. 

What do you think about when you hear the words "business" or "businessman"? These words still have a lot of negative connotations: egregious CEOs, money-grubbing executives and soul-destroying offices. 

This is understandable. Ruthless behavior and a winner takes it all philosophy used to be at the heart of the business world. However, as these blinks will show you, that era is long gone. 

Being compassionate, truly getting to know your colleagues and striving to create a more friendly and humane workplace are now the keys to success. Ultimately, it's about reintroducing love into business. But how do you do it? These blinks will show you.

In these blinks, you'll learn

  * why your fancy MBA is no longer enough;

  * how Intel made computer chips sexy; and

  * why compassion is the secret to a better workplace.

### 2. To succeed in today’s job market, you need a killer app: Love. 

Everyone knows that love is a fantastic feeling, so why not introduce it to the job market? This might seem odd, but love is especially crucial today when an Ivy League education isn't enough to secure you the job you want and need. 

It's true, a flashy degree from a well-renowned university was once the ticket to a lifelong career in which knowledge learned on the job was enough to get by on. But times have changed and, in job centers like Silicon Valley, people are paid according to what they know, not their job experience or alma mater. 

So what's the ticket to success in the workplace of tomorrow?

A _killer app_ is a revolutionary idea that transforms its surroundings. And today's killer app is _love_. 

But not love in the sentimental or physical sense — love in business, which involves smartly offering your three intangible qualities: _knowledge_, _network_ and _compassion._ First, knowledge is all the information you've gained and will continue to gain, whether you picked it up at school, work, or taught it to yourself.

Second is your _network_ — your web of relationships and potential collaborators. Your network lays the foundation for your success because without it, you can't apply your knowledge.

Last is _compassion_, your ability to warmly attend to those around you in a way that supports their growth. This last quality is key because the way people _feel_ about you is much more important than the way they _think_ about you.

In fact, these three intangibles are so essential that most passionate and successful people have already adopted them. These people are called _lovecats_. Basically they're high achievers who have left the loveless ways of yesterday's economy behind them, along with its aggressive vocabulary of predatory marketing, first-mover advantage and capturing market share.

For instance, Cisco's metric for success is the quality of its customer relationships, not its victories over competitors. The evidence for this lies in Cisco's basis for giving their salespeople bonuses, a decision marked largely by customer satisfaction, _not_ sales.

### 3. Being a lovecat means radiating love. 

OK, so being a lovecat is central to succeeding in today's job market, but what does that really mean?

Well, being a lovecat gives you six essential benefits.

First, it helps you _build an exceptional brand_ for yourself. Whether it's Michael Jordan, the iPhone 6 or CNN, people trust and like a solid brand. That's because without a brand, you'll just seem like a person doing something that hasn't yet been automated. Branding is key to setting yourself apart from the crowd. 

The second advantage you'll gain is _the ability to create an experience_. This is important because to earn customer loyalty and the right to charge premium prices, companies need to do more than offer a service: they need to spark interest and amusement. A great example is the way Intel has built a fun image around what's actually a boring business: computer chips. 

But it's not just products that can benefit from creating an experience; the same is true for people. So to make an impact, it's essential to create an experience that remains in the memories of your peers. 

The third benefit of being a lovecat is _people's attention_ — one of the scarcest resources in the world. 

Fourth, you'll emanate what's called the power of _positive presumption_. This is important because people are usually afraid of change, making it all the more essential to gain their trust by being a lovecat. When people trust your recommendations you'll find it easy to win any debate and make any change. 

The fifth benefit is _exceptional feedback_. This one is simple: when you offer more than just money to the people you do business with, you'll receive more from them. This could mean business tips, job leads, ideas, contacts and much more. 

The final pro of becoming a lovecat?

_Personal satisfaction_, because you can't be happy with your life if you're not happy with your job. When you're beaming out business love you'll feel genuinely satisfied.

### 4. Knowledge will take your career to the next level. 

Everyone has had a colleague who strolls down the hall offering people high fives and fake laughs. While people like this might be popular around the water cooler, they don't offer more than a few jokes and really just end up wasting people's time. You must avoid being _that_ guy.

Instead, be the knowledgeable guy, a move that will propel your career forward. It's simple: just learn enough that you can share your knowledge with others. This works because knowledge is powerful and consequential. Being able to offer it will give your career the edge it needs to advance. 

So, instead of putting on a show at the water cooler and going on and on about the latest reality TV show, share what you learned about disruptive technologies from Clayton M. Christensen's _The Innovator's Dilemma_. The difference will be clear. 

But how can _you_ become knowledgeable?

The best way is by reading books. Don't scour the news, which will only give you an awareness of what's going on today, or obsess over magazine articles that tend to be commercial vehicles full to the brim with obnoxious advertising. Instead go for books and you'll get an in-depth understanding that challenges your perspective. But magazines and articles hold some benefits, so split your time accordingly by giving them 20 percent and books 80 percent.

To decide which books to read just identify the right content to help you be the best at what you do. If you want to be a banker, you should pick the best books on investment strategies. By reading them you'll stand out at any meeting as you make valuable contributions to the conversation.

Now that you know that knowledge is crucial, it's time to learn a few practical steps to make that knowledge work for you.

### 5. Making knowledge work for you. 

Gathering knowledge is as simple as identifying your ideal learning environment and following a few easy steps. That's because finding your sweet spot for reading will improve the investment of your time. It might be a plane, a park or even your bed. Maybe you'll even decide to quit driving to work and enjoy an hour-long reading session on the train.

So, the next time you're waiting for a flight, don't fall into the same pattern as the other passengers who lazily stare at their phones, creeping ever closer to obsolescence. Instead, pick up a book!

Once you've found your favorite place to read, you're ready to learn the three steps to accumulating and sharing knowledge. 

The first trick to reading a book is to _encode_ it, which means actively reading so you digest everything. A good strategy is to always write while you read. In fact, oftentimes when you underline something slowly you're also forced to reread the sentence, thereby aiding your memory.

The second step is _processing_, which means being certain that you've properly taken in everything you've highlighted. You can do this by going over major sections before moving on, thereby enhancing your comprehension. Another great strategy is to commit to a once-weekly review of one or two past books.

The last step is _application_, a time during which you share your newly gained knowledge at work. First it's essential to know what you're going to share by visualizing a discussion. When you put down your book, be sure to summarize the key points in your head.

For instance, say you just finished the classic _How to Win Friends and Influence People_ by Dale Carnegie. Now you're capable of giving advice on how to remain likable, even when the going gets tough. Then, once you start a discussion, you can identify ideal moments to add value to the conversation. You'll find that discussions are full of entry opportunities, like when someone shares a problem, asks a question or states a concern.

### 6. A strong network is a powerful asset. 

We've all been there, sitting in an airport terminal watching droves of businesspeople waiting alone to catch their eighth flight of the month. Clearly, in a world like this, professionals are missing intimate connections to one another. In fact, when you're focused on your career it's normal to feel lonely, and as though you have nobody with whom you can share your feelings or look to for support.

But all that can change.

One problem could be that businesspeople, because of their personal shortcomings, can be reluctant to begin business relationships. However, striking up a conversation with a potential business partner is much less risky than, say, asking out a total stranger. 

After all, business people already know what they're in the market for and are always looking to expand their networks, whether by seeking out new investors, partners and employees, or tracking the competition and current trends. 

So, a fear of rejection shouldn't be the limiting factor here. Especially when you consider what's at stake: the potential for each new relationship to spur new connections that further expand your network.

That's because every new contact you make is one more person you can call up in case of a crisis or opportunity. Just think of all the people who land jobs based on the recommendations of their friends.

But additions to your network can also build your confidence. For instance, do you remember when you could walk into a party and know everyone there? How empowering that felt? Well, a strong network can give you a similar feeling of assurance.

However, there is a chance that people will use your network connections for their own benefit without showing the least bit of gratitude. But don't worry about it; the bottom line is that it didn't cost you a thing. So, while you may not gain anything, you didn't lose anything either. Just remember that you're in the business of sharing business love, and that means never saying, "You owe me."

### 7. It takes an intelligent system to build and share a network. 

Now you know the importance of a good network, but how can you build your own? The first step is to be a _collector_.

Collectors optimize the organization of their contacts by using a simple system to look up names. For example, you could break your contacts into five groups: co-workers, customers, prospects, family and friends. 

Being a collector also means always having business cards on hand, constantly exchanging them with others and storing all the cards you receive in a dedicated place. 

The next step to being a collector is the _input stage_. It works like this:

Collectors are always applying themselves to building their contacts, and that means keeping track of everything. So, after meeting someone it's important to record all potential _weak ties_ — pieces of personal information that might seem irrelevant but can start a new conversation at the next meeting. This could be anything from a hobby to a favorite sports team. And remember, always email a new contact to say what a pleasure it was to make their acquaintance.

The second step is to connect people. But you can't just randomly pair people and waste their time with poor matches. So, to succeed you need to focus on people's value: what they do, offer and need. For instance, maybe one of your contacts just lost his financial director and you know the perfect person for the job.

But it's also critical to do this quickly. If you take all week to come up with a name you'll arouse the suspicions of your contacts as they wonder, "what's this guy really up to?"

Then, once you make a connection, step back and only stay involved until the relationship can support itself. You don't want to end up as an intrusive middleman — a character notorious for creating friction. Just get out and let your contacts know you're doing so. By being upfront you'll demonstrate a commitment to their success without any personal expectations whatsoever.

### 8. Compassion in the workplace makes all the difference. 

Most people find workplace intimacy uncomfortable, but knowledge and a strong network aren't enough to win hearts and earn business. In fact, being compassionate is the new key to business world success.

But don't worry, there's nothing scary about compassion and closeness. So, while nobody wants to be the embarrassing "hug guy" in the office, there's really nothing to fear. As long as you've pre-established your credibility through knowledge and a great network, compassion will just be the cherry on top.

In fact, compassion can have a serious impact in the workplace. Would you want your kids to go to a school that separated them into dehumanizing cubicles? Definitely not, and you should feel the same about adult workplaces. That's because compassion can totally transform the way people see you and themselves when it brings the warmth of humanity into an otherwise cold environment that rarely celebrates it and deems physical contact off limits.

So, by showing compassion you can provide a service that few others do: you can be the person that makes others feel good. Even a simple smile can lighten the day of your entire department. Ultimately, this will mean a better experience with your business partners as compassion becomes catching.

Not just that, but being compassionate will also make the experience you create more memorable, something you'll struggle to achieve by conducting business as usual. That's because, "please sign here" and "thank you, Sir" don't exactly leave a lasting impression. Instead, try things like, "I love you dude. You're a rock star."

So while you could get into the good graces of others by simply sharing favors, striking deals and giving handouts, feeding the greed of your business partners will be difficult to sustain. A more lasting strategy is to care for others by offering kind words — a compelling action that no one will forget.

### 9. Train your compassion by paying attention and practicing. 

So, compassion is the ticket, but how can you cultivate it? Well, just as a vocal coach can help you hit new notes, practicing compassion can improve your ability to feel other people's needs.

Start by developing your sensory instincts. First, pay attention to body language by being observant. Notice how your colleagues respond when someone walks into a room. Do they go for small gestures like a pat on the shoulder or grander ones like a big warm hug?

As you watch your colleagues, you'll notice that some are immediately approachable and others are not. 

The second key is timing. It's important to listen for cues that tell you it's the right time to bond. 

Now you're ready to express your feelings and there are lots of ways to do so, like looks, words and gestures. For instance, you can make a point of establishing real eye contact with people, or train yourself to smile when greeting or speaking with others. Be sure to avoid phony grins, because they're actually worse than a blank expression.

Another tip is to use warm words. Try replacing "good job" with "I truly appreciate the work you've done." Lastly, use the power of a good hug to convey positive energy and perfect your handshake by copying the handshake style of the person with whom you're shaking hands.

Now that you know how to share your feelings, it's just a matter of finding the right opportunities to do so. For instance, salutations offer a great opening because every time you say hello or goodbye you get the chance to express yourself. So, seize the occasion and practice Dale Carnegie's advice: call people by their name and make them feel good about themselves.

And if you face rejection, don't manically try to make things right. Instead, wait for the right moment and keep practicing. You're sure to improve with time, and eventually you'll see that rejection is a small price to pay for the moments in which you make your colleagues brim with joy.

### 10. Final summary 

The key message in this book:

**Love is the path to business success in the twenty-first century, and there are a few tricks for spreading that love around. By generously offering your knowledge, network and compassion to your business partners and colleagues, you can radiate love, create positive relationships and earn yourself the success you deserve.**

Actionable advice:

**Take specific notes on every new contact to prepare yourself for your next meeting.**

When building your network it's essential to be specific. If you just met a new person, had a great conversation and got their business card it's not enough to write generalized notes like "He works for X company and does Y." Instead, take personal notes on the skills and interests of your new contact to help you strike up a conversation the next time you meet. For instance, "Tyler Durden was sitting next to me on a flight to Newark while I was reading about brand leadership. We started a conversation about his soap company and it turns out he's also into boxing."

**Suggested** **further** **reading: Start with Why by Simon Sinek**

_Start With Why_ gets to the bottom of why certain people and businesses are far more innovative and successful than others — even in situations where everyone has access to the same technology, people and resources. The book shows you how to create a business that inspires customers and has satisfied employees.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Tim Sanders

Tim Sanders has served as a consultant to Fortune 1000 companies, written several bestsellers and spoken all over the world. He uses his in-depth knowledge of business, psychology and marketing to help people and companies achieve their best in any economy.

