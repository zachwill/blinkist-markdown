---
id: 55a39b583738610007f00000
slug: loving-what-is-en
published_date: 2015-07-16T00:00:00.000+00:00
author: Byron Katie
title: Loving What Is
subtitle: Four Questions That Can Change Your Life
main_color: E73F60
text_color: C73653
---

# Loving What Is

_Four Questions That Can Change Your Life_

**Byron Katie**

_Loving What Is_ (2002) explains how you can conquer depression through a groundbreaking practice called "The Work" that helps you address, analyze and ultimately overcome problems to reach happiness and inner peace.

---
### 1. What’s in it for me? Learn how to conquer stress and unhappiness and live a more fulfilling life. 

Are you stressed? Unhappy with life, or exhausted by the world and all its problems? Many people are, and unfortunately, most are resigned to living a stressful life — as there seems to be no alternative.

Yet there are alternatives! There is no source of stress or anxiety that can't be conquered; all you need to do is approach what stresses you in the correct way.

These blinks introduce "The Work," a straightforward method for tackling life's many stresses. By mastering the simple steps that make up The Work, you too can live a happier, more fulfilled life.

In these blinks, you'll discover

  * why your thoughts can often be your worst enemies; and

  * why there is never any point in getting angry with the weather.

### 2. Overcome stress by understanding and mastering your own thoughts. 

You're in a long-term relationship and have been happy for years. Yet recently you've started to suspect that your partner doesn't love you anymore.

In this hypothetical situation, your stress grows as you try to find ways to make your partner love you again. But that's impossible! So you have to live with your stress and sadness. Right?

Wrong. Stress isn't caused by events or people in your life, but by your _interpretation_ of events or the actions of friends and lovers.

So it's not your partner's assumed lack of love that's hurting you — it's your interpretation of your partner's feelings. You've essentially read your partner's behavior to mean that she doesn't love you anymore. If she forgets to kiss you goodbye, you automatically think: She doesn't care for me.

How can you overcome this situation? Well, stress originates from your thoughts, so you need to _change_ your thoughts. This is where "The Work" comes in.

Start by writing down the thoughts that are troubling you. In our hypothetical case, you'd write, "My relationship is falling apart because my partner doesn't love me anymore."

Next, analyze your written thoughts by asking yourself four simple questions.

_Is this thought true?_ Reexamine your partner's actions to make sure you aren't being too rash.

_Can I be absolutely sure of its truth?_ Consider if there are any other interpretations for your partner's behavior.

_How does this thought make you react?_ Sometimes when we're anxious, we just make ourselves more stressed. You might be fishing for evidence that your partner doesn't love you because deep down, you're paranoid.

_Without this thought, who would you be?_ In this case, you'd be happier and enjoying your relationship more.

The answers to these four questions will give you a deeper understanding of your negative thoughts, so you can move on to making yourself feel better.

> _"There's only one problem, ever: your uninvestigated story in the moment."_

### 3. You can learn a lot about yourself and your feelings by turning stressful thoughts around. 

After you've answered The Work's four questions, you should have a better understanding of the thoughts that are bringing you down and making you feel stressed.

Now, it's time for the _Turnaround_.

The Turnaround is the last step of The Work. It's where you focus on _turning_ your thoughts _around_ to discover deeper truths about yourself, your feelings and your situation.

So if your thoughts lead you to feel that your partner doesn't love you and that you're losing your relationship, turn those thoughts around and see what happens.

Try approaching your problem from a different angle. Maybe your partner really _does_ love you and you've been walking down the wrong path.

Next, approach this new thought with the same four questions from The Work. You'll gain new insight into your dilemma when you closely examine the inverse of your original thoughts.

And don't stop there! Consider all possibilities and turn them all around, then see how these different scenarios make you feel.

For example, is it possible that it's actually _you_ who doesn't love your partner anymore, and this is the source of coldness between the two of you? Or are you actually just having a hard time loving _yourself_? Are you sad about another problem in your life and projecting that on to your partner?

You'll have different feelings and reactions to all of these possibilities — carefully consider all of them.

So which thought or feeling should you follow in the end? There isn't a strict rule: follow the thoughts that are right for you. Follow what feels true.

The Work only presents you with options — it doesn't give you an easy solution.

### 4. Don't stress over things you can't change – focus on what you can change instead. 

Have you ever gotten angry because bad weather forced you to cancel special plans, such as a birthday barbeque? Did you curse your luck, saying "It can't be raining, it's not fair!"

Of course we've all felt like this at one point or another. Yet when you get angry with something like the weather, you're trying to fight powers far beyond your control.

You can't change reality just by being frustrated about it. Stress won't help you — it'll just make you feel angry, disappointed and powerless. There's absolutely nothing you can do about the weather!

The key to achieving happiness isn't changing reality, but finding your true place in the realities you _can't_ change.

We often feel overwhelmed in the face of big problems, such as war, hunger or pollution. These are powerful forces, and a single individual is virtually powerless against them.

You will fail if you try to solve the world's problems on your own. You'll just get angry at the people you hold responsible. That anger might motivate you, but it also might make you run into walls.

Instead, be confident about the little changes you can enact. You'll feel better when you know that you _can_ make a difference, even in a small way.

Your actions will also come to you more easily — you'll know what to do without feeling pressure, anger or frustration.

You can't stop a huge corporation from destroying the rainforest, for example, but you _can_ stop buying their products. Accept that you _can_ do something about the problem and you'll feel much more satisfied.

> _"When we stop opposing reality, action becomes simple, fluid, kind and fearless."_

### 5. The Work is applicable in all aspects of life – career, family, relationships. 

You might think The Work is too simple to help you with life's major problems. Sure, it might help you overcome your fears or fix your relationship, but it won't address money or career issues, right?

Wrong again!

The Work can help in all areas of your life: friendship, love, work or anything else.

Let's say Joe is struggling in his business partnership. He's stressed, as he doesn't think his business partner is competent enough for the job, especially when it comes to accounting. His feelings about his partner are creating tensions between the two men.

Joe could gain a lot by implementing The Work to analyze his business situation.

While Joe might think his partner is useless, what if he turned his thoughts around? Joe might realize that his partner is weak in some areas, but certainly stronger in others. His partner just might be indispensable, even if he struggles with accounting.

If Joe focused instead on more positive thoughts, he wouldn't just feel better, he'd run his business more effectively, too.

When you apply The Work to all aspects of your life, you might discover that you've been suffering for nothing. Sometimes the thoughts that trouble us aren't obvious, and sometimes they're so obvious that we don't even realize that it's our thoughts that are causing the problem!

Do you find yourself struggling to reach career goals, despite all your hard work? If so, consider what you're actually working toward. Do you _really_ need to be financially successful to be satisfied? If not, you might be pressuring yourself to work toward something you don't actually want or need.

Are you sure you aren't looking for happiness instead? The Work will help you find your true path!

### 6. Final summary 

The key message in this book:

**Don't let life's challenges ruin you. Stress, fear and other negative emotions all spring from how you interpret your environment, not the environment itself. Use "The Work" to get a deeper understanding of your thoughts. When you master your own mind, you'll be able to find inner peace and truly be happy.**

Actionable advice:

**Write down your thoughts.**

You can make more sense of your thoughts if you can read them! So don't do The Work in your head, write down your thoughts so you can fully grasp everything that's going on in your mind.

**Suggested further reading:** ** _The_** **_Power_** **_of_** **_Now_** **by Eckhart Tolle**

_The_ _Power_ _of_ _Now_ offers a specific method for putting an end to suffering and achieving inner peace: living fully in the present and separating yourself from your mind. The book also teaches you to detach yourself from your "ego" — a part of the mind that seeks control over your thinking and behavior. It argues that by doing so you can learn to accept the present, reduce the amount of pain you experience, improve your relationships and enjoy a better life in general.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Byron Katie

Author and speaker Byron Katie in the 1980s personally suffered a severe bout of depression that lasted several years. Based her own experience, she now shares her method of conquering depression and unhappiness with people all over the world so they can lead a more fulfilling life.

