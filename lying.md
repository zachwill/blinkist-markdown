---
id: 59e3b6f1b238e100062ad1bd
slug: lying-en
published_date: 2017-10-16T00:00:00.000+00:00
author: Sam Harris
title: Lying
subtitle: None
main_color: 538058
text_color: 4B734F
---

# Lying

_None_

**Sam Harris**

_Lying_ (2011) explains why the act of telling lies is so dangerous. And that means all lies, from the tiny lies that people tell on a daily basis to the massive lies sometimes told on the world stage. All in all, it's always better to tell the truth.

---
### 1. What’s in it for me? Learn why we all need to stop lying. 

Lying is one of those things that everybody does, though most people wouldn't readily admit it. Lies are extremely useful, whether you're blaming the loss of your homework on your dog, attempting to avoid getting a parking ticket or just trying to be nice when someone gives you a gift you don't like. Lying, it would seem, is not only handy; it's a part of being human.

Okay, so what's the problem?

Well, even the tiniest of lies plays a part in the normalization of a system of untruth, and can lead to much larger lies — lies that have the power to start wars or make people lose faith in science and research. In these blinks, you will learn why lying is a big problem for humanity and why we should do all in our power to start telling the truth, even if that means hurting your grandma's feelings when she knits you a sweater three times too large.

These blinks also explain

  * that lies have made us distrust the leaders of society;

  * why witnessing someone you trust tell a lie has negative consequences; and

  * that lying might seem simple but is actually an energy thief.

### 2. There are two types of lie; neither should be told. 

Remember being told never to tell lies as a child? No one wants to be dubbed a liar, and while most people wouldn't dare tell a monumental lie, many often tell little lies that they perceive to be insignificant.

The majority of people avoid big lies because of the disastrous effects they can have. Such lies can end careers or result in jail time, can ruin a person's life or even throw entire societies into disarray.

Despite the detrimental effects of big lies, governments and the wider media continue to spread them. This has created a general feeling of distrust toward world leaders on a global scale. For instance, justification for the Iraq War hinged, in large part, on a lie; the Bush administration claimed that the country was hiding weapons of mass destruction — a claim that turned out to be untrue. This blatant deception caused many people to become skeptical of foreign policy in the United States.

Funnily enough, little lies are usually not regarded with the same moral rectitude. In fact, people generally think such lies are okay, since they're often used to spare the feelings of others. But white lies do damage, too.

Imagine a family getting ready to host guests for a week. Prior to the guests' arrival, the husband says to his wife in the presence of their young daughter that he wishes they weren't coming to stay. The guests arrive and thank the man for his hospitality, and he chirpily responds that he's happy to see them — but his daughter pipes up and repeats what he said earlier.

This clearly puts the man in an awkward position. He can't say that he's _not_ happy to see his guests; however, denying what he previously said also sets a bad example for his child. If he had welcomed his guests in more ambiguous terms — by say something like "That's why we have guest rooms!" — this whole situation could have been avoided.

Little lies may have less of an impact than big lies, but, in the long run, they can also have devastating results.

> _"Every lie is an assault on the autonomy of those we lie to."_

### 3. Lies have a negative effect on your relationships. 

You'd most likely do anything for those you truly care about. If, for instance, your relationship with your spouse, or with your parents, was in jeopardy, you'd probably be willing to tell a lie if you thought it could salvage the relationship. However, lying to someone close to you almost always destroys that closeness.

The problem with lying is that it erodes trust. Even if you're not the one who's being lied to, the fact that you know a person is capable of lying is bound to make you distrust them.

Say you witness a friend or a family member express a false opinion to someone else. You'll feel uneasy, because you'll immediately wonder how many times they've fed you such lies. And if they can lie about such a small thing so easily and convincingly, then can they lie when it comes to more serious issues, too?

The truth is, you want to be able to rely on those close to you. A best friend wouldn't let you leave the house in unflattering clothes, and family members who truly care about you wouldn't let you pursue a career that's not right for you.

Telling the truth can be very difficult. You may be privy to information that could cause someone a great deal of pain or completely change their life. Such knowledge comes with a lot of unwanted responsibility, but it's your duty to tell the truth.

Imagine that you know your friend has been cheated on but you're reluctant to tell them. You feel guilty for withholding the information, so you make excuses not to see them. Additionally, you're tacitly allowing your friend to unwittingly remain in a toxic relationship.

Although telling the truth may cause your friend great pain in the short term, it will vastly improve their well-being in the long term. Not only will your truthfulness allow them to make an educated decision about the relationship; it will be proof positive that they have at least one honest and supportive friend.

### 4. Lies can cause mental stress. 

Have you ever been so impossibly deep in a lie that you just know you're going to be found out? This feeling of entanglement goes hand in hand with a lot of stress and anxiety, and the only way to avoid it is to always tell the truth.

It takes a considerable amount of effort to lie, since you have to keep track of everything you've fabricated. The truth, on the other hand, requires no monitoring whatsoever.

No matter the nature of the lie you're telling, you'll tend to relay different information to different people depending on how you think they'll respond. This is where the confusion stems from: you're destined to forget what you've said and to whom.

And the situation can get even more complicated. You'll start worrying about whether the people who you've told conflicting stories to will start talking among themselves and notice the inconsistencies. You may have lied in an attempt to protect them from unpleasant information, but being exposed as a liar will far outweigh any good that your lie may have generated. Therefore, it's better to tell everyone the honest truth and leave nothing to be revealed.

Frankly, maintaining a facade is hard work. When you lie, you're essentially preventing another person from accessing reality. It's ludicrous to think that a person has any right to prohibit someone else from knowing the truth, or to determine which information they should be permitted to have access to.

As important as it is to refrain from lying to others, it's also necessary to not lie to yourself. When you're disingenuous about who you really are, you're forced to continually present a false image of yourself to the outside world. This can have a devastating effect on your self-esteem — you'll become increasingly dependent on the maintenance of that image and less connected to who you truly are.

### 5. For a better world, we should avoid telling lies of any kind. 

To improve your own life and that of others, you should vow to no longer lie — it'll be difficult but entirely worth it.

If you even allow yourself to tell the smallest of lies, the act will become natural to you and it can eventually develop into a perilous habit. There's the risk that the truth will become so alien to you that you'll find it challenging to be honest — even when it's necessary for your own well-being or for that of the people around you.

Lying is also dangerous because people tend to remember lies and wholeheartedly believe them. Take the case of Andrew Wakefield, a doctor who knowingly published false research that linked vaccinations to the development of autism in children. Although he had his medical license revoked and his research publicly disproved, many people still hold fast to this lie, since they already accepted it as true.

Issues like this occur so readily because lying is currently seen as normal behavior. By ceasing to tell tiny lies, the likelihood of big, damaging lies being told will decrease, because lying will eventually cease to be the norm.

If you work to promote trust instead, you'll help to improve the state of affairs on both a personal and a political level. Next time, tell the person who bought you an imperfect gift how you truly feel — that way, they'll be able to spend their money better in the future, and they'll also have greater respect for you because you were honest.

Imagine if politicians began telling the truth. They would not only win the respect of society; they'd also create a more trusting and open environment on a global scale. This in turn would encourage more forthrightness and, eventually, lying might die out entirely. And that, in all honesty, sounds like a pretty positive future.

> "Lies are the social equivalent of toxic waste: Everyone is potentially harmed by their spread."

### 6. Final summary 

The key message in this book:

**No matter how small or large, lies should never be told. You may sometimes think the lie you're about to tell is harmless, but remember: it isn't. It will negatively affect you and those around you to an unforeseeable extent. For your own benefit and the greater good of the world, it's better to stick to the truth.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Trust Me, I'm Lying: Confessions of a Media Manipulator_** **by Ryan Holiday**

through online media sites called blogs. By detailing his experiences with multi-million dollar public relations campaigns, the author takes us behind the scenes at today's most popular and influential blogs to paint an unsettling picture of why we shouldn't believe everything that is labeled as news.
---

### Sam Harris

Sam Harris's books have been translated into more than 20 languages. His other titles include _The End of Faith_ and _Free Will_. Five of his books have made the _New York Times_ 's best-seller list. He is also the host of the podcast _Waking Up_, which discusses spirituality.

