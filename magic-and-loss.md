---
id: 587b45281222e80004bb2688
slug: magic-and-loss-en
published_date: 2017-01-18T00:00:00.000+00:00
author: Virginia Heffernan
title: Magic and Loss
subtitle: The Internet as Art
main_color: 3747B2
text_color: 3747B2
---

# Magic and Loss

_The Internet as Art_

**Virginia Heffernan**

_Magic and Loss_ (2016) explains the profound effects the internet has had on our society, for better or for worse. These blinks explore the true magic of the online world, while shining a light on the social losses that come along with it.

---
### 1. What’s in it for me? Discover how the internet has changed our approach to the world. 

At a conference in 1995, Bill Gates allegedly said, "The Internet is just a passing fad." Whether he said it or not, this wasn't a particularly uncommon standpoint back then. In its early days, when the internet was painfully slow and nerds made homepages with pictures of their dogs, few saw its real potential.

Fast forward 20 years and it's almost impossible to imagine a life without the internet. It has made its way into every facet of modern life, from the way we find information and consume products, to how we socialize. In short, it's magic.

And yet, as we've immersed ourselves in this brave new online world, we can't help but feel a strange sense of loss: there's something uncanny about our Facebook friends' profile pictures, and although we can listen to any music we want whenever we want, there's a nagging nostalgia when we think back to the time when every record we bought really meant something.

This tension between the magic and the loss is what these blinks are all about.

In these blinks, you'll learn

  * why games and apps can make us less able to cope with the real world;

  * how our compulsive reading could kill us (and others); and

  * how Steve Jobs changed the way we listen to music.

### 2. Well-designed mobile apps take the edge off the internet’s chaos, but leave us with a social problem. 

The internet, in all its never-ending chaos, can be a bit jarring at times. Loud, annoying ads lurk behind every click and an incessant stream of spam seems to pour onto your screen. But somewhere in this madness, people manage to find beauty and peace.

That's in large part due to well-designed mobile applications, or apps, which counterbalance the internet's messy side — a side that is to be expected in a world that's more connected than ever.

After all, billions of people around the world are online, posting, commenting and linking. Such high traffic means the internet is jammed, chaotic and busy. And that's where the brilliant graphic design of well-made apps, free of any confusing words or unnecessary visuals, comes in to provide an oasis from the internet's wild storm.

Just take the puzzle game app, _Hundreds_. It doesn't require any wordy explanations like "when you do x, y will happen." It simply explains itself through graphics.

But such apps or games that are easily understandable through intelligently designed graphics can also keep us from fully learning to cope with the more mundane aspects of life, like boredom, stress and disorder. These apps offer a convenient pause from the stress and hassle of daily routines.

For instance, how many times have you reached for your phone and the comfort of Angry Birds to take refuge, far away from the noise of your office? Such dependency is problematic because, if people always seek such escapes, they'll miss out on essential learning opportunities, like how to tolerate things that lack instant gratification: waiting in long lines, listening to lengthy or boring lectures and persisting at a monotonous but essential task.

### 3. The internet’s vast collection of knowledge has shown us how little we know, but artificial intelligence might throw us a bone. 

When people think of "reading," they tend to visualize someone poring over a book or newspaper. But actually, scrolling through Facebook posts or checking matches on Tinder is another kind of reading, one that we do nonstop.

The internet has taught us that we don't know enough, compelling us to read all the time, wherever we are — even if it almost kills us. This is only logical since the web is packed to the gills with information, most of which we can access relatively easily.

But looking at the internet only serves to reinforce how little we know about the world, people, great music, international politics and all manner of other topics. This realization compels us to frantically read in an attempt to catch up with the overwhelming and constantly growing amount of knowledge that we feel ourselves to be lacking. In fact, we're so consumed with reading that we readily risk our lives, as well as those of others by, for instance, checking our news feeds while blindly crossing the street.

In short, technology is swamping us with knowledge — but this doesn't have to be the case. Technology could be a powerful tool to help us handle all the information it puts at our fingertips, and even do some of the thinking for us.

For instance, the alien nature of machines might scare some people, but it's possible that, some day, we won't have to think or worry about the things we don't know or can't decide on. We might not even need to read or learn since machines will be able to do all those things for us.

If that day comes, we'll no longer have to worry about major international dilemmas, like the right way to respond to the war in Syria, or even figure out minor confusions, like whether avocado or cucumber make a better facial mask.

### 4. The internet transformed our language from verbal to visual, fostering a distorted version of reality. 

When was the last time you sent an emoticon in a text? If you're like most people, it probably wasn't too long ago, and the popularity of these symbols points toward a more generalized phenomenon: the language we use today is far more visual than that used before the dawn of the internet.

As a result, people include tons of pictures, graphs and icons in their daily communication. Just think of the countless smiley faces, GIFs and memes you find popping up all over your phone and computer. Or consider Instagram, Flickr, Pinterest, Snapchat and the innumerable other visually-based social media networks; they're all platforms created for the express purpose of digitally sharing visual content.

But as these new visual forms of communication gained traction, the traditional function of photography fell by the wayside. Photoshopping now tempts people to present enhanced versions of themselves, and their lives, to a vast online audience.

This represents a marked change from how photography used to function. Just a few decades ago, photography was all about the faithful documentation of an event for those who weren't there to see it. For instance, if you couldn't make it to your family vacation or missed out on your class reunion, someone could at least show you photos of what the experience had looked like.

Then the technology changed. All of a sudden, everyone had a smartphone with a built-in camera. These accessible, on-the-go cameras opened up endless possibilities, like taking a selfie and editing out anything you find undesirable about your image.

Not only that, but with apps like Instagram and Flickr, people began adding filters to their photos, happily embellishing the reality they present online, rather than objectively documenting it.

> _"Ground zero for visual literacy is the Shiva-the-destroyer app, Instagram."_

### 5. Online video has turned consumers into producers and dramatically transformed the face of content. 

Did you know that for each minute you've spent in these blinks, 100 hours worth of video have been uploaded to YouTube?

YouTube has become vastly popular, and for all those people who used to be restricted by their amateur videography, online video has a solution: the producers are now the people who create the platforms for consumers — who are also producers — to create content for themselves.

This is a major shift since, only a few years ago, the great producers of cinema were, for the most part, the only ones who had the privilege of telling stories. But today, the great "producers" are the creators of platforms that enable everyday people to share their stories.

These platforms opened up a fantastic opportunity for people to share their online movies. And you don't need to be Woody Allen or Roman Polanski to tell your story through video; just about anyone can make and share them.

In the case of YouTube, it's simple to tell a story, regardless of your background. You need only shoot it, upload it and wait for the views to roll in. This is the quintessential illustration of the way consumers have become the producers of their own content.

And with this change in roles came a change in content, since a YouTube video is a medium in and of itself. Unlike traditional films, these videos are unencumbered by the rules of genre and range from pet videos to extreme sport stunts (and fails) to DIY tutorials and even snippets of TV shows and movies. Essentially, YouTube is whatever you want it to be.

### 6. iTunes made music readily available to the masses, but listening to it on iPods has social consequences. 

Can you remember Napster or Limewire, those anarchic programs that let you share music files with people around the world? Well, those days of the young internet's piracy are long gone. Now you don't have to wait forever to download a song, only to realize you got the wrong version.

In recent years, iTunes has made music more accessible than ever. One of the genius inventions of Steve Jobs' company Apple, this platform lets people access and purchase music of all types with ease. As a result, it's much easier to listen to your favorite songs — and the best part is, everyone has access.

You simply go to the store, search for the music of your choice, push "purchase" and, before you know it, you're listening to your favorite song on whatever device you want. But while they may be convenient, devices like iPods aren't necessarily the best choice for listening to music, especially because of their inherent antisocial element.

After all, the music people listen to on devices like this isn't "real" music; rather, it's a mere digital representation of it. In other words, when you listen to an .mp3, you're not listening to the strings of a guitar being plucked or a drum being beaten, or even an acoustic reproduction of it, as would be the case on a vinyl record.

On the other hand, people perceive it as music. So why doesn't it feel completely right?

Some people hypothesize that this is due to iTunes being a medium of hit singles rather than whole albums, which presents a more fractured listening experience. Others suspect that it's a result of how .mp3s are compressed to filter out any seemingly extraneous sounds.

But the main reason that an iPod doesn't quite hit home is that listening to music used to be a shared experience. By putting in your earbuds, you're actively shutting out everyone around you.

### 7. Final summary 

The key message in this book:

**The internet is a massive treasure trove containing everything you've ever wanted. It has enabled the sharing of information like never before and has become an utterly indispensable aspect of our lives. However, this accessibility has its costs as well, namely ones of a social variety.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Here Comes Everybody_** **by Clay Shirky**

Thanks to advancements in communication technologies and the widespread availability of the Internet, we can now contact one another and share information at unprecedented rates. _Here_ _Comes_ _Everybody_ explains how these changes aren't just affecting the way we communicate; they're affecting the way we organize, too. As the obstacles and expenses of communication diminish and the reach of our communication expands, we're now experiencing a significant shift in the ways we get together.
---

### Virginia Heffernan

Virginia Heffernan holds a PhD in English Literature from Harvard University. As a journalist, she has worked for the _New York Times_, _Harper's_ and _Yahoo! News_, among other publications, and is a highly sought-after speaker as well as a cultural critic.

