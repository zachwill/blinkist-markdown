---
id: 56c065264f33cc000700004c
slug: magic-words-en
published_date: 2016-02-18T00:00:00.000+00:00
author: Tim David
title: Magic Words
subtitle: The Science and Secrets Behind 7 Words That Motivate, Engage, and Influence
main_color: B02A32
text_color: B02A32
---

# Magic Words

_The Science and Secrets Behind 7 Words That Motivate, Engage, and Influence_

**Tim David**

_Magic Words_ (2014) discusses seven words that will motivate people into action. You need people to work with you, no matter your goals, and these blinks offer advice on building meaningful connections and getting your friends and colleagues to work with you on what you want.

---
### 1. What’s in it for me? Learn the seven little words that will make you extraordinarily persuasive. 

Imagine this scenario: your teenage daughter, after a night out with friends, comes home late. She has important homework to do — but she doesn't want to do it. Instead, she would rather sit and watch TV. 

What do you do?

Probably, you get really upset, and your daughter probably ends up locking herself in her room. But what has this accomplished? The homework is still unfinished and now you're all worked up.

There's a way to avoid such dissatisfying situations. By incorporating some simple "magic words" into our arguments, we can persuade and motivate others to see things our way — and these words are exactly what the following blinks are all about. 

You'll also learn

  * what makes names so powerful;

  * what a But Eraser is; and

  * why people called Peter prefer Pepsi to Coke.

### 2. Magic words connect you with others by motivating them. 

What's a _magic word_? Maybe words like "Abracadabra" or "Alakazam" come to mind — but, outside the realms of fiction and illusion, "magic word" means something entirely different.

Magic words motivate others, and thus make you a more effective communicator. And, of course, communication is key no matter what you want out of life, whether your dream is starting a business or devoting yourself to raising your children.

Yes, you can try to force people to do what you want, but that rarely works. After all, people don't like to be bossed around. That's why it's important to _motivate_ their actions — and that's where magic words come in.

Furthermore, magic words help you express yourself more clearly. There's a lot to consider when you're trying to influence other people. In fact, Albert Mehrabian, a noted psychologist, wrote in 1967 that words only constitute about seven percent of what we communicate — the rest is body language and tone of voice.

Luckily, magic words are here to help. They'll improve your communication, making your speech clearer even if your body language and tone aren't perfect.

A word of warning, however: magic words only work when they go hand in hand with a strong, positive human connection. We increasingly communicate with each other in cold and unfriendly ways. So you have to start caring more about other people if you hope to get them to cooperate with you and help you work toward your goals.

So what _are_ the magic words? Read on to find out.

> _"It's pretty hard to be a leader if no one is following. If you want to move mountains, then you need to be able to move people."_

### 3. The most important magic word is “yes” and the word you should avoid the most is “no.” 

How many times do you use or hear the word "yes" in your day-to-day life? Probably, you could handle a few more yeses in your life.

Why? Well, all humans fear rejection. We've evolved to be that way: being connected with others by being part of a group helps us survive. So the word "yes" is ingrained in our psyche; we love hearing it. It's the ultimate magic word.

So, when you're trying to get someone to do something for you, ask questions that elicit a "yes." For example, don't say, "Please fix this for me." Instead, ask, "Can we fix this?" Because, if you can get someone to say yes, they'll be more likely to actually do it. 

In fact, you're more likely to be successful if you can get the other person to say yes two or three times before you get to the actual request. One study found that a group of salespeople increased their success rate from 18 to 32 percent by inducing the people to say yes at least three times before coming to the sale. 

These yeses don't have to be complicated, either. All you have to do is get the other person to say it.

And there's another tactic that's just as important as using "yes" — avoiding "no". 

Stay in "yes" mode. When you mention something negative, the conversation moves away from positivity toward feelings of fear, caution and rejection. 

So if an airline captain tells her passengers "not to be concerned," that won't actually make them feel better. All negative words, even if they're being negated, inspire negative feelings. 

Don't _overuse_ "yes," however. Remember that it's powerful. Use it wisely.

### 4. Grab attention by using names and direct attention by saying “but.” 

Imagine you're in a noisy, crowded bar. You can barely hear the person right next to you, but someone across the room says your name and, just like that, they have your attention. Why does this happen?

We're biologically programed to pay attention to the sound of our name. That's why names are such great magic words. 

We've evolved to focus on sudden, loud exclamations aimed directly at us because it helps us stay safe. So, if you want to grab someone's attention and focus, say their name before you make your point. It's that simple. 

People also feel more important and more highly valued when they hear their name — or even just something similar to it! That's why it's been statistically proven that people named Cathy prefer Coke to Pepsi, and people named Peter prefer Pepsi. The number of women named "Georgia" is 88 percent higher in Georgia than it is in any other state. Names are powerful! Use them. 

The next magic word might come as a surprise to you: "but."

"But" has a very specific power. When you say the word "but" you decrease the impact of the preceding phrase (an effect called the _But Eraser_ ) and emphasize the phrase that follows (an effect called the _But Enhancer)_.

So start out with the information you want the person to forget, then use a "but" and end with the information you want them to remember. A physician trying to calm a patient, for example, might first mention the injection the patient is about to receive and then encourage the patient to relax by looking out the window.

> _"For all the value that using someone's name brings to the table, you can really blow it big time by forgetting their name."_

### 5. “Because” and “if” motivate people to act and think. 

People naturally want to understand the reasons behind the things they see and do. If you can give them a reason, they'll be more motivated to act. 

That's why "because" is the next magic word. A Harvard professor once did a study on the power of "because" by using the line for the photocopy machine in her office. First, she tried to cut the line without giving any explanation for her rudeness. She was successful 60 percent of the time. 

For the next round, she used "because," but gave completely illogical reasons, like, "because I have to make some copies." The success rate jumped to 93 percent.

"Because" is powerful, but remember: you can't _force_ someone to feel motivated. They have to find that motivation on their own. 

So if your company has recently had a problem with deadlines, ask your employees why they think there's been a problem with timing. That encourages them to search for their own "because" and, once they find it, they'll be much more motivated to act on it. 

The next magic word, "if," induces people to think more hypothetically and creatively. It has another powerful ability, too: it combats reverse psychology. 

Matthew T. Crawford and his colleagues once conducted a study on "if" by asking participants to gamble on one of two equally skilled football teams. If one of the researchers suggested they bet on a particular team, the participants bet on the _other_ team 76.5 percent of the time, just to show their free will.

However, when the researchers gave them advice using an "if" statement, like, "If you don't bet on them, think how you'll feel when they win," 73 percent _took_ their advice. 

So use "if" to guide people's thoughts in a certain direction. If the person insists they can't do something, don't just say, "Yes, you can." Instead say, "What would you do if you could?"

### 6. “Help” is useful when delegating responsibility and “thanks” keeps motivation levels high. 

How do you feel when someone asks you for help? It makes you feel important and needed, doesn't it? That's what makes "help" such a powerful magic word. 

However, if you need help or someone offers you help, you have to work with that person in a specific way. Say you ask for help with writing some code, for example, and the person who volunteers is a beginner. Should you let them give it a go?

Yes. You never know what beginners might come up with. He might develop some innovative new way of getting the program right. And, if he fails, he'll learn from it and be honored that you let him try. Either way, he'll become a happier and more satisfied employee. 

The last magic word is "thanks." "Thanks" is important in business: businesses need to thank their customers and bosses need to thank their employees. "Thanks" keeps customers happy and makes employees feel more valued and motivated.

An employee always feels more motivated to stay at a job if they see value in it. So employees should feel accomplished and proud when they finish a project or their boss gives them praise. If they don't feel appreciated, they might not stick around. 

Furthermore, the more often you repeat the same task, the less satisfying it is. When you get used to doing something, it doesn't feel rewarding anymore and your boss will probably stop praising you for it. 

So keep your employees happy by showing appreciation for all they do. Don't forget — your business couldn't exist without them! You can start by simply smiling more often. Aim to create a thanks-culture in your company, where everyone's contribution is recognized and valued.

### 7. Final summary 

The key message in this book:

**Communication is the anchor of any kind of human interaction. So, if you want others to work with you, build a strong connection, and make careful use of the** ** _magic words_** **. Elicit "yeses" and avoid "nos," direct attention with "but," motivate others with "because" and "if" and show appreciation with "help" and "thanks." Words are simple, but they have an incredible amount of power when used correctly.**

Actionable advice:

**Use a "no" when necessary.**

We saw before that "no" is counterproductive, but if someone is trying to exploit you, stay firm. Don't let someone take advantage of you: always use a "no" if you need it.

**Suggested** **further** **reading:** ** _The Like Switch_** **by Jack Schafer and Marvin Karlins**

In _The Like Switch_, author Jack Schafer explores the realm of nonverbal social cues and other communication practices that draws people to one another. A former FBI agent and doctor of psychology, Schafer presents useful strategies to make new friends and influence people. To find these blinks, press "I'm done" at the bottom of the screen.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Tim David

Tim David is a professional magician, mentalist and trainer. He runs a popular YouTube channel and focuses on helping others build more effective communication skills.

