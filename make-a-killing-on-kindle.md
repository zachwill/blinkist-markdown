---
id: 53a157ce6335360007260000
slug: make-a-killing-on-kindle-en
published_date: 2014-06-17T00:00:00.000+00:00
author: Michael Alvear
title: Make a Killing on Kindle
subtitle: The Guerrilla Marketer's Guide to Selling E-books on Amazon
main_color: E06136
text_color: B04C2A
---

# Make a Killing on Kindle

_The Guerrilla Marketer's Guide to Selling E-books on Amazon_

**Michael Alvear**

_How_ _To_ _Make_ _A_ _Killing_ _On_ _Kindle_ provides a simple step-by-step marketing strategy that will enable you to get your e-book onto Amazon's top-ten bestseller list (in your category) in a matter of weeks.

---
### 1. What’s in it for me? Learn how to market your e-book successfully in just 18 hours. 

For the longest time, getting a book you'd written into print meant putting yourself at the mercy of the gatekeepers: the publishing houses and critics.

But then came the digital revolution and the internet, giving aspiring authors direct access to the marketplace for the first time ever.

E-publishing certainly has made it easier than ever to become an author — but not necessarily a successful one. In addition to being a good writer, you also need to know how to market your work effectively, and how to exploit providers like Amazon.

Enter Michael Alvear's _Make_ _a_ _Killing_ _on_ _Kindle_. As these blinks show, Alvear's book is a great help to authors who are struggling to make a good profit from their books, or to those who are just starting out. In fact, the author claims that in just 18 hours you can implement a marketing strategy that will get your book into the top 20 in your category in a matter of weeks.

In these blinks, you'll also find out:

  * why the most important reviewers of your book are people you know,

  * why Twitter is useless for new authors and

  * why it's a good idea to place your introduction _before_ your table of contents.

### 2. Two myths you should ignore when self-publishing: that you must build an author platform and a social media campaign. 

You've finally finished writing your book and you're planning to self-publish — congratulations! Since you'll probably want to publish it as a Kindle e-book, it's now time to attract potential readers.

But how are you going to do that?

Many books and articles about self-publishing recommend that you follow the example of popular authors: in other words, spend months constructing a platform by getting people to subscribe to your blog and building a massive email list and Twitter following.

But this is a myth: unless you're already a famous author with a loyal fanbase, this is _not_ the best strategy for you.

An author platform is the number of people following you — via social media, articles, email lists, and so on. However, in order to sell a lot of books using this platform, you need to have hundreds of thousands of followers.

Why?

If you have a blog that contains an ad banner, the average clickthrough rate is three-tenths of one percent. Of these three-tenths, around four percent will buy your book.

So, if your blog has 10,000 subscribers, only 30 of them will click on an ad for your book, and approximately 1.2 of them will actually purchase the book.

Therefore, if you're a new author, with just a hundred or so fans, the author-platform approach simply won't work.

The second myth is that you need a social media campaign to sell your Kindle book. In fact, using social media for this purpose is a waste of your time.

While using social media is a good way attract followers, 90 percent of them don't own a Kindle device — in fact, just eight to ten percent of the United States' population own a Kindle e-reader.

Therefore, spending a lot of time and effort attracting followers on social media won't lead you to your potential customers.

So what can you do to attract readers to your book? As you'll see in the following blinks, there are several other tools you can use.

> _"You need hundreds of thousands of followers to make an author platform work."_

### 3. Choosing the right keywords and categories will guarantee that you attract the right customers. 

When trying to attract readers, there is one fundamental principle to remember: If a reader cannot find your book, they cannot buy it!

Readers search for books by entering keywords into online bookstores, or by browsing categories where they've previously found an interesting book, so you have to ensure that your book is visible in these areas.

For instance, you need readers to discover your book whenever they search for a similar one. To ensure that happens, you have to use keywords that prospective customers will search for. These keywords should be embedded in the book's title, description and search engine profile (the seven words you use to describe the book).

But how can you know what keywords people use in their searches? Well, Amazon certainly won't share that information with you — but Google will.

For example, when the author was publishing an e-book called _Flirty_ _Text_ _Message_ _Helper:_ _Witty_ _Text_ _For_ _Clever_ _People_, he searched for the phrase "Flirty Text" on www.google.com/AdWords and then received a list of related keywords and the number of searches made using them. From this list, he could select the most suitable keywords for his book.

In addition to choosing the right keywords, you also must ensure that your book is placed in a suitable category.

To do this, you have to find the categories that similar authors use for their books, and categorize your book in the same way. That way, any reader interested in your competitors' books will be able to discover your book in the same category.

However, you don't want to place your book in a category that's oversaturated with competition. Your main aim is to get your book into the top-ten list for your category, because those titles practically sell themselves, so you should pick a category where accomplishing this goal is easier.

For example, the author didn't place _Flirty_ _Text_ _Message_ _Helper_ in the "Dating" category, as he knew he couldn't compete there. Instead, he categorized it under "Etiquette," where the sales were good but the competition was minimal. Within a month, the book made it into the "Etiquette" top ten.

> _"Changing categories took my book from OK to OMG . . ."_

### 4. Your book's title and cover design are major attraction factors. 

The cover of your book is the first thing that readers see. So if the title is not clear and the design is ineffective, the battle to sell your book is lost before it's truly begun.

What can you do to ensure that both the title and design are effective?

First, the title: It should be short, clear and attractive.

Why? Because people tend to _scan_, rather than _read_ — and this is particularly true of e-book readers, who don't read every word carefully. For this reason, the book's title should be formulated so that it can be read and understood in just five seconds.

For example, could you say what each of these titles is about?

  * _Hopeful_ _Heart,_ _Peaceful_ _Mind:_ _Managing_ _Infertility_.

  * _Managing_ _The_ _Stress_ _Of_ _Infertility:_ _How_ _To_ _Balance_ _Your_ _Emotions,_ _Get_ _The_ _Support_ _You_ _Need,_ _And_ _Deal_ _With_ _Painful_ _Social_ _Situations_ _When_ _You_ _Are_ _Trying_ _To_ _Get_ _Pregnant._

The second title is actually much quicker to grasp: the bulk of the information has been transferred to the subtitle, which readers will look at more closely only _after_ they've been grabbed by the title.

In fact, this particular book was in the Kindle store for two years, selling just a couple of copies a month. However, when the author changed the title from the first to the second one, it made it to the top ten books on infertility within two weeks!

Secondly, since readers actually _do_ judge a book by its cover, it's vital that you avoid trying to design it yourself. No one buys a book that has a poorly designed cover, so you should hire a designer with the necessary expertise and skills to do a good job.

Although this can be expensive ($300–$2,000), you should think of the hire as an investment, since a professionally designed cover will help you to sell more books.

In addition to radically improving the appearance of your book, professional design will distinguish it from rival books with "homemade" covers.

> _"In the mind of a book buyer, cover means quality."_

### 5. Your book description should be a tool for the customer to get more engaged with your book. 

After you've attracted prospective customers with effective keywords and a well-designed cover, you then need to get them _engaged_ with the book itself.

How?

The first thing the reader reads is the book description. Depending on how interesting the reader finds this, she may or may not look inside the book to read the actual content.

This description should be written from the customer's perspective, not the author's.

One way to enter the mind of your prospective customer is to research the descriptions of books similar to yours, to see whether or not they intrigue you enough to want to read on. Once you've found an enticing, effective description, allow it to influence your own book's description.

After the book description, the "Look Inside" feature is the final battle you have to win in getting the prospective customer to make her decision.

On Amazon, customers can browse a free sample of your book. However, this sample is limited to ten percent of the book, so you should ensure that the material that can be previewed is that which is likely to be the most interesting to prospective readers.

One way to do this is to place the introduction _before_ the table of contents, as the author did with the present book. He also added a section called "Why You Need To Read This Book."

You should also ensure that the information in your table of contents is clear and precise in order to pique the reader's interest.

For example, consider the difference between "How To Set Up Your Blog" and "Which Platform Is Best For Your Blog — Wordpress or Blogspot?"

The latter title is more specific and precise, and will generate more interest in the book's content.

### 6. There are several price strategies; use the right one to get customers to click the “Buy” button. 

There is no single, correct way to price your book. Rather, there are several strategies that you can try until you get the maximum revenue possible.

The first strategy is "Rock Bottom Pricing," in which you price your book at the lowest price possible: 99 cents.

However, this strategy has its advantages and disadvantages:

The main advantage is that everyone can afford to spend 99 cents on a book. Moreover, at that price, readers won't feel they're taking a risk: if the book turns out to be of poor quality, they'll still feel that they didn't lose out. Also, because this price point will encourage more sales, you increase the chances of your book ranking in the top ten or 20 in your category.

The disadvantages are that some potential customers might assume that, for your book to be priced so low, it must be bad. Also, if people become accustomed to your prices being low, they may be dissatisfied if you raise them in the future.

Another strategy is to price your book according to your profile. This involves comparing both your book (its perceived value, length, speciality) and your profile (your platform, the number of books you've authored) with those of other authors.

This means, firstly, that if you have published more than one book, you should compare yourself only with authors who've done the same.

Secondly, if your book is self-published, you shouldn't compare your book with those released by a publishing house, as publishers have deals in place with Amazon that afford their books special placement and promotion.

Thirdly, only compare your book with those of a similar length. In short: if your book is just 50 pages, don't compare it with an 800-page tome!

By examining each of these factors, you'll find a way to develop the best price for your book.

### 7. You need reviews that encourage people to buy your book. 

Because they are a kind of "social proof" that your book is good, reviews are essential. When your potential customers see that other readers were happy with their purchase, their confidence in your book's quality is boosted dramatically. Moreover, if your competitor's book has good reviews and yours doesn't have any, you'll probably not sell many copies.

But how many reviews do you need? In fact, you need just three good reviews to drastically improve your sales ranking.

This figure is based on the following calculation: if you're ranked at 500 and you get three reviews, you can rise to 327th — which means, in concrete terms, you'll sell 57 more books per week.

You should do your best to ensure that your reviews are positive, as the negative effect of a one-star review on your sales is greater than the positive effect of a five-star review.

Reviews, though, can be hard to come by, so you should try to get people you know to write positive ones.

In fact, only a tiny percentage of readers will actually review a book after reading it — for instance, just 0.0002 percent of readers of _Harry_ _Potter_ _and_ _the_ _Order_ _of_ _The_ _Phoenix_ took the time to review it.

But if readers see that other people have made the effort to review your book, they'll feel more encouraged to do the same.

To this end, ask your family and friends to write honest and insightful reviews of your book to help people better understand what it is about and how it might be helpful to them.

Make sure that you get at least six "starter reviews," and that three or four of them are ranked with five stars, one or two with four stars, and one with three stars. Nobody will believe these reviews are real if they're all five-star rankings.

All of the previous steps can be carried out in just 18 hours. Now, having succeeded in selling it, you need your book to be a tool for selling even more copies. In the final blink, you will learn how to do just that.

### 8. Your book should be a tool to sell more copies. 

If your book is good and readers like it, you can get free publicity via social media with the rating feature that Amazon provides on the last page of Kindle e-books. This feature is called "Before You Go," and — if you use it wisely — it can help to increase your sales.

If all goes as planned, readers will be invited and encouraged to rate your book as soon as they've finished reading it. Then their feedback will be published using their Facebook and Twitter accounts, helping to spread the word about your book.

But be careful: if at the end of your book you include several pages of back matter — such as appendices and indexes — readers may close the book before they even reach the "Before You Go" page. Therefore you should limit the number of pages between the final page of writing in your book and the "Before You Go" page.

That said, there _are_ two pages that should definitely appear there. The first is a short author biography, designed to make you look as popular and friendly as possible. You want readers to like you!

Here you should mention any previous books you've written and include contact details so that readers can get in touch with you.

The second page you need is one in which you ask readers to review your book and share that review on Facebook and Twitter.

Using this feature wisely will help you enormously — but only if you get four- or five-star reviews. After all, if readers didn't like your book, and thus rated it poorly and published that information using social media, this would be a catastrophe for you!

> _"Readers are most receptive to your message at the end of the book."_

### 9. Final Summary 

The key message in this book:

**Unknown** **authors** **publishing** **their** **first** **e-book** **shouldn't** **waste** **their** **time** **building** **an** **author** **platform** **or** **social** **media** **campaign.** **Instead,** **they** **should** **do** **the** **following:** **Choose** **suitable** **keywords** **and** **subject** **categories;** **use** **a** **short,** **catchy** **title** **and** **professionally** **designed** **cover;** **describe** **the** **book** **in** **an** **enticing** **way;** **price** **their** **book** **effectively;** **encourage** **good** **customer** **reviews** **by** **getting** **acquaintances** **to** **post** **reviews** **first;** **and,** **finally,** **ensure** **that** **readers** **reach** **the** **"Before** **You** **Go"** **page** **at** **the** **end** **of** **your** **book,** **as** **this** **is** **a** **way** **for** **authors** **to** **sell** **even** **more** **copies.**

**Experiment.**

Try a number of different pricing strategies until you find the price that generates the highest revenue.
---

### Michael Alvear

Michael Alvear has 20 years of experience in media and marketing strategies, and has written and successfully sold several of his own e-books. His previous titles include _From_ _Text_ _To_ _Sex:_ _How_ _To_ _Text_ _Your_ _Way_ _To_ _A_ _Date_ _Or_ _A_ _Hookup_ and _The_ _Flirty_ _Text_ _Message_ _Helper:_ _Witty_ _Texts_ _For_ _Clever_ _People_.

