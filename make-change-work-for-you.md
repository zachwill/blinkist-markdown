---
id: 54feba8c626661000aaf0000
slug: make-change-work-for-you-en
published_date: 2015-03-12T00:00:00.000+00:00
author: Scott Steinberg
title: Make Change Work for You
subtitle: Ten Ways to Future-Proof Yourself, Innovate Fearlessly and Succeed Despite Uncertainty
main_color: DC8042
text_color: 9C5B2F
---

# Make Change Work for You

_Ten Ways to Future-Proof Yourself, Innovate Fearlessly and Succeed Despite Uncertainty_

**Scott Steinberg**

_Make Change Work for_ _You_ offers a searching exploration of why we resist change and why we should instead see it as an opportunity. In examining some of America's most successful companies and entrepreneurs, the book offers several case studies on how to successfully adapt to change — as an employee, team leader, or even CEO.

---
### 1. What’s in it for me? Learn how to let go of fear and embrace change. 

Fear is one of our best weapons against danger. Usually the things that we are most scared of (snakes, spiders, heights) are the things with the capability to kill us. Our fear keeps us from venturing too close to them and thus being injured.

However, despite its ability to keep us alive, fear can also hold us back. How? Because fear keeps us from exploring the unknown or trying something new.

Unfortunately, if we want to succeed in the modern world, where things are changing all the time, we need to constantly step outside our comfort zone in order to keep up. If we allow our fears of the new to dominate we will be left stagnating and unhappy. But how do we overcome those natural fears? These blinks will provide the answer.

In these blinks you'll discover

  * why being persistent is better than being intelligent; and

  * why being paranoid can help you stay one step ahead.

> _"Change is your secret weapon."_

### 2. Don’t let emotions slow you down; solve your problems in a confident, rational way. 

You're at a meeting with an important client, but he is no longer happy with your product and wants to cancel your contract immediately. What do you do? Panic and beg him not to drop you? Or resign yourself to the loss of business?

You could do neither. Rather than letting your emotions dictate your actions, you can instead approach problems rationally by using the _FEAR method_ : Focus, Engage, Assess and React.

Start by _focusing_ on your problem, studying it closely before _engaging_ and taking appropriate action.

Ask yourself: How threatening is this problem really? And does anyone on your team already have experience of solving a similar problem?

Is it possible to find a way to improve your product or service according to the client's wishes? Or might it be better to simply drop this client and search for new ones? If this has happened before, how did you resolve the situation?

After you have made sufficient preparations, it's time to develop an action plan. Analyze which resources you'll need, and what steps you must take to reach your goal. This could mean, for example, developing new technologies for your client which no other provider can offer.

Once you've begun implementing your action plan don't forget to _assess_ outcomes and _react_ accordingly. Always analyze the results of your plan. Are they satisfactory? If not, what are the underlying problems, and what can you learn from your mistakes?

If your plan has delighted your customer, that's great. But if not, what else do you need to do to stop him from searching for a new service provider?

Once you've assessed your plan's weaknesses, it's time to tweak your plan until you've solved the problem.

By differentiating opportunities and risks, the FEAR model provides you with a good opportunity to assess emotional situations objectively and produce the best outcome.

### 3. Courage is the willingness to take calculated risks in the face of fear and anxiety. 

Some people seem to take risks all the time without the slightest sense of insecurity. But how do they do it? No one is born with unlimited courage. Rather, it's a skill which can be learned.

Because having courage doesn't mean that you don't have fear. Instead, it simply means that you're being practical and realistic.

Courageous people carefully analyze risks before taking action. They also learn from their mistakes, and constantly recalibrate their next moves to make them more effective (and less risky).

If you undergo this process enough times, you'll become _resilient_, that is, you can turn adversity into positive feedback.

Psychology professor Martin Seligman explained it in _Harvard Business Review_ quite simply:

Imagine Douglas and Walter both lose their jobs. Douglas is courageous, and thus sees his predicament as a temporary stumbling block. He analyzes his earlier mistakes, looks for the jobs he can reasonably get and finds a new opportunity shortly after.

Walter, however, is full of fear. He therefore blames himself for his situation, becomes depressed and unable to move on.

If you don't want to end up like Walter, you'll need to train your courage by taking small risks.

Willpower, i.e., the force behind courage, is like a muscle — when you exercise your willpower, you strengthen it, and thus increase your tolerance for risks.

In practice, this means seeking out new opportunities at work, and getting involved in projects that test and expand your capabilities. As you surmount these small challenges and learn from your experiences, you'll simultaneously be preparing yourself for bigger, more serious challenges in the future.

So the next time you see an opportunity, analyze the risks and then take a chance! If it goes wrong, you can at least say that you learned something from it.

> _"Become more courageous and you'll become more aware of fear's hidden truth."_

### 4. At their core, your fears are little more than opportunities for feedback. 

Fear is a destructive force. It distracts us from our goals and erects barriers that negatively impact our choices. To overcome these barriers, you'll need to practice _mindfulness_, that is, being aware of when you feel discomfort or anxiety, and taking the time to pause and assess your situation.

As soon as you recognize your fears for what they are, they immediately lose their power over you, because now you can start working on them objectively.

The next time you find yourself getting worked up about a big presentation, take some time to analyze _why_ you're feeling so anxious. Is it because you don't like speaking in front of people? If you know that, then you have an opportunity to practice your presentation with a friend.

If you're unsure whether the facts you present are correct, then you have an opportunity to get feedback from colleagues to double-check your work.

By adopting this strategy, you effectively turn your worries into sources of feedback. In other words, your fears become learning tools.

Indeed, fear does have a positive side: it keeps us alert and open to potential signs change in important areas, be they markets, customers, relationships, etc. In this way, fear helps us consider problems before they even occur.

This gives us an opportunity to prepare our mind for possible upcoming challenges and think up creative solutions.

For instance, artists, who rarely have a stable income, always have to worry about where their next paycheck will come from. As a result, they must be creative to offer their audience what it wants. It is this forced creativity that has driven the band OK GO to not only produce music, but also run a record label, develop apps and produce videos.

Accept your fear for what it is: a normal response to stressful situations that you can use as both a learning tool and a source of motivation.

### 5. Use anxiety to build a system of self-assessment in order to make better decisions. 

Fear takes many different forms, one of which is _paranoia_, the suspicion of being outperformed and outmaneuvered by competitors.

As with the fear of failure, paranoia can also be translated into signals which can help you to assess your situation.

First, analyze whether competitors actually represent a real threat, and under what circumstances.

A good way to do this is by asking yourself tough questions: What would happen to your company if the economic conditions changed? If new technologies arise, or a competitor cuts prices?

If the answers to some of those strategies make you feel uncomfortable, think of some strategies to tackle possible problems and develop some back-up plans. Never let yourself feel too safe; it's easy to become complacent if you're not careful.

You'll want to be like the legendary skateboarder Tony Hawk. As he grew older, he realized that his career in sports couldn't last: there were always new, up-and-coming skateboarders ready to take his share of the market. So he decided to enlarge his business interests and keep himself relevant, developing his own brand of clothing and sports equipment, and endorsing a video game series.

Having established a self-assessment system, your mind will be primed for innovation, always trying to stay one step ahead of the competition.

It is this mind-set that is the ultimate driving force for successful businesses, as they can foresee problems well in advance, resolve them before they become unmanageable and foster growth opportunities.

P&G, for example, is able to continually improve its disposable Pampers diapers because it knows that competitors are following its lead and waiting for the company to falter. This means that Pampers must constantly produce new improvements, such as leak-proof waist shields, that have a lasting impact on the market.

> _"To put it bluntly: We must always be anxious and paranoid to some extent."_

### 6. Working constantly on your long-term goal includes taking well-evaluated risks. 

As we've seen, failure is a valuable part of learning for future projects. However, this means nothing if you don't have the will to actually carry through your goals.

Sustaining interest in a long-term goal and maintaining your efforts to achieve that goal require _grit_.

Grit demands a great degree of self-control. To achieve your goal, you need to prepare a solid plan which includes all the steps you'll need to take to get there. But you also need the persistence to carry your plan through — to never give up — and a tolerance for any small adaptations that have to be made along the way.

Grit is absolutely important in determining your success. In fact, according to the six-month-long study that psychologist Angela Lee Duckworth conducted with West Point cadets, college undergrads, working adults and Scripps National Spelling Bee contestants, persistence and perseverance are more important than IQ or GPA in determining success.

But having grit doesn't mean taking every single hurdle in your path head on. You still need to make sure that your actions aren't going to harm you in the long run.

Instead, you want to take _healthy risks_ that push your boundaries without extending yourself far beyond your capabilities and qualifications.

You can have all the grit in the world, but you still need to weigh potential losses against potential gains. Only then can you know which opportunities are worth taking and which are just too risky.

For example, Phil Gordon, professional poker player and owner of the app maker Jawfish Games, always has two experts test his apps before he puts them on the market. Sure, this might be costly, but the feedback gives him an idea of whether the apps are actually worth making available to the world.

Becoming a daredevil doesn't mean being free from worries. Rather, it means objectively scrutinizing challenges and making the right bets.

> _"Today, it's safer to take risks than it is to risk playing it safe."_

### 7. Experimentation and improvisation are business necessities. 

It seems like everyone is looking for some magical formula for success. Luckily, such a formula exists, but we tend to overlook it due to its simplicity. All we have to do is try, learn and try again, despite uncertainties and anxiety, until we succeed.

Indeed, with enough attempts and enough time, sooner or later you'll stumble upon a successful innovation.

This, of course, means dedicating time to tinkering and experimenting with your ideas, and likely failing in the process. But failure and experimentation are intertwined: the more you experiment, the more your failures will point you toward a successful solution.

Often this requires a shift in mind-set, one that requires you to see your career or organization as a constant work in progress.

You want the flexibility to adjust course when necessary, and this means adopting an attitude that can handle change.

It's this very principle that pushes companies to release beta versions of their products before release, to let the public test and scrutinize them, and detect problems on the spot. This open-minded attitude to development ends up saving a lot of money in the long run.

When unforeseen challenges do arise, it's time to improvise.

You don't need to be a genius, just ingenious. Your improvisational talent is worth more than intelligence when decisions have to be made quickly.

Today's young people are becoming experts in this way of thinking. The business world is undergoing a _generation flux:_ a new cohort of young professionals has entered the workforce, one with the talent to adapt to various circumstances, make fast decisions and think outside the box if needed.

> _"Try, learn, try again — the formula is simpler than you think."_

### 8. Spotting opportunities and using them helps you to stay relevant. 

Even if your business is successful and relevant, there's no guarantee that it will always be that way. In today's dynamic markets, relevance is more like a condition than a quality, so you'll need to keep changing to remain valuable to customers!

The key for sustaining relevancy is _constant reinvention_. In order to recognize and take full advantage of opportunities, you must always be willing to change and adapt yourself.

Becoming an expert in reinvention means learning new skills and keeping your finger on the pulse of market trends. In fact, in business a qualification that you earned from six-months' of evening classes can be worth more than a PhD (both on your CV and in practice) because it underlines your willingness to adapt to new challenges and your ability to learn fast.

Workers and managers who can't keep up-to-date with changes in market trends will quickly find the quality of their work slipping, while their skills and qualifications become obsolete.

The same applies to companies: they too must constantly be on the lookout for opportunities to gain a competitive advantage, e.g., with products or processes, in order to stay relevant.

This can pose a huge problem for large enterprises in particular, as they often aren't designed for change. Moreover, new adaptive strategies often fly in the face of convention. But it is possible!

Take the drugstore chain Walgreens, which had to deal with decreasing consumer spending and growing competition. To combat these trends, it implemented a whole new concept of customer service, including a mobile app and pill reminder and one-stop shops complete with a new assortment of healthy food, manicure services, and so on.

If your company can't undergo huge changes, small adjustments such as regular brainstorming meetings can help push you in the right direction.

As we've seen, change isn't always easy to welcome, but in order to stay innovative and successful we have to find ways to deal with it and see it as an opportunity.

### 9. Final summary 

The key message in this book:

**"If it ain't broke, don't fix it." This is a mantra that so many live by, and it's the mantra that will kill your business. Today's companies need to be on a constant lookout for opportunities, and to be prepared to take calculated risks** ** _before_** **things breakdown. Otherwise they risk being left behind.**

Actionable advice:

**Ask yourself the tough questions.**

Most people feel stress about their performance at work, wondering whether they are actually valuable to their company. Whether or not that's true can be easily assessed by asking a few simple questions: Are you taking all the opportunities you can? And are you constantly learning and taking on new projects which could expand your capabilities? If yes, then your company should be happy to have you as an employee. If not, then it's time to start taking risks!

**Suggested** **further** **reading:** ** _Smartcuts_** **by Shane Snow**

_Smartcuts_ shows you how to challenge conventions and build an innovative business with long-lasting success. Through stories about computer hackers, innovators and luminaries like _Tonight Show_ host Jimmy Fallon, these blinks show you how to buck the norm, achieve great things quickly — and even do your part to make society a little better.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Scott Steinberg

Scott Steinberg is a strategic consultant, professional speaker and columnist on topics such as leadership and innovation. In addition to offering leadership seminars and training workshops, he is also the author of _Get Rich Playing Games_.

