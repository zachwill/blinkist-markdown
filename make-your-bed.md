---
id: 59b64d39b238e10005c1faa5
slug: make-your-bed-en
published_date: 2017-09-11T12:00:00.000+00:00
author: William H. McRaven
title: Make your Bed
subtitle: Little Things That Can Change Your Life… And Maybe The World
main_color: 456F99
text_color: 456F99
---

# Make your Bed

_Little Things That Can Change Your Life… And Maybe The World_

**William H. McRaven**

_Make Your Bed_ (2017) is a collection of the author's ten most valuable personal experiences during his many years of service with the US Navy SEALs. Each is a simple yet priceless piece of advice that will have a positive influence on your life.

---
### 1. What’s in it for me? Change your life by learning the lessons of a retired Navy SEAL admiral. 

Sometimes, the simplest of things can be the most effective. And that's what these blinks are all about. It may seem obvious, but even something as small as making your bed in the morning — every morning — can hugely affect your quality of life.

As a Navy SEAL, William H. McRaven fought in both Afghanistan and Iraq. While there, and during his years of training beforehand, he collected an impressive array of life lessons. From the necessity of accepting failure and the importance of never, ever giving up, Admiral McRaven's advice is sure to inspire.

In these blinks, you'll learn

  * why you should never, in any situation, judge a book by its cover;

  * what a "sugar cookie" is (and why it's not as nice as it sounds); and

  * how the simple act of singing saved his comrade's military career.

### 2. Start the day off right by making your bed, and never underestimate the importance of a teammate. 

If you've ever seen a movie where young soldiers were struggling through boot camp, you may have noticed their bunk beds and how orderly and neat they're kept.

It may seem like a small and insignificant detail but a made bed can get your day started on the right foot.

The author and his fellow cadets were taught the proper way to make a bed in Coronado, California, as part of the basic training for being a member of the US SEAL team. And they also learned when to make their bed: first thing after waking up.

If someone failed to follow the bed-making code, they would perform the "_sugar cookie_ " ritual, which involved diving into the Pacific Ocean and then immediately rolling in the hot sand on the beach.

Now, you may be thinking, "What's the big deal about making your bed?" Well, it may be an _easy_ task, but accomplishing _any_ task first thing in the morning is the best and most productive way to start your day.

It gets the ball rolling, so to speak: by finishing one job, you'll find it easier to begin checking off the other tasks on your list. And before you know it, you'll be feeling great and productive — all thanks to making your bed.

Another life lesson from the military is the importance of teammates in your life.

The author learned this the hard way, after a near-fatal parachute jump. Falling through the air, McRaven was struck by another jumper's parachute, which caused his own parachute to become entangled with his leg. The force of his parachute's deployment broke his pelvis and tore his stomach muscles from the bone.

During his months of recovery, the author came to learn how important it is to have someone to help you carry on. If not for his wife, Georgeann, McRaven would likely have succumbed to depression and self-pity.

Everyone experiences a time in life when he needs the support of someone who believes in him. You can't make it through life on your own.

> _"...it takes a team of good people to get you to your destination in life. You cannot paddle the boat alone."_

### 3. Don’t judge a book by its cover, and don’t expect life to be fair 

Were you ever surprised by someone? Perhaps you initially assumed the person was dull and boring, only to end up, later on, as great friends.

The lesson here is to never make the mistake of judging someone by anything other than their heart.

The opposite scenario also holds true. You might think a stranger is something they're not, so before you place your confidence in them, ask yourself, "What do I really know about this person?"

When McRaven was in college and considering his future as a Navy SEAL, he visited a recruitment center to learn more. While he was there, he noticed Tom Norris, a small, balding and frail-looking man. He looked like the exact opposite of a badass SEAL team veteran.

Yet that's exactly who Lieutenant Tom Norris was: a war hero who'd been shot in Vietnam while risking his life to save others. Norris fought through painful injuries and exhausting recovery to continue serving his country as a member of the FBI's Hostage Rescue Team.

This brings us to another valuable lesson: that you shouldn't expect life to be fair and reasonable.

McRaven faced a fair amount of "sugar cookies" during his SEAL training — diving into the waves of the Pacific and then rolling around in the sand. This punishment was inflicted even if he hadn't gone against the SEAL training rules, which could make life seem unfair and unreasonable.

But after you spend enough time running around with sand rubbing against every inch of your body, you come to accept that life is unfair and that it's futile to resist this fact.

The ridiculous thing to do would be to waste time by refusing to accept life's challenges. Instead, you need to pick yourself up and march forward.

> _"...get over being a sugar cookie and keep moving forward."_

### 4. Learn from your failures and don’t be afraid to take risks. 

No one likes to hear the word "failure." It's loaded with negativity — something everyone wants to avoid.

But with the right perspective, failure can be used as an advantage.

Sometimes, failing is unavoidable. And though it can cause pain and suffering, that doesn't mean we should allow it to overpower us. Instead, we can use it to make us stronger and more determined.

While training with the Basic Underwater Demolition SEAL Teams, the author was part of a swim team that was constantly finishing in last place. This meant they were subject to another dreaded Navy SEAL ritual: _The Circus._

Legendary among SEAL members, The Circus is a strenuous endurance test that has been known to make many cadets give up and quit SEAL training.

The Circus was an exhausting trial for McRaven, but it worked — improving the results of his swim team. And when it came time for the graduation test, which featured a swim that was more challenging than any they'd faced before, they ended up finishing first. Their previous failure had made them stronger than any of the other cadets.

Part of learning from failure is a willingness to take chances. Because, in order to win big, you have to take big risks.

While comfort has its pleasures, there's a certain thrill in taking risks as well. And if you let your anxieties and fears control all your decisions, you won't get very far.

In 2004, McRaven was faced with a tricky situation. There was an enemy compound in Iraq that was holding three hostages and the intel was that the enemy and their captives would soon be on the move. His best chance of freeing the hostages required a risky daytime raid on the compound.

Of course it wasn't ideal. They'd be out in the daylight, and the compound was barely big enough to accommodate the three helicopters the team required. It was risky, but McRaven gave the orders to execute the mission. They had to push aside their fears of failure and death, but it was a success and the hostages were rescued.

> _"You can't avoid The Circus. At some point we all make the list. Don't be afraid of The Circus."_

### 5. Be courageous in order to keep pursuing your goals, and when life gets tough, be the best you can be. 

Life can throw a lot of challenges your way. You may be confronted by bullies or false friends. Or, if you're a Navy SEAL, maybe shark-infested waters are between you and what you want to accomplish.

The time may come when it feels easier to shy away rather than rise to the challenge, but this would be a mistake. Everyone has fears. You can't let them stand in your way. Be courageous and stay determined to reach your goals.

There are countless fears to overcome in order to join the ranks of the Navy SEALs. One night, McRaven and his swim partner had to swim four miles in the dark, which can be unnerving enough on it's own. But this night, there were reports that they'd be swimming with hammerhead sharks, leopard sharks and even the most aggressive and feared of all, great white sharks.

McRaven couldn't let a fear of sharks prevent him from completing his SEAL training, however, and so he used this goal to boost his courage and continue.

Courage is also what it takes to persevere in the face of tragedy and life's grimmest moments. It's during such moments that we need to put our best selves forward.

Times of darkness are sure to befall each and every one of us at on time or another. A friend, family member or loved one may die, or you may need to fight an illness that takes every bit of your strength. Though terribly trying, these times require that you rise to the challenge.

McRaven has seen far too many people die in battle. These are always the toughest and bleakest of times, but they're also the times when he's been most impressed with the endurance and resilience that people have shown.

After a Navy Special Operator died in Iraq, his twin brother was there to stand tall and offer a shoulder to cry on for grieving friends and family. It was an inspiring thing to witness. He said he wanted to make his twin brother proud.

> _"In life, to achieve your goals, to complete the night swim, you will have to be men and women of great courage."_

### 6. Be strong for others and don’t quit. 

Does this sound familiar? You're at the end of your rope, ready to call it quits, and then you talk to a friend who offers a fresh perspective and gives you that second wind.

These are moments that show how much difference one person can make.

We should all strive to be someone who can instill hope in others and lead them forward in life.

Even if you know very little about the Navy SEALs, you may have heard of _Hell Week_, a seven-day endurance test that often serves as the point where cadets either make it or call it quits. At one point, trainees have to spend a night sitting, covered in cold mud.

During McRaven's Hell Week, one of the cadets got up and walked away, ready to quit. But then one of the other men began to sing, and then a second and third joined in. Soon, they were all singing. It began with one man, but it was enough to get that cadet to turn around and rejoin the group with renewed hope.

This brings us to the final lesson: Don't give up!

Life is beautiful, even in the times when it's a painful mess. Remember, those blissful moments are impossible without the bad ones.

So when times get tough, don't feel sorry for yourself or blame others. Life is what you make of it, and it will only be as good as the effort you put into it. And if you give it nothing, you'll only feel regret.

In Afghanistan, a soldier named Adam Bates was badly wounded by a landmine. When McRaven visited him in the hospital, Bates was covered in burns; tubes ran in and out of him. Not only that, he'd lost both legs.

Nevertheless, Bates used sign language to signify that he'd be all right. It's the kind of response McRaven is so proud to see in soldiers. So often they face hardship but refuse to succumb to self-pity.

A SEAL learns to never give up. And if Adam Bates can find what it takes to keep going, surely you can as well.

> _"(...) lift up those around you and give them hope that tomorrow will be a better day."_

### 7. Final summary 

The key message in this book:

**Regardless of who you are, there are valuable life lessons to be learned from a veteran of the US Navy SEALs. A SEAL is someone who's been through the worst trials and tribulations; who's faced their worst fears and grown stronger from doing so; who's lost friends and witnessed acts of heroism that others can only read about. From these experiences comes a unique perspective on life and what it takes to make the most of it.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading: _You are a badass_** ** __****by** **Jen Sincero**

_You Are a Badass at Making Money_ (2017) offers a fresh and exciting perspective on what it takes to bring home the big bucks. Yes, you too can be a money-making maestro once you improve your mind-set and understand the energy of money. So stop making excuses and fooling yourself that only evil people are rich. Unlock your inner badass and open the door to success!

****
---

### William H. McRaven

William H. McRaven is a retired four-star US Navy SEAL admiral. During his 37 years of service, McRaven experienced quite a lot, both in training and in battle. He has led troops in both Iraq and Afghanistan.

