---
id: 54fec71b626661000a110100
slug: make-your-mark-en
published_date: 2015-03-13T00:00:00.000+00:00
author: Jocelyn K. Glei
title: Make Your Mark
subtitle: The Creative's Guide to Building a Business with Impact
main_color: EB4041
text_color: B83233
---

# Make Your Mark

_The Creative's Guide to Building a Business with Impact_

**Jocelyn K. Glei**

_Make Your Mark_ features the wisdom and tips of 21 of the most successful entrepreneurs and creatives of the last few years. These artists, coders, developers and writers share the secrets and ideas that have helped them take their respective markets by storm.

---
### 1. What’s in it for me? Discover the tips and hacks behind some of the most successful creative businesses. 

It seems like there has never been a better time to be a creative. The explosion of the internet has provided opportunities for people to turn their skills and knowledge into business opportunities.

Yet despite this chance, most of us have no idea how to start turning our passions into viable, profitable businesses. These blinks, inspired by the opinions of luminaries such as Seth Godin, Chris Guillebeau and Warren Burger, show how anyone with the right drive and skills can become a great entrepreneur.

In these blinks you'll discover

  * why every email you write at work should be available to the whole team; and

  * why the perfect leader is a servant.

### 2. Every great, successful business starts with a defining purpose, which focuses on answering a need. 

Though it might sound strange, many start-ups and entrepreneurs struggle to answer a simple question: "Why does your business exist?" Many point to their product's awesome features, or talk about how their service is cheaper than the competition. Yet these replies don't answer the question.

Regardless of what you offer or how it bests the competition, you will never be successful unless you _can_ answer this simple question. Why? Because the answer outlines your _purpose_.

Every successful business has at its core a purpose which aims to make society a better place and which its products and services aim to embody. No matter how many features your product may have, if it doesn't solve someone's problem, it can't succeed.

So, before you even begin your business venture, you need to figure out your purpose: How do you hope to benefit the world with your product or service?

Nike, for example, is famous for its innovations, connections with sports stars and cool image. Yet behind all this lies a purpose: to help people reach their athletic potential.

As your business develops you will need to constantly ensure that your actions align with your purpose. One good way to do this is by always asking "why."

For example, whenever you want to add something to your product or service, ask: "Why does this matter?" Why, for example, would you want to add a voice command function to your kettle?

Answers like "because it's cool" or "no one else has it" are unacceptable, since they don't relate to your purpose. On the other hand, if you are trying to solve a problem for arthritic customers who want to make a cup of tea, then your development has a clear purpose and you have a reason to proceed.

### 3. Build an operational system that promotes flexibility and development. 

While your purpose is fundamental to your success in business, it's not the only piece of the puzzle. You'll also have to develop your business in a way that actually moves you closer to achieving that purpose.

The best way to do this is by developing a flexible operating system. Indeed, many visionary companies, including Facebook, Airbnb and Dropbox, have adopted a _Responsive Operating System_ (OS).

An OS is a company's DNA; it is the method by which they get things done. A Responsive OS means that flexibility and a tolerance for risk are ingrained in a company's practice.

A Responsive OS enables an enterprise to more easily take risks — and learn from them.

In a more traditional OS, companies develop the practice of limiting their risk as they grow. In other words, they want to maintain their position rather than innovate.

Responsive OS companies take the opposite approach. Their strategy revolves around constantly looking for opportunities to take chances and learn from the results.

For example, a Responsive OS company will launch a new product or service if they see a promising opening in the market, even if it's unclear whether it will succeed. If it succeeds the company gains a market advantage; if it fails, at least they've learned which area not to explore in the future.

In order to pursue innovation, Responsive OS companies utilize a much leaner, more reactive structure that allows room for change.

Traditional companies often acquire large amounts of assets and resources as they grow, making them both sluggish and resistant to change.

Responsive OS enterprises, on the other hand, focus on being as lean as possible. This offers them a high degree of flexibility to experiment and make changes.

Many responsive companies follow the "two pizza rule" for meetings and projects: only teams which can be comfortably fed with two pizzas are allowed, thus keeping them small enough to make quick decisions.

### 4. Success starts with designing one product that delights users, and then capturing their attention with simplicity. 

When we think of creative genius and entrepreneurship, we often imagine prolific inventors who are able to come up with huge numbers of novel innovations. However, when it comes to business success, you don't need lots of good ideas and products. You really only need one.

In fact, trying to develop too many things simultaneously will simply leave you with lots of half-finished projects, i.e., _nearly there products_.

Instead, concentrate your efforts on creating just one great idea that people will love, and then developing it as far as you can.

This is exactly the approach that successful men's clothing company Bonobos took. Their story began when one of their co-founders realized that many men don't feel comfortable in any of the range of pants styles offered in stores.

So they created just one type of pants that fitted the way they liked in various colors and began selling them online. Their idea was a huge success: in six months they were generating a run rate of $1,000,000, and after another six months it had jumped to $2,000,000. Only after this initial success did they think about branching out to other wares.

However, it's not enough to simply create a great product, or even the _best_ product. You still need to get people to take notice of it.

One way to do this would be to have your product stand out in some way, just like a book cover entices you to read a book. You could also ensure that your product is simple to use and quick to understand.

According to entrepreneur Scott Belsky, once you have a potential customer's attention, you have less than 15 seconds to entice them to buy. It's your job, then, to make the process from discovery to purchase as fast, simple and streamlined as possible.

### 5. You never start with a great product: you grow one through experimentation, failure and feedback. 

When you see a product in a store, how do you think that it was conceived? Did it come as a stroke of genius to someone in R&D, complete and in its finished form?

Unlikely. In fact, _all_ great products are the result of tweaking and development, not someone's muse whispering in their ear.

Your initial product, too, will nearly always be less than perfect. Luckily, you have plenty of opportunities to improve it through a process of experimentation and failure.

Indeed, no matter how imperfect your initial product or idea may be, you have the chance to make it and test it, and solve any problems along the way.

According to Sebastian Thrun, one of the masterminds behind Google Glass, developing a product is a bit like climbing a mountain that has never been scaled. Sure, you can study the maps and carefully plot a route, but since no one has successfully done what you're setting out to do, the map will likely be useless.

Instead, the only way is to try first hand: find a path, test its suitability and discover whether it's passable. If it's not, then you go back and try another. If it is, then you're closer to your goal.

But you don't have to conduct these experiments by yourself. In fact, your customers can help you along the way.

Giving customers access to your product in the early stages of development will provide you with accurate information about what works, what doesn't and what you could do instead.

For example, Jane ni Dhulchaointigh, the inventor of the moldable glue Sugru, tested her initial product with users. She learned that they became confused by its physical consistency, and used her product like modeling clay instead. She discovered that in order to get customers to use the product correctly she would need to provide them with clear instructions.

### 6. Build an army of supporters by treating your customers as individuals and giving them your secrets. 

When a company releases a statement or an offer online, they often "hope" that it will go viral and win them lots of attention.

However, the most successful companies do this not in hope, but in expectation. For example, when entrepreneur and author Chris Guillebeau posted a single blog entry asking for donations to his Clean Water for Ethiopia campaign, he was able to raise $22,000 in a single day, all without "going viral." But how?

Guillebeau had built an army of fans, who act as his supporters and cheerleaders. So how can you create this kind of following for your business?

According to Guillebeau, one way to gain followers is to give them something that they want. This could be anything, really. TED, for example, gives its users free access to paid content. While you can pay to go to a TED event, presentations are put online shortly afterward for anyone to view.

You could also provide a paid service to customers for free. For example, although she normally charges for her coaching sessions, Pamela Sim also offers monthly "Ask Pam Anything" sessions for her fans for free.

If you don't yet have a following, then it's important to remember that every army starts small. In order to recruit your first users, you'll need to go the extra mile.

Although it runs against common business practice — in which everything you do should be scalable — going above and beyond for your first customers can help you form a large following of fanatic fans.

For example, when Airbnb started, they had difficulty finding people willing to open up their houses to strangers. They simply couldn't get people interested in the service.

So the founders went out of their way to attract the first users: they held meet-ups, stayed with their first customers and listened intently to their views and concerns. This extra effort helped them start a community which continues to grow to this day.

### 7. In order to get people talking about your brand or company, make your company’s story part of their lives. 

Do you know anyone who owns a coat made by the mountaineering clothing brand Patagonia? And have they ever been mountaineering in that coat? Probably not. How can this brand be so popular despite the fact that so few people use its products for their intended purpose? The answer lies in the stories they tell.

Indeed, the most successful brands tell stories about themselves which relate to the views and beliefs of society.

Patagonia tells a story which many people can relate to: as passionate environmentalists, they were among the first companies to give a percentage of their profits to charity and to create a line of goods made from recycled materials. This resonates with our will to be better people and create a better world.

And of course, by positioning themselves as an "activity" company, Patagonia likewise appeals to our desire be fit, inquisitive and adventurous.

Looking at the entirety of Patagonia's messaging, you see a good story that matches up with our own aspirations and beliefs. By creating a unique, relatable story, any company can attract many more loyal followers.

However, while you can create a good image through a story, modern media volatility makes controlling that image futile. You can merely try to influence it.

No matter how well you craft your image, it can be ruined by one bad customer experience propagated on Twitter. All you can do is influence this situation by providing the best service you possibly can, e.g., by carefully training your customer-facing staff, providing customers with little extras, offering free postage and packaging, and so on.

This way, you can mitigate any damage caused by the angry customer reviews that will inevitably crop up during the lifetime of your business.

### 8. The key to leading an effective, efficient team is to promote total transparency. 

Imagine that every email you sent from your work account was automatically sent to everyone on your team. Surely this kind of invasion of privacy couldn't lead to effective work. Right?

Wrong. In fact, the social network managing service Buffer does exactly that, and they enjoy both a loyal team and remarkable successes.

You see, making emails available to the entire team is just one aspect of their policy of total _transparency._ Everything the company does, from its metrics to its decision-making process, is fully available to everyone's eyes.

This works so well because transparency increases the amount of trust within a team.

Indeed, your team's ability to work happily together toward a shared goal is dependent on their ability to trust everyone else on the team. This trust comes from knowing that people are being honest and that everyone will be treated fairly.

For example, if you discover over lunch that your new colleague is earning a higher salary than you, it's a fair bet you'd feel cheated and undervalued. If this information isn't openly shared, we end up feeling that the information was withheld _on purpose_. However, if the information is freely available, then we're less likely to feel personally threatened by things like other people's career accomplishments.

Moreover, openness is essential for innovation. We all want our employees to embrace innovation and experimentation. This means giving them access to ideas so that they can tinker with them.

If things like products or business strategies are kept secret, team members won't get a chance to play with and develop them.

In addition to driving innovation, transparency in product and process development also encourages feedback. When employees have the chance to look at everything, they can give you their honest opinions, which can offer you valuable insight into the direction and viability of your idea.

### 9. If you want the best people to become leaders, you need to see the role in a positive light. 

Ask creatives if they'd like to be in a leadership position and most will say: "No way! I'd never want to give up my creative work just so I could boss people around. It's just not me."

While this sentiment is common, it is due, in a large part, to a fundamental misunderstanding of what leadership actually involves.

In reality, good management isn't about _bossing_ people around — it's actually about _serving_ them.

Management's goal isn't to get people to do what you want them to, but rather to help them achieve something great.

Let's say your team is responsible for creating a brand new, revolutionary vacuum cleaner that will transform the market. You've got a wide range of talent on your team, from creative designers to efficient manufacturers, all the way to persuasive marketers. As a leader, it's your job to help these diverse talents come together to achieve a common goal.

In other words, your job is to _help_ them reach their potential.

It's not enough, however, to simply wish to serve your team. Your ability to do that rests on your skill at communicating effectively.

The modern working environment is a flexible one where people work across teams and projects. This makes it difficult for managers to keep everyone working to the same goal.

As a manager, you'll need some ways to maintain communication to keep things on track.

In addition to your weekly meetings, you can create a public record of what everyone is doing. One way to do this is through a company-wide group forum where everybody can write down what they've been working on.

Another way is by constantly and consistently repeating yourself. Even if you have to say something ten times a day, you will nonetheless ensure that the message gets through to everyone on the team.

By following these tips, you can help your employees achieve their potential and move your business forward.

### 10. Final summary 

The key message in this book:

**There is nothing to stop any creative in the modern world from turning their skills and passions into a successful business. As long as you concentrate on the four key principles of purpose, product, customers and leadership, you can transform your idea into a business opportunity.**

**Suggested** **further** **reading:** ** _Manage Your Day-To-Day_** **by 99U and Jocelyn K. Glei**

_Manage Your Day-To-Day_ is a collection of ideas, wisdom and tips from well-known creative people. It offers readers valuable insights on how to develop effective work routines, stay focused and unleash their creativity.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Jocelyn K. Glei

Jocelyn K. Glei is the editor in chief of 99U, an organization devoted to giving creatives a grounding in the skills that don't get taught at school, but which are essential for making ideas a reality. In addition, she has edited 99U's previous publications, _Manage Your Day-to-Day_ and _Maximize Your Potential._

