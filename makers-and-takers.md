---
id: 58c6c9608d90030004072679
slug: makers-and-takers-en
published_date: 2017-03-17T00:00:00.000+00:00
author: Rana Foroohar
title: Makers and Takers
subtitle: The Rise of Finance and the Fall of American Business
main_color: 7BAFBD
text_color: 496870
---

# Makers and Takers

_The Rise of Finance and the Fall of American Business_

**Rana Foroohar**

_Makers and Takers_ (2016) investigates the role of finance in the 2008 crisis and subsequent recession. From the Great Depression onward, these blinks trace the history of loose regulation and blurred boundaries between commercial and investment banking, while highlighting the role of banks, businesses and politicians in the crisis. They also suggest actions the powerful can take to kickstart reform.

---
### 1. What’s in it for me? See how we moved from the 1920s to 2008 in the hands of Big Finance. 

On 29 October 1929, the financial markets crashed, thousands of small and large investors lost their savings and mass unemployment ensued. So devastating was the Great Depression of the 1930s that politicians were determined to act and prevent such an event from happening again. And yet the unthinkable recurred in 2008.

How did we once more end up mired in a major financial crisis? Didn't we learn our lessons from the past? Apparently not.

These blinks reveal how the financial industry controls our economy. You will be taken through time from the 1920s until today, see how politics tried to tame the world of finance, why the industry's behemoths knew how to resist and how a new crisis emerged.

You'll also learn

  * who's the culprit behind the credit card;

  * why Goldman Sachs likes to move around aluminum; and

  * how money-focused management at GM contributed to the death of 124 people.

### 2. Like the Great Depression, our own Great Recession was triggered by a flawed financial system. 

Americans who experienced the Great Depression of the 1930s probably felt a little déjà vu about the crash of 2008. Debt, credit and economic bubbles were instrumental in both financial crises. Just as in 2008, the Great Depression followed on from mounting debt and consumer credit.

Debt, like credit card use, is a financial product. The more debt is issued, the larger the financial sector grows. When the author was writing _Makers and Takers_, the financial sector was the same size it had been just prior to the Great Depression — a size it has not reached at any other point in history.

Credit is rampant today in the United States, just as it was in 1920s America. Back then, Americans used credit to gain access to around 75 percent of major household items. Before the Great Depression and our own Great Recession, credit was used to mask the severe income inequality that resulted from declining workers' wages and the skyrocketing profits made by stock market investors.

Finally, the years leading up to the 1929 financial crash were characterized by a steadily growing economic bubble. After the fall, the bankers weren't held accountable — sound familiar?

The 1920s bubble emerged after copper prices fell, and banks such as the National City Bank of New York sold stocks in copper mines, telling their oblivious customers that they were a prudent investment. This created the bubble that eventually triggered the 1929 stock market crash. While National City chairman Charles Mitchell was subjected to public shaming in Senate hearings, he returned to Wall Street soon after. In fact, he never spent a day in prison.

The same goes for the bankers responsible for our financial crisis. Richard Fuld, former head of Lehman Brothers, continues to work in the financial sector at Matrix Advisors and Legend Securities.

The similarities between both financial crises might leave you wondering whether we've learned anything from history at all. In the next blink, we'll investigate how the American economy managed to take us back to the 1930s in 2008.

### 3. In the decades after the Great Depression, financial rules were loosened again to meet demands for credit. 

After the stock-market crash of Black Tuesday, 29 October 1929, the Glass–Steagall Act was enforced. The new legislation separated the commercial and investment activities of American banks to protect the public from their risky trading.

Despite these regulations, the lines between commercial and investment banking remained blurred. It wasn't long before bankers began to use this to their advantage again.

In the late 1940s, Walter Wriston of the National City Bank of New York introduced a new product to further dissolve the boundary between investment and commercial banking — the _negotiable certificate of deposit_, or CD for short.

CDs were a special kind of savings account with higher interest rates. They were intended to keep rich people's money out of personal bank accounts, making it harder for tax authorities to keep tabs on them. Commercial banks have always managed bank accounts, but Wriston began selling the CDs for others to trade at a profit.

Another blow to the Glass–Steagall Act (and a win for traders like Wriston) arrived in 1967 — credit cards. They were invented after consumers became disgruntled about high inflation eating away at their buying power. Credit cards helped loosen regulation surrounding the issuing of credit and rates.

As commercial and investment banking continued to merge, it wasn't just bankers who were running the show — politicians also had a role to play.

After World War II, Americans came to expect more wealth. Rising inflation had stunted economic growth, and with growing demand for looser credit regulations, President Carter deregulated interest rates altogether in 1980. This allowed banks to offer whatever rates they liked to draw in funds as they created complex financial products that were nearly impossible to regulate, from variable-rate mortgages to derivatives.

> _"The scariest part of it all is that finance has yet to be properly regulated in the wake of the crisis."_

### 4. Companies are focused on short-term profits, not long-term growth or solutions for their customers. 

If someone pees in a pool, all the water becomes contaminated. The same goes for the economic system — what goes down in finance affects the waters of the entire economy. This is what happened when financial models for maximizing profit were introduced.

In finance, the focus on increasing shareholder value has led businesses to prioritize short-term gains over long-term company value, cutting back costs and endangering the quality of their product in turn. General Motors, for instance, had to recall hundreds of thousands of vehicles in 2013 because of a switch failure.

Engineers responsible for the switch had noticed the failure and introduced a redesign, but didn't relabel the new part. Why? Because they feared telling those higher up the ladder about the glitch in their strict, cost-controlling organization. The public paid a high price for General Motors' mistake: the switch has caused 124 casualties and many injuries.

Shareholder interests also dictate how companies produce goods. This goes against one of the fundamental concepts of the free market, in which consumer demand dictates what products and services companies offer. Nowadays, companies across multiple sectors barely seem interested in fulfilling a customer need with a new product. They remain focused on boosting shareholder value in the short term.

Consider how in 2010 a Morgan Stanley report called on the pharmaceutical industry to create value by paying shareholders and buying out companies instead of researching. This was a clear sign of the power Wall Street has over strategic decisions in the industries we depend on to help us live — and sometimes even to save our lives.

### 5. Shareholders might not fund the innovative products created by big companies, but they do reap the benefits. 

Imagine asking a friend for a dollar. They agree, but only on the condition that you pay them back two dollars later. Bad deal, right? Well, this is essentially what the current relationship between shareholders and companies looks like.

The benefits of profitable large companies primarily go to their shareholders. Back in the 1980s, shareholders who actively tried to influence corporate policy to increase their returns were branded corporate raiders. Today, they're simply called _shareholder activists_ and make millions.

After buying shares of high-profile companies, shareholder activists attempt to boost their value by influencing the board. Consider the relationship between Apple and shareholder activist Carl Icahn. Between 2012 and 2015, the company returned $112 billion (yes, _billion!_ ) to the investor and business magnate and the other shareholders. That's $112 billion that can no longer be put toward developing new and innovative tech to strengthen Apple's long-term value.

Apple isn't the only company playing this game — 91 percent of all US stock is owned by the wealthiest 10 percent of Americans. Shareholder pay-outs tend to make the rich even richer, while a minimal amount of pay-outs go to pension funds and regular citizens holding stock.

Perhaps it's only fair that companies repay their shareholders for the money they invest into the company so it can produce its products. However, shareholder activists rarely pay for R&D in the first place.

Usually, governments pay for innovation. Much of the technology required for smartphones, including touchscreens, GPS, voice activation and even the internet itself were developed as part of government programs and institutions like the military, allowing big companies to turn them into profitable products later on.

As wealthy individuals continue to profit from shareholder payouts for products they didn't help develop, many are wondering how we got to this stage. Let's investigate this in the next blink.

### 6. Banks aren’t the only players in finance today – and they don’t restrict themselves to finance, either. 

In recent decades, big brands have become increasingly involved in lending and investing — activities that were traditionally the work of banks and financial companies. It's getting harder and harder to tell what a company's core business really is.

Today, most large companies have a lending unit. Originally established to offer credit to customers who wanted to purchase one of their products, lending units became increasingly profitable over time. With each success, financial departments grew increasingly daring and began taking greater risks.

For instance, before the 2008 crisis, General Electric (GE) was busy buying and selling businesses to increase its own stock prices. GE also had a large stake in mortgages, which is where things began to go wrong. After the real estate markets had crashed, the government was forced to bail out GE for a whopping $139 million.

As companies begin to behave more like banks, banks are also giving companies a run for their money. In 2011, teams at Coca-Cola realized that there was something strange going on in the aluminum market. Demand wasn't rising, but prices were. It was revealed that Goldman Sachs had bought aluminum storage plants to cleverly exploit a loophole.

While it's illegal, after a certain point, to store stock and raise the prices, it wasn't illegal to rotate the same stock between plants, which bumped up prices just as much. As a bank, Goldman was betting on the price of aluminum in the commodities market, but as a business, it was actively involved in raising the prices.

### 7. Lucrative rental home schemes and poorly managed retirement funds see banks profit at the expense of average citizens. 

Imagine having your house taken from you by somebody who then demands you pay them outrageously high rent. This, in a nutshell, is what happened both before and after the 2008 financial crisis. As banks bought up houses and rented them out, the financial sector began to benefit from the housing crisis at the expense of the average citizen.

Following the 2008 crisis, both home sales and rent prices skyrocketed in the United States. You could read this trend as a sign that people are getting back on their feet and are buying properties again.

Unfortunately, this is far from the case. Instead, many low-cost homes have been purchased by investment groups to rent out to families who can't afford to buy their own home. This is a lucrative business — the Blackstone Group investment company owns 46,000 homes that provide a $1.9 billion income for the company.

The reason so many families can't afford to buy their own home in the first place is because all the affordable ones have been bought up by the very same investment groups renting them out. So although sales have risen, the percentage of Americans who own a home has been in decline since 2004.

It's not just houses that American citizens can't afford. Retirement has also been depleted by financial activities. On average, a retirement household has around $104,000 to subsist on for their whole retirement. For couples between 55 to 64 this just isn't enough to live a decent life for the coming 20, 30 or 40 years.

This is a result of the short-sighted management of retirement plans. Like companies, fund managers are also rewarded for short-term results. Rather than focusing their strategy on long-term, sustainable growth, fund managers take on riskier bets for the chance of bigger wins. However, these risks also lead to big losses and less profit overall.

### 8. After 2008, ties between banks and politicians undermined attempts to reform the financial system. 

Just as we ask caretakers and educators to look after our kids while we're at work, we also expect the government to watch over the financial system. After all, it's their responsibility. So why is it so poorly controlled?

After Lehman Brothers fell in 2008, politicians seemed determined to reform the financial sector. Eight years on, only a handful of reforms have been ratified. In 2014, a few lines of legislation were added to the federal spending bill.

This bill was supposedly written with the intention of forcing banks to give up their riskiest products, like swaps or derivatives, which allowed them to bet on the performance of stock market outcomes. This goal was directly undermined by the lines added to the legislation.

What's more, the politicians who voted for this amended bill to pass received 2.6 times more funding from political action committees (PACs) than those who voted against it. These PACs are comprised of J.P. Morgan, Bank of America, Goldman Sachs and Citigroup — banks that control 90 percent of the swaps market, and who couldn't let the bill damage their business.

Perhaps it should come as no surprise that governance and finance share such a close relationship. Government officials in charge of controlling finance typically take up jobs in finance when their term ends. The high salaries paid by banks are much more attractive than the modest public service wages paid to government officials. Since 1900, 13 out of 35 Treasury secretaries have previously worked at banks. Seventeen of them went into banking after leaving their Treasury post.

So what can we do to reform the financial system? The next blink will explore the author's key recommendations.

### 9. Reducing debt and encouraging financial transparency could help us fix the financial system. 

Before regulations in the financial sector were loosened, the sole purpose of finance was to support business and economic growth. In an age when companies have become banks and vice versa, how can we get finance to once again boost regular business?

The first step is reducing the complexity of banking and financial regulation, which will cut risk-taking. Every day, millions of financial transactions take place with a total worth of $81.7 trillion. The financial system has become so sprawling and convoluted that it's impossible to oversee. Banks like Citigroup are themselves so complex that even their own boards struggle to supervise their activities.

Regulation law, too, is far from transparent. Just like the Glass–Steagall Act of 1933, the 2010 Dodd–Frank Act attempts to separate commercial from investment banking to reduce the risk of banks failing.

But the new Act is 2,319 pages long (the 1933 Act was just 37 pages in total) with a whole host of new loopholes that can be exploited to blur the lines between commercial and investment banking once more.

It's also imperative that we limit debt in order to stabilize the economy by encouraging citizens to save and by requiring banks to fund their investments with at least 20–30 percent of their own capital. This takes political courage, as debt and credit allow governments to hide economic stagnation. Following the 2008 crisis, it's become clear that a healthy economy can only handle so much debt.

> _"As in medicine, so in finance — first, do no harm."_

### 10. Final summary 

The key message in this book:

**Unregulated, risky financial activity was behind both the Great Depression of the 1930s and the 2008 crisis. As the borders between commercial and investment banking, between commercial and political interests, and between companies and banks themselves continue to be crossed, rich shareholders profit while average citizens struggle to finance their own homes.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Only Game In Town_** **by Mohamed A. El-Erian**

The 2008 financial crisis dramatically changed the global economic landscape. Central banks now play a very different role than they did previously, and we now face a set of new economic risks and problems. _The Only Game in Town_ (2016) outlines the roots of these risks and problems, and what we can do to start overcoming them.
---

### Rana Foroohar

Rana Foroohar is a global economic analyst at CNN and a business columnist for _Time_ magazine. She is also a frequent commentator for the BBC, NPR and CBS.

