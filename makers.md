---
id: 55512d976332630007e40000
slug: makers-en
published_date: 2015-05-15T00:00:00.000+00:00
author: Chris Anderson
title: Makers
subtitle: The New Industrial Revolution
main_color: E16F2E
text_color: AD5623
---

# Makers

_The New Industrial Revolution_

**Chris Anderson**

_Makers_ (2012) outlines the radical changes that are taking place in the manufacturing world, made possible by the internet and digital manufacturing technologies, and explores its implications for business and society.

---
### 1. What’s in it for me? Tune into the future of manufacturing. 

Remember your first printer? Your first computer? What about your first Microsoft Word document, or whatever you used for word processing? Remember that tingly mix of excitement and curiosity? That, "I'm not sure what this is, but I like it?" sensation?

Well get ready, because that sensation is about to return. It won't come from computer gadgets, but from manufacturing. Just as printers allow self-publishing, and laptops perform complex calculations, soon we'll all be manufacturing things at home.

These blinks will show you just how close we are to another technological revolution. This time it's one that allows almost anyone to sit at home, download a file and "print" — or manufacture — a toy, a product, a device or whatever your imagination can cook up.

In these blinks, you'll discover

  * why this new technology could reverse decades of outsourcing;

  * how one company reached its $100,000 targeted funding in two hours; and

  * how the author could download his daughters some new toys.

### 2. The Maker Movement is about people manufacturing things and sharing ideas. 

People have always made things. Today, however, _Makers_ are designing things on computers and manufacturing them at home without physical resources, using digital tools. What's more, they share their ideas in online communities.

The DIY spirit of the punk and web era has arrived in the form of manufacturing, where instead of relying on large corporations for goods, people can create them themselves.

For example, when the author's daughters wanted new furniture for their dollhouse, they found the selection available from classic toymakers to be very limited. While they did find the Victorian sofas and chairs they wanted, they couldn't find the right size.

So, the author searched the web, and found a website offering online design files for dollhouse furniture. There, he found designs for Victorian furniture and modified the files so that they fit the size of his daughters' dollhouse.

But having the designs isn't the same as having the furniture. So, he simply printed the files on his 3D printer — for free.

The Makers are empowered by the web's culture of sharing and collaborating. Open source software, for example, enables people to write and modify software to fit their own needs for free instead of getting mass-produced, one-size-fits-all programs that don't always meet the needs of the user.

Just like the chair and sofa designs that the author found online, people actively design things that were previously unavailable and make them available for others. Then anybody with the know-how can modify the files and improve them. 

We see this exemplified by the micro-culture and community that has arisen around creating weapons for toy manufacturer LEGO.

LEGO itself doesn't make toys that resemble contemporary weapons, like assault rifles or grenade launchers. So, hobbyists design them online, where they can be shared and modified. These toys are then manufactured using 3D printers at home or by sending the files to online manufacturing services.

Some people even turned this hobby into a legitimate business, such as the LEGO weapon manufacturer BrickArms.

### 3. Cheaper and more powerful technologies always lead the way – just think of your printer. 

If you're an inventor, then you know that we're in a golden age. Whereas in the past inventors depended on large manufacturers to turn their dreams into reality, today they can produce prototypes from their own homes.

According to _Moore's Law_, there is a strong tendency for computer hardware to get both cheaper and more powerful over time. This is due to something called _compound learning curves_, whereby even incremental improvements lead to huge benefits. As a result, innovations follow extremely quickly, with breakthrough discoveries happening about every three years.

We can see this in action when we look at the world's first desktop laser printer, Apple's LaserWriter, released in 1985. Though expensive, it marked the beginning of widespread desktop publishing.

A mere 30 years later, anyone can have their own fast, high-quality laser printer for less than $100.

Following this same trend, 3D printers and other digital manufacturing tools will only become cheaper with time, and more of us will have them at home.

3D printing today is at the same point as Apple's LaserWriter in 1985. It's still expensive, but it's getting cheaper, and more people are using and adapting to it.

The adoption of 3D printing is analogous to the first desktop publishing tools a few decades ago (think Microsoft Word). When desktop publishing first became available, people realized that they didn't have a clue about things like fonts or formatting.

The same thing is happening with online manufacturing. It's still new, and people are struggling to understand the languages of industrial design, such as _G-code_ (a programing language that tells computerized machine tools how to make objects) or _rasters_ (programs that convert images to pixels or dots).

Only a few people experiment with these cutting-edge tools now, but it is only a matter of time before they become mainstream.

### 4. Crowdfunding helps shrink the gap between inventor and entrepreneur. 

Every inventor or start-up founder knows that they're going to have to raise money if they want to make their idea a reality.

Normally, start-ups have to raise funds with the help of venture capitalists or loans to manufacture their product, but crowdfunding changes all that.

Kickstarter, for example, turns sales into "presales" by letting people contribute money to a nascent project. In other words, people pay to be the first to get their hands on the product. Start-ups can use that cash the same way they would loans or venture capital in order to cover their initial costs.

In addition, crowdfunding sites transform customers into supportive communities. When you support a Kickstarter project, you're supporting a team of producers, whom you can communicate with in forums and who will update you on their progress.

This constant communication fosters participation from supporters, who feel more involved. This involvement can translate into action, and supporters might even help a project go viral by sharing it or spreading the word to people they know.

Take the extreme example of Pebble, a smartwatch start-up. Within a span of two hours it had reached its fundraising goal of $100,000. By the end of the day, it had reached $1 million, and within three weeks $10 million!

All of this was only possible because of their excited and engaged online supporters.

What's more, crowdfunding sites provide free market research, making new ventures less risky.

If you don't reach your funding goals with your crowdfunding campaign, then you know that your project simply didn't capture people's interest or meet their needs and preferences. It's much better to learn that lesson during the funding phase of your project than after you've put in an order to start manufacturing. Instead of having wasted money on manufacturing equipment and product development, you won't have lost a dime!

It also gives you an opportunity to fine-tune your project to better match the needs of potential customers and then try again later.

### 5. Manufacturing will eventually move back to developed countries. 

If you've ever been to Detroit or Toledo, it becomes depressingly clear that manufacturing has been outsourced for decades. Today that's all changing.

Digital manufacturing techniques make it possible for developed countries to compete with the low-cost labor in other countries, like China. By automating activities that once required huge amounts of human labor, they're able to make those activities cheaper.

We see this concretely in the American toy company Wham-O, which, after taking steps to further automate its production, decided to move 50 percent of its manufacturing from China back to the US.

Another American company, the ATM producer NCR, is likewise relocating its production from China to Georgia, thanks to automation.

In both cases, automation allows the companies to achieve faster market delivery and lower shipping costs. What's more, by producing closer to their customer base, they are able to be much more flexible, and can more easily react to customer complaints or suggestions. That means they can deliver new products and services faster than before.

Another reason for the shift away from offshoring is that increasing skills and pressure from labor groups have increased salaries in developing countries along with the costs of labor.

In China, for instance, the salaries for industry workers rose rapidly — by 17 percent annually in Guangdong and 50 percent in Shenzhen from 2007 to 2012. According to the Boston Consulting Group, the net costs of manufacturing in China will equal that of the US in 2015.

Furthermore, global supply chains and offshoring are risky in today's volatile world. Political uncertainties — like pirates in Somalia and strikes among factory workers in China — and environmental uncertainties, such as the eruption of an Icelandic volcano in 2010, can lead to huge delays in delivery and even lost shipments.

Increases in oil prices and the fluctuation of currency also play a part. Imagine if the Yuan appreciates against the dollar: suddenly, labor costs and shipping from China would be more expensive — enough to cut into profits and force companies to totally reconsider manufacturing practices.

### 6. There will be more small-scale manufacturers. 

So what will happen to the giant manufacturers? Will the Maker Movement kill them off?

Not exactly. Just as AT&T didn't vanish with the rise of the web, 3D printing probably won't destroy large manufacturers such as General Mills. Those companies will, however, lose their monopoly as manufacturers, and will have to compete with small enterprises that make things for niche markets. 

What has happened in the digital world is now happening in the real world: Manufacturing is becoming increasingly decentralized thanks to lower barriers to entry and new technology.

In the digital world, publishing became democratized with the help of tools such as Wordpress and Tumblr. Using these services, anybody can publish their thoughts.

Broadcasting too was pushed toward democratization. Just think of how many people today own a digital camera and can upload videos and distribute them on YouTube. They can even create their own channels!

In the real world, everyone will be able to produce real goods in their home or with the help of specialized factories using 3D printers and other digital manufacturing techniques.

This increasingly decentralized ownership of the means of production leads to greater access to production technology and less reliance on manufacturing giants. Now, small businesses and even private individuals have access too.

Take Local Motors, for example, an American car manufacturer that produces a small number of cars, 2,000 in fact, using 3D printing and other digital manufacturing methods.

Thanks to the digitalization and democratization of manufacturing, custom-made products become cheaper to produce and run in small batches. Instead of one-size-fits-all production, 3D printing manufacturers can offer one-size-fits-one solutions.

As anyone can design and modify objects in digital files that can be shared, producers no longer need to manufacture items _en masse_ to make a profit.

No matter what you need, from tools to dollhouse furniture, Makers will be able to create custom designs based on your specifications, and you might never again have to rely on a major manufacturer.

### 7. Final summary 

The key message in this book:

**Manufacturing is becoming democratized with the introduction of new design and production technologies. Increasingly, people have greater control over what goods are produced, and this will have major consequences both for the organization of industry as well as the products and services available to the consumer.**

**Suggested** **further** **reading:** ** _The Zero Marginal Cost Society_** **by Jeremy Rifkin**

_The Zero Marginal Cost Society_ (2014) lays out a strong case for the self-destructive nature of capitalism, demonstrating how it is sowing the seeds of its own destruction. But in its wake, a new, collaborative, democratized economy will materialize — one made possible by the internet.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Chris Anderson

Chris Anderson is a journalist, entrepreneur and author of the internationally acclaimed books _The Long Tale_ and _Free_. He is also the former editor-in-chief of technology magazine _Wired_ and CEO of the drone manufacturer 3D Robotics.

