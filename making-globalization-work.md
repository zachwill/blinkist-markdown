---
id: 557da25765333800074e0000
slug: making-globalization-work-en
published_date: 2015-06-16T00:00:00.000+00:00
author: Joseph E. Stiglitz
title: Making Globalization Work
subtitle: None
main_color: None
text_color: None
---

# Making Globalization Work

_None_

**Joseph E. Stiglitz**

_Making Globalization Work_ (2007) explores the problems and potentials of globalization. So far, globalization has been beneficial to wealthy countries but harmful to developing countries. We need to reform our international systems if we want to make globalization beneficial to everyone.

---
### 1. What’s in it for me? Learn how to make globalization the positive force it should be. 

When you think of globalization, what comes to mind? The label on your cool but affordable T-shirt with "Made in Indonesia" emblazoned across it? Or maybe that factory in your hometown that closed when production moved to China?

Globalization has been simultaneously good and bad for both the developed and the developing world. Access to new markets and products has brought substantial benefits — but it has given rise to huge problems, too: the steamrolling of local governments by multinational companies and the destruction of the environment, from rainforests to wetlands to oceans. The uncomfortable truth may be that, in spite of all its good points, globalization might be doing more harm than good.

But it doesn't have to be this way. These blinks, based on the thinking of Nobel Prize-winning economist Joseph Stiglitz, show you how we can make globalization work for all of us.

In these blinks, you'll discover

  * how drug companies tend to keep people from using their drugs;

  * why countries rich in natural resources are often impoverished; and

  * why free trade is currently far less free than you'd think.

### 2. Globalization has been harmful to countries in the developing world. 

You may be happy when the price of tea in your local supermarket goes down, but have you thought about what it means for the people who produce the tea in India? Or the shipping company that transports it?

Globalization has revolutionized commerce by allowing us to get products from all over the world. But has it had a positive impact overall?

In the early 1990s, when globalization really started taking off, many assumed it would improve life worldwide. It was thought that globalization would increase the global standard of living and result in unprecedented prosperity.

The promise did come true for some countries. Hong Kong, South Korea, Singapore and Taiwan benefited greatly, in what became known as the _East Asian miracle_.

Increased productivity, greater access to international markets and technological advances allowed the East Asian-miracle countries to strengthen their economies by exporting more goods. They've experienced an average growth rate of 5.9 percent over the past 30 years.

Things haven't gone as well in all parts of the world, however. And even in countries that _have_ become wealthier, the wealth usually isn't equally distributed.

According to a 2004 report by the World Commission on the Social Dimensions of Globalization, 59 percent of the world's population live in countries where economic inequality is increasing. The list includes the US, the richest country in the world.

This inequality has led many to believe that globalization is leading us in the wrong direction. If we want globalization to be beneficial for everyone, we need to change the way it functions.

Globalization _can_ be made to work if we get the politics right. There are seven main areas where changes in public policy could transform globalization into something more positive. We'll look at each of them in the following blinks.

> _"Another world is possible."_

### 3. The benefits of trade liberalization must be shared more equally with the developing world. 

Many people assume that free trade is one of globalization's positive aspects, because it allows both rich and poor countries to benefit by trading with each other. But is this true?

Well, not exactly. International-trade agreements, in fact, tend to be more profitable for wealthy countries.

Developed countries have often used these agreements to force markets in the developing world to open, without opening their own economies in the same way. They might do this by removing government subsidies, for example.

About one-third of the EU's farm industry is subsidized. That means European countries get a steady supply of cheap, subsidized products they can export to developing countries that aren't allowed to subsidize their own farms. That undercuts their local production and agricultural development.

However, trade liberalization doesn't automatically lead to increased trade and growth, either. Industrialized countries benefit from open markets; developing countries, however, face greater challenges when producing goods. They might lack the necessary infrastructure, for instance.

In fact, when Europe opened its markets to the _Least Developed Countries_ (LDCs) — countries with a GDP per capita of less than $800 — in 2001, there was almost no increase in trade.

We need to fight poverty in poorer countries if we want trade liberalization to truly improve trade. Developing countries need free access to industrialized markets, and we have to remove their agricultural subsidies and high tariffs to make trade really free.

Such changes would benefit the developed world, too. As long as economic conditions in the developing world remain harsh, people will keep immigrating to the developed world, which might ultimately result in significant societal problems.

Developing countries also can't provide goods and services for industrialized countries if their economies don't improve. Clearly, struggles in the developing world affect everyone.

There's a lot of potential in international trade. Globalization would function much smoother, however, if trade liberalization was equally shared.

### 4. We need to reform our intellectual property rights – especially when it comes to medicine. 

Pharmaceutical companies invest a lot in researching and developing new drugs. That's why they're granted the rights to anything they've produced.

These _intellectual property rights_ (IPR) are supposed to help drive innovation — but do they?

Contrary to popular belief, IPR _don't_ improve economic performance. In fact, they lead to economic problems and may even _impede_ innovation.

IPR regimes ultimately just protect monopolies, making it more difficult for new companies to enter the market. That decreases market competition, one of the main forces driving innovation.

This might be the reason that, in recent years, only a few drug companies have developed improved versions of drugs they already sell.

IPR are especially harmful in the developing world because they limit people's access to affordable medicine. Botswana, for example, has a population that's one-third HIV-positive. It doesn't have the resources to produce a generic AIDS medication and the _international agreement on intellectual property rights_ (TRIPS) forbids the trade of generic drugs.

Most people in Botswana can't afford to buy AIDS medication from the developed world. But if they aren't allowed to import a generic version, the population will continue to suffer.

All in all, IPR should be tailored to each country. International IPR laws are harmful in countries (like Botswana) facing extreme situations.

Some practices aiming to do this have already been put in place, such as _compulsory licenses_. Compulsory licenses allow firms in developing countries to produce and sell generic medicines if there is an urgent need for them.

This doesn't just increase access to important medicine. It also allows developing countries to build up their pharmaceutical industries so they can become more self-sufficient in the future.

It's dangerous to limit people's access to information, especially when it comes to health. International IPR laws need to be changed; if they're not, people in the developing world will keep suffering, all in the name of the interests of the wealthy.

### 5. Globalization has the resource curse, whereby countries suffer when they're rich in natural resources. 

The international market for natural resources is perhaps the best illustration of globalization's downsides.

Countries with many natural resources usually have weaker economies than those with few. This contradiction is called the _resource curse_.

In extreme cases, the resource curse results in war and violent conflict, as groups fight over the valuable resources. In Sierra Leone, for example, conflict erupted between the government and rebel groups in the 1990s. Over 75,000 people were killed and two million were displaced.

Two main reasons account for the resource curse. One is that countries with large reserves of oil, gas or minerals become economically dependent on those resources, making their economies highly susceptible to changes in the market. 70 percent of Russia's GDP is related to natural resources, so the country's revenue decreases when resource prices go down.

Wealth from natural resources also generates more power for the elite few who control them, and this elite often exploits the resources to stay in power, resulting in political instability and corruption.

Nigeria, for example, is rich in oil but has extreme income inequality. Most of the population is very poor.

The international community has to come together if we want to solve these problems. We need more transparency so we can enact anti-bribery laws and curtail corruption.

In order to diversify their economies and reduce their dependence on resources, wealthier countries also need to invest more in resource-rich countries. Botswana managed to decrease their dependence on diamonds by establishing a _stabilization fund_ to reinvest diamond profits into other sectors of the economy, resulting in more stable growth.

Not all countries succumb to the resource curse, however. Norway, for instance, is rich in oil and very prosperous. We have to fix political problems in the developing world before everyone can benefit from their country's resources equally.

### 6. Global warming is one of the greatest challenges of globalization. 

Global warming is a tremendous problem that nobody wants to pay to fix. The planet has been profoundly and adversely affected by the _Tragedy of the Commons_, whereby people don't consider the harm of their actions because resources seem endlessly abundant.

We need to change this — and fast.

The international community needs to act now to conserve energy, reduce damage to the environment and conserve natural resources more efficiently. There's still some uncertainty about how exactly climate change will change the world, but we need to prepare for the worst-case scenario. If the polar ice caps melt and great swaths of London and New York are submerged in water, it'll be too late.

The _Kyoto Protocol,_ a 1992 international treaty to reduce the global emission of greenhouse gases, illustrated the difficulties of fighting climate change on a global scale.

Developing countries opposed it because they didn't want to be penalized for the decisions of the developed world, especially at the expense of their own growth. Wealthy countries, on the other hand, didn't want to risk losing their wealth. That's why the US, the world's largest polluter, didn't ratify the treaty.

It's very difficult to make meaningful progress toward protecting the environment, and, in order to do so, it's crucial that we use certain market mechanisms to our advantage.

If we impose fees on environmental damage, the market price of a good would reflect its true cost. This would cause people to reduce their consumption, lessening the burden on the environment.

Forcing people to pay duties has worked before. The Montreal Protocol of 1987 used trade sanctions to ban _ozone-depleting gases_ (CFCs), which were used in refrigerators at the time. Companies felt so threatened that they phased out CFCs much faster than anyone anticipated.

There's no point in fixing globalization if our planet is uninhabitable. Reversing climate change is vital for absolutely everyone.

### 7. Corporate incentives have to align with public interests for globalization to be healthy and sustainable. 

For many, corporations are the evilest part of globalization. There are corporations richer than most countries in the developing world.

Corporations play a central role in globalization, and they have both positive and negative impacts.

It's true that corporations _do_ help bring the benefits of globalization to the developing world; they raise the standard of living by offering inexpensive goods, providing jobs and driving economic growth.

But they also weaken communities by destroying small businesses. Moreover, they tend to engage in bribery and corruption, illegalities they're rarely penalized for.

In Papua New Guinea, for example, the gold and copper mine O _k Tedi_ dumped tons of contaminated material in a local river over a period of several years. They also extracted six billion dollars worth of ore.

_Ok Tedi_ left after the mine was depleted, saddling the government with the cleanup costs.

Another problem with corporations is that they're international entities subjected to local regulation. International cooperation is needed to fix this.

Governments should limit corporations by enforcing global antitrust regulations that prevent monopolies from abusing their power. Mario Monti, an EU commissioner managed to do this with the American corporate giants GE and Honeywell. He prevented them from forming a large monopoly that would have reduced competition in the global market.

Corporate incentives also need to be changed if we want to make globalization work. We need a more active civil society that won't give their money to corporations engaging in unethical practices.

In recent years, international companies have become increasingly aware that harmful behavior in other countries damages their reputations. Many have enacted _Corporate Social Responsibility_ programs to improve their image.

Furthermore, social welfare will never be guaranteed if corporations focus solely on their profits. Governments and other key players in the international community have to work together to ensure that corporate incentives align with public interests, especially in the developing world.

### 8. We need to restructure the system for international debt. 

These days, it's common to graduate from college with a great deal of debt — and it's certainly difficult to pay for a house or car while also paying off your loans.

Excessive debt poses similar problems for countries. When governments are in debt they have to make sacrifices in other areas, like education or healthcare. If we want to avoid this problem, we have to get down to its roots.

Lending is a two-sided process: if someone is borrowing too much, someone else must be lending too much. Both lender and borrower are to blame.

In fact, lending countries tend to encourage other countries to go into debt because the lender stands to make a profit. Moreover, they're inclined to lend even more if there's an option of a bailout, because if the borrower gets into trouble, international agencies will pay the creditors out.

This happened in the East Asian Crisis of 1997. Creditors refused to renew their loans, and the IMF stepped in and leant credit to indebted countries so that the loans could be repaid.

If we want to fix this problem, we have to do more than just make over-lending less profitable. Debtor nations also need their debt restructured in a more straightforward way.

We need an international, debtor-friendly organization that can restructure debt in a less expensive way. Such an organization would need to ensure that governments can get out of debt quickly.

We're currently a long way from this. There's no form of international bankruptcy for governments, which makes getting out of debt even harder.

When Argentina defaulted on its debt in 2002, it took years of negotiations before an agreement was reached. We're clearly in need of a better debt system.

In 1996, the IMF and World Bank countries initiated a debt relief program that aimed to grant $56 billion to 28 of the world's poorest countries by 2005. Countries were disqualified if they didn't meet certain conditions, however, such as a minimum income per capita. This upped lender responsibility, which the program heads hoped would reduce the problem of excessive debt in the future.

> _"Banks lend money only to those who do not need it."_

### 9. The global reserve system needs to be restructured to make globalization work. 

The global financial system isn't working well, especially for the developing world. Wealthy countries rely on poorer ones for a great deal of their wealth and material goods.

The international dollar-reserve system is also highly imbalanced. The US issues the world's reserve currency and uses it to buy cheap goods from China. In return, China purchases US dollar reserves.

In order to be less susceptible to changes in the exchange rate, developing countries collect USD reserves, storing their wealth in a currency that's stronger than their own.

This system is unsustainable, however, and impedes economic growth. To break this pattern, the wealth that developing countries hold in reserves should be used to strengthen their own economies.

There's currently $4.5 trillion held in reserves throughout the world, and an additional $750 billion is added to that every year.

Moreover, as US debt mounts, confidence in the dollar falls, which results in investors putting their money into other currencies. If the world moves away from the dollar too quickly, it could cause a huge economic crash.

We need a new international-reserve currency. An international institution should distribute _global greenbacks_ to countries that can exchange them for their own currency in times of crisis.

Also, if the global-reserve currency wasn't able to accumulate excessive debt (like the dollar), people would have more faith in it, and the international economy would become more stable.

The countries affected by the Asian Financial Crisis of 1997 inspired the search for a better global reserve currency. China, Japan and South Korea made an agreement to hold their reserves in each other's currency, which decreased their reliance on the dollar and supported consumption in the region.

An improved reserve system would mean significant gains in international social justice by allowing poorer countries to reinvest more of their wealth in their own development.

### 10. We need to start restructuring globalization now to make it beneficial to everyone. 

Globalization clearly hasn't played out in the most effective way. We need to restructure it so that it benefits poorer countries, as well as impoverished people in wealthy countries.

For one, globalization is not managed democratically. International institutions like the _IMF_ and _World Bank_ protect the interests of industrialized nations much more than those of developing nations. In fact, a country's voting rights in the IMF are determined by the contributions they make to the IMF fund every year. Naturally, wealthier countries can contribute more, so they end up having greater power over poorer countries, which can't.

This has to change for globalization to work. Developing countries need better protection against wealthy countries, which are well positioned to exploit the poorer countries' resources.

Developing countries would also benefit if we removed their trade barriers and tariffs. That would lead to greater economic integration, work against the global shortage of capital and close the knowledge gap between richer and poorer nations.

Countries are growing more interdependent these days, so our problems can only be solved by collective action. We need international, democratic institutions to ensure that globalization improves the lives of people all over the world — not just a few of them.

Globalization has enormous potential, but we won't see the full benefits of it without reform. At the moment, globalization is harmful to virtually all but the rich, as well as being damaging to the planet. Our economic well-being — and perhaps our very survival — is at stake. We have to start reforming globalization now.

### 11. Final summary 

The key message in this book:

**Globalization has fundamentally changed the world. Though it** ** _has_** **brought great prosperity to some, it's been devastating to others and harmful to our planet. We need to reform several sectors of the international economy if we want to make it beneficial for all. Only collective action can solve our global problems — and we need it now.**

**Suggested further reading:** ** _No Logo_** **by Naomi Klein**

_No Logo_ takes a look at how the power of brands has grown since the 1980s, and how companies have emphasized their brand image rather than their actual products. _No Logo_ shows how this strategy has affected employees in both the industrial and the non-developed world. _No Logo_ also introduces the reader to the activists and campaigners who are leading the fight back against multinationals and their brands.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Joseph E. Stiglitz

Joseph Stiglitz is a world-renowned economic theorist and the former chief economist of the World Bank. In 2001, he was awarded the Nobel Memorial Prize in Economic Sciences.

