---
id: 55759fb83935610007660000
slug: making-habits-breaking-habits-en
published_date: 2015-06-11T00:00:00.000+00:00
author: Jeremy Dean
title: Making Habits, Breaking Habits
subtitle: Why We Do Things, Why We Don't, and How to Make Any Change Stick
main_color: 89D0E4
text_color: 4B727D
---

# Making Habits, Breaking Habits

_Why We Do Things, Why We Don't, and How to Make Any Change Stick_

**Jeremy Dean**

_Making Habits, Breaking Habits_ (2013) provides an overview of exactly what habits are and how we form them. Using this knowledge, it reveals how to create healthy habits and tackle the bad ones so that we can experience lasting, positive change in our everyday lives.

---
### 1. What’s in it for me? Take control of your habits – both good and bad. 

No matter how straightedge or in-control you think you are, your life is dominated by habits. The thing is, these habits can be good! They can be part of your morning routine, or constitute your mannerisms during conversations. Such habits should be celebrated, or even fostered, like when you want to start cooking more at home, or reading more.

However, not all habits are good. Many people know they should quit smoking or maybe lose a few pounds, but they don't think about quitting their more invisible habits, that is, their negative thought habits. These habits can be very dangerous.

These blinks start at the very beginning by explaining what habits really are, and then explain how you can take control of your habits and transform your life for the better.

In these blinks, you'll discover

  * why your family always sits in a certain order at dinner;

  * how to make _happy habits_ ; and

  * what merely trying to quit smoking does to your self-control.

### 2. Habits are repeated behaviors with little to no conscious intention. 

What happens when someone throws a ball to you? In all likelihood, you catch it before you consciously realize what's going on. This is a _habit_ — an action repeated so frequently that it's done unconsciously.

This first aspect of a habit involves _automaticity_, that is, not being aware of the execution of a given action, such as flicking on a light while entering a room.

Also, since repetition decreases enthusiasm, the act of performing habits is _emotionless_.

Consider your morning routine: Does it conjure up strong emotions for you? Or imagine looking out upon a mountain range for hours from your office window every single day. It's wonderful and breathtaking at first, but, over time, the pleasure of seeing it greatly decreases.

In addition to automaticity, _context_ also defines habits because of the associations you form between your surroundings and your behavior. Remember what it was like to be a student? The freedom of almost zero obligations and the beers you enjoyed with your friends? You may well connect the pleasures of socializing with the habit of drinking alcohol because of these early experiences, so now every time you socialize, you want to drink a beer.

But how are habits actually formed? Take a look at the following:

First, intentions create habits. For example, you want to have healthy, white teeth, so you start brushing them regularly.

Another way habits are formed is through explaining random past behavior by adding an intention later on. Say you always sit in the same place in your friend's kitchen because it was the only free spot when you first visited her. Now, however, you tell yourself it's your favorite place because the light is just right and the chair is comfy.

Finally, you can combine both the intention and explanation behind your habits. Perhaps you started biking to work because you were dieting, but you continue doing it because you enjoy being out in the fresh air.

> _"Awareness of the power of the unconscious to guide and change our thinking and behaviour is the first step to change."_

### 3. Habits are omnipresent, and when they’re bad, they can be very bad. 

What habits play a role in your life? Perhaps dieting and smoking spring to mind. But our lives are actually full of many different habits.

A minimum of one-third of our waking life is powered by our unconscious, where we operate on _auto-pilot_, not fully cognizant of what we're doing while we're doing it. It's not surprising, then, to find you have many more habits than you might think.

_Social habits_, such as who sits where at the family dinner table; _work routines_ like saying "mm hmm" and "a-ha" during meetings; _eating habits_ that help us sift through multitudes of food-related decisions every day — and the list goes on!

Do you ever catch yourself checking your email for the hundredth time only to discover that, still, nothing interesting has arrived in your inbox? Then you've experienced what behavioral psychologists call the _partial reinforcement extinction effect_, when you keep repeating the same action, even without reward, simply because you're used to doing it unrewarded.

Even if we get the rare reward of an interesting email, we keep robotically refreshing our inbox regardless, as we're used to the frustration.

But there are other habits that you can't see: habits of thought. If they are negative in nature, these habits can be connected to mental illnesses such as depression.

Whether thoughts are positive or negative depends on our _appraisal_ of something that happens to us, and sometimes we appraise in unhealthy ways.

Imagine you lost your job. If you're in the habit of perceiving yourself as powerless and culpable, you'll have trouble fighting the negative emotions that unemployment entails.

Another type of habit is _rumination_ — when you think about something over and over again. Some say that retrospection can help us learn from our supposed failures, but there's a difference between reviewing your past experiences and wallowing in the misery and pain of them.

> _"Habit provides a safety zone, but it's also kind of a cage from which escape is hard."_

### 4. Happy habits can be created. 

What you might not know about habits is that, even though they stem from your unconscious, you can manipulate them. This is good news indeed, especially if you want to form a new, healthy habit.

To do so, follow these three steps:

Determine your _motivation_. You need an overarching goal to help you overcome obstacles. To pinpoint your ultimate goal, you can employ the _WOOP_ — wish, outcome, obstacle and plan — _exercise_.

Start by writing your wish down, along with the best possible outcome, and the obstacles you are likely to face.

Say that your wish is to run daily. Your outcome might be to get in shape so you can complete a 10km run and your obstacles will probably be harsh weather and physical discomfort.

Next up, you need to plan by finding the right _implementation intention_ and act on "if x, then y" decisions. For example, "If I enter a building, I will take the stairs."

Positive statements like "I'll take the stairs" are far more effective than "I'm not taking the elevator," as self-denial reinforces the attraction of something — in this case, the elevator.

Next, remember to repeat your actions, as repetition leads to automation. If you're unsatisfied with your habit development, try _coping planning_ :

Anticipate challenging situations for your new habit, such as heavy rain or being late for work, and find the right if-then solution. For example, "If it is raining, I'll try my waterproof running gear."

Finally, make your habits _happy habits_. One way to do this is to avoid becoming _habituated_ to something and just going through the motions. Ironically, "habituation" is the anathema to happy habits.

Having happy habits means switching it up. This could mean taking different routes to work in order to keep enjoying your bike rides, or techniques like _savoring,_ where you pause and intentionally engage your senses to savor the moment, such as stopping to smell a fragrant flower.

> _"Developing a good habit will be most successful when we do it for its own sake, when it's done automatically, and when we take satisfaction in what we've achieved."_

### 5. Breaking habits is hard but you can do it. 

Nearly all of us have bad habits we want to kick. Perhaps we're overweight or want to rid ourselves of nicotine addiction. For many of us, though, it's hard to change.

One survey, conducted in the 1980s, at the University of Scranton, found that out of 213 people, 60 percent weren't able to stick to their New Year's resolutions. So what can we do to break free of these habits?

First, recognize the habit you want to kick. The consequences of bad habits are often more obvious than the habits themselves. It's easy, for example, to recognize that you're overweight, or, if you're a smoker, that you're out of breath.

To find them, you can use _mindfulness,_ the conscious recognition of what you're doing in any given moment.

Mindfulness takes a little practice. You can start with the following exercise:

Relax your body and sit comfortably in your favorite chair. Then, give your focus to one thing, such as your breath. As you sit, be non-judgmental and open-hearted with yourself and compassionate toward your thoughts and feelings.

This is the first step toward dropping your bad habits, as the more mindful you are, the more aware you'll be of what you're doing, including your habitual actions.

Another great approach working on your _self-control_.

Self-control is like a muscle; it strengthens through training. Merely trying to break a habit can help.

When you get frustrated, bear in mind that we sometimes tend to overestimate our ability to control ourselves. Say you want to quit smoking. Just trying to stop will be good for your willpower. Once you know you can go one week without nicotine, for example, you can try going for two.

Here are some other basic ways to get your habits under control: _monitoring behavior_, by, say, keeping a food diary; _distraction_, like chewing gum instead of smoking a cigarette; and _changing your environment_ — for example, moving to a new, non-smoking apartment.

### 6. Final summary 

The key message in this book:

**Habits play a huge role in our lives. With a little awareness of what habits actually are and a bit of practice in some straightforward psychological techniques, we can break free of our unhealthy habits and create ones that serve us.**

Actionable advice:

**Hack your diet.**

Try some of these tricks to make sure you stick to your diet:

  * We tend to eat food that is close and visible, so put some fruit on the table instead of cake.

  * Use smaller plates at meal times. Larger plates encourage you to eat more than you actually need.

  * Try eating with your non-dominant hand. This slows you down and gives you a chance to register that you're getting full, so you don't automatically reach for more.

**Suggested** **further** **reading:** ** _The Power of Habit_** **by Charles Duhigg**

_The Power of Habit_ explains how important a role habits play in our lives, from brushing our teeth to smoking to exercising, and how exactly those habits are formed. The research and anecdotes in _The Power of Habit_ provide easy tips for changing habits both individually as well as in organizations. The book spent over 60 weeks on the _New York Times_ bestseller list.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Jeremy Dean

Jeremy Dean is a psychologist and the founder of the immensely popular _PsyBlog_, a website dedicated to scientific research into how the mind works. _PsyBlog_ has been cited in such media outlets as _The Guardian_, _The New York Times_ and _Wired_.

