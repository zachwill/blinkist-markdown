---
id: 534d4f1666336100074b0000
slug: making-it-all-work-en
published_date: 2014-04-15T05:53:29.000+00:00
author: David Allen
title: Making It All Work
subtitle: Winning at the Game of Work and the Business of Life
main_color: 2158A6
text_color: 2158A6
---

# Making It All Work

_Winning at the Game of Work and the Business of Life_

**David Allen**

These blinks build on the principles laid down in David Allen's previous time-management smash hit, _Getting Things Done_ (also available in blinks). It explains how you can manage your tasks and pursue your meaningful life goals.

---
### 1. What’s in it for me? Find out how you can be productive in a meaningful way, not just busy 

These days we seem to be perpetually bombarded with distractions. Most days it's a miracle if we can go five minutes without being accosted by an email, a social media notification or text message.

In this mercilessly distracting environment, it's more important than ever to be able to focus properly.

If you spend most of your days working on pointless little tasks and feel you are being distracted, these blinks can help you realign your daily actions with the big priorities in life: your overall goals and dreams.

In these blinks you'll discover why we humans drop everything important at the beep of an email inbox.

You'll also learn why being busy is not the same as being productive.

Finally, you'll find out how to implement the great advice from _Getting_ _Things_ _Done_ in every sphere of your life.

### 2. We are easily distracted and therefore lose track of our priorities. 

How often has this happened to you: You're working on an important document when all of a sudden an incoming email pops up. What do you do?

If you're like most people, you'll probably instantly forget about the high-priority document and answer the email.

But why is this?

It's basically because our minds are fickle.

These days we're bombarded with new information in fantastic quantities, from tweets to emails to text messages.

And when this new information arrives, we have no way of knowing if it is important unless we divert our attention to it: If your email inbox goes "ping" to signify a new email, you won't know whether it's spam or a job offer until you look at it.

This means we are perpetually distracted, and therefore struggle to focus on the important task for long enough to finish it.

What's more, the constant influx of new information and new tasks means that the _quantity_ of work done is increasingly emphasized as a metric for productivity: salespeople have sales targets, teachers have student graduation targets, etc.

But the quantity of tasks done is not a good metric to focus on. Though sending lots of emails and attending lots of meetings may make an employee feel busy and productive, it in fact says nothing about the _quality_ of their work. A far better barometer for your productivity would be to see how much progress has been made on the predefined important goals.

This focus on quantity can also cause us to get so bogged down in the nitty-gritty day-to-day tasks that we lose track of our larger life goals.

For example, say your dream is to become a writer, but you're always so distracted at your day job that you need to work evenings to make up for your low productivity. In this case, you simply owe it to yourself to get organized, at work and at home, to be able to focus better and have the time to work toward your personal ambitions.

### 3. To reach your full potential, you need to be both creative and well organized. 

Would you consider yourself a dreamer or a doer?

This question highlights two major traits required to achieve your potential: you need both _perspective_ — meaning the ability to see the big picture — and _control_, meaning the ability to actually organize and manage your obligations in line with that big picture.

Most people, however, can only manage one of these aspects, if that. Indeed, some people seem to lack both control _and_ perspective. Such people are fairly helpless, anxious and seem to constantly be in a panicky emergency mode. They would probably struggle to host even a dinner party.

Then there are those with a firm penchant for control, but no perspective. Imagination and creativity are not their strong suits, and they prefer to classify and organize existing ideas than to come up with new ones. Someone like this might, for example, excel at tweaking the fonts of documents or restocking the printer with paper, but be totally helpless when it comes to actually composing new proposals.

And then there are those who lack control but are rich in perspective. These people have lots of ideas, as evidenced by their constant doodling and note-taking, but they never get very far with them, because they always get distracted by the next great idea.

As you may have guessed, the optimal solution is to have a balance of control and perspective. You can attain this enviable situation only by being very organized with your tasks and your goals. This will leave you with the freedom and confidence to be creative.

This combination is exemplified by people who have the perspective to dream up new, innovative ideas, but who also have the control to implement those ideas and bring them to life. Whether tackling a difficult project at work or just building a gazebo in the garden at home, their success derives from these dual abilities.

In the next blinks, you'll discover how to regain control over all your obligations and plans.

### 4. Outsource your memory: start writing down all your ideas and tasks. 

How many times have you gone to the store in order to buy three things and only walked out with two? Or how many times have you had a brilliant idea at lunchtime, but completely forgotten what it was by the evening?

Chances are this happens fairly often.

In fact, all of our to-dos and intentions are in danger of being forgotten unless they are captured permanently somehow. And the only way to capture all of them permanently is to _outsource_ _your_ _memory_.

This means just dumping all your thoughts and ideas onto paper. Get into the habit of using journals where you record every idea, task and thought that you have. Don't worry about bad ideas or everything coming out as stream of consciousness, the main thing is to put everything on paper. Don't rely on just software though — really do this on actual paper too. Keep a couple of these journals around your home and office so you can quickly jot down ideas as they occur.

Also, whenever you're holding a meeting, be sure to put up a whiteboard where you can scribble down notes and ideas that arise. Make mind maps of them and then take pictures of them afterwards so nothing is lost.

As you're capturing ideas and tasks, be sure you not only write down the immediately actionable ones, but also the longer-term projects, like your lifetime goals.

These could be projects like reassessing your investment portfolio, learning to play the piano or studying a new language. Also, every time you think of something that could help you pursue these dreams — say, hiring a piano teacher — write it down as well so you're more likely to act on it.

### 5. Organize your ideas and define simple, actionable tasks to pursue them. 

By now you know the importance of putting down in writing all the ideas that cross your mind and hopefully you have a nice long list of notes.

What now?

Obviously, to get the most out of your notes you need to sort and order them. Start by going through each idea and deciding which ones are actually actionable. For these, come up with easy, specific, physical actions that need to take place to make that idea come to fruition. The more specific you get here, the easier it will be for you to visualize and complete the task, so if possible, define a specific time and place where you will tackle it. Keep your calendar open to facilitate this.

The actionable tasks should be such that by completing all of them you will have also realized long-term projects or goals.

For example, when you initially scribbled down your ideas you may have included the thought to "do something for Dad." The first thing you should do is to turn this vague notion into a concrete project, like "Give Dad an amazing seventieth birthday party." This project is then broken down into multiple individual actions like booking a venue, ordering a cake and hiring a bluegrass band. But you might also find that "doing something for Dad" is part of an even bigger goal you had: spending more time with your family.

Another, more professionally oriented example would be if you had initially jotted down "improve employee morale." As before, you would first try to get to a more concrete level, defining, for example, the project "organize regular team events." This again comprises multiple individual actions, such as "book the karaoke bar" and "send out the invitation." On the other hand "improve employee morale" might only be a subset of an overall goal like "reduce staff turnover by 15 percent," which is intended to be long term.

### 6. Organize tasks into categories according to when and where you’ll do them. 

By now you should have a long list of actionable tasks that add up in a meaningful way. And if you're like most people, in addition to this list you're probably surrounded by a maelstrom of Post-It notes, paper to-dos and reminders in digital calendars.

For this pile of tasks in their various forms to make any sense, they need to be organized and prioritized.

Start by dividing your actionable tasks into three to-do lists: _tasks_ _to_ _be_ _done_ _now_, _at_ _a_ _later_ _time_, or _by_ _someone_ _else_. The tasks in the first list are ones that can be done immediately, whereas those that go into the second will probably benefit from subcategories that add more context to the task.

For example, if you come across the task "draft job application," you could put it under the subcategory "Later — tasks for when I'm at the computer."

Or if you see something that you'd like to read but don't have the time, you can categorize it under "Later — reading."

You can also define subcategories for all the important people in your life so you know what to say when you next come into contact with, for example, your spouse or an old colleague.

The third list should contain the tasks that you've delegated to someone else and whose results you are awaiting.

Once you've dealt with the actionable tasks, everything else should be stored for later use in separate lists. This stops them from burdening your immediate attention so you can focus on the essential.

An example would be a note saying that one of your life goals is to help the less fortunate in society. Such ideas should go on the "vision" list.

You can also have a "maybe" list where you add items that may become relevant in the future. For example, though it's not the case at the moment, you may in the future be interested in collaborating with a third party, if circumstances allow.

### 7. Maintain your lists and workspace regularly so you’re not overwhelmed. 

Let's assume that you now have several well-organized to-do lists. How do you maintain them?

After all, it's highly likely that you'll come up with new ideas and tasks faster than you can take care of them. This means that your lists will keep expanding and eventually will be completely out of control, comprising pages upon pages of unfinished tasks.

So what's the remedy?

Quite simply, you need to review your lists regularly, and ruthlessly chop out tasks that are "stale" or no longer relevant.

Spend up to two hours each week reviewing and updating the content of all your lists, calendars and projects. For example, say you spent months looking for a new job and finally found one. This should have a huge impact on your to-do lists: get rid of everything that was related to finding a new job, such as searching through vacancies.

At the same time you need to assess whether you need to move things around in the subcategories. For example, perhaps a major life goal in the "maybe" subcategory, like "get fit" could be bumped up to an actionable project like "run next year's marathon."

Just as important as doing housekeeping with your to-do lists is regularly doing the same for the physical places where you keep your tasks and ideas.

Clear out your inboxes and in-baskets every day or two to avoid them becoming cluttered, and make sure you keep a clear workspace as well by collecting all your documents into one place for processing.

One more drastic measure could be to move your office somewhere else. The major reorganization required can often be beneficial at the end of the day.

In the next blinks you'll learn how to organize your personal and professional goals to match your long-term, high-level thinking.

### 8. Ensure your day-to-day tasks are meaningful by organizing them into projects that move you toward longer-term goals. 

Have you ever spent an entire day taking care of all manner of little tasks, only to look back on them later and realize that they didn't actually add up to anything meaningful?

If you want to stay true to your professional and personal priorities, you need to avoid this and ensure what you do day-to-day is important. To achieve this, you must do three things.

First, identify all the _responsibilities_ you have in life to ensure that you live up to them, whatever you do.

Map out all your duties to yourself and others onto an organizational map that branches out into more detailed areas.

One example of a personal area of responsibility that is often neglected is health. You might mark this on your map and branch out subpoints from it, such as dental care, annual medical checkups, exercise and diet. They don't necessarily need to be more specific than this — it is enough that you are aware of them.

The second thing you need to do to ensure your daily actions are meaningful is to group the individual tasks into manageable _projects_. Projects in turn should move you toward some higher goal, to be discussed shortly, and they should be attainable in under a year.

In a personal sphere, these could be to learn the basics of German in order to pursue the goal of eventually becoming fully fluent in it.

In the professional sphere, a project could be to hire a marketing guru or to set up a new online store, both aimed at attaining a bigger goal, like 15 percent growth in sales.

Finally, you need to assess your goals. _Goals_ differ from projects in that they are more long term and strategic: goals tend to be achieved in one to three years. For example, your long-term goal may be to get out of debt, and this could demand a variety of projects, like renegotiating your loans or moving to a cheaper city.

### 9. Organizations need to define their vision for where they want to go and the principles they’ll maintain to get there. 

So far we've mostly looked at how you personally can achieve your ambitions, but what about organizations? How can you get an organization to where it should be?

Obviously the first task is to identify the destination. Visualize _where_ _you_ _see_ _your_ _organization_ _going_ to achieve success. This will make your direction clear.

For example, your company might decide that its ultimate vision is to be the most popular grocery store in the region thanks to selling the highest quality produce. This vision would provide a clear path to follow: start procuring the best produce around.

The second task then is to get more specific: management should define the guiding principles — meaning _values_ — for the business to live by.

Typically an organization has anywhere from three to 30 principles like "helping local communities" or "providing superior customer service."

These principles must be defined by the management team, and then implemented in real life at all levels. Some companies even have a full-time HR manager to ensure that people adhere to the principles.

For example, if one of your company's principles is "develop employees," you might bring it to life by instituting rigorous 360-degree reviews of all staff members.

Once an organization knows where it wants to go (its vision) and how it should behave on the way there (its principles) than almost any question can be resolved easily. Even tricky dilemmas like "Should we acquire that company?" "Should we invest more in research?" or "Should we expand to this market?" can be resolved easily, because any answer must take the company closer to its vision and adhere to its principles.

### 10. Define your own path and your personal values, and then boldly answer the big questions in life. 

Imagine yourself walking along a forest path. Every time you come to a crossing you must guess which path you should take, because you can only see a stone's throw ahead.

Now imagine the same situation, but your viewpoint rises dramatically into the air. You can look down at yourself from a great height and see your path as well as all the other paths perfectly, so you know exactly which one to take.

Just as this ascension allows you to see where you want to go in the forest, you must similarly rise above daily minutiae to get a big-picture view of your life.

Ask yourself, where do you see yourself in ten years? If everything falls into place and you become wildly successful, where would you be? Would you, for example, build a mansion? Travel the world? Start a charitable foundation?

Collect all these ideal futures into a list: include all the events you'd like to see happen.

This view of the future, though speculative, is the path that you see in the middle of the forest — the one you want to be on. You should focus your efforts on getting there, and review this goal every year.

But in addition to knowing what you want to achieve, you should also know what your _personal_ _values_ are. For example, many people seek to be honest, wise and generous while leading a sustainable lifestyle and benefiting their communities.

For most people, their values are identifiable because as long as they fulfill them, they don't care where they are or who they're with. Write down your own values so you can refer back to them.

Finally, you must also boldly ask yourself the big questions in life. For example, how do you want to be remembered? Should you start a family? Is your career inspiring? Only by answering these will you be able to truly focus on what's important.

### 11. Final Summary 

The key message in this book:

**To be productive you must take control of your daily actions. Start capturing all your ideas and tasks on paper, and organize them into meaningful hierarchies according to your bigger goals in life.**

Actionable advice:

**Take care of quick tasks immediately.**

When you're doing something and a new task arises that can distract you from your more important tasks, evaluate how long it will take you to be done with this new task. If it is likely to take you less than two minutes — for example, sending off a quick email — do so immediately. If it takes longer, try to delegate it to someone else. Adhering to these two rules will work wonders in keeping your inbox at a minimum.

**Keep lists in your email programs.**

You can manage your various to-do lists effectively by embedding them in your email system. For example, in MS Outlook, you can use the Notes function to compose and maintain lists, or in Gmail you can simply keep all your lists as email drafts under different subject headings, editing them as required.
---

### David Allen

David Allen is an author and consultant who specializes in effective time management. His productivity method _Getting Things Done_ attracted disciples from many walks of life, not least workplaces and businesses. He gives consultations to individual and organizational clients, empowering them to make the most of their time.

