---
id: 562e412f3864310007880000
slug: making-the-modern-world-en
published_date: 2015-10-28T00:00:00.000+00:00
author: Vaclav Smil
title: Making the Modern World
subtitle: Materials and Dematerialization
main_color: 3A81BE
text_color: 2B5F8C
---

# Making the Modern World

_Materials and Dematerialization_

**Vaclav Smil**

_Making the Modern World_ (2014) is a guide to humanity's material consumption through history and into the future. These blinks explain the major material categories of our time and how we can effectively manage them as we move forward.

---
### 1. What’s in it for me? Change the way you think about your daily use of materials. 

Shelter, clothes, tools, furniture, devices — the list of materials we encounter on a daily basis can seem endless. In fact, without giving it much thought, we are all constantly using a myriad of different materials, all of which make up our modern world. So, how can we begin to understand this expanding and increasingly complex use of materials? 

In these blinks, you'll take a journey through the evolution of human material uses, and learn which principle materials should be taken into consideration when we analyze the basic building blocks of the world today. 

You'll learn about the extraction and production of materials, and how material consumption is a major cause of environmental pollution, which we can help by reducing our current material dependence.

You'll also learn

  * what the quintessential twentieth century material is;

  * how many tons of paper were used to design the Boeing 747 airplane; 

  * why we'll never run out of sand or silicon.

### 2. Material use surveys should include the raw materials used in every sector of the economy. 

From the production of clothes to houses to all manner of electronics, the sheer variety of materials used by modern-day humans is dizzying. But how can we determine which materials to take into account when analyzing our modern material flow?

Well, no human material use survey would be complete without considering agricultural and forestry-derived products, as well as metals, industrial gases and non-renewable organics. 

Back in 1882, the _US Geological Survey_, or _USGS_, began preparations for one of the first reports on material flows ever conducted for an entire country. Their survey grouped materials into major categories and covered the period between 1900 and 1995.

The categories, of course, included all raw materials derived from agriculture, including cotton, seeds, wool and tobacco; everything the forest industry produced, like wood and paper; and finally metals, minerals and non-renewable organics derived from fossil fuels, like asphalt, waxes and oils. 

To these, the author would suggest adding industrial gases, because such materials are essential to our modern methods of production. Other than that, USGS classifications are still valid today.

So, breaking material surveys down into such categories is a long-standing procedure; but it only works because it only assesses raw organic materials that are designated for further processing, while omitting oxygen, water, food, fuel and all _hidden material flows_.

Hidden material flows are all the materials extracted during a production cycle that don't end up in finished products, like all the earth and rocks that are moved to reach a mineral deposit. Materials like this would actually account for the vast majority of total material flow in countries with large mineral-extracting industries.

So, water isn't listed for quantitative reasons, because it would overshadow practically all other materials; leaving oxygen off the list makes sense because it's practically an inexhaustible element of the earth's atmosphere; and food as well as fuel are excluded because they have historically been analyzed separately, and aren't quite materials but instead finished products.

> _"[... We] will have to…redefine the very notion of modern societies whose very existence is predicated on incessant and massive material flows."_

### 3. The eras of human history are marked by their dominant materials. 

You need only watch a bird building a nest to know that humans aren't the only creatures to use materials in creative ways. But what _is_ unique to human material use is its overall extent and complexity — a fact that dates back to prehistory.

The prehistoric, ancient and medieval eras were characterized by the use of wood, stone and metal, respectively.

In prehistoric times, people produced hammers, axes, arrows and knives by expertly selecting stones for well-tailored manufacturing processes. However, wooden sticks were still used to dig for roots and hunt small animals. 

Then, during antiquity and the Middle Ages, stone's durability made it the material of choice for everything from utilitarian construction, like aqueducts and roads, to funerary structures and ceremonial monuments like temples and statues. But wood remained crucial as the sole material used for constructing the hulls and masts of seafaring ships. 

And while the most important material development of antiquity was by far the ability to smelt metals like copper, bronze and iron, no material was as common in preindustrial societies as wood. It's easy to see why when you consider all the timber used for buildings, and the wooden components essential to tools. 

Today, we use mostly metals and plastics, but paper and textiles are still essential. In fact, alongside rapid growth in the consumption of metals, modernization has seen vast increases in the extraction of traditional construction materials like wood and stone. 

For instance, steel products dominated new markets because of their unprecedented strength and durability, while copper became the metal of choice for wires and cables. 

And while it's unquestionable that the quintessential material of the twentieth century is plastic, which replaced wood and metals in countless household and industrial products, two other materials deserve mention. 

First, paper's mass production was revolutionized at the dawn of the nineteenth century with the invention of the continuous paper-making machine. Second, textile production was crucial because mechanized weaving paved the way for the mass production of fabric.

### 4. The modern world consumes a tremendous variety of materials. 

With new roads built daily and countless packages mailed every hour, the day to day of modern society relies on a never-ending and highly diverse supply of materials. But which material categories are most important? There are six in total.

The first two major groups of modern materials are _biomaterials_, and those used for construction. 

The first, biomaterials, consists of things as diverse as lumber, straw, cotton, wool, beeswax and natural rubber. While our consumption of many of these naturally derived materials has seen a steady decline as synthetic substitutes become increasingly prevalent, two biomaterials, namely wood and cotton, remain extremely valuable. 

The second material category that prevails in the modern world, _construction materials_, is currently dominated by all manner of sand, stones, cements and concretes. For example, brick production has climbed to unprecedented global levels due to China's impressive construction projects. 

The third and fourth categories are metals and plastics. In the twentieth century, the growth of industrialization, advances in transportation, mechanization and the boom of mass consumption fostered a burgeoning demand for metals of all kinds. The same was true for plastic products, as they're especially essential to packaging, construction, electronics manufacturing and the auto industry. 

Another major material category of our time, and one indispensable to society as we know it, is industrial gases, with oxygen, hydrogen and nitrogen being the three most important.

Why are they so crucial?

Becauses gases like these play critical roles in just about every major sector of the modern economy. For instance, without industrial gases, the efficient production of steel would be impossible.

The final material category that's crucial to our modern modes of production is electronics, along with the material essential to the production of most consumer electronics, silicon. In fact, ever since Intel's release of the first microprocessor in 1971, a device built with silicon, our current era has been defined as the _Silicon Age._

### 5. Material-flow accounts are made easy with a few tricks – and recycling might be more appealing than you think. 

Do you know where the different components of your smartphone came from? Well, in the case of Apple's iPhone 5, the constituent parts were made by over 20 different companies, operating factories in 12 countries spread over three continents! 

While the iPhone isn't the only product with such a complicated assembly pattern, it's a prime example of how material accounting can quickly become a challenge.

You can imagine how such a complex production process would make it difficult to keep a simple record of each individual material used; a better strategy is to focus on national material flows. 

For instance, if you look at the United States' material flows, you'll see increasing material consumption across every category, a marked decline in the relative role of biomaterials and the increasing importance of recycling. 

Another way to approach material-flow accounting is by employing a _life-cycle assessment_, or _LCA_. An LCA works by quantifying the environmental impacts of materials over the course of their entire lifespan. Armed with this information, you can make decisions by choosing the materials with the lowest environmental impact. 

For example, when paving a sidewalk, which material would you imagine to be the least environmentally impactful, natural granite or modern concrete? 

Actually, because of the energy required to cut and transport the stone, a granite sidewalk can have anywhere from 25 to 140 percent more impact than one made of concrete! 

Employing LCAs can also reveal the increasing role of recycling in reducing environmental repercussions. 

Did you know that recycling itself is actually a highly desirable flow? This is because we use billions of tons of materials every year, and recycling can do everything from drastically increase the lifetime of known mineral reserves, to decrease the demand for new biomaterials, or even increase the utility of materials that are transformed into finished products. 

Plus, recycling can also save loads of energy while reducing environmental impacts.

### 6. Using materials in a smarter way doesn't guarantee it'll diminish their consumption – it takes more complicated approaches than that. 

If you looked up the verb "dematerialize" you'd think that it holds supernatural connotations: the _Oxford English Dictionary_ defines it as becoming free of physical substance. However, for our purposes, dematerialization isn't about completely eliminating a material, but rather making substantial reductions in the _use_ of materials. 

For instance, consider how much less paper the world consumes since the computerization of archives.

Keeping this definition in mind, it's important to know that every form of dematerialization involves complex material substitutions. For example, the design and development of the now-ubiquitous Boeing 747 aircraft, an airplane that was conceived in 1965 and launched in 1969, involved approximately 75,000 drawings, which amounted to the use of nearly eight tons of paper!

In the 1970s, seeing the huge material waste, Boeing developed a _CAD_, or _computer aided design_ system. Their database eventually held over a million drawings and made paper unnecessary for both large or small blueprints. 

But building and maintaining electronic designs requires a complex infrastructure of modern computing, massive amounts of data storage and tons of servers to handle all that data traffic. Therefore, for the dematerialization of paper to occur, a wide variety of other materials are required to build computers and supply them with energy. 

In addition, general dematerialization has actually led to an increase in total material consumption!

Using materials sparingly doesn't necessarily mean lower consumption levels; just take the evolution of cars as an example. Dematerialization, as is evident in the increasingly efficient use of metals, has been a major driver of falling costs, greater affordability and the mass accessibility of cars. 

Or consider consumer products like cell phones; the steadily decreasing mass and therefore cost of such commonplace consumer items has meant an increase in their use. This has fostered steep increases in demand for the materials these products are made of, like light metals, plastics, glasses and, of course, silicon.

### 7. We have the means to handle the rising material demands of modern civilization. 

Increasing populations and improved quality of life have meant a steadily rising demand for materials across the board. But how long can and will this trend continue? And in the face of such increases, how can we limit material consumption?

First of all, the world runs no risk of exhausting its supply of any major material. That's because the ones common to construction like sand, clay and stone are tremendously plentiful, meaning there's no threat of a shortage. Also, supplies of silicon, the electronic age's signature material are comparably abundant. 

In fact, there isn't a material in existence that could be exploited completely; well before such a state is reached, the costs of extracting a resource from insane depths or sparse deposits would make its mining economically unviable. 

But that's not the only good news — there're also plenty of simple opportunities for waste reduction. For instance, we can cut material use by adopting better designs. The easiest way is by using less material in packaging to reverse the trend of overpackaged consumer items so common in affluent modern societies.

Another way to cut waste is through more rational and less wasteful manufacturing. And of course, there's always the opportunity for more effective recycling. In fact, recycling-friendly design could prove especially important in the management of our increasing piles of e-waste. 

OK, so now that we know our material future is secure, what can we expect from the materials of tomorrow?

Well, there are some promising new materials that will find countless applications in several major industries. Take _graphene_, a recently developed, two-dimensional carbon fabric that's just one atom thick.

This futuristic cloth boasts both formidable mechanical strength and incredibly high electric and thermal conductivity. It could be used in everything from coatings and transparent conductive layers to high-frequency transistors. 

New plastics are also being developed that are 100 percent biodegradable, meaning they can be broken down by microorganisms into nothing but water and carbon dioxide!

### 8. Final summary 

The key message in this book:

**Our modern world consumes a tremendous quantity and variety of materials; the material demands of our economy are increasing everyday. Therefore, it's essential to use materials more efficiently while advancing recycling and waste reduction methods to ensure we can meet the demands of the future.**

**Suggested** **further** **reading:** ** _Energy_** **by Vaclav Smil**

_Energy_ (2006) offers insights into one of the most elusive concepts in the spectrum of human thought: energy. By understanding what energy is, how it has helped us get where we are today, and what dangers our reliance on certain forms of energy poses, we will be better equipped to handle the challenges faced by modern civilization.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Vaclav Smil

Vaclav Smil is an interdisciplinary researcher who has authored over 30 books and nearly 500 papers on energy, environmental and demographic change, food production, technical innovation, risk assessment and public policy. He is currently a Distinguished Professor Emeritus at the University of Manitoba and, in 2010, _Foreign Policy_ named him among the top 50 global thinkers.

© Vaclav Smil: Making the Mordern World copyright 2014, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

© Vaclav Smil: Making the Mordern World copyright 2014, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

