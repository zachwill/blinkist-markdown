---
id: 538f28833761300007d60000
slug: man-the-state-and-war-en
published_date: 2014-06-03T14:18:29.000+00:00
author: Kenneth N. Waltz
title: Man, the State and War
subtitle: A Theoretical Analysis
main_color: BD403E
text_color: BD403E
---

# Man, the State and War

_A Theoretical Analysis_

**Kenneth N. Waltz**

In _Man,_ _the_ _State_ _and_ _War,_ Kenneth Waltz develops a groundbreaking analysis of the nature and causes of war, offering readers a wide overview of the major political theories of war from the perspective of political philosophers, psychologists and anthropologists.

---
### 1. What’s in it for me? Understand the origins of war – and how we might avoid it. 

In 1959, when Waltz compiled his study on war, the Cold War was omnipresent in the public consciousness. Scholars were forced to seriously contemplate the causes of war in an attempt to prevent the nuclear destruction of the planet. 

Although the Cold War era is over and we haven't seen a world war for a long time, there are still wars all over the world with no end in sight. And so the following questions remain relevant: Why do humans kill each other on a massive scale? Are there ways to prevent this from happening?

_Man, the State and War_ tackles this question by examining the ideas expressed by prominent thinkers throughout history, categorizing them into three different types, which are referred to as _images_ : 

  * The nature of human beings (the first image)

  * The internal structure of states (the second image)

  * The anarchic structure of international relations (the third image)

These blinks go into each image to see which one, if any, explains why humans go to war. They conclude that the third image — the anarchic structure of international relations — is the key determiner but that we are also incapable of explaining war without the other two.

In the following blinks, you'll not only discover why that is, but also

  * why our human nature is not the main cause for all evil in the world,

  * why neither liberals nor socialists can offer a peaceful solution and,

  * finally, why no remedy for war has been found up till now.

> _"Asking who won a given war, someone has said, is like asking who won the San Francisco earthquake."_

### 2. First-image thinkers assume that human nature is the root cause of war. 

Everybody has their own ideas about what causes war: economic crises, authoritarian regimes, power-hungry leaders and so forth. For one group of people, namely the _first-image thinkers,_ war is a direct result of human nature. But not all first-image thinkers agree on what human nature _is_, which divides them into optimists and pessimists.

The _optimists_ believe human nature is malleable and improvable — and therefore see education as the cure for war. For them, if we change human nature through education, we will eliminate war.

While optimists of the past placed their faith in religious and moral appeals, modern optimists — the behavioral scientists — place theirs in the study of human behavior. They aim to discover educational methods and forms of social organization that would eliminate aggressive behavior and violence.

For example, in the time of World War I, English psychologist J.T. MacCurdy noted that preventive psychiatry — i.e., measures taken to prevent mental illness — was proving itself effective, and that it was therefore not illogical to hope that similar efforts could ultimately help prevent war. And Margaret Mead, the famous American cultural anthropologist, proposed that studying "primitive" tribes living in peace with each other could help us avoid war ourselves.

In contrast to the optimists, the _pessimists_ view human nature as unchangeable and essentially evil, which is why only external control can prevent humans from starting wars and killing each other.

Take Augustine of Hippo, the Christian theologian and philosopher, who claimed that, in the absence of government, humans would kill each other until the whole species went extinct; or seventeenth-century Dutch philosopher Baruch Spinoza, who affirmed that humans are led by their passions and not by reason, which is why we need to find ways to repress and compensate for our volatile emotions.

In the end, both the pessimists and optimists agree on the cause of war — human nature — though they disagree about the cure. While the optimistic first-image thinkers aim to figure out how we could strengthen the human characteristics that lead to a peaceful social existence, the pessimistic first-image thinkers believe in the control of human nature.

### 3. The optimists’ attempts to explain war are insufficient. 

At first glance, it seems obvious that the source of conflict, violence and war is the selfishness and egoism of human nature.

Yet there must be some good in human nature as it's also the source of peace.

We can see this in the fact that there are alternating periods of war and peace despite the steadiness of human nature. Plus, when we consider the amount of opportunities for evil, and the small amount of crime and wars that actually take place, it's even possible to state that human nature is fundamentally good. 

The main problem with the optimists' view of human nature is that, even if it were possible to change one or even many people, there would still be millions of others to change. And a global, simultaneous re-education of humanity is impossible because education is neither fast nor effective: according to Kurt Lewin, the German-American psychologist, it's easier for "society to change education than for education to change society."

Another problem with the optimists' point of view is that the various paths they propose to create a world order — whether via conquest, religious brotherhood or world federalism — have a common defect: they all state that just one creed, one form of state or one philosophy can dominate the world and thus rid it of war. But since different conceptions of the ideal world have always existed, there will always be disputes about which method should be applied.

On top of these problems, the optimists' fundamental mistake is in seeking the cause of war only in human nature.

Behavioral scientists and pacifists can make contributions to a peaceful world because the development of courage, faith and character are necessary to deal with aggressive impulses — but they underestimate the importance of a political framework in encouraging or dissuading aggressive emotions. 

Still, it's necessary to be aware of human nature because it helps us understand the imperfections in all social and political systems — and how best to deal with them.

> _"Goodness and evil, agreement and disagreement, may or may not lead to war."_

### 4. Second-image thinkers assume that defects in the internal structure of states can explain war. 

Whereas the first-image thinkers believe human nature is the cause of war, _second-image thinkers_ believe it is the internal structure of the state. They believe that if only the right state structure were instituted worldwide, it would guarantee world peace. But, just like the first-image thinkers, not all of them agree, and these thinkers can also be roughly divided into two groups: the liberal and the socialist thinkers.

_Liberal_ thinkers insist on the benefits of a free-trade market, decentralization and freedom from governmental regulation in preventing conflict and fulfilling individual citizens' needs.

This view was epitomized by Adam Smith, the Scottish economist who argued that the impersonal forces of the market — and not governmental regulation — created social harmony and guaranteed public welfare.

Liberal thinkers apply the same argument to international relations, arguing that free trade between states discourages war because, as their interests become intermeshed through trade agreements, they stand to lose more by declaring war than by settling for peace.

_Socialist thinkers_ beg to differ. 

They think the free market inevitably leads to internal state conflicts and external war. According to socialist thinkers Karl Marx and Friedrich Engels, the capitalist system produces a class struggle between the proletariat and the bourgeoisie, with the proletariat fighting for control of the means of production, and the bourgeoisie fighting to defend their control. 

This internal class struggle finds its external manifestation in war because the leaders of capitalist states aim to further their own class's — bourgeoisie — interests, while ignoring the desires of the proletariat to live in peace and harmony. For example, if the bourgeois interests of one state compete with those of another, the bourgeoisie will send the proletariat to war. And to maintain their power, governments use wars as pretexts to raise taxes and heighten control over the proletariat.

Hence the socialist thinkers conclude: with the abolishment of capitalist states, and the triumph of global socialism, war will disappear.

### 5. Liberals disagree about the best way to manage volatile states. 

John Stuart Mill, the famous liberal thinker, ranks among the greatest philosophers of all time. He asserted that liberty is the only infallible source of a better quality of life. Furthermore, he believed that individuals' desire to improve their lives would lead to an improvement of the state as a whole. For these reasons, there shouldn't be any restraints on individuals' behavior — and this should lead to peace.

But in some circumstances, liberal states _do_ go to war.

Which disproves the liberal assumption that people won't go to war because peace is in the interest of all men. That's because individuals' interest in peace is often not represented in the government, and consequently fails to be represented in the state's action. 

Another flaw in the liberal line of thought becomes clear when we ask the question: If an anti-democratic state endangers world peace, should a liberal state intervene? 

After all, wouldn't doing so go against their beliefs?

To deal with this issue, liberal thinkers developed two schools of thought: the _interventionists_ and the _non-interventionists_.

Mazzini, the interventionist and Italian patriot, argued that a state would have to intervene if democracy is in danger because "good" non-interventionism allows for the triumph of evil.

But the non-interventionists argue that it's not possible to attain peace by imposing a state structure through war, because there is no international agreement about what constitutes an ideal state. This means there will always be conflicts between democracies and dictatorships, monarchies and socialist states — and that intervention will do more harm than good. 

As an example, non-interventionists point to Woodrow Wilson, one of the United States's interventionist presidents; he argued that a war is justified if it is a struggle for peace and justice, and not for a new balance of power. But even if there is a good cause for war, who's to say whether that cause is just or not?

### 6. The socialist approach to creating international solidarity failed. 

Karl Marx and his followers had great dreams: they believed that with the victory of worldwide socialism, the nation-states would disappear — and war along with them.

But the Second International, an international organization of socialist parties formed in 1889, failed to achieve international solidarity and preserve peace during World War I.

Why? 

One reason was that each socialist party defended its own interests instead of the interests of the international organization. For example, the largest of all socialist parties, the German party, ended up supporting the granting of war credits — a move that angered socialist parties in other countries.

Another reason was a pre-war peace resolution made by the Second International that allowed the participation of socialists in case of a defensive war — just in case. But every state considered World War I a defensive war, so socialists were mobilized internationally — and often against each other. 

And come 1915, the socialists had to conclude that their belief in worldwide solidarity was a delusion.

What went wrong?

The socialist thinkers fell into a similar trap as the liberal thinkers: they believed that the various parties' _rationality_ would lead them to the best possible outcome and help them overcome their differences. But with World War I in full swing, the French, British and German socialists, facing each others' bullets, had to give up their dreams of international socialism. 

However, we can't disprove that international socialism could cure war simply because international socialism has never been established. Still, past experiences seem to indicate that its success is unlikely.

### 7. Third-image thinkers believe anarchy prevails in inter-state relations. 

According to the _third-image thinkers_, the international scene is characterized by lawless anarchy — like a country without a police force — and therefore it's a hotbed for conflict.

For them, the world's sovereign states are what Hobbes, the English philosopher, called "individuals in the state of nature." This means they aren't controlled by law, a higher institution of power or a common superior — which, according to Hobbes, inevitably leads to violence and conflict.

And while individuals must cooperate to survive, states are independent enough to not have to band together.

This argument is deepened by the Swiss philosopher Rousseau. He argued that states don't cooperate because state conflict involves exactly the same irrational behaviors as interpersonal conflict. Just like among individuals, the main cause of conflict is one state's particular interests interfering with those of another state.

Let's illustrate this with a meaningful example given by Rousseau: 

Imagine five hungry men who agree to hunt a stag together because it's easier in a group and a stag is big enough to satisfy everyone's hunger. On the hunt, one man sees a hare and decides to hunt it alone because it's easier for him — but it will only appease his own hunger, not the others'. He then gives up the initial communal project, jeopardizing the hunt for everybody else.

Another reason third-image thinkers believe anarchy prevails on a global level is because international relations work like a strategic game without written rules. Each state's freedom of choice is limited by the actions of all others. And if everyone's strategy depends on everyone else's, then the Hitlers of the world determine what kind of strategy to pursue: the most aggressive state will determine the actions of the states who aim to preserve peace.

### 8. World government is unattainable in practice. 

Third-image thinkers dream of stopping international anarchy by creating a _world government_ that could maintain order _._ But a world government is only thinkable if every state's main goal is self-preservation. Even if they did agree on this basic point — which they probably wouldn't — a world government would inevitably run into a fundamental problem.

Namely, that it cannot enforce its laws without violent action against the state that broke the international law.

As a man attacked by robbers expects the police to help him out, a state attacked by another state hopes for the intervention of the international community. But it is likely that such violent interventions would lead to a vicious cycle of revenge.

Furthermore, there's no guarantee that the leaders of the world government would always act in the general interest of its member states. We cannot ignore the possibility that the executive of the world government could be driven by personal interests or corrupted by some powerful and influential state.

Therefore, it's impossible to reach a completely effective world government. Nevertheless, it's crucial to build an international political framework with a binding judicial system and create as much order as we can.

> _"Fools learn by experience, Bismarck said once, wise men learn by other people's experience."_

### 9. Final summary 

The key message in this book:

**A** **deep** **understanding** **of** **war** **requires** **a** **multi-level** **analysis** **to** **pinpoint** **the** **different** **causes** **and** **their** **interrelations.** **By** **studying** **the** **great** **thinkers** **of** **the** **past,** **we** **can** **better** **understand** **the** **problems** **of** **the** **present** **–** **and** **help** **avoid** **war.**
---

### Kenneth N. Waltz

Kenneth N. Waltz was an American political scientist. He taught at Harvard and Peking University and the Arnold A. Saltzman Institute of War and Peace Studies at Columbia. He is the author of _Foreign_ _Policy_ _and_ _Democratic_ _Politics_ and _Theory_ _of_ _International_ _Politics._

