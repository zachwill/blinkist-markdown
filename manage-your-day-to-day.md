---
id: 54327f0c3835330008730000
slug: manage-your-day-to-day-en
published_date: 2014-10-07T00:00:00.000+00:00
author: 99U and Jocelyn K. Glei
title: Manage Your Day-To-Day
subtitle: Build Your Routine, Find Your Focus and Sharpen Your Creative Mind
main_color: DD4C4A
text_color: AB3B39
---

# Manage Your Day-To-Day

_Build Your Routine, Find Your Focus and Sharpen Your Creative Mind_

**99U and Jocelyn K. Glei**

_Manage Your Day-To-Day_ is a collection of ideas, wisdom and tips from well-known creative people. It offers readers valuable insights on how to develop effective work routines, stay focused and unleash their creativity.

---
### 1. What’s in it for me? Learn the most effective tricks to manage your daily life. 

_Manage Your Day-To-Day_ is a compilation of the insights gained from 20 creative minds across various fields. In reading about these famous creatives, you will learn the tricks that they personally use to manage their time, create effective work routines and nurture their creativity.

In addition, you'll see just how easy it is to adapt these principles and techniques to your daily life. Even the simplest tweaks can make your work all the more effective, help you to be more conscientious of how you use technology and ensure that creative blocks won't set you back.

After reading these blinks, you'll know

  * how flossing your teeth can actually make you more productive;

  * why you should never respond to any emails before lunchtime; and

  * why cultivating a garden can lead to your next creative breakthrough.

### 2. Develop a daily routine that matches your body’s rhythm. 

If you're like most people, then the first thing you do when you get to the office is check your email. While this might _seem_ like a good idea, starting your day with this simple task can actually reduce your overall productivity.

Our bodies follow what's called a _circadian rhythm_, whereby our energy levels wax and wane throughout a 24-hour period. These rhythms cause us to be more effective in the morning and we should therefore reserve that time for important or difficult tasks.

When we get to work and see an inbox full of unread messages, it's tempting to try to immediately resolve the stress of knowing that someone is waiting for our response by answering those emails. But answering emails is a simple task that doesn't require much thought or creativity.

What's more, responding to emails or phone calls in the morning causes you to use your most effective time working on _other people's_ priorities.

And when you finally get around to working on your own projects, you'll find yourself fatigued and less efficient. That's why creative coach Mark McGuinness suggests that you devise a work routine that allows you time to start your day with your own projects: make it a rule not to respond to any emails before your _own_ creative work is done.

In addition, you should make sure to take breaks and get enough sleep in order to maximize the body's available energy. How much sleep is enough? While it's hard to say, consider that only 2.5 percent of the population feels comfortable getting less than seven hours of sleep.

So, if you're like most, you should devise a routine that allows for at least seven hours of sleep every night, rather than relying on an extra cup of coffee.

And don't forget to also include breaks in your new work routine: studies have shown that we can maintain peak function for only about 90 minutes at a time. So make sure to pause and relax regularly so that you can recharge.

> _"A truly effective routine is always personal — a snug fit with your own talent and inclinations." — Mark McGuinness_

### 3. Working frequently is the key to an effective work routine. 

Doing creative work when you aren't feeling creative can be difficult. Unfortunately, we don't always have the luxury of waiting until we're in the right mood. But rather than relying on your mood, you can build a routine that facilitates creativity, no matter how you feel that day.

Essentially, if you work regularly, then you will get regular inspiration. When working on creative projects, many people feel that they can't work every day because they don't have enough inspiration. But this simply isn't true!

In fact, as outlined by author Gretchen Rubin, working every day on creative projects — even if it's only for half an hour — can help you develop many fresh ideas. By making it a habit to work every day, you bring the project into your daily life, and allow yourself to find inspiration even in completely unrelated situations.

Indeed, ideas can pop up when you least expect it. For example, you might finally find a great color scheme for your project, but only after your friend showed you her brand new neon-green sneakers. Because the project was always on your mind, you found inspiration in an unexpected place.

What's more, working regularly will greatly reduce the pressure that comes with creative work. Of course, there will always be days when you feel like you can't get anything done. However, working every day prevents the worry.

For example, imagine you're a journalist and have a deadline to finish your piece. If you start early enough and then continue to work every day, then you will have plenty of time to try out different approaches in order to find the best angle.

Even when the inevitable hurdles come up, like when a source takes too long to respond or an editor rejects your draft, you'll be less stressed, because you'll know you have enough time still to complete it.

But even working every day can't help you too much if you are too distracted to get anything done! Our following blinks will show you how to focus in spite of the inevitable distractions.

> _"Creativity arises from a constant churn of ideas." — Gretchen Rubin_

### 4. Practice focusing your attention regularly and ignore negative distractions. 

Admit it: like most people, you struggle with focusing on boring or unpleasant tasks. Unfortunately, we can't ignore those tasks simply because we don't like them. Luckily, author, lecturer and productivity consultant Erin Rooney Doland has two great tips to help us work through those trudging tasks.

First, find ways to strengthen your will power at work by training it in other areas of your life.

If you work in a chaotic environment, like a crowded office, then you know how distracting it can be: it's so tempting to take a quick break to chat with a colleague or focus your attention on things that aren't work-related, like a Facebook message from a friend.

But if you want to get anything done, you'll need to exert control over yourself and maintain focus. If you find it difficult to do this at work, then you can practice exerting control in other situations. If you make it a habit, for instance, to floss your teeth every night, you will not only delight your dentist but also integrate a habit of self-control into your life, even when the task is boring and time-consuming.

Put simply, adopting even small routines such as regular flossing can help you avoid the distractions of the workplace.

Second, you can replace any non-work related _negative distractions_ with _positive_ ones and then reward yourself when you're successful in doing so.

Consider this study, in which children sitting alone in a room with a tasty marshmallow were given two options: eat the sweet treat right now or wait a few minutes and get two. The children's reactions were then observed as they struggled to resist the temptation.

Those who were able to wait had replaced a negative distraction, the delicious marshmallows, with positive distractions, such as singing. In other words, they focused their attention on something other than the marshmallow.

You too can use this principle to overcome negative distractions, allowing Facebook messages and water cooler chats to be a reward for hard work.

### 5. For maximum efficiency, stick to one task at a time. 

If you've ever tried getting serious work done while watching your favorite TV show, then you must have realized how inefficient that style of work is. But why is that?

In essence, we're just not very good at multitasking. Despite this, according to psychologist Dr. Christian Jarrett, most of us believe that we're quite adept at doing two things at once. However, we are only able to do so successfully when one of the tasks is simple and automatic, such as walking.

When both tasks require our attention, then we switch our attention from one to the other, which is both time consuming and ineffective. For example, while we can walk and talk simultaneously, we can't talk and read.

This was verified in a study that showed that, when students tried to read a book while texting with a friend, it took them about 25 percent more time to read one page than students who focused completely on the reading task.

So, the next time you want to try and juggle more than one task at a time, keep in mind that it won't actually make you more productive. Quite the contrary: your work will be even less effective. To avoid this dilemma, make sure to set aside time in your day that is completely free of distractions so that you can effectively complete your work.

But even if you are absolutely determined to focus only on a single task, surely other people will want you to do something. So what can you do about that?

Professor Cal Newport has a great answer: set up a _focus-meeting_. If you need two hours to complete a task, mark it on your calendar just like you would any other meeting or appointment, and treat it the same way. This way both you and especially your colleagues know that you are unavailable during that time and you can focus solely on the project at hand.

Nevertheless, the more technology becomes a part of our work, the more distracted we find ourselves! Our following blinks detail the ways in which we can manage the technology that we both need and need to control.

### 6. Be aware of how and why you are using technology and social media. 

Think about the last time you checked your phone: do you know the exact reason why you did it? Most people don't — they absentmindedly pick up their smartphones, surf the web and check Facebook for no particular reason, and this is especially true for social media.

One good way to prevent yourself from becoming absorbed in technology is to consciously think about why you're using social media every time you use it.

We tend to habitually log onto social media whenever we're bored, looking for a distraction. The problem with this is that we never know when it's time to stop and move on to something else.

To combat this, Lori Deschene, founder of the blog tinybuddha.com, suggests that logging onto social media should always be _intentional_. You don't have to ban social media entirely! Instead, you should use it meaningfully, by being fully aware of your intentions.

Sure, you can log on to share the location of the restaurant where you've just had some amazing pizza, but just make sure to log out afterwards. This way you can continue with your real life instead of getting absorbed with what's happening on Facebook.

Parallel to this, you should consciously think about _how_ you're on the computer, including your posture!

For many of us, going to work entails sitting in front of a computer screen all day long. But as former senior high technology executive Linda Stone observed from an alarming study, our behavior in front of the computer or smartphone screen can be highly detrimental to our health.

For instance, when gazing into a computer screen, we unconsciously hold our breath or breathe in a shallow, unnatural way, which in turn contributes to stress-related diseases.

One way that we can combat this, and be more mindful of how we use technology in general, is to practice yoga and other conscious breathing techniques.

> _"The power of any tools lies in the intentions of its user." — Lori Deschene_

### 7. Technology is supposed to assist you – don’t let it control your life. 

Smartphones, laptops, etc., were invented to make our lives easier and more pleasant, yet, it sometimes feels like we live in a science fiction movie, totally at the mercy the technologies that we ourselves create!

While your smartphone doesn't control your body, it nevertheless controls your behavior, since you just can't live without it! You must remember that you don't _need_ your smartphone in every part of your life.

Smartphones are so conveniently sized that it's easy to take them with us everywhere — all you need is a pocket to keep yourself connected at all times. Author and filmmaker James Victore rightly asks whether this is really necessary.

Consider, for example, how taking a phone call at dinner used to be a serious social faux pas. Today, however, it's a completely normal occurrence. But are we okay with trading our good manners for excessive smartphone use? Just do yourself a favor and put your phone on airplane mode the next time you're out on a date.

The thing is, we can't resist using technology if it's available. If you want to be more effective, you'll have to turn it off!

Having a smartphone or a computer screen right next to us when we're trying to focus is like sitting next to a plate of delicious cookies when we're on a diet. In other words, it's hard to resist.

Unfortunately, this also means that people expect us to be available at all hours of the day, no matter what. This expectation is such a problem that Professor of Psychology Dan Ariely even suggests that the IT team at your company delays the delivery of email until a certain time of the day, as to prevent you from being constantly bombarded.

While a company-wide process such as Ariely's suggestion may not happen, you could just turn off all your devices or even just go to another room when you really need to focus.

So far, we've mostly focused on preventing distractions so that you can maximize your available creativity. These final blinks deal with boosting your creative resources.

### 8. Creative blocks happen to everybody, but there’s a way out. 

What do you usually do when you have a bit of spare time? Likely, you don't spend it sitting around doing nothing, right?

But you ought to! Having time in which you "do nothing" is essential for becoming and staying creative — especially if you're stuck with a creative block.

Whenever you're stuck on something, being alone and letting your mind wander can bring back your creativity. This feels quite counterintuitive, but it's true: if you think about nothing in particular, you will necessarily become more aware of your thoughts and surroundings, which then provide a great source for creativity.

Scott Belsky, the Vice President of Adobe, suggests that you set aside a certain time in your day to simply let your mind wander without focusing on anything in particular. If this feels difficult to you, then start by thinking about what you've learned and what your immediate plans are and then let your mind wander from there.

In addition, striving for perfection can also lead to creative blocks. Indeed, some of us are never happy with our accomplishments unless they embody perfection — but honestly, this rarely ever happens.

We should all take the advice of author Elizabeth Grace Saunders: stop being a perfectionist and let creativity be your guide!

If you're a perfectionist, then you surely know how difficult it is just to start a project, waiting impatiently until that ideal moment when you'll get the most amazing ideas. But if that moment doesn't arrive, you'll have to start anyway, and then you'll feel uncomfortable and even less creative.

A far better approach is to start your projects immediately, even if you don't feel like it. This way, you'll have something to start with and improve upon later. Even if you don't feel creative right at the beginning, you'll soon see your creativity kick in.

> _"My guess is that you'll find you produce far more and far better work with much less stress by aiming for the less-than-perfect." — Elizabeth Grace Saunders_

### 9. Relaxation and a new hobby can boost your creativity. 

Surely you know how difficult it can be when you have to come up with creative ideas under pressure. As we've seen before, creativity isn't something that you can just will into motion. Luckily, there are things you can do outside of work to boost your creativity:

For example, if you take time to exercise and get a good rest, you'll find that you are more creative afterwards. As we've already learned, rest and sleep are important ingredients for a productive lifestyle. By the same token, they are also essential for generating creative ideas.

In fact, researchers at Harvard University found that people who got adequate sleep were 33 percent more likely to come up with connections between different ideas — in other words, they were more creative.

In addition to proper rest, bike rides or other kinds of exercise were shown to boost brain activity and therefore creativity as well. Business coach Scott McDowell even advises his clients to take a nap or a brisk walk in order to get those creative juices flowing.

In addition, feel free to flex your creative muscles in seemingly frivolous ways — you'll often find that these creations can be the inspiration for amazing creative work.

For example, when author Julia Cameron starts a new book, she writes a few pages of complete gibberish — just to get in the rhythm of writing. She might even find some inspiration hidden in the nonsense!

Hobbies, too, are a good way to find unexpected inspiration. The founder of _Accidental Creative_ Todd Henry suggests that people garden in their spare time and be experimental with it.

Having the freedom to creatively cultivate your garden lets you create and experiment in an environment without pressure. Not only that, but hobbies like this will ensure that you won't get bogged down by the daily grind and might even provide you with fresh ideas that you could use at work.

### 10. Final summary 

The key message in this book:

**By paying attention to our natural rhythms, we can maximize our energy on our priorities and rest, exercise or do other hobbies when it's in tune with our rhythm. Sometimes, despite what we feel like doing, we just need to focus to complete certain tasks we don't feel like doing.**

**Suggested** **further** **reading:** ** _Getting Things Done_** **by David Allen**

_Getting Things Done_ introduces a world-famous productivity system aimed at helping people work on multiple projects at once — and to do so with confidence, clear objectives and a sense of control. The method has been specially designed to make it easier for you to work effectively and enjoy life in the meantime.
---

### 99U and Jocelyn K. Glei

Jocelyn K. Glei is editor-in-chief and director of _99U_, an online magazine dedicated to providing people with innovative tips on how to bring their ideas to life. In addition, she was also the editor of the follow-up to this book, _Maximize Your Potential_.

