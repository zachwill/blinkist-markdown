---
id: 585912e797bebb0004627192
slug: managing-online-reputation-en
published_date: 2017-01-06T00:00:00.000+00:00
author: Charlie Pownall
title: Managing Online Reputation
subtitle: How To Protect Your Company On Social Media
main_color: 2A6F8C
text_color: 2A6F8C
---

# Managing Online Reputation

_How To Protect Your Company On Social Media_

**Charlie Pownall**

_Managing Online Reputation_ (2015) offers insight into how companies and CEOs who don't know how to manage social media and the internet can damage their reputations. Find out how one small incident with an unhappy customer can spiral into a tornado of negative posts and angry tweets. More importantly, find out how you can prevent this from happening to you.

---
### 1. What’s in it for me? Understand how to protect your reputation online. 

In the olden days, if you wanted to damage a company or an organization's reputation you didn't have that many options and reaching out with your message could be difficult. After all, there's a limit to the negative impact of a defaced billboard. But today, as the internet and social media have become increasingly important for companies and organizations, that has changed.

These channels of information make it possible to advertise and campaign in a way that was simply unimaginable just ten years ago. That said, there's always the risk that someone will use these same channels against you, which can have disastrous consequences if you don't know how to handle it. So let's dig in and learn about how to manage your online reputation.

In these blinks, you'll find out

  * what we can learn from one of Qantas Twitter campaigns;

  * how Applebee's giving into a local pastor had unexpected consequences; and

  * what we can learn from how Malaysian Airlines handled their missing plane.

### 2. Online promotional campaigns can backfire and lead to a lot of negative publicity for companies. 

If you saw the TV show _Mad Men_, you have a good idea of how lucrative and glamorous the advertising industry was in the 1950s. Today, however, this profession faces a slew of utterly different challenges.

Nowadays, many companies rely on online advertising campaigns, which are by nature especially risky. Often, they backfire.

A successful social media campaign will definitely boost a company's profile; however, all social-media campaigns are also an open invitation for people to post any number of embarrassing comments.

In November 2011, the Australian airline Qantas launched a campaign on Twitter called #QantasLuxury. The idea was to advertise the advantages of its first class services.

Qantas asked customers to use the hashtag and describe their idea of an ideal luxury flight; the person with the best response would receive a free first-class flight.

However, within a matter of hours, the company was forced to deal with an avalanche of unhappy comments from customers who decided to use the opportunity to vent about the many issues they had with the airline.

For instance, one user dreamed of being under five-feet tall so as to be able to comfortably stretch out on a Qantas skybed. Others wrote about how luxurious it would be if Qantas flights actually took off and landed on time.

Qantas also learned that it's impossible to control a story once it has been picked up by internet media. Before long, the airline's old issues, as well as ongoing problems, were resurfacing.

One especially touchy subject was the airline's safety record: A year earlier, in 2010, an A380 aircraft had caught fire outside Singapore. Though no passengers were injured, it was an incident many users were all too happy to bring back up.

All of this quickly caught the attention of the mainstream press outlets, like the BBC and the _Sydney Morning Herald_, who regularly patrol places like Twitter in search of tasty tidbits just like this one. They posted it to their own Facebook and Twitter pages, attracting even more attention and putting it completely out of Qantas's control.

> _"Four years after it went astray, #QantasLuxury continues to be talked about online and the hashtag is till used to bash the airline."_

### 3. Social media is a useful way to attract customers, but it opens the door for attacks from competition. 

There's a good chance that you've clicked on a well-placed ad on Twitter or Facebook to find out more about a new product or special offer.

And since it costs relatively little to launch a social-media campaign, more and more businesses, both big and small — as well as groups operating outside the mainstream — have started using them to spread the word about their products and projects.

One of the most extreme examples is the terrorist organization ISIS. What was once a small fringe group with few resources has used social media to spread their message and grow considerably.

ISIS controls an app called "Dawn of Glad Tidings". It keeps users up to date on ISIS developments and uses their names and social media contacts to further spread their message.

ISIS is also well coordinated. Thousands of their supporters post and repost specific messages at just the right times so that these messages will show up on the list of various platforms' trending topics.

In 2014, the Brookings Institute determined that the online presence of ISIS included around 70,000 Twitter accounts that were actively supporting the organization. This shows that, even with limited resources, a social-media campaign can be a powerful way for an organization to get their message out.

Another effect of social media is that it's now much easier for competitors to attack one another through the use of anonymous smear campaigns.

In 2013, one anonymous user posted an online video that appeared to reveal a Hong Kong manufacturer using moldy jelly to make a herbal medicine for the company Hoi Tin Tong.

Though the company was quick to blame the leaked footage on a competitor — a former supplier and advertising executive — the damage was already done: The _South China Morning Post_ had already picked up the story, the video was trending and Hoi Tin Tong was swiftly losing clients.

### 4. The internet provides an opportunity for individuals and small companies to become highly influential. 

Though online media has its risks, it also has undeniable advantages. After all, it wasn't so long ago that the average entrepreneur's best option was to run an ad in the local paper. Today, anyone can reach the masses at little to no cost.

The internet has also given activists more power than they've ever had before.

Non-Governmental Organizations (NGOs) are interest groups whose ability to raise funds used to be limited. But, thanks to the internet, they've experienced strong growth in recent years.

In 2009, there were around 1.5 million NGOs in the United States, and 2 million in India. Many of these organizations limit their work to a specific region or country, but bigger ones like Greenpeace, Save the Children and Oxfam have global influence that rivals that of national governments.

Oxfam started an internet campaign to raise awareness about the discriminatory treatment and poor working conditions of women in the cacao industry of developing nations. These women weren't only receiving less pay; they were often forced to work without any contract at all. This changed in 2013, when Oxfam received the support of 100,000 people and managed to get the candy companies Nestlé and Mars to provide equal pay for female employees.

The internet also enables and enforces the political activities of individuals.

One example is graduate student Molly Katchpole, who was not happy with how Bank of America was treating its customers.

In September, 2011, Bank of America, despite having just received a $45-billion bailout from the government, announced plans to start charging customers with debit cards a new five-dollar-per-month fee.

This didn't sit well with Katchpole. So she used the online political petition website Change.org to launch a campaign against the bank. And after gaining 300,000 signatures on her petition, the campaign also convinced 21,000 customers to close their accounts, all of which persuaded the bank to cancel their plans for the excessive fee.

### 5. When dealing with an apparent crisis, make sure you know the facts before you respond. 

Customer service used to be a one-on-one affair. Unsatisfied customers would call in and talk to someone directly, which made it hard to ignore their complaints. These days, things have changed. With the advent of the internet, it became much easier to deflect, or totally ignore, customer dissatisfaction.

But it's in a company's best interest to know how to properly deal with an irate customer, and it's also important to make sure you react quickly to emerging crises. But not so quickly that you fail to get all the facts straight.

In December, 2011, a YouTube video emerged that featured a FedEx employee delivering a customer's package by throwing it over a fence. Worse still, the box contained a computer!

This was a crisis for FedEx's PR manager Shea Leordeanu. She knew the video was about to go viral; she had to act fast. But she took things one step at a time. The first step was to make sure the video was real and to identify the customer.

The video didn't provide many details, but there were a few clues: the employee's outfit, the sunny weather — things that proved it happened somewhere in the southwest United States. Finally, more than 24 hours later, they had their customer and proof that the video was legit.

With that important confirmation in place, Leordeanu immediately got on Twitter, where she apologized on behalf of the company, and offered to meet with the customer and provide a new PC. She also announced that the employee was disciplined appropriately.

FedEx's reputation took a hit from the ordeal. But after the apology on Twitter came out, the story faded. It was the best possible way the company could have dealt with it.

### 6. Employees can be just as troublesome as customers, but they also need to be dealt with in a sensitive manner. 

Back in the day, nearly all businesses stuck to a simple rule: The Customer is King. Disputes between customers and employees would almost always end in the customer's favor. But times have changed. The internet hasn't led to downright regicide — but it's certainly empowered employees.

Yes, overly assertive employees can cause their companies a lot of trouble. Nonetheless, giving in to customer pressure isn't always the best response.

In January, 2015, Pastor Alois Bell went to an Applebee's restaurant with a member of her congregation in St. Louis. The meal went fine, but when it came time to pay the bill, Bell crossed out the section that suggested an 18 percent tip and wrote a message: She gives God ten percent of her money, why should Applebee's get 18 percent?

The receipt was quickly posted to Applebee's Facebook page by one of the waiters, who also added a status where he said he hoped Jesus would pay for his rent and food.

A blog called _The Consumerist_ noticed the story just hours later and helped it go viral. In the process, some people criticized the waiter for posting a receipt, feeling it was a violation of the pastor's privacy, while others agreed that the pastor's behavior was hypocritical.

Applebee's didn't need this kind of attention, and they quickly apologized, both to the pastor and for the Facebook post. They had to clarify that company policy dictates that all guest information, including receipts, is personal and should never be shared.

The waiter who had posted the receipt was fired.

But this angered people, too. Many sympathized with the waiter and felt that firing him was inordinately harsh.

Applebee's error was that they only took into account the customer's concerns. If they'd paid closer attention to the public's reaction, they could have kept the story from getting out of hand. And they would have known that firing the employee was not going to sit well with the public.

### 7. When dealing with journalists or the press, be careful and diplomatic. 

There's an uneasy relationship between companies and the press. Businesses need journalists to promote and review their products, but the results often aren't what the businesses had hoped for.

This puts journalists in a powerful position. One bad article can seriously damage an entire company, especially if the reviewer doesn't take the company or the product seriously.

In January 2013, Tesla, the electric-car company, set up an important test drive for their Model S car, and invited John M. Broder, a respected journalist from the _New York Times_, to be the driver.

Broder's article came out a few weeks later, in the Sunday edition of the _Times_. He started his review by praising the car and its technology, but then went on to write that the cold weather had caused the battery to cut out, leaving him stranded on an exit ramp near Branford, Connecticut.

Tesla's CEO, Elon Musk, immediately took to Twitter and placed the blame on Broder. He'd improperly charged the battery, Musk wrote, and he'd taken the car on long detours. Broder then jumped in to defend himself. Thus began a lengthy series of attacks and counterattacks, with neither man willing to admit a mistake.

Musk, however, appeared to have evidence to back up his side of things. The vehicle logs indicated that Broder had indeed strayed from the agreed route and also showed that he failed to fully charge the vehicle.

But Musk had already made his mistake. He'd aggressively attacked an esteemed journalist from one of the world's biggest newspapers. Broder felt that he'd been put on the spot; this made him defensive and determined to retaliate against Musk and his company. In the end, _both_ sides ended up with damaged reputations.

Musk would have done well to avoid making personal accusations by sticking to the facts and keeping the tone constructive. Even better, he could have kept things out of the public eye and spoken directly to Broder to find out exactly what went wrong during the test drive. Who knows, maybe a follow-up article would have been published.

> _"A public attack on an organ like the NY Times should be your last line of defense, not your first."_

### 8. When facing a crisis, a fast response and a sincere apology are key to defusing the situation. 

If a friend betrays your trust, it may take years to repair the relationship. The same is true of relationships between customer and company. Trust is crucial, and any breach could drive customers away forever.

If you want customers to remain loyal after a crisis, you need to respond quickly and appropriately.

A failure to respond may well be your company's downfall.

On March 8, 2014, at around 2:40 a.m., Malaysia Airlines faced a major crisis: the flight MH370 disappeared.

Immediately following the disappearance, Twitter was flooded with users posting about the event, with the story at the top of the trending list as _#PrayforMH370_.

But the company was silent. For hours, Malaysia Airlines failed to communicate with the actively vocal and concerned public. And then, finally, they sent out their own press release at 7:24 a.m.

What hurt them even more was the delayed and insufficient social media response. After another hour had passed, at 8:13 a.m., the airline posted a link to the earlier press statement; there was no apology and no statement of concern for the passengers or their relatives.

In this kind of situation, a simple and sincere apology goes a long way toward resolving a crisis. And for the apology to appear sincere, it should be released as soon as possible, and posted on the social media platforms where users initially expressed concern.

Malaysia Airlines failed to do these things. And their apology for the tragedy didn't arrive until more than four weeks later.

In a completely different situation, KitchenAid executive Cynthia Soledad posted a shameful tweet in October, 2012, writing that Barack Obama's grandmother died four days before the election because she (his grandmother) knew that Obama would be a bad president.

However, she de-escalated the situation by immediately deleting the tweet and issuing a personal apology to President Obama in which she took full responsibility for her actions.

Though Malaysian Airlines faced a much more difficult and confusing situation, there's no excuse for not benefiting the company and the public by immediately apologizing and, if necessary, shouldering responsibility.

### 9. Final summary 

The key message in this book:

**Even a relatively small and embarrassing incident can become a huge crisis if it gets turned into a popular social-media story. Don't let this happen to your company. By employing effective and sincere communication, any company can prevent a PR disaster from destroying years of hard work.**

Actionable advice:

**Create a corporate blog.**

When faced with an embarrassing story on social media, it will be extremely useful for your company to have its own blog. That way, you can give a detailed account of events, supporting and illustrating your case with numbers or video footage. This provides room for the information you need to convey but can't fit into a 140-character tweet.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Reputation Economy_** **by Michael Fertik and David C. Thompson**

_The Reputation Economy_ describes how to optimize your digital footprint for a world where reputation has increasingly become the most valuable asset. The authors identify emerging trends and describe how you can improve your professional and financial prospects to succeed in this new world.
---

### Charlie Pownall

Charlie Pownall is a public-relations coach who specializes in social media. He provides expert advice to government and corporate agencies on how to manage these public channels and avoid reputational damage. He is also a frequent contributor to _Public Affairs Asia_, _Social Media Today_ and _Public Affairs._

