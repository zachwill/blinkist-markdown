---
id: 548629a86331620009100100
slug: mans-search-for-meaning-en
published_date: 2014-12-12T00:00:00.000+00:00
author: Viktor Frankl
title: Man's Search for Meaning
subtitle: None
main_color: 3D6999
text_color: 3D6999
---

# Man's Search for Meaning

_None_

**Viktor Frankl**

Originally published in 1946, _Man's Search for Meaning_ details the harrowing experiences of author and psychologist Viktor Frankl during his internment in Auschwitz concentration camp during the Second World War. It offers insights into how human beings can survive unsurvivable situations, come to terms with trauma, and ultimately find meaning.

---
### 1. What’s in it for me? Discover how one man’s terrible experiences helped him formulate a unique view of human nature. 

It is simply impossible for anyone other than survivors to know what life was like for a prisoner in a Nazi concentration camp. We can only imagine how people got through each day, and how they managed to stay sane when surrounded by atrocities.

Viktor Frankl, himself a survivor of the camps, helps explain how prisoners of the Nazi regime struggled through. These experiences also provided Frankl with evidence for his psychological theory, _logotherapy,_ which explains how, in order to thrive — and, in more dire circumstances, survive — we need to discover our personal meaning of life.

These blinks explain both Frankl's findings from the camps and his development of logotherapy.

In these blinks you'll discover

  * how to find meaning in your life;

  * how the concentration camps sucked the hope out of prisoners; and

  * how some people can find humor in even the worst situations.

### 2. Prisoners’ first reaction to the concentration camps was shock – first in the form of hope, then despair. 

Today, everyone has at least some awareness of the horrible, inhumane acts that were carried out in the concentration camps across Germany and Eastern Europe under the Nazi regime.

Likewise, the targets of Nazi violence during the Holocaust had at least some inkling of the terrible fate that awaited them. Because of this, you'd think that the initial reaction upon entering the camps would have been fear. Reactions, however, were split into three distinct phases.

The first phase began upon arrival at the camp — or even as inmates were being transported.

Prisoners were so shocked at what was happening that they desperately tried to convince themselves that, somehow, everything would be alright. Most prisoners had heard horrific stories about what happened at the camps, yet when they themselves were sent there, they told themselves that things would be different for them.

Those who arrived at the death camp Auschwitz, for example, were sent to the left or right as they exited the train — one group for hard labor and one for immediate execution. However, none of them knew what these groups meant.

Due to the shock of arriving at the camp, the prisoners succumbed to the _delusion of reprieve_, falsely believing that the line _they_ were in would somehow mean an escape from certain doom.

During this first phase, the prisoners who hadn't yet become accustomed to the horrors of the camp were terribly frightened by everything that went on. Newly arrived prisoners couldn't manage the intensely emotional experience of watching other prisoners being punished in the most brutal ways for the most trivial offenses.

Confronted with grotesque brutality, they soon lost their hope and began to see death as some kind of relief. Most, in fact, considered suicide as a way out — perhaps by grabbing the electrical fence around the camp.

### 3. After a few days in the camp, prisoners fell into a state of apathy, which allowed them to concentrate on survival. 

Following their initial shock, prisoners soon became "used to" the horror and death that surrounded them, thus becoming emotionally dull.

Instead, all their thoughts and emotions were focused on survival. Rather than muse about feelings like love or desire, for example, prisoners mostly talked and even dreamed about food or any other kind of vital, life-sustaining satisfactions that we normally take for granted, but which were severely limited in the camps.

While prisoners hid from the horror in the first phase, the dull emotions of the second phase acted as a shield, giving them the constitution to both live through the everyday cruelties of the camps and grab any opportunity to improve their own chance of survival.

For example, after several people died during a typhus outbreak in one of the camps, prisoners in the second phase no longer felt disgust or pity as they gazed at the corpses. Instead, they saw an opportunity to grab leftover food, shoes or other clothing items from the now deceased prisoner.

There was no foreseeable end to their time in the camp other than at the hands of the guards, which left prisoners unable to imagine that life still had any meaning.

Usually, we live for the future: we make big plans and get excited about seeing our life unfold. Prisoners in the camps, however, had a completely different view. For them, there was no excitement for the future. There wasn't even a future — nobody knew when (or if) their prison term would come to an end.

Most prisoners thought their lives were already over. They merely "existed" in the camp — they gave up "living" as there were no goals to reach.

### 4. Life after liberation from the camps was often characterized first by a feeling of disbelief, and then by bitterness. 

The prisoners who were lucky enough to survive the concentration camps had to face a new challenge upon their release. Most had spent such a long time in the camps that living a normal life became very difficult.

Immediately after their release, the prisoners were unable to grasp their freedom. Accustomed to a state of emotional apathy, they couldn't immediately change their perspective. At first, prisoners couldn't experience pleasure or joy.

Having dreamed so often of liberation, they found it unreal when it finally came.

After being liberated many prisoners felt as though, after all of the brutality that had been inflicted upon them, it was their turn to inflict harm on others. Having been made to suffer such inhumanity, it made complete sense to them to look for some sort of compensation, for instance, by taking vengeance against the guards in the camps.

What's more, liberated prisoners didn't always receive the warm welcome they imagined they would when they returned home. Unfortunately, many prisoners came home only to find that their family had been killed and their towns turned to rubble.

But their bitterness wasn't just about lost family and friends. They hoped for compassion, expecting that their suffering would be understood. All too often, however, the people they talked to after release — those who had never seen a concentration camp — would only shrug and tell that they too had suffered, for example, from rationing and bombing.

While returning to a normal life certainly wasn't easy for the liberated prisoners, after a while most of them managed to enjoy their lives once more and be happy that they'd survived the Holocaust.

### 5. Prisoners concentrated on their “inner” lives to distract themselves from what was happening in the real world. 

So far, we've seen how prisoners suffered inside the camp. But how was it possible to protect their sanity and survive the horrors? In essence, it all came down to where they placed their focus.

For some, imagining their loved ones and reminiscing about the past made it possible to mentally escape the terror and brutality of their environment. In fact, those who were able to find at least a bit of happiness in their memories were often better able to survive than others.

In the brutal reality of the camps they had no relief, as they were forced to do hard labor in the cold with little more than rags on their backs. Love, however, could bring them fulfillment. A nice conversation with their loved ones — even if only in their imagination — was something the camp guards couldn't take away from them.

Even the smallest slivers of memory were able to bring relief — mundane things like switching on the lights in their own bedrooms back home.

A few of the prisoners found solace by immersing themselves in nature and humor. An idyllic sunset or a cute bird could offer the inmates a fragment of happiness, even if it was only fleeting.

Prisoners managed small gatherings during their half-hour lunchbreak, during which they tried to distract themselves from their reality, for example, by songs or other small performances.

There were even rare moments when prisoners found their sense of humor.

This humor often involved imagining the future — after being released — and joking about how their camp routines might affect later situations. For example, sitting at the family dinner table, they might forget where they were and ask for soup from the bottom of the bowl, where the few nutritious peas would be found in the camp cook pots.

> _"A man who let himself decline because he could not see any future goal found himself occupied with retrospective thoughts."_

### 6. Most prisoners accepted their fate, but some tried to make decisions whenever they could. 

The freedom to choose, whether it's picking out our outfits, our lunches or the charities to which we donate, is something we all take for granted. Of course, in the camps, nothing could be taken for granted. The ability to decide for oneself took on a completely new meaning.

Most decisions were a matter of life or death, and many prisoners were afraid to make them.

Sometimes, for example, prisoners were ordered to go to another camp. However, the prisoners were kept in the dark about the true destination and the meaning of the transfer. The guards sometimes referred to these as "rest camps," but no one could be certain that they weren't being led to the gas chambers.

So, once prisoners realized that they would be sent elsewhere, some would become desperate to change that decision. This was sometimes possible if they worked harder for their captors, e.g., by volunteering for extra shifts.

Yet there was also the possibility that their new camp would actually bring them relief. There was simply no way for them to know what decision would be best, and thus many prisoners decided that they should not intervene in their fate.

There were other prisoners, however, who were determined to maintain even the tiniest freedoms, and therefore grabbed any opportunity to make decisions.

Despite their miserable conditions, these prisoners tried — as far as was possible — to live in accordance with their own values.

Their spiritual life, for example, was something that couldn't be taken away from them. Although they might have to abandon their rituals, they could still decide to live up to high moral standards.

For instance, some prisoners would give bread to those who were in greater need, even though they were hungry too.

> _"It is this spiritual freedom — which cannot be taken away — that makes life meaningful and purposeful."_

### 7. According to logotherapy, our motivation to act stems from our life’s meaning. 

The author witnessed many terrible scenes in the camps. During that time, he realized again and again that people need meaning in their lives in order to have something to look forward to.

Indeed, the prisoners who could maintain this meaning were stronger and more resilient than those who had lost it.

This observation helped confirm many ideas from his own theory of psychotherapy, _logotherapy_, which posits that our search for meaning is the greatest motivation in our lives.

There is plenty of research that supports this idea. For example, in a study from Johns Hopkins University, students were asked what they considered to be central in their lives. The vast majority — 78 percent — reported that finding a purpose and meaning in life was most important to them.

When we're unable to find meaning in our lives, we're left with what is referred to as an _existential vacuum_. People who are unable to live according to their values, or feel like their lives have no meaning, will find a kind of emptiness inside themselves.

You don't have to undergo serious trauma to experience the existential vacuum. Just take the widespread "Sunday neurosis," for example, which occurs when people start to relax after a structured week of hard work, only to realize that their lives are totally devoid of substance.

Logotherapy aims to help people find meaning, and thus prevent the negative consequences that could result from a persisting existential vacuum.

### 8. There is no general meaning of life; everyone’s life has it's own specific meaning in a given moment. 

Knowing how important it is to find a purpose in life, we're left asking ourselves how we go about finding our own. Indeed, many people believe that, in order to make the right choices in life, they must first discover their life's purpose.

Logotherapy, however, suggests that the opposite is true: it's how we act, and it's the responsibility we feel toward our choices that determines our meaning.

For example, the prisoners in the concentration camps who were able to maintain a purpose in life did so based on the choices that they made. The decision to look for beauty in nature or help others in greater need gave them a purpose, a realization that they were not beaten and could keep going.

One consequence of this is that our meanings don't have to be the same. In fact, everyone has his own meaning of life.

If you ask a chess grandmaster the best move, she'll tell you that there is no best move _in general_. There is, however, a best move depending on the varying situations during the game.

The same applies to life's meaning: there is no general meaning of life, and life's meaning depends on each individual's unique set of circumstances and decisions.

Logotherapy aims to help people understand the possibility that their lives can have meaning and that everybody has to figure out his life's purpose according to his own decisions.

The meaning of life has no restrictions. For example, you might discover that your new job at a recycling start-up offers you personal meaning (e.g., feeling like you're part of a positive contribution to the world) or it could go beyond the personal, and involve society and social conscience (e.g., seeing the improvement in other people's lives).

> _"Life's meaning is an unconditional one, for it even includes the potential meaning of unavoidable suffering."_

### 9. You can manage your fears by actively pursuing them. 

Although the ultimate goal of logotherapy is to help patients find the meaning of life, that isn't its sole application. Logotherapy has also developed a number techniques that are helpful for people who've developed mental disorders, e.g., after experiencing an existential vacuum.

Logotherapy is able to accomplish this by focusing on the internal rather than external factors that affect patients.

In normal psychotherapy, the patient is analyzed and his neurotic fears explained by his environment and other external events and circumstances. In contrast, logotherapy assumes that people are able to make decisions and define their life's purpose independently of their environment.

This basic understanding is necessary to help people realize that they are actually in control of their fears and anxieties in order to achieve long-term results. But how?

Logotherapy makes use of this strange phenomenon: when we fear something will happen, it often does, yet when we try and force something to happen, it never happens!

Imagine you have a nervous friend who is deathly afraid of blushing in front of other people. Since he's always thinking about it, he immediately starts blushing whenever he's in a crowd.

In this situation, logotherapy uses something called _paradoxical intention_, in which the patient is asked to do exactly the thing that she is afraid of.

Your nervous friend, for example, could start trying to blush as much as possible whenever he is around other people. Soon he'll notice that when he tries to force it, nothing happens, and he'll thus lose his fear of blushing.

### 10. Final summary 

The key message in this book:

**Our success, and sometimes our very survival, is dependent upon our ability to find our life's meaning. This doesn't have to be something grand or existential — your own personal meaning depending on your immediate circumstances will do just fine.**

**Suggested further reading:** **The Art of Happiness** **by Dalai Lama and Howard C. Cutler**

_The Art of Happiness_ is based on interviews of His Holiness the Dalai Lama conducted by the psychiatrist Howard C. Cutler. The combination of Tibetan Buddhist spiritual tradition with Dr. Cutler's knowledge of Western therapeutic methods and scientific studies makes this a very accessible guide to everyday happiness.
---

### Viktor Frankl

Viktor Frankl was an Austrian-born neurologist and psychiatrist as well as the founder of logotherapy, a form of psychotherapy. After surviving the Nazi death camps, he wrote several books on how to find the meaning of life.

