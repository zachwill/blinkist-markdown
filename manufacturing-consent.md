---
id: 52822d8633346400082d0000
slug: manufacturing-consent-en
published_date: 2013-03-12T16:24:05.636+00:00
author: Edward S. Herman and Noam Chomsky
title: Manufacturing Consent
subtitle: The Political Economy of the Mass Media
main_color: FBE684
text_color: 7A7040
---

# Manufacturing Consent

_The Political Economy of the Mass Media_

**Edward S. Herman and Noam Chomsky**

_Manufacturing Consent_ (1988) __ takes a critical view of the mass media to ask why only a narrow range of opinions are favored whilst others are suppressed or ignored. 

It formulates a propaganda model which shows how alternative and independent information is filtered out by various financial and political factors allowing the news agenda to be dominated by those working on behalf of the wealthy and powerful. Far from being a free press, the media in fact maintain our unequal and unfair society.

---
### 1. The media play a vital role in indoctrinating people to accept an unequal society. 

The media have many roles; it is the media's job to inform us, entertain us and to amuse us. But the mass media have an additional vital task: the promotion of shared social values and codes of behavior. The government and ruling institutions need an outlet to 'educate' the general population with their ideals, and the mass media fulfill this role.

As society is massively unequal in terms of wealth and power, the media's defense of the status quo is actually a defense of the interests of the dominant elite, ensuring that their politically and economically powerful positions are maintained. The media must therefore slant their coverage to produce stories that support the ruling political and economic classes: a tiny community of people who sit at the top of society.

In their role of defending the social hierarchies, the media effectively create propaganda backing the ruling classes.

The mass media often proudly proclaim that they produce objective and trustworthy coverage whilst holding the powerful to account; the coverage of the Vietnam War is often used as an example. In reality, the media have no interest in defending the public interest against the wealthy and powerful. Instead, they protect the rights and privileges of the ruling elites by reporting events through a narrow, biased lens. This ensures that audiences accept their position in the unequal and unfair structure of society. 

**The media play a vital role in indoctrinating people to accept an unequal society.**

### 2. The media will never criticize the ruling elite, but may appear to do so when opinions within the elite are divided. 

On occasion, the media do appear to criticize the ruling elites of society. There are numerous cases, the Watergate scandal being a fine example, where politicians or business leaders are criticized by the media and their misdeeds exposed.

Such incidents seem to disprove the idea of an inherent bias in the media toward the ruling classes. Certainly, media spokespeople do proudly proclaim that they act as the defenders of free speech and the wider community against the wealthy and powerful. Yet, in these cases, the mass media's so-called criticism merely consists of representing the interests of one group of the elite against another, never of a non-elite group versus an elite group. When criticisms come from outside the elite, the media will suppress or ignore them.

The Watergate scandal exemplified such a split in elite interests. The media were fully willing to investigate and pursue Richard Nixon and his accomplices, because the victims of their crime were the powerful Democrats, a political party who represented a section of the elite. When the Socialist Workers Party, a small party representing no elite interest, was illegally spied upon by government agencies, the media remained silent. 

**The media will never criticize the ruling elite, but may appear to do so when opinions within the elite are divided.**

### 3. The mass media follow a ‘propaganda model’ that filters out information counter to elite interests. 

The mass media heavily slant their coverage in favor of the interests and opinions of the elite. News which goes against these interests is deliberately ignored or suppressed.

Unlike the media in authoritarian societies, the media in the democratic west are not constrained by state ownership or heavy censorship. The forces which coerce the western media are more subtle and 'natural,' leading many to falsely believe that the West has a 'free' and 'objective' press.

The pressures which compel the mass media to publish propaganda for the upper echelons of the social hierarchy can be best explained with a 'propaganda model.' The model contains various _filters_ through which information must pass before it is 'clean' enough to become news.

These filters can consist of financial incentives, such as the need to be profitable or to appease owners or advertisers. For example, the multinational company General Electric owns a huge section of the mass media. They are involved in the contentious areas of nuclear power and the arms trade, and therefore lean heavily on their media networks to stay clear of controversies in these areas.

Other filters can be derived from the sources of the news and how news is presented. For example, because government agencies and large corporations provide a great deal of source material for the news, the media may come to rely on those sources heavily. Those sources could therefore exert control of the media by feeding them carefully selected news items framed in a certain light.

These various filters ensure that any news which is actually presented in the media is staunchly in favor of those in power. 

**The mass media follow a 'propaganda model' that filters out information counter to elite interests.**

### 4. Most of the mass media is owned by a few wealthy families and corporations whose main objective is profit. 

In Britain in the first half of the 19th century, the left-wing radical press owned by small, independent proprietors flourished. This medium helped represent and spread working class views, seriously threatening the ruling class's monopoly on information. Despite attempts to quell this media through libel laws and prosecutions, the radical press remained strong.

What destroyed the radical press's power was the free market. Where state repression had failed, the ultra-competitive market succeeded. The industrial revolution allowed large publications to reach mass audiences through new printing machines, while the cost of buying and maintaining such machinery squeezed out the underfinanced radical press. This left only the right-wing press backed by the dominant elites, and it prospered. This process continued and the industry consolidated, leaving a few massive giants to dominate the entire media market of the Western world.

Even today the mass media is owned and controlled by a few rich families and corporations whose sole goal is profit. The immense power they wield cannot be underestimated; the top 29 media providers account for over half of America's newspapers and the vast majority of sales and audiences of magazines, movies, books and broadcasts. Independent media has very little chance of survival against this monopoly.

Such control over the market makes the mass media an attractive industry to investors such as banks and brokers. In return for their capital, these investors expect the media to pursue profit through sales and advertising.

These two forces — control by a tiny elite and the need to prioritize profits — severely hamper the objectivity of the mass media.

**Most of the mass media is owned by a few wealthy families and corporations whose main objective is profit.**

### 5. The media depend on advertising revenue for their survival and will therefore take measures to keep advertisers pleased. 

In the media, business costs from studios, publishing facilities and reporters are high, and the industry is highly competitive. Success in the marketplace depends overwhelmingly on the sponsorship of advertisers. Media organizations that lack advertising revenues are very likely to fail.

Therefore, the main objective of media companies is the attraction of advertisers. To this end, the media will seek to please its advertisers through biased coverage. Thus, advertising becomes another filter in the propaganda model, allowing the wealthy to filter out critical news stories.

There are many ways in which the mass media are obliged to alter their reports due to advertising pressure. The most obvious is the suppression of news which could potentially damage big business. In one case, a US TV network lost its advertising funding after showing a documentary highlighting the malpractice of multinationals in the Third World.

In televised media, advertisers will even go as far as to demand that programs with a serious outlook be removed from the schedule as they might interfere with the 'buying mood' of the viewer. Advertisers prefer light entertainment over hard-hitting documentaries and dramas. 

The overriding interest for advertisers is of course maximizing sales. To achieve this, corporations will coerce the media to target their content to the interests of wealthier audiences who will buy more products from advertisers. Media that appeals to poorer working class audiences will stand less chance of attracting money through advertising, and thus the scope of perspectives available in the media is narrowed.

**The media depend on advertising revenue for their survival and will therefore take measures to keep advertisers pleased.**

### 6. The media’s need for regular material forces them to rely on government organizations and large corporations. 

The media need a reliable and constant stream of information to fill their news schedules and columns, but of course they cannot have reporters at every location where news stories might break. Hence, they must focus on sources which provide material constantly.

Such sources are overwhelmingly state institutions, such as the police and government agencies, or the press departments of large corporations.

The reasons the media use these sources are obvious. Their sheer size enables them to provide regular material, and in addition they are regarded as reliable and objective, meaning that the media are able to treat information as fact without the expense of having to check it.

But the overreliance on these bodies enables the ruling elites to control and 'manage' the media, adding another filter to the propaganda model that determines what news is allowed to trickle through to the masses.

The dominance of the government and big businesses in providing information allows them to set the news agenda. They can feed the media with stories at the optimum times in order to boost their position. For example, in 1984 the media released a carefully-timed false story about the supply of Soviet MiG aircraft to Nicaragua. The story helped stir up alarm in the United States and discredit the Nicaraguan election, thus helping President Reagan's political agenda.

Alternative sources of information are at a distinct disadvantage, because the news they provide is sporadic and open to criticism, especially if that information is contrary to elite interests. The media may even be under pressure to suppress alternative views for fear of upsetting their regular sources.

**The media's need for regular material forces them to rely on government organizations and large corporations.**

### 7. The elite punish critical media by generating ‘flak.’ 

If the media report the news in a way that offends the interests of the ruling social groups, they can find themselves facing a backlash. The criticism thrown back at the media by the elite is known as 'flak.'

Flak can take various forms: Journalists and media outlets can be directly threatened by the government or corporations, becoming the target of negative press releases. The elite can also create flak indirectly, leaning on advertisers to boycott certain media companies. Sometimes even legal action is taken against critical media.

The aim of flak is to put free-thinking media on the defensive, fostering an image of an unfairly critical media with a 'liberal bias.' Well-directed and funded flak generates fear in media companies, creating another important filter in the propaganda model.

The ruling elites can generate flak through right-wing 'think tanks' whose sole role is to target the critical media with flak. These organizations are often prestigious, powerful and richly funded by the elite, so when they say the media is being unfairly critical, people tend to listen. 

A good example of the power of flak is the dossier published by the right-wing think tank 'Freedom House' on the media's role in the Vietnam War. This document claimed the media were too pessimistic in their reporting of various campaigns in the war, and it even accused the mass media of having cost the United States the war by misleading the American public through this reporting. The dossier was well received amongst the elite and reported on positively in the mass media despite research inaccuracies and exaggerated conclusions.

**The elite punish critical media by generating 'flak.'**

### 8. The mass media view all events through the prism of the battle against communism. 

The mass media are coerced by the ruling elites to judge events through the lens of an ideological battle between the 'free' and communist worlds.

The actions of communist forces are always reported negatively and those of America and its allies seen in a positive light. Thus, executions and torture in a communist-controlled state will be reported at great length, whereas similar occurrences in a nation friendly to America will be completely ignored.

The advantages for the ruling elites from this ideological framing are clear: It helps to gather support across society against a common enemy. The specter of communism is a universal fear across diverse communities and helps bind them together in support of American policy. People are prepared to accept and defend the actions of those at the top of society as long as they help defeat the menace of communism.

The anti-American connotations of communism can also be used against groups which criticize the inequalities in society. Those who threaten the social hierarchies can be accused of being pro-communist and therefore by definition 'anti-American.'

Liberals are kept constantly on the defensive through fear of being labeled as a communist or as not anti-communist enough. As a result, to counter such criticism they may feel the need to adopt a more right-wing position. Because the majority of the media does this, the perceived political center of society as a whole also shifts further to the right.

This pressure to report right-leaning stories also constitutes another filter in the 'propaganda model.'

**The mass media view all events through the prism of the battle against communism.**

### 9. When reporting world news, the mass media heavily favor states that are allied with the West. 

The mass media like to proclaim that their coverage is objective, but if this were the case they would report events in the same way irrespective of where in the world they happened. Looking at the media's coverage of events, it is clear that this is not so, as they slant news stories differently depending on the political situation of those involved.

A perfect example of this is the mass media coverage of the issues concerning Central America. The nations in this area lie within the sphere of American influence and are therefore of interest to the US government and mass media. Some nations, such as Guatemala and El Salvador, are US-sponsored military dictatorships. Others, such as bourgeoning democracy Nicaragua, are left-leaning and are therefore mistrusted by US authorities.

The media slant their coverage of events from these nations depending on their friendliness to elite interests in the US, often completely reversing the actual truth if deemed convenient.

Take media coverage of elections in Central America. The sham elections in US puppet states such as Guatemala are viewed as genuine and their results, showing widespread support for government forces, are treated as reliable. This is despite the various reports of fraud, intimidation of voters and even violence.

On the other hand, the relatively free and open elections held in Nicaragua are reported as little more than propaganda exercises for the left-wing leaders, even though the wider international community regards them as fair and open to all.

**When reporting world news, the mass media heavily favor states that are allied with the West.**

### 10. The media will often use ‘expert’ opinions to support their biased views. 

The mass media will often employ the insights of an 'expert' when discussing events. This, the media hopes, will lend authority and objectivity to its reports. Yet, despite their unbiased image, media 'experts' are in fact a crucial component in spreading the propaganda of the elite.

The ruling elites are able to furnish the media with a constant stream of 'experts,' because they spend huge amounts of resources educating and employing them. Think tanks and similar institutions are created to fund and publish the studies of 'experts' who are able to spread ruling class opinions in the media.

It is important to realize that the actual role of the 'expert' is not to help understand events, but to provide much needed gravitas to the elite interests and opinions being broadcast. The media only hire 'experts' who espouse the views of the dominant elite.

An example of how 'expert' opinion can support biased reporting can be seen by the reaction to an assassination attempt on Pope John Paul II in 1981. The would-be assassin was a right-wing Turkish national. However, two 'experts' paid for by the media attempted to blame the plot on the Soviet Union. Their evidence was highly questionable and easily disproved; nevertheless, it was taken up without criticism and spread by the majority of the mass media. The credibility provided by the 'experts' had effectively allowed a weak conspiracy theory to gain precedence through the media.

**The media will often use 'expert' opinions to support their biased views.**

### 11. The mass media view some people’s lives as more worthy than others depending on what message their deaths send. 

In 1984, a Polish priest who actively campaigned against Poland's communist government was abducted, beaten and murdered by members of the secret police. The story was covered at great length in the US media with the emphasis on its emotional impact and its wider political implications for the communist system.

This story is perfect for the media, because it fits into the framework created by the interests of the ruling elites: It casts the communist enemy as a brutal and dangerous force which in turn helps gather support for American policies.

Compare this with the media's lack of coverage of the torture and murder of hundreds of religious representatives in Central American states friendly to the US. There are countless examples of how priests who stand against the autocratic governments in those states face vicious and violent oppression, yet the media remains largely silent.

By comparing the coverage devoted to each case, it would appear that the life of a priest murdered in Poland is worth one hundred times more than a priest murdered in Central America.

The reason is obvious. The media must gleefully report wrongdoings in enemy nations, using lurid details to raise anger amongst viewers and readers, while linking the crimes _directly_ to the ruling system.

With US allies, on the other hand, the media keep state crimes hidden to preserve unity; even going as far as to conceal the murder of US citizens in Central America. They are considered victims unworthy of our attention, because they died in the wrong circumstances and at the hands of the wrong people.

**The mass media view some people's lives as more worthy than others depending on what message their deaths send.**

### 12. Final summary 

The key message of this book:

**The mass media defend the interests and opinions of the ruling political and economic elites and promote the maintenance of a vastly unequal and unfair society. In order to achieve this, the media follow a 'propaganda model' that filters out critical views leaving the news agenda dominated by elite views.**

The questions this book answered:

**Why do the mass media defend the interests of the ruling elites?**

  * The media plays a vital role in indoctrinating people to accept an unequal society.

  * The media will never criticize the ruling elite, but may appear to do so when opinions within the elite are divided.

**What is the 'propaganda model' that allows the ruling elite to control the news agenda?**

  * The mass media follow a 'propaganda model' that filters out information counter to elite interests.

  * Most of the mass media is owned by a few wealthy families and corporations whose main objective is profit.

  * The media depend on advertising revenue for their survival and will therefore take measures to keep advertisers pleased.

  * The media's need for regular material forces them to rely on government organizations and large corporations.

  * The elite punish critical media by generating 'flak.'

  * The mass media view all events through the prism of the battle against communism.

**How do the mass media formulate their coverage in order to promote elite opinions?**

  * When reporting world news, the mass media heavily favor states that are allied with the West.

  * The media will often use 'expert' opinions to support their biased views.

  * The mass media view some people's lives as more worthy than others depending on what message their deaths send.
---

### Edward S. Herman and Noam Chomsky

Edward S. Herman is an academic specialising in finance who has written many works on political economy and the media.

Noam Chomsky is a world-renowned expert in the fields of linguistics and global political culture. He is best known for his criticisms of globalisation, American power and the mass media. He has authored numerous publications including _Hegemony or Survival_ and _Deterring Democracy_.

