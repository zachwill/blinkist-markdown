---
id: 56125211fd22820009000018
slug: marissa-mayer-and-the-fight-to-save-yahoo-en
published_date: 2015-10-06T00:00:00.000+00:00
author: Nicholas Carlson
title: Marissa Mayer and the Fight to Save Yahoo!
subtitle: None
main_color: 702480
text_color: 702480
---

# Marissa Mayer and the Fight to Save Yahoo!

_None_

**Nicholas Carlson**

_Marissa Mayer and the Fight to Save Yahoo!_ (2015) takes us on a journey through the ups and downs of this one-time leader of the early internet. These blinks explain how Yahoo has changed, and often struggled, as it progressed from its early start-up days to a multi-billion dollar corporation. The blinks also put a spotlight on Yahoo's latest CEO, Marissa Mayer, and her extensive efforts to keep the company moving forward.

---
### 1. What’s in it for me? Catch a glimpse behind the scenes at Yahoo. 

There was a time before Google ruled the internet. Back then, Yahoo was _the Internet._ But those days are gone: if Yahoo vanished off the face of the earth right now, it would hardly change your internet experience. For almost all of Yahoo's services, there's a better alternative out there.

In 2012, someone set out to rescue Yahoo — an ambitious character, used to achieving her goals. At just 37, Marissa Mayer was already looking back at an impressive career at Google Inc., leaving the company while at the post of vice president. 

While she's rumoured to be shy, Mayer's controversial quotes and daring decisions — like buying the microblogging platform Tumblr for the record sum of $1.1 billion — frequently make headlines. Taking the helm as Yahoo's CEO, Mayer made some terrible choices about the company's media business, but simultaneously gave Yahoo some much-needed direction, streamlining the product palette and betting heavily on mobile apps.

But did it work? The jury is still out.

In these blinks, you'll learn

  * about Yahoo's very modest beginnings at Stanford University;

  * about the company's most foolish decision (so far); and

  * about the meteoric rise of Marissa Mayer.

### 2. The early Yahoo was promising, and had no trouble finding financial support. 

Like many other success stories, Yahoo came from modest beginnings. In 1993, Yahoo's prospective founders David Filo and Jerry Yang shared the links to their favorite web pages on a website called _David and Jerry's Guide to the Internet_. 

Back then, the world wide web was relatively new and their site was little more than an index of web pages, allowing people who were new to the internet to look up pages they were interested in. Unexpectedly, traffic grew to 50,000 visits per day. It seemed that David and Jerry's site had tapped into something unique. 

The founders renamed the site to the more memorable _Yahoo!,_ an acronym for "Yet another hierarchical officious Oracle". They also liked the slang definition of the word "yahoo," referring to a rude, unsophisticated person. And so Yahoo was born.

Traffic kept growing steadily, reaching one million clicks per day in 1995. At this rate, the infrastructure provided by Stanford University wasn't enough to support the site. David and Jerry needed real servers for Yahoo — and for these, they would also need some cash. 

Fortunately for them, finding investors was a piece of cake. Sequoia Capital partner Mike Moritz decided that the newbie company needed "adult supervision" to scale up the business. This gave the founders the freedom to focus on the innovation side. Sequoia would become COO, while Stanford graduate Tim Koogle would take on the role of CEO and chairman. 

In March 1995, only days after Yahoo was incorporated, they declined an offer of $2 million from America Online to buy Yahoo outright, and instead sold 25 percent of the company to Sequoia Capital for $1 million. With adult supervision in place, it was time for Yahoo to create a serious business plan.

### 3. Yahoo became an online advertiser and a billion dollar company within four years. 

Yahoo had its investor and infrastructure — now it just needed to make some money. So, the company's leaders decided to turn to advertising to generate revenue. Back in 1995, online advertising was still finding its feet. The estimated volume of the entire online advertising market that year was only $20 million. 

Yahoo made its first deal with renowned international news agency Reuters, offering to publish their stories on Yahoo.com. Yahoo then signed a deal with Visa and General Motors to post ads, charging as little as $20,000 per month. Business picked up speed soon after.

By 1997, Yahoo's revenue surpassed $70 million and reached $200 million the subsequent year. Traffic at Yahoo.com went from six million clicks per day in 1996 to 167 million in 1998. Meanwhile, the number of employees had also increased from 200 in 1996 to nearly 2,000 in 1999. 

This incredible growth is also apparent when looking at how the company's value changed over the course of its first few years. 

After Yahoo held its initial public offering on April 12, 1996, the company had a market capitalization of $848 million. This meant that the remaining share that Jerry Yang and David Filo owned was worth $130 million each. 

In 1999, Yahoo had a market value of $23 billion, which made Filo and Yang internet billionaires just four years after they had started swapping lists of web sites with each other. 

Ultimately, it was the exponential growth of the internet and its users that made Yahoo a billion-dollar company. The basic idea behind Yahoo's business model — to create online traffic and sell ads — hadn't changed. But as it turns out, only 20 percent of the traffic was generated as a result of the site's initial feature, Yahoo's directory. The remaining 80 percent of traffic was directed at entirely new products developed by Yahoo. How did they develop these products? Find out in the next blink.

### 4. Innovative projects and data insights allowed Yahoo to rapidly create hundreds of new products. 

Yahoo's COO Jeff Mallett came from the world of computer software, where you essentially build products and hope users like them. But he realized that Yahoo had the potential for a much more auspicious approach.

Yahoo began to use traffic data to match products to customers' needs. Mallett realized that with the data about customers' preferences gleaned from web searches, he could simply analyze their clicks and search behavior on Yahoo to build exactly the products that these customers wanted. He wouldn't have to guess what people needed, because he would already know.

This was the beginning of Yahoo's general strategy: keep an eye on the server logs and develop products quickly that cater to the new internet user. A project-based organizational structure helped Yahoo develop hundreds of new products to generate traffic.

But how could Mallett foster the creativity needed to develop all these products? He knew that a conservative organizational structure just wouldn't cut it for the level of innovation he was aiming for. So he decided to hire as many people as possible and group them into product teams known as "pods" or "virtual sevens."

Mallet would take someone from Yahoo Finance, for example, and tell her to find six other people across the organization to build an entirely new product. They were free to collaborate with anyone inside the organization regardless of distance or department. 

By 2000, Yahoo offered more than 400 new products, including chat rooms, games, sports, real estate, a calendar and much more. There were hundreds of pods and ad hoc virtual sevens, and just a few classic core corporate functions such as HR and legal. Things seemed to be going brilliantly. 

But soon, Yahoo's hunger for growth would prove problematic.

### 5. Yahoo’s first big mistake was selling its name. 

By late-2000, things at Yahoo began to fall apart. The dot-com crash had hit Yahoo hard, and in that year, the company's value had shrunk by 90 percent from $128 billion to $12.6 billion. Where did it all go wrong? Put simply, Yahoo sold out. 

Since Yahoo was such a popular and big company, every time a new start-up officially announced its collaboration with Yahoo, the start-up's market value would skyrocket. Some Yahoo executives used this tendency to make deals with start-ups. 

Start-ups would pay Yahoo to become their premier bookseller, online travel agency or other main service provider. Then the start-up would announce this deal publicly and people would race to invest in it, simply because it was backed by Yahoo and the company was thus expected to succeed.

In 1999, a start-up called Drugstore.com was paying Yahoo $25 million for a major advertising partnership and to become its premier online pharmacy partner. Although the company had only $38 million in the bank and had an annual loss of nearly $20 million, everyone invested and bought its shares. At the end of its IPO, Drugstore.com had a market value of $2.1 billion. 

Companies that made an advertising deal with Yahoo were top-ranked in the search results as recommendations. These deals, and not the products' quality, were the basis of the listing, and Yahoo offered prime advertisement partnerships to unstable, underperforming dot-com companies. 

After a while, people became aware of this and stopped clicking on Yahoo's ads and top-ranked items. As a result, advertising became a lot less lucrative for Yahoo; consumers simply didn't trust them anymore. 

In Yahoo's early years, about five percent of users would click on the advertisements Yahoo would show them. By 2000, clicks were down to 0.5 percent and falling. The times when Yahoo could charge exorbitant prices for ads because of their success rate were over. So how did the company react?

### 6. Yahoo built a traditional advertising business and recovered. 

Yahoo had destroyed the integrity and profitability of their online advertising business, and it was time for its first CEO Ted Koogle to resign. His spot was filled by Terry Semel, former CEO of Warner Brothers. 

The new CEO reorganized Yahoo's ad business to make it a traditional ad sales business. Semel's strategy was simple: although much of Yahoo's market value was destroyed with the implosion of the dot-com bubble, more and more web users were visiting the site each day. 

Semel used this attention to build a traditional advertising business that didn't rely on questionable dot-com companies, and hired Greg Coleman, a seasoned sales manager. In turn, Coleman took care to hire salespeople who actually knew buyers in the companies they were trying to sell ads to, a link that Yahoo didn't have in place before. 

Coleman also hired sales representatives who knew how to sell ads over the phone and not just by email, which was the standard practice at Yahoo back then. Consequently, Yahoo started selling many ads to traditional companies. 

Coleman liked to use a fishing analogy to describe the changes he made at Yahoo. He said that during its first years, Yahoo was a fishing boat that didn't need to fish because the fish would willingly jump into the boat by themselves. Although it wasn't the case anymore, there were still plenty of fish in the water — Yahoo just had to learn how to actively catch them.

But did Coleman's fishing approach do the trick? Well, in the first year after Semel got the job as CEO, Yahoo still had an annual loss of $98 million. But by 2005, Yahoo had profits totaling $1.2 billion. The company's market capitalization went from $12.6 billion to $50 billion in the following year. 

All in all, building a conservative advertisement sales business had been good for Yahoo. Unfortunately, there were more problems ahead, and bigger fish to fry.

### 7. Not buying Google when it had the chance could be the biggest mistake Yahoo ever made. 

Yahoo wasn't doing too badly after its change of CEO. But in 1997, the company missed an opportunity that would have secured its dominant role in the internet for decades to come.

Larry Page offered Yahoo his thesis project BackRub, which would later become Google, for a meagre $1 million. He wanted the money to finance his PhD and become a professor. But Yahoo passed up on the offer! Why?

Although Yahoo conceded that the future of online searching lay with algorithms, the company didn't plan to have its own search engine. Instead, Yahoo wanted to continue buying search functionality services from other companies, giving them the freedom to change their search engine provider whenever necessary. 

Obviously, Yahoo would come to regret its decision. Soon thereafter, _google.com_ began to grow incredibly fast, with its search engine becoming far more popular than any other. Google had started to steal traffic from Yahoo.

The reason web users preferred Google was its improved approach to organizing search results and managing advertising. As we found out in the previous blinks, Yahoo chose to place the highest-paying advertisers at the top of their search page. 

Google's system was more sophisticated, but also produced more valuable results for users. According to Google's reasoning, an ad that only pays $0.55 per click, but gets clicked twice as often as a $1.00 ad, is more profitable (and more relevant for the user) and should thus be higher up on the results page. 

When Google turned out to be so successful and profitable, Yahoo tried in vain to buy it, again and again, offering up to $6 billion in 2002. It's now clear that Yahoo well and truly missed the boat.

### 8. Competition with specialized start-ups and lack of organization took their toll on Yahoo. 

The growing influence of competitors like Google started to chip away at Yahoo's traffic. Competing with too many specialized start-ups wasn't helpful for Yahoo's public and internal image; the company had spread itself too thin. 

Yahoo tried to compete with CNN in news, with ESPN in sports, with MySpace in social networking and of course with Google in online searches. When Semel joined Yahoo, it had 400 different products and services.

Of course, in every sector, Yahoo paled in comparison to well-funded start-ups that did just one thing very well. The reality was that no one knew what Yahoo was trying to be.

On a retreat, one manager asked his fellow managers to write down the first thing that came to their minds when he named a company. So, when he said "PayPal," they wrote "payments;" when he said "Google," they wrote "search;" but when he said "Yahoo," everybody wrote down something different. 

Another challenge facing Yahoo was a lack of internal organization. There were too many people with similar projects and overlapping responsibilities, which is a recipe for inefficiency. 

If a team's project is just one among many similar ones, they won't be as committed as when a project is "their baby." Moreover, if too many people are responsible for the same tasks, no single person can be held accountable for mistakes.

Yahoo's board knew they needed someone new who understood the tech business better than the current CEO and who could give Yahoo the vision and purpose it obviously lacked. Their choice was Marissa Mayer.

> _"Yahoo tried to be everything for everyone and failed to be exceptionally good at something."_

### 9. Marissa Mayer quickly gained traction and experience in the tech world. 

Marissa Ann Mayer was born in 1975 in Wisconsin, where a middle-class upbringing allowed her to shine at various hobbies. The ambitious and talented Marissa played volleyball and basketball, took piano lessons and at one point even took ballet for as many as 35 hours a week during middle school. 

Quiet, intelligent and with little in the way of social skills, she fit the mold of a nerd perfectly. But for all her nerdiness, it soon became clear that Marissa was quite good at giving directions. Whether in sports or class projects, she always took over a leadership role and was supported by her peers, who felt she was fair and exceptionally good at managing progress.

Marissa went on to study at Stanford University, undertaking the prestigious "symbolic systems" major. This blend of linguistics, philosophy, computer science and cognitive psychology also counts Instagram cofounder Mike Krieger and LinkedIn founder Reid Hoffman among its alumni. Mayer graduated among the top students in her class.

Right after graduating, she accepted a job offer from Google, where her tech-savvy approach and perfectionism were put to good use. She soon discovered her talent for designing flawless user experiences by optimizing the user interface (UI) of Google's products. 

Very quickly, she climbed the corporate ladder and became the leading figure in charge of UI for all of Google's products, especially search. 

Marissa's keen eye for detail and data became notorious at Google. In one meeting, she talked for hours about multiple shades of blue and their impact on user experience. She always demanded that decisions be made on the basis of user data. 

With a brilliant mind and considerable experience, Mayer had the potential to become Yahoo's saving grace.

> _"I don't really believe in burnout." - Marissa Mayer_

### 10. Mayer gave Yahoo a new direction: mobile internet. 

As we found out earlier, Yahoo could do everything, but nothing especially well. It was this aspect of the company that Mayer was determined to change. 

When Mayer became Yahoo's CEO, the company faced an important decision: what was it going to become? Would it be a media company that focused on large-scale online publishing, or the production of content such as videos and news? Or would Yahoo be better off as a tech company, centered on developing innovative new products? 

At this crossroads, Mayer chose the latter. Knowing that Yahoo's original success was borne out of making the early internet user friendly, she believed they could do the same for the _mobile internet._ So, to turn Yahoo into a tech company, Mayer's first step was to scale up the mobile app team. She resolved to personally ensure that Yahoo's mobile products would excel. 

Her market research was extensive, and yielded a list of the top-priority daily habits of mobile users, including news reading, checking weather, checking mail and photo-sharing. Mayer was determined to develop the best app for each at Yahoo.

But one of Mayer's top priorities in her first months was the development of Yahoo Mail, especially its mobile version. Mayer impressed the app team with her technical knowledge. After all, improving user experience by smoothing the user interface design was her main area of expertise from her time at Google, as well as her personal favorite part of the product to work on. 

Soon after Mayer had joined Yahoo, new versions of Yahoo Mail, Flickr and Yahoo's homepage were launched. Morale was high and people were impressed with the speed of the development process, but it wasn't enough.

> _"Yahoo was going to make apps, and Mayer was going to be picky about their quality."_

### 11. Behind the illusion of success lay a poorly managed media department and ever-dwindling revenue. 

On December 2012, Yahoo's stock was up 24 percent since Mayer became CEO. Only a few months earlier, she had outbid Facebook to buy Tumblr for $1.1 billion and morale was at an all-time high. More and more highly qualified candidates began sending their résumés to Yahoo. 

It looked like Yahoo was finally out of the woods. But it was actually far from it! Despite all its efforts, Yahoo's search market share was still shrinking, as was its traffic and, ultimately, its revenues from selling ads. As it turned out, Yahoo's rising stock value was mostly a result of the fact that it owned parts of Alibaba, an exploding Asian start-up.

Meanwhile, there was a major aspect of the Yahoo business that was being neglected: media. Yahoo's media business had three thousand employees and revenues of $1.5 billion. It was a big and stable contributor to Yahoo's $5 billion total revenues, and would have to play a big part in Yahoo's future.

After Mayer realized the importance of this part of business, she began to dive into it. But as it turned out, she wasn't made for the media business. While her decision making regarding tech products was always based on evidence, Mayer went with her gut when it came to media.

Unfortunately, her choices were often miles off the mark when it came to the preferences of Yahoo users. She would turn down programs that were huge commercial successes, simply because she didn't like them. When actress Gwyneth Paltrow, who had a best-selling cookbook at that time, was about to be hired as a contributing editor for Yahoo Food, Mayer blocked the hire because "she didn't even go to college."

It took Mayer a while to accept that she wasn't made for this side of the business. When she finally did, media was placed in more capable hands at Yahoo, and Mayer returned to the tech side of things. But Yahoo's business was still struggling all the while.

### 12. Mayer’s efforts weren’t enough to save Yahoo, and the fight continues. 

Mayer and her development teams increased their efforts even more and began to push themselves to the limit. Mayer's efforts to increase the speed of processes caused problems within Yahoo and among its users. 

The new Yahoo Mail was suffering multiple breakdowns, and users couldn't access their mail. The reason was that the development team was pushed so hard to produce results quickly, they couldn't properly test the underlying technology before launching. 

Slowly but surely, Mayer's ambitions began to grate on many of the company's employees. Some of her decisions and personal behavior saw many talented people leave Yahoo altogether.

According to Yahoo's employees, one of the biggest problems was Mayer's employee rating system. Because Mayer wanted to dismiss low performers and cut costs, managers had to grade their employees' performance on a fixed curve. Even on top-performing teams, someone always had to get a poor score. 

This caused much frustration and eventually encouraged hostility rather than collaboration among employees, since everyone was competing with each other. 

But Mayer still didn't give up. She engaged more actively in the development of apps, replaced some of her executives and gave motivational speeches in front of employees — but the numbers weren't improving. 

Revenues still shrunk because Yahoo's media content couldn't attract more customers. The apps, though very well designed, just couldn't generate enough traffic.

Why weren't things working? Well, Yahoo's biggest asset was that it had made the internet easy to use for normal people back in the internet's early days. But Google, Facebook and others were doing that now, too. 

When Yahoo tried to do the same for mobile internet, Apple's iPhone and Google's Android had already solved that problem for everyone. 

The company hasn't found its true purpose since. If Yahoo can target and solve one big problem that technology users face today, then they might have a shot at future success. In the meantime, the struggle to rescue Yahoo continues.

### 13. Final summary 

The key message in this book:

**After explosive success in its early stages, Yahoo is today struggling against its competitors and with the challenge of finding its true purpose. Marissa Mayer, the company's latest CEO, has given Yahoo some hope, but has yet to create a roadmap for the company to regain relevance in the present and for the future.**

**Suggested** **further** **reading:** ** _#GIRLBOSS_** **by Sophia Amoruso**

In _#GIRLBOSS_, author Sophia Amoruso tells her story of transformation, from high school dropout to CEO of a multimillion-dollar fashion empire. Whether starting a business from scratch, building a powerful community or choosing the right investors, Amoruso tells you what it takes to be a woman in business today.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Cover image: © Yahoo**
---

### Nicholas Carlson

Nicholas Carlson is _Business Insider_ 's chief correspondent and investigative reporter. He wrote about the histories of Facebook, Twitter and Groupon and is the author of _The Cost of Winning: Tim Armstrong, Patch and the Struggle To Save AOL_.

