---
id: 561bd1c93635640007150000
slug: marketing-3-dot-0-en
published_date: 2015-10-12T00:00:00.000+00:00
author: Philip Kotler, Hermawan Kartajaya and Iwan Setiawan
title: Marketing 3.0
subtitle: From Products to Customers to the Human Spirit
main_color: 5397D1
text_color: 3F729E
---

# Marketing 3.0

_From Products to Customers to the Human Spirit_

**Philip Kotler, Hermawan Kartajaya and Iwan Setiawan**

_Marketing 3.0_ is a handbook to the marketing strategies of tomorrow. These blinks explain how old tactics have grown outdated as people search for meaning in their lives and the companies that can provide it. Read on to learn how you can adapt your marketing strategy to the changing nature of consumer preferences.

---
### 1. What’s in it for me? Learn about marketing strategies that will work with modern-day consumers. 

Marketing seems easy today. You can enhance any photo into an advertisement with a few clicks, using a run-of-the-mill image processing program; you can use social media to spread your sales message around the world in a few seconds, with no printing or postage involved; and there's decades' worth of marketing research all translated into easy rules, ready for you to implement.

But here's the bad news: while your knowledge has increased and those technologies have gotten better and better, your prospective clients have changed, too! Consumers are bombarded with shiny images and catchy slogans. They have become wary of false promises, and they're no longer passive consumers of media — they want to participate. If you don't engage them, they'll simply stop listening!

Old-fashioned marketing is ill-equipped to deal with today's smart and socially conscious clients. 

Reading through these blinks, you'll learn about a new kind of marketing, _Marketing 3.0_ — the only kind that will work with your modern clients. 

In these blinks, you'll discover

  * why an American grocery store chain educates every cashier about gourmet food;

  * why creative folks are making the life of marketers all the more challenging; and

  * how bad-tempered actors playing Goofy might raise questions about Disney's integrity.

### 2. Advertising was transformed when consumers began participating in the design process. 

Remember the '80s? Back then, our relationship with entertainment media was different than it is today. It was something you passively consumed. Especially with activities like watching TV, the only "active" part usually involved snacking on some peanuts or popcorn. 

In those days, the distinction between the media and the masses was clear-cut, which made marketing a breeze. Marketers just had to explain how great their product was, and people would listen. 

But things are different these days. 

Widely accessible technologies have transformed media sources into networks of exchange. People don't sit idly by and consume their news, ideas and entertainment. Instead, they actively create them, marking our time as the _age of participation_. 

Two main factors account for this shift: first, technologies have emerged that allow us to connect and interact with others quickly and easily; second, these technologies have become much cheaper with the growth of open source sites. As a result, practically everybody can afford to participate. 

Especially through social media, the internet has transformed participation. But social media can itself be broken down into two categories. 

The first is called _expressive social media_ and includes blogs, Facebook, Youtube, Twitter and other channels through which people share their personal views. The second is _collaborative social media_, which uses _open sourcing_, meaning it can be developed and changed by almost anyone. A well-known example is _Wikipedia._

The rise of social media in general has also transformed advertising. Consumers now tend to trust the people in their social networks more than advertisers. 

As a result, classic advertising methods are losing influence and more companies are focusing on social media. In fact, collaborative social media has opened up new ways to win over customers because it allows consumers to develop products using shared platforms provided by companies. 

For instance, through its open innovation program, _Connect + Develop_, Procter & Gamble enlists its customer base in the product design process.

> _"Any customer can have a car painted any color that he wants, so long as it is black." - Henry Ford_

### 3. Creative people are shaping society and companies should attend to their needs. 

New forms of advertising are marking the emergence of a new economy, in which an increasing number of people work in the creative sector as filmmakers, writers, website designers and so on. 

While these creative workers still represent a relatively small sector of society, they exert considerable influence through their lifestyles and opinions. In addition, these consumers have sophisticated desires that demand a new approach to business and marketing. 

In this sense, creative people flip Abraham Maslow's _hierarchy of needs_ on its head. 

According to Maslow, humans' needs can be divided into levels. The most basic need is for survival, then safety, then love and belonging. Next comes boosting one's self-esteem or ego, and last is self-actualization. A need higher up on the pyramid can only be satisfied once the one below it has been. 

For example, someone who is incredibly hungry might steal or lie to obtain food, even if doing so hurts their self-image. Their base need for survival overpowers their need for self-esteem. 

The thing is, the pyramid doesn't hold up when applied to creatives. For them, making the world better while finding meaning, happiness and spirituality are stronger drives than the lust for material possessions. 

This is significant because the opinions of creatives guide the ideas and desires of others, especially through their use of social media. So, as a corporation, it pays to avoid making enemies of creative people, such as by avoiding poor business practices or products that can land you in trouble. 

On the other hand, it's always beneficial to offer something that strikes a chord with the values and spiritual inclinations of creatives. If you can, you should carefully communicate it through marketing campaigns and follow through with it in your actions. 

The changing nature of consumers means marketing needs change too. But what are the founding principles of _Marketing 3.0_, the marketing style of the future?

> _"Supplying meaning is the future value proposition in marketing."_

### 4. Modern marketing embraces everything that makes us human. 

A useful way to look at our existence as humans is to view us as comprising four basic components: a physical body, a rational mind that analyzes the world, a metaphorical heart at the center of our emotional needs and responses and, finally, the soul, our spiritual center. 

Each of these fundamental elements has its own needs, and successful marketing means appealing to these needs. However, traditional marketing only actually targets two of the above components. 

First, it appeals to the minds of customers with a clear _brand identity_. This happens when marketers find a way to insert their brand into the minds and memories of their customers. In saturated markets, achieving this requires companies to stand out, while also remaining relevant to their customers' rational needs and desires. 

Second, traditional marketing works hard to evoke emotions through the brand being marketed. To do this, marketers develop an excellent _brand image_. In ideal cases, this image goes straight to consumers' emotional needs. Think of a pair of shoes made by a prestigious brand; these can satisfy a customer's need for status or importance. 

But Marketing 3.0 helps you go further by doing all of the above, while also targeting a third aspect: the soul. 

How?

By establishing _brand integrity_, a state you can only establish by building trust for your brand and working in accordance with the values communicated through your brand identity and image. These three aspects together make up the _3i._

You can achieve the missing objective of integrity by proving that your company is capable of keeping its promises. 

For example, Timberland is a company that applies 3i. Its brand identity is of a socially responsible outdoor apparel manufacturer, and it stays true to it with a program called "Path of Service," which gives paid time off for employees to volunteer in their communities.

> _"Marketing in its culmination will be a consonance of three concepts: identity, integrity, and image."_

### 5. A company can demonstrate its commitment by defining a mission, vision and values. 

You know that lots of people are interested in making the world a better place while satisfying their spiritual desires, which means companies must demonstrate nobler goals. But how can you get started?

The first step for every business is its mission. Management legend Peter F. Drucker once said that businesses should plan based on their missions, not their financial goals. When you speak about a corporation's mission, you give it a reason for existing in the first place — a base purpose. For instance, in the case of Timberland, their mission is "to make it better."

But after the mission comes the vision, which is all about the future. In contrast to the mission, which has its roots in a company's beginnings, the vision is like a beautiful home at the end of a long road. It represents future goals and guides your strategic direction like a compass. 

But however distinct a company's vision is from its mission, the former is still based upon the latter. Thus, whatever a company states as its basic business model will determine its ongoing strategy. For instance, Timberland's corporate vision is "to be a twenty-first century example for socially responsible corporations around the world."

Once a company has a mission and a vision, its next step is to define the values that will guide its everyday activities. You can think of these as a corporation's institutional standards of behavior — but what are values exactly?

Values are articulated priorities that guide the behavior of employees and which benefit the company as a whole. Timberland took a straightforward approach to its values by defining them as "Humanity, Humility, Integrity and Excellence."

These three aspects are essential to every company, and they can make a huge difference on the path to gaining customers, employees and shareholders.

### 6. Define a mission that transforms people’s lives and tell it with a gripping story. 

Letting customers know that your company works for great things is vital, and the tool to get you there is a powerful mission that is well communicated. Here's how to make it happen:

First of all, your mission should involve presenting something that transforms your customers' lives. To that end, it's essential that companies work hard to produce innovative ideas that can truly make a difference. 

For instance, Google set out to make all the information in the world organized and accessible. Their path to doing so was a continuous reinvention of the concept of a search engine from 1998 onward. In the process, they earned themselves a spot in the dictionary, becoming synonymous with the act of searching for information on the internet. 

Google radically transformed the way people find information, and that's what Marketing 3.0 is all about — improving the lives of consumers. 

But having a mission isn't enough; it's also important to build a compelling narrative around it. For that, you need a _brand story_ composed of three elements: character, plot and metaphor.

First, a brand becomes a character when it represents something good valued by society. An example would be Disney, which represents family ideals.

Then, to make that character relevant to the lives of customers, it's necessary to have a captivating plot. One strategy is to use a _challenge plot_, a story in which your brand battles a stronger opponent and wins.

For example, The Body Shop tells the story of farmers in developing countries who are engaged in the struggle for fair trade. 

But to really get your narrative talking, you'll need to employ some of the metaphors that always catch people's attention, like balance, connection and control. 

Take control, for example; people are drawn to it because it helps reduce fear. A person might be scared of pandemics, a fear that pharmaceutical companies can address by describing how the human immune system can deal with a dangerous situation on its own — just as long as you have been vaccinated.

### 7. Authentic values help you attract stellar employees who will happily deliver your values to customers. 

Imagine your family is spending the weekend at Disneyland. Your kids desperately seek out Goofy, their favorite character, but are sorely disappointed to find him, or rather the actor who plays him, to be gruff and rude. What happened? Isn't Disney supposed to be the most fun and kid-friendly place on Earth?

This is a great example of how corporate values only appear to be authentic if they're shared by employees and reflected in the corporate culture. If your company claims to hold a certain value, like being family friendly, but doesn't reflect it through corporate practices, your employees and customers are sure to notice.

For instance, a company that gives money to family support projects would look pretty hypocritical if a former employee told the press that she was fired after becoming pregnant. 

So, to build a great corporate culture, you first need to compare your corporate policies with your company's core values and address any discrepancies. Then, you need to identify practices that connect your values with concrete actions. 

Why?

Because companies with strong values find it easier to attract talented and motivated employees, and hold a competitive advantage when searching for talent. Furthermore, employees that work for a company they identify with are much more motivated and therefore more productive. 

In fact, employees who live by the values of their companies make fantastic brand ambassadors. Whenever employees interact with customers, they build the brand's public image. Therefore, the more they represent corporate values, the better. 

For example, the American grocery store chain Wegman's says it knows food better than anyone. In support of that image, every employee who interacts with customers is required to be extremely knowledgeable about food. To ensure this, the chain offers extensive food-related education programs to everyone that works there.

> _"Good values attract good people."_

### 8. There are economically sound reasons to adopt a sustainable vision and convince your shareholders of it. 

It can be tempting to go for short-term profits to satisfy your shareholders, but you're much better off resisting the urge. After all, short-sighted management is harmful.

Short-sighted management decisions damage your company's long-term prospects, along with its stakeholders, who are the people your company affects, like its employees, neighbors and the general public. 

This reality became abundantly clear during the last major financial crisis and the collapse of banks like Lehman Brothers. In fact, the entire economy tanked because companies were opting for risky tactics that brought short-term profits. 

A better strategy is to build a sustainable vision for your company and convince your shareholders that it works. In the end, a company's value is mostly determined by its future performance and that performance is based on its corporate vision. Therefore, your vision needs to address sustainability. 

For instance, as resources grow scarcer, they become more expensive. So, the only companies capable of thriving in the future will be those capable of accessing a sustainable supply of resources, such as by investing in a solar energy station. 

But once you've built a sustainable vision, it's essential to convince your shareholders of it; for them, there are three advantages of sustainability that will be the most persuasive. Above all, shareholders care about income, and you're best off showing them the three ways that sustainable production increases revenue:

First, it reduces costs by consuming smaller quantities of expensive resources like gas, while producing less waste. In turn, this eliminates the need for high-cost waste removal. 

Second, there's a huge demand for ecologically-responsible products. In fact, according to _Forrester Research_, 73 percent of customers find environmentally-conscious brands desirable. Naturally, higher demand — and more customers as a result — means higher revenues and lower advertising costs. 

Third, sustainable practices improve a company's reputation and brand reputation can have a clear financial impact.

### 9. Final summary 

The key message in this book:

**Consumers have changed; they're no longer passive receivers of entertainment and information. The modern consumer interacts, shares her opinions and has sophisticated needs that every company must take into account. The changing tides of consumption warrant a new approach to marketing: Marketing 3.0.**

Actionable advice:

**Instead of simply donating money, link your product to a valiant social cause.**

A great strategy to prove the ethical responsibility of your company is to link a social cause directly to your product, a concept called _cause marketin_ g. While you may think that donating money to a good charity is enough of a sign of goodwill, cause marketing proves even more effective because it demonstrates the energy you've committed to a project. For instance, _Whole Foods_ ran a campaign in which they gave every customer a token to insert into a box labeled with the local charity of their choice. All the tokens represented cash that would then be donated to each of the organizations.

**Suggested** **further** **reading:** ** _Permission Marketing_** **by Seth Godin**

_Permission Marketing_ confronts the conflicts and challenges that modern marketers face in the digital age and offers a viable alternative. It explains how the advertising landscape is filling up and how this makes traditional advertising ineffective. The author suggests that smart marketers no longer simply interrupt consumers but invite them to volunteer their time and become active participants in the _marketing process._

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Philip Kotler, Hermawan Kartajaya and Iwan Setiawan

Professor Philip Kotler is a former director of the American Marketing Association and is known as one of modern marketing's founding fathers. He teaches international marketing at the Kellogg School of Management at Northwestern University _._

Hermawan Kartajaya is president of the World Marketing Association.. He has published five books with Philip Kotler and is the founder of MarkPlus Inc., a marketing consulting agency active in Southeast Asia.

Iwan Setiawan is Chief Knowledge Officer at MarkPlus, Inc.

Philip Kotler, Hermawan Kartajaya, Iwan Setiawan: Marketing 3.0 copyright 2010, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

