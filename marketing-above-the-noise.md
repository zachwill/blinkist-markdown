---
id: 55ac2b8a6536380007000000
slug: marketing-above-the-noise-en
published_date: 2015-07-20T16:00:00.000+00:00
author: Linda J. Popky
title: Marketing Above the Noise
subtitle: Achieve Strategic Advantage with Marketing that Matters
main_color: 2890C6
text_color: 1E6C94
---

# Marketing Above the Noise

_Achieve Strategic Advantage with Marketing that Matters_

**Linda J. Popky**

_Marketing Above The Noise_ (2015) guides you through today's marketing world, helping you separate useful advice from useless noise. Advising against jumping on the bandwagon and following all those hot new marketing trends, these blinks demonstrate that tried and true approaches to marketing are the best way to win over — and hold onto — customers.

---
### 1. What’s in it for me? Win more customers by cutting through the noise. 

Almost everything you read on marketing these days is about using new technology, following new trends and employing new approaches. More often than not, however, newfangled techniques are merely a quick fix — and the underlying issue persists. Plus, what about your poor customers, constantly bombarded online by ads and pop-ups?

Over the course of her 30-year career in marketing, Linda Popky has seen many a trend come and go. She's also learned that contributing to the noise of the marketplace — or trying to yell above it — isn't a good way to win customers. Instead, it's better to take what's useful from the new trends while focusing mainly on the classic, timeless marketing techniques that keep your focus on what's most important: convincing customers to trust you with their business.

That's why she made her Dynamic Market Leverage model, which covers all the bases of running a marketing organization with long-term success. These blinks go through every part of it, from strategy to market analysis.

In these blinks, you'll learn

  * why your employees are as important as your customers;

  * what was wrong with Obamacare's launch; and

  * how to avoid the mistakes made by JCPenney and Macy's.

### 2. Marketing noise is everywhere, but you can cut through it and reach your customers. 

These days, every organization gets sucked into the latest marketing trends. But all these flash-in-the-pan ideas have resulted in levels of marketing noise more deafening than ever before — noise that most consumers do their best to ignore. So how can you separate yourself from the pack and really get through to customers?

First, you must understand the two kinds of noise.

Marketplace noise is what customers get bombarded with. This includes messaging from your competitors — usually poorly written, confusing messages — and the ads that pop up on email and social media.

The other kind of noise is within your organization. It's what you have to work against, even when you have a brilliant marketing plan with a strong message.

But how do you get above the noise? The best way to start is by creating a positive sound.

In conversation, you can only avoid noise in two ways: talk louder than the other person or be completely silent. In marketing, if you go for the former, customers will get annoyed with you, and if you opt for the latter, they'll ignore you.

But in the marketing world, there's also a third way: you can create something positive that people want to be around.

To do so, you need to understand your environment. What are people discussing, and why?

You also need to study the concepts that have endured over the years. Chuck out any short-lived trends and go for what has been proven to work.

Lastly, integrate these ideas within your organization.

The result of all this is what the author captures in her _Dynamic Market Leverage_ model, which focuses on long-term strategies that get through to customers and go beyond fads and new technology. It encompasses everything, including strategy, products, customers, brand, communication, operations, sales channels and market analysis.

The _Dynamic Market_ is not about the hottest marketing trends. It simply focuses on convincing people to trust your organization.

### 3. Build a strong foundation by setting the right marketing strategy. 

The cornerstone of a great marketing campaign is strategy; if you don't know where it's going, how can you hope to market your company?

And if you want to build a stable foundation, you must define and prioritize your goals and establish how to present and address yourself to the customer.

Your strategy will orient you and help you reach your goal. Imagine ascending a mountain and suddenly realizing that you're climbing the wrong one; jumping from peak to peak just isn't an option.

Marketing is similar. You need to plan where you want to go and why you want to go there, because once you've reached the summit, you can't switch mountains. Ask yourself, "What do I want to get from the results of my marketing efforts?" And make sure you're specific. For example, are you focusing on revenue growth or on attracting new customers?

The crucial step in creating the right strategy is understanding the _market environment_ — that is, the product, place, customer, price, message and media.

But first off you need a _quality product_ — something that draws your audience in.

Next is the old adage — right place, right time. Ask yourself how this applies to your product. Is it readily and easily available to your customers, for instance?

You also have to know _who_ your customer is. Things like how often she buys your products.

Then you need to figure out what to charge for your products; price really does matter. Nobody's forking out $50 for hairpins. So, are your prices fair as well as competitive?

In addition to the above, you'll need the right _message_ promoting your product. What you're selling should stick in peoples' minds, and make you stand out from your competitors. Ask yourself why your customers should keep you in mind? Why is your product the best choice around?

Last but not least, tell people what you're selling! There are myriad ways to do that.

### 4. From the beginning to the end of your design process, stay in contact with your customers. 

While some marketing strategies haven't changed for years, others are constantly evolving. One major change is that marketers need to engage with and respond to customers quickly and effectively.

Being receptive to customer feedback is the best way to do this. The customers' opinions and desires should matter, not only when determining their initial requirements, but also during all stages of design and manufacturing. This will ensure that the final product meets their needs. 

Take the launch of the website for the Affordable Care Act (also known as Obamacare). It was a mess: links were broken, many parts of the site didn't work, and sometimes you couldn't even access it!

The team responsible for marketing the website probably didn't pay much attention to how intimately it was connected to the entire service. They failed to realize that, for the customer, the experience of Obamacare actually began with the website.

Many of these problems could have been avoided if the marketers had integrated customer feedback into the site itself. An assessment of whether it was ready to launch, along with some beta testing, also wouldn't have hurt!

One of the keys to obtaining feedback is being unafraid to ask. Many customers have interesting and useful things to say, and your job is to engage in conversations with them, as you would with your friends or colleagues. So, pick a topic of interest, pose a question about it and then listen to the response.

Then follow a three-step process on the feedback.

Start by _acknowledging_ the feedback you have received. Then _accept_ that the feedback contains important input and information. Finally, clearly outline how you plan to incorporate it — by sharing it with others in the organization, researching it thoroughly and making any necessary adjustments.

This way you'll ensure your customers' opinions and requirements are a core part of your organization.

### 5. Solicit customer input and get to know them. 

Sometimes merely welcoming customer feedback isn't enough to ensure that your customer's voice really gets taken into account in your organization. Sometimes you also need to make room for customer complaints.

No one loves hearing complaints. But a customer who complains often cares enough about your organization to stay engaged, even during rough patches. The great thing about people who complain is that they tend to have faith in you and your company and, by complaining, give you a chance to change things and make it up to them. Silent, disgruntled customers don't help at all; they simply disappear and there's nothing you can do about it.

So how can you make complaining a smooth process? Try offering a platform where customers can reach you easily, then demonstrate that you care about them by acting quickly on any complaints.

Customers want to be heard and they also expect you to know them well enough to modify your products and services according to their changing needs. 

It's also essential to really know who your customer is. If you are unable to define your customers, your marketing message will fail to reach the right audience and you will squander your effort and money on marketing for nothing.

The author is a long-time Macy's customer, so when she found out that Macy's planned to start mailing her personally-tailored catalogues based on her unique buying habits, she was eager to see what they might recommend.

But the result was disappointing. The only tailored thing about the catalogue was that it had her name printed on it! In fact, the catalogue was packed with products she had no interest in, like baby clothes, furniture and menswear.

Don't make the same mistake. The more accurately you can target the personality of your customers, the better you'll be able to cut through the noise and bring customers in.

### 6. Build and protect your brand. 

If you've ever turned your hand to marketing, you'll know that branding isn't just about a nice-looking logo or a catchy slogan. Everyone and everything has a reputation — things almost entirely created by branding.

Building a reputation takes time; destroying it, on the other hand, can happen very quickly indeed.

In 2011, JCPenney, a 109-year-old retailer, strove to overhaul its business strategy, and so it enlisted Apple's retail strategist, Ron Johnson. JCPenney changed almost everything — the logo, the products — and they even canceled sales and promotions and introduced higher-end products.

Unfortunately, this didn't go down so well with JCPenney's best customers; middle-class moms were shocked when they discovered that many of the brands they'd known and loved had disappeared. As a result, JCPenney lost its foothold in the market, along with billions of dollars and the loyalty of its customers.

The lesson here is that tactics that made some organizations successful — like Apple, in this case — may not work for you. JCPenney should've stuck with the image and reputation they had taken more than 100 years to build.

As well as being true to your brand, it's important to act in a way that will actually reinforce your brand identity.

Whole Foods Market, for instance, chose to label and eventually withdraw all GMOs from its shelves by 2018, since this no-GMO policy is consistent with Whole Foods' image as a vendor of natural products.

Another great example is CVS, the second largest pharmacy chain in the US. In 2014, they decided to cease selling tobacco products, thereby sacrificing nearly two billion dollars in annual revenue. Why would they do this? CVS couldn't reconcile the sale of tobacco with the health and wellness reputation it aims to uphold.

### 7. Develop the right tools and tactics to reach your customers. 

In the previous blink we saw that attracting new customers and keeping current ones is a matter of communicating in such a way that you can be heard above the noise. But how exactly do you do that?

You must create and curate content that interests your target customer.

Zillow.com, the leading US home and real estate marketplace for web and mobile, knew how to do this. When it launched in 2006, the real estate market had crashed six months beforehand. The company faced a formidable challenge.

They had no funds for advertising, so they decided to take a word-of-mouth approach. Chief Marketing Officer Amy Bohutinsky saw how this could work; she had noticed that no one was offering real estate data in an unbiased way. So Zillow started to create housing reports. Soon academics, economists, government housing groups and journalists were all citing Zillow's data.

Today, Zillow generates two to three housing reports per week for 350 cities across America.

Bohutinsky says that so many people know and use zillow.com because it's such a trusted content provider. This, in turn, creates demand and has helped it become a market leader.

As well as focusing on quality content, you can also amass new customers by offering them something for free.

These days, many companies offer either a free trial or provide a basic service for free, and then set a fee for the full product. LinkedIn, for example, offers both free and paid versions of the network; creating profiles costs nothing, while additional functions, such as using their database to find the right job candidates, has a fee attached to it. Their free version is immensely valuable, however, because the quantity of people using the free version is precisely what convinces people like sales reps or marketers to pay for the premium version.

So, if you decide to provide a free trial, remember that it must last long enough for customers to start using the product regularly and integrate it into their lives.

### 8. Get your sales and marketing teams to work together. 

Those in the industry know that marketing can be tricky when sales are thrown into the mix. In fact, it's not unusual for marketing and sales teams to rub each other the wrong way, even though they have similar goals. Sales reps think marketing people don't understand what it's like to be out there every day bearing the brunt of disgruntled customers' complaints; and marketing people are frustrated that salespeople fail to see how much they help them.

But there are organizations that have managed to move beyond this mindset and have changed the way marketing and sales deal with each other.

A great idea is to put marketing people on sales teams and have them participate in sales calls; this would show them how customers behave in real-life situations, giving the marketing team a better idea of what the sales team needs. In return, the sales team will value the efforts of the marketing team. Similarly, to help salespeople understand how marketing initiatives are created, you can get them involved in the marketing process.

Another way to get marketing and sales teams working well together is for marketers to understand that marketing can't just use rigid, old concepts. It has to help win sales.

Cold call sales are far less frequent today than they used to be. Most of the leads with whom your salespeople get in touch already know, from the internet or elsewhere, what your company does and what they want or need. So marketers must give sales reps the tools to thrive in these situations. They have to continually communicate with the sales team so that they understand the marketing team's message and the positioning of your organization. Keeping them up-to-date on competitors' activities, for example, can help them close a sale with an uncertain prospect.

Harmonizing the sales and marketing teams greatly reduces the internal noise that often thwarts your marketing efforts just as much as external noise in the marketplace.

### 9. Learn how to use customer and market data to get a competitive advantage. 

Today, and across all industries, _big data_ is one of the buzzwords most often bandied about. Companies are collecting and analyzing data using billions of transactions to learn as much as they can about customers and the market. But leveraging such information to understand your customers requires that you know how to use and filter it.

Amazon knows how to do this well. For example, the author's refrigerator filter needs to be replaced every six months. So when she ordered it on Amazon, she chose the automatic shipment option, and now receives a new filter every six months without having to manually place the order.

Making sure she gets her refrigerator filter is just one of the many ways Amazon uses its database to offer better services. It's now also considering an algorithm that predicts the item customers are likely to order next and deliver it to them before they actually indicate interest in it!

Here's the key to data: it's how you use it rather than the quantity you have. To be heard above the rest, marketers need to be able to understand _and_ interpret data.

When Craig Davison, previously head of marketing for Microsoft's Xbox games, moved online to Xbox Live, he began gathering data in order to ascertain what exactly convinced people to become paying subscribers. But he got bogged down in data — and none of the key performance indicators gave rise to advertising that led to more paying customers.

They needed to get in the minds of the actual gamers, not just trawl the numbers. By doing so, they learned that selling Xbox Live subscriptions via its dashboard menu was a far more effective route to take. They also learned that hardcore gamers despise advertising!

Turning their efforts away from external marketing through social media and emails, they instead focused on offering products within the games.

### 10. Your employees are as important to your organization as your customers. 

Sometimes the person responsible for sealing the deal is a shop employee who was friendly and held the door open for a customer. Remember: employees can be your most effective marketers — or a marketing nightmare!

Unhappy employees lead to unhappy customers, but happy, obliging employees can make a markedly positive impression on customers.

Author Shawna Suckow once exited a Southwest Airlines flight without her laptop — which contained her new manuscript. By the time she realized she'd left it behind, the plane was closed to passengers and gate crew. However, one Southwest employee took it upon himself to re-enter the empty plane and retrieve her laptop. Even though the Southwest employee wasn't even in customer service — his job was to service the plane — he helped her out. Grateful for the favor, the author related the incident in an industry publication and also went back to thank the employee.

Many companies pour hundreds of thousands of dollars into marketing campaigns. But few take the time to inform the organization from top to bottom about company culture and to encourage employees to feel they're involved in it, too. Remember: employees are the voice and face of your business. So it pays to ensure that they are exposed to (and can embody!) the key tenets of your brand. 

Employees can also be a wonderful source of ideas.

The playfully branded coffee company, Caribou Coffee, once asked their employees for an April Fools' idea that wouldn't cost much. 20 hours after the vice president of marketing sent out the question via email, she was flooded with ideas. Not only did Caribou Coffee find an idea for April Fools' Day — Caribou's Clear Coffee — they also collected many other great ideas that could be used for other occasions. People are often excited to participate and you never know where the next good ideas might come from.

### 11. Final summary 

The key message in this book:

**Many companies are too gullible when it comes to the hottest marketing tactics, even when these tactics clearly fail to help them! Instead, organizations and marketers should concentrate on longer-term strategies that have proved effective and will convince people to become customers and stay loyal to the company.**

Actionable advice:

**Learn to love complaints.**

It's not exactly fun to get an earful over the phone from a disgruntled customer, but sometimes they can spark a change in the company that helps to move it forward. You also get the chance to impress such customers by going above the expected service level, which will help you retain customers and bring in more through word of mouth about your second-to-none service.

**Suggested further reading:** ** _Guerrilla Marketing_** **by Jay Conrad Levinson**

Originally published in 1984, _Guerrilla Marketing_ is a classic must-read for marketers. It explains how small firms with limited marketing budgets can still compete with bigger competitors by getting creative.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Linda J. Popky

Linda Popky, the president of Women in Consulting (WIC), is a marketing consultant, speaker and author. With over 30 years' experience in marketing, she's worked with companies ranging from nonprofits to Fortune 500 companies such as PayPal and Sun Microsystems.

