---
id: 56b8e60dd119f900070000a2
slug: marriage-a-history-en
published_date: 2016-02-11T00:00:00.000+00:00
author: Stephanie Coontz
title: Marriage, a History
subtitle: How Love Conquered Marriage
main_color: BF2C3F
text_color: BF2C3F
---

# Marriage, a History

_How Love Conquered Marriage_

**Stephanie Coontz**

_Marriage, a History_ (2005) covers the history of the institution of marriage, from its genesis in the Stone Age to its recent crisis.

---
### 1. What’s in it for me? Immerse yourself in the surprising history of marriage. 

We're surrounded by conservatives lamenting the death of the good old traditional family with a breadwinning father, a stay-at-home mother and a brood of happy, healthy children (who'd never dream of questioning gender roles, of course). But how much of a "tradition" is this, really? Through the ages, marriage has actually served many different purposes. For instance, did you know that marriage as a bond of love is a rather new concept? The history of marriage is a strange and fascinating story — one that we should all be more familiar with.

In these blinks, you'll also learn

  * why our cave-dwelling ancestors invented marriage;

  * that people once _wanted_ more in-laws; and

  * how marrying for love caused hysteria.

### 2. Marriage was initially a way of establishing kinship. 

Imagine marrying someone you don't truly love. A frightening thought, right? Indeed, most of us would say that love must exist before marriage can take place. 

Historically, this is a rather recent development. In fact, for thousands of years, love had little to do with marriage.

In medieval Europe, for example, when people talked about "love," they weren't talking about married couples. Rather, "love" was something reserved for God, kin or neighbors.

And in India, falling in love was once even considered dangerous. Romantic love was viewed as antisocial, an emotion that could lead people to make irrational decisions and challenge the authority of the family.

So if marriage wasn't about mutual love between two individuals, what was it about? Why invent marriage at all?

The answer may sound strange to modern ears: marriage was a means of establishing kinship. But why was this something that people wanted to do in the first place?

Well, our hunter-gatherer ancestors were nomadic travelers, constantly searching for food. In this search, they would sometimes stumble upon strange and potentially hostile groups and a fight would break out.

Such fights could be avoided, however, if the bands made peace instead of threatening one another. They figured that one of the easiest ways to establish a reliable connection between the bands was to marry a member of one group to a member of the other. 

Through intermarriage, complete strangers (and even enemies) could be transformed into relatives, thus establishing kinships that helped guarantee peace. With this in mind, it makes sense that the old Anglo-Saxon word for "wife" meant "peace weaver."

### 3. Marriage was a crucial tool for extending economic and political power. 

For some people, the many benefits of marriage come with a serious drawback: in-laws. For our ancient ancestors, however, new in-laws were worth celebrating. 

In fact, the goal of growing the family through in-laws essentially gave rise to the institution of marriage. After all, more in-laws mean more people to trade with, and so groups with family ties were more likely to prosper economically and thus more likely to survive.

We see this principle exemplified by an odd practice of the Bella Coola and Kwakiutl societies, both located in the Pacific Northwest. When these two groups wanted to trade with each other but couldn't find a good match for a son or daughter, they would simply establish a marriage contract between a member of one group and anything belonging to the other group — a dog, for instance, or a person's foot. This way, they could acquire new in-laws (as in business partners) even if there wasn't anyone to marry off.

Up through medieval times, marriage also served a political purpose. Like the previous peacemaking aspects of marriage in prehistory, marriage during this time was also a way of formalizing peace treaties and military alliances. But it was also a tool for extending power.

You've probably heard of the love between Mark Antony, the Roman general, and Cleopatra, queen of Egypt. Though this couple has been the subject of many fictional works, such as Shakespeare's _Antony and Cleopatra_, their marriage in fact had little to do with true love.

While the two of them might have shared some mutual attraction, the main motivation for their relationship was political: Antony wanted money to finance his expensive military campaigns, and Cleopatra hoped to gain political power in Egypt with an influential Roman by her side.

Love was always considered a side effect of marriage — not the main reason for an engagement. In our following blinks, we'll look at how changing attitudes fundamentally changed the marriage institution.

> _"For centuries, marriage did much of the work that markets and governments do today."_

### 4. The Enlightenment and wage labor made love the main reason for marriage. 

For thousands of years, parents arranged marriages based on their family's social and economic interest. Strategic marriage was so crucial for a family's success that the very notion of letting the children decide whom (not to mention if!) they wanted to marry was simply unthinkable, especially if that choice was based on something as fleeting and irrational as love.

It wasn't until the late eighteenth century that major economic and cultural changes, most notably the Enlightenment and the emergence of wage labor, freed the individual from the dictates of their family. 

The Enlightenment spread radical ideas about the individual's right of self-determination throughout the Western world, and fundamentally changed how people viewed the marriage institution.

For example, the French Revolution and the American Declaration of Independence legitimized the idea that people are free to shape their personal lives and social relationships as they see fit. For young people, this meant freedom to choose their own spouse, rather than following the dictates of their parents. 

Suddenly, and for the first time in the history of humanity, love became a legitimate reason for marriage.

This shift in perspective regarding personal liberty was accompanied by huge economic upheavals: the emergence of the market economy and wage labor, which enabled young people to set up their own household _independent of their family_ — and to marry for love.

Before the existence of wage labor, people were economically dependent on family wealth and inheritance. Being the ones who put food in your belly and a roof over your head, parents also felt entitled to decide who your future spouse should be. 

With the emergence of wage labor, however, people (mostly men) could make their own money, move to the city and set up their own household with their beloved, with or without parental consent.

### 5. In the Victorian era, marriage became the locus of emotional fulfillment and sexual frustration. 

After a long history of arranged relationships, love-based marriage was uncharted territory for nineteenth-century Western societies. Consequently, it was some time before people began expecting from marriage what we now assume it should provide: emotional and sexual fulfillment.

For millennia, marriage had been a rational — not an emotional — undertaking. But, during the Victorian era (1837-1901), more and more people started expecting romantic love and personal fulfillment from matrimony.

During that time, marriage was viewed as the primary source of happiness and mutual intimacy. Suddenly, people found a deep, almost religious meaning in marriage. Spouses would write each other notes that said things like "Thou art my church and thou my book of psalms," as Annie Fields did, in 1863.

But while people of the Victorian era had become more open-minded about emotional fulfillment in marriage, sex was another story altogether. Back then, trying to find sexual fulfillment in marriage was a fool's errand.

At the time, women were supposed to be pure, moral beings, devoid of sexual appetite, a stereotype responsible for widespread sexual repression. Men, on the other hand, were thought to be lusty creatures, but cultural norms enjoined them to restrain their sexual impulses.

Of course, marriages were consummated, but marital sex was strictly regulated. Men were advised not to have sex more than once a month — that should suffice. As a consequence, prostitution boomed.

Naturally, the vast majority of women, married or otherwise, wanted to have sex, too. Barred by society from achieving relief, these women suffered severe sexual frustration, which, at the time, was called "hysteria." Physicians would treat these women by massaging their pelvic area, often bringing them to climax. 

This treatment was so popular that demand for it became too great. As a result, the mechanical vibrator was invented, allowing women to treat themselves at home.

### 6. After World War II, the male breadwinner marriage reached its Golden Age. 

When conservatives talk about the "good ol' days" of marriage, they're likely referring to the 1950s and '60s.

Indeed, the 1950s and early '60s were a special period in the history of marriage (at least in the Western world): never before in the twentieth century had so many people married so young.

After Word War II, young people wanted to start a family with their beloved as soon as possible. In 1959, almost half of all 19-year-old female Americans were married. And, in that same year, 70 percent of women under the age of 25 were married.

In fact, marrying young was so widespread that women as young as 21 began panicking, worried that they'd never find a husband and end up as an "old maid."

Marriage was so common that _confirmed singles_ (unmarried people who don't date or intend to get married) were frowned upon. Indeed, a 1957 survey found that 80 percent of Americans considered confirmed bachelors to be "sick," "neurotic" or "immoral."

Post-war marriage was characterized by the gendered division of labor we now consider "traditional": the husband as breadwinner, the wife as homemaker. Prior to the '50s, however, such a stark role division was quite rare.

For centuries, husbands, wives and children all worked together at home to earn a living. While it's true that the emergence of wage labor created the expectation that men should get a paid job while their wives handled the household and child-rearing, the reality was much different.

Men often didn't earn enough to feed the whole family, and so their wives got jobs, too. Moreover, men typically under-reported their actual share of household work and childcare, as they didn't want to admit to doing "woman's work."

It wasn't until the economic boom and rapid wage increase of the 1950s that single-earner families became the norm and a working wife the exception. 

At this point in the history of marriage, people believed they had finally found the ideal family model. But this was not the case at all.

> _"The cultural consensus that everyone should marry and form a male breadwinner family … crushed every alternative view."_

### 7. In the 1970s people had more freedom to choose a way of life, and marriage began to falter. 

Soon after the marriage frenzy reached its peak, it took a rapid downward turn. Throughout the 1970s, the number of marriages fell: people began marrying later in life, out-of-wedlock births increased and divorce rates rose steadily. During this decade, every second marriage in the US ended in divorce.

But why?

Strangely, the primary reason for the crumbling of love-based marriage was its core conviction — that love (and only love) can make a marriage! While love as a foundation for marriage can certainly make it more rewarding, it can also make it more fragile. 

When marriage is based on love, it's bound to end if that love withers away. Clearly, romantic love is much more volatile than the simple necessity and sense of commitment that once characterized marriage. 

But love-based marriage had been around far before the 1970s, so why didn't this crisis of marriage happen earlier?

Prior to the '70s, people weren't yet free to marry and divorce as they pleased, as huge practical — that is, financial — and cultural constraints stood in the way. The '70s changed all that.

For instance, women in particular rarely filed for divorce because they were economically dependent on their husbands. In the '70s, however, more and more women got college degrees, joined the labor force and, in turn, were able to earn more money. Consequently, they became economically self-sufficient and could divorce their husbands without descending into extreme poverty.

Moreover, a sharp shift in values also contributed to the decline of marriage. New generations questioned traditional gender roles, and refused to get married as early as their parents had. Instead, they sought to use that time for self-expression.

> _"...for thousands of years, people have been proclaiming a crisis in marriage and pointing backwards to better days."_

### 8. Though modern marriage is changing rapidly, it’s not doomed. 

Take a second to think about all the young married couples you know: it should be more than obvious that they differ significantly from the couples of previous generations! 

Today, the division between male-as-breadwinner and female-as-homemaker is no longer the cultural norm. It's much more common for both partners to be earners.

Women nowadays are less willing to give up career aspirations and stay at home, and they sometimes even surpass their husbands in terms of income. The numbers from 2001, for example, show that 30 percent of all working wives bring home more money than their husbands do.

Additionally, the legal privilege of marriage is being continuously challenged by alternative — and legally recognized — partnership arrangements.

For example, in France and Canada anyone can establish a legally acknowledged relationship in which both partners share responsibility for a child or simply pool their resources. Such relationships can be with a partner, a sibling or just a best friend. Members of these relationships enjoy many of the benefits married couples receive.

But despite the many alternatives, marriage still represents a bedrock institution. Marriage, first dismantled by and then rebuilt upon love, has become more fulfilling than ever. According to many studies, married individuals are, on average, both happier and healthier than unmarried people. Moreover, their marriage works as a sort of protection against financial and psychological threats, as married people can better cope with financial setbacks and depression.

So, though marriage has undergone numerous fundamental changes throughout its history, it's nevertheless here for the long haul. In fact, demographers predict that nine out of ten Americans will indeed get married... eventually.

### 9. Final summary 

The key message in this book:

**The institution of marriage has undergone lots of changes throughout history. From its beginnings as a means of keeping groups of hunter-gatherers from killing each other, marriage has come a long, long way and is now more fulfilling — and fragile — than ever.**

Actionable advice:

**Base your marriage on mutual respect and accept differences.**

There's no magic formula for the perfect marriage, but psychologists and sociologists have found a few things that are crucial for a flourishing marital relationship. The most important one is mutual respect and friendship. You can't push your partner to be something they're not.

**Suggested** **further** **reading:** ** _Why We Love_** **by Helen Fisher**

Helen Fisher's _Why_ _We_ _Love_ is not only a report on her latest astonishing research but a sensitive description of the infinite facets of romantic love. This book is a scientifically grounded examination of love that reveals how, why and who we love.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Stephanie Coontz

Stephanie Coontz is an author, social historian and professor at Evergreen State College. She also serves as the Director of Research and Public Education for the Council on Contemporary Families, and has published several other books on family issues and gender, including _The Way We Never Were_.

