---
id: 5a71be6fb238e100076ea9d4
slug: martin-luther-en
published_date: 2018-02-02T00:00:00.000+00:00
author: Heinz Schilling
title: Martin Luther
subtitle: Rebel in an Age of Upheaval
main_color: C37548
text_color: 8F5635
---

# Martin Luther

_Rebel in an Age of Upheaval_

**Heinz Schilling**

_Martin Luther_ (2017) details the life and times of one of the most important figures in European history. Five hundred years ago, the German priest Martin Luther sparked the Reformation, which would eventually bring the Protestant Church into existence and thereby create a permanent schism between the Protestant and the Roman Catholic Church. This book presents a fresh interpretation of Luther's life, investigating what his actions — and the Reformation in general — tell us about the modern world.

---
### 1. What’s in it for me? Discover how the Martin Luther forever changed the course of European history. 

Anyone who's traveled around Europe, and Germany in particular, is sure to have seen town squares adorned with statues of and monuments dedicated to Martin Luther.

And with good reason. Martin Luther's single-mindedness and desire to reform the Roman Catholic Church was a key first step toward the establishment of modern European culture; the Reformation he set in motion ushered in a new era of politics and thought.

It may seem fairly abstract to interpret the history of an entire continent through the actions of one monk, but a look at Luther's life will show just how influential religion and religious practice was in his time. Moreover, you'll see how economics, politics, belief, philosophy and religion were so closely intertwined, that criticism of a few seemingly academic religious matters caused the whole mass to unravel.

Understand Luther, and you'll understand what makes Europe and the Western world what it is today.

In these blinks, you'll find out

  * where the Protestant Church gots its name;

  * how Luther is linked to some of the worst atrocities of the twentieth century; and

  * what pun caused Luther to change his name.

### 2. Martin Luther was born at the end of the Middle Ages, a time of great societal turmoil. 

When looking at the modern world, it may seem that things have never changed at such a great speed as they are doing today. In reality, though, something quite similar was taking place some 500 years ago; the fifteenth and sixteenth centuries were characterized by radical societal, geopolitical and economic upheaval.

Europe was threatened from the east by the constant specter of Ottoman invasion. Meanwhile, to the west, Spanish and Portuguese vessels had taken to the seas and were in the process of "discovering" the New World.

European contact with the Americas was soon followed by the establishment of colonial trading routes. These brought an unprecedented influx of gold and silver from American mines to Europe, and these newfound riches laid the foundations for a new capital-oriented society in Europe.

In turn, a new and wealthy middle class of merchants began to emerge, and they were all too willing to challenge the old aristocratic order.

While all this was going on, the population of Europe was booming. It's estimated that Europe's population increased from 82 to 107 million over the course of the fifteenth century.

In the midst of all this change and upheaval, on November 10, 1483, Martin Luder — or Martin Luther as he would come to be known — was born to Hans, a mining entrepreneur, and Margarete in Eisleben, Germany. It was a comfortable household with domestic servants, but the family still lived modestly and avoided luxuries.

Luther, as his family's first-born child, was expected to safeguard the family's middle-class status and was thus sent to study law. Then, as now, a legal career was seen as a secure choice.

But, as we now know, Luther didn't follow his designated course — and his change of heart would affect not just his own life, but the course of history itself.

### 3. Luther entered a reformist monastery and was taught to criticize the Church in Rome. 

Young Luther's mind was restless. As he began his legal studies, he was troubled: what point was there examining the laws of the earthly realm, when it was more important to think about the path to righteousness and God's kingdom in Heaven?

Luther turned his back on the law before completing his first semester.

According to Luther's own account, his doubts on the matter were resolved in July 1505 when he was struck by lightning. As he lay on the ground, he swore to St. Anne that he would become a monk if he survived.

It was a formative experience: within two weeks he had renounced his possessions said his farewells, and headed to the nearby town of Erfurt to enter St. Augustine's Monastery.

There, the daily devotions were designed to engineer a path to salvation. They comprised direct acts of faith, such as prayer, repentance and self-flagellation.

Luther followed the prescribed routine, but he could never shake the agonizing doubts about his own salvation. Nevertheless, Luther was ordained as a priest in 1507. Soon thereafter, he began studying theology in Wittenberg, and by 1508 he was considered knowledgeable enough to work as a temporary substitute professor. By 1512 he had received his doctorate.

But it wasn't just the University of Wittenberg that educated and influenced Luther.

The Order of St. Augustine was among several orders that sought to reform the Roman Catholic Church. The reformers were especially angered by the Church's unnecessary pageantry and by the hot-button issue of the day: the selling of indulgences. This was the practice whereby people could pay to have their sins officially forgiven, thus easing their passage to Heaven.

Luther himself was even sent to Rome as a delegate of the Order of St. Augustine when they were demanding greater independence. He returned empty-handed, but this experience left a profound impression on him.

### 4. As a Bible teacher in Wittenberg, Luther developed the theological foundations for the Reformation. 

Luther's mission to Rome had met with no success. But upon his return, he buried himself in Bible study at the University of Wittenberg. The institution was in many ways the perfect place to conduct such research; as the reformist Order of St. Augustine had cofounded the university, it meant that it was far more liberal in its approach to Church doctrine than traditional universities.

This atmosphere allowed for unconventional approaches to teaching. Luther, as a theology professor, became fully immersed in this rebellious spirit, and soon found willing collaborators. Among Luther's acquaintances were the renowned painter Lucas Cranach, the polymath Philip Melanchthon and the jurist Hieronymus Schurff.

It was also at Wittenberg that Luther developed his own particular understanding of the Bible.

For him, the greatest theological problem of all was the existential question of whether the Lord really was a punishing God. For answers, Luther turned to the text of the Bible itself. His interpretations of the scriptures involved meditating at length over single passages.

Luther saw his work as a return to an early Christian manner of understanding the word of God, in keeping with the Gospels. He wanted to avoid blind adherence to the interpretations and teachings of the Pope and the Catholic Church.

Throughout this period, Luther thought little of Christ; indeed, whenever he did turn to Jesus, his mind was filled with terrifying images of hell.

His perception of God was therefore shaped by fear, and it was precisely this fear that spurred him on in his quest to understand the fundamental tenets of faith. He wanted to understand the Bible and align his life with it. But he was no iconoclast when it came to the Catholic Church — he just wanted to reconcile its practices with the scriptures.

However, 1517 marked a sudden break in Luther's approach — and things would never be the same again.

### 5. Luther’s posting of his theses set off a revolution, even if it hadn't been deliberate. 

October 31, 1517, is a date that echoes through the annals of history. On that day, Luther's _95 theses_ were nailed onto the door of All Saints' Church in Wittenberg.

But the act wasn't so radical as it is often remembered; Luther didn't actually set out to reform the entire church in one fell swoop. In fact, the 95 theses were essentially arguments against the flourishing trade in indulgences.

In Germany, a group of Dominican monks led by Johann Tetzel had started to stretch the complex papal doctrine concerning indulgences so they could bring in cash.

Some preachers even declared that the purchase of indulgences essentially gave you a pass for unrestrained sin, as if the purchase offered a guaranteed ticket to heaven.

Luther knew about this behavior all too well, and his study of scripture convinced him that it was wrong. He had even heard confessions from people who refused to atone for their sins, since they had bought indulgences.

Luther's famous 95 theses were a response to this nuanced theological debate.

It's only in retrospect that Luther's posting of his theses has been portrayed as a revolutionary act.

For instance, the famous image we have of Luther nailing the theses to the church door most likely never even occurred. After all, the theses were posted at several churches simultaneously, the same way all notices were.

In the end, it was printers who were responsible for disseminating the work.

There is no question, however, as to the effect the theses had; Church and state alike felt threatened by the stinging criticism.

From ordinary preachers to papal dignitaries, the selling of indulgences was endemic. It had even paid for the building of churches, such as St Mark's Basilica in Rome. Likewise, secular rulers had profited from indulgences, as they had been taking a cut from their sale for themselves.

Whatever the facts were, it's clear that Luther's decision to criticize indulgences had set him on a path to direct conflict with both the Church and several European rulers.

### 6. The publication of the theses caused Luther to be both celebrated and reviled. 

The explosive nature of Luther's theses was clear, and panic spread among those who had been selling indulgences. To clamp down on a possible backlash, they sought to prevent the theses from circulating.

Cardinal Albert of Brandenburg, who had himself profited greatly from the sale of indulgences, commissioned a report from the University of Mainz. The idea was to negate the validity of Luther's theses and ban their dissemination. The cardinal also contacted papal dignitaries who, in turn, instructed the head of the Order of St. Augustine to impose a vow of silence on Luther.

Additionally, the Dominican Order published counter-theses to refute Luther's — but these only succeeding in stirring interest in the debate.

Students at Wittenberg University even carried out protests in Luther's favor: they burned 800 copies of the Dominican counter-theses in the town square.

Luther himself was alarmed by these events, as they gave the impression that he was some sort of radical insurgent. At that point, the Dominican Johann Tetzel called for Luther to be burned at the stake as a heretic. Then, in March of 1518, Luther was tried in absentia in Rome.

Nonetheless, Luther's star in Germany kept rising; his criticisms found particular resonance among the educated and reform-minded middle class.

Within months of the theses' publication, Luther's works were being read by a larger audience, and his decision to publish in German rather than Latin only helped his cause — his message could be read and understood far and wide.

In the midst of this turmoil, Luther changed his name from "Luder" to "Luther," a reference to his preferred nickname "Eleutherios," which means "the liberated one" in Greek.

Then, in May 1518, Luther arrived at his central theological argument, which succinctly encapsulated the idea behind the Reformation.

He argued that only religious faith could lead to salvation, while repentance and acts such as the buying of indulgences could not.

Battle lines had been drawn — but Luther's conflict with the Church had only just begun.

### 7. Luther’s sophisticated media strategy meant he became the Pope’s most recognizable adversary. 

Luther was well aware of the perilous position into which he'd placed himself by challenging the Church. But despite his fears of persecution, he remained determined to spread his new understanding of God.

By 1521, Luther's celebrity status was in no doubt, and a modern media strategy had been at the heart of his success.

It wasn't just the theses that stirred the hype; Luther wrote more treatises, pamphlets and books. Their print runs were huge and his audience stretched well beyond Germany. His most famous work, 1520's _On the Freedom of a Christian_, had an initial print run of 4,000, with several reprints soon after — it was a categorical best seller.

Not only that, but Luther's likeness was captured in a series of woodcut prints that circulated throughout Germany. Everyone wanted to know what this new celebrity looked like.

But Luther wasn't after fame alone. He kept on publishing, and his criticisms of the Roman Catholic Church were becoming harsher and more radical.

Luther's thinking at the time was based on the central tenet that neither the Pope nor the clergy were needed to mediate between a devout Christian and God.

Luther argued that the Church had put Christianity under bondage by tying it to the figure of the Pope. After all, as he saw it, the Pope's authority derived not from scripture, but only from secular law. In fact, he went so far as to see the Pope as an incarnation of the Antichrist, obstructing Christians from following the true path to salvation.

In light of this, Luther called for radical reform. He demanded the dissolution of all papal apparatus and a return to a truer Christianity.

The Church's response came as no surprise: Luther was excommunicated. Even so, he took it in his stride, as he publicly burned the papal bull that decreed his expulsion from the Church.

Cast out by the Church, Luther's fate now depended on the whims of local rulers.

### 8. Luther openly declared his opposition to the Roman Catholic Church at the Diet of Worms. 

It wasn't just the Pope who wanted to put an end to Luther's agitation; the most powerful ruler in Europe, the Holy Roman Emperor Charles V, had Luther in his sights too. Charles V ruled over numerous German states, and he wanted Luther silenced and extradited to Rome.

It wasn't that easy though. Luther's popularity meant that the Emperor couldn't just gag Luther.

The Imperial Diet, essentially the Holy Roman Empire's parliament, demanded Charles hear Luther before an imperial edict was imposed. The Diet had come to this decision due to pressure from the Elector of Saxony, as well as the rulers of the empire's Imperial States, who held considerable sway.

Charles, newly crowned and just 21 years old, had little choice but to acknowledge these powerful voices. So it was that he agreed to hear Luther at the Diet of Worms in 1521.

But Charles and Luther were never going to see eye-to-eye. Charles had been raised a strict Catholic, and the papal excommunication was grounds enough for him to despise Luther. On top of this, the Holy Roman Emperor ultimately derived his own power from the Catholic Church; this was precisely what made his empire both holy and Roman. Charles was conditioned to accept the Pope's authority and respect the unity of the Church.

On the other hand, Luther didn't care for harmony within the Church. He was a religious reformer who recognized the authority of the Bible rather than papal institutions.

In the end, the Diet of Worms did nothing to quell Luther. In fact, he was hailed as a hero, and a wave of sympathy for his ideas swept across Europe. Most famously, he refused to recant his works at the Diet, supposedly declaring "Here I stand, I can do no other." He explained that, as the Bible did not contradict his theses, he had every right to uphold his claims.

It was no surprise to anyone when Charles V decided to enforce the imperial ban and extradite Luther to Rome.

Luther managed to leave Worms before the edict came into effect — and then, quite suddenly, he disappeared.

### 9. The Reformation took hold and was spread by the middle classes. 

The next stage of Luther's life reads like an adventure story. Luther had seemingly vanished, and while many people thought he'd been murdered, he had actually been "kidnapped" by his supporters and hidden away in Wartburg Castle in Saxony.

Rumors of his murder then sparked a wave of social unrest.

The so-called Imperial Knights, a group of the lowly nobles, were convinced that the time had come to do away with not only the Roman Catholic Church, but also all elements of the oppressive economic and political apparatus employed by the princes, bishops and abbots loyal to Rome.

Most famously, the Imperial Knights mounted an insurrection against the Elector of Trier in 1523.

At the time, Luther was under the protection of Frederick III, the powerful Elector of Saxony. Once it was revealed that he was alive, Luther distanced himself from the insurgents.

His reasoning was that his reformation was a purely spiritual affair, and that violence should play no part. Besides, if the Reformation became a revolutionary and chaotic movement, he would no doubt quickly lose Frederick's protection.

Nonetheless, despite Luther's commitment to maintaining civil order, the upheaval had reached fever pitch, even affecting the middle classes.

Across Germany, townsfolk and students drove away priests who were loyal to Rome and pressured town councils to have them replaced by Lutherans, for the most part without violence.

At a stroke, many aspects of the clerical machinery of power simply dissolved. Priests were no longer thought to be sacred, and clerical celibacy suddenly seemed a bit trite.

Churches, too, began to change. For the first time, prayers were said in German instead of Latin, and Luther's German translation of the Bible was introduced into services.

A number of German towns officially committed to the Lutheran Reformation. Where that happened, middle-class merchants and artisans took the place of the recently disempowered priesthood in the social hierarchy.

The scope of the movement had become clear: despite Luther's insistence that his Reformation be solely of a spiritual nature, a deeper social revolution had taken hold.

### 10. Thomas Müntzer developed a social-revolutionary movement built around the tenets of Luther's Reformation. 

It wasn't just the middle classes and knights who were galvanized by Luther's Reformation. Ordinary people also felt the time for change had come.

Specifically, the peasants at the bottom rung of society had had enough.

Due to the population explosion in Europe, demand for agricultural produce had increased; yet peasants weren't generally allowed to sell their produce on the market. What they reaped belonged either to their feudal lords or to the Church, either of whom would profit. The peasants were left with nothing, and facing this deepening poverty, many saw social upheaval as the only solution.

Before long, numerous social revolutionaries began to appear.

In Zwickau, Saxony, craftsmen began interpreting the Bible and even preaching. They had been inspired by Luther's teaching of an individual connection to God. In fact, one group — a weaver, a smith and a student — rose to fame as the "Zwickau Prophets."

However, the ideological foundations for the group had been laid by one Thomas Müntzer, who had developed an alternative variant of Luther's purely spiritual Reformation. It was a model, moreover that explicitly encouraged social equality and challenged societal power relations.

Like Luther, Müntzer radically demanded a way of life oriented towards the Gospel — but Müntzer went much further than Luther.

Müntzer thought that the pastor should be directly elected by the parish, and that tithes collected by the Church should be managed by the community. For example, they could be used to support the poor.

On top of that, Müntzer demanded that the commons — which included meadows, fields, forests and hunting grounds — be expanded.

Müntzer's rhetoric, backed by the power of the Gospel, was highly influential and united the peasant rebellions that had hitherto been sporadic and fragmented.

Needless to say, Müntzer's demands were irreconcilable with the interests of the ruling elite. A full-blooded social revolution was in the air.

### 11. In the Peasants’ War, Luther sided with the nobility. 

A desire for a radical social revolution paired with a desire to disempower a ruling elite will, as a general rule, lead to violent unrest. Müntzer's agitation proved to be no exception.

The German lands were in turmoil; peasant and prince alike committed themselves to bloody hostilities.

Peasants set about violently expelling oppressive rulers from their castles. At Weinsberg, for instance, the peasant leader Jäcklein Rohrbach instigated the killing of the local count and his retinue with dung forks.

The nobility was equally cruel: they had a particular penchant for burning peasants at the stake.

Consequently, the peasants' rebellion quickly grew into a national uprising when miners and impoverished townsfolk joined forces with the peasants.

Meanwhile, Thomas Müntzer gave his full-throated support to the peasant rebellion, which he saw as a means by which to establish a heavenly kingdom on Earth. Müntzer even began imagining that he was a disciple of the prophet Daniel, and used the Gospel to justify social revolution and violence.

Luther, though, was not particularly taken by Müntzer and instead condemned the peasants' rebellion. Luther had chosen to side with the nobility, as he saw them as an essential element for maintaining peace on Earth. To Luther, they were a ruling elite chosen by God and he would pursue his Reformation hand-in-hand with them.

Consequently, ordinary people lost much of their enthusiasm for Luther, and even started protesting against his preaching.

Meanwhile, Luther suddenly changed his opinion on the role of violence in his Reformation: he demanded that the nobility use arms to suppress the peasants' rebellion. He even promised that fallen noble soldiers would be granted immediate access to the Kingdom of Heaven.

The battle of Frankenhausen turned out to be the decisive battle in the war. Five thousand peasants died compared to just six soldiers fighting on the side of the nobility. Müntzer was taken captive and, under torture, was forced to retract his teaching. Ultimately, he was beheaded.

Luther attracted much criticism for his stance in the Peasants' War. Nevertheless, his Reformation would still prove to be transformative.

### 12. Luther’s Reformation brought lasting change to society. 

Once the Peasants' War had come to an end in 1525, Luther put an end to his itinerant life. He married and settled down to become a family man.

More importantly, the German nobility had a newfound confidence thanks to their victory. And with good reason — they now held uncontested power in the Holy Roman Empire.

This was good news for Luther. Over the course of the peasants' rebellion, Luther had lost his rural support. He was now heavily dependent on the nobility in every aspect of his life.

In fact, there's an argument to be made that Luther's reliance on the ruling powers changed the shape of the Reformation.

Luther's Church could no longer follow his original conception; instead of parish churches organizing themselves from the bottom up, the Reformation would now be imposed from the top down, as the nobility was in complete control.

Luther also knew that the Catholic Church was unlikely to dissolve. Instead, he sought to establish a separate, reformed church in the regions where the nobility was amiable to his ideas.

In 1529, 20 principalities and towns sympathetic to the Reformation submitted a _Protestation_ to the Imperial Diet, in which they petitioned against the renewal of the ban against Luther. This protestation is actually where the term Protestantism comes from, a movement that we still associate with the Reformation today.

Aside from radically reorganizing the Church, the Reformation also brought about lasting change to society as a whole.

Education was a central component in Luther's Reformation. He knew that intellectual capacity and literacy were critical to understanding the Bible.

As a result, Luther heavily promoted the building of a large network of municipal and village schools. This access to education brought about fundamental changes in society, as educational aspiration ushered in a new era of German history.

It's clear there's a lot to celebrate about Luther — but there's a dark side to his character that is often glossed over.

### 13. Despite Luther's progressive outlook in some areas, he was an anti-Semite. 

There's no denying it, Luther loathed the Jews and railed against them at length in his sermons.

However, Luther's opinion about Jews was notably inconsistent throughout his life. In his earlier writings, he emphasized that Jesus himself was Jewish. That said, he used that tidbit of information to insist that Jews should convert to Christianity.

In Luther's later writings, the Jews as a collective whole were identified as the murderers of Christ. Luther saw Judaism as a threat to society that existed within it and without it, on par with the hated Ottomans.

He demanded the ruling elite expel the Jews from their homes. He called them an "evil, blasphemous people." The classic tropes of anti-Semitism were all there: they were supposedly agents of Satan who poisoned wells, stole children and then tortured them to death.

However, although some people like to think otherwise, there's technically no direct link between Luther's anti-Semitism and the Holocaust.

The author suggests that historical research prefers to distinguish anti-Semitism of this period from more modern anti-Semitism.

He claims that the earlier kind of prejudice was based on religion and demanded the expulsion of Jews from Christendom, not their eradication as was later the case.

He also believes that the "option" for Jews to convert to Christianity and forcing them from their religious communities and homes was different from the anti-Semitism of the twentieth century, which had the elimination and extermination of Jews as its explicit goal.

While there can be no doubt that Luther's medieval anti-Semitism influenced the anti-Semitism of the twentieth and twenty-first centuries, we cannot place the blame for modern horrors solely at Luther's door. Responsibility for reviving such enmity must rest with the contemporary behavior of individuals and groups who selectively recall their historical and cultural "heritage."

In the next and final blink, we'll examine Luther's continued impact on the world today.

### 14. Luther set the course for fundamental change in Europe. 

Luther died in 1546 at the age of 63 in his hometown of Eisleben. Today, 500 years after his theses first came to light, what exactly is Luther's legacy?

Primarily, Luther and the Reformation he instigated laid the foundations of a modern and pluralistic Europe.

Luther's aim had been to unite Christianity and return it to its more evangelical origins — but he ended up sowing the seeds of discord. Instead of unity, a schism between Catholic and Protestant Churches emerged.

However, the overarching control of the Catholic Church, as well as the authority of the Holy Roman Empire, were also weakened. The stage was thus set for the emergence of modern territorial states in Europe.

Many of these states actually formed less than a century after Luther's death, during the Thirty Years' War, which lasted from 1618 until 1648. In fact, this war, which saw Catholic and Protestant regions battle against each other, would not have occurred without Luther's actions.

Consequently, the Reformation not only represented the transition from the medieval to the modern era, but it also provided the foundations and ideological basis for a pluralistic Europe of nation-states.

Luther's second great legacy was his influence on how we still conceive of what it means to be human in society.

Central to Luther's ideology was his disapproval of the idea that the Pope was the representative of God on Earth, and of priesthood more generally.

Luther taught that all human beings were equal before God, regardless of their position in the Christian hierarchy or their religious participation, such as through prayer or repentance.

It was his understanding of equality that provides the basis for our contemporary understanding of universal equality among human beings. That said, Luther was working in a specifically Christian context, and his ideas don't exactly correspond with our modern-day conception of universal human rights. But we can at least say that he planted the seed from which these ideas blossomed.

Luther was always the radical. He sought to redesign Christianity based on his understanding of the scriptures. However, he unwittingly set the course for a dramatically different, modern social order — one that was even more radical than he could have possibly imagined.

### 15. Final summary 

The key message in this book:

**Martin Luther was no saint. He was a man like any other, but stood by his unwavering faith. His inexhaustible will to fight for his creed and his beliefs was truly remarkable. Luther's Reformation gave rise to the Protestant Church and thereby created the schism in Christianity that still exists to this day. The events of the Reformation had major historical implications, thus making Luther one of the most important figures in European and world history.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Gutenberg the Geek_** **by Jeff Jarvis**

_Gutenberg the Geek_ (2012) examines the life and business of Johannes Gutenberg, inventor of the printing press, and, by drawing numerous parallels between him and modern Silicon Valley entrepreneurs, explains how he was a pioneer of tech entrepreneurship.
---

### Heinz Schilling

Heinz Schilling is Professor Emeritus of History at Humboldt University in Berlin. Schilling's work focuses on the history of religion, and he has published numerous academic works and award-winning popular histories. His book _Martin Luther: Rebel in an Age of Upheaval_ is considered the gold standard of Luther biographies.

