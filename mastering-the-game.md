---
id: 5e8c64be6cee070006fb28ed
slug: mastering-the-game-en
published_date: 2020-04-09T00:00:00.000+00:00
author: Strategies for Career Success
title: Mastering the Game
subtitle: Sharon E. Jones with Sudheer R. Poluru
main_color: B52734
text_color: B52734
---

# Mastering the Game

_Sharon E. Jones with Sudheer R. Poluru_

**Strategies for Career Success**

_Mastering the Game_ (2018) aims to level the playing field for women and people of color in the world of business — the traditional purview of white men. This book lays bare the unwritten rules of the game and outlines strategies diverse professionals can use to get unstuck at work.

---
### 1. What’s in it for me? Unlock the secret rules to the game of career success. 

It's no secret that in the world of business, it's more difficult for women and people of color to get ahead. Just look at the boardrooms of the companies that made the 2017 Fortune 500 list: only 6.4 percent were led by women, while less than 2 percent were led by people of Asian heritage and less than 1 percent by black men. There are many reasons for this, including conscious and unconscious bias, old-boy networks, and lazy stereotyping.

But there's another, less-documented hurdle that diverse professionals face on the path to career success. The white male networks that have traditionally dominated the business world created rules for playing the game, which have been passed down through generations of mentors and protégés. Women and people of color have struggled to win at the business game simply because they don't have any way of knowing these rules.

Until now. In these blinks to _Mastering the Game_, you'll find out about lawyer and diversity consultant Sharon E. Jones's decades of experience learning these rules, together with strategies to help diverse professionals use them to their advantage.

In these blinks, you'll learn

  * how underrepresented women and people of color are in the business world;

  * why building, nurturing, and serving your communities is crucially important; and

  * when to move past discrimination — and when to call attention to it.

### 2. Intentionality is a crucial first step on the road to success. 

It's easy to get distracted — and demoralized — by focusing on things you can't control. But in every situation, there's something you can have an impact on. Every choice you make has the potential to make a difference, and professional success only happens when you approach your career in an _intentional_ way.

There are two steps to implementing an intentional mindset. First, define what success means to you.

This can be more difficult than it sounds. External validation is important for all of us. At one point in her career, the author decided she wanted the top legal job at a corporation: General Counsel. It's a prestigious job that pays well, she reasoned, and was a logical next step in her career as a lawyer. But after two unsuccessful interviews, she realized she didn't actually want the job. Her version of success was doing work that made her happy, not work that others thought was impressive.

Your definition of success might not stay the same for long — it's normal for it to change as it reflects your stage of life, relationships and family, and personal priorities. And it's important to remember that happiness doesn't automatically come with success; in fact, it may be the other way around. Studies by positive psychologists at Harvard show that our brains are 31 percent more productive when we're feeling content. Therefore, a good strategy for achieving more long-term success is prioritizing your short-term happiness.

The next step to implementing an intentional approach to your success is goal-setting. Envision where you want to be 5, 10, 20 years in the future. Then work backward, figuring out how you'll get there.

George Doran's SMART goal strategy has been helping people set personal goals since 1981. As you might have guessed, it's an acronym. Your goals should be: _specific_, _meaningful_, _achievable_, _relevant_, and _time-bound_. When executing your plans, you should both _evaluate_ your goals and _readjust_ as needed.

To hold yourself accountable, write down your goals. Also, share your goals with a friend who can help you track your progress.

But there's more to mental preparation than intentionality. In the next blink, we'll learn how to stay several moves ahead in any professional setting.

### 3. Getting defensive can slow down your career, while an offensive strategy keeps you moving forward. 

There are many roads to success. Unfortunately, none of them are smooth — and for diverse professionals, it's even more difficult. There's no well-trodden path to the top, so they need to develop the mental toughness to surmount anything that comes their way.

That includes gender and racial stereotypes, which are unfair, of course, but also a fact of life. All too frequently, diverse people internalize these negative stereotypes. This hinders their performance and, ultimately, success.

The author has been stereotyped by supervisors many times in her career and has learned to shrug it off and keep moving. Taking something personally and getting defensive, she believes, distracts you from your ultimate goal and the proactive steps you need to take to achieve it. Being proactive is about forming an offensive, rather than defensive, strategy.

Risk-taking is necessary to any offensive strategy, but it requires self-confidence and assertiveness. Because of social conditioning, women and people of color often struggle with these qualities. So how do you get around that? Well, in the words of a black woman at one of Jones' talks — who had successfully negotiated a higher salary at her dream job — "all I had to do was channel my inner white male!"

In her case, that meant approaching her salary negotiations from an offensive position. She commanded respect through confident body language and the use of punchy, relevant jargon.

A good offensive strategy doesn't mean rolling over others, though. Instead, it incorporates their opinions. In order to improve, you need to not only be open to feedback, but respond in a way that invites more. Remember: stay proactive! Never get defensive. Instead, ask follow-up questions, and solicit concrete examples. Then decide how and whether to incorporate the feedback into your strategy.

Knowing when to move on is vital to success, too. If your company is about to go through a change, whether it's a merger or a scandal, it may be better for you to walk away. Stay attuned to how your company is faring overall, as well as to the opportunities you'll have if you stay. But keep your options open. Stay in touch with headhunters, and maintain your network of peers and mentors to ensure you'll be able to seamlessly pivot to a new opportunity if you decide you'd have a better chance of success elsewhere.

### 4. Understand the metrics you’ll be evaluated by, then over-deliver. 

In order to succeed at your job and move up the ranks, you need to consistently hit your numbers. But you can't do this without understanding which metrics are most important to your employer. These aren't always in line with how you personally measure success.

The author has experienced this firsthand. After a few successful years at a law firm, she left to join the US Attorney's Office. Her goal was to gain experience as a trial lawyer. But the government's priority, she learned at a dismal first-year performance review, was clearing as many cases from the docket as possible — which doesn't always involve taking a case to trial. Once she understood this, she was able to improve her performance.

In order to set yourself up for success, you need to add value to your organization. So when you start a new job, you need to figure out your company's real priorities as soon as possible.

There are three ways to do this. First, ask to see a copy of your organization's performance review form, and take note of the qualitative and quantitative metrics by which your performance will be judged. Next, schedule meetings with veterans of the organization, and get their take on what makes a successful employee. Finally, once you've had your first performance review, get a trusted colleague to help you interpret it.

But knowing your company's expectations is only half the job. You also need to make sure you're fulfilling them by being a reliable employee. This means consistently submitting error-free work on time. 

This is even more important for people from diverse backgrounds, who have to contend with employers' _unconscious bias_ — mental shortcuts that help process information and make decisions. Unconscious bias means we favor people with shared backgrounds and interests. It also means we buy into racial stereotypes.

Work that is late or that contains errors is more likely to confirm negative stereotypes that supervisors have about diverse people. The good news is that errors are easy to catch with a little attention. Once you're asked to do something, be sure you fully understand the assignment. Once you've completed it, read your work out loud to catch awkward phrasing and grammatical mistakes. Then check it, and check it again.

The only way diverse professionals can combat unconscious bias is to over-deliver on the aspects of the performance review that are measurable. After all, no one can argue with a job well done.

### 5. Building your network up and out will increase your opportunities and help you get ahead. 

Networking has a bad reputation. For a lot of professionals, the whole exercise feels boring and insincere. But the right kind of networking can both cushion your career in case of a misstep and propel you to ever-greater heights of success.

Take getting a job through a friend — something many people have done. 56 percent of respondents to a 1974 study by sociologist Mark Granovetter reported leveraging personal acquaintance relationships to get their jobs. Imagine how much more likely you are to get a job through an acquaintance if you have many of them!

For diverse professionals, a good network can also reduce feelings of isolation in a white male-dominated workplace. This can help avoid burnout and increase workplace satisfaction.

Good networking practices are pretty intuitive. Don't schmooze everyone you meet, get their business card, and forget about them. Instead, focus on making meaningful connections with people you genuinely like, and nurture the relationship by following up first the next day, then again a month later.

Also, don't only focus on the business "celebrities" in the room. A network of contacts from diverse backgrounds and in different types of roles will be valuable in the long run. You never know which executive assistant might be in the position to someday give you the inside scoop or a last-minute appointment. Be nice to everyone.

It's especially important for women and people of color to develop mentor and sponsor relationships. Because they've historically been excluded from business, women and people of color need extra help navigating the unwritten rules of the office and industry — ideally from someone with experience. A mentor can give you her time and attention, sharing knowledge and giving guidance; a sponsor, on the other hand, can use his influence to help you get to a high-performing level. In exchange for this leg up, you need to devote your time and attention to delivering high-quality results that your sponsor can use to build his personal brand.

In fact, reciprocity is important in maintaining all professional relationships. Don't ask someone a professional favor and expect that to be the end of it. Be sure you find a way to repay them in the future — by way of an introduction, a lunch, or credit on a project they helped finesse.

Reciprocity is the key to a robust network, and a robust network is crucial to career success.

### 6. Paying attention to your personal brand puts you in control of how others see you. 

In the past, personal branding was simple: be a white man, wear a suit, have a pretty wife and 2.5 kids. These days, branding is more individual; it's a balancing act between standing out from the crowd and looking the part. To be successful, your physical appearance and personal brand outside the office have to be just right.

This is especially true for people from diverse backgrounds, whose appearance is more subject to scrutiny because they deviate from the white male norm. While the ideal professional look varies from job to job, you can get a feel for what's appropriate by emulating how people two levels above you dress. When in doubt, opt for more formal — especially in ambiguous business casual situations. You want to be remembered for your work, not your outfit.

But your image is about more than just nice clothes. People don't seek out information on other people's accomplishments, so it's up to you to let your network know when you score a win. When you close a big case, take a casual "victory lap" around the office, letting everyone know how you did it.

Equally, you need to "lay a mattress" to cushion the fall when you see a setback coming. This means managing the narrative when you see signs of trouble — going around the office, for example, and making sure everyone knows why you aren't to blame for a negative outcome.

You can also use strategic self-promotion to shape the way you're perceived. Do a SWOT analysis — that's _strengths_, _weaknesses_, _opportunities_, and _threats_ — of your professional life, and use the strengths and opportunities quadrants to build your brand. For instance, if you want to become known as a successful litigator, only talk to your colleagues about your litigation victories. Your colleagues and their networks then present the opportunity for these strengths to become part of your reputation.

This might sound like bragging, but as the author says, bragging is okay — as long as you do it in an authentic manner. White men, after all, have been doing it forever.

Finally, a good personal brand extends beyond your office. Becoming a _Power Player_ — an influential member of a charity or other socially minded organization — can reap dividends for your career. Get involved in a cause you care about that's relevant to your industry, and look out for opportunities to become more visible in it.

But don't take on more than you can handle. In the next blinks, we'll learn how to balance work with other commitments.

### 7. Burnout can hinder your long-term success, so build recovery time into your schedule. 

Burnout is a real risk when you work hard — and not only because it impacts your performance. A 1999 study in the journal _Ergonomics_ showed that overwork can even damage your health, which the author experienced firsthand.

Because of her bad habit of working weekends and skipping vacations — along with the stress of moving to a new city — she ended up with extremely low cortisol levels, which can cause a raft of health problems. These days, she's learned to build recovery time into her schedule. Not only does she take vacations, but as soon as she gets back from one, she books the next! This gives her something to look forward to and provides her teams with plenty of advance notice.

Recovery time means little breaks, too. Every workday should include some time to refresh and reset from intense focus. The author recommends taking a lap around the office every hour. This allows your body to both recover from slumping over a screen _and_ put in face time with colleagues.

Weekends and vacations away from work are also important. Some people habitually put off vacations, thinking that taking time away will make them seem uncommitted. But one study shows that people who use all their paid leave have a 6.5 percent higher chance of getting a raise or a promotion than those who leave more than ten vacation days unused.

If you just need a change of pace, try working remotely from home or a cafe for a day. But don't do this too often, or you may fall out of the loop with your colleagues via the old "out of sight, out of mind" adage.

Finally, stave off burnout by treating your body like an elite athlete would. This means eating right, exercising regularly, and getting at least seven hours of sleep every night. Of these, sleep is the most important. Research shows that the average American worker loses the equivalent of eleven days of productivity per year due to insufficient sleep. Simply put, you can't do high-quality work on too little sleep.

A small amount of mindfulness can have a big impact — on your health and on the world at large.

### 8. Paying it forward is the best chance we have at changing an unfair system. 

As you rise through the ranks of an organization, you'll have more opportunities to make a positive impact. Lending a hand to other diverse professionals is the most effective way to level the playing field, and it's never too early in your career to start.

Sometimes, time and attention can best be repaid by paying it forward. Perhaps you want to thank your mentor for a great piece of advice but can't afford to take her to lunch or send a gift. Instead, let her know you'll be sharing her advice with your networks, thus amplifying her impact. Later, when you're in the position to share advice and contacts, let your mentees know you expect them to pay your kindness forward.

From within your company, you can work to ensure that diverse professionals are considered for jobs. Many companies hire from specific pools that often privilege white men. By circulating job postings among your networks and participating in hiring committees at your organization, you can help broaden the pool of applicants. If you have confidence in a particular applicant, coach him through the interview process, or even put in a good word with HR.

Also, use your growing status to help junior professionals from diverse backgrounds navigate the unwritten rules of business — like someone hopefully once did for you. Be generous with your contacts. You've built up a great network, so leverage it to help junior professionals achieve their goals. In doing so, you're building up your own personal brand and banking favors.

Another way you can create positive impact is to get involved with your company's charitable giving or community service initiatives. This way you can funnel your company's resources toward a cause you care about, as well as build your reputation with both organizations.

But with power comes responsibility. As you gain status in your organization, use your influence to call out bad behavior when you see it. As a role model in the office, it's crucial that you set a good example by responding when confronted with racist or sexist behavior.

Don't wait until you're a power player to think about your impact. On your very first day at an organization, you have power. Even if you don't have a job, you have power just by existing. It's up to you — and all of us — to respectfully assert that power to enact change in our communities.

### 9. Final summary 

The key message in these blinks:

**Diverse professionals have a harder path to success than others, but it's not impossible. Knowing the rules of the game is half the battle. Study them, work hard to get ahead, and then use your power to help enact positive change.**

Actionable advice:

**Keep a journal, and use it to visualize your positive actions.**

When you're a diverse professional in the business world, unconscious bias on the part of your superiors can lead to resentment and discouragement. Rather than letting these negative thoughts pile up, which can lead to poor performance, use a journal to clear your head. Then, write how you'll take positive action, burning positivity into your subconscious like an athlete visualizing the game-winning shot. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _The Long View_** **, by Brian Fetherstonhaugh**

You've just learned the unwritten rules of the game that women and professionals of color need to know to succeed in the business world. With these as a foundation, you should be ready to set some short-term goals and put into action your plan for achieving them.

But you don't just want short-term success. In our blinks to _The Long View_, by veteran businessman Brian Fetherstonhaugh, you'll learn how to build not just tactics, but a long-term strategy for a rewarding career. You'll also find practical exercises for assessing your skills and goals, as well as wisdom from many successful professionals on the smartest way to achieve your ambitions.
---

### Strategies for Career Success

Sharon E. Jones is a lawyer and consultant with decades of experience navigating the vagaries of the white male-dominated workplace. Her career has taken her from private law firms to the US government to running her own diversity consultancy.

Sudheer R. Poluru is an MBA candidate at the University of Chicago Booth School of Business. He is also an inclusion specialist at Jones Diversity, Sharon E. Jones's diversity consultancy.

