---
id: 557d9fd36533380007360000
slug: mastermind-dinners-en
published_date: 2015-06-16T00:00:00.000+00:00
author: Jayson Gaignard
title: Mastermind Dinners
subtitle: Build Lifelong Relationships by Connecting Experts, Influencers and Linchpins
main_color: 2CADDC
text_color: 1D708F
---

# Mastermind Dinners

_Build Lifelong Relationships by Connecting Experts, Influencers and Linchpins_

**Jayson Gaignard**

In _Mastermind Dinners_ (2015), author Jayson Gaignard introduces an innovative way to build networks that last, by inviting influencers and high-level executives to dinner. As a mastermind dinner host, you open up a whole new world of networking possibilities through gathering interesting people around a table to share a meal and connect meaningfully with each other.

---
### 1. What’s in it for me? Discover how hosting a dinner will expand your networking base for the better. 

Are you tired of managing all your social media accounts? Do the connections you make online seem superficial? If so, then it might be time to turn your networking strategy on its head.

Meaningful personal connections are the core of any successful network, and the way to build them is to actually meet people — preferably in a comfortable, casual environment, like at a dinner.

In fact, hosting a dinner party is the perfect tool in any networker's kit. But how do you transform a simple evening into a _mastermind dinne_ r, an event that will expand your network and boost your career? These blinks will show you exactly how it's done.

In these blinks, you'll learn

  * how to entice influential CEOs to break bread with you;

  * where to host a dinner if you want to meet top web designers; and

  * why nine guests at a dinner table is one too many.

### 2. Human connections, not cash, are what give value to your life. Seek to make your connections last! 

How do you feel after a long day at the office? Washed up, beat down, in need of some R&R?

So many of us expect to feel satisfied by just earning a paycheck. But there's no denying the old cliche that money alone can't buy happiness.

As a young IT entrepreneur, the author earned 22 times more than the national average salary. But did this fact make him 22 times happier, or 22 times healthier, than the average person?

Not at all. In fact, he suffered from stress-related kidney problems, at just 23 years old.

Sure, we all want a job that pays well. But a job that brings in six figures may actually be distracting you from what your true priorities are.

While the author's online ticket sales company was successful and brought in serious profits, the constant work and stress pulled the author further and further away from his original goal: to create a business that made him happy.

Many people, especially entrepreneurs, lead a lonely work life. After another 12-hour day, the last thing on an entrepreneur's mind is planning a get-together with friends. Often, daily stress and work travel make it difficult to connect with people, at work or even in the "real" world.

But as any good businessperson knows, networking is vital in unlocking new opportunities. Building on this concept, the author created a successful business — and life — by hosting networking events such as mastermind dinners and mastermind talks.

He discovered the personal benefits of human connection. And along the way, you will too. Want to find out more? Read on!

> _"When you hit rock bottom, you're left with your word and your relationship."_

### 3. Networking is about authenticity. Be honest about who you are, and your connections will be more meaningful. 

Networking is about who you are, not how you do it.

Having a ton of social media accounts or attending networking events every night doesn't mean you're a "good" networker. Getting out there is just the start; using networking tools helps you dig down and figure out who you really are!

In fact, if you're networking just to get ahead or for some other purely selfish purpose, your contacts will see this pretty quickly — and tune out.

If you want to avoid making a negative impression, then you need to adjust your attitude. Do you see networking as a zero-sum game, where you lose status when one of your peers moves up in the ranks? That's not how it works. 

One thing the author loves most about having a solid network of ambitious friends is being able to cheer on their successes, knowing that in doing so, the relationship grows and deepens.

Know too that the quality of your connections is much more important than the quantity. Networking is about _connecting_, not _collecting_! Do you think a person with 5,000 Facebook friends has a better network than someone with just 100? It's not necessarily the case.

Having 100 friends who "like" and share your posts and leave thoughtful comments is far more rewarding than 5,000 people who couldn't care less!

How can you ensure you make quality connections? You have to put yourself into it. Being honest about who you are and what interests you allows you to create deeper connections with your network.

Granted, not everyone you meet will take a shine to you, and being upfront about your life and work may lead to fewer connections. But you can be sure that those who do stick around will become part of your tightly knit, truly authentic network.

> _"A candle loses nothing by lighting another candle."_

### 4. Do you want to spend quality time with influential people? Invite them to dinner. 

So you've spent a ton of time building up your online networks or trying to meet people at conferences. Yet these strategies have nothing over the _mastermind_ approach to networking.

Masterminds know that there's no better environment for making connections with influential people than during a shared meal in a cozy room that's buzzing with conversation.

So if you want to build a solid network with influential people, invite them to dinner!

Being a guest at a hosted dinner with people is one thing. But organizing the dinner yourself is an opportunity for networking on a whole new level!

By cleverly connecting people whom you think will click, you'll have access to more influential circles than ever before.

So how do you build your guest list? You've got to offer something enticing for your potential guests. Mastermind dinners give influential people a chance to reconnect with old ties or to meet other important people who could become future connections.

If you're worried about a lack of RSVPs from higher-level guests, try inviting people who are more likely to say "yes" first. If you can confirm the attendance of a group of interesting people, it's then easier to get your more elusive guests to accept.

Remember: most people want to network with others similar to themselves. So if you want to set up a dinner for CEOs but you're not a CEO yourself, you should start by inviting one or two CEOs as bait to entice other invited high-level executives.

What's more, it's easier connecting the highly successful if there are other notable players already attending your dinner, as they act as social guarantees. And by gradually enticing more and more interesting people to your evenings, you're more likely to catch the really big fish once word gets around.

In sum, dinners are the secret to strong networking. But you can't host a dinner at any old joint! You'll need a place that serves good food, of course; but there's more to a great dinner than that. Read on to find out the key to hosting a mastermind dinner!

### 5. Build your local network with local dinners. Gather influencers from near and far with location dinners. 

So you want to plan your own mastermind dinner, but you're not sure how to start. Here are a couple of tips to help you determine which sort of event is best for your networking goals.

The first thing to consider is the type of network you want to develop. Once you determine this, you can better decide where you want to host your evening. For example, do you want to create a strong local network with a diverse group of people, or are you more interested in specific connections?

_Local dinners_ where you live or work have their advantages. As you know your neighborhood best, scouting for good locations is much easier.

Local dinners also enable you to build a great network that is physically close to you. This network can then act as the roots for larger, more widespread future networks.

_Location dinners_ are inspired by people traveling to a certain area for a scheduled event, like a conference or trade show. If you're looking to make specific connections with people in a particular field, a location dinner is ideal. So if you want to meet successful web designers, why not hold a dinner at the same time as a well-attended annual IT conference?

The benefit of a location dinner is that it offers an exciting off-site event for people who might not have a whole lot going on after the conference day is done. But if there's a special networking dinner scheduled, your guests will have something to look forward to! Conference organizers can also sometimes give you a list of attendees, which makes it easier to contact desired guests.

So depending on your networking goals, you've got two main options for mastermind dinners. Yet there are more elements involved in making your dinner a success. Our final blink will give you the crucial details in making your mastermind dinner memorable.

### 6. Attention to detail is a mastermind must. Your invites are your foot in the door; do it correctly! 

If you want your mastermind dinner to be memorable, attention to detail is crucial. Exactly how you invite your guests to your dinner, for example, is important.

Email is an inexpensive way to contact people, but it'll cost you a few guests if you don't do things correctly. The subject line of your email should always be personalized, and include the first name of your intended guest. This makes it easier to get past a potential gatekeeper like a personal assistant, directing your invitation directly to the recipient.

Emails should also be short and sweet — instead of a long list of details, make your pitch direct, such as: "Would you be interested in a free dinner with like-minded people?" A question like this makes a "yes" much more likely. Even if your guest isn't able to attend this time around, you'll still have an "in" that you can make use of later.

Another detail that makes a difference is the size of your dinner. Four to eight guests is an ideal size, as fewer guests makes a dinner more intimate, giving everyone the chance to speak and be heard.

And while a limited guest list is preferred, don't let your dinner get too small! If you have only four RSVPs and a bunch of cancellations, you'll risk making your guests feel awkward.

On the other hand, more than eight people at a table can make a discussion difficult to facilitate, as conversations tend to break apart into several separate chats. Hosting a large group will also cost you more, as the extra space needed might push you to booking a private room in a restaurant.

Such details are vital to remember when organizing mastermind dinners. So now you have all the tools you need for a successful networking evening. Get out there and start connecting!

### 7. Final summary 

The key message in this book:

**Reinvigorate your work life by connecting and reconnecting with like-minded people. By organizing mastermind dinners, you'll become the spider in the web of a network that, whether local or global, is a guaranteed source of support and new opportunities.**

Actionable advice:

**Find the uncommon commonalities in your guest list.**

People love connecting with people with whom they share something in common. And if what they share is quite uncommon, they'll be more likely to connect on a deeper level. So get to know your guests individually and in detail to plan the connections that will benefit them and you, too!

**Suggested further reading:** ** _Strategic Connections_** **by Anne Baber, Lynne Waymon, André Alphonso and Jim Wylde**

_Strategic Connections_ (2015) offers practical tips on developing the skills to become an effective networker. In an increasingly connected world, networking has never been more important. Find out which skills and knowledge you need to succeed in the new collaborative workplace.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Jayson Gaignard

Jason Gaignard is a serial entrepreneur who organizes large networking events for entrepreneurs and business people, and also hosts a highly rated business podcast on iTunes.

