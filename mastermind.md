---
id: 5398429f6466370007000000
slug: mastermind-en
published_date: 2014-06-10T12:11:04.000+00:00
author: Maria Konnikova
title: Mastermind
subtitle: How to Think Like Sherlock Holmes
main_color: D2221F
text_color: D2221F
---

# Mastermind

_How to Think Like Sherlock Holmes_

**Maria Konnikova**

_Mastermind_ explores Sherlock Holmes's unique approach of observation, imagination and deduction, and illustrates how anyone can harness his method to improve their thinking and decision-making skills. To this end, the book presents a variety of simple strategies, drawing on scientific research in psychology and neuroscience, and numerous examples from the original Sherlock Holmes stories.

---
### 1. What’s in it for me? Learn how to improve your thinking from a true mastermind. 

Generations have grown up with the adventures of the detective Sherlock Holmes and his friend Dr. John Watson, the crime-solving duo of Victorian London. Even today, their stories continue to be extremely popular, with the BBC series _Sherlock_ captivating viewers around the globe.

But why are we still fascinated by their adventures?

Our interest most likely stems from the detective's infallible mental prowess, and his astute reasoning paired with skills in precise observation and logical deduction — often seen in contrast to the uncoordinated and haphazard approach of Watson. 

This is where Maria Konnikova begins with her book, _Mastermind:_ _How_ _to_ _Think_ _Like_ _Sherlock_ _Holmes_. Packed with examples from the original novels, and drawing on recent scientific insights in neuroscience and psychology, the author takes us on a journey to discover how we can overcome our internal, hapless Watsons for the better and instead cultivate a mindset that resembles that of the great detective. 

In these blinks, you'll find out: 

  * How to harness Sherlock Holmes' mindset — his methods of deduction and his general thinking style;

  * How learning to juggle can transform your brain;

  * Why traders should probably take a day off when the sun is out;

  * And why having too much confidence often leads to mistakes.

### 2. System Watson in the driver's seat: Most of the time we are reflexive and mindless. 

At some point, we've all wished we were capable of making better decisions. But have you ever wondered _how_ your decision-making skills might be improved? 

To improve these skills, you first need to consider that the brain comprises two systems that govern our thinking and decision making.

The first of these systems is a kind of autopilot — a compulsive system that requires little conscious thought.

You can see this in the way the majority of people approach the following problem: 

A bat and ball together cost $1.10. Taken separately, the bat costs $1 more than the ball.

What does the ball cost? 

If you immediately answered $0.10, you were in autopilot mode — and quite wrong. The correct answer is, in fact, $0.05.

So, why do we make this obvious mistake? 

The brain's autopilot system is its default mode, which means that we tend to pick the first answer that comes to mind — the answer that appeals to our intuition. This is because conscious thought requires energy, so our brains turn most of our thoughts into routines and habits that are less mentally taxing. 

While this system is useful, it also has negative consequences for our decision making, often leading to quick, erroneous judgments based on our emotions and intuition. 

Let's name this autopilot system "Watson," after the fictional character Dr. John Watson — Sherlock Holmes' partner in the detective stories penned by Sir Arthur Conan Doyle. 

Why? Because Dr. Watson acts much like the brain's reflexive system. For instance, in one case where Holmes and Watson travel to the countryside to investigate a murder, Watson notes the "ideal spring day" and the "fresh and beautiful" scenery. Rather than being alert and cautious, Watson allows his compulsive system, and thus his emotions, to take over. The result is that he sees everything in a positive light — an inappropriate mindset for a murder investigation. 

However, the good news is that the brain has a second system — "System Holmes" — which we'll learn all about in the next blink.

### 3. There’s another way: System Holmes means mindful, reflective and deliberative thinking. 

In addition to System Watson, the brain has a second system, which is more rational, logical and deliberative.

In essence, this system represents what psychologists call _mindfulness_ : it entails being present in the now and maintaining a keen awareness of one's thoughts and actions. In other words, this system — "System Holmes" — is the very opposite of the reflexive, emotional autopilot of System Watson. 

However, engaging System Holmes involves conscious mental effort: rather than being driven by mere automatic response, this system requires more mental energy than System Watson. 

For example, correctly solving the bat and ball example from the previous blink requires that you resist your automatic impulses, and instead put in the requisite effort to activate the second system. 

This system exemplifies Holmes' particular thinking style, and can help us to make better decisions. 

System Holmes is much like its namesake, in that it is rational, logical and doesn't allow emotions to get in the way of reason. This system is, in fact, the epitome of pure reason, and the good news is that it's not just suited to crime solving, but also enables clearer, more logical thinking in general. 

Take, for example, the first meeting between Holmes and Watson. Without Watson having said a word, Holmes derives from Watson's appearance the fact that he is an army doctor who has just returned home from service in Afghanistan. 

How does he deduce that? 

Simply by combining his observations with logical reasoning: Watson's remarkably dark suntan suggests a recent stay in a hot country; his bearing represents typical military behavior; his haggard face reveals the hardship he has endured; and, finally, his wounded arm suggests a battle injury. 

For Holmes, all of these observations add up to one thing: an army medic in Afghanistan.

### 4. Good news: With practice and engagement, anyone can learn to think like Holmes. 

Since the second system is clearly the more effective of the two, it makes sense that we learn to override System Watson and activate System Holmes. 

How? 

The first thing you need is practice — a _lot_ of practice! 

Many studies show that practice and persistence are crucial because they can significantly develop a person's natural abilities. Some psychologists describe the outcome of practice as _expert_ _knowledge_ : we succeed not because of our natural ability but because we have practiced intensively for countless hours.

In other words, becoming a better thinker is not about innate talent, but is a matter of how much work you're willing to put in. 

One psychological study, for example, involved teaching a group of participants to juggle, then comparing the development of their brains with that of a group that did not learn to juggle. 

By the time the jugglers were proficient, the researchers found that their brains had developed in those areas that dealt with the retention of visual and motion information. In short, the jugglers hadn't just learned a new skill — they had also developed their brains. Similar results have been produced by many other studies.

The second thing you need is engagement and motivation. 

Specifically, this means that you must truly want to think like Holmes, and must make a constant effort to improve. At first, learning to control System Watson is a difficult task, but the more you're willing to reflect on and overcome your initial, automatic impulses, the more you will learn and the greater your motivation will be to continue. 

Indeed, recent research on learning and motivation shows that this approach works: engaged people learn better and faster. Some studies suggest, for instance, that motivated students do better on IQ tests, learn languages faster and have higher academic results.

### 5. Start with the basics: Your brain is like an attic that you can fill, sort and rearrange. 

Before we turn to look at Holmes's thinking process in detail, we first need to learn how the brain stores information. 

Holmes frequently uses the expression "brain attic" as a metaphor for his brain, and in fact, current psychological research agrees with this idea.

The brain is like an attic — a room in which you can store objects. Furthermore, this attic consists of two components: contents and structure. 

_Contents_ are experiences, knowledge and information — basically everything that can be stored in your brain. Think of these contents as packages that can be moved around. 

_Structure_ is the way that the brain stores incoming information. In other words, it determines where the packages of information will be placed, how they'll be labeled, and how they can be found when you need them. 

In order to engage System Holmes — and to disengage System Watson — the contents and structure of your attic need to be transformed.

How? 

Your attic is dynamic: its structure changes constantly, as new information is collected and other information is discarded. Indeed, there's only one area in this attic — think of it as the "main attic" — where information will be stored for the long term. 

Of course, the "main attic" is where you should store important information. To do that, you need the _motivation_ _to_ _remember_ — which means that you have to be motivated and interested when engaging with this content. 

In daily life, this entails making important information as tangible as possible, and linking it to your existing knowledge. For Holmes, that means remembering all the potentially crucial details of a murder case: he links a current case with previous ones, he takes in the smells and sounds at the crime scene, and he reads everything that's printed about the murder in the newspapers.

### 6. Heuristics, biases and environment: There are many cognitive traps on the way to becoming a better thinker. 

Even if you are careful about the way you store and organize information in your brain, there's still a lot that stands in the way of rational reasoning. Humans don't make perfect decisions because the decision-making process is influenced by a variety of problematic factors.

For instance, neuroscience research shows that people arrive at decisions with the help of _heuristics_ : in essence, these are "rules of thumb" that can help us to swiftly reach a solution.

The _availability_ _heuristic_, for example, restricts the information you'll use in your decision making to that which is available in your mind in the moment. This is clearly problematic: just because you can recall only certain information, this doesn't mean that other information isn't equally important. 

We are prone also to many _biases_ in our thinking — tendencies to think in an irrational way. For example, consider the _halo_ _effect_, in which one main impression colors the rest of our perception of a person. Watson, on one occasion, sees only the beauty of a client — her blue eyes and blond hair — and soon finds everything about her lovely, innocuous and positive. 

Furthermore, our thinking can also be affected by the natural environment, so we must be mindful of the consequences it has for our decision making. 

Take, for example, the influence of the weather on our decisions.

Research shows that people feel generally happier and more satisfied with their lives when the sun is out. The problem is that the optimism a sunny day generates can lead people — such as traders — to make riskier decisions. 

And students visiting a college they're thinking about attending are nine percent more likely to enroll in that college if they visited on a relatively cloudy day, because they pay closer attention to what really matters — in this case, the lecturers — than they would if the sun was out.

In the following blinks, you'll learn about Holmes' process in detail. However, as you begin to use this process in your own life, make sure you remember the heuristics and biases that can hinder your ability to think rationally.

### 7. Before you begin: Pause, take stock of the whole situation, and frame your goals. 

So, what are the steps you need to take to start thinking like Holmes? 

First, when confronted with a problem to be solved or a decision to make, take a moment to pause. Problem solving is not about speed but about _accuracy_. Pausing before you begin is essential preparation for the remainder of the process, as it brings you to an ideal starting position in which you operate with your quiet, reflective mind — System Holmes — and take in the whole picture: the sights, sounds and smells of the situation. 

When Holmes and Watson examine a walking stick left behind by a health practitioner at Baker Street, Watson dives right into the process without reflecting. He describes the stick as "dignified" and "reassuring," yet these are emotional stereotypes. Thus Watson is already trapped in his biases and heuristics from the very start!

Holmes, on the other hand, takes in the whole environment first. Rather than examining the walking stick immediately, Holmes pauses, looks at where it was left behind, and examines the situation more generally.

In addition to taking in the bigger picture before attacking a problem and making a decision, it's essential to frame your objectives in a way that will help you to solve them. 

While you pause to reflect, you should not only become aware of yourself and the environment, but also decide on the goal that you want to accomplish.

To do this, ask yourself: What is it that I want to achieve? And, what does this goal mean for all the steps that follow? 

You will need to have decided on this goal in order to align the rest of the process with your overarching objectives.

> _"If you get only one thing out of this book, it should be this: the most powerful mind is the quiet mind."_

### 8. Passive observation: Cultivate an attentive mind with inclusivity and engagement. 

Once you've activated your reflective mind and framed your goals, the next step is _accurate_ _observation_.

However, due to the way that our internal biases steer us off course, accurate observation is far from straightforward. 

Research shows that if people focus on one element of their environment, other elements simply vanish from their perception. The upshot is that we tend to erase whole parts of our environment — a phenomenon researchers call _attentional_ _blindness_.

For instance, it's been found that when people use only one ear to listen to a conversation, they completely miss what is being said into their other ear.

Hence, to observe accurately, we must cultivate an attentive mind. And we can do this by becoming what scientists call _passive_ _observers._

Here, being passive means doing nothing else but observing — in other words, actively observing but remaining passive toward all other activities. In this mode, you'll focus on what you set out to observe (determined by the overarching goals you established in the previous step) as objectively as possible. 

How? 

One way is to be as inclusive as you can, using all of your senses, just as Holmes smells, touches, listens and uses his magnifying glass. This will help you to get a more complete picture.

Another strategy is to pay attention to what is _not_ there. In one case, Holmes observes that a house owner's dog did not bark the night that an intruder entered the house, deducing from this that the intruder must've been someone that the dog was already familiar with.

Yet cultivating an attentive mind means nothing if you don't double-check your initial observations.

Even with the best strategy, you will always be prone to make mistakes, so you need to consider the potential errors in your collected observations (perhaps you missed or misinterpreted something?) and adjust them accordingly.

### 9. Creative imagination: Generating alternative options by distancing. 

After accurate observation, the next step in solving a problem is the creative phase. Here we use our imagination to generate many different options and approaches.

Think of imagination as a space in your brain's attic. In the creative phase, you play with your observations by mixing them with your existing knowledge and exploring different combinations and seemingly unrelated connections. The main goal in this phase is to generate alternatives that might help to solve your problem. 

The challenge in this phase is to imagine a novel solution — one that hasn't been imagined or considered before, and one that you've never seen.

For Holmes, that would be like imagining an ocean from just a drop of water, without having ever seen a lake, river or the sea in one's life. 

A good example of a radically creative solution is the "Fosbury flop" invented by high-jumper Dick Fosbury. In his time, high-jumpers would usually adopt the straddle or belly roll technique, but Fosbury found an entirely novel solution and a completely new way of jumping effectively — _backwards_, not forwards.

But how exactly can you kickstart your creative imagination?

One technique is called _distancing_, and it involves stepping away from the challenge at hand and giving your mind the space it needs.

Distancing can be achieved in a variety of ways — for example, by engaging in activities unrelated to the problem; employing mental distancing techniques; or creating actual physical distance.

Indeed, research has shown that distancing helps to improve the thinking process and leads to increased creativity. For example, people who find themselves stuck with a particular problem have been shown to find solutions after taking a walk in nature. And, in case you're wondering about Holmes' favorite distancing techniques, they are: smoking a pipe, playing the violin and attending musical concerts.

The key to distancing is to find an activity which stimulates you but doesn't demand much effort, such as meditation or walking.

> _". . . each challenge is an opportunity to learn something new, to expand your mind, to improve your abilities and add more tools to your attic for future use."_

### 10. Logical deduction: Distinguish the important from the incidental. 

After pausing, observation and creativity, it's time to enter the next phase in the problem-solving process: _deduction_. 

The aim with deduction is to consider the array of alternatives that emerged during the creative phase, then to determine which works best as a solution to the problem. 

The challenge here is to decide on the best solution without totally excluding the highly improbable ones. For example, in one case, Holmes must deduce how an intruder got into a house: through the door, window, chimney or a hole in the roof? Even though entering the house through the hole is the least likely scenario, it's not _impossible_ _–_ and therefore should not be excluded. 

While in the deduction stage, you have to ensure you keep System Watson at bay, as it likes nothing more than to simplify the process and to deceive you. 

Using System Watson, we have a tendency to select the most plausible scenario as the solution, based on our past experience. In the process, we subconsciously neglect all evidence that runs counter to our favored solution, or that complicates the issue.

Psychologists call these subjective narratives _natural_ _stories_. Consider, for instance, the following classic study. 

People were shown a video of a car accident and asked to estimate the speed of the cars at the moment of impact. However, the wording of the question was altered for different people — the cars either "hit," "smashed" or "bumped" — which had a dramatic effect on the estimations they came up with. Those who'd been asked the question employing the word "smashed" estimated the cars' speed as much higher than those whose questions had used "hit" or "bumped."

In fact, some of the "smashed" group even recalled having seen broken glass, even though there was none. 

However, you can diminish the power of natural stories by adopting a simple strategy: reflect, resist the impulse to simplify, and edit your intuitional answer. Ask yourself: Am I using all evidence? Am I giving it all the same weight?

### 11. Never stop learning and practicing: Fight complacency and overconfidence. 

Having explored Holmes' thought process step-by-step, it's now time to ensure that the Holmesian method becomes a habit. 

The aim here is to make Holmes the autopilot. At first, you'll find it hard to remember to go through each step of Holmes' thinking and problem-solving process, but the more you practice and persist, the easier and more habitual it will become. 

The best motivation to keep practicing is to remind yourself of the benefits of Holmes' approach. For example, the long-term rewards for your persistence include saving time when making decisions, and making better and more informed choices.

However, there's a pitfall in continued practice: the more practice and success you have, the more likely it is that you'll become overconfident in your abilities. And with overconfidence comes underperformance. 

For example, studies have shown that overconfident traders make more mistakes than their less-confident peers because the former become too certain of their abilities and begin to trade too aggressively.

But no matter how hard you try, in the long term it won't be a question of whether or not your thinking becomes sloppy, but what you should do when this happens. It's useful to remember that even Holmes makes the occasional mistake.

So, if you've taken a wrong turn and become lost in System Watson, ultimately making an erroneous judgment, you need to continue to learn from your mistakes and persist in your efforts. 

A good strategy is to enlist someone to help you in this learning process — perhaps a friend or family member. Holmes, for instance, often explains his thought process to Watson, which allows Holmes to reflect on his own reasoning and, equally importantly, gives Watson the chance to challenge him. 

As we've seen, Holmes is a great detective not because of an innate ability or genius, but rather as a result of his insight into his own weaknesses and his commitment to learn from mistakes.

### 12. Final Summary 

The key message in this book:

**By** **examining** **Sherlock** **Holmes'** **method** **of** **rational,** **mindful** **thinking,** **you** **can** **improve** **your** **own** **decision-making** **skills,** **creative** **abilities** **and** **logical** **powers.**

Actionable advice:

**Speed up your learning.**

A good method is keeping a decision diary. In doing so, you'll probably discover some common patterns in your decision-making process.

**Adopt** **the** **"hunter** **mindset".**

Be ever-alert, vigilant, ready to align your strategy to your environment and learn from previous mistakes
---

### Maria Konnikova

Maria Konnikova is a Russian-American writer and journalist with a background in psychology, creative writing and government. Her contributions have appeared in numerous publications, such as the _New_ _Yorker_, _Scientific_ _American_, the _New_ _York_ _Times_ and others.

