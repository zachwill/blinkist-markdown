---
id: 54f579dc373031000a010000
slug: masters-of-disaster-en
published_date: 2015-03-03T00:00:00.000+00:00
author: Christopher Lehane, Mark Fabiani and Bill Guttentag
title: Masters of Disaster
subtitle: The Ten Commandments of Damage Control
main_color: D1342A
text_color: D1342A
---

# Masters of Disaster

_The Ten Commandments of Damage Control_

**Christopher Lehane, Mark Fabiani and Bill Guttentag**

_Masters of Disaster_ takes you on a journey through a crisis that characterizes today's information age. Drawing on damage control operations, the authors have developed the ten commandments to help any politician, celebrity or company avoid scandal and a tarnished reputation.

---
### 1. What’s in it for me? Learn how to overcome crisis and scandal. 

Turn on your TV, open a newspaper or check your news feed, and you will probably find out about someone under serious pressure: a politician in a compromising situation with a lover, a multinational evading taxes, or a professional athlete accepting bribes. In short, crises and scandals hit everyday.

The constant nature of these scandals and their severity could deter you from striving for power and influence; some would instead wish for a quiet life, devoid of the risks of fame, yet also of the excitement of it.

But don't let that fear deter you. These blinks will show you how to be resilient to crisis, based on the experiences of some of the top disaster managers in the world.

In these blinks you will learn

  * why Rupert Murdoch's staff turned against him;

  * why the German football manager sent himself for a drug's test; and

  * why no one manages a crisis better than Hillary Clinton.

### 2. Publicized crises are increasingly common in modern society. 

When you hear the word crisis, what springs to mind? A plane crash? A bank collapse? Yet a crisis could be less overt, but equally troublesome: an organization's security protocol could be breached or a high profile figure could be misquoted on an important issue.

If we think about crises this way, we can see that they happen _every single day_. Today, several social and technological developments have unfortunately made crises the "normal state of nature." In our information age, virtually everyone with a smartphone can break news. Every stray comment and poor decision can immediately be shared across the world at lightning speed.

Say a risque joke at the office party and within minutes it's viral across the company. A businessman criticizes his staff to a journalist, and in no time at all, it's plastered all over the news feeds of his employees, shareholders and customers.

In this way, the news we receive and share via social media tends to be selective and biased. Simply through the sheer wealth of different sources of information online (social media, news sites, blogs etc.), it is increasingly easy to find news to fit your existing beliefs.

So, major crises can be created by people who naively think what they've heard is true and spread the false information online. Remember those claims that President Obama was not born in the USA? They were widely proliferated by Republicans and led to crisis-fed journalism, even though they were complete nonsense.

Once misinformation goes public, it's hard to restore one's reputation. But it's been done. In the next blinks, you'll learn how.

> Fact: According to the Edelman Trust Barometer, trust in information from self-selected social media sites reached 75 percent.

### 3. You can’t escape a crisis by hiding or spinning information or by blaming someone else. 

While crises have become increasingly frequent and publicized, we still aren't that good at combating them.

When a crisis breaks out, the first mistake we often make is responding too hastily and releasing inaccurate information. For example, in 2011, Congressman Anthony Weiner was accused of "sexting" six women, though he was married.

As soon as the crisis broke, Weiner rushed to respond, first denying the accusation, then claiming his Facebook account was hacked. In the end he was proven guilty and was forced to resign. If had responded calmly and released accurate information, he may have been able to ride out the situation.

The second mistake we make in a crisis is concealing problematic details or spinning things in our favor: whatever you try to hide will get exposed sooner or later.

For example, after the explosion of the Deepwater Horizon oil drilling platform, BP launched a huge PR campaign promoting overly optimistic outcomes to the damage caused by the oil. Yet, when the actual damage became apparent, the company looked like it had tried to hide (or even worse, make light of) its mistakes.

Finally, there's one more major "don't" in a crisis: blame shifting. By trying to make the crisis appear as someone else's fault, that someone else will attack you back. Rupert Murdoch learned this lesson the hard way.

When the _News of the World_ tabloid, owned by Murdoch, was accused of hacking cell phones, Murdoch blamed a number of the paper's executives. It's unsurprising then that these executives provided strong testimony against Murdoch in return. By trying to lay the blame on others, Murdoch only made things worse for himself.

Now we've seen all the things we shouldn't but often do when facing a crisis. So what _can_ we do? As it turns out, we aren't totally helpless.

### 4. Avert a crisis by planning ahead, acting calmly and thinking long-term. 

Get ahead of the game to manage a crisis: if you are prepared for the worst, you can improve outcomes even after a major blow to your reputation.

Think about potential crises and develop strategies to combat them: with which audience do you need to repair trust? For example, a professional sports franchise should be prepared to respond to a fight in which a rival fan is badly injured. Or, a non-profit organization should be prepared to reassure donors following accusations of embezzlement.

Having an emergency strategy will also help you keep cool when a crisis breaks. It will allow you to face the situation with care and attention.

In 1996, First Lady Hillary Clinton demonstrated an emergency strategy when important but apparently "lost" financial documents surfaced.

Rather than panic, the Clinton team made a planned and measured response, making the records public and explaining the situation as best they could. The crisis soon dissipated, with little to no damage to the Clinton reputation; in fact, her husband was re-elected the same year.

Here's something Hillary Clinton would also know: damage control is a _marathon_, not a sprint. Even if you don't manage to avert a crisis immediately, always think in the long term. If you want to restore your reputation and trustworthiness, you've got a long road ahead — so _keep on going._

For example, Michael Milken, an unscrupulous Wall Street trader, was sentenced to 10 years in prison for insider trading. But due to good behavior, he was released after only two years and began to direct his financial potency toward cancer research. Soon, he was featured on the cover of _Fortune_ as "The man who changed medicine." He's a prime example of long-term reputation recovery!

### 5. Recover from a crisis by being honest and presenting a solution. 

How can you restore people's trust in you after a crisis jeopardizes it? Do you provide people full disclosure? What if the truth is embarrassing or confusing? No matter what, you must remain completely honest and accurate.

In 1992 nine people died from Tylenol pills that had been laced with cyanide. The producer of Tylenol, Johnson & Johnson, told the public immediately what little they knew about the situation, and admitted that they couldn't answer many important questions.

Sure, the incident and the gaps in their knowledge made the company look incompetent. But, they demonstrated an honest approach to addressing the problem and managed to mitigate damage to their brand reputation.

After you've owned up to a crisis, you must provide a plan to fix the problem. Showing that you are in control and willing to make changes allows the public to see you're serious.

But, you must offer a plan you can deliver.

Johnson & Johnson demonstrated good damage control when they outlined an achievable plan that promised to prevent the Tylenol deaths from ever happening again. They informed the public that pills already on the shelves would be destroyed, and new batches would have a secure lid that would show evidence of tampering.

In doing so, Johnson & Johnson rode out the catastrophic crisis, and they have continued to be a market leader.

### 6. Limit a crisis by sticking to your bottom line and cooperating with authorities. 

Say you've made an honest statement about your crisis and presented your solution to the public. If you want to keep the situation under control, there are two things you've got to do. 

First, stick to _the bottom line_ : a statement you can maintain with certainty over the crisis' whole course. For example, Maple Leaf Foods, which was found to have released life-threatening bacteria-contaminated products into the market in 2008, responded, "Our actions are guided by putting public health first." It was a bottom line they could commit to as they rode their crisis out.

On the flip side, a careless bottom line will worsen the situation.

For example, German football coach Christoph Daum was accused of using cocaine in 2001. He announced he would take a drug test to prove his innocence; the drug test was positive, forcing Daum to admit use and resign. He later said that taking the test had been "a mistake."

In Daum's case, the better choice would have been to _commit to cooperation_. Most crises are reviewed by an official body. If you want to stand the best chance of a good working relationship with them, you have to let them know you are ready to cooperate _fully_.

Commitment to cooperation demonstrates that you want to get to the bottom of the issue without covering anything up. In Hillary Clinton's case, for example, she minimized damage by passing the documents to investigators immediately and telling them all she knew. The investigators, in return, were able to let the crisis smoothly pass over.

### 7. Let others take the spotlight, as you get out from under it. 

When a crisis happens, there is one place you simply don't want to be: the _spotlight._ How can you shift the focus of a scandal away from yourself?

First, let the issue become bigger than you. There may be others involved who are likely to play a greater role in the crisis. A whole system might even be brought into question, while you're perceived as just a tiny part of a systematic breakdown.

If you can see that the blame will likely be focused onto other broader factors or issues, then simply keep calm and quiet, and let the scandal drift to someone or something else. If you don't, you'll only prolong the chaos.

For example, in the huge MLB doping scandal many players were accused of doping, including one of the most successful players of all time, Barry Bonds. Roger Clemens, one of the accused, was close to retirement. He could have just kept quiet and let the media focus on Barry Bonds and MLB baseball. Instead he fired back at the accusers and released various statements of innocence which kept him in the spotlight for months!

What Roger Clemens didn't realize was that if you don't make the news again, you and your part in the scandal will quickly be forgotten. The overwhelming speed of spreading information and masses of content that we are bombarded with increases the likelihood of getting caught up in a crisis, but it also gives us a chance of getting out of it just as quickly.

Sometimes, though, having a passive approach isn't enough.

### 8. Defend yourself against unfair attacks and expose the opposing party’s motives. 

What if you come under fire due to accusations that just aren't true? Simply sticking your head in the ground like an ostrich and waiting for the storm to pass by might not be enough. Instead, you should expose those who brought the crisis into being and shine a light on their self-interested motives.

In a crisis there are always the subjects of the crisis and those who benefit from it. For example, in the 2004 US presidential election Senator Kerry made his military career in Vietnam as a commander of a Navy Swift boat a core aspect of his campaign. Shortly before the elections an allegedly independent group of Vietnam veterans calling themselves "Swift Boat Veterans for Truth" published several ads and a best-selling book questioning Kerry's honesty and integrity.

Superficial background checks would have revealed that the supposedly independent group had strong ties to many active Republicans. However, the Kerry campaign failed to react in time. This only won John Kerry an entry in the dictionary of the word "swift-boat" (meaning to launch a smearing campaign), but might have cost him the presidency.

Kerry should have defended himself and attacked their position. His failure to expose them cost him dearly. If you want to expose those who swift-boat you, you'll need to research thoroughly, connect the dots and find out who is behind your crisis.

In most crises there is some party who gains from your scandal. When you find a provable misrepresentation, seize it and condemn the opposition. This will shift the attention toward the low credibility of your opponent and lead your audience to question other claims they've made as well.

So, whether it's a slip-up of your own or a calculated attack by an opponent, these blinks have prepared you for any kind of crisis that comes your way.

### 9. Final summary 

The key message in this book:

**Crises are increasingly common in modern society, yet that doesn't mean that we should let them rule our lives. If you learn the secrets of damage control, you can maintain people's trust in you, no matter what you're faced with.**

Actionable advice:

**Admit your mistakes!**

Making a mistake when you are under pressure is a common thing. If this happens to you, don't despair! Simply admit the mistake as soon as possible and your humility may earn you support.

Fact: The Wall Street Journal analyzed several YouTube apologies from CEOs battling crises. They concluded that the sooner the person apologized, the more effective their recovery.

**Suggested further reading:** ** _Crucial_** **_Conversations_** **by Kerry Patterson, Joseph Grenny, Ron McMillan and Al Switzler**

We've all been in situations where rational conversations get quickly out of hand, and _Crucial_ _Conversations_ investigates the root causes of this problem. You'll learn techniques to handle such conversations and shape them into becoming positive and solutions-oriented, while preventing your high-stakes conversations from turning into shouting matches.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Christopher Lehane, Mark Fabiani and Bill Guttentag

Bill Guttentag is an Oscar winning director of numerous programs for TV networks including ABC and HBO, and documentaries including _Nanking_ and _Soundtrack._ He is also a lecturer at the Graduate School of Stanford University.

Christopher Lehane and Mark Fabiani are damage control experts who have worked for various high-level individuals and companies, including Bill Clinton. They both graduated from Harvard Law School and specialize in the art of damage control.

