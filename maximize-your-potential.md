---
id: 59887e58b238e10006290102
slug: maximize-your-potential-en
published_date: 2017-08-11T00:00:00.000+00:00
author: Jocelyn K. Glei
title: Maximize Your Potential
subtitle: Grow Your Expertise, Take Bold Risks & Build an Incredible Career
main_color: E4562E
text_color: B04223
---

# Maximize Your Potential

_Grow Your Expertise, Take Bold Risks & Build an Incredible Career_

**Jocelyn K. Glei**

_Maximize Your Potential_ (2013) is about making the most of your skills, talents and ambition. It offers action-oriented tips and tools that will help you execute your ideas and create an amazing career. Discover the traits and mind-sets of the most productive people and how to unlock your maximum potential.

---
### 1. What’s in it for me? Find the right life fit for you. 

"Follow your passion." At some point in your life, you've probably heard this advice. But is it any good?

Well, you might be passionate about the _concept_ of being a rock star, but unless you have some serious musical chops, it probably isn't the smartest thing to blindly pursue that goal. And you've probably noticed that having a natural talent for something often makes _doing_ that something more enjoyable.

Of course, skills can be developed and honed — but there's often something that we're naturally good at and naturally enjoy doing, whether it's science, math, art or baking. That's what maximizing your potential is all about: lining up your natural talent and your passion.

These blinks, inspired by the thinking of experts and influencers such as Cal Newport and Joshua Foer, show how anyone can easily reach their potential. Inside, you'll learn

  * Why you shouldn't focus on a job title

  * How you can increase your luck

  * And why asking for help makes you look good

### 2. To find your dream job, focus on the lifestyle you want and the skills you have. 

Do you wonder how some people simply seem to find the job they love doing? You might think it'll happen to you, too, as long as you stay true to your passion and let it guide you.

That might sound nice, but it's a bit off the mark. When people really find happiness at work it often has less to do with what the job entails and more about the culture and lifestyle of the job.

For instance, Bill McKibben loves his job as an environmental journalist. But this happiness doesn't come from a passion for writing or a devotion to environmental issues. Rather, McKibben is happy about the control he has in deciding when and where he works. He also likes that his writing has a positive impact on the world.

In his case, journalism was just one of many options that could have allowed him to meet these conditions for happiness.

McKibben's career also reveals how skill is more important than passion in helping you reach your career goals. After all, passion won't get you very far if you don't have the skills to back it up.

McKibben wasn't a highly skilled journalist when he was an undergrad, writing his first articles for the _Harvard Crimson_ newspaper. It took a lot of practice and tenacity — and over 400 articles! — before he was skilled enough to work for _The New Yorker_.

But he didn't stop there. He kept honing his skills, until he finally felt confident enough to be his own boss. Only then did he quit his job and retreat to a cabin in upstate New York to write his first book.

So, ultimately, McKibben got to where he wanted to be through hard work and focusing on his craft.

### 3. Give your career a mission and be open and receptive to opportunity. 

The rapid rate at which technology is advancing affects everyone. It can even make you feel uncertain about how much longer your position will exist, or be taken over by robots.

So what can you do to protect yourself?

One way of resting a little easier is to focus on a mission, not your job title. After all, not long ago the term "app designer" didn't mean anything. Job titles can disappear just as quickly as they pop up!

So rather than betting on the staying power of a specific role, create a mission for yourself that says what you want to accomplish and which problems you want to solve. This way, for example, instead of becoming an "online innovation director," you can be clearer about the value of your mission by being someone who wants to develop innovative ways of publishing books online.

Also, while you might feel uncertain about the future, you don't have to feel like luck isn't on your side. Luck isn't some mysterious force. It's something you can cultivate by being both alert and open to all of life's possibilities.

For example, one way to seemingly be presented with a lucky opportunity is to meet the right person. You could call these "lucky chance encounters," but you can also make them more likely to happen by putting yourself out there, meeting people and networking rather than staying glued to your computer.

Even if your job is writing, you can make sure to make time to meet up with friends. You never know — one friend might have another friend whose sister runs a publishing company.

The more people you meet, the more inspiration you'll get for stories and characters, as well as career opportunities.

So don't think of luck as something that only happens to other people. Good things can happen to you as long as you're open and receptive to life's infinite possibilities.

### 4. Develop an eagerness to improve by embracing mistakes and asking for help. 

When you start your day, which of these attitudes best describes your outlook? I'm going to _be good_ at my job. Or, I'm going to _get better_ at my job.

For the benefit of your career, you should be striving for a _get-better_ attitude.

Someone with a _be-good_ attitude wants to prove that she is very capable. She doesn't focus on how to get better, but on being good. In fact, it makes her uncomfortable when people excel when she doesn't.

On the other hand, someone with a get-better attitude will see someone with superior skills as a chance to learn and acquire new skills, which is an opportunity she's always on the lookout for.

In fact, the get-better person doesn't compare themselves to others. Instead, she measures her growth by comparing her work from today to her work from yesterday. With her focus on learning and growing, the get-better person handles setbacks well, doesn't give up and sees challenges as just another opportunity for improvement.

If you tend to approach work with a be-good attitude, don't despair. The brain is highly adaptable and can always be trained to learn better habits. How?

First of all, don't be afraid to make mistakes. There are a number of studies that show how people who freely acknowledge the possibility for errors are actually _less likely_ to make a mistake. In fact, by giving yourself the freedom to mess up, you are preparing yourself to notice and catch those mistakes when they arrive.

Another habit you can start is to not be afraid to ask for help. After all, you can't improve or learn if you don't admit that you need help and allow someone to teach you. If you're afraid of looking dumb, well — don't be. Studies show that, when you ask for help, the person you ask will tend to see you as _more_ capable, not less.

### 5. Using a journal is an easy way to develop ideas even when you don’t have a creative team. 

Keeping a diary or daily journal shouldn't seem like a childish thing to do. After all, Che Guevara, Virginia Woolf and Andy Warhol are just a few examples of the impressive people who've kept a diary.

But journaling isn't just a way to make a record for posterity, it's also a way for you to write down thoughts and ideas and be your own sounding board.

A sounding board is another person or group of people that can provide feedback on your thoughts. Of course, not all of us are lucky enough to have access to bright individuals who are capable and willing to nurture our ideas. So capture all your thoughts and sparks of inspiration in the pages of a journal and be your own sounding board. This way, moments of brilliance won't get lost or forgotten, and when you're searching for you next big idea you can look back and see which ones still have potential and deserve some nurturing.

But for a journal to really work like a sounding board, you can't just jot something down and leave it behind. You need to revisit those old pages regularly.

Finding a regular time to look back at your ideas — and nurturing the good ones! — is just as important as developing a daily journaling habit.

You can help make sure your journaling habit sticks by finding a regular time to write, even if it's just ten minutes, and finding a peaceful place to write in. By making it the same time _and_ location, you will ritualize the activity and increase the chances that it continues.

There's no need to put overwhelming pressure on yourself, either. Start out with a small goal of writing an entry every day for a week. When that works, keep it going for a month. From there, it will gradually become a normal part of your day.

### 6. To build resilient relationships, be open about how both sides can benefit and collaborate. 

Life is full of ups and downs, and this extends to your working relationships as well. It doesn't matter what business you're in, eventually you'll run into a problem with a new employee, a new boss or a troublesome supplier.

Try to remember, though, that the ideal business relationship is a strong collaboration where both parties happily get what they want. The way to build this kind of relationship is to form a social contract that lays out the terms of how you'll be collaborating.

Social contracting is a practice that was first introduced in the 1981 book _Flawless Consulting_, by the management expert Peter Block. He made it clear that a strong social contract clearly focuses on _how_ to collaborate.

You get to the _how_ by asking about and articulating what each party wants.

You might find this a difficult thing to put into precise words, but it's in your best interest to try. Once you clearly define what you expect from the relationship, it'll be a whole lot easier to manage.

And be honest! Even if your main goal in a collaboration is to get a promotion, it's best to be up front about it, because for the collaboration to work you need to find a way to align your goal with your partner's goal. So ask questions and find out what you can provide the other person — and what they're ready to provide for you.

Prior relationships can help you as well. Talk about what's worked in the past as well as what hasn't worked and what you want to avoid.

As you're building your social contract, take turns telling each other what happened in the perfectly successful relationships you had in the past. And ask your new partner what it was that their previous partner did to make it work so well.

### 7. Don’t miss out on opportunities by worrying about risks and potential pitfalls. 

In prehistoric times, our life expectancy wasn't very high. Dangerous animals, exposure to extreme weather and any number of other dangers were waiting around every corner.

So it's no wonder that, even though the world has changed, our instincts are still highly attuned to avoiding risk.

There's no escaping the fact that we live in an unpredictable world and that it's impossible for us to know every possible outcome to the decisions we make. No matter how much information you collect, no matter how much you try to make the most informed decision possible, you can still be left with doubts and worries over whether you made the _right_ choice.

These fears are part of our innate aversion to risk, but you can train your brain to focus on the opportunities that are part of the decisions you make, rather than the potential pitfalls.

How many times did you say no to something that sounded exciting yet too risky? Did you ever stop to think about the potential benefits?

This isn't to suggest that you should ignore risks altogether, but, there's no reason to fixate on them either.

Let's end on a classic example of an interesting time for The Coca-Cola Company. In 1989, when the Berlin Wall came down, Coca-Cola's German management team wanted to take the opportunity to expand into East Germany.

However, the division president at the time, Don Keough, quickly dismissed the idea as too costly and risky. But the management team didn't give up. Instead, they threatened to quit!

So Keough gave it a second thought and agreed to travel to East Germany, where his eyes were finally opened to some great opportunities. Eventually, the company purchased a couple of plants and expanded their business into the East. And sure enough, they encountered plenty of pitfalls — but they overcame each one and the area went on to become a profitable market.

There's nothing special in finding things to worry about. What _is_ special is seeing past those worries to the amazing opportunities that also lay ahead.

### 8. Final summary 

The key message in this book:

**With the right advice and tools at hand, you can make the most of your potential. It's all about forging opportunities, honing your craft, nurturing your relationships and learning how to handle risk.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Make Your Mark_** **by Jocelyn K. Glei**

_Make Your Mark_ features the wisdom and tips of 21 of the most successful entrepreneurs and creatives of the last few years. These artists, coders, developers and writers share the secrets and ideas that have helped them take their respective markets by storm.
---

### Jocelyn K. Glei

Jocelyn K. Glei lives in Los Angeles and specializes in helping others get the most out of life, both professionally and personally. As a modern productivity expert, she is especially obsessed with finding ways of remaining creative amid today's many distractions. Her other best-selling books include _Manage Your Day-to-Day_ and _Unsubscribe_.

