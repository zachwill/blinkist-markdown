---
id: 570264d38fb8ff000700007f
slug: me-myself-and-us-en
published_date: 2016-04-08T00:00:00.000+00:00
author: Brian R. Little
title: Me, Myself and Us
subtitle: The Science of Personality and the Art of Well-Being
main_color: F57431
text_color: A85022
---

# Me, Myself and Us

_The Science of Personality and the Art of Well-Being_

**Brian R. Little**

_Me, Myself and Us_ (2014) is about what it is that makes you _you_. These blinks outline the different aspects of personalities, what influences them and how they determine our behavior.

---
### 1. What’s in it for me? Get to know yourself and your behavior in a new and deeper way. 

Who hasn't at some point wondered what exactly causes and guides human behavior? You might have burning questions about why you can't overcome your shyness, why you made a fool of yourself that one night or perhaps why you just can't seem to connect with colleagues at work. Maybe you've even taken a personality test in a magazine and asked yourself if there was any scientific validity to its conclusions.

These blinks take a closer look at just these kinds of questions, drawing on the latest findings from scientific research on personality to explain what drives our behavior and, ultimately, answering one of the biggest questions of all: is our behavior set in stone or do we have the ability to change it?

In these blinks, you'll discover

  * one possible reason why you just can't get over your last breakup; 

  * why you'll probably be a better jazz musician if you have low conscientiousness; and

  * the three sources that fuel your personality.

### 2. Our first impressions are based on personal constructs, which also determine our behavior. 

Everyone knows the saying, "don't judge a book by its cover." But actually following this advice is not always as easy as it sounds. Over the course of any day, we regularly make judgements about other people based on our first impressions. 

And there's more than one kind of first impression. Imagine, for example, that you overhear a customer in a restaurant being very rude to a waiter. How would you feel? You might dismiss the customer as a tactless or abusive person, or you might construct a narrative explaining that he's just had a bad day at work and doesn't usually act so aggressively. 

Most people go for the easier explanation — he's just an inherently rude person. It's simpler to label people than it is to come up with stories that might explain their behavior. 

These quick judgements aren't objective, either. They're based on our _personal constructs_. 

The term "personal constructs" was first coined by the psychologist George Kelly as a way to describe the complex emotional lenses through which each person views the world. Personal constructs are different for everyone; they might prompt you to think of the restaurant customer as obnoxious, or think of him as authoritative and masculine.

Personal constructs also determine your behavior and ability to face challenges. The more limited a person's worldview is, the more difficult it is for them to cope with unexpected problems.

Imagine you have a friend who's unable to move on after a bad breakup, for example. If he becomes very pessimistic about humanity and starts viewing everyone he meets as untrustworthy, he'll have a hard time committing himself to anyone in the future.

### 3. Our personalities are defined by five major traits, including conscientiousness and agreeableness. 

Have you ever taken a personality test? There are many of them out there, but most psychologists consider the _Ten-Item Personality Inventory_ (TIPI) to be the most reliable.

The TIPI is founded on the belief that everyone's personality is based on the _Big Five traits_, which determine the way each of us reacts to the world around us. 

The first of the five traits is _conscientiousness_ : the ability to stay organized, dedicated and persevere with goals. According to the TIPI, people with a high level of conscientiousness often have a lot of academic and/or professional success. They know how to work hard, meet deadlines and avoid spending too much time on Facebook.

It's only possible to stay highly organized in predictable environments, however. People with high conscientiousness often struggle with things that are less structured, such as jazz music. 

The best jazz musicians know how to improvise and let their creativity wander. Psychologists have found that people enjoy jazz more when it's played by people with a low level of conscientiousness. Musicians who prefer rules and structure might be more suited to marching bands.

The next trait of the Big Five is _agreeableness_, which refers to how friendly, pleasant, supportive and empathetic a person is. 

Research has shown that agreeableness is the weakest indicator of professional success. People with a high level of agreeableness tend not to have very high salaries. 

But this doesn't mean that you will get ahead at work by being ruthless in interpersonal situations, and you're also not likely to advance your career if you're completely unfriendly. So, the ideal level of agreeableness in the workplace is likely somewhere in the middle.

### 4. Neuroticism, openness and extraversion are the remaining traits of the Big Five. 

After conscientiousness and agreeableness, the next major trait among the Big Five is _neuroticism_. Neuroticism determines how sensitive a person is to perceived threats and dangers. 

Our ancestors developed neuroticism thousands of years ago because it helped them survive in precarious environments. They needed to stay aware of threats, like saber-toothed tigers. 

Neuroticism was thus very important when humans were regularly threatened by predators, but it makes less sense in modern contexts and can actually push people into anxiety and depression. People with low levels of neuroticism tend to be the most stable and well-adjusted. 

The fourth essential trait is _openness_, which is the measure of how receptive a person is to new ideas, places and people. Openness is strongly connected to the arts. People with a high level of openness tend to be interested in music, film, travel or anything else that satisfies their curiosity.

So, openness is an important factor for success in anything that requires creativity and innovation. People who score low on openness, on the other hand, are less likely to branch out beyond their comfort zone. 

The last of the Big Five traits is _extraversion_. Extraverts are everywhere; they're the people who value external realities over internal feelings. 

As an example, the guy blasting his car stereo so he can show off his music taste to everyone is probably an extravert. In terms of, for example, friendships or work-related tasks, extraverts tend to favor quantity over quality, whereas the opposite is true for introverts. This can make it difficult for extraverts and introverts to coexist.

Imagine that an extravert and an introvert are colleagues at work and are assigned the same task — to produce a report. It's likely that the extrovert will finish faster, doing an acceptable job and making the fewest mistakes possible. In contrast, the introvert will probably work more slowly but will produce a more thorough report.

### 5. Free traits allow us to break away from the stable Big Five traits. 

A person's Big Five traits stay more or less the same throughout their life. _Free traits_, on the other hand, may change as a person gathers new experiences. 

Before we can understand how free traits work, though, we have to look at the three sources of personality, two of which are fixed and one of which changes. 

The first source of personality is _biogenic_, which refers to factors rooted in our genes and brain structures. We're hardwired with these traits from the start, which is demonstrated by the fact that even newborn babies have personalities! 

Some turn toward loud noises in the delivery room while others turn away from them — an early indicator of whether they're likely to grow into introverts or extraverts. 

The next personality sources are _sociogenic_, the traits you've been taught based on the environment in which you were raised. Children in East Asia, for example, are taught to blend in with their social groups, whereas American children are taught to stand out. 

The third sources, _idiogenic_ sources, are the ones that change. Idiogenic sources determine what we do with our daily lives, whether it's something simple like choosing to buy groceries, or committing to a cause like saving the environment.

When we work toward these goals, we break away from our stable traits and use _free traits_. Free traits can push you to do things that otherwise might be out of character. An introvert, for instance, might overcome her shyness and deliver a talk to a crowd of 200 students if she is passionate about her education.

Free traits help us complete goals we find meaningful, but they can be harmful if they push you to suppress other important traits. Psychologists have found that people who suppress certain aspects of their personality have more health problems. People who open up and stay true to themselves, on the other hand, tend to have stronger immune systems.

### 6. Your social behavior depends on whether you’re a high self-monitor or a low self-monitor. 

Some people always seem to have the same personality, while others seem to change immensely depending on who they're with. Why is this?

People change this way depending on whether they're a _high self-monitor_ (HSM) or a _low self-monitor_ (LSM). 

HSMs are strongly influenced by their social context. They're concerned about what others think of them, so they adapt to other people's expectations depending on the circumstances. You've probably met someone who is loud and open one day, but quiet and closed off the next. It can be difficult to figure these people out. 

LSMs, on the other hand, are less affected by their environments, and remain firmly guided by their own traits. You might have an LSM friend who doesn't act sillier or more serious depending on the situation — he's just always himself. He might even seem rigid!

Your romantic relationships are affected by whether you're an HSM or an LSM. Research has shown that when presented with information about potential romantic partners, HSMs paid more attention to their appearances, while LSMs were more interested in their personality traits. 

Because of this, LSMs are more likely to maintain stable relationships and HSMs are more likely to engage in affairs. 

So how do you know which category you belong to? The answer might be revealed by how you eat your steak!

Do you salt your meat before or after you taste it? A psychologist at Stanford University found that HSMs were more likely to taste the steak beforehand because they want to assess the actual condition of the steak. LSMs relied on themselves and their own salt preferences instead, salting it according to what they usually like.

### 7. Illusions can improve your well-being if you manage them correctly. 

How much control do you have over your life? Personality psychologists believe there are two main attitudes for approaching this question. 

Some people believe that they have a lot of control over their lives, and that luck and destiny play a small role. Others believe that their lives are mostly guided by external forces beyond their control.

Perceiving that you have control over your life has certain psychological and health advantages, which is why research has shown that nursing home residents are much healthier when they're involved in planning their daily activities. They stay happier and live longer as a result. 

But what about the _illusion_ of control? Does that make a person happier as well?

Everyone has some positive illusions about themselves. Most believe that their sense of humor is funnier or more active than the average person's, for example, even though it's not statistically possible for so many people to be above average. 

Even if they're false, some of these illusions are good for our mental well-being. Consider depressed people: depression arises partly from a heightened awareness that we actually have very little self-control — believing you have some control may help stave off depression.

This doesn't mean that we should blindly succumb to our illusions, however. Instead, it's important to _time_ them well. 

Illusions can lead you to make bad decisions when deciding on something important, like a job or a spouse. So it's best to be realistic and rational when weighing such choices. 

Then, once you're in a relationship or you've secured a steady job, a positive outlook can help keep negative thoughts away. Believing things are going well actually _can_ make things go better.

### 8. Your personality determines the way you deal with stress. 

Psychologists say that stress arises because of interruptions in a person's daily routine. Different personalities confront these interruptions in different ways.

A person's resistance to stress depends on their _hardiness_, which comprises three parts: _commitment, control_ and _challenge_.

In the 1970s, researchers conducted a study on a group of employees whose company was going through abrupt and difficult changes, to see how they each reacted to the stress. The people who dealt with it most effectively scored higher on commitment, control and challenge. 

They remained _committed_ to their everyday situations, even when those situations were in flux. 

They had a strong sense of _control_ over the changes that were occurring, instead of feeling passive or helpless. 

And finally, they considered the _challenge_ of the company's changes to be positive, focusing their attention on the new opportunities that these changes presented rather than seeing them as a threat.

It's possible for these traits to be _too_ strong, however. People who obsess over commitment, control and challenge suffer from more health issues, especially cardiac problems. 

By way of example, people with _Type A_ personalities are very ambitious and performance-driven, but their devotion to the three C's can come to hurt them. 

When Type A people are forced to give up some of their control, it hurts their self-esteem. They might become too committed to projects and turn away from other important things in their lives, or they can become unhealthily competitive. All of these traits can lead to high blood pressure and heart problems. 

All in all, the three C's can bring both great rewards and serious problems, so try not to succumb to aggression as Type A people often do. When you find the right balance of control, commitment and challenge, you'll maximize their respective benefits.

### 9. Final summary 

The key message in this book:

**Human personalities are very dynamic. While certain traits remain fairly stable throughout your life, others can shift and change your behavior as your priorities change. It's essential to keep a healthy balance of control, commitment and challenge, and make your positive illusions work to your benefit.**

Actionable advice:

**Pursue your passions.**

Get involved with projects you truly care about, and pick ones where you're likely to be successful. Finding meaning in life is an important part of being happy. 

**Suggested further reading:** ** _Personality_** **by Daniel Nettle**

Anyone can see that people have different personalities, but what exactly causes this? This book examines the factors that influence personality based on the research of numerous psychologists. You'll learn about the core traits that determine a person's overall character, the different strengths and weaknesses they have, and how to get the most out of your own personality.
---

### Brian R. Little

Dr. Brian Little is a psychology professor who has taught at Carleton University, McGill University, the University of Oxford and Harvard University. He specializes in personality psychology.

