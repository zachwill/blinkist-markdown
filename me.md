---
id: 5e2dcb8b6cee07000715f99a
slug: me-en
published_date: 2020-01-30T00:00:00.000+00:00
author: Elton John
title: Me
subtitle: None
main_color: None
text_color: None
---

# Me

_None_

**Elton John**

_Me_ (2019) is Elton John's autobiography. These blinks reveal the singer's troubled childhood and his struggles with addiction. They also explore his path to stardom, and the celebrity drama he's encountered on the way.

---
### 1. What’s in it for me? Take a tour down the yellow brick road of Elton John’s life. 

Elton John is one of the world's most popular singer-songwriters. But how did this ordinary working-class boy become an icon? You might have read the headlines about his private life, but what's the truth behind the tabloid fiction?

Charting Elton's rise from humble beginnings to pop wannabe to global megastar, these blinks reveal the man behind the legend. Packed with outrageous stories and celebrity gossip, this is a whistle-stop tour through the rocket man's most dramatic moments. So if you want to know about the hidden side of super-stardom, then look no further. From Michael Jackson to Princess Diana to Iggy Pop — you'll discover what really goes on when the cameras stop rolling.

In these blinks, you'll learn

  * what it's like to party with the Royal Family;

  * how Elton got sober; and

  * why Iggy Pop threw Elton off stage.

### 2. Elton John had an unhappy relationship with his parents. 

Elton John has hundreds of millions of dollars in the bank. He's also got a back catalogue that's bulging with hit records. But before the fame and fortune, Elton was a regular, working-class boy. He was born in 1947, and raised in social housing in the London suburbs. And he didn't have any adoring fans back then, either. In fact, Elton spent his early years without much love or affection.

**The key message in this blink is: Elton John had an unhappy relationship with his parents.**

When Elton was a kid, he spent most of his time trying to avoid a spanking.

His father, Stanley, punished him for everything. From the way he took off his school blazer to the way he ate celery, Elton could never get anything right. And he was terrified of his mum, Sheila, too. Not surprising, considering she potty trained him by beating him with a wire brush. She hit Elton until she drew blood. He was just two years old.

Stanley and Sheila fought with each other, too. They separated when Elton was eleven, but Elton believes they should have split up much sooner. But, in 1950s England, divorce was a dirty word.

After his parents split, Elton's father remarried and had more children. Stanley was warm and loving toward his new family. But his cold, critical attitude toward Elton never changed. When Elton became successful, his dad never congratulated him. He never bothered to see him play live, either. When Stanley died in 1991, the two hadn't spoken for years. Elton didn't go to his funeral.

Elton did keep in touch with his mother, but she was controlling and volatile for the rest of her life.

Later in life, Sheila turned her temper on Elton's household staff. She screamed at his cleaners and humiliated his housekeeper. She went through Elton's receipts too, trying to find out what he was spending. Even though her son was a multi-millionaire, she berated him for buying gifts for his friends.

Sheila's worst outburst happened in 2005, on the day Elton married his long-term boyfriend, David Furnish. His mother told the other wedding guests that she didn't agree with same-sex marriage. Looking back, Elton believes her resentment didn't stem from homophobia. She was just angry that David would now have more influence over her son's life than she did.

Once he became famous, Elton could buy almost anything he wanted. But he could never purchase his parents' approval.

### 3. Young Elton had a talent and a passion for music. 

Elton's childhood was full of sadness, but it was filled with music, too. In fact, Elton's family remembers that he could pick out a song's melody, just by hearing it, and then play it on the piano. You might not think that's too remarkable. Lots of people can play the piano by ear, right? Well, maybe. But Elton could do it when he was _three years old._

**The key message here is: Young Elton had a talent and a passion for music.**

As a kid, Elton was obsessed with pop music. If he unwrapped his presents on Christmas morning to find toy cars or building blocks, he'd be incredibly disappointed. The only presents he ever wanted were 78 inch records — and he religiously saved up his pocket money to buy them.

When Elton was nine, music shook his world. Standing in the local barber shop, he saw a picture in a magazine. It was a photo of Elvis Presley. Elton thought he looked like an alien from outer space — he'd certainly never seen anyone who dressed or posed like that in suburban London.

When his mom came home with Elvis's new record, "Heartbreak Hotel," Elton thought he _sounded_ like an alien too. But in a good way. From that moment on, Elton was in love with rock and roll. 

But there was just one problem. Elton loved the sounds of Elvis Presley and Jerry Lee Lewis, but his father hated them. Today, rock and roll seems tame in comparison to, say, gangster rap or heavy metal. But in the 1950s, it was shocking. In fact, this new music, squarely aimed at young people, caused a moral panic. Like most of the older generation, Elton's father worried that his son would become a criminal, or a gangster, simply by listening to Buddy Holly and Little Richard.

Elton's dad turned out to be wrong about that. But rock and roll did have one rebellious effect on Elton. It made him reluctant to attend his classical piano classes.

He took these classes every Saturday morning at the Royal Academy of Music. The Academy was a prestigious institution, and the eleven year old Elton had passed a tough exam in order to study there. But sometimes, instead of going to his lessons, he rode the subway all morning. On the train, he read magazines, and daydreamed of playing rock and roll instead of Mozart and Beethoven.

> _"I was just born with a good ear, the way some people are born with a photographic memory."_

### 4. Elton worked for years to get his big break. 

Can you pinpoint a week when your life changed? Elton John can. The year was 1970. The _Elton John_ album had just been released, and it was getting rave reviews. Elton arrived at Los Angeles airport and found a red London bus waiting outside. On the side it said, "Elton John has arrived!"

A few days later, Elton played the famous Troubadour nightclub. Rock and Roll legends that Elton had dreamed of meeting, like The Beach Boys and Neil Diamond, came to watch him play. The next day, the LA Times newspaper declared that rock music had a new star.

In the eyes of the world, it looked like Elton John was an overnight success. But the reality was very different.

**The key message here is: Elton worked for years to get his big break.**

In fact, Elton got his first gig when he was 15 years old. The venue was a local pub, and Elton regularly played old English drinking songs on the piano. Afterward, the customers gave him tips in a pint glass. But it wasn't always an easy job. That's because the customers enjoyed fighting more than they enjoyed the piano. Once everyone got a bit drunk, Elton often had to abort his performance and climb out the window to avoid being caught in the chaos. It was certainly a crazy beginning to Elton's career!

And violent pub gigs weren't Elton's only early venture. When he was 17, Elton left school and joined a band called Bluesology. The band spent most of their time on the freeway, playing venues up and down the UK. Sadly, Bluesology had little success. The band did release two singles, both written by Elton, called "Come Back Baby" and "Mr Frantic," but neither of them made a real splash.

Unsurprisingly, Elton wasn't making a lot of money with Bluesology. So he started working as a session musician, too. This involved going into a studio and recording terrible covers of current pop songs. Sometimes this work was hilarious. Once, Elton was asked to record a version of "Young, Gifted and Black," a song that doesn't quite work when sung by a white kid from suburban London. Another time, he had to impersonate the high-pitched voice of Robin Gibb from the Bee Gees. The only way Elton could do it was by half-strangling himself while he sang.

These experiences all played a part in Elton's success. Despite being risky, those pub performances made him a brave performer. And all those Bluesology gigs taught him how to put on a good show.

### 5. Elton met his lyricist completely by chance, and it changed his career forever. 

It might be a cliché to say that every cloud has a silver lining, but Elton John knows it's true. In 1967, Elton had his first solo audition for a music label. And he failed, miserably. Now, 50 years later, Elton knows that if he'd passed that audition, he never would have met Bernie Taupin.

**The key message here is: Elton met his lyricist completely by chance, and it changed his career forever.**

Although the record label didn't sign him that day, Elton didn't go away empty-handed. Just before he left, the music executive picked up an envelope from his desk and gave it to him.

The envelope contained song lyrics — and Elton's future.

Bernie Taupin was one of hundreds of young hopefuls who had sent his lyrics to the record label. That afternoon, the executive picked up Bernie's envelope to give to Elton, but he could just have easily given him one of the others that were stacked on his desk.

Elton was intrigued by Bernie's lyrics. His words were esoteric and haunting — filled with emotional depth. Certainly a lot better than anything Elton had written. When they met in person, Elton was impressed. Bernie was handsome and sophisticated. The only surprise was that, when he wasn't writing lyrics, Bernie worked on a chicken farm in a quiet Northern town. Not exactly glamorous.

In 1968, Bernie and Elton moved in together. Bernie wrote lyrics on a typewriter and then gave them to Elton, who set them to music. In the beginning, it didn't go well. After months of trying and failing to write songs that would interest record labels, Elton and Bernie moved in with Elton's mom. They shared Elton's childhood bedroom and even slept in bunk beds.

These were the humble beginnings of a musical partnership that has lasted 50 years and spawned dozens of hit records.

Everything changed one morning in 1969, when Bernie thought of the lyrics to "Your Song" while he was eating breakfast. He wrote them down and handed them over. Incredibly, it took Elton just 15 minutes to write the music for them. Soon afterward, a record label gave them 6,000 pounds to make an album — a huge amount of money at the time. That album turned out to be the _Elton John_ album, which would prove to be a hit. In 1970, it was nominated for a Grammy Award.

> _"There was just a magic that happened when I saw Bernie's lyrics, that made me want to write music."_

### 6. Elton’s been in some bizarre social situations as a result of his fame. 

Imagine Bob Dylan coming to your house party. Now imagine being so high on cocaine that you don't recognize him. Instead, you mistake Bob for one of your gardeners — a gardener who is trying to gatecrash your party! Now picture yourself shouting, in front of all your guests, that this gardener should leave immediately!

Unfortunately, this situation isn't imaginary; it really happened to Elton John. 

**The key message here is: Elton's been in some bizarre social situations as a result of his fame.**

Consider the quiet lunch party that Elton hosted in the 1990s so that his mom could meet his new boyfriend, David Furnish.

One of the lunch guests was a psychiatrist. At the last minute, he asked if one of his patients could join them. That patient turned out to be Michael Jackson. Michael arrived, covered in badly-applied makeup and insisted that the whole party sit indoors with the curtains closed. After remaining almost completely silent throughout the lunch, he disappeared. He was found two hours later — playing video games with the housekeeper's son.

Or there was the time when Elton surprised Iggy Pop by dressing up as a gorilla and running on stage at one of Iggy's shows. Unfortunately, Iggy Pop was so high that he mistook Elton for a real gorilla, and ran away screaming. A moment later, Elton was flying through the air. Iggy's security guard had panicked, and thrown him off the stage.

These are crazy stories, but when royalty and Hollywood royalty collide, things get even stranger... 

Elton found out when he invited Princess Diana, Sylvester Stallone, and Richard Gere over for dinner. Unfortunately, when Elton and his dinner guests sat down to eat, they found that Stallone and Gere were absent. Instead, they were in the hall, about to start a fistfight. Apparently, each thought the other one was hogging Princess Diana's attention. In the end, Stallone left in a rage. Diana seemed to prefer Richard Gere.

But Elton's most surreal experience was at Windsor Castle, where he attended Prince Andrew's twenty-first birthday party. Unfortunately, the DJ was so worried about offending the Queen that he turned the music down as low as it would go. In the middle of this strange silent disco, Her Majesty herself appeared, and started dancing with Elton. But instead of enjoying the experience, Elton had to concentrate on barely moving, so that the creaking floorboards wouldn't drown out the music.

As he shuffled around the dance floor, Elton wondered how an ordinary working-class kid had ended up dancing with royalty.

### 7. Elton was addicted to alcohol, drugs, and food. 

Once his career took off, Elton John enjoyed success, fame, and fortune. From the outside, his life looked perfect. But inside, he was battling some powerful demons.

**The key message here is: Elton was addicted to alcohol, drugs, and food.**

In the 1970s, Elton had the world at his feet. But deep down, he still felt like the shy little boy who was afraid of his parents.

So when he tried a new drug called cocaine, Elton thought he'd found the answer to his anxiety. Suddenly, he was confident and euphoric, and his shyness evaporated.

But the combination of cocaine and alcohol made Elton destructive. After one night of heavy substance abuse, he trashed his personal assistant's hotel room. In the morning, when his assistant showed him the damage, Elton demanded to know what had happened. "You happened!" was the reply.

Drugs made Elton irresponsible, too. Once, he awoke from a cocaine binge to hear his phone ringing. The person on the other end was calling to arrange delivery of his new tram carriage. Apparently, it would require two helicopters to lower it into his garden. Elton had no memory whatsoever of buying it.

Sadly, Elton was also addicted to food. Terrified of becoming fat, he became bulimic instead. Often he binged on tubs of ice cream before making himself throw up.

In July 1990, Elton finally decided to get help. Just before this, he'd locked himself in his house for two weeks straight, with only whisky, drugs, and pornography for company. Every five minutes, he took another line of cocaine.

Elton decided to go to rehab and went to a clinic in Chicago. But after six days, he walked out again.

The reason was simple. The clinic expected the patients to make their own beds and wash their own clothes — all things that Elton paid other people to do for him. Faced with the embarrassment of not knowing how to use a washing machine, he checked himself out. But by the time he got to the car park, he'd changed his mind. Where was he going to go? Back to the drugs and the pornography?

He went back inside, and finally got sober. He even enjoyed the experience. For the first time in years, he felt like an ordinary person. In rehab, he wasn't a megastar, he was just another addict — like everyone else in there.

Elton is still sober today, and he's helped other celebrities get clean too. When singer Rufus Wainwright was addicted to crystal meth, Elton persuaded him to go to rehab.

### 8. Elton has enjoyed an eclectic and surprising ride through the music industry. 

In October 1976, Elton John looked out at a crowd of 55,000 screaming fans. He was playing Dodger stadium in Los Angeles. The weather was perfect. Elton knew his performance had been perfect too. In fact, the whole trip was going pretty well. The city of Los Angeles had declared that week "Elton John Week." Two days earlier, he'd unveiled his new star on the Hollywood Walk of Fame.

During that perfect show, Elton had a realization: this was the peak of his career.

**The key message here is: Elton has enjoyed an eclectic and surprising ride through the music industry.**

Since then, Elton's sold a lot of records, but he still believes his career peaked on that afternoon in 1976. And frankly, it doesn't bother him all. Why? Because he has always understood that phenomenal success doesn't last forever.

Rather than trying to recapture the hype of the mid-seventies or top his career high, Elton has branched out instead.

In the 1990s, for example, he wrote the music to Disney's The Lion King. He did have some doubts about composing songs about a flatulent warthog, but the soundtrack was a major success. Elton even took home an Academy Award for his song "Can You Feel the Love Tonight."

The most extraordinary project he's been involved with though, was Princess Diana's funeral. It was also the saddest project. Elton and Diana were good friends, and her death was a terrible shock. Days before the funeral, Diana's family asked if Bernie could rewrite the lyrics to "Candle in the Wind." Elton performed this new version during the service, in front of a TV audience of billions.

But Elton wasn't just grieving during his performance, he was terrified too. What if he sang the wrong words? The original lyrics talked about Marilyn Monroe being found naked and mentioned "sexual feelings." Luckily, it went smoothly, but Elton never performed Diana's version after that day. In fact, for years afterwards, he refused to even play the original song at his shows.

Elton John's lived an incredible life, and he's had his share of pain and insecurity too. These days, Elton's a happily married father of two boys. Finally, he's surrounded by love at home, as well as on the stage.

### 9. Final summary 

The key message in these blinks:

**Elton John is known for providing the soundtrack to our lives for nearly 50 years. But alongside the fame, Elton has lived his life to the tune of addiction and anxiety. In an era of reality TV and fifteen-minute celebrities, his story is a refreshing antidote. Though, Elton didn't succeed quickly, or easily. In fact, his success was years in the making.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _The Polymath_** **, by Waqas Ahmed**

Elton John is a man of many talents — a _polymath_. In our next recommendation, you'll discover the qualities that all polymaths share, and learn how to become one yourself.

_The Polymath_ explains how hyper-specialization is stifling people's development and limiting their creativity. Explaining how our educational and professional structures can be reconfigured to reflect our innate human potential, these blinks reveal your capacity for learning and working in ways you never thought possible. So to discover your many hidden talents, check out the blinks to _The Polymath._
---

### Elton John

Sir Elton John, Commander of the Most Excellent Order of the British Empire, is an English singer, songwriter, and composer. He's one of the world's best-selling music artists, and has sold over 300 million records. John has also received five Grammy Awards, five Brit Awards, and an Academy Award.

