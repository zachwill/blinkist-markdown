---
id: 56b8ab65d119f9000700002a
slug: meaningful-en
published_date: 2016-02-10T00:00:00.000+00:00
author: Bernadette Jiwa
title: Meaningful
subtitle: The Story of Ideas That Fly
main_color: EFCC30
text_color: 806D19
---

# Meaningful

_The Story of Ideas That Fly_

**Bernadette Jiwa**

_Meaningful_ (2015) is a guide to making customers central to your business. These blinks, by teaching you how to produce a product that truly matters to and empowers your customers, will perfectly align your brand with the demands of the twenty-first century.

---
### 1. What’s in it for me? Learn which ideas, products and services your customers will connect with! 

What is a company without customers?

Exactly — it's nothing.

However, most companies don't have a very good understanding of what it takes to connect with, and thereby retain their customers. For these companies, marketing and product development are still about creating a need for a product that the company has already produced. 

But some companies have a different way of connecting with customers: finding out what they need and what matters to them. By understanding their customers, such companies are able to lead innovation and brand awareness, maximizing customer satisfaction and creating a meaningful relationship for both company and customer.

In these blinks, you'll find out

  * how pictures of Jennifer Lopez's gave rise to Google Images;

  * what fatal misstep Apple made at the launch of the iPhone 6; and

  * that Mary Anderson understood customer connection more than a century ago.

### 2. Stay relevant to your customers by understanding what they really want. 

Tons of new products and services enter the market every day. And some of them are the result of a truly innovative idea: those products and services that make your life easier, more enjoyable and, simply put, better. But a great product doesn't guarantee customer satisfaction.

In fact, the customers of today want products and services that truly take their needs seriously. It hasn't always been like this. In the past, a taxi driver wouldn't worry too much if a customer was grossed out by his cab, because it was unlikely that the two would ever meet again. Bad customer experiences didn't affect business.

But with apps like Lyft and Uber, that's all changing. 

That's because Lyft measures how long your ride lasts, who drove you and what your experience was like. As a result, the focus is put on the quality of the ride, and the customer's voice is absolutely essential. 

So, what does this mean for business in general?

That it's key to stay relevant to your customers. And that means tracking their behavior and gearing your product or service toward their way of life. A great example is the way Google developed Google Images. 

It began as a search engine just like countless others, but became a household name by showing users not just what they search for, but what they want. So, Google's success wasn't about tech-savvy developers, but about carefully observing customers and intelligently using the knowledge this observation afforded. 

For example, at the 2000 Grammy Awards, Jennifer Lopez donned a see-through Versace dress with an open neckline. The dress became so famous that it got its own Wikipedia page, but also presented a problem that Google had to solve. The dress rapidly became (up to that point) the most googled thing ever; people were dying to see photos of it. Hence, the birth of Google Images.

> _"Our task is to read things that are not yet on the page." - Steve Jobs_

### 3. Produce successful products by putting yourself in your customers’ shoes – or at least watching them take a few steps. 

The digital age and the dawn of the internet have made opportunities a dime a dozen; everyone has the chance to innovate, as well as connect with others, all over the globe. So why do some companies still totally outshine the competition? One word: empathy. 

Innovation isn't just about making new, improved products. It's about designing things that truly matter to the people who use them. 

Just take Apple. It may seem like the company is driven by design, but Apple's true strength lies in its customer-driven business model. For example, when they were developing the Apple Watch, the company spent two years brainstorming how a smartwatch could improve the lives of users. As a result, the hardware and software were built based on how customers would use the watch in real life.

How could Apple predict that?

The team observed the reactions of people wearing the product to gather and prioritize information on its design. For instance, say you get a notification that you've got a new text message. Depending on how long you hold your wrist up, the watch will either display the message or leave it unread. 

This useful feature was easy to include and it didn't cost billions of dollars, either — just the simple observation of a customer in action. However, plenty of businesses assume that if _they_ think their ideas sound great, their customers will, too. In fact, Apple is a great example here as well, because everybody makes mistakes:

Think back to the launch of the iPhone 6, a release that prompted the automatic transfer of U2's new album onto the iPhone and iTunes account of every iPhone owner. The worst part was, you didn't even have the option to delete it!

The problem?

Apple thought it was giving a generous gift, but failed to empathize with the customers who mostly saw it as an intrusion.

### 4. Customers don’t just buy a commodity. They buy a value system. 

So, customers are looking for something more and their options are more abundant than ever. But how can your business deliver what your customers want?

Actually, all successful companies share a common characteristic: they connect emotionally with their customers. Customers are no longer the passive consumers they used to be; they're partners, co-creators and community members who want to engage with the companies they buy from. They want brands that will commit to _them_. 

For instance, the clothing company Patagonia has 45 full-time employees who make about 30,000 customer repairs every year. In 2015, a team of them even took a road trip across America to repair the "tired and well-loved" clothing of their customers. 

That's because Patagonia's philosophy is to make high-quality clothing that lends itself to repair. The company knows how important it is to its customers that the clothes they're used to, articles that have seen countless other adventures, are made to last. 

In fact, spending money on a repair staff actually makes good sense for Patagonia's business. Success in the market of today isn't about selling commodities. It's about making a meaningful _connection by attraction_. That means customers want to shop with companies that hold their values, not just the ones whose products they like.

But it hasn't always been that way. For instance, when the author was growing up in Dublin, store-bought cakes were considered a luxury, and food companies like Mr Kipling maintained dominance through conventional advertising strategies. 

The ads told customers that Mr Kipling made "exceedingly good cakes" — and the customers believed them. It was a time when people didn't really question products. Today, however, people want to know what's in the food they're eating and how it will affect their health. 

So, when the company's sales began slowing down in 2014, Mr Kipling spent over £10 million on a new package design and clearer nutritional information that they put on the front of every package. In other words, they tapped into what their customers wanted.

### 5. Innovation means solving life’s hidden problems. 

In the year 1902, a woman named Mary Anderson visited New York. Like most people of her time, she toured the city on a streetcar. One rainy day, Mary noticed how the road devolved into chaos as the drivers' windshields became increasingly difficult to see through. 

So, though many people assume that windshield wipers were always a standard automotive feature, they actually came from Mary Anderson's determination to solve this _invisible problem_. This term was coined by Tony Fadell, CEO of Nest, the thermostat and alarm company. It describes problems that we run into every day — so often that they become normalized and we don't even see them as problems.

Why are invisible problems so key?

Because without them, there would be no innovation.

Consider the story of Nick Woodman, a recent innovator. He couldn't find a camera good enough to capture his and his friends' surfing exploits. Unbelievably, name brands like Sony, Canon and Panasonic simply couldn't deliver what Nick wanted. To get the kind of shots he was looking for, it was necessary to have another pro surfer in the water taking footage. 

So, Woodman set out to design a wearable camera that wouldn't bother him while he surfed but could shoot high-quality angle footage of his session. Sound familiar? It might, because that's how the GoPro was born. 

GoPro raked in $150,000 in revenue the year it was launched and its sales doubled every year. In 2012, 8 years after the first GoPro hit the market, the company had grossed $512 million. 

It just goes to show that creating solutions is largely a matter of discerning problems. Because finding meaningful solutions means considering the real issues and limitations that people face.

> _"The best — maybe the only? — real, direct measure of 'innovation' is change in human behaviour."_ _\- Stewart Butterfield, CEO, Slack_

### 6. Understand your customer’s perspective and create a story that resonates with it. 

OK, so your customers' feelings and problems are important, but to truly innovate you'll need to understand more than that. You've got to understand how customers will actually use your product. That means learning about their _worldview_ — the way they perceive things based on experiences, beliefs and culture. 

For instance, IKEA took 6 years to open its first store in South Korea because they knew it was essential to first understand the unique culture of the country and the worldviews of its citizens. In fact, IKEA's 360 stores worldwide vary tremendously from country to country. 

It only makes sense, because while a Japanese bedroom might feature traditional tatami mats, an American one would be more spacious and have lots of pillows on the bed. The result is that the company's catalogue is available in 67 versions and 32 languages, each one conforming to a particular worldview!

But how can you utilize _your_ customers' worldview?

By building a meaningful story that resonates with it — an easy task when you follow the author's _story strategy blueprint_ :

First off, go beyond basic demographics, like sex, age and income. To get closer to your customers' worldview, you'll need to learn things like how they spend their time and what invisible problems they encounter. The best way to do this is by communicating with your customers every day. These days, that's not such a hard task: information technology makes it a cinch to gather data!

Next, you'll have to brainstorm how you can improve your customers' lives and what exactly it is that they want. This process is likely to uncover important ideas for your product or service. 

The third step is to use these insights to determine what type of product, service and marketing style your customers will most respond to. Just think about what features and capabilities are necessary to solve your customers' problems. 

And finally, make sure you have a clear idea of what the customer should _feel_ like when using your product or service.

### 7. Final summary 

The key message in this book:

**Building a meaningful business is all about understanding customers — their perspective and what they expect from you. By directly consulting customers you can produce a product that makes a difference and build a compelling story around it.**

Actionable advice:

**Collect soft data.**

Tons of companies, from Google to Amazon, gather huge quantities of hard data every day that helps them understand their customers and deliver exactly what they want. But soft data is also essential — feedback, stories and even body language. For instance, Black Mill Clothing is always sure to consult user preferences on Pinterest before releasing a new collection. 

**Suggested** **further** **reading:** ** _Difference_** **by Bernadette Jiwa**

In _Difference_ (2014), author Bernadette Jiwa explains how you can stand out and stay competitive in today's riotous business world. To cut through the noise of advertising everywhere, you have to connect with people emotionally and offer them something _more_. This book shows you how!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Bernadette Jiwa

Bernadette Jiwa is a brand-story and marketing strategist. She's written an Amazon bestseller, and offers consulting to entrepreneurs, companies and business leaders, setting them on a path toward producing value for their customers.

