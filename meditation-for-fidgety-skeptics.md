---
id: 5ade2852b238e10007596e7e
slug: meditation-for-fidgety-skeptics-en
published_date: 2018-04-26T00:00:00.000+00:00
author: Dan Harris, Jeff Warren and Carlye Adler
title: Meditation for Fidgety Skeptics
subtitle: A 10% Happier How-To-Book
main_color: F03552
text_color: BD2A40
---

# Meditation for Fidgety Skeptics

_A 10% Happier How-To-Book_

**Dan Harris, Jeff Warren and Carlye Adler**

_Meditation for Fidgety Skeptics_ (2017) is a down-to-earth beginner's guide to meditation — especially for those skeptics among us who think that meditation is a lot of new age hooey. Dan Harris presents readers with a firsthand account of how even people with restless minds and no time to spare can start experiencing the scientifically proven benefits of meditation.

---
### 1. What’s in it for me? Learn from a skeptic that there are some very real benefits to meditation. 

Over the past decade or so, meditation has ceased being a practice strictly for monks and hippies. Today, it's an almost mainstream activity. More and more doctors and psychiatrists are recommending it for their patients, and even movie stars, singers and celebrities have started recommending their favorite meditation apps.

But maybe you're still skeptical, or maybe you think you're too much of an impatient worrier for meditation to work for you. Well, that's pretty much what author Dan Harris thought, too. He felt certain that he was too fidgety for meditation, but now he's here to tell you that such excuses are total nonsense.

With a little time and practice, anyone can appreciate the benefits of meditation. And the benefits extend far beyond just easing your stress levels; they include helping your immune system as well as your ability to focus on day-to-day work. The truth is, meditation is easy to get into. All it takes is stopping every once in a while to take ten deep breaths, and you'll be on your way toward forming a lifelong habit of self-improvement.

In these blinks, you'll discover

  * how an on-air meltdown can be an important wake-up call;

  * which US congressman is spreading the joy of mindfulness; and

  * the difference between the RAIN and STOP meditations.

### 2. Everyday life can be stressful, and the scientifically proven remedy is meditation. 

There are still a lot of fast-paced, city-dwelling professionals who think that meditation is a lot of new age nonsense. The author, Dan Harris, understands this feeling rather well since, not long ago, he was one of those people. If someone had told him in his twenties that one day he'd be promoting the benefits of meditation, he'd have laughed so hard that beer would have shot out his nose.

But life is full of unexpected twists and turns, some of which can be brought on by times of extreme stress.

For Harris, a big change came in 2004, when he was working as an anchorman for ABC News. While live on air, with an audience of five million viewers, the author suddenly began to stutter and slur his words — he was in the grip of a panic attack.

Like most panic attacks, however, this incident wasn't an isolated event. In Harris's case, it was the result of years spent reporting from war zones, the horrors of which had led him to sink into a state of depression. Yet, being the busy man he was, he'd failed to recognize or appreciate the symptoms he'd been experiencing.

These included trouble getting out of bed in the morning and a feverish feeling that never fully went away. After a brief attempt at self-medicating with cocaine and ecstasy, the on-air meltdown was Harris's personal rock bottom.

But this turned out to be a good thing. It was the wake-up call Harris needed to make some much-needed changes in his life. And it's what led to him discovering the benefits of meditation.

Like a lot of people, Harris was what you'd call a "fidgety skeptic" about meditation. But after doing some research, he was surprised to find a wealth of solid, scientific evidence showing the practice to be a proven stress reducer. It's benefits included lowering blood pressure and helping people recover from anxiety, depression and post-traumatic stress disorder (PTSD).

Harris also found legitimate data indicating that meditation both strengthens the immune system and rewires the neurons in the brain to improve attributes like self-awareness, endurance and compassion.

### 3. Meditation is about finding inner peace by focusing on a sensation that works for you. 

No matter what kind of meditation you're doing, the first step is always the same: getting yourself comfortable.

So take a moment to get yourself situated in your favorite chair and take a few deep breaths as you straighten out your spine and your posture.

It's good to start off easy, with a five- to ten-minute meditation. Remember, this practice is about finding peace of mind — so the most important thing to have, especially when you're starting out, is a positive, easy-going attitude. That means leaving behind any competitive feelings or preconceived notions of right or wrong.

As a beginner, you might find it difficult to sit quietly and peacefully. You might get annoyed by some noisy neighbors or easily distracted by negative thoughts. If this happens, don't give up. Remember, there's no right or wrong experience, and if you're fidgety, as Harris was at first, it doesn't mean meditation isn't for you. So forget your expectations of what meditation is supposed to be and just try to stay calm and open.

The next thing to understand is that meditation is about focusing your attention on a single sensation.

The most common and traditional sensation is the act of breathing. So, without changing anything about your normal rate of breathing, just gently focus your attention on your in-breath and out-breath. Focus on how the air enters through your nose, moves to your lungs and how your chest or stomach expands and contracts. Or make your focus more general, and simply pay attention to the rhythm of your breathing.

While you're in this zone, try to enjoy the experience and think of each breath as something to be savored, the same way you might enjoy each bite of a tasty meal or each sip of a delicious beverage.

To maintain focus, you can also count or "take note" of each breath, by silently saying "in" for each inhalation, and "out" for each exhalation.

But it doesn't have to be breathing. You can focus on whatever sensation works for you, whether it's the sound of a ticking clock or the sight of a candle's flame, the feeling of your body against your chair or your feet against the floor.

Whatever you choose to focus on, keep it up for five to ten minutes.

### 4. Get started with short meditations, which can easily lead to longer ones. 

One of the most common concerns Dan Harris hears about meditation is the amount of time it requires. Many people are under the impression that there's just no way they'll be able to find time to meditate in their hectically busy schedule.

But contrary to what you may think, forming a daily meditation practice doesn't have to require more than a couple minutes here and there. In fact, just 60 seconds can be enough to get your practice started.

One of the most difficult things about meditation is making it a daily habit, which is extremely important if you want to experience the full benefits. So, if all you have is one minute today, then use that minute and be proud you accomplished it.

If you really think about it, you should be able to find moments in your day where you can allow yourself a meditation minute. How about the moment after you brush your teeth? Or after your first cup of coffee? A lot of people find that their commute on the train or bus is the perfect time to close their eyes and meditate for a minute or more.

Here's a helpful guide to make the most of a one-minute meditation — it's called the _ten breaths meditation_, and it's super easy. Wherever you happen to be, just silently count along as you take ten long, deep breaths in… and out…

This exercise will instantly help shift your attention away from whatever worries or preoccupations you may be experiencing at the moment.

Most people find that these short meditations are gateways to finding the time for the longer ones.

While it might seem impossible to fit in a ten-minute meditation at first, once you get going you might find yourself _wanting_ to make room in your schedule for longer sessions. One minute today might turn into five minutes tomorrow, and, a couple of weeks later, who knows — you might be enjoying ten minutes of peace in the morning and the evening.

If this happens, it's a great sign, since it means you're moving from _extrinsic_ motivation, which is you thinking, "I _have_ to meditate," to _intrinsic_ motivation, or thinking to yourself, "I love meditating!"

Naturally, being intrinsically motivated means you'll be more likely to make this a regular part of your life.

### 5. It’s been an uphill battle to get mainstream acceptance, but meditation has a lot going for it. 

Have you avoided meditating because you were worried about what your friends might think? This is another common concern the author has encountered. But here's the truth: meditation has become far more popular than you may know.

It's been a long, uphill fight to turn meditation into a mainstream practice in the United States, but, in recent years, attitudes have finally begun changing.

For example, the conservative worlds of business and politics have long held out against meditation. But, even here, things are shifting.

On Washington's Capitol Hill, Congressman Tim Ryan has written a book called _A Mindful Nation_, which promotes the benefits of meditation. Ryan praises the practice, saying that it's not only good for individuals, but also, potentially, for the nation — that it could lead to better governmental policies on a range of issues, including education, health insurance and military training.

Ryan sees mindfulness and meditation as something both schoolchildren and soldiers could benefit from. And Ryan has walked his talk, too. He started a meditation group near Capitol Hill and convinced a public school in his home state of Ohio to be among the first to conduct mindfulness exercises in the classroom.

While Ryan did encounter some resistance, he's collected a wealth of comebacks and statistics to counter just about any argument.

To the parents who worry that meditation involves some kind of spiritual or religious element, Ryan points out that mindfulness is not forbidden by any religion, nor is it inherently spiritual to begin with. He also shows parents the compelling scientific evidence of how beneficial it can be for the development of healthy brains.

Ryan also cites the long list of diverse, high-functioning, successful people who practice meditation, from tennis champion Novak Djokovic to Steve Jobs. If someone expresses doubts, it's likely there's a public figure they respect who's practicing meditation, and this makes it much harder for them to maintain their skepticism.

So, with any lingering doubts put to rest, let's explore some more advanced practices.

> _"I think in the future, mental exercise will be considered as important and mainstream as physical exercise."_

### 6. Meditation is a great way to let yourself be lazy and connect with a companion. 

If someone recommended a _two-hour meditation_, your first thought might be to politely say, thanks, but no thanks. But, as the saying goes, "don't knock it until you've tried it" — because this could very well be your new favorite exercise.

Basically, a two-hour meditation is your free pass to live a life of leisure for a while.

Unlike a typical meditation, this one works by lying on your back — either on the floor, in a bed or on the couch. If you want, you could even have the TV on or music playing; there are basically no rules.

To begin with, close your eyes, take some deep breaths and enjoy the sensation of your body gradually relaxing. Now, imagine yourself sinking into the floor, couch or bed, and feel yourself letting go of _everything_. You can slowly lift your arms and let them flop back down beside you to encourage this sense of letting go.

Continue clearing away any worries or stress for the next two hours. If you happen to fall asleep, don't worry. That's not against the rules. And if your mind wanders, don't panic. Just bring it gently back to your sensation and continue to let go.

This meditation can also be done with a companion, be it a pet, a close friend or a partner.

Lying on your backs, snuggle up next to your companion and use your proximity to this other body as a way to heighten your focus on your own sensations. During your meditation, you can even focus on that connection to your partner's body, or on the slight movements caused by your companion's breathing.

This is also a great meditation for cultivating a deeper sense of compassion and love toward your companion, as well as toward yourself.

As with any meditation, you shouldn't be forcing any feelings. Just be open and welcoming to them, and if you do feel compassion and love during the session, gently register what this feels like by making a mental note.

While this meditation normally lasts two hours, it can be as long or short as you'd like it to be. What's important is that you're being kind to yourself and creating a space where it's okay to let go of the anxious world around you.

### 7. Meditation provides you with the tools to deal with your emotions. 

When you feel unwanted emotions, do you run away from them, bottle them up or try to pretend they don't exist?

Well, part of what makes meditation so valuable is that it gives you the tools to stop running and face your messy emotions.

If you know someone who's into massage therapy, you may have heard the saying, "our issues are in our tissues." This refers to all the tight knots and sore muscles we can end up carrying due to our stresses and anxieties. But it also means that, by relaxing our clenched jaw or tight shoulders, we can release the emotions that tightened them up in the first place.

The real trick to getting the most out of your meditation is to avoid running away or trying to control the newly freed emotions you encounter during your session. Instead, you want to act like a curious observer, identifying the feelings and tracking them back to their origins.

To do this, try employing the R.A.I.N. method, which stands for _Recognize, Accept, Investigate_ and _Non-identification_.

So, let's say you're meditating and a strong emotion pops up. The first step is to truly _recognize_ it and become familiar with its sensations. Does it cause your chest to feel heavy? Does it make your throat tighten up? Pay attention to every detail so that you'll be able to recognize it quickly next time.

The second step is to _accept_ that you have this emotion. If it's anger or jealousy, don't deny it just because you'd rather not have it. Accept it and allow yourself to have and feel the emotion. This will actually help it pass quicker.

Now comes the _investigation_, where you try to deduce the origins of this feeling. Is the guilt because you forgot to call your mom on her birthday? Maybe the resentment is due to someone getting the contract you were after? Understanding the causes of your emotions will allow you to master them, instead of being totally at their mercy.

Finally, there's non-identification, which is about learning that your identity is not determined by whether or not you feel certain "good" or "bad" emotions. Everyone has unwanted emotions; they are just storm clouds that will eventually pass to reveal the blue sky again.

This is difficult at first, but, with daily meditation, it will get easier. Of course, there may come a time when you need help, which is when a professional therapist can be invaluable.

### 8. If you have a high-pressure job, the S.U.R.F. meditation tool can improve your performance. 

Lots of people have stressful jobs, but, for some of us, a bad day can be a matter of life or death. For doctors, police officers and firefighters, a standard workday has far more pressure than meeting a deadline or delivering a presentation.

One of the best ways to cope with high-pressure jobs is to meditate, which is why Sylvia Moir, the chief of police in El Cerrito, California, has introduced a regular meditation practice for the officers in her team.

Moir's decision was based on a number of studies that show how both performance and quality of life are improved when people with dangerous jobs start meditating. This holds true for police as well as military soldiers and firefighters. Statistics reveal that meditation improves short-term memory, helps speed up the recovery from traumatic events and reduces the body's production of the stress hormone _cortisol_.

While some of the police officers in El Cerrito were initially hesitant, they soon embraced meditation as the benefits became apparent. Officers were recovering faster from difficult incidents and bringing home less stress and tension, making for a calmer family life.

One of the specific techniques used by those with dangerous jobs is called the S.U.R.F. meditation, which stands for _Stop, Understand, Relax_ and _Freedom._

This form of mediation is essentially a quick pause, taken whenever you're in a particularly stressful situation, and it's intended to keep you from making a mistake or reacting in a way that you'll surely regret.

So first you _stop_, and take a moment to pause and think before you react to the situation. If you're dealing with a client or coworker who's antagonizing you, stop and breathe for a moment before you take the bait and decide to yell at them.

The second and third steps, to _understand_ and _relax_, are about recognizing the emotion or urge that's driving you and understanding what it wants you to do — punching someone in the nose, for instance — and then doing the opposite by reacting with a calm, measured response.

The final step, _freedom_, is about embracing the fact that you're in control and that you don't have to automatically respond to provocations with anger or violence. All it takes is some mindfulness and practice.

### 9. Stay attuned to the benefits of meditation and work toward the goal of focusing on nothing. 

If you start meditating today, you'll probably think: this is great, I have a new practice that is going to improve my life! But the challenge is keeping it up and staying consistent, even when you catch a cold or go on vacation. Because once you miss a day or two, it's easy to miss another and, before you know it, it's slipped away completely.

This is why it's of the utmost importance to stay focused on the benefits.

We're a lot like those lab rats that will keep pushing a lever once they discover that doing so delivers a reward. But if we're distracted, we can easily forget about those benefits, move on and start pressing another lever.

So make sure you put the appropriate value on the rewards that meditation provides and take note of how enjoyable and calming and relaxing it is. Also, make sure you recognize all the transformative effects it can have on your life.

According to Bill Duane, a manager at Google, the company's employees have reduced their stress levels by 19 percent thanks to meditation classes. Now, you probably can't produce such precise personal statistics, so you'll have to make an effort to recognize how meditating has reduced your stress and anxiety. Keeping track will help you stay motivated in your practice.

The final tip is to eventually take your practice to the next level by trying to meditate on absolutely nothing.

To do this, and not focus on your breath or a sound or anything, you'll be casting aside your training wheels. But you don't have to remove them all at once. You can ease your way into the expert level by making the sensation you're focusing on more and more subtle until you reach nothing.

It's not easy to focus on nothing and do absolutely nothing at the same time, but it's also deeply rewarding to let go of everything.

There's a good chance this will be frustrating at first, as your mind will revolt and revert to unhelpful worries and distracting thoughts. But just keep acknowledging these thoughts and letting them go.

And remember: there is no right or wrong in meditation. Just relax, and enjoy.

### 10. Final summary 

The key message in this book:

**Meditation isn't only for hippy-dippy folks who smell of patchouli oil. It's a scientifically proven method for improving your health and sharpening the skills that help professionals and people working in dangerous situations. By implementing meditation in your life, and turning it into a daily habit, you'll soon be on your way to a happier, less stressful existence.**

Actionable advice:

**Increase your compassion by meditating with a special someone in mind.**

Meditation isn't just about making you more relaxed and happy. It can also make you more compassionate. If this sounds appealing, try engaging in a short compassion meditation. Think of someone who is currently in need of love or support. While picturing this person in your mind, spend 30 seconds focusing on your feelings of compassion and love for him or her. By making this a regular exercise, it's likely that you'll soon find yourself being more kind and compassionate with everyone around you.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Unplug_** **by Suze Yalof Schwartz**

_Unplug_ (2017), a useful beginner's guide to meditation, makes clear that meditation isn't just for hippies and monks; rather, it's a practical exercise that anyone can use to become calmer, happier and healthier. Get all the basic info you need to start a daily meditation routine, as well as a number of supplemental tools that will aid relaxation and improve your practice.

This is a Blinkist staff pick

_"These days, we are so busy at work and in our private lives that it becomes more and more difficult to truly relax. These blinks explain why meditation is the best medicine to fully recharge your batteries."_

– Robyn, German Production Manager at Blinkist
---

### Dan Harris, Jeff Warren and Carlye Adler

Dan Harris, a frequent contributor to _Nightline_ and _20/20_, is a longtime correspondent for ABC News, as well as the co-host for _Good Morning America_ 's weekend edition. Based in New York City, he's traveled the world to report from areas as diverse as Cambodia, Afghanistan and Haiti. He's also the creator of the _10% Happier_ podcast and app, both of which strive to bring the benefits of meditation to busy urban professionals.

Jeff Warren is a writer and teacher who's been called the "Meditation MacGyver." He's also the founder of Toronto's Consciousness Explorers Club, a vibrant meeting place for adventurous meditators.

Carlye Adler is a journalist whose work has appeared in _BusinessWeek_, _Forbes_, _Newsweek_ and _Wired_, just to mention a few. She's also the co-author of numerous books, including the _New York Times_ bestseller, _Rebooting Work: Transform How You Work in the Age of Entrepreneurship_.

