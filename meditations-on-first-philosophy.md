---
id: 55daea881f4e6e0009000003
slug: meditations-on-first-philosophy-en
published_date: 2015-08-24T00:00:00.000+00:00
author: René Descartes
title: Meditations on First Philosophy
subtitle: None
main_color: 105232
text_color: 105232
---

# Meditations on First Philosophy

_None_

**René Descartes**

_Meditations on First Philosophy_ (1641) is one of Descartes's most influential works, known as the source of the classic quote: "I think, therefore I am" or "_cogito ergo sum_." These blinks capture Descartes' thoughts on how we know what we know, and his attempts to prove God's existence along the way.

---
### 1. What’s in it for me? Discover the thinking behind one of history’s most influential works of philosophy. 

If you were asked to quote a philosopher, which quote would immediately come to mind? "Workers of the world unite!" possibly, but the vast majority of people would answer, "I think, therefore I am."

René Descartes' declaration of his own existence is one of the most famous utterances in history, but how much more about him, or his theories, is generally known today? Not much outside academic philosophy departments. That is where these blinks come in: they explain the main concepts contained in Descartes' most famous work of philosophy. Read them and that famous quote will make a lot more sense.

In these blinks you'll discover

  * why, according to Descartes, _The Truman Show_ might not be that far from the truth;

  * the proof of God's existence; and

  * why no one can trust what their eyes see, or their ears hear.

### 2. Our senses deceive us. 

What would you do if one of your friends told you little white lies over and over again? After a while, you'd probably stop trusting that friend, and certainly not rely on them for anything important. 

What we _can_ rely on, if it's not other people, are our five senses: sight, smell, touch, taste and hearing. Well, that's not quite true. Our senses feed us a constant stream of tricks and lies too. Not ready to believe that? Just think about what it's like to dream. 

Dreams can feel incredibly real, so real that we rarely realize that they _are_ just dreams. At least, until we wake up, and start to notice quite how bizarre and absurd those dreams were. Just as painters can make stunningly realistic images of life, the senses create vivid and convincing images in our mind. 

Of course, painters often combine ideas from life to create images of things that could never exist in the real world. Think of satyrs, the half-man, half-goat figures that we know from mythology. We know they don't exist, but still we can be fooled into thinking they do.

It isn't just in bizarre dreams that our senses can fool us. They can mislead us over the course of our entire lives. How? Senses can be tricked by external forces. 

Imagine, for instance, that something evil out there is determined to fool your perception. Sounds far-fetched, but let's go with it for the moment. They could be tricking your senses without you knowing it — right this very moment. 

Descartes drew this reasoning from popular beliefs about demons that were still prevalent during the seventeenth century. We find a more modern example of his argument in the film _The Truman Show,_ where the main character, played by Jim Carrey, is raised in an artificial world where he is constantly filmed. The result is aired as a TV series and watched by millions of viewers. The whole thing is controlled by an "evil genius" TV producer. Truman's world seems completely real to him, but this couldn't be further from the actual truth. 

So we can't trust our senses or what we learn from them. We should therefore treat all knowledge with _skeptical doubt_. Things like our body and the physical world around us might exist, but we can't be sure of it. So what can we be sure of?

> _"Once the foundations of a building have been undermined, the rest falls of its own accord."_

### 3. Our thinking proves that we exist. 

As our senses can so easily deceive us, we should spend our lives as vigilant skeptics. But what's there to gain from a life full of doubt? Well, it does give us one thing we can be sure of: the fact that we're thinking. 

There might be an evil genius messing with our senses, but we can challenge our senses and imagination by _thinking about them_. Regardless of what our senses tell us about the world and our place within it, the one thing we can depend on is that we think. This leads to one conclusion: we think, therefore we exist.

But how can we know we're really thinking? Well, imagine a piece of beeswax. Freshly made from honeycomb, it smells, feels and looks like beeswax. If we put it close to the fire it melts, yet we still know that it's a piece of beeswax. How do we recognize it?

Through thinking. Our mind makes judgments and definitions of the world outside, so when our senses can't be trusted, our mind fills in the gaps. But couldn't the beeswax be just a dream or a production of our tricked sensory impulses? Maybe, but the fact that our brain perceives it and makes the judgment that it's beeswax means we are thinking. This way our mind proves that we exist.

Even if we're just thinking nothing more than "I don't exist," the fact that we are thinking it proves we do exist. But if our mind can prove our own existence, what about the existence of other things in the world? Are they real at all? And how can we know?

> _"I think, therefore I am," or, in Latin, "Cogito ergo sum."_

### 4. All concepts of objects and things have three levels of origin and existence. 

Though we can't trust our senses, we can trust the fact that we think and therefore exist. But what about the world around us, given that we receive knowledge of it through our senses? What is its nature?

First, it's important to note that we can actually understand some things that exist in the world purely with our minds. Take geometrical forms: triangles, cubes and circles. Humans can grasp these concepts without necessarily having to encounter them in their true form in the physical world.

For another example, take the concept of the sun. If we use astronomy, based on our knowledge of geometry and mathematics, we know that the sun is a huge planetary body — knowledge which we wouldn't gain from seeing it with our eyes or feeling it on our skin. 

On the other hand, many ideas do come to us from the outside world. These things force their existence onto us, doing so independently of our own actions and thoughts. For example, we feel warmth when we're sitting by a fireplace whether we want to or not. 

But as we know, we can't trust our senses. So the truth of any concepts we perceive with our senses is less reliable than the truth of concepts that we can understand with our minds alone. 

Take the sun again. When we see it in the sky it looks very small. If we took this sensory information as knowledge, we might think the sun was very small, perhaps even smaller than a tree that's closer to us. 

There are also other concepts that we create ourselves. These ideas, like hippogriffs and satyrs, are combinations of ideas we know through our minds or from the outside world. These imaginary ideas have the smallest degree of reality. 

With these three different levels of our realities in mind, how can we approach the biggest existence question of all: Is God real?

### 5. Our inherent ideas and ability give us evidence of God’s existence. 

So it is really our mind that is the core of our existence. Our thinking proves we exist and the concepts we can understand with our minds alone are really the highest form of reality. So how did we get this human equivalent to an operating system?

We wouldn't be able to have ideas if the ability to think hadn't been given to us. As we possess this ability from birth, we ourselves can't be the source. So something outside of us must be the cause of our ability to think.

After all, nothing can cause something else without existing and possessing the same property, or source, to a higher degree. For example, nothing without the property of warmth, like a fire, can make something else warm. 

The ability to think is with us from creation, and this ability is the proof of our existence. Since we exist, and thereby think, from the time we are created, this also proves that this ability is in us from the start. This can only have been put in us by something with a higher level of thinking than our own. A supreme thinking being which is God.

So if God exists and he can create the highest levels of existence, what implications does that have for the body and mind?

> _"Thus I plainly see that the certainty and truth of all my knowledge derives from one thing: my thought of the true God."_

### 6. The mind and body are two distinctly separate things. 

We've seen how our ability to think confirms our existence, and how God's existence in turn is proven by the fact that this ability exists within us at all: we are "thinking things," so to speak. But don't "thinking things" require bodies of their own?

The mind is without physical properties like extension and substance. As we have seen, the existence of the mind is proven by the ability to think. But this thinking doesn't need any physical properties to exist; the mind (or soul) can exist without it.

The mind works on the highest level of existence and can perceive and understand things without necessarily seeing them in an outside world: we _know_ the sun more through our thoughts about it than through our sensory perceptions of it.

As we have seen in the proof of God's existence, he is able to create anything. If God can make things of the highest level of existence, he can undoubtedly make physical things too; therefore we can assume that it is possible that bodies exist.

While body and mind both plausibly exist, they have two different levels of existence. The mind is the first, highest level of existence, while the body exists in its secondary form of sensing. As they exist on two distinct levels, they are independent of one another and can exist without each other. From this we can argue that the mind or soul can live on after the body is dead.

### 7. Final summary 

The key message in this book:

**Thanks for listening to our blinks to _Meditations on First Philosophy_, by René Descartes. Remember, while we cannot be certain about the world around us, through logic and considered reasoning, we can make strong cases for our own existence as well as the existence of God, and gain a deeper understanding of the nature of a reality which we usually take for granted. **

**Suggested further reading:** ** _Leviathan_** **by Thomas Hobbes**

_Leviathan_ (1651) examines the relationship of society and rulers and is widely held as a classic work on the nature of statecraft. English philosopher Thomas Hobbes believed that man's natural inclination to war could only be tamed by a strong, centralized government. In these blinks, you'll learn why Hobbes felt a commonwealth of men under a strong monarch was the only solution to securing peace and security for all.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### René Descartes

René Descartes was the French philosopher and father of the skeptic tradition that broke away from earlier philosophy based on Aristotelian thought. His work is focused primarily on ontology and epistemology and was both admired and criticized in his day.

Translator Jonathan Bennett is a fellow of both the American Academy of Arts and Sciences and the British Academy.

