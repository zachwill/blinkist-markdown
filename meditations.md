---
id: 5713be4f5236f20007c16fa7
slug: meditations-en
published_date: 2016-04-20T00:00:00.000+00:00
author: Marcus Aurelius
title: Meditations
subtitle: None
main_color: 463D3A
text_color: 463D3A
---

# Meditations

_None_

**Marcus Aurelius**

_Meditations_ (170-180 AD) is a journey through the mind of the great Roman emperor, Marcus Aurelius. These blinks offer philosophical ruminations on the meaning of death and justice, the nature of the world and why things happen the way they do.

---
### 1. What’s in it for me? Understand the philosophy that drove an empire. 

The second century AD marked the height of the Roman Empire. After a long period of internal turmoil and unrest, Rome settled into a period of relative peace and prosperity. The first five emperors of this period are called the Five Good Emperors, and the last of these five was Marcus Aurelius. 

Aurelius wasn't only an emperor, but also a philosopher who based his ideas on the traditions of Greek Stoicism, a school of thought that viewed self-control and calmness as a way of overcoming negative emotions. 

Aurelius's philosophy led to his gaining the title of philosopher king. So what were his ideas? These blinks explore what made Aurelius one of the great Stoic philosophers, and why his thoughts have influenced writers like John Steinbeck and political leaders like former US President Bill Clinton (who regards this as his favorite book).

In these blinks, you'll find out

  * why everything that happens is part of a deliberate order;

  * why pain can only be inflicted upon yourself; and

  * why you shouldn't fear death.

### 2. Logos is a concept that encompasses and orders the entire universe. 

There were various philosophical schools during antiquity, covering a range of different subjects, from nature to human action. However, a central aspect of these ancient philosophical teachings that cut across various topics was the concept of _logos_. 

This word, which roughly translates to "reason," was applied by such famous philosophers as Heraclitus and Aristotle, and was also of central importance to the author, Marcus Aurelius. 

His view was that logos can be seen everywhere; it makes up the earth, trees and even us as humans. However, logos does not just give everything form; it also gives it order.

For humans, this means that logos determines who is placed where in society, and how that person should be respected. So, it is logos which decrees that slaves should be treated as such and that emperors should be treated much better. 

But why should we agree to such unequal placings?

Because logos, the immutable essence of life and underlying master plan for all events, encompasses the entire world and, therefore, constitutes the ideal way to order it. In fact, logos is perpetually working to move the universe forward in the best way possible. 

So, even when the author went through difficult periods in his life, he kept faith that they fit into the grand plan of logos, since everything that happens is exactly right and no one should desire change. Therefore, even when the majority of his family had passed away and uprisings challenged his empire, the author held true to his belief that it was all meant to be.

> _"My only fear is doing something contrary to human nature — the wrong thing, the wrong way or at the wrong time."_

### 3. Death is inevitable and should not be feared. 

In ancient times, death was an ever-present fact of life — infant mortality rates were extremely high and the average life expectancy was very low. As a result, one of the concerns most commonly voiced to the author was a fear of death. 

But the author had a different perspective: he didn't think that people should be afraid of death since all beings, living or dead, are still part of logos. As such, dying is simply logos leaving a body that began dying the instant it was born — when a person dies, they once again become part of the greater logos. From there, their essence is reused to form new living beings who continue this endless cycle. 

Furthermore, death only comes exactly when logos needs it to. After all, since logos has a greater plan, it's no use to fear any of the millions of things that could kill you. 

So, if the author was destined to die of cancer in old age, or on the battlefield in an instant, there would be nothing he could do about either fate. It would be useless to fear something so inevitable. 

And beyond that, he knew that even the best people die. So, at times when the author _did_ feel overwhelmed by death, for instance when he lost his wife, he reminded himself that everyone dies eventually. Whether you are a great emperor, a philosopher like Plato or a valiant gladiator, you must embrace mortality, not live in fear of it.

> _"Death: something like birth, a natural mystery, elements that split and recombine."_

### 4. Life is too short to waste your time complaining. 

So, anybody can die at any time, whether from a heart attack, a freak accident or simply old age. And since you don't know when your death will come, it's important to always be the best you can be. 

Letting yourself be annoyed by the things you have to do just takes away time that could be spent living. Nobody should waste their life complaining about how hard it is to live. 

For instance, even though the author didn't like having to hold court, he always did so happily because it was his belief that he shouldn't spend a moment of his short life begrudging his responsibilities. After all, if logos needed him to spend the day in court, he should do so and not let others suffer from his complaints or a non-functioning court. 

But beyond that, since our time on earth is limited, it's essential to get as much done as we can. For instance, instead of lounging in bed until midday, the author was always trying to be more productive. 

However, while he hated people who wasted his time with small talk and superficial arguments in court, he recognized it as his duty to serve the grand plan logos had laid out for him, even if it meant letting people waste his time occasionally. And on the occasions that he felt like giving up, he only needed to recall his role as an emperor and participant in logos to get back on his feet.

> _"To a being with logos, an unnatural action is one that conflicts with the logos."_

### 5. Logic is essential – emotions can kill our reasoning and cause us unnecessary harm. 

The author and the Stoic school of philosophy, of which he was a devoted follower, valued reason and a logical perception of the world over all else. Therefore, they considered a calm and analytical mind better than one ruled by desires and feelings. 

This approach makes sense since logos is primarily about governing through reason and order. It's a system in which everything that happens was supposed to and is therefore good. 

For instance, if your house burns down, you could see it as a disaster because all your belongings would be lost with it — or you could see it as beneficial since you can cash in your home insurance. Basically, the essence of any event depends on how you perceive it. 

So, if you accept the premise that logos has good reasons for everything that occurs, you should see such an event clearly and take it for what it is: necessary for the greater good. Perhaps your house burning down will make you move to a new neighborhood where you'll meet the person you eventually fall in love with. Or, you might use the insurance money to take a life-changing dream trip around the world. 

However, it is important to keep in mind that human emotions are a threat to reason. In fact, being obsessed with the idea that you're unlucky or making decisions based on carnal desires will create so much confusion in your mind that you'll be unable to see logos as the truth it is. 

This is precisely why the author hated being driven by emotions like revenge, hate, lust and infatuation; keeping his mind calm, collected and reasonable was essential for him to govern effectively.

So, whenever he felt overwhelmed, he would meditate on logos and his role in the grand scheme of things. By doing so he could remind himself of his place in the universe and find his calm, collected self.

### 6. The only pain you can truly suffer is the pain you inflict upon yourself. 

Ancient Rome was full of dangers, especially for an emperor. All too often, powerful people would fall victim to torture, poisoning and injuries in combat, or would see loved ones murdered by enemies. 

The author coped with the pain caused by all this suffering by maintaining his belief that experiencing physical pain is still a part of the greater good that is logos. The plan logos lays out for the universe necessitates that people sometimes suffer for the natural order to proceed. 

Therefore, if someone is tortured and killed, experiencing horrific personal suffering in the process, it is still correct in the grand scheme of things because it is what was meant to happen. 

In fact, the author lost nearly all 13 of his children during their infancy, and his wife eventually joined them, dying at a young age. But by reminding himself that all things happen for good and logical reasons, the author was able to remain calm through these hardships. After all, since logos is reasonable, anything that happens is necessarily good and rejecting such fate is unnatural. 

Humans are also entirely responsible for the decisions they make. Any harm done to a person from an external source is something beyond their control and therefore something that can't truly harm them. 

How so?

Well, since logos is a part of every human, the only thing people can do is accept pain and move on without complaining. Complaining simply disrespects the immortal logic of logos that is embedded in every person, and inflicts further pain upon oneself.

> "If thou art pained by any external thing, it is not this that disturbs thee, but thy own judgment about it."

### 7. Final summary 

The key message in this book:

**The universe and life itself are governed by a force that encompasses and orders the world: logos. Logos offers proof that all things happen for a reason. Therefore, there is no reason to fear death, suffering or doubt your duty to society, as each is part of a greater, flawless plan.**

Actionable advice:

**Treat others fairly and with kindness.**

You're the only person who can decide your fate and actions. So, even if others are unkind to you, don't let yourself stoop to their level. Always treat others with dignity and justice and you'll never suffer as a result of your own actions.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Republic_** **by Plato**

Plato's _Republic_ (c. 380 BCE) is a dialogue in which Socrates and his interlocutors discuss the attributes and virtues that make for the most just person and for the most just form of government. The _Republic_ also examines the relationship between the citizen and the city, and considers how this relationship bears on philosophy, politics, ethics and art.
---

### Marcus Aurelius

Marcus Aurelius Antoninus (121-180 AD) rose to power as the emperor of Rome in 161 AD. He has been called one of the greatest Emperors of Rome, a just man and a ruler who deeply valued philosophy.

