---
id: 5518a0c639613200079a0000
slug: meet-your-happy-chemicals-en
published_date: 2015-04-01T00:00:00.000+00:00
author: Loretta Graziano Breuning
title: Meet Your Happy Chemicals
subtitle: Dopamine, Endorphin, Oxytocin, Serotonin
main_color: DB926B
text_color: 8F5F46
---

# Meet Your Happy Chemicals

_Dopamine, Endorphin, Oxytocin, Serotonin_

**Loretta Graziano Breuning**

_Meet Your Happy Chemicals_ (2012) provides a detailed introduction to the four chemicals responsible for our happiness: dopamine, serotonin, endorphin and oxytocin. The book explores the mechanics of what makes us happy and why, as well as why some bad things make us feel so good.

---
### 1. What’s in it for me? Learn which chemicals make you happy and what you can do to optimize them. 

Peruse the shelves of any book shop and you will find a multitude of titles telling you how to be happy. It's no surprise that so many writers have tried to tackle this: we all want to be happier.

But what is happiness and where does it come from? These blinks show you how joyfulness (and its evil sibling unhappiness) are products of our brain — more specifically the chemicals of the brain. They explain how these chemicals work and crucially how you can stimulate the right ones for yourself.

In these blinks you will learn

  * why dominating others makes us feel happy; and

  * why true happiness is only ever 45 days away.

### 2. We feel happy every time we see something that is good for our survival. 

Everybody wants to be happy. In fact, if we had our way, most of us would _always_ be happy. But what does "being happy" actually mean? And what does happiness look like inside our brain?

Several structures in our brain, collectively called the _limbic system_, manage all of the chemicals responsible for our happiness. These _happy chemicals_ are brain chemicals — dopamine, endorphin, oxytocin and serotonin — that are released each time we see something that is good for our survival.

Whenever we sense something, the limbic makes a quick assessment to "decide" whether or not something is worth a spurt of happy chemicals.

The limbic system developed a very long time ago in our evolutionary history, and works today as it did in the past: things that increase the possibility of survival trigger happy chemicals, and things that decrease our chances of survival trigger unhappy chemicals.

Though we inherited the limbic system from our ancestors, our brain doesn't automatically know when to release happy chemicals. Rather, it's our experiences and the _neural pathways_ they form which determine what makes us happy and what doesn't.

Neural pathways are mainly formed when we are young: Each time we experienced something nice as a child, a neurochemical connection was built or strengthened.

When you were hungry as a child, for example, that experience probably made you feel bad. If your mom gave you a cookie to ease your hunger, however, you probably felt better. If this happens a few times a connection between your neurons is formed. That's why you now reach out for a cookie whenever you feel bad: your brain formed a connection between eating cookies and a happy feeling.

### 3. Dopamine and endorphin reward us for seeking our desires and let us combat physical pain. 

Our limbic system's "decisions" can be quite confusing sometimes. It's often unclear why one thing can make us happy when others don't. Understanding the limbic system requires you to first understand how your various happy chemicals work.

The happy chemical _dopamine_ is released whenever you expect a reward, and it's what motivates you to keep seeking it.

Unbeknownst to you, your subconscious mind is constantly looking for rewards in your environment. When it finds one, dopamine motivates you to go for it, and helps you manage the energy it takes to get it.

Imagine, for example, that one of your ancient ancestors was walking around looking for food, i.e., a reward. Whenever something promising came into his sight, his limbic system released dopamine, signaling that it was time to expend his energy to reach the reward.

Another happy chemical, _endorphin_, is triggered by physical pain. Of course, physical pain does _not_ make us happy. Endorphin, however, does, and serves to hide the pain to help you keep going.

If you're a runner you've likely experienced "runner's high" — that feeling you get after pushing yourself beyond your own limit and running that extra mile. With this rush of endorphin, you feel incredible instead of exhausted. Our primeval ancestors used endorphins not for the feeling, but to help them survive.

Imagine, for example, that a lion catches a gazelle, but then takes a quick break to catch its breath. During the brief moment when the gazelle is no longer stuck between the lion's jaws, endorphins allow it to use all of its remaining energy to sprint away, despite its terrible wound.

Pain is important for our survival, so it's a good thing that endorphins don't last forever!

> Fact: The painkiller morphine essentially imitates the release of endorphins in the brain.

### 4. Oxytocin lets us enjoy our social lives, while serotonin rewards us for dominating others. 

If you had to pick three things that make you happy, one of them would probably involve your friends, family or colleagues. But why is that? Here, too, you can thank your happy chemicals.

The chemical _oxytocin_ rewards you for building social alliances. The good feeling you get because you trust another person is because of the oxytocin that has been released in your brain. In fact, every experience of social belonging triggers oxytocin, because belonging to a social group is good for our survival.

Indeed, without these social bonds, we could hardly survive at all, and oxytocin plays a central role all throughout our social development.

When a child is born, oxytocin is released in its mother's brain, causing her to feel good and motivating her to look after her newborn. Oxytocin also flows in the child's brain, building the attachment between parent and child.

Young human children, much like other mammals, cling to their mothers without knowing why. Oxytocin is the reason — it just feels good.

Of course, our social relationships are highly varied. Certain kinds of relationships trigger other chemical responses in the brain. Specifically, _serotonin_ is released when we assert our position in the social hierarchy by dominating others.

Most people probably wouldn't immediately agree that dominating others makes them feel good. Such an admission feels awkward considering the values of our modern society. Nevertheless, our brain rewards us whenever others respect our position in the social hierarchy.

The reason for this becomes clear when we look at the animal kingdom: Those who dominate others have better access to food and more mating opportunities. So despite our attempts to promote fairness and kindness in modern society, we still feel good when we occupy positions of power and dominance.

Now that you understand how your happy chemicals work, the following blinks will look at how happy and unhappy chemicals shape our lives.

> Fact: Antidepressants, such as Prozac, increase serotonin levels in the brain.

### 5. Despite the great feeling that happy chemicals bring, unhappy chemicals are equally important. 

Our unhappy chemicals are incredibly important to our survival. We might not enjoy bad feelings, but the feelings themselves — along with the chemicals that promote them — are as important as the positive emotions.

_Cortisol_ is one such chemical. While happy chemicals help us to pursue things that promote our survival, the unhappy chemical cortisol grabs our attention whenever our survival is threatened.

For example, cortisol is released whenever you're hungry, motivating your brain to look for ways to mollify this feeling. Here, the solution is easy — just go to the fridge and fix up a snack!

Often, however, it's harder to work out why cortisol has been released. In fact, every time happy chemicals fulfil their function, the brain follows them with a spurt of cortisol. Even though you're in your "normal" state, things still don't seem quite right, resulting in a feeling that you need to "do something."

It is this release of cortisol that motivates you to reach for a chocolate bar even when you aren't hungry, simply to feel happy again.

The reason we get this "do something" feeling is that our brain is always looking for potential threats.

The limbic system isn't solely responsible for our behavior. It gets help from the _cortex_, which is responsible for rational analysis and assessing potential danger. It also helps us to predict threats, and thus promotes our survival.

The cortex works round the clock, meaning that your brain is constantly checking for the next potential threat and always releasing cortisol.

So if you are feeling nervous about going to the dentist, your nervousness won't actually stop after your appointment. Your brain will simply find the next thing to worry about: a work assignment, someone's birthday, and so on.

> _"Your brain would be happy if you could control everything...That doesn't happen, so unhappy chemicals are part of life."_

### 6. The happiness strategies our brain develops through past experience will eventually disappoint us. 

You could probably make a long list of things that make you feel good. But do these things actually make you happy?

Our experience has taught us what makes us happy, but unfortunately even those things will eventually disappoint us.

As we've seen previously, whenever we get the "do-something" feeling, our brain tries to mollify it by following routines that it knows will trigger the release of happy chemicals. Unfortunately, that behavior will become routine and ordinary, and thus no longer trigger the release of happy chemicals. This process is called _habituation_.

Some people and businesses, such as the French Laundry restaurant in California, try to prevent habituation and maintain the effect of happy chemicals. To accomplish this, the restaurant serves many tiny courses, meaning that you don't have time to get used to a dish, and always have something new to nibble on. This novelty allows for a continuous release of dopamine — and happy feelings.

However, in general, a repeated experience will never feel as good as the first time you tried it. But your brain doesn't understand this, and keeps its expectations high, causing you to feel disappointed when those expectations aren't met.

But there is a good reason for all this: habituation evolved because it promotes our survival. Sitting around enjoying what we already have doesn't help us to survive. Going out and looking for new ways to get those happy chemicals flowing, such as finding new sources of food, does.

If we continue to use happiness strategies that lead to disappointment, we'll eventually find ourselves in a vicious cycle.

Your brain will follow strategies that it's developed — despite having already been disappointed by them — because these strategies have worked in the past. This can have disastrous consequences.

Things like drugs, alcohol and junk food, for example, _will_ make us happy, but will ultimately lead to disappointment. The brain has a hard time figuring this out, which makes us susceptible to addiction.

### 7. Neural circuits form because our experiences make physical changes in our brain. 

Obviously, we don't want to become trapped in vicious cycles. It's important to understand how these patterns develop and understand why it's so much more difficult to change them in our adult lives.

Experiences are critical in the formation of your neural circuitry. One way they alter your brain is through _myelination_.

Myelin is a substance that coats the neurons that you use often. Neurons that have a myelin coating are much more efficient than other neurons. Actions that follow those pathways feel more natural than ones that don't.

For example, if you're learning a new language, speaking it feels awkward because the neurons you use don't yet have a myelin coat.

Most myelination happens before you turn 15, which explains why it's more difficult to learn new things as an adult and why it's difficult to change existing habits.

But just because our experiences form our neural circuits doesn't mean we're powerless to change them. We still have our free will, and thus have the opportunity to change neural circuits.

Neurochemicals are constantly swarming your brain and influencing your actions. But your _pre-frontal cortex_, the part of the brain that lets you decide where to place your attention, gives you the opportunity to ignore these chemicals and abstain from any action they might be trying to motivate you to do.

Imagine, for example, that you're in a meeting where a colleague introduces a new project. You realize that his ideas are utter garbage. How should you react?

You have two options: State your opinion aloud and present your own ideas instead (which will result in the release of serotonin) or inhibit yourself from humiliating your colleague and instead talk to him alone afterward. In the end, you won't miss out on any happy chemicals, as being nice to someone results in the release of oxytocin!

> _"When you know how your brain works, you can build more happy habits with fewer side effects."_

### 8. We can rewire our brains in order to enjoy things that are good for us. 

The knowledge that changing the way you approach happiness will mean changing your neuro-circuitry might make the whole process seem quite daunting. However, all you need is a little patience, and you'll soon have much more control over your happiness.

In fact, it takes only 45 days to construct a new neural pathway to happiness. When it comes to changing your habits, persistence is the most important thing: If you manage to keep up your new habit for 45 days straight, then you will have internalized it and it will feel natural.

At first it won't feel good to do something new instead of something you know you love — watching your favorite TV show has more appeal than studying. But if you persist, your newly formed habit will rewire your brain so that it releases happy chemicals whenever you do this new target action.

Using this 45-day strategy, you can increase the number of habitual activities in your life that trigger the release of happy chemicals.

Laughing, for example, is a great way to trigger endorphin release. Take time daily to find something that will really make you laugh. After 45 days you will see that laughing, and the rush of endorphins that comes with it, has become a totally natural part of your day — one that you won't want to give up.

But persisting for 45 days isn't always easy. If you find yourself struggling to integrate your target behavior into your daily life, it can be helpful to _mirror_ the behavior of those who have already incorporated positive habits into their daily routine.

So if you want to incorporate laughter into your everyday life and know someone whose laughter fills the halls at your office, pay close attention to them during the day. By studying them and mirroring their behavior, you'll soon discover the ways in which they are able to find a little humor in each day.

### 9. By constantly making decisions, we are responsible for our own happy chemicals. 

As useful as our complex neural structure is for our survival, it can also be a burden. Being on the constant lookout for potential threats and problems comes at the cost of ignoring the good things in life. However, we have the choice to change that!

To live means to constantly choose whether it's worth giving something up to gain something else. Your brain is always looking for opportunities to improve survival, but these opportunities always come with risks that might not bring the desired outcome. But there's no way to avoid having to make these decisions, so you might as well get used to the risk.

Don't be like those people who second guess every decision they've made, always looking back to find missed or squandered opportunities. This will only trigger unhappy chemicals!

Ultimately, managing your own happiness means not letting others make decisions for you. Some people believe that it's best to let others take control. This way, they never have to take the blame for any bad outcomes, and will always have a scapegoat.

But when you let others choose for you, you won't experience the joy and sense of accomplishment that comes with pursuing your goals. Not only that, but your brain always wants what it doesn't have, no matter who is making the decisions!

> _"Every decision is bad when you look for the bad in it."_

### 10. Final summary 

The key message in this book:

**Happiness isn't arbitrary. Rather, it's rooted in natural, chemical processes in your brain — processes that were developed long before even the evolution of mankind! But that doesn't mean you are powerless to determine your own happiness.**

Actionable advice:

**Rejoice for your unhappy chemicals!**

It's easy to lament the fact that we often feel less than jubilant, but the truth is that we _need_ to feel unhappy. In fact, without unhappiness, there would be no happiness. Accept your unhappy chemicals and come to terms with their influence on your life, because they aren't going away anytime soon.

**Suggested** **further** **reading:** ** _Why We Love_** **by Helen Fisher**

Helen Fisher's _Why_ _We_ _Love_ is not only a report on her latest astonishing research but a sensitive description of the infinite facets of romantic love. This book is a scientifically grounded examination of love that reveals how, why and who we love.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Loretta Graziano Breuning

Loretta Graziano Breuning is the founder of the Inner Mammal Institute as well as a docent at the Oakland Zoo, where she lectures on the social behavior of mammals and gives tours. She has written several other books, including _Beyond Cynical: Transcend Your Mammalian Negativity._

