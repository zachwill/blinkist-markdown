---
id: 5852c52e58b1350004ed16ce
slug: meltdown-en
published_date: 2017-01-02T00:00:00.000+00:00
author: Thomas E. Woods, Jr.
title: Meltdown
subtitle: A Free-Market Look at Why the Stock Market Collapsed, the Economy Tanked, and Government Bailouts Will Make Things Worse
main_color: B1232A
text_color: B1232A
---

# Meltdown

_A Free-Market Look at Why the Stock Market Collapsed, the Economy Tanked, and Government Bailouts Will Make Things Worse_

**Thomas E. Woods, Jr.**

_Meltdown_ (2009) gives you a guide to understanding the government regulations which in effect caused the 2008 global financial crisis. These blinks will explain how government spending has and always will worsen economic recessions, and importantly, what needs to be done to save the world economy.

---
### 1. What’s in it for me? Discover the truth about economic crises and how we can prevent them. 

In the seventeenth century, a single tulip from Holland was worth more than ten times the annual salary of a skilled craftsman. Tulip mania gripped the world, inspiring rampant economic speculation which resulted in a massive financial crash, stripping ordinary people of their life savings.

Sound familiar? Whether tulips or mortgages, global markets have enjoyed countless booms and suffered through numerous busts. The most recent crisis in 2008 reverberated all over the world, with millions of people losing their jobs, their homes and their sense of well-being.

Although many economies have since recovered, economists warn that the prevailing question is not _if_ there will be another crisis but _when_. There has to be a better way! These blinks will explain how we got into this mess and how we can free ourselves from this disastrous boom-and-bust cycle.

In these blinks, you'll learn

  * why the US government is to blame for the 2008 economic crisis;

  * how an Austrian economic theory can explain busts past and present; and

  * why suffering a bankruptcy isn't all that bad.

### 2. Deregulation and free markets didn’t cause the last financial crisis – government regulation did. 

It's common to see stories in the media about how unrestrained capitalism caused the most recent economic crisis. These pundits say that government should become more involved in the economy to fix the broken system.

But is it possible that government, the institution tasked with repairing the economy, actually caused its collapse in the first place?

Let's take a closer look. The crisis began with the government giving mortgages to people who wouldn't otherwise have been able to afford them. It started in 1999, when government-sponsored enterprises, better known as Fannie Mae and Freddie Mac, put into action a Clinton administration plan to assist low-income and minority families in purchasing homes.

As part of the plan, the government forged new mortgage requirements that allowed brokers to offer loans with zero money down, enabling people with no savings to buy houses. Not just that, but these new, risky mortgages were classified as creditworthy by government-backed rating agencies.

These agencies then, not wanting to call politically popular programs "risky," kept reassuring the public that the mortgages were secure.

But Fannie Mae, Freddie Mac and the rating agencies aren't the only ones to blame for the crisis. In fact, the Federal Reserve played a major role as well. Here's how:

In the early 2000s, the Fed slashed interest rates by printing tons of money. This input of cheap money, paired with relaxed mortgage rules, prompted a major housing boom, causing home prices to shoot up at an insane rate. With hopes of getting rich overnight, careless investors piled into the market.

As a result, in 2006 some 25 percent of all home purchases were made by speculators.

But the good times didn't last long. By the end of 2006, housing prices were sinking, and foreclosures had risen by 43 percent. Since no down payments were at risk, speculators just walked out on their underwater investments. The mortgage market fell apart, and the financial system that had stuffed billions of dollars into mortgage-backed securities soon followed.

This disastrous outcome came about because reckless government policies enabled people to spend money _they just didn't have_.

### 3. To understand the roots of the current crisis, we need to look at Hayek’s business cycle theory. 

The Nobel-prize winning economist Friedrich Hayek developed what was likely the most important economic theory of the modern world. It's called the _business cycle theory_, and its explanation of the boom-and-bust phases of the market applies to both the most recent crisis and economic catastrophes of the past.

Here's how it works. The business cycle theory is based on the effects of government-suppressed interest rates. That's because artificially decreasing interest rates by printing money produces the illusion that current production can increase more than is sustainable. This deception makes entrepreneurs invest in long-term projects that aren't based on the realistic savings necessary to feed current production levels.

For instance, a builder who thinks he has 30 percent more cement than he does will build a bigger house than he has the materials for. When he realizes he's out of cement, he won't be able to complete his build and will have wasted time and resources on something that's of no use.

This means that by artificially lowering interest rates, the government causes people to act like they have a lot more money saved than they do. Therefore spending often spikes before a big crash.

We saw the business cycle theory in action during the dot-com boom of the late 1990s, for example. Between 1995 and 2000, the stock prices of internet start-ups jumped dramatically. Why did this happen?

All the classical signs of the business cycle were accounted for: low interest rates prompted by the Federal Reserve's expansion of the money supply, causing record-high debt, coupled with quickly rising capital prices for things like programmers and real estate.

These factors meant that by 2000, the resources necessary to complete long-term market investments no longer existed. So the dot-com bubble burst, and the value of the Nasdaq stock exchange fell by 40 percent.

### 4. Just as government intervention causes economic crises, it also prolongs them. 

So it's clear what caused the economic crisis we're still mired in, but how can we best deal with it?

We can learn from past crises, such as the Great Depression. That's because the foundations of the Great Depression were set by the inflationary government policies of the 1920s. So just as business cycle theory predicted the 2008 recession, it could also have predicted the depression of the 1930s.

For instance, basic economics says that when the production of goods goes up, the prices of those goods go down. But that's not what happened in the 1920s. Instead, the government increased the money supply by 55 percent, to make it seem as if prices were stable. The government thought that if prices were stable, then the economy would be as well.

People gladly swallowed the government's story and kept spending, while the stock market grew at an increasingly unsustainable pace until 1929. And while most economists at the time thought of the American economy as invincible, Austrian economists foresaw the boom's eventual collapse. And their predictions were validated when, in October 1929, the stock market crashed.

We all know what happened next: President Franklin D. Roosevelt introduced the New Deal, a series of social programs to boost the economy and address unemployment. But this program didn't pull the United States out of the Great Depression; instead, it just prolonged the crisis.

That's because, instead of listening to reasonable ideas, Roosevelt kept throwing money into the economy. He refused to accept the lessons of the 1929 crash and its root causes. Neither the huge public works programs nor increased spending as a result of World War II would help save the economy.

In fact, by hiking taxes and funneling millions of dollars toward businesses that had no real demand, Roosevelt stood in the way of the economy's natural efforts to restart itself, based on the real desires of consumers. So it wasn't until the 1940s when New Deal policies were finally ended that the economy started to recover.

### 5. We have to end bailouts and reassess the purpose of the Federal Reserve. 

So just like prolonged government spending didn't do a thing to solve the Great Depression, using government bailouts to inject billions of dollars into the broken US financial sector is a losing strategy.

In fact, bailouts just exacerbate the problem. Instead, we should let failed banks and other financial institutions go bankrupt.

For instance, by handing out billions of dollars to Fannie Mae and Freddie Mac, the government sent a message that failure will be rewarded. What the government really should have done is let the enterprises go bankrupt.

That's because, in the short term, a small number of well-known institutions going bankrupt sends a signal that the government is using common sense and letting the free market do its thing.

Beyond this, it's necessary to end the Federal Reserve's unjust, Soviet-style central monetary planning. After all, with big time players such as investor Jim Rogers questioning the role of the Fed, it could be that we're on the verge of a shift in opinion about the role of government in regulating the economy.

But where should this shift lead us?

First of all, to end the Fed's obstruction of the free market, it's necessary to reassess the institution's relationship with the banking sector. For instance, since the Fed is the primary reason big banks can take ever-greater risks, it needs to have its position as the "lender of last resort" reevaluated.

Basically, if banks keep operating under the assumption that the Fed will bail them out when their risky practices fail, the boom-bust cycle will never end.

Second, the Fed needs to leave interest rates alone, as manipulating them only prolongs recessions. Instead, interest rates should float freely to fill their natural purpose of recalibrating the market to actual conditions, not artificial figures.

### 6. Introducing a gold standard and encouraging deflation may be the best ways to avoid future crises. 

At this point it's clear that a government's ability to print as much money as it wants leads to economic crises and poor investments. We need an alternative. But what exactly would that be?

Money that's connected to a commodity standard is a great way to limit government interference in the economy. That's because, unlike paper money which can be printed endlessly, a commodity standard is tied to a supply of material like gold, which can only increase as more is discovered.

But don't worry, this doesn't mean you'll have to walk around with a bag full of gold to buy your groceries! A paper substitute could be linked to the value of gold, and people could exchange paper money for gold at any time.

The government is naturally against such an initiative because, without the ability to print more paper money, it would need to use borrowing or tax changes to affect the economy. And it would be a lot easier for the public to protest such actions than remain perpetually in the dark, considering the secretive inflationary policies currently used by the government.

But that's not the only reason such a system would be beneficial. Another reason is that while inflation is bad for the economy, deflation can be good for it. That's because inflation is an increase in the money supply while deflation — the opposite — means lower consumer prices.

Some critics of a gold-based commodity standard say that having such a system would cause deflation, since the supply of consumer goods would consistently outpace that of gold. These critics maintain that falling prices would cause an economic crisis.

However, a 2004 study revealed that 90 percent of deflationary periods in the last 100 years (barring those that occurred during the Great Depression) did not in fact lead to economic depression. Deflation is, after all, a natural process in any growing capitalist economy.

Consider the technology market. While the quality-adjusted price of computers fell by 90 percent from 1980 to 1999, manufacturers were shipping out nearly 100 times more units by the end of that same period. This single example offers proof that deflation can also be good for both consumers and producers.

### 7. Final summary 

The key message in this book:

**While the mainstream media maintains that rampant capitalism caused the 2008 financial crisis, the federal government is actually to blame. That's because by depressing interest rates and fostering economic bubbles, the government caused the near disintegration of the US economy.**

Actionable advice:

**Lobby the government to stop its endless spending!**

When the government spends more money than it collects in taxes, where does the remainder come from? From debts that cause interest rates to rise. So when the government spends too much, it has to borrow money and then push down interest rates by pouring money into the economy, thereby devaluing the dollar and prompting an economic crisis. Thus cutting government spending is necessary — and as citizens, we need to tell the government to do so.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Misbehavior of Markets_** **by Benoit Mandelbrot and Richard L. Hudson**

The financial theories you learn about in school are coherent, neat, convenient — and wrong. In fact, they're so wrong that they might also be dangerous: in underestimating the risk of markets, we inadvertently set ourselves up for catastrophe. _The Misbehavior of Markets_ lays out the flaws of economic orthodoxy, and offers a novel alternative: fractal geometry.

### 1. What’s in it for me? Facts, failure-proofing and maybe even preventing the next apocalypse. 

When you hear the word _meltdown_, what comes to mind — an accident in a nuclear reactor, maybe? Once you've been through these blinks, you'll also be able to call on dozens of examples of specifically modern kinds of meltdown, their causes and how to prevent the same happening to you or your organization.

We're living in an age of unprecedented technical capability: transport, commerce, medicine, power… you name it, the systems around us are more advanced and yet also more complex than ever before. That's why authors Chris Clearfield and András Tilcsik wrote _Meltdown_, to unpack the ways complexity leads to failure. And they're making the antidotes available to us all.

In these blinks, we'll cover the core components that lead to modern system failure and how to tackle them. Then we'll break down the tools needed to failure-proof systems and organizations — like structured decision-making, diversity, dissent, reflection, iteration and a list of warning signs.

In these blinks, you'll learn

  * how the Fukushima nuclear disaster could have been avoided;

  * why diversity makes organizations stronger; and

  * how parents can learn from ER teams and harness the power of iteration.

### 2. Modern systems often fail for similar reasons in very different contexts. 

What do BP's oil spill in the Gulf of Mexico, the Fukushima nuclear disaster and the global financial crisis all have in common? Yes, they're all crises, but they also share the same underlying causes.

Modern systems are more capable than ever, yet increased capability has also driven up complexity and made systems less forgiving. Take the finance industry, for instance: the switch from face-to-face to computerized stock-market trading has helped reduce operational costs, increased trading speed and given more control over transactions. But the digital shift has also made the system harder to understand and increased the chance of complex, unexpected interactions. Finance has become a perfect example of what sociology professor Charles Perrow would call a _complex_, _tightly coupled_ system. 

Perrow was an expert on organizations hired in the late 1970s to investigate the causes of a nuclear accident in Pennsylvania. What he discovered revolutionized the science of catastrophic failure. 

Perrow identified a combination of small failures behind the disaster that interacted in a domino effect. Instead of blaming the nuclear plant's operators or calling it a freak occurrence, Perrow saw that the accident had been caused by features inherent in the plant as a system — complexity and tight coupling. 

Tight coupling is an engineering term for when a system is unforgiving or has little buffer between its parts. The system for cooking a Thanksgiving dinner, for instance, is tightly coupled: the meal involves many elements that depend on each other, like stuffing that cooks inside the turkey and gravy that comes from the roasted bird's juices. And with only one oven in most houses, one dish could set back all the rest.

Complexity in a system means that it's non-linear and hard to see inside. If we stick with the Thanksgiving dinner analogy, cooking a turkey is a complex system, because it's hard to see inside the bird to tell if it's cooked. Complexity in a system makes it tough to identify problems and their knock-on effects.

A combination of complexity and tight coupling takes us into what Perrow calls the _Danger Zone_. This is where meltdown — the collapse or breakdown of a system — becomes highly likely. So that tightly coupled, complex Thanksgiving dinner could well be doomed unless precautions are taken.

Perrow's _complexity/coupling_ formula reveals the shared DNA behind all kinds of modern meltdowns, so failure in one industry can now provide lessons in other fields. We'll find out how in the following blinks.

### 3. Failure can be avoided by reducing complexity and increasing the buffer between the parts of any system. 

When you go for a drive you put on a seatbelt even though you don't know the exact nature of any accident that could happen. You simply know the danger is there and that the seatbelt could potentially save your life. In the same way, Perrow's complexity/coupling formula helps prevent failure without showing the exact form it could take.

This means that you can plan ahead to reduce complexity, for instance by increasing the transparency of a system. Failure to do so can even lead to fatal accidents. Take 27-year-old _Star Trek_ actor Anton Yelchin, who, in 2016, died getting out of his Jeep Grand Cherokee when the vehicle rolled and pinned him against a brick pillar. 

The reason behind this tragic event was the design of the car's gearshift. It was elegant but it didn't clearly show whether the car was in "park," "drive," or "reverse." In other words, the system was unnecessarily opaque and complex, which led to Yelchin wrongly assuming the vehicle would remain still. Tragedy could have been avoided had the gearshift been designed more transparently and indicated clearly in which mode the Jeep was. 

Transparency reduces complexity, making it hard to do the wrong thing — and easier to realize if you've made a mistake.

Sometimes, though, transparency isn't possible. If you think of an expedition to climb Mount Everest, there are dozens of hidden risks from crevasses and falling rocks to avalanches and sudden weather changes. The mountain is always going to be an opaque system. So mountaineering companies troubleshoot small problems, like delayed flights, supply problems and digestive ailments _before_ they can accumulate into major crises. This stops such problems from hindering the final climb, where there's little margin for error.

When the complexity won't shift, there's always the _buffer._

Nuclear engineer turned management consultant, Gary Miller, describes how he saved a bakery chain from failed expansion by increasing their buffer. Before rollout, he spotted that their new menu was overly complex and relied on an intricate network of suppliers. When they refused to address this, Miller persuaded them to relax their aggressive launch schedule instead, which allowed them enough slack to deal with problems when they inevitably surfaced. 

Perrow's complexity/coupling formula helps figure out _if_ a project or business is vulnerable to failure and _where._ It identifies vulnerabilities in a system, even if it can't tell you exactly what will go wrong. As Miller says: "You don't need to predict it to prevent it."

### 4. Using structured decision-making tools can help you avoid disasters big and small. 

We often go through life making snap judgments or using our instincts. There's no harm in that until we find ourselves operating in a complex system.

Engineers of the Fukushima Daiichi nuclear plant in Japan used their instincts when they falsely estimated the height of their tsunami defense wall. On March 11, 2011, an earthquake caused a wave several meters higher than they had planned for, which flooded the generators responsible for cooling and triggered the world's worst nuclear accident in 25 years.

But how could the engineers have done any better?

Defenses of this kind are vast and expensive to build, and as they couldn't build an infinitely tall wall, the engineers had to anticipate a height that they were _very sure_ would work. To do this, they used a _confidence interval_, a calculation based on comparing the highest probable wave height and the lowest. The trouble is that humans are not very good at making these kinds of forecasts: the ranges we draw are too narrow. 

One solution is to use a structured decision-making tool called _SPIES_, or _Subjective Probability Interval Estimates_. It pushes us to consider a broader range of outcomes. Instead of just assessing the best and worst plausible scenarios, SPIES estimates the probability of several outcomes within the entire range of possibilities. It's not perfect, but studies consistently show that this method hits the correct answer more frequently than other forecasting methods. Had the Fukushima engineers used SPIES, they could have safeguarded against overconfidence and been less likely to overlook the seemingly implausible scenario that flooded their defenses. 

Another structured decision-making tool is the use of _predetermined criteria_, which helps us focus on the factors that really matter. Take the _Ottawa Ankle Rules_, for example: this set of predetermined criteria was developed in Canada in the early 1990s and to reduce doctors' use of unnecessary X-rays of feet and ankles by a third. By focusing only on pain, age, weight-bearing and bone tenderness to decide whether an X-ray was necessary, they avoided getting side-tracked by irrelevant things, like swelling.

In complex systems like medicine and tsunami prediction, the effects of our decisions are hard to understand or learn from, and intuition often fails us. That's when tools like SPIES and predetermined criteria can provide an interruption to business as usual, allowing us to approach our choices systematically.

### 5. Complex systems give off warning signals that we can use to save lives, money and reputations. 

We often choose to ignore warning signs — if your toilet blocks, for instance, would you simply consider it a minor inconvenience, or see it as a warning sign of an impending flood? 

Ignoring warning signs can sometimes have catastrophic consequences. In Washington DC in 2005, three metro trains came within a few feet of crashing deep under the Potomac River. Only luck and quick action by the train drivers saved the day. Engineers suspected that the underlying cause was a problem with the track sensors, but before they got around to fixing it, the problem went away. So they invented a testing procedure, hoping to ensure the same glitch couldn't happen again elsewhere. The trouble was, their bosses soon forgot about this near-miss and stopped running the tests. Four years later, the same error showed up in a different spot, causing a horrific crash and the deaths of nine people.

We often ignore the clues in small errors, as long as things turn out OK. That near-disaster in 2005 was a warning sign that the metro organization chose to ignore. An essential feature of complex systems is that we can't find all the problems just by thinking about them. Luckily, before things fall apart, most systems give off warnings.

Unlike the DC metro, the commercial airline industry is a prime example of how paying attention to small errors can pay off. By doing so, they have collectively reduced fatal accidents from 40 in every one million departures to two in every _ten million_ over the past 60 years in a process called _anomalizing_. 

Here's how it works:

First, data needs to be gathered on all flights. Next, the issues need to be raised and fixed — incident reports shouldn't gather dust in a suggestion box. The third step is to understand and address the root causes instead of seeing mistakes as a series of isolated incidents. If pilots on a particular route keep flying dangerously low, then there could be an underlying signal or signage problem, for instance.

The final step is to share learnings, which sends a clear message that mistakes are normal and helps colleagues anticipate issues that they might one day face. 

In systems like air and metro travel, we can learn from specific operational incidents. Business owners, on the other hand, can learn from a dedicated team or a trusted adviser hired to identify threats from competitors, technological disruptions and regulatory changes.

### 6. Encouraging dissent makes teams more effective and systems stronger. 

Speaking up isn't easy. In fact, neuroscience shows us that a desire to conform is hard-wired into our brains. But that doesn't make dissent any less valuable. Here's why:

In a strange-but-true study of airline crew errors, the US National Transportation Safety Board (NTSB) found that between 1978 and 1990 nearly _three-quarters_ of major accidents happened when it was the captain's turn to fly. That is, not the less-experienced first officer. That was alarming because the captains were flying 50 percent of the time, so their errors should have been equal to or less than their deputies'. 

So the NTSB dug deeper.

They confirmed that the captains weren't worse at their jobs — far from it — but that their seniority meant their mistakes were going unchallenged. Their first officers lacked the tools with which to give the captain feedback and were bottling up concerns or giving vague hints instead of raising alarms. Hierarchy was putting lives in danger.

So the airlines introduced a groundbreaking training program called _Crew Resource Management_ ( _CRM_ ). The pilots thought it so basic that they jokingly called it _charm school,_ but it broke the taboo around raising concerns. CRM radically reduced the number of accidents _and_ leveled responsibility to 50:50 between pilots and their deputies. By democratizing safety, the program empowered everyone from cabin crew to baggage handlers to voice concerns, also harnessing the motivational power of shared responsibility. 

So how do you encourage dissent in other types of organizations?

One effective way is through _open_, as opposed to _directive_ leadership. _Directive leaders_ will state their own preferred solutions at the start of a conversation and tell colleagues that the goal is to all come to an agreement. 

An _open leader_ will hold back their own opinion until last and encourage colleagues to discuss as many perspectives as possible. Amazingly, this simple technique is proven to yield more possible solutions and almost twice as many facts, which makes for a better-informed discussion. Simple!

If hierarchies, social concerns and even our brains' wiring work against dissent, then it's paramount for leaders to nurture more than just an open-door policy. As dissent expert Jim Detert explains, you need to actively encourage people to speak up, or else you'll be discouraging them.

### 7. Building diverse teams helps reduce risk and improve results for organizations. 

Most people agree that diversity is a fair and positive thing, right? But did you know that it's also been shown to reduce risk for organizations?

In a landmark 2014 study, scientists proved the benefits of ethnic diversity when it comes to decision-making. In a simplified stock-market simulation, they observed dozens of diverse and homogenous groups as far afield as Singapore and Texas and assessed the accuracy of their trading. And guess what? The diverse groups performed markedly better than homogenous ones, pricing stocks more accurately and making fewer mistakes. Fascinatingly, the study found that crashes were more severe and price bubbles more frequent in homogenous markets because homogenous groups put too much faith in each others' decisions, which caused errors to multiply. In diverse markets, participants were more critical of each others' decisions and copied less often, which led to more rational decision-making. 

In hindsight, we can apply the lessons of this study to understand the financial crash of 2007 and 2008. Former Citigroup CFO Sallie Krawcheck stated in a 2014 interview that those responsible for the crash weren't "a bunch of evil geniuses" able to foresee the financial downturn, but that they _were_ "peas in a pod." Krawcheck blamed the lack of diversity for the poor decisions leading up to the crash and argued that diversity makes it more permissible to ask questions without looking stupid or worrying you'll lose your job.

So why don't all companies have compulsory diversity schemes? They must be effective, right?

Wrong. In a 2016 _Harvard Business Review_ paper, sociologists Frank Dobbin and Alexandra Kalev found that the most frequently used diversity programs failed to get results. Not only that, but over the past three decades and in more than 800 US firms, compulsory diversity schemes actually made organizations _less diverse._ They found that managers had rebelled against compulsory schemes because they felt they were being policed. They had resisted hiring diversely just to assert their autonomy.

Luckily there are other solutions that work. Over the same three decades, voluntary as opposed to compulsory mentoring schemes were proven successful in helping diverse candidates progress. These schemes naturally reduced bias with positive messaging: managers felt like they were being exposed to new talent pools rather than having their hiring decisions policed. The study also found that formal mentoring schemes were more effective than informal ones because white male executives didn't feel comfortable approaching young women and minority men informally. Allocating them mentees removed this awkwardness and allowed them to mentor a diverse range of junior employees. 

With benefits like increasing accuracy and avoiding the next financial crash, diversity is the safe alternative to homogeneity and groupthink. The healthy skepticism that comes with diversity makes organizations and decisions stronger.

### 8. Reflection and iteration are essential coping strategies for high-pressure scenarios. 

We've all been there — with the end of a project in sight there's a strong urge to rush to the finish, even if the conditions have changed. 

Pilots call it _get-there-itis,_ but the technical term is _plan continuation bias_, and it's an alarmingly common factor in airline accidents. If you're only 15 minutes away from your destination, and the weather changes, it's much harder to divert to a nearby airport than when you've just started out. Perhaps you've experienced the same effect when working toward a deadline? 

Pilot Brian Schiff knew the dangers of get-there-itis when he refused to take a furious Steve Jobs on his charter flight. In spite of pressure and entreaties, Schiff remembered his training and calculated that hot weather, heavy luggage and hilly terrain would make take-off in their small plane risky. Schiff still remembers how intimidated he felt as a puny 20-year-old in the firing line of Jobs' wrath. But he didn't budge and refused to fly. Schiff stood up to a very important customer, but instead of being reprimanded, he was rewarded for pausing and prioritizing safety in a high-pressure situation, and saw his pay doubled. 

But sometimes there's no time for reflection when situations change: in an emergency room, for instance, medics have to balance caregiving tasks like resuscitation and administering medication with monitoring the patient's overall condition. This is where an _iterative_ process becomes indispensable.

Effective iteration includes three basic steps: tasks, monitoring, then diagnosis — or offering a solution. 

The three steps are then repeated in a cycle to evaluate and improve solutions on the go. 

A great domestic example of this is a parenting strategy from the authors of a paper called "Agile Practices for Families: Iterating with Children and Parents." To improve the chaos of their family's morning routine, the Starrs decided to hold regular family meetings to discuss what had gone well that week, what they could improve and what they would commit to improving in the week to follow.

After committing to changes, they would repeat the questions at the next meeting, allowing them to focus on the most effective solutions over time. Their experiment was so successful that when _New York Times_ columnist Bruce Feiler visited their home, he described theirs as "one of the most astonishing family dynamics I have ever seen." 

You can use iteration to check in whenever you have a backlog of tasks and deadlines. The key is to go through the steps and then _re-evaluate_ once you've tried a solution.

### 9. Final summary 

The key message in these blinks:

**We live in the golden age of meltdowns, but it's within our reach to bring that era to an end. The solutions listed in these blinks are hard to implement because they often go against our natural instincts or accepted organizational and cultural norms. But if we give due consideration to complexity and tight coupling, we can unlock greater innovation and productivity in modern systems while avoiding catastrophic failure.**

Actionable advice:

**Give yourself a pre-mortem.**

You might have heard of a post-mortem, but did you know that its inverse — _a pre-mortem_ — can help to prevent failure? 

When planning a project, try imagining failure as a foregone conclusion. Research shows that you'll then think of far more potential glitches than if you'd imagined what success would look like. This technique harnesses what psychologists call _prospective hindsight_, and it's also useful for finding more concrete and precise reasons for an outcome. __

So next time you're at the planning stage, instead of asking, "how can we make this work?" try "what could have caused this to fail massively?"

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Originals_** **, by Adam Grant**

In these blinks, you've learned why even the most well-crafted systems sometimes fail. But you've also learned some of the things we can do to make sure we catch the cracks in the system before disaster strikes. One important takeaway is that every team needs to have its share of non-conformists, people who can think outside the box and who aren't afraid of criticizing the status quo.

But non-conformity and the ability to think differently aren't just important in avoiding the meltdown of systems. In _Originals_, author Adam Grant argues that non-conformity and creativity are indispensable for anyone who wants to succeed in life. But how do you encourage creativity and non-conformity in yourself? Grant has some good advice. So if you want to give your creativity a boost, head over to our blinks to _Originals_.
---

### Thomas E. Woods, Jr.

Thomas E. Woods, Jr. is a senior fellow at the Mises Institute. An award-winning author, he wrote the _New York Times_ ' bestselling book, _The Politically Incorrect Guide to American History._

