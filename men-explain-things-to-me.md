---
id: 5723860bd1d9bc0003210c54
slug: men-explain-things-to-me-en
published_date: 2016-05-04T00:00:00.000+00:00
author: Rebecca Solnit
title: Men Explain Things To Me
subtitle: And Other Essays
main_color: E9C735
text_color: 826F1E
---

# Men Explain Things To Me

_And Other Essays_

**Rebecca Solnit**

_Men Explain Things To Me_ (2014) is a collection of essays that examine the range of misogyny in our culture, from everyday microaggressions to legal systems that fail to punish rape. Solnit explains how sexism perpetuates itself, and what we can all do to eliminate it.

---
### 1. What’s in it for me? Get enraged by the real stories of gender discrimination. 

If you've ever experienced men talking over your head in a meeting, been groped in a crowded elevator, or chosen your route home according to street lighting, chances are you're a woman.

We often think that men and women have become equals over the last few decades, but the experience of feeling unsafe, overlooked or patronized is still more common for women than men. So how can this be?

Well, the truth is that even though we've made progress, gender discrimination is very much alive. It affects all women, even if not all men are perpetrators.

So let's change that and take a look at the female perspective.

In these blinks, you'll find out

  * why men think they know more, even when a woman literally wrote the book on the subject;

  * what horrible crime is committed at least every six minutes against women in the United States; and

  * how a member of Congress made a distinction between legitimate and illegitimate rape.

**This is a Blinkist staff pick**

_"_ _These blinks were interesting to me as someone who grew up being taught that everything was equal between men and women, then found herself experiencing a world that told a different story. These blinks represent an informative brief covering subtle sexism and sexual and physical abuse against women."_

– Clare, Editorial Quality Lead at Blinkist

### 2. Men explain things to women because they think their knowledge and expertise is superior. 

Have you ever found yourself listening to a man explain to you how things _really_ are? If you're a woman, you undoubtedly have.

Men often assume (perhaps unconsciously) that they're more intelligent or knowledgeable than women, so they feel the need to teach them in a condescending or patronizing way. This is also known as _mansplaining_, a combination of "man" and "explaining."

The _New York Times_ dubbed "mansplaining" Word of the Year in 2010, and it had entered mainstream political journalism by 2012. The author didn't invent the term, but she's credited with popularizing it. She herself feels the word is a bit unclear, however, as it seems to imply that all men do this. Women do it too, though far less often.

Mansplaining occurs because of sexism and men's arrogance regarding women's knowledge, reliability and expertise.

Even an established writer like the author isn't safe from this kind of subtle, everyday sexism. In fact, a man once interrupted her to tell her about a great book related to a subject they were discussing, implying he knew more about it than she did. As he kept talking, the author realized he was telling her about _her own book_.

More extreme manifestations of this phenomenon occur in other parts of the world. In some countries in the Middle East, a woman's testimony of rape doesn't have weight unless there was another man there to witness it. A woman's word is considered less reliable than a man's word.

### 3. Gender discrimination forces women to fear for their lives and safety. 

Mansplaining is frustrating, but women face much more threatening forms of gender discrimination.

Women are perpetually at risk of falling victim to physical or sexual violence from men. A rape is reported every 6.2 minutes in the United States, and it's estimated that about four-fifths of rapes go unreported. That means there could be a rape occurring almost every minute.

In America, over a thousand women are murdered every year by their ex-spouses. That's roughly three women per day. In fact, it's the main cause of death for pregnant women in the country. A woman is beaten every nine seconds, too.

If people aren't shocked by these statistics, that's another part of the problem. The high levels of violence women face aren't the only challenge to overcome: society's response is just as problematic.

Individuals and the media often try to justify violence toward women instead of examining it as the systemic problem that it is. In the United States alone, there are 12 _murder-suicides_ committed by men against women every week. A murder-suicide is when a person kills someone, then kills him- or herself.

Unfortunately, the media often seeks to depict murder-suicides as isolated cases caused by class differences, mental health problems, concussions or depression. These explanations neglect the powerful role of misogyny. When women try to speak up against this endemic misogyny, they're often subjected to even more misogyny and threats of violence from men.

This happened to Anita Sarkeesian, a media critic, when she spoke out against misogyny in gaming culture. There was a massive backlash against her: one man even designed a game in which Sarkeesian was the target and players got points for beating her up. Cuts and bruises appeared on the image of her face.

Violence toward women manifests in many forms — not only in actual physical abuse, but also in the form of threats.

### 4. Not all men are violent or sexist, but all women live in fear of sexual violence and sexism. 

On 23 May 2014, a 22-year-old man's hatred of women induced him to murder six women at the University of California, Santa Barbara.

Women have to fear this kind of hate everyday, no matter what men think. That's why the hashtag #YesAllWomen, which began trending soon after the shooting, became so powerful. It served to counter the typical responses men give when the issue of violence against women comes up, like "Not all men are rapists," or "Not all men are out to get you." These responses only allow men to distance themselves from the problem.

These typical male responses miss the point about systemic gender violence by derailing the conversation and making it about their own innocence. One tweet, written by a woman named Jenny Chiu, summed it up perfectly: "Sure #NotAllMen are misogynists and rapists. That's not the point. The point is that #YesAllWomen live in fear of the ones that are."

Moreover, female victims of sexual abuse are often accused of lying when they come forward with their stories. This happened when Dominique Strauss-Kahn, the head of the IMF, allegedly assaulted Nafissatou Diallo, an African immigrant maid who worked in a luxury hotel in New York City.

When the case went public, Strauss-Kahn slandered Diallo's reputation by having tabloid newspapers run stories claiming she was a prostitute. The criminal charges against him were eventually dropped. Diallo won her civil court case, but silence was one of the terms of the private settlement that was reached.

#YesAllWomen is also powerful because it addresses the global nature of the problem. Women were abused in Tahrir Square during the Egyptian Revolution, bride burnings and honor killings still occur in some parts of South Asia and the Middle East, and rape is used as a weapon of war in Congo and Sudan. The list goes on.

> _"An exasperated women remarked to me, 'What do [men] want — a cookie for not hitting, raping, or threatening women?'"_

### 5. Men perpetuate sexism and sexual violence by blaming women and controlling those who speak out. 

Do you know the story of "The Boy Who Cried Wolf"? Women experience that story in reverse when it comes to sexual violence: they cry and cry and no one believes them.

Men psychologically and socially silence women as another form of control. Some of these controlling mechanisms are even built into our legal and political systems.

For example, before the _Married Women's Property Acts_ of 1870 and 1882, a woman's property legally belonged to her husband, regardless of her income or inheritance. Laws incriminating domestic violence were passed in the United States and Europe around the same time, but they weren't enforced until the 1970s.

Society's desire to control women also manifests in the debate about reproductive rights. Todd Akin, a former member of the House of Representatives, tried to deny a woman's right to abortion after rape as late as 2012 by saying, "If it's a legitimate rape, the female body has ways to try to shut that whole thing down."

Abusers know that casting doubt on a woman's credibility is another tool for keeping them under control. Women who speak up about sexism or abuse are routinely dismissed as hysterical, delusional or unable to take a joke. Men also often accuse women of making false accusations in an attempt to reframe the male perpetrators as the victims.

False rape accusations are very rare. A 2013 study by the UK Crown Prosecution Service found that of the 5,651 rape accusations that occurred in the period they studied, only 35 were prosecuted as false accusations — less than 1 percent.

When women are demonized for speaking out, it only adds insult to the injury they've already endured.

### 6. We have to discuss the problems of sexism and gendered violence if we want to eliminate them. 

We've made a lot of progress in the fight against gendered violence but there's still a lot to do.

The recognition that misogyny and sexism are societal problems that don't exclusively affect women was a big step forward. Consider the 2012 gang rape and murder of Jyoti Singh in New Delhi, for instance. She was a 23-year-old physiotherapy student traveling with her boyfriend on a private bus. Six other passengers, including the driver, raped and killed her and then beat her boyfriend.

The incident sparked outrage in India. Masses of protestors and activists pushed for a better legal process for punishing perpetrators of gendered violence.

More people are becoming allies in the fight against sexism. Domestic violence statistics are still high, but they've dropped considerably in recent decades.

It's important that we use the right language in the ongoing fight against these problems. Conversations help raise awareness, and we've been developing new vocabulary that helps us understand it better.

In late 2012, for example, the term _rape culture_ began to spread widely. Rape culture refers to the cultural practices that normalize or trivialize acts of rape or other forms of sexual assault. Examples include _victim blaming_, when people say things like, "She was asking for it" or suggest that a woman invited rape with her clothing. The glorification of men who "score" woman as sexual conquests is also part of rape culture.

The term "rape culture" is important because it addresses the systemic nature of these problems: they're deeply imbedded in the societies we live in.

Calling out casual instances of sexism, like someone saying, "She's a slut" or "I bought you dinner, so I deserve to sleep with you," also helps chip away at the problem. Even just a small step like that can help us eliminate these societal ills.

### 7. Final summary 

The key message in this book:

**Misogyny is deep-rooted and manifests in many forms, from subtle everyday occurrences like men subconsciously doubting women's expertise, to large-scale, systemic problems like courts that prioritize the testimony of rapists. Societal denial and efforts to silence women who speak out only serve to perpetuate the problem. Fortunately, we can all take steps to eradicate misogyny by educating ourselves on the terms that allow us to understand it, and calling it out when it occurs.**

Actionable advice:

**Question the media.**

The next time you hear a story about a woman accusing a powerful man of rape, look closely at the way the media depicts her and her testimony. Think about the cases involving Woody Allen, Roman Polanski, Phil Spector and Bill Cosby. Remember: these aren't isolated incidents. Sexual abuse isn't unusual — it's just unusual for victims to come forward about it.

**Suggested further reading:** ** _We Should All Be Feminists_** **by Chimamanda Adichie**

In _We Should All Be Feminists_ (2014), Chimamanda Ngozi Adichie expands on her much admired TEDx talk to address our deepest misconceptions about feminism. By masterfully interweaving personal anecdotes, philosophy and her talent for prose, she explains how men and women are far from being equal, how women are systematically discriminated against and what we can do about it.
---

### Rebecca Solnit

Rebecca Solnit is a writer, activist, historian, contributor to the _Guardian_ and contributing editor at _Harper's_. She's written 16 books on subjects as wide-ranging as art, politics, the environment, memory and the history of walking.

