---
id: 5681bb91704a88000700005f
slug: merchants-of-doubt-en
published_date: 2015-12-31T00:00:00.000+00:00
author: Naomi Oreskes & Erik M. Conway
title: Merchants of Doubt
subtitle: How a Handful of Scientists Obscured the Truth on Issues From Tobacco Smoke to Global Warming
main_color: 9E3125
text_color: 9E3125
---

# Merchants of Doubt

_How a Handful of Scientists Obscured the Truth on Issues From Tobacco Smoke to Global Warming_

**Naomi Oreskes & Erik M. Conway**

_Merchants of Doubt_ (2011) examines some of the world's major scientific debates on topics including the environment, smoking and nuclear weapons. These blinks will explain how a handful of extremely vocal scientists have heavily misrepresented these issues through the mainstream media, often with the goal of aiding corporate and industry interests.

---
### 1. What’s in it for me? Learn how certain industries and the US government sowed doubt about scientific evidence to protect their own interests. 

Is smoking bad for your health? Does nuclear weaponry present a danger to society? Do you believe the scientific explanations for acid rain, climate change and ozone layer depletion?

These questions may seem ridiculous to the modern-day reader, and most people accept that smoking is a health hazard, nuclear weapons pose a threat to humanity and that the science around climate change is airtight. But the answers to these questions might not have been so obvious if you had been asked them in the mid-twentieth century.

_Merchants of Doubt_ sheds light on a period during which the causes and harmful effects of many new phenomena were still being investigated — a time when misinformation and dishonest arguments were spread, with devious ulterior motives, by major business interests and the US government.

In these blinks, you'll discover

  * why, back in the '60s and '70s, you wouldn't imagine that smoking a cigarette was a bad idea;

  * how the US government managed to explain that climate change was nothing to worry about; and

  * how former US president Ronald Reagan felt about nuclear weapons.

### 2. The tobacco industry intentionally misinformed the public about the dangers of smoking. 

Nowadays, even a kid could tell you that smoking kills. But this now well-known fact was not such common knowledge in the second half of the twentieth century. It's shocking now, but plenty of people had no idea about smoking's adverse health effects. But did the tobacco industry itself know about its product's dirty secret?

Absolutely. 

In fact, they knew that smoking was harmful as early as the 1950s, when the tobacco industry first came under scrutiny regarding the harmful effects of cigarettes. Realizing that they had to take action, in 1953, the four biggest tobacco companies in the United States — American Tobacco, Benson and Hedges, Philip Morris and US Tobacco — joined forces in defense of their industry. 

Their strategy?

To hire a PR firm, Hill and Knowlton, to save tobacco's deteriorating image. This same decision would later be used as evidence in court to prove that the tobacco industry was well aware of its product's harmful effects, and had thus knowingly misled their customers. 

The strategy itself was simple: simply cast doubt upon the idea that smoking was bad for your health. So, as more research indicating tobacco's harmful effects began to emerge in the 1960s and '70s, the tobacco companies opted for the only strategy at their disposal: challenging scientifically proven facts by propagating doubt about their validity.

For instance, in 1979, the tobacco industry began a program that funded top universities like Harvard. They committed $45 million over six years for one purpose: to prove that smoking was not connected to health problems. 

But they didn't just fund universities; they also hired a respected scientist by the name of Frederick Seitz, who distributed the money himself and gave the tobacco industry's credibility an added boost. 

They even summoned scientists to testify in court that there was no connection between smoking tobacco and poor health. However, the industry could only suppress the truth for so long, and eventually people wisened up.

> _"A key strategy in the campaigns to market doubt was to create the appearance that the claims being promoted were scientific."_

### 3. The tobacco industry continued its campaign to cast doubt on the harms of secondhand smoke, as well as on science itself. 

Despite their best efforts, the tobacco industry couldn't keep its product's harmful effects under wraps. But when scientific evidence of smoking's health risks first came to light, it only concerned the act of smoking itself. In fact, the dangers of _passive tobacco smoking_ weren't proven and acknowledged until the 1980s!

In 1980 and '81, British and Japanese studies proved that non-smokers experienced decreased lung function after being in smoky environments. Then, in 1986, the US Surgeon General reported that second-hand smoke was _as_ dangerous as smoking. And finally, in 1992, the US Environmental Protection Agency, or EPA, issued a report detailing second-hand smoke's harmful effects. 

But the tobacco industry kept fighting the claims by disputing the science itself. So, while the industry's initial volley against anti-tobacco campaigns featured a variety of strategies that cast doubt on _specific_ scientific results, in their second barrage they waged a war on science itself. 

For example, they disputed the credibility of the EPA's "weight of evidence" approach — meaning that they had looked at all of the data and then published what the majority of studies had concluded. Seitz said that some studies were more respected than others, so lumping all the data together didn't constitute sound evidence. 

But that's not all; the industry also attacked the EPA review saying that it cited studies that gave _only_ a 90 percent assurance of certainty. This point was picked up by Fred Singer, another scientist fighting for tobacco interests, who used it dismiss the EPA's report as "junk" science that gave undue weight to imaginary problems. 

What both Seitz and Singer failed to mention was that the EPA report had been peer reviewed, not once but twice, and both times by a panel of scientists and consultants!

So, the tobacco industry misinformed the public and led them astray. But they're not the only ones guilty of such tricks. Read on to learn which other societal issues have fallen victim to similar tactics.

### 4. The scientific debate about nuclear weapons was artificially prolonged. 

Nuclear war has been a major topic of conversation since the first atomic warheads were developed and, as recently as the 1980s, the conversation about _how_ to prevent such a catastrophe was in full, heated swing. In fact, fear of nuclear war prompted scientists to wage their own war, against a government defense protocol of the 1980s. 

Here's what happened:

In the 1970s, US President Richard Nixon began an initiative known as _détente_, the aim of which was to establish peaceful relations between the Soviet Union and the United States. However, not everyone welcomed such efforts, and people like Fred Seitz, a man raised with the Soviet threat constantly in mind, did everything they could to convince the American government that the threat was real. 

The opponents of détente were successful, and when President Ronald Reagan entered office in 1980, a group of scientists sharing the conservative opinions of Seitz and company persuaded the president to sign on to the _Strategic Defense Initiative,_ or _SDI_ — a program that involved deploying weapons in space to intercept incoming nuclear missiles. 

Naturally, this initiative sparked controversy among scientists because with it came the threat of a nuclear war. By 1986, 6,500 scientists had taken a stand against the SDI. 

However, the scientific debate on the program was drawn out further by a few scientists. Fred Seitz was one of them, but he teamed up with two other conservative scientists, Edward Teller and Robert Jastrow. Together, the trio founded the George C. Marshall Institute, the main objective of which was to churn out propaganda about the Soviet threat and bolster the SDI. 

So, while the majority of scientists opposed the SDI, scientists at the Marshall Institute promoted an outsider critique of their peers using the _Fairness Doctrine_, a policy that obliges public TV and radio stations to give equal airtime to opposing views. 

This meant that although the ideas put forward by the Marshall Institute were broadly seen as scientifically invalid, they garnered enough public attention to keep the debate going.

### 5. The US government undermined scientific findings on acid rain in the 1970s. 

The latter half of the twentieth century saw the emergence of environmental issues that demanded the attention of scientists and politicians. One such problem was acid rain. In fact, the phenomenon has been studied in detail since the 1960s, and plenty of scientific evidence has been compiled on its origins and effects. 

Acid rain is essentially rain with a lower than normal pH concentration. It leads to a variety of problems such as decreased forest and plant growth, and it can kill animals such as fish. Over time, scientists began to trace acid rain to the burning of fossil fuels and determined that the pollution that causes it often occurs in a different geographical area than where the rain itself falls. 

But despite the data, the scientific evidence on acid rain was put aside by the US government throughout the 1970s. For instance, in the '70s, Canadian scientists found that 50 percent of the acid rain in Canada was a result of American emissions. Not only that, but the Canadian economy was highly dependent on resources that were being hurt by acid rain. 

So, in 1980, both countries formed technical working groups assigned to research the phenomenon. In 1981, their findings were reviewed by the US National Academy of sciences; but a year later, the White House commissioned a new panel of independent scientists to do the very same thing. 

The US government chose William A. Nierenberg to put the panel together, but then circumvented his authority to choose one of the panelists. His name was Fred Singer and he was primarily concerned with the financial costs of any proposed action to combat acid rain. 

As a result, the final review of the panel was edited so heavily that it made the results, and thus the proposed remedies, appear doubtful at best. In fact, a handwritten note found in Nierenberg's documents showed that he had even altered the review at the White House's behest!

> _"Lately science has shown us that contemporary industrial civilization is not sustainable."_

### 6. The hole in the ozone layer was debated into the 1990s. 

It's common for science to advance slowly, especially when industry interests are at stake, and this was especially true throughout the scientific discussions regarding the depletion of the ozone layer. It should thus come as no surprise that the conversation about whether there is in fact a hole in the ozone layer dates back almost 50 years. 

In the 1970s, scientists presented indisputable evidence that certain chemicals were causing the depletion of the ozone layer. Researchers had been investigating the effects of various chemicals on the ozone layer as early as the late-1960s, and in 1970, scientist James Lovelock shared his work: he hypothesized that certain chemicals, called _chlorofluorocarbons_, or _CFCs_, which were commonly used in aerosol products like hair spray, were in fact concentrating in the Earth's stratosphere, potentially depleting the planet's ozone. 

What followed was a rapid-fire scientific effort that came to fruition in 1985, with the discovery of a hole in the ozone layer over Antarctica. 

But the aerosol industry fought any scientific work on the effects of CFCs. To do so, industry representatives began campaigns to defend their products and named natural sources, like volcanic dust, as major causes of ozone depletion.

But despite industry attempts to stop them, the US government acted on reports made by the National Academy of Science, which linked CFCs to ozone depletion. As a result, in 1979, CFC propellant was made illegal, and in 1987, a United Nations regulation, known as the Montreal Protocol, insisted that countries which still produced CFCs cut their production in half. 

However, the aerosol industry wasn't done yet: during the '80s, they made increasingly desperate arguments against the science behind CFCs' effects. One of the people leading this fight?

That's right, Fred Singer. 

Singer published articles in media outlets like _The Washington Times_ as late as 1991, arguing that the scientific support for ozone depletion was too vague to be reputable.

### 7. The science proving global warming faced an uphill battle for recognition during the 1980s. 

It's clear that there have been alarming cases in which the public was misled about dire environmental issues, and another excellent example is global warming. Despite clear scientific proof, global warming wasn't taken seriously until the 1980s. 

Why?

In 1977, a group of physicists known as "the Jasons" concluded that increased atmospheric concentrations of CO2 would lead to rising global temperatures, especially at the Earth's poles. The White House-issued panel that reviewed their work reached essentially the same conclusion. 

But the government wasn't satisfied, and in 1980 they assembled another review panel called the Carbon Dioxide Assessment Committee. It was chaired by none other than William A. Nierenberg and intended to assess the condition of the climate and the potential issue of CO2. 

However, the committee faced a divide between the natural scientists, who insisted that global warming would occur, and the economists, who only saw the financial ramifications. The latter group framed the report by authoring its first and last chapters, which made the argument that the problem would most likely be resolved by new technology. And even if it wasn't, future generations would be able to adapt. 

It was this report that led the White House to dismiss calls for the regulation of fossil fuels. 

But the issue didn't stay quiet for long.

In 1988, the director of the Goddard Institute for Space Studies, James E. Hansen, said that global warming had started, and by doing so reawakened public interest. As a result, by 1994, 194 countries had signed the UN Framework Convention on Climate Change, which intended to stop global warming. 

Unfortunately, like countless other environmental issues, during the in-depth discussion on global warming, prominent scientists dismissed the idea as a media scare. By 1989, Nierenberg was with the Marshall Institute, and was claiming that global warming was a mere ruse. 

He went on to brief people at the White House on the institute's position: that any warming the planet was experiencing was merely due to solar activity.

> Only 56 % of the Americans who participated in a Time poll in 2006 thought that the average temperature has risen.

### 8. In the early 2000s, a long-dormant pesticide debate was resurrected as a posterchild attack on environmental regulations. 

Now that you know how science can be appropriated for political ends and lobbying, it's time for one last example: the pesticide _dichlordiphenyltrichlorethan_, or _DDT_. DDT was banned in 1972 because of its devastating environmental effects, which were first made public in the 1960s.

Here's what it does:

DDT kills insects, including those that carry malaria and typhus. In World War II, it was used for military purposes and, following the war, for agricultural ones. The problem is that the chemical concentrates in the food chain by remaining in the insects it kills. So, birds in the Catalina Islands still have DDT in their blood, even though the pesticide hasn't been used there for over 30 years. 

Seeing this danger, the American scientist Rachel Carson pointed to the negative effects of pesticides in general and DDT in particular. In 1962, she published a book called _Silent Spring_ that highlighted the harms associated with such chemicals. 

Although she initially faced harsh criticism, public opinion eventually changed, and in 1972, DDT was banned. However, the ban only covered commerical use in the United States, and it was still approved for domestic emergencies, as well as for export to countries facing malaria epidemics. 

Then, in the mid-2000s, Carson was vilified in an attempt to misrepresent environmental regulation. Her work was misconstrued by the media, and even major outlets like _The New York Times_ and _The Wall Street Journal_ accused Carson of bearing responsibility for human deaths supposedly caused by the DDT ban. 

Carson's critics focused on how malaria could have been overcome using DDT, while ignoring the fact that insects readily adapt to the drug and that it was never banned in malaria infected countries. 

It's clear that these attempts were a mere political stunt designed to turn people against environmental regulations in general. Ironically, the only people such regulations could actually harm are the very industry representatives who lobby so aggressively against them.

> _"The enemies of government regulation became the enemies of science."_

### 9. Final summary 

The key message in this book:

**Nearly every pressing public issue, from the danger of cigarettes to climate change, has been misrepresented by the media for political purposes. The weapon of choice for those attacking the scientific proof behind these problems has been the aggressive propagation of doubt and false information intended to mislead the public.**

**Suggested** **further** **reading:** ** _The Sixth Extinction_** **by Elizabeth Kolbert**

_The Sixth Extinction_ (2014) chronicles the history of species extinction and shows how humans have had more than a hand in the rapidly decreasing numbers of animal species on earth. Through industrialization and deforestation, not to mention climate change, humans have damaged the environment and disrupted habitats, leading to a massive reduction in biodiversity.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Naomi Oreskes & Erik M. Conway

Naomi Oreskes teaches at Harvard University where she specializes in the history of science. She has previously spent 15 years as a professor of history and science studies at the University of California, San Diego, and is also respected authority on geophysics and global warming.

Erik M. Conway is a historian and author, and currently works at the California Institute of Technology in Pasadena.

