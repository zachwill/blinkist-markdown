---
id: 592c01e2b238e1000507d11c
slug: messy-en
published_date: 2017-06-01T00:00:00.000+00:00
author: Tim Harford
title: Messy
subtitle: How to Be Creative and Resilient in a Tidy-Minded World
main_color: 60A442
text_color: 42702D
---

# Messy

_How to Be Creative and Resilient in a Tidy-Minded World_

**Tim Harford**

_Messy_ (2016) is all about order and tidiness, or rather, why they're overrated. These blinks explain how a preoccupation with neatness can stand between us and success, how messiness can boost creativity and why everyone should embrace a little disorder.

---
### 1. What’s in it for me? Learn to embrace messiness. 

If someone tells you that your desk — or your life — is a mess, it's not usually meant as a compliment.

But as you'll learn from these blinks, there's nothing wrong with messiness. You'll find out how life's little imperfections and ambiguities can boost your creativity — and why a tidy desk or a neatly organized schedule aren't nearly as helpful as you may think.

And you'll also discover

  * what's great about being distractible;

  * how a truly terrible old piano helped a jazz musician to excel; and

  * how one chess player wins by confusing his opponents.

### 2. We try to quantify and impose order on the world, but this has its pitfalls. 

Nowadays, everyone is talking about big data. Many hope that the proliferation of information will help us better predict future events like the rise of the Dow and the next torrential hurricane.

But the reality is, more data doesn't always mean more accurate predictions. In fact, whenever you quantify something, you're bound to pick up some random noise, such as errors in measurements. This poses problems for the big data approach since a model that takes into account all available information will suck up that noise, resulting in worse predictions.

Imagine you're comparing the past lows and highs of the stock prices for two oil companies. Your predictions regarding the future prices of both stocks will probably be better if you cut out data from rare or unpredictable events, like a massive oil spill that sent one company's stock plummeting.

Beyond that, the very act of measuring can distort the thing you're attempting to measure. Take heart surgeons, for example. If they're ranked by the number of successful surgeries they perform, some of them will naturally begin embellishing their numbers by hand-picking patients with the best prognoses.

Another human tendency is to prefer order over disorder. We like to see the world arranged into clear, predictable patterns. But actually, imposing order is not always beneficial.

Consider a 1990 Harvard study, where AnnaLee Saxenian compared two hubs of high-tech development, Route 128 in Massachusetts and California's Silicon Valley.

Silicon Valley came out on top, and the reason was its disorganized nature. Companies there let employees jump around from job to job and even allowed ideas and knowledge to be exchanged between firms. The result was a climate of cooperation that fostered innovation and made it easier to recruit experts, helping the entire area to prosper.

Meanwhile, the companies of Route 128 kept their businesses in neat little silos, walled off by legally binding non-compete contracts, which made it difficult for companies to react to new developments or to handle crises. As a result, the whole area suffered.

### 3. Disruptions and distractions prompt us to explore new avenues and get creative. 

In 1975, jazz pianist Keith Jarrett played a concert that would go down in history. For this performance in Cologne, he used an old, virtually unplayable Bösendorfer piano — the only one available at the venue.

Jarrett couldn't play the ancient piano like he would a new one. It was out of tune, too quiet, the pedals were sticky and the high notes had a tinny ring to them. So instead, he improvised.

To cope with the poor resonance, he played rumbling bass riffs. To boost the volume, he played while standing, pushing the keys harder and thereby giving the piece a new intensity. It was by playing in this unorthodox manner that he created a unique work of art.

This is not unusual: disruptions force us to find new, creative approaches. After all, as long as our habits and routines are functional, there's no need to alter them. Novel, potentially far-superior practices are usually discovered in periods of disruption.

Similarly, distractions can also spur creativity. In 1985, two Berkeley researchers gave blue and green slides to pairs of people, asking them to call out whichever color they saw. One person in each pair was actually an experimenter in disguise and had been given instructions to yell out the wrong answer at times, thereby distracting and confusing the real participants.

In the end, when these subjects were asked to free-associate words with the two colors, they came up with much more original answers than the control group.

Or consider a recent study by Shelley Carson that discovered a connection between distractibility and creativity. The study tested a group of students who had each accomplished some creative feat like publishing a novel or releasing an album. In the tests, 22 out of the 25 super-creative participants were found to be easily distracted by seemingly irrelevant details.

Clearly then, distractions and creativity are linked somehow.

Next up, we'll learn why neatly groomed workspaces are overrated, and why close teams might not be able to think outside the box.

### 4. The right workspace and network of people around you can enhance your creativity. 

Paul Erdös, the genius Hungarian mathematician, was extremely grateful for the inspiration he found through working with different peers. There were years when he would creatively collaborate with a colleague every ten days or so and, on average, write a new paper with a complete stranger every six weeks!

It might seem like a frenetic pace, but Erdös was definitely onto something. Working with a wide range of people fosters creativity.

This is due to a concept called the _strength of weak ties_, introduced by sociologist Mark Granovetter in 1973.

Working in groups has its advantages, but after a while, people in closely-knit teams will find their views converging. Conversely, opinions held in a network of weak personal connections are more diverse. As a result, people in a network of weak ties are exposed to a broader range of ideas, and they're also more likely to have their own ideas challenged.

Another factor that affects your innovative capacities is the space you work in.

Just take the Googleplex with its ping pong tables and slides. It's a great example of a fun workplace, known for fostering creativity. But Google was successful long before the Googleplex, and an important reason was the fact that employees were allowed to own and shape their environment. One Google engineer, for example, wanted to get rid of one of his office walls, so he knocked it down together with a colleague. No-one ever complained.

The lesson here is that the right to shape your workspace as you wish is empowering. It fosters a feeling that your opinion matters and that you can change things. This, in turn, will make you more engaged, creative and outspoken.

### 5. Daring to improvise can give you a serious competitive advantage. 

Martin Luther King Jr. used to work on his famous speeches meticulously. But in 1955, when Rosa Parks was arrested for refusing to give up her bus seat for a white passenger, King rushed to her aid with little time to prepare. He quickly organized a protest and improvised a speech, which was a huge success.

In fact, improvisation often promotes creativity. So how can you do it?

First, to make your ideas flow, you've got to stop censoring yourself. This is a risky step, but it's a risk you've got to take if you want to tap into the power of improvisation.

Just consider the march on Washington in 1963 where King tossed out his decent, yet uninspiring speech, opting for an improvised, unpredictable dialogue with the audience instead. You might have heard of this one; it's called the "I have a dream" speech.

Another example comes from the world of jazz: when researchers examined the brains of professional jazz pianists, they found that these musicians actually turn off the "inner critic" part of the prefrontal cortex that's responsible for judging and censoring their actions.

This non-judgemental mind-set is crucial since improvisation depends on creating unpredictability, a chaotic state only they feel comfortable with.

Take World Chess Champion Magnus Carlsen, for example. He wins so often because of his penchant for unconventional moves. His opponents can't anticipate such tactics and, feeling rattled, they often make mistakes — which Carlsen exploits.

And of course, there's also Donald Trump. He triumphed against his neatly groomed opponent because of his rapid-fire, headline-making Twitter responses that put everyone off balance.

> _"Trump made sure both the media and his opponents reacted on his terms."_

### 6. Too much automation and order can impede success. 

How many of your friends' phone numbers can you remember? Probably not many, because you've become reliant on your phone to store them.

People love this kind of automation, but one consequence is that we humans are becoming less skilled. There are many situations that we no longer need to handle ourselves, which is why we can't manage them when we actually need to.

Take plane crashes, for example. They're becoming rarer, but when they do happen, it's usually because the pilots aren't used to flying without the help of a computer. They've become reliant on a computer to do all the complex work of flying the plane, so when something unexpected happens, they can't handle it.

This is why we need to practice dealing with messy situations and break out of our overreliance on automation. But avoiding catastrophe isn't the only incentive to do so.

Embracing messiness and abandoning our obsession with order can also help us excel at work.

For one, meticulously organizing our files and emails tends to be a waste of time. In 2001, two researchers at AT&T Labs found that office workers who neatly file away all their documents only ended up with useless, bloated archives where they couldn't find anything.

Similarly, organizing your email is essentially pointless. It's quicker to use the search bar than to parse through a whole system of folders.

And don't fall into the trap of planning your days out too carefully in advance. This only makes you inflexible when something unexpected happens. A far better strategy is to devise a plan that's broad enough to leave room for maneuvering. For example, as governor of California, Arnold Schwarzenegger maintained a virtually empty calendar, helping him stay flexible and productive.

### 7. Final summary 

The key message in this book:

**Life is messy, and that's nothing to fear. Embrace the inevitable disorder of the world in the way you act, talk and think. You will be more creative and successful.**

Actionable advice:

**Encourage informal play to foster creativity in a child.**

Fostering your child's creative and social skills will help her cope with the messiness of life. Abandon your reliance on fancy toys and have her invent her own games. Informal games are much more demanding and therefore a better tool for developing your child's abilities. What's more, free play requires children to show empathy and make compromises to keep their peers involved and the game running.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Adapt_** **by Tim Harford**

The world is complex and unpredictable, from the inner workings of the humble toaster to the countless problems facing the very existence of mankind. Instead of applying traditional means of approaching problems and managing organizations, we must instead use trial and error. Only by experimenting, surviving the inevitable failures and analyzing the failures themselves, will we be able to adapt to the complex and changing environments we find ourselves in.
---

### Tim Harford

Tim Harford is an economist and award-winning journalist who writes for the _Financial Times_. He has written multiple bestselling books on economics and life, including _The Undercover Economist_.

