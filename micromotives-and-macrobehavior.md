---
id: 537c95573465300007b00000
slug: micromotives-and-macrobehavior-en
published_date: 2014-05-20T12:06:36.000+00:00
author: Thomas C. Schelling
title: Micromotives and Macrobehavior
subtitle: None
main_color: None
text_color: None
---

# Micromotives and Macrobehavior

_None_

**Thomas C. Schelling**

_Micromotives and Macrobehavior_ explores the links between individual and group behavior through the paradigm of economics and game theory. In particular, it describes the complexities and consequences of interdependent decisions, where individual behaviors produce surprising results at group level. It shows that we often fail to foresee how our actions, combined with the actions of others, can lead to unintended, significant and sometimes problematic outcomes.

---
### 1. What’s in it for me? Discover the explanatory power of game theory. 

The world is a complex place, and the more methods of analysis we can come up with, the better. During the last 50 years, economists like the Nobel Prize-winning Thomas C. Schelling have applied economic theory to wide areas such as international conflict and nuclear strategy with powerful results. Some of the most powerful models come from the field of game theory.

So what is game theory?

Game theory is the study of how rational individuals make decisions when the outcomes are influenced by other rational individuals' decisions. A famous example is the use — or lack of use — of nuclear weapons by superpowers. If one superpower uses a nuclear weapon, it is likely that another superpower will retaliate, which will lead to more nuclear weapons being used, and both parties being destroyed. This threat of "Mutually Assured Destruction" makes it unlikely that anyone will use a nuclear weapon, as they know that they too will be destroyed.

Game theory can also be used to analyze situations which are common in the social sciences. In particular, game theory can help explore the relationship between the behaviors of the individuals that make up a group, and the behavior of the group as a whole. What is surprising is that we can't simply predict group behavior by adding up individual behaviors. Instead, intriguing interactions start happening.

But to find out more you'll have to keep reading.

If you do you'll find out:

Why some political movements grow and become huge popular movements, while others shrivel and die.

Why speeding up the lifts at a ski resort actually makes waiting lines longer.

Why restricting people's freedom to choose sometimes leaves them better off.

### 2. Economics offers a powerful tool kit to analyze complex social behavior. 

Throughout history, people have tried to understand why humans behave in certain ways. For example, how do we make choices?

One way to answer this last question is to look at the realm of economics.

Although economics is usually associated with abstract topics like markets and money growth, it also offers a powerful tool kit to analyze human behavior.

One such tool is the subfield of _game_ _theory_, which proposes many useful models for understanding complex human behavior. It is used in economics to analyze, for example, people's interactions when they buy and sell goods in the marketplace. And because economic behavior is not that different to social behavior, the same tools can be used for both.

First of all, let's explore the economic concept of the _market_.

In economics, a market typically describes a complex of institutions — banks, shops, financial regulators — within which individuals pursue economic activities. These activities include lending and borrowing money, distributing and purchasing goods, offering jobs and getting hired.

Furthermore, the individual's activities are unguided, meaning that the government doesn't control these institutions beyond a basic system of laws and rules. 

Yet on the _aggregate_ _level_ — the level of all the individuals considered together — individual activities interlock perfectly and the overall system appears to run surprisingly smoothly. For example, when we go to the shop, the goods we want are usually there, and when we go to the bank, we can withdraw money easily — all this without any overseeing control. 

Now let's apply this market concept to social behavior. 

When we decide who to date and marry, we make use of the relationship and marriage market, which, just like an economic market, involves many individuals acting within an unregulated system of institutions — the many bars, clubs, parks and other places where we look for a date. And the final result? An overall effective system in which most of us find a partner — in the end.

### 3. Models are useful tools to explain social phenomena. 

So what's the best way to understand human behavior? One powerful method is to look at group behavior — why masses of people behave in a certain way. And the best way to look at this is through a _model_.

A model is a simple description of how complex systems work. For example, the _critical_ _mass_ model can explain why some social innovations succeed, and why others don't — for example, why some political movements fail, and others collapse altogether.

So what is critical mass?

Critical mass is the minimum number of active members necessary for a movement to be self-sustaining. If the amount of members falls below that level, the movement will collapse.

Say someone passionate starts a political organization. Many people turn up to the first meeting — but week after week, people gradually drift away. When attendance falls below a certain level — beneath the critical mass — people lose faith, start attending less and less and the movement collapses.

Some other models can be easily understood as simple metaphors — like the thermostat metaphor — which we can then apply to real life behavior.

So how does a thermostat work?

First, your thermostat raises the temperature to the set level, and overshoots it. Then the thermostat lowers the temperature until it undershoots the level — and the process begins again. Contrary to popular belief, it never just warms the room up and remains at the desired temperature: the process is cyclical.

This thermostat metaphor can be applied to real life situations: for example, the cycle of specialist shortages. Say there's a lack of engineers. When the lack becomes acute, study programs are launched to raise the numbers. Yet when people start graduating from these programs, society will soon find itself with too many engineers, which will lead in turn to the next inevitable shortage.

Why?

Because now the crowd of engineers create high competition and therefore low wages, which will make it an unpopular profession again. The number of engineers will then gradually fall to low levels once more, leading to the next shortage!

In the following blinks you'll learn more about the power of game theory to explore human behavior.

### 4. Changes within a closed system don't affect the overall outcome. 

Have you ever played the game musical chairs? Unless you change the rules, every turn ends the same: all but one of the players are left without a chair.

This childhood game serves as a metaphor for _closed_ _systems_, where the overall result remains the same even after changes. In musical chairs there are gradually fewer and fewer players, but the end of each turn has the same result.

Let's further explain with a real life example.

If you've ever waited forever in line at a ski lift, you might have dreamt of changing part of the system — for example, speeding up the chairs to reduce the waiting time.

However, speeding up the lift is actually counterproductive.

Why?

Because ski resorts are closed systems, where people move between four activities: going up the lift, coming down, standing in line and relaxing in a warming hut.

Time spent sitting in the hut is not affected by speeding up the lift, and neither is time spent coming down the slope. In contrast, the waiting time is influenced — negatively. If people get up the mountain faster, they will be back down to line up again sooner, making the lines longer! So every minute you save sitting in the speedier lift would add to your waiting time at the bottom of the slope.

So how can we actually reduce the waiting time?

We need to open up the system. If more seats or a second lift were added, fewer people would need to stand in line at any one time, and the waiting time would be reduced.

So be careful for what you wish for — sometimes you are stuck with the same result anyway!

### 5. Sometimes restricting people's freedom leaves them better off. 

Everyone accepts the fundamental value of freedom. But fewer people accept that sometimes freedom can limit us. This can be best explained by the most famous thought experiment in game theory: the _Prisoner's_ _Dilemma_.

So what is the Prisoner's Dilemma?

Two criminals — partners in crime — are arrested. The interrogator admits he lacks the evidence for the principal charge. However, he has enough to convict both on a lesser charge, and offers a deal to each of them separately: testify against the other prisoner on the principal charge, and all charges against you will be dropped.

There are therefore three possible outcomes:

A. Both betray each other and are sentenced to two years each.

B. One betrays and the other remains silent. The betrayer is set free while the betrayed is sentenced to three years.

C. Both remain silent and are sentenced to one year in prison each.

Now, you might be thinking: "Well, the best choice for both of them is to remain silent." But for each individual this isn't the case. Their best choice is actually to betray their partner.

Why?

Because if they take into consideration the fact that their partner might betray them, betrayal always offers a greater reward: if your partner betrays you, you still avoid the maximum sentence. But if they remain silent, you are set free. Therefore, because you aren't sure what the other will do, the rational decision is to betray.

This is why having the freedom to make a decision costs rather than helps the criminals. Without this freedom they both would have been facing one year each — but with it, they are most likely to receive two.

Let's look at a real life example.

In 1969, hockey player Ted Green suffered a fractured skull after he was struck by a hockey stick. His injury created a heated discussion about the need for mandatory helmets. Many players admitted they never wore a helmet only because they didn't want to appear as cowards.

However, they wouldn't mind — and would secretly welcome it — if the league limited their choice and made helmets mandatory.

### 6. Human behavior leads to little variation among members of social and economic groups. 

Some people are great athletes, some not so much. Yet members of any single sports club usually play at about the same level. Why does this happen? How do people end up in a club which just fits their skill level?

Because people like to be both good at what they do and feel challenged.

People like competition — as long as they are winning. So if most members at a club play far better than you, you will soon feel like a loser and eventually leave.

But the need for a challenge is just as strong as the need for competition — or else playing becomes boring.

Say you join a tennis club: you don't want to be always playing against opponents who are easy to beat. You want to face opponents who can challenge you, and push you to improve your skills.

These desires push us into certain groups which match the two conditions of competition and challenge, which results in _low_ _variations_ of abilities in groups.

People's preferences lead to low variation in many other systems as well.

For example, imagine an insurance scheme where every member pays the same premium, and only those who get injured are compensated. In this scenario, healthy, low-risk members get the worst deal: they pay the price but aren't rewarded.

Sooner or later, this bad deal will drive these low-risk members to leave the scheme for a cheaper one which excludes high risk clients. As a result, the proportion of high risk clients will increase, and so will the premiums. More low-risk members will leave, and the vicious cycle will continue until only a small high-mortality, high-risk group is left.

We are living in a highly diverse world. However, we often don't realize this fact because our preferences often lead us to associate with people similar to us — whether we intend to or not.

### 7. A slight preference for neighbors of the same race can cause and reinforce complete segregation. 

What kind of city do you leave in? Most North American cities consist mainly of residential areas, which are either nearly all white or black. Only a small fraction of neighborhoods have a population which doesn't consist of more than three quarters of one race. Why is that so?

Because even a mild preference for having similar neighbors can cause segregation.

How does this segregation emerge? Let's imagine the process with the help of a roll of pennies, a roll of dimes and a checkerboard.

Start by distributing an equal number of dimes and pennies randomly on the checkerboard, leaving approximately half the squares blank.

Let's assume that every dime wants at least half of its neighbors to also be dimes. Pennies are less fussy, and are satisfied if at least one third of their neighbors are pennies too.

Now move every penny and dime whose immediate neighborhood don't comply with these preferences to the nearest empty square. Repeat until all coins on the board are "satisfied."

What happened? Usually you should end up with highly segregated areas of just pennies and dimes — despite initially moderate preferences.

This is known as the _Schelling_ _Segregation_ _Model_ and can show how peoples' initial desire to have a few neighbors similar to themselves can soon lead to the development of highly segregated areas.

And once these areas have been developed, a preference to avoid being in an extreme minority can reinforce the segregation.

People who have to choose between polarized extremes, like choosing to move into a completely white or black neighborhood, will often choose in a way that reinforces polarization. This doesn't mean they prefer segregated neighborhoods — it just shows that they don't want to be in an extreme minority.

Ratios of blacks and whites in a residential block don't tell us much about their relationship. That's because even slight preferences to avoid minority status can create and reinforce complete segregation.

### 8. Selecting our children’s genes may have positive consequences – but would also create new conflicts. 

Imagine if you could choose your children's sex in advance. Or even their musical talent, or IQ. What would the consequences be?

It would allow you to create the child of your choice — but it would also create new areas of conflict between you and your family.

Why?

Because when you choose a certain characteristic, you can't isolate the individual favorable genes. Instead you have to choose whole chromosomes made up of many genes. This means that if you choose a chromosome that provides favorable characteristics it might have other less desirable traits attached.

For example, if your paternal grandmother's musical talent lies on the same chromosome as her bad eyesight — and you still pick it — you might later face accusations from your children like: "Why did you make me have bad eyesight?"

And choosing chromosomes could also become an area of conflict between you and your spouse. How would you explain to your husband that you don't want your child to inherit his stubbornness? 

Another potential consequence of designing our children is that attitudes to gender might change — for better and for worse.

One benefit could be that superstitions about childbirth in many cultures could evaporate. In some cultures, baby boys are a sign of virility or God's favor, and this creates a preference for male children. But if there was an easy way to choose your child's sex, having a daughter would be considered the parent's choice, not a sign of bad luck.

One negative consequence could be the widespread preference for one particular sex — such as a culture preferring boys — which could cause sex ratios to significantly deviate from the approximate 50–50 ratio. When the children belonging to the majority sex achieved marital age, they would struggle to find an opposite-sex partner. This would create a new social problem where governments might be forced to "stabilize" the birth ratio by, for example, providing tax breaks for people who have a baby of a particular sex.

### 9. The strategic value of possessing nuclear weapons is far too high to actually use them. 

In 1960, the British novelist, scientist and politician C.P. Snow declared that nuclear war was a "mathematical certainty" that would erupt within a decade — unless the nuclear powers drastically reduced their armaments.

Despite this warning, nuclear arsenals around the world have increased multiple times, and new powers have entered the nuclear game. But fortunately, this "mathematical certainty" did not occur.

Why?

Because the reluctance of the nuclear powers to employ their weapons has established a _tradition_ _of_ _non-use_.

Nuclear powers have repeatedly refrained from deploying nuclear weapons and have thus generated a nuclear "taboo." For example, the United States avoided using the atomic bomb during the Korean War, and the Soviet Union avoided using nuclear arms in Afghanistan — even though this decision might have cost them their victory.

They restrained themselves because they feared that even a single usage of a small nuclear weapon could lead to retaliation, which would be counter-retaliated, and so on in a nuclear escalation that would destroy both parties involved (and maybe the whole planet!). By acting this way they created a culture where nuclear abstinence is regarded as the only appropriate behavior. 

Another reason we are all still alive today is that nuclear weapons have become more valuable as a tool of _passive_ _political_ _influence_.

Let's clarify with an example.

What would happen if another country — for example, Iran — or a terrorist organization obtained a nuclear weapon?

There are two possible scenarios.

If they detonated it and killed people, it is very likely that the whole world would turn on them and obliterate them.

But if they held it in reserve, it would actually be much more valuable. This is because possession alone would make even superpowers like the United States or Russia reluctant to consider military action against them.

### 10. Final Summary 

The key message in this book:

**Economic** **models,** **and** **especially** **game** **theory,** **can** **be** **effectively** **used** **to** **explain** **social** **behavior.** **By** **elucidating** **the** **links** **between** **individual** **and** **group** **behavior,** **game** **theory** **can** **help** **us** **take** **control** **of** **the** **complex** **situations** **that** **involve** **us** **all** **–** **for** **the** **better.**

Actionable advice:

**Get** **a** **clear** **understanding** **of** **a** **system** **before** **you** **try** **to** **change** **it.**

It's easy for us to think that we've figured out the simple solution to a big problem. But sometimes systems behave in counterintuitive ways, so we should try to understand them with as many different models as possible before we intervene — and potentially make things worse than they previously were.
---

### Thomas C. Schelling

Thomas C. Schelling is an economist and professor of foreign affairs and national security at the University of Maryland's School of Public Policy.In 2005 he received the Nobel Prize in Economics for his contributions to game theory. His other books include _The Strategy of Conflict_ and _Arms and Influence_.

