---
id: 54a9903762333200097e0000
slug: millennial-money-en
published_date: 2015-01-08T00:00:00.000+00:00
author: Patrick O’Shaughnessy
title: Millennial Money
subtitle: How Young Investors Can Build a Fortune
main_color: 3EAB45
text_color: 2B7830
---

# Millennial Money

_How Young Investors Can Build a Fortune_

**Patrick O’Shaughnessy**

Although technological advances have made it easier than ever to invest in the stock market, today's Millennials (young adults born between 1980 and 2000) tend to be risk-averse. But this kind of thinking is misguided. Given that benefits like Social Security and retirement pensions are imperiled, it has never been more important for young people to start investing in their future financial security.

---
### 1. What’s in it for me? Discover how you can invest in the stock market and become financially secure. 

If you are a (relatively) young person do you think about retirement? More than likely you don't, and even if you do, you'll be thinking, "I don't need to consider saving for old age, it's 40 years away."

Yet this view is completely wrong. No matter how young we are, we should always give thought to the future. This is especially true for Millennials, the generation born between 1980 and 2000. To put it bluntly, if Millennials want to have a peaceful, comfortable retirement, they have to act now. If they fail to, they will find life as pensioners very tough indeed.

So what should Millennials be doing to be secure in old age? They should invest in the stock market. These blinks will show you how to do so effectively.

In these blinks you will discover

  * why you won't be able to rely on a state pension in the future;

  * how you can turn $10,000 into $4.7 million; and

  * why you shouldn't follow the crowd when investing.

### 2. To secure your financial future, start investing in the stock market as early as possible. 

Imagine yourself in 50 years: What kind of life would you like to have? Do you want to live comfortably, with lots of money saved, or would you rather be dependent on a tiny pension, feeling pangs of anxiety with every purchase?

The choice between these two realities is an obvious one. So what can you do to achieve lasting financial stability?

Although many believe that creating a savings account is the best way to prepare for the financial future, unfortunately, that's not the case. The truth is, interest rates on savings accounts are typically lower than the rate of inflation (annual price increases). Meaning: Money parked in a savings account actually _loses_ value, in terms of its real world purchasing power.

So if not savings accounts, then what? Well, you should _invest_ in the stock market as early as possible.

Don't underestimate the potential rewards of starting to invest early in life. After all, when you start young, your money has more time to multiply in value.

For instance, if you invest $10,000 every year on the stock exchange with an annual return of seven percent, you'll earn $4.7 million by the time you're 65 — _if_ you started investing at age 22. On the other hand, if you made the initial investment only at age 40, you'll end up with just $1 million.

Although the advantages of investing young are clear, many Millennials aren't doing so, partly because they entered adulthood in the midst of the 2008 financial crisis — the worst downturn since the Great Depression of the 1930s.

And as a consequence of the financial collapse, Millennials tend to be more risk-averse than their parents' generation. For example, a 2014 survey on risk found that only 28 percent of Millennials' money was invested in stocks, compared to the 46 percent invested by other generations.

### 3. For Millennials, rising public debt and an aging population spell an uncertain financial future. 

Some people think, "I don't have to invest for the future because when I retire, I'll have a pension!" But what if you don't?

It's a real possibility. After all, America's population is aging, leading to increasing pension costs for the government.

Consider the fact that average life expectancy increased from 47 in 1900, to 79 in 2013. Today, there are more people living past retirement age than ever before.

As a consequence, the government has higher costs for pensions and health care. Medicare spending, which has grown by 11 percent every year since it was first established in 1966, is one example of this trend.

And unfortunately, this upward trajectory will likely remain stable in the short-term, especially since baby boomers (the enormous generation born after World War Two) are just starting to retire.

And ultimately, since these rising health care and retirement costs are untenable, there's no guarantee that the government will continue to pay retirement benefits to future generations.

Remember, our government runs like any business: If it doesn't keep its accounts balanced, it risks bankruptcy. Or to put it more concretely, the government has to receive enough in taxes to match benefit spending.

But that's not currently happening: According to recent calculations, there's a $9.6 trillion gap between projected spending on benefits over the next 75 years, and what the government expects to raise through taxes.

For Millennials, that's a huge problem. It means that although this younger generation is currently paying taxes to support the elderly, it's unlikely to receive the same benefits when it reaches retirement age.

So this mean Millennials will probably have to support themselves without government assistance. And if they hope to do so, then they should start investing in the stock market as early as possible.

### 4. If you’re planning for your long-term future, invest in stock markets all across the world. 

If you've decided to start investing in the stock market, you're probably wondering where to begin. Well, the first guiding principle is to invest in companies all across the world.

Many people don't follow this advice: They put all their money into a few companies located in just one country.

But in the long-term, this is a risky strategy. Because if the stock market in that country struggles, you could potentially lose _all_ your money.

Consider Japan's stock market index, the Nikkei, which was growing at 30 percent in 1989. Although many people made huge investments in this seemingly "strong" market, the massive growth turned out to be a product of an unstable financial bubble — and the bubble burst in 1991. Ultimately, a 1989 Nikkei investor's portfolio would only be worth half as much in 2013.

As you can see, putting all your eggs in one basket is dangerous — so why do people do it? Well, many get swept up in the false promise of a stock market bubble.

Others feel that they're investing wisely because they're buying shares in companies they "know." For example, in 2010, American investors had 72 percent of their money in US stocks. Simply put, Americans prefer to invest in familiar companies, like Burger King and General Motors.

But if you want to invest successfully, get out of your safety zone and spread your investments over large, diverse areas. This approach will protect your money even if one company or economy collapses.

It's also worth noting that diversifying will help you earn more, as global currencies change in value. For instance, say you invest in Samsung, which is based in South Korea. If the South Korean won strengthens against the US dollar by six percent, the value of your investment will also increase by six percent.

In other words, when you buy stock market shares, you're not only investing in the company, you're also investing in the currency.

### 5. If you want to maximize your earnings, follow investment strategies that diverge from the mainstream. 

Many people determine their investment strategies by what the "experts" recommend.

And often those investment experts will recommend a tactic called _Sector Leaders Strategy_ — that is, buying shares of the best-performing companies determined by a particular _market index_, which tracks a portion of the stock market. The Standard & Poor 500, for example, is based on the biggest customers listed on the exchange.

So to follow the Sector Leaders Strategy, you could invest in an _index fund_, which aggregates stocks from the 500 biggest companies. Since index funds are calculated automatically and don't require much human brainpower to run efficiently, they're cheap to buy and also secure — which makes them an attractive investment option.

However, if you want to earn the highest returns on the money you invest, you shouldn't follow the herd. Instead, you need to _be different_.

Because although index funds tend to give good returns, they'll never beat the market — especially if they aim to match the returns of the market index.

But there's another reason to consider alternatives to index funds: The biggest companies tend to underperform when they reach the top. For instance, major American companies like Apple and Intel typically start posting a negative annual return once they hit the $400 billion mark. Therefore, investing in these firms may not be the most profitable choice.

By contrast, you can try following the _Sector Bargains Strategy_ — which entails buying the cheapest stock, not the best — to maximize your returns.

Although it might seem counterintuitive, these kinds of approaches often outperform the leader strategy in the long-run. For instance, since 1979, the value of the Russell 3000 (an index fund which follows, more or less, a bargain strategy) has outperformed the S&P 500 by more than 1,100 percent.

### 6. Choose an investment strategy that combines several good selection criteria. 

As we've already seen, being _different_ is the best investment approach. So let's examine this principle a little more closely.

For our purposes, the ideal investment strategy — let's call it the _Millennial Money Strategy_ — can be summarized thus: Buy cheap stock from valuable companies.

Why is this the best strategy? Well, by buying a cheap stock (that is, one with a lower share price), you're stretching your investment and potentially maximizing your future returns.

And as we noted, you're trying to find a true bargain — not merely the cheapest stock available. So when you're buying shares, you need to ensure that the company has value and that the (currently low) share price is likely to rise in the future.

To that end, what can you do to evaluate a company's long-term prospects? Start by looking at how much the company earns; also pay attention to how much cash it attracts (in some cases, reported earnings are actually coming out of credit or loans).

Then, to find the best bargain, compare the company's stock price to its cash flow. This relationship indicates _value/valuation_. Ultimately, you're trying to identify companies that have a cheap share price and secure finances.

You can also boost this strategy by finding the right momentum and buying shares just as the market notices a company's potential. An easy way of finding a stock that's riding a wave of momentum is to look out for cheaper shares that have seen substantial price increases over the last six months.

Ultimately, you should try to carve out an investment approach that combines value-seeking with momentum. And if you need a little more convincing, consider the fact that since 1972, companies that have matched these criteria have grown at an annual rate that outpaces market growth by a factor of two.

### 7. Don’t let your instincts drive your investment decisions. 

Humans are hard-wired to make investment mistakes; essentially, we're programmed to be irrationally fearful.

In fact, it's even been scientifically proven that we're subject to something called _constructive paranoia_ — that is, excessive sensitivity to loss.

Consider a Stanford study that asked students to make 20 investment decisions. During each round, students had to decide whether they wanted to invest $1. A coin toss decided the outcome of the investment: If the penny came up heads, the students lost $1; when it landed on tails, they earned $2.50. As it turned out, after experiencing a loss, students were only willing to invest 41 percent of the time. However, rationally speaking, the decision didn't make sense — because there was a 50 percent chance of winning a high reward!

This tendency affects the market. For instance, investors often buy at market peaks (for fear of missing out) and sell at market bottoms (because they're scared of losing everything). So plenty of people irrationally buy high and sell low, contrary to the most basic maxim of stock trading.

But constructive paranoia isn't the only investing mistake built into our programming: so is greediness. In general, we tend to pursue rewards even when our chances of getting these rewards are dismal.

And in large part, this impulse drives financial bubbles: Even though there's a strong likelihood that the bubble will burst, people still invest, because they're blinded by the possibility of future rewards.

Although you can never eliminate your instincts, it is possible to separate them from your investments by creating an automated system.

For instance, you could set up an automatic payment from your bank account into your investment account. You would determine the amount and regularity of the payment in advance, as well as the fund you'd like to purchase with the money.

So the idea is that once you've determined a particular strategy, you should apply it systematically, without letting your instincts get in the way. And having an automatized payment system is one way of doing this.

### 8. Long-term thinking is the path to profitable investment. 

Sometimes inexperienced investors think they need to sell their shares as soon as they start losing value. And yet, this kind of short-term thinking typically isn't profitable.

So why do we go for it in the first place? Well, our biology plays a big role: Humans have an emotional center, located in the brain (the limbic system), that pushes us to make decisions that will provide instant gratification.

This impulse explains why investors prefer to buy bonds during periods of financial crisis. Since bonds bounce less than stocks in the short term, they provide instant gratification — in the sense that they protect the investor from dealing with negative numbers. That way, the investor feels that her wealth is protected and preserved.

Short-term thinking also afflicts professional investors, who are preoccupied with their short-term career horizons. These professionals often create investment strategies designed to be profitable over a two- or three-year period, in order to demonstrate successful results to current or future employers.

And yet, when we look at the data, it's clear that the most profitable investments need several decades of time (around 30 years) to mature.

What's the reason for this? Well, although the market tends to fluctuate a lot, it mostly moves in short-term patterns. So although we often think negatively and tend to anticipate financial downturns, the market typically has an upward trajectory over the longer term.

For instance, when we look at one-year periods, we can see that stocks lose real value 31 percent of the time, on average. However, indexes of stocks never lose money over a 20-year period. On the other hand, bonds have been shown to lose their value half of the time over the same 20-year period.

### 9. Final summary 

The key message:

**In order to secure their financial future, Millennials should start investing in the stock market as early as possible. There are just a few simple guidelines that lead to investment success in the long-term: Diversify your portfolio to include stocks all over the world, stick to your strategy, and make investment decisions that go against the flow.**

Actionable advice:

**Make taxes a factor in your investing.**

You should pay attention to tax advantages when investing: if you keep your winning/most profitable stocks for at least one year, your gains will be taxed at a lower tax rate (long-term capitals tax rate).

**Suggested further reading:** ** _Young Money_** **by Kevin Roose**

Kevin Roose spent three years following eight young Wall Streeters in an attempt to find how the 2007 crash — and its aftermath — influenced the financial industry. _Young Money_ paints a decidedly grim picture of junior analysts who find themselves on a non-stop rollercoaster of all-nighters and extreme stress, while earning six-figure incomes.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Patrick O’Shaughnessy

Patrick O'Shaughnessy is a portfolio manager and a writer for publications like the _Wall Street Journal_.

