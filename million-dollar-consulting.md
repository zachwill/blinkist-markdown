---
id: 53e0ee6132306100075f0000
slug: million-dollar-consulting-en
published_date: 2014-08-05T00:00:00.000+00:00
author: Alan Weiss
title: Million Dollar Consulting
subtitle: The Professional's Guide to Growing a Practice
main_color: A8BCD7
text_color: 586270
---

# Million Dollar Consulting

_The Professional's Guide to Growing a Practice_

**Alan Weiss**

This fully revised fourth edition of the 1992 classic, _Million_ _Dollar_ _Consulting_, walks you through everything you'll need to compete — and win — in the highly lucrative and busy world of consulting. _Million_ _Dollar_ _Consulting_ offers you the tools you need to attract clients, organize your pipeline, and grow your current consulting business into a million-dollar one.

---
### 1. What’s in it for me? Start your own million-dollar consulting practice. 

Not everyone is satisfied with the grind of a nine-to-five job in the corporate world, and many people choose to take their knowledge, skills, and experience with them to another, more lucrative industry: consulting.

But as any successful consultant knows, solo consulting isn't just about amassing a large fortune. In fact, success in consulting involves surmounting a dramatic learning curve and collecting broad and diverse experiences and skills. It brings the possibility to make changes that can potentially impact the lives of tens of thousands of people.

These blinks will teach you how to set up your own company and acquire the kinds of clients that will make your million-dollar business. They'll also give you tips — some of which are quite counterintuitive — for success in this industry.

In these blinks, you will also learn:

  * why successful consultants actually fire their clients,

  * why putting relationships before money will actually make you wealthier,

  * how to ask for $50,000,

  * why you should never _ever_ enter negotiations with the "gatekeeper," and

  * why your free time is your true wealth.

### 2. A consultant adds value to their customers’ cause. 

What does it take to become a doctor or a lawyer? That's easy: there is a strict set of requirements you need to fulfill, like passing the bar or doing your residency, after which you're awarded a certificate that allows you to practice.

While the requirements for becoming a doctor or lawyer, like those for many other professions, are quite narrow, virtually _anyone_ can call themselves a consultant. If that's the case, then what does the word actually mean?

A _consultant_ is someone who has a unique set of skills and talents that help to create the value-adding components that their clients' businesses lack. The value that they add comes from two sources: content expertise and process expertise.

_Content_ _expertise_ is earned through study and work in a specialized field, where the wealth of your experience and professional relationships lie. In other words, this is your professional "comfort zone," where you are the most competent.

This expertise is rooted in the specific skills and talents that made you successful in one particular industry or field of study in the first place. If you are consulting for a specialized firm — for example, as an expert witness in a legal dispute — then you are acting as a content consultant.

This contrasts with _process_ _expertise_, which transcends specific industries, is applicable in almost any environment, and involves a set of highly effective methods.

For example, the company Bain & Co. has spent years conducting strategic planning projects, and thus has developed the experience and the methodology necessary to consider themselves as having expertise in strategic processes. Consequently, they tend to specialize in just that area, as they know it best.

For solo consultants especially, process expertise is often more valuable than content consulting. This is because process expertise has a wider range of applications across a broad spectrum of diverse industries, which can help to make up for a lack of content expertise.

### 3. Get discovered by making yourself visible. 

Starting a business isn't easy, and often the biggest hurdle is attracting the attention of potential clients to your services. So how do you do it?

There are two major ways in which organizations find consultants: word of mouth and an outstanding body of work.

_Word-of-mouth_ _marketing_ is by far the simplest and most effective of these two methods, as it leads the clients directly to you.

Unfortunately, since word-of-mouth marketing is based on direct recommendations by clients or someone else who has at least heard about your services, the process is therefore almost completely out of your hands.

In spite of this, it can be wildly successful. In fact, the author has traced many of his most valuable projects back to recommendations by clients or other completely unexpected sources.

It is therefore important that, apart from delivering first-class work, you treat every single interaction with a prospective client as a personal "moment of truth," and always think in long-term relationships. In the end, consulting is a "relationship business," and your success depends upon the relationships you build.

Of course, you can also help this process along by asking your satisfied clients for testimonials and recommendations directly.

Word-of-mouth marketing must be backed up by actual value, so it's important to keep a record of your success as well as your intellectual property in order to build your reputation and showcase your body of work.

Everything you've done — from hard-copy publishing, speaking, interviews and product sales, to websites and newsletters — enhances your visibility. Start producing intellectual property early in order to develop your body of work, and use this as references to your credibility rather than as mere sales vehicles.

The best way to get people to see the value you can add to their business is by showing them the value you've added to others'.

### 4. Focus on the big picture and don’t let your job get in the way of your career. 

How do you measure the quality of your work? Many consultants get tangled up in quantifying their tasks with arbitrary measurements, like the number of meetings held or reports written. In reality, the quality of your work comes down to one thing and one thing only: the value you add to your client's business.

In this way, consulting is about results, _not_ tasks. If you just fulfill tasks and define deliverables without connecting them to a specific value-adding result, you're just creating a heavy and unproductive workload for yourself, such as writing reports and sitting through meetings devoid of goals.

To avoid this, you should enter an agreement with your client in which "the 'how' is subordinated to the 'what.'" In other words, a fulfilling and positive consultant–client relationship is one in which the end result, rather than the method, is what's important.

However, in order to forge this kind of positive relationship, you will have to clarify to your clients that your work should be measured in terms of the value you add, rather than, for example, the time it takes to complete the project.

To do this successfully, you will need to come to an agreement on what the objectives are and how they will be measured _before_ you even write a proposal.

This requires collaboration, and, unsurprisingly, the best consultants collaborate with their clients. This is because the kinds of value a consultant adds — roughly divided into resolving an issue or transferring skills — can change depending on the specific needs of the client.

Whereas an _interventionist_ imbues an organization with highly effective skills, and an _independent_ _expert_ resolves many important issues critical to a company's success, only a _collaborator_ is able to do both at the same time.

By demonstrating their flexibility, collaborators not only offer the most value to their clients' organizations but also tend to have the strongest and longest-lasting relationships with the client, and thus the follow-up projects and referrals that come with them.

### 5. Be crystal clear about your strategic goals. 

What is it that defines your consulting business and sets it apart from the crowd? Every consulting firm has some unique motivational force behind it that sets out their strategic goals and paves the path for their business.

Unfortunately, companies too often formulate mission statements such as, "We want to help clients to achieve the best results possible," or "We want to be on the cutting edge of our field."

The problem is that these statements are far too vague and generic. Good mission statements get specific in their goals and their intended results. For example, if you want to improve customer service, you might have a strategy like this: "We will design and implement workshops that use customer feedback to create demonstrable changes in behavior on the job."

Or, if you specialize in process optimization, you might "assist clients in enhancing staff productivity through needs analysis, enhanced communication and joint decision making."

No matter how you formulate your strategy, the key is to show _how_ value is added, and thus differentiate your service from others.

This strategic goal will always be underpinned by this primary motivator: to grow, both financially and personally.

There is virtually no other profession with a steeper learning curve than consulting. Consequently, your ability to add value to your clients' operations increases with every new experience, gained ability and learned skill, all of which, in turn, justify a rise in your fees.

At the same time, every assignment will ideally leave you with a new valuable contact or strengthened relationship with your client, thus improving your network and word-of-mouth marketing.

This means that you should _always_ get something out of your contracts. If a business doesn't meet your growth strategy — for example, if the project doesn't meet your fee structure, is unchallenging or doesn't otherwise help you grow — don't do it.

### 6. Sometimes you need to fire clients in order to grow. 

Has your doctor ever told you that there's something you need to cut out of your life — maybe smoking or candy bars — in order to improve your health? Surprisingly, the same is true in the world of consulting: sometimes consultants fail to grow because they refuse to abandon bad clients.

This is because consultancy firms operate differently to other businesses, such as soft drink manufacturers, who will always try to sell to as many people as possible. In order to grow — both financially as well as in terms of client relationships and professional experiences — you will need to let go of less lucrative businesses.

As your wealth of experience continues to grow, your fees should grow with them. Not only is this good for your wallet, but it's also good for your reputation. If you become known as an "inexpensive alternative" desperate to accept any job, you will receive the pay and repute that come with such a perception.

Instead, ensure that the quality of your work is always above the quantity, i.e., one $50,000 project will always be more valuable to you than ten $5,000 ones. In fact, the time and cost required to sell a $100,000 project or a $10,000 project is generally very similar, thus making bigger projects all the more valuable to you.

And since the value you can provide increases with your expertise and your abilities, it only makes sense for you to raise your fees accordingly.

Consequently your strategic goals should incorporate the aim to accumulate increasingly higher-value assignments.

One good way to ensure that this actually happens is to look back at all of your assignments every two years and identify the bottom 15 percent that you then choose to no longer accept, thus making room for more lucrative business.

If you don't grow continually, your success will eventually erode beneath you. By constantly looking for opportunities to expand your business, you will steadily climb, growing your wealth with each step.

### 7. Consulting is a “relationship business,” so a likeable brand is crucial. 

A consultant's most crucial actions are to differentiate their business and find their niche — but how do you achieve this? The answer lies in branding yourself and building relationships.

Although we've been told not to, people really do judge a book by its cover. Knowing this, you should be sure you create a personal brand that others will both like and recognize.

Like it or not, part of this brand is your personal presentation. Have a few nice suits and accessories, and strive to behave like the professional you are. Try to stay natural, avoiding things like hairdryers, potent fragrances and tanning salons.

However, it is not your face, but your _name_, and the logo that accompanies it, that is your ultimate trademark. Thus you should always use your logo on everything you produce. Potential clients need to be able to recognize your work in an instant in order to make it easy for them to call on you.

You also want your business to be a legally recognized entity. Generally, if you're not incorporated, you're considered an amateur. You'll never be perceived as a true professional, and will consequently lose out on many lucrative business opportunities.

It's also important to consider _how_ you differentiate yourself: specialization makes you competitive and differentiated services make you distinct, but your _relationship_ with the client will determine whether you are perceived as a valuable collaborator or a one-timer.

The ideal relationship is one where the client has confidence in the consultant to make decisions on his own, act responsibly and in the client's best interest.

One simple way to gain trust is to provide your best clients with your private telephone number. The ability to call upon you during tough times provides a feeling of safety and partnership.

In addition, you shouldn't be afraid to take a stand on crucial issues, even when internal politics threaten the success of your project. In the end, good clients will understand that you are acting in their best interests, and will only trust you more.

### 8. Focus on value and partnership when negotiating with a potential client. 

Consultants who don't "make it" are often unsuccessful because they fail to convince the client of their value. This is because there are a number of hurdles that can make negotiations difficult.

Firstly, you will often find yourself sitting across from a _gatekeeper_ at the negotiating table — someone who is authorized to say "no" but not "yes," like, for example, HR personnel or a mid-level manager. Under _no_ circumstance should you ever enter negotiations with a gatekeeper and you should _always_ insist on the presence of the person who actually makes the ultimate decisions and writes the checks.

Secondly, you will have to overcome the four main reasons clients use to reject your services: no money, no urgency, no need, and no trust.

_No_ _money_ _or_ _urgency_ : The key to overcoming this hurdle is to focus solely on value in your negotiations. If you can successfully show how the issues your potential client faces will worsen over time, and explain exactly what kind of value you'll deliver, then your services will appear more urgently required and your fees more reasonable.

_No_ _need_ : Indeed, "the essence of marketing is to create need." Generally speaking, clients know exactly what they want, but not always what they need. To surmount this hurdle, you'll need to identify what their needs are, show them why they need these things, and then explain the value you can add by helping them meet their needs.

_No_ _trust_ : If you and the client have no relationship, then there is no trust upon which successful negotiations can rest. To develop this relationship, you have to identify the issues that excite or frustrate your client and provide candid feedback, positioning yourself "as potential partner, not as a salesperson or a sycophant," and convincing them that you have their best interests in mind.

If you can succeed in surmounting these obstacles, then they'll have no choice but to say "yes."

### 9. Don’t charge for time, but instead for value. 

It can sometimes be difficult to translate the services you provide into a dollar amount. Many consultants make the mistake of charging a _per_ _diem_ fee, but unfortunately for them, this is not only counterproductive, but can even be considered unethical.

This is because the time you spend on a project has _no_ intrinsic value for the customer in and of itself. The only thing that the client is interested in is improvement, or value.

If, however, your fees are based on the time spent on a project, then your goal is actually to maximize time rather than add value. Such a pay scale puts you and your client in opposition to one another: the client wants fast and profound improvement, and you want longer project times.

In reality, however, value-based fees and shorter project times are far more appropriate for meeting your client's need for fast results and your need for personal, professional, and financial growth.

The same applies for tasks. Clients don't care about the tasks — they care about results. Furthermore, since tasks are repetitive, they therefore lose value the more with each repetition due to economies of scale. Outcomes, however, only become more valuable as they accumulate.

So how should you structure your fees? Here are some things you'll want to consider:

What qualitative and quantitative benefits will you provide your client? How will affect your client's reputation? How much emotional energy will you need to invest in the project?

You should also consider the scope of the project, the number of people who will have access to your services, and the approximate duration of the project.

Ultimately, the only way to get the fees you deserve is to convince your client of the value you add and then simply _ask_ for them. Many people, however, can't ask for a huge amount of money convincingly. Make it easier on yourself by practicing saying, "the fee will be $50,000" in front of a mirror in order to convey the confidence that justifies the fee.

### 10. Don’t be fooled by immediate success, and always keep your pipeline evenly filled. 

How far ahead do you plan for your future? If you're like most consultants, you don't look much further than your next paycheck. This tunnel vision is what causes most consultants to miss the omens of bad times ahead. In order to stay above water, you'll need to closely monitor the volume and source of your incoming assignments.

The _pipeline_ _approach_ is one way to gain perspective on your incoming assignments. Your pipeline indicates all of your long-term, short-term, and immediate projects along a timeline, all of which are "signed and sealed," thus representing safe income.

Your pipeline should be filled with short-term sales you've closed, repeat business from current clients, and other business that comes by referral. You should also make plenty of time for prospects and direct marketing targets.

And because most companies have a policy that does not allow multi-fiscal-year agreements, most pipelines will represent a 12-month period. For you, this means that your pipeline offers a realistic estimate of yearly cash flow and profit.

A poorly structured pipeline, however, can lead to a world of problems.

For starters, a dry pipeline puts you under pressure to acquire new, short-term business, which almost always translates into poor business, poor fees, and fewer growth opportunities.

Additionally, when your projects are unevenly distributed or clumped together at some point in the pipeline, you will probably have to hire contractors or enter an agreement with another consultant in order to complete all the work. This, in turn, reduces your margin and opportunities for growth.

You should also take care to monitor your pipeline. If you notice a lack of new prospects and targets as well as a lack of repeat business from your existing clients, it might be that either your marketing efforts are ineffective or that you've failed to form strong relationships with your clients.

> _"There is a tendency in the consulting business to look no further than the latest client check."_

### 11. Diversification and investment are the best protections against bad times. 

As a result of the economic crisis of 2008, many consultancy firms suffered a loss of business and drastic cutbacks in their expenses. Others, however, were able to actually grow their business in spite of this economic hardship. So what did they do differently?

The answer: they _invested_ in their businesses instead of cutting costs, and made use of their highly diversified services and excellent client relationships.

You can think of the economy as acting like a hydraulic system: "When something goes down, something else goes up." Therefore, with the right strategy, you can actually use a crisis to your advantage.

Regardless of the economic climate, however, you should diversify your services — both your content and your process expertise — in order to address the industries that thrive in booms and those that thrive in busts.

Indeed, some industries — for example, the pet care and health industries — are quite resilient in economic depressions, and you want to be able to capture that business when the time is right.

Another way to protect your business is by expanding your market geographically. This strategy will ease the pain of regional economic downturns and keep you from being overwhelmed by local competitors.

And if you do fall into slow times, you can focus your time away from clients and become productive in other areas — most importantly marketing. For example, you could make it a habit to call your old clients and partners in order to maintain a good relationship, write articles for a magazine, or give speeches on the challenges of the crisis.

Finally, most competitors will start divesting in times of crisis and uncertainty. Clients, however, will immediately notice the falling quality and value of their services. You should do the opposite, and fill the void left behind by your competitors with your enhanced efforts to make your value noticeable to the clients.

> _"Even in the worst times, someone is always doing well."_

### 12. Think long term and wise up if you want to hold on to your dream client. 

In order to be the most successful consultant possible, your ultimate goal must be to become irreplaceable for your clients and to form lasting relationships with them in order to maximize your fees and keep your pipeline filled with assignments.

To achieve this, you'll need to think strategically and in the long term, even discounting your fees for special opportunities that allow you to gain enough knowledge about your client's organization to make yourself an irreplaceable asset.

With every project you are involved in, your worth will increase as the added value of your projects accumulates and the organization imparts more insider information to you.

Having been educated about the ins and outs of the culture and business of your client's organization, you can then be more proactive about finding areas where you could add value, suggest projects, and thus raise your fees.

In order to gain these valuable insights, start your project with "roaming time" in order to get an overview of how the organization works and is perceived by the different stakeholders and functionaries within the organization.

Roaming time never really ends during your project, as there is always more to learn, but in the beginning, you should take special care to meet all key senior staff as well as a wide cross section of middle- and lower-level managers in order to gain insight into their perceptions, and how they use the resources and technology available to them.

Additionally, you can accompany salespeople on the road, and even try to talk to the major customers to get their perspective.

Once you've successfully completed a couple of assignments and have gained sufficient knowledge about your client's organization, you have reached the ultimate point in your client–consultant relationship. In the words of the author, "you have become a wise person whose contributions are a synthesis of personal talents, organizational knowledge, and personal relationships established with top management."

### 13. Balance work and life – your real wealth is your free time. 

Hell-bent on being as financially successful as possible, many consultants end up spending their lives in airplane seats and clients' offices, leaving only the weekend for their families, if that. Others, however, believe that their _real_ wealth is their discretionary time, and that money is merely the means to achieve that lifestyle.

If that's the case for you, then growing your business may actually mean a _decrease_ in your wealth.

In fact, it is a holistic outlook on life, rather than one focused on business, that will truly make you a better consultant. This is because you can draw more energy and creativity from a balanced life, which in turn improves your work and helps clients to see you as both a complete human being and a professional.

But how do you achieve the balance between work and play that allows for a more balanced and rich life?

First of all, you must realize that there is no strict division between your professional and private life. There is only one life: your life.

Consequently, you should eliminate arbitrary time boundaries, and instead do things when "the spirit moves you." If you want to complete some work on Sunday and go to the beach on Monday, that's fine! As an independent consultant, you are the master of your time, so use it as you wish.

Secondly, the secret to a fulfilling life lies in variety. As Nietzsche once wrote, "a day has a hundred pockets if you know what to put in them," so you should make an earnest effort to never stop learning and to push yourself to try new things. Avoid isolation, find inspiration in others and help them in order to maintain perspective about the priorities in your life.

And lastly, stay healthy and fit, and reward yourself regularly. A healthy body is home to a healthy soul, increases your confidence and curiosity, and makes you more resistant to stress.

### 14. Final summary 

The key message in this book:

**Consulting** **is** **a** **"relationship** **business,"** **where** **no** **one** **cares** **about** **your** **methodology** **or** **degree.** **The** **only** **thing** **that** **truly** **matters** **is** **the** **value** **you** **add** **to** **your** **client's** **balance** **sheet.** **As** **you** **continue** **to** **grow** **on** **your** **journey** **toward** **success,** **keep** **in** **mind** **that** **it** **is** **your** **free** **time** **–** **not** **mere** **money** **–** **that** **is** **your** **true** **wealth.**

Actionable advice:

**Always** **think** **in** **terms** **of** **value,** **not** **in** **tasks.**

Your clients are not interested in the number of tasks you must complete to finish a project. They are only interested in one thing: the results. Consequently, it is better for you — and for them — to approach your fee structure from the perspective of the value you add to their organization, rather than the number of hours it will take you to complete the project.

**Put** **potential** **relationships** **before** **money.**

While sealing a $50,000 deal is a great thing indeed, it's far better for you to develop the kinds of professional relationships that will lead to repeated $50,000 deals. Your success and your fortune are highly dependent upon others' perceptions of you.

**Reading** **suggestion:**

If you liked these blinks, try _What_ _Great_ _Brands_ _Do_ by Denise Lee Yohn, who further elaborates on how to create specially customized value for your customers.
---

### Alan Weiss

Alan Weiss is the founder of Summit Consulting Group and has penned more than 50 books, such as _Getting_ _Started_ _in_ _Consulting_ and _Million_ _Dollar_ _Referrals_. In addition to being named among the National Speaker Association's top one percent of speakers in the world, he has also consulted for a huge number of companies and has consistently made more than $1,000,000 per year.

