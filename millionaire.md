---
id: 58e20472fe685600046c935a
slug: millionaire-en
published_date: 2017-04-06T00:00:00.000+00:00
author: Janet Gleeson
title: Millionaire
subtitle: The Philanderer, Gambler, and Duelist Who Invented Modern Finance
main_color: 365C48
text_color: 365C48
---

# Millionaire

_The Philanderer, Gambler, and Duelist Who Invented Modern Finance_

**Janet Gleeson**

_Millionaire_ (1999) tells the story of the late seventeenth-century gambler and economist John Law. While he spent his early life as a dandy in England, he went on to become a fabled financial authority and economist in continental Europe. Although Law's ideas about economics and money briefly brought both him and the nation of France incredible wealth, it also showed the inherent danger of speculation and boom and bust economics. Despite failing in his day, several of his ideas gained currency over time and remain intrinsic parts of the modern economic system.

---
### 1. What’s in it for me? Learn the fascinating story of John Law. 

In the 1980s, yuppies began to flood the streets of New York and financial centers across the globe. They flashed their cash, wore designer clothes, showed off mobile phones and drove around in Italian sports cars.

That era remains the archetype of excess in modern eyes. Since then, economic bubbles, greedy bankers and a reckless banking industry have become a part of the public consciousness, with the most recent financial crisis standing as a fresh reminder.

As you'll learn from these blinks, such cycles are nothing new. In the early eighteenth century, France witnessed a similar economic bubble that introduced the world to several aspects of modern finance. This important development was all thanks to one man: the gambler-turned-money-spinner John Law.

Although Law's empire of wealth would eventually come tumbling down, several of his notions regarding economics and growth stimulation remain with us to this day. This is the story of Law's rise from young dueling dandy to economic oracle.

In these blinks, you'll learn

  * about the first sustained national experiment in paper money;

  * how the first modern economic bubble took shape; and

  * the role John Law played in introducing key concepts of modern finance.

### 2. The central principles that guide the world of banking have been around for a long time. 

Can you imagine a world without banking or credit? While it might seem impossible to conceive of, the fact is that the banking system as we know it, one with credit and borrowing at its heart, is a relatively new configuration.

Ancient Babylon had a form of banking, we know that paper money circulated in China in the seventh century BC and that there were stalls and counters in the Greco-Roman world where money could be lent and exchanged. But the foundations of modern banking really began to develop in the sixteenth and seventeenth centuries.

This shift is associated with Italian cities, and with the Republic of Genoa in particular. In fact, it's from the Italian language that we get the word "bank," since the "banco" was the table where transactions took place. But as trade expanded, as new lands were colonized and as monarchs' lifestyles grew ever more extravagant, the old banking system simply stopped working.

This new world of commerce, however, was not built with material money and precious metals, but was instead formed by a complex system of credit.

Credit can be a wonderful thing, but, as we know from the recent banking crisis, credit only works when there is trust. In the seventeenth century, this trust was created by banks having money in their vaults as collateral.

Once these physical reserves were established, the idea of paper money took off. Banknotes could circulate while a limited amount of gold coins with real value could be kept in reserve.

In theory, you could take your paper money to the bank and exchange it for real currency. But such a financial system inherently depends on trust and political stability; it simply would not sustain the rush on the banks that would occur if everyone demanded to exchange their bills.

It was in this changing world that a figure emerged who would bring fresh ideas to the field of economics: John Law.

### 3. The Law family were dealt a poor hand, but John eventually came up trumps. 

John Law came from a Scottish clerical family. They were a liberal family unsuited to rural living and eventually moved to Edinburgh, which at that time was a poor city.

Having struggled through a life of poverty, John Law's grandfather encouraged his sons to learn a trade and become goldsmiths. John's father, William, heeded his own father's advice and became a successful smith; his marriage to the daughter of a prosperous merchant only expanded his wealth and horizons. William would eventually come to purchase his own castle near the Firth of Forth.

At a time when everything looked rosy, tragedy struck. William was diagnosed with bladder stones and died in 1688 during a medical procedure. John inherited a considerable fortune, and already well-schooled, he chose to take part in big city life and moved to London rather than attend university.

London, the largest city in Europe, had plenty of attractions to offer him — women and gambling among them.

London played an important role in shaping Law's personality and ideas. He lived off the winnings from his gambling, and his mathematical talents became an especially valuable asset. In turn, gambling became his entry ticket into society.

But his was a not life without risks. Although he was generally successful, he did occasionally run into trouble.

For example, in 1692, before he had even turned 21, he was faced with the prospect of incarceration for outstanding debts. But he got lucky — he was able to recoup the necessary funds by selling the family estate, which he had inherited after his father's death, to his mother.

This incident was a watershed. While he didn't give up gambling, from that point on he only did so when applying his considerable mathematical talents. Guided by his understanding of probability and its role in card games, Law only played when he was sure he could win.

It was in febrile dandified dens that Law honed his skills, and the knock-on effects weren't only to his immediate financial advantage; these exploits soon piqued his interest in the new science of economics.

### 4. Young John Law lived fast, treading a fine line between success and failure. 

The hustle and bustle of seventeenth-century London was not without its dangers — it was a world of fops and dandies challenging each other to duels. In 1694, Law found himself in this very situation with Edward Wilson, an infamous dandy, for reasons we still don't know.

Wilson was killed and his influential family pressed for a murder charge, even though Law, who was attacked first, should technically only have been charged with manslaughter at most. He was nonetheless found guilty of murder, sentenced to death and imprisoned in Newgate prison to await execution. At a second attempt, Law managed to escape to Amsterdam.

Law's London life had brought him to within a hair's breadth of death. But now, on the continent, it became clear that this experience had prepared him well.

He was able to stay afloat by gambling as he made his way across Europe. While living in France, he also met his eventual partner, Katherine, who at the time was the wife of a French nobleman. Katherine stayed with Law for the rest of his life.

But Law's interest in economics did not subside. He immersed himself in its study and began to refine his ideas.

Law believed that the countries of Europe had become far too dependent on the exploitation of raw materials for generating wealth. He was convinced that the introduction of paper money would stabilize these economies.

Law laid out his ideas in a 120-page pamphlet in 1705, but it met with a lukewarm reception.

In 1704, after ten years on the continent, Law beseeched the new Queen of Great Britain, Queen Anne, for a pardon so that he could return to his homeland; he wanted more than anything to test out his theories on the Scottish economy. But, since his plea was unsuccessful, he turned elsewhere: France.

### 5. In France, John Law found an ideal context to put his economic theories into practice. 

The year was 1705 and Law had set his sights on France as a suitable country upon which to test his ideas. There was just one problem: Britain was at war with France, and Law, stuck in Holland, could not reach Paris.

Undeterred, Law sent his ideas to Nicolas Desmarets, the King's finance minister. Law had chosen France for good reason; the country was in an extraordinary amount of debt, about 2 billion livres — that's roughly $11.7 billion in today's money.

Law saw increasing the supply of money as a potential solution. By doing so, he thought, the economy could be stimulated. More taxes could be collected, the debt could be reduced and investment increased.

Law's confidence in paper money would come to play a key role; there simply wasn't enough gold and silver in France to manage the debt.

The plan was truly revolutionary and France would become the first economy to run on paper money.

In 1715, Law's plans fell into place. Philippe, Duke of Orleans and regent to the new king, Louis XV, was receptive to Law's ideas. Law acted quickly, but was met with some resistance, including from the Duke of Noailles, President of the Finance Council.

Law's response was seen as outrageous at the time: in 1716 he founded a private bank, the Banque Générale.

At the same time, the Duke of Noailles took his own radical measures. He reduced the amount of precious metal in the national coinage and cut interest rates. Panic spread quickly and the real value of salaries started to shrink.

Law's bank offered a solution, but he was treated with suspicion because of his gambling history. Even so, traders started to warm to paper money. It had a fixed value and could be exchanged for its face value in gold at any time.

In short, this new paper money came to be trusted, and the French economy began to grow on the back of this innovative concept.

### 6. John Law set about making France the economic powerhouse of Europe. 

The introduction of paper money to France was an unmitigated success, but Law wanted to push his economic ideas further. In particular, he looked to France's possession in North America, Louisiana, a district which stretched from the mouth of the Mississippi River deep into modern-day Canada.

All of France's costly overseas endeavors had met with little success. Law identified the root of this problem as persistent underinvestment, and began formulating a plan to generate funds. Law set about transforming his private bank into a state bank, the Banque Royale, and obtained funds by issuing billets, a type of state bond. The money rolled in and, soon enough, Law had enough funds to invest across the Atlantic.

Law's plan hinged on reinvesting this money. In 1717, he bought the Mississippi Company with the money he had acquired through the Banque Royale's bond-issuing scheme.

His aim was simple: he wanted to monopolize trade. Consequently, the Mississippi Company bought out the rival Company of the East Indies and Company of China, and also began to muscle in on the tobacco trade from Senegal.

To say Law's ideas were a success would be an understatement; the value of shares in the Mississippi Company rose sharply and, by July 1719, Law was able to buy the Royal Mint. By autumn of that year, his position was unassailable. He offered to take over the state debt of 1.2 billion livres at an interest rate of three percent, and paid a further 53 million livres for tax collection rights.

Law's expenditure financed the issuing of more shares in the Mississippi Company. Their value, in turn, skyrocketed. It was a phenomenal economic boom that seemed unstoppable. What's more, it had all been made possible by Law's insistence on the value and potential of paper money.

### 7. John Law's success enriched many and was seen as an economic miracle. 

By 1719, Law had created in the Mississippi Company what seemed like an unstoppable wealth-creating machine. He was a celebrated figure and stories of successful investors abounded, many of whom became celebrities in their own right. But paired with this success was a certain amount of speculation, and prices ballooned. For example, at the time in France, just one chick could cost as much as half the average monthly salary of a craftsman.

In the midst of this madness, the term "millionaire" was first coined to describe those who had enriched themselves through the Mississippi Company. Law then started to branch out and encourage foreign investment. This was particularly important, as the system of paper money needed to be supported by real coinage kept in banks. More foreign coinage would mean more French banknotes could be printed. However, this strategy was heavily criticized: it led to ever-increasing amounts of credit being issued.

By the second half of 1719, the results were plain to see: a bubble had formed and the share prices of the Mississippi Company had risen twentyfold.

Law's star was in the ascendancy. People came from all over to speak with him, and he became a true celebrity. He was also awarded honorary membership to the Academy of Sciences. Despite all this attention and newfound celebrity, Law kept his feet on the ground; he invested carefully, especially in property, and devoted himself to streamlining the tax system, an especially weak point in France's bureaucratic structure.

The year 1719 had been one of tremendous accomplishments for Law, and it was capped off by his appointment as Controller-General of Finances in 1720. But just as his star was shining brightest, the clouds began to gather.

### 8. The vulnerabilities in John Law's economic system became more and more obvious. 

At the start of 1720, Law was on top of the world and the prospect of further enrichment through the Mississippi Company's success seemed assured.

Investors had flocked to invest in the company's putative accruement of wealth in Louisiana — but the scheme turned out to be no more than snake oil. The expected rich seams of gold and silver ores were never found, the land was infertile and the indigenous peoples remained resistant.

Before long, the share prices of the Mississippi Company leveled off. Unfortunately, the fate of France's entire economic system, now based on trust in paper money, was by this point inextricably tied to the company's success.

Any dip in the fortunes of the Mississippi Company would potentially spread, and from the end of 1719 onwards, the company's major shareholders began to drop out. People also began to take coinage out of the country, fearing the increasing likelihood that others would start trying to exchange their worthless paper money for hard currency.

Law was in a bind and set about imposing desperate measures. He banned the export of coins, and when people tried to exchange them for other valuables, including diamonds, Law forbade the wearing of diamonds.

It was a race to the bottom that Law could never win.

By the time Law had banned every other form of currency except banknotes, he had suffered a breakdown. Yet more notes were printed until the amount in circulation between January and May 1720 doubled to 2.6 billion livres, or about $15.2 billion in modern-day funds.

By December, Law was denounced as a charlatan as he cast aside the fundamental tenet of his economic system: he unilaterally devalued paper money by half.

The angry mobs swelled and seethed, and Law's decisions were quickly revoked. Soon thereafter, he was ignominiously removed from his post as Controller-General of Finances and placed under house arrest.

### 9. John Law’s economic ideas were eventually condemned and he died in poverty. 

Within a matter of months, John Law's life had been turned upside down — but his house arrest did not last long. Upon his release, he formulated a fresh plan to stabilize the economy and was reinstated as Controller-General of Finances.

But it was too late. Confidence in the local economy had been lost and people were rushing to the banks to exchange paper money for coins. There was nothing Law could do to salvage the situation.

By October 1720, the writing was on the wall; the next month, the system of paper money that Law had established would cease to exist. It was also the end of the Mississippi Company. Already close to bankruptcy, when the sea trade stalled because of an outbreak of the plague, it was clear that the company's days were numbered.

Enough was enough, and Law wanted out. He made his way with his son to Holland and Italy, but his wife and daughter were not granted permission to leave France. He was penniless and there was nothing that could be done.

Although he was eventually allowed to return to Great Britain, this was of little recompense. Katherine was still unable to leave France and the powers there had no interest in helping them or letting Law return.

After all, the scale of the consequences of Law's economic planning had by this point become quite clear, with half a million people claiming losses from shares and banknotes. It was an undignified end for Law himself, who fell ill and died, never to be reunited with his wife.

Even so, Law's ideas did not die with him. France may not have circulated paper money for the next hundred years, but it is now a core aspect of modern economies. Likewise, his approach to raising investment through the issuing of shares remains a cornerstone of modern finance.

Law's ambitious plans may have failed in his lifetime, but the dreams of this crafty gambler and creative economic strategist live on in every corner of the world.

### 10. Final summary 

The key message in this book:

**The use of paper money as a cornerstone of modern economic systems has its roots in ideas first put forth by John Law. Law's life story shows not only the downsides of modern economic systems based on paper money, but also its inherent value. While paper money can trigger economic booms, rapid downturns and economic chaos, modern economies would not run nearly as smoothly without it.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Wealth of Nations_** **by Adam Smith**

_The Wealth of Nations_ is a profoundly influential work in the study of economics and examines exactly how nations become wealthy. Adam Smith advocates that by allowing individuals to freely pursue their own self-interest in a free market, without government regulation, nations will prosper.
---

### Janet Gleeson

Janet Gleeson is the author of _The Arcanum_, _The Grenadillo Box_, and _The Serpent in the Garden_. She has worked for the Impressionist Paintings Department at Sotheby's and has written for many magazines, especially on antiques and art.

