---
id: 56f3ae092bf5b90007000001
slug: mind-gym-en
published_date: 2016-03-28T00:00:00.000+00:00
author: Gary Mack & David Casstevens
title: Mind Gym
subtitle: An Athlete's Guide to Inner Excellence
main_color: E1AB6F
text_color: 7A5D3C
---

# Mind Gym

_An Athlete's Guide to Inner Excellence_

**Gary Mack & David Casstevens**

_Mind Gym_ (2001) sheds light on the important role our minds play in physical performance and athletic excellence. These blinks draw on the Mack's work with professional athletes to provide you with the tools you need to acquire a top-performing state of mind.

---
### 1. What’s in it for me? Give your mind a boost! 

As anyone who's ever done long-distance running will tell you, it's not just about being fit. Sure, having a well-trained body is essential, but your mind, your attitude and your mental grit are equally important. Because, when you're nearing the end of the race and your body is on the verge of giving up, it's your mind that will push you over the finish line.

But a well-trained mind doesn't just come in handy in long-distance running. If you want to succeed in life, you should make sure to give your mind — the most important muscle of all — regular exercise. It's time to step into the Mind Gym. These blinks will show you the ropes.

You'll also learn

  * what your most important muscle is, no matter what the sport;

  * why your goals should be SMART; and

  * why 90 percent is better than 100.

### 2. Top physical performance is impossible without a well-trained mind. 

We all know that good old Nike slogan: _Just do it_. You might also know that, for all it's simplicity, this piece of advice is often hard to follow. Why? Negative thinking stops us from taking action.

It's funny how our thoughts seem to impact us precisely the way we don't want them to. If, while playing golf, you tell yourself not to hit the water, you're likely to hit it anyway. Why? Your thoughts were so focused on the water that your body began aiming for it, too. Avoid outcomes like this by keeping it simple and focusing on what you want, not all the things you're afraid might happen.

Of course, the power of thought alone can't carry us toward success. It's the combination of physical and mental training that allows us to perform to our full potential. One study of three groups of basketball players demonstrated just this.

The first group took free throws for one hour, the second sat and visualized their free throws for an hour, and the third did both for 30 minutes each. When it came to the free throws afterwards, the third group outperformed the others by far.

While most of us know more or less how to train our bodies, few really know what to do when it comes to training our brains. Like our bodies, our minds have their own strengths and weaknesses. Pinpointing these will give us both confidence in our abilities and a clear idea of where we need improvement.

Get started on your brain training journey with some inspiration from the Japanese concept of _kaizen._ This refers to constant daily learning simply for the sake of improving yourself one step at a time without stressing too much about the final outcome. If you think like this, you'll have an open mind ready to take in anything that can make you a stronger, better person.

> "_The mind is like a parachute — it only works when it's open_."

### 3. See yourself as a winner to become one. 

Do you consider yourself a winner or a loser? Sure, you might not think yourself a winner right now, perhaps because you haven't performed so well competitively, or because you feel your skills aren't quite there yet. But if you really want to increase your chances of success, it's time to put that all aside. If you think of yourself as a winner, it'll make you more likely to become one. 

_Self-consistency theory_ explains that the way we think about ourselves is powerful enough to impact our actions. This is why those with a negative self-image struggle to achieve top performances. Put simply, you can't win if you see yourself as a loser. 

But what if you don't think you're a loser, but know you aren't the best yet? Start taking steps toward where you want to be! Use _SMART_ goals to keep yourself focused on progress. These are _specific, measurable, achievable, realistic_ and _time-bound_ goals. So, if you're aiming to run a marathon at the year's end, your SMART goal could be running two miles for two days in the first week, three miles three days the week after, and building up in that rhythm until you can run 13 miles each week, which is half the length of the actual marathon. 

By contrast, if you decided your goal was completing a half marathon as quickly as possible, your training would collapse. Why? Your goal was unrealistic and, as a result, demotivating. On the other hand, if your goal was to just try and run a little each day, you'd be lucky to make it to the marathon at the end of the year. Why? Your goal just wasn't specific enough to ensure progress. 

While outlining and working toward your SMART goals, do your best to maintain determination, dedication and discipline. It's too easy to retreat to a negative self-image and stop working toward your goals. 

Of course, nobody becomes a winner without a few failures along the way. It's perfectly natural to fail, yet many athletes are incredibly afraid of it. So rather than learning from their mistakes, they only play it safe.

This is due to our false belief that we have to be perfect. But although perfectionism seems like a positive thing, it's ultimately a hindrance. An aspiring basketball player shouldn't aim for a 100 percent success rate at free throws. When you make the slightest mistake, you'll feel like you've failed yourself. 

There's no need to put yourself in that position! Your goal should be centered on steady improvement, which will help you to see failure for what it really is: feedback, that is, the information that will help you continue to grow and learn.

> "_To be a great champion you must believe you are the best. If you're not, pretend you are_." — Muhammad Ali

### 4. Achieving mental toughness means excelling in seven critical areas. 

We all know that in many sports, the physically strongest and toughest have an advantage. The strong, tough wrestler has a considerable advantage over an opponent weaker in body. And yet, toughness isn't all about bulging biceps: _mental toughness_ is just as important as physical toughness.

In order to develop mental toughness, you need to train yourself in seven areas — the _seven C's of mental toughness._

The first of these is _competitiveness._ The mentally tough are driven to battle against adversaries and come out on top. For example, it was competitive spirit that pushed Michael Jordan to leave basketball — a game he had mastered — and take up major league baseball. He couldn't let himself grow old without trying to master another sport he loved.

Having mental fortitude also relies on not being afraid of a difficult opponent. Therefore, it should come as no surprise that successful people have an abundance of _courage._ They aren't scared, even by the most daunting challenges.

Then there is _confidence._ Mentally robust people simply believe they are always going to win. To return to Michael Jordan, every time he stepped onto the court he was convinced he was the best player out there. This confidence helped ensure he almost always was.

Perhaps the most obvious signs of mental toughness are _control_ and _composure_. Those who are able to focus and keep their emotions in check even in the face of enormous pressure are the ones who will think and act in the clearest, most appropriate manner.

_Consistency_ is another vital component of mental strength. Winners are always at the top of their game, regardless of how they're feeling. For example, when a mentally tough person is under the weather, she doesn't use this as an excuse to perform poorly — she resolves to try as hard as ever.

Finally, mental toughness requires _commitment._ This isn't simply about having the skills to be a stellar athlete or businessperson — you have to want to be successful at it and commit to working hard until you achieve your ultimate goal.

### 5. Slow down and let your excellence shine from within. 

Now that we've spent some time investigating how you can work your way to success, it's time to take a closer look at success itself. What is it? We often consider success, excellence and top performance as quantifiable, in the form of records, medals and prizes. And yet, there's a form of excellence that can't be measured — and it also happens to be the most worthwhile.

Great athletes will benefit from aspiring to _inner excellence_. This is a reflective state of mind in which you are accountable for your thoughts, feelings and actions, no matter how much you want to give up. Here, competitions become challenges that kick your drive to succeed into gear, with no fear of failure to hold you back. It's in this state that you'll allow yourself to shine.

Channel your inner excellence next time the going gets tough. The setbacks you're facing now might end up being totally trivial in the scheme of things. Take basketball superstar Michael Jordan, who didn't make his high school basketball team! But by staying optimistic, he gave himself the chance to witness what he could do, not what he couldn't.

Another misconception we hold about success is that it's defined by pushing ourselves to the absolute limit. This simply isn't true. Though counterintuitive, a great way to get better faster is by _slowing down_.

Human muscle tissue makes a great analogy for this. Muscles can be considered in terms of two categories: _agonists_ and _antagonists_. While they both work hard when we run at top speed, _agonists_ are working to accelerate us while _antagonists_ will work to slow us down. But, if we only give 90 percent of our effort, we'll be faster. How? Because the antagonist muscle won't be working so hard to set you back.

> "_Relaxation happens when you stop creating tension. Over-trying leads to under-performing_."

### 6. Love, learning and labor are all vital to a successful and fulfilled life. 

When it comes to judging your success, whose opinion should you trust? Should you ask your colleagues? Friends? Maybe even your rivals? Well, when it comes to deciding how worthy your achievements have been, the only person you should ask is yourself. Only _you_ can tell whether you've given your all.

When judging yourself, take a look at a few of the areas considered by Harvard researchers to make up the metrics of a successful life.

The first area is _love._ Whatever you do, be it a hobby or a job, you have to love doing it, otherwise you won't succeed at it. Think of it this way: say you want to become a top athlete. In order to become outstanding, you'll have to push through a great deal of adversity. Without the love of doing it, you'll never overcome all the injuries, late nights, and aching muscles.

Love goes hand-in-hand with the next factor — _labor._ A strong work ethic is essential for success. In fact, it can be more important than natural talent. Consider basketball star, Larry Bird. Despite not being blessed with the perfect physique for the game, he worked tirelessly to become a basketball great. __

The next key area of a successful life, especially as you grow older, is being able to _learn._ Young people are often brimming with natural talent, such as a hardy constitution or an athletic physique. Yet, as we age, these natural powers begin to fade. With the most successful people, this faltering body is replaced by a stronger mind. Drawing from all of their experiences, their mistakes and successes, older people are perfectly positioned to learn the tips to staying successful.

If you bear all of these factors in mind, whenever you look in the mirror, you'll be able to judge yourself in a favorable light.

### 7. Final summary 

The key message in this book:

**Training your body and your mind go hand in hand when it comes to achieving your goals as an athlete. Positive attitudes you have toward yourself and your progress have a powerful influence on your performance. Learning to accept failure, set goals and slow down will help you to center yourself and unlock your full potential.**

Actionable advice:

**To be successful keep your mind in the present.**

It's when your mind and body are working together seamlessly that you're really in the moment. Focusing on the present will alleviate any pressure you experience. With no anxieties about the future or ruminations on the past, you can become totally absorbed in the task at hand. Pay attention to the skill you're working on, and save the judgement and analysis for later. Enjoy the present moment, don't rush through it, and you'll give your focus and fulfillment a major boost.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Bounce_** **by Matthew Syed**

In _Bounce_, Matthew Syed explores the origins of outstanding achievements in fields like sports, mathematics and music. He argues that it is intensive training, not natural ability that determines our success, and people who attribute great performances to natural gifts will probably miss their own chance to succeed due to lack of practice. To find these blinks, press "I'm done" at the bottom of the screen.
---

### Gary Mack & David Casstevens

Gary Mack is a sports psychology consultant who has worked with many professional athletes. David Casstevens is a writer for the _Fort Worth Star Telegram_ and a bestselling author. _Mind Gym_ is their first bestselling book together.

