---
id: 5e74be626cee0700068c8b9c
slug: mindf-star-ck-en
published_date: 2020-03-23T00:00:00.000+00:00
author: Christopher Wylie
title: Mindf*ck
subtitle: Cambridge Analytica and the Plot to Break America
main_color: 46E45F
text_color: 267D34
---

# Mindf*ck

_Cambridge Analytica and the Plot to Break America_

**Christopher Wylie**

_Mindf_ _*_ _ck_ (2019), written by a whistleblower, tells the story of the largest data crime in history to date. On the eve of the 2016 United States presidential election, consulting firm Cambridge Analytica harvested the Facebook data from 87 million people and used it to conduct a mass disinformation campaign. Now, the full story has finally come to light.

---
### 1. What’s in it for me? Learn the story of Cambridge Analytica from one of its founding members. 

In the months leading up to the United States presidential election of 2016, a Trump presidency still seemed pretty far-fetched. But as the numbers started rolling in on November 8, the nightmare — or pipe dream, depending on your political identification — became a reality.

Afterward, many were left wondering: _How did this happen_?

_Mind control_ might seem like a crazy answer. But, in fact, that was exactly the goal of Cambridge Analytica, the data company that harvested up to 87 million Facebook users' account data to target and rile up American voters. 

And they didn't stop with the United States. The firm conducted disinformation campaigns in Africa, the Middle East, the Caribbean, and the United Kingdom. 

In the following blinks, you'll discover whistleblower Chris Wylie's account of what really happened inside the walls of Cambridge Analytica, and learn how a group of data consultants used their skills to re-engineer the minds of citizens around the world.

Along the way, you'll learn

  * why you should stop taking those Facebook personality quizzes;

  * how two interns were tricked into illegally funding a Brexit campaign; and

  * where the "crooked Hillary" narrative came from.

### 2. New data technology and the use of social media are changing the way political campaigns spread their messages. 

In the past, volunteering in a political campaign meant spending long hours designing and distributing leaflets or phone banking just to garner a few votes. This system was impersonal, impractical, and worst of all, ineffective.

So when the author Chris Wylie, a young Canadian data consultant, was approached by the Liberal Democrats — a British political party — to help out with their 2010 campaign, he was horrified by what he saw in their office. There were electrical cables taped to the walls, a database designed during the Vietnam War, and obsessive discussion over how many leaflets they should print.

**The key message here is: New data technology and the use of social media are changing the way political campaigns spread their messages.**

Compared to the Obama campaign Wylie had observed just a few years prior, the Liberal Democrats, or Lib Dems, **** were still working with caveman technology. Then again, Obama's data team was light years ahead of everyone else. They understood how to leverage new technology to ensure that their messaging was as airtight and effective as possible.

How did they do that? Simple — by understanding the kinds of people they needed to talk to, and exactly what messages would resonate with them.

Obama's team used the Voter Activation Network (VAN), a data tool containing reams of information on voters, **** from their age and race to their magazine subscription and airline miles. Using this data, the team could predict whether someone was likely to be a Democrat or a Republican and which issues might be important to them. Then, it was just a matter of the team _microtargeting_ their advertising. This meant crafting messages geared toward specific types of voters to sway their opinions and get them to the polls.

As effective a strategy as this was for the Obama campaign, it was a dark moment for democracy. Political discourse had begun to resemble less of a town square and more of an online ad network. Different voters — even those on the same side of the aisle — were receiving different messages than their friends and families. Worse, those messages could be tailor-made to look like they came from a trusted friend, despite their calculated political intent.

Republicans were unable to cope with the Democrats' sophisticated and targeted messaging on social media. But all of that was soon to change.

> _"[W]e have been forced to place our trust in political campaigns to be honest, because if lies are told, we may never know."_

### 3. Political campaigns can predict voting behavior by collecting people’s psychometric data. 

When you hear the term _Tory_, what kind of person do you picture? If you're at all familiar with British politics, you probably imagine a posh landowner or rural working-class type. And what about a typical _Labour_ voter? Perhaps you imagine a union member or a council estate dweller.

Tory and Labour voters were easy for author Chris Wylie to pin down, but Lib Dems? They were much harder to define. However, as one of the party's data consultants, defining the Lib Dems was Wylie's first order of business. Then, he could get to work developing targeted messages and bringing in new voters.

**The key message here is: Political campaigns can predict voting behavior by collecting people's psychometric data.**

Traditionally, voter data homed in on socioeconomic and biological indicators like race, age, and income. Voters are typically categorized into monolithic blocs, like LGBT, black, or women voters. 

But how strong of a predictor is this sort of grouping, really? If you randomly grabbed a hundred women off the street, would they all have the same opinions? Of course not.

Tapping into this fact, Brent Clickard, an experimental psychology Ph.D. candidate at Cambridge, suggested looking into personality as a way of predicting voting behavior. Specifically, Clickard recommended the five-factor model of personality, or the Big Five, which measures behavior based on five traits: openness, **** conscientiousness, extraversion, agreeableness, and neuroticism.

Why the Big Five? Well, this model has been shown to be a strong predictor of many factors in people's lives. Those who score high in openness, for example, tend to be artists or creatives, while a high scorer in conscientiousness is more likely to do better in school.

Interestingly, the language of the Big Five can also be mapped onto political discourse. **** Consider the Obama campaign's focus on _change_ and _progress_, which signaled openness to new ideas — a trait Democrats are more likely to exhibit. Then, consider typical right-wing messaging, which highlights _stability_ and _tradition_ — a platform tied to the Republican-leaning conscientiousness trait.

Through the Big Five, Wylie had cracked the code of the Lib Dems: voters higher in openness and lower in agreeableness than either Tory or Labour voters. If you could build a database of people displaying those traits, you could target them with your political messages. This was a revolutionary finding.

But when it came time for Wylie to present his findings to the Lib Dem campaign, they were distinctly unresponsive. The Lib Dems might not have listened, but Wylie's next client would.

### 4. The SCL Group was a psychological warfare company that specialized in collecting and weaponizing data. 

In spring 2013, a few months after Wylie had left his consulting position with the Lib Dems, a party adviser told him about an opening at a company called the SCL Group, originally known as Strategic Communications Laboratories. Its board was a patchwork group of foreign politicians, former cabinet ministers, and retired military commanders — and it had access to secret British governmental information.

But what _exactly_ did SCL do? Over the next few years, Wylie would find out firsthand.

**The key message here is: The SCL Group was a psychological warfare company that specialized in collecting and weaponizing data.**

Wylie's first impression of SCL was its office, decked out with ornate chandeliers and tightly-woven carpeting. It was also stiflingly hot — which he later found out was done on purpose to make clients uncomfortable and sweaty during meetings.

Then came his meeting with SCL's CEO and ringleader, Alexander Nix. Nix cut a striking figure in a finely tailored suit, with a posh, Eton-educated accent to match. When explaining what SCL's work entailed, he pointed at a photo of Africa and said, "We win hearts and minds over there… _you know_, however that needs to happen."

When you hear the term _psychological warfare,_ it may __ sound like the stuff of science fiction, but, in fact, it's nothing new in human history. Just look at sixteenth-century Russia, where Ivan the Terrible set up huge frying pans in the middle of Moscow's Red Square. These were designed to roast his enemies alive **** — and terrify his own citizens. Or take Alexander the Great, who left factions of soldiers behind in cities he'd sacked so as to better disperse Greek culture and assimilate the new members of his empire. 

Today, with the proliferation of social media, war can be waged online. Terror groups can easily leverage social channels to gain new recruits. But how can a government retaliate against these threats? After all, you can't shoot a missile at the internet.

This is exactly where the SCL Group found its niche: gaining _total information asymmetry_ against threats. Essentially, their goal was to have so much data on their targets that they could use it to overwhelm, disrupt, and shut them down.

Over the years, SCL psychologically manipulated narcotics groups in South America, voters in Africa, and ultimately, British and American citizens. But how exactly did they do it? We'll find out in the next blink.

### 5. The SCL Group used its data-gathering and psychological warfare tactics to manipulate targets across the globe. 

When Wylie started working at SCL, its project was to disrupt a narcotics operation in a South American country. To do this, a team of psychologists first identified its targets — people who exhibited neurotic, narcissistic, or paranoid traits. SCL then fed them specific messages, like "The bosses are stealing from you," or "They're going to let you take the fall," for instance.

Once the targets had been fed these manufactured messages, they needed to meet each other. The original targets formed groups, sharing rumors and exacerbating each other's paranoia. Soon, the rumors began to balloon, infecting the minds of people outside of the group and destabilizing the entire narcotics operation from the inside.

**The key message here is: The SCL Group used its data-gathering and psychological warfare tactics to manipulate targets across the globe.**

While manipulating targets in South America, SCL was often a force for good in the Middle East, where there is a large population of young males at risk of being recruited into terror groups. SCL used their tried and tested psychological manipulation tactics to de-radicalize and draw these men away from the temptation of violence.

But SCL was far from being a moral agent. It interfered in elections in several developing countries — which, according to Nix, was perfectly acceptable.

Consider the example of Trinidad and Tobago. SCL agreed to help Trinidad's Ministry of National Security use data to predict which of its citizens were most likely to commit crimes in the future.

At least, that was the Ministry's stated goal — but SCL knew that if the Trinidadian government possessed a tool to forecast criminal behavior, they could also identify potential political supporters. 

Without extensive census data, SCL needed a different way of cataloging Trinidad's citizens. So they sent a team to several Caribbean telecom companies and asked if SCL could tap into their data. Their answer? Surprisingly, _yes_. 

This partnership with the telecom companies allowed SCL to choose any IP address and watch what its owner was browsing on the internet in real time. Once, the author watched as a Trinidadian switched between watching porn and looking up plantain recipes. Nix looked on, laughing uncontrollably.

Trinidad, South America, and the Middle East were one thing, but soon, SCL had the opportunity to work with a much bigger client. In October 2013, Nix asked the author to travel to Cambridge and meet someone he called "Steve from America."

> _"Nix preyed upon the colonial fetishes and insecurities of the men who ran the nations of the empire."_

### 6. Steve Bannon wanted to work with SCL to win the culture war in America. 

When the author arrived in Cambridge, he was ushered into a hotel, where he came face to face with a roughshod character with unkempt hair and a ruddy face. It turned out that "Steve from America" was none other than Steve Bannon — a nobody at the time, but soon to be an infamous key player in the Trump campaign.

At first, Wylie saw a typical old, straight white male in Bannon. But after a four-hour conversation spanning topics from Foucault to third-wave feminist Judith Butler, Wylie realized there was more to Bannon than met the eye. 

Eventually, Bannon confided to the author that he was interested in changing American culture.

**The key message here is: Steve Bannon wanted to work with SCL to win the culture war in America.**

SCL believed it could help Bannon by weaponizing data, just as it had done in South America and other places around the world. However, it had never conducted an operation at a scale the size of America.

So, to test its capabilities, SCL decided to run a proof of concept experiment in Virginia. They would start by sending a team to conduct focus groups and open-ended conversations with locals, just to get to know them.

SCL found that Virginians were particularly concerned about the Republican candidate for governor, Ken Cuccinelli. One of Cuccinelli's proposals was to reinstitute Virginia's "Crimes Against Nature" law, which would technically outlaw oral and anal sex. People in SCL's focus groups were generally against intercourse between men, so might have supported this law. But they weren't so enthusiastic about Cuccinelli's notion to ban oral sex, which targeted everyone, regardless of their sexual orientation or marital status. 

SCL was curious if they could sway Virginia Republicans into supporting Cuccinelli by using their knowledge of a typical Republican's psychometric profile combined with the data from their field research. They knew that, according to statistics resulting from Big Five personality testing, conservatives tend to score higher in conscientiousness, meaning they enjoy stability and dislike surprises. And SCL also knew that Virginians were put off by Cuccinelli's obsession with banning oral sex. 

Combining these two bits of knowledge, Mark Gettleson, a teammate of Wylie's, devised a campaign message that acknowledged conservatives' distaste for Cuccinelli's stance on the "Crimes Against Nature" law while arguing that his opinions were at least consistent and predictable. This played on their Big Five _conscientiousness_ trait. The message was: "You might not agree, but at least you know where I stand." 

That message outperformed all the other ones SCL's team tried. They had swayed a group of voters by utilizing their psychometric data. And in the process, they had proven that Republicans would be willing to accept and support a candidate that seemed crazy — as long as that craziness was consistent.

### 7. The newly-formed Cambridge Analytica used census information, data brokers, and Facebook to gather all the data it needed. 

After the rousing success of the Virginia experiment, it was time for SCL to secure the funding for its larger-scale operation in America. 

Ultimately, Nix and his team were able to convince a wealthy Republican donor named Robert Mercer to donate $20 million in funding to the project. Due to the legal complexities of political funding, SCL needed to form an offshoot company that would be based in America but still had access to SCL's data. To play on Bannon's self-identification as an intellectual and a philosopher, and despite SCL having no actual affiliation with Cambridge University, they decided on the name _Cambridge Analytica._

**The key message here is: The newly-formed Cambridge Analytica used census information, data brokers, and Facebook to gather all the data it needed.**

After securing the funding, it was time for Cambridge Analytica to start gathering the data it needed to begin its psychological warfare operation. Luckily for them, during the Virginia experiment, the team had already bought access to reams of citizens' personal information, much of it through data brokers like Experian and from the United States Census.

The final piece was gathering the psychometric data necessary to start targeting specific voters. To do that, Cambridge Analytica partnered with Aleksandr Kogan — a Cambridge professor who specialized in computational models of personality. 

Kogan designed an app through which users took a personality test in exchange for a small amount of money. The catch? Users signed in to the app using their Facebook logins. And that gave Cambridge Analytica an easy way to access data well beyond the scope of the original personality test.

Tens of thousands of users took the test, giving Cambridge Analytica a huge amount of psychometric data on a silver platter. And due to Facebook's lax permissioning rules, they were able to gather data on all of the test-takers' friends, too — including their likes, groups, follows, clicks, and even private messages. This information allowed the team's computational model to guess how a user _would have_ responded to the survey, despite them never actually taking it.

This was a watershed moment for both user privacy and Cambridge Analytica. From the 87 million profiles Cambridge Analytica eventually compiled, the team started identifying users who displayed "sensational and extreme interests" — like guns and the occult. In the next blink, you'll find out what they did with all that information.

### 8. Cambridge Analytica and its close partners, the Russians, used Facebook to prey on vulnerable groups. 

You may have heard of Gamergate, a well-publicized scandal that took place in 2014 wherein a community of misogynistic gamers doxxed and harassed women. Many of these were members of the online community of _incels_, or _involuntary celibates_. These were primarily young, straight white males who had been shunned by women and felt economically disadvantaged **** — and they were angry about it.

**The key message here is: Cambridge Analytica and its close partners, the Russians, used Facebook to prey on vulnerable groups.**

Incels and others like them became the unwitting primary targets for Cambridge Analytica. The company identified Facebook users who displayed personality traits belonging to the "dark triad" — narcissism, Machiavellianism, and psychopathy. These people were targeted not just through Facebook personality apps, but also through calculators and calendar apps designed by Cambridge Analytica. When downloaded, the app would pull all of a user's archived Facebook data.

After identifying a group of users, Cambridge Analytica then leveraged Facebook's recommendation algorithms to get them to join fabricated right-wing and extremist Facebook groups with names like _Incel Liberation Army_ and _I Love My Country_.

Since Cambridge Analytica controlled these Facebook groups, they could post material designed to rile people up. After the users were in a state of rage, Cambridge Analytica arranged real-life meetups where group members shared conspiracy theories and hate-filled opinions, further radicalizing them. From there, group members started inviting friends to future meetups and spreading the messages further.

This, Cambridge Analytica found, was how you create a movement of resentful and conspiratorial citizens, the _alt-right_.

And it wasn't just Cambridge Analytica who was using this Facebook data against American citizens. Toward the end of Wylie's time with the firm, Russians started randomly appearing in Cambridge Analytica's office. These were supposedly employees of the oil company Lukoil — which turned out to be a front for Russia's intelligence branch, the Federal Security Service (FSB).

In conjunction with the increased Russian presence around the office, odd questions started popping up in Cambridge Analytica's research — like _Is Russia entitled to Crimea?_ and _What do you think of Vladimir Putin as a leader_? Yet, no one the author spoke to could figure out who in Cambridge Analytica had authorized these questions.

In the end, Cambridge Analytica's involvement in Russian disinformation efforts was never definitively proven. But what did Russia's goal seem to be? Primarily, to cultivate pro-Russia sentiment in America. Then, to use fake profiles and Facebook groups — just like Cambridge Analytica did — to further polarize the nation and foster social chaos.

### 9. Cambridge Analytica funded two branches of the Leave campaign – one legally, and one illegally. 

In 2016, British Prime Minister David Cameron announced a referendum that would mark a major turning point in the country's future: whether or not it would remain in the European Union. The United Kingdom was divided into two factions — Leave or Remain.

**The key message here is: Cambridge Analytica funded two branches of the Leave campaign — one legally, and one illegally.**

Two of the main branches of the Leave side were Vote Leave, fronted by former London mayor Boris Johnson, and Leave.EU, which focused primarily on stoking anti-immigrant sentiment. With the help of Steve Bannon, Cambridge Analytica signed on to assist Leave.EU in October 2015.

Due to laws restricting collaboration between campaigns, Vote Leave couldn't legally use Cambridge Analytica for data-gathering purposes. So, instead, they chose a company called AggregateIQ (AIQ) to assist them. AIQ was based in Canada, but it had built infrastructure for SCL, including systems for Facebook data harvesting and ISP logs. It also possessed all of Cambridge Analytica's Facebook data. Basically, all it was missing was the Cambridge Analytica name.

The British government imposes strict spending limits during referendums so that one party isn't unfairly advantaged over another. So, while Vote Leave was able to use AIQ's data to target voters, it eventually needed more money to keep churning out ads.

Vote Leave landed on a project manned by two of its interns, Shahmir Sanni and Darren Grimes. Sanni and Grimes had previously worked together to form a progressive branch of Vote Leave — BeLeave. It used none of Vote Leave's funding, yet some of its content had gained even more traction than that of its parent company.

One day, Vote Leave's senior directors approached Sanni and Grimes. They instructed the two interns to set up a bank account as a separate campaign and write a formal constitution. It was unlawful for BeLeave to spend any money, since it was so closely tied to Vote Leave — but Sanni and Grimes weren't told this.

Soon, Vote Leave organized the transfer of £700,000 to BeLeave. However, none of it was to go into BeLeave's new bank account. Instead, Vote Leave would transfer it automatically to AIQ — illegally. And since Sanni and Grimes' names were on the legal papers, they would take the fall if any of this came to light. So, in the end, BeLeave succeeded in obtaining illegal extra funds and access to all of Cambridge Analytica's data. 

In the next blink, we'll look at why Cambridge Analytica's messaging was so effective during the Brexit referendum.

### 10. AIQ, SCL, and Cambridge Analytica took advantage of cognitive biases to win elections. 

Ever wondered why some people follow Fox News so religiously? Sure, its messaging plays into its viewers' existing political sentiments. But there are also some calculated psychological tactics at play.

**The key message here is: AIQ, SCL, and Cambridge Analytica took advantage of cognitive biases to win elections.**

Fox often broadcasts hyperbolic narratives specifically designed to stoke people's anger. Why? Because anger sparks the _affect heuristic,_ which leads people to take mental shortcuts heavily influenced by their emotions. You can probably recall a time when you said something you later regretted during a heated argument. That was the affect heuristic at play, causing you to think and act irrationally.

After Fox activates people's rage, it then ushers viewers into its in-group of "ordinary Americans." The hosts address the audience directly, making constant references to "us." This triggers yet another bias, _identity motivated reasoning_, which causes people to see factual statements as attacks on their identity. As a result, people reject any information — factual or not — that threatens their worldview.

In the United Kingdom, Leave was using similar tactics in its messaging. Meanwhile, Remain stuck mostly to fear-mongering, trying to draw voters' attention to the potential economic consequences of leaving the European Union. But those arguments were totally ineffective against the angry pro-Leave citizens.

Unlike Remain, Leave focused on making their voters angry and indignant, which reduced the need for fact-based reasoning. Worse, it actually made some target voters _support_ a potential threat to the economy. Why? Because an economic downturn meant that their primary opponents — metropolitan liberals and immigrants — would suffer, too. In a way, their votes would become a punishment for people on the other side.

Using AIQ's data, Vote Leave had released more than 100 different ads with over 1,433 different messages to their target voters in the week leading up to the referendum. These ads were targeted at just a few million voters, but they were viewed more than 169 million times. This meant that the targeting had worked, and the feeds of target voters were completely dominated by Vote Leave messaging.

The rest is history. On June 23, 2016, the day of the referendum, Leave won with 51.89 percent of the vote. British voters had been the victims of a scaled, targeted information campaign.

### 11. The author left Cambridge Analytica and then blew the whistle on the company, bringing to light the largest data crime in history. 

In his early days at SCL, the author was able to work on projects that aligned with his morals and with his areas of interest — data and culture. But around August 2014, after he came to terms with the impact Cambridge Analytica's immoral dealings were having, he left the firm. Unfortunately, though, his involvement wasn't over yet.

**The key message here is: The author left Cambridge Analytica and then blew the whistle on the company, bringing to light the largest data crime in history.**

In early summer 2015, Corey Lewandowski, Trump's campaign manager at the time, invited Wylie and a few of his former colleagues to help boost the Trump brand. _The Apprentice_ 's ratings were declining, and fewer people were staying in Trump hotels. If Trump was to be a successful candidate, his brand needed to improve its public image.

Wylie declined the offer. But a few weeks later, he received a letter from Cambridge Analytica claiming that he had solicited one of Cambridge Analytica's clients: Donald Trump. They intended to sue him. To smooth over the legal issue, Wylie signed a non-disclosure agreement with Cambridge Analytica. 

Then, in 2016, Wylie received a letter from Facebook asking him to confirm that Cambridge Analytica's data had only been used for academic purposes and then deleted. This was laughable, because Facebook had given Cambridge Analytica's data harvesting application _express permission_ to use the data for non-academic purposes. 

Ultimately, it was a journalist from the _Guardian_ newspaper which helped Wylie come forward. Navigating the legal battleground, Carole Cadwalladr worked with Wylie to publish the story about everything that had happened at Cambridge Analytica.

Over the next few months, Wylie found himself in meetings with _New York Times_ journalists, United States congressmen, and the FBI. With the help of a sting operation manned by Britain's _Channel 4 News_, Alexander Nix was caught in the act of offering some of Cambridge Analytica's questionable services to a fake Sri Lankan agent. Even more damning, another Cambridge Analytica executive admitted to completely fabricating the "crooked Hillary" narrative and disseminating it on the internet to assist Trump's election efforts. 

In the end, Wylie turned over hundreds of copies of documents, including project files and emails, which were passed on to both the FBI and its British counterpart, the National Crime Agency. Mark Zuckerberg, Facebook's CEO, was called to testify under oath as his company's stock plunged.

Cambridge Analytica had finally received its coup de grâce. Though some justice has been served, nearly all of the key players in the scandal — including Alexander Nix, Steve Bannon, and Facebook — have all walked free.

### 12. Final summary 

The key message in these blinks:

**Over three years, Cambridge Analytica exploited data it gathered from Facebook to spread false narratives, exploit people's cognitive biases, and ultimately, get Donald Trump elected. Cambridge Analytica also left its mark on Brexit, participating in Vote Leave's illegal financing efforts. Cambridge Analytica's efforts — and lack of real punishment for those involved — prove that powerful politicians and their friends are all too often above the law.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Zucked,_** **by Roger McNamee**

Shocked and horrified at what you've learned in these blinks about what goes on in the psychological warzone of social media? Well, steel yourself, because we've only scratched the surface of the manipulative techniques Facebook uses to keep you hooked.

In our blinks to _Zucked_, you'll learn how Facebook is helping to polarize public debate. You'll also see how easy it's been for external forces like Russia to use the site to influence US voters.

To become a more conscious social media user — or get the last push you need to give it up entirely — head on over to our blinks to _Zucked_.
---

### Christopher Wylie

Christopher Wylie is a Canadian data consultant known for being the Cambridge Analytica whistleblower. He now works in fashion-trend forecasting in London.

