---
id: 566610d5cb2fee0007000023
slug: mindful-work-en
published_date: 2015-12-10T00:00:00.000+00:00
author: David Gelles
title: Mindful Work
subtitle: How Meditation is Changing Business from the Inside Out
main_color: FED533
text_color: 806B19
---

# Mindful Work

_How Meditation is Changing Business from the Inside Out_

**David Gelles**

_Mindful Work_ (2015) provides an introduction to why and how mindfulness is practiced in the West today, on an individual, as well as professional level. Using examples from well-known companies, it explains the benefits that mindfulness can offer you, the people around you and the environment.

---
### 1. What’s in it for me? Calm yourself down; Get mindful. 

We're all in this stressful and crazy world together. A world that's moving so fast that you can't keep up with everything and everyone. How many times a day do you think, "I need to calm down," and before you know it, you're googling for cheap tickets to the Bahamas?

But wait! There's a cheaper way to go about it. You don't have to run away or jump on a plane every time you feel completely burned out. In fact, you don't even have to leave your workplace to refresh your focus and sustain your otherwise great ability to solve problems. Here's the key: Mindfulness.

Many companies have already opened up to the idea of mindfulness, and how it might benefit their work culture and thus business in general. And this is exactly what these blinks will teach you. They feature great insights into mindfulness, the way it works and who (aside from yourself and your business) is benefitting from it.

In these blinks, you'll discover

  * that the benefits of mindfulness are supported by scientific proof;

  * how the big US food company General Mills relates to mindfulness; and

  * why multitasking is a very bad idea.

### 2. Mindfulness is already practiced in well-known companies. 

For many people, work hours have become blurred and things like checking business emails outside of office hours have become commonplace. With this change, stress has become a considerable risk, not only to employees, but to companies at large. Shockingly, according to the World Health Organization, stress costs American businesses as much as $300 billion a year. 

To deal with this challenge, more and more companies are catching on to _mindfulness_. You can think of mindfulness as the easiest form of meditation. Basically, it means paying attention to the present moment and noticing your thoughts without judgment. 

So how does mindfulness relate to work? Simply put, it makes dealing with issues at work easier.

Mindfulness has many benefits, which we will delve into in the blinks that follow. But what all of these benefits have in common is that they make us more self-aware, train our focus and increase clarity. All of which are valuable qualities to have in the workplace.

Mindfulness allows you to step back from the problem and observe it so you can deal with it more objectively and reach solutions more easily. For example, working with a colleague who slacks off can make you livid, but being mindful can help you realize the reasons for their behavior. Perhaps they are tired because they have a newborn baby, and talking to them about it can solve the problem.

As a new generation of companies start to understand the benefits of mindfulness, a mindful work culture is already gaining momentum.

Even multinational companies have cottoned on. General Mills, for example, was a pioneer in providing mindfulness training during work time. The training was initiated by Janice Marturano, who began practicing mindfulness after dealing with personal and professional stress. Soon, hundreds of General Mills employees were participating, and now all of the firm's buildings have meditation rooms.

> _"Mindfulness, put simply, is the ability to see what's going on in our heads, without getting carried away with it."_

### 3. Mindfulness affects our behavior and our brains. 

Up until the 1990s, it was difficult for fans of mindfulness to get others on board with the idea. The effects they were experiencing with the technique weren't quantifiable and there was no scientific study to support the practice. 

However, since the invention of functional magnetic resonance imaging or _fMRI_ — a technology that can measure brain activity through blood flow — the benefits of mindfulness can now be scientifically proven.

By using fMRI, scientists can demonstrate that when we meditate, the area of our brain responsible for thinking about ourselves becomes significantly calmer. Experiments have also shown that when someone carries out a task that makes them think about their own thoughts or behavior, such as highlighting a word that describes themselves in a text, and then follow up with a mindfulness exercise, the brain's reactions to similar situations that involve self-reflection are gradually reduced. This means that with time, those who practice mindfulness can decrease the degree to which they are judgmental, because their brain has practiced being more objective.

Another benefit to mindfulness is that it shapes our brains. They aren't rigid structures — a brain can in fact change a great deal. It can be trained like a muscle and altered by repeated thought processes. This quality is known as _neuroplasticity_. Research has shown that mindfulness increases activity in the brain's prefrontal cortex, heightening our kindness and compassion. It also strengthens our cortical thickness — or gray matter — which aids memory function and emotion regulation.

Perhaps the most noteworthy finding, however, relates to the amygdala. In stressful situations, such as a job interview, our fight-or-flight response is triggered by the amygdala. This is obviously an overreaction, as the days of having to run from, or face and kill an animal predator are largely gone. When we practice mindfulness, the amygdala gradually relaxes in the long run, which helps us keep stress at bay.

### 4. The most popular form of meditation training today is Mindfulness Based Stress Reduction. 

As more studies and more high-profile people like Steve Jobs began endorsing mindfulness, people started paying attention. But it wasn't until one man in particular started tailoring mindfulness practices to Western culture that it really gained popularity.

During the late 1970s, Jon Kabat-Zinn developed a method called _Mindfulness Based Stress Reduction_ (MBSR). MBSR is independent of any religious belief and is based on training the attention, with the goal of becoming fully present and focusing on the physical sensations in the body.

For example, one exercise involves noticing if you're experiencing pain anywhere or if parts of your body feel relaxed. After a few repetitions, you'll begin to realize that pain and relaxation aren't constant, but continually change in intensity. In this way, MBSR trains your self-awareness and emphasizes the impermanence of things. Applied to stress, this means observing your thoughts and physical reactions and watching as they dissipate. By observing your reactions objectively, you'll come to learn that no pain or stress lasts forever.

With Kabat-Zinn's MBSR, mindfulness and meditation entered mainstream culture, offering an effective solution to the ubiquitous problem of stress.

> _"We can't always make a pain in our legs go away, but we can control our reaction to it."_

### 5. Practicing mindfulness enhances your focus. 

As we've seen, mindfulness is about observing your thoughts and emotions and deciding what to concentrate on. 

Focusing on one thing at a time is far more efficient than multitasking, and mindfulness is great at helping you do just that. At work in particular, we're bombarded with stimuli such as multiple computer screens, which cause perpetual distractions. So how do we react? By multitasking. 

However, contrary to popular belief, this isn't a productivity booster. Instead it means we're frantically jumping from one task to another. Furthermore, multitasking adds more opportunities for your mind to wander as you consider what to do next. It's far more effective to figure out in which order things should be carried out and then tackle one thing at a time.

Say, for instance, you start thinking about what you should do next at work. Thoughts like "I need to pick up some groceries" or "I have to confirm my reservation at the restaurant" will start sneaking in, because they're also on your to do list.

Thankfully, as mindfulness develops your focus, your ability to direct your attention slowly becomes automatic, and knowing how to prevent your mind from chasing every thought you have means you'll be less easily distracted.

But how do you train your attention? Well, be it at your desk, a basketball game or talking with a colleague, you must observe your thoughts so that you can bring yourself back into focus.

For example, when National University professor Mason Fries was a college wrestler, he was about to win a match against a former national champion. But when he got his opponent onto the mat, thoughts like "I can't win, he was a champ" crept in and he lost his grip. If he had focused on the moment, it would have helped him, and he could have won the match.

> _"It's about doing whatever it is we're doing fully, one thing at a time."_

### 6. Being mindful cultivates compassion. 

Have you ever noticed that people who practice mindfulness not only seem to be calmer, but also kinder and more open?

This is because mindfulness nurtures a sense of self-compassion and connectedness to others. As you learn to observe your emotions in a non-judgmental way, you'll start to see that feelings like frustration are natural and that we all have a bad day now and then. 

Once you get to this level of compassion, you feel a sense of relief as you learn to accept negative emotions and open up to joy and empathy. As it develops, it begins to radiate outward to people around you, enabling you to relate to their problems and emotions in a more empathic, non-judgmental way. This can be referred to as a sense of common humanity or social connectedness.

This sense of connectedness to others can be strengthened using a special meditation called _metta_, or loving kindness. Metta involves wishing yourself happiness, protection and freedom from harm. As you train these feelings toward yourself, you can start to wish them upon people directly around you, other people you know, and finally everyone in the world. 

Some people view compassion as something soft or weak, but it actually makes you stronger. It means being more aware of the feelings of others and wanting to prevent them from experiencing pain. For example, police officer Cheri Maples always used to become stressed and aggressive when she was called out on the job. But practicing mindfulness changed her perspective and she started to see the call as an opportunity to help rather than a lure into a fraught situation.

Of course, mindfulness isn't a magic wand that makes all our problems disappear, but it does make us more aware of our own and others' feelings. Because of this, it helps make us more tolerant, loving and kind.

> _"In time, mindfulness both creates the room for compassion and inspires us to help other people, too."_

### 7. Mindful companies develop social responsibility. 

Because it involves you and everyone around you, mindfulness can be applied directly to your work, fostering compassion for others and the world beyond.

Businesses with a mindful culture are likely to act responsibly when it comes to the environment, too.

When you practice mindfulness for long enough, you'll start to realize that you're not only responsible for yourself, but in part for society as well. For example, outdoor clothing brand Patagonia tries to inform their customers about mindful consumption as they want to lessen negative impacts on the environment. They even began one of their campaigns by asking their customers _not_ to buy their clothes, as a prompt to get them to reconsider their shopping habits.

But mindful companies care about more than the environment: they care about everyone involved in the company. Take Eileen Fisher, an apparel brand that gives at least ten percent of its annual after-tax profits to its staff.

In 2012, the company tackled the Chinese firms that made the silk they used in their clothes, because the production system was known for its harmful effect on the environment and poor conditions for workers. Now, 45 percent fewer chemicals and 25 percent less water is used for the production of Eileen Fisher's silk in China. In addition, the silk workers receive better salaries and more time off. 

Companies that support a mindful culture realize that they can initiate broader social change, too. Adherence to mindful principles can also influence employees and customers to support the social change they promote. For instance, ShareTheMeal, an app dedicated to collecting donations for hungry children worldwide, is very conscious of its goals. This not only encourages employees to believe in them, but also inspires them to spread the word about the app. 

It's important for companies to act socially responsibly, but this is only possible when the right people lead the way, as you'll see next.

### 8. Mindful leadership involves all the qualities of a great leader. 

What makes a good leader? Strength? Empathy? Strategic focus? Sadly, most leaders don't embody these qualities, seldom listen to their employees and fail to communicate their targets.

This is not the case for mindful leaders, however. Mindful leaders respond to challenges more easily, because they're more focused on their goals. They're aware of their own emotions and aims, acknowledge what is happening around them, and listen attentively. Because of this, they are far better positioned to make decisions based on honesty and compassion.

Furthermore, mindful leaders break challenges down into manageable parts and, as a result, they manage their time more effectively, producing beneficial outcomes for everybody involved. Leaders who don't practice mindfulness tend to become stressed out when faced with an urgent task and pass this stress on to their employees. A mindful leader will try to separate the task from stress and assign it to an employee who is skilled at handling that particular matter.

As well as delegating efficiently, mindful leaders act as role models, foster compassion, and have the courage to change company principles when necessary. This is exactly how they inspire their employees to take more responsibility.

Eventually, this positive approach causes a ripple effect and motivates employees to initiate other innovations and social change. It's similar to the way a charity functions: once touched by the power of giving, donors are more likely to start sharing with others. For example, letting your employees choose which organization a donation will go to may well inspire them to organize their own fundraising or charity event.

> _"If all our bosses were a bit more mindful — accepting, focused, compassionate — we all might be a bit happier under their supervision."_

### 9. There is more to mindfulness than following a trend and making money from it. 

As with any trend, there will always be people who jump on the bandwagon, eager to make a profit, and there will always be skeptics. 

Of course, some skepticism on the use and effects of mindfulness is warranted. Indeed, some critics have grounds to think that the spiritual or compassionate motives behind mindfulness practices are being capitalized on and exploited.

Although not all the criticism is justified, the fundamentals of the practice — developing compassion — are becoming increasingly watered down. For example, some classes teach mindfulness just as a therapeutic technique or to boost workplace productivity, sadly skipping over compassion. This diluted form of the practice has become known as _McMindfulness –_ a quick and shallow use of mindfulness as a tool.

Moreover, as mindfulness increases awareness of emotions, it can lead to emotional struggle, as the emotions that have long been suppressed start surfacing. It's not just about becoming happy, as some believe; it's about dealing with _all_ emotions.

Due to the plethora of mindfulness organizations, teachers and classes now on offer, there is a growing need for a regulatory organization. As you may know, searching for a mindfulness class can be confusing, as there are so many out there. Therefore some general standards would be useful for establishing training courses, certificates, and so on. This would also prevent fraud, as participants would know how to find and read up on certified practitioners. Furthermore, those still deterred by inexperienced or disorganized Western practitioners may be more inclined to try true mindfulness.

As we've seen, mindfulness can have a whole host of positive effects. However, it's still a technique that requires practice and the right teachers.

> _"What's most important is that people are practicing mindfulness for the right reasons."_

### 10. Final summary 

The key message in this book:

**Mindfulness can be practiced by anyone. Even in small amounts, it has a positive impact on your emotional and physical well-being, which is why more and more companies are choosing to implement mindful practices such as meditation classes into their culture.**

Actionable advice:

**Strengthen your focus.**

The next time you are stressed and feel your mind jumping around, try this: sit calmly with your eyes closed and focus on your breath. Notice how you inhale and exhale and how your breath flows through your body. As your mind starts to wander, gently coax it back to your breath. Even doing this for a mere five minutes can help calm and re-focus your mind.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### David Gelles

David Gelles has been practicing mindfulness for over 15 years since learning about the technique in India. He has also written on mindfulness for the _New York Times_, _Financial Times_ and _Forbes_.

