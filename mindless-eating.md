---
id: 5344491b6432320007030000
slug: mindless-eating-en
published_date: 2014-04-08T09:58:07.000+00:00
author: Brian Wansink
title: Mindless Eating
subtitle: Why We Eat More Than We Think
main_color: 87B0CE
text_color: 556F82
---

# Mindless Eating

_Why We Eat More Than We Think_

**Brian Wansink**

_Mindless Eating_ explores the diverse messages and influences that constitute our eating habits, which we tend to follow "mindlessly." It also offers practical solutions on how to exploit these subconscious influences in order help meet our health or weight-loss goals. Please note that the validity of some of the research underlying the author's work have since been called into question.

---
### 1. What’s in it for me? Find out why your plate size and the people around you can cause you to overeat. 

_Mindless Eating_ offers us a look inside the various, often subconscious factors that influence how much we eat, what we eat and whether or not we enjoy our meals. As you'll learn, the truth is much more complicated than us simply "eating what we want until we're full!"

In these blinks, you'll find out about the never-ending soup bowl and its drastic consequences for our health, as well as the steps you can take to help "trick" yourself into eating the amount of food that's right for you.

You'll also learn all about the tactics marketers use to make you salivate over otherwise boring foods and some simple tips to help you to make positive, life-long changes to your diet.

### 2. Restaurants and marketers can use all kinds of tricks to influence what we eat. 

Do you believe that marketing and advertising have no effect on you? Say, when you're choosing which groceries to buy? "Sure, other people may be swayed by good marketing," you might think, "but not me!"

And although you wouldn't be alone in this belief — almost everyone thinks they're impervious to marketing — it turns out that, when it comes to food, there are some marketing tricks that work on everyone.

First of all, good marketing can make foods seem more appealing.

For example, it has been found that plopping positive descriptive adjectives in front of the names of dishes on restaurant menus generates more sales and greater customer satisfaction than using "boring" names.

In one experiment, a restaurant served the same menu twice, one week apart, but used different names for the same dishes, for example, "Seafood Filet" was transformed into "Succulent Italian Seafood Filet." Patrons who'd seen the more descriptive name found the dish tastier, and felt better about the restaurant in general, despite the dish being made the exact same way.

Second, brand marketing can also influence our perception of food. For example, we often presume that recognized brands like Coca-Cola or Ben & Jerry's will taste better than "generic" alternatives. This impression is so powerful that, although blind taste tests indicate we perceive no difference between the brand-name and generic products, we're still willing to pay more for the brand.

Finally, we're also affected by things like lighting, music, temperature and decor when we eat — the things we would say comprise the "atmosphere" of the meal. When lights are bright and the music has a quick tempo, we tend to eat more quickly than with slow music and dim or natural light.

### 3. We can manipulate the subconscious factors that affect our appetite to our own benefit. 

As you've seen, when it comes to food, your choices can be influenced on a subconscious level through things like descriptive adjectives: a "succulent" dish, for instance, tastes better than a regular one.

While marketers can take advantage of this fact to affect our food choices, luckily, we can also harness some of our own subconscious thinking patterns to eat less.

For example, if we'd like to eat smaller meals to lose weight, we can fool ourselves into thinking we've consumed more calories in a meal than we actually have. That's because our eyes, not our stomachs, tend to determine how full we feel after a meal.

So if, for example, a meal _appears_ large, we'll feel full after eating it even if it contains relatively few calories. Armed with this knowledge, we can simply add lots of low-calorie garnishes to our meals to make them appear large enough to satisfy us, while still ingesting a low amount of calories.

Another way we can trick our subconscious into eating less is by changing how we decide we've had enough to eat during a meal. This saturation point isn't determined by our stomachs, either, but by social and environmental cues around us, called _scripts_, which we usually follow mindlessly to determine when we're "full."

For example, if you're watching a movie with a bucket of popcorn on your lap, you probably won't think about whether you're really hungry anymore or not, but will follow the _script_ you're used to following when watching movies, that is, continuously nibbling on whatever snacks are within arm's reach.

Luckily though, if we're aware of these scripts, we can change them to our own benefit. For example, in the movie situation, you could bring a small serving of popcorn instead of the whole bag, thereby forcing yourself to stop eating once it's empty.

### 4. The size of your serving – and the plate it’s on – can fool you into eating more than you need. 

As we've already seen, our subconscious plays a big part in how much we eat. Perhaps the most obvious and most easily adjustable factor affecting our food consumption is our food's presentation.

Quite simply, the bigger the plate or container of food given to us, the more we'll eat.

This was vividly demonstrated in an experiment where movie-goers were given either a "medium" or "large" bucket of popcorn. The catch was that both sizes were deliberately made so big that the portions would be impossible to consume in their entirety. Furthermore, the buckets were filled with five-day-old popcorn to make the snack less appealing.

The result?

Even though both buckets contained far too much popcorn to be consumed, people who had been given the larger bucket ate more of the vile snack.

So how can we put this knowledge to use?

First of all, it's important to understand that we rely mostly on visual cues to decide how much we want to eat. This means our appetite can be affected by optical illusions, specifically those relating to the size of the plate.

For example, if you take two equally sized portions but serve them on two differently sized plates, the portion on the larger plate will seem smaller. This means you won't feel as satisfied after eating it.

Why?

The empty space surrounding the food on the larger plate creates the optical illusion that the portion is smaller.

Restaurants know this, of course, and will occasionally deliberately use large plates to fool you into thinking you've eaten less so you'll order another dish.

So the next time you serve dinner, try this experiment: serve two identical portions, but put them on differently sized plates and then see who's hungry for seconds!

### 5. We rely on social cues to tell us when to stop eating rather than listening to our body. 

The human body has a wonderfully broad and intricate set of ways to convey what state it's in. For example, if we've eaten something dangerous we get nauseous, and if we've been injured, we feel pain.

Unfortunately, when it comes to eating, the bodily cues that signal fullness are not sufficient to stop us from consuming more food.

Why?

Because it takes too long for the "stop eating" cue to reach us. In fact, it takes about 20 minutes for our digestive system to inform the brain that we've had enough. And a lot can happen in 20 minutes: Just imagine how many soda refills or extra cookies you can consume in that time.

Of course, this wouldn't be a big problem if we ate slowly, but we don't. In fact, the average American gulps down his or her lunch in just eleven minutes. If you're similarly fast, your body never even gets the chance to let you know it's full.

So wait, if our bodies don't determine how much we eat, what does?

The answer is social cues — which we rely on to tell us when it's time to stop eating.

For example, we often continue eating until the last person at the table has finished their meal. If we finish our first helping and see someone else is still eating, we'll typically go for second and possibly even third helpings until that person has finished, too. Consequently, if you eat very quickly, you'll probably end up consuming much more than is actually necessary.

Although the exact social cues differ from person to person, they tend to follow patterns like the one in the previous example. 

So the next time you're having dinner with a group of people, try this experiment: be the last person to start your meal and deliberately pace yourself as the slowest eater in the group. Did you go back for seconds? Probably not.

### 6. Our inability to accurately gauge portion sizes can lead to serious overeating. 

Be honest: when was the last time you looked at the recommended serving size on your 20 oz bottle of Coke? Probably never. Because one small bottle equals one serving, right?

Wrong.

A 20 oz bottle actually contains 2.5 servings, but you probably never realized it and always happily guzzled it down in one go.

That's just one example of how we gauge the portion size based on packaging and _not_ on nutrition labels.

Why do we do this?

Basically, we don't tend to think of a package containing 0.5 or 3.5 servings of something. It's much easier for our brains to consider the package as a whole stand-alone _unit_ of food.

That's why, even if you drink half of your 20 oz Coke (that is, more than one serving), you'll still feel like you "didn't finish it."

And this is proving to be a serious problem in our "super-sized" culture.

Why?

Because evolution has hardwired our brains to seek convenient sources of food. We always want to put in the least effort to obtain the most calories, which means that as long as, for instance, buying a big bag of M&M's offers us more food for less money, we'll spring for it.

Today, supermarkets are filled to the brim with large, "family-sized" portions of just about every food imaginable. But while we believe these big packages are good value for money, they're actually promoting unhealthy eating habits: when we buy a family-sized package of, for instance, spaghetti, we end up making a bigger portion of it, because our brain can't fathom the serving sizes.

And when we make more of something, we also eat more, because, as you know, it's the size of the portion that determines how much we eat, not our stomachs.

### 7. Improve the health of your whole family by controlling the food you buy and the meals you cook. 

Many parents today regulate how much and what kind of television their kids watch. By doing so, they're encouraging their children to socialize and protecting them from inappropriate content.

But there's another important aspect affecting the health of a household's members: the food available to them. These foods should also be closely regulated.

How?

For example, if you buy food in smaller packages, you can help prevent overeating, because larger packages lead to greater consumption.

Another method would be to buy family-sized packages, but then divide them into smaller packs as soon as you get home to trick yourself into cooking and, in turn, consuming less.

Regulating what food makes it into your home is an effective method and a great first step toward creating a healthier lifestyle. The next step involves actually cooking meals with that food.

To do this well, you should strive to serve a variety of foods, even branching out to exotic foods you may not be familiar with from childhood.

One study demonstrated that this produces a healthier meal environment for families. Though each meal need not always be healthy, the variety of flavors present in diverse cuisine should gradually help families move away from the inborn human preference for salty, fatty and sugary foods.

So the next time "Pizza Friday" or "Taco Tuesday" rolls around, consider trying a new recipe. And if you're feeling especially adventurous, why not enroll in a cooking class or buy a new cookbook to give you some fresh ideas.

### 8. Modest, incremental changes to your diet are the best strategy for long-term weight loss. 

Have you ever tried to go on a diet only to find yourself relapsing into your old habits just a few weeks later?

If so, don't feel bad. It's less of a personal failure than you think. The fact is that diets demand radical changes to your routines, and radical changes are hard to adhere to.

So how can you make lasting changes to your diet?

Basically, you need to opt for modest, incremental changes rather than big, sweeping ones that require major life changes.

One example of such an incremental strategy is _The Power of Three Checklist_. Look at your calendar for the next month and write out three things you can do each day to help you consume 100 fewer calories than you normally would per day. This could mean steps like eating at least as many vegetables as meat, drinking only one soda over the course of the day and so forth.

Then check off the tasks you accomplish every day. The goal is to accomplish all three every day, but even if you don't, you'll know you're still making progress toward your goal and that will keep you motivated.

Another common reason why diets fail is that they're often too harsh, requiring you to completely give up the things you enjoy. If you love desserts and candy, but decide to completely abstain from everything sweet, you're bound to cave in sooner or later.

Instead, think about how you can combat the habits that contribute to weight gain without having to abandon the things you love.

One possible strategy would be to make "food trade-offs," meaning you promise yourself that you can, for example, eat a dessert after dinner as long as you've had a salad for lunch. This will prove far more durable than depriving yourself completely.

So the next time you are considering a diet, remember the story of the tortoise and the hare: don't push yourself too hard, for slow and steady wins the race.

### 9. Tracking what you’ve already eaten during a meal can help you eat less. 

Think back for a moment: When was the last time you counted how many breadsticks you ate at your favorite Italian restaurant? Probably never.

We don't normally keep track of how much we eat. In fact, we often have no idea how much we've actually eaten after a meal; we just basically assume that, however much we ate, it was probably pretty much the right amount.

This obliviousness to the amount of food consumed was illustrated by one ingenious experiment where researchers rigged a restaurant table to continually but inconspicuously refill some patrons' tomato soup bowls as they ate.

The result?

Not only did the patrons not notice that something was amiss, but they also overate considerably. They slurped down a whopping 15 oz of soup on average compared to the 9 oz consumed by those eating from normal bowls.

Why did they overeat so much? As discussed earlier, we rely mostly on visual and social cues to figure out when we should stop eating, and in the absence of such cues, we'll usually eat till we literally can't have another bite.

So how can we remind ourselves of how much we've already eaten during a meal?

One word: _evidence_.

Consider one experiment where students at a restaurant were given an unlimited supply of chicken wings delivered to their table by waiters. At some tables, the bones and other scraps were cleared away rapidly, while other tables had bowls for the scraps that were left on the tables.

This arrangement meant the latter group had evidence of how much they had eaten: a pile of chicken bones. As a result, they ate 28 percent less than the group whose scraps were out of sight and out of mind.

So the next time you go to an all-you-can-eat restaurant, try to keep some evidence or a record of how much you've eaten or you'll likely keep eating until you have to be rolled home!

### 10. Final Summary 

The key message in this book:

**Although it might seem counterintuitive, we have very little control over our eating habits: covert tricks used by marketers and restaurants as well as subtle social and visual cues all influence how, and how much, we eat. However, by being mindful of our eating habits and making conscious decisions about _how_ we eat, we can make modest, incremental changes to our diets.**

Actionable advice:

**Use smaller plates**

Our eyes are rarely actually bigger than our stomachs. In fact, they often determine how much we will consume during each meal. One way to take advantage of this is to use smaller plates: the negative space on larger plates makes portions seem much smaller than they are, and you consequently consume more. If you use small plates instead, you can make smaller portions seem larger, thus priming you to eat less.

**Don't deny yourself the things you love**

The quickest way to ensure failure in a diet is to deny yourself your favorite foods. A diet worth sticking to is a one that allows you to accommodate chocolate cake and greasy burgers; you simply need to find a way to incorporate those foods in a way that will result in a net calorie loss. Instead of saying "no" to ice cream, say "yes" to ice cream only if you've eaten salad for lunch!
---

### Brian Wansink

Brian Wansink was the Professor of Consumer Behavior and Director of the Cornell Food and Brand Lab at Cornell University, and Executive Director of the USDA's Center for Nutrition Policy and Promotion. He has been accused of academic misconduct in his research, which led to his resignation from Cornell. Several of his research papers have since been retracted.

