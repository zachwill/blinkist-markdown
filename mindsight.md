---
id: 5820ea7a4d1bae00032dc442
slug: mindsight-en
published_date: 2016-11-11T00:00:00.000+00:00
author: Daniel Siegel
title: Mindsight
subtitle: Transform Your Brain With the New Science of Kindness
main_color: 298FCC
text_color: 1F6C99
---

# Mindsight

_Transform Your Brain With the New Science of Kindness_

**Daniel Siegel**

_Mindsight_ (2010) introduces the reader to the many factors that shape the way we react to life's challenges. Emotional responses are tied to our bodies, brains and childhood experiences. With mindsight, we can learn to manage our emotions in order to improve our relationships and well-being.

---
### 1. What’s in it for me? Learn to apply mindsight to get in touch with your inner self. 

Life can be a troubling experience. Traumatic events during childhood can deeply affect us until old age. Injuries to our bodies can transform the way we perceive the world, and conflict in our social environments, be it at home or at work, can lead to stress and depression.

What can we do to find our way back to ourselves, to understand how we make sense of the world and how to live in harmony with our social environments?

We can apply mindsight, a set of psychological techniques designed to help us understand and reshape our inner worlds.

In these blinks, you will learn what mindsight entails, how it relates to your mind, body and social relations and how you can apply it. You will see various examples where mindsight is a valuable approach and will learn about its power to bring about positive change.

You'll also find out why

  * an ocean is a good metaphor for your mind;

  * what watching movies with the sound turned off can teach you; and

  * how mindsight will facilitate dialogue with your partner.

### 2. Mindsight allows us to learn about the connections between mind, body and attitude. 

Have you ever been in the middle of an important discussion when something pushes you over the edge? You might suddenly grow angry, your mind might go blank or perhaps you'll feel an uncontrollable urge to leave the room as quickly as possible. Sound familiar?

Many of us experience reactions like this. They can be deeply confusing, leaving us at a loss to explain our own behavior. To understand these situations, we need to understand our internal worlds — and to do this, we need _mindsight_.

Mindsight is the skill that allows us to reflect on the connection between the body and the mind. This is central to learning how to regulate powerful emotions. Mindfulness techniques such as meditation are examples of mindsight, as they increase our awareness of our heartbeat and breathing.

But mindsight isn't just something to practice when you have quiet time to yourself; it is a tool that you can use when life gets loud, messy and overwhelming. For instance, watching your kids scream and fight over food can make you upset. However, your children aren't the direct cause of your growing distress — it's your increasing heart rate.

By turning your attention and awareness to your heart rate, you can learn to regulate its influence on your emotions and get a better grip on the situation before you. By remaining calm and patient, you'll be able to settle the conflict between your children, rather than exacerbate it by reacting with frustration.

As well as looking into your own internal landscape, mindsight encourages us to see the world through the eyes of those around us. This is something all humans are innately capable of, even though we often take it for granted; without the capacity for empathy, we would struggle immensely.

This is a challenge faced by Barbara, one of the author's patients. During a car accident, the mother of three suffered damage to her prefrontal cortex, the part of the brain that enables us to use mindsight to empathize with others. Having lost her sense of empathy, Barbara has struggled to maintain caring relationships with her kids and friends.

So what does a life with good mindsight look like? Let's take a closer look in the next blink.

### 3. The goal of mindsight practice is a balanced, harmonic self. 

Some of us are addicted to rigid morning routines. Others start to squirm if something feels too "planned." Whether you're strict or spontaneous, too much focus on either extreme can make you hard to be around, and unhappier in the long run. Your best bet is to find the right balance by creating a _harmonic self_.

Having a harmonic self is all about keeping your personality balanced. With a balanced personality, you'll be able to adapt to external changes, but will also stay stable and true to your core values.

This is a bit hard to grasp in abstract terms, so mindsight-based therapy makes use of the image of a gently flowing river. Rather than clashing with changes in your relationships and environment, simply make room for them while continuing on your original course.

To make your personality flow, you must first accept that it's normal to act in a range of different ways, to have relationships that contrast with one another and that past experiences will shape you in different ways as you grow older.

A harmonic personality is also tied to creating a balance between rational, analytical thinking and emotional, intuitive thinking. While both modes of thinking are valuable, placing too much emphasis on one or the other can be detrimental.

Take Stuart, one of the author's patients, a retired lawyer who was suffering from depression. It turned out that because Stuart only accepted rational, logical thoughts, he'd been repressing his emotions for years; this imbalance was at the heart of his sense of emptiness. For Stuart, learning to appreciate his emotional and rational sides in equal measure was the first step to overcoming his depression.

### 4. Mindsight is brain training that keeps us resilient and emotionally healthy in everyday life. 

Mindsight can be a powerful part of psychotherapy. With a deeper understanding of how brain activity shapes the way we see the world, you'll be able to harness the power of mindsight. So how does it work?

Mindsight is like training for your brain. It teaches us how to reflect on thoughts and helps us create new connections between them. The more we associate two ideas with each other as we reflect on them, the stronger the neural relationship between those ideas will become.

Mindsight also trains our brains by boosting memory. Conscious reflection on past or future situations can be as vivid and powerful as experiencing those events themselves. By encouraging our minds to re-enact entire scenes in full detail, we activate the same areas of our brains that would light up if what we were imagining was taking place in real life. Neuroscience has even demonstrated that every single thought we have alters blood flow and neural signal rates.

By dedicating time to training our brains, we're better able to deal with unexpected challenges. Our neural behavior directly shapes how we manage issues in our environment — so much so that certain physical parts of our brain are responsible for different reactions.

The prefrontal cortex, located just behind the forehead, triggers moral judgments, our attention (or lack thereof), our sense of time and our sense of identity. The insula, on the other hand, is responsible for our emotions, as well as how we respond to the emotional displays of others. Mirror neurons are specialized cells that help us understand the intentions of those around us.

Since mindsight allows us to boost our awareness of these areas of the brain, we're able to better control the reactions that they create.

### 5. Mindsight helps us train the right side of our brains to acknowledge and manage discomfort. 

You might think that talking about your feelings is simply a matter of opening up. Sometimes, we experience emotions that we're not even aware of. But just like any other skill, becoming aware of our emotions can be learned; practice makes perfect!

This is a central focus of mindsight-based therapies. Mindsight targets the right hemisphere of the brain, which is responsible for our emotional awareness. Though we're more comfortable exercising the rational capabilities of the left hemisphere, the right hemisphere is the side we should exercise to deepen our understanding of our own feelings. Thankfully, this isn't too hard to do.

You can stimulate your right hemisphere by taking part in nonverbal communication games, from imitating facial expressions, to trying to read movie character's emotions with the sound off. You can also keep a diary to record all the emotional sensations and imagery of your experiences, rather than rationalizing them.

When you're confronted with your own intense emotions, you can use other techniques that focus awareness on bodily sensations. The next time you're stressed out, try doing the _body scan_. This entails lying down on the floor and focusing on different parts of your body, one by one.

This can be quite uncomfortable at first, as a sore back or itchy nose might make it hard for you to concentrate. But with practice, you can focus your mind on mental images of safe places to stay in control of your responses.

One of the author's patients hyperventilated while doing a body scan. After ensuring the patient felt safe with her, they prepared a mental image of a safe space together to try for next time; the safe space, in this case, was a cove at the beach. On her next attempt at the body scan, the patient was able to focus calmly, without letting the intense sensations of her body bother her.

### 6. Mindsight helps us see how feelings are fleeting experiences, not our defining character traits. 

While it's all too easy to tell someone to "keep their temper," emotions can be very tough to manage. Many people find emotions overwhelming and distressing, but mindsight helps us realize that they're all just fleeting parts of the human experience, rather than ingrained behaviors that indicate we're somehow flawed and dysfunctional.

Meditation exercises, for instance, train people to focus on one thing and one thing only. Such exercises require that, if you do get distracted, you bounce back and return to your focus. This is a great way to experience how thoughts and feelings are just temporary experiences, rather than the foundations of your personality.

Another exercise to try involves imagining that your mind is an ocean. Your thoughts and feelings are what moves over the ocean's surface; they can be ripples, or they can be storms. But no matter how big the waves, there is calmness at the bottom of the ocean. Stormy feelings are surface-level and temporary — it's up to you to find the calmness on the ocean floor!

More broadly, mindsight exercises like the above two examples are built around three key pillars: observation, objectivity and openness. By learning to take all three of these in your stride, you'll be better positioned to understand your own feelings.

Learn to _observe_ your mind by noticing when distracting thoughts pull you away from your focus. This might be during meditation, or even when negative thoughts slow your productivity at work or make it hard to fall asleep at night.

Next, realize that you can train yourself to _objectively_ follow the path of your attention. You can study where your thoughts carry you and how they make you feel. This, in turn, makes you more aware of how underlying prejudices or instinctive reactions are also thoughts that shape your experiences.

Finally, by staying _open_ and accepting that judgmental, depressing or confusing thoughts are just temporary, you'll see that emotions don't need to be cause for distress. They're natural, and by acknowledging that, you have the power to learn from them and change them.

> _"Tonight, I could see my explosion at him, like from a watchtower."_

### 7. Negative childhood experiences might shape how we see our world today, but we can use mindsight to overcome them. 

Who were your teachers in the first years of your life? Your parents, most likely! But parents do far more than teach us to read or how to ride a bike. We also learn a great deal from everything that they teach us subconsciously.

For better or for worse, our upbringing shapes how we interact with people today. Kids raised by parents who only showed affection inconsistently often feel that they can't trust anyone when they become adults — not in friendships, not at work, not even in loving relationships.

Similarly, children who had to take on a lot of responsibility at a young age learned that showing weakness is a failure. This means that they have genuine trouble opening up about their feelings as adults — since childhood, they weren't comfortable with being seen as vulnerable.

Although these misconceptions we learned during childhood have been with us for a long time, we can, in fact, overcome them with mindsight. A great way to start is to write down as many of your earliest memories as you can, as well as some of the most recent.

This helps you get everything off your chest, and will help you see what narrative you've used to make sense of your past as a child. Examine the story that you tell about yourself and consider the possibility that there is no need for one perfectly coherent narrative. Rather, we're all made up of multiple narratives within each stage and aspect of our lives.

The narrative you tell of your earlier life will reveal how your current difficulties are bound up with how you perceive your childhood. This helps you see these patterns for what they are: harmful. Discovering alternative stories to tell about your childhood and adult life is the first step to knocking down those walls you built around yourself as a kid.

### 8. Use mindsight to be receptive rather than reactive to your partner when addressing relationship problems. 

Many of us know what it's like to have stupid fights with our partners. It starts out with one person addressing a problem, but soon seems to devolve into fighting for the sake of fighting. What prevents these discussions from being resolved peacefully? In the end, it's a matter of attitude.

The key is whether you're _reactive_ or _receptive_ toward your partner. This one factor can have a massive influence on the quality of your relationship as a whole — so it's pretty important stuff! But what does it mean?

Well, being receptive means openly listening to your partner. Receptiveness makes your partner feel like you acknowledge and value their feelings, which in turn helps them open up and share what's on their mind.

Being reactive, on the other hand, is what we do when all of our partner's complaints feel like threats to us. We enter fight-flight-freeze mode, and either want to attack our partner, defend ourselves or stop talking and avoid eye contact. This is problematic for both of you, leading to a vicious cycle.

If one person feels the need to discuss an issue while her partner feels the need to flee whenever she brings it up, they'll just want to push the issue even further away; this, in turn, can lead to endless fights and miscommunication. So what can we do better?

Mindsight helps us be receptive toward our partners and facilitates dialogue in a relationship. Both partners should take the time to reassess the narratives they've told themselves about their lives and consider whether they're accurate, or whether alternatives are possible. They should then share these with each other, which can help each partner understand the other's motivations and emotional needs.

Other strategies to try include the _timeout method_. This requires mindsight, as you'll need to monitor your emotional state when you discuss a sensitive topic with your loved one. Call a timeout when you sense yourself reaching reactive mode — this gives you time to reflect and rejoin the conversation later.

### 9. Mindsight can help us with both past trauma and an uncertain present. 

We've already learned how our childhood experiences can shape our adult lives. However, even experiences from young adulthood can have an impact on our attitudes today, without us even realizing why.

Blackouts and angry outbursts due to excessive drinking or traumatizing experiences mean that some of our most painful memories are buried and forgotten. Even so, they continue to affect us on a subconscious level.

For example, one woman struggled with back pain for years. As a teenager, she had been sexually assaulted and had her back slammed against a table. She couldn't remember the assault, but her body continued to remind her of the traumatic experience.

With mindsight techniques such as the body scan, we can regain access to the memories attached to physical difficulties that we can't quite explain. As the brain focuses in on a particular body part, it activates related memories. This can bring repressed experiences to light, which in turn helps us accept and move on from trauma. The woman's back pain disappeared after she realized what had been triggering it.

Finally, mindsight is one of the best tools at your disposal when coping with everyday uncertainties. Humans evolved with the tendency to prefer predictability over precarious and threatening scenarios. However, this instinct can also feed into neurotic behaviors like obsessive-compulsive disorder.

Those suffering from OCD know how frustrating it is to have distressing urges that simply won't go away until they perform a ritual behavior. For example, someone with OCD may feel an intense need to knock on a desk three times when thinking about a relative dying, just to make sure it doesn't happen.

Thankfully, this disorder can also be addressed with mindsight. Imagine the distressing urge is a person with whom you can start a discussion. You're able to negotiate whether you really need to check five times that you locked the door, or whether twice is enough. Gradually, you'll find it easier and easier to deal with these urges.

### 10. Final summary 

The key message in this book:

**Difficult or troubling emotions aren't indelible parts of our personality; rather, they're shaped by the structure of the human brain and the experiences we have as children. Discover the power of mindsight by learning to reflect, train and regulate your emotional responses, which will help you when facing conflict with loved ones, dealing with past trauma and managing everyday uncertainty.**

**Actionable Advice** :

Worried about something? Observe it and name it to tame it!

The next time you can't stop worrying, whether it's about yourself, people you know or a bigger situation, take a step back to reflect on your thoughts. You could even try a body scan! Try to pinpoint the real source of your worries, and dissect them to learn more. By acknowledging and accepting your nervous feelings, you'll be able to manage them with ease.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Flourish_** **by Martin E.P. Seligman**

_Flourish_ (2012) reveals how optimism, motivation and character have the power to help us lead better lives. Based on extensive academic research and complemented by interactive exercises, these blinks introduce a new theory about what it means to live a good life.
---

### Daniel Siegel

Dr. Daniel Siegel is a clinical professor of psychiatry at the UCLA School of Medicine and received his medical degree from Harvard University. He is the author of numerous scientific articles, has contributed to several books and also wrote [_The Developing Mind_](<http://drdansiegel.com/?page=books&sub=the_developing_mind>), a book that gained international acclaim.

