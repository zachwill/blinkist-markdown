---
id: 53fc68c137316200081f0000
slug: mindwise-en
published_date: 2014-08-26T00:00:00.000+00:00
author: Nicholas Epley
title: Mindwise
subtitle: How We Understand What Others Think, Believe, Feel and Want
main_color: 919191
text_color: 5A5A5A
---

# Mindwise

_How We Understand What Others Think, Believe, Feel and Want_

**Nicholas Epley**

In _Mindwise,_ author Nicholas Epley looks at our ability to read the minds of other people, arguing that we believe ourselves to be far more adept at "mind reading" than we actually are. He reveals the common mistakes we make when trying to figure out what other people feel or want, and provides an entirely new perspective on how to handle both your own stereotypes and those of other people.

---
### 1. What’s in it for me? Learn how other people think by getting to know your own mind better. 

We're all mind readers. Each of us has the ability to divine the desires, wishes, thoughts and feelings of other people.

But while you might believe that understanding others — your friends and family in particular — isn't that difficult, the fact is that we all make mistakes when it comes to reading each other's minds.

In _Mindwise_, you'll explore the surprising variety of common errors that we all make in our attempts to understand one another. You'll discover the fascinating reasons behind such misunderstandings, including the stereotypes we hold, which have a major impact on our lives — and even our lifespan.

To become a more effective and more accurate "mind reader," the following blinks present a compelling case for the need to first understand our own minds. Learning how your own mind works will increase your ability to be more patient in your discussions and interactions with others, and enable you to see that even though you believe your opinions to be correct, you may not always be right.

In these blinks, you'll also discover

  * why it's advantageous to have a positive view on aging;

  * why tripping in public is not as embarrassing as you think it is; and

  * how talking to your car makes you reluctant to sell it.

### 2. We are often too confident that we understand our own minds. 

Are you certain that you know what your partner is thinking when you see her frowning at you?

You might think you do, but the fact is, you can only guess. But at least you know everything about your own thoughts, right?

Wrong! Unfortunately, we don't even have access to the mental processes that construct our _own_ behavior.

Even though we think we know ourselves well, the truth is that we're only aware of the "final product" of our thoughts. This is because the majority of our mental processes occur unconsciously, beyond our direct control.

The mind functions by making associations, meaning that two thoughts or behaviors that were previously connected can trigger one another.

For example: when you show a person the word "me" and then ask him to fill in the last two letters of the word "go_ _," he will write "good," rather than "goal."

Why? Because he associates "me" with "good."

Yet because of such associations, we may come up with an inaccurate self image. The connection between "me" and "good" may seem logical as you believe you are a good person; but really, it's just that your brain simply associates the two words automatically.

Thus because we cannot access our thought processes completely, we then construct stories to make sense of our behavior.

In one study, participants were shown photos of two separate people and asked to choose the one they found more attractive.

Later, the same participants were handed a photo and asked to explain why they'd picked it. However, the photo was not the one they'd originally chosen; it was the other one, of the "unattractive" person.

Surprisingly, only 27 percent realized that the picture they were given was _not_ the one they'd chosen. Even more so, those who _didn't_ realize the error then offered a compelling explanation for choosing the photo they were shown.

People create explanations for their own behavior in the same way as they try to read the minds of others: they observe their external _behavior_ and then try to come up with a suitable explanation.

### 3. We often think we know more about others than we actually do. 

As we've seen, we don't really know what's going on in our own minds. What chance then do we have of figuring out the minds of others?

Very little, it turns out. For instance, it's very difficult to know what others think of you.

Of course, we're not completely unaware when it comes to knowing our own reputation. This is because we are quite adept at guessing whether a group of people _in_ _general_ likes us. For example, if you work in an office, it's possible to gauge if your coworkers, as a group, like you or not.

Yet when it comes to figuring out an individual's opinion, we are stranded: you simply cannot guess exactly how a single person feels about you. So while you might be confident in your knowledge that you're well-liked at the office, it's almost impossible to tell if that guy from IT really likes you, or if he's just pretending.

But what about people close to us, such as a spouse or a sibling? Surely we have a better chance of reading their thoughts correctly than those of coworkers or strangers?

Yes — but the odds of reading their thoughts correctly really aren't _much_ better. Of course, this doesn't stop us from believing that we know these people far better than we actually do. The fact is that learning more about a person does not give you a greater ability to accurately read their mind; it merely creates the _illusion_ that you can.

In one study, participants were shown video clips of people who were either lying or telling the truth about being HIV-negative. They were then instructed to determine which subjects were lying. The results showed that they might as well have just taken a wild guess: participants correctly identified the liars 52 percent of the time.

Moreover, after receiving some extra information about the people in the video clips, participants became far more confident in their predictions — yet the rate of correct answers did not increase.

### 4. We like to think of objects having a mind, but sometimes perceive other people as mindless. 

Which is more human: your car or a homeless person? Although the answer might seem obvious, your brain might disagree.

Unfortunately, sometimes underestimating the minds of other people can lead to discrimination.

There's a region in the brain called the medial prefrontal cortex (MPFC) that's engaged whenever we're thinking about other people.

In general, the MPFC is most active when thinking about people close to us, because we consider these individuals to be as human as ourselves. However, the MPFC is far less engaged when thinking about people with whom we can't really identify.

In one study, American college students underwent an MRI scan while being shown photographs of their friends, strangers and homeless people.

Can you guess which photos triggered the MPFC the least?

The answer: the homeless people.

As the students in the study share little in common with homeless people, they couldn't relate to them. And as the MPFC was not stimulated by the images, the students thus struggled to see the homeless as mindful humans.

At the same time, however, we sometimes ascribe human qualities to inanimate objects.

In another study, car owners were asked questions about their vehicles. Most of these questions were standard, yet one asked owners to describe their car's personality — how irresponsible or creative the car is, for example. The people who attributed such human qualities to their car were less likely to consider selling it than those asked about the car's basic mechanical qualities.

Considering an object as mindful can thus affect the way you treat it or feel about it. If we see ourselves as sharing similar human characteristics with a car, we can therefore relate to it!

### 5. When approaching a problem, you start from your own egocentric point of view. 

One of the biggest obstacles in understanding other people is that we see things only from our own perspective.

It's crucial to remember that the way that you perceive a situation is unique. It follows logically that other people may have a totally different impression than your own.

Imagine you're in a crowded soccer stadium, supporting Team A. Suddenly, you see Team A's captain being fouled. All eyes are on the referee, who ultimately decides not to give Team A a free kick.

You are outraged, as are all the Team A fans, who make up half of the stadium.

The other half? They're happy as they support Team B, as well as the referee's decision.

Everyone in the stadium is watching the same match, yet opinions and attitudes differ so wildly because a single situation can yield many different interpretations.

You can use this knowledge to your own advantage the next time you're engaged in a heated argument. Take a moment to step back and try to understand how the situation looks from their perspective.

Yet why is it that we often think in such an egocentric way? The answer is simple: doing so makes us feel more important than we actually are.

For example, we're generally afraid of doing something which embarrasses us, as we believe people will judge us at that particular moment.

Think of the last time you were embarrassed. Perhaps you tripped and fell in front of all your friends? If you can recall this moment vividly, you'll probably also believe that everyone who was there remembers it too. Perhaps behind your back your friends remember you as "that guy who fell on his face."

The fact is that other people don't remember events in _your_ life as well as you do. Most of your friends will have forgotten all about the fall, and the ones who do remember will probably recall only the vaguest of details.

### 6. Snap judgments based on stereotypes muddle our thinking. 

You're walking past a man with no shoes and greasy hair and who smells like rotten garbage. What do you think about him?

Most likely, you'll make a snap judgment and place him in a specific category — homeless — without any further reflection. We all categorize people in this way on a daily basis.

But why do we have stereotypes? And how do we use them to learn about others?

Looking for similarities between people is simply far less interesting than is searching for differences. This is where stereotypes are born: we want proof that certain groups are entirely different from us.

However, although we use stereotypes to help us explain others' behavior, more frequently these stereotypes deceive us into finding differences where none exist.

Gender differences give rise to many stereotypes. These lead us to seeing men and women as distinct types of people, with very different mindsets (such as men as unemotional, women as over-emotional). In accepting this, however, we overlook the similarities between genders — and, of course, the fact that we share more in common than not.

But even though stereotypes are usually misguided, they hold incredible power. Often, stereotypes are _self-fulfilling_.

One reason that people seem to act in accordance with stereotypes is that stereotypes can often influence our actions.

In one study, researchers studied the effects of stereotypical thinking by asking volunteers about their thoughts on aging. The volunteers were aged 18-39, and their health histories were documented by researchers for 38 years.

Interestingly, more than 50 percent of participants who held strong negative stereotypes about aging ended up actually suffering from major heart disease, in contrast to just 18 percent of participants who held strong positive stereotypes.

Another study showed that people who held positive stereotypes on aging live an average of 7.5 years longer than those who maintain negative stereotypes.

Now that we know the reasons why we haven't been able to read others' minds correctly, the next blinks will show us what we can do to change this.

> _"Our stereotypes about group differences could be precisely right, but our explanations of those differences could be profoundly wrong."_

### 7. Body language doesn’t reveal much about the minds of other people. 

How can you tell if someone's angry with you? You might look at their body language or facial expression. A hard glare and clenched fists would be a pretty good evidence that you're in trouble!

Yet in fact, you express your emotions mainly through your _voice_.

Participants in a study watched short video clips of people who were discussing either a sad experience or a happy one. To understand how emotional signals are conveyed, the researchers had some participants watch the clip without sound, and others listen to the clip without video.

Interestingly, those who only _listened_ rated the storytellers' emotions more accurately than those who only watched the clip.

So while it's true that body language can reveal certain emotions and feelings, _listening_ to someone is a far more accurate way of guessing their state of mind.

One reason for this is that we often struggle to express our emotions through body language.

We all know whether we're feeling happy or sad. For this reason, most people believe that their emotions are equally obvious to other people. But this isn't true, as our facial expressions reveal little about our emotions.

In another study, participants were instructed to look at photographs of emotionally rousing things, and then to express their feelings openly or attempt to conceal them. Surprisingly, the researchers couldn't detect any difference between the concealed and unconcealed emotions.

Furthermore, not being aware that others may not be able to read our emotions can cause misunderstandings — especially in romantic relationships.

If you're angry because your partner has forgotten your anniversary and think that your disappointment is "written all over your face," you might be even more crushed when your partner still fails to read your emotional signals and respond appropriately.

> _"Bodies and faces can reveal thoughts and emotions, but the amount they communicate seems surprisingly limited..."_

### 8. Getting someone’s opinion directly is better than trying to assume someone’s perspective. 

Your mom's birthday is coming up, and you want to choose a great gift for her — a present that shows how well you know her.

How do you determine what she'd want to receive?

While it may seem reasonable to try and take another's perspective — to imagine how the world looks from their point of view — doing this to figure out what that person might want will probably fail.

It is simply impossible to know how others feel with any degree of certainty.

As you're shopping for your mom's gift, you ask yourself: what would she _really_ like? To answer this, you try to put yourself in her shoes. Perhaps she has recently taken up oil painting, so you consider, "If I were her, what would be useful to my painting?"

Unfortunately, this approach is ineffective. You have no idea whether she's happy with her current painting equipment. And even if you do know, you can't avoid thinking about the gifts that _you_, not she, would like.

So if it's impossible to get inside another person's head, what can you do to figure out their perspective?

Simple: ask their opinion!

You could ask your mom directly what she would like for her birthday. While this might seem a boring approach, it's actually the best one, given how difficult it is to guess someone's wishes accurately.

Yet even when asking outright for someone else's opinion, there's still room for error. To ensure you're getting the true perspective, it's crucial to understand your partner completely. Nothing is worse than asking for information and then misinterpreting it because of a lack of understanding.

For example, you learn that your mom wants a new book for her birthday. But which book should you choose? A love story? A murder-mystery? Or perhaps her current reading list consists solely of non-fiction?

The room for error here is massive. So, to ensure that your mom's birthday present is perfect, keep asking her questions until you understand precisely what she wants.

> _"Getting a person's perspective is the way you understand them accurately and the way to solve their problems most effectively."_

### 9. Final summary 

The key message in this book:

**We** **think** **that** **we** **know** **the** **minds** **of** **others** **better** **than** **we** **actually** **do.** **To** **learn** **how** **to** **become** **more** **precise** **"mind** **readers,"** **we** **must** **first** **learn** **more** **about** **our** **own** **minds** **and** **the** **many** **flaws** **and** **prejudices** **we** **hold.** **Even** **then,** **reading** **minds** **remains** **difficult,** **so** **the** **best** **way** **to** **learn** **about** **the** **thoughts** **and** **feelings** **of** **the** **people** **around** **you** **is** **through** **_direct_** **_communication_** **.**

Actionable advice:

**Put** **your** **embarrassment** **in** **perspective.**

The next time something embarrassing happens to you, remember that others are simply not as interested in you as you might believe. Indeed, they have their own embarrassing actions to grapple with! Remember that, when it comes to recalling events from your life, your own memory is much better than that of others, who simply won't recall (at least as vividly as you do) the event that caused you such embarrassment.

**Suggested** **further** **reading:** **_Spy_** **_the_** **_Lie_** **by** **Philip** **Houston**

_Spy_ _the_ _Lie_ reveals the typical strategies that liars use to try to deceive you, as well as the tools to help you detect them. These blinks draw on field-tested methods for lie detection developed by former CIA officers, which helps to spot the signs of a lie and ask the right questions to uncover the truth.
---

### Nicholas Epley

Nicholas Epley is a professor of behavioral science at the University of Chicago Booth School of Business. In 2001, he earned his doctorate in psychology from Cornell University, and later became an assistant professor at Harvard University. His research includes studying intuitive human judgment and social cognition.

