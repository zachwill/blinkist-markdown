---
id: 564a068266343100077f0000
slug: mini-habits-en
published_date: 2015-11-19T00:00:00.000+00:00
author: Stephen Guise
title: Mini Habits
subtitle: Smaller Habits, Bigger Results
main_color: 9ABE42
text_color: 5D7328
---

# Mini Habits

_Smaller Habits, Bigger Results_

**Stephen Guise**

_Mini Habits_ (2013) explains the logic behind an innovative approach to achieving your goals. Motivation and ambition aren't necessarily what will drive you to success; rather, it's your small day-to-day habits that will really get you on the right track. Learn how to harness their power with these blinks.

---
### 1. What’s in it for me? Learn how to create – and stick to – good habits. 

Do you check all of your social media accounts early in the morning without even thinking? Or have you gotten into the habit of buying yourself a sweet pastry every morning for the last week, and worry about a daily donut becoming part of your routine?

Habits are a central aspect of our lives; we all live with habits to some extent. Some of them are good, some are bad and some can get very bad — like donuts.

With _Mini Habits_, the author doesn't just provide some great tips and tricks on how to get bad habits under control and create more of the good ones; he also offers some useful knowledge on the very concept of habits — how they emerge, how they change and how they can help you achieve your goals. 

In these blinks, you'll discover

  * a method for building good habits, such as getting up earlier in the morning;

  * why you tend to eat more sweets when you are stressed out; and

  * which smartphone apps can help you maintain your good habits.

### 2. Most of our lives are governed by our habits. 

Do you know that feeling of being on autopilot? When we're taking our morning showers or brushing our teeth, we hardly need to think about what we're doing at all. Why? Because we've turned these necessary daily activities into _habits_ — that is, tasks we've done so often that we can do them automatically. 

In fact, a lot of our daily activities are governed by habit. According to a study from Duke University, 45 percent of our behaviors are habitual.

We're especially prone to falling into habitual behaviors when we're stressed.

This was demonstrated in another study at UCLA that uncovered our tendency to revert to habitual behavior when under pressure, tired or overwhelmed. Unfortunately, this happens whether the habits are good or bad for us.

Why does stress affect us in this way? Well, stress is often the result of being unable to make certain decisions. 

Habits, however, are something we don't have to make decisions about. They are pre-programmed into our daily life. When stressed and incapable of making a decision, we resort to our habits. If you often find yourself chatting online or eating donuts when stressed, chances are that they're habitual behaviors. Perhaps you wish they weren't!

If so, there's good news: bad habits can be changed. After all, habits are nothing more than neural pathways in the brain. They get thicker the more they're used, and deteriorate when neglected. You create your own habits simply by repeating activities until they get easier and easier. 

So, if you want to start getting up earlier in the morning, the first few weeks will be a struggle because those neural pathways are still weak. But soon enough, your brain will strengthen the connection between waking up and getting straight out of bed, while the habit of rolling over and going back to sleep becomes weaker and weaker. 

Before you know it, your neural pathways will be doing all the work for you, leaving you bright-eyed and ready to start the day.

### 3. Our brains feature a powerful habit-forming system. 

Imagine having to make an informed decision about every single product you buy at the supermarket. Doing the groceries would take forever! That's where habits come into the equation. 

One section of our brain, the _basal ganglia_, is tasked with programming our repetitive behaviors so that we hardly think about them. Consider the programmed behavior of choosing an ice cream flavor. Even at a fancy gelato shop with so many tempting flavors, we often just choose something familiar, like vanilla. 

Why? Well, thanks to our basal ganglia, it's something we're used to and thus something we automatically tend towards. The more we repeat a habit, the record of it in the basal ganglia becomes stronger and stronger. 

For instance, after consistently repeating the action of turning on your computer and logging into your Facebook account right away, your brain will eventually just go on autopilot — that's how we can end up on Facebook without even realizing it!

The basal ganglia is often so strong that it overpowers the part of our brain responsible for conscious, reasonable decision making: the _prefrontal cortex._ Unlike the basal ganglia, this part of our brain takes long-term consequences of actions, as well as abstract concepts like morality, into account. 

But the prefrontal cortex has one major flaw: it gets tired fast. 

Making the right decisions is in fact a huge energy drain. While we might find it easy enough to resist that tub of ice cream in the freezer for most hours of the day, chances are that your prefrontal cortex will give up once it's gotten tired. At this point, your basal ganglia will take over and you'll suddenly find yourself holding a big bowl of cookies and cream again!

### 4. Willpower, not motivation, is the best tool for creating good habits. 

Motivation is, of course, a good thing — but we are usually most motivated to do the things we really like, such as taking a holiday. When we need to get the spring cleaning done, on the other hand, motivation is nowhere to be found. 

Motivation has its pitfalls, especially because it fluctuates according to how we feel. On a morning when you feel wonderful, it's easy to do a set of 20 push-ups before breakfast — but it's not so easy when you've got a hangover! 

In addition, motivation actually decreases the more we do something because it gets boring. Imagine if you relied on motivation alone to brush your teeth every day — you'd end up with a mouth full of cavities!

Basically, motivation isn't enough to help you build up positive habits. Luckily, there's another tool that is far better suited to help us create change in our lives. Instead of weakening with repetition, this tool can only get stronger. So what is it?

Willpower. 

Willpower's benefits are well known to psychologists. One professor even had his students exercise their willpower for two weeks in order to improve their usual posture while sitting in class. Not only did they sit up straighter in their lectures, they also showed greater levels of self-control in other areas of their lives!

Every time you create a new positive habit, such as meditating each day, you flex your own willpower muscle. You can use this muscle for other activities too, such as cooking fresh food everyday, staying in touch with your family and anything else you'd like to achieve. Willpower, unlike motivation, is dependable. You can build it up, and once you do, you can rely on it.

### 5. Mini habits are the most efficient investment for your limited willpower. 

Sure, you can try to make it a habit to do 100 push-ups every morning. But it's pretty likely that your willpower will buckle after you hit 20, and it won't be long before you open the cookie jar again. Willpower is fantastic, but it doesn't seem that way when we're first exercising it. So how can we strengthen our willpower when starting from scratch?

The solution here is mini habits. You can avoid the loss of willpower with minor, almost ridiculously tiny goals. After all, the main threats to our willpower are effort, perceived difficulty and fatigue — why not choose a target that only takes a little effort? 

Picking an easy goal eliminates any perceptions of difficulty and is hardly daunting enough to make you feel fatigued. In other words, it's a comprehensive cure for weak willpower.

Mini habits also get you moving, and once you are in motion, you'll need less willpower to continue. As Newton's first law states, an object in motion will not change its velocity unless an external force acts upon it. 

In other words, the greatest hurdle we usually face is the first one: going from inertia to mobility. With a mini habit that helps you start small, you can be sure to start smoothly. In fact, you might even find out that you can achieve more than you set out to do! 

For instance, you might decide to do five push-ups, even though you only set a goal of doing one. Sure, it doesn't look like much on paper, but you'll be surprised at how pleased you feel with yourself!

### 6. Mini habits have a whole host of additional benefits. 

Remember that person you always wanted to date but never had the courage to ask out? Try taking a step in their direction — and then another. Repeat this a few times. Soon enough, you'll find yourself facing the object of your affection, who will probably ask you why it took you so long to walk across the dance floor. See? You're already talking. 

By doing things that you were never brave enough to do before, you can see how mini habits boost your self-esteem. The ability to believe in ourselves unfortunately gets gradually weakened through years of unreasonably high expectations from our parents, teachers and, eventually, ourselves. 

Thankfully, mini habits provide you with the unique opportunity to experience success — rather than failure — several times a day. When you set goals that you can easily fulfill, you'll feel great, no matter how minor your achievements were. 

Compare this to facing the fact that you haven't still achieved your one big goal every day. It's easy to see which approach will keep you feeling positive! Don't aim to become a famous pop star tomorrow; instead, just practice the piano for five minutes each day. 

Mini habits are also great at making you feel like you're in control. Humans hate the feeling of being out of control, or worse, being controlled by others. We like to make our own decisions, which is what makes us happiest.

A survey in Denmark demonstrated this, revealing that up to 90 percent of all employees are much happier when they have control over their own work and can make executive decisions. 

A huge goal, such as devoting yourself entirely to the poor and those in need, can suck up your time and energy until you come to resent the goal itself. In contrast, a mini habit, like giving a coin to every person who asks you for a donation, is simple, yet creates a positive personal interaction and leaves you free to live your life, all while starting you on the path to being a more generous person.

### 7. Plan and develop your mini habits carefully. 

Now that we've discovered the benefits of mini habits, it's time for you to create your own action plan. So where should you start?

First, choose your habits wisely. You can start with a list of habits that you'd like to have at some point in your life. You might want to practice a new language, read more, improve your math skills or become fitter. 

Next, ask yourself why these habits are appealing to you; this is to ensure that you've got the right motivations. Why, for example, might you want to pick up more languages? If you're passionate about travel and different cultures, then you've got pretty good reasons.

But if you're simply determined to impress colleagues and friends, carefully consider whether you might just be under external social pressure. Don't let this influence your habits! Habits should only be informed by what you really want to achieve.

With a list of motivated habits established, it's time to create mini habits to match. If you want to learn Spanish, make it a mini habit to learn one Spanish word a day. If your mini habit is so small that it sounds silly to you, that's great! That's exactly how they should be. 

Mini habits shouldn't be daunting at all. They should be so small that you can incorporate several of them into your daily routine, starting out with two or three per day. After you've determined your mini habits, define and write down your _habit cues._

Habit cues are signals that remind you it's time to perform your mini habit. Say you want to do a yoga pose before breakfast; the time of day, or the fact that you're getting hungry, could be your cues to engage with your mini habit.

### 8. Monitor your progress and don’t forget to reward yourself. 

Mini habits are easy to achieve, so you shouldn't just aim for a 95 percent completion rate — go straight for 100 percent. The simplest way you can avoid skipping or switching your mini habit is by _recording_ what you do, so be sure to write everything down.

A psychological study conducted in 2013 demonstrated that all thoughts have a stronger presence in your mind when you write them down. New apps such as _Lift_ or _Habit Streak Plan_ are perfect for helping you document your habits, or you can go for the old-school approach and use a big calendar to monitor your progress.

Whichever method you choose, it should be something you look at every day. This way, you can keep reminding yourself to complete your habits, maintain your progress and even build up more positive habits along the way. 

Soon enough, you'll be powering ahead. You might even find that you achieve far more with your habit than you originally planned. Say you go well beyond your target of writing 50 words and write 500 — these bursts of energy are great, but don't let them confuse you!

Suddenly switching your habit to writing 500 words won't do you any good, and you'll find it hard to maintain, even if it seemed so easy the first time. If you manage to exceed your goals, pat yourself on the back, but remember to see that as a bonus, and not a signal to push yourself harder. 

But what if writing just 50 words feels boring? Well, that's good! This signals that your mini habit is a bona fide habit. It's something you do automatically, without any resistance to complete it. This is something to celebrate, so reward yourself! 

Remember, it's not just the goal that your habits lead to that's important. Developing a daily routine that is full of helpful rituals is something you can be proud of too.

### 9. Final summary 

The key message in this book:

**Instead of trying to motivate yourself to achieve daunting goals, take things one step at a time. By building up a routine of positive mini habits, you'll give yourself the chance to enjoy small successes every day, while making real progress toward your true aspirations.**

Actionable advice: 

**Get inspired!**

Want to create your own mini habits but feeling stuck for ideas? Why not check out minihabits.com for some inspiration. Once you've selected two or three habits, don't let yourself forget about them — stick a reminder on the fridge or above your bed. Then, perform those habits for a week and watch as you become more energized! 

**Suggested further reading: _Manage your day-to-day_** ** __****by Jocelyn K. Glei**

_Manage Your Day-To-Day_ is a collection of ideas, wisdom and tips from well-known creative people. It offers readers valuable insights on how to develop effective work routines, stay focused and unleash their creativity.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Stephen Guise

Stephen Guise is a writer, blogger and researcher who specializes in personal growth and fun learning. His latest book is called _How to Be an Imperfectionist_.

