---
id: 55ffed69615cd1000900000e
slug: minimalist-parenting-en
published_date: 2015-09-21T00:00:00.000+00:00
author: Christine Koh and Asha Dornfest
title: Minimalist Parenting
subtitle: Enjoy Modern Family Life More by Doing Less
main_color: EDC62F
text_color: 6E5C16
---

# Minimalist Parenting

_Enjoy Modern Family Life More by Doing Less_

**Christine Koh and Asha Dornfest**

_Minimalist Parenting_ (2013) presents a new conception of parenting: fewer rules and more listening to your gut. Rather than following the herd's latest parenting trends, Christine Koh and Asha Dornfest present an approach to raising a family that puts your personal and family values at the heart of your life as a parent.

---
### 1. What’s in it for me? Discover a new and better way of parenting by listening to yourself. 

As anyone who's been to a store for children or babies lately knows, modern-day parenting can be a jungle. You find yourself staring down row after row of the latest baby equipment, toys, gadgets and parenting books. What to choose? And when did strollers get so expensive? Add to this the countless blogs and online parenting forums, and you might start asking yourself if it really has to be so complicated and expensive to raise kids. Isn't there a simpler way? Enter _Minimalist Parenting._

Minimalist parenting is about trusting your own values and preferences once again. As these blinks will show you, becoming a parent doesn't have to mean that your old life is gone forever; keeping your dreams and passions alive is not only good for you and your relationship, it's also good for your children. 

With a few tweaks and some reprioritizing, you can make your life as a parent much more fun. These blinks will show you how.

In these blinks, you'll learn

  * why being selfish is good for your kids;

  * why looking for the perfect school for your children is a waste of time; and

  * how minimalist parenting will help you have more money left over for fun stuff.

### 2. Step off the modern parenting treadmill and start following your own values and preferences. 

If you are a parent, you're familiar with the exhausting modern parenting treadmill of expectations, choices and pressure. Luckily, there is a way to stop the madness!

Chances are you already have everything you need for a great family life: time, love and probably enough money for basic necessities. The stress and hurdles come from an abundance of choice, stuff, obligations and guilt.

Choice is wonderful, but the incredible range of choices today, from philosophies to gadgets, is overwhelming. 

Minimalist parenting, however, is not about harsh rules or living like a monk — it's about giving yourself the permission to reject impossible expectations and so-called ideal ways of parenting. It's about living a joyful life that fits your personal values, not those of others. Parenting this way makes your kids stronger and allows them to grow.

Here are a few steps to get started with minimalist parenting:

Begin with a fresh mindset that challenges the modern parenting demand of "more." One way to do this is to stop comparing yourself to other parents. Even if their children have more toys, get better grades and do more activities, their style of parenting is still not yours. Everyone has to develop their own unique path.

To do so, you have to know yourself and your family. Your family might want different things than you do; if you crave adventure and travel but your family doesn't, you shouldn't focus everything on traveling the world.

Next, drop the idea of being perfect. Instead, try to improve bit by bit. Bear in mind that you're not alone in your struggles and that other parents share the same worries, so talk about your experiences with them.

Finally, remember that more doesn't necessarily mean safer or better. Your child's future isn't determined by his or her first violin! Real importance lies in being present, minimizing and being happy — with yourself.

### 3. Minimalist parenting is ultimately about you. 

There's a wealth of great advice out there, but it doesn't mean that you have to abide by everything you hear and read. In fact, it would be helpful to do quite the opposite.

You must learn to trust yourself, your interests and decisions. Enjoying your own interests and wanting things for yourself is healthy, and should be an important part of day-to-day life.

You need to weigh your own decisions against advice in books or from other parents. For instance, you may be told by someone else that now is the right time to have your child start sleeping in his own bed, only for him to cry for weeks on end. In this case, you can deduce that the time was not right in your situation after all.

It's natural to feel inclined to follow the herd, but your life is unique to you, so you have to discover your own path.

Ultimately, you have to put yourself into the spotlight. _You_ are your main priority. Only when you treat yourself well do you allow this goodness to shine through to your relationships with your partner, your children, your friends and your community.

Don't fall into the trap of seeing self-care as selfish — show your children that parenting is not about losing yourself.

Self-care is a critical part of living a healthy life. This doesn't mean abandoning everyone for weeks of solitude — you can just start small. For example, allow yourself to care about how you look. Even if you work at home, get out of your old yoga pants occasionally and put on something that makes you feel great. 

Allow time for your partner, too. Part of caring for yourself is nurturing this relationship. Rather than focusing on getting by and managing family logistics, start making room for "pointless" fun again. 

Minimalist parenting will give you the time and space to remember who you are and who you want to become. But how does it work in practice?

### 4. Time is limited, so it has to be organized, prioritized and managed. 

At this point, minimalist parenting may sound too good to be true — how do you find more time for yourself while still handling everything else? The answer is time management! 

Time is a limited resource. But rather than asking "How do I fit everything in?", ask "What is _most_ important to fit in?"

To figure this out, you have to know your family's pace and allow some flexibility. Let's say you're an early riser; you could try to work in the morning and rest in the afternoon. Likewise, if your teenage child is a night owl, don't force him or her to study early in the day. Let them stay up if they're using that time to study.

Make sure you also schedule wisely. Set aside time for yourself during your _golden hours_, the times when your energy is at its peak, then set aside family time while also allowing for transition time in between.

To make time management even easier, you can prioritize your activities, which will make them easier to achieve.

Tackle hard things first so you have enough energy for them, and leave time for serendipitous encounters, time with nothing to do and time that is open for your children. In addition, allow yourself to decline activities that you and your family don't want to do. Don't let obligations eat away at your time!

For example, if you have a large family, you don't have to go to _every_ family gathering. If you don't get on particularly well with your great uncle, say happy birthday if you see him — but don't waste hours being bored at his birthday party, just out of familial duty.

Lastly, giving your kids chores as soon as possible also helps with time management and makes them feel more like valuable family members. 

So, now that you have time, the next step is to get some space.

### 5. Having less stuff will help you be a better parent. 

Do you feel overwhelmed by how much stuff you have as soon as you get home? Well, bring out the garbage bags, it's time to declutter!

Minimalist parenting isn't about deprivation — you can still enjoy buying things. But there is so much that we hold onto purely for emotional reasons. The key is to focus on worth, and only keep things if you absolutely love them.

It's so easy to accumulate stuff when there is space to store it. But how about leaving empty spaces empty, or even moving to a smaller place? Try and be more mindful when you shop and ask yourself, "Do I really need this?"

This might limit new things coming in, but you may still have too much stuff. So, start decluttering. For many, the mere mention of decluttering feels overwhelming, but you can start small. For each item, as a rule of thumb, ask yourself whether you would really pay to replace it if you lost it in a fire.

One great thing about less stuff is that it saves you money. Still, assuming you can pay for the basics like food, accommodation and some cultural and leisure activities, more money doesn't necessarily equal a better life.

Money can mean more stress due to the options it offers and heightened expectations. But, if you find yourself with more money left over as a result of buying fewer things, you could choose to work less and instead have more time for yourself and your family.

In minimalist parenting, the question changes from "Do I have enough money?" to "Do I truly need it, and not just because others have it?"

Tracking and planning how you spend and save helps take the stress out of money, but you should also streamline your financial setup. Do you really need more than one credit card? You can also educate your kids about how to manage money, helping them understand where it comes from and how much things cost.

### 6. Education is not only about school, but schooling gets easier when approached differently. 

Education is always a hot topic and generates a lot of stress for parents: you want your child to be in the best school, have stimulating toys, be able to read as early as possible, play an instrument before school and speak several languages. It's time to drop these expectations.

Minimalist parenting embraces continual learning, and once you realize that your child is learning all the time, you'll no longer be under such pressure to find the "perfect" school.

Part of ongoing education means cultivating curiosity in your children. You can do this by listening to different kinds of music or bringing home new and exciting food, for instance. You can go grocery shopping together and let your kids pick what they want to eat. Encourage responsibility and independence, and make them feel like their decisions and knowledge are valued. 

Remember, everyday learning is vital and the home environment is at least as important as your child's school curriculum. 

Schools are central educational environments, but they're not everything; they must be compatible with your actual day-to-day lives. Long commutes to school, high living costs and long distances between friends, for instance, need to be considered when choosing schools. 

There are few additional strategies that will make the school year easier for your whole family.

First off, homework belongs to your children, so try to eliminate yourself from the task.

Next, get involved with teachers and the school community. School is a team effort and all parents are in it together. Get to know classmates or volunteer at school, and you'll find that school-related matters become much more enjoyable.

Remember, you shouldn't try to be perfect — every small step or effort is great. Don't feel like a failure just because you can't make breakfast everyday or volunteer at school. Go with what makes you feel right.

Now that you have school figured out, what about time outside of school?

### 7. Playtime and extracurricular activities are both very important but should not be planned too strictly. 

With so many books on which extracurricular and playtime activities are best for your child, how do you choose? Don't!

Children need playtime, but instead of following a strict plan, what matters most is to put playtime in your schedule, and make it simple and good fun.

You don't have to play with your kids every second — you should allow for independent play. This requires fewer toys, reduces clutter and makes room for more creativity. Even chores can have a playful element to them and you don't necessarily have to distinguish them from play.

So what about all this gadgetry we're surrounded by? 

Electronic toys are often considered a mixed blessing. Some say they harm creativity, while others argue they help children develop. But you don't have to be firmly in either camp. How about finding a balance that fits with your family? If your children get a kick out of some gadgets, see if you can get them to develop their own games with them.

Remember, though, that electronics are only one kind of activity your child can engage in. Going outdoors to forests and beaches or using wooden toys are also important. These activities can stimulate your child and let them learn how to play without store-bought toys.

Striking the right balance among extracurricular activities is a process and it's best to keep an open mind. There is no universally perfect approach, so go with your child's interests. It also helps to question your motivation for extracurricular activities; for example, is it only you who wants your child to play the violin?

Only if you allow your child space to get in touch with their feelings will they uncover their own passions and talents. This does not mean you shouldn't encourage them if their motivation is lacking, but always listen to them and question why you push them.

### 8. Shared meals are a great way for your family to connect. 

Do you get stressed even thinking about your next vacation, the next birthday or even tomorrow's lunch? With a bit of minimalist parenting, you don't have to!

Preparing a varied, nutritious and tasty meal can feel like a huge effort, but you needn't be a gourmet chef to pull it off — just keep it simple.

Start by planning and shopping ahead. If you find a tasty recipe for the whole family, make it regularly. You can also get your kids involved by taking them grocery shopping. This can be stressful, but it's also a wonderful opportunity to educate them. 

When you go shopping, look for local vegetable producers. This often means less driving and you can show your kids where vegetables come from, which will keep them interested in eating them!

When it comes to meals, simplify preparation and presentation, and instead focus on the conversation and connection that happens at the dinner table.

Employ some basic rules and give everyone a role to carry out, which will bring about a sense of family connectedness. It's also a good idea to focus on weekend meals, when it's easier to all eat together. And remember, if you don't like cooking, don't force yourself to concoct complicated meals!

When you get into the swing of minimalist parenting, you'll find yourself spending less money. You will no longer compare yourself to other parents, and can therefore stop buying things just to keep pace with them. Use this saved money to create lasting memories for yourself and your family: take a trip, go to a concert or do whatever else feels right for you.

Similarly, when traveling, be wary of excessive scheduling and stay open to new opportunities. Travel is a fantastic way to open you and your children up to experiences that aren't a part of your everyday routines.

### 9. Final summary 

The key message in this book:

**Having children doesn't mean you and your interests have to come last on the family priority list. Rather than looking for new ideas on parenting, listen to your gut and take stock of you and your family's values. Simplifying in this way will create more space and time for you to develop together and as individuals.**

Actionable advice:

**Get your kids outside to enhance creativity and independence.**

The big city, no time, no space — there are plenty of reasons to want to keep your kids indoors. But by getting your children basic outdoor equipment or teaching them games like hide and seek, your kids will have a great time outside, regardless of the weather or the limited space. Reduce your anxiety by having an open-door policy with other parents so playing outdoors can be more flexible and your children can always check in with a neighbor if they need to. If your kids are very young, find something to do outside with them without playing with them, such as reading or gardening. This will help your children be comfortable outside and learn to play safely. Children who play outside are healthier, learn to play more creatively and are more independent. 

**Suggested further reading: _The Opposite of Spoiled_ __ by Author**

_The Opposite of Spoiled_ (2015) is the essential guide to raising patient, generous children through financial education. These blinks will show you how to guide your child's development by talking to them about money, involving them in financial decisions and showing them the importance of generosity and work.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Christine Koh and Asha Dornfest

Dr. Christine Koh is a psychologist specializing in behavioral and cognitive science. Koh writes blog posts and articles on lifestyle and parenting, and is herself an excellent example of how to strike a healthy balance between being both a parent and a professional. _Minimalist Parenting_ is her first bestselling book.

With over 20 years' experience as a writer primarily in web publishing, Asha Dornfest now focuses on writing about parenting and maintaining her blog on the subject.

