---
id: 55f6de4029161a0009000060
slug: misfit-economy-en
published_date: 2015-09-16T00:00:00.000+00:00
author: Alexa Clay and Kyra Maya Phillips
title: Misfit Economy
subtitle: Lessons in Creativity from Pirates, Hackers, Gangsters and Other Informal Entrepreneurs
main_color: 9C276D
text_color: 9C276D
---

# Misfit Economy

_Lessons in Creativity from Pirates, Hackers, Gangsters and Other Informal Entrepreneurs_

**Alexa Clay and Kyra Maya Phillips**

_Misfit Economy_ (2015) sings the praises of people who break society's rules. You'll learn the strategies of such freethinkers — often working on the periphery of the mainstream economy — who succeed amid tough circumstances. Instead of shunning such "misfits," you'll instead be inspired to adopt such strategies in your own life and career.

---
### 1. What’s in it for me? Being a misfit isn’t a drawback, it’s a boon and will help you succeed. 

Could corporate types learn lessons from misfits like pirates, criminals and hackers? You bet. 

Living outside the law requires energy and ingenuity, not to mention serious organization and cooperation. Just think about the communitarian pirates of the eighteenth century who practically shut down transatlantic trade — their success was not random. 

So what can we learn from such misfits? In today's global economy, the creativity, willpower and innovative mind-sets of misfits are valuable tools, and can help you get ahead. 

Because really, it's not about being on the fringe and trying to break in, but about bringing the fringe into the center and revolutionizing society from the inside. In these blinks, you'll learn how. 

In these blinks, you'll discover

  * how a trio of German brothers built a business empire by copying; 

  * how a grade school misfit founded his own college; and

  * how a teenaged hacker started his own telecommunications company.

### 2. Are you different? Do you not fit in with “normal?” Misfit, this new world is for you! 

Misfit. The word describes a person who doesn't "fit" in, who chooses her own path contrary to others. 

Can this actually be a good thing? 

In today's rapidly evolving economy, being unique is valuable. As traditional industries are being upended by changing social and environmental conditions, misfits are needed more than ever.

Misfit David Berdish is a third-generation worker at Ford. He slowed down his career to campaign instead for ideas — unpopular at the time — that he nonetheless believed were vital for Ford's future, such as car sharing and other alternative mobility solutions. 

It was hard going at first. But eventually, Berdish garnered support for his ideas and, today, his efforts have helped the automobile industry as a whole to start addressing the future. 

Misfits are capable of dreaming big and, in turn, changing the world. This is because such outsiders have an unusual way of thinking. And while "weird" thinkers in the past were stigmatized, today companies of all stripes are seeking them out. 

For instance, German software giant SAP recently announced that it was planning to hire people diagnosed with autism disorders for positions in programming and debugging, hoping that this group's special skills and attentiveness to small details would be useful.

There's another advantage to hiring misfits — they're flexible. Modern corporations need to nimbly respond to external conditions. Employees must be creative and adaptable, and not specialized and methodical like many workers of the past. 

As a result, career paths are becoming less linear and more diverse. And misfits are especially well-suited to thrive in this type of environment. 

All in all, there are big advantages to being "different." So what can we learn from misfits?

### 3. Criminals are misfits at their core. Industriousness, adaptability and agility: these are all survival skills. 

A true misfit doesn't care about rules or conventions. Some don't even care about the law. 

In fact, criminals are prototypical misfits. And believe it or not, we can learn a lot from them!

Criminals have special talents that they use to survive, working in a tough environment characterized by brutal rivalry and boundless competition. The level of skill and energy required to succeed in the underworld, however, can also be channelled to legal business activity. 

That's the goal of Defy Ventures in New York, a program that provides ex-criminals with a business education, putting them on the path to legal entrepreneurship. 

The project has been hugely successful. Since 2010, some 72 of 115 Defy Ventures graduates have launched their own companies, earning a total of $13 million. Some 87 percent of participants are employed; and only five percent have returned to crime. 

This proves that law-abiding citizens can learn a lot from hard-working ex-cons!

Importantly, criminals are whiplash-fast at adapting to new situations and changing economic conditions. Somalia's "pirates" are proof positive of this principle.

After the government collapsed in the early 1990s, the waters off the Somali coast became dangerous for shipping and fishing vessels (many, granted, which were fishing illegally). Desperate locals began to attack these boats, seeking valuable items or kidnapping sailors in exchange for ransom. This marked the beginning of modern piracy. 

Quickly these pirate activities grew into a professionally organized, multimillion-dollar business. Pirates and their networks were nimble and fast to adapt, perpetually thwarting the efforts of shipping companies and countries trying to combat the practice. 

Sure, piracy isn't at its core a practice that should be condoned. Yet a group's ability to adapt quickly to changing circumstances can be a hugely profitable skill.

### 4. Can you do the hustle? When faced with serious obstacles, that’s when it’s time to get creative. 

Have you ever felt powerless when faced with a mountain of problems? Take heart, as we all sometimes encounter obstacles that often seem insurmountable. 

But the thing is, this is precisely when we've got to hustle — and _not_ give up. 

_Hustle_ is both an attitude and a strategy. A hustler has got passion and determination; and her strategy is all about improvisation. 

So don't worry if you lack an elaborate master plan or even basic resources. Just use whatever's available to you and be prepared to respond to whatever comes your way. 

When a DHL branch in Clinton County, Ohio, left 9,000 people unemployed, residents Mark Rembert and Taylor Stuckert started hustling. 

They built up a grassroots movement to connect the community and raise awareness for alternative business strategies. They launched campaigns to encourage people to buy from small businesses and revived the local Chamber of Commerce. 

Of course there will always be new obstacles, especially when you're trying to create something from nothing. For instance, starting any new business means tons of paperwork, and so anyone without resources will have to find work-arounds.

And that's perfectly normal. In fact, a study in the United Kingdom showed that 20 percent of small business owners had engaged at some point in informal or semi-legal activity. That might entail bartering website design skills for someone's marketing know-how. Or it might mean paying a supplier off the books. 

The report showed that such behavior wasn't about cheating the state, but was really about bypassing cumbersome regulations simply to get the business running.

### 5. Being a copycat isn’t only an effective business strategy, it also serves the greater good. 

With the boom in internet technologies, the question of copyright has been a serious issue for years. 

But there are other ways to look at the act of copying. It can actually be an incredibly effective business strategy, if of course you set moral and legal questions aside for a moment. 

Germany's Rocket Internet, founded by brothers Marc, Oliver and Alexander Samwer, has successfully launched "copycat" versions of several U.S.-based online services, like eBay and Airbnb. 

The brothers realized that a service like eBay's had a lot of potential for the German market. They initially proposed to build a German franchise as contractors, but when eBay didn't respond, the brothers decided to independently launch a copy company, called Alando.

Some six months later, eBay acquired Alando for over €38 million. Similarly, Rocket Internet's short-term vacation rental site, Wimdu, has become a successful competitor to Airbnb. 

Copying goods can be equally profitable. The market for counterfeit goods is estimated at $600 billion a year, or some seven percent of all global trade! 

In China, there's a whole business culture called _shanzai_ that focuses on copying branded goods for lower-priced markets both in China and abroad. 

So copying can make good business sense. But is it ethical? The answer is simple: yes. 

Copying breaks up monopolies and in doing so, serves a greater good. For instance, India and Brazil recently passed laws allowing pharmaceutical companies to copy certain branded drugs without paying licensing fees to developers. 

Since both countries are huge emerging markets, in effect these policies now cheaply provide vital and lower-priced medication to millions of people.

> _"Lax enforcement on intellectual property laws was the primary engine of the American economic miracle." — Doron S. Ben-Atar_

### 6. Copying and exchanging ideas promotes innovation and progress. Closed shops are a dead end. 

Facebook is the most successful social network around today, but it certainly wasn't the first of its kind. (Remember Friendster?)

And that's the thing — copying can lead to innovation. You take something that already exists and improve upon it. In fact, that's the story of some of history's most groundbreaking inventions, like the steam engine and the personal computer. 

Which goes to show you that copying doesn't just help differentiate similar products in a market, it actually can lead to totally new products!

Digital music downloading first surged in popularity with the advent of platforms like Napster. Since the service allowed users to stream and share songs for free, the music industry fell into a panic. Major labels tried to find ways to stem the practice aside from simply throwing lawsuits around. 

Legal streaming services like Spotify emerged as one viable solution. And although it started slowly, Spotify has grown in popularity and since 2008 has paid $2 billion in royalties to participating artists. 

In essence, we really should share ideas instead of only competing. Because when it comes to progress, collaboration tends to be more efficient than competition.

After two decades of working for the pharmaceutical company GlaxoSmithKline, scientist Chas Bountra chose a misfit path, developing _pre-competitive pharmaceutical research_. 

But what exactly is that?

Bountra helped competitors collaboratively develop new medications and after a certain testing stage, the companies that collaborated are offered the opportunity to buy the exclusive rights to sell and distribute the drugs. By allowing companies to build on each other's work instead of starting from scratch each time, the practice makes for more effective research. And at the same time, it preserves economic competitiveness. 

What's more, patents often act as deterrents to innovation by favoring large companies (with the financial resources to pay patent fees) over more independent and creative inventors. 

The numbers tell the story, as between 1999 and 2008, some 48 percent of US patents were issued to just 1.5 percent of all patenting firms!

### 7. At its core, hacking is about building deep knowledge to transform something and make it better. 

When you hear the word "hacker," what do you think of? A shady character, guzzling Red Bull and stealing credit card numbers from poorly secured servers? 

We tend to forget that _real_ hacking isn't just about online theft and compromised privacy. 

Real hacking is different. Think of Edward Snowden, who exposed the questionable surveillance activities of the US National Security Agency. His action was at its core about freedom of information. Similarly, the _hacktivist_ collective Anonymous is about challenging authority.

Hacking is about taking things apart and putting them back together in a better way. 

Consider Sam Roberts, who started fiddling with electronics when he was nine years old, taking them apart and rebuilding them differently. He once created a burglar alarm system out of a pair of walkie-talkies!

And when someone gave him a 2G mobile base station, he spent six months learning how it worked. Eventually, his deep knowledge allowed him to start a telecommunications business with his brother.

In addition to hacking computers, we can also hack organizations — or even society. 

That's what Gary Slutkin did when he took on the problem of urban violence. A leading disease prevention expert by trade, Slutkin spent five years analyzing the sources and causes of violence in neighborhoods. 

When Slutkin noticed that violence in a community spread like disease in a body, he decided to apply strategies from his medical career to address the problem.

In the end, he developed a program allowing citizens to call in _violence interrupters_ if they thought a violent incident was imminent. Primarily made up of community residents, this special task force would then try to de-escalate the situation. 

Slutkin's approach has proved promising and is being tested across the United States. In a way, he managed to hack inefficient prevention policies for the good of all.

### 8. Innovation can come from challenging the mundane and asking provocative questions. 

The first person who dreamed up the idea of an airplane surely seemed mad. But this goes to the heart of innovation: new ideas provoke people by upending their view of the world. 

Think about everything that makes up your life, such as family, work and school. Innovation happens when we stop taking these things for granted and start questioning them. 

At the age of 12, Dale Stephens decided to leave school, as he was frustrated with the regimented, fixed curriculum. Instead, he chose mentors and teachers who helped him follow his interests outside of the classroom. 

That experience eventually led him to found UnCollege, a program that helps students carve out alternatives to a traditional university career. And although he's constantly forced to defend his decision to leave school, Stephens has shown society that there are self-reliant, creative paths outside the traditional educational system. 

Challenging the mundane is one path to innovation. But radical ideas can also come out of art. 

At NASA's Jet Propulsion Laboratory, scientists are often inspired by novels and science fiction. Mind-bending stories can lead to progress and outside-the-box discoveries in the "real" world. 

To be sure, the process of innovation doesn't begin with answers. If you want to create something new, you have to ask provocative questions to broaden people's minds and create a new level of awareness. 

The results can be radical. The French feminist activist group, La Barbe, wears fake beards and crashes meetings at male-dominated organizations to draw attention to issues of gender inequality. 

Unsurprisingly, video footage from the group's protests often goes viral. The activists refuse to position themselves as experts; they're not interested in presenting solutions. Instead, they want to raise awareness of gender equality within society as a whole.

### 9. Forging a new path can open up unexpected possibilities and lead to greater fulfillment. 

Society encourages us to play it safe. But taking risks often pays off. 

Just ask Gib Bulloch. He had a comfortable, financially secure career at consultancy Accenture, but something was not quite right. He knew he wanted to make a difference in the world. 

So he took a sabbatical to work for a development project in Macedonia, where he noticed a lack of business expertise among other development workers. 

And that gave him the idea to build a new, non-profit department within Accenture to provide economic consulting for development organizations. The program allows employees to contribute to development projects for a period of time, temporarily receiving a reduced salary in exchange for greater personal fulfillment. 

Within Accenture, this pioneering project has been wildly successful. Today it manages more than 200 different development projects annually. 

Bulloch's initiative shows just how valuable it is to take risks. And sometimes the biggest, scariest risks — the ones that throw us into a new, unfamiliar environment — are the best kind. 

Former Brown University soccer player Tyler Gage abandoned an athletic career to explore the world, spending two years with an ethnobotanist in Costa Rica and nine months with a native tribe in the Amazon. 

He also traveled to Peru, which is where he came up with the idea of developing a sustainable business around _guayusa_, a local plant used for tea. 

And in doing so, he became a social entrepreneur. By introducing guayusa-based beverages to the US market, Gage supports Peruvian local farmers and helps battle rainforest destruction.

> _"Sometimes in leaving everything behind, you are forced to radically reinvent yourself."_

### 10. Misfit communities lack hierarchies and prioritize cooperation above competition. 

We typically think of misfits as loners. But that's not a totally accurate view.

Although misfits do value autonomy and self-reliance, they also seek out collaborative networks. These freethinkers are attracted to communities that lack hierarchies and are made up of independent individuals. 

Seeing the success of such structures, companies like Valve (which developed the bestselling computer game Half-Life and gaming platform Steam) are trying to emulate misfit communities within a corporate context. 

At Valve, there's no official hierarchy. Workers are encouraged to build informal work groups, and salaries are based on a _reputational currency system_. That means employees rate each other's teamwork, technical skills and overall contribution. In the end, this aggregated score influences each person's salary. 

Which gets at an important point: cooperation is one of the great strengths of misfit communities. 

Just look at Linux. The computer operating system was first introduced by one programmer as an open-source project. But over time, an enormous community of independent programmers and hackers has grown around it. And within this community, decision making is collective. This means that the group decides whether to implement changes to the operating system.

And that goes to show that misfits aren't always outlaws. When they belong to a community, misfits follow rules that are consistent with their beliefs. 

This principle even applies to pirates, the prototypical misfits. In the eighteenth century, pirates developed an informal market that evolved around growing transatlantic trade. And community was at the core of this system. On each boat, pirates shared rights, obligations and profits (or loot) equally among crew members. 

This communitarian structure created coherence and strengthened the bonds between crew members. And when pirates went after hierarchically organized trading ships, they had a major advantage!

### 11. Final summary 

The key message:

**"Misfit" is not a dirty word. On the contrary, breaking the bounds of establishment thinking can lead to innovation and progress. Misfits have always led the way, shaping the course of history with provocative ideas and blatant disregard for rules and conventions. Today, society needs misfits more than ever to meet the challenges of our fast-changing world.**

Actionable advice:

**Bring your misfit personality to work.**

We tend to leave our personalities at home when we head to the office, but it wouldn't hurt to open up more. It's OK to talk about your yoga practice at the workplace, as some of your coworkers might share your passion. You could even organize a group yoga exercise every morning, to help coworkers relax before starting a busy day!

**Suggested further reading:** ** _When To Rob a Bank_** **by Steven D. Levitt and Stephen J. Dubner**

_When To Rob a Bank_ (2015) presents a collection of articles published on the Freakonomics blog at freakonomics.com, which has now been going strong for ten years. Honing in on the unpredictable and downright strange, Levitt and Dubner cover everything from why you should avoid anyone whose middle name is Wayne to why some of us should be having more sex than others.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Alexa Clay and Kyra Maya Phillips

Alexa Clay is a leading expert on subcultures. Among a host of other projects, she founded the League of Intrapreneurs, an initiative to create change from within big businesses.

Kyra Maya Phillips is a writer and innovation strategist. A graduate of the London School of Economics, Phillips previously wrote on environmental issues for _The Guardian_.

