---
id: 593e8479b238e10005ae417d
slug: misplaced-talent-en
published_date: 2017-06-16T00:00:00.000+00:00
author: Joe Ungemah
title: Misplaced Talent
subtitle: A Guide to Better People Decisions
main_color: 2DA8DF
text_color: 1D6E91
---

# Misplaced Talent

_A Guide to Better People Decisions_

**Joe Ungemah**

_Misplaced Talent_ (2015) is a practical guide to hiring the employee best suited for the job and how to keep that employee happy and satisfied. Many businesses don't even know what skills they should look for in an employee — or, if they do, they'll forget about keeping that employee motivated once he or she is on board. It's important to know how to do both of these things, so find out how to avoid all the messy pitfalls and keep your workforce happy.

---
### 1. What’s in it for me? Hire the best, every single time. 

Anyone who has ever worked in HR will know that hiring people isn't a quick and easy process. Crafting a job ad, sifting through the hundreds of applications, interviewing and onboarding promising people — all of this can take weeks, or even months.

And what if you hire the wrong person? Or what if your new employee decides that his new job isn't for him? Well, then you have to start the whole process over again.

These blinks help ensure that this never happens. They show you how to set up your hiring and onboarding process to get the most suitable candidate every time. By following these simple rules, recruiting will become a cakewalk.

In these blinks, you'll find out

  * why you shouldn't look for a "talented person";

  * Why just interviewing someone isn't enough; and

  * why every employee has a psychological contract with their employer.

### 2. To ensure you get the best applicants, carefully define the job criteria. 

Hiring can be a costly process that many people would prefer to avoid. And yet, so many companies do it badly. In some industries, the turnover rate is around 20 percent. Having to constantly hire a fifth of your workforce will cost an awful lot of time and money.

These companies are in a _state of misplaced talent_ : they hire the wrong people for the wrong reasons.

So let's dive into how we can hire the right people for the right reasons, which starts by creating a framework that properly identifies what is required for the roles you are looking to fill.

This might sound obvious, but you'd be surprised at how often important criteria such as experience, skills and education are left unspecified.

Let's say you have a vacant customer assistance position. Without specific criteria, you could find yourself hiring the candidate with the most colorful résumé, or the one with the Ivy League education, neither of which are actually relevant.

To avoid this mistake, identify the job's main criteria — things such as relevant experience in customer service. This way you'll know just what to look for and avoid having to fill this position again after things go poorly.

Now, a good list of criteria will include more than just the technical aspects of the job. Companies like Microsoft and Apple have found that an ideal candidate should also fit the cultural framework of the company.

To identify these criteria, figure out what kind of values your company stands for and what your primary goals are. Are you focused on technological innovation, or maybe you want to provide an exceptional customer experience for travelers?

Whatever it is, try to ensure that your framework is precise, focused and realistic.

All too often companies create vague descriptions by saying they're looking for a "talented" or "skilled" person. But this isn't useful unless you have something to judge that talent against.

So it's best to precisely define the skills you're after and use specific language, and to mention the exact kind of degree you want your candidate to have.

### 3. Good company branding can help you attract great candidates. 

Once you know what to look for, the next step is finding the right candidates.

Contrary to what you may have heard, modern technology hasn't made this process a whole lot easier for the employer. All technology has done is make it far easier for people to apply for a job.

This means you might get a thousand applications for your job posting, each vying for your attention. Of course, just receiving a barrage of applications doesn't mean the candidates will be any good. Quantity doesn't automatically mean quality.

So start things off right by attracting the best possible candidates.

The companies that always find themselves at the top of the "great places to work" lists attract high-quality candidates — and manage to hold onto them as well.

Google, for example, often gets praised for providing a great work-life balance; they provide good benefits and also offer personal-development opportunities. Because of this, the company always has good candidates lining up, hoping to work for a great company.

This shows us that _employer branding_ is a great way to attract quality employees. And you can brand your company based on your _employer value proposition_, which is what your business offers in exchange for employment.

This includes everything applicants should expect if hired, including the kind of recognition, compensation and professional development they'll receive, as well as the quality of the management they'll work under.

The importance of these aspects will differ from person to person. So, to find out what potential candidates might value the most, check in with you current employees and ask them what they like the most about their work.

After doing this, you should have a better sense of what your company brand is. Once you know that, it's time to make it visible and advertise how much you value your employees.

The most obvious ways to do this are to update your website and enlarge your social-media presence.

### 4. A well-structured interview and diverse assessments can help narrow down your candidates. 

All right, at this point, you should have great candidates applying for your job posting, all of whom you've considered, keeping an eye out for those who meet your well-defined criteria.

The next step is to find out more about them and really narrow down which applicants might be perfect for the role.

There is no precise rule about how to do this, but there are a couple of great ways to put the skills of your top prospects to the test.

Interviews are, of course, a tried-and-true way to assess people.

But to get the best and most relevant information, you should make sure your interview has a meaningful structure.

The simple way to do this is to have a list of prepared questions that test the applicants' knowledge of the job and the field you're in, as well as their experience.

You can even test these questions out on current employees to make sure you're using suitably probing questions that provide the most valuable information about how suitable a candidate is for the job.

But sometimes an interview isn't enough to really narrow things down, which is when a good simulation can go a long way in helping you find the right fit.

A simulation allows you to test the skill set of a candidate and determine whether they have what's right for the job. So, if you're hiring a writer, give him a deadline to submit some work so you can see how he performs under pressure.

And you don't have to rely on just one simulation to get all the information you need.

One test might tell you how well he writes, but it might not give you any idea of how well he works with others. You should design different tests that give you a full picture of your top candidates.

Assessments might not be perfect, but they can be a great help when trying to determine an applicant's chances of success.

### 5. Keep your employees motivated with the help of praise and questionnaires. 

So you've found your perfect candidate. Congratulations! Now it's time to help that person reach his or her full potential.

When someone performs well, it's due to a mixture of capability and motivation to apply that capability. Most companies tend to forget about this second part — but you don't have to be counted among the forgetful.

There are a number of ways to provide motivation: offering rewards like a bonus or good compensation, or by praising and recognizing a task well done.

More often than not, managers rely on money and forget about how effective good praise can be. They assume money does the talking for them and says everything that needs to be said, but this isn't true.

Let's go back to the writer example and ask: What would make him motivated to keep producing great content? Many employees will tell you that receiving some appreciation works better than just getting paid.

A pay raise is another tactic, but on its own it can also backfire. A better motivational technique is to offer a pay raise along with the chance to write more pages of content. This way, the writer gets more responsibility to match the pay, and you get more content to go along with it.

It's important that you find out what method of motivation will work for each of your employees. And a good way to go about this is to send out questionnaires to gauge their personalities and find out what motivates them.

This way, if an employee says she loves hearing positive customer feedback about her work, you can then make sure these comments always find their way back to her.

But questionnaires also aren't a perfect method.

A badly designed questionnaire can sometimes suggest that employees are expected to feel a certain way, and this can lead to their confirming that assumption rather than providing a genuine answer that reveals their true feelings.

### 6. Keep employees and management on board through trust and professional development. 

Trust is important for any relationship, including that of an employee and an employer. For an employee to provide their best work and for a company to achieve its goals, both parties need to know that they're on the same page.

To ensure this connection, a _psychological contract_ is required _._

This is an unspoken agreement between the two parties based on a set of mutual expectations.

For example, the employee usually expects a regular paycheck and managerial feedback, while the employer will expect great work and punctuality.

When both parties agree to fulfill their side of the bargain, everyone can stay happy and motivated to do their job.

One way to make sure the psychological contract stays unbroken is to provide great development programs for your managers and staff. There are three things that you should keep in mind when initiating such programs:

The first is self-awareness, which means that both the employees and managers are aware of their strong points as well as their weaknesses.

To encourage self-awareness, you should create an environment where people feel free to make mistakes as a way of learning how they can grow and become even better.

Second, don't be afraid to challenge your employees.

Challenges allow employees to develop and improve their skills, especially the ones that will help them continue to grow professionally. 

Finally, you need to make sure you have the organizational support to meet the challenges and facilitate the development. This is generally done by having coaches and mentors available. 

These development programs are extremely useful, but be careful that you don't make them seem like a test. For example, you should never add an extrinsic reward, like a bonus, for the improvement. This will remove the self-development aspect of the processes.

Remember, there isn't one perfect way to hire a great employee and keep him or her motivated, but as long as you're following these simple guidelines, you'll certainly be on the right path.

### 7. Final summary 

The key message in this book:

**Hiring doesn't have to be a painful process. With the right perspective, it can be a very successful and rewarding operation, but only if its well planned and organized. For this to happen, you need to know what to look for, so be clear about what makes the ideal candidate a good fit for your environment. If you take all this into account, you'll end up with more capable, loyal and highly motivated employees.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Talent Magnetism_** **by Roberta Chinsky Matuson**

In _Talent Magnetism_, author Roberta Chinsky Matuson shows you how to transform your workplace into an environment that draws top talent like a magnet. The book offers practical advice on how to develop a strategy to stay ahead of the competition by identifying how evolving technology and a new generation of workers have changed business in the twenty-first century.
---

### Joe Ungemah

Joe Ungemah is a psychologist and an expert on how and why people make the decisions they do. His advice is sought after in the United States, Australia and Europe, where he's helped a number of major companies find more satisfied employees.

© Joe Ungemah: Misplaced Talent copyright 2015, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

