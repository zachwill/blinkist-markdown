---
id: 56e033f126ea84000700006d
slug: missing-microbes-en
published_date: 2016-03-11T00:00:00.000+00:00
author: Martin Blaser
title: Missing Microbes
subtitle: How the Overuse of Antibiotics is Fueling Our Modern Plagues
main_color: FBB232
text_color: 94691E
---

# Missing Microbes

_How the Overuse of Antibiotics is Fueling Our Modern Plagues_

**Martin Blaser**

_Missing Microbes_ (2014) explores the strange and microscopic world inside your guts. It sheds light on the crucial role played by microbes — tiny creatures that keep your body happy and healthy — and explains the dangers of overusing antibiotics.

---
### 1. What’s in it for me? Take control of your gut health. 

Did you ever experience a bad and painful infection that immediately disappeared after a short course of antibiotics? In the right situations, antibiotics can work wonders. 

But as you'll learn from these blinks, the very same medication can also do great harm. Just as too much of a herbicide will kill almost everything in your garden — including your favorite flowers — antibiotics can poison the very gut bacteria that keep you healthy. 

These blinks will help you balance the risks and the benefits of antibiotics. 

Read on and learn

  * why the earth would be barren and boring without microbes;

  * how wolves and elks are similar to your gut bacteria; and

  * about farmers who give healthy porklings tons of antibiotics.

### 2. Missing microbes may be behind the rise of chronic diseases like asthma, allergies and diabetes. 

Most people know that the proliferation of obesity, diabetes, asthma and cancer is a major concern. And the incidence of such ailments will only increase. But how can this be? What about the revolutionary wonders of modern medicine?

The answer lies in the tiny organisms that call your body home.

Indeed, this community of microorganisms is, by and large, what keeps you in good health. Known as the _microbiome_, it helps fight diseases, and so is extremely important for your immune system.

But where do these microorganisms come from?

When exiting the birth canal, newborns get covered with diverse microbes. These organisms then colonize the infant's skin and gut, and establish the microbiome that will remain with him or her for life.

Caesarian sections, along with the overuse of antibiotics and sanitizers, can alter your microbiome, resulting in a weaker immune system or fostering antibiotic-resistant bacteria.

Indeed, any change in your microbiome — especially the loss of one of the many bacterial species that compose it — can have serious consequences. 

The more diverse your microbiome, the better it can protect itself from unwanted intruders. If even one key species is removed from the microbial ecosystem, the whole ecosystem may suffer or even collapse. 

To get an idea of how this works, let's consider a much larger ecosystem — Yellowstone National Park. 

About 70 years ago, wolves were removed from the park. As a result, the elk population exploded. 

The elk then ate all the willows on the riverbanks, which meant fewer beavers and songbirds, who depended on the willows to build nests and dams. Consequently, riverbanks eroded. 

With the wolves gone, there were fewer elk carcasses. This caused a drop in the populations of animals that depend on carrion, such as ravens, eagles, magpies and bears. Moreover, bison, which share a diet with elk, were crowded out. 

All this just because one species was removed from the ecosystem. The same could happen in your gut!

> _"The loss of microbial diversity on and within our bodies is exacting a terrible price."_

### 3. Microbes were on Earth long before us, and are responsible for our very existence. 

Imagine 3.7 billion years of evolution mapped out as a single day. As you watch the evolutionary clock tick, you'll notice that microbes were there from the very first seconds; our human ancestors arrived somewhere between 47 and 96 seconds before midnight. And we, _Homo sapiens_, appeared only two seconds before the end of that 24-hour period!

Microbes have been around for billions of years, and without them there probably wouldn't be any life on Earth. In fact, for around three billion years bacteria were the _only_ living things on the planet. They are responsible for the chemical reactions that eventually created the _biosphere_ — the global sum of ecosystems that we and all other multicellular life depend on for survival.

Despite being invisible to the naked eye, microbes are all around us. As in the early days of our evolutionary history, bacteria are virtually everywhere: on land, in the water and the air. 

Not only are microbes everywhere — they're also immeasurably numerous. In fact, these invisible microbes actually make up the majority of the Earth's biomass. If you were to count up all the microbes on the planet, not only would they outnumber all the plants, fungi, animals and people on Earth; they would also outweigh them! 

We should be thankful that microbes are omnipresent. Without them, we would be neither able to eat nor breath. And though we humans depend entirely on microbes, the reverse isn't true: they will continue to thrive with or without us.

So bacteria are important to and even necessary for human life. But we shouldn't forget that they also cause the diseases that sometimes bring our lives to a premature end.

> _"We live in the Age of Bacteria (as it was in the beginning, is now, and ever shall be, until the world ends.)"_ \- Stephen Jay Gould

### 4. Antibiotics may have saved our species from being eradicated by an epidemic. 

Try to imagine some creatures capable of eradicating our early ancestors. What comes to mind? Fierce saber-toothed tigers? Ravenous wolves? Gargantuan bears? Wrong, wrong and wrong. You need to think a lot smaller! 

For centuries, there was no greater threat to humanity than _pathogenic_ — that is, disease-causing — bacteria. But it wasn't always that way.

In the early days of human history, bacterial epidemics weren't an existential threat to the species. When our ancestors were still living as hunter-gatherers, pathogenic bacteria, though a threat to individuals and communities, posed no real threat to the entire species because the world population was broken up into many small tribes.

So, when a pathogen got hold of an individual, there were essentially three possible outcomes: either nothing happened, the whole tribe got sick and died or some got sick and the others became immune. 

The pathogens were trapped in a closed system: even if they infected everyone in the tribe, there was no way for them to move beyond it. For a true epidemic to occur, there needed to be many more people bunched together in one place.

In other words, there needed to be cities. Early cities attracted animals, like rats and other pests, with their attendant parasites and bacteria. And with the pests came epidemics.

Perhaps the most famous epidemic is the Black Death, which started in 1347 and wiped out one-third of the European population over the course of ten years. 

Over time cities grew, and the larger and more populous they became, the easier it was for pathogens to spread. Despite improvements in hygiene, lethal epidemics like cholera and smallpox remained a huge problem even in the nineteenth century. 

Fortunately, we're no longer helpless in the face of pathogens, thanks to the work of Alexander Fleming. 

In 1928, Fleming discovered and developed the first antibiotic, _penicillin_ — largely by accident. This discovery laid the groundwork for modern antibiotics.

Unfortunately, antibiotics have almost created as many problems as they've solved.

### 5. Antibiotics are as much a curse as they are a blessing. 

Antibiotics are real lifesavers, and chances are quite high that you know someone who probably would have died without them. 

Indeed, the development of antibiotics is one of the biggest medical breakthroughs of the twentieth century. Without these medicines, countless people would have died from illnesses that today seem trivial. 

The author experienced this first hand after returning home from India and Bangladesh, where he had worked for a few months. Upon returning, he started to feel achy, developed a fever and eventually had to be admitted to the hospital. 

Being an expert on _Salmonella typhi,_ the bacteria responsible for typhoid fever, he advised his doctors on which antibiotics to use. Blood samples revealed that he had actually contracted a twin bacterium called _Salmonella paratyphi_, which luckily warranted the same course of treatment. 

With the help of antibiotic treatment and two weeks' rest, he recovered. Without antibiotics, however, he surely would have died or taken much, much longer to recover, as typhoid is a rather serious illness.

Antibiotics are perfect for treating bacterial infections, but they also have a serious downside. Today, antibiotics are everywhere — even in our food — and this comes with great risks.

The majority of antibiotics in the United States aren't produced for humans, but for farm animals. There are two main reasons for this. 

First is the unsanitary conditions in which the animals find themselves on almost every farm. These farms provide the perfect conditions for pathogens to spread, and antibiotics ostensibly keep the animals healthy. 

The second reason is that antibiotics promote growth, as the microbes that survive the antibiotic treatment cause the animals to gain more weight. Ultimately, this makes the process of food production much more efficient.

However, administering loads of antibiotics to livestock has dire consequences for us. Harmful antibiotic residues get carried into our food and water, and the microbes inhabiting farm animals develop antibiotic-resistant microbes.

### 6. The overuse of antibiotics is bound to change our microbiome, setting us up for infections. 

Here's a harrowing thought: antibiotics aren't nearly as safe as pharmaceutical companies would have you believe. 

Though antibiotics are effective at attacking and killing dangerous bacteria, they do the same to the bacteria in your gut, leading to unintended and even dangerous changes in your microbiome. 

Take, for example, the case of Peggy Lillis, a completely healthy and vibrant 56-year-old woman. Following a minor dental procedure in March, 2010, she received antibiotics. Just one and a half months later, she was dead.

Before her untimely death, Lillis was diagnosed with a _Clostridium difficile_ (C. diff) infection _._ Small amounts of C. diff can be found in the guts of healthy people; usually, it's kept in check by competing bacteria. But as soon as these competitors are wiped out — typically by antibiotics — C. diff quickly spreads and wreaks havoc on the guts, producing toxins that destroy the walls of the colon and causing fecal particles to enter the bloodstream. 

That's exactly what happened to Lillis. While it's uncertain whether the C. diff originated in her or if she contracted it elsewhere, antibiotics are what allowed this infection to develop.

Moreover, even a normal course of antibiotics can make you more susceptible to infections, as was demonstrated by the 1985 _Salmonella_ outbreak in Chicago that resulted in 160,000 infections and several deaths. 

At the beginning it wasn't clear what caused the outbreak. Soon, however, it was discovered that a certain brand of milk from "Supermarket A" was the culprit. 

When the health department conducted a study with the infected individuals, they found that of all those who'd drunk the milk, those who had been taking antibiotics the month prior were more than five times more likely to become ill than the others. Apparently, taking antibiotics, while deterring the bacteria that ail you, can also set you up for new illnesses.

> _"... people whose resident bacteria have been disrupted the most will be the most vulnerable."_

### 7. Reducing your antibiotic exposure and taking prebiotics will help your good microbes. 

Some people take antibiotics way more than is actually necessary. If you value your health, you should take care not to follow their example.

In fact, being sure to take antibiotics only when absolutely necessary is a great way to reduce your exposure to the dangers they pose to your body.

Unless you are a physician, you're probably ill-equipped to determine whether or not you need antibiotics. With that said, you can always tell your attending physician that you would prefer not to take antibiotics until you know whether it's truly necessary. 

The same holds true for children. Before giving them antibiotics, ask your doctor if it's truly medically necessary to do so. 

Some nations, such as France, are making concerted efforts to reduce exposure to antibiotics, especially in respect to children. In 2001 France consumed more antibiotics than any other European nation. But thanks to the campaign "Antibiotics Are Not Automatic," antibiotic use had dropped by 26 percent by the year 2007. For children under the age of three, the use of antibiotics declined by around 36 percent. 

Furthermore, you can be proactive in keeping your gut healthy by ingesting _prebiotics_, substances that encourage the growth and activity of microorganisms.

Indeed, many people report feeling better when taking prebiotics. However, the effectiveness of prebiotics hasn't yet been unequivocally proven. It's entirely possible that the good feelings that come from prebiotics are little more than a placebo effect.

That being said, it's likely that prebiotics and also probiotics (active bacterial cultures) will become very important in the future.

### 8. Final summary 

The key message in this book:

**Antibiotics are incredibly helpful — not just in treating the serious diseases that have plagued humanity for ages, but also in enhancing growth in farm animals. But there are also risks: in our fervor to kill bad bacteria, we risk killing the ones that also make life possible.**

Actionable advice:

**Use normal soap instead of sanitizers.**

Many modern sanitizers contain _triclosan_, which isn't an antibiotic but nonetheless kills bacteria. Ordinary soap, however, doesn't kill bacteria and will do the job well enough for the average person. These bacteria have been living on your skin for years, and many of them protect you from harmful germs. So why would you want to kill them?

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Gut_** **by Giulia Enders**

_Gut_ (2015) takes an entertaining yet scientific look at an organ that is just as interesting and important as the brain — the gut. By tracking a piece of cake as it works its way through the digestive system, you'll come to appreciate the gut for the sophisticated and impressive ecosystem that it is.
---

### Martin Blaser

Dr. Martin Blaser is professor of microbiology and Director of the NYU Human Microbiome Program. His work focuses primarily on bacteria, such as _Helicobacter pylori_, _Campylobacter_ and _Salmonella Bacillus anthracis_. More recently, his research has focused on the human micro

