---
id: 533b0e683961370007440000
slug: mistakes-were-made-but-not-by-me-en
published_date: 2014-04-01T09:36:51.000+00:00
author: Carol Tavris and Elliot Aronson
title: Mistakes Were Made (But Not by Me)
subtitle: Why We Justify Foolish Beliefs, Bad Decisions, and Hurtful Acts
main_color: FCE732
text_color: 635B14
---

# Mistakes Were Made (But Not by Me)

_Why We Justify Foolish Beliefs, Bad Decisions, and Hurtful Acts_

**Carol Tavris and Elliot Aronson**

Through studies and anecdotes, these blinks explain why, when we make mistakes, we often come up with self-justifications instead of admitting the mistakes to ourselves. It also shows how detrimental these self-justifications can be to personal relationships, medicinal care, the justice system and even international relations.

---
### 1. What’s in it for me? Learn to admit your mistakes and then learn from them. 

Have you ever been in a situation where you've made a mistake you need to tell others about? If so, in the moment you realized, you'd have to fess up to your mistake, you probably felt your chest tighten up and a lump rise in your throat. This is natural, as nobody likes admitting mistakes. But if you did admit your mistake, you probably felt liberated and could find ways to learn from it.

These blinks explore the human tendency to avoid admitting our mistakes by finding self-justifications for them.

You'll discover why your brain is willing to jump through any number of hoops, even fabricating memories to protect you from having to admit a mistake.

You'll also find out how refraining from admitting your mistakes can lead to broken marriages, misdiagnosed illnesses and even wrongful criminal convictions.

Finally, you'll learn why admitting your mistakes can be a good thing, and how it can even get you promoted.

### 2. Instead of admitting our mistakes, we tend to justify them. 

Everyone has done things they shouldn't have. For example, have you ever munched down an entire family-sized pack of potato chips in one go? Chances are pretty good you have.

But, if you're like most people, you probably told yourself that, actually, it was okay because, say, you'd had a hard week and fully deserved that little indulgence.

Most people do the same: they seek justifications for actions they know were wrong. That's because they want to reduce feeling _cognitive_ _dissonance_, or the unpleasant feeling of having two conflicting ideas in our heads.

In this case, if you consider yourself someone who adheres to a healthy diet, your overindulgence creates cognitive dissonance. To resolve the dissonance, you come up with a so-called _self-justification_ (e.g., the rough work week) for your actions.

Another example of cognitive dissonance would be someone who bemoans the dangers of smoking while continuing to smoke themselves. To resolve the cognitive dissonance of the conflicting views and behavior, this person will probably come up with a self-justification like, "I really don't smoke that much so the health effects are negligible."

Unfortunately, these kinds of self-justifications can make us cling to our beliefs even more vehemently — not change our behavior.

For example, after making a mistake, we may find a self-justification for it rather than dealing with it head on and trying to understand why it happened.

One example can be seen in the invasion of Iraq. Even though the weapons of mass destruction that served as the justification of war were ultimately not found in Iraq, and the invasion of the country resulted in an increase of Islamic radicalism that the war was meant to diminish, U.S. president George W. Bush was still convinced that he had made the right decision to go to war. Undoubtedly, this was a way of resolving his cognitive dissonance.

### 3. We justify our mistakes by looking at situations in a biased, narrow-minded manner. 

As we've seen, everyone can come up with justifications for their mistakes to avoid cognitive dissonance.

So how do we do convince ourselves our justification is right?

Through so-called c _onfirmation_ _bias_, or when we see the evidence of the matter in a way that supports our original view. This can make a lack of evidence, or even contradictory evidence, look like evidence that supports our case.

For example, in the U.S. in the 1980s, many people were driven to a moral panic because they were convinced that satanic cults were infiltrating society's highest levels and abducting children for satanic rituals.

The FBI duly investigated these claims and found no evidence to back them up. But instead of accepting this, confirmation bias led the accusers to assert that this showed precisely how adept the culprits were at hiding their crimes.

Self-justifications and confirmation bias can actually even change our view on morality through a step-by-step process, known as the _pyramid_ _of_ _choice._

To better understand this, imagine two people with similar morals both have the opportunity to cheat on their spouses.

According to the pyramid of choice, as they try to decide what to do, they are standing on top of a pyramid. From there, they have a bird's-eye view of the consequences of their choices: what will happen if they cheat or if they remain faithful.

Now, imagine one of the two decides to cheat while the other stays faithful. This means they both begin to clamber down the pyramid, though along very different paths.

As they descend, they lose their overview of their choices, and only see the narrow path they've chosen.

The tendencies of self-justification and confirmation bias mean that the person who committed adultery will be ever surer that it was the right choice, while the other will become surer of the opposite.

As they travel down, their paths diverge further, until they finish at opposite corners of the pyramid with totally different views of morality.

### 4. Far from an objective record of the truth, our memory is fabricated and adjusted to suit our needs. 

When asked about a prior event, most people are confident that their memory is an accurate representation of what happened. In fact, it's often not true.

First of all, we tend to bias our memories to better suit our current situation, for example, to justify our behavior or our beliefs.

This effect was seen in a study conducted in the 1960s, where teenage boys were interviewed about topics concerning sexuality. When the same boys were interviewed as adults, they "remembered" their responses as teenagers as having been more liberal than they really were. This was attributed to the fact that they tried to make their memories fit the commonly held view that the 1960s were a sexually liberated era.

Second, sometimes we can even have memories of events that never happened, otherwise known as "false memories."

For example, millions of Americans believe they've encountered aliens and even vividly "remember" the details of their encounters. But studies show such encounters are often the result of jet lag and fatigue from, e.g., driving long distances without resting. In these situations, the "abductee" has in fact experienced sleep paralysis, or feeling paralyzed while being aware of one's surroundings.

In around 5 percent of sleep paralysis cases, the person also hallucinates, essentially experiencing a waking dream and, in some cases, actually dreams up alien encounters. Although the memories are false, the individuals' brains still treat them as real, so they become part of their life narrative.

So how can we be sure our memories are authentic?

We need to cross-check them, preferably with undisputed historical facts and accounts.

For example, one author named Binjamin Wilkomirski wrote a popular book about his childhood in Nazi concentration camps. But after historical analysis of the material, his story was proven false. It turned out that he had not suffered in concentration camps, but had written the book based on various other sources as a way to cope with his own troubled childhood.

### 5. Self-justification of mistakes hinders scientific advancements, especially in the field of medicine. 

Have you ever been to the doctor's office to get a diagnosis for something that's been bothering you and then, at the last minute, just as you're leaving the office, the doctor exclaims, "No, wait! I made a mistake — that's the wrong diagnosis!"

Probably not. Because, hopefully, you've had good doctors, but also because even the best doctors are reluctant to admit their mistakes.

Which is problematic, as mistakes are a key part of advancement in any area of scientific research.

For example, in the nineteenth century, physician Ignaz Semmelweis found that many of his female patients were dying after childbirth. After analyzing the situation, he hypothesized that it was due to a mistake in the delivery procedure, more specifically, that his fellow colleagues weren't washing their hands before the delivery, resulting in infections. By enforcing stricter cleanliness requirements, he was able to save many lives.

Sadly, however, medical mistakes like this one are not always recognized, and can sometimes even be deliberately hidden. To avoid seeming incompetent, insecure, or being sued for malpractice, medical professionals don't readily admit to having made mistakes.

But once any scientific field blinds itself to its mistakes, that field becomes a _closed-loop_ that blocks any future developments, in this case medical improvements.

For example, one oft-made and thoroughly ingrained mistake in the field of medicine is known as _clinical_ _intuition_, when doctors assume their first judgment is correct and make the official diagnosis without exploring other alternative diagnoses, in the same vein as, "I know it when I see it."

Due to confirmation bias, once the doctor comes to her diagnosis, she forces all the evidence she later sees to fit this view.

### 6. The criminal justice system also makes mistakes due to the self-justification of its actions. 

Would you be worried if you were arrested for a crime you didn't commit? Probably. But your worries might be tempered by the fact that you, like most people, trust that the criminal justice system seeks the truth and works in a legally and morally justifiable way.

Unfortunately, this isn't always the case, as sometimes people's tendency towards self-justification can lead the justice system to bury the truth just to have the appearance of running smoothly and being error-free.

This can be seen in the prevalence of wrongful convictions: one study showed that 15 to 25 percent of exonerated prisoners had confessed to crimes they hadn't committed. Police officers are taught to believe innocent people never confess, but that's obviously not true. Yet, rather than changing the methods they use to interrogate suspects, they argue that such mistakes are merely a small exception to the rule.

The criminal justice system is so ingrained in its patterns that it's hard for officers and lawyers to revise their error-prone methods of investigating crimes. And without reform, some mistakes by the police can even become endemic.

One 1989 study found that, in Suffolk County, New York, police officers routinely brutalized suspects, illegally tapped phones and faked evidence to obtain wrongful convictions.

This kind of "ends justify the means" mentality can also be seen by the emergence of the new word "testilying," referring to the wide-ranging falsification of evidence by police officers in the early 1990s.

To clear away the mistakes that arose from self-justification, a dramatic reform of the criminal justice system is needed.

For example, new technologies like DNA testing have overturned some wrongful convictions, which has lead to reforms such as recording police interrogations on video and teaching police officers, lawyers, and judges about the dangers of confirmation bias.

### 7. Self-justifications and blame can damage or even ruin relationships. 

Our desire to avoid cognitive dissonance by finding self-justifications for our mistakes can undermine our relationships.

That's because one common way of avoiding cognitive dissonance is by shifting blame for mistakes onto our partners. Naturally, this engenders conflicts between us and our partners, and conflicts have in turn been shown to increase the likelihood of a break-up. This was evidenced by a study of couples that indicated that once the ratio of time in a relationship spent on good terms versus arguing goes below five to one, the relationship was unlikely to last.

And although not all relationships were meant to last anyway, coming up with self-justifications for why the other person is to blame can even ruin relationships where reconciliation would have been possible.

Because when couples don't confront their problems honestly, they descend into the so-called "blame game," where they refuse to accept their own mistakes by pointing out the perceived flaws and mistakes of their partners.

A study of more than 700 couples spanning many years found that self-justification leads to name-calling, refusal to compromise and, eventually, mutual hatred.

So how is it possible to get out of such a downward spiral?

The answer is simple: we shouldn't judge our partners personally for the mistakes they've made; the person and the mistake are not the same thing. In successful relationships, couples recognize that there's a difference between the mistakes made and the person who made the mistakes.

When you're criticized personally rather than for your actions, it generates a stronger sense of embarrassment and isolation.

That's why we we should give our partners the same lenient treatment that we often give ourselves: praising them for doing something good and blaming situational factors when they've made mistakes.

### 8. Even governments resort to self-justification, and this can lead to escalating conflicts. 

It turns out that, just like individuals, governments are also reluctant to admit their mistakes. Instead, they seek self-justifications for their actions. This is especially true in a crisis situation.

In a crisis, e.g., when under the threat of war, governments often create self-justifications for their erroneous actions by blaming their enemies for having "started it."

For example, when U.S. diplomats were held hostage in Tehran during the 1979 Iran hostage crisis, both the United States and Iran tried to justify their actions as being mere responses to the other side having started the conflict.

And, just as with relationships, when governments blame each other, it only mires all parties deeper in the problem. Because blaming the other party for inflicting the first wrong can serve as a justification for unnecessarily escalating the conflict.

This could be seen on an individual level in an experiment where people were told to exert pressure on each other's index fingers, with the pressure levels being monitored. Each party was asked to return the pressure applied by the other person, but it turned out that the levels of pressure applied kept increasing. Both sides justified the increases by saying the other side was responsible for the increase.

The same phenomenon can be seen in the Israeli–Palestinian conflict: both sides justify their escalating acts of violence by claiming that the other side is the aggressor and that they're only protecting themselves in response. Instead of accepting their own past mistakes, both sides focus on faulting the other.

Fortunately, however, once parties start accepting their mistakes, the tendency spreads and can eventually resolve the conflict.

For example, after the unjust system of Apartheid ended in South Africa, a commission was set up where both perpetrators and victims could openly discuss the violence of Apartheid and their own involvement in it without the perpetrators having to worry about vengeance. This allowed the perpetrators to move past their denial and other self-justifications so they could admit their mistakes. This helped both parties move towards reconciliation.

### 9. Only by admitting our mistakes can we grow professionally and personally. 

Most people have difficulties admitting when they've made a mistake — it's only natural. But, as we've seen, this tendency can be destructive, so here are a few tips on how to start admitting mistakes.

First of all, understand that admitting mistakes is not a sign of stupidity or weakness, but a great way to learn from them and resolve cognitive dissonance.

This was seen in one study comparing the educational techniques used in the United States, China and Japan. The study aimed to understand why Asian students performed better in mathematics than their non-Asian peers, and it found that U.S. students were embarrassed about making mistakes so they felt reluctant about working on difficult math problems in front of their peers. Their Asian peers, on the other hand, tended to learn precisely by making mistakes first, and then working to overcome those mistakes, even in front of the class.

Second, open yourself up to criticism from others and to the evidence that may show you've made a mistake.

This is beneficial because people respect, and are more likely to reward, those who own up to their mistakes.

For example, when the space shuttle Columbia exploded on re-entry, the NASA launch integration manager Wayne Hale took full responsibility for it. Instead of being fired, he was promoted for his honesty.

Similarly, anecdotal evidence suggests that if doctors make a mistake, they're better off admitting it, as it makes the patients' relatives less likely to sue for medical malpractice.

Another example is provided by President John. F. Kennedy, who apologized to the nation for the Bay of Pigs disaster, a botched invasion of Cuba, and saw his popularity soar after the apology. It's worth noting that he was the last U.S. president to apologize for making a mistake.

### 10. Final Summary 

The key message in these blinks:

**Not admitting your mistakes and instead finding convoluted self-justifications for them can severely damage not only your personal and professional life, but society as a whole.**

What are the questions these blinks answer?

**Why and how do we justify the bad decisions we make?**

When we make a mistake that is inconsistent with our worldview, it creates an unpleasant feeling of cognitive dissonance: we cannot reconcile the mistake with the idea in our heads. This leads us to conjure up self-justifications that seem to bolster the action we took. Once we commit to these self-justifications, they are near impossible to shake.

**What are the harmful effects of self-justification?**

In both medicine and the legal system, coming up with self-justifications prevents people from correcting mistakes they've made, whether misdiagnoses or miscarriages of justice. In romantic relationships and even the relations of nations, self-justifications can cause escalations in any conflict, which can in turn lead to disaster.

**How can we recognize our mistakes and begin to take responsibility for our actions?**

By being open and honest towards others and accepting new evidence, we can see our mistakes and learn from them more easily.
---

### Carol Tavris and Elliot Aronson

Carol Tavris and Elliot Aronson are social psychologists and lecturers. Carol Tavris has authored or co-authored several books, including _Anger: The Misunderstood Emotion_ and _The Mismeasure of Women_. Elliot Aronson has also authored and co-authored books such as _The Social Animal and Nobody Left to Hate._

