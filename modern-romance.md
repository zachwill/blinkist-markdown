---
id: 56cc51ea9bb21600070000b2
slug: modern-romance-en
published_date: 2016-02-24T00:00:00.000+00:00
author: Aziz Ansari and Erik Klinenberg
title: Modern Romance
subtitle: None
main_color: FAC6DA
text_color: 7A616B
---

# Modern Romance

_None_

**Aziz Ansari and Erik Klinenberg**

The internet and modern technology have revolutionized the way we communicate, learn, work and live. They've even revolutionized our concept of love. _Modern Romance_ (2015) explains how our idea of "love" has changed in recent generations, and how you can make the most of today's technology in your quest to find it.

---
### 1. What’s in it for me? See why love in the smartphone era presents totally new challenges. 

Did you find love? Did your parents? Your grandparents? We are all looking for love. We always did. Or did we?

Chances are that your grandmother and grandfather found each other under very different (and simpler) conditions than the ones that are currently available to you. But how exactly has the quest for a life partner developed in the past century?

Looking at love from generations ago all the way up to the smartphone-dominated dating scenes of today, these blinks have some answers.

In these blinks, you'll also learn

  * what made your grandmother "neat";

  * where people found love in Philadelphia in 1932; and

  * how your smartphone will find you love.

### 2. A few generations ago, people married younger, usually to someone who lived near them. 

Romance was quite different a few generations ago, for a number of reasons. 

For one, people didn't have mobile phones and there was less access to public transportation. So they were more likely to find love in their immediate surroundings: their city, their neighborhood or even their building. In fact, in 1932, one in every six people in Philadelphia married someone from their own block. One in eight married someone from their building.

Compare that with today. How many married couples do you know who met because they lived close to each other? Probably not many. Planes, trains and smartphones have allowed to us to form and maintain serious relationships with people in other cities, states, countries or even continents. 

In the recent past, people also got married at a much younger age — and their parents often had a big say in it!

"Adulthood" was thought to start earlier back then, so people were expected to get married earlier too. In the United States, for example, up until the 1970s the average age of marriage for a man was 23. The average age for a woman was 20. 

Women rarely had access to education as they often left their families to go straight into a new family life with their husbands, at an age when their interests and personalities weren't even fully formed. And their parents often had a big influence on their choice of husband. They sought a man with a respectable job and a good reputation. 

So our grandparents and great grandparents cast a smaller net and married younger, but interpersonal aspects of relationships were different too. Read on to find out more.

### 3. Marriage used to be about raising a family – today it’s about finding true love. 

Why do people get married? Not always for the same reason. 

When people were pushed into marriage earlier, they wed for simpler reasons. It was partly just societal pressure: getting married was just what you were supposed to do when you reached a certain age. 

When the author asked older people why they got married, many of them gave simple answers like, "She was a nice girl," or "He had a secure job." That was enough back then.

Women mostly wanted a man who could support a family and men wanted a woman who could raise children. So they teamed up to take care of a family with clearly defined gender roles. 

People look for something deeper than that nowadays. We've also let go of traditional rules about gender. 

Today, we imagine marrying our "other half" or "soul mate" — not just someone who can cook or has a steady job. When you ask modern newly married couples why they chose their marriage partner, they usually give big answers like, "He made me a better person," or "She's the best friend I've ever had."

So people choose to get married when they've found someone they feel completes them, not out of societal pressure to start a family. 

Gender roles are far more fluid today as well. Women are much freer to work or study if they choose, so they don't have to rely on finding a husband to support them financially. They can delay marriage till they've found someone they truly love. Men, on the other hand, can stay home and be supported by their wives if they so choose.

### 4. We now put more romantic pressure on our partners than people did in the past. 

Marrying for love might sound like a wonderful idea but it puts a lot of pressure on a person. 

We now expect our partner to fulfill _all_ of our needs, whereas those needs might have been spread across a whole village in the past. We look to our partners for security, fun and good sex. We expect them to be familiar and reliable, but also entertaining and surprising enough to keep things interesting.

Just imagine your perfect partner. You probably want an intelligent, funny and entertaining person who can be your best friend and your best sex partner. That may sound ideal, but it's a lot to expect from just one person — especially when they're expecting the same of you!

The process of finding the right partner is also a lot more complicated than it was before. In the past, Betty could meet Tom at a school prom, then they could go to a concert and they might be married a few months later. 

That's because people were looking for security, stability and to conform to societal norms: much easier goals to achieve. We want a lot more from marriage today, so it's much harder to find the right person. 

This is further complicated by the fact that we now have access to a much wider range of partners. Apps like Tinder give you instant access to hundreds of single people. Former social limitations like location, age and race matter less and less in the world of modern romance. 

This has its advantages but it also makes it more difficult to settle down with one person, because you always wonder if there's someone better out there. And you can take as much time as you like — we're no longer under social pressure to get married before the age of 25.

### 5. Use online dating sites and apps to connect with people, then go out and meet them! 

Online dating can be frustrating and feel futile. But don't give up! Online dating is great if you do it right.

First off, don't spend too much time online. Use dating sites to connect with people, then go out and actually _meet_ them. 

Studies have shown that Americans spend an average of seven and a half hours per day in front of screens. For Europeans, it's five to seven hours. That has a big impact on how we find love: between 2005 and 2012, one-third of Americans claimed to have found love through the internet.

Not everyone's so lucky, however. A lot of people who try online dating spend more time in front of the screen than they do on actual dates. 

Consider the story of a 25-year-old named John, for instance. He spends hours on Tinder but never meets any of the women he flirts with — he just chats with them and feels frustrated with being single. 

Don't fall into that trap. Use Tinder and similar apps just to make appointments to meet in person.

Also, remember: the people you chat with are real people. Don't think of dating apps as a safe space to write whatever nonsense comes into your head — there's a human behind every profile you see.

So don't send offensive texts like "Nice tits, baby." Would you say that to a stranger in real life? (Hopefully not!) Speak to people on dating apps just as you would in the regular world: be yourself, be polite and be personable. 

Don't send out a generic mass message to a wide range of people, either. No one likes to receive spam. Instead, look for a person with whom you share interests, and talk about the things you have in common.

### 6. Focus on a small number of people and invest more time in getting to know them. 

Have you ever gone on a date, but thought early on "Nope, this person isn't for me" and wanted to get out of there? 

First impressions aren't always correct, however. Humans are complex creatures; it can take a while to get an accurate reading of one.

It's hard to stay focused on one person when romantic options are nearly endless, so we tend to lose patience when dating around. We sometimes write a person off after just an hour or two — or even less! That's quite harsh, considering how complicated each individual is. 

Imagine you're on a date and your partner is very quiet and nervous. You might be tempted to dismiss them as boring or shy, and move on to the next person. But maybe they're going through a difficult period and just need some time to warm up to you before they're confident enough to be outgoing. Why not give them more of a chance?

Don't go on too many dates, either. Spend time looking for people you really have something in common with, then focus on them. You'll exhaust yourself and run out of steam if you try to date every single person you match with on Tinder.

There's no rule that online dating has to involve going on lots of dates. So instead of setting up ten dates as soon as you start, why not set up three and focus on those people for a while before you try anyone else? If you're looking for something serious, it's worth spending more time on each person.

### 7. Final summary 

The key message in this book:

**Nowadays, we expect a lot from our romantic relationships — our partners have to make our lives feel complete, as well as being companions with whom to set up home and potentially raise a family. The internet has changed the way we find love, opening up a wide pool of potential partners, which can make us reluctant to commit — especially when we're expecting perfection. So if you want to try online dating, focus on a small number of people with whom you share meaningful similarities.**

**Suggested further reading: _30 lessons for Loving_ by ****Karl Pillemer**

**_30 Lessons for Loving_ (2015) shares advice from hundreds of elderly people to reveal the secrets of building a long-lasting relationship, from first encounter to "happily ever after." You'll learn how to tell if your current crush might be "the one," how to communicate in a healthy way and how to keep the passion alive in a long-term relationship.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Aziz Ansari and Erik Klinenberg

Aziz Ansari is an actor, writer and comedian widely known for his role in _Parks and Recreation_. _Modern Romance_ is his first book.

Eric Klinenberg is an American sociologist and author. His writing has appeared in publications such as the _New York Times_, _Rolling Stone_ and the _Guardian_.

