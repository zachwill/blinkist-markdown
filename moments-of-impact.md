---
id: 54eaf9c5393366000ab90000
slug: moments-of-impact-en
published_date: 2015-02-27T00:00:00.000+00:00
author: Chris Ertel and Lisa Kay Solomon
title: Moments of Impact
subtitle: How to Design Strategic Conversations That Accelerate Change
main_color: CD2027
text_color: CD2027
---

# Moments of Impact

_How to Design Strategic Conversations That Accelerate Change_

**Chris Ertel and Lisa Kay Solomon**

_Moments of Impact_ gives leaders the tools to turn their boring meetings into environments that generate fresh, new, innovative solutions. It tackles every aspect of these conversations, from the environment to the attendees, and offers practical advice on how to make them as insightful as possible.

---
### 1. What’s in it for me? Discover the key to facilitating more productive meetings. 

You're in a bland hotel conference room with someone talking vaguely in your direction, or fiddling with the dry erase markers on a table while waiting for someone to set up the Powerpoint projector. Meanwhile, opportunities are passing by your company and all you can do is grin through your teeth.

No more! There's a better way to deal with meetings and conferences. Let's call them _strategic conversations_ and recognize them for what they are: opportunities. These strategic conversations are opportunities for your organization to get its greatest minds together and start tackling problems.

These blinks show how to take advantage of strategic conversations. That means big picture things like figuring out what the purpose of a meeting should be, as well as details, like making an agenda.

This pack will make you a better leader and cause you to shake your head at all the terrible meetings you've attended!

After reading these blinks, you'll know

  * when meetings shouldn't occur in the same old conference room;

  * how to avoid invisible gorillas; and

  * who to invite in order to have the best meeting.

### 2. Being able to lead great conversations is the most important leadership skill. 

What is the most important quality in a leader? Is it her determination? Her creativity? Or maybe it's her ability to rally her people together behind a common cause.

Actually, it's none of the above. A good leader is defined by her ability to design _strategic conversations_ ; that is, helping others channel their creativity and finding great solutions to her company's obstacles.

Among her most useful skills are the ability to plan a meeting and steer conversations.

Have you ever sat in a meeting or a two-day conference that promised to improve your skills, only to find yourself brought down by tedium and boredom?

While those meetings may have been meticulously planned, they missed one crucial thing: they didn't pay attention to their audiences and didn't help them work effectively.

As a leader, you want to organize and lead strategic conversations that make everyone's work as satisfying as possible. It will allow you to guide your employees towards enlightening moments, great problem-solving strategies and newfound creativity.

Unfortunately, many leaders — even well established ones — never learned to properly lead a strategic conversation. Despite its usefulness, you won't find courses that teach strategic conversations in most universities or other educational institutions.

For example, the authors recall a time when they received a desperate phone call from a senior executive who was planning a two-day conference. He had noticed about eight days before the actual event that, while he had planned the location and the topic, he had _no idea_ how to successfully steer those meetings to generate potent discussions and the effective solutions.

Despite his top-notch education, he felt totally lost, and ended up having the authors fly out to help him prepare. After reading these blinks, you'll have all the tools you need to design effective strategic conversations — and you won't have to pay for anyone's plane tickets.

### 3. Designing strategic conversations requires you to go above and beyond the basics. 

We all know that in order to plan a successful event, we need to get basics right: the venue, the participants, topics and the guests. But what should you be considering beyond those essentials?

To answer this question, we'll have to look at what defines a strategic conversation in the first place:

_Strategic conversations_ are all about creating a shared experience for every participant, in which the most pressing issues are explored openly to come to the smartest ways to proceed.

It means carefully examining your own assumptions and prejudices, rather than simply accepting them, and creating a space in which different perspectives and opinions are freely shared and accepted as legitimate.

Strategic conversations allow you to engage with entire groups on a deep level that most leaders couldn't dream of.

For example, rather than simply boring your staff with figures and forecasts during your quarterly update meeting, you could play on their assumptions about the business in order to create "aha moments."

Strategic conversations only become more important when the topics of these dialogues are ambiguous.

For example, if you're leading a meeting on a nebulous topic such as "How do we increase sales?" then you will need to be both creative and strategic to fight against the mediocrity that so easily arises from ill-defined meetings.

To look at this in action, we'll turn to one of the most successful companies in South America, Natura. They once had a meeting about brand values led by the company's Vice-President of Organizational Development and Sustainability Marcelo Cardoso. It was largely unsuccessful. Where did Cardoso go wrong?

He tried to treat this meeting like any other, despite the grand nature of "brand values" as a topic. By doing things like sticking to a schedule, keeping to the agenda and even using the same location for his meeting, he allowed his coworkers to relax into their default mode: bored, uninterested and uncreative.

But how exactly can you devise these strategic conversations? Our following blinks lay out the core principles of leading conversations.

### 4. Be clear about what you want to achieve. 

Your company is faced with a dilemma: one of your competitors is stealing some of your top employees! What do you do?

Your old routine won't suffice; if it did, your staff wouldn't be leaving. So, you'll have to devise new strategies.

These challenges, i.e., _adaptive challenges,_ require time to solve. Unlike smaller problems, adaptive challenges like aggressive competitors or a new and unexplored market opportunity will take at least a few months — if not a few years — to adapt to and solve.

In these cases, it's extremely important that you understand the scope of the challenge so that you don't allow yourself to get caught up in other people's haste. The last thing you want is to make a snap decision that simply destroys your plan before it even gets off the ground.

In the process of solving adaptive challenges, you'll likely be involved in many conversations that address the many diverse aspects of the problem. Addressing the different aspects can be confusing and it is important to have targeted discussions to address to details, as well as the broad topics. How can you make sure to cover all the important points?

One way is to set goals for each meeting to keep them free of distractions. By defining one clear purpose for each meeting or conversation, you define exactly where you want to be at the meeting's end.

If people are allowed to, they'll lose themselves in the details, so keep them focused. Ask precise questions and be clear about who is responsible for implementing the decisions. Questions like, "Can your team provide five actionable project suggestions by Wednesday?" is much easier to follow up on than, "Can you send me some project ideas?"

If people meander off track and start talking about things irrelevant to this meeting's purpose, you can simply remind them: "Good point, and let's table that for another meeting. We still need to solve the problem at hand."

### 5. Engage different parties and perspectives in your debate. 

At this point, you might think you have what it takes to face adaptive challenges. Unfortunately, however, it's not all about you. If you want to solve adaptive challenges, you'll need to surround yourself with the right people and perspectives.

This team should be comprised of people from diverse areas of expertise and backgrounds who will help you make the most of your meetings. Strategic conversations are only as good as the people involved, but the people you should involve aren't always who you might expect.

For example, if you're hosting an annual marketing convention, don't invite the usual "must-invite" team of ad companies, marketing departments and staff. Try a different route:

Think about how your company doesn't consist of a single department. Rather, it's made up of a multitude of different people and perspectives that aren't always aware of one another. Mix things up by inviting people with diverse perspectives and different areas of expertise, and embrace the diversity of your team.

Eamonn Kelly, Head of Strategy of Scottish Enterprise, would have benefited from those diverse perspectives when he arranged a meeting between various departments, including entrepreneurship, services and business infrastructure departments.

While the departments brought valuable thoughts and perspectives, they lacked the communication necessary to optimize sharing their knowledge.

To demonstrate to them the importance of diverse perspectives, Kelly sat the parties down and had them write a list of what they _gave_ to other departments and what they _got_ from them.

Unsurprisingly, when he hung the lists next to each other, his staff was surprised to see that _each_ department thought they gave more than they got. This realization actually made the participants want to learn more about including the other teams in their tasks.

> _"Diversity trumps groupthink."_

### 6. Frame your conversations so that they focus on the big picture. 

Have you ever heard of an invisible gorilla? They exist. Seriously!

At least, that's how it feels after watching one social experiment in which participants viewed a video of two basketball teams passing a ball back and forth. The participants were told to focus on the number of passes the white team made while ignoring the black team.

In the middle of the video, a woman in a black gorilla costume walked onto the court. You would expect people to react with dismay, yet over half of them didn't even notice it. Why? Because we sometimes ignore what's happening right in front of us when we're focused on something else.

Likewise, you don't want your coworkers to miss the "gorilla in the room" by focusing on irrelevant topics. As the _strategic conversation designer_, you are responsible for assembling the relevant content for your meeting without overwhelming the attendees.

Tell them exactly what they should be focusing on. If you're in a meeting about developing a new marketing strategy, don't let people wander off topic and talk about their department's latest progress unless it's absolutely relevant.

Be careful with your framing. Poor framing can lead people to focus too much on small stuff, like numbers and data, and miss the big picture: progress.

One of the biggest mistakes leaders make is framing too broadly or with too much familiarity. If you paint in broad strokes, you run the risk of having your meeting easily getting derailed, and if you focus on the same familiar topics over and over again, people will become bored and unmotivated.

Don't make your staff think _outside_ the box; have them think in a _different_ one. Make them question their assumptions and go into creative mode!

> _"Bad frames can blind you to important signs of change."_

### 7. Set the scene properly and use your venue to be productive. 

Once you've assembled your team and set your goals, it's time to decide where your conversation should take place. Are you content with the same old, dusty conference room? Or is it time to pull people out of their routine and stir their creativity?

The decision is an important one, because your venue of choice offers immediate clues about how your meeting will go.

Every time you walk into a meeting room, you will pick up subtle cues about the dynamics of the meeting. Even tiny things like the arrangement of the chairs at the table can have major implications:

Are chairs and tables arranged with one big, comfortable chair at the head of the table? This could reinforce hierarchy. Or is the setup such that it suggests collaboration?

Unfortunately, most leaders plan their meetings by considering everything except the environment. They forget the importance of people's first impression as they enter the room and the expectations that are built upon those impressions.

If you create an environment that looks like a "standard, nondescript meeting," then people will be less likely to work creatively and develop groundbreaking new ideas.

So how do you arrange your meeting room in a way that fits your conversation?

One way to is create a space that doesn't feel like it's part of the routine. Ensure there is plenty of natural light, and make sure there is enough space so that people don't feel cramped or stifled.

Give them some clear wall space to hang up notes, and make sure that your environment is as free from distractions as possible.

If you can create a great meeting space for your strategic conversations, people will feel more motivated and appreciated, and will be more likely to let their creativity flow.

### 8. Your primary goal is to create an experience. 

Sometimes people make agendas as if they're planning a march through the country. But they don't have to be so over-the-top. If you want to make your meetings memorable, simply find the balance between the content and the attendee's experience.

Organizers of large events often see the "experience factor" as being a nice bonus — a small side dish to the main course of content. But why? Simply put, they just aren't used to thinking any other way.

But you want people to _remember_ the content, and that means creating an experience. Attendees should see your meetings as a psychological, emotional and intellectual journey, not a five-hour snooze.

Ask yourself what kind of experience your group needs. You could employ unconventional methods, like war games exercises, more traditional role-playing workshops, or customized simulations. Ask yourself: What will engage the people around me the most?

Also, be sure to take a step back and let your team discover the solutions. The reason they're there in the first place is to work on problems and discover solutions and new ideas for themselves. They shouldn't just be accosted with bullet-point laden power points by speakers who ramble through a script.

Try to engage people in their entirety. In other words, don't just include the sober, rational and stiff stuff. Instead, they should be engaged with heart and guts in your strategic conversation.

Make them feel challenged! Turn it into a game! If you want your business to pull ahead, you're going to need more than a list of numbers and bullet points.

### 9. Make your impact. 

The goal of all of these strategies is more refined than simply "finding solutions to your problems." They aim to solve problems in a specific way — by creating _moments of impact_ — that changes a company's trajectory forever.

Your job, then, is to enable your dream team of professionals to have these life-changing moments of impact — insights that are so compelling that they _demand_ immediate action.

Having created a space of hope and creativity for your group, you will have given them the unique experiences that free the mind and allow for creative new directions for your company.

Thus, you want to make sure that your strategic conversations offer more than a vague sense of progress — the future should feel tangible to your conversation participants.

To get the participants to switch from vague ideas to tangible action, you'll need to pull them away from their worries and reservations and open people up to possibilities. As with any new revolutionary idea, many conversation participants will look at the ideas generated at your meeting with a skeptical "yeah, but..." attitude; it's important that you counteract that skepticism.

By giving people a taste of the future, you can start to shift their thinking. Although it won't totally snuff out people's doubts and skepticism, it could deprive their doubts of oxygen long enough to get them in a motivated and creative mindset.

But fighting skepticism isn't enough. We want long-lasting change! You'll need to bring your conversation participants into the _hope loop_, where people see beyond their achievements and crave more action.

For example, when the Rockefeller Foundation organized a conference about socially conscious investments, they followed the principles of these blinks to create moments of impact. As they had intended, conference attendees took action after the conference, launching several new projects, such as the Global Impact Investing Network.

### 10. Final summary 

The key message in this book:

**The defining quality of a great leader is her ability to lead productive conversations. The participants are engaged, interested, open and have an opportunity to express themselves genuinely. Not only are these strategic conversations interesting, but they are the cornerstone of successful business growth.**

**Suggested further reading:** ** _A More Beautiful Question_** **by Warren Berger**

_A More Beautiful Question_ highlights the importance of finding the right questions in our search for success. With the help of cutting-edge research and examples from the business world, this book defines the "beautiful" question and shows you how to start asking such questions yourself.

**Got fee** **dback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Chris Ertel and Lisa Kay Solomon

Chris Ertel advises senior executives at Fortune 500 companies as well as NGOs and government agencies, and has been designing strategic conversations for over 15 years.

Lisa Kay Solomon teaches innovation at the MBA in Design Strategy program at San Francisco's California College of the Arts.

