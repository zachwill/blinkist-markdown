---
id: 565c7f9b990675000700005b
slug: moms-mean-business-en
published_date: 2015-12-02T00:00:00.000+00:00
author: Erin Baebler and Lara Galloway
title: Moms Mean Business
subtitle: A Guide to Creating a Successful Company and a Happy Life as a Mom Entrepreneur
main_color: 7D1952
text_color: 7D1952
---

# Moms Mean Business

_A Guide to Creating a Successful Company and a Happy Life as a Mom Entrepreneur_

**Erin Baebler and Lara Galloway**

_Moms Mean Business_ is a guide to time management for mom entrepreneurs. These blinks help you discover where your true priorities lie, and provide you with planning techniques that will make it possible for you to dedicate more time to your ambitions and yourself.

---
### 1. What’s in it for me? Become a better and happier mompreneur. 

Today, American women are launching twice as many businesses as men — and many of these women are mothers. While these blinks are useful for all entrepreneurs, they offer especially valuable advice for mom entrepreneurs. You'll learn how to make sure you meet your own goals and not be bound to another person's agenda. 

These blinks also give advice on how to budget your time despite the various challenges which mom entrepreneurs encounter — and on how to find time for all the activities you love most. 

In these blinks, you'll also find out

  * about an inner voice you should ignore at all costs;

  * what a _twofer_ is and why it's so helpful for your time management; and

  * why it's actually a good idea to overschedule certain activities.

### 2. Eliminate unnecessary pressure by recognizing your true priorities. 

Today's entrepreneurial mother faces a host of challenges on her way to success. The wonderful wife, the caring mother, the exemplary hostess and the fierce business women: there's no end to the roles mothers are expected to juggle _and_ perform to perfection. 

Women quickly internalize unbelievable social pressure like this and become their harshest critics. But there's no sense in it. Being a mom entrepreneur is challenging enough — why put extra pressure on yourself? 

As a mom entrepreneur, you need a business plan that takes your needs and circumstances into account. You have a finite amount of time on your hands. And if you want to use it well, you need to separate other people's expectations from the things you really care about. 

Start by making a list of the top five things that matter to you. These could be your _values_, or things that you aspire to, such as independence, integrity and authenticity. _Motivators,_ the things that get you out of bed everyday, are also worth noting. These could be your children, a sense of autonomy or the opportunity to help others. Your passions, whether they're solving complex math problems or going hiking, also deserve a spot on this list. 

Now, take a look at your list. These top five things should be what you put most of your time and energy into. But do you really pull that off? 

All too often mothers find themselves sacrificing their time to complete tasks that aren't truly important to them, like maintaining a spotless household. This is the result of unnecessary self-pressure. By recognizing that your priorities lie elsewhere, you'll feel this pressure lessen almost immediately.

### 3. Chase what’s important to you right now, not what was important to you ten years ago. 

What do you think of when you hear the word _success_? Do visions of money or status come to mind? Do you see yourself as that hotshot entrepreneur you've always admired? If so, it's time to put those dreams away. Instead, create a vision of success that fits with your own life. This kind of success is sustainable and feels truly worthwhile. 

Before motherhood, many women dream of scaling the corporate ladder, traveling often and coming home to a nice house and car. But if they haven't achieved this after motherhood, they often feel frustrated, resentful or even angry. 

The reality is that becoming a mother changes your life profoundly. You may no longer have the energy or the time to work ten hours straight, like you used to. But so what? That doesn't mean you can't achieve success. The trick is to aim for a goal that is aligned with the top five things that matter to you _right now_, not ten years ago. 

Perhaps you loved to work out before you became pregnant and took great pride in how much you could bench press. And now that your child is six months old, you're ready to start working out again. But after a week at the gym, you realize that you simply don't have the energy anymore. 

Before you start criticizing yourself, take a look at your top five again. Is it really one of your priorities to look super fit and toned? Probably not. Realizing this will annihilate those unnecessary feelings of guilt. By keeping your priorities in perspective, you'll have more time to focus on what you really value.

> _"Spending the time on the things that are most important to you will make you feel as though you have the time you need."_

### 4. Designate time slots to each of your priorities and make room for the unexpected. 

As mothers, time is our most precious and most scarce resource. Despite this, it seems like we only ever seem to lose track of it. But things don't have to be that way! Let your priorities be your guide to effectively manage your time. 

It's time to go back to basics: grab a physical calendar so you can start treating time as currency. Every day offers you 24 hours to spend. So what are you going to spend it on? 

Sleep, chores, cooking, childcare and time for your romantic life should be automatically blocked off. Block off all time slots on the calendar that belong to regular family activities, such as morning routines, soccer practice or helping with homework. 

Next, put in your non-negotiable tasks: your work hours, time with your partner and time off. Now have a look at what's left. This time is yours to split between your business and yourself. Keep your top five handy and start filling in the free slots in your calendar accordingly. 

When you're arranging your time slots, there are a few things to remember. The first is that _energy levels fluctuate_ throughout the day. So don't try and schedule laborious tasks for the middle of the afternoon if you're usually sleepy then! You'll thank yourself for it later. 

Secondly, do your best to stay honest with yourself about when you need a break. You should never feel guilty about needing to relax! Having enough downtime will always make you more productive later, and that's a fact. So schedule some breaks and some time to reward yourself by doing what you love. 

Finally, don't forget that life inevitably gets in the way. Unexpected obstacles, whether it's a sick child or a minor accident, can be quite frustrating. Cope with them better by making room for them. How? By over-scheduling certain things. If you want to work out four times a week, schedule it for six. If something comes up, you can always use the next slot you've made for yourself to hit the gym.

> _"It's not a matter of if something will throw you off track, it's a matter of when."_

### 5. Self-care is vital, so make time for it! 

Burnout is real. Maybe you've even experienced it firsthand. If you want to avoid it, there's one thing you've got to make time for between your kids, your work, your relationship and your business: namely, self-care rituals. 

These are activities that you do because you enjoy them. But what if you just don't have room for "me time"? Short answer: you do. It's all about being smart with your scheduling. 

Kill two birds with one stone by scheduling _twofers_. A twofer means combining two self-care rituals to get double the enjoyment in half the time. You could hop on the treadmill while watching your favorite show on Netflix. Or run around the block while listening to your favorite Blinkist audio book!

You can also make a hitlist of the things that make you happy. Keep this list handy so that you know exactly what to fill an unexpected free slot with. Even little things, like a half-hour nap or a cup of ayurvedic tea will lift your mood more than you expect!

Once you've found time for a few self-care rituals during your week, start making time for more. But what if your slots are all full? 

Take a closer look: you might be losing time without realizing it. When was the last time you signed onto Facebook or Pinterest? More importantly, how long did you spend scrolling? Social media sucks up a lot of time. It's time to break the habit! Delete apps from your phone or log off to reduce the temptation and enjoy your newfound free time. 

Another way to free up your time is to let some things go. Learning to say no is a valuable skill, so why not practice it this week? If you don't help out with the school auction, no one will think less of you! Save your time for activities that bring you closer to achieving your goals. 

Finally, don't be afraid to ask for help! If you've got the funds for it, think about getting a virtual assistant or intern to help you out with menial tasks that you don't have time for. You don't have to go it alone! Remember: self-care isn't a luxury. It's a necessity — and one you'll truly be thankful for.

### 6. Turn your goals into reality with a concise, concrete plan. 

Mom entrepreneurs often call their business their baby. After all, you bring them into the world, raise them, celebrate them and eventually watch them take off on their own. When raising a child, this process works best when you let things happen naturally. But your business is a different story. As a mom entrepreneur, you have to create a plan for your "baby."

The plan doesn't need to be exhaustive. In fact, one page is all you'll need! So where do you start?

Determine your _vision_ : What need is your business fulfilling? Whose lives will you better through it? Next, work out your _mission_ : What motivates you to run this business? What principles are behind it? Finally, outline your concrete objectives. These goals should be SMART, that is, _specific, measurable, attainable, realistic_ and _time-related_. A SMART goal should look a little something like this: "In 2016 I want to earn an income of $85,000".

The next step for your one-page plan is all about putting your vision, mission and objectives into action. For this, you'll need strategies. If your vision is to give women aged 25 to 35 a product that will improve their lives, then you'll need to get in touch with them. You could create a targeted marketing campaign or establish a presence at local trade shows. 

To realize these strategies, you'll need resources. These should be accounted for in your plan, too. Ask yourself: What needs to be done next? You might, for instance, decide to hire a web designer to boost your company's online presence this month. 

You may feel a little daunted by your plan as you write it. After all, you're making a lot of promises to yourself here! If all the decisions to make and steps to take seem overwhelming, just think of all the resources you have. 

Your strengths, skills, personality traits, education, work experience, networks and support systems that you've developed and perfected over your life will stand you in good stead. Time to make use of them!

### 7. Boost your productivity as much as possible. 

So, you've made a killer business plan and time to take care of yourself. And yet ... something seems to be standing between you and the success you're aiming for. This would be a good moment to take a closer look at your working methods. 

Mom entrepreneurs need flexibility, which makes working from home quite the blessing. But it can also be a curse! If you find yourself doing the laundry during the time slots booked for work when a deadline is nearing, you've fallen victim to a form of productive procrastination that moms know all too well. 

The first step to ending procrastination is admitting that you're doing it! So stop pretending that sparkling windows are more urgent than your product proposal. If you've booked slots for housework, don't let it distract you outside of those times. 

When you're working on your business, always ask yourself how productive you really are. There's always room for improvement! It's worth checking out the latest apps created to help you boost your productivity in new and innovative ways. 

There are also a handful of old-school techniques that will serve you well. All you have to do is _chunk it, map it_ and _calculate it_.

Assign chunks of time to particular tasks: allow yourself just 20 minutes to reply to emails, dedicate two hours to a long work slot and reward yourself with 25 minute breaks. Segmenting your time in this way is a simple yet effective way to keep it manageable. 

If you've got a big project and don't know where to start with time management, then it's time to map it. Break your project down into smaller tasks and outline the time you'll need to complete each of them. 

Finally, get calculating with the 80/20 rule. This means you only deal with the 20 percent of tasks that give you the greatest returns. Armed with these tools, you're set to show the market just how powerful mom entrepreneurs really are!

### 8. Final summary 

The key message in these blinks:

**Being a mom can be tough, and being a mom entrepreneur even tougher! But with thoughtful planning and effective time management, you can achieve and sustain success that you really value. By knowing what really matters to you at this point in your life, you can align your projects to those priorities. That way, you can make sure all your time goes to what you love.**

Actionable advice:

**Whenever you're stuck, do something you love.**

Next time you're at your wit's end trying to balance your business with other commitments, take a step back. Give yourself some time out and perform one of your favorite self-care rituals. You'll be better able to reflect on your situation with a clear, relaxed mind. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Erin Baebler and Lara Galloway

Erin Baebler is an entrepreneur and public speaker. Her coaching company _Magnolia Workshop_ caters to the needs of businesswomen _._ Baebler contributed to _Chicken Soup for the Sou_ l and _Five Must-Know Secrets for Today's College Girl_.

Lara Galloway is an author, public speaker and founder of _MomBizCoach_. She has been featured in _Forbes_ and the _Chicago Tribune_. _Forbes_ lists her as one of the "Top 30 Women Entrepreneurs to Follow on Twitter".

