---
id: 54d9dc7b336661000a150000
slug: money-master-the-game-en
published_date: 2015-02-13T00:00:00.000+00:00
author: Tony Robbins
title: MONEY: Master the Game
subtitle: 7 Simple Steps to Financial Freedom
main_color: B79D48
text_color: 6B5C2A
---

# MONEY: Master the Game

_7 Simple Steps to Financial Freedom_

**Tony Robbins**

Do you want to master money, and make it work for you? In this book you'll discover the steps you need to take to achieve real _financial freedom._ Whether you're just starting your career or moving toward retirement, _MONEY_ offers sound advice from seasoned professionals on saving and investing so you can live the life you want.

---
### 1. What’s in it for me? Discover the steps you need to take to achieve financial freedom. 

What happens to couch potatoes? Sitting around all day doing nothing, they get fat.

Curiously, the opposite is true with money. Leave it sitting around, and you'll find that the total just gets smaller and smaller. To make that pile of dough grow, you need to exercise it, or make it work!

These blinks explain exactly how you can turn a small nest egg into a mountain of cash that will let you live your life exactly how you want to. If you invest your money in the right places, then financial security will follow. And if you're smart about it, you may never need to work ever again!

In the following blinks, you'll discover

  * how the seasons of the year can help you strategize your investments;

  * how much money you really need to save before you can quit your job; and

  * why believing in yourself is the best financial advice anyone can give you.

### 2. Compounding can ensure that your money keeps growing year after year. 

How hard is your money working for you? If you're like most people, your money probably hasn't been exercising at all. It's been sitting lazily in your bank account, growing only minutely — if at all.

This needs to change. We need to make our money work hard, especially as we won't be able to rely on traditional saving methods in the future.

You may imagine that growing your money is difficult. Maybe you're happy with your income, even if it's not enough to cover your retirement, because you assume you can rely on a pension or retirement fund to help you out.

Unfortunately, this isn't true. Many of the world's retirement systems are failing. In the United States, a 401k retirement plan was originally invented to supplement income in old age, but for many, it's the only thing they have to rely on.

Other pension plans were hit hard by the 2008 financial crisis, and those who paid into them lost a great deal.

You can avoid this if you let your money work for you, by _compounding_.

Compounding means letting your money develop year after year, by allowing interest to build up. Say you invest $100, and this generates a 10 percent profit. If you leave the investment untouched, you'll generate another 10 percent on $110 the year after, then on $121 the following year, and so on.

When Benjamin Franklin died in 1790, he left $1,000 to the cities of Boston and Philadelphia. He stipulated that it had to be invested and not touched for 100 years.

After that time passed, half a million was drawn from the account, and the rest was left untouched for another 100 years. By that time, the original sum had transformed into _$6.5 million_.

> _"Compound interest is such a powerful tool that Albert Einstein once called it the most important invention in all of human history."_

### 3. Always put some money into your investment fund each month, even if it's not a lot. 

So if you want to become financially secure, where do you start?

The first rule of financial security is simple: add money to your savings. If you don't, your situation simply can't improve.

Saving is never easy, but try not to think of it as a boring, sometimes painful pursuit. Imagine that you're adding to your _freedom fund,_ the base on which your financial freedom will be built.

Your freedom fund is like your own personal ATM, a place from which you can always withdraw funds. You won't have a lot of cash in the beginning, but you can build the amount gradually.

Think of it like climbing a mountain. At first, it's hard and you won't seem to be getting anywhere. When you reach the top, however, you'll suddenly realize why you worked so hard!

Adding to your freedom fund is so vital that you need to keep doing it, even when you think you don't have enough cash to go around.

You always can put a bit aside for your freedom fund. Do you really need to go out for dinner again this week? Can you order a pizza or cook for yourself instead? Make any small adjustments you can to be able to save more.

And luckily, the magic of compounding ensures that the more you add, the greater the returns you'll get.

Ultimately, you should aim to save 10 percent of your income, though that'll be difficult at first. Even if you can manage just 5 percent or less, however, you'll still benefit from the generated interest.

### 4. Don't fall for investment myths, but do your homework and research the best places for your cash. 

Any talk of investing automatically sets off alarm bells. You may think, "What happens if I invest in the wrong thing?" or "Should I get a professional to manage my investments for me?"

Financial professionals really don't know what's best for you or your money. Many people let stockbrokers manage their investment funds, but here's an important thing to remember. Your advisor gets paid, whether you profit or not. Their job is to sell you things, whether good or bad.

Other people invest in _mutual funds_, or combined pools of investment opportunities managed by a professional. Mutual funds often come with large fees attached, however. When you consider these fees and the average returns they earn, it's clear such funds aren't the wisest financial decisions.

There is one expert you can trust: a _fiduciary_. Fiduciaries are professionals who are required by law to have no other interests except your own (unlike stockbrokers). This means you can actually trust their advice.

You can learn to invest on your own, however, as long as you remember a couple of helpful rules.

The first rule is that you have to believe in yourself. If you have a fatalistic attitude when you try something, you're bound to fail.

Next, do your research. Don't fall into the trap of believing myths or blindly following what others do. Find out what you need for yourself.

You can also try learning about what other successful people have done with their investments, and see if you can do the same.

Finally, be cautious. Don't expect that you're going to beat the market, as very few do. Try your best, but know that there is no simple or magic path to success!

> _"If you want to change your life, you have to change your strategy, you have to change your story and you have to change your state."_

### 5. Do you have a financial goal? Do you want to just cover basic costs, or live the life of the rich? 

How much money do you think you'll need to feel completely free from financial stress? Do you need a couple hundred dollars, perhaps a few thousand, or even a few million?

The first thing you need to remember is to be realistic. Don't try for goals you can't actually achieve. Ultimately, your goals depend on you and what you want.

Here are five different goals to get you thinking about just how far you want to go.

_Goal one:_ Generate enough money from investments to cover your basic monthly bills, for things like rent, mortgage, food, energy and transport.

_Goal two:_ Generate enough to cover basic needs plus extra for fun things, like new clothes or entertainment.

_Goal three:_ Generate enough to secure your _financial independence_. This means living entirely on compounding interest and never having to work again. The average annual spending for an American adult is $34,688; so if you want to generate this amount each year, you need about $640,000 in your freedom fund.

_Goal four:_ Don't just get your investments to free you from work but make them _improve_ your lifestyle. Earn even more so you can go on better holidays or eat in nicer restaurants.

_Goal five:_ Achieve _absolute financial freedom_. This means having enough money to do anything you want at any time!

Think about these goals and determine which fits your dreams and financial aspirations.

If you don't have a plan, it's easy to feel overwhelmed or get lost in the details. When you know what you're aiming for, it's much easier to get there.

When you decide what amount you need in your freedom fund, you can start thinking about how you want to invest your money.

### 6. The path to financial freedom may be slow at first. But don’t give up; time is on your side. 

When you start your journey toward financial freedom, it'll be hard at first, but don't give up!

You _can_ achieve financial freedom, as long as you keep working at it.

Along your journey, you'll definitely encounter others who are saving more than you, and at times you may feel like your income isn't high enough for you to save.

But don't let these things discourage you! You have to ignore that inner voice that might tell you to give up.

Self-doubt isn't the only thing that might distract you. Another thing that often holds people back is short-term thinking. Many people _overestimate_ what they can accomplish in a year, but then _underestimate_ what they can accomplish in a decade.

If you don't meet your goals for your first year, keep working — you can still catch up over time.

As you keep working toward your financial freedom, keep a few tips in mind.

First, speed things up by changing your life and lifestyle. Don't wait until retirement to downsize. Do it now so you can save money on mortgage payments, heating and taxes.

Also, only invest when you know you'll get good returns, to ensure your compounding continues at a decent rate. A good rule to use here is to only invest when you expect returns of over five times the amount. Even if you fail to get those returns three out of five times, you'll still have earned enough.

Finally, always keep trying to lessen your tax burden. The average American pays 54.25 percent of their income in taxes. Make it a goal to reduce this.

### 7. To make the most of your freedom fund, diversify your investments and keep things balanced. 

So after you've saved enough in your freedom fund that you're ready to invest, what's next? 

The key to investing wisely is knowing how to diversify. You should invest in different financial products that have varying degrees of risk.

There are three areas, or _buckets_, where you need to concentrate your investments.

The first bucket is your _security bucket_. This is where you put investments that are the most secure, even if they aren't necessarily the most profitable. Bonds, for example, should go in this bucket. Bonds don't offer massive returns, but are also unlikely to lose value.

Next there is your _growth bucket_, which is for investments that are riskier. This is where you can earn big returns, but you can lose more, too. You might invest in _equities_, meaning stocks and shares. Many stocks beat the market average in the long run, but they may be volatile and lose value in the short term.

The last bucket is your _dream bucket_. This is where you put some of the profits you earn from your other buckets. Your dream bucket helps improve your lifestyle.

Remember: the entire point of achieving financial security is to spend your money in ways you enjoy. If you don't have a dream bucket, saving and investing is useless!

So how much should you put in each bucket? Well, it depends on your attitude, how risk averse you are, the strength of your freedom fund and what you're trying to get out of life.

Strive to keep everything balanced. As you earn and lose money, you'll need to keep constantly moving it around to ensure that each bucket has the optimum amount.

### 8. Take advice from smart investors to guide your path, but be sure to insure yourself against bad times, too. 

If you want to succeed in anything, it's a good idea to learn from people who have succeeded before you. 

Finance is no different. When you analyze and copy what other successful investors have done, you'll have a much better chance of reaching your goals.

Ray Dalio is a good role model. He founded _Bridgewater Associates_, the largest hedge fund in the world. Dalio's investment plan is known as the _All Season Allocation_. It's designed to help you make money, no matter what financial conditions you're facing.

The economy goes through different periods, just like the seasons of the year. All Season Allocation strategies guide you on how to make money despite the market's changing conditions.

Consider this investment mix, used by Ray Dalio: Put 7.5 percent of your assets in gold and 7.5 percent in commodities. Gold and commodities are often good investments, even during periods of high inflation. Then put 30 percent in stocks, especially during seasons of high growth in which you can earn more. Finally, put 55 percent into US bonds, which are very low-risk.

Also, no matter what method you're following, get yourself insured for the bad times. Convert some of your savings into _annuities_, or financial contracts where an insurance company guarantees future payments in exchange for immediate payments. This will help ensure that you get a guaranteed lifetime income.

When you are comfortable with All Season Allocation, and you establish your income insurance through annuities, you'll be able to reach financial freedom!

### 9. Final summary 

The key message in this book:

**Anyone can reach financial freedom if they're dedicated, willing to save and ready to follow the necessary steps. So keep at it, even if your progress is slow at first. Diversify your investments, seek advice you can trust, prepare for different financial "seasons" and get yourself insured. If you work hard, you can become the master of your money and live the life you really want.**

Actionable advice:

**Don't forget your ultimate goal: spending your money the way you want.**

Money itself can't bring you happiness, it's what you do with it that matters. Having lots of money in your bank account won't make you happy, but spending it the right way will. So remember that you're working to be able to spend it on experiences you enjoy, or that give your life meaning. Don't forget that saving itself isn't the point!

**What to read next:** ** _Awaken The Giant Within_** **by Anthony Robbins**

By putting what you've just learned into practice, you'll enjoy a greater control of your finances. But money isn't everything. In _Awaken The Giant Within_, Robbins goes beyond money and shows how you can improve all areas of your life. You start by making better decisions, changing your beliefs about yourself and about the world, and building better habits.

Robbins has a lot of valuable advice on how you can transform your life for greater happiness and success. To learn what they are — including how to use odd words to lift your mood — we recommend the blinks to _Awaken the Giant Within_.
---

### Tony Robbins

Tony Robbins is a bestselling author, entrepreneur and consultant. He's coached many influential personalities, including presidents, CEOs and celebrities, on business and financial strategies.

