---
id: 590630afb238e10008b85716
slug: moody-bitches-en
published_date: 2017-05-04T00:00:00.000+00:00
author: Julie Holland, MD
title: Moody Bitches
subtitle: The Truth About the Drugs You're Taking, the Sleep You're Missing, the Sex You're Not Having, and What's Really Making You Crazy
main_color: 9D2329
text_color: 9D2329
---

# Moody Bitches

_The Truth About the Drugs You're Taking, the Sleep You're Missing, the Sex You're Not Having, and What's Really Making You Crazy_

**Julie Holland, MD**

_Moody Bitches_ (2015) is your guide to the female body and brain. These blinks explain some of the reasons behind the emotions and fluctuating moods that women can experience and how they can better tune into themselves, embrace their feelings and their bodies.

---
### 1. What’s in it for me? Learn why women should be allowed to be grumpy. 

Women are told that they're grumpy and annoying quite a lot — when experiencing premenstrual syndrome, when menstruating, when pregnant, when recovering from pregnancy, when in love. If you're a woman, you've probably heard that you're being "moody" in other situations, too. Are you sick of hearing that?

Then this book is for you! Find out about the cycles and hormones that rule women's lives. They make them happy one moment, aggressive the next, then weepy and then suddenly hopelessly in love. And this is a good thing. These mood swings are related to hormones that help them be powerful women and caring mothers. Women and men alike should try to enjoy these moods as best they can, and, most importantly of all, try to accept them.

In these blinks, you'll discover

  * why having a latte after lunch isn't a good idea;

  * why women are entitled to be in a really bad mood quite often; and

  * why women shouldn't have to take medication that represses their feelings.

### 2. Women have feelings for a reason and don’t need antidepressants to fix them. 

A lot of women tend to chastise themselves for how they look and how they feel. But the reality is that women are biologically inclined to be moodier and more emotional than their male counterparts.

However, moodiness isn't necessarily a weakness. In fact, it can even be a great strength.

The hormonal ups and downs that women experience make them hyper aware of their surroundings, which, in turn, enables them to protect their families and to adapt to their environment.

Here's how:

These waves of moodiness are a result of the connections between female neurotransmitters and hormones, primarily _estrogen_. When estrogen levels drop, as they do during premenstrual syndrome (otherwise known as PMS), women become more emotional. They cry or get angry more often than usual. But such fluctuations also enable women to be vigilant, gentle or even aggressive, depending on what the situation calls for.

The downside is that this hormonal dynamic makes women more likely to suffer from anxiety and depression. Therefore, expressing these feelings by having a good cry is actually extremely important.

But since society is uncomfortable with the manner in which women tend to express themselves, women are also the main targets for antidepressants, so much so that doctors usually prescribe something as soon as a woman experiences depression.

Of course, antidepressants may well be indispensable for somebody who has suffered from depression for years. But they shouldn't be pushed on women who simply don't need them. Unfortunately, many doctors, in an attempt to fit in as many appointments as possible per day, often spend only a few minutes with each patient, prescribing pills that will "fix" them, instead of properly considering the situation and accepting that women sometimes need to be emotional.

But enough generalities. In the next blink, we'll get specific about what happens to a woman's hormones every month.

### 3. A woman’s menstrual cycle can be trying, but it offers a profound opportunity for emotional understanding. 

The menstrual cycle is somewhat enigmatic. During the first half of her cycle, a woman might feel hot and sexy, while the second half might feel like pure hell.

That's why it's essential for women to track their internal rhythm and understand their hormonal fluctuations. For starters, as briefly described before, the menstrual cycle is broken into two parts. The first is called the _follicular phase_ and is the period during which a new egg develops in the ovaries. During this phase, estrogen levels are high, causing women to feel a strong urge to attract a mate.

The second part is known as the _luteal phase_, which refers to the two weeks before the release of the egg from the follicle and the beginning of the period. The luteal phase is when the going gets rougher.

Instead of estrogen, progesterone begins to dominate, making many women feel cranky and lethargic. That's why women often feel moody or depressed during their periods.

Sinking estrogen levels also cause drops in _serotonin_, a neurotransmitter that helps regulate moods. In fact, low serotonin is a primary cause of PMS.

The intense emotions that women experience during their periods may seem exaggerated — but both these feelings and the need to express them are genuine. This also has to do with hormones; when estrogen levels are high, women can more easily hide their sadness and anger, but as estrogen drops, women are exposed to their feelings and given an opportunity to explore what's bothering them.

So if, during her period, a woman feels like her partner doesn't appreciate her enough, it may well be that she truly feels that way.

### 4. Women in love are affected by all manner of chemical changes in their bodies, some of which produce odd behavior. 

Have you ever noticed that a woman in love can act a lot like an obsessive, delusional junkie? Well, there's no reason to be concerned about such seeming madness. This reaction is in fact perfectly normal.

The behavior exhibited by women in love can be influenced by natural chemical messengers with effects as strong as pharmaceutical drugs. These chemicals are called neurotransmitters and their job is to send messages from one nerve cell to the next. When they're stimulated, they can produce all manner of physical and mental symptoms.

Just take _dopamine_, a neurotransmitter that's sometimes called "the motor of the mind." It's the essential chemical element in attraction, and it makes people feel that falling in love is going to be a beautiful and pleasant process.

But when dopamine levels get too high, serotonin automatically drops. This fluctuation causes people to experience obsessive thoughts, much like a drug addict in need of their daily fix.

Another powerful chemical effect in women who are falling in love is caused by elevated levels of sex hormones, a change that boosts their sexual desire. This too is connected to higher dopamine levels, which in turn increase testosterone levels. The effect of this spike is that women begin to experience sexual arousal much like men do.

But that's not the only chemical messenger at play. _Oxytocin_, the bonding and trust hormone, also comes into play, making women desire physical contact and making them less fearful of strangers.

Women have more receptors for this hormone than men do, which are especially stimulated during times of increased estrogen. As a result, women in the first half of their cycle are more likely to fall in love.

### 5. Women tend to choose partners who are different than themselves, leading to relationship issues down the line. 

Most relationships have a honeymoon phase and, for women, after six to eighteen months with a new partner, that magical feeling of falling in love starts to fade. At this point, a woman might find herself feeling more calm and relaxed, but also start to realize that the partner she's chosen is very different from her.

Women often seek out partners from different walks of life, which can lead to challenges in the future. The reason for this preference is that women instinctively pursue partners who will complement their strengths and sire the fittest offspring. That means a strong and sturdy woman will sometimes end up with a slender and sensitive man.

However, this tendency produces certain difficulties. For one, after the flames of lust fade to the embers of a committed partnership, women may get annoyed by the particular habits of their partners that differ from their own. Because of this annoyance, an attempt is often made to change their partners through intimidation or criticism.

Further complicating matters is that the male partner also often embodies characteristics that were considered taboo in the woman's family of origin. For instance, a woman who was raised in an emotionally constrained family may end up with a highly emotional, loud and boisterous partner. You can imagine the problems this might produce, especially when this partner comes into contact with the woman's family.

Luckily, there's a simple way to deal with this problem: bring mindfulness to the relationship. This may enable a woman to see why she becomes annoyed with her partner and to then learn how to soften the atmosphere in their relationship.

And women don't just look for partners with different personalities; they also seek out those who are different in bed. This leads to problems as well, for instance when women who desire an equal partnership are also romantically attracted to authoritative and confident men.

In such relationships, these authoritarian, sexy men often try to dominate the domestic life of the partnership and they often lack sensitivity when it's most needed.

> "_Stop fixating on how he isn't like you. Nobody is, and you wouldn't want to be yoked to your carbon copy anyway."_

### 6. Becoming a mother transforms a woman’s brain and body. 

Motherhood can be a magical experience. But it's also very draining and difficult. So, to stay safe, there are a few rules you might want to consider abiding by.

For starters, pregnant women should avoid medication and listen to their intuition when breastfeeding. In fact, complete avoidance of pharmaceuticals is a good idea and they should be especially wary of taking antidepressants.

This second rule is absolutely crucial. Indeed, a Canadian study conducted by professor Anick Bérard found that a boy's chances of developing autism are much higher if he's exposed to antidepressants while in the womb.

After the birth, special care also needs to be taken, namely in regards to breastfeeding. While this process can be difficult and even painful for some women, it's essential if a woman wants to stay in touch with her body and the needs of her child.

Breast milk plays a crucial role in a child's development; it's full of enzymes, growth hormones, proteins and _cannabinoids_, a naturally occurring substance similar to marijuana that makes babies feel happy and calm. Furthermore, mothers who nurse their children are less likely to develop cancer of the breasts or the ovaries.

Intimacy is also a central part of raising an infant.

This comes back to the bonding hormone, oxytocin. Babies get this hormonal rush through cuddling and bonding, both of which raise oxytocin levels and reduce stress. In other words, when women give love and affection to their children, they literally affect the young one's brain circuitry and the strength of their future relationship.

Babies need a tremendous amount of attention, but this shouldn't come at the expense of the connection a woman has with her partner. A good way to maintain this connection is by spicing things up in the bedroom.

That holds especially true as children age. It's essential for them to learn from an early age that their parents need private time and that sex is healthy and natural in a relationship.

### 7. Inflammation, stress and depression are deeply intertwined and can pose big problems if not addressed. 

Most people know that inflammation occurs when you, say, bump your elbow against a table, but there's more to the mechanisms, causes and effects of inflammation than you might think. Inflammation doesn't just involve the body; it affects the mind, too. In fact, it's closely linked to stress and depression, problems that many modern women struggle with.

When a woman is stressed, the level of the hormone _cortisol_ in her blood increases, her heart rate quickens, her immune system is inhibited and inflammation reduces. This is a normal response and is healthy when it happens every once in a while. But if this response occurs daily, thereby becoming chronic, it _increases_ inflammation and immune reactivity, potentially leading to obesity, heart disease and other chronic issues.

The resulting inflammation leads to a loss of energy as the body heals. If the stress persists, the body is unable to mend itself, pushing energy levels down and potentially causing depression.

It follows, then, that reducing stress is one of the best things a woman can do for her health.

But how?

Well, a primary stressor for women these days is the constant pressure to be "good" and suppress their true feelings, like anger. Pushing down these emotions affects a woman's hormonal balance and hurts her immune system, so it's essential to find ways to vent these emotions.

In an ideal situation, a woman should express her feelings as soon as they arise, even if it means raising her voice at her partner. That being said, it's sometimes difficult to immediately express anger and, in these instances, anger can be stored inside until a quiet room or outside space is accessible to let it out. Once there, she can pummel a pillow, or scream until the feeling subsides.

### 8. Food, sleep, sex and body image have a deep impact on a woman’s mental and physical health. 

The way a woman feels in her body is a direct reflection of how she feels in her mind. That means that, by taking care of their bodies, women can also produce positive changes in their minds. A great place to start is with a healthy diet, which is all about eating natural foods instead of processed ones.

In recent decades, the proliferation of processed junk food has turned us into sugar and carbohydrate addicts. Lots of these foods are loaded with sugar, fat and salt, which trigger our dopamine circuitry, fostering unhealthy addictions.

Natural foods are simply better because our bodies aren't designed to take in such large quantities of refined sugar.

Instead, our bodies need raw food, proteins and natural fats that lower cholesterol, while reducing blood sugar and insulin levels. Some examples are nuts, ginger, turmeric, beets, peppers and dark leafy greens.

But a clean diet isn't all you need to be healthy. Good sleep and sex are also key to rejuvenating body and mind.

This is especially important for women, since their hormonal fluctuations cause them to have higher instances of insomnia than men. Such a predisposition can lead to other issues, like diabetes, obesity and cancer.

Beyond that, many women sleep poorly because of exposure to screens before bedtime and excessive consumption of alcohol and caffeine. So, to tackle insomnia, women can eliminate coffee after 2:00 p.m. and cut back on their nighttime TV watching, replacing it with a relaxing bath, a good stretch and a cup of herbal tea.

And don't forget about sex!

Just like physical exercise, good sex and healthy orgasms de-stress you, stimulate your immune system and release endorphins that increase blood flow to the genitals, thereby preventing vaginal atrophy. This assists physical well-being _and_ boosts self-esteem.

### 9. Exercise and mindfulness can help women reconnect with their inner selves. 

Do you ever spend the entire day in front of a computer screen and find yourself feeling like total crap? Well, it shouldn't come as a surprise and the solution is to get physical.

In fact, a lack of physical activity is hands down the most dangerous thing for a woman's health. What's more, exercising isn't just good for your body; it's essential for your mind.

That's because cardiovascular exercise releases endorphins that are like an internal morphine drip for your body, making you happy and relaxed. Beyond that, they also regulate the levels of other mood hormones like serotonin, dopamine and norepinephrine.

And exercise doesn't have to be a pain. In fact, the best exercise is whatever exercise puts a smile on your face. It might be running, Zumba, hip-hop dance classes or kayaking. It doesn't matter what form your exercise takes as long as you enjoy it and it speeds up your heart rate.

That being said, it's also essential for women to slow down every once in a while to check in with themselves.

A good way to this is by spending time in nature, which can do wonders for mental health. Nature has restorative powers that can reduce anxiety, mental fatigue and even improve cognitive ability and creativity. Those are just a few of the reasons why humans need natural light and time outside in the sunshine.

Furthermore, people spend so much time indoors these days that three-quarters of the world's population are vitamin D deficient. This essential vitamin is responsible for muscle health, bone growth and for combating the symptoms of depression. Just take an American study that found that, after walking around a mall, 22 percent of participants were more depressed, while those who walked around outside were 92 percent less depressed.

And finally, it's essential for women to remember to breathe. This is as simple as taking a few moments to follow your breath, noticing each inhalation and exhalation as it comes and goes. It's an easy way to check in with yourself, reduce stress and calm down.

### 10. Final summary 

The key message in this book:

**To feel comfortable in both body and mind, it's essential for women to know how they work. Women are heavily influenced by hormonal fluctuations, particular needs and emotional space, which makes it all the more important for them to have a nuanced understanding of their bodies.**

Actionable advice

**Pills are not always the solution to your pain.**

The next time you have a headache, instead of reaching for a bottle of painkillers, try taking a break from whatever you're doing and go for a walk outdoors. Take in the sky, the trees and enjoy a deep and full breath of fresh air that will oxygenate your mind and body.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Anatomy of an Epidemic_** **by Robert Whitaker**

_Anatomy of an Epidemic_ (2010) traces the development of mental health medications and the relationship between pharmaceutical companies and the field of psychiatry. These blinks bust the myths we're told about such drugs, and, in the process, may make you question the wisdom of fighting psychological ills with a panoply of pills.
---

### Julie Holland, MD

Dr. Julie Holland is an American psychiatrist and author of _Weekends at Bellevue_, a best-selling memoir _._ She is a regular guest on the _Today_ _Show_ and CNN.

