---
id: 54bd207b633234000a410000
slug: moonshot-en
published_date: 2015-01-20T00:00:00.000+00:00
author: John Sculley
title: Moonshot!
subtitle: Game-Changing Strategies to Build Billion-Dollar Businesses
main_color: C92839
text_color: C92839
---

# Moonshot!

_Game-Changing Strategies to Build Billion-Dollar Businesses_

**John Sculley**

_Moonshot!_ describes how new technologies have permanently changed the business landscape, where today's customer is smarter and has more control. The book offers advice on how to become more competitive, through making an exceptional customer experience your main goal.

---
### 1. What’s in it for me? Learn to focus on the customer and watch your business boom. 

Technology has not only revolutionized how we learn but also how we buy. Consumers today are savvier and smarter, and to succeed, companies need to deliver a great product as well as a great buying experience.

Excitingly, starting a business has never been easier. You don't necessarily need a six-figure loan to see your business idea realized; but you do need to harness "big data" to make sure you're reaching your customer base in a way that creates loyal followers and in turn, lucrative returns.

These blinks will help you do just that. You'll grasp the secrets behind billion-dollar business concepts, learning how to reach out to your target market and cement lasting relationships.

In the following blinks, you'll discover

  * how what you "like" on Facebook helps companies know better what you want;

  * why even a great product will flop if the customer experience is lacking; and

  * how "shadow banking" can get you the cash your budding business needs.

### 2. New technologies have given consumers a lot more power as part of the buying experience. 

Have you ever tried on a sweater in a retail store, only to quickly check on your smartphone whether you could find the same item cheaper online?

New technology has changed how consumers think and act, and to remain competitive, it's clear that companies need to change too.

Smartphones are one example of an advancement that has changed in-store buying. A customer has instant access to product information as she browses, and can even compare prices or read online product reviews.

In general, online feedback influences a consumer's buying decision considerably. Complaints, poor ratings or positive recommendations are important and reach a wide audience.

Thus companies need to focus on creating a memorable, positive customer experience from the first point of contact, from a reliable, innovative product to peerless customer service.

These days, so-called _big data_ gives businesses the opportunity to gather a lot of information about potential and current customers. To really understand a target market, a company could comb social media and other online data — even GPS coordinates — to more accurately predict what a customer actually wants _._

Imagine, for example, that you "liked" a commercial on Facebook for a new brand of backpack. Later you visit a store that sells outdoor gear, when suddenly you receive a text on your smartphone, offering you a 10 percent discount on that same backpack in store.

U.S. President Obama too relied on big data as part of his second presidential campaign. His campaign team was able to accurately target voters at an exact time and place with a message that they would most likely relate to. This "data-backed information" was based on a voter's social media activity and what they watched on television.

From winning voters to winning customers, the power of technology is something every successful company will need to master today and in the future.

### 3. A revolution in processing power and data storage means consumers aren’t faceless anymore. 

There's never been a better time to build a successful company.

Why, you ask? Well, cheaper technologies now make it easier for companies to store larger amounts of data, which they can then use to improve the customer experience.

_Cloud computing_ is a concept where several electronic devices are connected to a central network, providing centralized data storage. And while the power of data processing increases exponentially, the cost of storing information in the "cloud" has become less expensive, to less than 25 cents per gigabyte today.

This change has been revolutionary — and it's altered the way we sell and price things.

Now that data storage is so cheap, those cost savings can be passed on to customers. Services such as mobile apps can easily be priced at just 99 cents. Big data too can be used to reach customers more effectively, as we've seen.

We're truly living in a new technological era. Developments like mobile devices and _miniature sensors_ have completely changed the product landscape.

Miniature sensors can transmit information about consumer needs and behaviors. For example, Apple's _healthkit app_ is a service that makes use of such sensors.

Healthkit consumers can use the app to track health and wellness information. They can record how many steps they take per day and how many hours they sleep. Healthkit also recommends new apps and other products that can help a consumer live healthier.

By the early 2020s, experts predict that there will be 40 billion wireless, connected devices in circulation. And these devices won't just be smartphones and laptops, but appliances like washing machines and heaters, too.

Miniature sensors will eventually use machine-learning systems, so they can get "smarter" on their own, without human programming. In fact, Ray Kurzweil, head of Google's engineering lab, predicts that machines will achieve consciousness and be more intelligent than humans by 2045.

### 4. A new and growing middle class has new and different demands; companies should take note. 

Most business people have in their sights the needs and desires of the "middle class." After all, this group has always been the foundation of society's purchasing power; it also forms the bulk of most of your company's customers.

Interestingly, the American middle class is changing, and this change is influencing the market.

Today there are more people who want a middle-class lifestyle than there are middle-class jobs to provide them such a lifestyle. Thus there is a greater demand for less-expensive products, such as clothes, phones and televisions.

So even if people aren't able to live solidly in the ranks of the middle class, they still want to consume as if they did, as long as products are affordable.

The millennial generation (those born between 1980 and 2000) is also the first generation that _doesn't_ expect to have better economic prospects than their parents. One effect of this is that young people are more keen to share products, rather than buy them.

This is how business models like _Uber,_ an app-based carsharing and taxi service, have come about.

On the other hand, the middle class in _global_ emerging markets is growing, and is expected to top 2 billion people by 2020.

This new middle class, especially in places like China and India, also wants cheap products. Large-scale Western companies need to adapt to the needs and desires of this growing class of consumer if they want to effectively tap into this market.

The brewer SABMiller, for example, changed the ingredients in its beers to appeal to more consumers abroad, using locally supplied cassava and sugar instead of barley and maize.

### 5. Disruptive technologies and business models create new opportunities for entrepreneurs. 

Today we can buy almost everything online. This ease of access is more convenient for customers, and also creates new opportunities for businesses.

Traditional business models have already been disrupted, and this trend will only increase. Since services like Amazon now offer a wide range of products at low prices, traditional retailers have to change to keep up.

Bookstores, for example, have had great difficulty competing with companies like Amazon.

The good news is that it is easy for businesses to obtain financing, so more companies can try to compete. The cost of establishing a start-up is much lower than it used to be, for a few reasons.

First, with inflation low, it is easier to borrow money. New companies also have the option of starting out as a virtual concern, connected only online. Such a company can save a great deal of money by outsourcing accounting or IT departments, and avoiding high rent for large office spaces.

New online platforms also connect businesses to independent contractors, so you can hire on a project-by-project basis instead of seeking full-time employees.

Also, if you want to raise money for your company, you don't necessarily need a bank loan. There are innovative ways of collecting money, such as _shadow banking_.

Shadow banking means receiving money from "non-bank" institutions, like hedge funds or insurance companies. The market for shadow banking has increased, from $26.1 trillion a decade ago to $71.2 trillion today.

You can also use crowdfunding through online organizations like _Kickstarter_. So if you want to sell books online and compete with Amazon, there are many ways to get started!

### 6. A positive customer experience needs to be the center of any successful business plan. 

So what's the most important element in any successful business? The customer experience.

By creating a positive, memorable customer experience, your company will be _better_ than your competition. Look at the brands that have created an exceptional customer experience, like _Apple_ or _Amazon_. These companies provide a level of service people love and have come to expect.

_BMW,_ as part of its brand message, says its cars are the "ultimate driving machine," so you feel like you're getting something really special. _Virgin Group_ companies put the Virgin brand on everything from mobile phones to music to airplanes, making you feel part of a special group. Amazon's service is similar, as they sell almost everything at affordable prices, and deliver it to your door.

Once your business is up and running and you make your first sale, remember that it's just the beginning. Exceptional service isn't just about getting people to buy your product! The purchase itself should essentially be the _launch pad_ for a great customer experience.

Consider this: It costs _10 to 15 times more_ to replace profits from a customer you've lost than it is if you hadn't lost the customer to begin with. So it's incredibly important that you care about your customers, and that they feel your concern.

Center your business on creating a positive, memorable customer experience. Throw out your old business plan and make a new _customer plan_ instead.

Work out how you're going to create the best possible customer experience in your industry. What do your competitors do? Supersede them. Include the rate of customer engagement and re-engagement, customer satisfaction, the cost of customer acquisition and most importantly, the lifetime value of a customer.

### 7. The ability to identify big problems is the crux of every billion-dollar business concept. 

Why are some companies so successful while others never take off? The question you have to ask is this: What problem does the company solve?

If you want to solve a major consumer problem, you've got to ask the right questions. If you want to find billion-dollar problems, you need to think outside the box.

What sort of service does the customer _need_? If you see an opportunity, you need to seize it, even if it seems unrealistic — because if you don't, someone else will.

When Apple's Steve Jobs first envisioned the technology for the _Mac_, he worked for ten years to see his dream come true. He never gave up, and in doing so, he created a groundbreaking, easy-to-use desktop publishing system.

Testing and re-testing your business plan can help you to make necessary changes. So be sure to question yourself and your business.

In the 1970s, Pepsi solved a big problem in the soft drink market by introducing the plastic bottle. Retailers were frustrated with glass bottles as they often broke. Pepsi recognized that this issue needed to be solved, and seized the opportunity.

You can pinpoint a billion-dollar problem and its solution only by _knowing_ your industry, so do your homework. Stay curious and have a thirst for knowledge. Read several different news sources on a daily basis, and ask questions like, "What's different today from when I last looked at this?"

Why is it that United States has the best medical technology in the world, but no comprehensive health system? When you look at the big picture, you see that the health care crisis relates to customer experience. In the healthcare industry, good customer service should mean _wellness_.

Now customers are demanding a better experience. And for companies in the field, it's a chance of a lifetime; or like sending a man to the moon — the ultimate "moonshot."

### 8. Final summary 

The key message in this book:

**There's no better time than now to build a billion-dollar business. So establish a successful company by making sure you create an exceptional customer experience. Try to pinpoint a hole in the market on which you can capitalize. Do something nobody's done before; that's the secret to great success.**

Actionable advice:

**If you want to be successful, stay committed.**

Never stop learning about your industry, your competitors and most importantly, your target market. Find an experienced mentor who can give you guidance when you need it, and remember: you can't expect to know everything, so build a strong team of people who support each other.

**Suggested further reading:** ** _Zero to One_** **by Peter Thiel and Blake Masters**

_Zero_ _to_ _One_ explores how companies can better predict the future and take action to ensure that their startup is a success. The author enlivens the book's key takeaways with his own personal experiences.
---

### John Sculley

Entrepreneur, mentor and investor John Sculley was formerly the CEO of both PepsiCo and Apple. He has also written the book _Odyssey: Pepsi to Apple_.

