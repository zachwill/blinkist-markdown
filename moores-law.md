---
id: 55476bd236663200070d0000
slug: moores-law-en
published_date: 2015-05-05T00:00:00.000+00:00
author: Arnold Thackray, David Brock and Rachel Jones
title: Moore's Law
subtitle: The Life of Gordon Moore, Silicon Valley's Quiet Revolutionary
main_color: 60A2D6
text_color: 3E688A
---

# Moore's Law

_The Life of Gordon Moore, Silicon Valley's Quiet Revolutionary_

**Arnold Thackray, David Brock and Rachel Jones**

_Moore's Law_ (2015) tells the story of Gordon Moore, a chemist from San Francisco who helped revolutionize the technology industry. Over the years, Moore's innovations have fundamentally changed all kinds of electronic technology, from digital watches and personal computers to the internet and Facebook.

---
### 1. What’s in it for me? Learn what made Gordon Moore one of the most influential people of the last century. 

Who would be on your list of most influential figures in the computer industry? Most of you would include Bill Gates or Steve Jobs; a few of you (those who saw that Benedict Cumberbatch film) might throw in Alan Turing. But there's one person who should be at the front of people's minds — though he probably isn't — and at the top of any computer-industry list: Gordon Moore.

Hardly anyone has done more to create the modern digitized world than Gordon Moore. As the co-founder of Fairchild and Intel he has played a leading role in two of the most influential companies of the 20th Century. He is also responsible for Moore's law, which, though coined back in the 1960s, predicted the rise of the modern, highly technological world of today with incredible prescience. These blinks take you on a journey through the life and career of this unparalleled influencer.

In these blinks, you'll discover

  * why an old Buick probably played a key role in computer history; and

  * how wristwatches cost Gordon Moore millions.

### 2. Gordon Moore was passionate about science from an early age. 

You've probably heard of _Moore's Law_, but what do you know about Gordon Moore — the man who came up with it?

Gordon Moore was born in San Francisco, in 1929, to Mira and Walter Moore. He was a very reserved child, but also an exceptionally concentrated one. His precocity and extraordinary intelligence led him early on to what would become his lifelong vocation.

In 1940, when he was eleven years old, Moore's life changed forever. After his best friend was given a chemistry set, the pair began using it to make explosives and blow things up. Science suited Moore's analytical mind, preferring it to math because he could see its visible effect on the tangible world. Moore had found his calling.

As he got older, Moore remained passionate about chemistry and experimentation. He took his first chemistry lessons at Sequoia High School, where he was far ahead of the rest of his class, and by the age of 16, he already had a sophisticated understanding of the subject and was very confident in his ideas.

Moore also retained his love of blowing things up. He experimented with nitroglycerine at home and eventually began making firecrackers for his friends, who used them to blow up mailboxes.

Nor was the chemistry in Moore's life just happening in the lab. In September 1947, Moore met Betty Irene Whitaker, a journalism major at San Jose State. Whitaker was outgoing, lively and headstrong — the very opposite of Moore. Naturally, that drew him to her. She, in turn, was intrigued by his quiet confidence and composure.

### 3. Moore's chemistry education brought him to Berkeley and Caltech. 

As Gordon Moore and Betty Whitaker grew closer, Moore began expanding his horizons. He took his first steps toward formulating Moore's Law at Berkeley University, right across the Bay from his childhood city.

Moore was accepted to Berkeley in 1948, with the help of recommendation letters by professors at San Jose State.

At the time, Berkeley was a particularly exciting place for a young chemist to be. The East Coast had long been the hub of academic science, but, partially due to California's booming economy, that was beginning to change rapidly. Moore had the privilege of working with a number of important people in the field. In fact, two of his professors, William Giauque and Glenn Seaborg, won the Nobel Prize for their discovery of the element berkelium shortly after Moore's arrival.

During his first years at Berkeley, Moore was greatly influenced by George Jura, an assistant professor in Berkeley's physical chemistry department. Jura taught his students that contemporary scientific literature was nearly always wrong, and urged them to disprove it.

This opened Moore's eyes to the significance of original research, which also suited his flair for experimentation. Jura also introduced Moore to the art of glassblowing, a talent that proved useful during his later research on his semiconductor equipment.

Moore quickly gained a reputation for his studiousness, and, in 1950, he was accepted to the California Institute of Technology, Caltech, which was regarded as one of the best science schools in the US at the time.

By then, Betty Whitaker had finished her journalism major at San Jose State, so Moore asked her to come with him. By the standards of the day, such an invitation was tantamount to a marriage proposal. The couple married and began their life-long partnership.

### 4. After achieving great success in chemistry research, Moore got into industry work. 

When Gordon Moore began his studies at Caltech, the area was booming. Multiple technological advancements had recently been made. In the 1940s, aerospace science, for instance, depended completely on electronics and IBM's special calculator, Harvard Mark 1, a kind of proto-computer that was used for calculations in missile testing and design.

It was a thrilling environment for Moore. His work in experimental chemistry flourished, particularly under the tutelage of Professor Richard McLean Badger.

Badger was conducting research on nitrogen compounds. His work was supported by the military, which was busy using nitrogenous explosives in the Korean War. Moore's previous work with explosives and nitroglycerine made him a perfect fit for the research, and he was soon working in the field.

In 1951, at the age of 22, Moore published his first scientific paper, on nitrous acid, in the _Journal of Chemical Physics_. He completed his research and finished his Ph.D. in 1953 — a mere three years after arriving at Caltech.

After completing his Ph.D., Moore felt finished with student life. He initially hoped to become a professor at a prestigious school, but couldn't find a position that matched his high demands.

Badger convinced him to look into industry work instead, as there was a high demand for his skills in the field. So Gordon considered a few companies, looking for one that would give him enough freedom to experiment independently.

He eventually decided on Applied Physics Laboratory, a research center funded by the Navy at John Hopkins University. The only downside was that it meant leaving California; he and Betty felt it was time for something new, however, so they bought a Buick and started driving east.

### 5. After the development of a highly powerful transistor, Moore realized the potential of semiconductors. 

In 1947, an important device was invented by scientists at Bell Laboratories: the _transistor_. Few people (if any) understood its potential at the time, but the invention ended up changing the world completely.

The transistor could be used to amplify and switch signals on and off, just like vacuum tubes. Vacuum tubes were fragile and expensive, however, whereas transistors were made up of solid material. They were also much smaller and required less power. Transistors would soon make an array of new technology possible - most prominently the _transistor radio_, which was ubiquitous by the 1950s.

By 1955, over half a million transistors were produced every month in the US alone. Moore had become interested in semiconductors under Badger's tutelage, but he only realized their true potential when transistors became popular.

While at Caltech, Moore attended a lecture by William Shockley, one of the inventors of the transistor at Bell Labs. Two years later, Shockley was trying to produce a new kind of transistor that used silicon as a semiconductor. He needed a talented young chemist to help him in his California laboratory, the Shockley Semiconductor Laboratory, and Moore's name came to mind. In 1955, he offered him the job. So the Moores got back in their Buick and drove west.

Shortly after their arrival, Shockley received the Nobel Prize for his work on the transistor. By 1957, Moore had made significant strides toward the company's goal of creating a transistor that used silicon as a semiconductor. All seemed to be going well.

Things began to get tense, however. Moore was confident they'd reach their goal, but, after 18 months of work, the team was still far from finished. Arnold Beckman, Shockley's partner, was beginning to get anxious.

### 6. Moore launched his own business venture to continue researching transistors. 

Shockley's apprehension mounted as his team struggled to make a breakthrough. Eventually, feeling that the time had come for something new, Moore and a few dissatisfied colleagues left the company to set up their own. They became known as the "traitorous eight" — and Moore was their leader.

Moore and his new colleagues began searching for an investor and soon found one in Sherman Fairchild, one of IBM's biggest shareholders. Their new venture, Fairchild Semiconductor, was born the same week that the Soviets launched Sputnik, the world's first satellite.

Moore knew that Sputnik would create an even greater demand for the kind of transistor they were trying to create; a fast-switching silicon transistor. In 1954, Texas Instruments had already offered a small batch of silicon transistors for military use. They were more robust and withstood heat better than other transistors on the market at the time, but they had one major drawback; they were slow-switching.

At the end of 1957, IBM secured a contract related to the development of the B-70 Valkyrie, a bomber armed with thermonuclear weapons and capable of flying at supersonic speeds. The engineers of the B-70 badly needed fast-switching silicon transistors, which would enable the onboard computer to switch quickly between electronic signals. And, word had it, there was a small start-up in California that was researching them.

Moore's team still had a number of problems left to solve with the transistor, however. They knew that Texas Instruments and Bell Labs (both of which also had enormous budgets) were also researching the technology. Moore had to make sure his team came first — everything depended on it.

In August of 1958, a mere year after founding the company, they succeeded. Fairchild Semiconductor became the world's first company to develop a fast-switching silicon transistor and bring it to market. They called it the ZN696 _._

The team had focused so much on producing the transistors that they hadn't thought about actually sending them to IBM. So Gordon went to the grocery store and bought the nicest container he could find — a Brillo box!

### 7. Moore's predictions about the future of complex microchips became known as Moore's Law. 

The silicon transistor soon became the industry standard, and its applications became more and more manifold. More computers, radios and television sets were produced as newer models replaced the old ones that'd relied on vacuum tubes. That wasn't all, however. A new and particularly important technology was also emerging: the _microchip_.

At the end of the 1950s, people still believed it would be too expensive to put several components on one chip, as the advanced technology it required was exorbitantly priced. They assumed it'd always be cheaper to wire individual components together. Moore, however, knew this was wrong.

So he started Micrologic, a division of Fairchild dedicated to _integrated circuits_. He knew that integrated circuits would lead to a new generation of smaller and more complex microchips. His microchips would later be selected by NASA for use in the onboard guidance computer of the Apollo program.

Moore had unique insight into the rapidly growing world of transistor-driven electronics. He knew where it was headed.

In February 1965, Moore published a journal article, "The Future of Integrated Electronics," which made some startling predictions. He wrote that, as people developed more applications for them, silicon transistor microchips would become more ubiquitous.

Moore noticed that the microchip's complexity (in terms of its number of components) had doubled every year since its development. He predicted that it would continue to double, while its manufacturing cost would halve each year.

Moore believed the microchip's exponential growth would lead to an explosion of computational power. His prediction, which became known as _Moore's Law_, turned out to be right.

According to Moore, there would be 65,000 transistors on a single microchip by 1975. In 1965 that sounded like science fiction, even to many of Moore's peers — that year, the number of transistors on a microchip was only 64.

> Fact **:** In 2016 alone there will be over 100 billion transistors produced per human being on the planet.

### 8. The development of microchips with greater memory storage made Moore (and Intel) very successful. 

In 1968, Moore set out on a new adventure that would change computing forever.

Moore and Bob Noyce, his business-savvy partner, were truly the perfect pair — but they needed to find a fresh market. They knew they couldn't compete with the giants in established markets, like Motorola and Bell Labs.

They found their niche when Moore noticed that the expansion of computers and calculators was creating more demand for greater memory-processing capabilities. Data was still stored on punch cards, which were cumbersome and took up lots of time to use.

So they started searching for another team member — someone who could create the next generation of memory storage technology. That's how they found Joel Karp, a microchip designer at General Microelectronics, in Santa Clara.

Karp would go on to create Intel's _1101 memory microchip_, which could hold 256 bits of data on 1s and 0s. The new microchip propelled Intel to the forefront of the emerging memory market.

The memory market was generating a solid profit for Intel when Moore got a visit from a Berkeley electrical engineer named Dov Frohman.

Frohman had an innovative idea for a new kind of memory chip. Earlier chips could only store data when the power was on, but his version would retain data even when it was off. That wasn't what made it revolutionary, however.

There were already some memory chips that could retain data without power, but their memories were _fixed,_ that is, the data was physically printed onto them. Frohman's chips, on the other hand, were reprogrammable. Moore immediately realized their potential and put them on the market straight away.

The new microchips, now called EPROMs, were a great success. Between 1972 and 1985, EPROM chip sales were Intel's main cash flow.

> _"We are really the revolutionaries in the world today, not the kids with the long hair and the beards." — Gordon Moore_

### 9. Moore was wary of selling personal computers after an unsuccessful foray into the electronic watch market. 

In the early 1970s, the American economy was faltering and unemployment was high. Despite this, Intel's business was booming.

But Moore was about to learn an important lesson: change and innovation are an important part of business, but sometimes you should stick to the things you know you're good at.

Moore was always looking for potential new markets in microchip technology and the emerging electronic-watch market caught his eye.

The Hamilton Pulsar, the world's first fully electronic wristwatch, was introduced in 1972, and cost a whopping $2,100. In today's money, that's roughly $12,000.

So Moore and Noyce went searching for partners. That's when they found Microma, a business that was just about to start shipping out their new LCD wristwatches. Moore invested huge amounts in the small company to help them develop.

It wasn't long, however, before Microma foundered. Their watches had a number of technical problems and competition from Texas Instruments rapidly reduced their price. Microma simply couldn't compete.

Ultimately, Moore would come to understand his mistake. Consumer products were different from what he was used to selling — they demanded a lot more than technical knowledge about microchips. A purely electronic focus would never be enough when it came to consumer goods.

Fortunately, he maintained a sense of humor about the loss. He even called his own electronic watch (which he wore a long time after the failure) his "15 million dollar watch."

Moore and Noyce later found another emerging market: home computers. Enthusiasts all over the country, in fact, had started to build their own personal computers. The first "personal minicomputer" set was sold, in 1973, by a small company called MITS.

This gave Noyce an idea: Intel could get into the personal computer industry by producing the rest of the kit along with each new chip.

He took his idea to Moore, who, still reeling from the Microma catastrophe, exploded: "We are not in the computer business," he said. "We build computer development systems."

### 10. The use of Moore’s and Intel’s microprocessors became widespread, leading them to team up with Microsoft. 

By the mid-1970s, Intel had become a hugely profitable company. Moore was right at the heart of a revolution that would forever change computers.

Up until 1979, it was extremely profitable to be in the computer memory industry. Intel's revenue was well over half a billion dollars per year.

Competition from Japanese makers started to change that, however. They began manufacturing chips more quickly and at a lower cost. Intel's business gradually became less and less profitable. Luckily, Moore knew where the wind was blowing: _microprocessors_.

Microprocessors were just making their way into the market. They allowed customers to create their own software programs and store them in an EPROM chip. Moore realized that microprocessors could revolutionize the industry and become a universal part of computing.

If Intel were to get into the microprocessing market, they couldn't continue doing things by halves. The market for home computers was expanding at a phenomenal rate, and they had to keep up. In 1978, Apple sold 25,000 units of their Apple II computer. And by 1980, 750,000 PCs were sold worldwide — and each of them had an Intel microprocessor inside!

Moore realized he'd have to invest heavily in research and development to keep staving off the competition from Japanese producers. They weren't only producing microchips faster; they were producing higher quality ones, too.

So Moore invested $100 million into researching and developing Intel's next game-changer, the 386 microprocessor. With a total of 275,000 transistors, it offered unprecedented computational power.

Intel further changed the industry when they started collaborating with Microsoft. In 1986, the companies produced their new PC, the Deskpro 386, which ran on Microsoft's software and Intel's microprocessors. It would rule the PC world for the next 25 years.

> _"If the auto industry had made similar advances, we could cruise comfortably in our cars at 100,000 miles an hour and get 50,000 miles per gallon of gasoline."_

### 11. Intel came to dominate the microprocessor market, but Moore was more interested in philanthropy as he got older. 

By this point, it was clear that computers were rapidly changing the world. And the good news for Moore was that all of them still needed microprocessors!

In fact, in 1990, the average US citizen spent 40 percent of her time looking at screens: watching TV, playing video games or using a computer.

And because Intel had quickly secured a dominant position in the microprocessor market in the 1980s, business was booming. Between 1981 and 1987, consumers spent billions of dollars on software and hardware for personal computers, and all of these computers relied on Intel microprocessors. This led Intel to leave the memory market and focus solely on microprocessors.

Intel's dominion was partly due to the industry's high costs. Microprocessors were increasingly complex, and they demanded enormous budgets and facilities. Other companies couldn't compete. By now, Intel was a multi-billion dollar corporation that churned out microprocessors of greater complexity every year.

By the mid-1990s, Intel had over 80 percent of the market share in PC microprocessors. And they've more or less been able maintain that ever since!

In 2001, for instance, the year Moore decided to retire, at the age of 72, Intel was hugely profitable. The previous year, Intel's revenue had reached $10.5 billion and its stock price _tripled_. Moore was finding other interests, however.

As Moore became less involved in Intel, he started turning to philanthropy instead. He made substantial contributions to Caltech and other educational institutions; the Gordon and Betty Moore Foundation joined the ranks of philanthropists — people like Bill Gates and Warren Buffet — who'd donated billions.

In 2005, _Forbes_ magazine named Gordon Moore the year's most charitable person.

So where does Moore's Law stand today? It's actually approaching its physical boundaries, as microprocessor technology nears the level of individual atoms. It's time for another game changer. Who do you think will be the next Gordon Moore?

### 12. Final summary 

The key message in this book:

**Gordon Moore followed his passions and talents from an early age. His work in chemistry led him to research semiconductors and microchips, and his success in those fields brought him to personal computers and microprocessors. After revolutionizing our technology with his advancements in memory processing and personal access, he focused on philanthropic work in his old age.**

**Suggested further reading: The Soul of a New Machine by Tracy Kidder**

_The Soul of a New Machine_ is a Pulitzer Prize-winning book about one team at the computer company Data General who was determined to put its company back on the map by producing a new superminicomputer. With very few resources and under immense pressure, the team pulls together to produce a completely new kind of computer.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Arnold Thackray, David Brock and Rachel Jones

Arnold Thackray is a writer and the CEO of the Chemical Heritage Foundation. David Brock is a world-renowned expert on electronics. Rachel Jones, a journalist, specializes in technology and entrepreneurship.

