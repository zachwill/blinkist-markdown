---
id: 54595aa13734660008400000
slug: more-money-than-god-en
published_date: 2014-11-07T00:00:00.000+00:00
author: Sebastian Mallaby
title: More Money Than God
subtitle: Hedge Funds and the Making of a New Elite
main_color: 1E9765
text_color: 197D54
---

# More Money Than God

_Hedge Funds and the Making of a New Elite_

**Sebastian Mallaby**

_More Money Than God_ takes a critical look at hedge funds, the powerful and often mysterious organizations that have a huge impact on global finance and our day-to-day lives. By looking back at the history of this influential investment practice, it reveals how hedge funds developed and how they operate today.

---
### 1. What’s in it for me? Learn key investment strategies from the most successful hedge funds in history. 

Hedge funds tend to stay out of the financial spotlight, because their success usually relies on finding an unexploited niche in the financial market. Throughout history, the most successful hedge funds were able to develop new approaches to investing before their contemporary rivals could even understand them. In these blinks, those secrets will be revealed, giving you key insights into how and when to bet big.

In the following blinks, you'll also discover

  * How one investor made $30 million in eight minutes;

  * How a small hedge fund can reverse the economic fortunes of a nation of 240 million people; and

  * Why, unlike investment banks, hedge funds never need a taxpayer bailout.

### 2. Hedge funds take a mixed approach to financial investing, which can include capitalizing on overvalued stocks. 

You've probably heard the term _hedge fund_ before. What does it mean and how do hedge funds actually work?

In 1949, A. W. Jones founded the first "hedged fund," as it was called at the time. Since then, hedge funds have become major financial institutions.

Today, the vast majority of hedge funds closely follow Jones' initial model, which took a mixed approach to stock market trading.

Here's what that means: Like other investors, hedge fund managers _buy long_ — selecting stocks in promising companies and hoping the values increase, thus earning a profit.

But what makes hedge funds special is that they also buy shares with the hope that the stocks' values will decrease. This practice is called _selling short_ and it entails borrowing stocks in less promising companies, selling them, and buying them back as soon as the price falls.

For example, let's say you think a certain company is overvalued. So you call a broker and ask him to lend you 50 stocks from that company, which you then sell for $2 apiece. Now you're "short" 50 stocks. Luckily, the following week, shares from the company fall to $1, so you buy up 50 and give them back to the person who lent them to you. In the end, you earned a $50 profit from the exchange.

Essentially, the people who run hedge funds understand the market deeply: They know how to buy long and sell short to maximize profits, but often they have little capital. So they find investors to lend them large sums, which allows the hedge fund to invest on a massive scale. This is how they earn money.

And although they often invest other people's money, hedge fund managers actually keep a portion of the profits they earn as a _performance fee_, which motivates them to trade successfully.

### 3. Although hedge funds involve an element of gambling, they actually tend to be risk averse. 

Are you concerned that hedge funds have a little too much in common with poker? How do these guys know, you might be wondering, whether stocks will rise or fall? How can they be so sure that they won't lose all their money on a bad bet?

Of course, all stock trading involves an element of gambling, since no one can ever fully predict the outcome of a trade. But hedge fund portfolios are diversified, which means they aren't actually very risky for investors. Traders assemble diverse portfolios by buying "long" and selling "short," a strategy that protects against market swings.

For example, if the market index rises and stock values jump accordingly, your "long" stocks will bring you profits that compensate for small losses incurred on the "short" stocks.

And on the other hand, if the market goes down and stock prices fall, you'll make profits on the "short" stocks to compensate for losses on the "long" stocks. So by having a mix of "long" and "short" stocks, your overall investment is insured no matter what happens to the market.

This strategy all but guarantees that hedge funds succeed, because it protects them from risk. Additionally, hedge fund managers tend to be cautious in general. Their own money is often invested in the fund along with clients' assets, so if a trader places a losing bet, he's likely to blow his own savings. Thus, hedge fund managers are more likely to think twice before making a trade.

Funds are also risk averse because they don't have a government safety net: If a hedge fund makes too many bad bets, the government won't step in to save it from insolvency, as it would for a major bank.

So now that you understand the theory behind hedge fund strategy, let's take a look at how real financial geniuses apply these techniques to win big in the market.

> Fact: Hedge funds usually borrow only once or twice the amount of money they already have.

### 4. Steinhardt, Fine &amp; Berkowitz was the first hedge fund to invest on a massive scale, with astounding success. 

After A. W. Jones famously struck gold with his "hedged fund" in 1949, others followed suit. And one especially notable early hedge fund was _Steinhardt, Fine & Berkowitz_.

Although founders Michael Steinhardt, Jerrold Fine and Howard Berkowitz mostly followed Jones' original model, they were the first to do it on a massive scale — which meant that they earned more profits than anyone had before.

Their strategy was simple: bet big to win big. This approach was actually enabled by changes to the stock market which occurred in the early 1970s. For the first time, large chunks of stock were offered at steep discounts to anyone willing to buy them in bulk.

Steinhardt (the driving force behind the firm — some later said his success was the product of a "gambling gene") was one of the few investors who had the guts to go big. He flourished in this new climate.

For example, once Steinhardt made a million dollars in just eight minutes, by buying up 700,000 shares of a bankrupt railroad company and reselling them immediately.

These huge profits were the result of not just luck, but skill and strategy. In fact, Steinhardt, Fine & Berkowitz were able to thrive at a time when other hedge funds were going bankrupt due to inflation, which ravaged the market at the end of the 1960s. Between 1968 and 1970, the biggest hedge funds lost two-thirds of their former capital.

On the other hand, Steinhardt, Fine & Berkowitz not only started their fund during this turbulent time, they also won big.

### 5. Commodities Corporation conducted deep data analysis to anticipate market changes. 

_Commodities Corporation_ was another giant in hedge fund history. The firm, which was founded in the early 1970s, further improved upon A. W. Jones's original method.

It did this by conducting deep analyses to foresee market changes. The company's president, F. Helmut Weymar, was a scientist at heart, so it's no surprise that he took a scientific approach to finance.

He and his team bet on commodities like cocoa, wheat and pork belly, relying on specialists' knowledge of the industries and analyzing information about weather, production and so on.

The idea itself was groundbreaking, but Weymar's faith in his method was blind. So when corn blight (a fungal disease) hit US fields, he ignored outside experts and decided to trust a plant pathologist he had hired. Weymar bet on corn prices falling, but they skyrocketed instead. As a result, Commodities Corporation almost went bankrupt.

Almost, but not quite. Although the corn blight incident nearly ruined the firm, Commodities Corporation pulled itself out of the financial morass through sophisticated data analysis.

Traders at the firm abandoned the practice of analyzing production conditions and shifted their attention to analyzing financial ones instead. And seeing that market trends were often driven by investor reactions, Commodities Corporation started to research investor psychology.

To this end, the firm's traders arrived at a simple insight: Investors tend to buy when a stock goes up, causing the price to rise even higher. The reverse is also true: Investors are likely to sell when a stock's price falls, which further drives down their value.

Commodities Corporation used this insight into investor psychology by buying up stocks when the prices first tipped upward, hoping that the move would encourage others to also buy.

They were essentially trying to create a snowball effect, and this strategy worked brilliantly: By the end of the 1970s, Commodities Corporation's capital had soared from just $1 million to $30 million.

### 6. George Soros made hedge fund history by betting on currencies. 

Meet the hero of the next chapter in the history of hedge funds: _Quantum_, a firm started by George Soros in 1973 (though initially under a different name).

Quantum achieved its big successes a decade later, in the 1980s, at a time when currency seemed to be stable. It was a period of economic _equilibrium_, and the dollar in particular was thought to be safe from speculations and crashes.

In fact, for many people, trust in the dollar was seen as patriotism. Many believed the dollar could only crash if American products were in less demand internationally, and foreign products were in higher demand at home. This scenario was thought to be impossible.

Another reason the dollar was considered so stable was that even if speculators could predict its future value, they couldn't do anything to influence it.

Enter George Soros, who totally upended the conventional wisdom for currency. Soros didn't view currencies as stable properties, so he decided that rather than betting on stocks, he would trade currency itself.

To do that, Soros first set out to understand how different factors (such as capital flow or political climate) could impact the value of a currency.

And then in 1985, Soros made his first big currency trade, betting that the dollar was going to fall. He bought up many "short" US dollars as well as stable foreign currencies, so he could later buy the dollars back.

Although at first it seemed that his predictions were wrong, on 22 September 1985, treasury secretaries from the United States, West Germany, Japan and Britain decided to push the value of the dollar downward.

As a result, George Soros earned $230 million, gaining a fortune from a single political event. Later on, he would be able to influence such events himself.

### 7. Tiger developed a method for picking the very best stocks. 

The year 1980 saw the birth of another influential hedge fund firm, _Tiger_, founded by Julian Hart Robertson.

Tiger was started at a time when most traders didn't actually care that much about stocks. Today, most people understand that when you're trading on the stock market, it helps to have insight into which stocks are likely to become more valuable.

But at the time, most financial firms didn't see it that way. We already saw a few classic approaches to stock trading: _Steinhardt_ traded big, _Commodities Corporatio_ n tried to crack the market with complicated scientific formulas, and Soros found his niche exploiting political events.

But until Tiger appeared on the scene, no one tried to do hedging simply by picking the best stocks.

The Tiger fund excelled at stock picking: Robertson and his small team analyzed all the information that was relevant to the stock's value, both in terms of the company's prospects and also the currencies and commodities they could use to buy the stock.

Armed with all this information, the Tiger fund searched the market for companies that were misvalued and traded them — either buying "long" or selling "short," depending on whether they were overpriced or underpriced. This strategy won huge profits for the firm.

For example, in 1985, Tiger identified underpriced shares belonging to _Aviall_, a company that distributed aviation parts. The fund bought shares for $12.50 and then later resold them for double the price — $25 a share.

Similarly, Tiger bought all of _Empire Airlines'_ shares for just $9 each and then sold them for $15 apiece. When the founder wrote to his investors that year, bragging about the firm's success, he joked that they shouldn't show the letter to their wives, unless of course they wanted to go shopping for diamonds.

### 8. Farallon built a sterling reputation by holding traders accountable for losses. 

Another important firm was immortalized in hedge fund history: _Farallon_, founded by Tom Steyer in 1985.

Farallon's main contribution to hedge fund practices was to make traders accountable for losses. Before the firm came along, traders were primarily motivated by performance fees, but these could have a negative effect on a hedge fund's overall profits.

We already discussed the impetus behind these performance fees: Hedge funds keep part of their profits as a way of motivating traders to achieve higher earnings.

But there's a downside to this practice, because making bigger profits sometimes entails taking bigger risks. And traders who don't feel the sting of the losses (but do benefit from the gains) are more likely to make overly risky calls, on the chance that they win big.

For the most part, hedge funds operate on the principle, "heads I win, tails you lose," which means they get to keep sizable chunks of the profits they make (thanks to performance fees), but aren't really held accountable if they have a bad year and lose money.

But unlike other hedge funds, Farallon wanted to distribute risk more fairly and to make traders accountable for their losses.

Steyer's fund was known as a first "event-driven" hedge fund, which meant that they specialized in analyzing the effects of different events like takeovers or bankruptcy on price valuations.

But the true secret of Farallon's success had nothing to do with refined economic strategies. Rather, it was the firm's strong sense of integrity: Steyer insisted that employees kept their savings invested in the fund, so they would "feel the pain" if they made a bad bet. This ensured that traders thought twice before taking a big risk.

This policy had a huge impact on Farallon's reputation. In fact, in 1990, even Yale University invested in the fund; just three years earlier, the university had claimed to have absolutely zero interest in hedge funds.

As you've learned, hedge funds can create huge fortunes in little time. And with money comes power, which is why they sometimes play an influential role in politics.

### 9. When an international crisis occurs, a hedge fund can either play hero or villain. 

In today's United States, you don't have to be a politician to get into politics, you just need massive influence — which most hedge funds can definitely claim.

Why are they so influential? Well, hedge funds have the power to invest enormous amounts of money fast, and that can devastate an entire country's economy.

This is precisely what happened in the early 1990s in Europe. When the Berlin Wall came down, it pulled the continent into a political maelstrom that subsequently wreaked havoc on Europe's currencies. The British pound sterling was doing especially badly, lagging at the bottom of the exchange rate charts.

Europe's distress was a treat for George Soros, who promptly took action to short-sell sterling. In an attempt to protect the currency and avoid devaluation, the Bank of England spent $27 billion. But their efforts failed, earning Soros $1 billion in profit.

But hedge funds aren't just capable of economic devastation. In some cases, they can also save economies.

Due to their massive size and scope, hedge funds have global reach and they can turn the international community's attention to their investments, sometimes setting off a domino chain of events that can ultimately lift up an entire economy.

This was the case in Indonesia, after the country's dictator resigned in the late 1990s. Around the same time, the nation's largest bank, Bank Central Asia, started teetering on the verge of bankruptcy. Soon afterward, the 9/11 attacks occurred. Taken together, these events made the world's largest Muslim country seem extremely unattractive to foreign investors.

But not necessarily for hedge funds. In 2002, Farallon bought control of Bank Central Asia. In less than five years, the firm not only managed to raise the share price of the bank by more than 500 percent, it also attracted other foreign investors to the country by setting an example, thus reversing the economic fortunes of a nation of over 240 million.

> _"Clearly, hedge fund managers are not angels."_

### 10. Although hedge funds do pose a threat to the economy, they never become “too big to fail.” 

Since hedge funds pose an enormous threat to governments and economies all around the world, you might be asking yourself why we allow them to exist at all.

It's worth remembering that financial risk is inherent to all kinds of institutions, not just hedge funds. And like any other firm or organization, hedge funds take various different forms: Some are nice guys, and others are vicious, but neither type characterizes the whole industry.

Furthermore, there's absolutely no evidence whatsoever that hedge funds engage in financial fraud or abuse more often than other institutions. In fact, the financial crisis of 2007–2009 proved that financial institutions are in general unsafe, and that no amount of regulation can make them secure.

From a public interest standpoint, the one main benefit of hedge funds (dangerous as they can sometimes be) is that they can never become "too big to fail."

Hedge funds can certainly be powerful and their investments can influence the economy in profound ways, but the exact same thing is true for pretty much every major financial institution.

But unlike other financial institutions, like banks, hedge fund failure won't wreck the entire economy in a catastrophic fashion.

Consider that during the 2007–2009 financial crisis, governments had to use taxpayer money to bail out investment banks in order to avoid even bigger economic losses. By contrast, over 5,000 hedge funds went bankrupt between 2000 and 2009, but not one of them received an injection of taxpayer money in order to prevent financial catastrophe.

That's because banks lend to other banks. The whole system is interconnected, so if one bank collapses, many others could collapse as a result. Hedge funds, on the other hand, rarely lend money, since they're far more focused on investing.

So although the dangers of hedge funds can't be denied, these institutions also have the potential to stabilize economies.

> Fact: Even when 5,000 hedge funds go out of business in a single decade, this doesn't have a disruptive effect on the economy.

### 11. Final summary 

The key message:

**Hedge funds operate by combining four tactics:** ** **Buying stocks "long," a**** **elling other stocks "short," l** **everaging their capital to maximize profits, and k** **eeping a part of the profits as performance fees.** **Although hedge funds can have a huge impact on both national and global economies, they are still far safer than investment banks.**

Actionable advice:

**Build a diversified investment portfolio.**

Analyze market changes to identify key areas for opportunity, but make sure you invest in a wide variety of stocks. Choose some stocks with an eye toward making short term gains, and others on the basis of their long-term potential.

**Suggested** **further** **reading:** ** _Liar's Poker_** **by Michael Lewis**

_Liar's Poker_ tells the story of Salomon Brothers, a leader in the bond market in the 1980s. This tell-all account of the author's experiences at Salomon Brothers explains how the firm became one of the most profitable investment banks on Wall Street through its role in establishing the mortgage bond market, and what it did once it reached the top.
---

### Sebastian Mallaby

Sebastian Mallaby is an editor at the _Financial Times_ and the Paul Volcker Senior Fellow in International Economics at the Council on Foreign Relations.

