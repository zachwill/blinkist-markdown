---
id: 5800d3df230ff80003a1b832
slug: mortality-en
published_date: 2016-10-20T00:00:00.000+00:00
author: Christopher Hitchens
title: Mortality
subtitle: None
main_color: 89B7D1
text_color: 4D6775
---

# Mortality

_None_

**Christopher Hitchens**

_Mortality_ (2012) presents a collection of essays written by Christopher Hitchens after he was diagnosed with esophageal cancer. In these blinks, you'll explore fundamental questions addressing death and life, the nature of pain and how we cope with them.

---
### 1. What’s in it for me? Explore Christopher Hitchens’s reflections on life and mortality. 

Life is a great gift, one we are fortunate to enjoy. Yet we also know that this gift is given to us for a limited period — we grow up, we grow older and eventually, we die.

But often, it seems we forget just how fleeting life is unless we are reminded of its transience.

Author Christopher Hitchens was given his "reminder" when he was diagnosed with cancer in 2010 and was told he had less than a year to live. In the year between this event and his death in December 2011, Hitchen's explored, at length, the question: How does living feel when you know the end is so near?

In these blinks, you'll read the reflections of a man for whom life was cut short by disease. You'll explore what it feels like to be confronted with one's mortality, how to handle severe pain and how cancer affects the body and mind.

In these blinks, you'll also learn

  * what you truly lose when you lose your voice;

  * how religion hinders groundbreaking cancer research; and

  * why Nietzsche's glib phrase on suffering is wrong.

### 2. When he was diagnosed with cancer, Hitchens saw how uncomfortable people are with the idea of death. 

British-American journalist, writer and literary critic Christopher Hitchens was on a book tour in 2010 when he collapsed and was rushed to the hospital. There he was forced to confront the weight of his mortality, after doctors diagnosed him with esophageal cancer.

Hitchens was overwhelmed by the idea of his death; it's true that most people don't think about mortality until they absolutely must. Hitchens's terminal diagnosis made him realize how many life milestones he would miss, such as his children getting married, or the birth of grandchildren.

Even though the concept of mortality was difficult to grasp, the reality of death became part of daily life as Hitchens suffered through the pain of his disease and extensive cancer treatments.

Hitchens also realized how awkward people became when interacting with a terminal patient. Some of his friends didn't know what to say, other than recounting "motivational" stories of people who had survived the disease.

For some, such stories might be comforting ‒ but Hitchens didn't find them helpful as the stories offered no practical information on how he could cheat death.

Other friends avoided the topic of mortality altogether, telling him instead to fight on. This made Hitchens feel as if he would be a failure if he were to die.

All these experiences made him realize just how uncomfortable most people are with the idea of death.

Hitchens eventually came up with a set of rules aimed at making his friends more comfortable. When people inquired as to his health, Hitchens tried to be honest. If being honest was difficult, he told jokes.

If a friend asked how he was feeling, for example, he might answer, "Well, I feel like cancer."

Mortality forced Hitchens to reevaluate his life and change the way he related to other people.

### 3. The phrase, “What doesn’t kill you makes you stronger,” doesn’t always apply to cancer patients. 

You've probably heard Nietzsche's phrase, "What doesn't kill you makes you stronger."

It's true that tragedy may inspire personal growth for those who survive it. For terminal cancer patients, however, this doesn't hold true. 

A patient can fight cancer for a time, but if the diagnosis is terminal, the disease will beat them in the end. Such people are _living dyingly_ instead of growing stronger.

Encouraging words can be helpful, as every patient needs support from loved ones. But over time, even the kindest words won't save a cancer patient from having to confront his terminal situation.

For Hitchens, this moment came when he realized that death wasn't his biggest fear — he feared the slow march toward it instead.

He was afraid that his proximity to death would cause him to lose himself, such as the convictions he held dear, like his commitment to atheism.

Moreover, he realized that chemotherapy wasn't about getting stronger but instead about delaying death. As his condition worsened, Hitchens watched as painkillers such as morphine took over more and more of his waking life.

Pain and suffering are inevitable for cancer patients undergoing chemotherapy treatment. The word "chemotherapy" is often associated with hair and weight loss, yet for Hitchens, these were the easiest challenges he faced during treatment. Instead, chemotherapy made him feel as if he was waging a constant battle against cancer.

As the battle consumed his body, he realized how intertwined mental health and physical health are; physical suffering takes a heavy psychological toll on a person.

> _"I don't have a body, I am a body."_

### 4. After Hitchens’s diagnosis, he explored his convictions about atheism in the face of death. 

Hitchens valued his commitment to the concept of atheism. And despite outside pressure, he stuck by his beliefs through his illness until the very end.

Many friends and companions pressured Hitchens to find faith as he neared death. Although he appreciated his friends praying for him, he didn't believe it would make a difference to his situation.

Even strangers would try to convert him. People callously made bets online about how long it would take him to accept God.

Hitchens also received much criticism from people who told him he'd "burn in hell" if he failed to convert before he passed away. Some even had the audacity to tell him this while he was on his deathbed.

Despite the pressure, Hitchens believed that changing his mind about religion would be dishonest, and certainly wouldn't help in the face of divine judgment. Becoming a Christian at the last moment wouldn't exactly absolve him from being an atheist most of his life. Surely he couldn't prove his faith was true if he only accepted God just before death?

Hitchens never understood why organized religion is so exclusive when it comes to the afterlife. What if he converted to Christianity, for example, only to discover that Judaism or Islam was the true religion?

The prospect of death made Hitchens realize how important it is for people to stand by their convictions. Ultimately, nobody knows if there is life after death. Nobody knows what happens to the "soul" when a body dies.

Hitchens would have been lying to himself, his loved ones and his fans if he were to back down, just because he was afraid of death.

> _"If I convert, it's because it's better that a believer dies than an atheist does."_

### 5. Hitchens realized just how important communication is when his illness took away his voice. 

Hitchens lost a lot of weight during treatment. He also suffered through several rounds of chemotherapy, feeling each time as if poison was being injected into his body.

While living through the pain, however, Hitchens could at least joke verbally about the situation with friends and family. This situation changed when he lost his voice.

The loss of his voice made Hitchens understand the true value of expressing one's self. Communication is such a fundamental part of being human that we base our entire lives around it — and often neglect to think about how important it is.

Language is the key to the world. We use it to share thoughts, absorb new information, fall in love, argue and create. Through language, a person can offer opinions and engage with the stories and lives of others.

If you can speak, once you lose your voice it is difficult to experience the world fully. It is increasingly challenging to engage with your surroundings and interact with people's stories, whether tragic or beautiful.

People often don't realize how important a voice is until it is lost. Few stop to consider how vital human communication is to being alive.

Hitchens, a naturally talkative person, was hit especially hard when his illness caused him to lose his voice. Losing the ability to communicate was one of the most difficult parts of his struggle.

Voiceless, Hitchens felt excluded from the rest of the world. He was suddenly confined to a place in which he couldn't express his thoughts but could only listen to other people.

### 6. Cancer treatment is still primitive, and religious interests hinder modern research. 

Once a person is diagnosed with cancer, his only hope of survival is to get quality treatment, and fast. Cancer treatment technology is still primitive, however, and much research needs to be done.

Thousands of cancer patients still die each year after having undergone treatment. Chemotherapy, one of the most common cancer treatments, essentially kills a patient as much as do cancer cells.

In short, society is in dire need of more effective medicine to combat cancer.

One of the most promising new forms of cancer treatment today is to analyze, or _sequence,_ the genome of a tumor, so the damaged DNA can be targeted with medicine.

Hitchens learned of this approach from Dr. Francis Collins, who was researching such methods when Hitchens was diagnosed with cancer.

Hitchens was so desperate to be cured that he volunteered to be a research subject. During this process, however, he realized how much cancer treatment is hindered by society's deeply rooted religious beliefs.

To perform his research, Dr. Collins required embryonic stem cells. Yet he was denied access to stem cells because a federal judge in Washington, DC ordered an end to state expenditures on such research projects — exactly at a time when Hitchens could have benefited the most.

The judge's ruling was based on the Dickey-Wicker Amendment, named after Congressman Jay Dickey who in 1995 worked to abolish government funding of embryonic stem cell research. Supporters of the amendment, in particular, wanted research on human embryos banned on religious grounds.

Hitchens was appalled that even though such embryos had no other use (they were not going to be implanted or carried to term), religious powers in the United States still blocked their being used for research — even though such research could have saved countless lives.

### 7. Final summary 

The key message in this book:

**Everyone is mortal, but we rarely stop to consider what mortality means. We are often uncomfortable or avoidant when it comes to the idea of death and take fundamental parts of our lives for granted. What is more, cancer research is still primitive and hampered by religious taboos. All in all, as a society, we need to pay closer attention to the questions that mortality and terminal diseases such as cancer elicit.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _God Is Not Great_** **by Christopher Hitchens**

_God is Not Great_ traces the development of religious belief from the earliest, most primitive ages of humankind through to today. It attempts to explain the dangerous implications of religious thought and the reasons why faith still exists today. It also helps explain why scientific theory and religious belief can never be reconciled.
---

### Christopher Hitchens

A journalist, author, literary critic and political activist, Christopher Hitchens focused on the topics of religion, science and atheism. He died in December 2011.

