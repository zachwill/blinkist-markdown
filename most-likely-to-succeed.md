---
id: 5820d0924d1bae00032dc425
slug: most-likely-to-succeed-en
published_date: 2016-11-09T00:00:00.000+00:00
author: Tony Wagner & Ted Dintersmith
title: Most Likely to Succeed
subtitle: Preparing Our Kids for the Innovation Era
main_color: 2E87E5
text_color: 2469B2
---

# Most Likely to Succeed

_Preparing Our Kids for the Innovation Era_

**Tony Wagner & Ted Dintersmith**

_Most Likely to Succeed_ (2015) takes a close look at the current state of the US education system and its failure to prepare the next generation of kids for the era of innovation. These blinks detail the history of education, why it's essential to rethink the way we teach our children and how we can do just that.

---
### 1. What’s in it for me? Find out how the United States can fix its badly out-of-date education system. 

Many schools today seem to follow the same approach: students go to class, memorize concepts and facts and take tests where they regurgitate everything they've learned. This style of education may have worked well in the industrial age, in which most workers only needed to learn how to complete a task and then repeat it over and over again. But in today's context of constant innovation, a new approach to education is needed.

So, how should we update our education system to fit the needs of today's economy? Put simply, we shouldn't be focusing on teaching students what to learn, but _how_ to learn _._ This way, the next generation will be better equipped to tackle the challenges of the future with creativity and adaptability.

In these blinks, you'll discover

  * how apprenticeships first came about;

  * why Napoleon's wars led to mandatory education in Prussia and the United States; and

  * how learning methods like _ConcepTest_ are helping Harvard students think critically.

### 2. The US education system leaves people unsuccessful, unhappy and uninformed. 

When you were fresh out of college, did you have any difficulty finding a fulfilling job? Or did you nimbly climb right up the career ladder without encountering a single obstacle along the way?

Well, if you're like the vast majority of US citizens, you probably fall into the former group. Right now, the education system in the United States isn't sufficiently preparing students for career success.

If you think otherwise you need only look at the data:

A Gallup study done a few years ago found that only 11 percent of American business leaders feel that colleges prepare students to be successful in the workplace. It also found that over half of all recent college graduates are either unemployed or employed in jobs they could have done without a pricey university education.

But producing workforce-ready graduates isn't the only thing the education system is failing to do — it's also failing to create informed citizens.

For instance, the non-profit educational institute Just Facts conducted a poll of American voters, and found that people who vote most often know very little about the basic facts surrounding major election issues. This same group of people could only answer about 20 percent of questions correctly on topics such as government spending and climate change.

And finally, our education system is actually making people unhappy. Just consider the teen suicide rate; since 1950, the rate of suicide among college-aged people has doubled — and for high schoolers, it has tripled!

So, the education system is a disaster on multiple fronts, but our societies are still obsessed with academic credentials.

As a result, people are still asking questions about whether someone went to college and how prestigious their school was. It's easy to see why when you consider that the aforementioned Gallup poll also found that 94 percent of American adults think college is essential to their children's career prospects.

But why are we so obsessed with these supposed markers of success when students don't seem to derive much benefit from them? To answer that question, we need to delve deep into the history of education.

> "We worship at the altar of academic credentials. We live in a society obsessed with people's degrees."

### 3. Education began with a meaningful apprenticeship model, but changed as society became more religious. 

In the early days of humankind, our ancestors taught each other how to survive — that is, how to hunt and avoid falling prey to more capable predators. In this system, parents would educate and pass knowledge on to their children.

In a way, this was the start of the _apprenticeship model_, a form of education in which assistants learn by doing and are taught by a master in their field. However, as society evolved, the apprenticeship system changed along with it.

Soon, education and credentials became tightly intertwined. After all, as society became more sophisticated, it came to include tradespeople and craftsmen like farmers and blacksmiths. In this changing system, a master's reputation became his student's _credentials_.

But how exactly do credentials work?

Well, say Mr. X the blacksmith has an excellent reputation as a reliable craftsman. It's safe to assume that the apprentices who work under him will also be good at what they do. Mr. X also has an incentive to produce reliable apprentices since, if they fail to master the craft, his reputation will suffer as well.

In this way, education and credentials have a close historical link. And since an apprentice's credentials gave important indications about his profession and expertise, this link became quite meaningful.

Over time, as religion came to exert more influence in society, greater numbers of children began to forgo apprenticeships and attend grammar schools instead. During this period, a new education system took shape as demand for the Bible grew.

As more people came to want a copy of the good book, Latin grammar schools began popping up where monks and priests would learn how to transcribe the Bible. In such schools, standardization was the guiding principle; the pupils were taught to minimize error, work efficiently and there was zero tolerance for creativity or deviation from the norm.

But what do bible-copying monks have to do with the current education system? Well, as you're about to see, many of the traits first seen in these early grammar schools permeate the systems we have today.

### 4. Societal changes during the eighteenth and nineteenth centuries led to further changes in the education system. 

Have you heard of the economic term _spillover effect_? It applies to situations in which one economic event precipitates another in an entirely different context — and the same idea can be applied to the US education system.

Socio-economic developments like industrialization and the outcome of wars have shaped the way current American schools are structured. Just take the Industrial Revolution, which occurred at the turn of the nineteenth century; it demonstrated the need to educate young people so they could adeptly perform new manufacturing jobs.

At around the same time, the Prussians, shocked by the military defeat they suffered against Napoleon, implemented a strict, mandatory eight-year education system focused on subjects like reading, arithmetic and obedience. This was the first time the distinct classes and grading structures of our modern education systems were ever used.

Then, later in the nineteenth century, this model spread to the United States — but for different reasons. In America, the goal was to educate workers and immigrants to participate in the burgeoning industrial economy. A major focus was placed on efficiency, thanks in large part to Frederick Winslow Taylor.

Taylor was known as the father of workflow efficiency — a practice for developing streamlined production techniques, like narrowing the expertise required of workers by assigning them a single discrete task.

This so-called Taylorism spilled over to the education system, which meant students were educated like workers on an assembly line, being taught basic literacy, punctuality and to do whatever an authority figure ordered them to.

So what was the goal of such a system? To produce workers that could perform repetitive tasks quickly and efficiently.

Even the Committee of Ten, a prominent working group put together by an American teacher's union, recommended that students be taught to execute repetitive tasks quickly and without error, while exercising zero creativity or innovation. In fact, the Committee of Ten saw this as the very purpose of education.

### 5. Despite massive changes in society, the US education system has remained largely unchanged throughout the last century. 

The nineteenth century may have brought a tidal wave of industrialization to the United States, but the twentieth century witnessed a large-scale shrinking of industry.

The result has been a growing middle class of "white-collar" workers whose job is to think, not make. Beyond the economic implications of this shift, the move toward a de-industrialized economy has also led to increased prioritization of education in the US.

After all, the country had to keep up with the demand for _knowledge workers_, a term coined by the professor and management expert Peter Drucker in 1959 to refer to employees who use their heads rather than their hands. To do so, the federal government increased the minimum number of years a person was required to spend in school, and the number of college and high-school graduates rose accordingly.

But that didn't stop criticism of the education system from intensifying in the 1980s. Much of the uproar at the time came about because the original Prussian elements of teaching repetitive task completion had remained at the core of the system.

This fact was causing American students to fall behind their international peers, doing increasingly poorly on standardized tests and failing to keep up with a rapidly changing world. At this point, it would have been logical to rebuild the broken system from the ground up; but instead, the government simply reinforced it.

For instance, they doubled down on standardized test preparation. Unsurprisingly, students didn't do any better and, since then, the education system really hasn't changed.

### 6. Most schools teach students to memorize content – with little success. 

Lots of schools boast mission statements with catchy lines like "to help students uncover their passions," but their supposed intentions rarely correspond with reality. The truth is that the educational methods employed by our schools are still based on the memorization of content, like specific facts and concepts — an approach that's been proven largely ineffective.

But that's not the worst of it; many core subjects, like arithmetic and language, are taught in the exact same way they were decades ago!

Basically, students are expected to memorize loads of content and regurgitate it during an exam. The problem is that students don't retain this information.

For instance, ten years ago, the Lawrenceville School tested their methods by having students retake their final exams after their three-month-long summer vacation. Before the break, the average grade was a B+ — but after the vacation it dropped to an F!

So, for the most part, memorization doesn't work. What we really need is a clear, overarching education strategy that actually prepares students for success in life; education should endeavor to teach students _how_ to learn, not _what_ to learn.

For example, instead of teaching students facts they can easily find on Wikipedia, we should be showing them how to solve problems using creativity and innovation. But beyond that, education should help students find their passions and purpose in life, while giving them the skills they need to follow their dreams. Such a system would inspire students to give their absolute best every day, while also becoming active, informed citizens.

That's not to say that students shouldn't learn about Abraham Lincoln and Shakespeare, but rather that mastery, memorization and standardized content should not be an education system's top goals.

> "To have good prospects in life — to be most likely to succeed — young adults now need to be creative and innovative problem solvers."

### 7. The labor market has changed, and the education system should be revamped accordingly. 

You don't need a PhD in economics to know that the labor market you face today isn't the same one your parents faced when they were applying for their first jobs.

In fact, compared to just a century ago, the labor market has experienced dramatic shifts. For instance, during the twentieth century, the US economy was soaring. It was a time when major corporations like AT&T and General Motors created thousands of entry-level jobs for the masses.

However, as the century came to a close, the American economy experienced a radical change. This was in large part due to the advent of the digital economy, which made information and resources readily available to millions of people through the internet.

But the onset of the information age also made lots of jobs obsolete, replacing workers with automated processes or outsourcing their positions to other countries. As a result, workers today are faced with a new economic reality — and the education system should be adapted to suit it.

So, instead of producing workers who simply memorize concepts and repeat tasks, we should be teaching students to think creatively and grow into an innovative workforce capable of tackling the problems our future is sure to present.

If we don't make this shift, we'll merely be preparing students for a society that no longer exists. We will lay the foundation for a future in which only a privileged few scrape their way to the top, the majority of them likely coming from wealthy families that can afford high-quality education. As a result, the gap between rich and poor will grow wider and poverty will only get worse.

On the other hand, if we teach young people to solve problems creatively, they'll be able to not only support themselves, but also participate in the global labor market, increase their productivity and accumulate greater wealth.

Of course, it's not just _what_ we teach that matters; it's also _how_ we teach it, which is exactly what we'll explore next.

### 8. Lectures need to make way for new modes of teaching. 

How many times have you had to sit through a grueling 60-, or even 90-minute lecture, barely able to keep yourself focused on the professor's words? For most of us, saying "a few" would be a complete understatement; while we might have made it through such torturous classes, how much did we actually learn?

When it comes down to it, lectures aren't working. One key issue is that, with such tremendous resources and content available to students 24/7, they don't really need lectures. Such a system might have made sense in the past, when a teacher was the only entity that held certain important and specialized knowledge — but we all know that's no longer the case.

Nowadays, students don't have to endure monotonous lectures that drag on and on, which is really for the best since so many people struggle to sit through them. For instance, the head of MIT's Media Lab, Joi Ito, recently asked a student to monitor her brain activity for a week. He discovered that the student's brain was least active during lectures, exhibiting even less activity than during sleep!

We need to rethink the way students learn, and one strategy to do so is called _ConcepTest_. This technique was invented by Eric Mazur, Area Dean of Applied Physics at Harvard University. He used to have his students answer thought-provoking questions like, "does the size of a hole in a rectangular metal plate increase, decrease or remain the same when the plate is uniformly heated?"

At first, the students were asked to answer on their own — but then they'd form small groups in which they'd offer their ideas and discuss their often divergent solutions. Once they'd reached conclusions as a group, they'd present them to the rest of the class.

By structuring his class in this way, Mazur never answered a single question or even gave a hint. Regardless of whether they reached the correct answer or not, students learned to think critically, form an opinion, communicate it and collaborate, in addition to countless other skills that are essential to success in the modern world.

### 9. Transforming the education system requires reframing the problem, speaking out and taking initiative. 

You now know that a shift needs to occur in education. But when will this turning point come?

Well, that depends on what action we take, and the first step is to reframe the issue of education altogether. As it stands, the mantra seems to be one of reforming the current system — but as we've seen, that hasn't been enough.

The US education system also needs to move away from the language of reform and toward a vision of rebuilding from the ground up. By doing so, young students will be able to meet the demands of the modern economy in stride.

Finally, people need to voice their opinions. This is crucial because, for ages, education and business leaders in the United States, as well as local and state policymakers, have been aware of the country's inadequate education system. But instead of taking substantive measures, they sat silently, waiting for national leadership to step up.

This begs the question: why wait for them?

All federal policymakers have done is attempt to reform the broken system; they're certainly not reimagining a new one.

In such a stalled environment, local and state actors must take the initiative and speak up about the new skills students need to succeed. For instance, they could hold education summits in which people come together to discuss the implications of the innovation era for the overall goals of US schools.

Beyond that, initiatives could be put into place through local communities. Just take Scarsdale, New York, where the school board collected innovation funds and successfully helped teachers develop new courses.

Such local efforts are of the utmost importance, because if everyday American citizens don't get moving on this critical issue very soon, they'll be stuck educating students for a time that has long since passed, while the era of innovation passes them by.

### 10. Final summary 

The key message in this book:

**Society may be advancing at an astonishing speed, but our education system is stuck in the nineteenth century. As a result, we're educating our children to succeed in a bygone era. To give our kids the opportunity to succeed, we must creatively reimagine education for the innovation era.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _How Children Succeed_** **by Paul Tough**

These blinks explore the reasons why some people struggle in school and later on in life, and why others thrive and prosper. Using scientific studies and data from real schools, the blinks dive into the hidden factors that affect the success of children.
---

### Tony Wagner & Ted Dintersmith

Tony Wagner is an education expert who taught English for many years and also worked as a school principal. He holds a doctorate from Harvard University's Graduate School of Education and is the author of several books, including _Creating Innovators_ and _The Global Achievement Gap_.

Ted Dintersmith is an expert in innovation and technology, and holds a PhD in Engineering from Stanford University. He is a major player in the world of venture capital, a partner at the major venture firm Charles River Ventures and the director of the National Venture Capital Association.

