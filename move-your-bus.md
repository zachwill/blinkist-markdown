---
id: 562e388838643100076b0000
slug: move-your-bus-en
published_date: 2015-10-30T00:00:00.000+00:00
author: Ron Clark
title: Move Your Bus
subtitle: An Extraordinary New Approach to Accelerating Success in Work and Life
main_color: FFF033
text_color: 666014
---

# Move Your Bus

_An Extraordinary New Approach to Accelerating Success in Work and Life_

**Ron Clark**

_Move Your Bus_ (2015) presents an easy-to-follow plan to boost your organization and enhance your own personal success. It's about understanding that not everyone can perform on the same level, so managers can be most effective by supporting high achievers. It includes steps for guiding other kinds of workers up the achievement ladder — and shows you how to become a top achiever yourself.

---
### 1. What’s in it for me? Discover how to create a more successful company by getting everyone to push in the same direction. 

Just like society in general, every company or organization consists of different kinds of people, all with different attitudes and ways of looking at things. Some are highly productive and help the company move forward, while others just settle in for the ride without contributing much.

So how do you create an organization in which everyone helps push toward a common goal?

Part of the answer lies in expecting more of everyone in the company. People grow when those around them have higher expectations of them. It's also important that those who contribute more, who are more engaged and motivated to move the company toward its goals, get to shine and receive the credit they deserve. Everyone in the company should try to learn from them. 

In these blinks, you'll learn

  * what the Flintstones have to do with creating a more successful company;

  * why every organization is like a big bus; and

  * what's wrong with work ethics in the United States today.

### 2. If you want to build a successful organization, you must cultivate high expectations. 

For business owners, it's a tough world out there. But luckily there are a few simple tools for improving ourselves, motivating others and helping our companies succeed. 

First, cultivate high expectations of yourself and others. In other words, set goals to accomplish more, be better and succeed faster. 

But here's the thing: high expectations don't mean bupkis if you aren't accountable. Meaning, if you expect a lot from someone, you have to make it possible for them to deliver by clearly communicating what you expect. For instance, you might ask them to prepare two pages on a specific subject by tomorrow morning. 

And if they don't come through, deliver the message again with more clarity and specificity. You should be encouraging and make sure you don't communicate everything all at once. By making requests in small doses, you'll make it possible for the other person to take it one thing at a time. 

Consider Ron Clark's eponymous Ron Clark Academy, an award-winning nonprofit middle school in Atlanta. The institution makes a point of educating its teachers, thus sending them into the classroom with energy and passion. When it conducts staff seminars, the school motivates teachers by challenging them, helping them set high expectations for their own classroom performance. 

To show others how to adapt this model, Ron Clark developed the following metaphor: imagine a bus in a Flintstones cartoon. There's no engine, so your team has to push the bus to get it to move. The bus stands for your goals: you'll only be able to move the bus (i.e., reach your goals) if everyone performs together at a high level. 

The "everyone" part is really crucial: each member of the team — no matter what their role — has a contribution to make. And in the end, you'll only succeed by working together.

> _"You have to communicate the expectations very specifically and then find ways to uplift people and let them shine."_

### 3. Different workers perform at different levels; learn how to navigate those differences in your organization. 

Let's stick with the bus metaphor to understand how it works. On our Flintstones bus, people have different roles. 

First, there's the _Driver_ — steering the team, pushing everyone forward. Then there are the _Runners_, who are top performers. Next we've got the _Joggers_ : these are conscientious, diligent workers doing a good job. Then come the _Walkers_, who contribute little forward momentum. And then finally, the _Riders_ : essentially dead weight.

In an organization, Drivers are the managers; they give direction and support. Drivers should make the Runners their first priority: these workers want to be part of something special. They typically contribute many new ideas, have a strong work ethic and don't make excuses. In other words, Runners put their job first. And if you let them, they'll be the backbone of your organization. Just make sure you don't crush their spirit with criticism — instead give them direction and support. 

Meanwhile, Joggers don't regularly exceed expectations, but when called upon, they will switch into high gear. Typically, these workers believe that they're already doing their best and lack the confidence to try harder. But at the same time, they burn for recognition. 

Next come the Walkers, who perform slowly but steadily. Walkers don't like change and they often complain, which slows others down and spreads negativity throughout the organization. 

But they're still far better than Riders, who only perform when someone is watching, choosing to hide behind the team instead. These workers are often smart, but they lack a work ethic. Turning them into Walkers is tough, since they're only likely to become motivated once fired. That makes it very hard to handle them. 

Nonetheless, Walkers and Joggers do have the potential to become Runners. And in the following blinks, we'll figure out how to make that happen.

### 4. Become a Runner by showing up early, dressing well, completing tasks and learning from others. 

If you want to improve your organization, you must adopt the habits and behaviors of a Runner. Start with the easy parts: show up early, put in the time to perform well and always dress your best. 

That last point might seem trivial, but if you want to be a Runner, you should have no dress-down days with regards to your clothing _or_ your performance. By dressing well, you're displaying your engagement and commitment to your job.

You should also be mindful of how you communicate. Make sure you never participate in negative conversations. Instead, either change topics or pose the question: "How can we make things better?" Positive conversations are the only way to produce positive results and empower those around you. 

One more thing: make sure you complete tasks. If you have something on your plate, finish it. That way you will show others that you can meet high expectations. 

The best way to speed up your progress is by learning from Runners themselves. Ask for help: this won't show weakness; it'll let others know that you care about your performance. (Plus, networking with high achievers will help you move forward in the future.)

But when you reach out to Runners, make sure you're open to their criticism. After all, you're trying to improve. And you might not be able to see what others see — so take their advice and make the best of it. 

For example, if your boss points out a mistake, there is only one acceptable answer: "I am sorry. It will never happen again." That way, instead of having a long conversation about it, you can simply show your boss that you've heard their feedback. And then you can both move on. 

But what happens if you just don't have it in you to become a Runner? Keep reading to find out.

### 5. To become a Runner, know your role and put your organization’s needs first. 

You don't have to be a Runner to play a vital role in your organization. But you _do_ have to put the needs of the organization ahead of your desire to reap rewards. Personal rewards _will_ come to you — but first you have to know your role.

If you're not a Runner and you want to become one, start by taking on menial tasks, take the hint when your ideas are put down and listen more than you talk. Be present in meetings, look others in the eye while they're talking and be attentive: show that you value other people's ideas by taking notes. 

And when you do voice ideas in meetings, always ensure you present solutions. If your solution doesn't work, try again. Show that you're a solutions person and pretty soon, people might start to see you as a Runner.

It's also hugely important to be highly credible: honor your commitments and complete your tasks. You should also smile a lot to show your positive spirit. 

Following those tips might help you become a Runner. But in the meantime, step aside to let the already established Runners reap their rewards. 

This might seem counterintuitive: contemporary work culture has created an environment of entitlement. Many of us feel that we deserve bonuses and high paychecks, as though everyone on the team deserves a trophy, and not just the Most Valuable Player. But that sends the wrong message: you shouldn't _get_ something if someone else worked harder for it. Unfortunately, today many people have forgotten how to work hard, but it's important to remember that you don't deserve an award just for showing up.

We've discussed how to become a Runner and how to work with other Runners. But what do you need to know to succeed as a driver?

### 6. Drivers and Runners must work together to create a productive team environment. 

The Driver is the boss: you map the route, you give directions, you ask for support. 

It's a vital role, but sometimes Drivers worry that the Runners will overtake them. Don't think that way! For the team to succeed, Runners and Drivers have to work together.

At first, Runners rely on their Driver for management and direction. But if the Driver doesn't allow the Runners to shine they will feel unappreciated and slow down. The Driver can prevent that by motivating employees, protecting them from jealousy and making sure they aren't constrained. 

Then, after taking care of the Runners, the Driver can turn their attention to other staffers. Joggers need support to become their best selves: to help boost their performance, tell them what you like about their work and give directions with praise.

Walkers can also improve. These workers typically lack role models — they have never learned how to have a strong work ethic. With Walkers, you need to be explicit about your high expectations — but if you guide them, they will grow. Help them cultivate a willingness to learn, grow and improve. 

Like Walkers, Riders are hopeless without role models. You have to decide whether they have the capacity to improve and make a worthwhile contribution to the organization. If so, start them out on grunt work and spend time guiding them. If that doesn't work, kick them out. 

And lastly, focus on the workplace culture. Make sure each member of your staff has the tools they need to meet your high expectations. Then make the journey toward your goals fun and enjoyable. 

To that end, creating space for special moments and joking around will boost productivity and produce an inspiring environment. For that to work, it's hugely important that you show your appreciation on an individual basis. 

Last tip: whenever you receive a reward, don't take it for granted — otherwise, that'll be your last reward!

> _"When your staff has fun at work, they work together more collaboratively. Every single day at work becomes a team-building activity."_

### 7. Final summary 

The key message in this book:

**If you want to build a successful organization, don't waste your time coddling underperformers. Instead, make high achievers your priority: these top workers are the ones bringing speed, creativity and energy into your organization. And if you can keep them motivated, they'll be the backbone of your organization.**

Actionable advice:

**Credibility is everything.**

If you want to play a significant role in any kind of organization, you must be credible. Otherwise, people won't feel that they can rely on you to do what's best for the team.

And the thing is, once you lose your credibility, it's almost impossible to gain it back. That's why you should _always_ honor your commitments and take responsibility for your mistakes. 

**Suggested** **further** **reading:** ** _How to Find Fulfilling Work_** **by Roman Krznaric**

If you feel trapped in your job or long for more fulfilling work, you're not alone. _How to Find Fulfilling Work_ (2012) explores the core components of what makes work meaningful and full of purpose, detailing exactly which steps you need to take to find work that brings out the best in you and keeps you truly happy.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Ron Clark

Ron Clark is a best-selling author and top consultant who helps huge corporations, small businesses and other organizations become faster, better and more effective. In 2007, he founded the Ron Clark Academy to train educators and businesspeople. _Move Your Bus_ is his fourth best-selling book.

