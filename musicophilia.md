---
id: 5385e0cc6162380007440000
slug: musicophilia-en
published_date: 2014-05-27T13:25:39.000+00:00
author: Oliver Sacks
title: Musicophilia
subtitle: Tales of Music and the Brain
main_color: 56C2F0
text_color: 316F8A
---

# Musicophilia

_Tales of Music and the Brain_

**Oliver Sacks**

_Musicophilia_ explores the enriching, healing and disturbing effects of music. It delves into fascinating case studies about disorders that are expressed, provoked and alleviated by music.

---
### 1. What’s in it for me? Discover the weird and wonderful world of the musical brain. 

Since the dawn of humanity, music has brought people together: to sing and dance, to relax or to go wild. This common love for music — our _musicophilia_ — creates a universal bond between all human beings.

Right?

Not quite.

In these blinks you'll explore not only the wonders, but also the horrors that music can induce: how music can both cure immobility and bring back language to people who've lost the ability to speak, but also cause terrible hallucinations and epileptic seizures.

You'll also learn about the people who see beautiful rainbow colors when they listen to music.

And finally, you'll find out how to suddenly develop musical creativity. Simple: just get yourself struck by lightning.

> _"Music, uniquely among the arts, is both completely abstract and profoundly emotional."_

### 2. Not everyone can comprehend, produce or even enjoy music. 

_Musicophilia_, the propensity for producing and the desire to listen to music, exists in virtually every culture.

But this doesn't mean that every single individual is occupied or preoccupied by music. In fact, some people don't have musical abilities, while others are completely indifferent to music. This latter category of people have a condition called _amusia,_ which means they lack certain kinds of musical abilities.

One kind of amusia is _tone_ _deafness_, present in about five percent of the population. People who are tone deaf don't realize if they hit the wrong notes while singing, and are also unable to recognize when others sing off-key.

Another kind of amusia is _rhythm_ _deafness_, meaning the inability to follow the rhythm of a piece of music. One famously rhythm-deaf person was Che Guevara. It was said that when the orchestra played a tango, he would dance a mambo.

There are also many cultural forms of rhythm deafness, meaning the inability to follow musical rhythms from "foreign" cultures. Research shows that infants can detect all kinds of rhythmic variations, but by 12 months their range has already narrowed down to the ones they hear in their daily lives — the ones in their culture.

While those with gross tone or rhythm deafness can still enjoy music and dancing, people with amusia in its most absolute sense don't even experience music as music. For such people, melodies simply don't sound like music and might even acquire a disturbing character. Instead of hearing what we think of as music, a man with absolute amusia said he heard the sound of a screeching car!

There are also people who don't have any problems perceiving music, but simply don't enjoy it. Certain historical figures reported that they were indifferent to music. For example, in his autobiography, Darwin reveals that in his adult life he lost his feeling for music. And Freud once explained that he never listened to music voluntarily because he was incapable of deriving any pleasure from it.

> _"The embedding of words, skills or sequences in melody and meter is uniquely human."_

### 3. Musical training results in noticeable physical changes to the brain. 

Now that we've looked at people who seem to lack musical abilities, let's turn our gaze to musicians. What distinguishes them from the rest?

Research shows that musicians' brains are noticeably different from those of non-musicians. Using brain imaging, neuroscientists have made precise comparisons of the sizes of various parts of the brain in musicians and non-musicians. These differences are so noticeable that experienced anatomists can instantly recognize the brain of a professional musician, whereas they would find it difficult to identify the brain of a writer, a visual artist or a mathematician.

But how do musicians' brains get so musical? Are they born that way?

Research suggests that musicians are in fact not born with noticeable differences in brain structure, but acquire them through training. The effects of early study in particular are huge: the neurological changes observed in musicians' brains are strongly correlated with the age at which they began training, as well as the intensity of their rehearsal and practice. One study showed that just a single year of violin training resulted in pronounced changes in children's brains. But in fact our brains respond rapidly to musical training at any age. A study has shown that five-finger piano exercises can start changing the brain in a matter of minutes!

However, it's the _innate_ _musicality_ enjoyed by nearly everyone that provides the foundation that allows musical training to happen in the first place.

Research shows that virtually everyone responds to musical training, suggesting that we can all train our brains to become like those of a musician. The clearest example of this is the Suzuki method, in which children are trained to play the violin through listening and imitation: practically all children who can hear respond to the Suzuki method.

So while musicians aren't born with musical brains, their training certainly changes their brain structures remarkably.

> _"It really is a very odd business that all of us, to varying degrees, have music in our heads."_

### 4. People with absolute pitch can tell the pitch of any note – but this is not always a good thing. 

What pitch do you blow your nose in? To most people this seems like an absurd question. But people with _absolute_ _pitch_ can instantly recognize the pitch of any sound they hear — even you blowing your nose.

For instance, Sir Frederick Ouseley, a former professor of music at Oxford, could detect that the wind whistled in D, that thunder rumbled in G, and the clock struck in B minor. Amazingly, when his remarks were tested, they would always be correct, demonstrating that for people with absolute pitch, each tone is absolutely distinguished from all others by a unique and characteristic quality.

But not everyone is blessed — or cursed — with absolute pitch, and it's much more common in musicians than in the general population. While only one person in 10,000 has absolute pitch, research shows that ten percent of all musicians might have absolute pitch, often acquired through early musical training.

Absolute pitch doesn't seem to be of much importance to musical abilities though, even to extraordinary musicians. For example, Mozart had it, but Wagner and Schumann lacked it.

And while absolute pitch may sound like a favorable extra sense, for some musicians it can be distressing and even disabling, because of — for example — their hypersensitivity to out-of-tune musical instruments.

_The_ _Oxford_ _Companion_ _to_ _Music_ describes an outstanding pianist struggling to get through the otherwise fairly easy "Moonlight Sonata" simply because the pitch the piano was tuned to was unfamiliar to him. He was perturbed by playing the piece in one key, and hearing it so clearly in another.

> **Remember: It's never too late for an instrument.  

** Studies show that your brain responds to musical training even in late adulthood, so if you're longing to play bluegrass banjo, don't hold back: it's never too late!

### 5. Musical abilities can be enhanced by blindness or a condition called synesthesia. 

Have you heard any orange songs lately? What about green ones? For most people, these questions sound surreal, but for a select few they make total sense.

These people have a condition called _synesthesia_. Synesthesia is the activation of one sensory experience by the stimulation of another sensory experience: for example, seeing the color blue when hearing a G sharp. And in some cases, synesthesia can lead to extraordinary musical capabilities.

We see this in musical synesthetes who feel that their disorder is central to their musical thought and abilities. Most musical synesthetes automatically see colors when they hear music, just like the composer David Caldwell, for whom each musical key, pattern, and mood has its own color. He explains that color helps enrich and clarify his musical endeavors, because when the colors seem right, he knows he's composing in the right direction.

Another disorder that can enhance musicality is _blindness_. For example, blind children's lack of a visual world often leads them to fill the void with a rich world of sound instead. Some spontaneously exhibit exceptional musical abilities even without any formal teaching, which indicates that the musicality is a direct and natural response to a lack of visual stimuli.

This is further illustrated by research showing that when visual input is suddenly lost, the brain's visual cortex won't just remain functionless — instead, other sensory inputs will be reallocated there. This means that the brain cells that used to deal with visual inputs now start devoting themselves to dealing with auditory sensory information, thus "heightening" that sense. This can help explain why 60 percent of blind musicians have absolute pitch, compared to only ten percent among sighted musicians, and why the loss of vision — and the consequential rewiring of the brain — sometimes also leads to synesthesia.

### 6. Extraordinary musical gifts can appear in people with very low intelligence. 

It is a widespread belief that it takes a highly intelligent person to become musical — but this is far from always the case. Actually, musicality is the most common type of talent among _savants_ — mentally disabled people who show extraordinary abilities.

Take, for instance, Martin, a retarded man who developed a great passion for music as well as a _phonographic_ _memory_, meaning that he could remember every piece of music he heard: he knew more than 2,000 operas, and could sing the melodies or play them on the piano from memory. The author tested him on his knowledge of operas, and realized that Martin not only remembered them perfectly, but also remembered the exact score for every instrument and singer. Furthermore, when he was played a piece he had never heard, he could repeat it almost flawlessly, and was even able to transpose it into different keys.

The heightening of Martin's musical powers, along with the poor development of his other abilities, can be explained by damage to the left hemisphere of the brain. This can result in an increased access to the musical abilities of the right side of the brain that are normally inhibited by the left hemisphere.

Another group of people who are typically extremely musical are those with _Williams_ _syndrome,_ who usually have an IQ of less than 60.

Williams syndrome is very rare, affecting perhaps one child in ten thousand, and is characterized by heart defects, unusual facial structures and retardation, but also a general friendliness and sensitivity to music. Studies show that people with Williams syndrome employ a much wider set of neural structures to perceive and respond to music, making them almost helplessly attracted to music. For instance, Gloria Lenhoff, a young woman with Williams syndrome, learned how to sing operatic arias in more than 30 languages. And at the same time, she couldn't even add four plus three.

### 7. Music can ease movement disorders and even help people regain motion of their limbs. 

Have you ever been out jogging and been pushed extra hard by the energizing effect of a song? In fact, this mobilizing effect is also used in a variety of medical domains.

For example, Tourette's syndrome can sometimes be eased by music. Tourette's syndrome is characterized by often explosive tics, like jerking, mimicking and even yelping. But playful jazz or rock has been found to help sufferers due to the genres' freedom to improvise. For example, the professional jazz drummer David Aldridge used drumming to direct the unruly energy of his Tourette's into an orderly flow.

Parkinson's disease, a disease of the nervous system marked by imprecise movement and muscular rigidity, can also be helped by the rhythm and flow of music. For many Parkinson's patients the right kind of music — smooth with a well-defined rhythm — can help them move more easily. For the patient Frances D., music seemed to be as effective as a drug, and whenever music was played, she could regain once more her normal flow.

On top of treating diseases, music can also activate people who have become immobile due to a stroke or an accident.

When someone loses their mobility — for example, because their leg has become temporarily paralyzed — they sometimes "forget" how to use the affected limb. In these cases, music sometimes has the ability to kickstart the limb into action again, because it activates the motor system.

For example, the author had a patient at a nursing home with a leg that was useless due to the many weeks of immobility caused by a hip fracture. But through a systematic exposure to Irish jigs — music that seemed to automatically reactivate her leg **–** the patient slowly started to recover her ability to move her leg and was eventually able to walk again.

> _"Music can pierce the heart directly; it needs no mediation."_

### 8. Music therapy can help people with dementia and speech problems. 

Even though it seems that the power of music has always been central to humanity, the concept of _music_ _therapy_ is relatively new.

In music therapy, patients compose, listen to and play music to accomplish individualized therapeutic goals. It arose in response to the return of damaged and traumatized soldiers from the Second World War. The first music therapists soon discovered that music could soothe both their physical and psychological misery.

Today, music therapy is widely recognized as a form of therapy and is mainly used to help people with _aphasia_ (i.e., they can't speak).

For example, Samuel S. developed severe aphasia following a stroke, and was unable to express even a single word despite two years of intensive speech therapy. But even though he couldn't speak he could still sing a little. And after hearing him sing two words of "Ol' Man River," a music therapist decided to take up the challenge — and he soon started to make progress. After only two months of singing along with his therapist and practicing different ballads, he was able to make short but meaningful responses to questions.

For Samuel, music therapy worked much better than speech therapy because his stroke had caused disturbances in the speech area of the brain without affecting the structures involved in singing. Therefore, activating the singing part of his brain helped stimulate activity in the damaged linguistic parts.

But music therapy isn't only used for people with aphasia. It can also be beneficial to people with _dementia_.

Dementia is a brain disorder caused by brain disease or injury, and is characterized by impaired reasoning and increasing memory loss, leading to a seemingly mindless state.

Luckily music therapy can help even in extreme cases of dementia, because musical memory and emotion can survive long after other forms of memory have disappeared. For example, the dementia patient Woody, who couldn't remember much of anything about his life, could still recall and sing almost every song he had ever sung. And when he sang, for a moment, he returned to life.

> _"Music can pierce the heart directly; it needs no mediation."_

### 9. Some people experience terrible seizures from hearing music. 

If you really don't like a particular piece of music, you probably just turn it off, or move out of its reach. But imagine reacting so strongly to certain songs that you have _a_ _seizure_ as soon as you hear it.

This can happen to some people who have _epilepsy_, a disorder involving convulsions and loss of consciousness, often in reaction to intense sensory input. But while most people know that epileptic seizures can be triggered by bright flashing lights, it's less well known that some epileptic patients can also get seizures from music.

This disorder is called _musicogenic_ _epilepsy_, and the music that triggers this kind of seizures varies from patient to patient. Some sufferers relate that their seizures are provoked only by certain instruments, while others are sensitive to certain notes, melodies or songs. For instance, one of the author's patients, Silvia N., would have an epileptic seizure every time she listened to her favorite CD of Neapolitan songs, while a Mr. S. had a seizure from simply _imagining_ a piece of music!

These music-induced seizures can become so severe that they lead to a fear of music. Nikonov, a famous nineteenth-century music critic, developed strong _musicophobia_ following the terrible seizures he experienced listening to music. After his first seizure at an opera, he became increasingly sensitive to music, until eventually almost any music, however quiet, would send him into convulsions.

### 10. Hearing loss can lead to intrusive and distressing musical hallucinations. 

You've probably experienced a catchy tune that sticks in your head for hours or days on end. But imagine what it would be like if these "_earworms"_ had the quality of _actual_ perception — as though you had real speakers playing in your head!

Many people actually do experience this kind of _musical_ _hallucination_, which feels as though they have actual music playing internally. Most sufferers emphasize that the music seems at first to have an external origin, and it's only when they can't find an external source for it that they realize the music is being generated by their own brain. One of the author's correspondents called his hallucinations his "intracranial jukebox" — the jukebox in his skull.

One cause of these musical hallucinations that physiologists have identified is the loss of hearing.

This is shown by the large amount of musical hallucinations in elderly people suffering from increasing deafness. For example, Mrs. C. is one of the many people who started having severe musical hallucinations after experiencing profound hearing loss. Her music was loud and intrusive, jumping erratically from song to song, switching between songs from musicals, Christmas carols and patriotic tunes. Her "music" would stop only when she was intellectually engaged with something like a conversation or a card game.

But although musical hallucinations can be explained physiologically, they have no specific cure.

Mrs. C. was afraid that her musical hallucinations were signs of mental illness — until a neurological examination proved otherwise. The examination showed that the hallucinations were a result of her sound-deprived brain trying to stay active by creating its own audio stimulation. This auto-stimulation activated the same brain structures that are normally activated in the perception of "real" music — which created the hallucinations.

After attempting many treatments and even having her hearing restored, Mrs. C could not free herself from the hallucinations, and eventually just learned to live with them.

### 11. Some people become “possessed” by musical powers quite late in life. 

Wouldn't it be nice if out of nowhere you suddenly became blessed with musical super powers? You might want to be careful what you wish for: it has actually happened to a few, and some become so possessed by their musical ability that there's no room in their life for anything else.

For example, after being hit in the face by lightning, Cicoria, a family man of 42 previously indifferent to music, became so obsessed with it that he scarcely had time for the rest of the world.

A couple of weeks after the accident, when his energy had returned and neurological examinations had shown that he hadn't suffered any brain damage, he suddenly developed an uncontrollable desire to listen to piano music. He started buying recording after recording, but because he also craved the ability to play the music, he borrowed a piano, and started constantly practicing and writing the compositions he heard in his head. Eventually, he dedicated so much time to music that his wife divorced him — but even that hasn't stopped his passion, and now he plays brilliantly.

However, the neurological basis of his sudden musicophilia and his musical powers remains unknown. His brain scans showed no signs of brain damage — usually the cause of the sudden release of musical talents.

Another of the author's correspondents, Grace M., described a similar emergence of musical passion and talent — although she wasn't struck by lightning, nor seemed to have any other physical or psychological condition that could explain the abrupt change.

At age 55 she suddenly started to hear song fragments in her head, and since she didn't know how to notate music, she recorded her melodies onto tapes. This sudden appearance of songs and song fragments surprised her, since she had never had any musical ability beforehand. Three years later, she had recorded more than 3,300 fragments, and made four complete songs a month that received praise from even professional musicians.

### 12. Final Summary 

The key message in this book:

**People's** **sensitivity** **to** **music** **varies** **greatly:** **some** **people** **can** **tell** **the** **pitch** **of** **any** **note,** **while** **others** **completely** **lack** **musical** **ability** **or** **feeling.** **Brain** **disorders** **can** **change** **our** **experience** **of** **music,** **some** **inducing** **great** **musical** **passion,** **and** **others** **making** **the** **sound** **of** **music** **a** **horrifying** **experience.** **Music** **also** **has** **the** **power** **to** **heal** **a** **troubled** **mind,** **and** **even** **revive** **a** **motionless** **limb.**
---

### Oliver Sacks

Oliver Sacks is a British-American physician, writer and professor of clinical neurology at Columbia University. He is also the author of _Awakenings_, which was adapted into an Academy Award-nominated film, and the bestselling _The_ _Man_ _Who_ _Mistook_ _His_ _Wife_ _for_ _a_ _Hat_.

