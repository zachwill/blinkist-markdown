---
id: 5398598d3163350007190000
slug: my-age-of-anxiety-en
published_date: 2014-06-10T13:35:01.000+00:00
author: Scott Stossel
title: My Age of Anxiety
subtitle: Fear, Hope, Dread and the Search for Peace of Mind
main_color: FCF135
text_color: 635F15
---

# My Age of Anxiety

_Fear, Hope, Dread and the Search for Peace of Mind_

**Scott Stossel**

This book offers a candid, valuable glimpse into the world of the clinically anxious. The author takes us through his personal struggle with anxiety while presenting us with scientific, philosophical and literary work about the condition and the treatments available for it.

---
### 1. What’s in it for me: Find out what it is really like to suffer from clinical anxiety. 

Most of us suffer from "normal" anxiety occasionally, such as being nervous about a big event, or giving a presentation in front of an audience.

Some of us also experience a more severe anxiety such as worrying a lot or having a phobia. However, these things don't usually keep us from leading a "normal" life.

In extreme cases, there is clinical anxiety. Those who suffer from clinical anxiety are scared of things that aren't intrinsically dangerous (like public speaking) and have extreme reactions to them, such as vomiting or fainting.

In these blinks, you'll discover:

  * Theories on the cause of anxiety;

  * How everyday actions can become almost impossible for those who are clinically anxious;

  * How anxiety got the author into embarrassing situations, like flooding the Kennedy family's bathroom.

### 2. Clinical anxiety is the most common form of mental illness. 

Most us would agree that anxiety is fairly normal. However, few people are aware that clinical anxiety is actually the most common form of mental illness. It is diagnosed even more often than depression, and one out of six people worldwide will be clinically anxious for at least one year in their lifetime.

In addition, anxiety seems to be a characteristic that defies the boundaries of both culture and time.

For instance, in Spanish-speaking South America anxiety is referred to as "_ataques_ _de_ _nervios_," Greenland Inuit call it "kayak angst" and Iranians talk about "heart distress." However it's labeled, these terms all refer to a common state.

Clinical anxiety not only shows up across countries and cultures, but is also not tied to any particular era and has been discussed in documents throughout history.

For instance, Plato and Hippocrates had their theories about the disease, Spinoza wrote of it, and Sigmund Freud was preoccupied with finding and defining the mechanisms of anxiety.

It is important to note that anxiety is not a weakness of character. In fact, some argue that anxiety is actually the motor of civilization, creativity and inventive genius.

Indeed, many people noted for their success and influence such as Gandhi, Charles Darwin and Barbra Streisand have struggled with anxiety. Along with these well-known and respected personalities, some 40 million Americans have also been diagnosed with the illness.

With such an astounding statistic, it would be absurd to call all of these people "insane."

Far from being a "crazy" person who cannot succeed in life, the author himself is a successful journalist and editor who can provide for his family and raise children despite his chronic anxiety.

So it is evident that clinical anxiety is a universal illness to which no one is really immune. So what is it like to live with anxiety?

### 3. Anxiety is a disease that makes one’s life hard, stressful and embarrassing. 

Life with anxiety is a constant battle. Some people compare it to living with diabetes, insofar as the anxious have to handle painful inflictions every day. Like the diabetic who has to constantly monitor blood sugar levels and inject insulin, a clinically anxious person must perpetually watch out for stressful situations and be ever-ready to drug themselves for relief.

Sadly, being anxious is a dramatic limitation to ones everyday life — many anxious people are housebound, often preferring to stay home in order to feel safe and in control.

For example, one clinically anxious man wasn't able to walk within a five-kilometer radius around his house without vomiting blood uncontrollably.

Some of life's basic demands are almost impossible for an anxious person to fulfill. For instance, the author is not able to get on an airplane or speak in public without consuming a concoction of medication and alcohol.

Attachment is another serious issue for anxious people, as they tend to get overly attached to their loved ones.

For example, as a child, the author couldn't stand to be separated from his parents and when they were gone he would call their friends, convinced that his parents had died.

Another struggle faced by anxious people is that their behavior is often unpredictable and embarrassing.

For instance, when traveling in a foreign town, the author visited nearly all of the sanitary facilities but none of the actual sights, because he was so wracked with nerves.

The author also once had the opportunity to visit the Kennedy family. Unfortunately, due to his sensitive stressed-out bowels, he ended up clogging their toilet, and leaving the entire bathroom flooded.

### 4. The cause of anxiety might be determined in childhood. 

There are different theories on what causes anxiety. Psychoanalysts, for example, believe anxiety derives from the repression of taboo thoughts that first arise during childhood.

Take, for example, Freud's Oedipus complex theory. Freud believed that boys sexually desire their mother and envy their father, while girls desire their father and envy their mother.

These sexual or envious thoughts are never acted out, because the child is scared of being punished for thinking them. As a result the thoughts persist and are projected onto an object that causes the child to feel anxious.

Freud himself believed that his own phobia of trains originated from seeing his mother naked on a train when he was a child, and desiring her.

However, the Oedipus complex is now deemed an outdated theory.

Another theory looks specifically at mother–child relationships.

Specifically, it is thought that children who endure longer separations from their mothers tend to become anxious.

One study on Rhesus monkeys showed that those that were separated from their mothers suffered from long-term effects like anxiety, in addition to aggression and social abnormality in adulthood.

The way a mother interacts with her child seems to be of particular importance in determining how anxious the child will become in stressful situations.

For example, one experiment showed that a child whose mother is mindful, caring and loving behaves less anxiously in new situations (for example, when a stranger enters a room and the mother leaves) than a child whose mother acts ambivalently and anxiously herself.

So what could have caused Freud's anxiety? Well, it could be explained by the fact that his mother became depressed after his brother died and stopped taking care of her older son.

The author, too, had an anxious and phobic mother and this may have influenced his own anxiety.

Therefore it appears that the way a child is raised has a considerable influence on its future behavior and level of anxiety.

### 5. Anxiety is an evolutionary adaptation and is inherited through genetics. 

Although it is not easy to live with, anxiety is not necessarily a deficit or a disadvantage. It is in fact an evolutionary adaptation that can be found in our genes.

According to the "survival of the fittest" theory, we are programmed to be anxious toward dangerous things, since someone who is afraid of a threat such as a snake or a high cliff is more likely to survive than someone who is not.

Therefore these phobias are easily developed because they were useful fears in the past.

However, clinically anxious people are afraid of things that aren't intrinsically dangerous and the potency of their anxiety does not make evolutionary sense.

Take the author's phobia of cheese, for instance. Clearly this not adaptive, nor useful.

It has also been found that the chance of being clinically anxious increases with certain genes.

Higher levels of anxiety can already be observed a few weeks after birth, which indicates a genetic facet to the illness.

Studies have shown that 15 to 20 percent of infants are significantly more anxious than others only a short time after birth. Studies also show that these infants become very anxious when they reach adulthood.

The author also notices some genetic link between his anxiety and his daughter's anxiety, given that she even shares the same phobias, despite her parents raising her in a loving, caring manner.

Which genes are responsible for anxiety, then?

Well, scientists have been able to identify some of them.

The stathmin gene, for example, provides the ability to feel fear. When this is removed from mice, they no longer show fear.

There is also a correlation between variants of a gene called RGS2 and people who are highly anxious.

So we can see that a certain amount of anxiety ensures ones survival. However, some people are _overly_ anxious and this may be caused by their genes.

### 6. Anxiety is a product of the body and can therefore be influenced by drugs. 

So we have seen how anxiety came about through evolution and how it can be passed on genetically. Now let's take a look at the physiology of the condition and how it may be influenced by drugs.

First, we know that anxiety is created in the brain.

Thanks to functional Magnetic Resonance Imaging (fMRI) brain scans, we can observe that anxiety is linked to hyperactivity in certain brain areas.

This hyperactivity can be seen in the frontal lobes of the cerebral cortex when worry about future events is triggered, whereas increased activity in the anterior cingulate is linked to fear of public speaking.

Aside from knowing where clinical anxiety occurs, we also know that it derives from a defective neurotransmitter system in the brain.

Specifically, clinically anxious people produce less serotonin (a neurotransmitter that regulates emotions such as satisfaction and happiness) than normally anxious people.

So what can we do with these findings? Well, we can look into drug treatment. Medication is one way to treat anxiety, but it is not for everyone.

Anti-anxiety drugs work by influencing neurotransmitters in the brain. The drug Xanax works by binding to GABA neurotransmitters that inhibit the activity of the central nervous system, which has a calming effect.

Drugs are a popular option for clinical anxiety sufferers. In 2005 alone, there were 53 million prescriptions for Ativan and Xanax.

However, medication is controversial as there are severe side effects, risk of addiction and theories that certain drugs aren't any more effective than placebos.

Surprisingly, a 2003 study showed that only one out of three patients actually felt better after taking anti-anxiety drugs.

As for addiction, the author himself became addicted to anti-anxiety drugs, including Xanax and Paxil. He once tried to go without them but found the experience unbearable, and as a result returned to medication after just one week.

### 7. Therapy can also be valuable in treating anxiety. 

We have seen how medication functions in the brain. However, there are also alternative treatments such as cognitive behavioral therapy or CBT, which we will look at below.

CBT is based on the theory that there are behaviors (like anxious behavior), that can't be controlled through rational thinking.

The most common approach used in CBT is exposure therapy. This involves a patient having to directly face what he or she fears in order to learn that there is no actual threat present in the feared object.

For example, the author himself suffers from emetophobia — the pathological fear of vomiting. In an attempt to treat this, he once tried exposure therapy.

As part of the exposure, he had to take emetics to induce vomiting. Unfortunately, this ended in disaster, and rather than vomiting from the emetic, he ended up choking.

Cognitive behavioral therapists also believe in getting to the source of anxiety and that this is often hidden in the patient's mind.

The author's therapist used what is known as _imaginal_ _exposure_, believing that there was something more existential that caused his anxiety.

In therapy sessions, the author had to list a hierarchy of things that scared him, then picture those things and tell the doctor what he was feeling.

On one occasion, the author started crying without really knowing why. However, the doctor believed that they were on the right path to finding an explanation for the author's anxiety.

To this day, the author is still fighting his anxiety with drugs and CBT.

Although there seems to be no universal cure for anxiety, there are certainly ways to reduce its intensity. The author hoped that this book would help him with that.

### 8. Final summary 

The key message in this book:

**Clinical** **anxiety** **is** **a** **more** **common** **and** **more** **severe** **illness** **than** **one** **might** **think.** **However,** **there** **are** **many** **different** **ways** **to** **reduce** **and** **treat** **the** **condition.** **It** **is** **important** **to** **note** **that** **even** **when** **a** **sufferer's** **anxiety** **is** **not** **always** **under** **full** **control,** **it** **doesn't** **mean** **that** **a** **life** **with** **anxiety** **is** **a** **poor** **life.**
---

### Scott Stossel

Scott Stossel is an American journalist, editor of the _Atlantic_ magazine and author of the book _Sarge:_ _The_ _Life_ _and_ _Times_ _of_ _Sargent_ _Shriver._ He wrote _My_ _Age_ _of_ _Anxiety_ with the hope that it would help him understand his anxiety and find relief from his suffering.

