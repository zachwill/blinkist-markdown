---
id: 5a65babfb238e100076c4ef3
slug: my-stroke-of-insight-en
published_date: 2018-01-26T00:00:00.000+00:00
author: Jill Bolte Taylor
title: My Stroke of Insight
subtitle: A Brain Scientist's Personal Journey
main_color: 608235
text_color: 4D692B
---

# My Stroke of Insight

_A Brain Scientist's Personal Journey_

**Jill Bolte Taylor**

_My Stroke of Insight_ (2008) is about Jill Bolte Taylor, a neuroscientist who suffered a stroke in her mid-thirties. These blinks detail her personal story from medical school to experiencing a stroke to learning how to walk, talk and even identify colors again.

---
### 1. What’s in it for me? Get insight into the workings – and malfunctions – of the human brain. 

Have you ever had that feeling of getting an epiphany, grasping something you didn't before? Most of us have experienced that moment when a stroke of insight makes us understand or see something in a new light. Now imagine that it was an actual stroke.

That was what happened to the author on 10 December 1996 when she was 37 years old.

Up to that point, she'd spent most of her life figuring out the workings of the human mind. The stroke made her experience first hand the different traits of the two halves of her brain. Here we will get her personal story, and the resulting wisdom, of suffering a grave blow — and recovering from it.

You will also find out

  * how schizophrenia made the author interested in understanding the brain in the first place;

  * the remarkable differences between the brain's right and left hemispheres; and

  * how the author's stroke made her experience bliss

### 2. The author became a neuroanatomist to understand her schizophrenic brother. 

The author, Jill Bolte Taylor, grew up in Terre Haute, Indiana. She was actually inspired to become a neuroanatomist — a doctor who specializes in human anatomy and how the nervous system functions — by one of her brothers, who was diagnosed with schizophrenia.

Bolte Taylor was intrigued that his view of the world was disparate from hers despite the fact that they were siblings so close in age. The way he processed information, and therefore his behavior, differed from hers.

Consequently, the author became interested in the way the human brain functions. She embarked upon intense academic training so that she could research the biological grounds for schizophrenia.

Bolte Taylor was accepted as an undergraduate at Indiana University where she studied human biology. At the same time, she got a job at the Terre Haute Center for Medical Education. It was here that she worked as a lab technician in both the Human Anatomy Lab and the Neuroanatomy Research Lab.

Later, the author joined a six-year Ph.D. program in the Department of Life Science at Indiana State University. In 1991, she received her Ph.D. and a couple of years later spent time at Harvard Medical School as a postdoctoral researcher with the Department of Neuroscience.

Eventually, she landed her dream job at the Laboratory for Structural Neuroscience at McLean Hospital in Belmont, Massachusetts. The lab was run by renowned neuroscientist Dr. Francine M. Benes. Bolte Taylor admired Benes for her expertise in analyzing the postmortem brain in order to understand schizophrenia.

This is how Bolte Taylor became interested in, and began working with, the human brain. In the following blink, you'll find out some scientific facts behind the harrowing, life-changing experience she endured when she had her stroke.

### 3. Ischemic and hemorrhagic are the two types of stroke; Bolte Taylor suffered the latter. 

Most people know what a stroke is to some extent. It's commonly understood that it involves the brain, is dangerous and can be fatal. What's less well-known is that there are two types of strokes: _ischemic_ and _hemorrhagic_.

The first type of stroke, ischemic, occurs when blood clots in the artery. This prevents the oxygen normally carried in the blood from flowing to the brain's cells and neurons.

The arteries themselves become thinner as they extend away from the heart to reach more distant parts of the body like the brain. Therefore, if the blood starts to clot and can no longer flow through the artery, the brain's cells are starved of oxygen and either become traumatized or die.

The other type of stroke, hemorrhagic, is what Bolte Taylor experienced. This occurs when an artery bursts and blood floods the brain. The heart pumps blood into the brain through the arteries and then retrieves it via the veins. In the case of a stroke, the blood is pumped at a very high pressure through the arteries and it's far too much for the veins to handle. That's why humans are usually born with a capillary bed which lowers the pressure.

However, if an individual is born with _arteriovenous malformation_, like Bolte Taylor was, they have a malformed capillary bed or none at all, which means there's no buffer between the arteries and veins. Instead, the blood flows directly from the arteries to the veins until the veins can no longer handle the pressure, causing them to burst.

When this occurs, blood directly mixes with the brain's neurons, resulting in severe or even fatal damage to the brain.

Now that you've learned about the two types of stroke, it's time to discover more about the brain.

> _According to the American Stroke Association, 83 percent of all strokes are ischemic strokes._

### 4. The two cerebral hemispheres are different but complement each other. 

Structurally, your brain has two halves — the left and right hemispheres — which look very similar but have completely different functions.

The right hemisphere controls all sensory aspects; here, what you see, smell and taste is combined with you thoughts. This is then translated into a big picture of what's happening at any given moment.

The right hemisphere is concerned with "now." This half of the brain doesn't have a way of recollecting past experiences and isn't concerned with the future, so it's where creative and intuitive thinking happens.

Contrarily, the left hemisphere focuses on language, numbers, patterns and categories. The fluid moments that the right side deals with are ordered by the left brain. This portion gives you a sense of time and linearity; it's here that you understand past, present and future.

This area of the brain makes you appreciate the fact that to achieve B, you must first do A. Consider the process of getting dressed: thanks to the left hemisphere, you know that you need to put on your socks before your shoes.

Despite having different jobs, the two hemispheres collaborate so that you can function in everyday life. They complement one another by piecing information together to create a nuanced perception of reality.

Think about the way both halves work together to formulate language. The left hemisphere allows us to appreciate the meaning of words and how sentences are structured, while the right hemisphere contextualizes language. You need it to process aspects of communication including nonverbal cues like facial expressions. If the language center of your right hemisphere were damaged, you'd take every message literally.

You now understand the brain's hemispheres as well as the types of stroke a person can suffer. Up next is the author's personal experience of having a stroke.

### 5. On the morning of her stroke, the author experienced disconnection, momentary awareness and bliss. 

Bolte Taylor woke up on 10 December 1996 with an awful headache which she could feel behind her left eye. Trying to alleviate the feeling, and unaware of the danger she was in, she began to exercise.

As the morning continued, she felt increasingly disconnected from herself and her surroundings. The author experienced a peculiar feeling that gradually took over both her mind and body. She was oversensitive to light and sound and felt her cognitive function disintegrate as if she were observing rather than participating in her own life.

To Bolte Taylor, it seemed like a limbo state — that moment right before waking up where you're caught between reality and some other world. But rather than awaking, she stayed trapped there.

Soon afterward, Bolte Taylor found her physical abilities began to degenerate. As she traveled to the bathroom, she noticed that her balance was off and her movements were suddenly jerky and graceless. She attempted to get into the bathtub by leaning against the wall for support and found that she had to fiercely concentrate to coordinate her thoughts and movement.

While showering, she switched back and forth between full awareness of what was going on and what she described as a blissful state.

The author could only comprehend what was occurring in brief bursts. She kept forgetting who she was and her understanding of language disappeared. Interestingly, she also felt as if she were in a place of tranquility — no thoughts flowed through her head.

At one point, she managed to realize the trouble she was in and waddled to the phone. In a moment of clarity, Bolte Taylor recalled the number of her colleague Dr. Stephen Vincent. She was only able to grunt meaningless sounds, but he understood the gravity of the situation and rushed over to get her to the hospital.

### 6. The days that followed were tough for Bolte Taylor, but her physical abilities quickly started to improve. 

When a person has a stroke, their doctor usually orders an _angiogram_. This is an x-ray used to see the blood vessels in the brain so that the doctor can understand what type of stroke they're dealing with and treat the patient accordingly.

Three days after Bolte Taylor's stroke, the doctors had the results of her angiogram. She had suffered a hemorrhagic stroke caused by an undiagnosed arteriovenous malformation. The neurosurgeon suggested that they perform a _craniotomy_ — a surgery that involves removing a piece of the skull to access the brain.

The surgeon wanted to remove what was left of the author's arteriovenous malformation as well as the blood clot, which was similar in size to a golf ball. He was concerned that without this procedure, the author was likely to experience more hemorrhages.

Bolte Taylor needed to be persuaded to have the craniotomy because she was worried that she'd never fully recover from such an invasive procedure; she was already exhausted. However, she understood the potential consequences of not going ahead with the operation.

In preparation, the author started to ready herself for the surgery. Luckily, she made remarkable physical progress; a few days after her stroke, she managed to both sit down and stand up with someone supporting her.

In the weeks that followed, Bolte Taylor continued to improve. Soon, she was able to move herself from lying down to sitting. However, she wasn't able to do this in one fluid movement — she had to divide the effort into smaller steps. She would rock her body back and forth in sequences, attempting more motion each time until she was finally able to propel herself upward.

Although the author's progress was impressive, she still had much work to do to prepare herself for surgery and the long journey toward recovery that lay ahead of her.

### 7. Bolte Taylor returned home and steadily improved both mentally and physically. 

Five days after her stroke, the author was released from the hospital. In the months that followed, her mother stayed by her side and helped her recover from surgery. She had to learn how to talk, read, write and walk from scratch.

Luckily, her mother intuitively knew how to support her with such tasks. The pair would regularly go for walks together, and every time Bolte Taylor made progress, her mother would celebrate with her.

Throughout her recovery process, the author needed considerable rest and sleep. Learning to walk was a slow, tiresome practice. Although she eventually fully regained her ability to walk, even walks to the bathroom were taxing at first.

Despite the hardship, Bolte Taylor continued to accomplish more. It only took her a matter of weeks to relearn how to walk with some assistance. Then, she started putting puzzles together and, in doing so, rediscovered the existence of color.

Next, she began to read again, which proved trying. After losing most of her ability to understand the world, the author had to really rack her brain to remember, say, what a prawn sandwich was.

In fact, letters weren't just letters when she started reading again: they appeared to her as odd squiggles. The practice of attributing sound to them was hard, and then putting them together with other sounds to create a word with a meaning attached to it seemed nigh on impossible.

However, Bolte Taylor managed all of it. As each day dawned, she regained a little more of her physical and mental strength. Eventually, she was ready for surgery.

### 8. The author realized that she needed people to believe in her and wanted to help others experience nirvana. 

The surgery was a success — with the clot removed from her artery, Bolte Taylor felt like her usual self again and was no longer separated from reality.

While she was recovering, she had several epiphanies. Firstly, she realized the importance of being surrounded by others who believed in her recovery and treated her accordingly. On several occasions, she heard doctors say that those who have survived a stroke shouldn't expect to ever fully recover — especially if they haven't done so within six months of the incident.

This notion demotivated Bolte Taylor, but as she healed, she found there was no truth to it. The author believed in the plasticity of the brain — the concept that when the brain is stimulated, it has the ability to change its neural connections. Therefore, the brain is capable of regaining some of its original functions.

The author thought that if the people around her had decided that she was no longer the intelligent person she once was, she wouldn't have had the impetus she needed to endure her recovery processes.

Bolte Taylor's second realization related to helping others feel the peace or _nirvana_ that she experienced. Buddhism teaches that nirvana is the ideal state of mind — it results in a feeling of sheer inner peace and joy.

Only the right hemisphere of the author's brain was operable: it was this portion that created a feeling of fluidity and being one with the universe. Therefore, she believes that by connecting with that half of the brain, everyone can experience that same tranquility.

This kept Bolte Taylor going throughout her recovery; if she didn't heal, she wouldn't be able to share her story with others to help them enter this blissful mind-set.

### 9. Final summary 

The key message in this book:

**Suffering a stroke is a debilitating experience. It takes a lot of perseverance and patience to recover, but as this personal account proves, it's possible to regain your former quality of life. In fact, valuable wisdom can be taken away from the experience.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _When Breath Becomes Air_** **by Paul Kalanithi**

_When Breath Becomes Air_ (2016) tells the incredible story of Paul Kalanithi, a neurosurgeon and neuroscientist who was diagnosed with and died from cancer in his mid-thirties. These blinks detail his extraordinary journey in search of the meaning of life in the face of death.
---

### Jill Bolte Taylor

Dr. Jill Bolte Taylor is an American neuroanatomist who specializes in mental illnesses. Her TED talk was the first of its kind to go viral and inspired many to buy her book, which subsequently became a _New York Times_ bestseller. For over a decade, Bolte Taylor was the president of NAMI, the National Alliance on Mental Illness, and is now the NAMI president emeritus for Bloomington, Indiana.

