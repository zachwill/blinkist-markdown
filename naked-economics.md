---
id: 5421391863656400081f0100
slug: naked-economics-en
published_date: 2014-09-25T00:15:00.000+00:00
author: Charles Wheelan
title: Naked Economics
subtitle: Undressing the Dismal Science
main_color: None
text_color: None
---

# Naked Economics

_Undressing the Dismal Science_

**Charles Wheelan**

This book provides you with an accessible yet comprehensive overview of economics. You'll be guided through the major themes of economics without all the baffling jargon, graphs and equations.

---
### 1. What’s in it for me? Learn the basics of economics effortlessly. 

_Naked Economics_ was written to cut through all the esoteric jargon that surrounds economics. It provides a broad overview of both micro- and macroeconomics in a manner that's accessible and interesting to non-experts. It explains the dynamics of economics on a grand scale, illustrating key principles that power everything from corner pretzel stands to massive trading deals between countries. These blinks also talk about why people invest so much money in the stock market and how governments make decisions about tax policy.

In addition, you'll learn

  * why SUVs need to be taxed;

  * why traffic lights aren't privately owned; and

  * why China keeps loaning the United States money to buy Chinese products.

### 2. The core principles of economics state that people and firms make decisions about money based on their best interests. 

Whether you're getting a cup of coffee or making a retirement plan, you probably make decisions about money every day. But of course, money doesn't grow on trees. So how do you decide when to part with your earnings? What drives your decisions to spend versus save?

Economics is the study of the processes involved in the production, consumption and transfer of wealth, both on a smaller scale (e.g., personal finances) and a larger one (e.g., international trade policy). The field is built on two assumptions about how we create and consume wealth.

The first assumption is related to why we buy things, namely that people strive to maximize their _utility_.

What do we mean by utility? _Utility_ relates to satisfaction with the material world. For example, buying a Ferrari might satisfy your lifelong dream of owning a luxury Italian sports car, but if buying it prevents you from paying your mortgage and your house is repossessed by the bank, you may end up homeless and significantly less satisfied — i.e., with less utility — than you were pre-Ferrari.

Basically, this first principle says that people make decisions about money according to their overall best interests.

But people aren't the only ones striving to maximize their utility: _firms_ and companies do, too.

A firm is any economic actor that sells products: it can be as small as a man selling pretzels on a street corner or as large as a multinational conglomerate like Boeing.

Either way, firms try to maximize their profits while minimizing their costs. This is the second principle of economics: like individuals, _firms_ operate according to their best interests.

The dynamic between the individual consumer and the firm that creates products is what shapes the market, the place where exchange happens.

These assumptions — that both individuals and firms strive to maximize their utility — are at the core of economics.

> _"As Gordon Gekko told us in the movie Wall Street, greed is good, so make sure that you have it working on your side."_

### 3. Supply and demand determine what products are sold where, and for what price. 

Picture a supermarket: the fresh fruits are towards the front, the dairy towards the back, and those delicious, chocolate-covered pistachios right near the checkout. Why do grocery stores seem to always have _exactly_ what you need?

It goes back to the core principles of economics: individuals and firms always strive to maximize their utility. This creates _supply and demand_, which not only determines what's available but also the _market price_.

The _market price_, or how much someone will pay for a product, is determined by two factors: how many other people also want to buy it and how much of it is available. For example, diamonds are in high demand but relatively scarce and thus expensive.

As stated before, _supply and demand_ also determine availability.

You'd be hard-pressed to find champagne and caviar at a football game, though you'd have no trouble finding plenty of beer and fast food. And that's not because hot dogs are _better_ than caviar but because not many people want to eat fish eggs at a stadium.

That's how a market economy works: products flow to where they're most wanted because that's where they'll be most profitable.

And, not surprisingly, companies have developed strategies to use the principle of supply and demand to maximize their profits.

For example, a company can sell the same item to different people at different prices based on how much they'll pay.

This technique, called _price discrimination_, is frequently used by airlines. People sitting in the same row on an airplane often pay vastly different prices for their tickets. One person may have purchased a cheap ticket months in advance while the other booked something pricey at the last minute. If airlines charged the same price for every seat, they wouldn't be able to earn enough profit to compensate for their cheaper fares.

So now that we understand the basic mechanisms of economics, let's see how they affect our society and governments more broadly. As you might have guessed, their impact is huge.

> _"Airlines are the most obvious examples of price discrimination, but look around and you'll start to see it everywhere."_

### 4. Taxation and other government policies aim to align our private consumption habits with society’s needs. 

In the 1700s, Daniel Defoe quipped that death and taxes were the only two certainties in life. Three centuries later, little has changed: taxes are an annoying mainstay of our world. But have you ever wondered _why_?

Part of the answer is that government regulation is necessary to control the consequences of a market economy.

One such consequence is an _externality_, or the gap between the private costs (the cost to you) and social costs (the costs to others) of your behavior.

For example, if you're interested in off-road driving, you might want a large, 4x4 car. Of course, a new Jeep Wrangler will have a variety of private costs: it'll be pricier than a sedan, consume more gas and have higher insurance rates.

But it will also have a social cost: 4x4s have greater CO2 emissions and speed up climate change, which is already having a devastating impact on people in the Philippines. However, if the social costs don't directly impact you, and if you can afford the private costs, you might buy the 4x4 anyway.

In this scenario, you don't have an incentive not to harm others.

But governments can impose taxes to deal with externalities to bridge the gap between the private and social costs of buying a 4x4.

Governments use taxation and other forms of regulation to reduce the incentive to purchase something, better aligning private consumption habits with society's needs. Taxing 4x4s at a higher rate decreases the amount of them on the road, thus cutting CO2 emissions.

That also allows people who _really want_ 4x4s to continue to buy them while making it possible for society to benefit from the taxes. This strikes a balance between the needs of individuals, government and society.

We've learned that some government intervention is necessary in a free market economy. However, as the following blinks will show, regulations can also have negative consequences.

> _"Taxing a behavior that generates a negative externality creates a lot of good incentives."_

### 5. The government relies on taxation to provide public goods; however, too much taxation creates monopolies. 

Contrary to popular opinion, taxes don't exist solely in order to annoy you. In addition to regulating the economy, the government relies on taxation to provide us with goods and services known as _public goods_. In many cases, these can only be supplied by the government.

These services aren't necessarily expensive or hard to make, it just usually doesn't make sense for a private person or company to supply them.

Take traffic lights: if you'd personally paid for the traffic lights in your neighborhood, all your neighbors would benefit even if they hadn't contributed any funding. These neighbors and others who benefit without contributing, i.e., _free riders_, make it so that there's no real incentive for you to invest in building traffic lights. So without the government stepping in, no one would ever install traffic lights. Instead, we'd all wait for someone else to do it so we could use them for free and, in the meantime, intersections would be full of crashed cars.

As we've seen, government intervention has lots of benefits. However, too much of it can lead to _monopolies_.

What's a monopoly? A monopoly is when a single market or trade is exclusively controlled by just one person or company. In many cases, monopolies stifle competition and curb incentives for consumer satisfaction.

This is the case in communist Cuba, where the state controls _all_ aspects of the economy. From the prices of goods to employee wages, everything is set by the government at fixed rates. Yes: those famous Cuban cigars have the same price at every shop nationwide. Furthermore, all salesmen are paid the same wage regardless of performance.

As a result, there's no incentive for salesmen to sell more cigars or for manufacturers to make better quality cigars.

So as we've seen: although taxation is necessary, extremes can lead to unfavorable outcomes.

The good news is that economics isn't just about government and taxes. It also deals with the distribution of wealth and what it takes to get rich. To figure out how to play the market (and even score big), check out the next blinks.

### 6. Financial markets help us generate money and protect our savings. 

What comes to mind when you hear the word "Wall Street"? Men in suits all over the stock exchange floor throwing papers at each other and shouting about money.

But what's really happening here? What does it mean to trade on the market?

The main goal of trading at financial markets, like the New York Stock Exchange, is _making money_.

People trade a variety of items — e.g., securities, commodities, futures, bonds — depending on the market. While some of these items represent the exchange of tangible goods in the real world (like gold), other trades deal only in _intangible financial assets_.

One such intangible good is _futures_, or agreements to sell things for a set price later. And it gets even more complicated: people also buy _options_ on futures, allowing them either to buy or sell the futures later on.

As you see, the world of investing is complex, but the overarching goal is simple: to make a profit on your trades by selling for more than you initially paid.

But investing isn't just about generating money. It's also a way for people protect their surplus capital, e.g., their savings.

Why is the financial market an effective way of protecting money? Let's answer that question by considering an unwise alternative to it: storing money under the mattress.

There are two reasons the latter is reckless:

First, it might get stolen.

Second, due to the increase of prices and decrease in the purchasing power of money (called _inflation_ ), your cash could lose value. One dollar today is worth far less than it was 50 years ago, when a single dollar could buy you a six-pack of beer.

But if you invest money in the financial market by buying shares of companies that seem likely to succeed or by betting on the future value of goods, your money can grow in value. That way you have more later when you need it.

Financial markets are complicated but they help generate money and protect it for the future.

And as we'll see in the next blink, making broad and diverse investments can increase your surplus money.

> _"Yes, it's actually possible to make (or lose) money by trading smog."_

### 7. To protect your investments and potentially earn a profit, diversify and be patient. 

You might be wondering whether there's a single golden rule for investing your wealth wisely. Unfortunately, it doesn't work that way. However, understanding basic economics can help you protect your investments and maybe even earn you a profit.

One general principle of investing is that a _diversified_ financial portfolio will lower your risk while still maintaining high returns on your investment.

What makes a financial portfolio _diversified_? When you've invested portions of your money in many different areas instead of making just one large investment into a single venture. In other words, if you wanted to have a diversified portfolio, you wouldn't put all your money into Microsoft. You'd invest _some of it_ in Microsoft, some in real estate, some in gold, and so on.

Why is this a good strategy? Because if you spread out your investments, you reduce the risk of losing all your money at once. That way, if Microsoft has a bad quarter and the real estate market crashes, you could still avoid major losses thanks to your gold investment.

Besides having a diversified portfolio, there's another key principle to profitable investing: patience. You want to invest for the long run.

A lot of people buy stocks in the hope of selling them for profit in the short term, i.e., in a few months, weeks, days — even hours! The final case is known as _day trading_ and, although it's exciting, it's not financially prudent.

Why not?

By thinking short term, you incur all the costs of trading stocks, such as commissions and taxes, without reaping the benefits of a long-term investment. Stocks fluctuate constantly, so it's hard to know if a stock's value will increase in the next few hours. However, most stocks do gradually increase in value over time, so it's a decent bet that your investment will be worth more in ten years' time than it is today.

We've seen the impact that economics has on our behavior and the way it shapes government policy and markets. But how does it work on a global scale? Does economics play a role in foreign policy?

### 8. When countries borrow money from each other, it has huge geopolitical consequences. 

Countries constantly squabble with each other. Pick up any newspaper and the front page will probably have a report on the latest international dispute. And, a lot of the time, economics is at the center of it.

Here's why: like companies, countries often need loans to finance their activities. A country might borrow money in order to import raw materials needed to produce certain items for export. In that case, the country hopes it can produce and sell enough products to help pay back the loan over time.

This is a calculated risk. It's similar to college students taking out student loans in the hopes that the investment will help them get a high-paying job to pay back the loans later.

But when countries borrow money, it can have huge geopolitical ramifications. This is the case with the economic relationship between the United States and China.

Some background: The US is the world's largest debtor, currently owing around $17 trillion to other countries. A massive portion of these loans are from China, to which it owed $3 trillion at the time of this book's publication.

A large portion of this money funds America's imports. And a lot of these (cars, food, etc.) _also_ come from China.

So essentially, the United States buys products from China using money that was borrowed from China.

Why would China do this? Well, the Chinese economy is heavily reliant on exports and depends on the US to import as many Chinese products as possible.

This complicated relationship highlights the kind of political maneuvering that goes on all over the world. Although the US may want to sanction China for environmental or political reasons, doing so could jeopardize their economic relationship. Don't bite the hand that feeds you, as they say.

### 9. To prosper and protect local businesses, countries must strike a balance between promoting and regulating trade. 

We've seen what happens when countries loan and borrow money from each other, so let's talk about another important international economic activity: trade.

Trade allows a country to prosper, both economically and otherwise. This is crucial because resources and skills aren't distributed evenly across the world.

For example, certain countries are better than others at producing various items. Take Saudi Arabia, which can produce oil much more cheaply than the United States. However, the reverse is true for corn, which is easily produced in the US. Each country produces what it can easily and then sells the surplus to its peers. It makes no more sense for Saudi Arabia to grow its own corn than it would for Tiger Woods to do his own car repairs.

What's more, trade with other countries frees up time and increases productivity. For example, mechanical engineers in the US might be better at manufacturing shoes than laborers in Bangladesh. However, by importing shoes from Bangladeshi workers, the engineers are free to work on more valuable products, like airplanes.

International trade clearly has its benefits, but governments often control it in order to protect local businesses. In some cases, it can be detrimental to import products that are more easily made elsewhere.

For example, oranges are cheaper to grow in Brazil than in the US. Therefore the US could import oranges and lower the price of orange juice nationwide. It sounds good (and delicious), but there's a serious drawback: it would destroy the local orange industry in Florida, putting lots of farmers out of work. The US government mitigates this problem by putting a large import tax on Brazilian oranges — over 60 percent — to ensure that the US product is cheaper.

As you can see, there's a lot at stake. That's why each government makes trade decisions based on its own best interests.

### 10. Final summary 

The key message in this book:

**The core principles of economics state that both people and companies make financial decisions based on their own best interests, thereby shaping markets and determining what products are available and for what price. Governments regulate the free market economy to align private consumption habits with society's needs. Economics also impacts trade between countries, often with geopolitical consequences.**

Actionable advice:

**Want to get rich playing the stock market? Be patient.**

If you want to make money by investing in the stock market, you'd better not be hasty. The only way to safely make money is to invest for the long term. Why? First of all, short-term investing incurs relatively high costs because you have to pay commissions and other fees on each transaction you make. Secondly, stocks fluctuate so much that it's almost impossible to know whether your short-term investment will pay off or not, so you're basically just flipping a coin. And, finally, the market generally increases in value over time, so while it might take a while, a long-term investment will probably generate profits.

**Suggested** **further** **reading:** ** _The_** **_Undercover_** **_Economist_** **by Tim Harford**

_The_ _Undercover_ _Economist_ explains how economics defines our lives. From the price of a cappuccino to the amount of smog in the air, everything is tied to economics. The book shows us how economists understand the world and how we can benefit from a better understanding of economic systems.
---

### Charles Wheelan

Charles Wheelan is a columnist for Yahoo! and a senior lecturer at the University of Chicago's Harris School of Public Policy. Before that, he was a correspondent for _The Economist_.

