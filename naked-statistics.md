---
id: 540f1b8e39346400081b0000
slug: naked-statistics-en
published_date: 2014-09-09T00:00:00.000+00:00
author: Charles Wheelan
title: Naked Statistics
subtitle: Stripping the Dread from the Data
main_color: None
text_color: None
---

# Naked Statistics

_Stripping the Dread from the Data_

**Charles Wheelan**

_Naked_ _Statistics_ offers an insightful introduction to statistics and explains the power of statistical analysis all while revealing its common pitfalls. It shows us how important a solid understanding of statistics is for good decision-making and gives the reader the tools to critically examine descriptive statistics.

---
### 1. What’s in it for me? Finally understand the mathematical world of statistics. 

Statistics has the reputation of being uninteresting and inaccessible. However, this comes in spite of the relevance statistics has on our daily lives. From batting averages to presidential polls, we use statistics and statistical thinking _every_ _day_ in order to better understand the world around us and make better decisions.

These blinks set out to change that reputation by giving you the tools you need to think statistically.

In fact, you'll learn that statistics can actually be _overly_ _accessible_. Indeed, anyone with data and a computer can run sophisticated calculations with a few keystrokes. However, while the calculations may be correct, a poor understanding of statistics can lead to faulty conclusions: when the data is unreliable or the statistical techniques are used improperly, the conclusions can be wildly misleading and even potentially dangerous.

In these blinks you'll also discover

  * how high school dropouts can help improve a school's test scores;

  * why one-in-a-million doesn't always mean what we think it means; and

  * how to _not_ predict the next president of the United States.

### 2. We encounter statistics every day: without them, we’d be up to our ears in spam emails. 

Statistics are extremely important tools for understanding the world around us. They can be used to explain everything from DNA testing to why playing the lottery is mere idiocy. It helps us identify the factors associated with diseases, helps us spot cheating on important exams, and it even helps us win on game shows.

But while you might not ever be on a game show, you will definitely interact with statistics every day in at least some small way.

For example, the spam filters on your email are based on statistical analysis. One part of this analysis is statistical inference, through which we draw conclusions from observable data.

Spam filters utilize a common tool in statistical inference: hypothesis testing.

Your spam filter starts with a _null_ _hypothesis_, i.e., a starting assumption, which will either be verified or reformulated based on the observations that can be made from the collected data. For instance, your spam filter's null hypothesis is probably: "this email is not spam."

It then scours the email for clues that can be used to reject this hypothesis, such as huge distribution lists or phrases like "penis enlargement." Once its hypothesis has been rejected, a spam email then lands in the "junk" folder.

Or do you ever watch Netflix? The film recommendations they offer you are also based on statistical analysis.

In fact, Netflix's predictions about our taste are shockingly accurate. The algorithm they use for film recommendations is based on correlations, that is, the degree to which phenomena are related to one another.

Whenever we rate films through their service, Netflix then compares our ratings with those of other customers in order to determine which users have submitted ratings that highly correlate with our own. With enough data, Netflix can then recommend us films that like-minded customers have rated highly, that we have not yet seen.

As we can see, we don't have to look far to see how statistics are relevant to our everyday lives!

### 3. Descriptive statistics provide meaningful information in an accessible way. 

If we want to demonstrate that Derek Jeter is a great baseball player, we need to do more than just say so. One way would be to use raw data and list every bat he's ever played. However, we could also summarize this data by referring to his batting average of .313. This is called _descriptive_ _statistics_ and it uses three basic quantities: the _mean_, the _median_, and the _standard_ _deviation_.

For example, if you wanted to answer the question, "Is the economic health of the United States middle class changing?" you could come up with an answer by calculating the change in per capita income in the United States: total income divided by the size of the population.

This is the _mean_, or average, and is the most basic measure of the "middle" of a distribution.

However, there has been explosive growth in incomes at the top end of the wealth distribution — for example, for CEO's and hedge fund managers — which means that the mean might not suffice to answer our question.

For this reason, we have another way to measure the "middle:" the _median_, the midpoint of a distribution. For example, if we look at a set of numbers, say 3, 4, 5, 6, and 100, then the midpoint is 5. Because the median is not sensitive to outliers, we can better assess the economic health of America's middle class by looking at changes in the median wage.

Sometimes, however, we still want to know how much deviation from the middle is "normal." We can do this by calculating the _standard_ _deviation_, which measures how dispersed data are from the mean.

Imagine, for example, that your doctor tells you that your HCb2 count (a fictitious blood chemical) is 134, whereas the mean is 122.

Your count is 12 points higher than the average. But does that mean you're ill? Not necessarily. If the standard deviation is greater than 12, then your count remains within the area of usual dispersion, in which people are still considered healthy.

### 4. Probabilities help us to manage life's many uncertainties. 

In events that involve uncertainty — like buying stocks or flipping a coin — we can use probabilities to weigh our actions by looking at what's likely and what's less likely to happen. Consider, for example, this report by the Australian Transport Safety Board, which quantified the fatality risks for different modes of transportation:

Despite a widespread fear of flying, the risks associated with commercial air travel are almost nonexistent. In fact, Australia hasn't had a commercial air fatality since the 1960s, so the fatality rate per 100 million kilometers traveled is essentially zero. For drivers, however, this rate is .5 fatalities for the same distance traveled. For motorcycles, it's thirty-five times higher than for cars!

Armed with probability data, we can calculate the _expected_ _value_, i.e., the sum of all the different outcomes, each weighted by its probability and payoff, for a given event.

These calculations are important any time you need to make a decision that might have serious consequences. For example, you might decide whether or not to buy a particular stock based on its price and the probability that it will gain value.

Or if you would prefer to play the lottery, you would come to the saddening conclusion that the expected value of the ticket is far smaller than its retail value.

Indeed, probability's predictive power is so strong that entire industries have been built from them.

For example, in the insurance industry, insurance companies need to collect more in premiums than they pay out in claims in order to stay profitable. To accomplish this, they calculate the "expected loss" for every policy.

From a statistical standpoint, buying insurance is a "bad bet," since you will almost undoubtedly pay more than you get back. The consolation, of course, is that it can protect you against outcomes that would otherwise wreck your life.

### 5. Statistical tools enable us to conduct polls, but still, conducting polls is challenging. 

How can a sample of, say, 1000 people offer us insights into the attitudes of an entire country? Can we draw any meaningful conclusions about the views of hundreds of millions of people from such a small sample? Well, that's what polls aim to do.

Polls rely on one of the most important tools in statistics to draw their conclusions: the _central_ _limit_ _theorem_, which states that an adequately large and properly drawn sample will resemble the population from which it's derived. Moreover, the probability that any sample will deviate significantly from the greater population is very low.

However, while polls _can_ be accurate and representative of larger populations, creating a proper poll is quite challenging: it can be difficult to both reach an unbiased sample as well as elicit information from them that actually represents their beliefs.

For example, if you want to conduct a poll by making random calls to different numbers until a sufficiently large sample of adults picks up and answers your questions, you would find that your sample would be biased towards those who are likely to both be home and to answer the phone when you called, e.g., the unemployed or the elderly.

But let's say you were able to stratify your sample across demographics without bias. Survey results can still be extremely sensitive to the way you pose your questions. For example, voters are less concerned about "climate change" than they are about "global warming," even though global warming is an aspect of climate change.

Furthermore, people aren't always totally honest, especially when the questions you're asking are embarrassing or sensitive. For example, respondents may overstate their income or inflate the number of times they have sex in a typical month in order prevent themselves from feeling embarrassed.

However, if you can get the sample right, and if you can get them to answer questions honestly and without bias, then polls make great statistical instruments.

> _"We cannot treat humans like laboratory rats. As a result, statistics is a lot like good detective work."_

### 6. Regression analysis allows us to unravel statistical relationships in data – but we have to be careful. 

Suppose we know for a fact that many British civil servants smoke cigarettes. If we look at all British civil servants who suffer from poor cardiovascular health, how do we tell which part is due to their sedentary office jobs, and which part is due to the smoking?

While these two factors seem inextricably intertwined, we can untangle them with a statistical process called _regression_ _analysis_, a statistical tool which can isolate the statistical relationships that we're interested in.

For example, consider the relationship between height and weight: people who are taller tend to be heavier. However, there are other factors, such as age, sex, diet or exercise, that might also explain their weight.

Regression analysis allows us to specify a relationship between a pair of variables while holding other variables constant. For instance, we can isolate the correlation between height and weight among people of the same age and sex to discern the independent effect of each of the potentially explanatory factors.

In spite of its explanatory power, regression analysis still has its drawbacks.

For instance, regression analysis only specifies linear relationships between two variables. In other words, plotting the variables on a graph yields a straight line. However, if the relationship between the variables is nonlinear (e.g., it's exponential, logarithmic, etc.), then regression analysis will produce erroneous conclusions that can in turn lead to false predictions.

Moreover, a sloppy regression equation can produce a relationship between two variables where none actually exists. This misrepresentation happens if we mistake correlation for causation.

What would happen, for example, if we were to include annual per capita income in China as an explanatory variable for the rising rate of autism in the United States? We would certainly find an association between rising incomes in China and rising autism rates in the U.S. simply because they both have been rising over the same period, even though they are not causally related.

> _"Statistics is like a high-caliber weapon: helpful when used correctly and potentially disastrous in the wrong hands."_

### 7. Descriptive statistics can be accurate and yet misleading. 

Although descriptive analysis is a great tool, we have to interpret the data that it provides, often leading to a variety of conclusions.

Indeed, even the most accurate descriptive statistics can suffer if there is a lack of clarity regarding what exactly we are trying to explain. For example, if you want to find out how healthy the American manufacturing sector is doing, statistics will present you with two contradictory views:

  * In the 2000s, the U.S. manufacturing sector steadily grew.

  * Employment in the U.S. manufacturing sector has steadily declined.

These two statistics don't answer our question in isolation. In order to get the complete story, we'll need to look at both. But even then, our question is difficult to answer, because the "health" of U.S. manufacturing depends on how we choose to define it: does "health" refer to output or employment?

In addition, descriptive calculations like the mean and the median can be used for deceptive ends.

Looking at the set "3, 4, 5, 6, 102," for example, we have a mean of 24 and a median of 5. If we want this group of numbers to appear "big," then we focus on the mean. If we want to make it look smaller, we will instead cite the median.

Suppose, for example, that you suffer from a potentially fatal illness. A new drug has just come to market that increases the median life expectancy by two weeks — hardly encouraging news. But what if the raw data also shows that some large number of patients, say 40 percent, are cured entirely?

This astounding success wouldn't show up in the median, since the median simply divides the distribution into two halfs and looks at the value in the middle.

Often, descriptive statistics don't offer a single "right" answer. Instead, we're left to interpret the data, and different descriptive statistics can tell different stories.

> _"It's easy to lie with statistics, but it's hard to tell the truth without them."_ — Andrejs Dunkels, Swedish mathematician

### 8. Probabilities don’t make mistakes. People using probabilities make mistakes. 

As we've already seen, we're not always great at keeping our biases away from our interpretation of descriptive data. Here are our two most common pitfalls.

We often assume that events are independent when they actually aren't, skewing probabilities.

To illustrate this, we'll take a look at the exceedingly rare sudden infant death syndrome, or SIDS, a phenomenon in which a healthy infant suddenly dies.

The rare nature of SIDS caused Sir Roy Meadow, a prominent British pediatrician, to conclude that the chance of having two incidents of SIDS within the same family is so extraordinarily small that in those two cases, it couldn't be SIDS — it must be murder!

In reaching this conclusion, Meadow assumed that two incidents of SIDS were independent. However, it is quite possible that two incidents are actually linked genetically, which would increase the probability of two incidents dramatically.

Unfortunately, because of Meadow's assertions and because postmortem examinations cannot necessarily distinguish natural deaths from those in which foul play is involved, many parents were sent to prison based on his testimony.

Furthermore, we often neglect the context surrounding statistical evidence. A famous example of this is the _prosecutor's_ _fallacy_. To illustrate how this works, imagine that you hear the following testimony in court:

  * A DNA sample found at the scene of a crime matches a sample taken from the defendant.

  * The probability that the sample recovered at the scene of the crime would match by chance is only a one-in-a-million.

Fortunately for the defendant, our one-in-a-million chance that the sample was recovered coincidentally does not mean that there is a one-in-a-million chance that the defendant is innocent.

Consider that the defendant's DNA was part of a database that includes millions of people's DNA. Because millions of people were tested to create the database, there were thus millions of one-in-a-million chances of finding a coincidental match, making the chances of getting _at_ _least_ one coincidental match from the database relatively high.

> _"Statistics cannot be smarter than people who use them."_

### 9. No amount of statistical analysis can make up for flawed data. 

Even the finest recipe isn't going to save a meal that uses spoiled ingredients. The same goes for statistics: good data are fundamental to any reliable statistical analysis.

In fact, biased data samples make statistical analysis completely useless.

Consider this example from 1936, when Alf Landon, a Republican, ran for president against Franklin Roosevelt. One influential news magazine at the time, _Literary_ _Digest_, wanted to predict the results of the election by polling the public. So, they mailed a poll to their subscribers, as well as to automobile and telephone owners whose addresses could be culled from public records.

Their poll predicted that Landon would beat Roosevelt with 57 percent of the popular vote. However, as the history books tell, Roosevelt won with 60 percent. So what went wrong?

Their failure to predict the results was due, at least in part, to a biased sample: the magazine's subscribers were wealthier than average Americans and therefore more likely to vote Republican, as were homeowners and those with telephones and cars at the time.

Moreover, data can be flawed when important factors go unobserved, thus skewing the result of statistical analysis.

For example, suppose that a high school principal proudly reports that students' test scores at his school have risen steadily. Each year, students have done better than in the year previous and by every possible measure of statistical analysis: mean, median, percentage of students at grade level, and so on.

Suppose, too, that the students in fact learn nothing, and that each year half of them drop out. If each year students with the lowest test scores drop out, then the average test scores of those still in school will steadily increase — all without anyone actually doing better on the tests!

As we can see, the integrity of statistical analysis very much depends on the data's reliability.

### 10. Final summary 

The key message in this book:

**Statistics** **is** **an** **amazingly** **powerful** **tool** **that** **we** **encounter** **every** **day** **and** **that** **provides** **meaningful** **information** **in** **an** **easily** **accessible** **way.** **However,** **the** **power** **of** **statistical** **analysis** **is** **tempered** **by** **its** **delicacy:** **if** **used** **incorrectly,** **it** **can** **produce** **misleading** **results** **and** **be** **used** **for** **nefarious** **ends.**

Actionable advice:

**It's** **not** **worth** **insuring** **everything.**

Insurance is a "bad bet" from a statistical standpoint: on average, you will pay far more in premiums that you'll get back in claim. Therefore, it only makes sense to insure yourself against things that you absolutely could not afford to manage on your own. So, while it might be a good idea to buy health insurance, you should probably never buy additional warranty on your household gadgets.

**Suggested** **further** **reading:** **_How_** **_to_** **_Lie_** **_with_** **_Statistics_** **by** **Darrell** **Huff**

A classic since it was originally published in 1954, _How_ _to_ _Lie_ _with_ _Statistics_ introduces readers to the major misconceptions of statistics as well as to the ways in which people use statistics to dupe you into buying their products. Above all, this book is a call to the public to be skeptical of the information dumped on us by the media and advertising.
---

### Charles Wheelan

Charles Wheelan is an American economist, journalist and author. In addition to writing for several news magazines, including _The_ _New_ _York_ _Times_ , _The_ _Wall_ _Street_ _Journal_ and _The_ _Economist_ , Wheelan is also author of the bestselling book _Naked_ _Economics:_ _Undressing_ _the_ _Dismal_ _Science._

