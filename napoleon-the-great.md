---
id: 5829d3ae92e9210004940bc6
slug: napoleon-the-great-en
published_date: 2016-11-17T00:00:00.000+00:00
author: Andrew Roberts
title: Napoleon the Great
subtitle: None
main_color: AA4122
text_color: AA4122
---

# Napoleon the Great

_None_

**Andrew Roberts**

_Napoleon the Great_ (2014) is an in-depth look into the life and times of the infamous French conqueror, Napoleon Bonaparte. These blinks detail how Napoleon, once a penniless young man, became a general at the age of 24 before going on to revolutionize the French military and government, and leaving an indelible mark on European and world history.

---
### 1. What’s in it for me? Get to know the real story of Napoleon Bonaparte – both in battle and at home. 

The notion that history is made and its outcomes influenced only by "great men" is rightly considered old fashioned these days. However, there are still a few historical figures who seem to have driven and dictated events almost single-handedly during their lifetimes.

Napoleon Bonaparte was certainly one of these. From his humble beginnings on the island of Corsica, Napoleon rose through the French army's ranks to become Emperor of France and conqueror of much of Europe. Not only that, he also revolutionized French government, law and military tactics.

These blinks will take you on a journey through Napoleon's life, with all of its momentous ups and downs. It's a truly fascinating story and one that continues to define European history and identity to this day.

In these blinks, you'll discover

  * what the Code Napoleon was;

  * how his wife, Josephine, crushed his heart; and

  * how he managed to combine his military campaign in Egypt with his wish for enlightenment.

### 2. Despite being born into a lower-class family, Napoleon rose to become one of the French army’s youngest officers. 

If there's one thing we all know about Napoleon, it's that he was as French as they come, right? Well, the truth is he wasn't even born in mainland France; rather, the life of this great conqueror began on August 15, 1769, on the island of Corsica, a Mediterranean island freed from Italian control in 1755 — and which only became part of France in 1768.

In other words, Napoleone di Buonaparte, as he was named at birth, came from Italian roots. And although his family had a respectable position in Corsican society, his father had to apply for nobility to ensure his son's prosperity.

Napoleon's parents were Carlo and Letizia Buonaparte and, although Carlo was a proud Corsican, he jumped at an opportunity to obtain a secure government job. Around 1769, Carlo pledged his loyalty to Louis XV of France.

The Buonaparte family was large and Letizia bore 13 children, eight of whom survived childhood. So, in 1771, to provide for his family, Carlo applied for the Buonapartes to be recognized as Corsican nobility, in order to open up all the benefits that would afford his children. This application was successful and among those benefits was that Napoleon was able to attend the Royal Military School of Brienne-le-Château.

Once there, he studied hard and was soon excelling in his classes. He was often teased by his classmates for being one of the first Corsicans to attend the school, and considered as fake nobility, but that only made Napoleon work harder to prove himself. He studied eight hours a day, learning all he could about math, Latin, history, weaponry and the arts. It was during this time that he also learned to speak French, though he would speak it with a Corsican accent his entire life.

In the end, his hard work paid off; at the age of just 16, he became one of the youngest French army officers, and the only Corsican at that time to hold a prestigious artillery commission.

> Of the eight of Bonaparte children who survived, three became kings, two princesses, and one an emperor.

### 3. Amid the chaos of the French Revolution, Napoleon proved himself on the battlefield and quickly advanced in military rank. 

During his youth, Napoleon was always a proud Corsican. But as he began his military career, French politics were being turned on their head and he soon found himself pledging unconditional allegiance to France.

When the French Revolution began in 1789, Napoleon supported the ideals of the revolutionaries; yet, his position in the military meant that he was a part of the effort to stop the uprising against the monarchy. He supported the revolutionaries because he disliked the monarchy and the power it gave to the clergy, but he also believed in the ideas of the Enlightenment, including the writings of Rousseau and Voltaire, both of whom were proponents of liberty and opposed to monarchy.

So, when King Louis XVI was dethroned, Napoleon supported the overthrow and joined the anti-royalist Jacobins. It was during this chaotic transition from monarchy to republic that Napoleon rose through the military ranks, eventually becoming a lieutenant in 1791. Little did he know at the time, but a much more significant promotion was just over the horizon.

Napoleon's loyalty to the Jacobins helped him secure the position of artillery commander, fighting the pro-royalist forces of Britain, Prussia and Austria, all of whom were trying to take the strategic port city of Toulon.

Reports from the battle for this naval city describe Napoleon as an astoundingly intelligent officer and his letters to superiors show signs of a meticulous and natural-born leader. For instance, during the battle for Toulon, Napoleon helped lead an assault to capture the key position of Fort Mulgrave, having his horse shot out from beneath him in the process. From this position, he aimed his cannons on enemy ships, ensuring a French victory.

It was because of the vital role he played in this battle that he earned his next promotion — this time to the rank of general. He was just 24 years old.

> Due to his Jacobin ties, Napoleon was jailed for ten days in 1794 during the Reign of Terror that followed the French Revolution.

### 4. An impressive Italian campaign made Napoleon a French hero. 

Having been promoted to general, Napoleon became second-in-command of the Army of the Interior. During this charge, he was tasked with protecting Paris during a violent uprising on October 5th, 1795. His ability to boldly and mercilessly suppress the uprising prevented a civil war and further elevated his status.

It also won him command over a division called the Army of Italy, setting the stage for a military campaign that would soon earn his name legendary status.

Napoleon took up his new post on March 26, 1796 — but he had already been preparing for the opportunity for three years. He'd studied the landscape and general situation in northern Italy, forming plans for how to advance through the northern state of Piedmont, push back the Austrians in the area and capture their fortresses.

If that wasn't daring enough, he planned to carry out this bold campaign while outnumbered; he would have just 50,000 French forces under his command, while his Austrian and Piedmontese enemies would be 80,000 strong.

Despite these stacked odds, as soon as he got his promotion, Napoleon put his scheme into action, and to brilliant effect: the French, after transporting their supplies over the mountains of Liguria, would win a series of decisive victories by exploiting their divided enemies' communication issues.

First, the town of Milan fell following a tremendous display of courage. In this historic battle, on May 10, 1796, a mere 3,500 French soldiers defeated 9,500 Austrians. The battle took place on a bridge in the town of Lodi, and it would go down as Napoleon's first significant victory.

But this was just the beginning of his campaign. One month later, in June of 1796, the next battle, known as the Siege of Mantua, began. The siege would last until February of 1797 and Napoleon's eventual victory would make headlines in Paris.

From there, he crossed the Alps in March of 1797, ready to threaten Vienna and force a peace agreement with Austria. In so doing, he became a hero to the French people.

### 5. An Egyptian campaign cost Napoleon many men. 

Napoleon was winning battles left and right, earning the admiration of the French people. But the country's new leadership wasn't so keen on the young general — in fact, they saw his rise in popularity as a potential threat.

So, they sent Napoleon off to Egypt in a campaign to disrupt British power in the Mediterranean. He set sail on May 19, 1798 with about 38,000 Frenchmen and, while the campaign got off to a good start, it wouldn't last.

When the French forces landed in Egypt, they easily captured Alexandria. But as they marched toward Cairo on July 7, 1798, Napoleon's soldiers began to suffer the harsh conditions of the desert. This was the first time a Western army had attempted a desert crossing and 200 of his men were literally blinded by the sun.

Other soldiers would contract malaria, while still others would kill themselves rather than march on. To make matters worse, those soldiers who fell behind the rest were easily killed by small bands of Mamluks, Egyptian soldiers on horseback, who wore medieval armor and were led by Ibrahim Bey and Murad Bey, the rulers of Egypt.

In hindsight, the campaign probably should've stopped right then — but instead, Napoleon, spurred on by his earlier victory in Alexandria, advanced to Jaffa in modern-day Israel, where he'd pay the price for his overconfidence.

Once in Jaffa, Napoleon sent a message to the governor, urging surrender. The messenger was promptly beheaded and, in response, Napoleon's soldiers violently pillaged the city, taking thousands of lives. This horror show was compounded by the fact that many French soldiers caught the plague in Jaffa and died horrible deaths within a mere 24 hours.

From there, the remaining French soldiers moved on to Acre where they battled the Turks, Mamluks, Afghans and British at a fort defended by the latter. This conflict took place on the coast of modern-day Israel and would last for months.

Finally, in May of 1799, Napoleon, unable to break through the fort's defenses, and knowing that no more reinforcements were coming, ended the campaign and returned to France.

> _"Napoleon respected Islam, regarding the Koran as 'not just religious; it is civil and political. The Bible only preaches morals_. _'"_

### 6. While his marriage was off to a rocky start, Napoleon was spreading Enlightenment thought through his military campaigns. 

Most people know Napoleon as an accomplished general. But he was also a prolific letter-writer, even during his military campaigns. Some of this correspondence was to ensure proper equipment and supplies for his troops, but during his Italian and Egyptian campaigns, he also wrote many letters to his wife, Josephine.

That's right, Napoleon was married and it was quite a rocky relationship as well, especially in its early years.

Napoleon and Josephine married on March 9, 1796, just days before he left for Italy, but this union wasn't as romantic as it might sound. In fact, many historians believe that although Josephine may not have initially loved Napoleon, she saw the marriage as a smart move given her situation; she was a widow, a mother and six years Napoleon's senior.

Napoleon, on the other hand was deeply in love with Josephine from the start. He also adored her son, Eugene, and would take him along on his Egyptian campaign as an aide-de-camp.

There was, however, a small problem; Josephine loved another man. His name was Lieutenant Hippolyte Charles and he carried on a secret affair with Napoleon's wife for years.

That is, until July 19, 1798. It was on that day when, while in the Egyptian desert, Napoleon learned of Josephine's infidelity. He was devastated, but there wasn't much he could do while fighting a campaign, especially since his focus was on trying to combine his military campaign with Enlightenment thought.

To proliferate Enlightenment ideas, Napoleon had brought along, in addition to his stepson and 38,000 soldiers, a group of 167 different scientists, artists, botanists, zoologists and geographers.

In Cairo, armed with this intellectual army, Napoleon founded the Institut d'Egypte for the research of science and art. His goal, as a supporter of Enlightenment ideas, was to create a place that would promulgate scientific advancements world-wide.

And the institute in Cairo certainly did that. For instance, in 1799, his researchers discovered the legendary rock stele, the Rosetta Stone.

> The scientists' groundbreaking insight into the ancient Egyptian world was published in the enormously influential _Description de L'Egypte_.

### 7. Napoleon participated in a risky coup against the French government, securing even greater power. 

Following his campaigns in the Middle East, Napoleon returned to Paris on October 16, 1799 where he received a hero's welcome, cheered by a public who saw him as their savior. Indeed, one couldn't have blamed the French people for wanting a savior. After all, while Napoleon was away, the political situation in France hadn't improved.

The government was riddled with corruption, and economic inflation and pro-royalist uprisings were roiling the country. Not only that, but the military had suffered multiple other defeats while Napoleon's forces were being beat back in the Middle East.

Amid this political turmoil, Napoleon and a group of other men set out on a risky mission to topple France's government.

Napoleon joined a group of conspirators to seize political power. Among the group's members were the chief of police Joseph Fouché; former foreign minister Charles Maurice de Talleyrand; and Napoleon's brother Lucien Bonaparte, who held a high-ranking position within the Council of the Five Hundred, the country's legislative body.

On November 10, 1799, the group launched its daring plan. Napoleon entered the council chambers and tried to convince them to sign on to the coup — but when the council realized what was happening, they charged at Napoleon in fury, calling him an outlaw and a dictator.

After being expelled from the council chambers, Napoleon and his brother Lucien approached the council guards, announcing that fanatics, influenced by the British, had infiltrated the council. Lucien drew his sword, pointed it at his brother's chest and told the guards that if he were acting against the interests of French liberty, he'd stab his brother in the heart.

The guards, moved by this display and story, forcibly removed the council members from their chambers, effectively eliminating any threat from the council. Immediately following this, the co-conspirators assembled and drew up a new constitution, replacing the existing Directorate with a Consulate, and Napoleon was given the most powerful position as First Consul.

### 8. Napoleon fought off an Austrian attack, winning large parts of Italy for France. 

From his new position at the head of the government, Napoleon issued a series of popular reforms that centralized government, gave greater rights and protections to the people and reinvigorated French business, all while cutting taxes. The economy soared and things were going very well; that is, until April 19, 1800, when Austrians laid siege to a French fort in the northern Italian city of Genoa.

So, Napoleon would head back into battle.

Napoleon set out across the Alps once again, this time in the company of 51,000 men, thousands of horses and mules and a large stock of supplies and cannons in an epic crossing that took 11 days. Meanwhile, the French forces in Genoa were under siege and suffering badly.

The Austrians had cut off their supply chain and the French soldiers were being forced to eat cats and dogs. But instead of advancing directly on Genoa as the Austrians expected he would, Napoleon attempted to draw the Austrian forces to the West, thereby trapping them.

He divided his army in an attempt to block off all enemy lines of retreat, but in so doing, deviated from his reliably successful strategy of maintaining a concentrated force. As the battle of Marengo began, he immediately recognized his mistake: 30,000 Austrians had secretly assembled and the French troops standing against them were outnumbered by 2:1.

Early on June 14, 1800, the Austrians opened fire and Napoleon quickly called for reinforcements. He had no choice but to carefully retreat until four in the afternoon, when fresh troops arrived. With the addition of 11,000 soldiers, the battle shifted sides as French forces pushed a surprised Austrian army out of Marengo and back to the city of Alessandria where they agreed to an armistice.

This peace treaty gave France all of Piedmont and Genoa, and most of the Lombardy region; northern Italy would remain securely within Napoleon's empire for 14 years.

> Napoleon's army crossed the Alps in half the time it took the Carthaginian commander Hannibal to cross them in 218 BC.

### 9. Although some would fail, Napoleon secured historic peace deals. 

In the early 1800s, following the battle of Marengo, France and Austria entered lengthy peace talks. At the same time, Napoleon continued to work on his _Code Napoleon_, a political project that included eliminating the privileges of royalty, separating the church from the state and standardizing education.

The peace talks lead to eventual alliances, albeit shaky ones, between European nations. On February 9, 1801, the Treaty of Luneville was officially signed, ending nine years of continuous war between Austria and France. But peace isn't all the treaty secured for France — it also gave them additional territories in Italy, Belgium and the Rhineland, and made Britain the country's last major enemy; that is, until Britain found new allies.

Shortly after the Luneville treaty was signed, Russia, Denmark, Sweden and Prussia all signed treaties with Britain. As a result of this powerful opposition, France agreed to peace with Britain by signing the Treaty of Amiens on March 25, 1802.

The problem with this short-lived treaty was that it was silent on a couple of key issues, namely future commerce between the two nations, and the matter of strategic territories. The failure to deal with these important factors meant that Britain would declare war on France just a year later.

Yet, however quickly it collapsed, the Treaty of Amiens was considered a political triumph for Napoleon, as he had temporarily made peace across all of Europe, an unprecedented move that made the French ruler wildly popular. He was declared France's First Consul for life and, on December 2, 1804, a coronation led by Pope Pius VII officially anointed Napoleon and Josephine Emperor and Empress.

That same month, Britain formed what is known as the Third Coalition. In so doing, the country united Sweden, Russia and Austria against France. Then, by August of 1805, Austria again mobilized its troops, crossing the Bavarian border and capturing the city of Ulm. Responding to this act of aggression, Napoleon sent 170,000 men on a march from the northern coast of France down across the Rhine, in the largest campaign of its time.

### 10. With impressive victories at Austerlitz and Jena, Napoleon aimed to force a British surrender. 

On September 25, 1805, Napoleon's army crossed the Rhine and, on October 6, shifted its front 90 degrees to the south, planning to cut off the Austrian forces. This maneuver constituted the most massive change of front ever known and it would lead to a series of victories as Napoleon surrounded and secured Ulm, forcing another Austrian surrender.

But his work didn't end there; two of Napoleon's most impressive victories were still to come in Austerlitz and Jena.

After marching on Vienna, Napoleon's forces faced the army of Tsar Alexander at Austerlitz. In this battle, the French leader benefited from a mistake by his Russian counterpart, the Tsar himself, who spread his formidable army too thinly over an uneven battlefield.

So, on December 2, 1805, as a misty morning gave way to sunny skies, Napoleon launched a decisive attack. By one in the afternoon, he'd divided the Russian army in two and forced their retreat back to Hungary. From there, Napoleon turned his attention to King Frederick William of Prussia who'd made a formal declaration of war against France, seeking autonomy for his country from both France and Austria.

To settle this matter, William's and Napoleon's forces met on the battlefield of Jena on the foggy morning of October 14, 1806. Following hours of brutal warfare, Napoleon formed a major line of assault that pushed the Prussians back six miles.

Truthfully, however, Napoleon didn't want to be at war with Prussia, Russia or Austria; what he really wanted was to force a British surrender. After all, the British were the ones financing the Austrian, Prussian and Russian military campaigns against France. Napoleon figured that if he could cut Britain off from the rest of Europe, he'd lock the nation into an economic stranglehold.

### 11. It took a series of brutal battles for Napoleon and Russia to finally reach a peace agreement. 

Although Napoleon pushed back the Prussian forces at Jena, Frederick William wasn't ready to surrender just yet; instead, he retreated and hoped to receive Russian support. As a result, there would be more brutal fighting before Napoleon could finally reach a peace accord with Prussia and Russia.

In December of 1806, Napoleon was moving through Warsaw and the Russian army was retreating. But as the French forces moved east, the impending winter meant increasingly harsh conditions, with French soldiers trudging through snow and mud up to their knees, while suffering from hunger and exhaustion. The conditions were so bad that hundreds of French soldiers took their own lives.

On February 7, 1807, the French soldiers finally caught up to the Prussians and Russians. What followed was the two-day battle of Eylau, a horrific fight between forces totalling 115,000 soldiers both sides taking massive casualties; the French lost as many as 5,000 men within the battle's first 15 seconds.

By the end of the first day, Napoleon was moved to tears by the immense loss of life, and on the second, he led one of the greatest cavalry charges of his career, securing victory. His move was so bold and impressive that the Russian cavalry were forced all the way back to the infantry line.

However, while Napoleon may have daringly won the battle of Eylau, it would take yet another to end the war. The next fight took place at Friedland, where the Russians lost more than 40 percent of their troops before suing for peace.

That second battle resulted in the signing of the Tilsit Peace Treaty and the adoption of the Continental System, as well as a surprisingly friendly interaction between Napoleon and Tsar Alexander, who spoke late into the night about politics and philosophy.

Among the agreements were that Russia and Prussia would join the Continental System, Napoleon's European trade agreement, which was designed to economically blockade Britain and force them into peace negotiations.

### 12. Napoleon hoped that the Continental System would unite nations – but all it did was produce another deadly campaign. 

Napoleon intended to exert further pressure on the British by capturing Portugal, but instead, the fighting that ensued in Spain spiralled into a deadly disaster known as the Peninsular War. This war might have turned out better if Napoleon had been more focused on it; but, unfortunately, he had other problems to handle in Europe, many of which related to his insistence on making the Continental System work.

The problem was that for this to happen, countries like Russia and Austria would need to resist the temptation to profit from illegal British trade — which they certainly did not. During this time Napoleon divorced Josephine, and married Marie Louise, the daughter of Austria's emperor. But not even a royal wedding could overcome Austria's issues with France and the Continental System.

Austrian resistance wasn't the only problem: Russia also detested the Continental System. This disdain resulted in Alexander siding with Britain against France in 1810 and produced the incredibly violent Russian campaign.

At the campaign's start, in June of 1812, Alexander's army fell back deep into Russian territory, while Napoleon's army of 600,000 men marched right after them. In the months to come, 140,000 French soldiers would lose their lives in Russia to typhus or dysentery and, by the time of the Battle of Borodino on September 7, 1812, Napoleon's forces numbered just 103,000.

During the battle, the French would suffer another 28,000 casualties while the Russians would lose 43,000 men. Despite these massive losses, Napoleon eventually reached Moscow, but only after the Russians abandoned their city, which was in flames, shortly after the arrival of French troops. With that, Napoleon was left with little choice but to attempt the journey home before winter arrived.

This French retreat out of Russia was hell for Napoleon and his forces. The Russians closely tracked the French soldiers, capturing those weakened by the freezing weather. These captured soldiers were tortured, skinned alive or left naked to freeze to death, while the starving soldiers ate dead horses, some of them even resorting to cannibalism.

> At Borodino, 200 limbs were amputated, and it would remain the bloodiest battle in history for over a century.

### 13. Unable to protect Paris from the Coalition Forces, Napoleon was exiled to the Mediterranean. 

It's a truly shocking figure: Napoleon lost somewhere in the neighborhood of 524,000 men in Russia. Only 40 percent of those casualties were suffered in combat, while the rest were the result of disease, starvation and suicide.

Napoleon would never truly recover from what happened during that campaign in 1812, nor would he be able to keep the Coalition Forces, an alliance of all of the armies fighting against Napoleon, out of Paris. Upon returning to Paris on December 18, 1812, he tried to rally as many new troops as possible, but some of these soldiers were only 15 or 16 years old.

In the end, he managed to pull together another 151,000 soldiers whom he hoped would be able to prevent Russia, Austria and Prussia from tearing apart the past seven years of French achievements. But despite retaking sections of Saxony and narrowly prevailing in the Battle of Dresden on August 27, 1813, Napoleon found himself surrounded, outnumbered and suffering a catastrophic defeat at the Battle of Leipzig of October of 1813.

Riding the momentum of that victory, the Coalition Forces were ready to march on Paris. But in an absolutely amazing display, Napoleon, with just 30,000 men, left the capital and raced his army through the French countryside, defeating one allied army after another in a series of 13 battles.

His success was so rapid that, in the month of February, he won four battles in just five days. However, it simply couldn't last, and on March 30, 1813, 60,000 Allied soldiers marched into Paris, threatening to raze the city. Napoleon rushed back to the capital only to be met with betrayal; one of his marshals had surrendered.

In the end, Napoleon was forced into exile on the Mediterranean island of Elba, and the royalists put Louis XVIII in charge of France. Napoleon would spend about nine months on Elba, keeping himself busy by making improvements around the island, but he was deeply saddened by being separated from his wife Marie Louise and their young child, both of whom had returned to Austria.

> _"The spell is broken." -_ Tsar Alexander, after Napoleon's disastrous campaign.

### 14. Escaping from Elba, Napoleon made an audacious move to reclaim his throne and save his empire. 

While confined to Elba, Napoleon was kept under the watchful eye of British Colonel Sir Neil Campbell. The two men actually got along quite well and Campbell admired Napoleon's intelligence as well as his charm.

Nonetheless, on February 16, 1814, when Campbell left the island on a short trip, Napoleon jumped at the opportunity to reclaim his throne. He'd been following French affairs closely and knew that things were going back to the pre-revolutionary ways of the monarchy.

Not only that, but the allied forces had failed to keep their part of the deal with Napoleon; he wasn't receiving the expenses they had agreed upon, and he'd even heard rumors that they would force him into an Australian penal colony or to the remote British island of St. Helena.

Rather than leave it to chance, Napoleon set sail for France on February 26, 1815 with three generals, 607 men and a bold plan that put him back in control of France. Here's how the events unravelled:

On March 1, 1815, Napoleon landed on the southern coast of France and commenced a northward march, now known as the Route Napoleon, which would take him over mountains and through villages, covering 190 miles in just six days.

In the course of this journey, he encountered pro-Bonapartist soldiers who gladly joined his advance before arriving in Paris on March 20, 1815. Upon his arrival, Napoleon was met with zero resistance from the monarchy. Rather, a concerned and overweight Louis XVIII literally had his loyal subjects carry him to his carriage, in which he fled to Ghent.

The next day, Napoleon went back to work, drafting a new constitution that would make it even more difficult for royalist families to seize power again. He abolished all forms of slavery, ended censorship, divided power between the emperor and legislature and renounced all ambitions of empire.

### 15. After a crushing defeat at Waterloo, Napoleon was sent to the island of Saint Helena where he would live out the rest of his days. 

Napoleon's return to power may have been swift, but it came to an end just as quickly. On May 15, 1815, following his return, the Coalition Forces declared war on France once again. And, by straying from some of his most effective strategies, Napoleon would come to suffer a decisive defeat at the battle of Waterloo.

He managed to put together an army of 280,000 men and, in contrast to his successful strategy of centralized troops, he divided his forces, sending one group after a retreating Prussian army. Then, at 11 in the morning on June 18, 1815, he began his attack on the allied forces, led by the Duke of Wellington.

Napoleon's attack, however, was also delayed, giving Wellington time to position his troops and receive Prussian reinforcements. As a result, Napoleon didn't send orders for his other troops to return until noon.

Unlike the majority of Napoleon's battles, Waterloo was absolutely disorganized: infantry attacks were spread thinly rather than set in columns, cavalry charges were left unsupported by artillery due to miscommunication and, by nightfall, French soldiers were fleeing in all directions. The battle ended with 25,000 to 31,000 French soldiers killed or wounded and the loss of 26 generals, one of whom was literally cut in half by a cannonball while standing next to Napoleon.

To compound matters, the British Navy blocked any opportunity for Napoleon to escape and, on July 15, took him into custody, escorting him to the British island of Saint Helena. There, in the isolated waters of the south Atlantic ocean, Napoleon would live out his years, working on memoirs and eventually succumbing to stomach cancer.

On May 5, 1821, after years of horrific pain, Napoleon died in his bed aged 51 years. But it wasn't until December 2, 1840, on the anniversary of the battle of Austerlitz, that Napoleon was given a proper funeral in Paris. When he was finally laid to rest, a million people came to celebrate the life of this heroic leader.

### 16. Final summary 

The key message in this book:

**Napoleon Bonaparte, the founder of modern France, was one of history's greatest leaders. He transformed his country, brought a long-sought-after — albeit fleeting — peace to Europe, and many of the reforms he instituted can continue to play an important role in French and European society to this day.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Alexander the Great_** **by Philip Freeman**

The eponymous hero of _Alexander the Great_ (2011) is remembered as one of the greatest military commanders who ever lived. Setting out from Greece at the age of 21, Alexander waged a ten-year campaign, during which he defeated the Persian Achaemenids and, in so doing, created the largest empire the world had ever seen. By spreading Greek culture and language throughout Eurasia, his legacy remained influential for centuries after.
---

### Andrew Roberts

Andrew Roberts is a renowned and award-winning historian and biographer. He is a fellow of the Royal Society of Literature, and his other titles include _The Storm of War: A New History of the Second World War_ and _Salisbury: Victorian Titan_.

