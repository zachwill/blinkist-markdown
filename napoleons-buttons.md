---
id: 587248e26267350004ccad28
slug: napoleons-buttons-en
published_date: 2017-01-11T00:00:00.000+00:00
author: Penny LeCouter & Jay Burreson
title: Napoleon's Buttons
subtitle: How 17 Molecules Changed History
main_color: 98555F
text_color: 98555F
---

# Napoleon's Buttons

_How 17 Molecules Changed History_

**Penny LeCouter & Jay Burreson**

_Napoleon's Buttons_ (2004) is all about the molecules that have guided the course of human history in the unlikeliest of ways. These blinks explore how major geopolitical and social changes can be traced back to the simple bonding of atoms in a molecule.

---
### 1. What’s in it for me? Discover the massive impact of the tiniest things. 

At some point, you've probably heard the theory of how a butterfly flapping its wings in one location can create a tsunami on the other side of the planet. But what if this notion were applied to our history? What if the tiniest change in a molecule's structure could alter the outcome of historical events — and human history as a whole?

It might sound like a radical idea, but as we'll see, even minute factors like how the molecules bond together in cotton, a nut or the resin from a tree have had a massive impact on our history. This hasn't happened by itself, but has been pushed forward by inventions and modifications from a few people whose ideas came to revolutionize the way we interact with our world.

In these blinks, you'll learn

  * why citrus fruit was an essential tool for explorers;

  * why gin and tonic was once seen as a medication; and

  * how witchcraft can still cure your headache.

### 2. Chemistry has had a major impact on the course of history. 

How much do you remember from high school chemistry class? Well, while your teacher probably went on at great length about electrons and the periodic table, it's unlikely that you learned anything about the impact of molecules on the course of history — which has been significant, to say the least.

Just take the molecular composition of the buttons worn by the soldiers in Napoleon's army; this little detail might well have lost them their catastrophic Russian campaign.

When Napoleon's army marched on Russia in June of 1812, they were over half a million strong. By December of the same year the _Grande Armée_, as it was called, numbered less than 10,000, ravaged by battle, starvation and the cold.

One theory that explains the army's rapid collapse blames the buttons that held together the soldiers' cloaks, trousers and coats. The buttons were made of tin, a material that doesn't hold up well when exposed to low temperatures; when it gets cold, this hard and shiny material crumbles into powder.

This shocking transformation was known as _tin disease_ and could explain why, by the time they reached the city of Borisov, Napoleon's soldiers were described by some as ghosts wrapped in hole-riddled carpets and cloaks.

And that's not all chemistry can explain about history. The distinctive properties of molecules have been center stage for major events throughout the course of recorded human life. After all, material properties can be affected by even the most minute changes in molecular bonds, and such transformations can have dramatic consequences.

For instance, cotton, which is made of cellulose, is much more effectively produced in humid climates. The ambient moisture helps the fibers stick to one another, making them less likely to split during weaving.

As a result, rainy northern England became an ideal place for the expansion of the cotton industry, which transformed this part of the country from a simple agrarian society into an industrial powerhouse. With the rise of industry came abominable working conditions in factories, which in turn resulted in widely implemented legislation to improve working and living conditions.

### 3. New York would still be called New Amsterdam if it weren’t for a seventeenth-century battle over nutmeg. 

There was a time in medieval Europe when pepper was so expensive that a mere pound of the stuff could buy the freedom of a serf from a nobleman's estate. In fact, there were many spices so highly valued that they fuelled exploration and led to wars, but none was more sought after than nutmeg.

For instance, this simple spice brought about a conflict that resulted in the Dutch ceding their colony, New Amsterdam on Manhattan, to the English in exchange for the Spice Island of Run.

In the 1660s, the Dutch were the major spice-trading power with a near monopoly on the trade in nutmeg. All they needed to attain total global domination was the atoll of _Run_, a place where nutmeg could be grown year round.

However, this island was under the control of the English, who had also recently taken over the Dutch colony on the island of Manhattan. The Dutch laid siege to Run and, in retaliation, the English attacked the ships of the Dutch East India Company, which were loaded with precious cargo.

Three years of war ensued, after which a treaty was signed, allowing both sides to save face. The agreement was that the Dutch would keep Run and the English would take the then-unassuming island of Manhattan.

But how did nutmeg, now a common addition to baked goods, have the power to launch a war?

Well, while nutmeg, like other popular spices of the time, was used to flavor and preserve food, the real reason for its high cost was its supposed ability to ward off the _Black Death_.

This belief caused people to wear nutmeg-filled pouches on necklaces, and while this behavior might seem superstitious, the chemical makeup of the spice provides some justification: nutmeg's distinctive smell is caused by a chemical called _isoeugenol_, a pesticide created by plants to deter ruminants and insects. Since the plague was carried by a specific type of flea that clung to rats, nutmeg may well have provided some protection against the disease.

### 4. Sea exploration was fatal for many sailors until they learned that Vitamin C prevents scurvy. 

Looking at an orange, you probably wouldn't expect it to impact the way the earth's exploration took place. But citrus did just that.

The invention of larger sailing vessels that could endure the open seas, meant that ships no longer needed to sail along the coast. In exchange for this greater freedom to roam, the ships' crews would be forced to rely on preserved food. Within six weeks of leaving port, many of them would begin to suffer from scurvy - a disease caused by a lack of vitamin C.

For centuries, the sailor's diet consisted primarily of salted meat and hard crackers, devoid of the vitamin C and _ascorbic acid_ found in fruits and vegetables. This dietary deficiency meant that scurvy killed more sailors than battles, shipwrecks and pirates combined. In fact, ships would often set sail with crews as much as 50 percent larger than needed to account for deaths caused by scurvy.

At the time, preventative measures for the disease were known but largely ignored, at least by most people. For instance, in 1601, Captain James Lancaster gave his crew daily doses of lemon juice, thereby preventing every one of them from getting scurvy. However, the expense of fresh and preserved fruit meant that a daily glass of juice wasn't seen as a viable option.

More than a hundred years later, in 1747, a Scottish surgeon by the name of James Lind conducted the first controlled study of citrus juice. He took 12 scurvy-ridden sailors and divided them into groups, one of which received oranges and lemons with their meals, while the others got seawater, vinegar, cider or diluted sulfuric acid.

His finding that citrus juice prevents scurvy was ignored for over 40 years, but eventually allowed for incredible exploratory feats like those of the famous Captain James Cook, the first explorer to cross the Antarctic Circle and the discoverer of the Great Barrier Reef.

> While scurvy ravaged crews on European ships, Chinese sailboats had long since been leaving port with pots of Vitamin C-rich fresh ginger on board.

### 5. Rubber as we know it today wouldn’t exist without human ingenuity. 

Did you know that rubber was first extracted from plants in Latin America as early as 1600 BC? At the time, it was used to make balls for games, as well as other items like hats and boots.

Upon reaching the Americas, European explorers soon became enchanted with this material, using it to make golf balls. But rubber's natural form wasn't durable or flexible enough for mainstream applications, and to make it so would require human ingenuity.

The first problem was that raw rubber doesn't hold its shape; it becomes brittle in cold weather and melts in the heat.

So, various techniques were devised to reinforce it. For instance, a Scottish chemist named Charles Macintosh used a flammable oil called _naphtha_ to turn rubber into a flexible fabric covering, thereby inventing the waterproof jacket. That's why, in Britain, raincoats are still known as _mackintoshes_, or _macs_ for short.

Then there was Charles Goodyear of _Goodyear_ tires. In 1839, by sheer happenstance, he discovered how to _vulcanize_ rubber: by combining it with sulfur and heat, he hardened rubber into an infinitely more practical and versatile form.

These innovations were highly significant because rubber is now essential to our mechanized society. For example, it's crucial to vehicle manufacturing, used for everything from tires to the conveyor belts they're made on.

This, in turn, had far-reaching consequences as the introduction of mechanized tractors and plows reduced the need for human labor in agriculture, thus enabling populations to shift toward cities.

Rubber is also essential to space travel since it's used to make the suits astronauts wear, as well as rockets and space stations. In this way, rubber even helps humans explore the universe.

However, the majority of the rubber we use today doesn't come from plants, but is instead a synthetic equivalent. By the dawn of World War II, militaries' need for rubber was huge and there simply wasn't enough natural rubber to go around.

This pressing need forced Russia to use dandelions as an emergency source of rubber and eventually led the United States to mass produce many of the artificial equivalents we call rubber today.

> The word "rubber" comes from an English chemist who found the South American plant extract to be a more effective pencil eraser — then also known as a "rubber" — than the existing method of using moist bread.

### 6. The quest for colors gave birth to the chemicals industry. 

Looking at an old painting, one thing becomes abundantly clear: people used to wear some seriously colorful clothes. In fact, dyes have been used to tint garments for thousands of years.

For instance, the Aztecs used to harvest carmine beetles to make crimson, while the Greeks produced indigo dye from huge shellfish harvests. However, there were problems with lots of these natural dyes.

For one, they tended to be very costly and difficult to obtain. Just take Saffron, a popular dye at one point in history. Handpicking the stamen of this flower from the Crocus is still a laborious process to this day, which is why it remains the world's most expensive spice.

Not only that, but the colors of naturally-dyed garments lose their intensity over time. This is even apparent today with jeans, the blue or indigo dye of which fades with wear.

As such, the invention of the first synthetic dye revolutionized the textile industry. It happened in 1856 when an 18-year-old chemistry student named William Henry Perkin attempted to synthesize _quinine_ from coal tar in the hopes of making an antimalarial drug. During his experimentation, he happened to create a dark purple liquid with formidable staining abilities.

After testing his invention on silk, he got in touch with Scottish manufacturers to determine how much it was worth. They said that, if it could be made cheaply, it would be one of the greatest inventions of their time.

So, Perkin left school to set up a factory and began producing _mauve_. It wasn't long before Europe was going crazy over the color, with both Queen Victoria and French Empress Eugénie wearing it.

In the years that followed, the lust for more colors gave rise to the chemical industry. Perkin's methodology was used to discover new colors and, by the end of the 1800s, dyers had 2,000 synthetic hues at their disposal.

More importantly, the scientific discoveries and technological innovation that resulted from this research into color laid the intellectual and financial foundation for the chemicals industry to develop things like antibiotics, fertilizers, explosives and plastics.

### 7. Two women transformed society by pushing for the invention of the first effective oral contraceptive. 

As antiseptics and antibiotics became more widely available in the twentieth century, a greater percentage of children survived childbirth. This positive change gradually led women to seek effective forms of birth control more urgently than ever.

After all, until "the pill" was developed, women had no effective means of birth control. Up until that point, oral contraceptives _had_ been used by women throughout history, but most were highly suspect. Among the most shocking examples were snake and spider eggs, or a seventh-century Chinese remedy that involved frying mercury in oil.

In short, the situation was bleak. In response, two women pushed for the birth control pill and began a social revolution in the process.

By the early 1950s, research had already commenced on the steroid _norethindrone_, a compound that laid the foundation for birth control pills. However, this drug wasn't being developed as a contraceptive, but rather as a means of relieving irregular menstrual cycles: by stopping ovulation for a few months, the hope was that a "rebound" effect would occur after withdrawing the drug, thereby jump-starting ovulation.

Meanwhile, Margaret Sanger and Katharine McCormick had been fighting for decades in the United States for a woman's right to control her body. By that point in their seventies, Sanger and McCormick traveled to Massachusetts to enlist the help of an experimental biology lab to create an affordable, reliable pill that could be "swallowed like an aspirin." The whole thing was funded by the independently wealthy McCormick.

By 1965, around 4 million women were taking the pill; within 20 years, that number had jumped to as many as 80 million women worldwide.

The effects on society were numerous and far-reaching. Birth rates dropped, of course, but a less obvious effect was that more women were obtaining an education and entering the workforce. The pill also played an important role in the expansion of feminism and the sexual revolution of the 1960s.

### 8. Witches used plants to treat all manner of ailments, eventually leading to their persecution. 

While witches have gotten a bad rap historically, in medieval times, they were often healers who provided herbal remedies and lucky charms. So why do we think of them so differently in the present?

Well, they had a reputation for other things too, like flying off to a demonic orgy known as _sabbat_.

Witches were known to create _flying salves_, ointments made from plants that included deadly nightshade and mandrake. These plants contain _alkaloids_, chemicals with a powerful effect on the nervous system; codeine and benzocaine are two modern-day examples.

When applied to the skin, these flying salves would induce out-of-body experiences, visions of beasts and euphoria. Some accounts state that witches would smear the salves on broom handles, which they would straddle while naked. So, while some witches might have confessed to flying off to sabbat because they truly believed they had, in reality, the rituals they performed were probably a form of escapism for women who suffered short and grueling lives.

Nevertheless, this stigma meant that plant healing went from being socially tolerated to being seen as the devil's work, even though witches employed lots of plants that would later be used by the pharmaceutical industry. For instance, the extracts they used to treat headaches were derived from willow trees, an acid of which would later be synthesized to produce aspirin.

But although the use of plants as medicine began to be frowned upon, it was still a very widespread practice, with even the Christian church allowing it; that is, as long as they were the ones to administer cures, which they could then tout as religious miracles. This monopoly on plant-based healing meant that magic practiced outside the church was deemed the devil's work and, in the 1350s, the church began to crack down on witchcraft through the Inquisition.

As they began to run out of heretics to burn, they turned their torches onto the local village healers, resulting in the torture and murder of hundreds of thousands of people over the course of more than three centuries.

> _"The last witches executed in Scandinavia [were] 85 elderly women burned at the stake in 1699."_

### 9. Quinine was the first molecule used to combat malaria. 

Did you know that the word "malaria" comes from the Italian for "bad air"? For centuries, people thought this blood-borne parasite carried by mosquitos was actually caused by noxious mists emanating from marshlands.

But while they may have been mistaken as to its origins, people knew that malaria was a serious killer, probably one of the biggest of all time. Even today, it affects 300 to 500 million people annually.

So what has been done to cure it?

Well, quinine was the first antidote discovered by humans. This cure came from high up in the Andes Mountains where the bark of a tree belonging to the _cinchona_ genus was found to contain a molecule that would come to save countless lives.

While locals had brewed tea from the bark for ages to cure fevers, accounts vary as to how Europeans found out about it. One story tells of the Spanish _countess of Chinchón_, who fell terribly ill while visiting the region. Her European doctor had no clue what to do and turned to the indigenous people of the area for help. They cured her and her story lives on in the name of the cinchona tree species.

But regardless of how it happened, word quickly spread about the amazing properties of quinine and global demand for this antimalarial tonic soared, quickly outstripping the supply.

The shortage led European governments to decide to plant cinchona trees on other continents. But, since the quinine trade was wildly lucrative for South American countries, they banned all exports of the tree's seeds.

As a result, seeds were smuggled out and planted by the Dutch and English. However, the quinine content varied dramatically across species and the quinine level of the seeds they acquired was well below the 3 percent necessary to make them economically viable.

During this time, the Dutch paid $20 for Bolivian seeds obtained by an Australian botanist, which contained a whopping 13 percent quinine. This transaction has been called the most profitable investment ever because, by the 1930s, the Dutch were growing practically the entire world's supply of quinine.

> _"The British habit of taking quinine as a prophylactic precaution against malaria developed into the evening 'gin and tonic.'"_

### 10. Final summary 

The key message in this book:

**The world hidden from the naked eye, the one composed of the molecular bonds between atoms, is responsible for some of the most significant events in human history. By telling the stories of these molecules, they can be brought out of the chemistry lab and put into the context of the real world.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Violinist's Thumb_** **by Sam Kean**

_The_ _Violinist's_ _Thumb_ is an exploration into DNA, the double-helix of life itself. The following blinks chronicle the scientific discoveries that led us to understand DNA and the major role it's played in the emergence of life on earth.
---

### Penny LeCouter & Jay Burreson

Penny LeCouteur is an author and professor of chemistry based in Vancouver, Canada, and is the recipient of the Polysar Award for Outstanding Chemistry Teaching in Canadian Colleges.

Jay Burreson, PhD, is an industrial chemist and manager of a technology company.

