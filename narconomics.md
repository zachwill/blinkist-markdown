---
id: 5846c0e8714e5f00047cdca5
slug: narconomics-en
published_date: 2016-12-09T00:00:00.000+00:00
author: Tom Wainwright
title: Narconomics
subtitle: How to Run a Drug Cartel
main_color: FFF233
text_color: 666114
---

# Narconomics

_How to Run a Drug Cartel_

**Tom Wainwright**

_Narconomics_ (2016) gives us a tour of the business side of the $300-billion global narcotics industry. From human resources to PR to franchising and diversification, these blinks show how drug cartels run their operations not unlike highly successful businesses. By exploring the economic phenomena at work behind the world's drug problem, the author presents new insights into how governments can defeat it.

---
### 1. What’s in it for me? Learn more about the worldwide drug industry. 

The Mexican government estimates that, between 2007 and 2014, more than 164,000 people died in the war waged against the country's drug cartels. Despite massive efforts to combat them, the cartels have grown to become enormous enterprises, no longer just selling drugs, but playing a vital role in human trafficking as well.

So how do these cartels work? How have they been allowed to grow to become such a big problem?

These questions are exactly what these blinks will aim to answer, taking a detailed look at the drug industry, both in terms of how its members operate and relate to the challenges of the market, as well as the challenges for governments trying to stop them.

In these blinks, you'll discover

  * why the legalization of drugs could be advantageous to society;

  * why cutting the amount of drugs can lead to higher profits for the cartels; and

  * how some drug cartels recruit their members.

### 2. The US government’s efforts to attack the supply side of the drug industry are flawed. 

It was in 1971 that then-US President Richard Nixon launched the "war on drugs." Between then and now, drug incarceration rates have shot up exponentially in the United States. But the amount of drugs being produced and consumed globally has not decreased; in fact, there's more money in the drug market right now than ever before. How is this possible?

Presumably, the US government wanted to tackle the drug problem at its root. As such, drug policy has been designed to attack the drug industry in one area — supply. Take crop dusting in South America, for instance.

South American countries are an important source of many of the drugs that enter the United States. So, the US government entered into agreements with these nations to crack down on the farming of coca leaves by "crop dusting" them — that is, spraying pesticides on crops from low-flying planes to destroy them. This seems like a logical fix, right? After all, it'll be harder for the drug industry to flourish if it doesn't have the supply to meet demands.

Unfortunately, things aren't so simple. This approach has led to what researchers call _the balloon effect_. The phenomenon gets its name from the way that when one part of a balloon is squeezed, the air inside moves, but the volume of the air itself doesn't decrease.

Like air inside a balloon, cartels can also move around, in this case from one area of South America to another. When one government cracks down on coca crops, cartels just pack up and move to another country. As a result, the problem keeps moving around, but never really disappears.

That's not the only issue with the US government's approach. Their supply-side attack targets farmers, without ever directly confronting cartels or consumption — two aspects without which the drug industry wouldn't exist.

Cartels can be described as _monopsonies_, that is, single buyers. Cartels decide how much to pay farmers, who are essentially at their mercy. By targeting impoverished farmers and not the cartels, the US "war on drugs" hardly gets to the real root of the problem.

What's more, when supplies decrease, consumers become okay with paying more for the same amount. Therefore, yearly revenue generated from drugs will remain the same or grow, even if supply decreases.

### 3. Legalization of marijuana is one major threat to cartels. 

It's estimated that the US marijuana industry makes a whopping $40 billion each year. A total of $7 billion comes from the legal marijuana market, which is a fairly significant proportion. How does an economic arrangement like this benefit a government fighting a war on drugs?

Well, legalizing marijuana allows the government to weaken drug cartels and generate revenue while doing so, by becoming the cartels' rival and through taxation, respectively.

Legal marijuana crops can be grown in better conditions. With access to larger fields bearing nutrient-rich soil, the resulting product is of a much higher quality than what most cartels can offer. This forces the cartels to reduce their prices in response to this new legal rival on the market, cutting their profits in the process.

And, as with any other legal commodity, the government is able to make money off marijuana through taxation. In Colorado, for instance, marijuana was legalized in 2014. Just one year later, marijuana sales by head shops — shops licensed to sell the drug — hit $996 million in the state, with $135 million going to the state's tax coffers.

Of course, legalized marijuana also has a twofold benefit for its consumers. By supervising and regulating the growing and selling of marijuana, the government is able to ensure that consumers don't experience any of the health risks that come with ingesting a bad batch of cannabis, which can send an unlucky user to the hospital.

Legalization also opens up the growing of cannabis to horticulturalists, who are free to experiment with different plants and techniques. This leads to the creation of diversified products and higher customer satisfaction.

In Colorado, marijuana legalization has even seen the introduction of a "cannabis menu." All head shops in the state have a menu that provides customers with information about the expected high, side effects, the likelihood of a hangover and any other health risks regarding the products sold in the shop.

So, it seems that dealing with the drug problem on the customer-facing side, rather than the supply side, has benefits for both governments and consumers. But to really get to the bottom of why attacking the supply side of the drug industry doesn't work, let's take a look at the cocaine trade.

### 4. Drug cartels engage in both competition and collusion. 

We've learned about the relationships between governments and cartels, but how do cartels behave toward each other? Well, from an economic standpoint, there are two possible models: competition and collusion.

In Mexico, rivalry reigns as drug cartels compete violently with each other. Take the city of Juárez for example; the Juárez Cartel and the Sinaloa Cartel are now famous for their brutal and public acts of vengeance against one another, which include dumping decapitated bodies on roadsides or hanging corpses from signs along the highway.

It has been estimated that at least 60,000 people died between 2006 and 2012 as a result of the violent cartel rivalry in Juárez. As well as individuals directly involved in the cartels, many civilians, law enforcement officials and investigative journalists lost their lives in the crossfire.

In El Salvador, on the other hand, drug cartels collude, which has both positive and negative consequences.

Collusion is when economic competitors join forces to earn more money for themselves at the expense of the consumer and the overall market. In El Salvador, cartels set high prices, dividing up neighborhoods into dedicated segments and splitting profits among themselves. Consumers, as we learned in the first blink, are always willing to pay more. So, in the case of collusion, neither drug consumption nor the number of those working in the trade decrease.

There is, however, a surprising social benefit: rates of violence and murder in El Salvador have decreased significantly. Back in 2009 before cartels began colluding, 71 people were murdered out of every 100,000 people in El Salvador, largely due to violent gang rivalry. But after the 18th Street Gang and Mara Salvatrucha partnered and divided the country into sectors, where each had rights to all operations and profits, violence and murder decreased significantly. By 2012, figures stood at 33 murders per 100,000 people, a marked improvement.

While neither competition nor collusion affect the rate at which narcotics are sold and consumed collusion is much more beneficial to the community, since much less violence and murder occur as a result.

### 5. Recruiting employees and ensuring they stay loyal aren’t just concerns for companies, but of drug cartels too. 

Finding the right person for the job can be a headache for any business. But if you're a drug cartel operating in total secrecy, you can't just pop an ad in the newspaper or on LinkedIn when you need staff. So where do drug cartels find their new recruits?

Doing human resources for drug cartels means looking in alternative places, like prisons. Cartels target inmates knowing that it'll be difficult for them to find normal jobs back on the outside. Joining a cartel is a way for inmates to make sure they have work once they're released.

The Mexican Mafia, a US-based cartel, has its members reach out to new inmates not only to offer them jobs once they're out, but to provide them with work while they're still inside. Tasks inside jail include extortion and threats of violence against other inmates. Once new recruits are released, they're set to work on trading and smuggling drugs.

While some businesses concern themselves with the challenge of keeping employee satisfaction up, cartels experience human resources problems on a whole different level.

Cartels suffer from what economists refer to as the _collective action problem_. This is where there is a conflict between the interests of the group and the interests of the individual. In other words, cartels need to ensure that their employees won't steal from them. But how?

By distributing power. Take Nuestra Familia, a cartel in Mexico. This cartel is built around a hierarchy of generals, lieutenants and soldiers. The general is in command and is the most powerful. Ten captains are ranked just below the general, each of whom is in charge of a certain number of lieutenants who, in turn, each lead a group of soldiers.

In this system, power is distributed to ensure that lieutenants and captains have a say. A general can fire captains but cannot hire replacements, while the replacements are voted in by their lieutenants. Furthermore, though in charge, the general can be booted out by a unanimous vote on the part of the captains.

This structure allows lower-ranking members to hold their superiors accountable, and ensure that leaders don't take advantage of employees. By dividing hiring and firing privileges among the ranks, members can ensure they receive fairer treatment, and are likely to stay loyal as a result.

> _"These two conundrums — how to hire staff, and how to make sure they do what they are told — are what occupy drug cartels' human-resources managers."_

### 6. Drug cartels, like big brands, use social responsibility as a PR tool. 

These days, we expect businesses to demonstrate social responsibility. Some invest in employee health care, while others lead campaigns to support charities. But in most cases, businesses don't do these good things to _be_ good; most of the time, it's a strategic public relations ploy. They simply want to look good and boost their brands above competitors.

Interestingly enough, the same applies to drug cartels. Despite their unsavory line of business, cartels are known for trying to market themselves in a better light than their rivals. A common strategy is to decry the drug-related violence in their communities.

Remember the Sinaloa cartel in the city of Juárez? They decided to demonstrate social responsibility by highlighting the city's rising murder rate. They had billboards put up to condemn the violence and proclaim that their cartel would never target women and children, nor did they allow the kidnapping and extortion. This put them in a more positive light as compared to their rivals.

Drug cartels also present themselves as heroes filling the voids left by failing public services. Well aware that law enforcement and social security are absent in their communities, cartels take up roles as protectors and patrons.

Drug alms, known as _narcolimosnas_, are a great example of this — cartels take measures to give back to the community by distributing money among the poor, or by funding the construction of churches and chapels.

Cartels also offer a form of security for individuals. By paying a fee, individuals can hire cartels to protect them or threaten or attack anyone bothering them. This way, cartels seemingly fill the role of the police.

### 7. Both corporations and cartels use offshoring as a means to increase profits. 

Ever wondered why certain consumer objects can be sold so cheaply? Low prices are usually the result of _offshoring_, when a company moves its production overseas to save money and boost profits. Cartels do exactly the same.

Corrupt governments in other countries offer a great opportunity for cartels to make some money. Loosely enforced laws and weak governmental institutions create an ideal environment for drug cartels.

In Honduras, for example, police officers' salaries are low. The police force has neither the resources nor the incentive to go after cartels, who know just how easy it is to buy (or kill) individual officers off. As a result, Honduras became the destination for around 75 percent of cocaine smuggling flights between 2009 and 2012.

And how do other national governments fighting the war on drugs tackle offshoring? Well, one way is to have the international community publicly shame the country in question for letting cartels have their way. This makes the country less appealing to transnational companies looking for places in which to invest, which is a strong incentive for a country to make changes quickly.

One organization, Transparency International, alerts the international community to countries making life easy for drug cartels by publishing yearly corruption indexes that rank countries around the world. These reports include information for transnational companies looking to invest overseas, including the prevalence of bribery, national attitudes toward politicians, the use of public funds and crime rates.

Through these reports, countries like Costa Rica have proven themselves better places in which to invest and do business in than countries like Guatemala and Honduras. While the latter two nations struggle with high murder rates and endemic corruption, Costa Rica was able to lift its score because of the impartiality of its judges and the reliability of police forces.

### 8. Franchising is a double-edged sword for drug cartels. 

What can drug cartels learn from the McDonald's business model? Quite a lot, actually. Let's take a look at how cartels use franchise models to make financial and political gains.

A franchise is a business model whereby companies allow other individuals to undertake specific commercial operations as agents of their brands and organizations. The benefits of franchising for cartels are twofold, allowing them to both expand their territory and secure a steady stream of revenue.

Rather than sending drugs from A to B, a cartel with a franchise agent, or _franchisee_, in a region that includes point B, can gain access to greater profits. Los Zetas, a criminal syndicate in Mexico, adopted the franchise model for these reasons.

Los Zetas identified the regional markets over which they wanted influence, and partnered with local leaders there. This meant that rather than getting into conflict with local groups, and lose their soldiers, drugs, weapons and profits in the process, Los Zetas shared the total profit between themselves and the local leaders turned franchisees. In return, the franchisees provided protection for Los Zetas' local leader and arms for their staff.

In this way, franchising allowed Los Zetas to secure a certain amount of money without the risk of the gang losing members or spending more money than the region is worth.

While franchising has many benefits for cartels, it's also a double-edged sword. For one thing, franchising often leads to competition within a territory, which results in a loss of revenue.

To illustrate this, let's imagine that two McDonald's restaurants have opened next to each other. The revenue that can be gained from a certain area will be split between the shops, increasing antagonism between them. Similarly, if multiple local leaders and staff join a single cartel, revenue will be split among them and franchisees will earn less, which, in turn, will lead to conflict and tension.

In addition, the decentralization of power that goes hand-in-hand with franchising makes it harder to hold someone accountable for mistakes. Why? Because with every franchise, it gets harder and harder to make sure everyone in the cartel follows the rules.

In one instance, a Los Zetas member murdered a US Immigration and Customs agent working in Mexico, breaking the unwritten rule of Mexican cartels to never kill an American — especially not those working in law enforcement. The result was an increase of American and Mexican law enforcement arrests, which slowed down cartel operations considerably.

> _"The franchising model that has powered the cartel's growth could yet prove to be its undoing."_

### 9. Synthetic drug manufacturers are able to create new products and stay ahead of the law. 

Ever heard of legal synthetic drugs? Yes, they exist, and they pose a major challenge to governments combating the drug industry.

Drugs can be created in such a way that they can be sold legally until they're made illicit. Once a synthetic drug is made illegal, makers simply create a chemically altered version that's technically a different legal drug.

In the early 2000s, Mark Bowden, a New Zealand drug dealer, began selling benzylpiperazine, or BZP. This drug had been designed in the 1940s as an antiparasitic agent for cattle. But when humans ingested it, BZP created a rush similar to that of amphetamines. The drug was safe enough on its own, but led to dangerous anti-social side effects when consumed with alcohol.

By April 2008, BZP had been outlawed. But this didn't stop drug dealers, who knew they could always stay ahead of the law by changing the chemical composition of other amphetamines with a high similar to that of BZP.

This leads to a cycle that creates huge regulatory problems for governments. The cycle goes like this: Drug manufacturers alter the composition of a banned drug to create a new legal drug. The new drug's harmful effects come to light, and regulators ban the drug. Then drug manufacturers alter its composition to make a new legal drug. And so on.

Despite this, some governments are attempting to crack down on legal synthetic drugs by increasing safety procedures. Some even create labs that prove the safety of drugs as soon as they start circulating.

Other governments also changed legislation to hold drug manufacturers accountable. This was the case in New Zealand, where laws were changed in 2013 so that it was up to drug manufacturers to prove that their drugs were safe, and not the prosecutor's responsibility to prove that they were dangerous.

### 10. Cartels diversify to increase their revenue, often by expanding into people smuggling. 

Did you know that back in 1977, Coca-Cola made its first forays in the wine industry? It might sound like a strange business move, but all they were doing was _diversifying_ — that is, expanding from their primary product to take on operations in another field to increase profit. Cartels use the same strategy.

Many cartels have expanded from their primary service, drug smuggling, to another field: people smuggling. Why? Simple. Cartels go where the money is. As such, many have diversified their business to include smuggling people across borders, and specifically the United States-Mexico border.

People smuggling into the United States has become economically lucrative due to the increase in security screenings following 9/11, as people struggling to move across the border seek out other options. Furthermore, people smuggling offers more financial security for cartels. While drugs can be seized at a border and prevent cartels from receiving their payment, those hoping to cross the border illegally must pay up-front.

So how much does it cost to have yourself smuggled over the border? Well, it's certainly not cheap, as prices are on the rise. Foot crossing with a guide but without documents has gone up from $2,000 to $5,000. Crossing with fraudulent documents used to be $5,000, but today costs around $13,000.

For drug cartels, people smuggling has become great business, and one that has only required applying the structures they already had in place. After all, the drug industry knows no national boundaries, and nor does people smuggling, which again makes smuggling a particularly tricky matter for governments today.

Widely differing drug laws in different countries mean that international cooperation has remained limited on what is truly an international problem.

The director of the White House Office of National Drug Policy in the United States, for instance, has been in the strange position of having to speak out and take action against marijuana growing and sales abroad, while some US States like Colorado, Oregon and Washington have already legalized marijuana.

### 11. Final summary 

The key message in this book:

**Although much emphasis is placed on attacking the drug trade from the supply side, an economic perspective on the behavior of cartels highlights how they can be weakened in the same way that a company might frustrate their rivals in a competitive market. As we struggle to formulate effective drug policies, it's worth remembering that drug cartels, after all, are businesses — ones that create HR strategies, practice social responsibility, build franchises and diversify.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Chasing the Scream_** **by Johann Hari**

_Chasing the Scream_ (2015) gives a riveting account of the first hundred years of the disastrously ineffective War on Drugs. Weaving together fascinating anecdotes, surprising statistics and passionate argumentation, Hari examines the history of the War on Drugs and explains why it's time to rethink addiction, rehabilitation and drug enforcement.
---

### Tom Wainwright

Tom Wainwright is the UK editor for the _Economist_ magazine. As a journalist and correspondent, he has also formerly covered Mexico and Central America for the _Economist_. His writing has been published in the _Times_, the _Guardian_, and the _Literary Review_.

