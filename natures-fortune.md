---
id: 55914e923038330007230000
slug: natures-fortune-en
published_date: 2015-06-29T00:00:00.000+00:00
author: Mark R. Tercek and Jonathan S. Adams
title: Nature's Fortune
subtitle: How Business and Society Thrive by Investing in Nature
main_color: 739134
text_color: 5F782B
---

# Nature's Fortune

_How Business and Society Thrive by Investing in Nature_

**Mark R. Tercek and Jonathan S. Adams**

_Nature's Fortune_ (2013) challenges our views on economic development and the environment. Drawing on research about the ways we work with and against nature, nature lover and former investment banker Mark Tercek presents a compelling case for investment in green infrastructure, and shows us how economic growth and conservation can benefit each other.

---
### 1. What’s in it for me? Learn why the best defense against Mother Nature may be Mother Nature herself. 

Mankind has achieved so much in taming nature. We have built barriers to keep out the sea, sewers to channel waste, and dams to hold our drinking water. All our activities seem pretty impressive, right?

Well, what if we have been wasting our time? What if we already had a defense against nature, provided by nature itself? The uncomfortable truth is that many of our most effective pieces of infrastructure are natural, from floodplains to wetlands. In building our great defense systems we might be doing more harm than good.

These blinks explain how we can benefit from nature's bounty and how we should all be making efforts to protect the environment.

In these blinks you'll discover

  * just how powerful an oyster bed can be;

  * why Coca-Cola is very keen to protect the developing world's clean water supplies; and

  * why levees can directly lead to flooding in other areas.

### 2. Nature provides services similar to human-built infrastructure. 

When we are confronted by dirty water what do we do? We build a sewage plant. When we are faced with a persistently flooding river we contain it with levees. In short, when nature provides us with a problem, we build man-made infrastructure to combat it.

What's more, we tend to take all this modern infrastructure for granted. So let's just take some time to look and consider why we build them in the first place.

We primarily set up infrastructure in order to access various services.

For instance, dams and levees provide the service of shielding us against droughts and floods, and sewage and filtration plants provide safe and clean water in our homes and workplaces.

All the pieces of infrastructure mentioned above are examples of human or _gray infrastructure_, so-called because it is usually made of concrete.

The Hoover Dam, for example, is the largest water reservoir in the United States and consists of 3.3 million cubic metres of concrete. Just for scale, this is the same quantity that would be needed to construct nearly 40 Wembley Stadiums.

But nature already provides us with a kind of _green infrastructure_ that does the same job — lakes and reservoirs are naturally created bodies of water that do the same as a huge man-made dam.

Green infrastructure can provide services to filter water, break down contaminants and store excessive water for use in dryer times. Remarkably, our coral reefs can even provide protection against flooding, as they break down waves before they get a chance to hit the land.

You can see, then, that we already have many of the kinds of infrastructures we need to thrive in place on our planet.

### 3. Large companies already invest in nature when their business opportunities are affected. 

When we think of powerful companies and their impact on the environment, large-scale catastrophes such as the Deepwater Horizon oil spill spring to mind. So it may come as a surprise that large companies are actually increasingly investing in nature, especially as they start to realize how much we rely on it.

Environmental protection has become a trend that is rapidly gaining momentum in the business world.

Since 2013, 400 of the largest 500 American companies have issued sustainability reports and many of them have committed to mitigating their environmental impact.

Some companies have gone even further and spotted business opportunities in environmental protection.

In 1996, Dow Chemical, the world's second largest chemical manufacturer, came under regulatory pressure to increase the water treatment capacity of their plant in Seadrift, Texas. But rather than investing in gray infrastructure, Dow created a nearby wetland to filter the water. The motivation behind this was economic: constructing a water treatment plant would have sapped around $40 million of their budget, whereas a wetland serving the same purpose cost them a mere $1.4 million.

Many companies have also begun to invest in nature as a reaction to seeing their core business come under threat.

Today, for example, Coca-Cola heavily invests in water preservation and is committed to becoming water neutral by 2020. That means that for every drop of water they use in their products, they promise to return the same amount of clean water back to nature. But it was actually a PR disaster that sparked these conscientious efforts.

In 2004, farmers' wells in Kerala, India, dried up. The cause of this turned out to be a nearby Coca-Cola bottling plant siphoning off the communities' groundwater, and as Coke's transgressions were pointed out, a boycott against the company began in the Western world. The company had to take a long hard look at how their operations sourced water.

Coca-Cola FEMSA, the world's largest independent Coke bottler, also spends millions on water funds that finance projects for the protection of Latin American forests. Why?

The reasons, again, are economic. Specifically, to lock down Coke's most important ingredient: water, which is threatened by the decline of the forests.

> _"Sustainability is moving from a fringe concern to a core focus of business decision-making."_

### 4. Investing in nature is often cheaper than investing in gray infrastructure. 

If your son came home from school and told you he got a D minus on his report card, you'd be right to be concerned. But what if that was the grade your country was given for some of its critical infrastructure? You may not know that the US levee system and the US wastewater facilities situation is rather dire. In fact, in 2009, both were hit with a D minus from the American Society of Civil Engineers.

The type of gray infrastructure seen in much of the United States is fairly complex and often requires a lot of maintenance, usually demanding extensive construction work and complicated chemical, biological and/or physical processes.

Consider water filtration. In 1989, when New York faced stiff regulations regarding its surface water, the estimated cost of a new filtration plant totalled around $8 billion.

As this infrastructure would deteriorate over time, it would also require costly maintenance in order to keep it running. Under budgetary constraints, the problem would snowball as maintenance was neglected in an attempt to save money.

New Yorkers decided against constructing another expensive filtration plant and instead opted for a cheaper, alternative venture: they began an environmental protection project in the Catskills, which supplied most of their water. At that time, farming was among the main sources of water pollution and so the project created monetary incentives for farmers to manage their land in a way that preserved clean water.

The project was a huge success and not only resulted in long-term clean water, it also stimulated an economic boost for the region.

This is where the benefits of green infrastructure become clear. It's true that just like gray infrastructure, green infrastructure also decays. However, unlike gray infrastructure, it can renew itself. Hence it doesn't require artificial maintenance.

We simply need to let it be — meaning it's often far less expensive than gray infrastructure.

### 5. Green infrastructure often works better than human engineering. 

Our recent history tells a tale of persistent attempts at bringing nature under our control. In doing this, we've accomplished some significant innovations. However, our engineering has also sometimes created systems that changed things for the worse.

It's probably no surprise that gray infrastructure can come with negative effects or even increase the very risks it was designed to reduce.

We can see this in our artificial flood protection, which usually doesn't work particularly well. This is because seawalls engineered to shield shores against floods often redirect the wave energy back into the water and in doing so, wreak havoc on natural habitats.

Our levees, too, sometimes increase the risk of river floods as they speed up the flow of rising water and then exacerbate flooding in populated areas.

On the contrary, green infrastructure usually delivers services that aren't harmful.

For example, natural protection of shores against floods doesn't damage habitats — it actually preserves them.

Many shores, for instance, have oyster reefs that act as incredibly efficient natural coastal buffers. They are able to both redirect the wave energy and absorb up to 75 percent of it.

Nature also regulates itself with floodplains, which are a superior method of flood control.

Floodplains are flat areas near rivers where excess water flows if the river can no longer contain it. Unlike concrete levees, floodplains are able to adapt to changing river patterns.

This could be seen in 2009, when the rainfall in Arkansas and Louisiana was much higher than usual and Monroe city officials nervously witnessed the water level of the Ouachita River steadily climbing. All of a sudden, the level dropped and the situation died down. But what exactly happened? A levee had burst 20 miles upstream of Monroe and the excess water from the Ouachita spilled into a vast area of abandoned fields that reconnected it with its floodplain.

> _"Nature offers solutions to the problems of both too little and too much water."_

### 6. Investments in nature have many positive outcomes. 

Green infrastructure — such as the oyster reef — usually serves multiple purposes.

Oyster reefs don't just buffer the land from the sea, they are also an important source of water filtration. Astoundingly, a single adult oyster can filter up to 50 gallons of water per day.

Scientists estimate that before their eradication from Chesapeake Bay, the local oysters filtered through the bay's 19 trillion gallons of water every _three days_.

Filtered water is essential for a flourishing habitat for animals and humans alike. Crabs and young fish need sea grasses and these only take root if the water is clear enough for sunlight to penetrate through to the ocean or riverbed.

If we invest in environmental protection, the advantages invariably spread to multiple areas.

This is particularly true regarding habitat restoration, like the creation of _marine protection area_ s. These reserves ban fishing, which means that the fish can grow bigger and produce more offspring. Typically, four years after the protected zone is established, it will have enough fish for them to spill over to neighboring areas.

This can also be good for local economies. In Fiji, researchers found that within five years of setting up two community-managed no-fishing areas, the spillover of the reinvigorated fish population approximately doubled the local income from fishing.

Another benefit of preserving nature is that it allows us to enjoy areas for recreational purposes.

Recent experiments suggest that interacting with nature improves our stress responses. In one study, for instance, scientists measured the stress levels of people taking a challenging math test. Those from rural areas responded with lower levels of stress than those who lived in cities. And the most stressed people? Those who had been born and raised in cities.

> _"Protecting and restoring the world's oyster and coral reefs is both an economic and ecological imperative."_

### 7. Whether companies or the government should invest in nature depends on different circumstances. 

Even with the knowledge that investing in the natural world is hugely beneficial, we can't expect companies to jump at every chance to do so.

That's because companies want to know how soon they'll see their returns.

It follows, then, that short-term returns are the most attractive to businesses.

This was the case in the mid-2000s, when sugarcane farmers in Cauca Valley, Colombia, were facing dwindling revenues. Water scarcity was a real issue and farmers were forced to cut back their yearly irrigation cycles, resulting in a loss of $6 million per year. So they joined a water fund conserving the Cauca River's watershed. Although it came with a $1 million to $3 million price tag for eight years, this was abated by the fact that the farmers were able to keep running their five irrigation cycles per year.

Eight years is not too long term for most businesses, but what about long-term investments? Investing in oyster reefs, for example, offers a lot of rewards, but it would take a private investor seeking increased fish stocks around 20 years to break even. In these long-term cases, the government needs to step in.

Another point to consider is that the success of an investment usually depends on the characteristics of the market. Take deforestation in Brazil.

In 2006, Greenpeace showed the connection between soy farming and deforestation. They honed in on McDonald's, which quickly pressured Cargill, their main soybean supplier, to take action against the deforestation. As a result, Cargill agreed to purchase soy only from land that had been deforested prior to 2007. This was doable, as the soy business mostly involved larger farms that could be controlled by Cargill using satellite imagery.

But the campaign against cattle farmers who were clearing the rainforest was unsuccessful. Cattle from thousands of ranchers couldn't be controlled easily and farmers could sell their products locally. In this case the government would have to intervene if any meaningful change was to take place.

### 8. Foster nature in cities to create the best opportunities. 

A city park is an oasis in a concrete jungle. Remember the last time you went to a park? How did you feel? But parks need not be the only places city-dwellers can enjoy nature. Investing in greener cities could change the way we live.

Our increasing urban population poses an important challenge. With more than half the people on the planet living in cities, we have become an urban species coming up against a variety of problems.

These problems include elevated stress levels, asthma from air pollution and excessive heat. But it doesn't stop there.

City sewer systems often pollute the environment.

One common type of sewer system accumulates both sewage and surface water from rainfalls.

In times of heavy rain, combined systems buckle under the large volume and dump raw sewage into the rivers in order to prevent a system failure. This approach to sewerage needs to change, especially as by 2050, the population of cities is expected to increase by nearly three billion people.

So how can creating green cities benefit us?

Well, landscape architects and engineers have been working with a variety of techniques to make cities more like forests.

First, rain gardens can be constructed next to sidewalks to absorb water and keep it out of the drainage system. Above the sidewalks, rooftops of buildings can be covered with soil and vegetation that also suck up rainwater. These methods cool and clean the air and reduce runoff by up to 80 to 90 percent.

Green rooftops also insulate the buildings and save energy on heating and cooling. A 2009 study of Philadelphia's $1.67 billion investment in green infrastructure found that, over 40 years, the total value of the green infrastructure will rise to almost $3 billion.

> _"The best place to appreciate the value of nature may be where it is most lacking."_

### 9. Creating market prices for nature will help fighting climate change. 

Can you imagine a stampede of companies racing to invest in rainforests?

In a competitive market economy, the companies that survive are the ones who are most effective at reducing costs, which leads to an efficient use of scarce resources. Right now, emitting tons of carbon dioxide into the atmosphere doesn't cost a company anything, so nobody bothers to reduce emissions.

So what if we put a price on the capacity to generate carbon emissions — the more emissions you make, the more you must pay — _and_ allowed firms to trade their allowances for making carbon emissions? This is called the _cap-and-trade approach_.

Each company can then decide whether it makes more financial sense to reduce its emissions and sell credits to others, or to purchase credits from other companies that have reduced their own emissions. The competitive market becomes an efficient tool in reducing environmental damage.

In the United States, the cap-and-trade approach worked wonders at reducing acid rain. The 1990 Clean Air Act enlisted cap and trade to cut back on sulfur emissions from power plants, the main culprit behind acid rain.

Since the law was signed, sulfur emissions in the United States have halved — at a far lower cost than economists predicted.

This is where the stampede to invest in rainforest conservation comes in.

Today, a shocking 15 percent of greenhouse gas emissions come from the loss of rainforest. This is equal to the amount that the world's traffic contributes.

It's cheaper to save a ton of carbon in the rainforest than to build or upgrade expensive filters to remove the CO2 from your factory's emissions. Hence companies might save their credits by preserving rainforests rather than by reducing their own carbon emissions.

### 10. Final summary 

The key message in this book:

**Economic growth and nature preservation don't have to fight against one another. In fact, protecting the environment is in businesses' very own interest. Nature provides a host of vital services that human engineering either cannot provide at all or can only recreate at a much higher cost.**

Actionable advice:

**See what nature can do.**

Search for "Oyster filtration" on YouTube to see an amazing time-lapse video showing how quickly oysters clean up a dirty aquarium.

**Suggested further reading:** ** _A Better World, Inc._** **by Alice Korngold**

Many of us are quick to assume that big corporations are the enemies of the environment. _A Better World, Inc._ explains how the opposite is true: companies are in a better position to solve some of the world's biggest problems than many governments and campaign groups. This book outlines why, and lists the steps companies can take to improve our planet, while raising their profits at the same time.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Mark R. Tercek and Jonathan S. Adams

Mark Tercek is CEO of The Nature Conservancy, an American charitable environmental organization. Prior to this, he was a managing director and partner at Goldman Sachs.

Jonathan Adams is a conservation biologist, author of _The Future of the Wild_ and co-author of _The Myth of Wild Africa_.

