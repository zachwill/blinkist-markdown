---
id: 5948c80fb238e10005eecf6a
slug: negotiating-the-nonnegotiable-en
published_date: 2017-06-23T00:00:00.000+00:00
author: Daniel Shapiro
title: Negotiating the Nonnegotiable
subtitle: How to Resolve Your Most Emotionally Charged Conflicts
main_color: D55D2B
text_color: A34721
---

# Negotiating the Nonnegotiable

_How to Resolve Your Most Emotionally Charged Conflicts_

**Daniel Shapiro**

_Negotiating the Nonnegotiable_ (2016) offers insights into a new framework that can be applied to solve stubborn conflicts in both our personal and professional lives. The blinks emphasize the importance of the "tribal mind," while also illustrating how we actively address emotional pain and examining the role of identity in conflict resolution.

---
### 1. What’s in it for me? Learn how to get past your own self-righteousness. 

At some point in all of our lives, and probably quite often for many of us, we'll get into an argument with someone else. It might be a good friend, an acquaintance, a parent or perhaps even the love of your life, but you might suddenly find that you can't get past the same cycles of disagreement on the same issues, over and over again.

Soon enough, you realize you've spent the whole morning, day or even week in a state of anguish and without having made any progress toward a resolution.

So how can we get past the insidious entrapments of interpersonal conflicts, and what do our self-perception and identities have to do with it? Well, as you'll find out in these blinks, the way we perceive ourselves actually has a key influence on our approach to conflict.

In these blinks, you'll learn

  * what constitutes your identity;

  * how your identity can act as a primary trigger of conflicts; and

  * what vertigo has to do with arguments.

### 2. Quarreling is about more than reason and emotion – identity also plays a central role. 

We've all had an argument at some point. But to resolve conflicts, we need to understand the complex dynamics at play.

Traditionally, it's believed that two main factors contribute to conflict, namely rationality and emotion.

We generally begin an argument by appealing to rationality. The side of our personalities that appeals to rationality and rational decision making is known as the _homo economicus_, which, naturally enough, is a concept taken from the field of economics. This concept posits that we act as individuals. We try to maximise our own gains, as well as those which are mutually beneficial, especially when it comes to money or time.

The other factor at play is emotion. Emotions, such as fear, anger or trust, are often irrational but can nevertheless dominate our perception. A good shorthand for this part of our personality is _homo emoticus_.

However, beyond the two factors of rationality and emotions that are generally cited as the core of conflict, there's a third factor to consider that is often overlooked: identity.

Identities are formed by our self-conception and by our search for meaning in existence. We're as much _homo identicus_ as we are _homo economicus_ or _homo emoticus._

Identity is also the foundation for _tribes_, which are defined as groups united by similar ideas, values or religious beliefs.

But let's make this idea more concrete by looking at an experiment, conducted by the author, to show the power of tribe identity in conflicts.

A total of 45 participants were randomly divided into six groups, with each group then being asked a series of questions on a range of themes. What were their opinions on capital punishment, or what did they consider the most important values of each tribe?

After 50 minutes of discussion, the groups had to choose just one tribe out of the six to represent all of them. If they failed to do so, the earth would supposedly be destroyed.

The author repeated this experiment around the world with many different groups, and despite the high imaginary stakes, the earth was only "saved" a handful of times!

It's clear that participants became so wrapped up in their new identities that they preferred to destroy the planet rather than take on the identities of another group. New tribal bonds formed so strongly and so quickly that the conflict simply couldn't be resolved.

### 3. Core and relational identities are critical to understanding conflict. 

If you've taken a moment to consider it, you've probably already recognized the two constituent parts of your identity.

First, there's what's called _core identity_. These are the characteristics that make you who you are, which can be subdivided into five further elements.

These are _beliefs_, including morals; _rituals_, such as eating with your family; _allegiances_, like patriotism; _values_, including justice; and _emotionally meaningful experiences_, such as the birth of a child.

And it's not just individuals that have core identities. Social groups, companies or states can have them too, which is precisely how personalities, brand values and constitutions all take shape, respectively.

While your core identity isn't necessarily fixed in the sense that you could, for instance, adopt new and different values, in the end your subjectivity and core essence would remain unchanged, and you would still remain yourself as an individual.

By way of contrast, let's turn now to _relational identity_. This defines you according to your relationship to other persons or groups.

Remember the tribe experiment from the previous blink? The groups had three rounds of negotiation to save the world. When they began, the different tribes worked as colleagues toward their joint goal. However, as negotiations continued, tensions arose and their willingness to cooperate disintegrated.

Why was this? The author thought it happened because they experienced rejection. In light of this rebuff, their relational identity _–_ which fluctuates more easily than _core identity_ — shifted. Tensions arose because the groups realized how other groups perceived them. Relationships between groups were tested and became strained, but core identity had nothing to do with it.

Core identity is built on finding meaning within ourselves; in contrast, relational identity is formed from finding meaning through coexistence and association.

The existence of relational identity explains why maintaining an atmosphere of cooperation is so important when it comes to achieving success in negotiations or conflicts.

### 4. When our identity is threatened, conflict can be caused by the “Tribes Effect.” 

We've all been there, arguing with someone and feeling utterly sure that our perspective is the correct one. When it seems that nothing can distract us from our absolute certainty, there's likely one specific factor causing us to think this way; it's called the _Tribes Effect_.

The Tribes Effect is a mindset that pits your identity against your opponent's, leading to a case of "you versus me" or "us versus them."

In the end, this is an evolutionarily sound principle, in that it protects groups and bloodlines from outsiders, but it can likewise be scaled down to a two-person conflict.

The Tribes Effect often kicks in to protect your identity from attack. It's wise to learn how to recognize when this occurs, as the Tribes Effect tends to generate a self-righteous, adversarial and closed mind-set.

And this is also something you can feel! You'll think that you're in the right, unable to see what you have in common with your adversary; you'll only see the differences and won't listen to arguments.

So what triggers the Tribes Effect? Simple — it takes hold whenever our identity is threatened. Even seemingly minor differences between people can spark the Tribes Effect into action.

To illustrate this, the author carried out an exercise in which people were told to argue the comparative importance of "humanitarianism" and "compassion."

While outsiders might consider the distinction to be trifling, and perhaps not even worth debating, this made the results of the experiment all the more interesting.

The author found that once the Tribes Effect took hold, minor differences led to major conflicts.

The participants felt their identity to be threatened if they gave way on the beliefs they had invested in. Any inch of concession or modification of belief was perceived as a defeat.

### 5. Vertigo is a fundamental aspect of the tribal mind, so stay aware of it. 

Have you ever been deep in an argument with someone only to notice that half an hour has flown by, even though it only felt like five minutes or so?

This effect is known as _vertigo_.

Vertigo is like a trap. When you're ensnared by it, it seems as if your conflict is the only thing that exists; it becomes all-consuming.

The symptoms of vertigo are difficult to address. You'll forget your surroundings, stop being able to self-reflect properly and will only focus on negative ideas.

Let's consider a hypothetical example of a husband shopping with his wife. She sees a bedspread she wants, but he doesn't like it. What begins as a minor dispute spirals out of control, and before long they are questioning why they ever got married!

Suddenly, they notice that 20 minutes have passed. They've been completely sucked into vertigo and have blotted out everything else around them. They have entered their own world and their abilities to self-reflect have been lost in a cloud of negativity.

So how do you avoid vertigo? The first step is to be aware of its presence, to begin with. Ask yourself these questions if you find yourself in the midst of an argument with someone.

First, has the conflict consumed you? If your mind is on it and nothing else, you're probably in a state of vertigo.

Second, do you see your sparring partner as only an enemy? If you think of the other person as an adversary rather than as a person with differing opinions, vertigo is likely the cause.

If this is the case, you should take a deep breath and slow down while trying to moderate your perspective. This will help you stop vertigo from taking over; while it's an instinctive tribal impulse, it can be overcome.

Another signifier of tribal conflict are _taboos_, which we'll learn about in the next blink.

### 6. Taboos can lead to conflict, so learn how to approach tricky topics. 

Pause and have a quick think about your friendships and relationships. Are there any things you just don't like to discuss with people? There almost certainly are, and it's likely they would lead to conflict if you did.

These social transgressions are called taboos. Certain groups define their taboos according to feelings, ideas or urges that they deem unacceptable to feel, think or act upon. Consider premarital sex, a prime example of a religious taboo.

In essence, taboos exist to protect individuals from what are seen as offensive values by a certain community.

Nonetheless, taboos are relative rather than absolute, and aren't universally offensive. A given society might punish and ostracize those who transgress, but remember, too, that without the threat of punitive measures, a taboo might not seem like such a misdemeanor after all.

Taboos, however, are indeed part of the tribal mind, and thus part of our identity. Therefore, conflicts can arise between tribes that differ in opinion as to whether a certain action is a taboo or not.

However, there's a way to confront taboos if they are a cause of conflict.

First of all, you must recognize the taboo, then create a safe space to discuss it and finally agree on whether to accept or disregard it.

Accepting a taboo is one way of resolving conflict. Even so, it's important to note that though acceptance may lead to harmony in the short term, acceptance may not last forever; things can always change.

Perhaps you live in a country that has a taboo against drinking excessively. If a good friend of yours has a drinking problem, you might be able to accept it at first — but over time your attitude may shift in order to reflect cultural norms.

Disregarding a taboo can also be difficult because doing so can be perceived as a very direct action. But it's not impossible.

Consider Nelson Mandela. He addressed taboos that forbade white and black people from interacting and mixing in South Africa, and it was through bold activism that he set in motion the beginning of the end of apartheid.

In summary, by confronting taboos, we can forge better relationships.

### 7. Identifying the mythos of identity is certain to help with reconciliation. 

Think back to the last time you were quarreling with someone. There's a good chance you saw yourself as the victim in the situation, and this attitude is caused by what's known as the _mythos of identity._

The mythos of identity refers to the self-written narrative that fixes one's identity in relation to someone else.

We each have a mythos, and the most common constellation is to think of ourselves as a victim and our adversary as the villain.

The author examined this phenomenon in a game he set up. Participants were divided into two classes, one representing the economic elite, the other the lower-income class. The former group was given more money and resources.

After three rounds of free trading designed to maximize individual wealth, the elites were allowed to set new rules.

The elites imagined themselves as saviors of the poorer Lower classes and implemented rules designed to benefit the lower classes — but they didn't actually ask the Lower classes what they wanted! Conversely, the lower classes presumed the elites were out to exploit them.

This showed that both groups were attached to their respective mythos of identity, which just compounded the in-game conflict.

But if we set out to understand others' mythos, we'll be better equipped to resolve conflicts.

There's a three-step method that can resolve conflicts where the mythos of identity is involved. It's called _creative introspection_.

First, establish a _brave space_. Here, people can freely discuss sensitive issues without judgment.

Second, identify each other's mythos. This way, you'll understand why the other person is behaving as he is. Perhaps an arrogant colleague is overcompensating because he was bullied as a child; in the end, he's simply used to being a victim.

Finally, revise your own mythos. Let's use the imaginary arrogant colleague as an example. Perhaps you could support and encourage his work, or maybe you could give him more responsibilities, which will allow him to revise his mythos. From there, you can start making victims into leaders.

### 8. A three-step process can help you work through emotional pain. 

Have you ever wanted to exact revenge on someone? It's an understandable desire, but it's not worth it. In the end, it won't solve the deeper pain, and there are more productive ways to deal with anguish.

First of all, it's important to acknowledge emotional pain — not just the hurt you're feeling, but also the pain felt by your adversary.

Emotional pain has two aspects.

_Raw pain_ is that gut reaction you feel in the moment, and which you can identify by examining the immediate sensation of your emotions and body. For instance, do you have a knot in your stomach or tight shoulders?

The other type is s _uffering_, which is the feelings you receive when you try and understand the raw pain. Suffering can manifest itself as the desire for revenge or anger that bad things keep happening to you.

You can overcome both sentiments if you understand what provokes them. For instance, if the thought of seeing your boss might be bringing you out in cold sweats, maybe this is because he dislikes your ideas. Perhaps this is indicating that you'd like more praise for your work than you first thought.

The second stage is to mourn the loss you've suffered.

There's no way around it — all conflict entails loss of some sort. A couple in the midst of a divorce is losing the possibility of a future together, while warring armies must each mourn their lost comrades and friends.

It's one thing to understand loss as an intellectual exercise, but it's quite another to handle it on an emotional level.

An important step that will allow you to mourn is to comprehend and appreciate your loss. For some, this is best done verbally, through conversations or simply by asking yourself what makes the loss so traumatic. For others, rituals, such as religious ones, are helpful.

The final step is to contemplate forgiveness.

Forgiveness is the most difficult stage of all, but once you've forgiven, you'll no longer see yourself as a victim and the urge for revenge will dissipate.

In every relationship you have, it's essential to temper emotional pain. Reconciliation can only be achieved by shifting perspectives brought about by confronting this pain.

### 9. Achieve reconciliation by reconfiguring your relationship. 

There may be times when you think a conflict seemingly has no solution, but this is probably because you haven't yet shifted your perspective and, in doing so, have not reconfigured your relationship to your adversary.

The first step in this process is to identify the identity under threat. Now is the time to take on the mythos of identity we learned about earlier.

Consider a real-life situation: Linda and Josh are a couple. Linda, who's Protestant, wanted a Christmas tree; Josh, however, was averse to the idea because he's Jewish.

In this case, the Christmas tree is actually a symbol for something deeper. When Linda was young, her mother died and, from then on, her father would always put up a tree for her. For Linda, it represented her father's love; meanwhile, for Josh, it felt like a betrayal of his family values.

The second step is to use the _SAS system_, which is the method used for envisaging how identities might coexist harmoniously. It comprises three stages:

First, _separate_ the identities. For our couple, this could mean dividing the house up. One part could be set aside for Christmas festivities.

Second, _assimilate_. This means embracing a part of your adversary's identity. Linda could, for instance, adapt to Josh's beliefs or vice versa.

The third and final stage of the SAS system is to _synthesiz_ e, which allows both core identities to coexist. The couple can still have a tree; for Linda, it could be a Christmas tree, while Josh could think of it as a Hanukkah decoration.

Returning to the broader process of reconfiguring your relationship to your adversary, the final step is to deliberate which scenario or combination of scenarios might work best.

Use the SAS system to evaluate possible options to overcome the conflict, which might range from unrealistic to realistic. Then, decide together what works best.

In the end, Linda and Josh decided against a Christmas tree, and instead decided to celebrate Christmas at Linda's father's house. This way, they were able to respect all their traditions, rituals and beliefs.

It can take time to reconfigure a relationship, but it's always worth the effort if you want to succeed when resolving conflicts.

### 10. Final summary 

The key message in this book:

**To resolve conflicts, it's essential to be aware of the relationship between adversaries as well as how they see themselves. By being critical of yourself as well as of others, remaining aware of social taboos and examining the personalized narrative that defines your identity, you will be well equipped to resolve many of the conflicts you confront. In the end, no conflict is non-negotiable.**

**Actionable Advice**

The next time you find yourself feeling personally offended in an exchange with someone, consider taking a step back and examining which part of your identity is under threat. Is it the part that considers yourself to be a proud, progressive liberal, or a nurturing, caring parent? If so, you might be experiencing an assault on the aspects of your identity that seem most sacred to you. Recognizing that you will not always execute every role perfectly might help dissipate the anger and offense that you feel.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Nonviolent Communication_** **by Marshall B. Rosenberg, PhD**

These blinks introduce the principles of Nonviolent Communication (NVC) as a compassionate way of being with ourselves and others. Through simple techniques, you can learn how to consciously change your language and thinking to forge better quality relationships with others.

**This is a Blinkist staff pick**

_"Nonviolent communication is not only a tool to learn how to achieve a more peaceful communication style, but also uncovers why we misunderstand one another every now and then. My favourite blinks!"_

– Robyn, German Editorial Production Manager at Blinkist
---

### Daniel Shapiro

Daniel Shapiro founded the Harvard International Negotiation Program and is an associate professor of psychology at Harvard Medical School. He is also a consultant for Fortune 500 companies and various public institutions, and has created several conflict resolution initiatives in Asia, Europe and the Middle East.

