---
id: 59ec88adb238e1000645542f
slug: neuro-linguistic-programming-for-dummies-en
published_date: 2017-10-27T00:00:00.000+00:00
author: Romilla Ready, Kate Burton
title: Neuro-linguistic Programming for Dummies
subtitle: None
main_color: FDF333
text_color: 636014
---

# Neuro-linguistic Programming for Dummies

_None_

**Romilla Ready, Kate Burton**

_Neuro-linguistic Programming for Dummies_ (2015) is an introduction to the basic principles behind NLP. If you've ever struggled to say what's on your mind, NLP may be of use to you. Discover the human traits that can keep us from understanding each other and the helpful methods that can lead to clear and effective communication.

---
### 1. What’s in it for me? Develop intelligent communication skills. 

Talking is easy; communicating is very hard. Successfully transferring, via language, what is in your head into the head of someone else is remarkably difficult. Indeed, as anyone who has ever tried to do this can tell you, effective communication is really the avoidance of _mis_ communication. When attempting to communicate, people often either fail to listen, hearing only what they're prepared to hear, or fail to consider how they might sound to their interlocutor, making it much harder for their message to be received.

Thankfully, neuro-linguistic programming (NLP) can help. Starting off with recognizing that each person has a unique and different perception of the world, NLP offers tools that help you connect with and understand the worldview of others.

In these blinks, you will learn

  * how a lemon changes depending on how it's perceived;

  * what questions you can ask to better understand what other people experience; and

  * how to establish strong connections and bonds with other people.

### 2. Neuro-linguistic programming recognizes that everyone is different and aims to improve our modes of communication. 

So what exactly is _neuro-linguistic programming_?

NLP, as it's commonly referred to, is the study of how we understand and experience the world around us.

Let's start by looking at what everyone has in common — the five senses that we use to experience the world around us. That world includes our own body, the objects and people around us, as well as everything else that can be touched, heard, seen, tasted and smelled.

This sensory information travels through our neurological system to the brain. Our perspective is unique to us because what we experience gets filtered by a number of things, including our personal values, social and cultural backgrounds, memories of past experiences and so on.

A person's unique set of filters constitutes what's known as that person's _internal representation_, or IR.

Perhaps you've had this experience, or something similar to it: You and a friend go to a party, at which you talk to the same people, listen to the same music and eat the same food. Afterward, this friend starts talking about how unenjoyable the party was, though you had a fantastic time.

Well, this is a classic example of a difference in IR. Your past experiences and personal proclivities primed you to find the evening enjoyable; your friend's, on the other hand, primed her to dislike the food and be bored by the conversation.

An awareness that everyone lives according to their own set of filters is central to NLP. And with this understanding comes the belief that communication and understanding can be improved.

So the aim of NLP is to help us become better prepared to communicate with, as well as more flexible in our understanding of, others.

Sounds good, right? In the blinks ahead, we'll be looking at some of the specific ways in which NLP attempts to improve communication, starting with how to establish good rapport.

### 3. Nonverbal communication, along with matching and mirroring, is key to establishing good rapport. 

If you've ever had a conversation with someone where words and ideas flowed easily and each of you was full of respect for the other, then you know what it means to have established _rapport_.

Good rapport makes it much easier to communicate, so it's no wonder that researchers are keen to better understand this desirable quality.

What researchers do know is that nonverbal communication is essential to good rapport.

The importance of nonverbal communication has been understood since the 1960s, when UCLA professor Albert Mehrabian identified the three elements of a face-to-face conversation: _words_, _tone of voice_ and _nonverbal communication_, such as hand gestures and facial expressions.

Professor Mehrabian believed that nonverbal communication made up 55 percent of an effective, impactful conversation. He then listed tone of voice as making up 38 percent and the actual words of the conversation as having only 7 percent of the overall impact!

Clearly, both verbal and nonverbal communication contribute to effective rapport and the ability to have a sincere and engrossing dialogue.

So if you have a job interview coming up, don't keep your hands on your lap or in your pockets the entire time, or wave them about distractingly. While explaining your qualifications in a clear and confident voice, use calm gestures that punctuate your message and keep the conversation lively.

Another way of establishing rapport is called _matching and mirroring_, which is all about finding the rhythm of the conversation and being in sync with the person you're talking to.

This isn't just mimicking a person's gestures; it's picking up on how they're thinking and talking and deliberately matching their energy, thereby creating a naturally syncopated rhythm for the conversation.

Pay close attention to how the other person is breathing and moving, and try to match their energy level, as well as the speed and tone of their voice. When all of these things are in sync, you're sure to achieve good rapport and conduct an effective conversation.

> _"Rapport is like money: you realise that you have a problem only when you don't have enough of it."_

### 4. The mind can form powerful links to positive and negative experiences, which can be used to your advantage. 

It's never wise to underestimate the power of the human mind. And one thing the mind is especially good at is creating links, or anchors, to moments and experiences in life that were particularly impactful.

Sometimes this is a good thing, but these anchors can also be a nuisance if they weigh us down with unwanted emotions and reactions.

You probably have a few smells that trigger happy memories for you, like a perfume or cologne that a loved one wore and that, when you smell it, brings you back to a fun date or a first kiss. In this case, the smell is the anchor firmly set in your mind.

But an anchor can also link you to a bad memory. You may have had an old flame that liked a particular brand of whiskey, and now, whenever you catch a whiff of that whiskey, it takes you back to a night when you had a drunken argument that resulted in the relationship falling apart.

Well, NLP offers a three-step technique to create new, positive anchors that can help you get through difficult situations.

The first step is to think of the emotional state you want to be in. Happy, energetic, calm — pick your ideal state and keep it in mind.

The second step is to think of a moment from your past when you were in this state. Perhaps you felt it when you passed an important test, or when your first child was born. Whatever it may be, hold on to that moment.

The third and final step is to think of the sound or movement that you want to associate with this emotional state. It could be anything from a certain Elvis Presley song to a certain hand gesture, like a thumbs-up.

Once you have your anchor in place, you can put it to use at any time. If it's a happy emotion, you can use it whenever you're feeling down in the dumps. Just put on that Elvis tune, or give yourself a thumbs-up, and bring yourself back to the positive experience that it's anchored to.

### 5. The logical-levels model can help you discover the right solutions to problems that may arise. 

Life comprises a wide variety of situations that we need to navigate. And some are more challenging than others.

To help make sense of it all, NLP has identified five _logical levels_ to help understand where in your life a challenge stems from: _environment_ ; _behavior_ ; _capabilities and skills_ ; _beliefs and values_ ; and _identity_.

So let's say you're desperate to move to a new cubicle that isn't located right next to the men's room. This would fall in the _environment_ category. This is something that doesn't require an internal change on your part; it could be changed relatively easily by talking to a manager and getting approval to relocate.

Now, let's say you're in medical school and suddenly realize that you don't actually want to be a doctor. This reflects a whole different situation, one that has to do with your beliefs and values; it's a more complicated, internal dilemma.

No matter the situation, there's a three-step _logical levels model_ that can help you get to the bottom of things.

The first step is, naturally, to recognize that there _is_ a problem.

Sometimes things happen gradually, such as growing distant in your relationship with a partner, so before it can be dealt with you must first be in touch with the negative feeling this has brought up.

The second step is asking the right questions to determine the cause of this problem, and what logical level it represents.

Is it an environment problem — maybe it started when you moved to a new neighborhood? Or is it a behavior problem — maybe there's a third party involved? Or maybe it's a values problem, and you and your partner want different things out of life.

Finally, once you've pinned down the kind of change that needs to be made, you can take the appropriate action.

Perhaps the problem was behavioral, and spending too much time at work was driving you and your partner apart. One solution would be to put a limit on the evenings spent at the office and to make time for nights out together at least twice a week.

### 6. The meta-model helps you ask the right questions to get more meaning from your conversations. 

There's a good chance that you've struggled to find the right words to express yourself at one time or another. But according to NLP, there's more to expressing ourselves than just words.

There are actually three processes going on when we describe an experience. These are _deletion_, _generalization_ and _distortion_.

Deletion represents the fact that we're usually leaving certain things out when we're explaining ourselves. When, on Monday morning, your coworker talks about his weekend, he might shrug and say he just went to see a movie that was half-decent. Clearly, he did much more than _just_ that.

Generalization refers to how we're often vague or unspecific in details and tend to extrapolate the outcome of one situation and apply them to another. A friend might explain that she performed poorly on her last test and that, therefore, she feels like a complete failure in general. This is obviously a rather wild extrapolation.

Distortion means misinterpreting events around you. For example, if your boss doesn't greet you in the hallway and you immediately conclude that she hates you, it's clearly a distortion. Your boss was probably just preoccupied.

By being aware that these three processes color the way everyone — including yourself — describes experiences, you'll be much better equipped to understand and communicate with others. And a good way to clarify the descriptions of others is by asking the right questions.

Regarding deletion, get your coworker to be more specific by asking what else he did during the weekend and with whom.

When it comes to generalization, ask questions that challenge the person's perspective. This way, your test-failing friend will see that her tiny failure doesn't mean that much in the grand scheme of things.

And finally, to clarify distortion, you can open people up to a more realistic perspective by asking questions that expose their lack of genuine evidence, like "How do you know your boss hates you?"

Everyone is unique, but we're also all human beings with many of the same traits — traits that often interfere with clear communication. With the tools of neuro-linguistic programming, we can get closer to having effective, meaningful discussions.

> _"Words offer just a model, a symbol of your experience; they can never fully describe the whole picture."_

### 7. Final summary 

The key message in this book:

**Neuro-linguistic programming is all about the way we experience life and the world around us. With the right awareness and the proper tools we can make real progress in understanding what sets us apart and what unites us. Now, perhaps more than ever, we need to work at understanding each other and having productive discussions.**

Actionable advice:

**Tell stories to build rapport.**

In trying to establish rapport, you might benefit from the tools of good storytelling. People react differently to stories than they do to standard arguments., because a good story taps into memories and emotions. So if you're trying to win someone over, try telling them a story that illustrates your point. For instance, to get employees motivated, you could share a story of how your business began in a tiny dorm room and the battles you fought to grow it. This is the kind of tale that employees can connect with and be inspired by.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Ultimate Introduction to NLP_** **by Dr. Richard Bandler, Alessio Roberti and Owen Fitzpatrick**

_The Ultimate Introduction to NLP_ (2012) offers a fascinating primer of Neurolinguistic Programming _,_ or NLP, a novel approach to the ways your thoughts and language can "program" your emotions, behavior and communication. When you learn to master NLP, you'll connect better with the people around you, have a healthier outlook on the future and lead a happier life, too.
---

### Romilla Ready, Kate Burton

Romilla Ready is the director of Ready Solutions, Ltd. and a passionate speaker and trainer who is committed to helping people forge better relationships. She strives to help people become more resilient and impactful in both their personal and professional lives.

Kate Burton is an expert coach of neuro-linguistic programming. Her popular workshops have helped people around the world lead better and more productive lives.

© Romilla Ready, Kate Burton: Neuro-linguistic Programming for Dummies copyright 2004, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

