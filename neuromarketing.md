---
id: 55e616367a20120009000029
slug: neuromarketing-en
published_date: 2015-09-04T00:00:00.000+00:00
author: Patrick Renvoisé & Christophe Morin
title: Neuromarketing
subtitle: Understanding the "Buy Buttons" in Your Customer's Brain
main_color: EF4830
text_color: BD3926
---

# Neuromarketing

_Understanding the "Buy Buttons" in Your Customer's Brain_

**Patrick Renvoisé & Christophe Morin**

By drawing from brain research and innovative marketing techniques, _Neuromarketing_ (2002) offers insights into how we make buying decisions. Understanding the brain's ancient decision-making processes will equip you with the tools necessary to close deals and motivate people.

---
### 1. What’s in it for me? Use the workings of the brain to sell more. 

Have you ever seen _Mad Men?_ Probably you have. So you know that, in the past, advertising and marketing was a largely hit-and-miss affair. Ad men and women would spend hours trying to come up with attention-grabbing campaigns. Sometimes they'd work; sometimes they didn't. 

Today, however, things are completely different. Thanks to huge advances in neuroscience, we now have a much deeper understanding of how people's brains work. We know why people react to stimuli, such as adverts or marketing pitches, and for the marketing profession, such information is pure gold. Don Draper or Peggy would have killed for such knowledge.

Fortunately, you needn't get homicidal to unlock the secrets of the brain; you can simply read these blinks, which give a concise introduction to the use of neuromarketing.

In these blinks, you'll discover 

  * why you should try to have something in common with your prospect;

  * why you need to feel your customer's pain; and

  * how to use grabbers to nab people's attention.

### 2. If marketers want to grab our attention, they need to target the old brain. 

When you decide to buy coffee on your way to work in the morning, do you ever consider _why_ you made this choice? Did you rationally compare all prices of nearby competing cafes in order to get the best coffee for money? Or did you just feel like getting a cappuccino?

No matter which strategy you followed, when making that choice of where to get your coffee, you were using what is known as your _old brain_. 

The old brain is your decision-making center; it assesses the information coming from the other two parts of the brain, the _new_ part and the _middle_ part.

The new part of the brain provides rational insights, like, "This latte provides the best value for my money." The _middle_ part handles emotions and "gut feelings."

Marketers need to appeal to the old brain if they want to increase sales. So what can they do to ensure they reach it?

One way _not_ to reach it is language. Humankind didn't develop spoken words until a mere 40,000 years ago. Written language is even younger, developed around 10,000 years ago. The old brain, in contrast, is 450 _million_ years old — and rational language can't adequately capture its attention. 

You'll have to be more clever than that to reach the old brain.

For example, we know that the old brain is self-centered, caring only about its own prosperity and survival. Therefore, good marketers always concentrate on how their products will improve the lives of those who purchase them. 

The old brain is also lazy. It only focuses on the beginning and the end of something, rather than the middle. So if you want to sell, make sure that the beginning and end of your ad are bold and attention-grabbing. This way, people will actually remember them. 

Appealing to the old brain is crucial, and the following blinks detail different methods for attracting its attention.

### 3. With the right preparation, you can prime your material such that it appeals to the old brain. 

In order to optimize the message you plan on delivering to the old brain, you'll need to do some prep work. Here are three steps that'll help you prepare most effectively.

Start by _diagnosing the pain_. Be attentive and listen to your customer, and they will give you insight into their _pain_ — that is, the reason they desperately need your product.

Say you're trying to sell an industrial drill. What might your customers' pain be? Well, they probably want to bore a hole in a dense surface, but lack the equipment to do so.

Next, _differentiate your claims_. Ask yourself: What makes me and my product special, and how will it help solve my customers' pain? Try to come up with an answer that is as concrete as possible.

Let's go back to your drill company: Maybe your unique differentiator is that your customers believe you have the most reliable drills, the best customer care or the quickest delivery of replacement parts.

Finally, _demonstrate the gain_. Your goal here is to definitively demonstrate how your product answers potential customers' pain and adds something to their lives.

Don't be vague! Here, you'll want to provide hard evidence. Customer stories, testimonials, short demonstrations, prototypes, data or even your personal vision make your product more real and palpable.

For example, tell your customers about that time the city of Indianapolis was able to finish their water pump project two weeks ahead of schedule because their operation went so smoothly — all thanks to that drill of yours!

Once you have these three areas prepared, it's time to move on to the next step.

> _"Your Selling Probability = Pain x Claim x Gain x (Old Brain) 3"_

### 4. Build and deliver your message in a way that appeals to the old brain. 

Having primed your communication campaign with the preparatory steps outlined in the previous blink, it's now time to focus on some practical ways to structure your message — ways that intrigue and entice the old brain.

First, make use of the following _building blocks_ when constructing your message:

Use _big pictures_ to help potential customers visualize what your solution can do for them. These images are most effective when they demonstrate a contrast, such as life before and after adopting your solution.

To visualize this, imagine you are selling mattresses. On the left side of your ad, you could show a tired, unkempt man struggling at work because of sleep deprivation; on the right, this same man is happily and comfortably snuggled up next to his attractive partner on one of your company's mattresses.

Another such building block is your _proofs of gain_, that is, the evidence that your customer's life will improve upon purchasing your product. One way to clearly demonstrate this proof is through statistics. It's hard to misinterpret something like "99 percent of our customers sleep better after switching to our mattresses."

Second, fine tune your message by adding _impact boosters_ — small added details based on the three styles of learning: _visual_ (seeing) _, auditory (_ hearing) and _kinesthetic_ (doing) _._ They're all important, so make sure you appeal to all of them in your marketing campaign.

For example, when selling your mattresses, the visual element described above should be complemented by something auditory, perhaps someone telling the story of how the bed made them feel rejuvenated. Then add something kinesthetic by letting your audience participate — telling their own stories, for instance, or even trying out the bed themselves!

### 5. No matter what you are presenting, you should always try to appeal to the old brain. 

Though there are many forms of marketing and advertising, one of the most common is the presentation, where you convince people — face-to-face — to consider, and hopefully purchase, your product. As with other forms of marketing, it's absolutely essential that you concentrate on communicating to the old brain when making a presentation. 

The secret to a great presentation lies in immediately grabbing the audience's attention. To make a great impression, you need _grabbers_, techniques that nab your audience's attention. Here are a few that you can try:

_Mini-dramas_. With this grabber, you describe your customer's typical day, focusing on their deepest pain (which your solution will cure). Next, you show the contrast between life before and life after implementing your solution. Through this combination of pain and emotion, mini-dramas are easily memorable.

Imagine, for example, that you want to sell "toughbooks," nearly unbreakable laptops. Your mini-drama could focus on the devastation that your customer feels as their laptop slips from their hands and falls to the ground. Luckily, right before their laptop smashes to the pavement, they remember that there's no reason to worry — their toughbook is unbreakable!

Another type of grabber is _rhetorical questions_ — questions that aren't meant to be answered, but to get your audience thinking about something specific or to illustrate a strong point. 

Ask hypothetical questions about the audience, questions that draw comparisons or contrasts; the old brain can't help but immediately fixate on finding an answer.

_Props_. Research shows that your audience will more easily remember your presentation when you demonstrate the value of your solution by using tools and props. If you're delivering a presentation aimed at winning a contract for your security company, for example, you could use your locks as a prop displaying your security competence.

By using these grabbers, you can hold the attention of an audience member's old brain long enough to successfully deliver your message.

### 6. Close any deal by handling customer objections with confidence. 

Have you ever had doubts before signing the paperwork for, say, a new car? Doubt is human, and thus a completely natural part of any selling process. In fact, as a salesperson, it's a good sign when your customer expresses doubt: it means they're actually considering the purchase.

But you can't leave doubt unchecked, so you'll need strategies to handle these last-minute objections.

The very first thing you need to do is differentiate between misunderstandings and valid objections. It's entirely possible that your presentation wasn't as fluid as you thought it was, which could lead to misunderstandings that give rise to objections. Clearing up such objections, however, is as easy as following these four steps:

First, _rephrase the objection_ in order to ensure that you really understand what your customer is worried about.

Second, _step into the objection_. Physically move toward the objector, signaling that you aren't afraid of their objection.

Third, _listen to your customer_. Give them time to express their perspective.

And finally, _prove your point_. Tell them a story, offer a prototype or do anything else that backs up your claims while also addressing their old brain.

Sometimes your customer will make an objection that has nothing to do with misunderstanding. For example, they may simply think that the price is too high. To manage these objections, start with the steps you just learned and then add two more:

_Express your own opinion_. Your customer made a valid objection, one that you can't really disprove. However, you can offer your own opinion: "I think our prices are actually pretty competitive." 

Next, _highlight a positive side of the objection_. Connect to the old brain via stories, demos or customer testimonials and create a link to the customer's objection. In this case, you could link "high price" with high quality by telling the story of your wife's heart surgery. The surgery was performed at the best hospital in town and cost a fortune, but in the end it was worth it. Your product is no different.

> _"Your body language does speak louder than your words."_

### 7. If you want people to trust you, you have to be credible. 

Would you immediately trust someone that you met on the street? Of course not. You need a reason to trust them. Sales presentations are no different: you need _credibility_ if you want to earn your customers' trust and close the deal. 

Credibility isn't something that can be faked, and people will know if you are being disingenuous. Luckily, even if you lack credibility, there are things you can do to make yourself more credible.

It all starts with the _passion_ you have for your product or service, and with your _integrity_. If you are enthusiastic and genuine about something, people will believe you.

Second, understand that we're more attracted to _similar_ people than to those who are completely different from us. Indeed, you're far more likely to connect with someone who dresses like you or has a similar accent to yours than with a complete stranger. Do a little research on your audience before you make your presentation so that you can highlight your similarities.

Next, focus on honing your _expressiveness_ — the way you speak and carry yourself. Would you feel more inclined to trust someone who mumbles or someone who speaks confidently?

You should also flex your _creativity_ by being ready to change your approach to fit your audience. One of the easiest ways to do this is to switch the colors in your marketing to generate proximity with a particular audience.

Blue, for example, is thought to represent trust and authority. Blue would be an appropriate color when presenting in front of a bunch of old suits. That might not be the case, however, when delivering a pitch at a tech-leader conference for young females. Colors like orange or pink would be better suited to such an audience.

Finally, be _fearless_. People trust those who are self-confident. Approach your presentation with a contagious enthusiasm. You'll be far more welcome this way than if you come across as too smart to engage with your listeners.

### 8. Use vivid language and emotional stories to make your message memorable and sticky. 

Think of your marketing message like a cupcake: the real substance of your message (like that spongy cupcake) is enticing in and of itself, but if you really want to grab someone's attention, you'll need to add some sprinkles. It's easy to do this in your sales presentations.

One way is to adjust your language according to the old brain's preferences. For example, use the word "you" as often as possible, as the old brain likes to be talked to directly.

Another trick is using sharp contrasts. These are much easier for the old brain to process and remember than vague comparisons. Before/after, with/without your solution, you/your competitors, now/later — the possibilities are virtually limitless. Just be creative!

Whatever you do, keep it short and simple. Remember, you're trying to address the survival center of the brain. Its attention span is short, for good and obvious reasons.

But it's not all just about your words. There are also other tools you can use to make your message more easily digestible:

Try to elicit _emotions_, as they create a direct channel to the old brain and make events more memorable. Think about those times when you were _really_ happy. Do you imagine a complete picture full of detail? Of course! If you make customers happy with your advertising, they'll be more likely to remember you, too.

The old brain also loves _stories_. And, usefully, it can't differentiate between a well-told story and reality.

A good story has details that appeal to the senses. It invites listeners to feel the warmth of the sun, the rain on their face and so on. And remember to be explicit about why the story matters to them, otherwise the selfish old brain will just ignore you.

Finally, a good story has a good punch line. Your story should end on something funny, memorable or engaging. This way, it will stick with your audience.

> _"Telling does not equal selling."_

### 9. Communicating with the old brain can also help you sell yourself in job interviews. 

The old brain developed hundreds of millions of years ago, long before advertising existed — but not before decision-making existed. You can appeal to the old brain any time you need to convince someone of something, like during a job interview.

Ultimately, a job interview is a sales situation in which you sell yourself to the potential customers, the interviewers.

Here are four ways to optimize your communication with the old brain:

_Diagnose the pain_. Research as much as you can about the relevant position, the person you're replacing and the exact kind of pain the company wants to ease by filling this position. If your predecessor left after only six months, the fear of losing their replacement just as quickly might be part of the pain. If this is the case, be sure to mention that you're looking for something long-term.

_Differentiate your claims_. Listen to what they are looking for; pay attention to statements about you and the competition, and then form your claims. What makes _you_ the cure for their pain? Why you and not competitors with similar backgrounds?

Moreover, focus on what you can do for them. Don't just tell them that you were a successful project manager. Tell them that your project management skills will directly benefit their team.

_Demonstrate the gain_. Be as specific as possible about how their company will benefit from hiring you. You could even bring documentation, such as an overview of contributions to your last project, that proves your points.

_Finally, deliver to the old brain_. Use everything you've learned in these blinks. From a firm hand-shake and strong eye contact to moving toward the interviewers when they object to your claims, always appeal to the old brain. If you can, demonstrate that you're the perfect match with a good story or even a mini-drama.

With these new tools in your toolbelt, you can now use your knowledge of the old brain whenever you find yourself needing to persuade people!

### 10. Final summary 

The key message in this book is:

**People don't behave completely rationally, so it's not rational argument that will win you the sale. Rather, you have to appeal to people's ancient decision-maker, the old brain. By knowing how to communicate with the old brain, you'll be that much closer to closing the deal.**

Actionable advice:

**Check for reading patterns if you do international business.**

The next time you want to implement a strong before/after contrast in your sales presentation, do some research on your target audience. Are they from a part of the world that reads left to right, or right to left? Or perhaps even top to bottom? Whatever their accustomed reading pattern is, it's your job to adapt your presentation accordingly.

**Suggested** **further** **reading:** ** _Hooked_** **by Nir Eyal**

_Hooked_ explains, through anecdotes and scientific studies, how and why we integrate certain products into our daily routines habits, and why such products are the Holy Grail for any consumer-oriented company. _Hooked_ gives concrete advice on how companies can make their products habit-forming, while simultaneously exploring the moral issues that entails.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Patrick Renvoisé & Christophe Morin

Patrick Renvoisé is co-founder and president of SalesBrain; during his career, he's closed deals worth more than $2 billion.

Christophe Morin is also a co-founder of SalesBrain, as well as a marketing expert. He is currently pursuing a Ph.D. in Media Psychology.

