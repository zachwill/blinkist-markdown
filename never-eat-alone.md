---
id: 52cd29303762370008050000
slug: never-eat-alone-en
published_date: 2014-01-23T09:00:00.000+00:00
author: Keith Ferrazzi
title: Never Eat Alone
subtitle: And Other Secrets to Success, One Relationship at a Time
main_color: F18B30
text_color: 8C511C
---

# Never Eat Alone

_And Other Secrets to Success, One Relationship at a Time_

**Keith Ferrazzi**

In _Never Eat Alone_, Keith Ferrazzi, a successful businessman and marketing expert, takes us through the secrets to successful networking. He focuses on building lasting relationships rather than merely exchanging business cards, which seems to be many people's idea of networking today. He summarizes his findings in a system of tried and tested methods.

---
### 1. Having a personal network is a prerequisite for a successful career. 

We are all dependent on other people to achieve our goals and realize our dreams.

Nobody can hope to be successful in the long term without thinking connectedly and gaining the support of others. Sooner or later, every lone wolf will reach his limits and his career will come to a standstill.

Trying to build a successful career without a strong network is like building a house on the sand. Your foundations will weaken over time, and in the end you may find yourself sinking.

But what is it exactly that makes a network so important?

It might sound like a truism, but personal contacts open doors. One classic study, outlined in the book _Getting a Job_, showed that among the 282 men surveyed, 56 percent had found their jobs through personal contacts, whereas only 19 percent had found theirs through job advertisements and 10 percent through applications of their own initiative.

It is precisely in times of economic downturn, with their high employee turnover and high job insecurity, that having a personal network is more important than ever. People who are good networkers rarely have any problems finding a job.

We are working in an ever-changing world. A person who is an assistant today might be the CEO tomorrow. If you are known by many people and are, importantly, liked by those people — for example, if you have shown yourself to be generous, friendly and helpful — then this will eventually pay off.

### 2. Anyone can learn the art of networking. 

Many people fear rejection, and this fear can prevent them from approaching others and hence building a supportive network.

Fear of rejection is familiar to all of us and is nothing to be ashamed of. Few of us are natural networkers with the courage to approach strangers to try to win them over.

Yet there are a few tricks that can help even the most shy among us take that first big step:

  * _Learn from the best_. Simply take notes on how an expert networker goes about approaching others, and let yourself be inspired by their methods.

  * _Keep studying_. Maintaining a good demeanor and developing artful rhetoric are just like any other skills in that they can be systematically learned; for example, by taking courses on communication and rhetoric.

And sooner or later, you simply have to take the plunge. Like with all things in life, the best way to learn is by practicing. It can be helpful, for example, to set yourself a target of getting to know one new person a week. Every little bit of experience and practice you gain will make building your network that much easier.

The fact that networking can be so easy on the one hand and so difficult on the other is perfectly demonstrated by the example of Keith Ferrazzi's father. As a laborer of humble means, he wanted something more for his son, so he approached the only person he knew who might be of help: the head of the firm. In other words, his boss's boss's boss's boss. The head of the firm was so impressed by his courage that he paid for Keith to attend the best school in the country.

### 3. Successful networking is based on generosity and loyalty. 

A good networker will never ask, "How can others help me?" Instead he will ask, "How can I help others?"

The basic thinking behind this is clear and simple: the more ready you are to help others, the more ready they will be to help you in return.

Generosity builds trust and mutual understanding. It cements and deepens our relationships with others, and in time we will reap double or even triple what we have sown.

We shouldn't see each relationship as a short-term investment and expect to have every favor returned instantly. For relationships are not a finite entity, like a cake that gets smaller and smaller with every slice someone takes. Relationships are more like muscles: they grow and strengthen the more you use them.

People who are neither generous nor helpful, who simply wish to rid themselves of their business cards and only ever think of how to work things to their own advantage, are doomed to go through life powerless. Their behavior pushes others away, and sooner or later they will find themselves alone. For who really wants to work with someone that selfish?

In order to move beyond a self-centered view, and to truly strengthen and deepen your relationships with the people in your network, you need to show constant loyalty to them. Loyalty can take many forms; for example, by offering emotional support when someone is going through a rough divorce, or by donating your time to help someone with problems that lie outside of their professional life. A half hour of your time here and there could potentially pay you back in a life-long relationship.

### 4. A good networker builds her network before she really has need of it. 

One of the myths surrounding networking is that you should start looking to others only when you need their help.

This is a big, though unfortunately common, mistake. It is like trying to buy safety vests after your ship has already started sinking.

Good networking follows exactly the opposite principle. You should approach people _before_ you need their help. Doing so allows you first to build trust and mutual understanding: two qualities which are crucial for a functional network. Nobody likes a networking nuisance — in other words, someone who is insincere, unscrupulous or gladly exploits others for their own advantage.

A good networker is very patient and builds her network one step at a time. She knows that to earn the trust of others, she really has to work for it bit by bit — she is, you could say, more of a marathon runner than a sprinter. Her reward comes not through speed but through stamina.

Bill Clinton lived by this rule long before he was president. As a 22-year-old student, he had already started his habit of sitting down every evening to write down the names of all the people he had met that day on cue cards. He was always friendly and approachable to others and showed genuine interest when talking to them. He ensured that he built relationships based on trust and mutual understanding, thereby creating his future network at the same time.

### 5. A good networker looks for relationship glues. 

Really getting to know other people is not about how much time you spend with them but rather about the way in which you spend time with them.

Friendships and close acquaintances are formed by spending quality time with each other. You should therefore pay great attention to _relationship glues_ ; in other words, those factors that have the power to turn a contact into a real friend or trusted acquaintance.

One example of a relationship glue could be a shared interest or activity. It can be basically anything that you both enjoy and could therefore bring you together, whether it be sport, good food, stamp collecting, baseball tickets, politics or sky-diving.

Away from the office and formal events, we are much more relaxed, genuine and approachable. Hence there is no better time to really get to know someone than on occasions when they are genuinely having fun.

Of course, on such occasions you should also be prepared to be a good conversation partner. To do this, you need to follow one fundamental rule: the best way to make good small talk is not to make small talk at all. Instead, be attentive, honest and open with the other person. Rather than exchange mindless banalities, you should always look to share something interesting or personally enlightening in a conversation. The other person will remember you in a positive light and will look forward to each subsequent encounter.

### 6. A good networker is sociable and patient. 

What is the major thing that a Hollywood star and a good networker have in common?

In order not to disappear entirely from the scene, both will be ready every single day to make new contacts and to connect with other people.

There are plenty of examples of successful people who show a steadfast patience with others and who seem determined to jump at any opportunity to network.

A CEO might, for example, make sure to speak to at least 50 people every day. Instead of simply shutting himself away in his office and using the telephone for all communications, he will spend hours walking through the company offices, speaking to people on the highest and lowest rungs of the company ladder.

Hillary Clinton is a clear example of someone with the impressive amount of patience and determination it takes to make a good networker. Keith Ferrazzi spent some time traveling with her when she was First Lady, and on one particular day she got up at 5 a.m., made her telephone calls, delivered four or five speeches, and then attended several cocktail parties as well as visited people at their homes. In the course of that day, she shook around 2,000 hands — and she also remembered the names of many of the people she had met during that trip.

### 7. A good networker always has something to say and embodies a unique message. 

"Would I want to spend an hour with this person at dinner?"

Almost everyone asks themselves this question in one form or another when they meet a person for the first time.

This is part of the proverbial act of sorting the wheat from the chaff. Someone who no one wants to listen to or spend time with is never going to succeed in building a strong and lasting network of people, eager and willing to help him out.

The following two points are essential if you want to capture another person's interest:

1\. _Have something to say._ You must be in the position to hold an intelligent discussion on a variety of topics, including politics, sport, travel or scientific subjects.

_2._ _Embody a unique message_. If you want to really captivate someone, you must offer more than just pleasant, fleeting conversation; you need to stand for something — preferably something that sets you apart from others. This original and attractive message will make others want to be part of your network.

Your message could be about an idea, a habit or a skill. Whatever it is, it must be something on which you are a leading authority, an expert.

It's up to you how you go about gathering this expert knowledge, and hence how you start to embody this unique message — there's no standard blueprint that works for everyone. What is necessary, though, is that you are sufficiently open, ready to learn and healthily ambitious.

Keith Ferrazzi, for example, became an innovative marketing expert by constantly engaging with the latest marketing ideas, getting to know the leading thinkers in this area and by reading everything he could on marketing. Eventually, he was able to put his expert knowledge into practice in his profession.

### 8. A budding network is dependent on so-called super-connectors. 

Super-connectors are quite simply people who have thousands of contacts. What is remarkable about them is firstly the sheer number of contacts they have, and secondly that their contacts come from all different walks of life.

By the 1960s, social psychologist Stanley Milgram had already brought the power of these super-connectors to light. Milgram sent identical letters to 160 people living in Omaha, Nebraska, and asked that they ensure the letter reached a stockbroker in Massachusetts. They were not, however, to send the letter directly to the stockbroker's address. Instead, they should look among their acquaintances for someone they thought might know the stockbroker.

Those letters reached the stockbroker via all manner of different routes, changing hands a variable amount of times. However, the last point of contact for the majority of the letters were the same three people — the super-connectors. They were the crucial contacts, and through their numerous contacts with people from all walks of life, they enabled the letters to reach their destinations.

Although it is possible for super-connectors to be active in every conceivable field, they tend to appear most often in certain areas, notably the following:

  * politics and lobbying

  * journalism and public relations

  * bar and restaurant management

These professions, among others, seem to be the tailor-made jobs for super-connectors, because they allow super-connectors to meet a wide range of people. If you want to boost your personal network, you should try and get to know someone from these professions.

### 9. You can only hope to be successful if you have the right goals. 

Whatever you want to become in life, be it the president, a chief executive or a top athlete, you are never going to get there by chance.

Instead, you must make your life goals very clear and concrete, and then decide how you are going to set about achieving them. Only then is it possible to develop strategies that will enable you to find success.

When you begin setting your goals, always focus first on finding your _blue flame_. The blue flame represents the crossover between our passions and our abilities. It is therefore incredibly important in helping us make the right professional decisions. Many people make their living doing work that is actually not satisfying at all. They resort to jobs that require talents they do not possess or that are simply not right for them, which only makes them discontent and unhappy. People who, by contrast, follow their passion and use their true talents (i.e., their blue flame) will never have such problems.

If you're not yet sure what your blue flame is, you can find it by using a simple test. First, make a list of all your dreams and goals, regardless of how ridiculous or banal they seem. Then make a second list of everything you enjoy doing and that brings you pleasure. Finally, combine the two lists and look for the places where your dreams and the things you enjoy doing overlap.

To this end, it can also be helpful to speak to someone you really trust. What does that person admire about you? In which areas do they think you could use a bit of help? What do they consider to be your strengths and weaknesses?

When you find your blue flame, and a job that allows you to kindle it, then the world is your oyster.

### 10. If you want to achieve your goals, you need to have a Networking Action Plan. 

A mission does not become a reality overnight. Just as if you were building a house, you need the right tools and the right plan.

Both the tools and the plan are combined in the _Networking Action Plan_ (NAP). The NAP helps us to achieve our goals because it provides us with a clear to-do list of tasks we can incorporate into our daily routine, and it also motivates us to be active in working toward our goals.

The first step of the NAP is the _development of personal long-, middle- and short-term goals._ Think about what goals you want to achieve within three years, and then work backward methodically in three-month and one-year stages, filling in an interim goal for each stage until eventually you end up with a clear and structured three-year-plan.

It is constructive to have an A-goal and a B-goal for each time slot. Say, for example, your A-goal is to be a teacher in three years' time; your B-goal could be to live in the catchment area you would like to work in.

For every A- and B-goal, write down the places you need to go and tools you need to acquire before you can achieve your goals, as well as the names of anyone you think could help you along the way.

The final version of your NAP should be as individual and personal to you as your dreams are. Your completed NAP will become the tailor-made driving force of your missions. It allows you to see your goals before you, and provides you with all the information you need about where to go, what to do and who to meet on your journey to reaching your blue flame.

### 11. If you want to be successful, you have to be your own brand. 

We are surrounded by brands. We are confronted with them on a daily basis, and they are responsible for the way we perceive things. On seeing the three-stripe logo, you immediately think of Adidas. The stylized bitten-apple image will immediately make you think of Apple.

But it's not only companies and products that can create brands — completely normal people can do so too!

So how do you do it? How do you make yourself into a brand?

The first step is to develop a personal branding message. To do this, you should ask yourself the following questions: What sets me aside as being different or special? What are my strengths and weaknesses? Which of my achievements am I most proud of? What is it that I do to create a feeling of worth? What do I want people to think of when they hear my name or see it written down? What would I like to be famous for?

Having answered these questions, you should be in the position to create a personal branding message using just two sentences.

Next you have to think about the brand packaging. This means thinking hard about your clothes, conversation style, hairstyle, business cards, letterheads and your office. Everything that plays a part in creating an outward impression should be tailored to suit the image you want the world to see.

The next step is making your brand known. The best way to do this is by becoming your own PR company: do things that put you in a good light, and concentrate on getting positive attention. This could include taking on projects at work that nobody else wants to do or putting forward ideas and suggestions that make your boss notice your initiative and creativity. Naturally, being your own PR company also includes doing your utmost to avoid negative attention. Do not, for example, demand a pay rise if your work has not been up to scratch.

### 12. If you want to be successful, you need to have the right mentors around you. 

Great musicians know it, and so do sports stars. If you don't have a good coach behind you, you will be unlikely to perform to your best.

Young artisans learn their trade through an apprenticeship; they begin developing their own style only after they have worked with the old masters of the profession. The same goes for business: young people can learn a great deal from experienced professionals. Hence, any good management will introduce a mentoring program that allows experienced workers to pass on their knowledge to the junior staff.

In today's business-world, where teams with a variety of functions are expected to react sharply to changes in fast-moving and dynamic surroundings, a mentorship is an effective way of getting the best out of employees.

Surrounding yourself with the pioneers, big fish and other successful people can be a big advantage to you when building a personal network. Many studies have shown that the people around you significantly affect how well you achieve. Both success and failure can be attributed to the nature of your peer group. If, for example, you are surrounded by people who have many good contacts, you will find that your own list of contacts begins to grow. And if you are surrounded by successful people, you yourself are far more likely to succeed.

### 13. Final summary 

The main message in this book:

**Successful networking is based on openness, determination and empathy. You can be a good networker only if you really understand yourself and what it means to appreciate others.**

This book in blinks answered the following questions:

**Why is it worth your while to become a good networker?**

  * Having a personal network is a prerequisite for a successful career.

  * Anyone can learn the art of networking.

**What are the signs of good networking?**

  * Successful networking is based on generosity and loyalty.

  * A good networker builds her network before she really has need of it.

  * A good networker looks for relationship glues.

  * A good networker is sociable and patient.

  * A good networker always has something to say and embodies a unique message.

  * A budding network is dependent on so-called super-connectors.

**How can you achieve your goals and be successful in life?**

  * You can only hope to be successful if you have the right goals.

  * If you want to achieve your goals, you need to have a Networking Action Plan.

  * If you want to be successful, you have to be your own brand.

  * If you want to be successful, you have to have the right mentors around you.

**Suggested further reading: _Coffee_ _Lunch_ _Coffee_ by Alana Muller**

_Coffee_ _Lunch_ _Coffee_ is a practical guide to networking. Using her personal and professional experience, along with tips and exercises, author Alana Muller demonstrates how to develop networking skills and build lasting relationships that can help us in our personal and professional lives. A must-have for anyone who wants to succeed professionally.
---

### Keith Ferrazzi

Keith Ferrazzi worked his way up from humble beginnings to the elite universities of Harvard and Yale, moving from there to become one of the most sought-after marketing experts in the United States. His network ranges from Washington, to the most powerful managers of America, to the top rungs of Hollywood. At the World Economic Forum in Davos, Ferrazzi was mentioned as one of the "Global Leaders of Tomorrow."

