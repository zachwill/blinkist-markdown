---
id: 5a3ee58db238e10007128bb2
slug: never-split-the-difference-en
published_date: 2018-01-02T00:00:00.000+00:00
author: Chris Voss and Tahl Raz
title: Never Split the Difference
subtitle: Negotiating As If Your Life Depended On It
main_color: F6C931
text_color: 756017
---

# Never Split the Difference

_Negotiating As If Your Life Depended On It_

**Chris Voss and Tahl Raz**

_Never Split the Difference_ (2016) is your guide to negotiation. Based on the extensive FBI work of Chris Voss, the authors offer up hands-on advice about how to negotiate your way to success, whether it's in the office, the home, or a hostage stand-off.

---
### 1. What’s in it for me? Become a master negotiator. 

Have you ever tried and failed to convince your partner to go to a new restaurant, a car salesman to give you a better deal, or a potential client to sign on to your business pitch? Most of us fail to convince others on a fairly regular basis. No matter how hard we try, our pleas fall on deaf ears.

That's because we aren't negotiating right.

And that's exactly where these blinks come in. You'll find out the secrets of successful negotiation from none other than Chriss Voss, once the FBI's number one international kidnapping negotiator.

You will also learn:

  * which emotional characteristic is your strongest weapon;

  * what kind of voice to use when negotiating; and

  * how labeling can save lives.

### 2. Negotiation occurs in every aspect of life – and there’s more to it than rationality and intellect. 

People tend to think of negotiation as something reserved for lawyers and corporate board rooms, but the truth is, humans negotiate in every part of life. In other words, while negotiating is what the police do when dealing with hostage situations, it's also something that happens at work, at home, with your partner and with your kids.

In a simple sense, negotiation is just trying to get things to go your way; it's having an interaction or communication with a specific outcome in mind. Whenever two or more people want something from one another, negotiation is taking place. Say you want a raise and your boss wants your salary to stay where it is. Or maybe you want your kids to go to bed by eight, but they want to stay up till ten.

OK, so negotiation is more common than most people think. But what makes for a successful _negotiator_?

It's more than just mathematical logic and a keen intellect. That's because humans aren't always rational; they often fail to act on the basis of logic or reason. To make matters more complicated, humans aren't always predictable either. People often act based on their animal nature, which is irrational, spontaneous and a bit wild.

That's precisely what the psychologist Daniel Kahneman and the economist Amos Tversky found after years of study. Their findings challenged conventional thinking on negotiation. Here's how.

In the 1970s, when negotiation first became defined as a field, it was based on the assumption that each individual acted rationally and to her own advantage. However, Tversky and Kahneman's research discovered that humans are prone to what's called _cognitive bias_, which makes them unconsciously irrational.

They even identified 150 different types of biases, including the so-called _framing effect_ ; this concept states that, when faced with the same options, people will make different choices depending on how the alternatives are framed.

Simply put, to be a successful negotiator, your approach has to take into account the complex nature of humankind. In the blinks that follow, you'll learn how to do just that.

### 3. Successful negotiation is about building trust and getting information. 

Good negotiators approach the bargaining table attempting to gain as much information as possible, both about the situation and their counterpart. Naturally, during this process, new stuff comes to light, so success means being prepared for a bend in the road.

For instance, you won't really know what a hostage-taking terrorist wants or how he'll behave; he might be armed even though you were told that he wasn't; he could even feed you incorrect information to lead you astray.

A real-life example of such a situation dates back to 1993 when the author was involved in negotiations after a robbery resulted in the taking of three hostages in a Manhattan bank — two bank tellers and a security guard. The robber who communicated with the FBI said that there were four robbers, but he was in fact alone; while his partners had just robbed the ATM, he went for the whole bank, taking hostages in the process. Looking back on the situation, the author realizes that the robber only spread this misinformation to confuse the author and his colleagues, buying himself time to plot his escape.

So, information is key, and to get it you need to establish an amicable rapport with your counterpart. That's why one goal of negotiation is to get the other party to talk a lot. As she does, you'll be able to figure out what she needs and wants.

That being said, nobody is going to provide you with information if they don't trust you, and that's why rapport is essential. If you manage to establish it, you'll build trust in the process, making it much more likely that the other person will divulge useful information.

But how can you establish rapport? Explore the next blink to find out.

> _"(...) until you know what you're dealing with, you don't know what you're dealing with."_

### 4. Closely listening to and even repeating what your counterpart says can build trust. 

So, you know that trust is key, but how do you establish it?

The best route is to engage in _active listening_, which means showing empathy and demonstrating that you understand what the other person is going through. Several techniques can help.

The first is called _mirroring_, which essentially means repeating what your counterpart says but with an inquisitive tone. Just consider the Manhattan bank robbery negotiation from the previous blink. The robber in that situation, Chris Watts, made continual demands for a vehicle. He mentioned that his own car was gone as his driver had fled.

Hearing this, the author mirrored it by saying, "Your driver was chased away?" In response, Watts went on to say that the driver had fled when the police arrived on the scene. The author held onto this information, along with other tidbits he teased out through mirroring, many of which assisted the FBI and NYPD in apprehending the driver.

But why does mirroring work?

Largely because it makes the other person feel that you're similar to him. After all, your counterpart is only human and will naturally be drawn to similarities. That's because, just like other animals, people like to be in groups with similar traits. Doing so gives us a sense of belonging and forges trust. This is powerful in a negotiation: when your counterpart starts to trust you, he will become more likely to talk and find a solution.

To test the effects of this approach more scientifically, the psychologist, Richard Wiseman conducted an experiment in which waiters would take orders from customers. One group of waiters was asked to use mirroring while the other was asked to utilize _positive reinforcement_ through phrases like "no problem" and "great." In the end, the waiters who mirrored the orders made by customers received much higher tips, earning 70 per cent more than the other group.

> _"Contrary to popular opinion, listening is not a passive activity. It is the most active thing you can do."_

### 5. The tone of your voice can do wonders for negotiation. 

Have you ever been upset with another person not because of _what_ she said, but because of _how_ she said it?

Well, it makes sense since intonation and the human voice are powerful tools that you can use in successful negotiating. For instance, if the other party is likely to become upset or nervous, you should employ a deep but soft voice, or what the author has called your _Late-Night FM DJ voice_. By being slow and reassuring, this tone is sure to have a profound effect on the other person.

After all, it'll comfort him, making him more likely to share the information you're looking for. At a certain point during the bank robbery negotiation, the author had to take over communication with the robber from his colleague, Joe. To prevent Watts from growing upset or nervous due to the shift, the author told him in a deep, calm voice that Joe was out and he was in. It was put forward in such a downward-inflecting manner, radiating calmness and reason, that Watts didn't even flinch.

That being said, most situations call for a different tone, namely your _positive/playful voice_. This voice communicates that you're easygoing and empathetic; it puts things in a positive light with an encouraging attitude.

If you smile while speaking, this voice will often come out naturally. Even if your counterpart can't see your smile, it'll come through in the tone of your voice.

While on vacation in Istanbul, a colleague of the author's was amazed by his girlfriend's ability to cut great deals with backstreet spice merchants. He soon realized that she always pushed for better prices, but did so in a playful, positive way. While the merchants were themselves skilled bargainers, her approach drew them in, convincing them to give her a better deal. Try this yourself, when you're at a store or market!

### 6. Understand and state the emotions of your counterpart to position yourself effectively in a negotiation. 

In psychotherapy, progress is made by tapping into and understanding a patient's emotions. The same goes for negotiation.

Rather than ignoring emotions, you have to combine them with empathy to your tactical advantage. However, being empathetic doesn't necessarily mean agreeing with the other person. It just means attempting to see his perspective. This is where _tactical empathy_ comes into play; it refers to using your understanding of your counterpart's perspective to better position yourself in the negotiation.

One technique to do so is called _labeling_. It simply refers to telling your counterpart that you understand and acknowledge both his position and feelings.

This simple approach works by calming the other person down and making him behave more rationally. Just take a 2007 study by psychologist Matthew Lieberman at the University of California. Lieberman showed participants pictures of people expressing a strong emotion, thereby activating their amygdalas, the brain area responsible for fear. However, when the same participants were asked to state what emotions they saw, their brains experienced activity in the areas related to rational thinking.

Or consider an example from 1998 when four prison fugitives, believed to be in possession of automatic weapons, hid out in a Harlem apartment. The author figured out what they were feeling and then labeled those feelings; he told them that he knew they didn't want to leave the apartment, that they were _worried_ that if they opened the door, they would be shot and that they must be _scared_ of going back to prison.

After six hours of dead silence, the fugitives surrendered and later told the author that he had calmed them down. In other words, his labeling had worked. He simply understood and acknowledged their emotions, reaching a favorable outcome in the process.

> _"Emotions aren't the obstacles, they are the means."_

### 7. Don’t accept the other party’s demands, don’t compromise, and don’t rush. 

Have you ever been in such a hurry to settle a dispute that you ended up unhappy with the final result? Nobody wants that, and it's crucial to remember that accepting a bad deal or even compromising is always a mistake. This is called _splitting the difference,_ and you've got to avoid it at all costs.

After all, every human, your counterpart included, has thoughts and needs that she won't share or maybe isn't even aware of. When your counterpart asks for something, you can never be sure if she actually wants it, so giving her what she asks for won't necessarily solve the issue.

Say someone is holding a politician hostage and saying that he'll cut off her head unless he receives one million dollars. While the hostage-taker says that money is what he's after, he might want to make a political point. If that's true, and you give him the ransom, there's no telling if he'll release the hostage.

For the same reason, it's essential to take your time, even when your counterpart sets deadlines. Remember, your job is to learn about the other party and, if you're pressed for time, chances are your judgment will be clouded. It's important that you avoid this. It can help to remember that most deadlines are flexible and relatively random.

Just take an example in which the wife of a Haitian police officer was kidnapped. The kidnappers demanded $150,000 and, following weeks of negotiation, the author saw a pattern; as Friday approached, the kidnappers would push extra hard for the ransom before laying low for the weekend. He realized that they wanted to party and, to do so, they needed money!

Understanding this, the author could ascertain that the deadlines weren't so serious and that he could negotiate a much lower price since you don't need $150,000 to have a good time in Haiti.

As in all other negotiations, patience, time and information were critical for a successful outcome.

### 8. Final summary 

The key message in this book:

**By employing a few simple techniques and understanding where the other party is coming from, you can negotiate your way through just about any situation, whether it's with your boss, your partner at home, or your local used-car dealer. The key is to stay calm and establish trust with the other person.**

Actionable advice:

**Be prepared for the extreme.**

In one respect, negotiation is just like war: it's crucial to know your enemy. That's why you should always let your counterpart make the first offer. That being said, you should also be prepared for this initial offer to be extreme. In fact, it's entirely normal for first offers to be far afield of your expectations. Just keep in mind that this only represents the limit for your counterpart and you can almost certainly get a much better deal. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _You Can Negotiate Anything_** **by Herb Cohen**

_You Can Negotiate Anything_ (1980) shows that negotiations occur in every walk of life and that it is vital to have the skills and understanding to deal with those situations. The book outlines the key factors affecting negotiation success, as well as ways of negotiating for win-win solutions.
---

### Chris Voss and Tahl Raz

Chris Voss is a former lead kidnapping negotiator with the FBI. His many years of experience negotiating with all manner of criminals make him an expert in the field. He's the founder of negotiation consultancy The Black Swan Group and a professor who has taught negotiation courses everywhere from Harvard University to MIT's Sloan School of Management.

Tahl Raz is a journalist and co-author of the _New York Times_ bestseller, _Never Eat Alone_.

