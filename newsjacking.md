---
id: 569388427e61c70007000001
slug: newsjacking-en
published_date: 2016-01-11T00:00:00.000+00:00
author: David Meerman Scott
title: Newsjacking
subtitle: How to Inject Your Ideas Into a Breaking News Story and Generate Tons of Media Coverage
main_color: FFF133
text_color: 666014
---

# Newsjacking

_How to Inject Your Ideas Into a Breaking News Story and Generate Tons of Media Coverage_

**David Meerman Scott**

_Newsjacking_ (2012) is about the best new way to get media attention — not by planning ahead but by reacting quickly and cleverly. These blinks not only explain how this new form of generating news works, but also how you can and should react to it, and how to use it to attain the best possible media coverage.

---
### 1. What’s in it for me? Learn how to hijack headlines and get your message into the public realm. 

One question nearly all companies will unavoidably ask themselves is how they can get their message out there and make sure it's heard through all the media noise. The answer is simple: you hire an advertising firm to do it for you, right?

Although this may well work, it is no longer the only way — and it's certainly not the best way. New media channels, like Twitter and Facebook, as well as content-hungry journalists, have made it possible to hijack the media's attention and use it to your advantage. And the best thing is, you don't have to be a big-budget player to do it.

With the right knowledge of how the media and journalists work, you can ride the news and get your company or organization attention that would have cost millions with traditional advertising. Let's get you started!

In these blinks, you'll learn

  * how Rick Perry masterfully hijacked the news;

  * what the London Fire Brigade has to do with Kate Winslet; and

  * why you should ban Paris Hilton from your hotel.

### 2. Newsjacking is a powerful tool to get media attention. 

Do you have a message you want to share with the world, but no idea how to get it out there? If you don't have a great deal of influence or a huge marketing budget, you have to be clever about it: don't plan media coverage, just jack the headline.

Newsjacking is a powerful tool, and it's really all about timing. 

On August 13, 2011, all mass media in the United States was poised to report on Iowa, the site of a key Republican Party fundraiser and preliminary poll. But on the same day, Texas governor Rick Perry announced he would run for president.

The timing was ingenious. Perry landed on the front pages of national newspapers, while the results from Iowa were relegated to the back pages. This is just one instance of brilliant newsjacking. 

In today's information age, the way we consume news has changed dramatically. We expect second-by-second updates, 24/7; a single tweet can change the direction of news coverage completely. As a result, planned news campaigns no longer have the power they used to. For potential newsjackers, this is an ideal situation.

Effective newsjacking requires an understanding of what's trending in the media at any given moment, even though it changes incredibly quickly. If you manage to add something new to a trending story, you'll get the news coverage you want with minimal effort. 

Another useful example took place when Paris Hilton was arrested for drug possession in 2010. At a time when headlines about her arrest dominated media, Wynn Resorts announced they were banning Hilton from their properties — despite the fact that Wynn Resorts had nothing to do with Paris Hilton. 

And yet, Wynn received astonishing media attention that they couldn't have bought with a planned campaign. It was a media reaction worth millions of dollars, and all a result of Wynn Resorts' impeccable timing. 

The new media landscape is a paradise for those who are observant and know when to act. If you're not ready to get newsjacking yourself, chances are somebody might do it to you!

### 3. When newsjacking, your objective is the second paragraph of a news story. 

Ever noticed how the first paragraph of any news article is all about the facts — the who, what, where and when? It's usually not until the second paragraph that the "why" or a "what next" makes an appearance. If it's not possible to find yourself at the center of an incident in the first paragraph, you've always got the opportunity to make it into paragraph number two. 

For instance, Sir Richard Branson was hosting Kate Winslet and other famous guests at his house on the British Virgin Islands when a sudden lightning strike sent his house up in flames. In the ensuing chaos, Winslet helped Branson save his 90-year-old mother. It was a miraculous story, but not meaty enough to make for a full headline. 

The London Fire Brigade took this as their opportunity to jack the news by announcing they would give Kate Winslet the chance to train with their firefighting crews. This announcement was mentioned in every news item about the lightning strike that followed. 

What's more, the Fire Brigade was also able to raise public awareness about the importance of home smoke detectors. No other planned campaign would have had such a powerful impact. 

It's important to remember that not all journalists are Walter Cronkites or Bob Woodwards. Today, journalists are under pressure to spit out articles every minute, so they're not going to waste time hunting for a scoop; they'd rather have it land right in their lap. This is where you, the newsjacker, come in. 

Journalists aren't just seeking news to report on, but a story or comment that gives that news broader significance. If you can offer this to journalists, you might enjoy a mention in that crucial second paragraph. Doesn't sound so hard, does it? Well, the trick is getting your story to the journalist quickly enough.

### 4. Follow trending topics in your industry closely to find your newsjacking opportunities. 

Newsjacking is impossible if you can't keep up with the relevant news in your field and beyond. Although we're faced with an overwhelming tide of information, it is possible to follow the news and keep your head above water. 

Bloggers, analysts and journalists that cover your industry are a great resource, so make the most of their presence by following them in real-time through their Twitter feeds, on other social media or by using RSS. Monitor the keywords and phrases they use, set alerts on Google News and make sure you update your alert settings as trending topics shift. 

Next, ensure your stories can be linked back to you. Media requires traceable quotes, so never post anonymously. While some reporters rely on known commentators to provide them with quotable responses to certain events, other reporters are looking for diverse sources. These reporters will give you a chance, but only if you can show that your online presence is credible and insightful. 

You can do this by consistently providing comments and reflections on changes in your industry. Don't just post your insights, share them with journalists and other figures who you think might be interested. You can even turn the announcements of your competitors into publicity opportunities by responding with one-page announcements called media alerts. 

SMTP.com, a leading e-mail service provider, did exactly this when Amazon announced their entry into the e-mail delivery market. SMTP.com's media release got a lot of attention and even drew the focus away from Amazon. 

If you're able to keep up with trends and newsjack with speed, you'll not only be able to bring your cause media attention. As you'll see in the last blink, newsjacking can also be used to recover from a PR crisis.

### 5. Newsjacking is powerful enough to save your reputation, so use it well! 

Skilled newsjackers can use the right message to steer media attention away from news items that can damage their reputations and toward those that can improve them. Larry Flynt, publisher of _Hustler_, took advantage of newsjacking after landing himself in the midst of a government-linked sex scandal. 

At around the same time, Wikipedia was arriving on the scene, so Flynt announced that he would donate $50,000 to their cause. He suddenly turned his reputation from that of an unsavory character to a defender of free speech, and jacked a lot of Wikipedia's media coverage while he was at it. 

Of course, it's best if you don't do anything to damage your reputation in the media in the first place! Avoid this by always staying on guard. You must be prepared to react to a PR crisis at any time. Be statesmanlike when it comes to rivals and competitors; stay cordial and welcoming, and never be malicious or vindictive.

Similarly, you should pay close attention to your online writing style. Use full sentences and avoid slang and excessive jargon to keep your presence credible and relevant. Avoid making clever or cute quips when the news involves people suffering. Finally, stay genuine. If your attempts to gain publicity are thinly veiled and self serving, they'll fall flat. 

There is no formula for perfect newsjacking. Although you may not succeed with every post, you should never stop trying to make the most of opportunities that come your way. Stay active online, stay on the lookout and think outside the box to link your cause to the headlines trending around the globe.

### 6. Final summary 

The key message in this book:

**Rather than wasting time and money on a planned media campaign, get your big break by jacking the news and taking over headlines. Follow trending topics in your industry closely, react quickly and give journalists what they need to bring your cause priceless media attention.**

**Suggested further reading:** ** _Contagious_** **by Jonah Berger**

_Contagious_ examines what makes a product, idea or behavior more likely to be shared among many people. The book explores the question of whether contagious things are accidents or the results of good marketing, or whether contagiousness is an inherent feature of a product, idea or behavior. It argues that, far from being merely a matter of luck, the majority of very popular products and ideas are the result of a combination of savvy planning and execution.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### David Meerman Scott

David Meerman Scott is an international public relations and marketing expert, specializing in online news. An in-demand keynote speaker, Meerman Scott has also written several best-selling books, including _The New Rules of Marketing and PR_.

© David Meerman Scott: Newsjacking copyright 2012, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

