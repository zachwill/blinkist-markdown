---
id: 53d8087c3331610007000000
slug: nice-girls-dont-get-the-corner-office-en
published_date: 2014-07-29T00:00:00.000+00:00
author: Lois P. Frankel
title: Nice Girls Don't Get the Corner Office
subtitle: Unconscious Mistakes Women Make That Sabotage Their Careers
main_color: 2A9FD0
text_color: 1B6585
---

# Nice Girls Don't Get the Corner Office

_Unconscious Mistakes Women Make That Sabotage Their Careers_

**Lois P. Frankel**

This book focuses on the reasons why women often don't make it to the top ranks in the world of business. Frankel explains how women unconsciously behave in ways that undermine their business aspirations and presents female readers with measures to consciously counteract their self-defeating behavior.

---
### 1. What’s in it for me? Learn why women are still unequal in the business world – and how to change that. 

Did you know that, even after years of progress in women's rights, women all over the world still earn consistently less than men? Women are also less likely to hold highly influential positions. For example, they make up a mere 3.8 percent of CEOs of Fortune 500 companies.

Even in Western countries, women are still not on an equal footing with men. There are many reasons for this inequality, most of which can't be changed by one woman alone.

Yet there's one thing women _can_ change: if you're a woman, chances are good that over time you've learned to _unconsciously_ _sabotage_ _yourself_. For example, you may avoid assertive behavior for fear of being labeled "bossy" or find yourself reluctant to compete because competing is "unfeminine."

_Nice_ _Girls_ _Don't_ _Get_ _the_ _Corner_ _Office_ will make you aware of the typical mistakes that prevent women from reaching their full potential. It will help you learn how to stand your ground in a competitive environment, such as the workplace, and find the right balance between embracing your femininity and not making the wrong move in a game still ruled by men.

In these blinks, you'll find out:

  * why some girls sigh in relief when they lose a game of Monopoly,

  * why smiling _less_ might be the key to getting that promotion,

  * why you could work 24 hours a day and still not get promoted, and

  * why there's nothing immoral about capitalizing on your relationships.

### 2. Women are still underprivileged in the business world. 

Did you know that, until 1934, an American woman would lose her citizenship if she married a man from another country? Or that, until 1977, a married West-German woman wasn't allowed to sign her own work contract without her husband's approval? In many Western countries, men and women weren't equal before the law until well into the twentieth century. In fact, most American and European women weren't even allowed to vote before the end of World War I.

Since then, women's rights movements in many Western countries have successfully established near gender equality before the law — but when it comes to their careers, women are still at a disadvantage.

For example, women earn consistently less than men.

In the United States, Hispanic women make only 59 percent of what Hispanic men earn for the same job. And while Caucasian women fare better, they still earn only 77 percent compared to their male co-workers. Furthermore, in the first year after they finish college, female graduates already earn 8 percent less than their male peers; two decades down the line, the gap increases to 20 percent.

But this gap is not only a problem in the United States: women are less likely to hold well-paid and highly influential positions all over the world.

In fact, a modest 8 percent of all top executives worldwide are women, and there are no more than twenty female heads of state throughout the world.

### 3. Women are taught to strive for sympathy rather than respect and influence. 

Have you ever felt guilty for hitting a winning streak at Monopoly? You might be surprised to learn that this reaction is as widespread among women as it is rare in men. Is it due to some natural feminine reluctance to compete or something else at work here?

In actuality, girls are _raised_ to strive for sympathy and physical attractiveness — not success or self-assertion.

From early childhood on, girls are taught that they will only fare well if they win the sympathy of the people around them. For example, they will be praised and rewarded for being polite and compliant, and punished for being aggressive, whereas boys' aggressive behavior tends to be tolerated a lot more.

How childrens' toys are _gendered_ also reflect these role expectations. Many toys typically given to girls are associated with attractiveness and mothering, like Barbie dolls, whereas typical toys for boys are related to fighting and competition, like wrestling action figures.

But the gendered treatment of girls doesn't stop there: in adult women, too, assertiveness and competitive behaviors are discouraged.

Why?

Because they counter social stereotypes of femininity, like being passive, agreeable and compassionate. This means that a level of competitiveness deemed acceptable in men may raise eyebrows when exhibited by a woman. Just imagine the public's reaction if a female electoral candidate savored her victory over an opponent by violently punching the air!

These continual behavioral expectations mean that if a woman acts assertively, just like a man would, she may be ridiculed or criticized for being "bossy."

Imagine a hospital's new medical director learning that some of the physicians used to drink small amounts of alcohol at work. If she confronts the staff and implements a program of random alcohol testing, chances are that she'll be called bossy — or even a bitch.

> _"If you're concerned only with being liked, you will most likely miss the opportunity to be respected."_

### 4. Asking for too much advice and letting others decide for you can seriously hurt your professional image. 

Imagine your boyfriend asks you how you'd prefer to spend your joint summer vacation — cycling through Scandinavia or chilling on the beaches of Hawaii. Are you one of the many women who react by asking, "Well, what would _you_ like to do?"

As well intentioned a response that may be, this manner of letting another decide for you is best limited to your private life.

Why?

Because if it's your job to make a decision and you let others decide in your place, you'll appear reluctant or incapable of doing your job.

Let's say your boss wants you to decide when to inform your company's shareholders about a substantial upcoming loss — either right away or after the extent of the loss has been specified.

In this case, she explicitly makes it your job to decide what to do next. Therefore, if you respond by asking your boss which course of action she'd take, what you're really telling her is that you won't do what she asked. Plus, she'll get the impression that you're incapable of making a risky decision.

Considering others' opinions too carefully can damage your professional image in other ways. For example, if you can't take action without polling everyone, you'll appear unfit to lead.

Why?

Because it's important for a leader to be able to take action quickly and decisively. Imagine your company has the chance to seize a great business opportunity. If you asked your whole team what they thought about it first, it might be so time-consuming that your competitors beat you to it.

Furthermore, sometimes leaders have to be responsible for taking unpopular steps, like reducing bonuses. Asking advice from the affected employees would only slow everything down and make things more painful.

### 5. Acting or dressing too femininely can undermine your professional image and distort the message you seek to convey. 

When you picture some successful female politicians, like Thatcher, Clinton and Merkel, do any of them strike you as particularly feminine? Is that because too much femininity is detrimental to a woman's career?

In a word, yup.

For example, it's very feminine to smile a lot. And although smiling can win you lots of sympathy, it can also distort your message and make you appear less assertive. This is tough for women to control, because they're raised to smile much more than men, and thus often smile without being conscious of it.

Yet smiling makes you look less authoritative and can distort the message you're trying to deliver. For example, if you're trying to deliver a serious message, smiling will clash with what you want to communicate.

Another smiling issue arises when women smile as they criticize another person. This may be out of habit or due to the desire to soften the message, but it can make a woman look insecure or insincere. Moreover, in situations of potential conflict, a smile could be mistaken for a smirk — and escalate the conflict.

Here's something else women need to be aware of: if you present yourself in an overly feminine way, you'll distract people from your professional competence and what you want to say.

The feminine features of your presentation make a strong impact on other people — at the expense of other aspects of your professional persona. If you tend to style yourself in a noticeably feminine or playful way, those may well be the features that others will associate with your personality — and not your professionalism. For example, if you wear too many accessories to a conference, people might be too mesmerized by the jewels dangling from your ears to listen to the well-founded thoughts you want to share.

> _"Avoid touching your hair unnecessarily. Think (...): Every time I touch my hair, I reduce my credibility by one year."_

### 6. The business world is no place for little girls. 

They say that in every woman there still lives the little girl she used to be. This may be true, but it's crucial to leave your childhood at home when you enter the office.

Why?

Because thinking like a little girl will affect your professional judgement.

For example, it make sense that many little girls see fathers as authority figures. But if you're still viewing men in authoritative positions as father figures, you won't have the objectivity and independence to deal with them in a professional way.

If you see your boss as a father figure, your reactions to him might be very emotional. For example, you might find yourself tuning in to his moods rather than paying attention to the information he's communicating.

Furthermore, bending to your boss's every mood is not part of your job description and might distract you from the tasks that you're responsible for. And while you might be busy improving your boss's mood with compliments and home-made cookies, a colleague might earn himself a promotion by solving the problem that fouled your boss's mood in the first place.

Little-girl thinking has other negative consequences for businesswomen. Little girls are often taught to doubt their own knowledge and intuition and, over the years, this self-doubt becomes so deeply ingrained that even seasoned experts in a field will second-guess their judgements when laypeople doubt their expertise.

This can lead you to make very bad decisions in spite of the fact that you know more. For example, if you believe that someone you're negotiating with is better informed than you are, you're likely to be won over prematurely. But if you're aware of this tendency, you can pay closer attention and make sure you don't fall into the little-girl trap.

> _"No one can make you feel inferior without your consent."_ \- Eleanor Roosevelt

### 7. In a competitive business context, you have to act the part. 

Imagine you're a manager interviewing two highly qualified applicants for a top position: Would you go for the candidate who sits upright, speaks with a loud voice and accentuates his remarks with lively gestures — or for the one who seems to shrink before your eyes, speaking so softly you wish you wore a hearing aid?

Sadly, too many women fit the profile of the second candidate without even realizing it. If women want to make it in the business world, they need to make themselves aware of this behavior because taking up too little space makes you look too timid and insecure for a leadership position.

The problem is that women are taught to take up as little space as possible all their lives. They learn to hold their arms close to their bodies, cross their legs at the knees and use few gestures if they give a presentation. And, just as taking up lots of space makes you look confident, taking up too little space conveys another message: that you're insecure, submissive and unwilling to take risks. Not the ideal self-presentation to win over any manager.

Another bad habit women learn as they grow up is using too many _qualifiers_ and _minimizers_ when they speak.

If you use qualifiers — e.g., "We have to take committed action, but I might be wrong" — you'll appear unsure about what you're saying and, in turn, less convincing and competent. If you use minimizers when you talk about your work or achievements — e.g., "Oh, I'm _just_ a teacher" — then you're saying that what you are isn't important.

So pay attention to how you talk, try and cut out the minimizers and qualifiers, and you'll start to appear much more confident.

### 8. Working too hard will get you nowhere – regardless of your gender. 

Do you agree that employees show their dedication by working hard and skipping breaks? If so, then how could working hard be detrimental your career?

Because if you invest all your time and strength into getting work done, you may miss out on the crucial social aspects of career development.

Although delivering high-quality work is important for getting promoted, networking and good teamwork skills play an equally important role. So if you tend to isolate yourself to get more work done instead of chatting with your colleagues, you'll soon appear like a mediocre team player.

Furthermore, working too hard diminishes the quality of your work and actually makes you look ineffective.

Productivity experts have found out that a high level of concentration and accuracy — both requisites for high-quality work — can only be maintained when a break is taken every 90 minutes. Furthermore, working without a break can actually give a bad impression: people will think that you're working so much because you're inefficient or overwhelmed by your workload.

Finally, no matter how hard you work, if you do the _wrong_ _kind_ of work, you won't get any closer to a promotion. If you're always busy with low-profile assignments, you'll never take the risk necessary to catch your superiors' eye. So _what_ you're working on is actually far more important than how much you work.

Imagine a student who wants to get into a doctoral program. In order to make a good impression on her supervisor, she volunteers to do all the grunt work, like writing protocols and copying scripts for her fellow students. Ultimately, however, the person who'll get accepted won't be someone like her but a student who distinguished herself by taking a bold move, like applying to a top research group.

> _"Stop volunteering for low-profile, low-impact assignments. If necessary, sit on your hands rather than raise it."_

### 9. If you want to get that corner office, you can’t ignore the strategic value of relationships. 

Imagine you're at a party and you've just met a charming man. You're both chatting away when he tells you he's an executive of the company you're planning to apply to. Would you ask your new acquaintance to support your application?

Most women are reluctant to take advantage of this kind of interaction. But if you're not willing to capitalize on relationships, you're missing the whole point of _networking_ and the great chances it offers to push your career forward.

Let's admit it: in the end, whether it's friendships or workplace connections, the point of every relationship is to trade favors or other things you need. There's nothing immoral about capitalizing on relationships! Furthermore, relationships are crucial because some immaterial goods, e.g., exclusive positions, are distributed solely via relationships and networks.

One way women don't capitalize on potential relationships is their reluctance to seek out a _sponsor_, i.e., a mentor who introduces them to people who can help them get promoted, and who uses his or her influence with senior executives to promote them as leaders.

So if you shy away from asking someone to be your sponsor, you miss out on a great career accelerator. In fact, it's been shown that women get fewer promotions than men _because_ they're less likely to have a sponsor. And women without sponsors are less likely to even try to get a top position.

The people you know help pave the path to your own corner office. You might not like it, but working hard while avoiding office politics is not an option — if you're going for the corner office.

### 10. Final summary 

The key message in this book:

**Girls** **are** **not** **traditionally** **raised** **to** **be** **tough** **players** **in** **the** **competitive** **workplace** **environment.** **They** **learn** **from** **a** **very** **young** **age** **to** **self-sabotage** **their** **success** **by** **undermining** **their** **value** **in** **the** **eyes** **of** **others,** **and** **by** **shying** **away** **from** **behaviors** **traditionally** **perceived** **as** **exclusively** **male,** **such** **as** **competitiveness.** **The** **way** **to** **overcome** **these** **self-defeating** **behaviors** **is** **to** **become** **aware** **of** **them** **and** **take** **active** **steps** **to** **correct** **their** **negative** **effects.**

Actionable advice:

**Answer** **only** **to** **your** **name.**

Whenever someone in business calls you a nickname, they are subtly relegating you to a childlike status. The next time you introduce yourself, make sure to use your full formal name, and only answer when that's how people address you. Even if people usually call you a nickname, if you establish this new dynamic, people will adjust over time. This simple measure will ensure you're taken seriously.
---

### Lois P. Frankel

Lois P. Frankel is an internationally recognized women's leadership expert. After the widespread success of _Nice_ _Girls_ _Don't_ _Get_ _the_ _Corner_ _Office_, she followed up the topic with _Nice_ _Girls_ _Don't_ _Get_ _Rich_ and _Nice_ _Girls_ _Just_ _Don't_ _Get_ _It_ (with co-author Carol Frohlinger) — and all of them were international bestsellers.

