---
id: 58ff6a3fb238e100061eedef
slug: night-school-en
published_date: 2017-04-27T00:00:00.000+00:00
author: Richard Wiseman
title: Night School
subtitle: The Life-Changing Science of Sleep
main_color: FFF533
text_color: 666214
---

# Night School

_The Life-Changing Science of Sleep_

**Richard Wiseman**

_Night School_ (2014) is about an often neglected yet essential ingredient for happiness and success in life: a good night's sleep. People have been struggling with sleep for centuries, and while we've come up with some methods for alleviating insomnia, we've also gained a lot of insight into why sleep is so crucial.

---
### 1. What’s in it for me? Get inspired to become a world-class sleeper. 

When people talk about how to improve their lives, the most typical goals include losing weight, being more mindful, exercising more, eating healthier or simply becoming a better person in general. But how often have you heard someone make a New Year's resolution to get better at sleeping?

It might sound like an excuse to be lazy, but sleep is a key process for the body that influences our ability to achieve other goals. But how do you get better at sleeping?

These blinks will look at what happens to us when we sleep, and when we don't, as well as the key factors that can improve or impair your sleep. It's time for a bit of Night School.

In these blinks, you'll learn

  * why only 15 percent of people get the amount of sleep they think they need;

  * how sleeping badly can even have an environmental impact; and

  * what the most important factors are for better sleep.

### 2. A good night’s sleep involves five stages where muscles relax, brain waves decrease and dreams begin. 

Everyone knows that sleep is just what you do when you're not awake, right? Well, while some of us might take sleep for granted and think of it as a simple daily routine, it's actually a fascinating and complex process.

Before we fall asleep, we need to gradually help the mind and body relax.

With an electroencephalogram, or EEG, scientists can monitor brain activity and see that our brains produces between 12 and 30 electric waves per second when we're awake. But when we settle into bed, this slows down to around 8 to 12 waves per second.

In the first stage of sleep, the muscles and the mind relax, and brain activity slows down even further, to around three to seven waves per second. This is when the brain begins to produce creative and less rational ideas.

Salvador Dalì took advantage of this stage by holding a spoon over a glass, and as he would drift off into the first stage of sleep, his muscles would relax and the spoon would fall into the glass, waking him up. He would then use the abstract images or idea that he had just imagined in his art.

In the second stage, muscles continue to relax and your breathing becomes deeper. This is the stage when your throat muscles relax and snoring can ensue.

After the second stage, we move from light sleep into the deep sleep of stages three and four.

At this point, brain activity decreases to just one or two waves per second, and the body is entirely disconnected from its surroundings, which is why it is difficult to wake someone up when they're in the third or fourth stage of sleep. And if they do wake up from these stages, they'll likely feel groggy and have a hard time focusing for a little while.

Finally, in stage five, the heart rate accelerates and rapid eye movement (REM) begins, which is a sign that the person is dreaming.

From here, the whole cycle starts again. Progressing from stage one to stage five generally takes about 90 minutes — so the entire process can repeat about five times in an average night.

> _"In the second half of the night, there is almost no deep sleep, and the dreams can last up to 40 minutes."_

### 3. Ever since the invention of the light bulb, we’ve been getting less and less sleep. 

If you've ever been out camping in the wilderness or spent some time in a cabin, away from electricity, you may have noticed that the quality of your sleep improves. This is because the invention of the light bulb has completely changed our relationship with sleep.

The inventor of the light bulb, Thomas Edison, considered sleep to be a waste of time. The way he saw it, why sleep when he could be inventing gadgets in his New Jersey laboratory? The only thing stopping him from working around the clock was the difficulty of working by candlelight or oil lamps at night.

This was one of the reasons he was determined to create an electric light source. He knew an electric bulb could be brighter than any other artificial light; all he needed to do was figure out what kind of filament could create a steady and reliable electric glow. Eventually, Edison settled upon carbonized bamboo and the light bulb was born, with the ability to stay lit for 1,000 hours at a time.

But the light bulb's benefits weren't only about being able to work nights; a whole new kind of nightlife arose, in which people could go out and socialize more freely than ever before, and for as long as they wanted. As a result, everyone was sleeping less.

With technological progress continuing since then, this sleep deprivation has only gotten worse.

With constant, 24-hour television stations, internet access and smartphone notifications, people are staying awake far too late.

In Paul Martin's book, _Counting Sheep_, the author cites a major sleep study from the year 2000, in which over 200 countries participated. The results showed that only 15 percent of participants got eight or more hours of sleep per night, and only 50 percent felt they even needed that much sleep.

According to the United States' National Sleep Foundation, in 1960, the majority of people slept between eight to nine hours every night — a figure that had dropped to seven hours by 2000.

In the next blink, we'll take a closer look at why sleep is so important to human health.

### 4. Sleep deprivation has serious consequences and losing even small amounts of sleep can cause accidents. 

Everyone knows the tragic story of the Titanic, but what was the root cause of the ship striking the iceberg that led to its demise? Was it merely that the captain had the ship going too fast — or did sleep deprivation also play a role?

We may never know all the circumstances at play with the sinking of the Titanic, but we do know that sleep deprivation can have a hand in such fateful disasters.

This was the case in March of 1989 when the _Exxon Valdez_ oil tanker was cruising off the coast of Alaska. The third mate noticed some large and dangerous ice floes ahead, but what he didn't notice was that the ship was on autopilot, which needed to be turned off before adjustments to the course could be made.

Unfortunately, by the time he figured out the problem, the ship was on a collision course with an underwater reef, which sliced open the hull, releasing millions of gallons of oil into the ocean and destroying huge swaths of the area's natural marine habitat and biodiversity.

It later turned out that the third mate had only slept for six hours in total over the two nights prior to the accident, making him less alert and less able to prevent the disaster.

Studies show that losing even a couple hours of sleep can make people more accident-prone.

In 2003, sleep psychologist Gregory Belenky studied the connection between reflexes and sleep deprivation by placing participants in different groups based on how much sleep they would get. For consecutive nights, some groups got three hours per night, others five, some seven and some nine.

Afterward, everyone was shown a series of patterns on a screen and told to push a button the instant a specific pattern appeared. In general, those who got more hours of sleep had much better reaction times. And even though the seven-hour group said they felt awake and alert, the results showed that their reflexes were far slower than those in the group that got a solid nine hours of shuteye.

### 5. Some people can thrive with a minimal amount of sleep, but they actually have a genetic mutation. 

You may have met people who brag about how little sleep they need in order to feel bright-eyed and bushy-tailed in the morning. Some of them might just be trying to impress you, but others may well be telling the truth.

In the 1970s, sleep psychologist Ray Meddis discovered a remarkable phenomenon after he was introduced to a 70-year-old woman, known as Miss M., who claimed she only needed one hour of sleep per night.

Miss M. was invited to Meddis's London lab, where they hooked her up to an EEG in order to observe her brain waves while she slept. However, due to all the attention she was getting and state-of-the-art scientific equipment around her, Miss M. was excited and wasn't able to fall asleep. It was only on the third night that she managed to get 90 minutes of sleep.

For the next several nights, the team monitored her sleep and found that her story was true; her sleep averaged out to about an hour per night, and she didn't need any more than that. Meddis's team thought Miss M.'s EEG brain scans might reveal some reason for this abnormality, but they looked completely normal.

Over the next few decades, researchers would discover that there are a number of people who need only a small amount of sleep and that these people thrive in the business world as entrepreneurs or CEOs.

The reason for this remained a mystery until 2009 when a research team from the University of California at San Francisco discovered that a specific gene mutation named DEC2 caused this reduced need for sleep.

After working with mice, the scientists were able to determine that the gene tends to show up in different members of the same family since it can be passed down from one generation to the next.

### 6. Centuries of fighting insomnia have produced some phony remedies, but there are helpful ones as well. 

If you've ever had trouble sleeping, you've probably realized that the harder you try, the less likely you'll be to actually drift off.

For centuries, people around the world have struggled to get to sleep and, as a result, have come up with a host of different tricks and tips for catching some shuteye.

One person who was always on the lookout for ways to help him fall asleep was the novelist and notorious insomniac, Charles Dickens.

He tried everything, from sleeping in the exact center of his bed to turning the bed so that it faced north, which was supposedly the most calming direction for the mind. But nothing seemed to help, so he often ended up roaming the city streets at night.

Victorian England was full of people like Dickens, which is why dozens of "miracle" cures were popping up, advertising their amazing powers against sleeplessness.

_Lodestones_ were one such remedy. They were magnets that people could place under their pillows to supposedly calm the mind.

Other dubious recommendations included sleeping with your legs elevated by a large pile of cushions, or wrapping a towel around your soapy hair and going to bed like this for two consecutive weeks.

Unsurprisingly, none of these remedies had the desired effect — but over the years, a few practical and effective tips have emerged.

One of the easiest and most important things you can do is avoid bright light from lamps, phones and computer screens before going to bed.

Even an hour in front of a bright light can prevent the increase of the hormone melatonin in your system, which fools the body into thinking it's still daytime and keeps you from entering that first key stage of sleep. In darkness, melatonin is released, which makes you drowsy and prepares the brain for sleep.

Another beneficial precaution you can take is making your sleeping environment as quiet as possible. If background noise is unavoidable, you can use white noise, such as recordings of ocean waves and rainforests, to create a soothing environment.

### 7. Sleepwalking is a mysterious condition that can have surprising and even dangerous results. 

Have you ever opened your refrigerator in the morning and found your leftovers were gone? Or maybe there's evidence that someone came into your kitchen and prepared an entire meal while you were asleep? Before you call the police to report a hungry burglar, you should know that there might also be a perfectly innocent explanation.

Sleepwalking is a mysterious phenomenon that almost always occurs in the deep sleep phases before rapid eye movement sets in.

While the causes of sleepwalking are unknown, a 2002 study showed that 60 percent of children with two sleepwalking parents will end up sleepwalking themselves, suggesting a genetic connection.

However, the condition often involves behavior that is stranger and more dangerous than making a sandwich at three in the morning.

One night, in June of 2005, the police and fire department of Southwest London came to the rescue of a 15-year-old girl who was found walking along the arm of a mechanical crane, 130 feet above the ground.

The girl's family lived near the construction site and, apparently, she had simply walked out and climbed the crane in her sleep before a neighbor luckily spotted her and called the authorities.

When the firefighters managed to bring her down using a hydraulic lift, they used her phone to call her parents who eventually managed to get the girl to wake up.

In another close call, in 2004, a man in Basingstoke, England was found bloody and inebriated at a pub after crashing his car into the traffic barriers of a nearby road. At first, the police were skeptical when the man insisted he'd been sleepwalking the entire time, but when they looked into his medical and family history, they found he was telling the truth.

But sleepwalking doesn't need to involve leaving the house: Robert Wood is a middle-aged man in Scotland who routinely gets up in the middle of the night to cook and eat a tasty meal.

This kind of sleep eating isn't uncommon and often leaves people puzzled about unexplained weight gain or missing food from their refrigerator.

### 8. There’s evidence that we can be influenced during sleep through repeated words and phrases. 

If you've ever wished you could break an annoying habit, you might be able to do it in your sleep.

In 1942, psychologist Lawrence LeShan observed a group of boys at a New York summer camp in order to conduct a sleep experiment. He split the camp into two groups: those who bite their nails and those who didn't. The nail-biters were then split into two groups, one being a control group that would not receive any sleep suggestion, and one that would.

Then, each night, LeShan quietly entered the room of one nail-biter group and played a recording while they slept. The voice said, "my fingernails taste bitter," 300 times in a row. At one point, the phonograph machine broke, so LeShan had to go into the room and whisper the phrase hundreds of times himself.

Eventually, LaShan had the results he was looking for: when camp ended, around 40 percent of the boys who slept in the room with the repeated phrase had stopped biting their nails. In the control group, which didn't receive any sleep suggestions, all of the boys were still biting their nails.

Even though LaShan's experiment was the first empirical evidence to show a link between sleep and learning, Buddhist monks have a long tradition of using similar methods. To help them learn sacred texts, novice monks would often be whispered to when they slept.

Sleep-learning has been successfully used for other purposes as well, including prisoner reform.

In the 1950s, the warden of California's Tulare County Jail placed tiny speakers underneath inmates' beds, and when the prisoners slept, the speakers hypnotically repeated phrases like, "you will no longer drink alcohol."

Remarkably, the technique resulted in a 50 percent success rate in changing behavior.

### 9. Dreams can play an important role in coping with unpleasant experiences and moving on with your life. 

You likely wouldn't consider a nightmare about being trapped on an examination table and cut open by an evil doctor to be a good thing — but there's a reason we have such graphic or terrifying dreams.

Dreams are an important way for us to process our life experiences, especially when they're difficult or traumatic.

In 1972, psychologist Richard Greenberg conducted an experiment that shows how dreams can relieve trauma.

Two groups of participants watched a gruesome movie involving a bloody autopsy before going to bed. While everyone was woken up five times during the night, the first group was awoken while they were dreaming, whereas the second group was roused before they entered their dream stage.

The next day, participants viewed the film a second time and were asked to describe their reaction. Remarkably, all of the participants who were kept from dreaming expressed being much more disturbed by the movie than those who had dreamed.

The results of Greenberg's experiment support the belief that dreaming can help us process traumatic events.

In addition, dreams can also play an important role in our emotional balance.

During the 1970s, Rosalind Cartwright, a psychologist from Rush University in Illinois, was researching women who suffered from depression after a divorce.

When she checked back in with her subjects after a year or two, she found that the women who experienced weeks or months of intense bad dreams had overcome their depression.

Conversely, when examining the group of women who didn't have traumatic dreams after the break-up, Cartwright found that these women were either still suffering from depression, or had been depressed for much longer that the first group.

This isn't to say that nightmares are a reliable cure for depression. Bad dreams can also keep us from getting a good night's sleep, which is a cornerstone of our overall health and well-being.

### 10. Final summary 

The key message in this book:

**Sleep isn't just the domain of your unconscious thoughts, it's a fluid, complex and fascinating process that helps us process and cope with difficult emotional experience and our everyday lives. Likewise, sleep is of vital importance for our regenerative capabilities — both mental and physical.**

Actionable advice:

**Try using white noise in your bedroom.**

It can be very difficult to insulate your bedroom against all noise, which is why a good alternative is to cover external sounds by playing a recording of white noise, like say ocean waves lapping, while you sleep. Such soundscapes are available on Youtube, and you can even buy a machine expressly for generating this ambient noise.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Quirkology_** **by Richard Wiseman**

_Quirkology_ (2007) takes a uniquely scientific look at some common questions that are often dismissed as trivial: What kind of impact does astrology have on our lives? Is the number 13 really unlucky? Can a joke truly be harmful? And more!
---

### Richard Wiseman

Richard Wiseman is a professor at the University of Hertfordshire and one of Britain's most acclaimed psychologists. He is celebrated for making complex concepts accessible to the general public through his popular YouTube videos and his best-selling books, such as _59 Second_ s and _Quirkology_.

