---
id: 52b89892373766000c1f0000
slug: no-logo-en
published_date: 2014-01-01T13:57:30.834+00:00
author: Naomi Klein
title: No Logo
subtitle: None
main_color: ED3F30
text_color: BA3126
---

# No Logo

_None_

**Naomi Klein**

_No Logo_ takes a look at how the power of brands has grown since the 1980s, and how companies have emphasized their brand image rather than their actual products. _No Logo_ shows how this strategy has affected employees in both the industrial and the non-developed world. _No Logo_ also introduces the reader to the activists and campaigners who are leading the fight back against multinationals and their brands.

---
### 1. Brands are obsessed with appearing cool; therefore, they capture and mine youth subcultures for their ideas. 

A brand can succeed or die depending on whether or not it is considered cool. Companies therefore spend vast amounts every year trying to find out what is seen as cool and then incorporating this into their brand.

This fixation with coolness stems from the companies' overreliance on the youth market to generate sales. In previous decades, baby-boomers drove the consumer economy, but during the recession of the early 1990s they began to seek cheaper alternatives to high-end brands. This forced brands to find new customers, and so they turned towards the growing teenage population.

To properly target teenagers, companies analyzed youth cultures and incorporated the traits that were considered cool into their brand images. Aspects of traditionally alternative subcultures in music and fashion, such as punk and grunge, were appropriated by brands. Even rebellious characteristics like 'retro' and 'irony' were turned into marketable commodities.

Consider for example hip-hop and Black culture. The break out of hip-hop artists in the 1980s led to the style becoming popular with young people throughout society. Brands such as Nike and Tommy Hilfiger were able to ingratiate themselves into the movement by sponsoring artists and sports stars and engaging in aggressive marketing that pushed their image to center stage. The strategy was so massively successful that brands now help to dictate the development of this subculture and directly influence which products are considered cool. Black culture and identity has been captured by the brands and transformed into a profit-generating phenomenon, and Black communities are forced to follow where the brands lead.

**Brands are obsessed with appearing cool; therefore, they capture and mine youth subcultures for their ideas.**

### 2. Brands must continuously grow and regenerate, otherwise they will die. 

We spend our entire lives surrounded by brands. Our streets and public spaces are peppered with advertisements, our sporting events and heroes sponsored by brands, and even the clothes we wear are often covered by brand names and logos.

Brands are ubiquitous in our culture precisely because, to survive, they need to be constantly and aggressively advertised and marketed. They must constantly align themselves with changing demographics and new trends, such as the rise of alternative music in the early 1990s, otherwise customers become disinterested and the brands die. As one advertising executive stated, "Consumers are like roaches — you spray them and spray them and they get immune after a while." Levi Strauss, once considered to be one of the coolest brands, failed to improve and update its image and marketing strategy and suffered an alarming slip in sales while its competitors moved forward.

Brands find themselves in this powerful yet precarious position because their success depends far more on the popularity and 'coolness' of their name than on their actual products.

Hence, to survive, a brand must be highly visible in every area of society. It has to connect to consumers in every sphere of life and on multiple levels, and it must continuously renew these connections. In schools and universities, for example, an increasing number of brands are not only sponsoring sports equipment, canteen spaces and the like; they are also getting involved in the curriculum itself. Brands sponsor research grants and even have their products appear as examples in exam questions.

**Brands must continuously grow and regenerate, otherwise they will die.**

### 3. The success of a brand depends very little on the actual product itself, and far more on brand identity. 

In the late 1980s, it was widely thought that the era of the branded product was over. Price, rather than brand loyalty and image, seemed to be the method by which consumers were increasingly choosing their purchases. The 1990s, it was believed, would be the decade of 'value,' where consumers focused on economic rationale.

Instead, brands dominate today's society in unprecedented ways. Far from shriveling into nothing, brands have, on the contrary, dramatically increased their power and prestige.

Behind this re-emergence and remarkable growth lies a crucial change in marketing and advertising. Rather than concentrate on their products, which could always be sold cheaper by competitors, companies sought to gain the advantage by pouring money into marketing, research, and cultivating their brand image. Today a brand's success and possible market dominance depend on the 'coolness' of its name and logo rather than on what it actually sells.

The most successful brands are concept-driven and thus appeal to people on a more emotional and spiritual, rather than rational, level. Instead of being identified with a certain product, the brand aims to be known for a set of values it supposedly stands for.

Nike, for example, spends very little on the manufacturing of their products compared to the amount it ploughs into advertising, sponsorship deals and marketing. Rather than just selling shoes and sports equipment, it promotes itself as an enhancer of people's lives through sports and healthy lifestyles. It has even positioned itself as an organization that empowers women and Black people, a concept far removed from merely selling sneakers.

**The success of a brand depends very little on the actual product itself, and far more on brand identity.**

### 4. Brands use aggressive tactics to expand their market share and crush competition. 

There are very few towns in America that don't have a Wal-Mart or Starbucks. In the past few decades, these incredibly successful firms have grown spectacularly quickly. In 1986, Starbucks had only a few cafes in Seattle; by 1999, they had over 1,900 stores across the globe.

The fantastic success of such brands largely stems from aggressive business models that allow them to capture huge swathes of the market and ruthlessly eradicate rivals. The business models of major brands almost always depend heavily on economies of scale but differ in their approaches to expansion, depending on their particular market.

For example, _The Wal-Mart Model_ relies on two principles: price and size. Huge stores are built on cheap land on the outskirts of urban centers and filled to the brim with stock. The sheer volume of goods purchased forces suppliers to drastically lower their prices, and the products can then be sold to consumers for much less, forcing competitors out of business.

_Clustering_, the method favored by Starbucks, is when a brand swamps an area with clusters of its stores. The aim is to saturate the market to the extent that even the brand's own stores take customers from each other. Huge companies can afford to take losses in some stores, whereas independents and smaller companies cannot.

_The Branded Superstore_ is popular with high-end brands like Diesel and Tommy Hilfiger. Brands build huge flagship stores on prime locations in the centers of towns. These are part-store, part-theme-park and 100% advertisements for the brand. They often operate at a loss but provide celebrity exposure to the brand, hence entrenching it in the consumer's mind.

**Brands use aggressive tactics to expand their market share and crush competition.**

### 5. Multinationals outsource their manufacturing to developing countries with devastating consequences for workers there. 

Over the last few decades, many multinational companies have followed what is known as the 'Nike Model' of manufacture. Basically, in order to save on labor costs, firms have shut down their factories in Western countries and _outsourced_ their manufacture to the less-developed world where labor is much cheaper.

The workers who make the products that were previously produced in Western factories are contractors rather than employees; hence, the multinationals have no responsibility for them.

Most of the outsourced manufacture takes place in 'Export Processing Zones.' These are specifically designated areas within less-developed countries where income and export taxes are suspended to encourage investment. However, to make the zones desirable, many domestic governments go even further by eliminating minimum wages, labor laws and union rights.

The employees who work in these zones, the majority of whom are young migrant women, find themselves in a kind of legal limbo. They are neither the responsibility of the corporations whose factories they work in, nor are they protected by standard domestic law. To minimize labor costs and maximize productivity, they must endure awful working conditions and accept ever smaller wages. Women in Mexico, for example, must prove they are menstruating and therefore not pregnant, because pregnancy will result in them having to take time off. Those who are found to be carrying a child or who refuse to be checked are sacked. Wages are always incredibly low, in some cases a mere $0.13 an hour.

Far from being beneficial to less developed nations, Export Processing Zones in fact facilitate the exploitation of workers.

**Multinationals outsource their manufacturing to developing countries with devastating consequences for workers there.**

### 6. The outsourcing of jobs has also had a negative impact on employees in the Western world. 

Traditionally, a large multinational company would directly employ the vast majority of its workforce to manufacture the company's products. The unionization of the company's workforce enabled a large proportion of its employees to have relatively well-paid, full-time permanent jobs. Working conditions were good, and many employees and employers felt loyalty toward one another.

The policy of outsourcing changed all this in the 1980s. Multinationals realized that they could dramatically cut labor costs by employing overseas contractors to manufacture their goods instead of using the factories at home. As branding became more important than the products themselves, companies preferred to spend their money on advertising and marketing rather than on manufacturers.

As domestic factories began to close and manufacturing jobs shifted overseas, the very nature of Western economies changed dramatically: manufacturing jobs were replaced by a massively different type of employment in the service sector. These new service jobs, which became known as 'McJobs,' were far more likely to be part-time, non-contracted and poorly paid.

In many workplaces, union membership is discouraged, "trouble-makers" are fired, and work satisfaction is very low. Companies prefer to hire young employees as they are generally cheaper and less likely to make demands regarding working conditions and labor laws. Costs are kept even lower by contracting staff through 'temp' agencies rather than employing them directly, hence depriving the workers of many employer benefits.

This has led to a workforce which is deeply cynical and which neither feels loyalty toward employers, nor expects any in return.

**The outsourcing of jobs has also had a negative impact on employees in the Western world.**

### 7. The increasing dominance of certain brands will lead to limitations in consumer choices. 

In 1994, the film production company Viacom purchased Paramount Pictures and Blockbuster Video. This gave Viacom an enormous advantage because it could profit when its films played in Paramount cinemas and also afterwards when they came out on video.

This is known as _synergy_, where corporations come together to complete a branded loop comprising a range of interconnected brands. From mergers and takeovers to cross-marketing initiatives such as McDonald's handing out Disney toys with meals, synergy pushes brands into new media and markets.

As brands attempt to push their ubiquity further and further through synergy and aggressive expansion policies, the consumer could be faced with a shrinking range of choices.

This is because firms that have a dominant position in their market, such as Wal-Mart, can impose their own form of _corporate censorship_. Wal-Mart, which plays heavily on its family image, can and does refuse to stock music and magazines that could upset a family audience. Wal-Mart can even force artists to comply. Even the immensely popular band Nirvana was forced to alter their songs and artwork to appease Wal-Mart.

A brand's obsession with protecting its image means it also seeks to control every aspect of its marketing. When sponsoring events or advertising in magazines, it seeks to regulate the other content adjacent to its own.

Through its takeovers, Viacom now has an enormous amount of power. It can control, to its own advantage, which films play in the cinema and which films are prominently displayed in its video stores. It can also severely limit the exposure of other producers' films, giving itself a near-monopoly status and leaving the customer with a narrower selection.

**The increasing dominance of certain brands will lead to limitations in consumer choices.**

### 8. In expanding their power and profits, brands have inadvertently made themselves the main targets for activists. 

Brands can often find themselves the targets of activist campaigns protesting, for example, their use of sweatshops, unecological practices or unethical advertising.

Certain activist groups attach themselves like leeches to a brand and follow their practices across the globe, bringing the brand's actions to wider public attention.

In the past, activists largely considered companies as neutral entities caught up in wider political conflicts, but now they are increasingly seen as the direct cause for suffering across the globe. Brands have overtaken governments as the main focus for criticism, mostly due to their increased dominance of our cultural, political and economic lives since the 1980s.

The enormous economic power that multinationals possess through their brands has led to them gaining increased political power, too. Huge corporations are increasingly becoming the major political entities in the world, influencing and even controlling national governments. Multinationals, however, possess this enormous amount of power without being constrained by the legal and electoral framework that controls and oversees governments. Activists campaign because they feel that these corporations must be held to account.

As brands have grown in influence, they have become an increasingly prominent part of our everyday lives — in many ways, they are the dominant iconography of our times. We tend to think of our lives in branded terms and are increasingly likely to form emotional attachments to certain brands. This means that when brands are accused of malpractice or unethical behavior, we are more likely to feel emotionally responsible ourselves and more likely to speak out against them.

**In expanding their power and profits, brands have inadvertently made themselves the main targets for activists.**

### 9. Targeting and subverting a specific brand can be an effective method for criticizing a multinational. 

Brands wield a tremendous amount of cultural power in modern society. They battle for our economic and cultural attention in many areas of society and continuously push themselves into new spheres of our lives.

Due to this constant brainwashing, brand names and their logos have burned themselves onto our consciousness. Certain brands and their images, slogans and logos are known by almost everyone. Yet, for companies, there is also a negative side to this cultural ubiquity.

Brands can be subverted and their popularity and omnipresence used against them to frame criticisms of the multinationals. Activist campaigns can be made relevant to people by attaching them to a well-known brand, severely damaging its image.

The many campaigns against Western multinationals' use of sweatshops — the very cheap, unregulated labor in the Third World — have been given an extra kick by tying the campaigns to brands. Taking a branded piece of clothing and highlighting the conditions of the workers who made it can provide a relevance to Western consumers who can then boycott that brand until they improve their practices.

Another way in which brand identities can be subverted is through so-called _culture jamming_, where activists use a brand's own dominance of public space against them. Culture jammers will often use a spray can and a stencil to subtly change advertisements and other branded images to represent another point of view. Slogans can be rewritten and images touched up to provide a subverted meaning. For example, Nike's slogan 'Just do it' could become 'Justice, do it Nike' or 'Just don't do it.'

**Targeting and subverting a specific brand can be an effective method for criticizing a multinational.**

### 10. Final Summary 

The key message in this book:

**Since the 1980s, the power and influence of brands has grown massively. Brand image and identity, as well as brand promotion through advertising and marketing, has taken over as the driving force behind a company's power. This policy has given brands unprecedented power, and activists have realised that the best way to improve the global economic system is to target brands directly.**

The questions this book answered:

**Why have brands become so powerful and dominant in our economy and our culture?**

  * Brands are obsessed with appearing cool; therefore, they capture and mine youth subcultures for their ideas.

  * Brands must continuously grow and regenerate, otherwise they will die.

  * The success of a brand depends very little on the actual product itself, and far more on brand identity.

  * Brands use aggressive tactics to expand their market share and crush competition.

**What are the results of the triumph of marketing and image over manufacturing and products?**

  * Multinationals outsource their manufacturing to developing countries with devastating consequences for workers there.

  * The outsourcing of jobs has also had a negative impact on employees in the Western world.

  * The increasing dominance of certain brands will lead to limitations in consumer choices.

**Why and how do some people protest against the power of multinationals and their brands?**

  * In expanding their power and profits, brands have inadvertently made themselves the main targets for activists.

  * Targeting and subverting a specific brand can be an effective method for criticizing a multinational.
---

### Naomi Klein

Naomi Klein is an award-winning Canadian author and journalist who has written for various publications including the _New Statesman, The New York Times_ and _Newsweek International._ Along with _No Logo,_ which was shortlisted for the Guardian First Book Award, Klein also wrote _The Shock Doctrine: The Rise of Disaster Capitalism._

