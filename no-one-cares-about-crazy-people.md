---
id: 5a5ca38cb238e10007f154b0
slug: no-one-cares-about-crazy-people-en
published_date: 2018-01-18T00:00:00.000+00:00
author: Ron Powers
title: No One Cares About Crazy People
subtitle: The Chaos and Heartbreak of Mental Health in America
main_color: A5212D
text_color: A5212D
---

# No One Cares About Crazy People

_The Chaos and Heartbreak of Mental Health in America_

**Ron Powers**

_No One Cares About Crazy People_ (2017) takes a detailed look at the history of mental-health care in America. These blinks explore the current state of psychiatric care, how it came to be the way it is and how the changing trajectory of treatment has affected both patients and society.

---
### 1. What’s in it for me? Learn the dark story behind America’s approach to mental illness. 

Depression, anxiety, schizophrenia — mental illness can mean many things and take many shapes, from the depressive feelings of struggling teens to the overwhelming paranoid thoughts of those with severe schizophrenia. But despite the fact that mental illness is exactly that — an _illness_ that requires medical treatment — Americans struggling with mental illness have been funneled out of hospitals and into the country's prisons and streets.

It all started back in the 1970s, when US mental-health policy took a major turn for the worse, a turn that's resulted in suffering for patients, terror for families and a sizable financial burden for taxpayers.

In these blinks, you'll learn

  * how one court decision sealed the fate of people with mental illness;

  * why freedom isn't always ideal; and

  * how mental illness packed America's prisons.

### 2. Schizophrenia is a mental disorder caused by genetic and environmental factors, and it can be devastating. 

Schizophrenia can be an incredibly debilitating mental illness. It radically alters the brain, and may diminish a person's ability to function within society. Certain genes that may contribute to the onset of the disorder exist in the brains of people with schizophrenia from birth, but symptoms usually don't present until adolescence, when the brain begins _pruning_ cells.

This pruning process, which tends to occur during a person's late teens and early twenties, destroys particular _cortical synapses_, the brain structures that facilitate communication between neighboring cells. Destroying these synapses is the brain's way of cleaning house and making space for new synapses crucial to adult life.

But there's a problem. When the old synapses are pruned, the genes involved in schizophrenia come to life, filling in the gaps. If these genes are then activated — by stressful environmental conditions, for instance, or marijuana abuse — the affected person enters what's known as the _prodromal phase_ of schizophrenia. During this phase, no major behavioral differences are necessarily discernible. However, this phase often leads to a psychotic breakdown, which may manifest in a number of ways, including paranoid delusions or delusions of grandeur. This often grabs the attention of friends and family, making them, and the person affected, painfully aware of the disorder's existence.

That all being said, the onset of schizophrenia is always the result of external triggers. Stress is one of the most common, so it's no coincidence that schizophrenia often dawns as a young mind begins facing serious stress for the first time, whether it be related to school, socializing or a first heartbreak. Any and all of these can jumpstart the prodromal phase of the disorder.

Another major external trigger is cannabis use, which can predispose a person to all manner of psychotic disorders — including schizophrenia — if the genes for those disorders are present. A recent article published in the _Wall Street Journal_ cited research by many prestigious medical journals, like the _Lancet_, which confirmed the link between cannabis use and psychosis.

### 3. Mental illness has been de-legitimized and denied, resulting in horrific suffering for patients. 

Have you ever heard the name Thomas Szasz? This prominent 1960s psychiatrist saw the treatment of mental illness as authoritarian and inhumane. So he set out to prevent others from interfering in the lives of those with mental illness.

His influential book _The Myth of Mental Illness_, published in 1961, did much to increase people's skepticism of the existence of mental illness. In the book, Szasz claimed that what was commonly called mental illness was merely a set of pathologized behaviors that society found annoying and inconvenient; according to Szasz, the mind — unlike the brain — is intangible. Therefore, mental illness cannot be regarded as something that is tangibly "wrong".

This book and the theory it espoused were so influential that they contributed to a wholescale decline in the number of medical professionals who chose mental health as their specialty.

In 1975, the Supreme Court made into law what Szasz had written. Suddenly, it was a violation of civil liberties to hospitalize or medicate a person with mental illness without her consent.

Today, one can't help a person suffering from psychosis without first obtaining either a court order or consent from the person herself. However, it can take months to receive such an order. And the great irony here is that most people diagnosed with schizophrenia find it nearly impossible to recognize that they need help in the first place. Naturally, such a symptom means they rarely seek out treatment themselves.

This is only complicated by the fact that long periods of untreated psychosis tend to exacerbate a person's condition. During such periods, schizophrenia takes an even deeper root in the brain.

Despite the obvious danger of these outcomes, Szasz co-founded the Citizens Commission on Human Rights in 1969 to push for his perspective. The group remains active to this day and both it and Szasz's opinions have been extremely influential in casting doubt on the legitimacy of the field of psychiatry and how mental illness is viewed within it.

### 4. The widespread closure of mental-health institutions resulted in dire conditions for patients. 

Freeing Americans with mental illness from mental institutions was, supposedly, a well-intentioned political move. However, it wasn't sufficiently thought through and its implementation was an absolute disaster. Furthermore, the simultaneous advent of "wonder drugs" further encouraged what, in the end, proved to be an ill-advised mass deinstitutionalization.

Here's the story:

In the 1960s, a drug called Thorazine was touted as a cure for schizophrenia. So when psychiatric institutions closed, their patients were sent off with a shot of the stuff and expected to be in the clear. But the truth is that there isn't a cure for mental illness; drugs like Thorazine can stabilize patients, but they can't make their illness vanish.

Despite this fact, since deinstitutionalization began, pharmaceutical companies have persistently capitalized on the natural desperation for a cure, as well as the general lack of public knowledge surrounding mental illness. These companies have paid out countless settlements for advertising that falsely promised to cure mental illnesses, which, for them, is a better option than telling the truth. After all, the money they're making far exceeds what they end up paying in legal fees. Since no pharmaceutical CEO has ever faced jail time for such an offense, it's just a simple economic calculation.

But this sorry state of affairs wasn't inevitable. In fact, there was supposed to be state-level community care and support for people with mental illness after they were released from institutions.

This plan was laid out in 1963, when President John F. Kennedy signed the Community Mental Health Act, which set aside $150 million for the opening of community mental-health treatment centers across the nation.

The implementation of the program began well, but the Vietnam War stopped it dead in its tracks and funding was slashed. As a result, by 1973, less than half the proposed centers had been built, and the institutions were closed nonetheless. Some 280,000 patients were discharged from institutions and they had nowhere to go. Americans with mental illness were on their own, with nobody to care for them and no tools to care for themselves.

### 5. Americans with mental illness have been funneled onto the streets and into prisons. 

So Americans with mental illness no longer have institutions to care for them. And the consequences have been disastrous. Deinstitutionalization has resulted in the exponential growth of an extremely marginalized group of Americans: the homeless.

Here's how:

After mental institutions were closed, the patients they once housed were sent out into the world. Naturally, they were incapable of fending for themselves; they failed to get work, couldn't support themselves and couldn't even deal with the bureaucratic red tape involved in receiving government assistance.

So, with no other recourse, they slept on the streets, frightening people with the symptoms of their untreated mental illnesses. From there, it wasn't long before many resorted to criminal activity to get by.

In other words, the inhumane fact that people with mental illness cannot receive medical treatment against their will means that many of them commit crimes to support themselves. They're then arrested and, after failing to receive a proper diagnosis, are thrown right in with the rest of the prison population.

If that wasn't horrible enough, they're also often punished with one of the most damaging experiences that can be imposed on a person who's struggling with mental normativity: solitary confinement.

In addition to being incarcerated en masse, people with mental illness are also at high risk of becoming victims of police brutality. In fact, there are numerous instances of police officers shooting to death people who are in a psychotic state.

One example is the case of James Boyd, a man diagnosed with paranoid schizophrenia. He drew the attention of an irritated bystander on the street by talking to himself and acting erratically. When police arrived, they shot him dead, despite the fact that he was unarmed. Following the incident, two of the officers were indicted for murder.

> _"Reports of mental health problems among state prison inmates have reached a rate of 52%."_

### 6. Mental health should be a priority of our society. 

Mental illness is a hidden threat, easy to evade until it comes knocking on your door. But turning a blind eye is no solution and, by taking concrete action, we can protect countless people from ever enduring this trying experience.

The first step is to make serious efforts to diagnose mental illness early on. For the author, this step is personal; he lost a son to suicide brought on by a schizo-affective disorder that causes a frightening combination of hallucinatory episodes and manic depression. If his son had received an earlier diagnosis, it's possible that the disease could have been stopped before it became so aggressive.

The author's other son currently lives with schizophrenia, and the author can attest to how stressful it was to have to wait until his son posed a threat, either to himself or to others, before being able to hospitalize or medicate him. Naturally, the family was terrified of losing yet another son to suicide. Luckily, they didn't, but if he'd been diagnosed sooner, he probably wouldn't have reached the dangerous stage of psychosis.

But beyond early diagnosis, we also need to see the universal benefits in caring for people with mental illness. For example, the National Alliance on Mental Illness has estimated that the $50,000 it costs, per sentence, to jail a person with mental illness could be avoided by spending a mere two or three thousand on treatment, thereby saving thousands of tax dollars.

Or take the psychiatrist Courtney Harding. In the 1950s, she found that patients who didn't respond to medication alone derived tremendous benefits from being mentored on how to integrate into the community and develop self-confidence. After Harding's study, 51 percent of participants were still doing well years after this intervention.

Based on this success, Harding developed a method called _psychosocial rehabilitation_, a clear departure from the traditional psychiatric approach of medication, maintenance and stabilization. Her treatment, unlike the more common ones, results in patients who can actively and positively contribute to society.

And finally, we must remember that people with mental illness often have no voice of their own. It's therefore incumbent upon all of us to speak up for them and their families.

### 7. Final summary 

The key message in this book:

**For decades, those with mental illness have been sidelined by society and treated as disposable. While the damaging effects of decades of poor mental-health policy are nearly impossible to turn back, we can raise awareness about mental illness, advocate for those affected and begin moving in the right direction.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _An American Sickness_** **by Elisabeth Rosenthal**

_An American Sickness_ (2017) takes an honest look at the state of the American health-care system and frankly diagnoses its many ailments. When big business started taking over what were once charitable organizations, things began to go truly wrong. Rosenthal presents valuable information on how to reduce health-care bills and not get taken for a ride by greedy hospitals and over-prescribing doctors.
---

### Ron Powers

Ron Powers is a celebrated novelist and journalist. In 1973, he was awarded the Pulitzer Prize for Criticism, and, in 2000, he co-authored _Flags of Our Fathers_, a _New York Times_ best seller. He has a deeply personal perspective on mental illness.

