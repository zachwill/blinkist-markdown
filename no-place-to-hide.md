---
id: 53c628523839340007210000
slug: no-place-to-hide-en
published_date: 2014-07-15T00:00:00.000+00:00
author: Glenn Greenwald
title: No Place to Hide
subtitle: Edward Snowden, the NSA and the Surveillance State
main_color: C8282A
text_color: C8282A
---

# No Place to Hide

_Edward Snowden, the NSA and the Surveillance State_

**Glenn Greenwald**

In _No_ _Place_ _to_ _Hide,_ author Glenn Greenwald details the surveillance activities of secret agencies as according to information leaked by American whistleblower Edward Snowden. Rather than serving as a means to avoid terrorist attacks, as the US National Security Agency (NSA) claims, Greenwald explains that these dubious activities instead seem to be a guise for both economic espionage and spying on the general public. _No_ _Place_ _to_ _Hide_ also brings to light the media's lack of freedom in detailing certain government and intelligence agency activities, and addresses the consequences whistleblowers face for revealing secret information.

---
### 1. What’s in it for me? No matter who you are or what you do, government spying affects you. 

The name Edward Snowden, a former contractor for the NSA and the Central Intelligence Agency (CIA), has been mentioned often in the media. Since he leaked a huge cache of government documents to journalist Glenn Greenwald in June 2013, worldwide news outlets have kept up a steady stream of reports outlining the depth and extent of spying activities by intelligence agencies in the world's most advanced democracies.

_No_ _Place_ _to_ _Hide_ delves into the content of Snowden's leaks and explains how the revelations affect all of us. Author Glenn Greenwald fervently argues against surveillance and the lack of support in the mainstream media for whistleblowers, while fighting for freedom and privacy from his standpoint as an experienced independent journalist and constitutional lawyer.

In the following blinks, you'll discover:

  * how data related to a phone call, like a phone number or call frequency, can reveal more about you than the content of a single call,

  * how ineffective the government's ability to conduct mass surveillance activities appears to be in preventing terrorism, and

  * how our opinions regarding something like drugs can change depending on who's listening.

### 2. Individual privacy fosters who we are and what we believe in politically. 

Behind closed doors and among friends, are your opinions and behaviors different to those you might reveal in public, in particular to certain people such as your pastor, a policeman, your parents or employer? Most of us would answer, "Yes."

Privacy is crucial to our sense of freedom. When it is compromised, our individuality suffers and we tend to instead behave in a way that is _expected_ of us.

When we know we are being observed, we alter our behavior and become more fearful. We can see this, for example, in a 1975 Stanford University experiment. This study revealed that 77 percent of participants were in favor of legalizing marijuana. However, when they were told their statements were to be viewed by police for "training purposes," the figure in favor plummeted to 44 percent.

This shows that surveillance significantly affects how we think, especially when it comes to political standpoints.

It is important to have political dissent to maintain a healthy democracy. However, governments have throughout history spied on citizens who dare to go against the status quo.

In recent years, the internet has provided a platform for people to share knowledge and protest authority. It has enabled movements, such as Occupy Wall Street, to stand against gross abuses of power.

Unfortunately, that a government spies on political dissenters is no new discovery. As far back as 1971, the US Federal Bureau of Investigation (FBI) monitored various political groups, such as those who were anti-war and those involved in the civil rights movement. Shockingly, the FBI even infiltrated some of these groups, posing as members, and tried to influence others to commit crimes.

The resulting paranoia from this covert behavior was sufficient to curb political dissent and subsequently the growth of political movements.

As we'll see in the following blinks, spying by intelligence agencies impacts our lives, diminishing our privacy and our freedom.

> **Fact:  

** In the 1970s, the FBI labeled 500,000 US citizens as potential subversives, including John Lennon.

### 3. The NSA leaks were published amid great risks for both the whistleblower and the reporter. 

The Snowden leaks — one of the most significant government leaks ever — began with a considerable leap of faith for author Glenn Greenwald. He suspected that the person who had contacted him could be a fantasist or even part of a government trap, used to bring down whistleblowing journalists.

But the person reaching out was Edward Snowden. And Snowden had planned everything carefully, despite the likelihood of being caught.

Snowden is a cybersecurity expert who had worked for the CIA and as a private contractor for the NSA. Over the years he had changed jobs, working in different countries and accepting lower pay, in order to gather classified documents.

He had combed through the files before presenting them to the author, knowing full well that he faced life in prison for doing so. The documents Snowden handed over were the first in NSA history to be leaked.

After the leaks, Snowden needed a secure place to go, so he travelled to Hong Kong, which has more political freedom than mainland China, and was comparatively safe from US interference.

Following initial meetings, the discussions Snowden had with journalists, newspaper editors and lawyers threatened to thwart the documents being published at all.

Snowden had agreed to hand over some of the NSA documents to a _Washington_ _Post_ journalist, then regretted this after the paper assembled a team of lawyers, who began to intervene and issue warnings. Fearing potential consequences, the newspaper also barred its journalist from meeting Snowden face to face.

The author's employer, UK-based newspaper _The_ _Guardian_, also enlisted lawyers and was concerned about being shut down as by law, the newspaper had to inform the NSA of its actions. Subsequently, the NSA attempted to stall the leaks and claimed that the paper's editors were not serious journalists.

In the end, it was the courage of Snowden, the author, _The_ _Guardian_ editors and other independent reporters that led to the published leaks which quickly captivated the world.

> _"How will we know it's him?" said the author, referring to Snowden. "He'll be carrying a Rubik's Cube."_

### 4. Snowden forfeited his freedom to fight the oppression and invasion of privacy of the public. 

When Snowden emailed the author in December 2012, he refused to provide further details until Greenwald had installed an encryption program for his web chat and email. Snowden knew what he was doing, as his awareness of state surveillance activities warranted the need for secure, encrypted communication.

Once the secret NSA documents revealing data collection targets were given to Greenwald, his communications with journalists were conducted using encryption technologies.

When Snowden and Greenwald met, Snowden insisted the battery of Greenwald's phone be removed or placed in the freezer, as intelligence services could remotely activate the phone and use it as a listening device.

Snowden's actions showed that he had chosen to live with the repercussions of being one of the most significant whistleblowers in history.

One of Snowden's goals was to allow the documents speak for themselves. He never tried to conceal his movements because he knew he'd be discovered and didn't want anyone else to take the blame for his actions.

When his identity was exposed, he was staying in a five-star hotel in Hong Kong under his own name.

So why did Snowden leak the documents?

He thought of the internet as an extension of himself and his personality — a place to discuss media, to make friends and form opinions on politics, philosophy and so forth.

As we've seen, when you are aware that someone is observing you, you are less likely to express your opinions, especially if they might be considered radical.

Snowden wanted to rally against surveillance — on the internet or elsewhere — that served as a way to maintain compliance.

This struggle for freedom on the internet and to inform the general public of what was being done in its name was so meaningful to him that he gave up his well-paid senior job, his girlfriend and family to do so.

> _"I call the bottom bunk at Gitmo." -Snowden to Greenwald in Hong Kong, referring to Guantanamo Bay_

### 5. Government agencies, including the NSA, collect email and telephone records, invading the privacy of innocent people. 

So what exactly does the NSA do? They collect intelligence for the United States, for homeland defense and anti-terrorism purposes. But this may entail more than you think.

The NSA also gathers the phone metadata of all US citizens, snooping on millions of regular people who pose no threat to security. This is because a secret court permitted the NSA to gather communication details of US citizens, every single day.

Take telephone communication, for example. Verizon, one of the largest telephone companies in the US, handed its metadata to the NSA. Although metadata doesn't contain call content, it does consist of details such as phone numbers, call duration and time of call.

The NSA claims that collecting metadata isn't surveillance but is necessary to protect against terrorism. However, metadata can be more telling than the contents of a single phone call, and is seldom related to terrorism.

Although an NSA analyst wouldn't know the content of a call, they would, for instance, be informed when someone called a sexual health clinic, then their health insurance, and then an AIDS clinic.

In addition to phone logs, data is also collected from the servers of the largest internet companies.

Documents show that a program called _Prism_ involved the world's biggest technology companies creating methods by which the NSA could extract the content of users' internet activities.

For example, Facebook and Google provide metadata and the content of private messages, chats and web searches, and Microsoft relinquishes data related to Skype calls.

But what about privacy rights? Surely the NSA needs a warrant to look into US citizens' private communications?

Well, yes. Surprisingly, however, the NSA doesn't need a warrant to gather communication details from non-US citizens or between a non-US citizen and a US citizen.

It's fairly clear that the NSA encroaches on the rights of citizens. And it's no surprise that this has caused controversy.

### 6. The NSA spies on foreign governments, even some US allies, for political and economic advantage. 

It's not only domestic surveillance that has brought infamy to the NSA. It's also known that the agency spies on foreign governments and organizations. These activities extend far beyond the NSA's focus on homeland security. There is also evidence that the NSA has conducted surveillance on political leaders and international organizations.

For instance, while spying on the United Nations, the NSA learned the secretary general's talking points prior to his meeting with US President Obama. They also learned the voting intentions of members of the UN Security Council. This kind of information could assist the United States in communicating with other nations in order to sway a vote in their direction.

In addition, Brazilian President Dilma Rousseff's personal communications were targeted by the NSA, and text messages to and from Mexico's presidential candidate (now president) Enrique Pena Nieto were collected and scrutinized. This interception could provide the United States with advantages in Latin America, seeing as Brazil and Mexico are major oil producers with substantial influence in this sector.

Although the NSA denies economic espionage, a portion of the leaked documents describes activities that have no alternative explanation.

One NSA memorandum in particular discusses programs for snooping on the trading activities and energy production of countries, that while not close allies of the United States, are classified as cooperating with the NSA.

For example, Japan is said to share signals intelligence with the United States, yet evidence exists that their trade activities are secretly targeted by the NSA.

Other documents reveal that the NSA and its British counterpart, the Government Communications Headquarters (GCHQ), have a list of foreign businesses as targets for surveillance, such as the Brazilian energy company Petrobras.

All of the above offers proof that the surveillance activities of the NSA are clearly overstepping the agency's boundaries with regard to its homeland security and anti-terrorism efforts.

### 7. The NSA has unfettered power and it lies to Congress about its activities. 

As far back as the 1970s, revelations over the extent of US intelligence collecting were published. A resulting Senate committee was supposed to do something about this abuse of power. However, recently, intelligence collecting seems to have become far more insidious.

One of the reasons the situation has become worse is because there exists a dummy court that "rubber stamps" NSA requests to observe its targets.

This court — known as the _Foreign_ _Intelligence_ _Surveillance_ _Act_ (FISA) court — was formed in 1978 by the US Congress to decide on which sort of surveillance activities could be carried out by surveillance services. This court assembles in secret, with the government as the only party present. Therefore, it is not a normal court, which would instead include different sides offering opposing stances on an issue.

Remarkably, up until 2002, the court did not reject any application for surveillance; since then, only eleven of over 20,000 applications have been denied.

One instance of the NSA being permitted by the FISA court to set up surveillance was the aforementioned Verizon case, in which the company was ordered to provide all details of calls between the United States and abroad, in addition to calls from within the United States.

Throughout the years, senators have requested that the NSA estimate the number of US citizens whose communications are being intercepted. Yet on each occasion, the NSA has denied its capacity to retain this information. But this has been found to be an outright lie.

We know this because one leaked document revealed an NSA program code-named "Boundless Informant." This report quantified precisely the data about which senators had inquired. It showed that the NSA had tracked 3 billion calls and emails made or sent by Americans and over 200 billion such communications globally in a single month in 2013 alone.

While there may have been instances of the NSA showing some accountability, amazingly, the agency continues to carry out invasive actions with relative freedom.

### 8. The NSA and its closest allies sabotage global communications and encryption. 

We all know there is a group of nations that share the same language — the United States, the United Kingdom, Canada, Australia and New Zealand. They also share capabilities to spy on the rest of the world.

These five allies utilize and manipulate advanced communication systems over the globe, leaving no corner truly private.

The NSA intercepts the delivery of computer network hardware, such as routers and servers, to international customers and inserts spying tools into them. The devices are then repackaged and continue their transit, as if nothing ever happened.

Another example of interception is the UK's Tempora program, which conducts mass interception of communications data from fiber-optic cables that run along the ocean floor.

In addition, the UK's GCHQ also informed the allied countries that it had created systems to access communications from satellites that are used to operate phones on commercial flights.

Most of the leaked documents detailing these actions are coded, to indicate that they are to be shared among these five countries only. The countries hold annual conferences where they discuss and even take pride in their expansion and success.

The above activities allow for unprecedented population surveillance and pose a threat to security systems worldwide.

For example, we know from the leaked documents that through a program dubbed _Project_ _Bullrun_, the NSA and GCHQ are able to break encryption on internet banking and medical records.

Security experts say that the protocols of the internet are now untrustworthy. The breaking of internet encryption leaves us exposed to hackers and the snooping of foreign governments. What we once considered to be safe and secure is now open to attack.

So now that we've seen the extent of NSA surveillance, let's take a look at how the agency is able to execute such actions.

### 9. The government broadly interprets the US Constitution in the name of “anti-terrorism.” 

The guardedness of Americans over their privacy can actually be traced back to the American Revolution. Over time, however, the capability of intelligence agencies has only risen, while the privacy of innocent people has become more and more limited.

For example, George W. Bush's response to the 9/11 attacks was the _USA_ _Patriot_ _Act_. This included adjustments to government law, with the result that intelligence organizations could gain access to sensitive documents such as an individual's medical history and bank transactions without needing to prove the probable cause of a crime; instead, the documents could just be deemed relevant to an investigation.

Then President Barack Obama took office, pledging to create the most transparent government in US history. However, the NSA leaks show that during Obama's tenure in office, the agency secretly collected records on an immense number of individuals.

Even the people who wrote the law were shocked that the Patriot Act could be used in this fashion. A federal judge claimed that the mass collection of phone metadata was likely to be unconstitutional.

More shockingly, the powers retained by the NSA have rarely been used to thwart terrorist actions.

For example, between 2006 and 2009, the "sneak and peek" provision of the Patriot Act, which granted a search warrant without the target's consent, was used in 1,618 drug cases and 122 fraud cases. The amount of times it was granted for terrorist cases? Just fifteen.

Although this is hard to confirm, evidence from judges, think tanks and other sources show that post-9/11, massive data collection has never prevented a single terrorist attack from taking place.

Intelligence agencies did not stop the 2012 Boston Marathon bombing or the major attacks in London and Madrid, for example.

So how effective is the NSA's blanket surveillance when it comes at the cost of Americans' hard-won privacy?

> **Fact:  

** By July 2013, most Americans considered the danger of surveillance of greater concern than the danger of terrorism.

### 10. The US mainstream media maintain pro-government views. 

We all know that newspapers can be biased, favoring one political position over another. But you might not know the extent to which a supposedly objective free press panders to the desires of the US government and the NSA.

The media is obligated to inform the government of leaks. Therefore, the government has the ability to stop or delay the publishing of stories on intelligence gathering. They demand that the media alert them before publishing stories that could threaten national security.

In 2004, two _New_ _York_ _Times_ journalists were to report a leak revealing that George W. Bush's government had permitted eavesdropping on thousands of Americans. The paper's publisher was subsequently called to the White House, where the president said if the paper published the report, it would be assisting terrorists. Consequently, the story didn't go to press at the time.

However, the story did surface just over a year later, after Bush had been re-elected, and just before the discoveries were to be published in a book by one of the journalists.

In reality, stories important to the government and intelligence agencies are filtered, while the government leaks stories that work to its advantage.

For instance, newspapers such as _The_ _New_ _York_ _Times_ and _The_ _Washington_ _Post_ omitted the word "torture" when referring to interrogation techniques implemented when Bush was in power.

And in 2005, _The_ _Washington_ _Post_ reported on undisclosed CIA foreign prisons used to hold suspected terrorists. These places, known as black sites, breached human rights laws. However, the names of the countries involved in hosting these sites were kept hidden.

Some leaks are approved by the US government, such as drone attacks on Pakistani militants and the assassination of Osama Bin Laden. But media outlets were barred from reporting on the details of the Bin Laden attack, for example.

These methods are just some of the ways the government skews the public's perception of its own actions and the actions of the NSA.

### 11. Whistleblowers and journalists who work with them are smeared by mainstream media and targeted by the government. 

In 2010, the website WikiLeaks began releasing documents that revealed US government war crimes and the contents of diplomatic telegrams.

But why do we know as much about the eccentricities of the website's founder, Julian Assange, as we do about these documents? Because media outlets try to discredit whistleblowers and those who report on secret information.

After leaking the NSA documents, for example, Snowden was described as a low-level IT administrator and a fame-obsessed narcissist. The author, who published stories on Snowden's leaks, was described as a mere blogger or activist, as opposed to a professional journalist. This distinction is important, as journalists can be legally protected for publishing government leaks.

Every whistleblower is at risk of character assassination. For example, when Daniel Ellsberg leaked the Pentagon Papers, showing that the Johnson administration had deceived the public and Congress about the Vietnam War, Nixon's subordinates raided Ellsberg's psychoanalyst's office, looking to expose sordid secrets of his sex life in order to discredit him and avoid further leaks.

When intelligence leaks are reported, the government starts to target the journalist who leaked them.

In recent years, one federal court gave permission to the Department of Justice to read the emails of _Fox_ _News_ Washington Bureau Chief James Rosen. Rosen was accused as a "co-conspirator" as he had worked on material with a source of a government leak. The warrant stated that Rosen had flattered the source in order to get the leak and detailed their communications on avoiding surveillance. However, this is normal behavior for journalists, and doesn't breach anti-espionage laws.

Sadly, we can hardly call it "democracy" when whistleblowers and reporters are characterized as freaks and criminals rather than individuals fighting for the transparency of the government and its intelligence agencies.

> _"The only thing I can't live with is knowing I did nothing."_ -Edward Snowden

### 12. Final summary 

The key message in this book:

**Intelligence** **agencies** **in** **the** **United** **States** **and** **those** **of** **its** **allies** **secretly** **monitor** **the** **communications** **of** **most** **people** **worldwide.** **Rather** **than** **criticizing** **this** **surveillance,** **media** **outlets** **instead** **have** **attacked** **the** **people** **behind** **the** **leaked** **secret** **information.** **Edward** **Snowden's** **revelations** **have** **brought** **to** **light** **abuses** **of** **power** **in** **government** **and** **intelligence** **agencies;** **and** **the** **author** **urges** **everyone** **to** **understand** **the** **importance** **of** **freedom** **and** **privacy.**

Actionable advice:

**Use** **encryption** **programs** **when** **sending** **email.**

Snowden's leaks reveal that using the services of Google, Facebook, Microsoft (including Skype) and other companies can mean that all of our private communications are exposed. For journalists, politicians, activists or anyone who wants to keep his private life truly private, using encryption tools such as Pretty Good Privacy (PGP) or Off The Record (OTR) chat is essential. But beware: there is an NSA program that even infiltrates The Onion Router (TOR) network, software that is supposed to enable anonymity online.

**Support** **independent** **journalism.**

Journalists are supposed to keep our elected politicians in check, to reveal corruption and stick up for the rest of society. Instead, many media outlets run scared of the government, withholding or watering down important information their journalists may dig up. However, there are alternatives: reporters, bloggers and activists who are not scared of the truth and work to shed light on what really is going on. Taking a look at alternative news sources or even just being aware that independent journalism exists can help keep different opinions flowing and provide an outlet for the truth.
---

### Glenn Greenwald

Best-selling author and journalist Glenn Greenwald is a former civil rights attorney and lawyer. Having blogged about the surveillance state for a number of years, Greenwald caught the attention of Edward Snowden as someone who could help him leak documents exposing the secret behavior of government and intelligence agencies. Greenwald is a highly influential political commentator and in 2014, shared the Pulitzer Prize for Public Service.

