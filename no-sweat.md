---
id: 55b7e75832353900078c0000
slug: no-sweat-en
published_date: 2015-07-29T00:00:00.000+00:00
author: Michelle Segar
title: No Sweat
subtitle: How the Simple Science of Motivation Can Bring You a Lifetime of Fitness
main_color: None
text_color: None
---

# No Sweat

_How the Simple Science of Motivation Can Bring You a Lifetime of Fitness_

**Michelle Segar**

_No Sweat_ (2015) will help you stop viewing exercise as a chore and start believing in its benefits for your whole life. These blinks explain how you can boost your motivation and develop a sustainable approach to fitness and self-care.

---
### 1. What’s in it for me? Discover how to stick to an exercise plan and live a better, healthier life. 

We've all been there. You say you're going to start exercising more, even investing in a pair of running shoes or a gym membership. For a couple of weeks you stick to it, and even feel good about it. Then gradually your motivation dries up and you're back to lazy evenings on the couch.

Why is it always so hard to stick to an exercise plan? The reason so many of us fail is that we're exercising for all the wrong reasons.

By discovering the real meaning behind why you exercise — whether to become healthier or live a more inspired life — you can find your true motivation and learn how to stay disciplined.

The key to success? Develop a strategy that will turn those push-ups and sit-ups from a chore into something you truly enjoy. These blinks will show you how!

In these blinks, you'll learn

  * why self-control won't help you reach your exercise goals;

  * why understanding the deeper meaning behind exercising is so important; and

  * how you can use your smartphone to live a healthier life.

### 2. Popular health and fitness messages just aren’t motivating enough to keep you exercising. 

How many times have you started a new diet and lost a few pounds, only to gain them back quickly with the help of a comfy sofa and a tub of ice cream?

If this has happened to you more than once, you're not entirely to blame. The reality is that today's health and fitness messages just aren't motivating enough.

Marcia gained an extra 30 pounds after her first child and had tried everything to lose the weight, from special foods to fasting, doctors' diet plans to hours on the treadmill. Nothing worked!

Why? Because Marcia really wasn't motivated.

She kept hearing the same messages. You eat too much junk. You're overweight because you don't exercise enough. If you exercised more, you'd become more healthy and attractive!

Such messages aren't motivating, as the benefits of exercise are really only apparent in the future — that is, weeks, months or years from today. But we want to be healthy and attractive now! Humans are hardwired to prefer immediate gratification. We don't like waiting for rewards.

In 1994, the author looked at whether ten weeks of exercise would decrease anxiety and depression among breast cancer survivors. Participants were split into two groups: one that exercised and a second that didn't. Interestingly, the group that exercised showed lowered levels of anxiety and depression.

And yet, three months later, nearly all of them said they had stopped exercising! Why did this happen?

Despite the clear benefits of exercise, the participants didn't feel compelled to continue. This is because the promise of better health in the future just couldn't keep them motivated.

### 3. “Wrong whys” lead you to forming a negative image of exercise, and prevent you from getting fit. 

What does exercise mean to you? You might associate it with concepts like "effort," "hard work" or "unpleasant chore." It's exactly these sort of associations that put you off your exercise routine.

Though you might not realize it, everything you do, exercise included, has an underlying meaning that you create, based on your experience of doing it.

So everytime you try to get fit and fail to meet your goals, this bad experience reinforces the negative feelings you have toward exercise — creating a cycle of failure that rears its ugly head every time you try to get back on that treadmill.

The cycle of failure often begins with a "wrong why" — an _unhealthy_ reason on which you base your decision to get healthy. A wrong why could include a desire to meet social expectations, or wanting to please someone. These reasons can make you see exercise as a chore, something you're obliged to do, often for other people. This will only lead to failure.

The author conducted a study to examine the impact of why people start exercising. She found that 75 percent of participants cited weight loss or better health as a top reason, while 25 percent said to enhance the quality of their daily lives.

Yet the results explained a lot about how "wrong whys" don't motivate. The people who said they wanted to lose weight or get healthy actually spent the _least_ amount of time exercising, up to 32 percent less than the other group!

So to turn exercising from a chore to a celebration, you need to change what exercise means for you. But how? By replacing those negative associations with positive ones. To do this, you need to first start by choosing the correct strategy to keep you exercising daily.

You can't just rely on willpower, however. Overtaxing your willpower in one area might make you weaker in others. Experiments have shown that dieters who try not to eat too much candy while watching television, gorge on ice cream later on once the TV's off.

So what are the best strategies? Let's explore some options in the next blink.

### 4. Find a movement you love doing – like walking or biking – and give yourself that gift every day. 

The brain uses two different systems to process information and drive decisions. One system is driven by logic; the other by emotion.

Though you might not like to admit it, you make decisions based on your emotions far more frequently than you do based on logic! Emotions are often just more powerful.

If you want to start exercising and stick to it, then you'll need to keep the power of emotional decision-making in mind.

Sure, there are many _logical_ reasons why you should exercise. But at the end of the day, these reasons aren't going to motivate you. You can make sure you stay on track with your routine by picking exercises that _feel good_.

Think about how nice it is to do something that makes you feel good, especially when you're sad or stressed. People make themselves feel better in different ways, from shopping to listening to music to eating out. So why can't exercise be a treat, too?

So focus on making exercise something that you do for yourself, and that makes you feel good. By making exercise a treat, you can turn it around from a daily chore to a daily gift. The author loves walking, for example; her husband begins almost every day riding a stationary bicycle.

There's another way to stop seeing exercise as a chore. It's simple: remember that _everything counts._

You don't _need_ to exercise for one hour, 30 minutes or even ten minutes. Research suggests that even if you only want to exercise for seven minutes, that's perfectly fine. Everything adds up.

Once you realize this, you'll find many _opportunities to move_, such as meeting up with a friend for a nice walk in the park instead of coffee at a cafe. In this way, you'll find yourself building a successful cycle of motivation.

Enjoyment (a correct "why") results in success (exercising). You then continue to view exercise as a gift that you can _give yourself_ whenever possible.

### 5. Give yourself permission to look after yourself! Self-care is crucial in maintaining a balanced life. 

With so many roles and responsibilities — from taking care of children to elderly relatives to work stresses — it's often difficult to look after yourself.

But if you don't start prioritizing your own well-being, then you won't be able to continue performing all those other roles, either!

Self-care is vital. Each one of us has different self-care needs, that we too often neglect.

Take a moment to think about what your needs are. Do you want to feel joyful? Less anxious? More present in the moment?

Now ask yourself which self-care behaviors might help you realize these needs. Do you need more sleep or exercise? Do you need to take time to meditate, read a novel? Or perhaps you need an app to remind you to take a warm, relaxing bath every evening?

Once you know what your needs are, set out a plan to make them happen every day.

Isla, a writer, chose to focus on self-care to become more inspired. To realize and prioritize her needs, she set a reminder on her smartphone that would pop-up at the end of each day, asking her to rate her day to see how inspired she was feeling.

Self-care cycles begin with motivation — to become a better person, partner, parent, professional, community member or friend. This motivation then converts your self-care behavior into a daily need. Then, you prioritize your daily need in your life.

When we stick to our self-care behaviors, these behaviors in turn sustain us, by helping us with what matters most: being a better person.

But how can we keep up with self-care when times are tough? Read more to find out.

> _"What sustains us, we sustain."_

### 6. Arm yourself with a self-care negotiation plan and a learning mind-set to make good behaviors last. 

It's vital to prioritize self-care. But what happens when self-care and physical activity clash? To keep a balance, you'll need to find ways to negotiate.

The best way to maintain a balance is by getting organized, through a self-care negotiation plan.

First, put together an outline of things you want to achieve from being physically active as well as any other personal goals, such as quitting smoking. Look at how these goals fit into your schedule.

Ask yourself: What physical activities will help me achieve the benefits I want? When will I perform them? For how long, and where?

Then anticipate possible scenarios in which you'll need to negotiate, such as "If challenge A occurs, _then_ I'll do X, Y or Z." Setting up scenarios will help you better adopt new behaviors.

After Sam outlined his goal of quitting smoking on his self-care schedule, he found that signing up with his company's free quit-smoking program was a good option toward meeting his goal.

He then anticipated the possible scenarios that would set him off his goal. One "if" scenario was when a coworker approached him to go and enjoy a cigarette outside. His "then" plan was to try to avoid the situation; yet if he couldn't avoid it, he would at least say "no" and take a walk instead in a nearby park to relax.

After you've made your self-care negotiation plan and put it into action, you'll need to start evaluating it, too. Reviewing how your strategies have worked at the end of each week is a vital part of a broader learning process.

In fact, shifting to a learning mind-set is another element that can increase your motivation.

We all want to learn, and this motivates us to find better strategies during challenging times in all contexts, including fitness! So don't regret your mistakes on your fitness journey. Instead, dust yourself off, learn from the challenges and continue caring for your body.

### 7. Final summary 

The key message in this book:

**Your struggles with exercise may be the result of a lack of true motivation. By swapping negative associations for positive ones, you can start to see exercise not as a chore but as a gift to yourself. To stick to an exercise routine, plan ahead for situations where your motivation might be challenged. And above all, never stop learning from your mistakes!**

Actionable advice:

**Count everything as exercise and choose to move.**

Next time you have an extra ten minutes, even three minutes, use it to physically move. A few minutes is better than no minutes at all! While you're waiting for a friend to arrive, or while commuting to work, try and move your body a little. Why not take a short walk? Walking is great for the body and easy to do. The author even parks her car 20 minutes away from work just so she can get to walk a little!

**Suggested** **further** **reading:** ** _The Miracle Morning_** **by Hal Elrod**

In _The_ _Miracle_ _Morning_, Hal Elrod explains the techniques that got him through the aftermath of a near-fatal car accident. Elord realized that the key to a successful and fulfilling life was dedicating some time to improving ourselves every day. He details six crucial steps we can take every morning to help us jump-start our days and get us well on our way to a fulfilled life.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Michelle Segar

Michelle Segar is an American behavioral sustainability scientist, writer and speaker. She is the director of the University of Michigan's Sport, Health and Activity Research and Policy (SHARP) Center, and also chairs the US National Physical Activity Plan's Communications Committee, advising the group on new strategies for the American public and policy makers.

