---
id: 59fe49a6b238e100073f4216
slug: nobody-wants-to-read-your-sh-star-t-en
published_date: 2017-11-08T00:00:00.000+00:00
author: Steven Pressfield
title: Nobody Wants to Read Your Sh*t
subtitle: Why That Is and What You Can Do About It
main_color: BB8C3F
text_color: 87652E
---

# Nobody Wants to Read Your Sh*t

_Why That Is and What You Can Do About It_

**Steven Pressfield**

_Nobody Wants to Read Your Sh*t_ (2016) is a guide to the ins and out of writing, whether it's a book, a screenplay or advertising material. Making a living writing isn't an easy thing to do, especially since the last thing a busy person wants to do is read some poorly written manuscript. But with these helpful tools and the insight of a 30-year veteran of the industry, you can be on your way to writing the kind of captivating work that people love to read.

---
### 1. What’s in it for me? Learn how to write so people will read it. 

Have you ever dreamed of being a prized novelist, famed screenplay writer or a slick _Mad Men_ -esque copywriter? You're not alone. Even so, chances are no one wants to read what you write — that is, unless you are ready to take some tough but tried and tested advice to heart. If so, you're in luck.

These blinks will look at several different writing genres and different strategies for each of them, as well as useful techniques that work across almost all types of writing. So, get ready to learn the stuff you need for people to read your sh*t.

In these blinks, you'll find out

  * how having a strong theme is essential to virtually all writing;

  * why you should have a strong lead character if you write a script; and

  * how reading is an easy shortcut to better writing.

### 2. Nobody wants to read your shit, unless it’s exceptionally good. 

If you've just put the finishing touches on your novel or screenplay, after months or even years of hard work, you might be at the stage where you're nervous and excited about how readers are going to react.

Well, here's your first piece of advice: don't get too excited, because odds are nobody wants to read your writing.

The author works in advertising, and even when he gets a commercial made, no one wants to see it. People hate commercials and avoid them however they can!

We grow up thinking there is an audience for our work, since teachers and other students read our writing in school — but that's only because they're either being paid or forced to read it.

The reality is, people have other things to do. Most parents are less than eager to read their kid's newest blog post or _Harry Potter_ fan fiction.

A big reason for this is that most writing just isn't very good. So, if you're a young author looking for your big break, this doesn't mean that all is lost — it just means that your writing needs to be exceptionally good.

Now, you may think that "good" writing means clever phrasing or high-minded literature, but that's not the case. The good writing that will give you the best chance of capturing and holding people's attention will be clear and easy to understand.

You don't want to bore people, so make sure your work is either suspenseful and scary, beautiful and tragic or just plain fun; in other words, don't spend a hundred pages describing the existential crisis a character experiences over his breakfast. You want to create writing that people won't be able to put down.

What most people find difficult is learning to write for someone other than themselves. So, instead of focusing on what you find fascinating, spend time thinking about the interests of your potential readers.

While you might find the migration patterns of turtle doves fascinating, if you want to develop a readership, you need to find a way of injecting romance, adventure or tragedy into that subject.

> _"When you understand that nobody wants to read your shit, you develop empathy."_

### 3. It’s hard to find success in the writing business, but an apprenticeship can teach you lessons and build contacts. 

You're probably wondering: if nobody has the time to read, how is my writing going to become the next best seller or blockbuster?

Well, nobody said it would be easy, did they?

Making a living through writing is tough, and it can take a lot of hard work before you start making a name for yourself.

The author himself landed in Hollywood in the 1980s with big dreams of becoming a successful screenwriter.

Over the next five years, he worked on nine different scripts, each one taking him around six months to complete. Yet each and every one of those scripts was rejected by producers; clearly, something needed to change.

One of the best ways of getting your foot in the door of the writing business is an apprenticeship. After five stagnant years, Pressfield's agent recommended that he partner up with Stanley, an established screenwriter with two successful movies under his belt.

It wasn't a dream gig by any means. Stanley was always late showing up to their writing sessions — and not just five or even 30 minutes late, but three to five hours late. Before long, Pressfield was beginning to understand how this arrangement was supposed to work; he would do all the hard work so that Stanley could swoop in, make a few brilliant changes and call it his own.

While this might seem unfair, Stanley did know how to make a successful script, and he had plenty of Hollywood contacts that could make life a lot easier for the author.

It's just a fact of the business that a writer might have to slave away as an apprentice before they learn the tricks of the trade. While Stanley got all the credit for the scripts they worked on, Pressfield learned the valuable lessons of what makes a successful script and how to sell one.

With this insight, Pressfield could then venture out on his own. But as we'll see in the blinks ahead, there was still a lot left to learn.

### 4. Whether you’re working in advertising or writing literature, you have to have a concept. 

You don't have to watch a whole lot of TV before you see a bad commercial that makes you think, "no one's gonna buy that." When this is the case, it's usually a result of the commercial having bad writing and a lousy slogan.

Successful advertising has a great concept — an idea that turns a dull product into something special.

The secret to coming up with a perfect concept is to think about what the audience would find surprising, intriguing or otherwise exciting.

Let's look at Avis Rent-a-Car, which has always struggled in the shadow of Hertz, the world's number-one car-rental service.

With this competition in mind, Avis came up with a great concept for an ad campaign that turned being second banana into an advantage. Avis told people that being in second place meant they had to work twice as hard to make sure each customer gets the best possible service. So, when you pick Avis, you're sure to have a better experience than Hertz, a company that's just resting on its laurels.

But it's not just advertising; literature and good storytelling also need to have a strong concept.

Even Homer's _Iliad_, one of the world's oldest stories, has a great concept.

The _Iliad_ is about the Trojan War, which lasted ten years. If Homer had tried to tell the full story of the war, it would have been exhausting and probably rather dull.

So Homer found a strong concept by focusing on just a few days of the war and the thrilling story of Achilles and his anger.

Achilles is one of the great heroes in Greek mythology, a fearsome fighter who was once insulted by Agamemnon, the King of the Greeks. Angered by the king's words, Achilles withdrew from the war and, as a result, the tide began to turn against the Greeks. But then, in the nick of time, Achilles charged back into battle and saved the day.

As you can see, even something as expansive as Greek history can be made into a tight and compelling narrative with the help of a good concept.

### 5. In advertising as in fiction, defining the problem or the theme is the first step toward creating a story. 

If you're the "tortured artist" type, then you might feel like you have no shortage of problems. But when it comes to writing, problems are a good thing to have, since they lead to both conflict and resolution — which is what a good story is all about.

In advertising, figuring out the problem is a big part of the process.

Every writer trying to come up with a good advertising concept asks the question, "Why is this product not selling?" Maybe a product is simply bad, like a bathroom mat that turns red when it gets wet. In these cases, the best ad campaign in the world won't help matters.

But for many other products, once you identify the problem, it can lead you toward the solution.

Let's look at classic brands like 7UP and Burger King, both of which are constantly battling against more successful competitors like Coca-Cola and McDonald's.

In 1967, the marketing wizards behind 7UP came up with the "Uncola" slogan, which was a perfect way to set it apart from Coca-Cola. This way, it wasn't presented as the inferior beverage, but was something else altogether. Like its ad campaign's catchphrase said, "There's no cola like the uncola." Soon thereafter, sales went through the roof.

When writing fiction, once you find the problem, the next step to crafting your story is finding the theme. The theme is what allows you to cut right to the core of what your story is about; is it a tale of greed, revenge, envy or perhaps survival?

In the popular TV series _Breaking Bad_, it was all about transformation.

In the very first episode, we're introduced to our protagonist Walter White, and find out he's a chemistry teacher diagnosed with terminal cancer. In order to make enough money for his family to live comfortably after he's gone, White begins an unlikely transformation into a methamphetamine cook.

There are a lot of great stories over the five seasons of _Breaking Bad_, and coming up with good stories is always a challenge. But whenever the show's screenwriters were stuck, they could always come back to the underlying theme, transformation, and get back on track.

### 6. Reading and writing are important to finding your voice, but great writing comes with maturity. 

There's a good chance you're reading these blinks because you have stories you want to tell but don't feel you're talented enough.

But here's the good news: writing is like any other skill, so the more you keep at it, the better you'll get. And one of the best ways to improve your authorial voice is to read.

Spend some time getting to know your local libraries and read the classics that represent the kind of story you want to write. For example, when the author was getting serious about writing and developing a strong authorial voice, he went to his local library and immersed himself in the classics he'd neglected, like Tolstoy's _War and Peace_, Flaubert's _Madame Bovary_ and _The Red and the Black_ by Stendhal, just to name a few.

If you feel like your writing is coming off as phony, strengthen your authentic voice by writing more letters to your friends.

When we communicate to people we're familiar with, we lay off the fancy language and stylistic flourishes that can make us sound inauthentic. You might even have some friends who say they love the prose in your letters but dread having to read the next draft of your novel. Instead of struggling with that phony voice, focus on the authentic voice that appears in your letters.

It's also important to be patient, and remember that real wisdom and authenticity come with age.

For years, the author felt frustrated that all his writing felt fake and contrived. But as he started to mature, his writing got better.

At the beginning, the author's writing was like a selfie — a disposable plea for attention that was all about him and his life. But since he hadn't done much living, there wasn't much substance.

Young authors often fall into this trap. Eventually, though, they begin to experience more of life and all the hardships, broken hearts and suffering that it entails. This is where real writing can begin and when writers will have genuine wisdom to offer their readers.

### 7. Most stories follow a three-act structure, unless you’re aiming for an epic narrative. 

If you've seen enough movies, you've probably noticed that most of them follow a very similar story structure. And there's a good reason for this, since a good structure can ensure that a movie delivers a satisfying story.

For most movies, this is the three-act structure, which condenses and breaks a story down into three segments that all help to make sure audiences are kept in rapt attention.

The first act is all about hooking the audience and getting them interested, the second act is about creating tension and conflict and the third act is about resolution, which often comes in the form of a showdown.

Just about any traditional story can fit into this structure, whether it's a joke being told by a stand-up comedian, or a narrative that's used to market a product. And this isn't some cheap gimmick — it's a tried and true recipe that even Shakespeare adhered to.

_Romeo and Juliet_, for instance, although it technically has five acts, can be divided into three distinct parts; the first part introduces the characters and setting, and leads up to the first encounter of the titular duo; the second part piles on the conflicts and problems stemming from their rival families; and the third builds toward the tragic death of our star-crossed lovers.

Now, as with most rules, there are exceptions, especially when it comes to long, epic stories.

_Lawrence of Arabia_ is a famous movie for many reasons, including its epic length of nearly four hours. The movie's director, David Lean, specialized in epic films and believed these stories were best structured using eight to 12 segments.

In a structure like this, each segment can be treated like it's own self-contained movie. But, since they are all still part of a linear narrative, each one should prepare the audience for what's coming next. This way, it all adds up to a grand and epic tale.

If you're working on a four-hour movie or a 400-page book, you might try to keep this in mind.

But in the next blink, we'll take a closer look at how you can best use a traditional three-act structure.

### 8. The first act hooks the audience, the second act highlights the villain and the third act delivers the climax. 

One of the reasons _Star Wars_ was and continues to be so popular is that it drew audiences into a whole other world, making them leave the theater with a feeling of wonder and amazement.

If you want to carry your audience off to another world, you first need to hook them — and this is what the first act of your story should do.

Now, at the same time you're hooking your audience, you also need to kick off the plot of your story. Let's see how David O. Russell's _Silver Linings Playbook_ managed to do this.

The first act introduces us to our hero, Pat Solitano, as we learn about his psychiatric issues and obsession with getting back together with his ex-wife. Pat is then invited to a dinner party where he meets Tiffany, a charming but equally unstable woman.

The first time Pat and Tiffany meet is the precise moment when the plot truly begins, and it's also when the audience gets hooked. We know that these two are going to fall in love, but we don't know how Pat is going to overcome his obsession with his first wife.

Moving on to the second act, this is a good time to focus on the villain or antagonist.

You introduced the hero in the first act, so now you can create a clear conflict by introducing the details of the villain and let them get the upper hand, if only for a moment. In Christopher Nolan's _The Dark Knight Rises_, after reestablishing Batman, it's the Joker who takes center stage in the second act. In other stories, the villain might be some other inner or outside force, like an approaching hurricane, an economic crisis or even some internal demons within the hero.

By the end of the second act, the stage should be set for the third act and story's final showdown. In _Bridget Jones's Diary_, this came with the revealing of important information: Bridget Jones, the protagonist, finds out that her love interest, Mark Darcy, isn't actually in love with another woman, only for Darcy to then find harsh words about him in Jones's diary — can love triumph above all?

### 9. Great actors are attracted to complex characters and important themes. 

If you want to write a movie that will be brought to life with the help of megawatt star power like George Clooney and Jennifer Lawrence, then it makes sense to write something that will appeal to these actors, right?

To pull this off, you need to win them over by creating truly memorable characters.

Let's say you want Tom Hanks to be the male hero in your film. Remember that this was the man who played the lead role in _Forrest Gump_, _Sully_ and _Philadelphia_, all of which are movies that feature a unique character with depth and nuance, thus allowing him to give a memorable performance.

Or think of the characters that legendary actors Diane Keaton and Jack Nicholson played in _Something's Gotta Give_, characters that were layered, well rounded and worthy of their talents. If you don't create something brilliant, you shouldn't expect a brilliant actor to waste their time with it. If it doesn't live up to these high standards, you know you still have work to do.

One of the secrets to writing a great character is finding a meaningful theme that supports a memorable, larger-than-life performance.

A lot of popular movies are filled with car chases and sex scenes, but if you want something award-winning and memorable, you'll need to go deep.

_Out of Africa_ won seven Academy Awards, including Best Picture, and the character of Baroness Karen von Blixen was played by the award-winning actress Meryl Streep. She was drawn to the role because the movie touches on complex themes about how everything in life is fleeting, including our relationships and our dreams of creating something meaningful.

In this movie, Streep plays a character who is passionate but unwilling to commit to a relationship, and though she wants to build a great school in Africa, her dreams fall apart. With these rich themes and possibilities for nuance, it was the perfect role for a great actor.

### 10. Nonfiction follows the same basic rules as fiction, and both rely on a strong theme. 

You might think that your book on the migratory patterns of the turtle dove doesn't need to be as thrilling as the latest _Game of Thrones_ novel, but there's really no reason to think that way.

Nonfiction can be every bit as exciting as fiction, and the same rules apply to both genres.

When the author was working on his nonfiction books, he was dealing with subjects that didn't necessarily lend themselves to thrilling stories of heroes and villains. One of those books is _The War of Art_, which is about the process of creative writing. Nevertheless, he treated it the same way he would if it were filled with devilish villains and car chases.

So, if you're looking at your thesis on nineteenth-century opera and thinking there's no way it can be interesting, here are some helpful tips.

Just like fiction and a good screenplay, nonfiction needs to be focused on a central theme in order to be engaging and coherent.

Let's say you're writing a biography on your great-great-grandmother Rosie. She was an early American settler who crossed the country in a covered wagon while she was pregnant, faced the threat of potentially violent native Americans, and had eleven children and three husbands before she passed away.

This is a potentially thrilling story, but if you just go from one event to the next, it runs the risk of being stale and predictable. Instead, you need to find a theme and focus on it.

In this case, you could focus on the American dream and the quest for a better life in the face of hardship. Now, whether it's being stranded in a broken-down wagon or facing the loss of a child or husband, every event in her story would have this theme running through it, tying them all together.

So, now there's no excuse for turning in a dull manuscript, screenplay or ad campaign. Now that you know what to look for go, it's time to get to work crafting a great story.

### 11. Final summary 

The key message in this book:

**A good story doesn't require clever wordplay and flowery prose. Instead, it's about creating a powerful, well-structured story that has a meaningful theme and characters with depth and nuance that reflect that theme. Whether it's exciting, dramatic or romantic, by working hard and being well read, you can reflect on your own experiences in life and use structured narratives to create something people will love to read.**

Actionable advice:

**Work your way through the resistance.**

If you are asked to write a short novel in three months, you might throw up your hands in despair. But when it comes to writing, time is not the problem; the problem is the resistance we have to following through with our tasks.

This resistance takes the form of saying it is not possible and that you can't do it, and not trying as a result. Instead, just sit down and write the short novel in three months. The first draft may not be perfect, but at least you completed the assignment, and it may lead to something amazing down the road.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Write It Down, Make It Happen_** **by Henriette Anne Klauser**

_Write it Down, Make it Happen_ (2001) offers useful and practical advice for your personal, professional or romantic life. The advice is centered around writing down your goals and visualizing both your fears and aspirations as a way to overcome your perceived limitations and make your dreams reality.
---

### Steven Pressfield

Steven Pressfield is a best-selling writer who has worked in advertising, screenwriting and as an author of both fiction and nonfiction. His books include _The Legend of Bagger Vance_, which he also turned into a screenplay for the movie of the same title starring Will Smith and Matt Damon. His experiences and advice on writing can be found on his popular blog, stevenpressfield.com.

