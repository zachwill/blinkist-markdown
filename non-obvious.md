---
id: 555b453b6135330007300100
slug: non-obvious-en
published_date: 2015-05-22T00:00:00.000+00:00
author: Rohit Bhargava
title: Non-Obvious
subtitle: How To Think Different, Curate Ideas & Predict The Future
main_color: FFF233
text_color: 666114
---

# Non-Obvious

_How To Think Different, Curate Ideas & Predict The Future_

**Rohit Bhargava**

_Non-Obvious_ is all about detecting important trends, an essential part of running a business. You can't accurately predict the future by looking at obvious, surface-level influences — the important ones are hidden underneath. These blinks offer advice on identifying meaningful changes, while outlining some prominent trends we're likely to see in the future.

---
### 1. What’s in it for me? Learn how to become a trend curator. 

In the 1960s, there was a very popular science fiction program, Space 1999. It was set on humankind's first colony on the moon. Back then, in the middle of the space race, it seemed obvious to people that by the end of the millennium, humanity would have spread across the solar system. How wrong they turned out to be.

This is an example of an obvious prediction, a simple guess about the future based on what you see around you. They are incredibly common and almost always wrong.

But this doesn't mean that you can't predict future trends, it just means you have to do it the right way. These blinks show you how to look deep into modern society to notice the possible, largely hidden, trends that could emerge. Reading them will also give you an idea of what trends you might expect to see this year.

In these blinks, you'll discover

  * why one NFL team tried using yoga to become successful; and

  * why being fickle may in fact be a good thing.

### 2. You curate trends by keenly observing the non-obvious patterns in today's society. 

Everyone in business wants to stay a step ahead of everyone else. We all want to be the first to figure out the next big thing. We want to predict trends before our rivals do.

Yet when people attempt to predict the future, their ideas are often uninspired or even a bit lazy. In the 1960s and '70s, hundreds of people thought we'd be all driving flying cars in the next few decades. This was the time of growing rocket power, air travel and car ownership. It seemed only natural that the technologies would all come together to produce elevated personal transport.

But predicting trends based on what seems obvious at the moment is futile. The present is constantly changing: The world isn't nearly as obsessed with space exploration as it was sixty years ago, and we're not nearly as motivated to go to the moon.

If you want to get a better idea of what people will be doing in the future, look for _non-obvious_ trends.

Non-obvious trends aren't right there in front of us. They're buried under lots of other information, and if you want to spot them, you need to become a _trend curator_.

Think of what a museum curator does. First, they collect things, like works of art or other artifacts. It's important to build this collection, but the collection alone does not a museum make. Curators also have to bring out the meaning in the objects they present.

A trend curator is similar. They gather lots of information about what people are doing, and what's happening in technology, fashion or whatever other field they're interested in.

For example, a keen trend curator might notice that social media, increasingly advanced technology and personalized adverts are pushing us towards a future in which people will have a stronger desire to be the center of attention.

> _"The first thing I realized was that if I was going to develop ideas that others weren't thinking of, I couldn't do it using the same information they did."_

### 3. Identify hidden trends using the haystack method. 

Finding non-obvious trends isn't easy. It's kind of like finding a needle in a haystack, which is why the method for identifying trends is called the _haystack method_.

The first step in the haystack method is _gathering_. Gathering is about collecting stories, information and ideas from various fields or markets around you.

Next, start _aggregating_. You aggregate by taking individual ideas and disconnected thoughts, then grouping them together. When you learn about something new, ask yourself why it's of interest to you in the first place. Then, collect information based on your answers.

When you finish aggregating, you can _elevate_. Elevating means thinking about the underlying themes that connect your groups of ideas. What's the bigger picture?

Say you learn about a new tool for helping people find the best price for their medication. Then, you might read that Macy's is investing in creating apps to improve their in-store shopping experiences. Elevating means finding the connections between these two things: People are looking for a more optimized shopping experience.

Once you've got your idea, you can _name_ it. Give it a name that's simple, understandable and memorable. One good way to do this is to mash up a name from two different concepts. _Likeonomics_, for instance, is a combination of "economics" and "likability."

_Proving_ is the final step. That's where you go back through your ideas about growing trends. Can you find enough instances of them to call it a trend?

When you're analyzing your conception of a trend, you have to look at it from a few angles. Is the idea meaningful? Does it impact people's behavior? Is it likely that it'll continue to have an effect?

If your idea meets these criteria, you have a trend. If not, start again!

### 4. Become a trend curator by staying curious, fickle and observant. 

It takes a lot of work to be a trend curator. Don't expect it to be easy.

But if you want to be the best curator you can be, you'll need a few essential attributes.

First, you have to be _curious_. You should always be asking yourself questions like, "Why is this happening?", "Why are people doing this?" or "What will this technology do for us?"

This curiosity will help you unlock the meaning in all the information you collect. It'll allow you to sort through it and spot the trends.

If you aren't curious, you won't discover anything. Just think about Bjarni Herjulfsson, the Viking explorer who first encountered the New World by mistake. Despite landing on a new continent, he wasn't curious about it at all and sailed right back to Europe. It was only later that more curious adventurers "discovered" the landmass when they went exploring farther inland.

You also need to be _fickle_. We generally think of fickleness as a negative quality, but it's actually useful in trend curation.

Why? Because when you're fickle, you keep moving back and forth between trends and ideas. You won't get too fixated on any one, trend or idea which is great: You generate more ideas when you explore. You'll have more material to sort through and curate!

Finally, you have to be _observant_. Being observant is about training yourself to spot the minor details that others miss.

You can become more observant by imagining a child is next to you throughout your day, constantly asking questions about your surroundings. Why is that car red? Why is that person wearing that shirt? If you think of how you would explain things to a child asking these kinds of questions, you'll start noticing more of the little details that often go unnoticed.

### 5. Trend curators also have to be thoughtful and elegant. 

It's absolutely vital to be curious, fickle and observant if you want to be a trend curator. But these aren't the only traits you'll need.

Trend curators also have to be _thoughtful_. Thoughtfulness is about reviewing all the material you've collected, then reflecting on it.

When you assess your material, don't just go with your initial reaction to something — this doesn't usually make for the best decisions. Take the time to think carefully about what you know, what you need to do and how you can do it. Only then can you proceed.

Just think about the comments at the bottom of an online article. Most are petty and rude, and these almost always get ignored. If you write out a thoughtful comment, however, the author might actually answer you.

Finally, when you curate thoughtfully, you gain a deeper understanding of your topic. You'll also get more insights that'll come in handy later.

Trend curators also need to be _elegant_. Elegance allows you to describe a concept in a way that's simple and clear to your audience, but still beautiful.

If you go on for too long about your thoughts, people won't understand them. On the other hand, if you express your ideas in language that's _too_ concise, people won't pay enough attention to them. Elegance helps you achieve the right balance between these two potential problems.

One of the best ways to remain elegant is take some inspiration from poetry. Use visual metaphors and similes to stir up people's emotions. If you can create a vivid image in someone's mind, they'll understand what you're saying — and remember it too.

### 6. After you find a trend, use workshops or intersectional thinking to figure out how to use it. 

Your work isn't over when you learn how to curate. Identifying trends isn't very meaningful if you don't know how to make use of them!

It's here that _intersectional thinking_ becomes a helpful tool. _Intersectional thinking_ means looking for similarities _betwee_ n trends, and the points where different trends intersect.

The new drink _RumChata_ is a good illustration of how important intersectional thinking can be. RumChata is a mix of rum and _horchata_, a milky cinnamon drink made with almonds that's popular in Spain and Latin America.

RumChata became successful because it brought together a few growing trends: an increasing desire for authentic products, a growing interest in Latin American culture and a new love for creative food.

_Workshops_ are another great tool for companies looking to take advantage of trends. Workshops can be run in-house or through professional trend experts. They usually last a day or two.

Trend workshops allow groups to work together on particular trends, so they can generate ideas on how to take advantage of them. Collaborative environments always help inspire creativity. There are many ways you can make your workshops more productive, but two are especially important:

First, adopt a _Yes, And_ approach. This means that when someone has an idea, you should build on it by saying "Yes, and..." before you introduce your own thoughts.

The Yes, And approach ensures that whenever someone presents an idea, the other participants immediately add to it.

Second, be sure to choose the best leader for your workshop. Remember, the best leader is the person who can best facilitate discussion, _not_ the person who's most familiar with the topic. If you choose the wrong leader, you'll have a slow and unproductive discussion.

### 7. This year, we'll see certain new trends in the ways we buy services. 

We've covered the theory, so let's look a few trends we can expect to see in the year ahead.

Many of the trends we'll see in 2015 will relate to ways customers interact with companies and their products. These days, people's expectations of customer service are changing. Undoubtedly, companies have to take note.

In this context, the first upcoming trend is called _Everyday Stardom_. Everyday Stardom refers to how people's normal experiences are now being transformed by their desire to always be in the spotlight.

Some companies are already prepared; Disney, for example, has developed the _MagicBand_ and _MyMagic+_ bracelets for visitors at their theme parks to wear. They contain personal information as well as people's plans for their visit.

Disney uses this information to offer their customers a highly personalized trip. The staff know the visitors' names and wishes, and the park offers gifts and products like photo albums that portray the visitor as a star.

Soon, customers will expect this kind of treatment everywhere. Businesses will have to adapt if they want to keep up.

Next, there's _mainstream mindfulness_. Modern society places a huge emphasis on the importance of mindfulness, as well as meditative practices like yoga.

As an example, the number of yoga practitioners in the US is expected to increase by 4.2 percent in the next five years. Even prominent organizations like Google and the Seattle Seahawks have incorporated mindfulness and yoga into their business practices.

This trend will force companies to start displaying more empathy and balance in what they do. They'll have to show that their products and business practices are thoughtful and considerate, or else risk losing the public's interest and being looked down upon.

### 8. We'll also see new trends in the ways that people interact with companies. 

A number of upcoming trends relate to the shifting relationship between _finding_ products and buying them.

The first trend in this area is called _reversible retail_. In the past, businesses may have had an online shop, but they sold most of their products at their physical stores.

But this is no longer the case, as people are buying things online much more often. This doesn't make physical stores redundant, it just means that they play a different role nowadays.

Physical stores are now a space for building brand loyalty by offering customers a good experience and atmosphere. Online stores are for the products.

Apple stores are a prime example of this trend. They're designed to offer you a positive experience that you can associate with the brand. They display its cool character and offer their expertise. We should expect more physical stores like this to pop up in the future.

Another important new trend is _glanceable content_. Our attention spans are shrinking: Content has to be optimized to fit our rapid consumption rates.

Brands that use flashy headlines are already benefiting from this. Take Oreo, who successfully used glanceable content in their _Cookie vs. Creme_ campaign. They pitted Oreo lovers against each other based on their preferred part of the cookie. They made the company's story stand out and instantly grabbed people's attention.

Another new trend is _predictive protection_. We all want to be protected from harm, whether it comes from the outside world or we inflict it upon ourselves. Low-maintenance technologies that keep us safe will become more and more popular.

One of Bahrgava's favorite gadgets is a simple wristband that buzzes if you're inactive for too long, prompting you get up and go for a walk or do some exercise. Devices like this can help us be more mindful of our health and well-being, and require minimal attention or upkeep.

### 9. Some upcoming trends relate to changes in the way we consume products. 

Finally, let's look at some trends concerning changes in how we consume what we buy.

_Microconsumption_ is a particularly interesting trend. Traditionally, if you wanted to buy something, you had to buy a complete version of it, produced by a company. So when you wanted to see a new movie, you went to the cinema and paid the full price.

Nowadays, people only want to pay for what they want, and nothing more. Companies need to be aware of this and start offering some alternatives.

The _Teatreneu Club_ in Barcelona is a good example of this. Before, if you wanted to see a comedy show, you paid a flat fee. The Teatreneu Club, however, uses facial recognition technology to charge you based on how much you laugh. So, you only pay for what you actually enjoy.

Content-based companies like media outlets are also developing similar products. Some allow their customers to purchase small chunks of content for a lower price rather than buying the whole article, TV show or film.

Finally, there's _disruptive distribution_. These days, companies are taking more control over the distribution of their products — they're cutting out the middleman completely.

This is perhaps most noticeable in the music industry. Artists are skirting the control traditionally exerted by record companies and are connecting with their fans directly. Taylor Swift, for instance, celebrated the release of her new album by inviting some lucky fans to a private pizza party in her apartment.

Disruptive distribution allows companies to circumvent the traditional gatekeepers of the industry. These days, any business can reach out to its customer base through social media. This presents a lot of room for creativity and innovation.

### 10. Final summary 

The key message in this book:

**Don't expect trends to pop up in the obvious places. If you want to predict them accurately, become a trend curator by being curious, fickle and observant. Stay thoughtful and elegant, and you'll learn to take advantage of upcoming trends like everyday stardom and disruptive distribution.**

**Suggested further reading:** ** _Trendology_** **by Chris Kerns**

_Trendology_ examines how brands can use social media such as Twitter to make direct contact with their customer base, and can benefit from tapping into trends in the social media conversation. In addition to social media theory, _Trendology_ offers a step-by-step guide on how to build a successful social media program and succeed in real-time marketing.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Rohit Bhargava

Rohit Bhargava is an author, marketing and business expert and trend curator. He is also CEO and founder of the Influential Marketing Group, and a two-time TEDx speaker. _Non-Obvious_ is his second bestselling book.

