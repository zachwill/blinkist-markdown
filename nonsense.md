---
id: 5740af502edd1b00032e6b82
slug: nonsense-en
published_date: 2016-05-25T00:00:00.000+00:00
author: Jamie Holmes
title: Nonsense
subtitle: The Power of Not Knowing
main_color: 48B3D3
text_color: 2E7387
---

# Nonsense

_The Power of Not Knowing_

**Jamie Holmes**

_Nonsense_ (2015) is all about _ambiguity_ and the effect it has on our lives. We encounter situations we don't understand all the time, both in regular daily life and in extreme situations like natural disasters. These blinks outline the widespread effects, both negative and positive, that this nonsense has on our behavior, and why it's so important to understand it.

---
### 1. What’s in it for me? Find out how nonsense can influence your decisions. 

Have you ever felt puzzled by a piece of modern art? Maybe you've seen René Magritte's _Son of Man_ and thought: why is there an apple in front of this man's face? What does this even mean?

To most of us, the famous painting appears to be utter nonsense, something ambiguous we just cannot explain. The experience can be irritating, stressful or funny. So how do people deal with ambiguous and uncertain situations? You might be surprised by how much nonsense can affect our lives.

In these blinks you'll learn

  * why ambiguity in a joke makes us laugh;

  * how a loud printer decided a criminal case; and

  * the most crucial skill in hostage negotiations.

### 2. Amusing or eye-catching ambiguity can be a useful tool for comedians and marketers. 

There's a lot of nonsense and ambiguity in our daily lives, even though we don't always notice it. We can't always make sense of the things around us — and sometimes that makes us laugh!

Humor often hinges on subtle ambiguity. Consider the joke, "There are only three kinds of people in the world: those who can count and those who can't." It's funny because it confuses you for a moment, then you realize that the joke rests in the fact that the teller can't count. Small ambiguities like that grab our attention, then amuse us.

The same thing happens if someone says, "Call me a cab" and another person responds with the joke, "You're a cab." The humor hinges on the ambiguity of the first phrase, where "call" could mean make a phone call or call someone's name.

Marketers know that people like this kind of playful ambiguity, and they know how to use it to their advantage. Good marketers know that ambiguity can be an effective way of grabbing people's attention in advertising.

Sweden's Absolut Vodka did this in the 1980s and 1990s when they launched a series of ads that featured hidden bottles you couldn't notice right away. The fact that the vodka bottles were obscured made the ads more intriguing.

One ad, "Absolut Boston," for example, featured several dozen Absolut Vodka cases floating in Boston Harbor. At first glance, the cases appeared to be placed randomly, but upon further inspection, you noticed that together they formed the shape of a bottle. The ad garnered a lot of attention for the company.

Nonsense isn't always positive, however. It can also have the opposite effect.

### 3. Uncomfortable ambiguity can influence people’s opinions by making them more or less confident. 

It's common to want to influence people around you. You probably know someone whose opinions you've wanted to change, whether it was your right-wing uncle or hippie aunt who never got over the sixties.

Ambiguity can be a useful tool here. Studies have shown that when ambiguity isn't consciously resolved, it creates anxiety and can influence your beliefs.

Harvard psychologists Jerome Bruner and Leo Postman did an experiment with this phenomenon in 1949 by quickly flashing a series of playing cards at research participants. Some of the cards had reversed colors and symbols, like a black heart or a red spade.

The subjects didn't notice the cards that had been switched, as their minds showed them what they expected to see. The mismatched cards created subconscious anxiety, however, and it had an effect on their opinions.

Subjects who were against abortion, for example, were even more strongly opposed to it afterward. The unresolved ambiguity caused by the cards induced them to cling onto things they felt more certain about.

Ambiguity can also influence people's opinions by making them stressed out.

According to the psychologist Arie Kruglanski, there are two ways stress can affect your decision-making. In the 1980s, he found that if people weren't confident about their opinion on a criminal case, he could influence them by placing a loud printer in the room. They changed their opinion more often and more quickly the louder the printer was.

People who were confident about the case, on the other hand, became _more_ confident with the noisy printer there. You might want to keep this study in mind for your next important meeting.

### 4. When it comes to processing ambiguity, children are much more comfortable and imaginative than adults. 

Imagine you see a white crow in the distance. You'd feel unsettled by it, prompting your brain to try to make sense of the situation. Did you mistake the color? Is it really a dove? The uncertainty would unnerve you and you'd most likely settle for a simple explanation, missing out on the chance to discover an albino corvid.

Adults have a hard time processing that kind of ambiguity. Children are different, however.

Children easily navigate through ambiguity. If they see a white crow, they'll come up with a more imaginative explanation for it. Maybe it's been dipped in white paint, or its feathers have gone white because it's old.

Jean Piaget, the famous psychologist, once asked a young girl how wind was made, and she quickly answered that it's generated by trees waving their branches around. Children explain ambiguity with ease, but that ability diminishes as we get older.

That's why adults sometimes have a hard time processing irrationality, like the kind found in some forms of modern art.

Travis Proulx did a study on this in 2009 by having research participants read an excerpt from "A Country Doctor" — an absurd text by Franz Kafka. The story contains a number of inexplicable events, like horses that bite the cheeks of the doctor's maid and a boy with worms in his wounds.

The ambiguity made the test subjects more alert. After reading the text, they could identify patterns in a series of letters better than test subjects who hadn't read the text. They were more motivated to restore a sense of rationality in their minds.

### 5. Our aversion to ambiguity can fuel destructive politics and even extremist cults. 

When George W. Bush addressed the nation after the 9/11 attacks, he famously proclaimed, "I don't do nuance." His popularity skyrocketed. In times of uncertainty and anxiety, people want straightforward answers. Straightforward answers aren't always the best ones, however.

An inability to weigh new or different options is usually a hindrance in politics. It's a big part of the reason that dissenting minority voices are often silenced.

In 2003, psychologist Antonio Pierra did a study on this. He found that when people feel pressured and want a quick solution to a problem, they tend to favor dictatorial systems where one loud voice prevails.

Consider the US invasion of Iraq, for instance. A lot of people opposed it, insisting that Iraq didn't have any "weapons of mass destruction" that justified such an attack. Those dissenters turned out to be right, but they were silenced by the fear that followed 9/11.

Our desire to favor simple explanations over ambiguity can even fuel extreme cults. In 1954, one such cult arose around Dorothy Martin, who preached that the world was about to end and only the members of her cult would be saved. She said they'd be rescued by a flying saucer at midnight on 20 December that year.

She didn't know that a psychologist named Leon Festinger had infiltrated the group to observe them. When the prophesied flying saucer failed to come on the scheduled night, they frantically searched for an explanation. They decided that aliens hadn't come because their prayers had appeased God, so Armageddon was no longer necessary.

> _"The test of a first rate intelligence, is the ability to hold two opposed ideas in mind at the same time, and still retain the ability to function." — F. Scott Fitzgerald._

### 6. People will make rash decisions in order to resolve feelings of extreme ambiguity. 

If you start packing a week before a trip, you have a lot of time to weigh the importance of your dental floss or makeup kit. If there's an earthquake, it's easier to make up your mind. You just want to get out of there.

People make drastic decisions during and after natural disasters because they're faced with an ambiguous future. This is especially true when it comes to romance.

After the famous San Francisco earthquake of 18 April 1906, the city broke its own record for the number of weddings in a single month. Some were so eager to get married that they settled on people they hardly knew. One groom named Mutty Sullivan couldn't even give the name of his prospective bride, but they were still allowed to marry.

Why did this happen? The uncertainty generated by the earthquake made people desperate to cling onto something stable. Couples can be just as easily ushered into divorce, however.

When a hurricane hit South Carolina on 10 September 1989, it cost the city billion of dollars but also affected many romantic relationships. Couples who were drifting apart quickly filed for divorce afterward and those who'd been hesitant about marriage went out to buy rings. The following year, an unusually high number of babies were born.

The extreme ambiguity following the hurricane pushed people to reassess their romantic relationships. They made decisions more quickly, whether those decisions were about divorce, marriage or just sex.

People react very strongly to ambiguity, even when it's less extreme than an earthquake or a hurricane. So what can we do to cope with it?

### 7. Michel Thomas developed a new method for dealing with the ambiguity in language instruction. 

Did you ever have a language teacher who made a student panic by asking them a question they didn't understand in front of the class? When faced with an ambiguous question, we can only guess what it means.

Fortunately, some language teachers take a different approach. The legendary linguist Michel Thomas became one of the most respected language teachers in the world after he developed a new teaching method in the 1990s.

Michel Thomas taught himself to speak 11 languages and opened schools in New York and Los Angeles. He had a number of famous clients, including Bob Dylan, Emma Thompson and Grace Kelly.

What was his secret? His lessons were based on informality and relaxation. When he was given a group of students who were doing poorly in their French class, for example, he started by reorganizing the furniture. He got rid of the desks and benches and brought in armchairs, coffee tables and plants.

Thomas didn't want his students to read or write, either. They didn't take notes and didn't even try to remember what they had learned in class. He told them he just wanted them to relax.

His method might sound ineffective, but it produced amazing results. When the French teacher checked in on the group after a week, she found her students stringing French sentences together.

Thomas knew that if he removed the anxiety of ambiguity in the classroom, language would come much more easily to the students. Anxiety and ambiguity were holding them back.

So remember Michel Thomas the next time you have to give someone an ambiguous task: the best strategy is to make them feel relaxed.

### 8. It’s crucial to be able to cope with ambiguity in high-pressure situations. 

What would you do if someone put a gun to your mother's head and said they would kill her if you didn't give them your money? In profoundly stressful situations like that, it's critical to stay calm.

Military strategists, for instance, have to cope with a lot of high-pressure ambiguity. For a long time, they traditionally preferred more straightforward tactics, but this has been changing in recent decades.

In the Yom Kippur war of October 1973, for example, Israeli strategists had access to mounting evidence that Syria and Egypt were preparing an attack. They neglected to prepare for it, however, even when the USSR started evacuating its citizens out of the two countries.

The Israeli army wasn't as prepared as they could have been because their leaders failed to consider seriously the different possible outcomes. Militaries now often keep _red teams_ in their units to try to avoid these kinds of mistakes. Red teams are always on the lookout for weaknesses in the leaders' proposed decisions.

Professional negotiators are also trained in dealing with stressful ambiguity, especially because negotiations can take a long time.

The 1993 conflict between police forces and a cult in Waco, Texas was a dramatic illustration of this. David Koresh, the cult leader, and his followers were suspected of several federal crimes but refused to leave their ranch.

A negotiator named Gary Noesner was dispatched to the case. Koresh slowly engaged with the negotiation process, releasing 18 children and two adults after three days. The police had no patience for situation's ambiguity, however, even after further hostages were released. Noesner was taken off the team after 26 days.

None of the members agreed to leave the ranch after Noesner left. Tragically, they ended up killing themselves so they wouldn't have to surrender.

### 9. Final summary 

The key message in this book:

**Adult humans are naturally unsettled by things they don't understand. Light ambiguity can be amusing or intriguing, but more extreme cases of ambiguity can induce us to make rash decisions. All in all, ambiguity will always be a part of our lives. It's important to be able to cope with it, especially in dangerous situations like war or hostage negotiation.**

Actionable advice:

**Don't make important decisions when you're stressed out.**

Don't quit your job just because you've had a bad day at work. Wait until you've calmed down before making any big decisions. If you're feeling anxious about some kind of ambiguity, you're not thinking straight.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Ignorance_** **by Stuart Firestein**

_Ignorance_ investigates the strengths and weaknesses of the scientific method and reveals the importance of asking the right questions over the discovery of simple facts. Using real-life examples from history, _Ignorance_ shows that it is our awareness of what we _don't_ know that drives scientific discovery.
---

### Jamie Holmes

Jamie Holmes was an economics research coordinator at Harvard before he became a writer. His work has been published by the _New York Times_, _CNN_ and _The Huffington Post_. _Nonsense_ is his first book.

