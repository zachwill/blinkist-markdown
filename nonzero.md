---
id: 53bc47723439330007020000
slug: nonzero-en
published_date: 2014-07-08T00:00:00.000+00:00
author: Robert Wright
title: Nonzero
subtitle: The Logic of Human Destiny
main_color: 819DC5
text_color: 4E6078
---

# Nonzero

_The Logic of Human Destiny_

**Robert Wright**

_Nonzero_ examines our evolutionary and cultural history, concluding that the direction of our evolution was determined by a natural tendency to create win-win situations. It argues that humankind's biological and cultural history is directional, even purposeful, pushing towards increasing complexity and ultimately goodness.

---
### 1. What’s in it for me? Learn what’s behind human progress. 

Have you ever wondered how it is that human life got to where it is today? That out of the many possible ways that humanity could have developed, _this_ is the way things ended up? Digging deeper, have you ever wondered what the purpose of it all is, anyway?

_Nonzero_ attempts answer to these profound questions by explaining human history through the lens of _non-zero-sum_ _interactions_ played out between individuals and societies.

But what exactly are _non-zero-sum_ _interactions_?

Some interactions, where one person wins and another loses, are called zero-sum games. If I add my winnings to your losses, the net result is a big fat zero. A non-zero-sum interaction, in contrast, is often a win-win situation in which both sides can benefit from the interaction.

Think of a vendor selling a car to a client: though the motives of each party are different, they are both interested in a successful deal. As such, they communicate — and perhaps both even make concessions — so that, in the end, one has gained a car while the other has gained another sale, thus resulting in a net gain for everyone.

_Nonzero_ argues that the historical accumulation of these win-win interactions has acted as humanity's engine towards progress and will ultimately propel us to a brighter future.

In these blinks, you'll find out:

  * how whale hunting led one culture to develop a strict social hierarchy,

  * why everybody benefits from war, and

  * why the Dark Ages actually weren't all that dark.

### 2. The complex life on planet Earth is a result of growing cooperation between cells and organisms. 

When dealing with a problem, do you generally look to profit at someone else's expense, sacrifice your own needs for the benefits of others or find a solution that will benefit everybody? Most people look for a win-win solution in which everyone profits at least a little bit. In other words: a _non-zero-sum_ _interaction_.

As we'll see, these interactions are far more than a mere decision-making strategy: they're actually the basis of the development of life on Earth.

In fact, the very development of multicellular life owes itself to non-zero-sum interactions. In the beginning, life on Earth was composed exclusively of simple single-cell organisms, such as bacteria or fungi.

Eventually, these individual cells began to communicate. They then "realized" that they all had essentially the same interest — i.e., spreading their genes — and that they could better achieve this by pooling their efforts and joining together, thus forming the first multicelled life-forms.

We can see the legacy of this realization in our own bodies, and in those of plants and animals:

Each of our cells has two vital components: the nucleus, where DNA is stored, and mitochondria, which generate energy. Originally, these two cell parts were actually separate, individual cells, which came together in a non-zero-sum interaction to form the basis of complex life.

And as multicellular life continued to grow, it also had to grow increasingly complex in order to survive the competition with other life-forms.

In order to accomplish this, cells began specializing in various tasks, such as maintaining the health of the organism (for example, by producing blood), getting the lay of the land (the senses) and handling the big decision-making (the nervous system).

This development continuously accelerated as organisms competed for resources, like food and shelter, both within their own species and with others. For example, when a predator species becomes faster or smarter, its prey must adapt accordingly in order to survive.

It is this evolutionary "arms race" that led to the development of increasingly complicated life-forms, and eventually to human society and culture.

### 3. Human history is part of a cultural evolution towards increasing complexity. 

Just as organisms have grown increasingly complex, human societies also have ventured along the same path. And it seems that the path of civilization is a one way street, always pointing towards more sophisticated levels of organization.

Across the world, from China to the New World, humankind independently evolved in the same way. We started with savagery, where we lived in small hunter-gatherer-tribes in which almost everyone was related. Then, we moved to barbarism, in which chiefdoms and ancient city-states formed the beginnings of political leadership. Finally, we arrived at civilization, with its empires and nation-states and complicated political and economic structures.

With each rung up this "cultural ladder," both the amount and scope of non-zero-sum relationships increased to everyone's benefit. These interactions included things like developing more efficient systems of trade and transport and more efficient systems of governance.

These changes, in turn, made it much easier for people to produce, sell and consume goods and ideas.

For example, in 4000 BC, many Mesopotamian villages united together to form the first city-state of Uruk, ruled by a single king. Uruk then went on to develop a bureaucratic system for taxation and political administration, as well as an elaborate system of roads whose arms stretched out into the surrounding areas.

These roads, combined with an efficient political organ, helped make it easier and more profitable for merchants to trade within their domain, and eventually helped turn the city into the cultural and economic center of ancient Iraq.

Of course, these developments didn't just stop. We are now approaching the stage of development where humanity could join together to become a _planetary_ _brain_.

As connective technologies, such as the internet, are becoming more widespread, humanity is moving closer to becoming a single entity that creates and shares knowledge and information without regard to borders or boundaries. You can think of individuals as becoming the "neurons" of one single, global brain — perhaps the ultimate stage in human development!

### 4. Our cultural evolution is the result of an increase in non-zero-sum interactions. 

When you come home with your bags of groceries, do you share some with your neighbors? Families of the African tribe !Kung San do. When they hunt down a giraffe, they share the meat with a neighboring family, knowing that one day they might have to depend on the meat hunted by other families if they ever fall on hard times.

In fact, they, and humans generally, act in this way because cooperation is hard-wired into our brains; we have evolved to understand that we need to work together in order to survive on this planet.

As a result, this constant stream of non-zero-sum interactions has led our societies to be heavily dependent upon each other.

Just think, for example, of today's global economy. Nearly all of the world's economies have joined together in trade — to everyone's benefit. By encouraging trade between nations, countries that participate can then buy and sell the resources and products that they need in order to progress.

A consequence of this, however, is a very strong mutual dependency that nations have on one another. For example, a country might depend heavily on raw material imports from one nation in order to export manufactured products to another. Were these relationships to suddenly disappear, these countries would collapse.

And it is this increased interdependence that pushes our political and social structures to become more sophisticated. As interdependence grows in a system, more complex social hierarchies must be developed in order to manage these structures.

We can see an example of this in the Taleumit Eskimo tribes living on the Arctic shore. As a whale-hunting people, they have stricter social hierarchies than settlements have further inland, because whale hunting requires team effort, division of labor and correspondent coordination from a supervisor if it's to be successful.

Likewise, the more integrated and complex our globalized economy becomes, the more complex the social hierarchies will be that manage them.

### 5. Technological innovations accelerate cultural evolution. 

Let's consider a very straightforward non-zero-sum interaction: imagine you're an American shoe manufacturer and you want to expand your reach by selling your shoes in a Chinese supermarket. Both of you benefit from this situation: you sell more shoes and they acquire more customers.

However, although the benefits are clear, there are still two obstacles you must overcome before you can seal the deal:

  1. The _trust_ _barrier_. What if the store manager suddenly chooses to ignore the agreements you made?

  2. The _communication_ _barrier_. How can you make agreements with Chinese companies when you're not in China and don't speak Chinese?

A century ago, these barriers would have been painstakingly difficult to overcome. How could you know what the store manager was doing? How would you make sure you understood one another?

Today, this is much easier thanks to technologies such as flight, telephones and the internet, which help us to overcome these obstacles and thus progress further as a society. In fact, technological revolutions such as these often precede giant leaps in societal advancement.

A perfect example of this is one of the most ancient forms of technology: agriculture.

Before the development of farming around 10,000 years ago, human societies around the world were composed of individual tribes who lived in small nomadic, family-based groups. Agriculture, however, changed everything.

Agriculture made it possible to produce more food, which meant that communities came together to share the bounty. And this greater concentration of people then led to more complex social organization, and leadership emerged to link these villages together and create a common political base for prospering trade.

With the discovery of farming in North America, for example, the loose Native American tribes began to come together as larger "chiefdoms" of communities that agreed on one political leader, the"Big Man," whose most important functions included settling trade disputes, coordinating labor division and stimulating productivity with religious ritual.

As these technologies continue to grow, there's no telling what kinds of amazing advances we will make as a global society.

### 6. Competition ultimately fosters non-zero-sum cooperation. 

Have you ever put everything you had into a competition, hoping to come out on top? This competitive drive is totally natural. In fact, archeologists have found evidence for competition — collectively through war or individually through a struggle for status — in every primitive tribe and ancient society.

This competition is the direct result of the natural human tendency to strive to put ourselves in the most advantageous position possible. Although such conflict is usually _zero-sum_ — i.e., one party wins and the others lose — in the long run, it actually promotes teamwork.

For example, we might think of war as a force that is purely divisive, focusing on the bombs and bloodshed that accompany it. However, the threat of war actually pulls members of a society closer together.

In order to win a war, a society needs to expand its non-zero-sum efforts, such as better organization, division of labor, cooperation and solidarity. Here, military power alone is often insufficient, as the military itself depends upon the economic prosperity of the nation: the stronger the economy, the greater the arsenal and the more qualified the troops.

Moreover, war is not just about crushing an opponent. It can also promote alliances across societies, where political leaders join together to secure a peaceful future or fight a common enemy.

In addition, the personal struggle for social status helps to drive innovation. This is because successfully using and/or creating new technology can raise an individual's social status, either directly, through praise or reward, or indirectly, by opening up more opportunities for innovators.

Consider this: Agriculture didn't start in one place and then spread throughout the world. The idea of farming arose independently at least five times — not because of any great economic foresight, but because amassing food was a handy way to elevate an individual's social prestige. For instance, in some societies, a prosperous farmer could provide for multiple wives.

Without this competition for resources, however, our prosperous farmer would have never had the fields to plow, since agriculture wouldn't have come about.

> _"If two nearby societies are in contact for any length of time, they will either trade or fight."_

### 7. During periods of  chaos and stagnation, human progress does not go backwards. 

Human history has seen some crazy times — the Age of Barbarism, the Dark Ages, two World Wars, to name just a few. These dark times can make us question our very nature, leading some to believe that human progress has waxed and waned over time.

Contrary to this belief, however, humanity didn't go "backwards" during these dark periods. In fact, our cultural evolution continued onward.

The concept of the culture-defying, eternally savage Barbarian — i.e., a people and clans that are perceived "uncivilized" by surrounding societies — is nothing more than a myth.

Although historians have found many examples of barbarian hordes invading and raiding more advanced societies considered to be higher on the "cultural ladder," such as the devastating sieges of the city-state Rome by Germanic Visigoths around 410 AD, these barbarian societies themselves were socially evolving groups, and thus made valuable contributions to the world's cultural richness.

For example, after invading Rome around 450 AD, the Barbarians learned to instrumentalize and expand the Roman tax apparatus, and they even established military outposts to ensure safety along trade routes.

And because of their nomadic lifestyle, they were important mixers and spreaders of cultural items, assimilating songs, games, technologies, etc., from one neighboring society — or one they had plundered — and bringing it to the next.

In addition, even the infamous Dark Ages did not reverse the course of human development, contrary to popular opinion.

The Dark Ages refer to the period of economic and intellectual "stagnation" that allegedly befell Europe in the early Middle Ages, characterized by the disappearance of the artistic flair of the Classical period and the introduction of the _feudal_ _system_, where peasants lived much like slaves under their feudal lordship.

And yet, this time period also contained some crucial economic developments, such as the invention and steady improvement of the water wheel and the advent of the textile industry. These innovations lay the groundwork for the capitalist economies that we find ourselves in today.

Now that we've seen how non-zero-sum interactions have shaped our biological and cultural evolution, the following blinks examine what's in store for the future of human progress.

### 8. Cultural evolution comes with positive moral progress. 

Among today's nations, you'll find that those who are the most liberal and open-minded tend to be the most prosperous.

Why is this?

It is because individual freedom is essential to fully realizing economic potential. The free flow of goods and information between all members of society "lubricates" trade and innovation, and thus brings economic prosperity with it.

For example, the Industrial Revolution started in England in part because it had the most liberal trade and press policies in late 18th century Europe. Good ideas, therefore, could spread without unnecessary constraints, thus inspiring even more new inventions and promoting the dissemination of useful machinery and business strategies, such as the assembly line, that then led to rapid development.

What's more, the increased interdependence between nations that comes with liberal trade policy promotes greater tolerance.

If one country has a significant amount of business partners in another nation, it would make sense for them to have a friendly foreign policy stance towards that nation in order to maintain and foster trade. For example, if you were the Head of State, you might relax your customs barriers or allow free migration.

A great example of this is the open-border principle of the EU. Although it was primarily established to boost trade within the EU, it also brought more personal freedom to EU-citizens. Even if this tolerance is merely a byproduct of pursuing our own economic interests, true acceptance of other peoples and nations will grow as these relationships develop.

Finally, technological development in global communication systems, such as the internet, unite people with shared interests across the globe, while simultaneously dissolving divisions based on race, religion or gender.

As our shared interests bring people together all over the world, it becomes difficult to maintain the hatred and prejudice that divides us.

Yet, even as the prospects of world peace seem closer than ever, there are still dangers that threaten to tear it all apart.

### 9. Our current era is the most dangerous, yet promising, evolutionary threshold ever. 

Globalization has brought us many benefits. The computer or mobile phone that you are reading this on, for example, is likely the result of the cooperation of many different national and international players.

But globalization can also breed anger and, possibly, deadly resistance.

For example, the internet can bring even the most obscure niche interest groups — fan clubs like "Persian Cat Lovers of Colorado" — together to pursue a common interest. But not all of those interests are benevolent. The internet can also unite tribes of _angry_ _men._

Disgruntled by the rapid process of globalization, where they feel they are losing control over their own fate and being robbed of their sovereignty, these angry men find themselves more empowered than ever before.

In our globalized system, it has become easier to procure pretty much everything, and this includes lethal technologies, such as nuclear or bio-chemical weapons. If we can manage to control these threats, however, we could finally move into an era of global peace.

In fact, the global threats of terrorism or environmental catastrophe have an unexpected silver lining: fighting against them unites nations and strengthens international policy. If the international community works together, then they have a better chance of understanding the frustrations of these angry men.

They can then use this understanding to slow the pace of globalization, thus mollifying their fears and the threat to global civilization that accompanies them.

One way to do this might be to empower supranational institutions, such as the EU, to regulate socially damaging trade. The EU, for example, has banned genetically altered food that many see as a threat to their way of life.

In doing so, we can ensure that the values and ideals of peoples and communities are maintained despite the pressures of globalized capitalism. This may help us calm the angry men and move on towards a period of both great peace and prosperity.

### 10. It’s likely that life on Earth has a higher purpose. 

Why are we here? Why did we develop the way that we did, and not some other way? Was it all random, or are we the products of some sort of "design?"

Nature's persistence towards complexity does, in fact, suggest an underlying purpose to our existence. We can see this in the apparent teleological nature of biological and cultural evolution: no matter how radically interrupted, life eventually snaps back to pursuing increasing complexity, flexibility and intelligence.

As we've seen by now — despite the wars, pillaging, and brutality — humanity's progress has continued along the same path.

This has been possible because of the fundamental goodness of nature, which has gifted us with the propensity for cooperation and altruism that ultimately developed into our enormous capacity to love even those we are not related to.

Indeed, over the course of our cultural evolution, the increase of non-zero-sum interactions — such as those we've seen in these blinks — seems to bring an increase in personal freedom, peace and tolerance, suggesting that nature tips the scale in favor of goodness.

Finally, nature has given us the _consciousness_ that gives our life meaning.

We have to ask: Why are we even conscious? Consciousness itself is functionally useless for intelligence. For example, a computer is intelligent, but a conscious mind is not necessary for it to perform any of its tasks.

So _why_ did nature create consciousness when it could have easily produced a world full of automatons? This is, to this day, a mystery to modern science.

But there is one clue that can offer insight into the reason for human consciousness: consciousness is what gives us morality, good and bad. With it, we can experience our surroundings, reflect on them and make decisions on a level that transcends the mere calculation of facts and numbers.

Perhaps history is better viewed not as the product of divinity but as its _realization_. We see that humanity, and indeed all life, ultimately moves towards good. Maybe nature programmed it to do so from the very beginning?

> _"The history of life on earth is too good a story not to have been written."_

### 11. Final summary 

The key message in this book:

**Life** **on** **Earth** **has** **a** **long** **history** **of** **cooperation,** **leading** **it** **to** **evolve** **towards** **ever-increasing** **complexity.** **When** **we** **examine** **the** **long** **histories** **of** **organic** **and** **cultural** **evolution,** **we** **notice** **that** **they** **seem** **to** **be** **directed** **towards** **something** **that** **is** **essentially** **purposeful** **and** **good.**
---

### Robert Wright

Robert Wright is a U.S. American scholar and journalist, famous as an advocate for cultural evolutionism and the critically acclaimed author of a number of books, including _The_ _Moral_ _Animal_ and _The_ _Evolution_ _of_ _God._

