---
id: 5e839a026cee070006fb6050
slug: notorious-rbg-en
published_date: 2020-04-05T00:00:00.000+00:00
author: Irin Carmon and Shana Knizhnik
title: Notorious RBG
subtitle: The Life and Times of Ruth Bader Ginsburg
main_color: CFAA2B
text_color: 826B1B
---

# Notorious RBG

_The Life and Times of Ruth Bader Ginsburg_

**Irin Carmon and Shana Knizhnik**

_Notorious RBG_ (2015) chronicles the life story of US Supreme Court Justice Ruth Bader Ginsberg. From her childhood in Brooklyn — when she was known by her nickname, Kiki — to capturing the public imagination with her scathing dissents on the bench, these blinks portray one woman's relentless fight to give American women and men equal rights under the country's legislation.

---
### 1. What’s in it for me? Get to know America’s most iconic Supreme Court justice. 

In 2013, Supreme Court Justice Ruth Bader Ginsberg, known by her fans as RBG, was 80 years old. Many said that her career was over. But then something interesting happened. Inspired by her fiery liberal dissents, millennials made RBG an internet sensation. Coauthor Shana Knizhnik, who was a young NYU law student at the time, saw a Facebook post referring to the 5-foot-tall judge as "Notorious R.B.G." — a tongue-in-cheek comparison to the late 300-pound American rapper Notorious B.I.G. Taking the reference further, Knizhnik created a Tumblr tribute page for the judge by the same name.

Among other tributes, _Notorious R.B.G._ went viral. Virtually overnight, RBG became an unlikely pop icon. Yet while she gained more traction online with every dissent, many of her new fans didn't realize that this was just one chapter in her story.

In these blinks, you'll find out

  * which necklace RBG wears when reading a dissent;

  * why RBG kept her second pregnancy a secret from her employer; and

  * two pieces of advice from RBG's mother that led to her success, in and out of the courtroom.

### 2. Born in Brooklyn, RBG had a normal childhood but experienced tragedy in her family. 

Known during her childhood by her nickname Kiki, Joan Ruth Bader was born in Brooklyn's Flatbush neighborhood on March 15, 1933 — a time when prejudiced treatment of Jews was common, even among ethnic minorities who'd also recently arrived in the United States. Still, RBG remembers her childhood fondly.

RBG's mother, Celia Amster Bader, was a first-generation American and one of seven children whose parents had fled the Austro-Hungarian Empire. Though Celia had graduated with top grades from high school, she didn't go on to college. Instead, she was expected to be a loving wife, and her income as a bookkeeper was partially divested to fund her brother's college tuition at Cornell. When it came to her own daughter, she instilled a love for learning and hoped that Kiki, at least, would attend university one day.

As a young woman, the popular but quiet Kiki spent ample time at the library and was especially fond of Nancy Drew mystery novels. She admired Nancy as an independent woman with a knack for adventure and a dominant role in her romantic relationship. Kiki also attended Camp Che-Na-Wah in the Adirondacks in the summer, and played the cello. It was a fairly normal childhood — so normal that no one at the time predicted the success she would go on to achieve.

But things weren't always easy for Kiki. When she was two years old, her sister Marylyn died from meningitis, and her mother was diagnosed with cervical cancer when she was 13. Not wanting her friends to feel sorry for her, Kiki kept these facts about her family a secret. She also knew that she could make her mother happy by focusing on her studies. The work paid off, and Kiki was accepted into Cornell with multiple scholarships to her name. But Kiki didn't end up attending graduation. The night before the ceremony, her mother tragically passed away.

Despite her early death, Celia remained an important inspiration throughout Kiki's life. Kiki particularly held onto two pieces of advice from her mother: The first was to be a lady — meaning that she should never let herself be overcome by pointless emotions like anger or envy. The other lesson her mother had often repeated was to be independent. As it turns out, Kiki's mother had exercised her own independence by saving an additional US$8,000 for her daughter's education.

### 3. As a student at Cornell, Kiki’s future husband was the first boy to appreciate her intelligence. 

When Kiki arrived at Cornell in Ithaca, New York in 1950, the university had a ratio of four men to every woman. Kiki found that though many of her female peers at Cornell were smarter than their male counterparts, they often suppressed their intelligence. Instead of a career, they were after what was known as a "MRS. degree" — in other words, finding a husband.

Kiki was different. She was in the Alpha Epsilon Phi sorority but avoided parties; instead, she'd smuggle books into the bathroom to study while her peers celebrated. She chose to major in government, studying constitutional law from the acclaimed professor Robert E. Cushman. She began to observe things about the United States she hadn't noticed before — like the fact that the American army was segregated until the end of World War II.

During the Red Scare — the widespread fear of communism during the Cold War — Cornell zoology professor Marcus Singer was indicted and deprived of his teaching duties for protecting the names of his fellow Marxist study group members. At the time, Kiki was assisting Cushman with an exhibition about censorship; her mentor pointed out that lawyers had been defending the rights of the censored professor.

Witnessing this injustice led Kiki to the realization that she herself wanted to work as a lawyer after graduating. It was not only a good career option, but also a way to help society. Her father expressed reservations, wondering if she would be able to support herself in a career that wasn't easy for women, but he was appeased by the fact that Kiki had found a husband.

Kiki didn't see her relationship with Marty Ginsburg that way. She had initially met Marty through friends while they were both dating other people. But as they became closer and romantically interested in one another, Kiki noticed that not only was Marty smarter than her boyfriend at Columbia Law School — he was also the first boy she met who cared that _she_ was smart.

Marty won Kiki over by showing her how much he respected her and her career ambitions. Days after Kiki graduated from Cornell in 1954, she married Marty in his family's living room with 18 guests in attendance, symbolizing the Jewish number for life.

### 4. In spite of setbacks, RBG made the Law Review at both Harvard and Columbia. 

Like Marty, RBG was accepted at Harvard Law School. Before RBG enrolled, however, the couple had to spend two years at Fort Sills US Army base in Oklahoma, while Marty — who had been in the Reserve Officers' Training Corps during college — taught at the artillery school. Her career as a lawyer would have to wait, but she was determined to make the best of her time as an army wife.

In Oklahoma, RBG took the civil service exam in order to work for the government. One day, on a trip to the Social Security office, she mentioned in passing that she was pregnant. What she didn't realize was that being pregnant meant that she was immediately dropped to the lowest ranking position in the government — and forbidden from attending any job training that might've resulted in a promotion.

It wasn't just in Oklahoma that RBG faced discrimination. At Harvard Law School, where she began studying in 1956 as one of nine women in her class, RBG was unable to borrow books from the university's Lamont Library because women weren't allowed to enter. At the same time, she had a baby to take care of — her daughter Jane had been born the summer of 1955.

Ultimately, RBG's concern about managing law school as a new mother ended up working in her favor. She excelled in her studies and made the Law Review, a legal journal published by senior students and faculty, something that even Marty hadn't achieved.

Unfortunately, in RBG's second year at Harvard Law School, Marty was diagnosed with testicular cancer. Having lost her mother to cancer, the young law student wasn't going to let the disease determine the fate of another person she loved. RBG did everything in her power to see that Marty wouldn't fall behind in his studies. She retrieved notes from his classmates, typing them up with the help of a friend, and helped Marty dictate his papers until he dozed off around 2:00 a.m. She would then get to her own work — and discovered that she could get by on just two hours of sleep.

Thanks to RBG, Marty graduated and was hired as a tax attorney in New York City. With his health still tenuous, RBG refused to split up their family. She decided to finish her studies at Columbia Law School, where she continued to excel; she once again made the Law Review, and tied for first place in her graduating class.

### 5. RBG became a lawyer at a time when women were disregarded by most legal institutions. 

RBG graduated from Columbia Law School at the top of her class, but she wasn't surprised when she was refused a clerkship by Supreme Court Associate Justice Felix Frankfurter after being recommended by her professors. After all, she was well aware that law wasn't the most welcoming profession for women.

Still, her Columbia constitutional law professor Gerald Gunther was adamant about getting her a job. He promised Edmund L. Palmieri — the federal judge for the Southern District of New York — to find a male replacement if RBG couldn't keep up with her responsibilities while taking care of her child. His stipulation was that if the judge didn't hire her, Gunther would never recommend a clerk again.

After two successful years of clerking, RBG accepted an invitation in 1961 from a Columbia acquaintance, Hans Smit, to spend two years in Sweden cowriting a book about the country's judicial system. Marty agreed to hold down the fort, and RBG took the opportunity to test her independence.

In Sweden, RBG observed how women in the workforce had begun to make demands. Swedish women argued that their prescribed gender roles meant that they were to hold a job as well as raise their children — the latter being something they felt wasn't biologically connected to _having_ a child.

Back in the United States, RBG began to teach some classes at Columbia, which Smit had suggested she do to improve her public speaking skills. In 1963, she accepted an offer from Rutgers University to teach civil procedure, becoming the second woman ever to teach at Rutgers School of Law full-time. She then found out that she was pregnant with her second child.

This time, RBG wasn't going to let her pregnancy affect her employment, so she kept it a secret until after she had her contract renewed. She now knew that the law in most states mandated that employers could fire a woman for being pregnant. This was in line with most laws at the time, when women generally had fewer rights than men in all spheres of life.

But RBG was living on the cusp of huge social change. In the 1970s, women began to wake up to the discrimination they faced and rejected their prescribed domestic roles. Echoing the civil rights movement in the 1960s, they began to take to the streets to demand equal rights as citizens.

### 6. RBG found her life’s work in the fight against gender discrimination. 

RBG was characteristically one to keep her head down and pick her battles. It wasn't her style to go out and protest during the women's movement in the 1970s. Instead, she began her own kind of fight for women's rights — using her skills as a lawyer.

Inspired by her students' involvement in the women's rights movement, RBG offered a course about gender in law. And as a volunteer lawyer for the American Civil Liberties Union, or ACLU — a nonprofit organization defending the liberties and rights of American citizens — she began looking into letters of complaint written by women.

In 1971, RBG volunteered to write the brief for _Reed v. Reed,_ a case in which she argued that the state had no right to assume that women were less capable of administering estates than men. She realized her only chance of winning lay in educating the judges about how women were effectively second class citizens. In her brief, Ginsberg pointed out that laws disabling women from political or economic activity were viewed as protective — while the same laws applied to ethnic or racial minorities would have been viewed as unlawful.

Although RBG won the case, she knew that her work for women's rights was just beginning. In 1972, RBG co-founded the ACLU's Women's Rights Project, or WRP, which had a three-fold mission: educating the public about sex discrimination, changing the law to give women equal rights as citizens, and supporting people to bring their cases to court.

As head of the WRP, RBG's first time arguing in front of the Supreme Court was with _Frontiero v. Richardson,_ in which she defended US Air Force lieutenant Sharron Frontiero after the lieutenant was denied benefits for her family on the basis of her gender. Frontiero won, and the court ruled that her work benefits for her family were of equal importance to her fellow male servicemembers'.

It wasn't only women that RBG fought for. In 1975, RBG represented Stephen Wiesenfeld, a widower who had been denied Social Security benefits to support his infant child on the basis that he was a man. For RBG, the _Weinberger v. Wiesenfeld_ win represented an opportunity to show that being discriminated against on the basis of gender was disadvantageous for everyone. It was the beginning of a strategy that she developed out of a belief that change happens through hard work, one step at a time.

### 7. RBG’s dedicated work and supportive husband led to her appointment to the Supreme Court in 1993. 

In 1972, RBG accepted Columbia Law School's offer to become a tenured professor — a first for women at that institution. But the fact that Columbia was her alma mater didn't stop her from taking on the institution for its own shortcomings in gender descrimination. Immediately after being hired, she helped female employees at Columbia win a class-action lawsuit to get equal pension benefits and pay.

In 1980, RBG's career took a turn when President Jimmy Carter nominated her to the United States Court of Appeals for the D.C. Circuit. During her time as a judge on the D.C. Circuit, RBG became widely known as a moderate who focused on finding compromises wherever possible. The work was often relatively boring, but the position was known as a path to becoming a Supreme Court justice.

When it came time for President Bill Clinton to appoint a nominee for the Supreme Court in 1993, RBG's name wasn't at the top of the list. This time, Marty took the reins. Having become a successful New York tax lawyer in his own right, Marty used his network to make Clinton aware of his brilliant wife. Luckily, Clinton's favorite nominee, Mario Cuomo, backed out before the President even made his offer — and RBG was brought in for an interview.

Minutes into her interview, the President saw RBG's potential and nominated her to be the new justice. The Senate confirmed her with a 96 to 3 vote, making her the second female in the history of the US to sit on the Supreme Court.

In her early days as a Supreme Court justice, RBG was never the most liberal person in the room. Her mission was to find a consensus with her fellow judges and move the court forward, rather than making sweeping demands. Still, in 1996, a case came up that gave her the chance to continue her work for women's rights.

The case in question was _United States v. Virginia,_ which sought to challenge the exclusion of women from the Virginia Military Institute. RBG took on the assumption that women weren't capable of being worthy students of the academy and backed the women who wanted to attend. In this case, she educated her fellow justices about why there was no justification for excluding women — a foreshadowing of her position and strategy over the next two decades of her tenure.

### 8. RBG’s relationships were defined by mutual respect, both on and off the bench. 

With over 30 years' experience, RBG knew that the only way to convince her fellow justices of the equal stature of women was by keeping her cool throughout the process. Her calm demeanor wasn't only successful in the court; it also created the foundation of her five decades of marriage.

Marty and RBG had a lifelong relationship characterized by mutual respect. Just as RBG had left Harvard to support her husband's career, he too left his career in New York when she was appointed to the D.C. Circuit _._ His support for her career didn't end there. When RBG crashed her car, Marty began driving her to the court every day until she was appointed a driver. He also cooked her dinner — a department in which he had the greater talent — and kept her on track during grueling work days by reminding her to sleep.

Marty's respect and support of RBG's career when it grew bigger than his own became a glowing example of how a progressive family structure might function. RBG commonly referred to Marty as her life partner and said that his humor complemented her often serious demeanor. Understandably, Marty's diagnosis and eventual death from metastatic cancer in 2010 was one of the greatest losses in RBG's life.

Apart from Marty, her children, and grandchildren, RBG enjoyed friendships with many of her fellow Supreme Court judges, such as Sandra Day O'Connor. When RBG was diagnosed with colorectal cancer in 1999, it was O'Connor who advised her to receive her chemotherapy treatments on Fridays so that she would have the weekend to recover. Thanks to this and the support of her other fellow justices, she didn't miss a day on the bench while recovering her health.

But RBG wasn't only friends with the court's liberal justices like O'Connor. She was also close friends with conservative Justice Antonin Scalia — even if they disagreed on their politics. After all, the two judges did have something in common: a shared passion for opera.

### 9. RBG began a streak of fiery dissents after the Supreme Court swung right during Bush’s presidency. 

Nowadays, when people see RBG wearing a bib necklace made of glass beads on velvet, they know what card she's about to pull. The Banana Republic necklace, which she received in a gift bag at _Glamour's_ Women of the Year event in 2012, is her designated dissent necklace.

Contrary to her public image today, RBG wasn't always a dissenter. In fact, she only ever argued when she deemed it completely necessary. But as history would have it, her voice was destined to maintain the balance of the court.

During the presidential election in 2000, RBG was one of four to dissent against the election of George W. Bush after the recount of votes in Florida was called for in _Bush v. Gore_. With the election of Bush, things soured for liberal politics. During his presidency, Bush appointed two conservative justices, shifting the entire composition of the court to the right. By 2006, RBG was suddenly one of the most liberal of the nine justices. Increasingly, this meant the need to raise her voice in dissent.

For example, RBG firmly believed that a woman's right to controlling her reproductive life was essential to her equal participation in the economic and social realms of the US. On April 18, 2007, RBG fired a fierce dissent with _Gonzales v. Carhart_ — an abortion case that ruled to uphold restrictions on partial birth abortion. Her statement was a criticism of her fellow justices' intentions, summarizing that the court "pretends that its decision protects women."

That May, RBG dissented the court's decision that ruled against pay discrimination based on gender in _Ledbetter v. Goodyear._ She again dissented in _Burwell v. Hobby Lobby Stores_ in 2013, which ruled to allow employers to deny insurance coverage of birth control on the grounds of religion.

In June 2013, _Shelby County v Holder_ ruled to dismantle a key provision of **** the Voting Rights Act, a landmark legislation from 1965 that prohibited racial discrimination in voting. Eighty-year-old RBG delivered a fiery dissent, pointing out that the Voting Rights Act had been effective and that the rule was, in her words, like "throwing away your umbrella in a rainstorm because you are not getting wet." She hoped that the public might hear about the case and stand up in favor of the provision.

### 10. Since 2013, RBG has become an unlikely pop icon who has refused to retire in spite of her age. 

Historically, one of the main reasons to dissent was in hopes that the public outside of the court would listen. And thanks to the internet, when it came to RBG's dissents, they did.

The June 2013 ruling to dismantle key provisions in the Voting Rights Act drove many progressives to despair. Thousands of polling places have since closed, and access to early voting was curtailed in areas that disproportionately affect minority communities. 

Yet Ginsberg's dissent didn't go unnoticed. Inspired by her incendiary fight for justice, a number of millennials began to post about her online. Among the many RBG memes to go viral was a picture of her sporting a crown while wearing her dissent necklace, coupled with the slogan "Can't Spell Truth Without Ruth."

Over the next months, a woman who had once been considered austere in her politics became a pop icon and the voice of women of every generation. RBG's name was suddenly ubiquitous among feminists and beyond. Every time she delivered a dissent, a new wave of internet posts would follow. She inspired RBG tattoos, a recurring character on _Saturday Night Live_, and even Halloween costumes. And, though her own humor remained characteristically dry, RBG did find humor in her impersonators. To the surprise of some of her closest friends, she embraced her new role as a public figure. She even jokingly acknowledged the similarities with her namesake, the Notorious B.I.G. — pointing out that they both grew up in Brooklyn.

RBG's rise to public consciousness was astounding — especially given that her critics had been suggesting she retire since 2009, when she was diagnosed with an early form of pancreatic cancer. But RBG wasn't ready to stop working, even if that meant President Obama couldn't replace her with a liberal judge during his time in office. 

RBG still feels she has work to accomplish. She feels compelled to represent the Supreme Court as one of its few female justices. Regarding retirement, she's said it would take forgetting the names of the cases that she once recited at the tip of her tongue to stop her from continuing her life's work: fighting for Americans on the nation's highest court.

### 11. Final summary 

The key message in these blinks:

**After losing her mother at the age of 17, RBG made her way to the top of the US judiciary system by putting in the work and remaining calm in the face of her adversaries. Throughout her career, she was considered a moderate and known for her serious, quiet demeanor. Only after Bush installed two new conservative judges in the Supreme Court did she feel the need to voice dissents that have since captured the public imagination — making her an unlikely pop icon and modern feminist hero.**

Actionable advice:

**Work out like the Notorious RBG.**

After RBG was diagnosed with colorectal cancer in 1999, Marty suggested that she start working out with a trainer. Having water skied up until her 70s, RBG knew that her fitness was key to staying on her best mental game. To try her trainer Bryant Johnson's signature workout, start by warming up on the elliptical for five minutes. Then, holding a 12-pound ball, sit on a bench. Stand and press the ball to your chest. Toss the ball to your workout partner, and have them hand it back to you. Sit back down and repeat this exercise ten times.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _My Beloved World,_** **by Sonia Sotomayor**

As you've just learned, RBG was the second woman ever to sit on the US Supreme Court. If you were inspired by RBG, you'll discover another hero in Sonia Sotomayor. Nominated by President Obama to serve the Supreme Court in 2009, Sotomayor joined as the third woman and first Latina — to the delight of RBG. 

In _My Beloved World,_ Sotomayor details her journey to becoming a Supreme Court justice, from her childhood hardships and winding path to studying at Princeton and Yale Law School. To discover her story of resilience, friendships, and lifelong learning, head over to our blinks to _My Beloved World._
---

### Irin Carmon and Shana Knizhnik

Irin Carmon is an Israeli-American journalist and commentator. She is a CNN contributor, a senior correspondent at _New York Magazine,_ and was previously a national reporter at MSNBC.

Shana Knizhnik is an American lawyer and author from Philadelphia. In 2013, she created the viral _Notorious R.B.G._ Tumblr, where she chronicled memes and articles about Justice Ginsberg.

