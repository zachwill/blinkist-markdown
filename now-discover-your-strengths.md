---
id: 56a6b130fb7e870007000039
slug: now-discover-your-strengths-en
published_date: 2016-01-29T00:00:00.000+00:00
author: Marcus Buckingham
title: Now, Discover Your Strengths
subtitle: How To Develop Your Talents and Those of the People You Manage
main_color: B93225
text_color: B93225
---

# Now, Discover Your Strengths

_How To Develop Your Talents and Those of the People You Manage_

**Marcus Buckingham**

_Now, Discover Your Strengths_ (2004) provides insight into what strengths are, where they come from and why we should focus on them. These blinks outline tips and techniques for detecting natural talents and using them to put yourself or your employees on the path to excellence.

---
### 1. What’s in it for me? Make your strengths the focal point of your life. 

By this point in your life, you probably know all about your flaws and shortcomings. Your punctuation is poor, you have a hard time with networking and basketball just isn't your sport. 

But what are your strengths? What is a strength to begin with? And most importantly, why don't you focus on them?

In _Now, Discover Your Strengths_, you'll find valuable knowledge on strengths, where to spot them, how they're created and what they consist of. These blinks will also provide some great insights into how you can engage with your employees' strengths and make your business thrive. 

In these blinks, you'll discover

  * how your brain holds the key to your hidden talents;

  * why you should hope that showing empathy comes naturally to you; and

  * whether you're an analytic type or a commander.

### 2. A focus on correcting weaknesses has left our strengths ignored. 

In schools and in workplaces, we're constantly encouraged to seek out and correct our weaknesses in order to improve our productivity. But why? After all, it's uncovering and using your _strengths_ that will really send you on your way to success. 

How do you know a strength when you see one? There are many different answers to this question, but we can stick with the simplest: a strength is an activity that you can perform repeatedly to perfection. In other words, any activity that you can do well time and time again, while also enjoying yourself, is a strength.

It's no surprise that organizations where employees are able to use their strengths every day are successful and sustainable. The Gallup Organization asked 198,000 employees across different businesses whether they're able to do what they do best at work each day. 

The 20 percent of employees that strongly agreed with the question were 50 percent more likely to work in organizations with lower employee turnover, 38 percent more likely to work in more productive businesses and 44 percent more likely to work for companies with higher customer satisfaction. 

Despite this, many organizations still waste time, money and resources by concentrating on fixing employee weaknesses. Think about it: employees get sent off to special training programs not to expand on their strengths, but to correct weaknesses. By focusing on damage control rather than development, the true potential of employees is neglected. 

Of course, damage control is sometimes necessary; a clever but inarticulate employee would benefit greatly from a communication class. Still, damage control should not be the focal point of employee management. If you want your employees to develop and grow, and your company to grow with them, you need a different approach.

### 3. Combine natural talent with knowledge and skills to build a new strength. 

Everyone loves to do things they're good at. But why are we good at these things in the first place? We often hear that practice makes perfect — but that's not quite true! You've built up the strengths you have because you started off with a _natural talent_. 

Talents are patterns of thought and behavior that make some things easier for you. You might, for example, be great at connecting with strangers. That's a talent! Others don't find socializing so easy. Either way, we can't alter this predisposition. 

But what we can do is take a predisposition we like and develop it. Developing your strengths entails refining your talent with _knowledge_ and _skills_. 

Knowledge can be factual or experiential. If you're learning to play the piano, you'll have to retain a certain amount of factual knowledge, such as by learning different musical notations. But you'll also acquire knowledge through the experiences of practicing, performing and listening.

As you gather experiential knowledge, you also start building up skills. These are the key aspects of your chosen activity that will improve your performance. So, if you're an experienced public speaker, you'll have become skilled at catching and holding the attention of your audience.

Developing your strengths is a long-term process, and being specific about the skills you have will help you identify exactly where your strengths lie.

By way of comparison, think about how diverse the language of human flaws is. Neurosis, psychosis, mania, perfectionism, depression, anxiety — the labels for human difficulties are plentiful and precise. Strangely enough, the language for human strengths is incredibly limited.

Human Resources professionals are known to choose candidates based on their "people skills." Yet, two people with people skills could consist of one person that is great at building trust in long-term relationships, while the other is great at facilitating conversations.

It's time we give talents the close-up attention they deserve. In the next blink, we'll dive into one particularly important question: how are talents created?

### 4. The neuroscientific basis of natural talents makes them crucial to your strengths. 

In the previous blink, we learned that a talent is a pattern of thought or behavior, and that we naturally possess some patterns but not others. But why is that? To find out, we can take a closer look at the neuroscience behind the development of talents. 

Up until the age of three, our brains possess a hundred billion neurons that are capable of making around fifteen thousand connections between synapses. These synaptic connections allow brain cells to communicate with each other — which is a lot of communication! Does this mean we all had the brain of a potential genius as toddlers?

Not quite. For our brains and our intelligence to really develop, we had to lose a significant portion of synaptic connections. By the time we turn 15, billions of these connections are wiped out permanently. But it's actually a good thing: with fewer synaptic connections, sensory overload is prevented and specific connections can be reinforced. 

Some connections become much stronger than others, and these are the connections that allow us to perform certain actions with greater proficiency. This explains why certain movements, responses or activities come to you naturally, while others will always feel uncomfortable. 

In order to improve certain connections, we need to practice them so that they're strong enough to overtake the connections that cause behavior we don't want. 

But, of course, this isn't always possible. If you've ever worked on your ability to stay calm in an argument, you might find that your short-tempered reactions still take over. All that practice to help you reflect, maintain eye contact or keep an open and accepting attitude go right out the window!

### 5. Discover your talents by observing your responses to everyday situations. 

Take a look at all the people around you. Whether you're on the train or in your workplace, you're surrounded by people who possess strengths. Unfortunately, many strengths go to waste when talents remain undetected. 

Discovering talents is a crucial task. We're actually presented with numerous opportunities to uncover talents in our daily lives. But if you want to find talent where you least expect it, start by observing your spontaneous reactions. 

Say you're attending an event where you don't know too many people. If you hang back and chat to people you're already acquainted with, you might have a talent for reinforcing existing relationships. If you dive into the crowd and start making friends with strangers, you might have a talent for winning people over. 

Or, imagine one of your employees informs you that he can't come to work because his child is sick. If you automatically asked about the child, this might be a sign that you've got a natural ability for empathy. On the other hand, if you immediately thought about who could potentially fill in for your employee, you might have a talent for rapid problem solving. 

When examining your instinctive reactions, it helps to understand the two different indicators for different kinds of talent: _yearnings_ and _rapid learning_. 

Yearnings reveal the talents that appear early on in our lives. Mozart was only 12 years old when he wrote his first symphony! If you had a burning interest in as a child, why not explore it further? You may find a hidden talent there.

Rapid learning is another way to spot talent. When you pick up a new skill, how fast do you learn the ropes? If you master something in record time, there's a good chance that a natural talent is calling out to you. Believe it or not, Henri Matisse only began painting by chance. He started off with a basic manual to painting as his guide, and was accepted to Paris's most prestigious art school just four years later!

> _"Hide not your talents. They for use were made. What's a sundial in the shade?" - Benjamin Franklin_

### 6. Recognize employees’ specific talents so they can do what they do best. 

Now that we've explored the origins and signs of natural talents, it's time for you to start seeking them out in your employees. By uncovering their strengths, you'll help them and your organization achieve top performance and productivity. 

In the second blink, we learned that it was important to be specific when describing talents. This is particularly important in the workplace, where different roles have diverse requirements. There are at least 34 types of talents relevant to organizations, so let's look at a few that might already exist in your workplace. 

Do you have an _analytical_ employee? He's the one that challenges others, argues objectively and believes that data reveals the truth. He likes to back up his claims with logical thinking, and expects the same of others. This is something to remember when giving instructions to an analytical employee: keep them as detailed, clear and sensible as possible. 

Or perhaps you've discovered that one of your employees is a _commander_. She's rather skilled at convincing others to take her side in a conflict, and is always ready to share her opinion. She's determined and won't back down from confrontation when looking for solutions. 

Managers can rely on commanding types for insights about what's going on within workplace relations. But if they start overpowering quieter employees or ignoring commitments, you'll need to meet them head-on and back up your claims with concrete examples. 

Another employee of yours might be a _restorative_ type. He likes solving problems and is always there to lift spirits when things seem hopeless. There's no challenge for which he can't tease out a solution, which makes him great for positions in customer relations. Managers can turn to restorative types for help and expect a focused, reassuring response.

### 7. Final summary 

The key message in this book:

**It's time we stopped focusing on correcting weaknesses and started recognizing and developing our strengths. If you give yourself a chance and know what to look out for, you'll see natural talent jump out at you. These talents can be combined with skills and knowledge to bring powerful strengths to life.**

**Suggested** **further** **reading:** ** _First, Break all the Rules_** **by Marcus Buckingham and Curt Coffman**

The book shows how great management differs from conventional approaches. The authors demonstrate how some commonly held notions about career and management are actually misleading. Based on interviews conducted with successful managers (research that the authors did for Gallup) the book introduces its readers to the key notions that great managers — those who get their employees to achieve performance excellence — use in their jobs.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Marcus Buckingham

Marcus Buckingham is a _New York Times_ best-selling author, as well as a consultant and speaker. He is the co-author of the best-selling books _First, Break All the Rules_ and _The One Thing You Need to Know_.

