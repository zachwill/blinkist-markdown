---
id: 52822f17333464000c1f0000
slug: nudge-en
published_date: 2012-12-23T15:59:44.000+00:00
author: Richard H. Thaler & Cass R. Sunstein
title: Nudge
subtitle: Improving Decisions about Health, Wealth and Happiness
main_color: CD595D
text_color: B24D51
---

# Nudge

_Improving Decisions about Health, Wealth and Happiness_

**Richard H. Thaler & Cass R. Sunstein**

The message of _Nudge_ is to show us how we can be encouraged, with just a slight nudge or two, to make better decisions. The book starts by explaining the reasons for wrong decisions we make in everyday life.

---
### 1. We don’t always make decisions that are best for us in the long run. 

Most people have a pretty clear idea of what's good for them and what's not. They are determined to eat healthily, save money for retirement and not smoke. 

But when it comes to actually seeing these things through in everyday life, they often do the opposite: they eat unhealthily, they're reckless with money and they smoke too much. 

Unwise behaviors crop up in all parts of our lives, from eating habits to investment decisions to the way we structure our daily schedule. We grab a chocolate bar even though we know that an apple would be better for us; we hit snooze every time our alarm rings despite knowing this means we'll only have to rush later.

Such behavioral patterns cause problems when quick decisions result in serious long-term consequences.For example, statistics show that when it comes to saving money, most US citizens save very little even though they know this can cause them trouble later.

If we suddenly lose our income due to illness or unemployment or have unexpected expenses like getting our car fixed, and have no savings to fall back on, we become angry about the irrational decisions we made in the past. And yet, despite knowing this, we continue to make financial decisions based on short-term rather than long-term goals. 

**We don't always make decisions that are best for us in the long run.**

### 2. We often make bad decisions based on either too little or overly complex information. 

Why do we so often make decisions that aren't really the best choices for us? In many cases, we simply have too little or too much information, or we can't foresee the consequences of our actions. It's only when we have, and can process, the right amount of information that we are in a place to make rational decisions.

For example, when we have to choose an ice cream flavor at an ice cream shop, we usually make that decision rather quickly. We see which flavors there are and we know from experience that strawberry ice cream tastes better than mango, i.e., we have all the necessary information and can process it rapidly.

However, we feel differently in more serious situations, like when deciding whether to take out a loan. After all, there are all different kinds of loans: purchases on account, deferrals, consumer loans, securities, preliminary financing, interim financing, and many more. Some of them have a fixed interest rate, others a variable; all of them have different additional costs. And this information isn't exactly laid out in plain writing. Usually, it's hidden in the small print on the bright and colorful advertisement.

In other words, even when we are privy to all the relevant information, it can still be difficult to sort it all out and compare the services objectively. Situations like this can make us feel completely overwhelmed.

**We often make bad decisions based on either too little or overly complex information.**

### 3. Human beings act mainly on gut feelings. 

When we see a laughing baby, we can't help but smile. That "decision" to smile is made unconsciously; our response occurs entirely on its own.

But when we want to figure out the answer to 347 x 12, we confront this task in a conscious way, because finding the answer requires an effort and takes some time.

These examples illustrate two different systems of thought:

  1. In the case of the baby, we are using our _Automatic System_, i.e., our gut instinct.

  2. In the case of the math problem, we are using the rational or _Reflective System_.

We don't have the time or energy to reflect upon every little action we take on an average day, which is why we often let our Automatic System govern our actions.

It works well a lot of the time — but not all the time.

Grounded in simplifications, the Automatic System is often supported by a weak scaffolding of spontaneous emotions and subjective experiences. For instance, when we estimate our own likelihood of suffering from a stroke, the first thing we think about is the amount of people we know who've already had one.

If we don't know anybody who's had a stroke, we automatically assume that there's only a low risk that we'll have one. And so, even if our own risk is actually extremely high, we don't take any precautions. In other words, our gut feeling has led us first to a misjudgment and then to an unwise decision.

**Human beings act mainly on gut feelings.**

### 4. Sometimes we make faulty decisions because we succumb to temptation or act without thinking. 

If you offer a long-time smoker a cigarette while she's in the process of quitting, it'll be difficult for her to resist the temptation. Although she'd like to quit and knows how bad it is for her, she'll probably end up bumming a smoke. 

The reason for this unwise response is a lack of will: despite her intentions, the temptation is simply too great. A certain degree of thoughtlessness also takes hold of us in such situations.

For example, experiments have shown that bigger portions cause us to eat more. If we have a big portion in front of us, we eat more of it than we should — even if it doesn't taste good.

In one experiment, researchers sent their test subjects to the movies. Before they entered the theater, the subjects received a bag of stale popcorn — half got a small bag, the other half a medium-sized bag. 

Although most of the participants later stated that the stale popcorn didn't taste good, they ate a lot of it anyway — and the people with the big bags ate even more of theirs. 

**Sometimes we make faulty decisions because we succumb to temptation or act without thinking.**

### 5. Some companies exploit the human tendency to make the wrong decisions. 

If companies want to be successful, they have to sell products and earn profits. And to do that, they have to fulfill their customers' needs.

Whether that has positive or negative consequences on customers doesn't matter to some companies — their main concern is whether customers are buying.

Sometimes companies don't just satisfy the needs that customers already have — they can create new needs in their customers too.

For example, many people can't help themselves in the face of temptation and don't think twice about the consequences when making spur-of-the-moment decisions. Companies use this knowledge to tempt their customers into buying more than they originally intended. This is the core approach of companies that promote XXL-sized portions instead of normal ones, and the availability of these over-sized portions is one of the reasons why so many people eat far too much.

Trial subscriptions to periodicals are another case where companies exploit human weakness. You can sign up and receive the periodical for free for a certain amount of time, but, if you don't terminate your subscription within the cancellation period, it is automatically renewed, and you continue receiving the periodical — and paying for it. 

**Some companies exploit the human tendency to make the wrong decisions.**

### 6. Nudges are subtle changes in context that help us avoid making bad decisions. 

How can we keep ourselves from making the wrong decisions? With _nudges_, or little pushes in the right direction. 

Nudges are not "no-nos" or clever advertising messages. Rather, they are subtle actions that make it easier for us to act in the way that is best for us without dictating a specific behavior. Simply put, nudges leave us free rein while making it easier for us to choose the right thing. 

Displaying fruit in clearly visible spots and putting junk food in less prominent ones in a cafeteria is a nudge: we have the choice between buying an apple and a candy bar, but the arrangement of the products prompts us to go for the healthier apple.

Of course, nudges can also be used for less righteous purposes, e.g., by companies who want to suggest certain purchasing decisions. Companies have been using nudges for a long time — in supermarkets, for example, the shelves at eye level usually contain the most expensive goods. 

But the example of the fruit being prominently displayed in the cafeteria shows that nudges can also help people choose healthier and better alternatives.

**Nudges are subtle changes in context that help us avoid making bad decisions.**

### 7. Defaults are highly effective nudges that cause people to automatically do what’s best for them. 

Considering that we don't rack our brains every time we make a decision, decision-making situations should be designed so that automatic responses produce a positive outcome.

The e-mail service _Gmail_ does just that with its attachment reminder. If words like "please find attached" are written in e-mail text but no file has been attached, the e-mail program automatically recognizes it and asks the user, "Did you want to send an attachment?" This little nudge can save people a lot of time and trouble.

Companies could choose a similar system to assist their employees to make wise decisions about, e.g., enrolling in company pension-scheme programs.

Since most people are naturally inclined to laziness and don't really want to change their status quo, enrolling in a company pension-scheme program should not be optional. It would be much better to set up the program so that all employees are automatically enrolled in the scheme — and have to explicitly object if they do not want to be.

Companies should choose _defaults_ guaranteeing that employees do the right thing even when the employees do _nothing at all_.

**Defaults are highly effective nudges that cause people to automatically do what's best for them.**

### 8. Nudges are most useful when we have too many choices or when the future is at stake. 

The most opportune time to use nudges is in situations where it's particularly difficult for us to make the right decisions. 

Making the wrong decision is easy when we have an immediate pay-off and don't feel the negative consequences until later. We take that second piece of cake and drink that cocktail after work because they give us pleasure in the moment and we only feel the negative effects later — when we step on a scale at the end of the month, or wake up the next morning with a headache. 

Another cause of bad decision-making is when we don't have previous experiences to refer to. 

At some point or another, we have to decide on which insurance company we want to use. Since we only make this decision once or twice in our lives, we can't refer to earlier experiences to assess which insurance company would be best for us. 

And even after choosing a health insurance company, there is yet another decision to be made: which policy? 

For example, if an insurance company offers many different policies, but some customers who just want general coverage are having difficulty choosing which policy would be best for them, the company could offer a recommended "default policy" — say, one that covers more than 80% of a person's entire medical costs. This helps the less insurance-savvy customers choose a policy that covers a wide range of treatments for the most common medical problems.

**Nudges are most useful when we have too many choices or when the future is at stake.**

### 9. Many people use nudges to achieve their goals. 

At the end of each year, many of us come to the conclusion that, yet again, we didn't manage to stick to our New Year's resolutions: the number on the scale is higher than last year and the ashtray is still full. 

And yet some people are successful. How did they do it? Generally, the people who are able to stick to their resolutions do so by using nudges to keep them from making the wrong decisions throughout the year: they place bets with their friends, take advantage of public weigh-ins, or use Internet programs tailored to help them monitor their progress and stay on target.

On the website _Stickk.com_, more than 100,000 people have signed so called _commitment_ _contracts_ with themselves to achieve their goals. After signing up on the website, you state your goal, set milestones with specific deadlines, and have the option to put money on your success. If you achieve your goal, you get your money back. If not, your money goes to whatever institution or person you initially selected (one particularly motivating recipient, for example, might be the rival team of your favorite football team). 

This successful website is an example of how nudges can be harnessed to help you modify your behavior and fulfill your ambitions.

**Many people use nudges to achieve their goals.**

### 10. States and other institutions should use nudges to encourage wise decisions. 

Nudges encourage individuals to make better decisions — and this often benefits more than just the individual alone. An entire society benefits when the majority of its members make wise decisions. If fewer people smoke or are overweight, healthcare costs decrease — and everybody wins.

Even though a nudge may cost us something at first, the longer term benefits are generally not long in coming. 

In the area of environmental protection, the mere act of instating a public obligation to report carbon emissions has the power to change peoples' behavior. This obligation doesn't explicitly forbid companies to exceed a certain limit of pollutant emissions. Instead, it acts as a nudge by creating incentives to voluntarily cut down on emissions (especially because environmentalists frequently use such information to publicly denounce polluters). The reporting obligation thus creates a sort of emission-cutting competition — without using any legal coercion.

As the dollar-a-day program shows, nudges can also reduce the amount of unwanted teenage pregnancies. Many young mothers who've already had a child as a teenager often get pregnant again shortly thereafter. In order to tackle this issue, several US cities pay teenage mothers one dollar for every day they don't get pregnant. This program is actually cheaper for taxpayers than the costs of supporting the children of the young mothers. 

Companies also use nudges to keep their customers from making mistakes. If we forget to put on our seat belt in a car, the car beeps at us until we buckle up. If our gas tank is almost empty, a warning light blinks to remind us to stop and fill it up. These little things don't force us to fasten our seat belts or fill our gas tanks: they do, however, prevent us from accidentally forgetting.

**States and other institutions should use nudges to encourage wise decisions.**

### 11. Final summary 

They key message of this book is: 

**People often make unwise decisions, but the slightest of changes — so-called nudges — can give people incentives to make better decisions. That's why countries and private institutions should also use nudges.**

The questions this book answered:

**How and why do we make the wrong decisions?**

  * We don't always make decisions that are best for us in the long run.

  * We often make bad decisions based on either too little or overly complex information.

  * Human beings act mainly on gut feelings.

  * Sometimes we make faulty decisions because we succumb to temptation or act without thinking.

  * Some companies exploit the human tendency to make the wrong decisions.

**How can nudges help us deal with this problem?**

  * Nudges are subtle changes in context that help us avoid making bad decisions.

  * Defaults are highly effective nudges that cause people to automatically do what's best for them.

  * Nudges are most useful when we have too many choices or when the future is at stake. 

**How can each and every one of us use nudges?**

  * Many people use nudges to achieve their goals.

  * States and other institutions should use nudges to encourage wise decisions.
---

### Richard H. Thaler & Cass R. Sunstein

Richard H. Thaler (b. 1945) is a professor of Behavioral Science and Economics at the University of Chicago. Cass R. Sunstein (b. 1954) is a professor at Harvard Law School and serves as an advisor to president Barack Obama.

