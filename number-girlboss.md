---
id: 544fce09376133000a2b0000
slug: number-girlboss-en
published_date: 2014-10-30T00:00:00.000+00:00
author: Sophia Amoruso
title: #GIRLBOSS
subtitle: None
main_color: E4BAAE
text_color: 806861
---

# #GIRLBOSS

_None_

**Sophia Amoruso**

In _#GIRLBOSS_, author Sophia Amoruso tells her story of transformation, from high school dropout to CEO of a multimillion-dollar fashion empire. Whether starting a business from scratch, building a powerful community or choosing the right investors, Amoruso tells you what it takes to be a woman in business today.

---
### 1. What’s in it for me? Learn how to find your passion and have it drive you toward success. 

What kind of chief executive drops out of high school and then spends years dumpster-diving, shoplifting and working mediocre jobs to pay the bills?

A truly kick-ass one like author Sophia Amoruso, who used her colorful life experience to start one of the most successful fashion companies in the past decade.

In _#GIRLBOSS_, Amoruso explains how she turned a vintage clothes hobby into a multimillion-dollar business. In these blinks, you'll learn how she also used her creativity and her business savvy to launch a worldwide movement to connect young women.

In the following blinks, you'll also discover:

  * what hitchhiking teaches you about entrepreneurship;

  * how you need to forget everything you learned in school to truly succeed; and

  * why working at a big chain bookstore can make you the world's best boss.

### 2. Discover your personality and natural strengths, and let them drive you to success. 

When you were growing up, did your parents tell you that if you failed in school, you'd fail in life?

Well, it's not true.

Some people do well at school; others don't. Either way, good grades won't determine whether you'll achieve success later in life.

After all, there are many different kinds of people in the world, and each person has different skills. Being successful is matter of figuring out what you're good at and parlaying that skill into a career.

Once you identify your special talent, this will free you to focus on what you enjoy and what you're good at. This way, you can perform to the best of your ability and in tandem, succeed.

That's the story of Sophia Amoruso. She had a knack for negotiating discounts on coveted vintage clothing and then reselling items for a healthy profit. The venture started as a hobby, but the thrill and challenge eventually drove her to establish her own clothing empire.

The author was simply being herself and doing what she was good at; _that's_ what made her successful. But she was able to find this path as she had accepted her own strengths and weakness.

Accepting yourself is incredibly important — because as mentioned above, people are different. There are extroverts and introverts, and each type has an environment that works best for them.

Extroverts flourish when they're interacting with others. This personality type is well-suited for working on Wall Street, for example, where you have to push and shove and shout to make trades. But such an environment would be a nightmare for an introvert.

In Amoruso's case, her inner introvert blossomed thanks to social media. She built customer relationships online, where she could interact with fans in a measured, non-aggressive way.

> _"Be yourself; everyone else is already taken." — Oscar Wilde_

### 3. Follow an unconventional path and try odd jobs to discover what truly drives you. 

If you want to achieve greatness, it's important to realize that no matter how motivated you are, there's no straight line to success.

And in fact, trying to follow a straight path will only limit you, as you might miss out on new and surprising opportunities.

Imagine an aspiring astronaut who has spent years with his head buried in books. During his first zero-gravity flight test, he discovers he has severe motion sickness. Even though he spent his whole life training to be an astronaut, his laser-like focus prevented him from finding the right path that would truly suit his abilities.

So choose a more serpentine path — yet know that you might have to take a few odd jobs along the way. Some people resist this, but you'll see that you can learn something new with each opportunity.

Although she was an anti-capitalist, Amoruso once took a job working at a large commercial bookstore chain. She started with a bad attitude and carried a lot of prejudices, but ultimately acquired customer service skills that would later set her own business apart.

There are unexpected advantages to following an unconventional path. There is also, of course, the other side of the coin: failure.

Failure can happen to anyone — so don't fear it, own it! Chances are, if you aren't succeeding at something, then what you're doing isn't fueling your inner passion. If it were, it would be effortless.

That's exactly what Amoruso discovered. Prior to starting her own business, she'd had her fair share of failures, dozens of jobs from which she was either fired or quit. At the time, she thought the reason things weren't working out was because she was lazy.

But the truth was that these jobs simply didn't work for _her_.

Yet when she was collecting clothes and bargaining and selling her creations on eBay, Amoruso could work all hours of the day without even once checking the clock.

### 4. To succeed, become an expert at what you love to do. Collect experience and try everything. 

Once you've figured out what you love to do, how can you succeed in doing it?

Start by becoming an expert in your field, and learn everything you can. If you're in the business world, the best way to gain expertise is through experience.

For Amoruso, this meant building her own company from scratch.

In the beginning, she did everything herself: bought the clothes, packed and shipped them to customers. This ensured that she knew the whole business, inside and out.

And so when items didn't sell, she would adjust her marketing approach, through changing text or images on her website. By being so adaptable, she gained lots of experience and knowledge about what exactly her customers wanted.

During the first few years, Amoruso also styled all her own fashion shoots. In the process, she did much more than sell clothing; she offered young women an immersive, inspiring fashion experience.

She did this by choosing approachable, cool-looking models and dressed them in a quirky way that her customers loved and wanted to copy. Ultimately, customers didn't just connect with her product, they also connected with each other, creating a powerful community.

This approach gave her fledgling business a major advantage over other vintage shops on eBay, many of which had dull photos and poor marketing. So when she moved from eBay to her own site, her community followed.

And their loyalty didn't end there. Her customers loved _NastyGal_ so much they shared their find with friends. This word-of-mouth marketing helped Amoruso grow her customer base.

By working hard and building experience, the author developed expert insight into what her customers wanted. But what really helped her succeed was that she was able to put all this expertise into building an influential, loyal community.

> _"You don't get taken seriously by asking someone to take you seriously. You've got to show up and own it."_

### 5. Ask, and you shall receive; be creative and break some rules, but don’t break the law. 

There are no stupid questions. But there are questions that go unanswered because they were never asked in the first place.

So if you want to do something, you have to start by _asking for it_.

Think about it this way: If you don't tell people what you want, you're never going to get it.

It's simple! You have to vocalize your hopes and dreams if you want people to help you achieve them.

Amoruso learned this one day when she was trapped on the side of the road during a heavy rainstorm. Although she didn't have a dollar on her, she took a chance and held out her thumb to hitch a ride from a Greyhound bus. The bus stopped, and the driver let her ride for free.

This lesson hit her like a lightening bolt: Ask, and you shall recieve.

Another important aspect of getting what you want is trusting your intuition. That means knowing which rules are okay to break.

After all, to build a successful business you have to be creative and break some rules. It's not about seeing the world in black and white, but rather in shades of gray.

Being more flexible in your thinking will also prevent you from worrying so much about whether you're doing something right. Instead, break some rules, cross some lines — and make something new.

Yet not all rules are meant to be broken. Amoruso learned this when she was busted for shoplifting.

After paying a fine to avoid jail time, the author was humbled by her experience. She decided that the only rules worth breaking were arbitrary ones, like wearing last year's shoes to a fashion party.

So when it comes to breaking rules, don't waste time by breaking the law. Unless you really feel like learning a different lesson in jail.

### 6. You can’t control what others think of you, so ignore it. Stay positive and focus on your goals. 

So we've learned that there are some rules you can break, and others you can't. But there's also the _Golden Rule_ : Do unto others as you would have them do unto you.

Basically, this means if you put positivity out into the universe, you'll receive the same in return.

And what's more, by focusing on what you like rather than what you don't, you'll free up energy that you can then pour back into your passions.

Amoruso was tempted to ignore this advice when a friend copied the entire _NastyGal_ website and tried to pass it off as her own. If the author had dwelled on this incident and stayed angry, it only would have helped her competitor, who would have used the opportunity to steal customers who weren't getting the great customer service they were used to.

But the author didn't cave. She didn't waste time plotting revenge, and instead rededicated herself to her business.

As Amoruso learned, you can't worry about what others are doing or thinking. If you spend your time obsessed with others, you'll neglect the innovation and creativity inside of you.

High school was miserable for the author. She was always paralyzed with fear by what others thought of her. So she left school early. Ultimately, she realized that worrying about what others think of you only wastes time and energy.

So to stay focused and ignore the noise, create _sigils_. These are small words that are encoded with your goals. To make a sigil, jot down your goal in a word or short phrase and then slice it up, removing or combining whatever letters you want until you're left with a pictogram no one else understands.

These private codes can empower you to think positively about your dreams and stay motivated.

Now that you know to stay positive and be true to yourself, how do you apply these skills to your entrepreneurial goals? The next blinks will show you how.

### 7. Set yourself apart by connecting your entrepreneurial side with your creative passions. 

When we're children, creativity comes from paint sets, crayons and paper dress patterns. But as adults, we learn that problem-solving too is inherently creative.

And when you combine creativity with business, you can set yourself apart.

A creative entrepreneurial approach relies on questioning everything; in doing so, you create options for your business. Accepting the status quo will only limit you.

For example, _NastyGal_ does not heed the fashion-industry standard of thin, flat-chested models. The company rather established a reputation for creatively dressing different body shapes and marketing its products to women with curves.

This innovative approach to styling wasn't just a creative choice; for _NastyGal_, it was also a smart business decision that helped generate revenue.

However, creative people sometimes get so swept up in the more imaginative aspects of their work that they ignore the more practical business side. It's important to balance the two.

While the author enjoyed thinking about the font on her website and the photographs she used to showcase her products, she also devoted plenty of energy and attention to acquiring and retaining customers.

She always considered the shopping experience from her customer's point of view, while making sure the aesthetics were consistent and unique across the site. Through this, she connected her entrepreneurial side with her more creative talents.

Creative people can often be especially effective at marketing, so think of focusing your energies here. It's a way of tapping your inner artist while also growing your business.

Some people think that just because they're a stylist (or some other creative type) that they can't also be a businessperson. Thinking this way is negative, and almost guarantees that you will fail.

### 8. Find like-minded investors and employees that will support your dreams, not undermine them. 

As an entrepreneur, it's important to choose the right team of employees and investors. But how?

The best sort of team to work with is honest, hard-working and loyal.

The author learned this one year when the _NastyGal's_ warehouse chief quit during the crucial Black Friday shopping period, leaving the warehouse in the lurch.

Yet a number of motivated employees jumped in to help. They packed boxes, fulfilled orders and made sure the whole operation ran smoothly. They went above and beyond their job descriptions to make sure the company didn't miss any customer orders during a critical time.

For a CEO, a staff like this is a dream. Knowing that you can count on employees makes it easier to sleep at night.

And meanwhile, as you and your team are hard at work, make sure your investors are supporting your efforts, and not undermining them.

Investors are there to help you grow your dreams, not impose their own. As Amoruso personally experienced, some investors can be bullies. One potential investor, for example, asked the company's male senior executives whether the author, as a woman, had trouble managing money.

His small-mindedness was totally out of sync at a company like _NastyGal_. Especially one built from the ground up by a woman who'd never even taken out a business loan.

Eventually, the author found investors who understood that she was starting a movement, not just a fluffy site for women to waste money.

In the end, it's important that you hold on to your principles and refuse to compromise, just for financial gain. Certain decisions might seem appealing in the short-term, but they'll only cause problems in the long run.

### 9. In business and in life, there’s nothing cooler than confidence. Wear it with pride. 

You know how some people have a glow? It's all about confidence; when you feel good in your own skin, you look good too.

This holds true both on a personal level and on a business level. If you know what you are as a company, you'll be able to feel comfortable and confident following your own path.

Otherwise, you'll just get distracted by every new trend that springs up in your industry.

With N _astyGal_, the author learned that it's important to represent a business persona as a kind of girl who can talk to anyone, even a stranger. Maybe it would be more "cool" to seem aloof, as some competing brands do, but that kind of unfriendliness hints more instead to a lack of confidence.

And as we all know, you can't buy confidence. Sure, great clothes can help, but only if you have the smarts to back them up.

This means listening when others are speaking and having the courage to form your own opinions.

It also means knowing when to delegate a decision to someone with more expertise or experience. Never insist that you know everything, as it will only alienate. Instead, remain humble and confident.

If you follow these suggestions, you'll be naturally attractive to everyone you meet. And if you pair that with a fantastic dress? You'll blow them away.

> _"You don't have to choose between smart and sexy. You have both. You are both."_

### 10. Final summary 

The key message in this book:

**Becoming a successful entrepreneur isn't about doing well in school, it's about figuring out what you really love and letting your passion drive you. So if you want to succeed, ask for what you want and find a way to connect your entrepreneurial side with your creative talents. Most importantly, be confident!**

Actionable advice:

**Create a** ** _sigil,_** **or a small word encoded with your goals**.

Lost sight of your goals and need a boost to get back on track? Write down one of your dreams in a word or short phrase and then literally slice it up, removing or combining whatever letters you want until you're left with a pictogram no one else can understand. This small, private code will empower you to think positively about your dreams and stay motivated.

**Suggested** **further** **reading:** ** _Nice_** **_Girls_** **_Don't_** **_Get_** **_the_** **_Corner_** **_Office_** **by Lois** **P.** **Frankel**

_Nice Girls Don't Get the Corner Office_ focuses on the reasons why women often don't make it to the top ranks in the world of business. Frankel explains how women unconsciously behave in ways that undermine their business aspirations and presents female readers with measures to consciously counteract their self-defeating behavior.
---

### Sophia Amoruso

Sophia Amoruso is the founder of _NastyGal_, a successful e-commerce site for women's clothing, shoes and accessories.

