---
id: 561c16913635640007480000
slug: numbers-rule-your-world-en
published_date: 2015-10-13T00:00:00.000+00:00
author: Kaiser Fung
title: Numbers Rule Your World
subtitle: The Hidden Influence of Probability and Statistics on Everything You Do
main_color: 80C03E
text_color: 4D7325
---

# Numbers Rule Your World

_The Hidden Influence of Probability and Statistics on Everything You Do_

**Kaiser Fung**

_Numbers Rule Your World_ (2010) is a guide to statistical reasoning and how you can use concrete statistical information productively to understand as well as improve your world. These blinks walk the reader through the five key principles of statistics and how they can be applied to improve decision making in various contexts.

---
### 1. What’s in it for me? Discover how statistics rule your world. 

There's an invisible force that affects us every day, and it is neither god-like, nor chemical: it is the hidden world of _statistics_, and whether they apply to your job, your vacation or your health, statistics rule your world.

But if these endless sets of numbers play such a crucial role in our lives, why don't we give them more consideration? These blinks aim to remedy the lack of attention paid to statistics, providing rich examples of how we can learn from statisticians to better our world. You'll learn about the five principles that are central to statistical thinking, and how they can be used to make sound decisions. 

In these blinks, you'll also learn

  * how long lines at Disney World can be avoided;

  * how statistics can fight the spread of E. coli; 

  * what dying in a plane crash has to do with winning the lottery.

### 2. According to statisticians, variations from the average are more relevant than the average itself. 

Have you ever been at an amusement park, waiting in line for a ride in the hot sun thinking, "if they just had one or two more roller coasters, _all_ the lines would be shorter"? While it may seem that adding attractions would lead to quicker queues, it's actually not the case.

Why?

According to statisticians, and using the example of Disney World, it's the varying pattern of _when_ guests arrive at the amusement park, and not the average number of guests arriving, that makes the lines so mind-numbingly long. Since lines form when demand exceeds capacity, it seems logical that if Disney World accurately predicted demand, they could build capacity to accommodate it. Unfortunately, it's not so simple. 

Statisticians are positive that even if Disney could accurately predict the number of park visitors hopping on the Dumbo ride on a peak day, a line would still form because guests come at irregular intervals throughout the day, and because the ride's capacity remains fixed. So, planning for capacity can deal with average rises in demand, but not with _fluctuating_ demand. 

Disney has come to deal with this issue using a feature called FastPass, which entitles guests to come back to a ride at a designated time and jump into an express lane. FastPass works because it reduces the variability of guests arriving at any given time. So, while it doesn't change the ride goers' actual waiting times, it does give them the freedom to enjoy other activities in the meantime, thereby boosting customer satisfaction. 

But theme parks aren't the only place this logic plays out: statisticians can point to the same trend in traffic jams. Just like at Disney World, highway congestion is the result of suddenly high volumes of cars, which exceed the road's average capacity. 

To deal with this issue, the Minnesota Department of Transportation uses a technique called "ramp metering", in which traffic lights on ramps regulate the pace at which cars enter the highway, thereby stabilizing the number of cars on the road.

> _"One can, in fact, define statistics as the study of the nature of variability."_

### 3. Statistics work by showing causation and correlation. 

To understand the basics of how statistical reasoning can be applied to real-life situations, it's helpful to consider two examples: the first offers a peek at epidemiologists who try to explain the causes of diseases; and the second shows how credit modelers seek correlations that indicate a person's financial responsibility — essential information for banks and insurers. 

In the first example, epidemiologists use statistics to identify causal relationships that can pinpoint the outbreak of a disease and its origin. For instance, in September of 2007, several patients in the United States tested positive for E. coli, a bacteria that can lead to severe discomfort, kidney failure and even death. These few cases were enough to uncover the disease's source and protect the health of others. 

How?

Five of the affected patients from Oregon were questioned intensively about what they eat. The food that four of them had in common was bagged spinach. From there, existing statistics were consulted, which showed that only one in five Oregonians would eat spinach in any given week. The data allowed the epidemiologists to pin the outbreak on spinach, since the ratio of consumption evidenced in the patients was much higher than that of a typical Oregonian. 

But statistics can be used for more than revealing contaminated vegetables. By identifying patterns, credit modelers can determine a person's creditworthiness. 

For example, in America today, many people take out mortgages quickly without undergoing invasive interviews. Creditors don't need to ask people loads of questions before lending them money because a computer essentially already has, tracking a person's innumerable past loan decisions and identifying patterns, or _correlations._

A mortgage lender may have discovered a correlation among borrowers in a particular line of work, and an inability to repay loans. Using this information, they can then instantly determine the creditworthiness of an application.

### 4. Statistics take group differences into account to ensure equality. 

As the idea of correlation demonstrated, differences between groups of people are essential to statistical reasoning. Whether they show disparities between an older generation and a younger one, or people who hold a college degree and those who don't, group differences are fundamental to statistics. 

For one, statistics take group differences into consideration to design fair exams. Statisticians can make sure standardized tests like the SAT are fair by omitting questions that give preference to one social demographic over another.

For example, a question could be unfair if its phrasing is clearer for white test-takers than for the African-American community. To determine which questions are fair, statisticians compare the performance of black and white students on any given question. However, a difference in performance doesn't necessarily imply an unfair question — statisticians must also take any group differences into account. 

In this example, group differences would be present if one group of students constituted a majority of low-performing students and the other constituted a majority of high performers. To take this difference into consideration, statisticians don't compare all African-American students to all white students, instead comparing both high-performing and low-performing students of each demographic. 

Group differences are also key to fair insurance, a system that works by collecting premiums from a majority of policyholders to subsidize the damages incurred by a few. This model seems fair, since anybody could be on the giving or receiving end of an accident. However, our insurance system is unfair when group differences between insured people aren't considered. 

For instance, if property insurance treated houses along a coastline the same way as inland houses, all homeowners would pay the same premium based on their average exposure to risk. However, statistics show that houses located inland are much less likely to be affected by natural disasters like hurricanes than their counterparts with an ocean view. Armed with this information, insurers can adjust the premiums according to the relative risk.

> _"Like disease detectives, lenders must master the educated guess."_

### 5. Decisions based on statistics face a trade-off between two types of errors. 

It may seem that, in modern times, it's difficult to keep the truth from surfacing. After all, people have developed all manner of methods to uncover facts, like drug testing to expose steroid use among athletes, or polygraphs to unearth what people won't admit. However, these modes of testing are not entirely error proof.

In fact, if testing for drug use, two types of errors can cause problems: the _false positive_, when an athlete who did not take drugs is falsely accused of doing so, and the _false negative,_ when an athlete who did cheat is exonerated. 

As a result, the people administering drug tests face a trade-off: if they try to minimize false positives, an error that serves to diminish their authority, their actions will bear the unintended side effect of letting more cheaters walk free.

Assume that ten percent of all athletes use performance-enhancing drugs. As testers are trying to avoid false positives, they use testing methods that only return positive results when the evidence definitively points to cheating. 

As a result, statistics show that only about one percent of athletes test positive. However, since we know that about ten percent of all athletes use drugs to cheat, we also know that nine percent of them test negative despite using drugs. Therefore, it is reasonable to conclude that nine out of ten athletes who use drugs get away with it. 

Lie detector tests are prone to the same unavoidable trade-off. Polygraphs work by monitoring the test subject's medical statistics — like their breathing or blood pressure — while they are asked a series of questions. The collected data can point to false information by showing a correlation with changes in the person's physiological state. 

Naturally, the detectives administering the tests want to avoid false negatives, that is, criminals who go free. However, the problem is the same as with the athletes: avoiding false negatives means accruing false positives and accusing many innocent people.

### 6. Statistical thinking teaches us to question patterns. 

While most people wouldn't associate the field of statistics with fighting crime or erasing irrational fears, statistical logic can do exactly these things. 

Statistics teach us to question patterns that appear obvious. For instance, on October 31st, 1999, an EgyptAir jetliner plummeted into the Atlantic Ocean off the coast of Nantucket Island, Massachusetts, leaving no survivors. But between 1996 and 1999, before this tragedy occurred, three other jets had crashed into the ocean right around the same location. 

As a result of the EgyptAir crash, some people stopped flying in the area. Their logic was that four crashes in four years was too many to be a coincidence, and that it must indicate some kind of disaster-prone pattern in the region.

However, statisticians looked at the bigger picture, and while they saw the four crashes in four years, they also saw millions of planes safely traversing the same airspace during the same period. As a result, they questioned what appeared to be an obvious pattern. In fact, statisticians approximate that the odds of dying in a plane crash are one in ten million — about the same chances of winning the lottery. 

On the topic of the lottery, it provides a good example of how statistical thinking also teaches us to question patterns that appear unusual. For instance, between 1999 and 2005, the Ontario Provincial Lottery issued tickets resulting in 5,713 "major" winners, meaning people who raked in prizes of 50,000 Canadian dollars or more. But what's more shocking is that 200 of the winning tickets were cashed in by the very store owners who sold them. 

Assuming fair conditions, store owners shouldn't be any more likely to win than anybody else. A statistical analysis of the lottery winners exposed that store owners should have accounted for only 57 wins instead of 200. 

The statisticians' findings led them to suspect fraud, a hypothesis that was proven correct when it was revealed that most of the winning store owners had cheated by claiming they held the winning tickets of others who had just earned a free play.

> _"Unlike some of us, they [statisticians] don't believe in miracles."_

### 7. Final summary 

The key message in this book:

**Statistics are based on five key principles. Statisticians search for variations from the average, uncover causation and reveal correlation, account for group differences, are realistic about the unavoidable trade-offs they face and question patterns whether they appear obvious or odd.**

Actionable advice:

**Use statistical thinking to overcome your fear of flying.**

If you're one of the many people who fear flying, a bit of statistical logic might help. A key principle of statistics is to question patterns, so if your fear is a result of a recent spell of plane crashes, you might be able to overcome it by considering what the pattern really indicates. Remember that for every plane that crashes, thousands of others land safely at their respective destinations; the odds of dying in a plane crash are about 1:10,000,000!

**Suggested further reading:** ** _Freakonomics_** **by Steven D. Levitt and Stephen J. Dubner**

_Freakonomics_ applies rational economic analysis to everyday situations, from online dating to buying a house. The book reveals why the way we make decisions is often irrational, why conventional wisdom is frequently wrong, and how and why we are incentivized to do what we do.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Kaiser Fung

Kaiser Fung is a statistician and author of the book _Numbersense: How to Use Big Data to Your Advantage._ He is also the creator of the two popular blogs _Junk Charts_ and _Big Data, Plainly Spoken._ He received his MBA from Harvard Business School and holds degrees in engineering and statistics from Princeton University and the University of Cambridge, respectively.

