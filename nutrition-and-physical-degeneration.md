---
id: 536a13e06631340007120100
slug: nutrition-and-physical-degeneration-en
published_date: 2014-05-06T11:24:01.000+00:00
author: Weston A. Price
title: Nutrition and Physical Degeneration
subtitle: A Comparison of Primitive and Modern Diets and their Effects
main_color: 1A7980
text_color: 1A7980
---

# Nutrition and Physical Degeneration

_A Comparison of Primitive and Modern Diets and their Effects_

**Weston A. Price**

Based on the author's meetings with many of the world's indigenous people, _Nutrition and Physical Degradation_ presents a comparison of the health of those who consumed only local whole foods and those who had begun to include processed foods in their diet. The author found that the latter suffered from problems with their teeth, bodies and brains, while the former remained strong and vigorous. Having investigated the differences between processed and local whole foods, the book argues that diets made up of processed foods lack the requisite vitamins and minerals for maintaining a healthy body.

---
### 1. What’s in it for me? Find out how to get the most out of your food. 

More than ever before, the diet of the Western world is by and large made up of processed, inorganic foods. Even in those cases where we try to make healthy choices during trips to the supermarket — choosing whole-wheat bread over white bread, for instance — we end up with food that's either completely stripped of its nutrients, or at best contains few of the nutrients required for maintaining a healthy body.

Such is the argument presented in Weston A. Price's _Nutrition and Physical Degradation_, a seminal work on human nutrition first published in 1938. Price's arguments were based on his travels to isolated indigenous communities, from the icy tundra to the bush of Central Africa. During his visits he learned from the locals, documenting in great detail their diets, their teeth, their facial structure and their rituals. 

For those who are religious about keeping a balanced diet, who are planning to have kids, or who are forever stuck in the city, this book will be a startling revelation. It's a formidable work that will transform your ideas about what you eat and drink, and prompt you to reevaluate how you care for your teeth and your body. 

In these blinks, you'll find out how some people can be in great health without eating vegetables. 

You'll find out why it is that certain people can go without brushing their teeth, and yet their teeth remain in perfect condition throughout their life.

Finally, you'll learn why the fluctuating price of coconut meat, used to make coconut oil, affected the incidence of tooth decay in the South Pacific.

### 2. The natural, local diets of indigenous people equip them with healthy bodies. 

Who are the healthiest people in the world?

Is it those who are forever committing themselves to the latest diets? Or is it those slaving away on the treadmill or the crosstrainer?

The answer is neither. Instead, the secret to good health can be found in the nutritious diets of the world's indigenous people.

Their diets are based on locally sourced foods high in vitamins and minerals. Take, for example, the Eskimo population of the Arctic. They eat caribou and some types of whale meat, and they also eat smaller quantities of seaweed and berries that they collect and freeze for consumption during winter.

Such diets have massive health benefits. For instance, they provide indigenous people with healthy teeth well into old age, without the help of toothbrushes or dentists. While, for example, Eskimos wear their teeth down to the gumline, the gum tissue itself doesn't recede. The flesh around the teeth is also healthy and the population is largely free from gum-tissue diseases like gingivitis.

Furthermore, Eskimos don't have fillings, even though they don't clean their teeth. This is because the minerals in their food generates saliva that hardens the teeth and protects against the bacteria responsible for tooth decay.

Another benefit of indigenous diets is that the bodies and organs of those who practice them are strong and resilient to disease. One doctor, who had over 30 years of contact with Eskimos and North American Natives, didn't report a single case of malignant disease in those who had stuck to their traditional diets. Also, any problems with their kidneys, stomachs, appendices or gallbladders were very rare.

The highly nutritious diet of indigenous societies is the result of centuries of development. Ancient skeletons found in the Andes, on the South African coast and in the Rhone Valley in France show that nearly all have excellent teeth and very few have tooth decay.

### 3. Processed foods are popular and convenient, but they cause tooth decay and illness. 

In the Western world many of us are living in an era of abundance. With easy access to a wide range of cheap and tasty processed foods, like chocolate and white bread, it's no wonder that a diet of convenience has become the norm.

But is it healthy?

While processed foods may be simple to prepare and appetizing, they're actually far less healthy than traditional foods.

For example, following the First World War, the price of coconut meat increased dramatically. In coconut-rich Tonga, trading ships visited regularly to purchase the meat, and in exchange gave the natives white flour and sugar.

Those residents who made room in their diets for these new imported foodstuffs saw a massive decline in the quality of their teeth. A remarkable 33.4 percent of these people suffered from tooth decay. In contrast, of those who continued to survive on native foods only, just 0.6 percent suffered from tooth decay. Moreover, when the price of coconut meat decreased, and the traders lost interest, incidences of tooth decay declined.

Why is this?

Unlike the native foods, the processed foods don't contain as much of the fat-soluble vitamins, such as A and D, that are required to prevent tooth decay.

But tooth decay is not the only health risk posed by processed foods. For instance, the author recounts the story of one boy raised on processed foods like white flour products who had rheumatic fever, arthritis and heart problems (as well as the expected tooth decay).

Yet by overhauling his diet to include fresh whole wheat, whole milk and high vitamin butter, his health rapidly improved and much of his pain abated.

The message is clear: we cannot absorb as many of the minerals from inorganic, processed foods as we can from foods in their natural form.

### 4. Processed foods are rich in calories but lacking in vitamins and minerals. 

Because many of us lead a sedentary lifestyle, we require fewer calories than those who lead active lives. However, we also need to ensure we get enough vitamins and minerals.

Yet while much of the processed food that we eat provides us with the energy necessary for daily living — that is, enough calories — it has lost much of its nutritional value during the production process.

White flour, for example, retains just 20 percent of the calcium and phosphorus present in the original grain. Furthermore, the milling process removes most of the grain's vitamins — like vitamin E, which is crucial to the body's growth, to the efficient functioning of the organs and to reproduction.

Even the "whole wheat" bread sold in bakeries and supermarkets has lost many of its nutrients during production. In fact, the original nutrients are provided only if the whole wheat is consumed soon after grinding.

Moreover, even if there are minerals in the food we consume, the body cannot use much of it. For instance, according to the author, the recommended daily mineral intake is 0.68 grams of calcium and 1.32 grams of phosphorus.

Yet we should eat more than this, as most of us absorb, at best, only half of the calcium and phosphorus that we consume. Therefore we would usually need to double our intake of these minerals to fortify ourselves adequately, and in stressful periods — like growth, sickness or pregnancy — we should quadruple the amount.

Our mineral absorption rate depends on the presence of certain vitamins which our bodies use to absorb the minerals in food. Unfortunately, these vitamins are scarce in processed food, like packaged cereals, so we have to eat a massive amount of these foods to ingest the required amount of minerals.

To consume sufficient minerals, we need to eat natural produce — seafood, for example, which is extremely high in minerals and vitamins.

### 5. Local produce, like animal products or whole grains, is a powerful way of nourishing your body. 

Take a look at your last supermarket receipt. How many of the items you bought are locally sourced? With a little research, you'll notice that many of the items were imported from distant places, and packaged to have the longest shelf life possible to increase its chances of being sold.

This is far from the perfect way to eat, especially when compared with the diets of indigenous people that are based on the food available in their local environment.

In North America, for instance, Eskimos rely on local Caribou, ground nuts stored by mice and vitamin C-rich whale organs. Or take Native Americans, whose environment doesn't support dairy animals, or seeds and fruits: their diet mainly comprises animal meat.

Such diets provide high levels of minerals in comparison to those based on processed foods.

For example, the North American indigenous, who survive temperatures of -70 degrees, receive from their diet 5.8 times more calcium, 5.8 greater phosphorus, 2.7 greater iron, 4.3 greater magnesium, 1.5 times more copper, 8.8 times more iodine and ten times greater fat-soluble vitamins in their local diet than those indigenous people who adopted processed-food diets.

Compare these amounts to the vitamin content of processed foods. Refined white sugar, for example, is very low in nutrients yet is used as a preservative in many foods. As a result, we'd need to consume more than 20 one-pound jars of jam or marmalade to fulfill the required daily intake of phosphorus. Yet this would amount to over 20,000 calories — far exceeding our recommended daily allowance.

While it may not always be possible to eat the same nutrient-rich foods that these indigenous groups consume, it is important to understand which foods provide the most nutrition.

### 6. We can’t rely exclusively on vegetables to give us the strength we need. 

You probably know at least a few vegans, who refuse to eat or make use of any animal products. You may even be a vegan yourself. It's true that there are many benefits to a vegan diet, but is it — as many believe — the best way to live?

Certainly some plants and vegetables are rich in nutrients and can fulfill essential dietary requirements.

Maoris, Eskimos and Peruvian indigenous people, for instance, eat kelp because it has generous amounts of iodine and copper. These minerals help with the uptake of iron, which in turn increases the blood's efficiency in carrying oxygen. This is crucial for survival in the mountains, where there's less oxygen.

However, it's very difficult to survive solely on vegetables and to consume all of the required vitamins and minerals — especially during periods of growth or other physical stress.

In fact, the author did not find any completely vegetarian indigenous group that also maintained strong bodies. This is because they do not get enough vitamin D, a nutrient essential to healthy teeth and bones. Vitamin D is not synthesized by plants, but rather is provided only in animal products, such as meat and milk. Clearly these foods are a crucial part of a healthy diet.

But is there any way to get these nutrients without eating animal products?

Unfortunately not.

While some people choose to supplement their diets with synthetic vitamins, like Viosterol, these don't provide all the nutrients in the vitamin-D group. Also, while sunshine might provide us with vitamin D, it's not enough to sustain our health.

Sufficient vitamin D can be absorbed only by consuming animal products like cream, butter, egg yolks, liver or fish.

So while the maxim "eat your greens!" certainly contains a lot of truth, our diets must also include natural animal products.

### 7. We should give more back to our food sources, replenishing soil and providing animals with healthy living conditions. 

As the global population swells, necessary advances in food production technologies and processes increasingly disconnect us from the sources of our food. This leads us to simply not care about the environments from which our food is derived.

This lack of care is risky, because it can result in a situation where we borrow nutrients from the soil but don't replenish them. Agriculture, for instance, results in soil depletion, diminishing its mineral content and productivity for an extended time. In fact, much of the mineral content of farmland, like the calcium and phosphorus in wheat or corn crops, is transported to cities where it's deposited via sewers to the ocean, rather than returned to the soil.

To rectify this situation, we should follow the example of indigenous tribes who try to maintain the fertility of the soil. For example, while some African tribes crop areas of the forest to grow plants, they are careful not to overharvest it, ensuring that the surrounding trees protect the cropped land from wind, and taking care to avoid the formation of gullies which might wash away the fertilizer or nutrients.

Our lack of concern about our food also leads us to mistreat the animals whose produce we consume. Consider, for example, that if enough time isn't given to drying hay sufficiently, it will lose its chlorophyll, diminishing the hay's vitamin content. In some cases, this has resulted in calves being born blind or even stillborn, and some cows developing loin disease.

Therefore cattle should be fed on young grass, as this avoids the risks of hay and provides us with health benefits. For instance, the butter produced from cattle fed on this grass (the best is wheat and rye grass) is rich in vitamins. And though concentrated cattle food — like linseed cake — does provide minerals, the cows fed on young grass tend to be in much better health.

### 8. A child’s health depends on the vitamins and minerals its parents consume, both before and after birth. 

There's certainly no shortage of advice given to newly pregnant mothers on the do's and don'ts of their diet for the next nine months. But in fact, the child's health also derives from the fertility of both parents before conception — a fact that is reflected in the diets of indigenous people.

Eskimos, for example, feed salmon sperm to men and fish eggs to women to increase their fertility — a practice shared by the indigenous people on the Peruvian coast. Such diets are rich in vitamins that are important to the development of traits like healthy eyes and strong bones in their offspring.

Also, childbirth is a much simpler process among these indigenous tribes: their natural labor is shorter and much less painful than for many women on modern diets.

Yet, of course, Western people have a distinctly different diet, depending on synthetic nutrients that don't provide the security of natural whole foods. For instance, people worried about the lack of nutrients in their diets take supplements like Viosterol, but this was shown to prolong labor and give babies the appearance of postmaturity.

The message is clear: we in the Western world must seek to improve our diets in order to increase the nutrients required during periods of physical growth, such as childhood. And we would do well to learn from the dietary practices of the world's indigenous people.

For example, for pregnant women or nursing mothers, some African tribes use red millet in their diets, as it's rich in carotene and calcium. They, along with the Peruvian indigenous, also use a cereal called "linga-linga," commonly known as quinoa. Quinoa contains an abundance of minerals and also stimulates the production of breast milk.

Besides giving one the appearance of good health, these kinds of foods are rich sources of the extra minerals required for the critical periods of reproduction and lactation.

### 9. Final Summary 

The key message in this book:

**Because imported, processed food is often stripped of nutrients, we should avoid eating it as much as possible. Instead, we should eat food that is whole, natural, unprocessed and sourced from the local environment. This will help you get the most vitamins and minerals with which to maintain a healthy body and strong teeth.**

Actionable advice:

**Make healthier choices at the supermarket.**

The next time you're in the supermarket, buy whole milk instead of semi-skimmed, whole wheat instead of white bread, fresh fruit and vegetables instead of tins. In fact, buy everything as fresh as possible. You should also make sure you eat the yolks of eggs, cod liver oil and butter.

**Be brave!**

Don't shy away from eating animal organs, particularly the liver, as they store the vital fat-soluble vitamins. You should also eat the marrow from bones, and the eggs of fish and birds. And, if you can bear it, try eating insects and ants for their free nutrition.
---

### Weston A. Price

Weston A. Price (1870–1948) was a dentist who spent 50 years researching the relationship between diet, nutrition and physical health. His research on indigenous diets opposed the orthodoxy, and his work reverberates among the whole-food movement even today.

