---
id: 5640556938663300071b0000
slug: obedience-to-authority-en
published_date: 2015-11-10T00:00:00.000+00:00
author: Stanley Milgram
title: Obedience to Authority
subtitle: The Experiment That Challenged Human Nature
main_color: EFD6AF
text_color: 706452
---

# Obedience to Authority

_The Experiment That Challenged Human Nature_

**Stanley Milgram**

_Obedience to Authority_ (1974) explores some of the darker elements of the human mind. It addresses tough issues, like why we follow orders and how far we'll go when commanded to do something. With the author's own seminal experiments as a backdrop, you'll learn how even the most upright people can turn into cruel monsters under certain conditions.

---
### 1. What’s in it for me? Find out how anyone can become an obedient inflictor of pain. 

When we hear words like "obey," "order" and "command," it's easy to think of a totalitarian dictatorship or a religious sect. But obedience is also an integral part of society's hierarchical structure. We obey laws, or managers at work, or parents and so on. In fact, without obedience society might not function very well at all. So what's the problem with obeying?

Well, there is a darker side to obedience. Throughout history, it has enabled genocides and war crimes by making ordinary people into murderers and torturers. So what are the mechanisms that make us do these things?

In these blinks, we'll accompany Stanley Milgram on his path to understanding how susceptible we all are to becoming obedient to authority. We will explore in depth what is now the classic Milgram Experiment and find out how the better angels of our nature are easily dissuaded.

In these blinks, you'll discover

  * why being physically close to someone makes us more resistant to authority;

  * what the agentic state is and how it's integral for obedience; and

  * how a lab coat can make all the difference when it comes to following orders.

### 2. Obeying authority has led to some of the most heinous crimes in human history. 

Think back to your younger years. When teachers told you to be quiet, or your parents told you to take out the trash, did you do it? If so, you obeyed. Of course, there's nothing wrong with that. Obedience to authority, however, is not always harmless.

Indeed, obedience is at the heart of many atrocities, including every massacre and genocide.

The genocide of millions of Jews during the Holocaust, for example, was carried out by people who were following orders and obeying the regime. 

While some brave souls tried to fight against these mass murders, most Germans didn't resist Hitler's government, which massacred so many people in concentration camps that the corpses formed piles as high as small hills.

Then there was the Vietnam War, during which the United States incinerated innocent civilians with napalm bombs while soldiers raped and murdered women and children. When these soldiers returned, however, they claimed to be innocent. 

But how could they claim that after committing such terrible deeds? They were "just following orders."

The uncritical obedience to a higher authority is evident in most disasters in human history. Whether it's the Nazi soldiers or an average person next door, if a person perceives someone else as an authority, they will follow their orders and commit almost any atrocity, while still denying responsibility for their deeds.

And yet, most people would say that they'd never harm another human being. After examining some historical atrocities, most of us would claim that we'd _never_ act on the command to kill or harm someone. 

We erroneously place trust in our "superior" morals and ethical standards, along with the trust in our own humanity, to save us from turning into the cruel beasts that will do whatever they're told.

Without some sort of scientific reference point, it's hard to say how cruel we might become under the pressures of authority. This is where the author and his experiments on obedience enter the stage.

### 3. Stanley Milgram sought reasons for human obedience in a series of experiments. 

Shocked by the genocide and horrors of the Nazi regime, the author, in an attempt to discover how people could do such things, conducted a series of experiments.

His initial experiment was simple, yet effective. While it might seem difficult to construct an experiment that successfully simulates the pressures under which people obediently hurt other human beings, the author nonetheless found a way.

Participants were told that the experiment was about improving learning abilities via punishment. The setup required three people: 

  * _the_ _experimenter_, an approachable man dressed in a lab coat; 

  * _the learner_, an actor posing as a volunteer; and 

  * _the naive subject_, who had actually volunteered for the experiment. 

Both the experimenter and the learner were in on the experiment, while the naive subject was tested on his or her readiness to obey. 

To get a broad sample, thousands of people participated in the various stages of the experiment. A rigged draw of roles, however, guaranteed that the naive subject always drew the role of _teacher_.

During the experiment, the learner (the actor) and the teacher (the participant) were separated by a partition, and the teacher was instructed (by the experimenter) to help the learner learn word pairs by administering an electric shock, by pressing a button, each time the learner got a pair wrong. 

The shocks started at a tolerable 15 volts and went up to a mortally dangerous 450 volts, with the pain level clearly displayed on the shock generator.

The teacher was told that the shocks would indeed be very painful, but wouldn't leave permanent tissue damage. If the teacher voiced concerns, the experimenter would simply tell her to continue with the shocks. Even as the learner started screaming in agony and begging to be released, the experimenter advised the teacher to continue.

What the subjects didn't know was that no shocks were actually administered. Thanks to the learners' acting, however, it all seemed disturbingly real.

### 4. The results of the experiment were shocking: people obeyed. 

One would expect that, distressed by the learner's agonized screams, the teachers would have refused to continue shocking the learner, despite the experimenter's commands. What actually happened, however, is a much more troubling story.

The incidence of obedience was much higher than that of disobedience. The author and other psychiatrists expected obedience only up to a certain shock level, and were astonished to discover just how many teachers obeyed until the learner went silent, as if dead. 

In the initial study, an average of 26 out of 40 subjects (more than 50 percent) complied with the command to administer the highest possible shock, 450 volts. The rest complied even at very high shock levels before finally disobeying. Why did everyone obey so readily?

The subjects saw the experimenter as an authority in the lab. The experiment was held in the prestigious labs of Yale University, and the experimenter looked like someone who belonged there and held a position of power.

His white lab coat, as well as his general appearance and demeanor, were all evidence of the experimenter's authority. And once the naive subjects recognized him as an authority figure, it became difficult to disobey. Why is that?

We are groomed to follow the commands of authority figures. From a very young age, we're taught that following authority is good, and that rebelling against it will have dire consequences.

We obey our parents, our teachers and the police. We even obey waiters when they tell us to take a seat at a particular table. The list goes on.

And when we disobey authority, we're often overcome by bad feelings. We want to live conflict-free lives, and disobedience leads to conflict. If you disobey an authority figure, like that police officer, the consequences could be damaging. You might have to pay a fine or spend a night in jail; you might even be killed. It's much safer to lead a conflict-free lifestyle.

### 5. People who obey orders don’t feel responsible for their actions. 

Observing the subjects in his experiment, the author noticed that they entered a state of mind which he termed the _agentic stat_ e; in this state, subjects shifted responsibility for their actions onto some external entity — in this case, the experimenter.

A certain pattern of behavior accompanied this shift. 

During the experiment, the subjects repeatedly asked the experimenter if he would assume all responsibility for any possible harm done to the learner, and constantly sought assurance that carrying on with the shocking was what the experimenter — that is, the authority — really wanted.

Upon receiving this confirmation, the subjects felt that their actions were no longer really theirs. They were just obeying orders, allowing the agent of authority to act through them. And this is the agentic state.

The agentic state absolved the subjects of feelings of guilt and responsibility, placing the onus entirely on the experimenter, whose commands they were merely following. What does this tell us?

The agentic state is dangerous, and should be avoided. When individuals shirk all responsibility for moral and ethical behavior, and instead simply obey a perceived authority, our society is in trouble.

Looking back to the Nazi regime, much of their campaign, including the genocide of multiple marginalized groups, could not have come to fruition without the agentic state.

For instance, the Nazi soldiers who guarded the watchtowers of the Auschwitz concentration camp didn't directly kill any prisoners, but their participation enabled the genocide that took place, and they are thus just as responsible for the hundreds of thousands of deaths as those who released the gas into the chambers. 

But the agentic state does not allow for that kind of understanding. As far as those soldiers are concerned, they were only doing their duty. It was their commanders and the Nazi authorities that ultimately made the decisions to carry out the massacres.

But what happens when the person giving the commands _isn't_ an authority? Do the same rules still hold?

### 6. When commanded by non-authority figure, many of Milgram’s subjects were appalled by the cruelty of the experiments. 

Intrigued by his findings, the author wanted to see whether people would blindly follow _any_ command, or if obedience was directly related to perceived authority status. So, he modified his experiment to find out.

In the new variation, commands no longer came from the experimenter, but from a person without credible authority.

The experimenter would leave the room, first telling the naive subject that they were to continue with the shock treatment. 

After the experimenter stepped out, another actor, posing as a second subject (that is, a second "teacher") told the naive subject to increase the shock voltage. In other words, the subjects were still told to shock the learner each time an incorrect answer was given, but the command to increase the voltage now came from someone perceived as a peer, not an authoritative superior.

So, what happened?

The subjects were appalled by the cruelty of this ordinary person. While the subjects were willing to obey the command of the experimenter, they rebelled against the actor posing as a fellow subject. 

The actor subject was called cruel and sadistic, a horrible, immoral human being, for his desire to up the voltage. Some subjects even physically pushed the man away from the shock controls, complained to the experimenter and never wanted to see the other man again. 

So how do we account for the difference in behavior?

Unlike the ordinary man, who seemed to act out of some sort of unnatural cruelty, the experimenter acted for a higher reason: advancing scientific knowledge. This gave him the status of an authority — why else would he be in the Yale labs facilitating this kind of research?

### 7. Some subjects tried to lessen the shocks and help the learner, yet didn’t dare to openly disobey the authority. 

In the experiment, the question of whether aggression was at the root of obedience was quite natural. Did subjects obey because they harbored some innate sadistic tendencies that they could now act on with impunity? The answer was a clear no. In fact, the opposite appeared to be the case.

If the subjects saw an opportunity, they tried to disobey the commands of the experimenter. Indeed, if the experimenter seemed not to be watching, many subjects avoided increasing the voltage.

For example, whenever the experimenter left the room and gave commands only over the phone, many subjects had an easier time not following through on them. Instead, they would tell the experimenter that they had obeyed, while actually keeping the shock levels low, or would try to help the learner by emphasizing the right answers in a way that couldn't be mistaken.

Yet, despite these small acts of rebellion, virtually no one openly disobeyed or left the room and stopped the experiment. Why?

The subjects perceived disobedience as virtually impossible. They felt that leaving wasn't possible without causing a commotion, losing their payment and having a fight with the authority figure. 

The subjects found themselves in the impossible situation of choosing between disobedience and the guilt they would feel if they ignored the apparent suffering of the learner and followed the commands till the end.

All this clearly demonstrates that the subjects didn't _enjoy_ harming a fellow human being. Regardless of whether the subjects ended their participation in the experiment or carried on, however, one thing was certain: nearly no one enjoyed administering pain. 

On the contrary, they felt extremely stressed and nervous, ever hopeful that the experimenter would allow them to stop hurting the learner.

After the experiment, many felt bad about just how far they had gone when commanded to do so!

### 8. The further removed the subjects were from the process of shocking, the more obedient they became. 

The degree of authority that subjects ascribed to the experimenters was an important factor in their willingness to obey commands. Equally important was the directness of the subjects' involvement in the process of shocking. So the author modified his experiment once again, this time to see how proximity would affect obedience.

In one variation of his experiment, the author told subjects that they had to administer shocks by bringing the learner's hand into contact with a shock plate. Not only did the teachers have to shock the learner; they had to physically force his hand onto the shock plate.

Such proximity to the learner, and the direct involvement in his suffering, caused the subjects to confront their own behavior. In this new setting, the palpable immorality of the proceedings prompted more subjects to openly disobey.

This _touch-proximity experiment_ actually had a 70-percent disobedience rate, compared to the nearly two-thirds obedience rate of the initial experiment.

In this new variation on the experiment, the subjects were no longer able to deny their culpability by transferring moral responsibility to the experimenter. They had to face the consequences of their actions directly, witnessing the learner's face twist in agony and hearing his cries of pain right beside them. They couldn't pretend not to hear the other person, and certainly couldn't pretend they weren't harming him. 

With no room left for denial, they were forced to face the cruelty of their own actions.

### 9. Breaking free from authority is extremely hard to do – and yet that’s what makes heroes. 

As you've learned, the majority of participants participated in cruel behavior at the behest of the experimenter. But what about the ones who flat out disobeyed? What caused them to resist?

Subjects were put under immense emotional and intellectual strain during the experiment — so much, in fact, that a few decided to break free.

The higher the shock level and the severer the pain inflicted on the learner, the more strained the subjects' relationship with themselves and the experimenter became. As the experiment progressed, the subjects experienced increased stress, nervousness and disdain for the authority figure.

Still, they were conflicted. On the one hand, they didn't want to offend the experimenter and ruin the experiment. On the other, they didn't want to cause harm to a fellow human being. 

Eventually, this dissonance became too much, and they decided to break free and follow their own moral code instead of the cruel command of authority.

Sometimes, disobedience can make you a hero. Even though society tells us to blindly follow the commands of authority, some people dare the seemingly impossible: they disobey.

Looking back to the Nazi regime, most Germans tried to deny or forget the cruel massacres, or simply accept them if told to do so by the authorities. Yet some heroes daringly rebelled against the Nazis, hiding entire families of Jews and other persecuted groups in their basements for months, or working in the resistance to overthrow the Third Reich and help the allies.

Their disobedience was so courageous that, decades later, humanity still sees these rebels as heroes and idols, people so exemplary that we all want to follow in their footsteps.

So what is the final takeaway of Milgram's experiment? We can all follow authority, but being moral and ethical despite the inhumane commands of authority is what heroes are made of!

### 10. Final summary 

The key message in this book:

**Most people will blindly obey the commands of perceived authority, even if it means hurting someone else. This is because disobedience takes an enormous effort — one that will be accompanied by stress and self-doubt. But that doesn't mean we're doomed to follow the herd. All of us are potential heroes.**

**Suggested** **further** **reading:** ** _Intelligent Disobedience_** **by Ira Chaleff**

_Intelligent Disobedience_ (2015) offers insight into why we're so quick to follow orders — even when we know we shouldn't. It gives you all the tools you need to effectively resist the rules, regulations and orders that you know are wrong or harmful — without putting yourself at risk.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Stanley Milgram

Stanley Milgram remains one of the most important and influential scientists in social psychology. His famous "Milgram Experiment" at Yale University has made its way into psychology and popular science books around the world.

