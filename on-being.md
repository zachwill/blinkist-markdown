---
id: 53b3c5c33135350007230000
slug: on-being-en
published_date: 2014-07-01T00:00:00.000+00:00
author: Peter Atkins
title: On Being
subtitle: A Scientist's Exploration of the Great Questions of Existence
main_color: 703B88
text_color: 703B88
---

# On Being

_A Scientist's Exploration of the Great Questions of Existence_

**Peter Atkins**

_On_ _Being_ makes the case for the superiority of the scientific method over religion and mysticism in studying the great questions of existence. Even in those cases where science is not yet able to replace every aspect of religious belief with objective facts, _On_ _Being_ suggests that it's just a matter of time before they do.

---
### 1. What’s in it for me? Learn all about where to look in your search for truth. 

Where do we come from? Why are we here? What happens after we die? These are questions that nearly everyone asks at some point in their life, and in every society there are religions that try to offer an explanation for life's many great mysteries.

They claim that only _they_ have the answers that you seek; only _they_ can offer an explanation as to how we got here, and for what purpose.

Yet, the secret of life, the universe and everything in it is not written upon the pages of religious texts.

Rather, it is _science_ that has paved the way for human understanding, and it is indeed the only real tool available to us that will help us answer the "big questions" in life.

In these blinks, you will learn how the scientific method can be used to explain everything we could possibly want to know. It can reveal to us where the universe, and humanity, comes from, why you look the way you do, and, crucially, what will happen to you after death.

You will also learn how science drives the progress of humanity. In the last 300 years we have learned far more about ourselves, our planet and the universe than in the previous 3,000 years, all thanks to the rigor and meticulous nature of the scientific method!

In these blinks, you'll also learn

  * why men have nipples, but don't breastfeed;

  * why you resemble your parents and not frogs or newts; and

  * why we probably didn't emerge from God's armpit.

### 2. The scientific method is the only reliable tool for understanding the great questions of existence. 

Have you ever wondered how the universe and life as we know it came to be? Or what comes after death? Usually, these questions are reserved for the realm of religion and mysticism, but wouldn't it be better to approach them scientifically?

Indeed, the _scientific_ _method_ is the only real means we have of understanding our physical world. Since the seventeenth century, scientists have found wild success in using the scientific method to develop falsifiable theories and scrutinize their validity through experiments and observations. In fact, as a comparison, science has made more progress within the last 300 years in understanding our existence than religion made in the last 3,000 years.

Its success rests upon its tendency to challenge authority and demand that we discard outdated ideas. This stands in direct opposition to a religious approach to truth, which claims that there is a single supernatural yet infallible explanation — i.e., God — behind life's every mystery.

Moreover, centuries of scientific experimentation have proven that nothing exists beyond the physical world. According to the author, in order to acquire true knowledge we need theories that are continually tested with new evidence. Put simply, theories that rely on the existence of metaphysical phenomena, like God or the human soul, lack objective evidence.

And yet, in spite of this, religious belief remains popular. The widespread acceptance of the phenomena found in religious texts is the result of a deep longing to understand our place in the universe, and these beliefs offer us an easy answer to the great questions of existence and meaning.

However, as comforting as these beliefs may be to some, we lack the evidence that would support them, and thus cannot base our knowledge on mere feelings. To gain true knowledge, we have to look for answers using scientific inquiry.

So, scientists should not shy away from grappling with life's "big questions," many of which we will discuss in the following blinks.

> _"My own faith, my scientific faith, is that there is nothing that the scientific method cannot illuminate and elucidate."_

### 3. Although the exact mechanism is not yet clear, scientists are certain that the creation of our universe occurred naturally. 

Do you believe that an omnipotent being set the universe into motion? If so, you're obviously not alone. In fact, most religions assert that there was an _uncaused_ _first_ _cause_ — i.e., God — that created something out of nothing and conjured order from chaos.

Scientists, however, are not looking for easy answers to these complicated questions. Instead, they seek to demonstrate that the answer to "how the universe came to be" can be found just by looking at the physical world, without grasping at the supernatural.

In fact, a multitude of theories already exist that show a wide range of explanatory approaches to this fundamental question. For example, some theorists suggest that our world originated from earlier universes, and is only one part of an infinite chain of universes whose beginning cannot be pinpointed.

Others, in contrast, theorize that the key to understanding the creation of the universe lies within the origins of electric charge and the properties of _dark_ _matter_ — the as yet unproven concept of mass that appears to interact with the universe but which we cannot see. Although both of these concepts could add a great deal to our understanding of how the universe came to be, scientists remain quite far from uncovering the exact details.

But we shouldn't take as problematic the fact that no single theory has yet been proven: the scientific method is cautious, moving forward in baby steps, slowly building on the foundations of previous discoveries.

What's more, science aims to tackle the "big questions," which, of course, doesn't happen overnight! It takes the cumulative work of generations of scientists to even begin to answer those questions — something that religious thinking simply can't come close to matching.

> _"If anything is worthy of worship, then it is our 4.52 billion year old sun."_

### 4. Science provides the only rational explanation for the origin and development of organic life on earth. 

Would you believe that the Australian High-God Karora gave birth to the first man through his armpit? Probably not. Luckily, science has several more believable accounts of how you and the life that surrounds you came into being.

In fact, the theory of evolution accurately explains how life developed. This is particularly evident in the fossil records discovered in various geographic regions and dating back to many different time periods. In more recent years, scientists have unlocked the evolutionary journey of organisms by looking directly at our DNA.

But how exactly did this process occur? Essentially, what we see today is the result of a _junk-war_ of DNA, in which _junk-DNA_ — basically piles of DNA whose function is not immediately discernable — gets randomly mutated and then "competes" for its usefulness within a given environment.

Some of these changes result in useful traits; others don't. Ask yourself, for example, why men have nipples, but cannot breastfeed, and you start to get an idea of how weird these mutations can be.

Darwin's theory of natural selection asserts that random junk-DNA is constantly competing for its usefulness within a resource-constrained world, which then affects the course of our development. Those who have DNA information that's useful for their environment — like our opposable thumbs or the giraffe's neck — can better survive and are thus more likely to pass on their useful DNA to the next generation.

But how did this DNA come to be? Chemists have now shown in multiple experiments how inorganic matter can become organic life, though they can't know for certain what the exact conditions were that led to life on earth.

However, even if we are still unsure of what transformed dead stone and clay into living organisms, we don't have to embrace creationism. The answer lies within physical, verifiable science; there is simply no need to believe that God breathed life into clay-made Adam and Eve.

> _"At this stage, there is no need to throw in the towel and accept that it was the divine finger that poked rock into life."_

### 5. Despite the mystery and romance surrounding sex in our culture, science can tell us why it’s integral to our existence. 

So, how exactly did you arrive on planet Earth? Were you a twinkle in your father's eye? Did a stork carry you through the window? Not only can science answer this question with pristine clarity, it can also tell us _why_ the process of reproduction exists in the first place.

In fact, in decoding the structure of our DNA, Francis Crick and James Watson uncovered the secret of how reproduction and trait inheritance function at the most fundamental level. It is our DNA that causes our children to resemble _us_, and not, say, a frog or a newt.

It is these genetic building blocks — the sequences of amino acids that form our DNA — that govern the chemical reactions within our bodies, thus determining things like our hair color and the shape of our fingernails.

Yet, the replication of DNA is never perfect: when DNA replicates, there are always errors. And while these errors can result in horrible diseases, they also give us the gift of variability across generations.

In fact, humans _need_ sexual reproduction because we were not perfectly designed for our environment.

The simplest (and least socially awkward) method of reproduction found in nature is asexual reproduction, where organisms recreate perfect replications of themselves.

This is fine for single cells, but _humans_ need to adapt to the ever-changing conditions in their surroundings. The random transformations and combinations of DNA information that are made possible through sexual reproduction bring greater flexibility to cope with this changing environment.

If we were to produce mere copies of ourselves, then we would never develop as a species. And it is science, not religion or mysticism, that has led us to this understanding. Ancient explanations for our existence, such as the Greek belief that Aphrodite arose from the foam of the god Uranus's semen, don't measure up to close scrutiny.

### 6. Human existence ends with death and decomposition – that’s all. 

What happens after death? Do we move on to the afterlife? Or do we rot in the ground?

Our inability to grasp the end of our existence has led to the creation of many myths regarding a life after death. It's quite clear, however, in examining dead and dying bodies that we're destined for rot and decay.

Immediately after death, the body slowly starts showing telltale signs:

The first is a falling body temperature. Humans are mostly made up of water, and as the water in a dead body begins to match the surrounding temperature, the body cools down.

Also obvious are the post-mortem changes in our muscular system. After the initial relaxation of our muscles, the body then undergoes _rigor_ _mortis_, whereby those muscles become stiff and rigid.

Finally, the body undergoes _post-mortem_ _lividity_, in which blood pooling in regions near the surface of the skin causes purple blemishes. Nowhere in this easily observable process do we witness the soul escape the body.

After the body has gone through it's own dance of death, it slowly decomposes at a speed dependent upon its surrounding climate, flora and fauna. Essentially, this happens because the body is no longer able to sustain the chemical processes that prevent decomposition.

To keep the bodily functions running, the chemical reactions that help our bodies grow and sustain themselves must never reach equilibrium with the destructive forces that cause us to deteriorate. They must always stay one step ahead.

During a human life, metabolic activity, fueled by things like food and solar energy, prevents this equilibrium from being reached. After death, however, there's nothing to stop the multitude of bacteria living in our bodies from turning on their former host, devouring and decomposing our once lively body. Here, too, no soul escapes; the energy from our body is simply re-appropriated by the natural process of decomposition.

### 7. It is likely that we, our planet and the universe are not immortal: everything is finite. 

Back in those dark days when our quality of life was generally miserable and the threat of death seemed to loom around every corner, the idea of an eternal afterlife for the soul was quite appealing to the adherents of many religious groups.

Today, we are closer than we've ever been to achieving immortality — the trick is to make sure you don't die before we finally get it right!

Technically, scientific medicine has brought humanity a good step closer to immortality by constantly extending human life spans. It seems there is no good reason why science could not find a way to prevent the process of death.

However, this earthly immortality is totally separate from the religious concept of the resurrection of the soul after death. In fact, the soul doesn't even exist.

There is no evidence whatsoever for any supernatural substance linked to all human beings. Some try to ground the idea of the soul in science by suggesting that it might be the inexplicable "dark matter" of human consciousness. But this is just wrong: consciousness is a result of brain activity, and when the brain stops functioning — e.g., after death — our consciousness goes with it.

In addition, our world is most likely on a timer: eventually, our time will be up, and everything in the universe — you, your dog, the Earth, everything — will come to an end. Of course, many religions assert that either this world or a transcendental world after death is eternal. Yet, these hopes are simply that: hopes, for which there is no evidence whatsoever.

But no matter what they believe, it is highly probable that at least our solar system will come to a grisly end in about 5 billion years, once the sun has finally burned through the remaining hydrogen in its core. All the great achievements of science, and likewise the myths of the world's religions, will vanish during the process.

> "Those who cannot countenance their extinction as their fate, cannot imagine the world going on more or less the same without them."

### 8. Final summary 

The key message in this book:

**When** **answering** **life's** **big** **questions,** **the** **scientific** **method** **is** **the** **best** **approach.** **Religious** **thinking,** **on** **the** **other** **hand,** **undermines** **our** **ability** **to** **establish** **objective** **truth** **regarding** **the** **important** **questions** **of** **creation,** **evolution,** **reproduction,** **death** **and** **afterlife.**
---

### Peter Atkins

Peter Atkins is a British chemist and professor emeritus of Oxford University. In addition to having written a multitude of chemistry textbooks, he has also published numerous pieces of popular science literature. He is also an outspoken atheist and humanist, as well as the first Senior Member of the Oxford University Secular Society.

