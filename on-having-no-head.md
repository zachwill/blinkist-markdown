---
id: 5e8752226cee0700065b508a
slug: on-having-no-head-en
published_date: 2020-04-07T00:00:00.000+00:00
author: Douglas Harding
title: On Having No Head
subtitle: Zen and the Rediscovery of the Obvious
main_color: AF5332
text_color: AF5332
---

# On Having No Head

_Zen and the Rediscovery of the Obvious_

**Douglas Harding**

_On Having No Head_ (1961) is a one-of-a-kind classic of philosophy, spirituality, and mysticism. Combining empirical observations, mystical experiences, logical arguments, personal introspection, practical exercises, Zen Buddhism, and other Eastern spiritual traditions, its aim is to smash through the dualisms that lie beneath much of Western thought: subject and object, mind and body, self and non-self, internal and external world. In their place, the author contends that we can see ourselves and the world around us in a radically different way.

---
### 1. What’s in it for me? See yourself and the world around you in a totally new way. 

Where does consciousness take place?

If you're like most people, you'd answer this question by pointing at your head. After all, that's where your brain is located. And your brain is the part of your body that enables you to think, feel, and perceive the world around you, right? 

That's what modern science tells us — and our common sense and everyday experiences of consciousness seem to agree. For example, as you look at the world around you, where are you looking at it from? Your head, of course — specifically, the eyes inside your head. 

But if you put aside these notions for a moment and just pay close attention to your firsthand experience of consciousness itself, you'll find something very different. And that's what these blinks are all about.

In these blinks, you'll learn 

  * why your consciousness can be described as a headless void; 

  * why that headless void is the cornerstone of your consciousness; and 

  * how Zen Buddhism can help us to understand the meaning of all this.

### 2. The author’s reflections were sparked by a life-changing experience in his early adulthood. 

When he was 33 years old, the author had an experience that completely transformed the way he saw himself and the world around him. All of the ideas we're about to explore are basically reflections on that one experience. So, before we dive into them, let's take a step back and discover what the author experienced.

**The key message here is: The author's reflections were sparked by a life-changing experience in his early adulthood.**

Here's what happened. One day, the author was going for a walk in the Himalayas, when, all of a sudden, he stopped thinking. At that moment, he entered a simplified state of consciousness. He was no longer reasoning, imagining, or interpreting the world through language. For a short period of time, he even forgot his name and the fact that he was something called a "human being." 

Instead, he became focused entirely on the present moment and the immediate sensory experience he was having within it. Here, his attention was drawn to his visual field in particular, and he started mentally tracing the outlines of his own body. Following it downward, he found his pant legs ending in a pair of shoes. To the sides, he found his shirtsleeves ending in a pair of hands. And moving upward, he found a shirtfront ending with — well, nothing. There was absolutely nothing there on top of his shoulders! 

Of course, we know what "should" have been there: his head. But when he looked around and focused solely on his immediate visual perceptions, he didn't see any head. Instead, he just saw an empty space where his head "should" have been. It was a "headless void," as he would later call it. 

However, as he looked closer, he noticed there was something rather odd about the empty space of this headless void. It wasn't really empty at all. In fact, it was quite the opposite. It was totally occupied — filled with grass, trees, hills, mountains, and the bright blue sky of the Himalayan landscape that he was looking at. The absence of his head made room for the presence of the entire world around him. 

But it wasn't just his head that was absent. There was something else that was noticeably missing from the vast and beautiful scene in front of him: the author himself. There was no "he" who was observing it. There was just the scene itself. The world was simply present — existing as a "self-luminous reality" that was "brightly shining in the clear air, alone and unsupported, mysteriously suspended in the void," as he would later describe it.

That might sound rather esoteric or mystical, but to him, in that moment, it was as simple as could be, and it filled him with a sense of peace and joy.

### 3. The author’s experience in the Himalayas changed the way he thought about himself. 

The author's experience in the Himalayas lasted for just a few moments. When it was over, he immediately started to try to make sense of it. 

**The key message: The author's experience in the Himalayas changed the way he thought about himself.**

To begin with, the author realized that he came into his experience with a vague preconception of himself, which went as follows. First, there was his body, which was like a house. Then there were his eyes, which were like two windows at the top of the house. And finally, there was his conscious self, which was like a person living inside the house who could peek out the windows to see the outside world. 

But when he focused on just his immediate visual experience, he found something very different. For starters, there weren't two windows. As far as he could see, there was only one window: his overall visual field. However, on closer inspection, it wasn't even really a window at all, because it had no frame. In other words, there was no border surrounding his visual field; it was just a wide-open expanse — the "headless void," as he called it. 

And then the whole house analogy truly started to collapse, because here came the real kicker: **** not only was there just one frameless "window" that was really a void, but there was no one even looking out of that "window"! There was only the void itself and all of the images filling it — nothing more, nothing less. 

In fact, as far as he could see, there was absolutely nothing intervening between the images and the void. It wasn't as if there were pictures of mountains, hills, and grass being projected against a blank screen or filtered through a pane of glass. The images were simply there, immediately present within the void. 

Nor was there even any distance or separation between the void and the images that filled it. The images and the void were completely fused together into a single, unified reality, which was all that he could see. As far as his visual experience was concerned, nothing else existed. There was only the view itself, existing in a state of utter simplicity.

### 4. You can’t see your own head. 

After learning about the author's experience and his initial observations, you might be feeling a little perplexed or incredulous at this point. What the heck is this all about? What is it getting at? And why did he think it was such a big deal?

To answer these questions, let's try to recreate his experience for ourselves. The first part is pretty easy, and you can do it right now. Look down and to your sides. There you'll see your legs, arms, and torso. Now, follow your torso upward, and eventually, you'll come to where your head "should" be. But it's not there. It's missing. 

**The key message here is: You can't see your own head.**

So what do you see on top of your shoulders? Well, if you're searching for your head, you could say "nothing." It's as if there were an empty space where your head "should" be. But if you look at what you're seeing in that empty space, you'll notice that this "nothing" contains everything — all of the images you're observing in your visual field.

What do you see in those images? Well, it'll depend on where you are, of course. You could be reading this in a cafe, while another person could be listening to it in a car, and you'll both see very different things as a result. But no matter where you are and what you're looking at, you'll never see your head. You can confirm this for yourself by searching for it. Turn your eyes in every direction, and take in everything around you. All sorts of things are on offer to your gaze — but where is your head? It's nowhere to be found. 

In fact, you could go everywhere in the universe and see everything there is to be seen — traveling to the most distant stars in a spaceship or peeking at the tiniest particles through a high-powered microscope. But the one thing you'll never be able to see is your own head.

Now, at this point, you probably have many skeptical questions and objections bubbling up in your mind. If so, you're not alone; so did most of the people with whom the author shared his experience and observations. But have no fear; in the next blink, we'll address these questions and objections head-on — no pun intended!

### 5. You cannot really see your head in a mirror, a photograph, or by any other means; you just see splotches of color. 

You can't see your own head. As strange or silly or obvious as it might seem, that's the basic point of contention here. Later on, we'll take a look at why it matters so much, but first, let's look at the objections that can be made against it. After all, there's no point in trying to understand the significance of the claim if it's not even true in the first place.

Let's start with the most obvious objections. First off, can't you see your head in a mirror or a photograph? Well, imagine you're looking at your image and focus on what you actually observe in your field of vision. Do you actually see your head? Or do you just see some splotches of color? 

**The key message in this blink: You cannot really see your head in a mirror, a photograph, or by any other means; you just see splotches of color.**

Labeling the splotches with the words of everyday speech, we could say that you see a reflection, or a photographic reproduction of your head. But you're not directly seeing your actual head in either of these images. You're interpreting them and ascribing identities to them after the fact of perceiving them.

OK, we could respond. But can't you "directly" see your nose? That's part of your head, and it sure seems like you can see it if you angle your eyes downward in a certain direction. That would seem to disprove the claim that you can't see your own head. You can see part of it, at least.

But can you really? Once again, look down and observe what you actually see. Depending on your skin tone, you'll just see a couple of pink, tan, or brown splotches in your field of vision. You interpret these images as corresponding to your nose, but they're not your nose itself. 

Think about it this way: If someone came up to you and showed you a couple of splotches on a piece of paper, would you call them a nose? No, you'd just call them a couple of splotches. And rightfully so — a couple of splotches do not equal a nose. After all, a nose, as we commonly understand it, is a three-dimensional part of the body that consists of flesh, blood, mucus, and other biological components. That's a far cry from a couple of splotches of color. 

Already thinking of more objections? Don't worry; we're just getting started!

### 6. Just as you can’t see your head, you can’t see your body, either. 

We're on a roll, so let's keep the objections coming!

Continuing where we left off, we could say the following: OK, sure, without the aid of a mirror or a photograph, we can only see a couple of splotches of color that we interpret as being our nose. And maybe it's true that there's a distinction to be made between those splotches of color and what we think of as being our nose itself. But can't the same argument be made about every part of our body?

**The key message here is: Just as you can't see your head, you can't see your body, either.**

Earlier, we talked about looking down and seeing our feet, legs, arms, hands, and torso. But when we look down, what do we actually see, if we just pay strict attention to what's right in front of us? Well, we only see more splotches of color, much like our nose. Sure, the images are sharper — maybe we should call them shapes instead of splotches. But all the same, aren't they both just images at the end of the day? And, in that case, couldn't we say that we're unable to see any part of our body — not just our head?

But what's wrong with this line of reasoning? Far from being an objection, isn't it just the truth? Look down at your torso. What do you see? There's just a roundish, rectangular shape displaying the colors of whatever article of clothing you're wearing. Is that your torso itself? Is that the thing you have in mind when you think of the word "torso?" Is it an assemblage of bones, muscles, organs, and all of the other things that you learned about in anatomy class? No, it's just a colored shape. And similar arguments could be made about all of the other parts of your body that you supposedly see. 

So, yes, not only have you lost your head, but now you've lost your entire body! Or to be more precise, you never had either of them in your conscious awareness to begin with; you only thought you had them. Really, you just had visual perceptions that you interpreted as being your body. 

The plot thickens. The mystery deepens. And in the next blink, the objections and counter-arguments will kick into an even higher gear.

### 7. The claim that we don’t have heads or bodies is more reasonable than it might appear. 

Alright, so at this point in the argument, we've lost not only our heads, but our whole bodies as well. Have we also lost our minds in the process? Surely it's ludicrous to claim that we don't have heads or bodies, no matter how clever our argument might be.

**The key message here is: The claim that we don't have heads or bodies is more reasonable than it might appear.**

If the claim is ludicrous, then we should be able to disprove it easily. But what evidence do we have to say we do have heads and bodies?

Well, as we've seen, our visual perceptions cannot help us out here, but we have many other senses, too. Can't we feel aches and tingles in our various body parts? Can't we put our hands on those body parts and feel them? In fact, can't we even put our hands on our heads and thereby prove that they're right there on our shoulders?

But, once again, put aside any interpretations of your perceptions and focus just on the perceptions themselves. What do you actually observe? You just feel a bunch of sensations of pain, tingles, and touch. Even combining them with your visual perceptions, do they add up to a foot, a torso, a head, or any other part of your body? You interpret them as signs that those body parts exist. You believe that they correspond to the external objects you call your foot, your torso, and your head. But none of them are those objects themselves. 

In fact, you have no direct evidence that any such external objects exist in the first place. All you have is an assortment of perceptions and sensations. You can mentally reconstruct them into objects, and you can tell yourself that those objects exist. But these are all ideas that you are entertaining in your mind and reading into the evidence provided by your senses. 

Perhaps those ideas happen to be true; perhaps they're false. But right now, that's beside the point. The point is simply that you don't have any evidence for them. They're only inferences, not established facts. And here, to understand the nature of our conscious experience, we're just trying to focus on the facts. 

Speaking of facts, what about science? Surely a scientist would have a good objection to make to all this philosophical talk. In the next blink, we'll listen to what she might say.

### 8. The testimony of outside observers does not disprove the claim that we do not have bodies. 

We started with a claim that we can't see our own heads. We've now expanded that claim into an even bolder one: as far as we can tell, based on the direct evidence provided by our senses, we don't even have a body, let alone a head! 

Now, here's one of the most obvious objections that can be made against this expanded version of our claim: Can't we just go up to someone and ask, "Hey, do I have a body?" She might give you a rather quizzical look and ask if something is wrong with you, but surely she'd say, "Yes, you have a body." And everyone else in the world would agree. Doesn't their testimony provide us with evidence for having a body? 

Well, no, because they're in the same position as we are.

**The key message here is: The testimony of outside observers does not disprove the claim that we do not have bodies.**

Just like you, all they ever see of your alleged body is a bunch of colored shapes. All they ever feel is a bunch of sensations of touch. And all they ever smell — well, you get the point. 

But if an ordinary outside observer can't be of help to us here, maybe an expert observer could come to our rescue. What could a scientist do to try to get around this conundrum? Well, she could probe into our bodies and see all sorts of things with various microscopes and scanners. She could see all of those things that we call organs, cells, and molecules. She could point them out to us and tell us their names and functions. 

But once again, at the end of the day, all she sees are the splotches of color that show up in her instruments. In this respect, she's got a more varied, more detailed, and better labeled picture than ours — but it's still just a picture. 

However, she can also assemble the bits and pieces of that picture into a sophisticated scientific model of how our brain works. Now, our brain is a part of our body — and according to our modern, scientifically informed common sense, it's not just any part; it's the one that gives rise to consciousness. So maybe the scientist can explain this whole matter to us and put an end to all of this business about not having a body. 

Let's see.

### 9. The scientific explanation of consciousness fits the claim that we do not have bodies. 

To avoid getting bogged down in too many details, let's focus on just one aspect of our conscious experience — the same one we started these blinks with: our vision. How would modern science explain this to us? 

**The key message here is: The scientific explanation of consciousness fits the claim that we do not have bodies.**

Science would tell the story of vision something like this. First, a lightwave hits an object — a mountain, let's say. Part of the lightwave bounces off the mountain and reaches your eyeballs, where it ends up triggering some chemical changes in the light-sensitive cells inside your retinas. 

Disturbed by the light, the cells pass on their agitation to other cells, thus creating a signal that eventually reaches a certain part of your brain. And here, at this terminal point, the signal somehow gets converted into a consciously experienced image of a mountain. 

We could call this terminal point the "Grand Central Station" of your experience of the here and now. Because it's only here and now, at this particular point, that the visual experience of your consciousness somehow springs into being. And at this mysterious point, we come right back to where we started — plunged into an experience of consciousness that envelops everything we can ever be aware of. All of our perceptions, sensations, feelings, and thoughts pass through this experience — even the very words you're hearing and the thoughts you're having about the scientist's story!

As for that story itself, assuming that it's true, it would only seem to provide further corroboration of our argument. The scientist is basically telling us that the mountain we see isn't an object existing "out there," in an external reality that's separate from us; it's just an image corresponding to some sort of neural excitement happening "in here," inside our brains. 

The same is true for all of our other sensations and perceptions. According to the scientist, they're all just various types of experiences corresponding to various types of neurological activities. Add them all together, and you have your overall experience of consciousness. And what does that experience consist of? 

Well, various sensations of sight, sound, taste, and so forth. Once again, we're right back where we started — surrounded by a realm in which we can only find splotches of color and things of that sort. You can interpret them however you want — but the result is just an interpretation. And as far as you can tell, even that interpretation itself only exists in your experience of consciousness.

### 10. As far as we can tell, there’s nothing beyond our immediate experiences. 

To recap the author's argument and then drive it home, let's imagine we're in his shoes, going for a walk in the Himalayas. After realizing we can't see our own heads, we stop and look around. What do we see? 

**The key message here is: As far as we can tell, there's nothing beyond our immediate experiences.**

As we look around, we see mountains, hills, and grass, but those are words we're using to label the images that fill our visual field. Let's just focus on the images themselves. What do we really see? There's simply a bunch of colored shapes. 

For the sake of convenience, we can call these shapes by their names, but let's remember that these words are extra elements we're adding to the scene. The same is true of the concepts and knowledge those words conjure up for us. For example, we might associate a patch of green with the word "grass," which we know is a type of plant that's pretty small compared to a mountain. 

With that knowledge in mind, we can infer that a blade of grass is very close to us, and a mountain is very far from us if they appear similar in size. After all, we could reason, the actual blade of grass and the actual mountain are not really the same size; they just look that way because of the position from which we're viewing them.

But these are all ideas and sequences of reasoning that we're adding to our immediate visual experience. Again, if we just focus on the experience itself, all we see are the colored shapes, existing in a single field of visual perception. And within that field of perception, there aren't any differing amounts of distance between us and the two shapes in question. 

In fact, there's no distance at all, and there's not even a separate "us" from which the shapes could be distant in the first place. If we just focus on our visual experience, we only see a single field of perception in which the two shapes are equally co-present. In this respect, they're like two splotches of paint on the same canvass. We can mentally project distance onto the splotches, but it's not a part of the splotches themselves; it's something we're reading into them. 

In truth, however, there is no canvass; there is no surface on which the shapes are being painted. Within our immediate visual experience, the shapes are simply present, and there's literally nothing behind them.

### 11. Nothingness is at the heart of consciousness. 

At this point, all of these ideas might seem like much ado about nothing. And in a way, that's exactly what they are. But this nothingness is absolutely essential to our experience of consciousness. So it's well-deserving of our attention. 

**The key message here is: Nothingness is at the heart of consciousness.**

To see why, imagine if you _could_ see your head — not indirectly in a picture, a mirror, or some other reflective surface, but directly in your field of perception. In other words, imagine if the headless void of your visual field was filled up with your head. 

In that case, your head would blot out your entire field of vision, and you would cease to see anything else. By being absent from your visual field, your missing head makes room for the world to be present. As far as your visual experience is concerned, the nothingness of your headless void is the precondition for everything else to exist. 

For the sake of simplicity, we've been focusing on visual perceptions, but we can apply the same logic to all of our other senses as well. For example, we can't hear any sounds unless there is a backdrop of silence from which they can emerge. Likewise, we can't taste, smell, or touch anything unless there's an underlying absence of taste, smell, or touch against which those sensations can be contrasted. In fact, we can apply this logic to all aspects of our conscious experience — even our thoughts and emotions. To experience them, we need to have some sort of blank mental space in which to think and feel them. 

Now we can start to understand the significance of the nothingness in question. It's not just at the core of our visual experience; it's the central fact of every aspect of consciousness in general. If we drill down into all of our perceptions, thoughts, and feelings, we find nothing underneath them. That nothingness is both the precondition for them to exist and the medium in which they take place. In short, everything we experience emerges from nothingness. And that nothingness, in turn, contains everything we experience. 

By focusing our attention on this nothingness, we can therefore cut to the very heart of our consciousness and put a finger on its underlying nature. 

And at its heart, we find… well, nothing.

### 12. If nothingness is at the heart of consciousness, we can dispense with many assumptions of Western thought. 

Alright, so nothingness is at the heart of consciousness. But who cares? Even if it's true, why does it matter? 

Well, if it _were_ true, it would demolish many of the main assumptions underlying Western ideas of consciousness and selfhood. And in their place, we could construct a very different conception of who we are as conscious human beings. 

**The key message here is: If nothingness is at the heart of consciousness, we can dispense with many assumptions of Western thought.**

To bring these assumptions to the surface, imagine if an alien asked you to provide the most basic description of yourself as a human being. It might go something like this. To start off, you have a body. That body has various parts — two arms, two legs, a torso, and a head. Inside that head, you have a brain. And thanks to that brain, you've got a consciousness. 

And what is that consciousness? Well, in the language of Western philosophy, many of us understand it in terms of a subject-object duality. On one side, there's your mind. That's you — the thinking, feeling, and perceiving subject. And then, on the other side, there's everything your mind apprehends: trees, buildings, other people, and even your own body. All of these things are objects, which are external to your mind. 

This leads to even more dualities: mind and body. Self and non-self. An internal mental world and an external material world. These dualities underly many of our common sense notions of the nature of reality and ourselves. And they all follow from that initial model of consciousness. 

But as we've seen, if we just look at our actual experience of consciousness itself, we don't find anything of the sort. We don't find any external objects corresponding to our supposedly internal perceptions. We don't find a separate subject who observes those objects like a person peeking out of the windows of a house. We don't find any body from which our mind could be separate. Heck, we don't even find a mind, if by mind you mean a perceiving subject who stands apart from the things he or she perceives. 

Instead, we just find a single continuum of conscious experience. Our thoughts, feelings, and perceptions of the world are all fused together into a unified whole. 

Now, our common sense might reject this. Surely there must be a subject who is thinking these thoughts, feeling these feelings, perceiving these perceptions, and experiencing this experience! 

But if you go looking for that subject, all you find is nothing. And that's precisely the point.

### 13. Zen Buddhism contains many parallels to the author’s experience and reflections. 

If all of this talk about "nothingness" sounds kind of "Zen" to you, well, there's a reason for that. As the author tried to make sense of his experience in the Himalayas, he studied many ancient and modern texts of philosophy, religion, spirituality, and poetry. Some of the most useful texts, he found, were those written by the masters of Zen Buddhism. Here, he found many reflections and descriptions of experiences that seemed to be similar to his own. 

**The key message here is: Zen Buddhism contains many parallels to the author's experience and reflections.**

For example, consider the story of Tung-shan, a ninth-century Zen master from China. When he first became a student of Zen, he found himself perplexed by one of the tradition's main teachings — a teaching expressed in one of Mahayana Buddhism's most well-known texts: the _Heart Sutra_. According to this collection of aphorisms, the body is just "emptiness" and there is "no eye, no ear, no nose." 

One day, Tung-shan was going for a walk, when he looked down into a pool of water and saw a reflection of his own face. At that moment, he realized that the image of his face was over "there," in the water. Meanwhile, over "here," in the consciousness where he was experiencing that image, there was just a transparent empty space. The teaching of the _Heart Sutra_ suddenly made sense to Tung-shan, and he went on to become the Zen master who founded Soto, the largest sect of Zen Buddhism. 

Now, fast forward to the sixteenth century, when the Chinese Zen master Han-shan wrote about another similar experience. He too was going for a walk when he suddenly came to a stop and realized that he "had no body or mind." Instead, he could only perceive "one great illuminating Whole — omnipresent, perfect, lucid, and serene." 

Let's end by focusing on that last idea: serenity. As the Zen Buddhists have been teaching for centuries, there is a deep sense of inner peace that can come with the vision of reality that the author experienced. In this vision, there is no isolating separation between us and the world around us — no dualities between subject and object, self and non-self, internal or external world. In fact, there is no "us" in the first place. We are nowhere to be found, and in some sense, we are nothing. 

But if we look closely into that nothingness, we'll see that it is also everything — the whole world that fills up our headless voids, resting in a state of self-contained perfection.

### 14. Final summary 

The key message in these blinks:

**If we pay close attention to our firsthand experience of consciousness, we find that it is both simpler and stranger than we previously thought. We do not find any dualities between subject and object, mind and body, self and non-self, or an inner and outer world. Instead, we just find a headless void — a nothingness that contains everything we experience.**

Actionable advice: 

**Experience this for yourself.**

Imagine trying to describe the taste of bread to someone. No matter how vivid or precise you make your language, you can never fully convey the actual experience of tasting it through words alone. For that, the person has to taste the bread for herself. The same goes for the experience the author had. To experience it for yourself, try pointing your finger at yourself, straight toward the place where your eyes "should" be. Now, ask yourself — what are you pointing at? Don't just answer the question verbally and say your "eyes." Try to see what you're pointing at. If you look close enough, you'll see nothing — and that's the whole point! Once you get the hang of seeing this nothingness, try practicing it as a form of meditation. The more you do this, the more you'll be able to get into the headspace of headlessness, which can bring you peace and joy. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Waking Up_** **, by Sam Harris**

The ideas you just encountered might seem rather mystical or unscientific — but the popular writer, neuroscientist, and New Atheist Sam Harris thinks they provide a lot of food for thought. He's discussed them in many places: in his online videos, his podcast, his meditation app, and his _New York Times_ Bestselling book _Waking Up_. 

Here, Harris tries to bring spiritual ideas and practices to readers who are just as non-religious and scientifically-minded as himself. Like Douglas Harding and the masters of Zen Buddhism, Harris takes aim at the notion of a self that is separate from the external world. He argues that our sense of self is an illusion, and we can see this by practicing mindfulness meditation. To learn about these and other intriguing ideas, check out our blinks to _Waking Up_, by Sam Harris.
---

### Douglas Harding

Douglas Harding was an English philosopher, mystic, and spiritual teacher. He was the author of many books, including _The Hierarchy of Heaven and Earth_. The famous writer and Christian theologian C.S. Lewis wrote the preface to that book, where he praised it as "a work of the highest genius." On the opposite end of the religious spectrum, parts of _On Having No Head_ have been championed by the prominent New Atheist and neuroscientist Sam Harris.

