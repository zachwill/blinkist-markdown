---
id: 58c6b8be8d9003000407262c
slug: on-his-own-terms-en
published_date: 2017-03-17T00:00:00.000+00:00
author: Richard Norton Smith
title: On His Own Terms
subtitle: A Life of Nelson Rockefeller
main_color: BE7E4A
text_color: 8C5D37
---

# On His Own Terms

_A Life of Nelson Rockefeller_

**Richard Norton Smith**

_On His Own Terms_ (2014) tells the remarkable life story of Nelson Rockefeller, who used his family's notoriety and wealth to change the world for the better. These blinks walk you through Nelson's life, from his involvement in the family oil business to his extended career in politics.

---
### 1. What’s in it for me? Get to know one of the most influential public figures in twentieth-century American history. 

Particularly in the United States, the name Rockefeller has become synonymous with industry and wealth, but also with philanthropy and public buildings. All over the country, schools, libraries and research institutions bear this prestigious name.

The foundation for the family empire was Standard Oil — the world's largest oil refiner at the time and one of the world's first multinational corporations. With it, the Rockefeller family rose to prominence and infamy at the end of the nineteenth century and dominated the business landscape of the first half of the twentieth century.

With such an esteemed last name, it's often easy to look past the very individuals who carry it. These blinks will focus on the life of Nelson Rockefeller, the successful businessman, philanthropist, Republican and — perhaps above all else — restless, devoted worker, who carried his family name into the latter half of the twentieth century and left an indelible mark on American politics.

In these blinks, you'll find out

  * how Rockefeller's interest in Latin America shaped US foreign policy;

  * which international institution's headquarters Rockefeller brought to New York City; and

  * how Rockefeller's policies as governor of New York would have a lasting impact.

### 2. Nelson Rockefeller was born into an extraordinarily wealthy family, but experienced his share of struggles as well. 

You've likely heard of John D. Rockefeller Sr., the famous American oil tycoon and often considered the wealthiest person in modern history. But have you heard the name Nelson Rockefeller?

Born on July 8th, 1909, the third child of Abby and John D. Rockefeller Jr., Nelson was the grandson of John Sr. who, along with his brother, William Avery Rockefeller Jr., founded Standard Oil, one of the largest oil companies in the world.

William Avery Rockefeller Sr., Nelson's great-grandfather, was a scoundrel and a con man. He earned a living by traveling the country peddling fake pharmaceuticals, a practice that earned him the nickname "Devil Bill."

But Nelson's father, John D. Rockefeller Jr., took a different tack, spending much of his life, and the family's money, on philanthropic projects. He passed this passion onto his children, and Nelson, in particular, would inherit his father's devotion to improving the world. But Nelson had another thing going for him: the creative business instincts of generations of Rockefellers before him.

While he came from a very privileged background, Nelson's life was not always one of ease and comfort. He struggled with a learning disability that would affect him until his death.

While growing up, it became clear that Nelson suffered from dyslexia, which made his schoolwork extremely difficult. Fortunately, he had a team of fantastic tutors and was enrolled in the Lincoln School, a progressive educational institution that let students build their own courses and learn by doing. For instance, if a child was obsessed with boating, they could use their passion to learn about geography, history, music and literature.

It was as a result of this supportive environment that Nelson managed to make his way through school, excelling in math, science and biology. He also proved to have boundless energy and an attention-seeking personality.

While his disability left him with grades that were just average, his marks were still good enough to get him into Dartmouth College in New Hampshire. There he earned the lifelong nickname "Rocky" and developed his passions for photography, art and architecture.

### 3. Rockefeller applied his passion for art to his work at MOMA and the Rockefeller Center. 

Toward the end of his time in university, Nelson tied the knot with his longtime sweetheart, Mary Todhunter Clark. The newlyweds then embarked on a ten-month, around-the-world honeymoon.

Upon returning from this trip, in April of 1931, Nelson began helping his mother, Abby, establish the Museum of Modern Art, or MOMA, in New York City. This role was perfect for him as Nelson, inspired by his mother, had grown up with a passion for art.

Nelson became chairman of the Junior Advisory Committee at MOMA and helped the museum introduce artists like Picasso, Matisse and Diego Rivera to the American public. Simultaneously, he assisted his father in establishing Rockefeller Center, which proved to be a formidable challenge.

Nelson's father had secured the lease for 228 dilapidated brownstone buildings in Manhattan where he intended to build a commercial center designed around an opera house. With a price tag of over $200 million, it was the biggest-ever deal of its kind.

At first, the Rockefeller Center was going to be named Metropolitan Square, but following the 1929 stock market crash, the Rockefellers had no choice but to finance much of the project themselves and thus gave it their name.

Nelson worked with architects to design the now-iconic buildings and began wooing businesses to lease spaces inside them. In fact, Nelson was closely involved in the artistic aspects of both MOMA and the Rockefeller Center. For instance, he advocated for the local New York architects Philip L. Goodwin and Edward Durell Stone to design MOMA, despite more famous international architects, such as Le Corbusier, expressing interest.

What was even more controversial was Nelson's choice to commission a mural by the leftist artist Diego Rivera for the Rockefeller Center lobby. In the end, the mural had to be painstakingly and expensively removed after Rivera added a prominent portrait of Vladimir Lenin. Nonetheless, Nelson still admired Rivera and, some eight years later, the two mended their friendship.

### 4. An ambitious South American project landed Nelson his first job in politics. 

By 1934, Nelson had fathered two children and was proving to have a natural gift for business. Nelson's father happily rewarded his son's emerging talent with $3.2 million worth of stock in a Standard Oil subsidiary called the Socony-Vacuum Oil Company, shortly before opening a $12-million trust fund for all his sons.

With this capital in hand, Nelson jumped into action, thinking of business projects that would pair the love of art he had inherited from his mother with the philanthropic drive instilled in him by his father. This search led him on a path to revitalizing the economies of South America.

When visiting Creole Petroleum, a Venezuelan subsidiary of the family's oil business, Nelson searched the region for artifacts as a representative for New York's Metropolitan Museum of Art. This visit opened his eyes to the region's severe inequality, as Creole Petroleum's offices were located right next to a poverty-stricken community.

So, he sent directives to the company to practice social responsibility and share its wealth with the community by building schools, hospitals and infrastructure and working to eradicate disease. With this mission in mind, Nelson launched the Venezuelan Basic Economy Corporation, or VBEC, to help local South American economies loosen their dependence on oil, attract more tourism and enjoy a higher quality of life.

This project landed Nelson his first job in politics. At the time, he was meeting with South American leaders and his natural gift for diplomacy was on full display. For instance, after a meeting with then-president of Mexico, Lazaro Cardenas, Nelson impressed the leader, and also improved foreign relations, by hosting a huge MOMA exhibition on 2,000 years of Mexican art.

US President Franklin Delano Roosevelt took notice of the exhibit and made Nelson Coordinator of Inter-American Affairs, or CIAA, in 1940. During his time in this position, Nelson focused on the review of laws, coordination of research between departments and on proposing new legislation to improve relations with South America.

### 5. Nelson’s status in Washington rose as the United States entered World War II and the United Nations was formed. 

While Nelson and the Rockefeller family had long supported the Republican party, Nelson was a great admirer of the Democratic president Franklin Delano Roosevelt, whose problem-solving abilities he respected immensely. It was also thanks to FDR that Nelson was beginning his career as CIAA and, since he held this position during World War II, the scope of his work would grow to be quite extensive.

As the United States entered World War II, the nation passed a "good neighbor" policy toward Latin America, and Nelson's importance grew.

In Washington, his CIAA section came to be known as "The Rockefeller Office" and his purpose was clear: Nazi sympathizers in South America were on the rise and it was the CIAA's job to convince Latin Americans that democracy and capitalism were the better route. To help him achieve this, his team of staff grew from 75 to 1,400.

This mission allowed Nelson to continue carrying out the program he had begun with VBEC. He carried on advocating for increased foreign aid packages, improving the standard of living in South America through public health campaigns and making the region more appealing to foreign investment.

It wasn't long before FDR approved a $25-million budget for Nelson's Latin American programs and gave him a promotion to Assistant Secretary of State for Latin American Affairs. But Nelson again faced adversity in 1945, when the United Nations formed.

After all, with the attention of most Americans fixed on Europe as World War II came to a close, many people in Washington failed to see the importance of aiding South America, and especially Argentina, a country that had been harboring Nazi spies and sheltering Nazi investments.

Nonetheless, Nelson fought hard to sign Latin American countries onto a regional peace treaty and even lobbied on behalf of Argentina, securing the country a place in the UN.

### 6. After the war, Nelson combined his knack for business with his passion for overseas economic development. 

When FDR passed away in 1945, Vice President Harry Truman took office, and the establishment of this new administration left Nelson out of a job. So, he simply picked up where he had once left off, stepping in as president of the Rockefeller Center and securing MOMA a place at the core of New York's art world.

But he didn't just throw away all the hard work he had put into establishing connections in South America; instead, he went to work establishing a business that would function like a charitable organization.

With the intention of beating back the tides of Marxism — and proving that capitalism could also have a heart — on July 1, 1946, Nelson founded the American International Association for Economic and Social Development, also known as the AIA. However, he knew that this organization would need to make money to survive, so he formed a for-profit extension to fund the charity. Called the International Basic Economy Corporation, or IBEC, it would be composed of international investors and stockholders.

Through the AIA, Nelson was able to show that, with a mere $9,000, he could successfully halt a cholera outbreak among Brazilian hogs. The AIA also formed thousands of clubs that taught farmers agricultural methods to increase their yields, while working to spread refrigeration and open supermarkets to improve the distribution of food. In so doing, the association helped slow the formation of shantytowns in Brazil, which were being created by farmers flocking to cities in the hopes of landing better jobs.

Unfortunately, this work was an uphill battle. It would be more than a decade before IBEC turned any profit, and the AIA could never overcome what was proving to be an increasingly complicated and dangerous political climate in South America.

### 7. After helping bring the UN to New York City, Nelson jumped back into politics with a position in the Truman administration. 

Although he was frustrated by the problems he encountered in his philanthropic work, there was a bright spot for Nelson during this period — an event that would remind everyone of his impressive political power.

It happened in 1946 when Nelson received a tip that his hometown of New York City was in the running to headquarter the United Nations, competing against Philadelphia, Boston and San Francisco. Invigorated by the news, Nelson had found exactly the type of challenge on which he thrived; the UN bid was a problem in need of a solution and he was determined to be the man to find it.

So, with his partner, the architect Wally Harrison, he drew up plans for an impressive building. Then, through some wheeling and dealing enabled by his vast network, Nelson secured a location in Manhattan, right along the East River in Turtle Bay.

With the deal nearly closed, Nelson rang up his father to inform him of the opportunity and the tremendous prestige they would earn by bringing the UN to NYC. John Jr. was sold; he agreed to pay the $8 million dollars needed for the land and the deal was struck.

Naturally, securing the UN for the city pleased many New Yorkers and increased Nelson's popularity in the city — but that wasn't enough for him. He was still hoping to get back into international politics and, when the Korean War broke out in 1950, he found just such an opportunity.

With the Cold War intensifying, Nelson became instrumental as a public figure who could put a good face on capitalism. So, Truman gave him a job on the International Development Advisory Board. In this capacity, Nelson helped create Partners in Progress, an initiative that improved job training, education and public health in underdeveloped nations, while promoting free trade and foreign investment.

### 8. During the Cold War, Nelson faced Republican opposition but made a lasting impression through a key research project. 

Throughout Nelson's political career, the Republican party would shift to the right, straying further and further from the ideals of the progressive-minded party once led by Abraham Lincoln. Because of this ideological turn, Nelson's disputes with certain Republican leaders worsened during the administration of Dwight D. Eisenhower.

Things were so strained that, when Truman left office, he advised Rockefeller to switch allegiances and join the Democratic party. But Nelson chose to remain a loyal Republican.

While he was less than impressed with Eisenhower, he accepted the new president's offer to join the President's Advisory Committee on Government Organization and become the Undersecretary of the newly founded Department of Health, Education and Welfare, also known as HEW.

Despite their differences, Eisenhower and Rockefeller did share some common ground. For instance, both men believed that the Republican party should be progressive and that the government should expand social security while fighting cuts to education and health care.

So, as undersecretary of HEW, Rockefeller proposed the addition of millions of new jobs in the social security sector, along with increases to monthly benefits. He also proposed similar ideas regarding the reform of education and health.

The problem was that such sweeping changes would mean an overhaul of the budget, and most Republican legislators were busy advocating for smaller government that would let individual states handle such issues. Naturally, Nelson's inability to do anything about this frustrated him, and he resigned from his post at HEW.

Following his resignation, the Eisenhower administration authorized him to conduct the Special Studies Project, an endeavor that would leave a lasting mark on American politics. Through this project, Rockefeller brought together the best minds from across the country to discuss politics, business and defense, as well as how to improve the federal government and military.

Among those included in these discussions were scholars, scientists and a Harvard professor by the name of Henry Kissinger who would go on to play a decisive role in US politics over the ensuing decades. Notably, many of the ideas developed through the project would be used by future presidents from John F. Kennedy to Ronald Reagan.

### 9. Nelson became governor of New York and, if not for pushback from the Republican establishment, might have been president. 

Following the Special Studies Project, Nelson went back to being a private citizen. But not for long. Although he had been working hard to prevent the Brooklyn Dodgers from moving to Los Angeles, his focus quickly changed when it became clear that he had much higher approval ratings than any of the candidates running for governor of New York.

In fact, no other American politician appealed to such a broad range of people quite like Nelson Rockefeller did. He threw his hat in the ring and, in the 1958 race for governor of New York, Nelson ran a vigorous campaign across the state. He traveled 8,500 miles, gave 135 speeches and shook hands with some 100,000 people.

While his dyslexia put Nelson at a disadvantage when reading prepared speeches, his natural charm easily made up for it. Not only that, but unlike any other politician of the time, Nelson appealed to voters across the political spectrum.

As a result, when he won the election, it was with a remarkably high measure of support among registered Democrats, including one-third of New York's black and Jewish voters. But it wasn't just his charm that won him the governorship; people were also drawn to his big ideas on how to reform education, transportation, working conditions, housing and health care.

Given the fact that many previous presidents, including both Roosevelts, had used their elections as governor of New York as springboards to the White House, a presidential run wasn't inconceivable for Nelson. However, the Republican party had another candidate in mind.

During this time, Nelson was receiving a great deal of press that billed him as "a Republican FDR" and "the Wunderkind of American Politics." But when the 1960 presidential election rolled around, Republican leaders firmly fell in line behind Richard Nixon.

Nixon and Rockefeller were seen as the two best candidates, but their differences — along with Nelson's refusal to compromise his views to conform to Republican rhetoric — meant that Rockefeller was snubbed by the Republican party.

> _"He's impossible not to like."_ \- Hall of Fame baseball player Jackie Robinson on Rockefeller

### 10. Nelson faced the deaths of many people close to him, a scandalous divorce and the tragic loss of his son. 

As 1960 began, Nelson lost the Republican nomination to Nixon. But that wasn't the only loss he suffered that year.

In the first two months of 1960, his press secretary died, along with his friend and advisor, Frank Jamieson. After that, Nelson's father, the legendary John D. Rockefeller Jr., passed away on May 11, 1960 — and that was just the beginning of what would be a heartbreaking series of events.

While Nelson and his wife, who went by Tod for short, had worked hard to keep up a happy front, their marriage had been a struggle for 20 years and Nelson knew that it was time for a divorce. In fact, from the late 1930s onward, Rockefeller had been exhibiting a strong penchant for affairs, especially with his female secretaries.

Only this time, he was actually in love and wanted to marry his executive secretary, Margaretta Large Fitler Murphy, better known by her nickname, "Happy." At the time, Happy was also married with four children.

When you're a politician, there's never a good time to announce your divorce and Rockefeller must have known that the American public had never elected a president who had been divorced. But after being shut out of the 1960 election, he decided to publicly split with Tod.

The president at the time, John F. Kennedy, along with countless others, told Nelson that divorcing the mother of his five children would be political suicide. But that didn't matter to Nelson; at that moment, his love life meant much more than politics.

Then, on November 19, 1961, just as his marriage was coming to an end, things got much worse. A mere 48 hours after the news of their divorce went public, Nelson and Tod's 23-year-old son Michael was lost at sea while visiting the Asmat region of Indonesia.

Michael, who had inherited his father's love of art, was traveling around taking pictures and collecting tribal art for a potential museum exhibition when his ship capsized in the Arafura sea. Despite Nelson's commitment to searching for his son, his body was never found.

> _"No man would ever love love more than he loves politics."_ \- John F. Kennedy on Nelson's choice for divorce

### 11. In another race for president, Nelson faced the immense challenge of political extremism. 

The political climate in the United States had been fraught for decades, but the 1960s saw more discord than ever before. A perfect example of this schism is that, in June of 1961, while the Republican Party chairman was hosting a whites-only dinner in Mississippi, Nelson Rockefeller was hosting Martin Luther King Jr. for dinner at his home.

In fact, Rockefeller had always been a strong proponent of civil rights, even while his fellow Republicans were courting Southern white voters and growing more extreme by the day. As a result of this climate, during the 60s, there were a number of extremist right-wing political organizations, including the Conservative Party, the Christian Anti-Communism Crusade and the notorious John Birch Society, a group so extreme that it branded former president and World War II commander Dwight Eisenhower a communist agent.

Many in the Republican party were not above working with these extremists to elect their candidates. Even the Young Republicans were publically opposed to a federal mandate ending segregation. Naturally, in this climate, Rockefeller was an odd man out within his own party, ashamed that Republicans were becoming the "white man's party."

As he continued his run for the presidency, the Republican Party showed that, just as Nelson was upset with them, they were quite irritated with him as well. This became apparent in the race leading up to the 1964 Republican Convention, where the party nominated Arizona senator, Barry Goldwater.

At the convention, as Rockefeller got on stage to give his speech, the crowd of delegates battered him with boos, heckling him as a "goddamn socialist." While Rockefeller lost the nomination that year and Goldwater went on to lose to incumbent President, Lyndon Baines Johnson, in a landslide, Nelson didn't mince his words at the convention.

In the face of all this jeering, he gave an incredible speech, one highly critical of the political extremism that was on the rise in the United States.

### 12. As governor of New York, Nelson made many notable and enduring changes. 

After the Democrats won the presidency in 1960 and again in 1964, many Republicans were eager to blame Nelson for the party's lack of unity, as he had rejected the party's rising conservative and extremist bent. At the same time, Nelson was facing opposition as governor of New York, but through skillful leadership, he managed to put plenty of ambitious and progressive plans into action.

For instance, when Rockefeller was elected governor, the New York education system was a mess, ranking next to last in polls on US higher education. Nelson worked hard to secure more money for the State University of New York, or SUNY, and created more community colleges, along with the City University of New York. By 1973, SUNY was the largest university system in the world, with 64 campuses and a quarter of a million students.

Nelson was also a staunch defender of clean water in his state. He created the Pure Waters Initiative, which dramatically improved water quality in New York. The success of this program even prompted Washington to launch a national Water Conservation Fund.

Beyond that, Nelson recognized that New York's transportation systems were in dire need of assistance, with many of them facing bankruptcy by 1966. So, he pushed through a $2.5-billion plan that combined highways, mass transit and airport repairs, while also playing a key role in the merger that led to the creation of the Metropolitan Transportation Authority in 1968, an institution that runs New York City's transit system to this day.

But that's not all Nelson did; many of his other reforms were also well ahead of their time. For example, 30 years before the Federal Disabilities Act was passed, he issued a mandate that required all state buildings in New York to be accessible to people with disabilities.

He also raised the minimum wage and established the Urban Development Corporation, a controversial project that let him circumvent local city zoning ordinances to develop and bankroll cultural and educational facilities.

### 13. Nelson had serious trouble working with NYC mayor John Lindsay but found ways to get along with President Richard Nixon. 

Nelson was making some highly unorthodox maneuvers within the Republican party, and his tactics were rapidly antagonizing his fellow politicians, especially the mayor of New York City, John Lindsay.

Lindsay had been elected mayor in 1966 as a brash, young politician who was dead set on doing things his way. In many ways, he was like Nelson at the beginning of his career. But the relationship between the two men got off on the wrong foot as a fierce budget battle erupted.

Following his election, Lindsay immediately asked Rockefeller to approve a $600-million increase to the city's budget and an increase in income tax. These requests were made despite the fact that NYC was already receiving one-quarter of the state's budget.

Rockefeller emerged from that battle victorious, but the pair would continue to fight about virtually everything. Lindsay considered Nelson a "schmuck" and Rockefeller thought of the mayor as "misinformed and irresponsible."

But perhaps the biggest battle between these two politicians began during the 1968 NYC Garbage Strike, through which sanitation workers, who then took home $8,000 a year, were demanding better pay.

After rejecting proposals for a $450 annual raise, Lindsay refused to negotiate above an increase of $400. As a result, Rockefeller was left to handle Lindsay's mess, all while the city's streets filled with trash.

During this time, Lindsay even refused to meet with Rockefeller. Nonetheless, when Rockefeller successfully negotiated a compromise of $425, Lindsay expressed outrage to the press over Nelson's unilateral decision and willingness to accept the union's tactics.

However, the mayor wasn't Rockefeller's only political adversary; Richard Nixon was another rival of Nelson's, although they would eventually have better luck working together.

After Rockefeller ran for the Republican presidential nomination in 1968, again losing to Nixon, the president took several people from Nelson's staff into his administration, Henry Kissinger among them. Despite this move, Nelson would work with the president, assisting him with Latin American policy and providing informative reports on social and economic policy.

### 14. During his final term, Nelson lost much of his political goodwill through a few bad moves. 

Over the course of Nelson's term in office, New York State transformed dramatically, and his response to the new cultural climate would eventually cast a dark shadow over much of his legacy.

For instance, Nelson tended to be considered too liberal for Republicans and too conservative for Democrats, but he was also a pragmatist who took in the facts of a situation and responded accordingly. Since the economy was in much worse shape in 1970 than it was in 1960, Rockefeller employed more conservative policies during his final term.

Perhaps the worst of the bunch was his anti-drug legislation, known as the "Rockefeller drug laws."

After years of failing to handle New York's rampant drug problems, Rockefeller created extreme penalties, including life sentences for traffickers of hard drugs like LSD, amphetamines and heroin. But instead of keeping people off drugs, the law sparked a massive rise in the prison population, forcing future governors to revise and roll back Nelson's legislation.

And even before this major misstep, Nelson was already facing a crisis at Attica prison in upstate New York. On September 9, 1971, a group of prisoners, protesting the "fascist concentration camps" they lived in, took control of the penitentiary.

They held the staff and guards hostage for four days while issuing a list of 30 demands, which included improved living conditions and freedom of religion. Rockefeller was convinced that he shouldn't appear on the scene, despite the pleas of his staff who believed his presence could lend credibility to their willingness to compromise.

When all was said and done, ten hostages and 29 prisoners were killed, while hundreds more were brutally beaten during a raid that eventually ended the stand-off. Nelson's reputation took a bad hit, as many blamed his inaction for the tragedy.

### 15. As vice president, a frustrated Nelson Rockefeller clashed with Gerald Ford’s staff and Republican Party leaders. 

A crisis of a different kind came right on the heels of the Attica tragedy, and one that would destroy the administration of President Richard Nixon: the Watergate scandal. In the aftermath, Gerald Ford would become the new president of the United States — and he needed a vice president.

Ford reached out to Nelson and, going against the advice of some of his closest advisors, Rockefeller accepted Ford's invitation. Many of those close to Nelson argued against this decision, since they knew he didn't have the right personality or temperament for the job. Truth be told, they were right.

Despite Ford's assurances that Nelson would oversee policy and play a key role in the administration, Rockefeller was frustrated in his position as vice president and clashed with Ford's staff. While Nelson was bogged down with Senate hearings for his confirmation, the other members of the Ford administration were solidifying their roles and clearly marking their territory.

As a result, when Nelson arrived at the White House, it was practically impossible for him to secure a powerful position.

The Chief of Staff, Donald Rumsfeld, had already firmly established himself as Ford's right-hand man and blocked most of Rockefeller's attempts to involve himself in Ford's council on domestic policy. When Nelson finally did find a role for himself, such as by chairing a group assigned to investigate illegal activities undertaken by the CIA, the results often clashed with what other Republican leaders wanted.

For instance, Nelson's report on the CIA was a 299-page dissertation that was highly critical of the agency. It revealed illegal assassination plots against foreign leaders and called for reform, as well as improved oversight.

Beyond that, Nelson also often voted against Southern Republicans, especially in the great 1975 debate over the Voting Rights Act. Because of this tension, Rockefeller came to believe that it was Donald Rumsfeld who convinced Ford to seek a new running mate for the 1976 election.

### 16. Nelson Rockefeller’s death was a strange and controversial affair. 

Nelson's rocky time as VP meant he was excluded from the Republican ticket in 1976. In that year's election, the Democrats, led by Jimmy Carter, took back the White House and Rockefeller went back to living as a private citizen for the first time since the 50s.

During this time, Nelson invested his energy in organizing the charitable Rockefeller Brothers Fund along with his siblings. Then, in 1980, as he was beginning work on a series of art books, he died under rather odd circumstances.

In the final years of his life, Nelson was working with a young woman named Megan Marshack to prepare his books for publication, and the two had also become romantically involved. The 25-year-old Marshack was with Nelson when emergency medical technicians responded to a 911 call made by a woman who said someone was dying at 13 West 54th Street, the address of one of Rockefeller's offices.

The EMTs attempted to save Nelson's life, but in the end, he was taken to the hospital and pronounced dead on January 26, 1980, at the age of 70.

Since Marshack was with Nelson when the ambulance arrived, it was believed that she had called 911, but the voice was later identified as belonging to Nelson's neighbor, Ponchitta Pierce.

The simple truth was that Nelson had had a heart attack while having sex with Marshack in her nearby apartment at 25 West 54th Street. Marshack panicked, called Pierce for help and possibly alerted Hugh Morrow, Rockefeller's longtime aide.

It's believed that Marshack spent an hour trying to get Nelson into his clothes and moved to his residence down the block, wasting time in which his life could have been saved. Despite the odd circumstances surrounding his death, a moving ceremony was held for Nelson shortly after he passed away, attended by Gerald Ford and Jimmy Carter, among other notable leaders.

### 17. Final summary 

The key message in this book:

**Nowadays, people might better remember Nelson Rockefeller for the mistakes he made later in his life. But these errors shouldn't obscure what came before them: an incredible life of towering accomplishments that left a lasting impact on the politics of New York State and the United States as a whole.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Snowball_** **by Alice Schroeder**

_The Snowball_ (2008) offers a revealing look at the life and times of one of modern America's most fascinating men: Warren Buffett. Find out how this shy and awkward man earned his first million dollars and how following a few fundamental rules enabled him to become the world's wealthiest man.
---

### Richard Norton Smith

Richard Norton Smith is a critically acclaimed biographer and frequent contributor to both PBS and ABC News. He has served as director for a number of presidential libraries, including those dedicated to Abraham Lincoln and Dwight Eisenhower.

