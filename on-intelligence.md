---
id: 533b100239613700075c0000
slug: on-intelligence-en
published_date: 2014-04-01T08:04:21.000+00:00
author: Jeff Hawkins, Sandra Blakeslee
title: On Intelligence
subtitle: How a New Understanding of the Brain Will Lead to the Creation of Truly Intelligent Machines
main_color: 2882CA
text_color: 2371B0
---

# On Intelligence

_How a New Understanding of the Brain Will Lead to the Creation of Truly Intelligent Machines_

**Jeff Hawkins, Sandra Blakeslee**

These blinks provide an overview of the human brain's capacity for thinking and for comparing new experiences to old memories. They also explain why today's machines still aren't able to emulate this capability, but why we may soon be able to build ones that can.

---
### 1. What’s in it for me? Understand why the intelligent machines of the future will be based on your brain. 

Ever since the advent of the first machine capable of doing simple computational tasks, humans have dreamed of creating artificial intelligence. Indeed, science fiction novels and movies are filled with various sentient machines, and not all of them have humanity's best interests at heart.

But what kind of intelligent machines can we really expect in the future, and how will they be made possible? And, most importantly of all, what does this mean for humanity?

In these blinks, you'll discover why World Chess Champion Garry Kasparov isn't dumber than a machine just because he was beaten by one.

You'll also come to understand the neurology behind predicting the future, for example, how you know that turning your car key will result in your engine starting.

Finally, you'll find out why intelligent machines won't ever overthrow or exterminate humanity.

### 2. Making computers more powerful will not make them more intelligent. 

In the past decades, computers have become ever smaller and more powerful.

This development has inspired some researchers to dream of a computer powerful enough to actually think like a human being. However, despite the fact that modern computers have much more raw processing power than the human brain, they are still nowhere near _intelligent_, i.e., capable of being creative and understanding, and of learning from the world around them.

That's because computers and our brains are built on fundamentally different principles.

Computers are programmed to do certain tasks, and that's the extent of their abilities. They never learn anything new, but rather just store information without the ability of using it later to understand new incoming information.

The brain, on the other hand, is not limited to pre-programmed tasks, but can understand and learn new things. That's what makes it intelligent.

For example, one famous computer, Deep Blue, beat Garry Kasparov, the world's best chess player, at his own game. But Deep Blue didn't win because it was more intelligent than Kasparov.

An expert chess player like Kasparov can look at the board in any situation and instantly judge what moves make sense for his strategy and what kind of counter-response will follow. A computer, however, can only run the numbers of every single possible move and countermove, calculating the probabilities to victory. It does not understand chess any more than a pocket calculator understands the rules of math, despite its capacity for adding up numbers.

In this sense, making processors more powerful or adding more memory capacity won't necessarily help make computers intelligent. Rather, it will only make them faster at calculating — a task computers already perform better than humans. Nevertheless, computers still won't be able to understand the world and think about the information they store the way humans do.

It thus seems that the first step in building a truly intelligent machine will be understanding the workings of the human brain.

### 3. The information from our senses is processed and stored as memory in the many layers of our brain. 

Have you ever wondered why you're capable of perceiving the world around you? Or how the sights and sounds and tastes that you sense are turned into the fluid experience of your surroundings?

This is, in fact, the result of a complicated process taking place in your brain, where the sensory information that comes in is combined with your existing memories.

The part of the brain responsible for sensory perception and conscious thought is known as the _neocortex_. When our senses tell us something, it is the neocortex that combines the incoming information with our previous memories. The neocortex is made up of multiple layers, one on top of the other, and information from the senses travels through these layers, each adding increasingly detailed prior knowledge to the raw sensory information.

Say, for example, you see a familiar face, and your eyes send this information to the brain. The lower layers of the neocortex pass this visual information on, and a higher layer connects what you see with your memory of what human faces look like. Augmented in this way, the information is passed on to a yet higher layer that then recognizes what you see as the face of, for example, your spouse or boss.

Your neocortex is so fast and efficient at complementing sensory information with prior knowledge that we are not consciously aware of it. This allows us to experience the world as fluidly and seamlessly as we do.

Of course, if you see something totally new, none of the neocortex's layers can connect it to previous memories. This means the information goes right up to the top layer, which stores the experience as a new memory for future reference.

And so, our brains have an ever-growing database of things to compare new experiences to.

### 4. Our brain uses memories of how it reacted in the past to predict future events. 

When you turn the key in the ignition of your car, what do you think will happen? Well, that's a no-brainer — the engine starts up. But how did you _know_ that? What makes us capable of predicting future events?

Basically, it's due to the way memories are connected in our brain, which consists of many regions, each of which stores different kinds of memories.

When you experience something familiar, all the memories in the various regions relating to the experience become active in a sequence, like a _pattern_.

For example, when we listen to music, one region of the brain tells us that we're hearing notes because it recognizes notes based on a former memory. Another region tells us what words we hear and yet another realizes that the words and notes are connected, forming sequences. When all this information is combined, our brain knows that we're listening to a song.

These patterns allow us to predict future events, because whenever we experience something, our brain looks for similar prior experiences. The nerve cells that were activated by the previous experiences become active again, as do those that became active shortly afterward. Thus, the brain not only knows how it reacted the last time, but also what its following reactions were, all of which serves as a basis for guessing what will happen this time around.

For example, when we see a traffic light turn green, we know that on previous occasions this was followed by cars beginning to move. We don't know for sure if the same will happen again, but based on past experience, our brain makes that prediction.

So, while we cannot actually see into the future, our brain is able to make predictions. In fact, every new experience the brain encounters helps it adapt those predictions. It's in a constant learning process.

In the next blinks, we'll find out how this understanding of the brain can be used to make truly intelligent computers.

### 5. Neural networks won’t be able to imitate the brain because they lack the brain’s complexity. 

Scientists know now that they can't create truly intelligent machines by building traditional computers — the so-called _Artificial Intelligence_ (AI) approach. Once they realized this, they started to explore another option: developing _neural networks_, i.e., machines based on the model of the human brain, where information travels along pathways of artificial neurons.

Unlike in a computer, there is no central storage unit in a brain: memory and knowledge are stored throughout the brain as a vast network of neurons. This is the architecture neural networks seek to emulate.

As an example of how this works, say you input the letter "a" followed by the letter "n" into a neuron in a neural network. That neuron is connected to many others, and the input causes it to send signals to some other neurons. This starts a pulse-like wave that goes through the entire network of neurons, as each neuron sends signals onward selectively. The strength of each signal varies, depending on how sensitive each neuron is to the combination of "a" and "n," with the most sensitive ones triggering the strongest signals, which eventually wind up as output from the network. And which neurons are most sensitive to "a" and "n"? Probably the ones related to the word "an," so the output would be this word.

As of today, though, these artificial neural networks are still too simple to mimic the sophistication of the human brain.

For example, one limitation is that information can only flow in one direction in neural networks. In the brain, by contrast, there are also feedback loops through which the higher, more sophisticated regions of the brain can affect the flow of signals. That's what allows us to, for example, think hard about where we've seen something before and then remember it.

Another limitation is that neural networks can't build up a memory bank, so they don't know what inputs they have received before. This means they can't actually learn things and use them later.

Next, we'll find out how scientists are trying to improve neural networks to build truly intelligent machines.

### 6. Scientists will probably be able to build intelligent machines in the near future. 

So how far away are we from designing intelligent machines? To be honest, pretty far — but not quite as far as you might think!

The first obstacle to overcome is providing the machines with at least the same memory capacity as the human brain. In order to simulate the brain's synapses, the machine would require roughly eight _trillion_ bytes of memory. A computer today has around 100 billion bytes, so this would need to be increased eightyfold.

Unlike some years ago, this seems like a feasible target today. Such computers can, in fact, be built in labs already, but researchers also face another challenge: the machines must be small enough to be of practical use.

This, too, seems feasible today thanks to silicon chips, which are small, robust and consume very little power. In fact, in the not-too-distant future, there will probably be silicon chips that far exceed the memory of the human brain.

However, there is yet another obstacle: nerve cells in the brain are connected to thousands of other cells, and to truly emulate the human brain, the memory of the artificial intelligence must be similarly connected. This is a tough problem to solve and, currently, scientists do not know how the bytes in a silicon chip could be connected in this way.

One potential solution is on the horizon: _single fiber optic cables._ These are cables that can transmit huge amounts of data very quickly, developed by telecommunications companies to be able transmit over a million concurrent conversations over a single cable.

If scientists keep working on this technology, the dream of building intelligent machines seems realizable after all, as there is promise of overcoming each technological challenge.

### 7. Intelligent machines are no threat to humanity; on the contrary, they will provide numerous benefits. 

Anyone familiar with science fiction novels or movies probably knows this scenario: machines attain self-awareness and turn against humanity. Happily, such horrors can only exist in fiction, and the intelligent machines of the future have little in common with such imaginary killer robots.

In fact, the worry that intelligent machines might feel their "slave" status under humans is unfair and so revolt in order to exterminate humanity is based on a false analogy. Since their intelligence would be based on the brain's neocortex, intelligent machines will not automatically become capable of emotions and feelings. Human emotions, such as fear, desire, love and hate, aren't generated there, but in an older, more primitive part of the brain. As long as the latter part of the brain is not emulated by the machines, they will not be human-like in this respect, but useful, unemotional tools.

Instead of hurting humanity, intelligent machines will provide countless benefits, many of them not even imaginable yet.

An intelligent machine could have a memory far exceeding that of the human brain, and since it would never die, it would eventually accumulate more knowledge than any individual human. This would enable it to generate ideas mere mortals would never think of.

For example, intelligent machines could fundamentally change the way weather is predicted. Using weather sensors across the globe, an intelligent machine could collect global data and gain a fantastically detailed understanding of weather patterns. This is impossible for humans due to the complexity of weather systems, but the machine could "understand" weather as we understand language, and could then spot patterns hidden to us humans.

It seems clear that once intelligent machines are created, they will quickly surpass humans in the ability to think and process knowledge. But it's nothing to be afraid of, as it will provide an unimaginable wealth of benefits to mankind.

### 8. Final summary 

The key message in this book:

**Thus far, computers are nowhere near as intelligent as humans: they can't emulate the way we think, learn or use past knowledge to predict future events. To be able to do so, they would need to be built in the form of a neural network, similar to the human neocortex. Happily, the technological obstacles to doing so are being overcome.**

What questions were answered in this book?

**Why have we not been able to build intelligent machines so far?**

The human brain is a incredibly complex network of neurons which cannot be emulated by traditional computers, which are not capable of understanding the world around them or learning new things.

**How can our brains predict future events based on our past experiences?**

All the sensory information your brain receives activates memories throughout the various regions of the brain in a sequence. When you encounter similar sensory information again, your brain recognizes the pattern and remembers what areas were activated the previous times it encountered this pattern, thus it can guess what will happen next.

**Can and should we build intelligent machines?**

The technological obstacles needed to build neural networks that would emulate to workings of the human neocortex can potentially be overcome in the near future. Such a development would pose no threat to humanity, but would rather bring untold benefits. 

Actionable advice:

**Thought experiment.**

Here's a little thought experiment: imagine every computer in your home were several times more intelligent than you. How would you use their abilities? What kind of tasks would you have them perform? How would you spend your days?

**The brain is a very special organ, so don't do anything that might damage it.**

It has taken millions of years of evolution for it to become as complex and sophisticated as it is, and you should never forget this. Some people seem to, damaging their brains by, for instance, taking drugs and drinking to excess. This is a tremendous waste of such a sophisticated organ; you should strive to keep your beautiful brain functioning at its highest capacity.
---

### Jeff Hawkins, Sandra Blakeslee

Jeff Hawkins is the co-founder of the companies Palm and Handspring. After inventing the PalmPilot and the Treo smartphone, he began working for the Redwood Neuroscience Institute, a non-profit organization. It was there that he developed some of the theories presented in these blinks.

Sandra Blakeslee writes for the _New York Times_ as a science correspondent. She is the co-author of several books such as _Phantoms in the Brain_.

