---
id: 543bcf4536623900085f0000
slug: on-liberty-en
published_date: 2014-10-14T00:00:00.000+00:00
author: John Stuart Mill
title: On Liberty
subtitle: None
main_color: 9DC3C6
text_color: 4D5F61
---

# On Liberty

_None_

**John Stuart Mill**

The iconic political text _On Liberty_ delves into questions of how to balance authority, society and individuality. Combining abstract philosophical reasoning and concrete examples, _On Liberty_ provides a thoughtful and vivid defense of personal liberty and self-determination that has made a huge impact on our liberal societies and political thought today.

---
### 1. What’s in it for me? Learn about liberty from one of the most important political texts in history. 

Living in nineteenth century England, John Stuart Mill foresaw that the upcoming centuries would exhibit an ongoing power struggle between society and individual freedoms. In this seminal text on tyranny and personal liberty, Mill discusses the nature and limits of the power which can be legitimately exercised by society over the individual.

He lays the foundation of liberal political thought and our contemporary understanding of freedom of religion and speech — topics that continue to raise heated controversy. His insights are valuable, not only to those who wish to create fair and just governments, but also to those who want to rationally evaluate their own behavior and their own assumptions about the world around them.

In these blinks, you will learn

  * when society has the right to punish you for a night of heavy drinking,

  * why democracy alone can't ensure your personal liberty and

  * why you have the right to save someone from an accidental death.

### 2. Democracy alone is no guarantee of personal liberty. 

Throughout the course of history, democratic self-government was developed in order to end tyrannical rule. From ancient Greece to England, nearly every society has struggled with its share of tyrants. In fact, in the somewhat distant past, tyrannical and strong leaders were even viewed as an important tool for ensuring societal security and order.

Gradually however, it became obvious that these leaders, who had either inherited or conquered their lands, often acted against the interests of their subjects. More and more people began realizing that the power of political authorities should have limits. The democratic process is designed to achieve this check on political powers, with the key component being the people electing their officials.

Yet, democracy didn't solve everything. Democratically elected officials can threaten personal liberty, just as tyrants do. Those who fought for democracy thought that, once the interests of political rulers and the people are identical, there would be no danger of tyranny. Not so!

Even when the government is elected democratically and acts responsibly, there is still a need to limit the power of society and government; democracy doesn't equate to self-governance, but governance over every individual by the majority.

This majority can easily become tyrannical and therefore threaten personal liberty via _social tyranny_, i.e., the imposition of beliefs and ideals onto those who hold different views. Imagine, for example, that the majority adhered to a single religion. By using the democratic rule, the majority could use the force of the government to impose this belief on religious minorities.

Clearly, democracy alone cannot guarantee personal liberty. But there are some steps we can take to avoid the dangers of social tyranny.

### 3. Only a society that is based on rational principles can guarantee personal liberty. 

If democratic societies want to protect liberty, then they will have to examine various aspects of their culture that cannot be understood without thinking about historical processes and human behavior.

The question of personal liberty must be addressed from a rational point of view. Yet, the rules and laws of a society as well as public opinion are influenced mainly by the likes and dislikes of a given society. In other words, they are entirely irrational. What's more, all people in that society almost automatically assume that their customs are right and good.

Consider, for example, how people follow the beliefs and customs of a particular religion without ever asking themselves _why_ they do so. If a Muslim, for instance, who does not eat pork, were raised in a Christian society, she would probably have no qualms at all about eating pork, and would instead forgo all types of meat during Lent.

As a result, modern civilizations have made little progress regarding the question to what extent social control is justified, since the issue of personal liberty has never been considered from a rational perspective.

The sole exception to this lack of progress is, strangely enough, the existence of religious tolerance in modern societies. After centuries of conflict, religious groups on the European continent have simply had to accept that religious tolerance is necessary for stability, and that the individual should determine which religion he wants to follow.

But religious tolerance was born out of necessity, not rational principle. As long as there are no clear principles that define when governments may legitimately exercise power over individuals, and we instead rely on our irrational feelings and the convictions of our social environment, there is no guarantee that our personal liberty will be protected.

Therefore, we must identify a rational, more objective principle. But what does such a principle look like?

> _"It is not because men's desires are strong that they act ill; it is because their consciences are weak."_

### 4. Personal liberty is as useful for societies as it is for individuals. 

What if someone you know is unconvinced that the powers of society and the government should have limits? How could you respond? What reasons could you provide to convince him otherwise?

Some philosophers such as John Locke have argued that ethical standards follow from natural or inborn rights. But, as we've seen in the previous blinks, what we define as "natural" is very much subjective, based on the cultural whims of the societies in which we participate.

In order to discuss the question rationally, we have to approach the concept of an ideal society based on the concept of _utility_ : how beneficial is the law or other aspect of governmental rule to the well-being of humankind?

In fact, only a society that respects personal liberties based on utility will flourish. Without the freedom to make their own decisions as individuals, people will be unable to develop their own mental and moral capabilities, which will hurt their character and well-being.

However, personal liberty isn't just important for the individual; it's also important for society as a whole. Only in a liberal society are people free to develop their own skills and ideas and realize their individuality. The diversity of individuality empowered by personal liberty has the potential to result in an environment in which people learn from each other and combine their strengths, thus helping all of humanity to further progress.

Indeed, history has shown that societies that value diversity over conformity have a greater shot at flourishing in this world.

But should there be any limits on personal freedom? Should we really be free to do _whatever_ we want? The following blinks will explore these questions in more depth.

> _**"** A state which dwarfs its men...will find that with small men no great thing can really be accomplished."_

### 5. People should have the liberty to do what they desire. 

No one else is responsible for an individual's well-being other than the individual herself. As human beings, we have the capacity to come to our own judgments through careful reflection; people must decide on their own what to think and do, and no one should be allowed to exert their will on someone else's liberty.

One exception, however, is children. While adults have the mental capacity to make sound decisions, children, on the other hand, lack the maturity to do so. As a result, it's acceptable for adults to interfere with children's liberty, in their own best interest.

The same is true for societies that are still less developed, and are therefore not ready for personal liberty. In this case, political authorities may exercise authoritarian rule in the interests of their people. However, in advanced societies, individuals can be trusted to know best what they desire and what is good for them. Here, neither society nor the government may interfere.

Furthermore, society must not impose its beliefs and way of life onto all individuals. 

Unfortunately, members of a given society sometimes hold the view that it would be better for an individual's health or morality if they followed the particular religious ideas and moral norms of that society. These people often even feel offended by the way others live and act, and might even want to prevent them from living this way.

For example, because Islam prohibits eating pork, some Muslims believe that _nobody_ should be allowed to eat pork. Or consider the seventeenth century English Puritans, who wanted to forbid both public _and_ private entertainment.

However, holding these views does not give you the right to impose them on others. Individuals are well within their rights to attempt to convince others to adopt their beliefs, but society may not impose these beliefs with the force of law.

So, in principle, members of modern societies have the liberty to do what they want. This liberty, however, has some limits.

### 6. Interfering with an individual’s liberty is only justified when it is for the purpose of preventing harm to that person or others. 

There are some strong cases for limiting an individual's freedom in order to prevent harm. For example, society has the right to stop those who get drunk and then act violently. Drunkenness alone is no reason to revoke someone's liberties. But we may certainly revoke the liberties of those who have _already_ acted violently under the influence of alcohol and get drunk again, for example, by imposing fines or jailing them. Because they are prone to become violent when they are intoxicated, getting drunk in _their_ case is a crime against others. 

However, sometimes _not_ doing things can harm the people around us, too. Consequently, society is justified to force individuals to do things that they might not want to do, such as make their fair contribution to finance infrastructure projects, like hospitals or roads.

In addition, interference in personal liberty can be justified to save others from accidents. Although no one has the right to interfere with the liberty of individuals if no one is harmed, sometimes people are genuinely ignorant of the harm their actions might cause to others or themselves.

Suppose, for example, that someone is about to cross a bridge without knowing that this bridge is structurally unsound. Suppose too that a police officer is around, but there isn't enough time to warn the person of the dangers he faces.

In this case, the officer may pull the person back, thereby restricting his freedom of movement in order to save him, because we can safely assume that it was probably not the person's intention to injure himself.

> _"The only freedom...is that of pursuing our own good in our own way, so long as we do not attempt to deprive others of theirs."_

### 7. Society must be cautious about what qualifies as harm and how to prevent it. 

You might feel hurt when someone takes a parking spot you wanted, but is this the kind of harm we're talking about? Would it justify interference from society or the government? Probably not. So when does harm justify interference with someone's liberty?

Not everything that might have negative consequences for others actually justifies interference. In fact, interference is only justified when somebody harms others by violating an obligation he has towards someone else or society in general. For example, although people are generally allowed to get drunk, it is clearly forbidden for on-duty police officers as they are responsible for the security of society.

Similarly, people have obligations towards their families. Although it is not society's job to stop people from drinking or gambling, when it leads to a situation in which people no longer care for their families, these vices can be considered punishable offenses.

Clearly, preventing harm is a complex issue. Some might suggest, for example, that, because certain things will necessarily cause damage to individuals and societies, we have an obligation to prevent that harm. For example, some shops sell poisons that could cause harm or death under the wrong circumstances.

However, because this is only potential harm, interference in our liberties isn't justified. Not only are arguments in favor of preventing potential harm easily misused, but allowing these liberties can lead to social benefits. For instance, shop owners who sell poisons can employ more people as long as they can sell those products, and those employees in turn use that money to stimulate commerce.

If society is truly afraid of potential harm, such as poison abuse, then it can take other measures to prevent accidents, like educating people on poisons or requiring purchasers of poison to submit personal data.

As you can see, what qualifies as harm and what measures should be taken against it aren't as simple as they might first appear.

So far, we've focused mostly on the freedom to carry out actions. The following blinks focus on the importance of our more abstract freedoms: the freedom of ideas.

### 8. Limiting freedom of thought and speech is against crime to humankind, as it obscures our search for truth. 

Nowadays, there seems to be no question that freedom of thought and speech are protected and respected. However, many people have probably never thought deeply about why these liberties are so important to begin with.

As we've seen, when it comes to their beliefs and convictions, people too easily follow the opinions of their social groups and accept the words of institutions, such as the church or political parties, as absolute truth.

However, neither we, nor the authorities to whom we defer our judgments, are infallible. In order to uncover truth, everybody must have the freedom to think freely and to express her opinion. Suppressing others' freedom of thought and speech will inevitably suspend the discovery of truth, and therefore harms humankind. 

History offers many good examples of the suppression of ideas because they did not conform to popular societal beliefs. Even Socrates, one of the most important philosophers of all time, to whom humankind owes so many insights, was killed because people believed his ideas were dangerous.

But it's not only "true" opinions that need protection. Even if they aren't 100 percent accurate, contrary opinions might contain enough truth to still be valuable to society. Sometimes, opinions can add to the public discourse precisely because they are only partially true, and discussing them further helps us get to the truth underneath.

For example, we now know that Newton's theories were not completely correct. However, if society had barred him from expressing his thoughts because his theories contradicted common knowledge, we would have lost many important insights that led to major scientific progress! 

But what if someone's opinion is clearly wrong? Should he be granted the same liberties?

### 9. Allowing false opinions is also useful for societies. 

Most of us probably think that some opinions are just flat out wrong, and that people who hold those opinions would be better to just keep their mouths shut. This is especially true for hot-button issues, such as abortion or gender equality. But would this actually be good for society? In short: no.

Incorrect opinions are valuable in that they force society to think about common, entrenched beliefs and reflect on why their opinions are correct.

If a society wishes to commit itself to the pursuit of truth, then it is necessary that people be confronted with contrary opinions, otherwise they just blindly follow the beliefs of an authority or of the majority.

To come back to the example of gender equality: it isn't enough to accept gender equality simply because laws say so. We must also understand _why_ equal rights are justified and why they are important. In this sense, whenever we are confronted by people who reject the notion of equal rights across the gender spectrum, we are forced to reflect on why our beliefs are justified and why great moments in women's rights are such achievements.

Furthermore, if our common convictions are not continuously questioned, they will eventually be formally accepted and become ineffective. If we want to ensure that our most treasured convictions don't lose their power to affect our character, then it is necessary for us to be continuously confronted with contrary opinions. Otherwise our various ethical and moral convictions become reduced to mere custom and we forget the underlying reasons.

Freedom of thought and speech are fundamental liberties that must be respected and protected, no matter how questionable the thought or speech may seem sometimes.

### 10. Final summary 

The key message in this book:

**The whims of culture and society aren't objective enough to make democratic governance a strong protector of our personal liberties. Instead, societies must use rational principles in deciding which kinds of behaviors should be tolerated, in order for the individual and society to flourish.**

**Suggested** **further** **reading:** ** _Breakfast with Socrates_** **by Robert Rowland Smith**

_Breakfast with Socrates_ whips you through a normal day with commentary from history's most venerated thinkers, explaining exactly how their major contributions to philosophy, psychology, sociology and theology impact your daily routine: wake up with Descartes, brace yourself for a world of Freudian conflict, and when you go to work, either submit to Marx's wage slavery or embrace Weber's work ethic. Argue with French feminists and then slip into a warm bath, bubbling in Buddha's heightened consciousness. Finally, end the day by drifting away into Jung's collective unconscious.
---

### John Stuart Mill

John Stuart Mill, a philosopher and economist, is among the most influential liberal political theorists and moral philosophers in the history of Western thought. In addition to his groundbreaking work _On Liberty_, Mill is also famous for his book _Utilitarianism_, a foundational text on utilitarian philosophy.

