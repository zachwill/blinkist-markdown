---
id: 56de96cf3d0f8d0007000000
slug: on-paper-en
published_date: 2016-03-08T00:00:00.000+00:00
author: Nicholas A. Basbanes
title: On Paper
subtitle: The Everything of its Two-Thousand-Year History
main_color: C5DFCA
text_color: 535E55
---

# On Paper

_The Everything of its Two-Thousand-Year History_

**Nicholas A. Basbanes**

Paper: we use it so much we don't realize how fundamental it is to our society. We don't just record our thoughts on it, we base our currency on it, use it for entertainment and employ it for hygiene. These blinks of _On Paper_ (2013) outline the history of this simple but amazing tool.

---
### 1. What’s in it for me? Learn why paper is here to stay. 

Some people say we live in a paperless society or that we're on the verge of one. It's certainly true that a lot of the things we used paper for in the past can now be done without it. Just look at emails.

But, as you'll see in these blinks, paper is no dinosaur on its way to extinction. Its long history and many uses show the unique qualities of paper and why it's here to stay.

In these blinks, you'll learn

  * that paper can be deadly;

  * the life expectancy of a one dollar bill; and

  * the origin of the art of origami.

### 2. Paper, a dried composite of water and cellulose, was developed in China. 

We rarely pause to remember that paper once revolutionized the world. But where does paper come from?

Most historians consider paper to have been developed in China in AD 105 by Cai Lun, a man who created tools and weapons for the royal court of Emperor Ho Ti. There is some dispute, however. Historian Tsien Tsuen-Hsuin argues that fragments found in 1957 in an ancient Chinese tomb are the oldest known pieces of paper — they date back to 140 BC. These specimens, along with others found in different locations in the region, suggest that paper changed over time. 

So what exactly is paper? Paper is made from a mixture of water and bits of pulverized cellulose that are sieved and dried. The cellulose can be made from tree bark, old fishing nets, cloth, hemp from rope, cooked straw, boiled banana peels, crushed walnut shells or a number of other materials. In short, paper is made using a screen mold by combining water with any material containing cellulose fiber.

The process whereby cellulose fibers attach to each other through molecular cohesion is called _hydrogen bonding_. This bonding is what makes paper different from _papyrus_, though the word "paper" comes from papyrus. 

Papyrus is made from dried pieces of stalk from the papyrus plant, a marsh reed. These bits of stalk can be attached to one another because of chemicals released by the plant.

Paper arrived in China at just the right time. Alternatives to paper, like silk and stone, were inconvenient because they were either too expensive or too cumbersome.

### 3. The Japanese developed new uses for paper in a number of fields, including architecture and warfare. 

Paper traveled out of China with Buddhist monks going two directions: west through Central Asia along the Silk Road, or east to Korea, and then on to Japan.

In Japan, paper took on a new life once it became established in the seventeenth century. Though originally a luxury item only available to the nobility or military samurai elite, by the nineteenth century, paper had become more widespread in Japan than anywhere else in the world. 

Paper was flexible and cheap to produce, so it could be used to make a variety of items, like handkerchiefs, lanterns, dolls, fans, kites and kimonos. It was even used to make armor. Funeral effigies were made from paper, and people started using it for personal hygiene too, despite the fact that the first disposable toilet paper was actually discovered in China.

The introduction of paper even changed Japanese architecture, as it could be used with wood, earth and reeds to build houses. _Shoji_ screens, for instance, made from ultra-thin layers of paper, served as windows. 

In the Second World War, paper was also used in aerial warfare. The Japanese military developed paper balloons 32 feet in diameter to carry bombs across the Pacific to the United States. Between 3 November 1944 and 5 April 1945, they launched 9,000 balloons, 1,000 of which made it to North America. 

The balloons only ended up killing six people, so they weren't a very effective weapon. A plaque in Oregon commemorates their victims, calling the site "the only place on the American continent where death resulted from enemy action during World War II."

### 4. Paper spread from the Muslim world to Europe, then from Europe to the Americas and Australia. 

Paper evolved further after the Arab world inherited the process from the Chinese about a century after the death of the prophet Muhammad. It was initially only used to record the word of God in the Koran, but people soon found other uses for it.

Muslim empires brought paper to present-day Iraq, Syria, Egypt, Spain and North Africa, establishing it as a key component of Muslim life. From Spain, it then spread to the rest of Europe, North America and Australia. 

The Ottoman Empire was the first political entity to use paper to manage its bureaucracy. The Ottomans preferred paper because it was more permanent than parchment. Ink permeates the paper and can't be scratched off — a drawback of parchment — so it was very useful for recording things like tax information. 

Europeans later dominated the global paper production market after the introduction of printing presses in the 1450s, and the Ottomans fell behind.

Jonathan Bloom, an Islamic and Asian art expert at Boston College, suggests that the Muslim world may have been less interested in developing a printing process because it considered writing to be sacred. Ottoman Sultans Bayezid II and Selim even issued edicts in the fifteenth century that banned printing in Arabic or Turkish. The edicts held for 300 years!

In 1719, a physicist and naturalist named René-Antoine Ferchault de Réaumur did a study of nesting behavior in insects and found that they made a kind of paper from ground wood pulp. With this discovery, paper could suddenly be produced in much greater quantities than ever before.

> _"By the production of a cheap writing material, and its supply to markets both east and west, the Arabs made learning accessible to all."–_ Alfred von Kremer

### 5. Paper is still used as currency because it is versatile, resilient, and deters counterfeiting. 

Societies developed paper currencies as paper manufacturing became more widespread and people started to realize its many uses.

US firm Crane and Company, for example, produced paper money for the Treasury Department (exclusively, as a matter of fact, since 1879), a line of 100 percent cotton stationery for the British royal family and brands like Tiffany and Cartier. 

It was Zenas Marshall Crane of the second generation of the family firm who was the main reason for the company's continued success. In 1844, he created a unique way of threading silk strands through the rag paper to make the bills harder to counterfeit. Every one dollar bill contained one thread, a two dollar bill two, and so on. This technique gave the company and their currency the edge. 

American banknotes are almost entirely made of rag paper, making it the most resilient folding currency in the world. According to the Federal Reserve, the life expectancy of a one dollar bill is 41 months, and it can be bent back and forth 8,000 times before it'll tear!

A British five-pound note, on the other hand, has a more intricate design but lasts less than twelve months.

### 6. Americans began using wood in papermaking due to increased demand for paper. 

Americans initially preferred the European rag-based method for all paper-making, but a shortage of material coincided with an explosion of uses for paper. There was such desperate need for rags that entrepreneurs imported linen wrappings from mummies exhumed in Egypt!

The demand for paper grew still more at the end of the seventeenth century. During the Revolutionary War and subsequent Civil War, paper was even used to make cartridges for rifles. Furthermore, hundreds of newspapers began to be published. On 2 July 1863, the _Vicksburg Daily Citizen_ was even printed on the blank side of a piece of wallpaper!

Once paper started being mass produced, people began to experiment with making it out of straw or vegetable material. Paper mills eventually turned to wood, due to its easy availability. 

Though ground wood was known to have potential in papermaking since the early eighteenth century, it was Friedrich Gottlob Keller who patented the first viable plan for using it in mass production in 1845. Keller's method soon became the main form of paper-making.

It's very simple to make paper from wood pulp: logs of equal length are cut, the bark is removed, the wood is ground underwater and more water is added to the shreds to produce a pulp.

It's a durable, quick and easy process but it also produces lignin, a tough, fibrous polymer that makes the paper fragile and yellow. However, in 1882, when nearly all American newspapers used lignin-based paper, chemists started developing processes to remove the lignin and preserve longer fibers. American papermakers would soon gain control of the global paper market.

### 7. Paper is vital for personal hygiene. 

Some people speculate that paper will die out, but that's because they haven't considered how important it is in personal hygiene.

Nobody questions the utility of toilet paper — arguably the most indispensable use of paper in the modern world.

But toilet paper is just one example of paper being used for personal hygiene. Kimberly-Clark opened a specialist tissue and towel manufacturing company and soon began producing single-use, paper-based health and hygiene products, like Kleenex tissues and Kotex sanitary napkins.

When the company formed in 1872, they initially focused on producing paper for newspapers. They weren't a major player in the industry, however, so they began experimenting with ways to expand. In 1914, they hired a chemist named Ernst Mahler.

Mahler used a kind of bleach to remove lignin from paper more effectively. With this innovation, multiple colors could be printed on the paper without bleeding together. Major newspapers like the _New York Times_ started using it in their magazine supplements. 

Mahler also invented a new kind of paper called _Cellucotton,_ which could be used to make surgical dressing and filters for gas masks in the First World War. 

After the war, Kimberly-Clark realized they could use Cellucotton to make feminine hygiene products, after receiving letters from army nurses who had used it as such during the war. So Kimberly-Clark started producing disposable sanitary pads, which women could use instead of cloth napkins. 

In 1924, Kimberly-Clark found another use for Cellucotton. They catered to the rapidly growing cosmetics industry by making Cellucotton into absorbent facial tissues. Before that, women mostly used cotton balls to remove their makeup. 

And in 1929, Kimberly-Clark developed the pop-up tissue box we still use today.

### 8. Paper is unlikely to die out, largely because of bureaucracy and cigarettes. 

Electronic documents become more pervasive every day but hard copies are still an important part of our bureaucracy. We still prefer them for important documents like birth certificates and deeds. 

In many cases, paper is still more reliable than electronic storage, especially as the threat of security breaches increases as we store more and more information online. Even emails are printed and archived in the United States National Archives, to ensure they'll always be available.

The mass production of paper spurred the growth of the most important personal identification document to arise in the past five centuries: the passport. Paper is also indispensable for spy work: think false identification cards, paper that dissolves quickly in water, _flashpaper_ that can be burned instantly and doesn't leave a trace, or propaganda leaflets dropped from aircrafts in wartime.

But there's another reason paper isn't likely to go away anytime soon: cigarettes!

Some say paper cigarettes were first used in Spain by vagabonds who made them by rolling leftover cigar tobacco in scrap paper. Others say they originated in the 1854 Crimean War among French soldiers who substituted pipes with paper torn from gunpowder bags.

What is clear, however, is that the Crimean War, which pitted England, France, Turkey and Sardinia against Russia, spread paper cigarettes to the world. 

Men who fought in the war went home with paper cigarettes, which were initially viewed as feminine, as they lacked the panache of cigars. However, they were cheap and convenient, so they soon became popular. It was easy to produce them too, thanks to the steady supply of inexpensive paper.

> _"The emperors of today have drawn conclusions from this simple truth: whatever does not exist on paper, does not exist at all."_ — Czeslaw Milosz

### 9. Paper’s value can increase over time and it can be used to produce other work. 

Even if it's often disposable, paper is far from worthless. Even newsprint, scrap paper and highly classified documents can be repurposed into recycled material like cardboard packaging. 

Some paper products become valuable collectibles over time, like historical archival documents or stamps. One rare stamp that was printed in Mauritius in 1847 sold for four million dollars in 1993. 

Old notebooks kept by artists are also highly valuable, as they offer insight into their creative process. The notebooks and paper themselves even influence the creative process directly, as they affect the way the artist writes or draws.

In fact, Martin Kemp of Oxford University believes that Leonardo da Vinci's productivity wouldn't have been possible without the paper he sketched his ideas on. In earlier times, parchment was scarce and couldn't be used so liberally.

And of course, da Vinci wasn't unique in this respect. Great works of architecture like St. Paul's Cathedral in London probably could never have been accomplished without paper. 

In the times before architectural plans could be mapped out and recorded in detail prior to work beginning, buildings took many years to construct. Builders struggled to keep their work standardized; they often had to improvise and modify what they were doing. It could be very hit or miss. 

Alexander the Great allegedly used grain to outline building plans in Alexandria in 334 BC, but a flock of birds came and ate them! Paper is important for architecture even in today's electronic world. Architects and engineers still print building plans so each builder can carry their own copy.

### 10. Paper isn’t only important because of its utility – it’s beautiful, versatile, and always being reinvented. 

People tend to assume that paper is just a medium: a tool that allows you to do something else. In some cases, however, paper is something special on its own. Intricately crafted books, origami and paper decorations like kites, lamps, fans, pop-up books and even religious ornaments illustrate how versatile paper can be.

Origami is the prime example of this. Origami is the art of creating intricate figures made of a single piece of paper that's not cut, taped or glued.

And origami paper doesn't have to be very high quality, unless you're a master of the craft. The important thing is that the paper is folded and creased properly, creating a beautiful but sturdy structure. 

The art of origami rests on the fact that paper can be folded. That's a very simple property, but it's important for a lot of paper's other uses too, like currency and communication.

Paper companies remain innovative with their development and production. P. H. Glatfelter Inc., for instance, rarely uses traditional methods to produce their paper tea bags. Instead, they make paper from unusual substances like the plant abaca, which is strong enough to withstand boiling water without coming apart. The company also produces playing cards and the disposable soufflé cups used for ketchup and mayonnaise.

H. Glatfelter's ingenuity shows that paper is here to stay. We can always find new uses for it, though its original uses still also remain important. We still turn to paper when our technology fails during emergencies like 9/11. Paper is a fundamental part of our lives.

> _"The paperless society is about as plausible as the paperless bathroom."_ — Jesse Shera

### 11. Final summary 

The key message in this book:

**Since its development thousands of years ago, paper has changed human life. It allowed us to pass on information, changed our relationship with money, improved our hygiene and let our cities flourish. Paper is so versatile there are art forms based on it alone. Digital technology is advancing at exponential rates, but paper is here to stay.**

Actionable advice:

**Get a hard copy.**

Keep a paper copy of any important document you have. It's more reliable than anything digital.

**Suggested further reading:** ** _Gutenberg the Geek_** **by Jeff Jarvis**

_Gutenberg the Geek_ (2012) examines the life and business of Johannes Gutenberg, inventor of the printing press, and, by drawing numerous parallels between him and modern Silicon Valley entrepreneurs, explains how he was a pioneer of tech entrepreneurship.
---

### Nicholas A. Basbanes

Nicholas Basbanes is a former literary editor of the _Worcester Sunday Telegram_. He's written several books, including _A Gentle Madness: Bibliophiles, Bibliomanes, and the Eternal Passion for Books,_ and his work has been featured in the _New York Times_ and the _Los Angeles Times_.

