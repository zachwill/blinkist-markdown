---
id: 589500ff0be3560004037188
slug: on-saudi-arabia-en
published_date: 2017-02-07T00:00:00.000+00:00
author: Karen Elliott House
title: On Saudi Arabia
subtitle: Its People, Past, Religion, Fault Lines – and Future
main_color: 559F52
text_color: 396B37
---

# On Saudi Arabia

_Its People, Past, Religion, Fault Lines – and Future_

**Karen Elliott House**

_On Saudi Arabia_ (2012) gives a fascinating overview of a country rife with contradictions. Despite being immensely wealthy, Saudi Arabia is filled with people who live in abject poverty. And although on its way to being counted among the world's most powerful countries, it has an education system that's received execrable rankings. Add to this a liberal dose of religious fanaticism and a complex royal family and you'll begin to see why Saudi Arabia has struggled to come to terms with itself.

---
### 1. What’s in it for me? Go behind the scenes of Saudi Arabia, one of the world’s most shut-off nations. 

Saudi Arabia is an absolute monarchy — one of the few that still remains. What makes this kingdom so special, though, is its unique history. After World War I, the Ottoman Empire was no longer around to keep the peace in the Arabian Peninsula and, in 1932, after a lengthy military conflict between local tribal leaders, a new Saudi Arabian state was founded. It's leader was King Ibn Saud.

The kingdom fared better than some of its neighbors. In 1938, large quantities of oil — which is still the bedrock of the economy — were discovered. Much of the population, however, lived in utter poverty — and still does. As non-religious tourism is still extremely limited, the following blinks will give you a unique look at the region that gave birth to Islam.

In these blinks, you'll learn

  * how women fit into the male-dominated Saudi society;

  * what role social media is beginning to play in youth activism; and

  * why immense oil wealth isn't solving Saudi economic problems.

### 2. Saudi Arabia is one of the last nations to have an absolute monarchy – in part because of religious influences. 

If asked to think of royalty in a Western nation, you'd probably think of Britain. The British royal family has fascinated people for many generations — but no one has ever considered letting them _run_ the nation's government. Yet this is exactly how it works in Saudi Arabia.

Saudi Arabia's king has practically unlimited power: He nominates the top religious leaders, appoints all the judges and chooses who makes up the 150 members of the country's powerless parliament. Furthermore, throughout the nation, the various regions are ruled by princes with blood ties to the king.

To ensure a pristine public image, the king has put his relatives in charge of Saudi Arabia's media outlets. And he also pays close attention to social and civic organizations to make sure they are discouraging people from organizing any political protests that might threaten the monarchy.

This power is backed by the royal family's vast wealth, which derives from the primary driver of the nation's economy: petrol oil.

However, the king's power over his people has more to do with religious influence than economic coercion.

It wasn't until the nineteenth century that Saudi Arabia settled down to having one absolute ruler. Before then, rival factions of nomadic Bedouin tribes ruled the land — recognizing no single absolute leader.

However, following a thirty-year military campaign, King Ibn Saud defeated the rival Arabian houses in 1932, conquering what would eventually become known as Saudi Arabia.

At the top of the new king's to-do list was making sure that the Bedouin tribes remained loyal to his leadership. For this to happen, he needed to change the country's social structure.

He used Wahhabism, a puritanical form of Islam, to convince the nomadic tribes to form settlements and communities known as _umma_, where people share one belief and obey the Qur'an without question.

Since the royal family, and Ibn Saud, were considered to be Allah's representatives on earth, this arrangement would guarantee that the newly formed monarchy would be followed with unquestioning loyalty.

### 3. Saudi Arabia remains religious, but there is disagreement between its leaders on how religion should be followed. 

Whether you grew up in a secular or slightly religious culture, you're probably familiar with a prayer or two for a certain holiday. Maybe you say grace with your family before eating dinner. In the West, it's common for religious commitment to be casual. This casualness becomes especially pronounced when compared with the centrality of religion in Saudi life.

In Saudi Arabia, everyone is well acquainted with _salat_, the Muslim prayers that people are obligated to perform five times a day.

These are quite different from the prayers of Western religions, which are often focused on giving thanks and praise. Muslim prayers are a very formal ritual that ends with prostrations toward Mecca — a submissive gesture to Allah's will.

And there is only one proper way to perform a salat, which begins with the worshiper purifying himself by washing his hands, forearms, face, nose, mouth, ears, head and feet.

Moreover, there is no excuse not to pray. Wherever they might be (mid-conversation, shopping, walking on the street), people in Saudi Arabia will stop, take out their prayer mat and begin the ritual.

However, due to ongoing conflict between the nation's religious leaders, the religious establishment is often in disagreement with itself.

At the center of this conflict are differing opinions about how men and women should, or shouldn't, interact in society.

In 2009, a senior Saudi Arabian religious scholar, Sheikh Shatri, criticized how men and women were mixing at the kingdom's new universities. Other religious scholars disagreed, and, in their defense, they pointed to Prophet Mohammed, who was known to have socialized with women he wasn't related to, some of whom even washed his hair.

This led the head of the vice police to order his officers to stop interfering with men and women who wished to meet in public spaces. (The grand mufti — the highest religious authority aside from the king — expressed his displeasure in this.)

All of these disputes could be read in the public newspapers, and, as a result, people began to feel that Wahhabi Islam's rules perhaps weren't so absolute and unwavering.

> _"Most people want to be more religious than the Prophet. But...the relationship between men and women is not war."_ \- Sheikh al Ghamdi

### 4. Saudi Arabians have traditionally remained isolated from one another, but social media is changing this. 

It's easy to feel isolated and alone, even if you live in a crowded city. But there are many reasons for a Saudi Arabian to feel this way.

For generations, Saudi Arabian society has been one of isolation and cultural suppression. 

Spending millennia in the harsh desert conditions of an arid peninsula have had some side effects on Saudi society. No emphasis is placed on the kind of cultural or festive events that bring people together. Instead, people tend to stay isolated within their own tribes and families; what matters most is the survival of their own small clique.

This mentality is encouraged by the monarchy. Social barriers are enforced: women are kept hidden behind veils, and garments generally are uniform. The monarchy also imposes severe religious doctrine, which discourages people from gathering together to share laughter or emotion and encourages only prayer. All of this has kept the isolated people of Saudi Arabia from organizing any serious challenge to the ruling power.

But this is changing. The presence of satellite television, mobile phones, the internet and social media has helped Saudi Arabians unite and become more critical of the regime.

The Saudi Arabian government is beginning to find it harder to keep their people isolated from outside influences as well as one another.

On Facebook, most Saudi Arabians keep conversations friendly. But, in the face of tragedy or social injustice, emotions can run high.

In 2009 and 2011, sudden floods struck the country. Both times, the government was ill prepared and the nation's poor sewage and drainage systems wreaked havoc on public health. The citizens were furious.

How could this happen in a prosperous and wealthy country? Compounding the outrage, the government lied about the number of people who were affected by the flood. So the people took to social media to criticize their government's neglect and mismanagement.

> _"Unhappy Saudis routinely e-mail each other photos of flooding, destitute children, dilapidated schools and displays of lavish living by princes."_

### 5. Women are challenging their traditional role in Saudi society, and their efforts are starting to take effect. 

It's a common mistake to think that the traditional Islamic world relegated all women to household duties. After all, the Qur'an describes the prophet's first wife as an accomplished businesswoman, and a subsequent wife as being a leader of an army.

Yet, despite these empowered forebears, Islamic women today are still fighting to be recognized as having a place outside the home, while modern society in Saudi Arabia is unsure about what women should be allowed to do. 

As it is now, Saudi society is structured to keep a woman in her home, under the control of her husband or male relative. It is also the last remaining society to forbid women from driving, which can only be seen as a symbolic restriction meant to prevent women from traveling freely without male supervision.

This travel ban finally faced significant opposition after the Arab Spring in 2011: Many Saudi women took part in organized "drive-ins" and got behind the wheel of a car in defiant protest.

Women still can't legally drive, but the struggle is slowly gaining positive results thanks, in part, to King Abdullah, who reigned from 2005 to 2015 and was relatively supportive of women's rights.

In 2011, he announced that women would finally be allowed to take part in the parliament elections of 2015. And since 2013, women have been eligible for appointment by the king to the Consultative Assembly of Saudi Arabia, the governmental body that proposes new laws.

Though these reforms have positioned the king as a champion of the women's movement, there's little doubt that his gestures are largely symbolic. Keep in mind, though, that the political bodies of Saudi Arabia have no real power.

### 6. A new generation of rebellious youth has emerged in Saudi Arabia, which reflects growing unrest. 

Western teenagers are practically expected to rebel against their parents and other authority figures, but this concept is a new one to other places in the world. 

In Saudi Arabia, previous generations were mostly complaisant. They recognized, or even revered, the power of the authoritarian religious state. This makes the recent waves of youthful rebellion especially momentous.

But it's no big surprise. The nation's population is actually one of the youngest on the planet, and there's no shortage of rules and restrictions for them to rebel against.

The movement has presented a particular challenge for parents and the religious establishments. In March of 2011, the Arab Youth Survey revealed that 31 percent of Saudi youths claimed to no longer believe in traditional values, which was the highest percentage of the ten Arab countries that were surveyed.

The new rebellious spirit is expressed in many ways. Some choose religious extremism, vandalism or violence; others have started wearing Western clothes — a defiant statement of modernism.

Some of these acts have put especial pressure on the government to embrace reform.

One rebellious Saudi by the name of Feras Bugnah made a powerful short film called _We Are Screwed_. It captured powerful images of the squalid living conditions in the poor neighborhoods of Riyadh, Saudi Arabia's capital.

It again raised the question: How can one of the wealthiest countries in the world sit idly by while families live without electricity and struggle to survive on discarded furniture and less than 2500 riyals — about $650 — per month?

After being posted on the internet, Feras Bugnah's nine-minute film went viral and received nearly a million views in Saudi Arabia alone.

It also resulted in Bugnah's arrest. After 15 days of intense public pressure and outcry against the government on social media, he was finally released.

### 7. Saudi Arabia has an unusually large number of princes, but most of them are trying to help the country. 

In the West, royalty is generally something that only a few lucky people are born or married into.

But there's a different royal landscape in Saudi Arabia, where passing generations have left the nation filled with princes who have a lot of spare time on their hands. 

Responsible for all these princes is the founding king of Saudi Arabia, Ibn Saud. He had 30 wives and sired 44 sons and numerous daughters. So, three generations later, there are thousands of princes and princesses with direct lineage to the throne.

A prince is supposed to oversee his own little part of the country or have some high-ranking governmental job, but there just aren't enough positions available for all of them. So the majority of them are given a healthy minimum allowance — about $19,000 a month — and told to search for something to do.

Now, you might think these princes would take advantage of the opportunity to waste their time and money, but many try their best to be of service. 

Prince Abdullah is a fine example. As a grandson of the founding king, he was actually receiving only a small allowance from his father. But he had ambitions, so, with the help of a bank loan, he opened a successful paper-recycling business in 1991.

Ten years later, Prince Abdullah continued his humanitarian efforts — this time finding inspiration in sports and US football. He noticed that the American players tended to regard defeat as something they'd brought upon themselves, something they could have worked harder to avoid. Saudi athletes, on the other hand, liked to chalk up defeat to external causes.

This observation led prince Abdullah to write a book comparing US sports culture to that of Saudi Arabia, an endeavor he hoped would spur Saudis proactivity and greater responsibility.

### 8. Despite huge investment in public education, the unemployment rate in Saudi Arabia is dangerously high. 

It's no secret that some people in Western cultures can take things for granted. Take education, for example. Many kids moan and groan about having to go to school every day, when this is actually a great privilege. 

In Saudi Arabia, the public education system is in poor shape. 

Some of the problems in schooling can be traced back to the fact that, before the 1960s, there wasn't any public education system at all. Back then, only two percent of girls and 22 percent of boys attended school.

Saudi Arabia has tried to turn things around over the past few decades by investing hundreds of billions of dollars in the education system.

At this point, the country spends more money on education than the United States. Yet the education rate remains extremely low. Many factors are at play here, including a lack of good teachers. Not only are women not allowed to teach male students; well-educated men would rather make money in the lucrative oil business.

Sadly, when the American National Center for Education Statistics conducted worldwide comparative studies in 2011, results showed that Saudi Arabian students were among the least competent in multiple subjects — including math.

This naturally plays a big role in Saudi Arabia's alarmingly high unemployment rate, which, for people between the ages of 20 and 24, is at 45.5 percent for women and 30 percent for men.

The fact that unemployed youth were more likely to become troublesome rebels or get recruited by Islamic extremist put education high on King Abdullah's list of priorities.

He tripled education spending, investing $137 billion in 2010. In 2009, he appointed a new minister of education, his Stanford-educated son-in-law. And he appointed Norah al-Faiz as deputy minister, the first woman to receive such a high-ranking position.

But it will continue to be an uphill battle for anyone hoping to improve education in Saudi Arabia, since most religious leaders want schools to teach only Islamic theology and history.

### 9. The Saudi economy is overly dependent on oil and foreigners, and, so far, efforts to remedy this have failed. 

Having a huge supply of oil in your backyard might not sound like such a bad thing, but when you have too much of a good thing, it can lead to problems. Saudi Arabia has come to know the downside of being oil-rich.

Over the years, Saudi Arabia's economy has become dangerously dependent on oil and foreign labor. 

And now they're faced with the fact that oil won't be able to sustain their economy forever. The reserves are being steadily depleted, and there's no sign of any previously undiscovered sources being revealed.

The prognosis of Jadwa Investment, a Saudi financial institution, is grim: When you consider the depleted oil reserves alongside the increased spending necessary to maintain political stability, the country will be drowning in debt by the year 2030.

Saudi Arabia is also overly reliant on imported labor. The economy is heavily dependent on the labor and expertise of foreign workers who are either better educated or simply willing to accept low pay. Naturally, this adds to the high unemployment rate and the troubling statistic that 40 percent of Saudis live on less than $850 a month, with many of them in poverty.

This is a problem that the country has been struggling, and so far failing, to solve for years. They've spent billions trying to strengthen the economy and help the poor and unemployed by putting money into welfare benefits and investing in the military, schools and religious establishments.

Part of King Abdullah's solution was to focus on the public sector. He created a minimum wage for government jobs and gave these workers a bonus of two month's wages. He also managed to create 126,000 new jobs in the fields of education, health care and security.

This might look good on paper, but it actually reinforces the problem at hand. It gives the Saudi middle class even less incentive to compete in the private sector. And until they take that step, they'll never find new revenue streams, and they'll continue being dependent on oil and foreign workers.

### 10. Final summary 

The key message in this book:

**The monarchy in Saudi Arabia has been able to keep control of their country thanks to religious influence and incredible affluence through oil money. But as the petrol reserves diminish, the country is faced with the challenge of becoming a modern state.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Lawrence in Arabia_** **by Scott Anderson**

_Lawrence in Arabia_ (2013) reveals how a small cast of characters forever changed the Middle East during World War I and the Arab Revolt. At its center was T. E. Lawrence, a brash and untrained young military officer who was torn between two nations and experienced firsthand the broken promises of politics and the horrors of war.
---

### Karen Elliott House

Karen Elliott House is a Pulitzer Prize-winning author. After studying journalism at the University of Texas, she became a reporter for the _Wall Street Journal_. She spent much of the past 35 years visiting Saudi Arabia.

