---
id: 54b3a9816466330009330000
slug: on-the-edge-en
published_date: 2015-01-14T00:00:00.000+00:00
author: Alison Levine
title: On The Edge
subtitle: The Art of High-Impact Leadership
main_color: 719DD5
text_color: 49658A
---

# On The Edge

_The Art of High-Impact Leadership_

**Alison Levine**

This book is about how to be a great leader. Alison Levine shares her experiences from business and mountain climbing to illustrate that great leaders all have certain common traits — whether they're on a summit or in an office.

---
### 1. What’s in it for me? Learn how leadership principles from extreme adventure apply to business. 

What does leading a team of software developers have in common with climbing Mt. Everest? In principle, _everything_, according to Alison Levine, the author of the book _On the Edge: The Art of High-Impact Leadership._

In these blinks you'll learn how leadership principles that work in the world of high-impact adventures also apply to contemporary business environments. Both contexts require the leaders to make critical decisions in environments that are far from perfect.

You'll also learn

  * that "practicing" sleep deprivation can help you perform in crisis situations;

  * why employees with experience, expertise and ego are preferable; and

  * how complacency may kill you.

### 2. Team leaders need to empower every member of the team. 

Failing to rise to a challenge never feels good. You'll end up wondering if things would've gone differently if you'd only been more ready. So when a team confronts a challenge, the team leaders need to be very prepared.

As a team leader, you always need to be in top form. You owe that to your team — they'll be expecting more from you than from the others. So leaders need to perform at the highest level, both physically and psychologically.

This means many things. For one, you'll need to condition yourself to be able to handle situations where you can't sleep. You never know when you'll have to face a crisis situation and stay up all night. This is true whether you're leading an expedition to Mount Everest, or a team of programmers in a company.

If you, as the leader, get stressed out about something minor like being tired, you'll stress the rest of your team out as well. So it's a good idea to "practice" sometimes, so you'll be ready when you have to experience it.

Good leaders also understand that it's their duty to inspire leadership in others. They need to ask team members to rise to new responsibilities, so they'll grow as leaders themselves.

This sort of leadership makes the team function more effectively, and also prepares them for the worst possible scenarios. This could be a matter of life or death in a place like the Himalayas.

Leaders should prepare their teams so well that they're skilled enough to move ahead with the mission even without the leader.

Leadership skills are essential, so you should never stop developing them.

> _"Everyone is in a leadership position — regardless of age or title or tenure or where we work."_

### 3. Leaders should choose their team wisely, by looking for experience, expertise and a healthy ego. 

In extreme environments, it's vital that every single team member works to ensure success. And if someone starts behaving badly, there isn't any employee handbook you can look in to figure out how to deal with them!

This means that leaders need to choose their team members very wisely. On climbing expeditions, where teams face danger on a daily basis, climbers literally put their lives into their partner's hands.

You've already got enough uncontrollable factors affecting you on a mountaintop — you don't need team coordination problems too! In the most dangerous situations, that kind of trouble can even be deadly.

In business environments, proper team dynamics can mean the difference between winning or losing market share. Whether you're in sports or business, it's crucial you choose your team members well.

When you're seeking out new team members, search for people with experience, expertise and the right kind of ego.

When you're taking on a mountain, you don't want to be up there with people who are just fun and easy-going. They need the necessary survival skills to succeed in that environment. You've got to find people with skill and experience.

Your team members also need to have a high _performance ego_. That means they have to have confidence in their abilities, so they'll be ready to take on big challenges.

It's also crucial that your team members have _desire –_ and not just the desire to climb, but the desire to be part of the team. You want people who are proud to work together and take care of each other — people with _team ego_.

In a strong team, each member cares about helping the others as much as they care about themselves.

> _"Screen for aptitude, then hire for attitude." - Alfred Edmond Jr._

### 4. Team leaders have to build strong partnerships with other teams. 

Whether you're in the mountains or an office, other teams can be a big help to you if things go wrong. In fact, you might even need them for things to go _right_.

Collaboration is always critical to success. In business, it's quite rare for a team to operate independently, without working with other teams in the same organization. Different units have to rely on each other when working towards a certain goal.

Engineering units, for example, usually rely on research and development to optimize their manufacturing processes.

The same is true for expedition teams. Separate teams have to coordinate their climbs when going for the same mountain, to make sure the route doesn't get crowded. Team leaders have to communicate clearly with each other, so each team has the best possible shot at success.

Effective collaboration is also vital for staying safe. In very dangerous situations, climbers need to be ready to reach out to other teams too.

Strong relationships can save lives in the most critical situations. Things can always go wrong unexpectedly, even for the most prepared teams. You never know when a climbing rope might tear and a person might fall.

So you'll be much safer when you can count on other people from outside your team, in case an emergency arises. In an expedition, you can't assume people you don't know will come rescue you if you need it. It's very important to foster relationships with other climbing teams.

If other teams know you're out there, you're much less likely to be overlooked if your team goes missing. If you need to call for help, there's a better chance you'll get it, because people are more likely to help others if they know them, as opposed to if they're strangers.

So, whether you're climbing a real or metaphorical mountain, strong partnerships are critical for success.

### 5. Complacency can be deadly, and agility is the key to avoiding it. 

Dangerous situations often result from people failing to react to changes around them. When things are going well, we run the risk of becoming complacent. In some cases, that can be deadly.

Sherpas, the local climbing guides of Mt. Everest, can fall victim to this. They can become overconfident on the hazardous terrain because they're familiar with it, which has led to incidents of some not taking the proper precautions.

In 2012, a climbing guide named Namgyal Tshering Sherpa tragically died when he fell into a crevasse. He hadn't clipped into his safety rope, which could've saved his life.

Complacency can lead people to engage in risky behavior like this because they've gotten away with it in the past. It's easy to become complacent when you settle too much into a routine.

Because complacency can be so dangerous, leaders need to watch out for it. The key to avoiding complacency is _agility_.

In the mountains, you have to move swiftly and remain alert. You need to be ready to respond when your environment changes. This sort of agility increases your chances of survival.

And it isn't just adventurers who can fall victim to complacency; businesses also suffer if they don't adjust to changes in their environment.

This happened to _Research in Motion_ (RIM), the maker of the _BlackBerry_ smartphone. In 2008, it was valued at $80 billion. By 2012, that figure had dropped below $5 billion. Why?

RIM failed to adapt to changes in the smartphone market. They didn't anticipate the rise of touchscreens, and their business model wasn't agile enough to adjust when their consumers' behavior changed.

You never know when you'll need to be ready for an oncoming shift, whether in business or the mountains. It's absolutely vital to always be prepared for change.

### 6. Good leaders make their team members feel valuable, and help them compensate for any weaknesses. 

In extreme situations, the way you deal with a weak link in your team could mean the difference between success and failure.

In a business, you often have to work with people who are less experienced or capable than you. In climbing teams, members will certainly have different physical capabilities. In either case, when confronted with a team member's weakness, most leaders will simply try to "cure" it. This isn't the best approach, however.

Why? Because there's a fair chance their weakness _can't_ be cured.

You _can_ change people's work attitudes, but some limitations can't be overcome, like height, speed or the specific way our brains process information.

Good leaders know it's their responsibility to help team members become _productive_, so that everyone benefits. This doesn't necessarily mean eliminating their weaknesses.

Instead, focus on _compensating_ for weaknesses. Alison Levine learned this when she took part in a long-distance skiing expedition in Antarctica.

Levine's short legs couldn't carry her as fast as her taller teammates, so she was the slowest in the group.

Her team leaders compensated for her weakness instead of trying to overcome it. They told the others she was carrying too much weight, and had some of them carry part of her food supply to distribute the weight more equally.

This decision took her emotional well-being into account. They didn't blame her for her weakness, which would've made her feel even worse. When they asked the other team members to help, they also engaged them so that everyone was working on the problem together. It solved the problem, and kept them close together as a team.

### 7. Leaders have to make sure their team members are properly equipped. 

When you take on a challenge — no matter what it is — you're always responsible for your own preparation. Team leaders, however, have the most responsibility.

Leaders are responsible for knowing what tools are needed to get the job done. Even if you have the skills, you'll fail without the right equipment.

Germany learned this lesson in June of 1941, when they invaded the Soviet Union. They had millions of armed soldiers, and thousands of tanks and horses, but they didn't bring warm enough clothes for the winter, or the right tools to maintain their equipment in the cold.

Most historians agree that this was the main reason Germany's attack failed: they simply didn't pack well.

So even if you've got people and enough money, you won't achieve your goals without the proper gear. Leaders have to know what skills and tools are needed to get the job done.

Leaders also need to make sure their team is properly equipped. Not having the right equipment can be dangerous.

Levine experienced this when she climbed the Carstensz Pyramid, the highest peak in Oceania. She misjudged the time it would take to get to the summit, then had to climb back down in the dark.

She didn't have a working headlamp, so she couldn't see anything while climbing. She put her life at stake by not being prepared.

So if you're thinking you can't afford all the gear you need, ask yourself if you can afford not to have it.

Also, remember that even if one team member doesn't have the right equipment, it could affect the team as a whole. Leaders need to be sure that everyone is prepared.

> _"Leaders should never expect the people on their teams to take any risk that they would not be willing to take themselves."_

### 8. Good leaders show their team that they care about them and take the time to get to know them. 

Don't ever underestimate the importance of treating others with respect and kindness, especially when it comes to achieving a goal. When you treat people well, they'll usually reciprocate.

Good leaders show their team that they care about them. It doesn't matter whether you're leading an expedition or a software development group — your team has to know they're important to you.

You don't automatically get trust and respect by virtue of being a leader. You have to earn it, by treating your team members right.

Mark Zuckerberg, for example, keeps his desk in the center of the common workspace at Facebook. He works alongside his employees, and sends the message that they're all in it together.

Leaders guide the team towards the goal, and also make sure the team members are performing as best they can. They have to get to know the people they lead.

When you take time to get to know your team, you'll show them how much they matter to you.

One good way to do this is to find out what's important to them, like their families, hobbies, interests outside work, or hopes for the future. Those interactions lay the foundation for strong bonds.

When you really get to know your team, you'll also be able to assess their abilities. A person who likes being part of a big family, for instance, probably wants more social and collaborative tasks.

You have to approach each person individually when it comes to helping them perform at their best. The more you know about them, the more influence you have as a leader.

### 9. Leaders should break the rules when necessary. 

Generally, rules exist for a reason. But don't think of them as strict laws that you have to follow blindly. Instead, view your rules as guiding principles.

Sometimes great things can happen when you break the rules. This happened to Channing Moss, a soldier, when his platoon was on patrol in southeastern Afghanistan in 2006.

Moss's convoy was hit with some rocket-propelled grenades (RPGs). An unexploded RPG entered Moss's body, essentially turning him into a human bomb. The standard procedure for that situation would've been to sandbag him in an area where he wouldn't be a danger to anyone if he exploded, then basically wait for him to die.

Fortunately, the medical team chose not to obey the rules. Moss was airlifted to an aid station and the explosive device was removed — at the extreme risk of the medical personnel. The procedure was successful, and Moss was saved.

In extreme situations like that one, leaders have to make decisions that can move everyone toward the best possible outcomes. This often requires breaking the rules.

In fact, just robotically following the rules can be harmful.

In 2011, for example, a parade was planned in Shelter Island, New York to pay homage to a soldier named Joseph Theinert who was killed in Afghanistan. American flags were hung on the utility poles along the parade route.

LIPA, the local utility company, billed the town for hanging the flags, because it was a rule to charge a fee to use them. The amount of the bill was $23 in total.

When the story hit national news, LIPA had serious PR problems — it faced a popular outrage.

So, there are times to follow rules and times to break them. Leaders need to know the difference.

> _"Everyday rules can also be ignored if doing so means achieving more favorable results."_

### 10. Companies need credos – and leaders need them too. 

Our actions say a lot about who we are. So we should all have a _credo_ — a set of key words that reminds us how to behave in our professional and personal lives.

Companies need credos too. Leaders can't be everywhere at once, so they've got to trust their team to do the right thing even when they aren't being watched.

Verizon, for example, has a credo built around the words _integrity, respect, performance excellence_ and _accountability_. If a company has a set of guiding principles like those, employees will have better judgement in tricky situations.

A leader can build her identity by making a conscious effort to demonstrate her credo. As a leader, you have to know what you stand for, and what you value most.

So what's your credo? How do you want people to describe you?

Levine's credo is "Count on me." She aspires to be the person her teammates always go to when they need help. A credo like hers can help you become the great leader you truly want to be.

Remember, however, that having a credo isn't just about writing down some words. It's about taking action and _living_ those words every day.

When you live by your credo, you'll be able to gain the trust and loyalty of your team members. They'll know what to expect from you. So be sure to demonstrate your leadership philosophy on a daily basis!

Your credo will also help you stay true to yourself and your beliefs, even in extreme circumstances. Physical or mental strain can threaten your values, but your credo will remind you of who you want to be.

### 11. Final summary 

The key message in this book:

**The leadership** **principles that apply to extreme adventure sports also apply to today's extreme business environments. Both environments require the team to face sudden, unintended, high-risk situations. So be prepared, take care of your team members individually, and inspire them to become leaders themselves.**

Actionable advice:

**Don't try to eliminate all weaknesses.**

Remember that some weaknesses simply _can't_ be eliminated. Compensate for weaknesses, rather than trying to squash them. Doing so allows team members to make the most of their strengths, and work around any setbacks.

**Suggested further reading:** ** _The Leadership Challenge_** **by James Kouzes and Barry Posner**

In _The_ _Leadership_ _Challenge_, James Kouzes and Barry Posner explain how anyone can become a better leader. Citing various examples from their 25 years of experience and extensive research, the authors present their theories on what makes a successful leader, and give practical advice on how to learn good leadership behavior.
---

### Alison Levine

Alison Levine serves on the board of the _Coach K Center on Leadership & Ethics_ at Duke University. She's climbed the highest mountain on each continent, served as a captain of the first A _merican_ _Women's Everest Expedition_, and skied to both Poles. She's also worked on Wall Street as an associate with _Goldman Sachs_.

