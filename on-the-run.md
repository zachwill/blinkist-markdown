---
id: 53cf8f5136306500078b0100
slug: on-the-run-en
published_date: 2014-07-22T00:00:00.000+00:00
author: Alice Goffman
title: On the Run
subtitle: Fugitive Life in an American City
main_color: 547F85
text_color: 44666B
---

# On the Run

_Fugitive Life in an American City_

**Alice Goffman**

In _On_ _the_ _Run_, author Alice Goffman dives into a dangerous world unknown to most Americans: the poor, predominantly black and crime-ridden neighborhood of Sixth Street in Philadelphia. Living in the area for six years, Goffman witnessed the daily life of the neighborhood and thus gained a unique insight into a crime-plagued society, its members constantly "on the run."

---
### 1. What’s in it for me? Learn about day-to-day life in a crime-ridden culture “on the run.” 

Have you ever seen the award-winning TV show _The_ _Wire_? But have you ever wondered what it's actually like to _live_ in a poor black neighborhood, to be surrounded by drugs and crime, and wary of police officers hungry for their next bust?

_On_ _the_ _Run_ offers a horrifying picture of the day-to-day life of many young black Americans who live in suburban ghettos. This account of social alienation, race relations and criminalization as a result of America's prison-industrial complex is based on the extensive ethnographic research that author Alice Goffman, daughter of the renowned sociologist Erving Goffman, collected over the six years she spent living in Philadelphia's notorious Sixth Street neighborhood.

The reality she reveals through these stories of everyday urban lives is shocking, and you'll struggle to believe what life may be like for people who could be your neighbors.

In these blinks, you'll discover:

  * how some people can make money from urine,

  * why going to the hospital when injured might not be the smartest idea, and

  * why Sixth Street women call the police to apprehend their boyfriends.

### 2. Large swaths of US society live their lives under threat of police arrest. 

Sixth Street in Philadelphia is just another poor black neighborhood in America. Those who live there embody a lifestyle familiar to many Americans: a life centered around evading the law, or a _fugitive_ _culture_.

But from where does this culture originate?

One major source is the US justice system, which has criminalized large parts of society.

US prison statistics show that since "tough-on-crime" and "war-on-drugs" policies were implemented in the 1970s, prison confinement rates have sharply risen. Today, about 3 percent of US adults sit either inside prison or outside under some type of police supervision (such as under house arrest). To put things into perspective, only _labor_ _camps_ _under_ _Stalin_ _in_ _the_ _Soviet_ _Union_ match the levels of incarceration in America today.

Further statistical analysis reveals that black Americans are a group that is particularly targeted. While this group represents only 13 percent of the population of the United States, black Americans make up 37 percent of the prison population. This recent increase was largely driven by welfare cuts that pushed many in black communities into the drug trade to survive — just when the government crackdown on the drug economy began.

We like to think that people choose to be criminals. But most people in poor neighborhoods become criminals as there is little else to do to survive. Living in areas that are plagued by high unemployment, an individual might sell crack on the side to make ends meet. But while selling drugs could help provide needed cash for your family, it could also be the first step in an endless cycle of warrants, court hearings and jail time.

Most of the young people who live in this Philadelphia neighborhood — the Sixth Street boys — are caught up in this cycle and as a result are always on the run.

In this cycle, minor events often balloon into full-scale criminal activity, dragging everyone into its pull. For example, an adult is arrested for possessing marijuana; as a result, he loses his driver's license. Out on probation, he drives his brother to school, but is then caught driving without a license; he's arrested and put in jail. Now the brother robs a store to "earn" the bail money...and the cycle continues.

> _"Black people make up 13 percent of the US population, but account for 37 percent of the prison population."_

### 3. In a fugitive culture, it’s usually better to run from the police than cooperate. 

You might think that the best way to avoid problems with the law is to respect the law. But unfortunately, for many Sixth Street boys, this simply doesn't work.

Why?

Because in a place like Sixth Street, you are always a target no matter what you have — or haven't — done.

If you live in a fugitive culture, there's always a reason for the police to nab you. Perhaps you've a half-smoked joint in your pocket, or the rules of your probation forbid you to be on a certain street. Thus when the police approach you, even if you feel you've done nothing wrong, the risk is high that something will come up.

Chuck, a Sixth Street boy, summed it up for his little brother: "Whoever they're looking for, even if it's not you, nine times out of ten they'll probably book you."

To avoid getting arrested, the Sixth Street boys have developed an impressive sense of police awareness.

There are two key parts to escape the police successfully: Identify an officer quickly, and learn how to run. If you're trapped in the fugitive culture, you'll have learned what a police officer looks like when working undercover, how they move and where they are likely to appear. And once you notice one, the plan is always the same: run and hide. Little boys learn the importance of running from an early age, and from their older siblings, they learn escape tricks and the location of secret hideouts.

The fear of the police is so strong that even the author and her white friends often caused little boys to run down Sixth Street, alerting their friends to whom they thought were undercover police.

If you live in Sixth Street, you need to be always alert and keep a healthy distance from any form of police authority. One moment you're minding your own business, and the next you're playing another round of hide-and-don't-get-caught.

### 4. Police can often catch fugitives by targeting their families’ social insecurities. 

Try imagining for a moment that you can't trust anybody: not even your family or friends, people who should ideally always be there to support you.

For many of the Sixth Street boys, this is their reality. They are never sure if their friends or their family won't turn them in. This is because in a fugitive culture, the police are always able to use information they have on your friends or family to make them talk about _you_.

How?

The police use all kind of tactics to extract knowledge from your closest acquaintances.

Often, a fugitive's father, for example, might himself have an open warrant, or a step-mom might have her own history with the law. So when threatened with arrest for housing a fugitive or interfering with an investigation, a family member knows that things could get bad for them quickly.

Moreover, mothers and partners may give in to questioning if a police officer "suggests" he might alert Child Protective Services, the state agency for child welfare, or "let" a family's social housing benefits be taken away. Adding to the weight of these threats, the police actually often have the legal grounds to follow through, if they find drugs in the house, for example.

Because the police are sure to target their closest family or friends — people who know all their secrets — Sixth Street boys have developed new ways of escaping arrest.

You might think that your best friend would be the best person to hide you. But Sixth Street boys know better and never run to where they might be given away, like an uncle's or the mother of their child. Instead, they flee to spots even their friends wouldn't think of, such as a strange old neighbor or the friend of a friend's friend.

> "Nine times out of ten, you getting locked up because somebody called the cops, somebody snitching."

### 5. Avoiding the police means living outside mainstream society and its benefits. 

On Sixth Street, you've got to make sure you're not in the wrong place at the wrong time — which can be pretty much anywhere. And because the police are always looking for their next arrest, people on Sixth Street have to make sure the police can never predict where they're going to be.

This means you can never develop a regular routine, because as soon as the police want something, having a routine means the police's extensive surveillance system will quickly track you down.

Therefore a resident of Sixth Street has to constantly avoid doing predictable things. And as a consequence, living in a fugitive culture entails missing out on a lot of life's great things.

For example, all social gatherings are taboo. Chuck, a friend of the author, couldn't even attend the birth of his first child because of an outstanding warrant for some minor offense. He knew that the police often search waiting rooms at hospitals and run the names of all visitors through their files in search of wanted people.

On top of being denied any sort of routine, Sixth Street boys can't even call the police when they actually need them.

Living on Sixth Street means that if your house is robbed, the justice system will hurt more than help you. If you talk to the police, they'll then find out where you live, and thus easily find _you_ next time you get in trouble.

Sixth Street boy Reggie was robbed on a street where he shouldn't have been, based on the rules of his probation. He therefore couldn't call the cops or even seek medical treatment. His injured nose is still crooked, as he was afraid of police questioning at the hospital.

Because he is a member of the fugitive culture, Reggie was unable to take advantage of some of the most basic societal institutions: the justice and health care systems.

### 6. If you can’t get out of the fugitive culture, you might as well make the best of it. 

You know the saying: If life gives you lemons, make lemonade. And the lemons in a fugitive culture are so bitter that the residents of Sixth Street have to make a lot of lemonade.

For example, when living on Sixth Street is no longer safe, residents will try to get themselves locked up in prison instead.

Why?

Because every once in a while, a traditional conflict boils up in the neighborhood. For example, one time a Sixth Street boy accidentally shot a man from Fourth Street. The conflict escalated, and the following weeks many others were shot and wounded. For a time, Sixth Street was a bit too "hot."

To avoid the conflict, many Sixth Street boys chose prison for safety. The author witnessed how one after the other, Sixth Street boys would turn themselves in on an outstanding warrant that they would have otherwise ignored. One of her Sixth Street friends even initiated a police chase through the city to get arrested and then jailed.

Another way some Sixth Street boys make the best of their circumstances is by using the bail office as a bank.

For example, say you had to pay $2,000 in bail and you're on probation for a year. Your cash is certainly much safer with the authorities than lying around at home for your crack-addicted sister to sniff out. And even though you'll lose 20 percent in charges, banking with the bail officer is still better than having all your money stolen.

On top of that, you might even use the bail as a form of collateral for informal lending. After he was denied a bank loan because of his criminal record, the author's friend Mike borrowed $1,000 from a local marijuana dealer and promised to pay it back with interest from his bail money.

> **Fact:  

** When they feel that the streets are too dangerous, many women call the police to have their partners or sons arrested.

### 7. The fugitive status is used as a tool in social relationships. 

Fugitive culture affects almost every area of an individual's personal life. Even the person you choose as your romantic partner is also affected by it.

Consider the story of Lisa, a Sixth Street girl. When she got pregnant, her partner Reggie was quick to deny all responsibility. But as soon as she had the chance, Lisa got back at him in a normal Sixth Street fashion: by threatening to call the cops.

On Sixth Street, women threaten to call the cops to control their partners.

Reggie made the mistake of getting involved in a shooting in front of Lisa's house. Immediately, she reported him for attempted murder, but agreed to withdraw the charges after he apologized — and took responsibility for the baby.

In some cases, women even make up allegations to pressure guys to leave new girlfriends or to stop hanging around with other boys. They can do this because they — and their boyfriends — know that the police will always believe their false accounts over the true accounts of their partners.

The fugitive status also changes how people show their affection or dislike.

Why?

Because in a fugitive culture, relationships are in large part defined by mutual risk-taking. Hiding a fugitive friend can cause some trouble for yourself: Imagine the cops search your house and not only find the friend but also your unlicensed gun, or stash of marijuana.

This means that one of the strongest ways of showing someone you like them is to take that legal risk and hide them in your home. Similarly, Sixth Street residents don't take on risk for just anybody, and might even deliberately deny shelter to some as a token of dislike.

### 8. The penal system gives meaning and structure to criminalized youth. 

Imagine this situation: Your mother sits in the front row in her best dress, your girlfriend looks stunning sitting next to her and you are surrounded by well-respected representatives of society. Sound like a graduation ceremony? If you come from Sixth Street, this is more likely your first big court trial.

This is how the fugitive culture replaces the typical coming-of-age structure in society.

Instead of celebrating a first job or a first semester at college, Sixth Street boys tend to experience their most important "firsts" as part of the criminal justice system: Their first drug job, their first juvenile case, their first time in prison.

And similar to parents that nag teachers for better grades or organize after-school activities, Sixth Street parents will spend much of their time appealing to judges, writing petitions or driving to prisons to pick up their children. The structure is similar, only the content differs.

This involvement with the penal system directly shapes the boys' characters.

While boys in other parts of town create identities based on their hobbies or classroom performance, Sixth Street boys have different status markers: being cool is a matter of being level-headed amid violence and outwitting the law.

Yet no matter how well an individual manages, every aspect of the penal system is still demeaning and humiliating. Living with a void of positive reinforcement does have a negative psychological effect on many adolescents.

Growing up in the fugitive culture doesn't mean however that you lose all structure and live in social disorder. The justice system just replaces what most boys remember as their life-changing moments.

> _"Surviving on the run requires skill and cunning for which a person can be admired and granted some degree of respect."_

### 9. Fugitive culture is a phenomenally entrepreneurial society. 

Can you believe that it's possible to exchange urine for cash? Or that you could make a living smuggling drugs into prison? These are all services and commodities within the fugitive economy.

Inhabitants of Sixth Street often have to resort to such "entrepreneurial" activities as they are cut off from the normal economy.

Why?

Because it's almost impossible to find a decent job if your employer learns you've been arrested for drug possession or assault. At the same time, money is always short, social security benefits are limited and a significant share of income in the fugitive culture is spent on fines and bailouts.

As a consequence, people must become inventive and many turn to the multitude of underground economy jobs.

Some get involved with straightforward illegal activities, like smuggling drugs into prisons. For $50, a woman called Shonda smuggled marijuana into a Philadelphia correctional facility by hiding it in her underwear. Starting at first with her boyfriend, she soon acquired a broader base of clients and sometimes visited the prison three times a day.

Other residents of Sixth Street get by through stretching the law but without committing an actual crime.

Rakim runs a urine business. He sells his own drug-free urine to men on probation who have to constantly gives urine samples to their parole officers. And after his initial success, Rakim even expanded his business and contracted out urine production to two women!

In another example, Jevon made money from his ability to perfectly imitate voices. For $5 an hour, he would house-sit in the evenings and answer the phone for men on curfew as part of their probation requirements.

People will always make money if they see an opportunity. And in the fugitive culture, it's no different.

### 10. Fugitive culture creates extreme divides. 

While Mike, Reggie and their friends — typical Sixth Street boys — spend most of their days hustling in the streets and hanging out, there is also another community living on Sixth Street: the _clean_ _people_.

If you live in the fugitive culture and have three warrants on your record, Sixth Street considers you _dirty_ ; anyone who has managed to stay out of the penal system is considered _clean_.

But although residents live side by side, there is a big divide between clean and dirty people. To not get dragged into the fugitive culture, clean people do all they can to stay away from dirty people. They try to set themselves physically apart by leaving Sixth Street for work during the daytime and spending their free-time indoors.

If they don't, clean people fear they will end up like Josh. When Sixth Street boy Josh landed a $60,000-per-year job at a pharmaceutical company, he failed to break off ties with his dirty friends. Soon enough, coworkers overheard telephone conversations about flying bullets and somebody found out that Josh had a record for cocaine possession. Shortly after, Josh lost his job and was back on Sixth Street.

On top of the clean/dirty divide, there is also a large male/female divide.

When you look at who's dirty and who's not, it's mostly men who are fully entangled in the penal system. Women, on the other hand, constantly assist and support their partners in getting through the justice system. As a consequence, women bear most of the work at home, raising their children alone, and because they spend less time in jail, are often better educated.

Sixth Street is divided. The fugitive culture has not only managed to criminalize large parts of society, but also has driven a wedge between families, partners and old friends.

### 11. In trying to manage a rise in crime, the US government has produced ineffective solutions. 

Regular drive-by shootings, crack sold openly on the streets in front of children — this has been the day-to-day life in places like Sixth Street for years. But during a wave of crack-related crime in the 1980s, concerned mothers and citizens decided enough was enough, and pushed politicians to act.

So why do problems still exist?

Because while politicians responded, they applied the wrong tools.

It had come to a point where politicians couldn't ignore drugs and drug violence any longer. Yet they treated the complex social problems of racial segregation, poor education, drug addiction and abuse like something that could be solved by a _tough-on-crime_ _policy_.

Consequentially, the police and the penal system are pretty much the only public institutions "dealing" with neighborhoods like Sixth Street.

However, many police officers know perfectly well that their work isn't improving the situation. They realize that the only tools they have are handcuffs and jail time, and that's simply not enough. But the police officers have to work in these neighborhoods, and many reluctantly accept the reality: instead of protecting communities from a few offenders, they put a whole neighborhood under suspicion and surveillance, which only increases social tensions.

This tough-on-crime approach has led to a new kind of institutionalized racism.

In fact, the drug and petty crime laws in place since the 1970s and 1980s in the United States have had a similar effect as the so-called _Jim_ _Crow_ laws, the legal basis of US racial segregation that was in place until 1965 and that disfranchised millions of black Americans and deprived them of their civil rights.

Just like during the Jim Crow era, many blacks cannot move freely as they are always on probation; they cannot vote because they are in prison; and they cannot run for office and are excluded from many normal jobs because of their penal record.

Politicians like easy answers to complex problems, and tough-on-crime policies seemed like a good idea at the time. However, the government's response to drug crime has caused more harm to neighborhoods like Sixth Street than in actually curbing criminal activity.

> _"In Philadelphia, between 1960 and 2000, the number of police officers increased by 69 percent."_

### 12. Final summary 

The key message in this book:

**Through** **ineffective** **policies,** **the** **US** **penal** **system** **has** **essentially** **criminalized** **large** **parts** **of** **poor,** **black** **suburban** **neighborhoods.** **As** **a** **consequence,** **the** **lives** **and** **social** **relationships** **of** **many** **young** **black** **Americans** **have** **evolved** **to** **center** **around** **a** **lifestyle** **of** **being** **always** **on** **the** **run** **from** **the** **police.**
---

### Alice Goffman

Thirty-one year-old Alice Goffman is an assistant professor at the University of Wisconsin. She has received numerous awards for her ethnographic work about the Sixth Street boys of Philadelphia and _On_ _the_ _Run_ is her first major published work. Goffman is the daughter of Erving Goffman, considered one of the most influential US sociologists of the twentieth century.

