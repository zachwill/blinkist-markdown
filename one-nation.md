---
id: 55101e81666564000a4f0000
slug: one-nation-en
published_date: 2015-03-25T00:00:00.000+00:00
author: Ben Carson
title: One Nation
subtitle: What We Can All Do to Save America's Future
main_color: AE9957
text_color: 7A6C3D
---

# One Nation

_What We Can All Do to Save America's Future_

**Ben Carson**

_One Nation_ (2014) outlines what America should do to stop its current decline and to once more become the most prosperous and successful nation in the world. These blinks share recommendations that are based on faith, common sense and the values upon which the country was founded.

---
### 1. What’s in it for me? Learn how America can return to greatness. 

Why are political pundits talking about Ben Carson as a hot conservative pick for the presidency in 2016? What's special about his worldview?

Carson has a biting, logical and down to earth way of thinking about America's biggest problems, from faith and morality to the media and ignorance. In these blinks, you'll find out more about how he thinks America can be saved from its current decline and returned to its rightful position, leading the world.

In these blinks, you'll find out

  * how the liberal media is hurting progress;

  * why America's experiencing a moral crisis; and

  * why government handouts make us all serfs.

### 2. Liberal elites and political correctness prevent America from facing its problems. 

Have you ever wanted to say something but in the end decided against it because you were afraid that your comment could be interpreted as being politically incorrect? That was a mistake. Political correctness is an invention of the liberal elites of the Far Left. All political correctness does is prevent the sort of discussion that can move America forward.

The _Political Correctness Police_, including the media and some politicians, forbid people from speaking freely about a continually growing list of topics, and bludgeon people who violate their rules.

For example, when appearing on TV to discuss how marriage is a long-standing tradition that shouldn't be changed, the author compared gays to others that engage in non-traditional sexual relationships, including pedophiles.

Immediately, the secular progressive media seized on the comment, twisting the author's words and silencing what could have been a useful debate on marriage.

Meanwhile, elites create entitlement programs for less fortunate groups of people in order to solidify their positions of power in the media or politics. Yet these liberal policies have done little to improve the lives of those unfortunate people in our society. Black communities in particular have been destroyed by these handouts, while continuing to support those that provide them.

Martin Luther King Jr. and other black leaders understood this danger and supported self-reliance and self-help. Unlike the liberal elites, they believed that all people are capable of helping themselves.

> _"In their heart of hearts elites see themselves as society's savior, but they are blinded by pride to the results of their actions."_

### 3. Ignorance and bigotry divide America and prevent progress. 

Sometimes, people do wrong not for malicious reason, but simply out of ignorance. However, this simple ignorance, which leads to bigotry and political infighting, deeply harms the unity of American society as a whole.

America suffers in particular from _ignorance of history_. We must remember and understand the history of America and the values upon which it was built. If we always keep those values in mind — particularly the values of small government, individual rights and citizen engagement in politics — America cannot be astray.

For example, many people nowadays forget how important religion was in America's historical development. Secular progressives ignore this heritage and try to remove God from the lives of Americans. However, without a higher authority, people will be able to justify anything. Society simply cannot thrive without a strong moral social structure.

Another product of ignorance is bigotry: The more sheltered a person is, the more likely he is to have negative views of others who are unfamiliar to him. Bigotry, whether it is in the area of race, religion, gender, age or sexual orientation, not only ruins social relationships, but also blinds us to the real issues at hand.

Just think of segregated neighborhoods, where blacks and whites had few opportunities to work and socialize together. Whites believed blacks to be unintelligent and promiscuous, while blacks believed whites were greedy and cruel. Of course, neither was true, but these beliefs poisoned race relations forever.

> _"We must exercise our ability to identify the divisive forces and vote them out of office."_

### 4. Excessive government spending and national debt is enslaving American children. 

Do you ever stop to think about the future of America and get terribly worried? The author does. For example, many of the author's friends were wealthy people. They lived lives of abundance and extravagance, but when their circumstances changed, they had little to leave their children.

Similarly, America today is leaving little to its children. Instead, national debt is topping $20 trillion. And we know that debt leads to disaster: just look at what's happening in Greece.

In recent decades, the Greek government expanded expenditures and raised taxes on businesses. When lenders realized that this would continue indefinitely, they feared that businesses wouldn't be able to pay back loans, so they stopped extending loans, which led to crisis. When the government wanted to curb spending, people felt entitled to the help they had been receiving, and riots broke out.

What's happening in Greece is nothing more than a matter of history repeating itself: As the size of the Ancient Greek government grew, taxes had to be raised to support it. The tax burden became so great, that it reduced many citizens to serfdom. As serfs, they became dependent on handouts from the government. This made them de facto government slaves.

But when hard decisions are necessary to get out of a dangerous debt position, sometimes you have to select a less favorable option in order to survive.

For example, the author once climbed a dangerous mountain with friends. When a ledge he was standing on broke, his only hope was to reach for a cubbyhole full of spiders. Despite his distaste for spiders, the alternative was tragic, and he reached for the spiders and was able to climb to safety.

America is climbing a similar mountain with its debt, and it will soon have to adopt fiscal responsibility, which, while unpleasant, is far better than the alternative.

In the following blinks, we'll see how Americans can help reverse America's decline.

### 5. Push back, respectfully disagree and try to find a compromise. 

Have you ever been bullied? The author was, until one day he challenged his bully to a fight — the bully never bothered him again. Similarly, Americans should stand up to bullying by the liberal elites, bigots and overzealous spenders described above.

One place where bullying is rife is on liberal university campuses. Most professors are liberal and they penalize students with different views. At one Florida university, a professor asked students to stomp on a piece of paper with "Jesus" written on it. When one devout Mormon requested to be excused, the professor insisted, and after complaining to his superiors, the student was suspended.

Fortunately, the boards of trustees at these institutions are often staffed by more moderate members. Students should not be afraid to bring problems directly to them. Similarly, students can also publicize their grievances, as universities are terrified of bad PR.

In order to find compromises, we need to mutually respect each other. Disagreement is rarely a problem in and of itself. Indeed, it should be welcomed. One way to encourage respect is to refrain from name-calling, even when irritated. This manifests respect not only for others, but also for oneself. In an environment of respect, even disagreeing parties can identify what is important for them, what they would be harmed by, and find things where both can be tolerant.

A good example of such compromise is the gun control debate: Some want to ensure citizens can fight back against an out-of-control government, while others believe keeping guns from unstable individuals will reduce mass murders. Both can agree that they want to remove dangerous weapons from unstable individuals, while disagreeing on the target of controls: personal freedom vs. social safety.

Isn't that enough of a basis for respectful dialogue that can lead to compromise?

> _"Once you identify these bullies, you can stand up to them with courage, and they will step down."_

### 6. Yearn to be wise and well-informed. 

As a neuroscientist, the author knows one thing for certain: You can never learn enough! Learning — from childhood to old age — is important not only for employment and prosperity, but also because America's political system can only work with an educated populace.

How so?

Well, dishonest politicians are all too keen to take advantage of ignorant voters by promising bribes, like free health care, or handouts, while never intending to deliver them in the first place. An understanding of history, current events, science and finance gives citizens the ability to evaluate what politicians say and do.

Then Americans can choose who to vote for on the basis of their record instead of promises, party affiliation or name-recognition.

Education also leads to prosperity. You have a choice: The first 25 years of your life can be spent preparing yourself or not. Preparing well will benefit you for the next 60 years, while those who prepare poorly will face the consequences of being less employable.

In other words, investing in hard work for a few years can reap huge dividends.

It's also important to remember that expertise is not necessarily wisdom. Many people who are very well educated have a hard time sustaining themselves, while others, like Bill Gates or Steve Jobs, are hugely successful despite never finishing college. While this doesn't mean that higher education is useless, it does show that sometimes it's more important to use knowledge wisely than just to acquire it.

One example of well-educated people who lack wisdom are "experts" in the media. Just look at those who claim that the economy is sluggish because America does not borrow and spend enough money, and that new stimulus money is needed. While these people claim to be knowledgeable and present their degrees as proof, even the author's mother, who only finished the third grade, has enough wisdom to see that won't work!

> _"How does one acquire wisdom? First and foremost, one must be humble enough to recognize that one doesn't know everything."_

### 7. Help others, care for your neighbors and become your brother’s keeper. 

Do you know of anyone with a disability? Do they receive government assistance or do they get help from others? Take the author's uncle Albert. He was developmentally disabled, but his brother cared for him from a sense of duty — a model that has been sadly losing ground in recent years.

Everyone should take charge of their close ones, instead of relying on the government.

That's why we should be thankful for capitalism. Unlike socialism, where basic needs are met by the state, Capitalism recognizes personal responsibility and compassion.

True compassion means giving people a way out of poverty, instead of giving them handouts and keeping them poor. Some politicians even foster dependency on handouts so that voting blocs will continue to support them. That actually prevents any real, deep solution to their problems.

That's because people on handouts are discouraged from finding a job, even a low-paying job. But low-paying jobs can be very rewarding, and can be a path out of poverty. For example, the author used to work as a lowly X-ray technician while at medical school. But learning how to operate that machinery led to his discovery of a technique for visualizing a difficult part of the skull, which facilitated a complex neurosurgical procedure.

Some people, however, are simply irresponsible. If they are healthy and capable of taking care of themselves, their behavior is likely learned, and can be unlearned. If they don't want to learn how to take care of themselves and be more responsible, these people should be allowed to experience the consequence of their choices.

What that means is, if poor people are too irresponsible to hold on to a job, they should learn their lesson. Those who disagree, or are horrified by this suggestion, are free to care for these individuals. It's unfair, however, to force this responsibility on everyone else.

Now that we know what we can do to help make America better, let's look at a vision for America we can all work toward.

### 8. We need worthy role models to help give us a vision for our lives. 

Did you have a role model when you were growing up? The author grew up in a neighborhood where most role models for kids were either factory foremen or drug dealers, but he saw doctors as his heroes.

Even though he rarely met them, the author's medical role models guided him as he strove to educate himself, escape poverty and become a celebrated neurosurgeon.

Today, many young people see sports stars and entertainers as their heroes. But most of these people do little to intellectually contribute to our society. Despite being exemplary for their commitment and ambition, they often fail to prepare for life after their short careers are over.

Children should look elsewhere for their role models.

In fact, good role models are not at all hard to find. The author also looked up to his mother, for her tenacity, courage and her commitment to her childrens' education. Good teachers are also good role models. After all, they're often the difference between success and failure in people's lives. Finally, historical figures, particularly inventors, can also serve as excellent role-models.

Just like kids with role models, America could also benefit from more vision and direction — a path to follow.

The country's original vision, reflected in the US Constitution, was of a country where the rights of the people are protected from an ever-expanding state. Nowadays, politicians have replaced this vision with their own ideologies. Instead of creating an environment where diligent people thrive, they're trying to care of everyone and redistribute citizens' wealth.

The key to returning America to its original vision is understanding the Constitution and voting any politician who refuses to uphold it out of office. Americans should also convene community meetings to learn about the Constitution, identify those candidates that honor it and above all, defend it from those who don't.

### 9. Everyone must understand the origin of morality. 

Throughout life, everyone has to deal with the basic questions of morality: What is right and what is wrong? How do we know?

These questions aren't just important for us as individuals — they concern the nation too. Seeking the answers together is vital to preserving America's unity.

For generations, Americans looked to God and the Bible to guide them through moral dilemmas. However, in recent years, faith in God and objective moral standards has dwindled. People are going to church less often, the traditional family is falling apart as gay marriage spreads, and more.

Not surprisingly, society has been thrown into disarray.

In times of confusion, looking to God and the Bible may answer many of the questions we have concerning morality, where it comes from, and why we need it.

Take the question of evolution. Animals don't have morals — is it wrong for a lion to kill a lamb? Of course not — so if humans evolved from animals, how is it possible that we suddenly got morals?

People are not animals, they are able to make choices. That morality is given to us by God, not formed through evolution.

Sadly, America has experienced a concentrated effort to remove references to God and the Bible — the traditional sources of morality — from public spaces, the currency, and even from the Pledge of Allegiance.

While seeing these symbols may be offensive for non-believers, the Bible says that you cannot serve both God and man. That means America must make a choice about our source of morality: God or man?

That decision must form our societal values for the future of the nation.

### 10. Final summary 

The key message in this book:

**Liberal politics, ignorance, and excessive spending are responsible for America's decline, but every American can take practical steps to prevent this: by pushing back, becoming informed and helping others. In the end, however, America needs a vision based on a shared understanding of morality to prosper.**

**Suggested further reading:** ** _The Audacity of Hope_** **by Barack Obama**

_The Audacity of Hope_ is based on a keynote speech Barack Obama delivered at the 2004 Democratic Convention, which launched him into the spotlight of the nation. It contains many of the subjects of Obama's 2008 campaign for the presidency.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Ben Carson

Ben Carson is a celebrated neurosurgeon who is mulling a run for president in 2016. Candy Carson, the co-author, is the author's wife, and a co-founder of the Carson Scholars Fund.

