---
id: 55c8acb73864640007230000
slug: one-of-us-en
published_date: 2015-08-14T00:00:00.000+00:00
author: Åsne Seierstad
title: One of Us
subtitle: The Story of Anders Breivik and the Massacre in Norway
main_color: 9F2427
text_color: 9F2427
---

# One of Us

_The Story of Anders Breivik and the Massacre in Norway_

**Åsne Seierstad**

_One of Us_ (2015) tells the story of Anders Behring Breivik, the Norwegian terrorist who killed 77 people on July 22, 2011. Beginning with Breivik's _personal_ life and detailing the development of his extremist political views and his planning of the massacre, these blinks give you an unflinching look into the mind of the man who carried out this devastating and senseless attack.

---
### 1. What’s in it for me? Learn about the life of one of Europe’s worst terrorists. 

On July 22, 2011, Anders Behring Breivik shocked the world by carrying out the deadliest attack on Norwegian soil since World War II. His two acts of terrorism were directed at the civilian population, the government and a Workers' Youth League summer camp on the island Utøya; in total, he killed 77 people. Upon arrest, which he didn't resist, he showed no remorse.

In these blinks, you'll learn about the life of one of Europe's worst terrorists — from his unstable childhood to how he became obsessed with right-wing extremism and, finally, ended up receiving the maximum penalty for his atrocities.

You'll also learn

  * how Breivik made enemies out of everybody;

  * how he went from gaming to immersing himself in far-right ideologies; and

  * how Breivik argued for a terrorist attack in his manifest.

### 2. Breivik’s childhood was unstable. 

In 2011, Anders Behring Breivik shocked the world. He killed 77 people in a single day — the most horrific terrorist attack in Norwegian history. Why would anyone do this? Many have searched for answers by analyzing Breivik's childhood.

Breivik's early years were marked by instability and dysfunction. When Breivik was born, in 1979, his parents, Jens and Wenche Breivik, had only been together for a year, and when Breivik was six months old, his father was appointed as a counselor to the Norwegian Embassy in London, where the young family relocated from Oslo.

While her husband was busy working, Wenche mostly stayed home. It seemed to her that Jens was only interested in a well-groomed wife who could maintain a dust-free home. After six months of feeling unappreciated and lonely, Wenche chose to divorce her husband and move back to Oslo with her one-year-old son.

When Breivik was four, his mother no longer felt able to care for him and his younger sister. She asked for respite care, and the family was evaluated by psychologists. Experts concluded that the entire family was "affected by the mother's poor psychological functioning," and recommended that Anders be removed from the family, noting how "the mother is continually provoked by the boy and is locked in ambivalent positions, making it impossible [for him] to develop on his own terms."

Despite this, the family did not act on this recommendation. An appointed home visitor concluded that the conditions were not adverse enough for Anders to be taken away, and Wenche maintained custody of her son.

### 3. Breivik’s provocative behavior made him a social outsider in his school years. 

Breivik grew into a shy yet short-tempered boy; as early as preschool, he struggled to find friends. During the later years of his schooling, however, he wasn't unpopular. He found his niche in the graffiti scene.

In 1992, Breivik entered secondary education and switched to a new school. There he found three friends and a brand new hobby: tagging. He and his friends often went on tours to buy spray paint, and the four acquired a certain level of notoriety. They were caught twice by police, yet never revealed to authorities the identities of others in the graffiti scene and, for this, Breivik's crew gained respect from their peers.

And yet, because of his lack of empathy, Breivik soon found himself excluded from the scene. In the tagger scene, novices were known as "toys," while top taggers were called "kings," a title coveted by all, and especially Breivik.

An unwritten rule forbade toys from writing over the tag of a king. But Breivik did exactly that, and word of this act of spite quickly got around. He lost the respect he'd gained in both the tagging scene and at school. His actions made him an outsider. "Anders used to be part of the 'gang' but then he made enemies of everybody," wrote his classmates in the school yearbook.

In a later attempt to replenish his stock of spray cans, Breivik was caught a third time by police. Breivik's father had occasional contact with his son, but after this third arrest, he made it clear to Breivik that he wanted nothing more to do with him. Breivik never saw his father again.

### 4. Breivik was active in the Norwegian conservative party but failed to make it to the front row. 

In 1999, at just 20 years old, Breivik became a member of the right-wing Norwegian Progress Party. But it wasn't until 2002 that he realized he had an opportunity to rise in the ranks.

In that year Breivik was invited to join one of the three new local youth branches that were being established in Oslo. At the inauguration meeting, he was appointed the deputy chairmen of the Progress Party Youth, Oslo West Branch. This appointment motivated Breivik to delve into politics.

He began to build a network among the Progress Party Youth's social scene, gaining friends in high places and building a reputation for himself. But when he failed to receive a nomination for the city council elections, Breivik lost interest in the party.

Though Anders still hoped to get called later by the nomination committee, and tried to make an impression on the people responsible for the decision, he was unsuccessful. Just before Christmas of 2002, the list was finalized, and Breivik wasn't on it. Disappointed that the Party didn't see his potential, Breivik stopped attending their social events, and in 2004 he paid his last membership dues.

### 5. Breivik spent five years playing online games and surfing the web, and developed far-right ideas. 

For a time, Breivik sold fake diplomas online to earn a living; however, in February, 2006, fear of detection forced Breivik to mend his ways. Fast running out of money, he moved back to his mother's place, where online games became his new full-time occupation.

The real world, which had failed to appreciate his traits, disappointed Breivik. But in the online world, Breivik finally found the approval he had been craving. With the nickname "Andersnordic," he began playing World of Warcraft, a massive, multiplayer online role-playing game, at which he was quite good.

But it didn't take long before his hubris struck him down again. Breivik switched to another server, where only the best gamers played, since he wanted to become Nr. 1 in World of Warcraft. But during all those years of play, he was never even ranked among the top 500 on the server.

When not playing World of Warcraft, Breivik was surfing the web. In 2008, he stumbled upon a new world that caused him to stop playing altogether. Breivik began to take a serious interest in far-right websites and the ideas they promoted. He soon became immersed in websites such as document.no, Gates of Vienna (gatesofvienna.net) or Stormfront (stormfront.org), where users shared their racist and islamophobic ideas.

He was especially impressed by a user called "Fjordman," a fellow Norwegian, who was known for spreading islamophobic ideas over Gates of Vienna. While he was sitting in a room at his mother's place, plunging ever deeper into far-right ideas and conspiracy theories, Anders rarely met up with friends.

In 2009, on one of these rare occasions of face-to-face socializing, Breivik told his cousin that he was writing a book about conservatism and the Crusaders, clear evidence that he was already obsessed with the idea that Europe required a new crusade against Islam. Breivik had found a new purpose in life: the writing of his manifesto.

### 6. Breivik’s manifesto declares that the decline of values in Europe necessitates a terrorist attack. 

Influenced by the online right-wing platforms he so admired, Breivik published a compendium for opponents of Islam on document.no, in September, 2009. The document, a 1518-page rant titled "2083: A European Declaration of Independence," referenced the 1683 Battle of Vienna, in which a Christian coalition had won a decisive victory over an Ottoman army invading Europe.

Breivik devoted the first part of the manifesto to blaming various social currents for a decline of values in Europe. In his view, the values of the 1950s should be revived, including patriarchy, the condemnation of homosexuality and the sacrosanctity of marriage.

Breivik blames three things — "cultural marxism," feminism and the Islamization of European countries — for modern society's moral deterioration. He backs these views up by citing long passages from conservative and right-wing writers like Robert Spencer, Henryk M. Broder and, recurrently, Fjordman, often without crediting them.

Concluding that armed resistance is necessary if the Islamization of Europe is to be stopped, Breivik uses the last part of his book as a guideline on how to plan and conduct a successful terrorist attack.

As well as providing advice on how to build a bomb and where to buy weapons, Breivik even devotes a chapter to an interview with himself, in which he swaggeringly talks of deeds he never performed and explains in detail how he prepared for his terrorist attack.

### 7. To prepare the assault, Breivik joined a gun club and spent months on a farm abroad, building a bomb according to online instructions. 

In his manifesto, Breivik explained how he would conduct his attack. The next step for him was to prepare, by acquiring a firearms license and weapons. In the winter of 2010 - 2011, he attended shooting classes at Oslo Pistol Club without a hitch; building a bomb without getting noticed, however, was a bit trickier. 

To build the bomb, Breivik rented a farm in the Norwegian outback. Online, Breivik found various instructions for building the bomb he had in mind, shared by laboratories, amateur chemists and militant organizations such as al-Qaida.

He ordered the necessary ingredients online and in different local pharmacies using plausible cover stories, claiming that he required powdered sulphur to clean out an aquarium, for example.

As the building of a bomb required both handling chemicals and a lot of space, he needed a place where he could work undisturbed. Under the pretext that he was going to start sugar-beet production, he rented a farm in Vålstua, a hamlet north of Oslo.

The farm he rented was also his explanation for the large amounts of fertilizer he bought for the bomb. In the spring of 2011, Breivik moved to the farm and began constructing his bomb.

The instructions he had read were not always correct and it took him several attempts to create a functional bomb, but by July, 2011, he had one ready. During the time he spent on the farm, neighbors came by a few times, but Breivik was able to hide what he was doing and no suspicions were raised.

### 8. Breivik reached the island Utøya undetected due to communication failure within the Norwegian police force. 

The writing of the manifesto and the bomb-building constituted the first phase of Breivik's cruel plan. After sending his manifesto to a thousand like-minded people via e-mail, and completing the bomb's preparation, Breivik started the second phase on July 22, 2011.

Not all went smoothly, however. While placing the bomb, Breivik was spotted. To transport and place the bomb, Breivik had rented a delivery van. Dressed in a police uniform, he parked the van right outside of the building that housed the Ministry of Justice and the Prime Minister's office. Breivik primed the bomb and then entered another van, which he had parked nearby. One man noticed Breivik and wrote down the van's registration number to report his strange behavior to the police.

Unfortunately, communication problems prevented an immediate manhunt. Nine minutes after Breivik lit the fuse, the bomb exploded, instantly killing 8 people, and nine minutes after the explosion, the man who had previously spotted Breivik passed along the van's registration number to the police.

The operator realized this was an important tip and jotted it down on a Post-it note. The operator put the note on the desk of his supervisor, who was busy on the phone. As the supervisor failed to notice it, the note lay there for 20 minutes, delaying its immediate tracing.

When they finally were informed of the suspicious van, patrol cars in the relevant areas chose to ignore the order and carry on with their regular duties. 78 minutes after the explosion, a nationwide alert was finally sent out. But the police station in the district where Breivik was headed never received that alert for a horrifyingly simple reason: the computer was on energy-save mode and failed to display any notifications.

### 9. A series of major mistakes by the police gave Breivik 75 minutes alone on Utøya, where he killed 69 people. 

The Norwegian police were unprepared for an attack of this magnitude, making it possible for Breivik to reach his final destination, an island called Utøya. Sadly, the series of mishaps didn't stop when he started shooting down innocent people on the island.

When Breivik reached the ferry that connected the island with the mainland, the officials in charge had already been informed of the explosion and had cancelled all scheduled transports to the island. But Breivik, still dressed in the police uniform, told them that police were doing a routine check in various locations to make sure nothing more would happen, and persuaded them to conduct a ferry crossing with him.

On the island, the Norwegian Social-Democratic Youth League had their annual meeting. For Breivik, they represented the traitorous "cultural marxism" that he so despised. Upon arriving on Utøya, Breivik gathered the youngsters around him and, without warning, began shooting them. He killed 69 people, most of them teenagers, before police arrived.

Fatal mistakes prevented the prompt arrival of a strike force. When the first two policemen arrived at the jetty on the mainland, they were ordered to wait for reinforcements instead of being allowed to cross over immediately. This went against official regulations, which require immediate intervention in case of a shooting.

The vanguard told the reinforcements to meet at the jetty next to the island, but the reinforcements got the information wrong and went to a jetty three kilometers away. The boat that took the police over was weighed down with heavy equipment and the engine gave out, delaying them further.

After 75 minutes, when the police finally arrived, Breivik surrendered without resistance. His attack had been a success and he was now eager to show the world who he was.

### 10. Breivik showed no remorse during his trial, instead using it as a stage to present his radical ideas. 

The writing and distribution of his manifesto was the first phase of Breivik's operation; the attack itself was phase two. Phase three was his trail, where he used the public lawsuit as a stage to remorselessly promote his worldview.

Hundreds of media outlets were accredited to cover Breivik's trial, which started in mid-April and ended in mid-June of 2012. For Breivik, the court was a stage, and the very first thing he did upon entering it was to raise his arm in a right-wing extremist salute.

After his charges were read aloud, Breivik was permitted to speak. He admitted to committing the actions but denied his guilt. Instead, he pleaded the principle of necessity. When prosecutors outlined Breivik's life and his preparations for the attack, they screened a video he had uploaded to YouTube in which he promotes his worldview. Watching it in the courtroom, Breivik's eyes welled with tears. A psychologist interpreted these tears not as remorse, but as sign of Breivik's love for himself.

After the arraignment, Breivik read out a document that was what he called the framework of his defence. The speech he then gave was essentially a summary of his manifesto. He claimed that a resistance movement was necessary to rescue Europe from the rule of Marxism and Islam, even declaring his attack the "most sophisticated and spectacular in Europe since the Second World War."

As prosecutors spoke about the victims that Breivik had killed or injured, he merely looked down at his papers silently. Allegedly, Breivik claimed to acknowledge the pain and grief he had caused, but he did not apologize during the trial.

### 11. Though Breivik’s sanity was debated, he was ultimately found legally responsible for his actions. 

Looking at Breivik's actions and his manifesto, the question of whether he was sane or not was a crucial one for the judge, and would determine whether Breivik ended up in prison or not.

The first report deemed Breivik not responsible for his actions. Prior to the trial, a pair of psychiatrists met Breivik to judge whether he should be deemed insane. They observed that Breivik believed he knew what other people were thinking, a phenomenon often tied to psychosis. He also viewed himself as the focal point of all events and assumed that psychiatrists envied his expertise, signs indicative of dangerous delusions of grandeur.

Additionally, Breivik lacked a clear perception of his own identity, and instead used his own invented words and concepts to describe himself, such as "suicidal Marxist" — another indicator of psychosis. These psychiatrists concluded that Breivik suffered from paranoid schizophrenia and could therefore not be held accountable for his terrorist attack.

However, a second report concluded that Breivik was indeed legally responsible. Interestingly, it was this report that Breivik himself supported, though the former would have helped him avoid jail time. Fearing that his manifesto would lose credibility if he was declared insane, Breivik demanded a new report and, in February, 2012, another pair of psychiatrists analyzed him.

They posited that Breivik suffered from antisocial personality disorder with narcissistic traits. As this disorder is not associated with psychosis, Breivik could be held legally accountable for his actions. On August 24, 2012, Breivik was convicted and sentenced to the maximum penalty: 21 years detention in custody, a sentence that can be extended as long as he poses a threat to society.

### 12. Final summary 

The key message in this book:

**Breivik's story is a chilling one, from his unstable childhood, troubled youth and controversial online activity that led him to commit the worst terrorist attack in Norway's history. Perhaps even more frightening are the fatal mistakes made by an ill-prepared Norwegian police force. Ultimately, it is cases like Breivik's from which we must learn, so as to avoid them in the future.**

**Suggested** **further** **reading:** ** _ISIS_** **by Michael Weiss and Hassan Hassan**

_ISIS: Inside the Army of Terror_ (2015) charts the rapid rise of the Islamic State in the Middle East, from its early beginnings to its self-proclaimed caliphate in Iraq and Syria. Grippingly told, the story of ISIS's domination over al-Qaeda in Iraq and its slow but ruthless push in Syria also shines light on the failings of the West in dealing with this fanatical yet disciplined jihadi group.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Åsne Seierstad

Åsne Seierstad is a Norwegian journalist and writer best known for her work as a war correspondent. In addition to receiving the Peer Gynt and Den Store Journalistprisen, the highest honor that can be bestowed on a reporter in Norway, she is the author of _The Bookseller of Kabul_, _A Hundred and One Days: A Baghdad Journal_, and _The Angel of Grozny: Orphans of a Forgotten War_.

