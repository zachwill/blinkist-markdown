---
id: 530c9be634623100089f0000
slug: one-simple-idea-en
published_date: 2014-02-25T08:00:00.000+00:00
author: Stephen Key
title: One Simple Idea
subtitle: Turn Your Dreams into a Licensing Goldmine While Letting Others Do the Work
main_color: F9E232
text_color: 7A6F18
---

# One Simple Idea

_Turn Your Dreams into a Licensing Goldmine While Letting Others Do the Work_

**Stephen Key**

_One Simple Idea_ (2015) argues that it takes only passion and one simple, marketable idea to start your own business. The book shows how building long-term partnerships with your manufacturers and vendors can help your business run smoothly, and how catering to your customers' needs and desires helps you sell your product on the market. In short, _One Simple Idea_ gives concrete, firsthand advice for anyone who wants to get their one simple idea off the page and out into the world.

---
### 1. What is in it for me? Learn how to start your business with only one simple idea. 

Have you ever seen a product and thought, "I could do better than that"? Or, while browsing the store shelves, been suddenly struck with a great idea that will fill a gap, and think: "Why hasn't anyone invented this yet?" No matter what your inspiration or motivation is, now's the time to make something out of your idea.

_One_ _Simple_ _Idea_ will teach you how to turn your passion into a business and bring your product to market yourself. The author provides a roadmap to guide you through the sticky terrain of developing your one simple idea into a fully realized successful business, providing you with tools and useful advice along the way.

This book in blinks answers the following questions:

  * What does it take to start my own business?

  * How do I bring my product into the market? 

  * How will I be able to grow and manage my business?

### 2. Seize the opportunity to bring your idea to the market, and find an experienced mentor. 

You might think that, given the recent economic downturn in the U.S., now would be a bad time to create a product and attempt to bring it to market.

But you'd be wrong. Even during a financial slump, consumer spending in the U.S. accounts for 60-70% of the national economy.

The fact is, there will always be certain products that consumers will find irresistible. As an entrepreneur, your main task is to identify such products, then use the right channels to promote them.

The products that sell even in a dismal economy are those which improve customers' quality of life without breaking the bank, because consumer spending is motivated by _desire_, not by necessity.

And when it comes to selling your product, the predominance of the Internet, especially social networks, means that nowadays you can easily work out of your home, selling your product online without having to fight for a place on store shelves.

Furthermore, social media can help you to connect with your customers, to understand and address their needs and desires, and — because you're closer to your customers — to react swiftly to the whims of the marketplace.

But even with the above in your favor, you'll still need a lot of guidance. So find yourself a mentor who can give you some practical, firsthand advice — ideally someone who's already successfully launched a similar product.

How do you find one? Begin by looking in your hometown, or search the web for successful entrepreneurs from your region.

Once you've found a suitable candidate, get your foot in the door by expressing interest in what they've achieved, even massaging their ego a little. Most people enjoy talking about themselves and will welcome the satisfaction that helping you to succeed will bring them.

There are many opportunities and people out there to help you get your business started. Now you have to grab the chance and start with one simple idea.

### 3. Be passionate about your idea and ready to take full responsibility for your business. 

Starting a business for the first time can be scary, and you might even find yourself wondering if you're cut out to be an entrepreneur.

But no one is born an entrepreneur. Like other apparently innate "gifts," being an entrepreneur is actually learned, and the first step is to make yourself aware that starting your own business requires, above all, passion for your idea and unyielding persistence to get that idea off the ground.

Fortunately, passion drives persistence: if you have passion for your idea, and for your customers and work, it will be so important to you that you'll be extremely motivated to promote it and share it with everyone — which, of course, will help you to sell your product.

Also, passion helps you to face the risks and survive the steep learning curve that starting a business always presents. Believing in your idea will spur you to be innovative and opportunistic whenever the business needs it, and to motivate you to learn from the mistakes all businesses make when they are young.

Equally importantly: passion for your idea will drive you to go further and try even harder, so that, finally, you will have transformed your great-but-embryonic idea into a full-fledged successful business.

Clearly, passion is essential to starting a successful business. But it's not sufficient. In addition to having passion, you need to be prepared to take full responsibility for your business.

This means that, when starting a new business, you must stand behind every decision and mistake you make and deal with the consequences. As leader of your very own enterprise, it's your job to lead your employees in the right direction — after all, you're the boss.

Finally, taking full responsibility means working hard, usually for long hours, because you have to juggle many tasks and constantly deal with the kinds of changes and surprises involved in getting a business off the ground.

### 4. Your idea has to be simple, marketable and profitable. 

One of the main reasons startups fail is because their idea is too ambitious and too complicated. So, if you want your business to succeed, you should start small and keep it simple — and the simplest way is to make a small modification to an existing, successful product.

This offers a number of benefits:

First, an established product serves an existing demand, so you don't have to educate the market about its potential value.

Second, you can target a different market niche, by adding a unique feature. For example, CelebriDucks added images of celebrities to the rubber duck, transforming a children's toy into an adult collectible.

Finally, by making your product similar to an existing one, it'll be more likely to find a manufacturer who can produce it at a reasonable cost.

But in order to create a sellable product, you first need to study the marketplace.

First, get an overview of the market you want to enter, analyze its trends and major players, and learn your customers' lifestyles and desires. Next, find your unique selling point by solving a common problem or unmet demand in a new way. Once that's done, test your idea on the market. This will give you the chance to modify your product based on customer feedback.

After you've gotten to know the market, you need to find out as early as possible whether your idea is a potential "money-maker." This involves ensuring your product will sell for a certain price and in a high enough volume, and that the costs to produce it will still give you the desired profit.

To estimate your retail list price, one simple method is to multiply your per-unit production cost by 5. For example, if your manufacturing and packaging cost is $2 per unit, your retail price should be around $10.

All that's left to do is find out whether the price you need to charge to be profitable is a price customers are willing to pay.

### 5. Write a short, focused business plan, and start small and simple. 

So you've developed your product idea and gotten to know your market. What now?

First, it's time to make a business plan. This is an invaluable step in setting up a profitable business as it will force you to describe your business clearly and concisely: what you're doing, how you're doing it and why. It will also help you to forecast your financials — i.e., projections about revenues and expenses — and thus ensure that your business will be profitable. And if you're looking for external financing, you'll need a business plan to present to potential investors and to get feedback on your business concept.

Second, you need to start small and simple with your business.

You don't need to spend a lot of time or money in order to establish a professional business entity. You can even start by working from home, as many entrepreneurs before you (but don't use your "home office" for anything else, and make sure you get yourself a business address, like a PO Box). And the only equipment you need to get started is a phone line, a computer with Internet access and, of course, a website.

For tax and legal purposes, you need to set up your business entity and decide which structure fits you the best. For example, becoming a limited liability company or a corporation has the advantage of protecting your personal assets, while a smaller and simpler legal entity saves you a lot of time and effort.

Starting your own business doesn't have to wipe you out financially. With a thorough business plan and a simple set-up your business is ready to launch.

### 6. Protect your business idea by outsmarting the competition and building partnerships. 

If you care about your business idea, then it's natural to be afraid that someone might steal it.

Fortunately, however, there are several ways to protect your great idea from the competition.

While patenting is probably the most famous method, it's not always the best. For a start, it involves withholding your idea from people who could help you realize it and bring it to market. Also, the patenting process is costly and takes some time to complete — and time is precious when you need to get a jump on the market.

So what are the alternatives?

First, since trying to beat big corporations at their own game is hard, you can outsmart the competition by designing something unique. That way, you can claim and defend your Intellectual property rights.

Second, use your smallness as an advantage. As a small company, you're more flexible than a big company, and can therefore bring your new, great idea to the market before they can. Remember: the first to market usually has a huge advantage, so speed is one of your best allies.

Finally, keep the creative juices flowing even if your first idea is a market success; you need to stay current to stay ahead of the pack.

Another way to protect your idea is to build relationships with everyone you work with. Forming relationships with some of the major players can convert them from competitors into allies — especially if you offer them a business opportunity. For example, Hot Picks eliminated one of their leading competitors, Dunlop, by contracting them to produce their guitar picks.

Building strong relationships with your retailers is equally essential. They are on the front line and can provide great protection for your business if you provide them with great products, service and competitive prices.

Being quicker than the competition and keeping good relationships with your partners will give you protection and peace of mind before you introduce your product to the market.

### 7. When launching your business, try to use your own money and get whatever “free” money you can. 

Even if you start small and simple, at some point you'll need cash. Of course, there are many funding options available to you. The simplest way, however, is to use your own money and avoid any external obligations as far as possible.

This is because people tend to work harder to keep their own money; they don't spend it as freely as borrowed money and tend to have more control over it. After all, would you spend your own money on a business-class flight without a second thought?

Another reason to use your own money is that you won't allow as many external constraints or as much external pressure to build, which gives you more freedom to run of your business as you see fit.

Of course, this advice has its limits: no one would recommend liquidating all of your assets and depleting your savings to get your business off the ground.

So what's an alternative source of funding that won't put too much pressure on your business?

"Free" money.

"Free" money — for instance, via crowdfunding or grants — helps you limit the amount of initial debt you generate, so you can bootstrap until some cash comes flowing in.

Crowdfunding means that a group of individuals provide small amounts of money to a startup. You can promote your idea via online crowdfunding platforms, which reach a large number of potential contributors. Non-equity crowdfunding is particularly attractive, since it means you literally get the money as a gift.

Grants have the advantage that they oblige you only to demonstrate that you need the money and explain how it will be used for your business. Grants are available to help with both startup costs and growing your business later on.

So, to keep it simple and finance your business without a mountain of debts in the beginning, take all the "free" money you can get your hands on, and start growing from there.

### 8. Find the right manufacturer and set up your supply chain carefully. 

If you can't make your product at the right cost, you're not going to make a profit. It's essential, therefore, to find the right manufacturer. Most contract manufacturers specialize in a certain area, so some might have the experience and expertise you need to produce your product.

To find the right manufacturer, you need to vigorously investigate those you're considering, by, for instance, getting a referral from someone who produces a similar product to yours, and using due diligence when evaluating other potential manufacturers.

Investigate the most promising manufacturers thoroughly by getting into direct contact and, if possible, visiting the facilities.

Since you'll need to make changes to your product, especially in the beginning, it's best to choose a manufacturer based in your home country. It's much simpler to communicate and negotiate with such a manufacturer than with one overseas, as that would involve using a sourcing agent.

To help you narrow your choices down, ask potential manufacturers for a quotation to check out their prices, capabilities and services. Then choose the option that suits you best.

So you've decided on a manufacturer. What's next?

You need to set up your supply chain. In order to collaborate with your suppliers and partners to reduce costs and improve efficiencies, it's crucial to build tight relationships with them so that they can keep you updated about any issues affecting your product, as well as to understand every link in your supply chain.

Once you finally do start selling, keep a tight rein on your inventory. At the start, you should have a certain amount of products in stock to serve your retailers on time, and you should also organize your inventory according to metrics, such as: "How long does it take to receive the products from my manufacturer from the day I submit the order?"

Keeping on top of your manufacturers and your inventory is crucial, and it's much easier if you start small and stay close to the process.

### 9. Pull the market and build relationships with your customers. 

In today's consumer-centric, highly-connected world, traditional so-called "push marketing" is not the most promising strategy for small businesses. Instead, what startups need is to understand their customers and to make their products all about them. In other words, they need "pull marketing."

Nowadays, marketing is not about selling a product but providing value to your customers. Small companies have to let their customers know that they are able to give those customers with what they need and want.

But earning people's attention takes time, and new, small businesses don't have enough money to spend on expensive advertising. So instead of costly print ads, it's better to make use of cheap or free-of-charge marketing channels, such as websites, social media pages and even Youtube.

To make the most of these channels, it's a good tactic to incentivize word-of-mouth marketing as this will spread the news of your product quickly. Moreover, it's important to reach out to the most influential people and give them something of value to earn their trust; they will tell the rest of the people how great you and your product are.

But targeting and building your customer base isn't enough. You also need to create a strong brand that appeals to your customers' lifestyles and needs.

Your brand must communicate what your product is all about and what makes it unique. For instance, your brand identity — company name, logo and tagline — has to fit to your product and appeal to your target audience. For example, people typically expect the colors of eco-friendly products to be light green or blue, rather than black or gold.

Also, you can communicate your product's value to your customers by choosing a catchy, suggestive and percussive _tagline_ ( _slogan_ or _brand_ _tag_ ).

As you see, you don't have to spend lots of money on marketing — you just need a simple idea that customers want, and the ability to connect with them and provide personal value for their money.

### 10. To succeed in retail, place your product in the right stores, and create happy customers through great customer service. 

To sell your product successfully, you have to get it into the stores. But which?

Fortunately you're spoiled for choice: in the U.S. alone there are 1.5 million retail stores, collectively making more than $2.5 trillion each year.

The best way forward is to start small with orders from a few local and regional stores.

Starting small in this way is important since big stores want to stock only those products they're certain will sell. Moreover, they want to be supplied in fairly large volume at the outset.

Placing your products in such stores also means you can stay in close communication with each store and keep track of how your product is selling. It also frees you to make changes to your product based on customer feedback.

And if you succeed in getting repeat sales, these stores will continue to stock your product.

So your product is on the shelves, and it's selling well. Now you're going to want to hold on to those customers, and give them a reason to keep coming back.

How?

The simplest way is to provide great, personal customer service.

As a small business, you can create one-on-one relationships with your customers, giving you a massive advantage against big companies who can't provide a service that personalized. Such a relationship ensures your customers' satisfaction, and gives you the opportunity to learn even more about your target group to further improve your offers.

Also, this kind of customer service means that a large amount of your customers will be motivated to ask stores for your product, which in turn will give retailers a reason to increase the size of their orders.

Great customer service means being fully available to your customers, and making good on your promises. If you provide a great product and service, your customers and thus the stores will keep coming back and spread the word to get you even more sales.

### 11. Keep track of your business by carefully monitoring your finances and inventory. 

If you succeed with your business idea, at some point your business will become large, busy and complex. As the captain of the company, it's your job to steer the ship, and the most important levers are your financials and your inventory.

Managing a business's finances is a crucial factor in managing that business effectively. This requires that you can pay the bills, so keep your finance records up-to-date and review your monthly reports carefully. You should also negotiate your vendors' payment terms and incentivize your customers to pay on time so that you're always able to pay on time, too.

Finally, while it's normal for a startup to have only a small profit margin for the first couple of years, or even to make losses, if you're not breaking even after two years of work you need to evaluate how you're running the business.

Another way to keep track of your now-established and successful business is good inventory management, as this will ensure a steady cash flow.

The main consideration here is to ensure you produce enough of your product to fulfill incoming orders immediately. At the same time, you should beware of overproduction: you don't want to pay for the production and storage of products you can't sell.

For example, if you've managed to reach the point where you're supplying a big retailer, such as Walmart, you'll have to scale up your business and build inventory ahead of time so that you're able to ship immediately.

Another method to ensure that there's always enough cash available is by reducing the lag time between your payables (to suppliers) and receivables (from customers).

As you can see, keeping an eye on your cash flow will help you manage your inventory by enabling you to better coordinate your production with your customer orders.

The bottom line for your business to survive is that its finances are managed well and that a sufficient amount of cash is always available.

### 12. Before expanding your business into a new area, make sure it’s the right fit and be ready to sell it when the time comes. 

When successfully growing a startup, you (like most entrepreneurs) will be itching to further expand your brand.

But you should be aware that this entails an increased effort in managing your business relationships, monitoring the quality of your products, and even involves hiring new people. You should therefore be careful about expanding your business into new areas, and first make sure that it's the right fit for your brand.

The area you want to expand into should complement your core business. For instance, if you want to launch a new product, you should make sure that it appeals to the same market. Otherwise, because your customers will be different, it will be like starting a completely new business.

Consider Hot Picks, for example. Licensing other characters and celebrities like Disney and Taylor Swift worked for them because it was closely related to their core business. The only difference was that the new product design appealed to an additional sub-audience of their target group, which had a different lifestyle.

What if you're considering selling up at some point? To prepare yourself for this, you should structure your company in such a way that you can simply hand it off to someone else when you're ready.

This means that your brand has to work and sell without you. So, while setting up your business and building the brand, imagine that you're writing a playbook that someone else can follow in your absence. However, you should remember that increasing your sales is ultimately what will make your business more attractive to potential buyers.

### 13. Final summary 

The key message in this book:

**You don't need millions of dollars in funding nor a solid background in business to build a profitable business. All it takes is one simple idea you're passionate about. Also, when setting up your business, start small, grow slowly and keep it simple. Finally, use the advantage of being small and flexible and try to build personal relationships with everyone who has a stake in your business.**

Actionable ideas in this book:

**Use social media to target and learn about your customers**

When starting your own business, use social media networks offer to study customers' needs and reach your target group with a product they really want. Also, because you will probably have a very small advertising budget, social media offers an increasingly effective alternative to expensive conventional advertising.
---

### Stephen Key

Stephen Key is an expert on getting business ideas off the ground and has successfully licensed more than 20 simple ideas, generating billions of dollars of revenue. In _One Simple Idea_, Key shares the lessons he learned while starting, managing and growing his own small (later successful) company, "Hot Picks."

