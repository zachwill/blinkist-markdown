---
id: 562e33ea38643100074f0000
slug: one-summer-en
published_date: 2015-10-29T00:00:00.000+00:00
author: Bill Bryson
title: One Summer
subtitle: America, 1927
main_color: EAB598
text_color: 856756
---

# One Summer

_America, 1927_

**Bill Bryson**

_One Summer_ (2013) tells the story of the summer of 1927, a particularly pivotal three months in American history. The summer of 1927 marked the emergence of the United States as a major power on the international scene and set the stage for the Great Depression of the '30s. _One Summer_ takes a closer look at a number of 1927's important events, such as Charles Lindbergh's famous flight across the Atlantic Ocean, Babe Ruth's recording-breaking 60 home runs in a season and the execution of Italian anarchists Sacco and Vanzetti.

---
### 1. What’s in it for me? Learn how the summer of 1927 helped shape American history. 

Many of us have a memory from our childhood or adolescence of a magical summer that changed everything. But could the same thing happen to an entire country? In hindsight, the summer of 1927 seems to have been exactly that sort of pivotal moment for the United States. 

Many of the culturally significant events that would help mold the nation into what it is today happened in 1927, and it was a time of affluence that had never been seen before. The summer of 1927 signalled the dawn of American consumerism and global power, but also held early omens of the bad times that would roll in with the Great Depression.

In these blinks, you'll discover

  * why Charles Lindbergh's flight across the Atlantic saved US aviation;

  * how almost half of the world's products in 1927 were manufactured in the United States; and

  * why the 1920s should also be known as the Age of Loathing.

### 2. The United States was behind the rest of the world in aviation until Charles Lindbergh’s flight across the Atlantic. 

It's hard to imagine the United States without planes. The United States Air Force is a global powerhouse, red-eye flights take people from coast to coast every night and one of the largest airplane producers in the world is American. But this wasn't always the case. 

Airplanes were rare in the US military before 1914, but the outbreak of World War I showed military leaders how useful they really were. Airplane production and use increased exponentially during the war; planes were used for monitoring enemy troops, directing artillery fire and dropping aerial bombs. 

Civilian aviation increased after the war, but only in Europe. Flights became widespread in post-war Europe, with Germany safely transporting 151,000 passengers by 1927, France operating nine airlines and British airlines racking up nearly a million miles per year. 

Following the war, Americans didn't develop their aviation at the same rate; passenger air services didn't exist at all until the spring of 1927. 

But Charles Lindbergh's flight across the Atlantic completely changed things, especially as it prompted greater interest in aviation among everyday Americans. 

Lindbergh set out from New York in his plane, _The Spirit of St. Louis_, on May 20th, 1927 and landed in Paris the next day. He became the first person in history to be in the two cities on back-to-back days, which earned him the highly coveted _Orteig Prize_ in aviation. 

Lindbergh's extraordinary accomplishment captivated the world's attention and imagination. His fame helped promote commercial air services, but also aviation in general. In fact, it spurred around $100 million in aviation investments in the United States. 

Within a year of Lindbergh's flight, the airplane manufacturer Boeing grew from being a small company in Seattle to employing a thousand people.

### 3. The United States’ cultural influence and national pride increased in 1927, thanks largely to the film industry and the celebrity of Babe Ruth. 

Before 1927, even Americans generally felt that the most exciting things on earth were happening in Europe — but that one year seemed to change everything. 

One important change was the rise of the American film industry, which greatly expanded the United States' international and cultural influence. In 1927, the motion picture industry in Hollywood made Los Angeles the country's fastest-growing city and the richest American city per capita. Hollywood was producing around 800 feature films per year by 1927 — 80 percent of the world's total output. 

Film was the United States' fourth-largest industry, but Hollywood still struggled financially. Its salvation would be the 1927 film _The Jazz Singer,_ the first film ever to have sound. _The Jazz Singer_ heralded the end of the silent film era and marked the arrival of _talkies_ — movies with dialogue and sound. 

Talkies were a truly American development. They popularized American speech, thoughts and attitudes on a global scale and played a key role in increasing the United States' influence around the world. 

Another new cultural phenomenon that boosted American pride was Babe Ruth, the baseball hero. Babe Ruth symbolized baseball, celebrity and the United States itself. No athlete had ever been so beloved — some newspapers even ran a daily column called _What Babe Ruth Did Today_!

In 1927, Babe Ruth was playing for the greatest team in the country, the New York Yankees. That summer, he hit an unbelievable record of 60 home runs in a season, after already setting and breaking the previous single-season records of 30, 40 and 50. 

Ruth alone hit more home runs than all the other national league teams except for the Giants, Cardinals and Cubs. In the winter of 1926-1927, he made nearly a quarter of a million dollars from newspaper columns, product endorsements and his film, _Babe Comes Home_.

### 4. In 1927, the United States was plagued with social problems like racism, xenophobia and paranoia. 

The 1920s have a number of cheerful nicknames: the _Jazz Age_, the _Roaring Twenties_, the _Age of Ballyhoo_. But despite the lightheartedness of these nicknames, the '20s were by no means a happy time for everyone in the United States. In fact, the _Age of Loathing_ might be a more fitting label for the decade as a whole. 

The 1920s were filled with racist fears of outsiders, and fears of left-wing politics as part of the _Red Scare_. 

The _Espionage Act of 1917_ and the _Sedition Act of 1918_ set severe penalties for anyone who displayed "anti-American" sentiments. This extreme nationalism, combined with a steady increase in immigration, led to rampant xenophobia. 

Italians, for instance, were seen as fascists, Bolsheviks, anarchists, communists or leaders of organized crime. Nicola Sacco and Bartolomeo Vanzetti, two Italian anarchists, were accused and convicted of murder in Massachusetts. They were executed in 1927 and their death sparked great controversy, as many suspected they had been innocent. 

Racism was also alive and well. Bert Williams, a black comedian, was wealthy enough to afford a luxury Manhattan apartment, but he was only allowed to live there if he used the side service entrance and elevator. 

The popularity of the theory of _eugenics_ likewise illustrates the pervasive racism of the time. Eugenics was the idea that the United States was becoming dangerously overpopulated by inferior people, such as the mentally and physically disabled, orphans, gays and promiscuous women. According to eugenics, these people would ideally be sterilized so that their genes wouldn't be passed on. 

The physician Dr. William Robinson summed up the sentiment behind eugenics when he said that people with so-called "inferior natures" would have "no right in the first instance to be born, but having been born, they have no right to propagate their kind."

Eugenics was praised in publications like the _Yale Review_, and at least 60,000 people were sterilized under the direction of Princeton-educated Harry H. Laughlin.

> _"Americans take more care over the breeding of cattle and horses than of their own children." - American Eugenics Society_

### 5. The United States’ economic wealth was unprecedented in 1927, but it relied heavily on borrowing. 

The United States is an economic powerhouse today, but did you know that it was even richer 100 years ago?

The United States was staggeringly wealthy in 1927 and was home to the most prosperous citizenry in the world. Of the nation's 26.8 million households, 11 million had a phonograph, 10 million had a car and 17.5 million had a telephone. A remarkable 42 percent of all the world's products were produced in the United States at that time. 

In fact, the United States held half the world's gold — at a time when gold reserves were the basic marker of any nation's wealth!

This wealth also stimulated the growth of the stock market, which rose by a third in 1927 alone. No country in history had ever been so affluent. 

The United States' economic success went hand-in-hand with the rise of consumerism. With the advent of installment plans and widespread borrowing from banks, "buy now, pay later" became the nation's motto. Nearly everyone was able to invest in stocks and purchase all kinds of goods. 

Products like refrigerators, radios, telephones, electric fans and electric razors became staples in American homes long before they were in other countries. 

However, the country's increased borrowing ended up contributing to the stock market crash and the Great Depression. Much of the United States' continued growth was dependent on the exploitation of innocent investors: stocks could keep going up as long as there were fresh buyers to push the prices higher and higher. 

The stock market bubble was also fueled by banks borrowing excessively from the Federal Reserve. They would borrow at four to five percent interest, only to lend the money to brokers at 10 to 12 percent.

### 6. Economic policies set in 1927 created a bubble that, when it burst, caused the Great Depression. 

The mood of the 1920s in the United States was set by President Calvin Coolidge, who only worked four hours a day and was famously uninterested in matters of state. 

President Coolidge's laid-back attitude made it easy for questionable policies to be implemented. Many such policies advanced private interests but eventually ended up undermining the national economy. 

Andrew Mellon, as Coolidge's Secretary of the Treasury, oversaw tax cuts that more than doubled his own wealth. During his term in office, Mellon's personal net worth reached $150 million, while his family's wealth surpassed $2 billion. He even had the Internal Revenue Service, or IRS, send its best men to prepare his tax returns and limit his taxes as much as possible.

Moreover, in 1927, international bankers met with the Federal Reserve and pressured them to lower their interest rates. They hoped to help Europe's struggling economy by doing so, but inadvertently laid the groundwork for the Great Depression instead. 

Business leaders often manipulated share prices at the expense of innocent investors, further contributing to the market bubble. The inevitable burst illustrated the high costs of consumerism, naïve optimism and reckless borrowing. 

Coolidge was spared from the resulting troubles, however. In the summer of 1927, he stunned the country when he announced that he wouldn't run for another presidential term. As a result, the stock market crash of 1929 took place during the presidency of Herbert Hoover.

In the three years that followed the crash, unemployment rose from three percent to 25 percent, and average household earnings fell by 33 percent. Industrial production dropped by 50 percent, while the value of the stock market as a whole fell by 90 percent.

### 7. Final summary 

The key message in this book:

**The summer of 1927 in the United States was a landmark moment in its growth and development as a nation. The country was optimistic, successful and busy forging a uniquely American identity, but was also plagued by racism, xenophobia and reckless economic policies that paved the way for the Great Depression. Although it was both an exciting and dark period, the summer of 1927 ultimately changed the course of the country and the world at large, and had a major impact on the United States' modern-day character.**

**Suggested further reading:** ** _A_** **_Short_** **_History_** **_of_** **_Nearly_** **_Everything_** **by Bill Bryson**

_A_ _Short_ _History_ _of_ _Nearly_ _Everything_ offers an enlightening summary of contemporary scientific thinking relating to all aspects of life, from the creation of the universe to our relationship with the tiniest of bacteria.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Bill Bryson

Bill Bryson is the best-selling author of over a dozen books, including _The Mother Tongue, Notes From a Small Island_ and _A Short History of Nearly Everything_, which won the 2004 Aventis Prize.

