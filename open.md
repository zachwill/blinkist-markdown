---
id: 59f7bd60b238e10006f6080a
slug: open-en
published_date: 2017-10-31T00:00:00.000+00:00
author: Andre Agassi
title: Open
subtitle: An Autobiography
main_color: 504B25
text_color: 504B25
---

# Open

_An Autobiography_

**Andre Agassi**

_Open_ (2009) is a revealing account of the turbulent life of one of America's all-time best tennis players. More than anything else, it's a story of Agassi's battle for balance and self-understanding, all while dealing with the constant stream of complications that arose from fame and public scrutiny.

---
### 1. What’s in it for me? Take a peek into the life of a remarkable tennis player. 

It's a common belief that those who reach the highest level of fame and success must have an inherent and extraordinary passion for their field. But this couldn't be further from the truth in the case of Andre Agassi, who first burst onto the scene as a rebellious tennis champion from Las Vegas.

In these blinks, you'll learn about the unusual life trajectory of this Grand Slam master who'd rather have been penning poetry than playing tennis, and who didn't know how to handle the fame that came from receiving tennis's highest honors.

From his early tennis grooming to his endeavors to help at-risk children improve their lives, you'll get an insight into the highs and lows of Agassi's celebrated career.

You'll also learn

  * how many tennis balls seven-year-old Agassi was forced to hit every day;

  * why he was taunted for being all style and no substance; and

  * how Brooke Shields helped him embrace one of his biggest insecurities.

### 2. Andre Agassi’s childhood was difficult and dominated by an overbearing father who forced him to practice tennis. 

Andre Agassi has been blasting back tennis balls for as long as he can remember. At the age of seven, Agassi faced off every day against a machine that his father had built in their backyard in Las Vegas, Nevada. Agassi called this contraption "the dragon." It growled and bellowed smoke as it shot out tennis balls like a cannon. Agassi faced up to 2,500 balls a day as his father barked directions at him from behind: "Hit harder!" "Hit earlier!"

In these circumstances, it's easy to understand why Agassi hated the dragon and the sport of tennis. But Agassi's father wasn't just determined for his son to succeed. He was prone to violence as well. Agassi didn't dare resist.

An image of his father's aggression is still etched in Agassi's mind. One day Agassi senior, in a fit of road rage, pulverized another driver with his fists and left him unconscious in the middle of the street. Would he die? Or would another car accidentally run over him? All this had happened simply because the other driver had honked his horn.

The pressure was palpable. His father's demands and expectations had a source. He himself had dreamed of becoming a famous tennis player. As a young child in Tehran, Agassi's father had watched British and American soldiers playing the game. In fact, he'd even acted as a ball boy for them.

Sadly, there weren't any other kids with whom he could play tennis, and Agassi's father took up boxing instead. He even represented Iran in the 1948 and 1952 Olympics.

He never won a medal himself, but he was damned sure his son wouldn't be short of them. He wanted his son to become the best in the world.

> "My arm feels like it's going to fall off. I want to ask, How much longer Pops? But I don't ask. I do as I'm told."

### 3. Agassi’s professional career began after a rebellious stint at an oppressive tennis academy. 

When Agassi was just nine years old, he famously hustled legendary football player Jim Brown in three straight sets. But that was nothing compared to what happened a few days later. Agassi was beating his father 5–2 in the first set when Agassi senior suddenly abandoned the match.

Playing against older players was nothing new to Agassi. He'd already been playing in junior tournaments against older kids for a year by the time he beat his father.

The pressure continued. Agassi wasn't even allowed a proper education: at 14 he was enrolled at a tennis academy in Florida, the Nick Bollettieri Tennis School.

It was more like an army barracks than a school. Children aged between seven and 19 slept in rows of uncomfortable bunk beds on the rare occasions they weren't forced to practice tennis. They were cut off from the outside world.

The food was bad, and they often went unsupervised. In this environment fights were common. Once, a couple of kids traded punches after a racial slur was used. One had his jaw broken in return for his wit. But no one was disciplined.

However much Agassi dreamed about escaping his father's control, it was an impossibility. Within a year of being at the school, he started to rebel. He started drinking, smoking weed, wearing jeans during tennis practice and even styled himself a pink mohawk.

Bollettieri, the school's founder, was forgiving. He let Agassi act up because he was one of the most talented players to grace the school.

But eventually, 15-year-old Agassi snapped in front of him. The repressive environment had become too much. Why wouldn't they enter him into professional tournaments?

Bollettieri had no words. Agassi was ready to compete, and Bollettieri became Agassi's first professional coach.

> _"People like to call the Bollettieri Academy a boot camp, but it's really a glorified prison camp. And not all that glorified."_

### 4. Agassi struggled with his image as well as his feelings about tennis. 

Agassi turned pro on April 29, 1986, his sixteenth birthday. That day, he lost in the final of the Florida Masters tournament. He faced a dilemma. If he accepted the $1,100 second-place prize, it would mean accepting a livelihood as a professional tennis player.

Sure, it was great to have some much-needed money. But it didn't make an iota of difference to his feelings about tennis. He still hated it, and he'd have to face that for the rest of his career. He took the money.

In his first year as a pro, he traveled the world, earning just enough to keep on the circuit. And a $20,000 Nike sponsorship deal came in handy too.

Agassi felt lost though. He hadn't had a proper education and had to give up an interest in writing and poetry. All he knew was how to play tennis, and he hated that!

Things came to a head in Rome. He'd been there for a tournament but used the opportunity to see the city's art. He realized a life inspired by such culture would be far greater than what he had.

The conflict was a real one, and Agassi was torn within, confused over his identity and image. He was a contradiction. He was shy but simultaneously had a rebellious streak that resulted in a flamboyant appearance that ensured he stuck out from the crowd. He wore denim tennis shorts and cut his hair into a spiky mullet, replete with frosted tips.

He had some stand-out victories in 1987, like beating the current Wimbledon winner Pat Cash, but they left him hollow inside.

He lost to some giants too. Ivan Lendl was the most cutting blow of all. He dismissed Agassi as nothing more than "a haircut and a forehand."

But in truth, Agassi never truly felt as though he had an identity of his own.

### 5. Agassi is eternally grateful to his trainer Gil Reyes and his spiritual advisor, J.P. 

In 1987, Agassi shocked the world by taking home $90,000 from his first tournament win in Brazil. He was just 17 years old. He couldn't resist splashing the cash and bought a white Corvette.

Success continued apace in 1988. But, after he lost in five sets to world number three Mats Wilander in the semi-finals of the French Open, it was clear his stamina and physical abilities needed to be worked on. He needed a trainer.

Agassi found Gil Reyes at the University of Las Vegas. Reyes seemed like an actual giant to the young Agassi. He had the largest hands he'd ever seen. Reyes would become more than a trainer to him. It was the start of a beautiful friendship.

Reyes didn't just design personal workouts on unique equipment; he also opened his home to Agassi. Agassi ate with Reyes's family frequently. In due course, Reyes — 18 years Agassi's senior — became very much a second father, one who always held Agassi's best interests at heart.

Another key figure for Agassi at this time was John Parenti, more commonly known as J.P., the pastor of a Las Vegas church. He was known for leading his congregation casually dressed in jeans and a T-shirt.

Agassi reached out to J.P. They sat down and talked. The pastor was able to help Agassi understand his issues. Agassi began to consider his internal contradictions for what they were, a consequence of his upbringing. He finally understood that it was fine not to be perfect all the time.

Andre wound up bringing J.P. to tournaments, and sought his counsel for several years. He wanted to better understand himself as an individual, beyond tennis and his relationship with his father.

Once Reyes and J.P. were in his life, Agassi's physical strength and confidence grew. He was perfectly placed to defeat tennis legend Jimmy Connors in a five-set match at the 1989 US Open.

### 6. Wimbledon success brought Agassi newfound respect; in Brad Gilbert, he found the perfect coach. 

Reyes was the consummate professional, and before long Agassi's game improved rapidly. The trainer even developed specific exercises that took account of Agassi's lower back problems. Agassi had been born with spondylolisthesis, a slippage of the vertebrae that can be exceptionally painful. Reyes also blended a special sports drink to keep Agassi hydrated during and after matches. This was the famous "Gil Water," a mixture of water, carbohydrates, salt and electrolytes.

The fundamentals were in place. But a grand slam title was still beyond Agassi's grasp. Achieve that, and he would finally gain the respect of his peers and the press.

He'd done himself no favors in a 1989 Canon camera commercial when he'd played to stereotype. The advertising slogan, "Image is everything," only served to double down on his all-style-no-substance reputation.

It was a shock to everyone, then, when he landed a Wimbledon title in 1992. After all, grass courts were his least-favored surface, and he was ranked 12 — his worst position since 1988.

The press erupted at his first grand slam win. They finally recognized that Agassi was a player with the talent to compete and win at the highest level. Even Agassi's hyper-critical father was begrudgingly and tearfully proud.

One thing that was slowing him down was his coach, Nick Bollettieri. He hadn't adequately prepared Agassi for the mental aspects of tennis. Bollettieri could never help Agassi overcome those feelings of pointlessness that overcame him when he lost a match.

Brad Gilbert was the perfect replacement, and he had much to teach Agassi about winning and motivation. He made Andre play smarter: chasing winning shots, tiring his opponents out and letting them make unforced errors.

### 7. His relationship with Brooke Shields turned into a troubled marriage, but much more was wrong. 

After his Wimbledon success, Agassi's life entered the realms of the surreal. The president invited him to the White House, and he had a brief but intense friendship with Barbara Streisand.

He was also introduced to and began dating Brooke Shields, an actress whom he'd had a crush on for years.

On their third date, Agassi confessed: he'd been going bald for years, and he wore a hairpiece to disguise the fact. The hairpiece had actually almost caused an embarrassing disaster during the 1990 French Open. The night before the big match, the hairpiece disintegrated. Agassi had to use 20 bobby pins to brace it back together. Distracted by his hair, Agassi unsurprisingly lost the match.

Shields wasn't so bothered by the hair. In fact, she helped Agassi become "liberated." He chucked the toupée and buzzed his hair short. He felt like a confident new man. It was thanks to this positive attitude that he won the 1995 Australian Open, beating Pete Sampras in the final.

Agassi would eventually propose to Shields in Hawaii, but he had some niggling doubts. Neither one of them understood what the other did for a living. Shields was always perplexed, for instance, as to why Agassi felt so beat up and depressed whenever he lost. It meant she just left him to stew alone.

Consequently, as time passed, they spent longer apart. It wasn't all Shields's fault — Agassi wasn't much enamored of their sterile house in Los Angeles. Nonetheless, they still got married in 1997.

Theoretically, it should have been a great year for Agassi. But it didn't feel like it. He felt empty. His marriage seemed hollow, and he was losing match after match. He even began seeking solace in crystal meth with his personal assistant, as a way to escape the emptiness.

### 8. Founding a school made Agassi’s life and career much richer. 

Soon after he got married, Agassi hit rock bottom. The tennis authorities informed him that he'd tested positive for an illegal drug. He ended up lying his way out of trouble, but he felt dreadful doing so, and his ranking dropped to 141.

But Agassi knew himself well. He needed to set himself a challenge, to aim for something. His goal would be to regain his number one spot in the rankings.

It took him two years to move beyond this low point and to work out who he really was. In part, the progress he made was thanks to the new school he'd established.

The Andre Agassi College Preparatory Academy started taking shape in 1998. The idea was to help at-risk children in West Las Vegas make it to college and to envisage better futures for themselves. It was a cause close to Agassi's heart as he'd always felt he'd missed something through his own lack of education. Helping underprivileged children was something about which he could feel passionate.

Construction began on the school in late 1998, but Shields wasn't there for the important groundbreaking ceremony. It was another sign that their marriage had fallen apart and a divorce followed soon after.

Andre's coach, Brad Gilbert, was optimistic.

Agassi's worries about his failing marriage had been toxic for his tennis game. Gilbert was sure that if 1999 were drama free, Agassi would bounce back. What's more, the school helped Agassi free himself from feelings of self-loathing. Now, on the court, he felt he was playing for the school and the students. It wasn't just about him.

He had purpose. And a comeback was the next step.

### 9. Agassi finally found the perfect match in Steffi Graf, and he made a brilliant comeback. 

Steffi Graf, the German tennis great, was also due to play the 1999 French Open.

Gilbert had a hunch that Agassi and Graf would hit it off and so made sure to book Agassi's practice court right next to Graf's. Things were slow and hesitant at first, just a little small talk, and Graf had a boyfriend whom she'd been with for years.

But the two had a lot in common, and before long they realized they were perfect for one another. Graf's father had also pressed her into a tennis career at a young age and, unlike Shields, she had a real sense of the turmoil Agassi felt. She understood what it meant to search for individual worth in a life that felt overwhelming.

They were soon inseparable. Even their careers matched one another. They each won the French Open in 1999, and they both lost their respective Wimbledon finals. But it didn't matter a jot to Agassi: he had a date with Graf after the match.

Though Graf announced her retirement after that Wimbledon, Agassi was determined to keep pressing on since he was on the verge of regaining his number one ranking.

Graf was there watching at the 1999 US Open as he battled through in a five-set win against Todd Martin. Thanks to that effort he was able to finish the year top of the rankings, displacing fellow American Pete Sampras.

It was tough though. His body was beginning to give out. At barely 30 years old, playing had taken a toll on his back, and his hamstring wasn't faring much better.

Despite that, in 2000, Agassi still made the final of the Australian Open. It was some achievement, and it meant he became the first player since Rod Laver to reach four consecutive Grand Slam finals.

### 10. Agassi’s final years of tennis were some of his most rewarding, and his family was there for it. 

In early 2001, Graf was pregnant with their first child. Agassi responded by bouncing around that year's French Open like a teenager. Sadly though, his body couldn't take the strain, and he crashed out in the quarterfinals to Pete Sampras.

Their first child, Jaden, was born soon after. A second daughter, Jaz Elle, followed in 2003. These two kept him motivated in his last remarkable years of professional tennis, despite the pain he was experiencing.

When Agassi won his last grand slam in 2003 — the Australian Open — he became the oldest player in over 30 years to manage the incredible feat of winning a grand slam so late in life.

Unfortunately, the condition of his back continued to deteriorate, so he had to rest more between the big tournaments. This, in turn, meant he often felt rusty on court. Nonetheless, he was still able to win a few of the tour events, such as Cincinnati in 2004 and the 2005 Mercedes Benz Cup in Los Angeles.

At the award ceremony for the latter, Jaden memorably ran out onto the court to take the trophy.

It was the start of the tennis era dominated by Rafael Nadal and Roger Federer. They seemed unbeatable. Nadal sprang around the court while Federer played with no apparent weakness. Agassi just couldn't compete. The 2006 US Open would be Agassi's last tournament.

By then he had a good perspective on his career. He'd even had the confidence to correct a reporter who was amazed at the transformation Agassi had made over the years. It wasn't a case of transformation, he said, because no player can be a finished product. Every player is in a state of progress.

Agassi, that rebellious and flashy teenager, might have given the impression that he knew what he wanted from life. But it's really something he's still working toward, even now.

### 11. Final summary 

The key message in this book:

**Andre Agassi was one of the biggest sports stars in the world in the 1990s, but there was more to him than a spiky mullet and flamboyant outfits. Agassi was raised to be a champion from the day he was old enough to hit a tennis ball. But this meant he was robbed of a real childhood and an identity of his own. Though he hated tennis, it was all he knew, so he struggled to find a way of playing the game and conducting his life on his own terms.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading: _The Inner Game of Tennis_** ** __****by W. Timothy Gallwey**

_The Inner Game of Tennis_ (1972) explains the tension between your conscious and unconscious minds, and how this conflict relates to performance, specifically through the lens of tennis. These blinks offer concrete advice on how to harness your natural ability and excel both on the court and off.
---

### Andre Agassi

Andre Agassi retired from tennis in 2006. During his career, he was ranked world number one and won eight Grand Slam titles. He is one of few tennis players to achieve a Career Golden Slam, the feat of winning every Grand Slam tournament, as well as a gold medal in the Olympics. He now devotes his time to the Andre Agassi College Preparatory Academy, which improves the lives of at-risk children in his hometown of Las Vegas, Nevada.

