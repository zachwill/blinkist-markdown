---
id: 57e6b9a7afd7bf0003b704f7
slug: orbiting-the-giant-hairball-en
published_date: 2016-09-27T00:00:00.000+00:00
author: Gordon MacKenzie
title: Orbiting the Giant Hairball
subtitle: A Corporate Fool's Guide to Surviving with Grace
main_color: F0F030
text_color: 707016
---

# Orbiting the Giant Hairball

_A Corporate Fool's Guide to Surviving with Grace_

**Gordon MacKenzie**

_Orbiting the Giant Hairball_ (1996) is a guide to surviving at a corporation while holding onto your creativity. These blinks explain how to evade the dark, creativity-stifling vortex of corporate bureaucracy by balancing corporate restraints with personal creativity.

---
### 1. What’s in it for me? Rediscover your creative genius. 

Do you sometimes wonder where your creativity has gone? Maybe you were once a hugely imaginative child, always busy drawing, playing, inventing stories or crafting crazy objects, but now that you've finally earned yourself a "creative" job, the artistic ideas just aren't flowing.

The good news is that your creativity isn't dead, you just need to free it.

Unfortunately, however, it's being held hostage by a giant hairball of corporate regulations, guidelines and practices trying to make you normal — too normal to be creative. These blinks tell you all you need to know about that hairball, including how to escape it without leaving your company altogether.

You'll also find out

  * about a bunch of hypnotized chickens;

  * what's so great about being foolish; and

  * how a creative company is a bit like a dancefloor.

### 2. Everyone is a creative genius, but society suppresses this talent. 

Are you an artist? Well, if you don't make paintings or write poetry, you might be inclined to say no. But don't be so quick to dismiss this possibility. In truth, every one of us is born a creative genius.

Undisciplined children make rash decisions, diving spontaneously into risky situations and saying and doing whatever they like. They are driven purely by curiosity and innocent desire. These childhood tendencies are an expression of creative genius at the core being of every person. Everyone is born with this blissful "foolishness" that leads us to discover and evolve.

However, lots of people fear that a society of "fools," that is, one made up of people who follow their creative genius, can never function. So, they stifle their creativity in an attempt to be "normal." This makes perfect sense from a societal perspective. After all, "fools" are seen as impulsive, unpredictable rule-breakers, and the enforcement of strict social norms is about protecting society from this unpredictability.

Unfortunately, such protection also suppresses creative genius.

For example, on many occasions, the author asked children from different grades if they considered themselves to be artists. When he asked first graders, they would all enthusiastically raise their hands, but when he got to the second graders, about half would identify themselves as artists. By the time he reached the sixth graders, only one or two kids would raise their hands.

So, we can see that society suppresses creativity. But we can do something about it. 

To break open the full potential of our creative genius, or if you prefer, our foolishness, we have to stop aiming for normalcy and stand up to the rules that enforce it.

> _"Our creative genius is the fountainhead of originality. It fires our compulsion to evolve. It inspires us to challenge norms."_

### 3. Corporations are giant hairballs that pull us into their web of standards and procedures. 

Can you remember the last time you bought a greeting card? Well, chances are it was made by Hallmark. After all, the company is a household name around the world. And like the majority of corporations, Hallmark is also a huge hairball of _corporate normalcy._

In 1910, when Joyce Clyde Hall set out to start his "social expression company," there wasn't a single comparable business around. Hall had to write his own rules.

He developed the first policies and procedures for doing business. These became the first two hairs of the corporate hairball. Then, in the following years, thousands of other policies and procedures were produced, all of them tangling together and forming a massive knot.

This knotted hairball is the symbol of corporate normalcy — all the procedures, policies and guidelines that define what will conventionally make a company work more effectively, quickly and cheaply.

Naturally, people are pulled into these hairballs of corporate normalcy as well. After all, we know from physics that gravity exerts a tremendous pull toward the earth that prevents us from careening out into space. We also know that the force of gravity becomes greater as the mass in question grows larger and larger.

In a similar way, as the hairball becomes larger, and as more layers of guidelines, standards and procedures are added, so too does the pull toward it become increasingly powerful.

Say you work for a new advertising company. The firm starts off by introducing a standard corporate design, which means every ad you make should have the same basic layout and color palette. Then they add a creativity guideline requiring you to attend brainstorming meetings and, finally, a new accountability guideline instructs you to write a daily progress report. With each additional rule, you're pulled deeper and deeper into corporate normalcy.

But is all that necessarily bad? Isn't there some comfort in being part of a giant corporate hairball?

Maybe, but it can also be problematic, which is what we'll explore next.

### 4. Avoiding the giant hairball means orbiting around it. 

Have you ever been given a so-called "creative" assignment that required you to use a particular material, format and color scheme?

If so, you know this is exactly what "creating" is _not_. Rather, creating is the act of producing something truly original, totally unobstructed by procedures and guidelines. And to engage in such creative work, it's essential to avoid being snagged by the hairball, which is solely made up of how things used to be.

For instance, early on in his time at Hallmark, the author was a sketch artist tasked with "creating." But he found himself abiding too strictly to the company's style. How did this happen?

Well, the hairball is made up of old rules and practices; in other words, things that once worked. And since the hairball is so full of these old ways, there's no space for novelty or originality.

If you want to truly create, you have to first avoid being pulled too deep into that tangle of procedures. You need to orbit around the hairball, engaging with _responsible creativity_, which is a handy way to go beyond corporate normalcy. Responsible creativity entails straying from the standard corporate path, but not by too much. After all, if you disregard the company's mission and aimlessly drift off into space, you won't be part of the company at all.

So, it's crucial to remain within the orbit of the hairball by treating your company with loyalty and respect. But it's also important to keep a critical distance and avoid being sucked in by its uncreative procedures and bureaucracy.

For example, the author was initially hired by Hallmark's editorial department. But the people in his department lacked any eccentricity, and just plodded along with the usual corporate decorum. So the author transferred to the Contemporary Design Department, Hallmark's crazy stepchild.

This department was doing tremendously successful and original work, and it was all thanks to the team's freedom, chaos and playfulness. The author knew that the best way he could be creative for the company was to work in a department that granted him a bit more freedom.

In other words, finding your personal sweet spot is the key. In the next blink, we'll explore more in-depth ways to find your orbit and stay with it.

### 5. Corporate culture can be hypnotic, but by holding on to what makes you unique, you can stay creative. 

Unless you're starting to lose your marbles, you probably know that you're not a chicken. Even so, you might be just as mesmerized as a chicken and in line for a similar fate.

How so?

Well, in 1904, the author's father spent the summer in Ontario, Canada with his aunt and uncle on their farm. One playful Sunday morning, he and his cousin drew lines on the porch with a piece of chalk and then headed for the chicken coop. There, they pulled out dozens of chickens, one by one, put them on the chalk lines and held them in place for a moment. As a result, the chickens stood perfectly still, hypnotized in place.

And, whether you believe it or not, you're prone to such hypnosis as well.

When you're brought on board by a new company, they tell you all about their history, philosophy, procedures and politics. They then explain that adherence to these principles forms the essence of the company, and that it's the ticket to their, and your, success.

Here's where you need to look out because, in essence, the company is drawing a chalk line for you to stand on. They'll hold you to it, and, if you're not careful, you'll be hypnotized and immobilized before you know it.

But there's a way to avoid this hypnosis: hold on to your creativity and move away from the corporate line by following your own unique mind. After all, regardless of what opinions you hold, you're unique insofar as you are the only _you_.

There's nobody else who has exactly your experiences, skills and passions. And to stay creative when working within a corporate culture, it's essential to hold on to what makes you unique. Find the company goals that resonate with you and contribute to them in your own distinctive way.

> _"In losing connection with your one-of-a-kind magic, you are reduced to nothing more than part of the headcount."_

### 6. Companies should ditch the job descriptions and set their employees free. 

Nobody likes hanging out in small, enclosed spaces. So why do corporations hold on to job descriptions that serve as nothing more than restrictive cages?

Granted, the container _was_ an amazing invention. For instance, our ancestors needed water to survive, but it was risky to venture out of their caves for a drink. So, they looked for something to hold their water inside the caves and the container was born.

But when it comes to companies, containers only serve to confine people in unnecessarily narrow job descriptions. Some job descriptions dictate exactly what the employee should and shouldn't do, as well as what they're supposed to deliver and how.

In general, _all_ job descriptions limit employees to a certain position from which they're not allowed to deviate; in other words, they're restrictive.

But it doesn't have to be this way. Instead, corporations can let their employees free on the dance floor, so to speak.

Many organizations fear that, without job descriptions, employees won't know what to do; they're worried that conflict, chaos and confusion will ensue. But this assumption is totally false. After all, on a dance floor, dancers move freely without colliding and, similarly, employees who are free of restrictions do their work and cooperate. They're capable of adapting their behaviors to those around them and to the changing demands of their company.

So, getting rid of job descriptions is good for employees, but it's also good for companies. Removing job descriptions will make a company more adaptable, since it can assign tasks to whoever is capable of handling them.

For example, a graphic designer from the product design department who's particularly well suited to web design could seamlessly join a group tasked with building the company's new website.

> _"If we are to achieve the quantum leaps the future seems to be demanding of us, we must risk to leave our containers-turned-cages."_

### 7. Final summary 

The key message in this book:

**Beware the dark, dull space of corporate normalcy, the place where any original thought will be smothered through procedure, guidelines and policies. Instead, let your natural-born creativity blossom by balancing the missions and goals of your company with your own originality.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Scaling Up Excellence_** **by Robert I. Sutton & Huggy Rao**

_Scaling_ _Up_ _Excellence_ is the first major business publication that deals with how leaders can effectively spread exemplary practices in their organization. Readers can expect to learn about the latest research in the organizational behavior field, lots of instructive industry case studies, and many helpful practices, strategies and principles for scaling up.

The authors help leaders and managers understand major scaling challenges and show how to identify excellent niches, spread them and cultivate the right mindset within their organizations. They also set out scaling principles that guide leaders in their daily decisions.
---

### Gordon MacKenzie

Gordon Mackenzie worked at the greeting card company Hallmark for 30 years. Starting out as an artist, his position within the company shifted over the years and he eventually became a kind of company guru with the title _Creative Paradox,_ someone to whom people turned with their ideas.

