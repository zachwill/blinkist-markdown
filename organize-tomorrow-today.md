---
id: 5713a5965236f20007c16f42
slug: organize-tomorrow-today-en
published_date: 2016-04-18T00:00:00.000+00:00
author: Dr. Jason Selk, Tom Bartow and Matthew Rudy
title: Organize Tomorrow Today
subtitle: Eight Ways to Retrain Your Mind to Optimize Performance at Work and in Life
main_color: FEEC33
text_color: 665F14
---

# Organize Tomorrow Today

_Eight Ways to Retrain Your Mind to Optimize Performance at Work and in Life_

**Dr. Jason Selk, Tom Bartow and Matthew Rudy**

_Organize Tomorrow Today_ (2015) is the definitive guide to achieving a successful career and fulfilling life. These blinks offer you valuable information, giving you the tools to unlock the power of your mind, increase your self-confidence and become your most productive self.

---
### 1. What’s in it for me? To change your life for the better, change one bad habit at a time. 

What's so special about people who excel? Is it superior talent? Were they born advantageously? 

No. Successful people come from all backgrounds and walks of life, and not every person starts out as a stellar success. But they all do share one feature — they all have awesome habits. 

So what exactly are successful habits, and how do you get them, too? These blinks will explain in detail the eight ways to retrain your mind and change for the better. Even focusing on one new habit will help you go the distance when it comes to becoming more focused and productive. 

In these blinks, you'll discover

  * why being abnormal is preferable to being normal;

  * how multitasking is just a ruse to pretending you're productive; and

  * how establishing a ritual can help you to turn good behaviors into habits.

### 2. It’s just not productive to focus on more than one goal at a time if you want positive results. 

Have you ever felt the need to make a positive change in your life? Maybe you've already made a list of all the small things you'd like to alter. But making a long list isn't the most effective way to go about making improvements.

Why? It's a fact that our conscious mind can only process between five to nine concepts, or pieces of information, at a time. This phenomenon is called _channel capacity_, and was first identified by psychologist George A. Miller in 1956.

No matter what you're reading or looking at, from words to colors or numbers, your _working memory_ can only store a limited amount of data at any given time. And working memory is all a person has from moment to moment, whether making an argument or trying to solve a math problem.

So given this limitation, it makes sense that trying to tackle too many tasks at once can be overwhelming. This also holds true when you try to change too many things at once in your life.

Since every task you try to complete requires its own amount of data, your working memory can suffer from information overload when you try to juggle multiple tasks. When this happens, important data can get improperly processed and you might make a mistake, or worse, your conscious mind will shut down. You can think of this like a computer freezing when you've opened up too many applications.

The author has witnessed this dilemma firsthand. People have told him that when they try to change two or three things in their life at the same time, they get stuck. But when they focus on only one thing, the results can be unbelievable.

So the best way to improve your life and get positive results is to commit yourself to changing just one thing at a time!

> "_Your mind is like a powerful fire hose. Don't dilute the strength by splitting the stream …_ "

### 3. Prioritizing tasks and organizing tomorrow is the key to better time management and productivity. 

People may look productive when they frantically hurry from one task to the next, or by multitasking to the point of distraction. But such behavior just points to a person who has poor time management skills!

The secret to having a productive tomorrow is to _organize it today_.

Organizing effectively means prioritizing your tasks by making a list of the _Three Most Important_ things you want to achieve. Be sure as well to include the time needed to accomplish these tasks. 

Then from your three tasks, pick one that absolutely needs to be done that very day. 

In doing so, you'll be sure to focus on what's most important to you, and will be less likely to forget or push aside a task that might be more difficult in favor of an easier goal. 

Plus, by only focusing on three tasks, instead of being overwhelmed, you'll find increased confidence in being able to accomplish all your stated goals.

There are also three important things to keep in mind when making your list.

First: Don't make the mistake of listing a big, complex project as one item. Instead, find smaller and more manageable tasks within the big project that you can add to the list.

Second: Prioritize and schedule wisely. Carefully identify your most important task for tomorrow and make sure you schedule enough time to complete it. For many, the period between lunch and 3 p.m. is a great window for uninterrupted work.

Third: Write it down. Putting your plan on paper is about more than just remembering what to do. Writing activates a specific part of your brain called the _reticular activating system_ (RAS). This area acts like a filter and puts important information, such as the things you write down, at the forefront of your mind.

So with a prioritized, written plan of attack, you'll be ready to conquer the day ahead!

> _"...being busy is a waste of time: Being productive is the goal."_

### 4. Maximize the time you have by finding ways to structure goals and increase meeting productivity. 

Do you ever look at your to-do list and wish there were more than 24 hours in a day? Here are four tips on how you can make the most out of the time you have.

Tip #1: Don't waste time waiting around.

You might not think the time between appointments is valuable, but these short periods can add up to significant hours. So instead of twiddling your thumbs or checking Facebook, spend your extra time productively. Chances are there's an email you can reply to, or paperwork that needs to be done.

Tip #2: Use the _ask and chop_ technique to avoid procrastination.

This is a helpful technique when you're confronted with a task that feels too overwhelming to even start. First, _ask_ yourself: What is the first step that needs to be taken? Then simply focus on _chopping_ off that first step by completing it, then repeat the question. By chopping away the task in small steps, you'll make progress without being intimidated by the scale of the larger project.

Tip #3: Set your game clock.

Create a challenge by giving yourself a short amount of time for a small task. When you meet your deadline, you'll see how efficient you can be and learn that you really can do more with less time.

And finally, tip #4: Pre-structure your meetings to meet solid goals.

Unstructured, aimless meetings are a waste of time. People end up jumping from topic to topic or spending the meeting focusing on minor details. Make your meeting productive by giving it a focus.

Open the meeting by articulating a clear goal that outlines what you want to accomplish. Everyone at the meeting will then be aware of the meeting's focus, and you can spend the rest of the time productively in reaching that goal. Close the meeting by dedicating a short amount of time for questions or proposals.

With these tips, you'll be well on your way to creating more time in your working day by using each minute as efficiently as possible.

### 5. Create a ritual of your new lifestyle goals to avoid procrastination and stick to new, positive behaviors. 

Many people make New Year's resolutions to kick a bad habit or to get into better shape. Unfortunately, it's not uncommon to revert to old habits or bad behavior after just a few weeks. 

So how can you stick to a plan and develop new, better habits? Start by making a ritual of the positive habit you want to adopt.

Making a ritual means doing something at the exact same time, each and every day. If you want to go to the gym more often, ritualize this new habit by setting a specific time to go, like 8:30 a.m. every day, and commit to it. By making your new goal a ritual, you'll be less tempted to procrastinate or postpone.

But if you start to doubt or feel like slacking off, remember that changing habits gets easier with time!

As you start trying to change your habits, you can run into situations where old behaviors, like lying on the couch watching television, might feel like a better option. When this happens, resist the temptation by reminding yourself that this is a normal, temporary phase on the path to a great new lifestyle. 

You might have to fight through these early stages — but it will get easier the longer you stick with it!

To boost your fighting spirit, ask yourself two questions: How will I feel when I win this fight and stay committed to my new habit? How will I feel if I give up? These questions will help provoke feelings within you that can motivate you to stick with your goals.

Finally, imagine how your new lifestyle will change things for you in the long run.

It helps to do this through visualization: imagine your new self as a healthy, productive and confident person. This technique keeps your goal in mind and will motivate you to keep going for it!

By applying these basic thinking techniques, anyone can stay committed to establishing new habits or making lifestyle changes — until the change finally becomes second nature.

### 6. Self-evaluation is effective as long as you focus on the process, not just the results, of your work. 

Do you ever hesitate starting a task for fear that you may make a mistake or fall short of your own expectations? If so, you might be a perfectionist, and this can get in the way of evaluating yourself fairly. 

When a perfectionist performs a self-evaluation, he is likely to focus more on shortcomings over whether something as a whole was done well.

By basing a self-evaluation on the highest of standards, you are simply setting yourself up for disappointment. You can either fail to reach those standards and let yourself down, or you can meet expectations and see no reason to appreciate good work. 

This can result in negative feedback, which in turn leads to stress and low self-confidence. 

The better method is to appreciate when you do things well! Take the advice of legendary basketball coach John Wooden. He said that the most successful people always give themselves credit when it's due.

It's also important to focus more on the process than on results. 

By paying attention only to results, you won't learn what needs to be done to get the results you want. When you focus on the process, you'll learn what works best to reach your goals and what you might need to improve.

Consider creating a _success log_, or a daily journal that will help you evaluate your process in a useful way.

To start your journal, write down the realistic goals you hope to achieve each day, and take the time to evaluate your goals afterward.

You should answer three questions: What did I do well today? What is one aspect I'd like to improve for tomorrow? Is there one thing I can do differently? When you answer these questions, you are focusing on setting goals around behavior and not on the external results.

So be on the lookout for behaviors that contribute to success — they'll point the way to continued progress!

> _"Greatness is predicated on consistently doing things others can't or won't do."_

### 7. How you think and talk to yourself can either build you up for success or let you down toward failure. 

Have you ever talked yourself out of doing something? Maybe you were going to apply for a new job or ask for a promotion, but then you thought, "Why bother, it's not going to happen, I'll just end up getting my hopes crushed." This is a bad habit that you can change.

How you talk to yourself impacts your self-confidence. Over time we tend to believe whatever it is that we tell ourselves. You can either convince yourself that you'll fail by thinking negatively, or you can boost your confidence and increase your chances of success by encouraging yourself.

Even if you are doubtful in the face of a challenge, you'll find that by continuing to feed yourself with positive thoughts, you'll eventually feel the necessary confidence to get on with it.

After all, you'll never even approach a challenge if you don't allow yourself the belief that you will succeed!

Think of it like a coach: When a team is facing a tough match, it's a coach's job to rally the team with positive messages and keep them motivated. Without hope, no one would bother trying!

Another important technique is to keep yourself from overthinking a problem.

When you overthink, your mind turns the problem into an unsolvable obstacle, which can end up paralyzing you into inaction. Your brain releases stress hormones, leading to tension and the loss of any creative intelligence. This usually ends with the overthinker giving up to escape the stress.

A much better way to achieve a goal is to relax and, instead of focusing on the problem, concentrate on a solution.

Take stock of everything you have at your disposal, the tools that can help you solve your problem. Rather than becoming overwhelmed, take the task step by step, think positively and work out a gradual solution.

> _"That which you focus on expands. Focusing on the negative is essentially like fertilizing the weeds in your yard."_

### 8. By writing thoughtful speeches, practicing often and speaking calmly, you’ll get your message across. 

Chances are you don't enjoy making speeches in front of large groups of people. But every professional can learn how to give a decent speech; here are some keys to help you learn how.

There are two important points to giving a good presentation: _preparation_ and _practice_.

When you prepare a speech, you want to make sure your audience understands your main ideas. In previous blinks, we learned that the mind can only handle a certain amount of information. Building on this, be aware that your audience, to stay engaged, requires a well-structured speech.

You'll also need to remove any elements in your speech that might distract a listener or obscure your message. To do this, write down your entire presentation and look at the first and last five minutes. Take out any details that seem superfluous.

With your presentation lean and organized, you can then rehearse delivering it to an audience.

If you're overly nervous, you might appear awkward, fidgeting with clothes or hair. Before you take to the stage, reassure yourself that you're well-prepared and boost your self-confidence by rehearsing the speech until you can recite it effortlessly. Take moments throughout the day, even during mealtimes, to mentally recite the details in your speech.

Don't forget to talk slowly, and pause after you make a point. People who speak at a calm, measured pace are often seen as more confident than people who speak quickly. And by allowing for pauses, you'll give your audience time to better process the points you are trying to make.

In contrast, if you're preparing a one-on-one talk, there are two important things to keep in mind:

First, keep the tone of your voice calm and friendly. This will allow the other person to be at ease, and what's more, the person will also be more receptive to your message.

Second, be sure to give the other person a chance to voice his thoughts, and listen carefully to what he has to say. If you speak slowly and pause frequently, this gives your partner the opportunity to respond.

### 9. Being a normal person can get in the way of achieving greatness. Seek to be abnormal! 

Why are people so hung up on being normal? Can you think of any great artist, writer or scientist who was considered normal?

_Normal_ people have traits, or "viruses," that can keep them from achieving their goals. But _abnormal_ people are immune to these viruses, and refuse to let them interfere with personal success.

The main virus that plagues normal people is _procrastination_.

The normal person is scared to pursue an ambitious goal, worried that failure awaits. And for that normal person, it is easy to find excuses not to start: laundry, a dirty kitchen, and so on. 

But the abnormal person doesn't look for excuses. Abnormal people look for ways around any obstacle. If the dog ate a notebook, the abnormal person will staple together some paper and make a new one. Abnormal people take control of their fate, convinced that they alone can achieve their goals.

A secondary virus, related to procrastination, is focusing on things you can't control.

Normal people can blind themselves from the things they can control by obsessing over things they can't. It's too cold or rainy outside, the neighbor's dog is barking, the coffee shop raised its prices, and so on. By focusing on such concerns, normal people distance themselves from the aspects of their life that they can actually improve.

For abnormal people, such concerns are a waste of time and energy. Instead, they pay attention to what they can control and the parts of their lives they can improve.

So do what abnormal people do: create a step-by-step plan of action on what you'd like to improve in your life and how to make it happen. You can even add a list of things that you can and can't control, to make sure you don't get distracted along the way.

> _"Normal is fine. Acceptable. Average. But if you want to do great things, you need to be abnormal."_

### 10. Final summary 

The key message in this book:

**If you want to be highly successful, you need to know how to control your mind and talk with yourself in a positive fashion. Always remember to focus on the solution rather than on the problem. And even when you're at the top of your game, never forget the process that got you there.**

Actionable advice:

**Build an evaluation ritual.**

Next time you have a bad day at the office and you feel that your self-esteem is flagging, don't focus on what you did wrong. It's much better to write down everything you did well. Such a positive evaluation will immediately increase your confidence, motivate you and improve your performance tomorrow. 

**Practice the** ** _Mental Workout_** **to reduce anxiety about a task.**

First, relax your mind by controlling your breath. Inhale for five seconds, hold your breath for two seconds, and exhale for six seconds. Next recite a personal mantra, or a fixed set of encouraging statements. For example, "I'm strong, smart and confident and things will work out well." Spend 60 seconds visualizing the situation you're afraid of. Imagine what you're going to see, how you're going to feel, and what you're going to say. Repeat your personal mantra and return to the breathing technique.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _The Miracle Morning_** **by Hal Elrod**

In _The_ _Miracle_ _Morning_, Hal Elrod explains the techniques that got him through the aftermath of a near-fatal car accident. Elrod realized that the key to a successful and fulfilling life was dedicating some time to improving ourselves every day. He details six crucial steps we can take every morning to help us jump-start our days and get us well on our way to a fulfilled life.
---

### Dr. Jason Selk, Tom Bartow and Matthew Rudy

Dr. Jason Selk is the director of mental training for the St. Louis Cardinals, a Major League Baseball team in the United States. He is also the author of _10-Minute Toughness_ and _Executive Toughness_.

Formerly a successful college basketball coach, Tom Bartow is now a financial advisor, and has created an advanced training program for high-level advisors at Edward Jones, a financial services firm.

Matthew Rudy has coauthored over 20 books covering a wide range of topics, including golfing, business and travel.

