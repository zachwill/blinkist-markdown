---
id: 556c5d443236630007220000
slug: organizing-from-the-inside-out-en
published_date: 2015-06-02T00:00:00.000+00:00
author: Julie Morgenstern
title: Organizing From The Inside Out
subtitle: The Foolproof System for Organizing Your Home, Your Office and Your Life
main_color: B7D7D1
text_color: 60706D
---

# Organizing From The Inside Out

_The Foolproof System for Organizing Your Home, Your Office and Your Life_

**Julie Morgenstern**

_Organizing From The Inside Out_ (1998) is the essential handbook on how to organize your life in a personalized and sustainable way. From your office to your house to your suitcase, these blinks outline clear step-by-step strategies to organize your entire world.

---
### 1. What’s in it for me? Personalize your tidying-up strategies to make them work for you. 

Do you sometimes wish you could tidy up like Mary Poppins? Wouldn't it be great to simply snap your fingers and watch a cluttered room organize and clean itself?

Unfortunately, the laws of physics prohibit such shortcuts, but there is another way to make tidying up simple — even fun. It's called the _organizing from the inside out method_, and it works because it fits around your work schedule. These blinks teach you all about this method and how you can incorporate it into your life.

In these blinks, you'll discover

  * why you should never get angry with a messy partner;

  * how long it takes to clean a room on average; and

  * why your messiness may have psychological causes.

### 2. Start getting organized from the inside out by identifying the causes of your disorganization. 

It's happened to all of us: Desperate for a break from the chaos of our lives, we follow one of countless organizational tips and techniques touted as great solutions for eliminating disorder. For most of us, however, the effects are disappointing.

Why?

Many of these techniques are predicated on an _outside in_ mentality. They are too general and we struggle to apply them to the real-world hassle of our lives. A better organizational solution is to work from the _inside out_ — a strategy specific to you and your needs.

But before you can re-organize your life from the inside out you'll need to understand why it's _dis_ organized.

You can't fix something if you don't know why it's broken, and by focusing on these three common organizational problem areas you can find out where your system can be improved:

First off, there are often simple, technical shortcomings that prevent your organizational system from reaching its full potential; think of missing labels or spaces that lack a clear purpose.

Second, there are environmental problems — things beyond your control — that inhibit organization: Unrealistic expectations at work; big events, such as a new baby or a divorce; domestic problems, like an uncooperative partner; or simply not having enough space.

The third area is psychological obstacles, things within yourself that propel you toward disorder — having unclear goals, for instance, or fears that prevent you from getting organized, or taking comfort in being surrounded by stuff.

When working through these three areas keep in mind that several may be true for you in different ways. Don't panic if you start to identify a lot of issues. Just stay honest with yourself and remember that all problems have a solution.

The first step to beating disorganization is to identify what causes it. Only by honestly assessing your situation can you build a lasting solution. So start by knowing your obstacles.

> _"Many creative or "right-brained" people who have always worked in chaos both crave and are frightened of getting organized."_

### 3. Caringly help your messy partner get organized by unleashing their motivation to clean up. 

Imagine this: You spent hours cleaning up, attending to all the details, and the effort shows. Your home gleams! Then your partner gets home, throws his muddy shoes in the middle of the room, hangs his wet coat on the sofa and dumps the contents of his pockets on the table. In seconds, your hours of work are negated.

At this point, you probably feel like strangling your partner. But restrain yourself — there's help!

Angrily insisting that your partner change his habits isn't a great strategy. It requires a cleverer approach to actually tidy up someone's habits, and here's why:

You can't motivate someone with your own motivation. To get them moving, you need to help them tap into their own. One way to do this is by asking a question: What does your clutter cost you?

Maybe your partner is always losing her keys and wastes hours searching for them. Or perhaps she almost broke her neck tripping over a pile of her stuff. Remind your partner of these things; it'll be hard for her not to respond.

Once your partner gets motivated, she'll want to change her ways. Here's one way to help:

Of the many solutions to messiness, a great one for shared-living situations is to co-design rooms, giving each person their private space.

This technique doesn't only afford each partner their own little piece of home, it also helps them both learn about each other's thinking and to see what's important to the other one. In this way, you can see the logic behind your partner's mess — maybe why that box of junk is so essential — and show them how you like to do things.

Who knows: Maybe your good habits will even rub off on your partner.

Now you know how to motivate a messy partner to clean up their act. The next step is to focus on _your_ ability to organize from the inside out.

### 4. Begin by analyzing the situation to get the most from your re-organizing. 

When organizing your life it's tempting to dive in and make huge changes right away. But jumping into your mess without evaluating it first can be disastrous.

That's because the key to organization is to start by _analyzing_ the problem. Analyzing your situation helps you reach your goal by getting a clear idea of what it is. Use these five-needs assessment questions to evaluate _your_ organizational challenge:

First off, what works?

It's likely you're not doing e _verything_ wrong. So take a minute to notice what runs smoothly. This will not only prevent you from eliminating good habits but also help you apply what's working to what's not.

The next question is what _doesn't_ work _?_

To figure out what's wrong with your system, try keeping a list of the inefficiencies in your day-to-day life and how they slow you down. Hang the list in a prominent place to keep your areas of improvement in mind!

Now that you've identified how you can improve, ask yourself which items are essential to you. This could be anything. Say music helps you relax at the end of a hard day. In that case, your CD collection would be a priority.

After you decide on what you absolutely need to keep, try writing down _why_ you want to get organized. Getting organized is difficult and knowing your motivation will help you stick with it.

Finally, ask yourself what's causing the problems you see. Is it a lack of will or skills? Maybe it's your partner's disorganization?

This is key because you'll only be able to solve your problems when you know what causes them.

Analyzing the situation takes time and it's tempting to skip it and dive right in, but investing time up-front will pay off tenfold down the line when you become truly organized. Just remember: Without an honest examination, true organization is impossible.

> _"If you don't know where you are going, how will you know when you get there? In order to reach any goal, you have to begin by defining it."_

### 5. Strategize before you organize. 

So you've analyzed your organizational situation, and if you weren't tempted to make changes before, you definitely are now. But first, it's important to develop a few strategies.

Organizational strategies will guide you, force you to pace your efforts and give direction to your work.

Try out these two:

The first strategy is called _The Kindergarten Model of Organization_ and it works by dividing everything into different activity areas. It just takes three steps:

First, define your activity zones. For instance, the place you work or where you watch TV.

Then, draw a map of your space. Take into account your natural habits and preferences, considering the relationships between your activities. For example, if you like to watch TV with friends make sure your TV area has plenty of seating.

Last, rearrange your furniture based on your map and watch as the changes you mapped out undo the chaos before your eyes! Take advantage of the diverse applications of this strategy by mapping anything, from your entire house to a single drawer.

The second strategy is to _honestly_ estimate the time necessary to get organized.

This strategy is essential because most people are unrealistic about the time commitment required to organize their lives.

Unrealistic expectations can derail your plans when your overestimation scares you out of starting or your underestimation causes you to lose motivation when you're not magically successful.

Avoid these fates by carefully planning how long each step will take. Remember, on average, organizing takes up to one and a half days per room.

Say you estimate the time required and have second thoughts. Well, consider this:

According to The Wall Street Journal, the average U.S. executive wastes six weeks every year searching for missing items in disorganized desks and files. That's an hour per day!

Now that you have the strategies to build a plan and estimate the time necessary to implement it, you're ready to attack!

### 6. Get organized by tackling your mess. 

So you've analyzed your situation and strategized a game plan. Now comes the exciting part: getting organized.

Using the organizational acronym _SPACE_ will get you started _._

"S" is for _sort_.

Start by asking yourself questions like "Do I use this?" and "What category does this belong in?" Move through your mess methodically, identifying what's important and grouping similar items.

"P" stands for _purge_.

It means decide what to get rid of and how. Remember, not every unessential item is trash; you can give some away, sell some or store them for another time. Make purging simple by setting up labeled boxes to easily put things in their place.

Of course, getting rid of your stuff isn't always easy. To motivate yourself imagine all the things you'll get in return: space, time, money and satisfaction. For instance, imagine all the space you'll have for new books if you toss the ones you've read!

"A" stands for _assign a home_.

Choose a place for your things by grouping similar items and considering what you use the most. Group your CDs with your stereo, for instance, and put the books you read in front of those you don't. And store those least-used items, like the tent you drag out once a year, in the basement or attic.

"C" is for _containerize._

Containers are key because they keep your sorted items grouped and separated. Have fun with this by color-coding or using themes, like keeping your kid's pirate toys in a treasure chest.

Last but not least, "E" stands for _equalize._

Once you get your organizational system in place, don't pin it down. Monitor your set-up and make adjustments when necessary. Then keep organized by setting appointments to review the success of your system!

After following through with _SPACE_ you'll be amazed at how _your_ space transforms.

Now that you're equipped with the tools to organize, it's time to take a look at examples of how to apply your knowledge.

> _"You've analyzed and strategized, and now you're ready to put on some comfortable clothes, roll up your sleeves, and begin the rewarding experience of transforming your space."_

### 7. Organize everything, from your kid’s room to your schedule, by putting your system into practice. 

The beauty of organizing from the inside out is that it works in all scenarios. You can apply this system to anything, from huge jobs to small tasks, from situations of complete chaos to those that are only in slight disarray.

By following the three-step process of analyzing, strategizing and then attacking, you can organize everything from your suitcase to your kitchen.

But sometimes you'll be faced with organizing someone else's mess — most commonly that of your kid's. When organizing your child's space, don't push them aside and do it all yourself. Instead, enlist their help and ask for their input.

Why?

Well, your kid won't keep her room organized if it's not done according to her needs. To ensure your child's room gets clean and stays that way, make sure they play a role in every step of the process.

But, of course, kids can be uncooperative and getting them to help out can be a pain. The best way to overcome this is by making organization fun. For instance, make tidying up dirty clothes a blast by installing a basketball hoop over the hamper.

Getting your whole house — even your kid's bedroom — in order will feel like a huge accomplishment, but don't stop there. Organizing from the inside out can even be used to conquer time!

Organizing your time is just like organizing your closet. You can use your new tools to turn your chaotic schedule into one that puts you in control.

You just need to remember the three steps:

Analyze to decide which activities are worthwhile and which ones you can do without.

Strategize to decide what needs to be done and by when.

And then attack by cutting out time-wasting activities and organizing your tasks into logical groups. For instance, paying all your bills at once.

Now that you know the power of organizing from the inside out, it's time to apply it to _your_ life!

### 8. Final summary 

The key message in this book:

**Even the most disorganized people can transform a life of chaos into one of order. But you'll have trouble succeeding unless you personalize your strategy. You can whip your world into shape today according to** ** _your_** **needs by analyzing, strategizing and then attacking your organizational problems.**

Actionable advice:

**Start analyzing your organizational needs today.**

See where your organizational structure can improve by keeping a list of the things that slow you down each day. Pin the list on your fridge or on your front door to keep in mind how you can improve. Then use the suggestions to strategize and implement a plan for action!

**What to Read Next:** ** _The Life-Changing Magic of Tidying Up_** **, by Marie Kondo**

So you now have a solid understanding of how to organize your life. But maybe you're not quite convinced. If you're still unsure whether being tidy is right for you, then it's time to learn from Marie Kondo, the ultimate authority on decluttering and reorganizing.

In _The Life-Changing Magic of Tidying Up_, you'll learn how to tidy and declutter — Japanese style. Marie Kondo's techniques have changed the lives of millions of people. Learn how to implement them yourself — and how to properly fold your clothes — in the blinks to _The Life-Changing Magic of Tidying Up_.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Julie Morgenstern

Julie Morgenstern founded the professional organizing company Taskmasters, a consultation firm with clients such as American Express. When she's not running her business, she's publishing her ideas as a columnist or speaking about them on TV. Organizing from the Inside Out was her first bestselling book.

