---
id: 5614322e36323300070b0000
slug: orientalism-en
published_date: 2015-10-07T00:00:00.000+00:00
author: Edward W. Said
title: Orientalism
subtitle: Western Conceptions of the Orient
main_color: 7DCCE5
text_color: 467280
---

# Orientalism

_Western Conceptions of the Orient_

**Edward W. Said**

_Orientalism_ (1978) shines a light on the often unquestioned assumptions about Eastern civilizations that are persistently prevalent in the West. By unearthing and analyzing the West's biases, Edward Said aims to undermine Orientalism's influence on how the West perceives and interacts with the East.

---
### 1. What’s in it for me? Understand the Western invention of the Orient. 

Have you noticed the way Asian or Middle Eastern countries are portrayed in travel commercials? The images are usually very exotic and seductive, conveying a dramatically different culture from the West's rational and scientific way of life.

You might consider these depictions to be harmless, but as you'll learn in these blinks, this simply isn't the case. Rather, these images constitute a way for the West to dictate what the East actually _is_, by branding these countries with the unifying term "the Orient". 

You'll learn how Orientalism, the discipline of studying the Orient, has affected and continues to affect relations between the East and the West. In addition to exploring the historical construction of this body of knowledge, these blinks will also reveal how — as Nietzsche said — what poses as truth is a fiction we have forgotten is a fiction. 

You'll also learn

  * how Orientalism aided colonialist expansion;

  * how the idea of the nation-state spurred on resistance movements; and

  * how the image of the Arab sheikh can legitimize Western intervention.

### 2. Orientalism is a Western creation that deals with a fabricated concept: the Orient. 

When you see a travel commercial for an Asian or Middle Eastern country, how is the country depicted? Perhaps the ad displays images and ideas of exoticism and seduction that exude a sense of geographical and historical distance. Despite there being an outdated feel to such images, they continue to be commonplace today, and these depictions can be linked to a body of Western knowledge called Orientalism. 

Orientalism constructed a specific image of the East — known as the Orient — as a means to approach it. Modern Orientalism was conceived during Napoleon's expedition to, and invasion of Egypt in 1798. In addition to his army, Napoleon had brought along civilian scholars, scientists and researchers who would produce a 23-volume encyclopedia on the country, entitled _Description of Egypt_.

This team of researchers was responsible for defining Orientalism, and the "experts" on the East were known as Orientalists.

This concept was fleshed out by other colonial powers, most notably Britain during the 19th century, and was the lens through which the West viewed the entire Orient, which was considered to include the Middle East, Asia and the Far East. 

The resulting image of the East was an exotic, erotic and irrational one, while Eastern stereotypes found in travel journals, newspapers and scientific publications began to proliferate. These presented 

the Orient as exotic and unfamiliar; as one equally strange and foreign entity, regardless of country, people or culture; and as the place where unseemly passions could run amok. 

Eroticism was viewed as the emblem of the Orient, with harems viewed as places where the "lustful Oriental" could be found. 

Finally, the people of the Orient were perceived as irrational and incapable of logic; the accompanying assumption was that the opposite of these traits were considered Western traits.

> _"Orientalism is the discipline by which the Orient was (and is) approached systematically, as a topic of learning, discovery and practice."_

### 3. Orientalism was influenced by economic and political interests, and claimed knowledge about the Orient that the Orient didn’t have. 

The act of studying something is surely positive, in that it enables us to better understand the subject in question. But what if doing so actually yields negative results? This is exactly what happened with Orientalism, in the sense that it actually reinforced the subjugation of the areas under scrutiny. 

For starters, Orientalism as a science was fueled by economic and political interests, starting with the Egyptian expedition and invasion led by Napoleon and his army.

Napoleon's research expedition included over 150 scholars and scientists, and the Orientalist scholars that it spawned served as educators and advisors to colonial powers who wished to better understand their colonies, while also increasing their influence within them.

These research teams helped protect French trade in Egypt. In order to gain more trade from Egypt, and to have Egyptians support French interests, Napoleon enlisted local imams alongside his Orientalist scholars to interpret the Koran in such a way that depicted the French army's presence as an advantage to the region's inhabitants. 

With the help of his scholars, Napoleon characterized himself and his army as the "true Muslims" in order to gather Egyptian support for French occupation. 

Furthermore, Orientalism was able to establish itself as an authority over the people in the Orient, as it was steered by "experts" who ostensibly knew more about the ancient Orient than the very people of the Orient. These scholars and linguists found and translated ancient Egyptian hieroglyphs and uncovered previously unknown archaeological digs, revealing ancient Egyptian monuments. 

This knowledge made it easier for the Orientalists to assert their dominance and influence over the local people.

> _"Knowledge of the Orient, because generated out of strength, in a sense, creates the Orient, the Oriental and his world."_

### 4. Socioeconomic developments forced Orientalism to change and adapt. 

Have you ever met someone you've heard so much about, but the person ends up being not at all how you expected? Well, the image of the Orient similarly didn't line up with the way many experienced the East, and this disrupted the way Orientalists examined the Orient.

First, the experiences of writers and social scientists no longer matched up with what was once recorded about the Orient. French poet Gérard Nerval, for example, once romantically wrote about a past Orient that he had never actually witnessed, but which he simply dreamed up in his book _Voyage en Orient_. 

When he did finally visit the Orient, he was shocked to find that it didn't line up with his vision, an image created by Orientalist practices and documents.

Second, anticolonialist and independence movements in the 19th and 20th centuries, such as the Egyptian Revolution against the British in 1919, pressured Western nations to take the East's own view of itself into account. 

These resistance movements and revolutions gave the Orient power, and Orientalists had three ways of responding to this disturbance.

In the first two ways, they would continue to observe and document the Orient as if it were a static object dictated by texts, and they would also attempt to determine how one's vision of the Orient had to change given recent events.

One example of these two methods in practice is Orientalist historian H.A.R. Gibb, who, in his 1945 lectures at the University of Chicago, kept to the traditional Orientalist narrative. 

Eighteen years later, however, while director of the Center for Middle Eastern Studies at Harvard University, he gave a lecture on "Area Studies Reconsidered," which supported the need to adapt research to the changing world.

The third way of responding to the Orient's newfound influence was to entirely give up the study of the East as "the Orient" — but this approach was seriously considered by only a handful of Orientalists.

### 5. Orientalism tried to prove its main findings about the Orient in different ways. 

After the resistance and revolutionary movements in the East, the West found it increasingly difficult to disseminate its so-called discoveries. Orientalism responded to this trend in a few different ways.

One approach was to expand. To compensate for their discipline's lack of findings, Orientalists pushed back the borders of the places under scrutiny, while also learning more about the people there.

Eighteenth-century Orientalism — as practiced in British and French colonies — was first centered around Islamic territories in Egypt and the Middle East. In the 19th century, it began encompassing other territories such as India, China and South America.

Trade development, travel writings, scientific reporting and utopian imagery that exoticized these areas all assisted in the expansion of the geographical scope of the Orient.

Another response was to enter into dialogue with the Orient with the aim of shaping it. One example of this was academic research in comparative disciplines. For example, George Sale's 1734 translation of the Koran and accompanying commentary was one of the first commentaries on the Orient that dealt with its subject by interacting with Arabic scholars.

Rather than merely stating what the Orient was, Sale was the first to converse with those living there and presented their views and accounts regarding the text. This far more open-minded attitude increased communication between the West and the Orient.

However, although there was dialogue, it wasn't necessarily equal. While this dialogue allowed scholars to report what people from the Orient had to say, the resulting findings were still used as a means to advance Orientalist aims of Western colonialism and economic presence.

For example, the West-East dialogue meant that administrators and functionaries from local populations could help spread European ideas. This happened as early as the late-18th century with Napoleon's enlisting of muftis and imams, who, with the help of the Koran, championed the righteousness of French presence to the Egyptians.

### 6. Two people in particular helped establish the findings of Orientalism as a science through categorization. 

In the eyes of many Orientalist scholars, the best way to understand the Orient was to categorize its types of inhabitants, and this was done by studying the region's languages.

Silvestre de Sacy (1758-1838) was among the first to categorize the Oriental mentality in accordance with each region's respective languages. De Sacy was a linguist, and joint founder of the Asiatic Society — a French academic society focused entirely on Asia. De Sacy believed that thorough research into the language of a people would offer a deeper understanding of that culture's mentality.

To his credit, de Sacy was among the first who wanted to understand a language in order to understand the mind using it. However, his approach ignored linguistic nuances and variations within the Orient, and filtered Oriental languages through his native French. 

Instead of adapting, de Sacy utilized French to typify the peoples of the Orient, with French considered the epitome of rationality and logic, and Semitic languages typified as irrational and emotive. To de Sacy, the speakers of the languages and the languages themselves were one and the same.

Ernest Renan (1823-1892) continued some of de Sacy's work, but instead connected language with race. 

There was a fundamental tension in Renan's work, which fit well with the doctrine of Orientalism: while he saw each language as a development of an earlier language and related to others throughout human history, Renan focused his effort on how certain races were inherently superior to others and, specifically, how Europeans were superior to Orientals.

While emphasizing historical diversity, Renan formed an Oriental type, reporting that their language actually rendered them incapable of progressing to the level of the West.

> _"In time, the Orient as such became less important than what the Orientalist made of it."_

### 7. The very tools used by Orientalists to observe the Orient prevent them from observing its nuances. 

When visiting a foreign city, do you find yourself hoping to avoid any touristy activities, and instead wanting to do as the locals do? The Orientalists of the 19th century did exactly that in order to gather more information about the Orient. 

Extended stays and immersion in local cultures allowed Orientalists to form categories with which to observe the Orient. 

Edward William Lane (1801-1876), author of _The Manners and Customs of Modern Egyptians_ (1836) and translator of _1001 Nights,_ is one example. Lane spent much of his life in the Orient, dressing in local clothing styles and even sending his sister to harems and women's bathhouses to study the lives of Oriental women.

There were two key parts to Lane's research: First, he would immerse himself in daily Oriental life, fastidiously recording practices and routines. Second, he would withdraw from that life in order to be able to report back on his findings.

However, categorizations developed by Orientalists clouded the nuances of the cultures in question.

A number of practices and categories created by the Orientalists made it particularly difficult to observe each culture's complexity.

Categories such as Oriental, Semitic, Arab, Muslim, Jew, as well as categories of race, mentality, type and nation, while somewhat helpful, would simultaneously lump variations and diversity within individuals, families and cultures under the same umbrella, thereby obscuring their numerous differences.

Exceptions to broad categories were seen as transgressions from general trends and not worth spending much time on. For example, Orientals were seen as fairly irrational and primarily guided by passion, so any Oriental demonstrating clear rationality was simply seen as an exception.

### 8. Colonialism, resistance movements and the First and Second World Wars changed Orientalism. 

The turn of the 20th century saw the rise of an increasingly globalized and connected world, and the East began asserting itself against European dominance. 

Anticolonialist resistance movements began to change relations between the Orient and the West. In other words, they caused the West, and specifically Europe, to question its economic and political presence in the Orient.

Such movements included the Indian rebellion of 1857, the Egypt revolutions of 1919 and 1952 and other anticolonial movements in African colonies. 

Gradually, the concept of the nation-state became an ideal worth emulating, and the idea of nationalism, which was likewise a European invention, was employed by colonies against their respective colonizers. 

People in the Orient used the concept of nation-states against the West, branding the West a foreign invader that was hindering national sovereignty, while embracing the notions of liberty, equality and fraternity.

Nation-states such as France or England were viewed as an ideal, and their virtues, such as liberty, equality and fraternity, were to be adopted. 

In addition to these movements, the two World Wars likewise shifted the West's attitude toward the Orient.

The World Wars diminished European power and weakened European territorial claims around the world. Following the two costly wars, European nations could no longer involve themselves in the colonies as much as they were previously able to, largely because of the vast workforce and huge sums of money required to rebuild at home.

So, colonies began to cost more than they were worth, which brought increasing economic strain upon the colonial powers. In turn, this meant European powers could no longer assert their superiority over the East. The Orient thus became neither an economic resource to mine wealth from, nor a defeated area over which to rule.

> _"Orientalism calls into question not only the possibility of nonpolitical scholarship but also the advisability of too close a relationship between the scholar and the state."_

### 9. The United States has become the center of Orientalism today. 

You may think that as colonialism came to a close, Orientalism also ended. Orientalism, however, is still very much alive today, but in a new form based in the United States, and with three key characteristics.

The first key characteristic of this American brand of Orientalism is its presence in popular imagination. Take the concept of the "Arab," for example, which first entered the Western imagination during the oil crisis of 1973.

During that time, images and cartoons of an Arab sheikh with a hooked nose by an oil pump were common. These pictures continued to employ "Semitic" images that were found in anti-Semitic pamphlets throughout the 19th and 20th centuries.

By depicting the Arab as the strange or uncultured Other, the East was cast as the villain in popular imagination, making it easier for people to accept the notion of Arabs being less civilized and providing a perpetual justification for American intervention.

The second characteristic is Orientalism's presence in universities.

Though there may be no departments of Orientalism per se, what used to go under that name continues to be practiced in the university system across various disciplines, including political science, sociology, anthropology, history and psychology.

Here, stereotypes of Eastern nations and people, along with generalizations referring to "Muslims," "Arabs" or "Islamic laws and cultures" are used, positioning them as fundamentally opposed to the West, and a threat to Western civilization.

The third characteristic is Orientalism's role in government policies.

Public policy groups and think tanks support modern Orientalism. Consider the broad influence of Samuel P. Huntington's _Clash of Civilizations_, which is commonly used as a reference text to analyze cultural differences. Difference is framed by applying the aggressive word "clash," suggesting that fundamental, irreconcilable differences separate cultures from one another.

It's crucial to note that books such as Huntington's, as well as ongoing research funded by think tanks, are used as a means to plan foreign policy; the resulting knowledge then becomes the dominant theory behind Western government practices.

### 10. Final summary 

The key message in this book:

**The notion of Orientalism was fabricated by the West in an attempt to understand the East. However, the Orient as it was created doesn't represent the real East; rather, it is a lens through which the East was approached, studied and categorized by the West through its own imagination.**

Actionable advice:

**Listening for words and watching for imagery deepens understanding.**

The next time you see a news report on the Middle East or the Palestinian-Israeli conflict, observe the ways in which the two sides are portrayed, particularly the adjectives and images used. The best way to understand the nuances of a conflict is to notice the words associated with both sides.

**Notice Western marketing of Eastern destinations.**

The next time you see a travel advertisement for a vacation in the Middle East, such as in Dubai, notice how the location is exoticized. You'll likely find that the East appears to be a timeless place and a resort for the Western vacationer to visit and discover the area's treasures.

**Suggested further reading:** ** _From the Ruins of Empire_** **by** **Pankaj Mishra**

In _From the Ruins of Empire_, author Pankaj Mishra examines the past 200 years from the perspective of Eastern cultures and how they responded to Western dominance. The book charts in detail the colonial histories of Persia, India, China and Japan in the nineteenth century to the rise of nation-states in the twentieth century. Select stories of cultural figures help to humanize the often violent clashes of cultures, showing the powerful influence of individuals in the course of history.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Edward W. Said

Edward Said was a highly influential Palestinian-American intellectual and leading literary critic who helped found the field of postcolonialism. His works include _The Question of Palestine_, _Covering Islam_ and _Culture and Imperialism_.

