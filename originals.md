---
id: 577fbf53c088090003fe9f1f
slug: originals-en
published_date: 2016-07-13T00:00:00.000+00:00
author: Adam Grant
title: Originals
subtitle: How Non-Conformists Move the World
main_color: D32A97
text_color: BA2586
---

# Originals

_How Non-Conformists Move the World_

**Adam Grant**

In _Originals_ (2016), Adam Grant taps into the field of original thinking and explores where great ideas come from. By following unconventional rules, Grant gives us helpful guidelines for how we can foster originality in every facet of our lives. He also shows that anyone can enhance his or her creativity, and gives foolproof methods for identifying our truly original ideas — and following through with them.

---
### 1. What’s in it for me? Embrace the original in you! 

We have all heard of that elusive quality known as "originality." But while it remains something highly coveted, referring to someone as an "original" can also bring up connotations of eccentricity or weirdness.

So, can there be originality without originals?

These blinks look at the ways in which you can be non-conformist and original without getting shunned, as well as some of the originals who have challenged the status quo and pushed innovation throughout history.

In these blinks, you'll find out

  * how your choice of web browser can indicate whether you're original or not;

  * why advocating against your innovative idea is a great way to get people on board; and

  * how procrastination paved the way for one of the most famous speeches in history.

### 2. Originality is your key to a fulfilling career. 

Look in any dictionary and it'll tell you that originality is the quality of having a unique or singular character. But what's _an original?_ In today's context, originals are people who not only dream up novel ideas and shake up the status quo, but who also take the initiative to make their unique vision a reality.

Even the smallest things can identify an original. Economist Michael Housman discovered in his research that a certain percentage of customer service employees stayed in their jobs far longer than others. Seeking clues, he discovered a surprising link between how long someone kept their job and their choice of internet browser. Sounds crazy, right?

Get this: employees who installed browsers other than the default Internet Explorer were not only more likely to keep their jobs, they were also more likely to take initiative, confront challenges and find new solutions. In the end, the tendency to install Google Chrome or Mozilla Firefox was tied to problem-solving abilities that, in turn, allowed these employees to stay in their jobs an average of 15 percent longer.

As for the other employees who simply used built-in browsers, they approached their roles in the same conventional way as they used the internet. They accepted the standards given to them and were unable to solve problems, which eventually made them sick of their jobs.

If you want to survive in the working world, your best bet is to become an original. The good news is, anyone can do it. Though we might not all be able to found our own companies, compose a musical masterpiece or alter the course of history with a stirring speech, we all have unique ideas with the potential to improve our work, our communities and our relationships.

Putting new ideas out there requires courage and the determination not to back down when you want change to happen. The first step toward becoming an original is overcoming your fear of taking action and standing up for your own ideas. But how? Find out in the next blinks.

### 3. Quantity leads to quality when it comes to generating great ideas. 

Legend has it that physicist Isaac Newton was relaxing under a tree when an apple fell on his head. In a flash of brilliance, Newton was inspired to develop his law of universal gravitation. Unfortunately, great ideas like this usually don't fall from trees; new ideas require hard work.

When it comes to idea generation, what's more important — quantity or quality? As it happens, they're equally important, specifically because quantity paves the way for quality in brainstorming. Psychologist Dean Simonton, renowned for his study of creative productivity, demonstrated in his research that highly creative individuals don't necessarily produce _better_ ideas; rather, they just make more of them.

By creating a larger volume of work, they had a higher probability of developing a small handful of brilliant ideas. For instance, Picasso's entire body of work includes countless rugs and prints, 2,800 ceramics, 1,800 paintings, 1,200 sculptures and more than 12,000 drawings. And yet, only a small number of these pieces gave Picasso his success and status as an international art icon.

In other words, when it comes to quantity and quality, you can't have one without the other!

Another of Simonton's findings demonstrated that even geniuses can't tell which of their works will become timeless classics and which ones will flop. So, again, the more you produce, the better. Simonton found that Beethoven judged his work quite differently than later experts did. Comparing letters where Beethoven rated 70 of his own compositions with the evaluations of contemporary critics, Simonton calculated that Beethoven had disagreed with them about 33 percent of the time!

Generating ideas, and lots of them, is the first step to unlocking your creative potential. But you shouldn't see your brain as a creativity factory, pumping out original ideas the way cars are manufactured on an assembly line.

To create great ideas, we need to take it slow. That could mean taking a detour and procrastinating, or just making the time here and there to relax under a tree!

### 4. Procrastination can work creative wonders when you use it strategically. 

Procrastinating, we're told, is your productivity's arch-nemesis; but is this really the case?

Leaving stuff to the last minute makes us more creative by forcing us to _improvise_. Would you have guessed that Martin Luther King Jr.'s most famous line was the result of procrastination? King was set to give a speech at the March on Washington for Jobs and Freedom, but didn't even start writing the speech until the night before.

King's iconic "I have a dream" line was partially improvised — gospel singer Mahalia Jackson cried out during King's speech, imploring him, "Tell them about the dream, Martin! Tell them about the dream!" King abandoned his script and began to speak freely about his inspiring vision of the American future.

King's speech is a fantastic example of the _Zeigarnik effect._ The phenomenon, named after Russian psychologist Bluma Zeigarnik, describes the way that our mind stays open to new ideas and insights, even after we attempt to finish a task and give up. Essentially, King's unfinished speech left room for his brain to come up with brilliant lines.

For great originals, procrastination is a key strategy. It allows them to make gradual progress while remaining open to a range of possibilities. Leonardo da Vinci is another example of history's prolific procrastinators. He began painting the Mona Lisa in 1503, then abandoned the project before returning to the painting some years later. The Mona Lisa was finally completed in 1519, 16 years later!

Historian William Pannapacker believes that this allowed da Vinci to procrastinate in a calculated way, experimenting with optical illusions and new painting techniques. Without this experimentation, and the procrastination that created the space for it, we may never have had the Mona Lisa or other brilliant works by original thinkers.

### 5. Admitting your weaknesses during a pitch will garner you more support. 

Have you ever gotten a less-than-encouraging response to what you thought was your best idea yet? Rest assured, it's not always because your idea is useless! There are a few common factors behind rejections.

For one thing, voicing an opinion that threatens to upset the status quo can be a threat to your business career and your network. A massive study conducted across nonprofit, service, retail and manufacturing companies revealed that the more frequently employees voiced their ideas and concerns to their superiors, the less likely they were to receive raises and promotions over a two-year period.

This is quite a troubling trend! So, what can you do to get people on board with your ideas? Strangely enough, your best option is to tell people why they should _not_ accept your proposals. Start by being open about the shortcomings of your projects; this will surprise your audience and show them that you're an honest person regardless of the situation.

This is what entrepreneur couple Rufus Griscom and Alisa Volkman did when presenting their online parenting magazine and blog network "Babble" to potential backers. To their audience's great surprise, Griscom was up-front and told them that their website's user engagement was lower than they'd expected, 40 percent of the news on the site was taken up by seemingly irrelevant celebrity gossip and their back end was in major need of an update.

Though it sounds like they were shooting themselves in the foot, investors were charmed by their approach. They trusted them, and Babble brought in $3.3 million in funding before being acquired by Disney in 2011.

### 6. Make radical ideas familiar by getting them out there often and finding common points of reference. 

Do you think you look better in the mirror or in a candid photo? Most of us would prefer the reflection we see in the bathroom. Photos of ourselves can be cringeworthy and off-putting. Why is this? Well, because we're seeing ourselves from an unfamiliar angle. It's a classic human tendency to reject things that aren't familiar to us — even our own images!

As you might have guessed, this presents another hurdle to dreaming up original ideas. But there are strategies you can implement to make even the most conventional coworker feel comfortable with your unorthodox solutions.

One of these is the _mere exposure effect_, where repeating yourself will give others time to warm up to your ideas. Research shows that exposing people to new ideas more often will make them more receptive over time. So, speak up and repeat yourself!

To make this easier, keep your ideas short and snappy, blend them with other ideas to show their different applications and be prepared to keep pushing your solutions for as long as necessary. Keep at it, and you'll be surprised by how your peers' responses improve!

Another useful strategy for making new ideas seem less controversial is to frame them in a familiar context. When the idea for the animated classic _The Lion King_ was first pitched to Disney, producers were initially turned off by its dark storyline.

But in a meeting between scriptwriters and Disney executives, CEO Michael Eisner and producer Maureen Donley turned things around by highlighting the film's similarities to Shakespeare's _King Lear_ and _Hamlet_. This was enough to persuade the producers, who were much more enthusiastic once the unconventional storyline was tied to a common point of reference.

_The Lion King_ went on to become 1994's highest-grossing film and the recipient of two Academy Awards. This example illustrates how great ideas can become a reality when their novelty is offset with familiar elements to win support.

### 7. The best collaborators are the ones that love to prove you wrong. 

Regardless of what you're pursuing, if you only listen to people who praise you, you probably won't get very far. It might not be pleasant, but sometimes you need a bit of criticism to help you grow.

This was illustrated in a pivotal experiment by psychologist Charlan Nemeth. Groups of participants were asked to hire one of three possible job candidates. The first candidate, John, was presented as having the best skillset for the job.

Even so, some of the participants showed a preference for the less qualified candidate, Ringo. But when some participants argued in favor of the third candidate, George, the chance that the participants would end up hiring the best-qualified candidate quadrupled. How can we make sense of this?

By throwing a minority opinion into the mix that differs from the two leading views, the consensus is disrupted. Group members are then pushed to assess the situation for themselves and not simply follow what others are thinking. This is a great strategy to break up _groupthink_ and encourage everyone to share their real opinions.

Groupthink occurs when people organized in groups prioritize avoiding conflict and reaching consensus over making the best choice possible. This concept, developed by Yale research psychologist Lester Irving Janis, is the underlying problem in poor team decision making. Another way to prevent groupthink hindering your own creativity is to surround yourself with people who constantly question your ideas.

This was the strategy used by Ben Kohlmann, a founding member of the _Chief of Naval Operations' Rapid Innovation Cell_ (CRIC), when his team began to work on innovative ideas for the navy. They succeeded in creating a whole range of creative solutions and were even the first to bring a 3D printer on a ship to print spare parts in case something broke while at sea.

This creativity wouldn't have been possible without the powerful group dynamic that emerged as a result of Kohlmann's calculated decision making. He chose junior officers with a track record of facing discipline as a result of challenging authority. Though these officers all had their own backgrounds and objectives, uniting their disruptive mindset with a common goal created the perfect environment for creativity.

### 8. Learn to disguise your ideas to get the supporters you need. 

Though you might have a network of people who share the same goals and values as you, it's no guarantee that they'll support your ideas. If you want dependable allies, you need to win over your peers by hitting the right tone in your messaging. The trick is not to go over the top, but also to keep people interested.

Though we tend to think that common goals are what brings a team together, research has shown that the opposite is true. Dartmouth College psychologists Judith White and Ellen Langer illustrate this finding through the theory of _horizontal hostility_ ; this is a form of prejudice that surfaces in relationships between members of the same minority group.

For instance, the most dedicated members of radical political groups tend to attack each other more than they confront impostors and sell-outs within their movement, even though they share the same set of core values.

You can avoid horizontal hostility in your team by making your ideas seem a little less radical. To do this, you'll need a disguise — or even a Trojan horse! The goal is, after all, not to convince people to change their attitudes entirely, but to connect with the values you know they already believe in.

Meredith Perry, the inventor of wireless power solutions for charging electronic equipment, received little support when she first presented her ideas to her physics professors and engineers. They all unanimously agreed that it was simply not possible at the time to charge electronic devices through waves passing through the air. So what did Perry do? She changed her tactics and used a Trojan horse.

By disguising her idea and telling people that she simply wanted to design a transducer, and not one that sent power wirelessly, she received a lot more support: her idea was interesting, but not too far-fetched. Collaborators and funders were much more willing to team up with her, and Perry was able to create her product and company, _uBeam_, which today provides innovative wireless charging solutions.

As we can see, it's not enough simply to have creative ideas — you have to know how to find the right supporters and collaborators to make them a reality.

### 9. Final summary 

The key message in this book:

**Unleash your creative potential by developing lots of ideas and sharing your best ones with others. To further boost your creativity, surround yourself with disruptive thinkers and make them feel comfortable about sharing their opinions. Learn to make your original idea seem familiar, accessible and appealing to get the support you need, and you'll be ready to turn your unique plans into real-world solutions.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Art Of Non-Conformity_** **by Chris Guillebeau**

Based on the author's blog and online manifesto, "A Brief Guide to World Domination,"_The Art of Non-Conformity_ deals with ways of pursuing unconventional living and offers tools for setting your own rules, succeeding with your passions and leaving a legacy.
---

### Adam Grant

Adam Grant is an acclaimed consultant and public speaker on the topic of human resources and management. Grant's clients include Google, Goldman Sachs and Disney Pixar; he is an award-winning professor at the Wharton School of the University of Pennsylvania; and he contributes frequently to _The New York Times_ on the topic of workplace psychology.

