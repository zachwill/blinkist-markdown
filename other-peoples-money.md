---
id: 596105fcb238e100054fb0be
slug: other-peoples-money-en
published_date: 2017-07-13T00:00:00.000+00:00
author: John Kay
title: Other People's Money
subtitle: The Real Business of Finance
main_color: B8252C
text_color: B8252C
---

# Other People's Money

_The Real Business of Finance_

**John Kay**

_Other People's Money_ (2015) offers a detailed breakdown of the financial sector: how it functions, the effect it has on economies and what its purpose should ideally be — as opposed to what its current purpose is. Find out why the international financial sector has become a ruthless mechanism made up of rotten parts, and discover how these parts can be eliminated and, eventually, replaced.

---
### 1. What’s in it for me? Get a fresh understanding of the modern financial sector. 

If you follow the news, you will have noticed that the one thing that seems almost as prevalent and inevitable as war and violence are constant financial crises.

Most of us will remember the significant damage done by the world's last major economic collapse, the 2008 financial crisis — but far fewer of us will understand why exactly it happened. Was it the fault of banks, businesses or governments?

The problem is, it all seems very complicated. What were the root causes of the financial crisis? How does the world of finance operate, anyway? These blinks set out to explain why the financial sector is so important, and also dangerous.

In these blinks, you'll learn

  * why finance is so necessary;

  * what went wrong with the financial system; and

  * how the world of finance can reflect societal values again.

### 2. History shows that a healthy financial system can improve lives and strengthen economies. 

These days, the global financial system has earned a reputation as one of the root causes of many of the world's problems. But it didn't start out this way.

Finance was originally intended to improve our quality of life by making it easier to conduct business.

The financial system allows us to buy the things we need to live comfortably and connects people who need money with those who can lend money. It also makes the insurance business possible, giving us the chance to protect ourselves from major disasters or emergencies, and allows us to organize our personal assets so that we can pass them along to subsequent generations.

Mortgages are another mutually beneficial part of finance. They make it possible for people to own property by taking out a loan, and subsequently making monthly payments with interest to the lender as a reward for supplying the loan.

On a larger scale, a healthy financial system can benefit society as a whole, which becomes clear when we look back at the progress we've made over several centuries.

It was a strong financial sector that allowed Britain and the Netherlands to become world powers in the early days of industrialization. As investments continue to pour into developing countries, it's strong finances that allow this money to spread and improve the standard of living.

We can also see how the undynamic financial systems of communist countries lead to failure time and time again. In these centrally controlled systems, the money is unable to flow to businesses in need, which leads to fewer jobs and poor economic growth.

However, even though capitalist freedom has led to the kind of prosperous industrialization that became a cornerstone of modern society, not all of its financial innovations have been good. In fact, some of them haven't been beneficial to us at all.

As it turns out, the financial industry has lost sight of what's good and bad for the global population as a whole. We'll get to the bottom of why this has happened in the blinks that follow.

### 3. The derivative market has driven finance further away from benefiting the real economy. 

In its early days, finance centered around helping people get the money they needed to realize their ideas. Think of a bank providing a baker with enough capital to open up his own bakery.

But due to rampant financialization, those good old days are gone, and we're drowning in needless transactions that aren't doing us any good.

The rampant trading activity and massive growth in the finance sector is called _financialization_. The problem is, financialization helps banks and stock markets, but hasn't had any positive impact on household income, small business growth or the economy at large.

Financialization started in the 1970s, when large institutions started trading more and more securities, which are financial assets such as stocks, bonds and mutual funds. But what really pushed financialization over the edge was the emergence of a new market based on derivative securities.

Derivatives are like contracts that base their value on how well other assets perform. Based on the contract created, one party can end up benefiting from an asset's price going up or down, depending on the kind of derivative it is.

One kind of derivative is known as a _credit default swap_ (CDS), which allows a bank to protect itself against a borrower defaulting, or failing to make payments, on their loan or mortgage.

In a way, it functions like insurance: the institution investing in the CDS is promising to pay the bank in the event of a default, while the bank is promising to eventually reimburse them, with interest.

This is how we ended up in the recent financial crisis of 2008: too many banks were giving out loans to people who were in no position to honor them. Then, too many people defaulted on their loans, and suddenly all the institutions needed money from each other. The system collapsed upon itself.

This crisis was further impacted by technology that made it easier than ever for people to trade securities and derivatives, leading to massive artificial inflation of the financial sector.

These practices are tantamount to gambling with other people's money, which, as the recent crisis has shown, isn't helping anyone.

### 4. Since the 1970s, finance professionals have had less incentive to act in their clients’ interests. 

In the years before financialization, the economy was relatively stable due to a financial sector that promoted a culture of caution and planning. By contrast, today the finance sector acts as though it has little to lose, so we end up with a far more volatile economy.

And though it may sound strange, banking executives aren't exactly encouraged to act in their banks' best interests.

Prior to financialization, executives were expected to invest their own money into their banks, which gave them a great incentive not to fail — and to avoid taking too many risks.

So, when it came to providing mortgage loans, it was in their best interests to grant these to reliable people who were likely to hold up their end of the bargain and make regular payments.

Back then, being a bank manager was also a lifelong job, so they would be around long enough to face the long-term consequences of their actions. But now, these executives make decisions based on an "I'll be gone, you'll be gone" approach, which involves making as much money as possible in the short term, and then moving on before it all falls apart.

In the years leading up to 2008, countless mortgages were being granted to people who were clearly unlikely to be able to pay them off, which was a major contributing factor to the crisis that followed.

Another conflict of interest can be found in the new breed of broker-dealers.

Brokers were traditionally agents who helped bring two like-minded dealers, or traders, together. But during the time of financialization, brokers began making their own deals. They were no longer just making money on commissions from their clients, they were also making profits from their own deals.

Naturally, this meant it was no longer in their best interests to steer clients toward the best deals. A broker-dealer is now more likely to steer clients toward a less lucrative trade and save the best deals for themselves.

### 5. The banking collapse of 2008 was due to transactions made by greedy dealers with no accountability. 

Many people involved in the global financial crisis of 2008 like to claim that it was impossible to see it coming — but this isn't true at all. In reality, what happened was the logical culmination of decades of dangerous and selfish decision making.

It's hard to see what else would happen when everyone in the finance sector was driven by an insatiable desire to make as much money as possible.

This mentality can be directly connected to the transition banks made from being largely private or family-owned businesses to being run by corporations.

Once banks became owned by shareholders and banking executives were in control of shareholders' money rather than their own money, they were now free to take much greater risks. And with no personal attachment to the money, they were in no position to be held accountable for any losses that might occur.

With so little at stake, they were free to be driven by greed and make deals that would either pay off with huge rewards or cause damage that would have no effect on them personally or professionally.

Meanwhile, the introduction of credit default swaps and _mortgage-backed securities_ (MBS) only added to the lack of accountability and further destabilized the financial sector. As the name implies, for a mortgage-backed security to reap rewards, the mortgage and its associated payments must go smoothly.

So, the whole mess began when greedy bank managers offered mortgages to people who were clearly unable to make the necessary payments.

Naturally, this caused the MBSs that Bank A was trading with Bank B to be very insecure. To offer added security, Bank A would also trade some CDSs to Bank B, thus supposedly protecting themselves from possible defaults.

But because Bank B was buying CDSs with nonexistent security from MBSs, both banks were destined to end up in deep trouble when the mortgages defaulted.

In the end, both banks needed money but had none, which is why national governments then had to bail them out.

### 6. Through lobbying and donations, financial institutions have a massive influence on government policy. 

The titans of finance often have friends in high places, including senators and other politicians. Some financial institutions even employ former government officials as consultants, guaranteeing them access to political networks and giving banks an inordinate influence in legislatures and legal systems.

Perhaps it is unsurprising, then, that the finance sector spends more on political lobbying than any other sector.

In the United States, between 2012 and 2014, the finance sector spent $800 million on lobbying, which doesn't include an additional $400 million that was donated to various candidates' electoral campaigns.

Naturally, this amount of money doesn't come without expectations, and those who win the elections are likely to feel a responsibility to somehow return a bank's generosity.

This leads us to the immense government bailouts that followed the 2008 crisis, a clear sign of how much influence the financial sector has on the US government.

Beyond that, it also set a dangerous precedent: it sent the message that it's acceptable for banks to be reckless, since taxpayers will clean up their messes. Any lessons that could have been learned from what became a global disaster were quickly pushed aside.

The banks should have known they were playing with fire by financing subprime mortgages, that is, mortgages for people with lower credit scores and who were deemed to be a higher default risk. But the big corporate banks were also well aware that they were considered too big to fail by the government, so they knew they had a permanent safety net.

The finance sector has yet another important advantage working in their favor: since most people don't understand their complex deals and confusing terminology, it's easy for them to get away with greedy and irresponsible decisions.

Unless you work in finance, it's doubtful that you know what words like "subprime" or "derivative" really mean, and unscrupulous dealers can use this to their advantage when coming up with excuses for why they lost all your money.

Before 2008, many people assumed that the well-paid professionals in the finance industry made rational decisions. But the crisis revealed the truly irrational and risky behavior that's been going on for years.

The sad part is, they didn't pay for these actions — we did.

### 7. Financial regulations aren’t always helpful. 

Another word you'll hear a lot when people talk about the financial crisis is "regulations." And contrary to what some might say, there were a lot of regulations in place when everything came crashing down — some of which date back to the infamous crash of 1929 and the Great Depression that came immediately after.

But this is another important lesson that we should take from the 2008 crisis: sometimes regulations do more harm than good.

When we pile on one regulation after another, it becomes more difficult to implement effective policy oversight. More often than not, these excessive rules will merely encourage people to find new and creative ways of working around them, eventually making matters worse.

This is what happened with _Regulation Q_, which set a maximum interest rate on bank deposits, such as those in a savings account.

Regulation Q worked until the mid-1980s, when US banks began bypassing the regulation by depositing their money into European banks that weren't beholden to the regulation, and then transferring the money back to the United States.

So, rather than discouraging bad business practices, as the regulatory agency had hoped, it just created more work for the authorities.

One potential solution would be to create a set of international regulations, which is the primary topic of discussion at G8 and G20 summit meetings — but these meetings have yet to be very productive.

Another troubling reality of regulations is that they make finance extremely complicated, which generally benefits the institutions more than their customers.

Added regulations can make it harder for the average customer to understand even basic transactions. And when we become dependent on supposed experts or specialists in a field, we become far more vulnerable to being taken advantage of.

The Securities and Exchange Commission (SEC) is in charge of enforcing financial regulations in the United States, and their aim is to make the system as transparent as possible.

However, the average person will never know what's really going on with their money if the information available to them is so full of jargon that it might as well be in a different language.

And as long as outsiders are kept in the dark, it will to continue to be business as usual for the financial sector.

### 8. The finance sector needs to be restructured, with positive change coming from within. 

Today's politics are divisive, but there is one thing most people should be able to agree on: we need to stop the financial sector from making profits through shady deals that end up costing hardworking people their life savings and require governments to bail out major banks with taxpayer money.

If we want to be sure that we're not walking into another crisis, we need to restore trust.

This means restructuring the financial sector in a way that removes the temptation for professionals to act in their own self-interest over their clients'.

One step in this direction would be to better separate the jobs in the financial system and make sure that positions like broker-dealer are no longer possible. This way, brokers would only be rewarded for negotiating successful deals between two parties, and would be prohibited from acting as dealers and arranging self-serving financial transactions.

Another step would be to protect people's savings deposits and keep them off-limits from banks and other financial trading institutions.

This would keep people's life savings safe in the event of another major collapse, and would prevent banks from using their clients' money as just another set of assets to include in their payments and trades. Also, it would give a bank's greedy traders far less of other people's money to play with, thereby reducing the likelihood they'd take on high-risk transactions.

But perhaps most important of all is to have proper ethical conduct be promoted from within the financial system.

Having the SEC threaten, and occasionally enforce, billion-dollar fines is clearly an ineffective way of promoting good behavior. Ethical business practices must be instilled from within the institutions themselves, by executives and leaders who set a good example and reward those who adhere to similarly ethical practices.

Obviously, this is easier said than done. But every transformation has to start somewhere.

### 9. Final summary 

The key message in this book:

**As the recent banking crisis proved, the financial sector has become a dangerous, self-serving beast that offers little in the way of benefits to the average person. However, history shows that our financial system can work when regulations are streamlined and ethical business practices become the norm rather than the exception. With some considerable restructuring, the financial sector could once again be a valuable and useful system that makes our economy stronger and improves our overall quality of life.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Makers and Takers_** **by Rana Foroohar**

_Makers and Takers_ (2016) investigates the role of finance in the 2008 crisis and subsequent recession. From the Great Depression onward, these blinks trace the history of loose regulation and blurred boundaries between commercial and investment banking, while highlighting the role of banks, businesses and politicians in the crisis. They also suggest actions the powerful can take to kickstart reform.
---

### John Kay

John Kay is an economics professor and has been a fellow of St. John's College at the University of Oxford since 1970. He is also a regular contributor to the _Financial Times_ and the author of the books _Obliquity_ and _The Long and Short of It_.

