---
id: 55f6e05b29161a0009000081
slug: our-kids-en
published_date: 2015-09-16T00:00:00.000+00:00
author: Robert D. Putnam
title: Our Kids
subtitle: The American Dream in Crisis
main_color: 4F9492
text_color: 346160
---

# Our Kids

_The American Dream in Crisis_

**Robert D. Putnam**

_Our Kids_ (2015) takes a look at the crisis of opportunity in America, where success depends more on wealth than on hard work and ability. The American dream has been betrayed, the U.S. opportunity gap is widening; this book will tell you why, and explain what every U.S. citizen can do to fix the problem, ensure equality and save democracy.

---
### 1. What’s in it for me? Learn how we're killing the American dream. 

We tend to assume that the world is becoming smaller each day — just think of the global connectivity of the internet. At this very moment, however, the United States is becoming more and more divided.

This isn't a division between different states, but between different classes. Say you have a middle-class background. Chances are, you're living much of your life in a middle class bubble — house in a middle class neighborhood, children at school with other middle class children. You get the picture. But things haven't always been like that. For someone who went to school in the 1950s, it would have been fairly common to sit right next to the child of the local factory owner and play catch with children of blue-collar workers. 

Why does this matter? 

Because this kind of segregation is one of the main things hampering upward mobility. For instance, in that 1950s class, a child with uneducated parents would pass the day with children possessed of rich vocabularies, to positive effect. Today, that child would be put in a class with other lower-class children, in a school ill-equipped to prepare even the brightest students for competitive colleges. By reading these blinks, you'll learn more about the opportunity gap in U.S. society. 

You'll also find out 

  * why many of your children's career opportunities were determined by the time they were five;

  * why it's advantageous for radical political groups for kids to be left behind; and

  * what you can do to fight social inequality in everyday life.

### 2. The promises of the American dream are slipping ever further out of reach. 

We've all heard the phrase "from rags to riches," a slogan that sums up the _American dream_. Believing that anyone can pull himself up by his bootstraps and, with nothing but perseverance and some natural ability, achieve economic success, has been in the mainstream for some time. But how realistic is it today?

Well, popular opinion about this dream has remained more or less stable, and practically all Americans agree that everyone should be afforded equal opportunities. That's because few things represent the popular view of U.S. society as well as the American dream. 

For instance, studies have shown that 95 percent of Americans agree with the idea that society should ensure every citizen the right to equal chances for economic success, regardless of their parents' affluence.

However, while Americans still support the idea of the dream, the reality is that access to opportunity across social classes is grossly disproportionate. That's because the overall economic prosperity of the '50's and '60's provided an unprecedented and since-unattained level of equal opportunity. 

Just consider Port Clinton, where the author grew up, a city that well represents the rest of the country economically, socially and demographically. In 1959, people living there enjoyed a lot of upward mobility. That meant, regardless of their class, the majority of the kid's the author went to school with went on to pursue higher education and realized greater affluence than their parents. 

But Port Clinton has changed. Social classes are now separated by an economic divide, and children from different neighborhoods are afforded drastically different career opportunities. In fact, the same thing is happening across the country. For instance, in 2004, children whose parents were in the top 25 percent of wealthiest and most-educated people were 17 times more likely to go to an elite university than children whose families fell nearer the bottom quarter.

### 3. Modern family structures favor children from higher social classes. 

So what is the American dream really about? It's about getting a fair start in life, and, for most people, that means a period of nurture and care provided by a family. However, family norms have changed as families have grown more diverse, a fact that has had mainly negative effects on children born into the lower classes. 

The fact is that traditional families are no longer the rule. Things like a breadwinning father and a stay-at-home mom are things of the past, in large part because an increasing number of households require two incomes to stay afloat and more women are pursuing careers — a major win for the feminist movement. 

But that's not all. The advent of modern birth control methods led to a rupture between sex and marriage, and those so-called "shotgun" marriages of the 50's, in which couples were forced to wed due to pregnancy outside of wedlock, are now long gone. 

So what does this all mean for class in America?

These shifts in family structure have negatively impacted childhood development. 

As premarital sex became commonly accepted across classes, the unplanned birth rate skyrocketed, especially among less-educated people. At the same time, divorce rates for this group rose faster than for couples with college educations. 

This fact is a contributing factor to the increased likelihood of lower class kids living with a single parent or a chaotic family plagued by shifting partners and financial pressures, all of which have a demonstrated connection to decreased educational success. 

Meanwhile, these changing norms have had primarily positive effects for the children of highly educated parents. For example, women with higher levels of education wait to have kids until they have a firm foothold in a good job. As a result, studies have shown that many of these parents, despite their both working, still spend as much time with their kids as stay-at-home moms did 50 years ago.

> _"Children pay the cost of early childbearing and multi-partnered fertility in the form of diminished prospects for success in life."_

### 4. Parenting determines the success of children later in life. 

If you looked at a classroom full of five-year-olds, you'd probably think they're all brimming with joy and potential. But, even at this tender age, the race for success is already well under way, and privileged students are often miles ahead of their peers. That's because the fundamental foundations on which life success is built are laid early on in childhood, a time when parenting has its biggest impact. 

For instance, essential developmental steps for the human brain occur in the first 18 months of a child's life. During this time, the bases for verbal aptitude and mathematical reasoning take root. But in order to develop these skills, children require frequent interactions with attentive caregivers. 

In fact, some studies have shown that parents who are warm and nurturing raise children with stronger cognitive, social and emotional skills, like self-control and emotional stability, all of which have a profound impact on the child's future success, both in and out of school. 

However, lower class parents tend to have a harder time aiding the development of their children. That's because kids who grow up in low-income families are much more likely to be exposed to stressful events like physical abuse, neglect or seeing a family member struggle with drug abuse, alcohol or crime. 

Not just that, but low-income parents have considerably less time to speak with and read to their kids, not to mention their vocabulary may be limited due to sub-par education. This fact could explain why only 19 percent of school-age children from poor families have learned the alphabet, compared to 72 percent of their middle-class counterparts. 

So, while poorer parents struggle to prepare their children for life, well-educated and affluent parents are much more likely to learn about good parenting techniques and to employ them. But they also have the time and money necessary to give their children higher-quality out-of-school educational opportunities, from good day care to extracurricular activities.

### 5. Socially segregated neighborhoods widen the opportunity gap. 

As you can see, the promise that anybody can achieve anything, upbringing notwithstanding, is currently empty. In fact, it's not just the socio-economic status of your parents that affects your future prospects, but literally the neighborhood you are born in.

That's because neighborhoods across the country have grown increasingly socially homogenous over the past few decades, many being drawn along distinct class lines.

Why?

For one, richer families opted out of city life in favor of the space, better schools and increased safety of the suburbs. The income gap also increased. For example, say that 60 percent of the residents of a particular area are wealthy. As the cost of living changes based on their incomes, surviving in that area becomes virtually impossible, even for middle-class people. 

Also, misguided housing policies, like tight zoning regulations requiring massive minimum lot sizes for suburban properties, make homeownership in these areas impossible for society's poorer families. 

Take Port Clinton, where forty years ago the city's neighborhoods and schools boasted a mix of children from all different backgrounds. Today, the city is clearly split into wealthy neighborhoods, like those on the lakeshore, and poorer ones, like those near downtown. 

The effect?

Children in poorer neighborhoods see several disadvantages. For one, families in low-income areas have less access to good education, daycare and other public services beneficial to childhood development. But poor and poorly educated families also tend to have weaker support networks, a fact that is only exacerbated by neighborhood segregation. 

For example, studies have shown that residents of low-income neighborhoods are less likely to trust or offer assistance to their neighbors, something that hurts poor children in particular. That's because they can't avail themselves of valuable forms of support through informal mentors — a privilege willingly afforded to their middle-class peers. So while a retired teacher in a wealthy area might help a neighbor's child study for her SAT, a child in a poorer neighborhood would likely have to do so on her own.

### 6. Schools also increase the opportunity gap. 

Today, though the majority of American high school students will graduate, academic achievement across the board is anything but equal.

Why?

For one, not all high schools offer the same opportunities. In fact, when it comes to getting students ready for future success, secondary schools vary widely. Unsurprisingly, the best schools often cater to wealthy students. 

How?

Privileged schools often offer incentives to attract stellar teachers — things like motivated students, better salaries and state-of-the-art equipment. But whom you attend school with can also have a major impact on your career. 

For example, students with educated parents tend to be more excited about academic pursuits. When many such students conglomerate in a school, the result is a competitive environment that motivates students to learn more than they would at less competitive schools. This shouldn't come as a surprise; in high-achieving environments, academic success can mean higher social standing. 

On the other hand, if your peers don't care about school, as is the case for many students in underprivileged school districts, the motivation to get an A is hard to muster. In fact, in schools like these, earning a good grade or being academically engaged might actually lead to your being bullied. 

Extracurricular activities are also key to educational success, and poorer children have less access to them. For instance, things like team sports and music classes are essential for building _soft skills_ — things like self-discipline and leadership — that are major predictors of success later in life. However, to participate in such activities, students rely on their parents' financial support, whether in the form of added funding or pay-to-play services. 

As a result, many low-income students are deprived of these opportunities simply because their parents can't afford the additional expense.

### 7. American society is duty-bound to provide equal opportunities for all American kids. 

You can think of the American dream as a responsibility: you alone are in charge of succeeding and no one else can be held accountable for your failure. However, using this individualistic philosophy to justify the opportunity gap is economically shortsighted and morally reprehensible. 

As it is, countless youth are left in the dust; there are compelling economic reasons to alter this. 

For instance, a many youth between the age of 16 and 24 are neither in school nor at work. If we can't provide them with employment opportunities, this group, over the course of their lifetimes, will cost society $4.75 trillion in taxes and lost economic growth. 

In addition to economic incentives, however, society has a moral obligation to give these kids the chance to succeed. After all, to deny young people the opportunity to a decent life based on their honest labor and abilities contradicts the core values of American society. Not just that, but holding children responsible for the choices made by their parents and the conditions they were born into is cynical and unfair. 

If that's not reason enough to make a change, consider that the opportunity gap is a major threat to our democracy. Democracy is government by the people; each citizen, therefore, should have equal opportunity to participate. But being part of American democracy by voting or taking civic action requires both time and information, luxuries low-income people aren't afforded. 

Furthermore, not only are people who feel incapable of changing their lives less likely to be politically active, they often become politically alienated. Many uneducated U.S. citizens feel this way, and are therefore more likely to adopt anti-democratic ideologies that threaten our political system. 

Knowing about the major inequalities facing U.S. society, the American dream seems practically ludicrous. But these gaps in opportunity aren't a natural fact; there's much that politics and individuals can do to narrow the equality gap for children.

> _"For economic productivity and growth, our country needs as much talent as we can find, and we certainly can't afford to waste it."_

### 8. Political action can help narrow the opportunity gap. 

So the American dream is in crisis. How do we save it? Regardless of your political agenda, there's a lot that can be done. 

In fact, political action is necessary to make sure that kids are raised in nurturing families. For instance, programs that educate people about effective birth control methods could slash the number of unmarried births, as well as the instances of single-parent homes. But that won't help the many low-income parents already struggling to raise their children. So to make sure that these parents do right by their kids, it's essential to offer parent coaching and affordable high-quality day care. 

Giving minor financial assistance to poor families also increases the educational success of kids in those families, especially when aid is given during their early infancy. That means that well-aimed tax benefits for low-income families with young children and the expansion of anti-poverty programs could have a big impact. 

However, helping families provide their kids with opportunities isn't enough. It's also necessary to reduce the social segregation of our schools and neighborhoods. In fact, reversing the social segregation of schools is an inextricable part of desegregating neighborhoods, although political action in service of the latter is highly contested. 

How to desegregate schools?

Subsidizing more mixed-income housing might help, or changing the lines along which school districts are drawn. But these tactics are by no means a total solution. 

We could also try bolstering the academics of struggling schools by redistributing funding from more privileged ones that tend to also benefit from the donations of wealthy parents. This money could be used to increase the quality of education by attracting good teachers to schools that lack them. 

But in addition to bringing low-performing schools up to speed it's necessary to improve the reputation and funding of alternatives to a university-level college education, like community colleges and vocational schools. These institutions are a meaningful path to higher education for students who can't attend college.

> _"Our civic leaders will need to reach across boundaries of party and ideology if we are to offer more opportunity to all our children."_

### 9. Everyone can be involved in closing the opportunity gap. 

Social inequality is accelerating at this very moment and has already left millions of young people in its wake. It's clear that something needs to be done, but political decision-making can take years of heated debate and stalled action. 

So we've got to wait? 

No way. Political change may be crucial, but everyone can pitch in today. 

For example, serving as a mentor is a great way to aid the development of underprivileged youth. That's because, as you saw earlier, low-income families often lack the extended social networks that can help young people deal with difficult times. 

There are many ways to be a mentor. In fact, all different types of mentoring have proven effective, and religious communities in particular have the tools necessary to build more formal mentoring programs. 

But there's more you can do to fight the opportunity gap. Another way is to challenge pay-to-play policies in schools. Because extracurricular activities were designed to give kids, regardless of their background, the chance to build essential social and cognitive skills, the money that pays for them should come from schools, not families. As a parent, it's easy to challenge this system by opposing it at _your_ kid's school.

One more way everyone can help is by promoting a healthy neighborhood environment along with all its beneficial effects. It's clear that a unified neighborhood that fosters social cooperation and brings together children of varying classes has many benefits and anyone can help make such neighborhoods a reality. 

So, while political action is necessary to stop the homogenization of neighborhoods, it's up to the people living in mixed-class neighborhoods to unify, build social trust and cooperate while they still can. This could be as easy as organizing a town garden and inviting everyone in the neighborhood to join in!

### 10. Final summary 

The key message in this book:

**The opportunities of rich American children are markedly different from those of the poor, and the gap is increasing everyday. Our society is failing to give children the educational resources necessary to have a fair chance in life and in the process betraying the American dream. While the causes of this crisis are complex and many, everyone can lend a hand.**

**Suggested** **further** **reading:** ** _The Spirit Level_** **by Richard Wilkinson and Kate Pickett**

This book provides a detailed explanation of how inequality is responsible for many of our present-day problems, including violence and mental illness. It provides detailed explanations and studies to support this and shows how inequality not only hurts the poor but everybody in a society.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Robert D. Putnam

Robert D. Putnam teaches at Harvard University, where he is the Malkin Professor of Public Policy. A renowned political and social scientist, he has written over a dozen books, including the bestselling _Bowling Alone: The Collapse and Revival of American Community._

