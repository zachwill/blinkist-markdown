---
id: 564a07fc66343100078d0000
slug: out-of-character-en
published_date: 2015-11-19T00:00:00.000+00:00
author: David DeSteno and Piercarlo Valdesolo
title: Out of Character
subtitle: Surprising Truths about the Liar, Cheat, Sinner (and Saint) Lurking in All of Us
main_color: B29A6C
text_color: 806E4D
---

# Out of Character

_Surprising Truths about the Liar, Cheat, Sinner (and Saint) Lurking in All of Us_

**David DeSteno and Piercarlo Valdesolo**

_Out of Character (_ 2011), introduces a more flexible idea about character that goes beyond the classic dichotomy of the saint and the sinner. These blinks use psychological experiments to demonstrate how many of the traits we consider fixed are prone to influence by outside events, often in surprising ways.

---
### 1. What’s in it for me? Learn why there is a cheater in every saint. 

If there's one thing we humans like it's putting things into neat little boxes. This is especially true with the way we look at people around us. We observe how they behave and conclude that one person's a cheater, another's a drunk, and another's a gambler. 

However, as these blinks will show you, the idea of anyone having a fixed character in this way is deeply flawed. Under the right — or wrong — circumstances we can all change and become a seemingly different person. This shouldn't surprise us. A wealth of psychological experiments have shown that our perceptions and behaviors are influenced in sometimes surprising ways by a number of factors.

In these blinks, you'll learn

  * what ants and grasshoppers have to do with character;

  * why women should trust their noses more; and

  * how good posture can work miracles for your social status.

### 2. Character is the product of dueling forces in every moment. 

Have you ever wondered about the origins of the word _character_? Actually it comes from an ancient Greek word that referred to the indelible marks pressed into coins to differentiate them. However, the nature of character is anything but fixed and eternal. So when people act "out of character" they are often just expressing a part of their true nature. 

In fact, character is really quite flexible and it's common for people to behave differently at different times since each of us contains both vice and virtue. This divide is classically depicted as a battle of opposing ideas within each of our minds: the proverbial angel and devil on our shoulders. These forces are seen as guiding our actions and early on each person is swayed more by one than the other. 

However, the duality of angel and devil is problematic for three reasons:

First, it's too simplistic to think that each of us is guided by only a good and a bad force. In reality, the powers that influence our behaviors are much more nuanced. 

Second, it's not possible to be certain about which side to trust.

And, third, external events greatly affect our thoughts and actions, a fact that we'll see explained in detail later on. 

So what's a better metaphor?

That of the ant and the grasshopper, drawn from a fable by the ancient Greek storyteller Aesop, which goes like this:

The ant, always thinking long-term, is busy preparing for winter while the grasshopper enjoys himself, singing, playing and not worrying about winter until it arrives. 

The two mindsets symbolized in this story better describe our internal struggle because they depict the battle between immediate rewards like going to a party and long-term planning like spending the night studying. Rationally speaking, it's the future gains that bear the greater potential. For instance, if you save money it will grow but you also have to live long enough to enjoy it. Therefore, both sides are important and both are contained within all of us.

### 3. Hypocrisy and morality are closer to each other than you might expect. 

From the time we're kids we're told that morality is good and hypocrisy is bad. But in reality this division is just too simple!

That's because being hypocritical isn't so much violating your morals as it is shifting them to better suit your needs. People do it all the time. A politician might speak out against prostitution only to be caught soliciting a prostitute himself. When the media grills him on the subject he can't seem to see a problem with his actions because his mind has convinced him that he did nothing wrong.

But this idea has also been proved in a psychological experiment:

Two groups of participants were given a test that consisted of a short, fun task and a long, boring one. One group got to choose which task they did themselves and which task the others had to do. They were also offered the option of flipping a coin to decide. As you can imagine, most opted to make the decision themselves and chose the fun activity. 

Then, when they were asked to rate their behavior on a scale from "not at all fair" to "very fair" they put themselves in the middle. However, when they were asked to evaluate another person who did the same thing they described their actions as "very unfair". 

Hypocrisy is complicated, but so is morality. That's because when it comes to making moral decisions no strategy is perfect and thinking logically can be just as misleading as following your intuition. 

For example, another experiment asked people if they would push a big man off a bridge to prevent a streetcar from hitting three others. One group watched Saturday Night Live beforehand and another a serious documentary. 

The effect?

The former group was more likely to say yes and it was due to their changed emotional state. So, our moral judgment is prone to manipulation, but it's also flexible in even more surprising ways.

### 4. Even the most devoted partner can be overcome by lust for someone else. 

Have you found the person you want to spend the rest of your life with? Well, even if you have, it won't necessarily prevent you from feeling and acting on your lust. 

That's because lust is easily defined by most people, but love is another story. In fact, it eludes definition. For instance, Plato used to say that humans once had two heads and four legs and arms, and at some point, were split in two by the gods. As a result, we are each in search of our missing half, but how can we know when we find it?

On the other hand, the idea of lust, of pure physical attraction, is pretty simple. It's also the starting point of every relationship. However, interestingly enough, the traits we consider attractive have little to do with us personally. 

In fact, they are simply bodily features that bear an evolutionary association to the ability to pass on healthy genes. For example, a symmetrical body indicates reproductive success. Therefore, our attraction to others is based on survival instincts.

And when these impulses take over, resisting can be difficult. For example, one experiment had women smell the unwashed T-shirts of men. The women who were ovulating were more attracted to men with symmetrical features and they hadn't even seen them!

However, when women are not ovulating they're more drawn to physical cues that signal the capacity for a stable relationship, like non-verbal responses or nods. So, it's clear that the battle between lust and love is a never-ending one propelled by hormones. 

But jealousy can also change our behavior in serious ways and no one is exempt from this fact. Experiments have shown that when made jealous people will opt for the chance to punish their competitors by giving them a painfully spicy sauce to drink.

### 5. Pride is not always bad, but can be if it’s not earned. 

The bible lists pride as one of the seven deadly sins, but this impulse isn't actually such a bad thing. In fact, pride can help you work harder and even improve your social status. 

That's because if you take pride in your work you have more of a motivation to excel at it. Plus, prideful people are more capable of overcoming obstacles and mastering difficult scenarios. For instance, in studies on test-taking, it has been shown that knowing you scored well isn't what makes you work harder: it's receiving praise for scoring well that does. 

When those around you care about your work they can boost your pride. But we can also build pride by taking a look at ourselves and being our own audience.

For example, have you ever heard someone talk about being pumped up with pride? There's actually some truth to this idea as experiments have shown that people who stand erectly with an expanded posture are also the most prideful. In fact, people intuitively connect this posture to status. 

Another experiment showed that, in a group setting, people who received positive feedback were much more vocal: a position offered to them by others, but also claimed for themselves. 

But pride does have a downside. It's called _hubris_, but even this sore spot isn't _all_ bad. In fact, the reason overly prideful people can sometimes be so detested is because they claim pride without having earned it. This annoying state is called hubris but sometimes it can serve as a necessary protection. 

For instance, after the financial crisis of 2007, a lot of unemployed stock brokers continued making the commute to New York City to be what became known as "Starbucks executives", using Starbucks and its free WiFi as their office space. In the case of these traders, hubris allowed them to maintain both their social status and self-worth.

### 6. Compassionate or cruel? It all has to do with how we perceive others. 

Did you know that the divide between cruelty and compassion is actually quite narrow?

Just consider the Ivory Coast where, from 2002 to 2007, a civil war was fought between the north and the south. The fighting was non-stop, except a period of respite during which the warring sides joined together in support of the nation's soccer team as they competed in the World Cup. After the tournament the fighting resumed.

So why do we treat some people cruelly and others with compassion? 

It has to do with how we perceive a person's similarities to ourselves. As we can draw from the example of the Ivory Coast, when the country united around its soccer team, both sides demonstrated a desire to return to being an "us", instead of an "us" versus "them".

In fact, we constantly judge the people around us based simply on physical indicators. On top of that, we're much more likely to help someone we think of as being like ourselves. For instance, one experiment showed that undecided voters would go with a presidential candidate whose picture had been combined with their own to make a composite image. 

And when we see others as different from ourselves?

It can mean terrible acts of violence and sadistic behavior. That's because we have a strong inner voice that tells us to take care of ourselves. It's this voice that tells us, for instance, to not give our spare change to a beggar and it works by convincing us that they're not like us. 

This goes so far that we even dehumanize other people to make it easier to walk past their suffering. In fact, this was exactly what the Nazis were doing when they construed Jews as vermin — depicting them as objects to use or throw away. 

The good news is that there's a way to avoid this tendency: 

Just surround yourself with as many different types of people as you can. You'll be amazed at how the "us" perception grows.

### 7. Feeling gratitude affects our behavior and our trust. 

Have you ever noticed how the smallest gesture of kindness from another person can totally transform the way you feel and act? Feeling gratitude makes people more willing to offer assistance to others. 

In one experiment, participants were first given a boring computer-based test after which the screens would go blank, seemingly erasing the data. Then, an anonymous helper would step in to recover the information. Afterward, the participants automatically expressed gratitude even though they didn't know who specifically had helped them and, later in the day, when they were asked for help by a stranger, they gladly lent a hand. 

Fairness is more flexible than you scratch my back and I'll scratch yours. However, when the person offering help was personalized by being referred to by name, the participants felt gratitude solely for him or her.

Experiments have also shown that gratitude builds trust. For instance, one experiment divided participants into pairs, giving each subject four tokens, each worth $1. If they gave a token to another participant, its value would increase to $2 for the new owner. The most advantageous strategy for everyone would be to exchange all their tokens and turn their $4 into $8. But how could they know that everyone would reciprocate?

The participants who had been prepared to feel grateful by being bailed out of a computer dilemma gave three or four of their coins away on average while the others who hadn't only gave two. 

But there are two sides to the coin: we're also more likely to do wrong if we see others doing so.

Experiments that gave participants money for every math equation they solved shed light on how cheating occurs. In these tests, if one participant quickly said they'd solved all the problems, implying that they'd cheated, cheating became more common across the group. Because when cheating appears "normal" our inner grasshopper rears its head and we take the easy route.

### 8. Taking chances has little to do with character and everything to do with perceived risks and rewards. 

Do you play it safe and feel better saving your money than investing? Or are you more willing to take risks? Well, whichever tendency you lean towards, both exist within us all. 

In fact, the difference between risk-takers and safe-players isn't character, but actually how they view risk and reward, a perception that's constantly shifting because of external factors, especially those that influence our emotions.

For instance, if there's a lot of media coverage around an airplane crash people tend to think of such crashes as being more likely. This isn't a matter of the event's real probability, but a product of feeling like it's possible. 

In addition, the more clear and immediate the reward, the more likely people are to take risks. In one experiment, cookies were given as a reward. Participants took greater risks when the cookies were removed from the oven before their eyes, giving them a strong whiff of the freshly baked treats. 

Other factors that play a major role in risk-taking are age and biology. For instance, one group that tends to be less risk-averse is teenage boys. That's because they haven't yet developed the ability to see the consequences their actions might hold. 

Finally, changes to our state of mind affect how we view risk. Just consider how anger can cloud your judgment when weighing different potential outcomes. One experiment found that people who were upset after reading about an anti-American protest were much more likely to overestimate the potential of something aggravating occurring, like getting stuck in traffic or tricked by a salesperson. 

On the other hand, feeling happiness causes people to overestimate the chances of a _positive_ event taking place. Experiments have shown that if people are told more about the risks of breast cancer and less about the advantages of getting a mammogram, they will be more likely to see a doctor. That's because different perceptions — either viewing mammograms as helpful or being scared about getting cancer — change people's behavior.

### 9. Prejudices and stereotypes might affect your behavior more than you think. 

Are you one of those people who think they don't hold prejudices?

Well think again, because people hold prejudicial beliefs and stereotypes for an evolutionary reason: they help us avoid danger. However, in the modern world such thoughts can make us behave in abhorrent ways and people easily succumb to them. 

For instance, an experiment conducted in a school showed how easy it is to embed prejudice and stereotypes in people:

A teacher simply informed her class that brown-eyed students were superior and continued to repeat the statement again and again. The result was that the brown-eyed children began bullying those with blue eyes. 

Then, the following day the children were told that the opposite was true: that blue-eyed kids were better than their brown-eyed peers. Instead of stopping entirely, the bullying just changed direction. 

While this test was conducted with children, subsequent experiments have shown that the same is true of adults. Even though adults are not as easily influenced as kids, it was easy to embed prejudices in them when they were mad. 

That's because we hold subconscious biases that can seriously affect our actions, especially when we're angry or stressed out. Take Mel Gibson, who's known for exploding with anger but also as a well-tempered, prejudice-free guy. Well, both are true because his prejudices become clear when he's mad or under stress. 

And the same is true for most people. For instance, one experiment asked participants to watch scenes from a street on a TV with two buttons on its front labeled _Shoot_ and _Don't shoot_. At times a man would cross the scene, carrying something in his hands. The participants were tasked with pressing Shoot as quickly as possible when they thought the man was holding a gun and Don't shoot if they thought he had something else. 

What happened?

The all-white participants were much more likely to press Shoot when the passer-by was an African-American.

### 10. Final summary 

The key message in this book:

**Character is not fixed. It is highly flexible and prone to changes due to shifting contexts. That means that no one is simply a saint or sinner — everyone is a combination of complex moral forces.**

Actionable advice:

**Accepting that character is flexible will let you influence it.**

When you begin thinking about character as a set of psychological mechanisms that compete to control your behavior, you can start controlling that competition by noticing the context. That's because you can't always trust your intuition and it's important to be aware of subtle changes such as anger that can mislead you.

**Suggested** **further** **reading:** ** _The Truth About Trust_** **by David DeSteno**

_The Truth About Trust_ not only explores what trust exactly means, but also how it impacts almost every single aspect of our everyday lives. The author's own extensive research and progressive experiments from the fields of psychology, economics and biology reveal the surprising ways in which trust deeply matters.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### David DeSteno and Piercarlo Valdesolo

David DeSteno is an associate professor of psychology at Northeastern University and director of the school's Social Emotions Lab. His work has been published in _The New York Times_ and _Scientific American_.

Piercarlo Valdesolo is an assistant professor of psychology at Claremont McKenna College. His work has appeared in _The New York Times_, _Newsweek_ and many other notable publications.

