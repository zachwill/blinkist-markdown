---
id: 53babfc53136620007000000
slug: out-of-control-en
published_date: 2014-07-08T00:00:00.000+00:00
author: Kevin Kelly
title: Out of Control
subtitle: The New Biology of Machines, Social Systems and the Economic World
main_color: F9C557
text_color: 7A612B
---

# Out of Control

_The New Biology of Machines, Social Systems and the Economic World_

**Kevin Kelly**

Though written from the perspective of 1994, these blinks paint a startlingly current and still futuristic image of how technological developments like the internet and artificial intelligence could affect society and humanity.

---
### 1. What’s in it for me? Take a trip in a time machine and see what the future of technology used to look like. 

Imagine you're in the year 1994, and have just heard about this newfangled contraption called "the Internet." Would you have foreseen the amazing impact it has had on humanity in just two short decades? Unlikely. But don't be astonished if these blinks leave you believing that Kevin Kelly is some kind of medium with psychic powers, because he managed to predict so many developments with astonishing accuracy.

In these blinks, you'll find the answers to the following questions:

  * why this book was required reading for the entire cast of _The_ _Matrix_, including Keanu Reeves,

  * why the creation of artificial intelligence _should_ in fact scare us,

  * why credit cards should have disappeared in the last 20 years, and

  * how a computer program could prove that Darwin didn't get the whole story when he described evolution through natural selection.

### 2. The future of technology will see the merging of natural and artificial characteristics. 

Think back to the year 1994 — if you were alive then. Remember how back then the internet had not yet really caught on, and there were no social networks, tablets or camera phones?

Well, despite this technological "primitivity" scientists and technologists back then were already asking the same questions raised today about the future of technology.

One such key question is: How can we drive technological progress by learning lessons from nature?

As an example, consider artificial intelligence. At the moment, computers and machines can only perform the tasks they are programmed to do: they only exhibit _clockwork_ _logic._ For instance, if you program a machine to build a car door, it can repeat this task over and over again, but it can't do anything else without being reprogrammed.

But in nature, we find far more complex "technology." Take the human brain, for example. It can think, learn and evolve thanks to the experiences it gathers. This is known as _vivid_ _logic_, and if we wish to improve on artificial intelligence, we need to emulate this vivid logic in machines too.

Yet learning from nature is just one lane in a two-way street: we can also add elements of technology to nature. In other words, while we can learn from nature to build machines that are capable of learning, we can also enhance natural systems with the help of technology.

One example of this is _bioengineering_ : breeding and modifying plants and animals in specific ways designed to benefit mankind — for example, by selectively breeding cows so that their offspring produce more milk.

Going even further, we can also see the convergence of nature and technology in bionic _vivisystems_ : networks of individuals and machines in which the network itself is a living entity. One natural example of a vivisystem in nature is a beehive, which is capable of learning, adapting and surviving, but is not an individual organism in itself.

> _"A law of God: When something works, don't mess with it; build on top of it."_

### 3. To take advantage of natural principles in technology, humanity must relinquish control. 

One common trait in current technologies and machines is that they all demand intense control and supervision by humans. But as we begin to merge the artificial and the biological, we will come to find that we humans must begin to relinquish our control.

Why?

As nature and technology merge, nature will be the dominant partner. This is because firstly, nature is the foundation of all life on earth, so inevitably it is technology that must adapt to nature, not the other way around.

What's more, natural processes tend to be more efficient than artificial ones. For example, nature is the ultimate recycler: the nutrients stored in dead plants and animals are reused by the ecosystem far more efficiently than by any artificial system.

In order to take advantage of similarly efficient natural processes, we must be willing to relinquish our control. Instead of acting as iron-fisted managers, we must instead consider ourselves shepherds who do better by guiding the flock as a whole in a general direction rather than trying to control every single individual sheep.

Such an approach would also allow artificial systems to develop according to three natural principles: _autonomy,_ _creativity_ and _adaptability_.

  * Autonomy: machines react independently to situations and make their own decisions; 

  * Creativity: machines invent better ways to perform tasks and even think of new areas in which to apply their skills;

  * Adaptability: machines learn and evolve as the circumstances and the environment change.

One of humanity's main challenges for the 21st century will be to relinquish control over artificial systems so they can develop according to these natural principles.

> _"The story of automation is the story of a one-way shift from human control to automatic control."_

### 4. We can leverage the flexibility of bee swarms in our own technological networks. 

Have you ever seen a swarm of bees? If you have, you were no doubt amazed at how it moved uniformly, as if it were a single organism and of a single mind.

In fact, we humans can learn a lot from _swarm_ _systems_ : they already exhibit many of characteristics we hope to attain by merging nature and technology.

For example, swarm systems lack a centralized hierarchical command. Instead, all the subunits in the system are autonomous and exist on the same level in the command hierarchy. In a bee swarm, every bee is autonomous, and there is no single "leader bee" that makes decisions. Instead, all decisions are made collectively.

What's more, swarm systems are highly adaptable and resilient when subunits are lost. Even if several individual bees die, this only affects part of the swarm, while the whole survives.

So how can we apply these beneficial traits to technology?

By building _networks_. A network is like an artificial version of a bee swarm; it consists of various subunits known as _nodes,_ each of which is connected to every other node in the network.

Let's say we have a network like this for communicating information from one node to another. The network would be highly stable: even if individual nodes malfunctioned, the information could still find alternative paths to travel. This is why the internet, for example, cannot be totally knocked out — only individual sites or parts of the net can be taken down.

Growing a network like this is easy, as more nodes can simply be added without any fundamental change to the network itself.

What's more, each added node exponentially increases the number of connections for information to travel along, and thereby the robustness of the network. For example, try drawing a network comprising three nodes. As you can see, it has three connections. Now try adding one more node. You should now suddenly have not four, but six connections!

### 5. Network thinking can transform the economy into a more ecological, consumer-friendly one. 

Imagine a world where there are no big corporations or organizations, but instead everyone runs their own one-person company. Sound crazy? Well, if network thinking were applied to the economy, that's what the result would look like: a _network_ _economy._

Today, if you want to buy a chair, typically a single company would control every stage of the production and delivery process, from designing the chair to shipping it to your home.

In a network economy though, each individual stage would be handled by a specific node. For example, you (node 1) would identify a general production manager (node 2) who would contact a designer (node 3). The designer would in turn send the designs to a carpenter (node 4) who would build the chair and contact a logistics expert (node 5) who would ship the finished product to you.

This process might well be unique: once the chair has been shipped, this exact same combination of nodes might never be used again, and all the later chairs created will probably take a different path through the network.

So what are the benefits of this kind of network economy over a traditional one?

First, a network economy would be more ecological. This is because goods are only produced when a consumer in the network demands it, so there is no excess production. What's more, network economies encourage recycling, because when one person is done with something, they can pass it forward in the network for someone else to use or break down into raw materials.

Another benefit is that consumers have more power. This is because producers must respond to specific consumer demands: "I want a blue, wooden chair with five legs." Also, consumers themselves can be part of the production network, as in the case of crowd-sourced software such as the web browser Firefox.

### 6. In a networked economy, privacy requires that we encrypt information. 

As you have seen, network thinking can provide many benefits. But networks also raise some concerns for their members. Foremost is _privacy_.

Concern for privacy arises from the fact that being part of a network would require individuals to share a lot of information with others so that the network can function efficiently. But of course no individual would want all their information to be released to just anybody, so it is important that networks have a way to protect the privacy of individual nodes.

This would require a means of deleting information about yourself from the network. But who should do this? One might think that this should be left to the private sector to avoid centralized government control, but this would also be a mistake — if private companies had the power to erase data from the network, there would be nothing to stop them from selling this service to criminals.

Hence encryption is a better tool than selective deletion — rendering information into a form that only authorized people can decode.

One example of an advanced application of encryption is _electronic_ _cash_. You may think that this is what credit cards are, but true electronic cash is anonymous, like regular cash, only it can be transferred to merchants as effortlessly as payments from a credit card. Merchants will no longer be able to see your purchasing history and personal information every time you use credit cards. For electronic cash to work, it requires strong encryption that makes it impossible for the merchant to decipher who exactly was the payer in any given transaction, unless the payer authorizes it.

### 7. Stable ecosystems cannot be designed, they can only emerge from natural randomness. 

In the 1930s, a famous ecologist called Aldo Leopold set his sights on a very ambitious task: he wanted to create his own ecosystem on an old farm purchased by the University of Wisconsin. Specifically, he introduced certain species into particular climatological and environmental conditions in order to create a prairie ecosystem similar to the large grasslands of the United States.

Yet despite having the right climate and introducing the right plant and animal species to the farm, Leopold found that a true prairie ecosystem didn't emerge: non-prairie plants and animals kept thriving.

It turned out he had forgotten one crucial element of natural prairies: fire. In nature, wildfires periodically ravage prairies and thereby regulate them.

This example demonstrates just how difficult it is to artificially design ecosystems. No matter how clever and methodical scientists try to be, natural ecosystems comprise such a complex web of factors that it's impossible to recreate them accurately. Indeed, scientists have always failed at their efforts to do so.

So how do stable ecosystems emerge if not by design to a clear goal? They simply develop over time through an evolutionary process that has a degree of _randomness_ and uncertainty to it.

Herein lies a lesson for the biotechnology of the future: complex machines must also arise from a similarly random process, where the end goal is not defined but rather emerges on its own.

But for this to work, humanity must relinquish its desire to control and deliberately design biotechnology. Just as Leopold had to give nature a big say — e.g., with wildfires — in order to get his prairie ecosystem to work, so must we give chaos and randomness a big say in our biotechnology.

### 8. Artificial intelligence can emerge from artificial evolution. 

As you know, life has evolved on earth as dictated by Darwin's natural selection and certain conditions like the availability of oxygen, water and sunlight. This process has resulted in our human intelligence.

But in fact, if you think about it, if we were to design an "evolutionary" computer program with certain rules and conditions governing it like those applied in the evolution of life, artificial intelligence could well evolve from it.

So how can such a program be designed?

The way the human brain works is an important inspiration: it's a huge decentralized network made up of millions and millions of interconnected neurons, like nodes in an information network. Contrary to what our intuition says, we have no actual control over the way our brain develops and what kind of attitudes and character traits emerge in an individual.

Thus, if we were to develop an artificial version of such a network, would it not work in the same way as a human brain? It would learn and develop just like a human brain, and we would have no idea whether it would become good or evil.

More importantly though, once we attain this level where an artificial intelligence can be self-sustaining and self-improving, we will be very close to artificial evolution. In a sense, we will be God-like, though with one important distinction: we will have to share our universe with our creation, and the unintended consequences are unknown.

> _"Give up control, and we'll artificially evolve new worlds and undreamed-of richness. Let go, and it will blossom."_

### 9. By playing with artificial evolution, we can learn a lot about our own. 

The artificial evolution explained in the previous blink is a tantalizing prospect not only because it could result in artificial intelligence, but also because it could teach us a lot about how our own evolution happened.

For example, when we define the right parameters for artificial evolution in a computer program, we can watch and study how natural selection works. Even better, it is also conceivable that we could alter the conditions of the program so that the evolution would be driven by a mechanism _other_ than natural selection.

After all, it was only conditions on earth that made natural selection the driving evolutionary force. In the case of artificial evolution, we can set conditions which may result in an entirely different evolutionary force emerging!

The question remains, though, what higher power originally set the conditions on earth that initiated our own evolution? Whatever it was, we may as well call this higher power God.

This idea is known as _postdarwinism_.

Another way artificial evolution can teach us about natural evolution is by the fact that some aspects are shared by both processes.

For example, we might observe that contrary to what Darwinists believe, genetic mutations were not random, but rather came about as responses to environmental signals.

Or we might see that evolutionary developments converge on certain shared standards that last for a long time, in that same way that the auto industry has settled on the standard four-wheeled model of the automobile. Perhaps life has also landed on such norms through a process which we can observe first-hand in our artificial evolution.

### 10. We can make predictions even about apparent chaos, but only for the short term. 

What do the stock market and an unknotted balloon zipping around a room erratically have in common?

They both seem at first glance completely chaotic processes, but are on closer inspection somewhat predictable.

To a novice investor, stock price curves seem to bounce around wildly. But as she learns about the economy, she'll find many of the movements predictable.

Similarly, if you unknot a balloon and then try to catch it as it flies around the room, you'll at first find it a near-impossible task. But soon you'll find yourself improving as you learn to anticipate its movements.

It seems that in both cases there is some order in the apparent chaos after all.

But in neither case do we actually understand the process behind the chaos. Instead, we only formulate simplified rules of thumb about what happens next. This so-called _positive_ _myopia_ is not totally accurate, but it helps us make short-term predictions about chaotic processes. In the long-term though, it becomes outdated.

For example, in the stock market, an investor may observe that the price of oil has gone up consistently for many years, and then starts making investments based on a rule of thumb that this trend will continue. She may well reap profits for a short time, but sooner or later the price of oil will fall and she will lose money. The rule of thumb will have become obsolete.

In the case of biotechnology, like the swarm systems described earlier, it is clear that they can develop in wholly unpredictable ways. And even though we can use our positive myopia to make short-term predictions, in the long-term we have no idea what will happen.

In fact, historically scientists have been no better at predicting the future in the long-term than if they made random guesses.

Thus we must simply accept that we can only predict the immediate future. Like it or not, our control does not extend further than that.

### 11. Final Summary 

The key message in this book:

**As** **technology** **advances,** **the** **natural** **and** **the** **artificial** **will** **intermingle** **and** **merge** **into** **networks** **and** **systems** **that** **will** **greatly** **affect** **the** **fate** **of** **society** **and** **humanity.** **It** **is** **crucial** **that** **humans** **learn** **to** **relinquish** **control** **in** **these** **developments.**
---

### Kevin Kelly

Kevin Kelly is the founding executive editor of _Wired_ magazine, a leading source of analysis on the societal effects of cutting edge technological advances.

