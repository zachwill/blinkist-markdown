---
id: 5746d4a8b49ff200039c4db9
slug: outsider-in-the-white-house-en
published_date: 2016-06-02T00:00:00.000+00:00
author: Bernie Sanders with Huck Gutman
title: Outsider in the White House
subtitle: None
main_color: 2893C6
text_color: 1E6E94
---

# Outsider in the White House

_None_

**Bernie Sanders with Huck Gutman**

_Outsider in the White House_ (2015) tells the story of Bernie Sanders, the presidential candidate and US senator. From marching for civil rights in the 1960s to campaigning against big money in politics as a 2016 presidential candidate, Sanders has always been at the forefront of US left-wing politics. First published in 1997 as _Outsider in the House_, this updated version of Bernie Sanders's autobiography traces his lifelong fight for social justice and economic fairness.

---
### 1. What’s in it for me? Get to know one of the United States’ most influential political outsiders. 

Bernard "Bernie" Sanders is seen by many as the ultimate outsider. In his bid for the 2016 Democratic Party presidential nomination, he's challenged many establishment figures in Wall Street, Congress and the Democratic Party itself.

This has been the case throughout his career. As the mayor of Burlington, Vermont; as a member of the US House of Representatives and, later, of the Senate; and as a candidate in the Democratic presidential primaries — he's always stood behind what he believes, whether it pleased the establishment or not.

These blinks provide an intimate look into the life of Bernie Sanders, a politician who is not afraid of political opposition, and who always tries to find a way to transform his ideas into real political action.

In these blinks, you'll find out

  * how Sanders's upbringing formed his political ideas;

  * that, despite opposition, Sanders actualized his political vision in Vermont; and

  * how Sanders has influenced lawmaking in the US Congress.

### 2. The financial hardships of Sanders’s upbringing taught him how greatly economics can affect people’s lives. 

Bernard Sanders was born in 1941, in the Brooklyn borough of New York City. During his childhood, money was tight; his family managed to stay above the poverty line, but Sanders's parents often fought about the best uses for the little money they had.

These conditions, however, taught Sanders the value of money.

From his mother, Dorothy, for example, he learned the importance of being thrifty. One day, instead of shopping at an inexpensive supermarket, Sanders bought groceries from a smaller, local store. When he got home, his mother scolded him, stressing the importance of frugality.

Sanders's father, Elias, had lived through the Great Depression, so he knew what it was like to get by in hard economic times.

Originally from Poland, Elias worked hard as a paint salesman to provide for his family, and his strong work ethic sometimes led to conflicts between father and son.

For instance, when Sanders first applied to college, his father initially opposed the idea. Elias thought it wiser for Sanders to start making money rather than continuing his education.

It was Sanders's older brother, Larry, who helped turn their parents' common-sense economics into concrete political ideas.

At Brooklyn College, Larry was a chairman in the Democratic Party's liberal-minded Young Democrats group, and every once in a while he would bring young Sanders along with him to meetings. During this time, Larry also introduced his brother to progressive political literature and newspapers.

So, by the time Sanders entered Brooklyn College, in 1959, he was well informed and keenly interested in politics, economics and history.

These interests and ambitions continued to blossom a year later when he transferred to the University of Chicago to study political science. Sanders immersed himself in literature in the school's basement library, spending countless hours reading everything he could get his hands on.

### 3. Sanders became politically active in his late 20s. 

When he didn't have his nose buried in a book, Sanders participated in a number of political groups at the University of Chicago, including the Young People's Socialist League and the Congress of Racial Equality.

During this time he continued to grow more socially conscious, attending civil rights protests against the city's segregated housing and schools.

Then, in 1968, Sanders moved to Vermont, and, at the age of 27, his career as a politician began.

Sanders's involvement with party politics began after an old college friend told him about a small left-wing party called Liberty Union.

Sanders liked the Liberty Union platform, which centered on ending the Vietnam War and campaigning for social and economic justice.

In 1971, Sanders attended his first Liberty Union meeting in Burlington, Vermont. The meeting went so well that that he volunteered to run for a seat in the US Senate on the party's ticket.

Although he lost the election, coming in third place with just two percent of the vote, the experience had a profound effect on Sanders. He cites the debates with Republican and Democratic candidates during the campaign as important, formative experiences.

During the debates, he received an overwhelmingly positive reaction from audiences, proving to him that his left-wing views and policies had mainstream appeal. People felt as he did; they just needed someone with the guts to stand up and vocalize these beliefs.

So, despite the defeat, Sanders was ready to try again: Less than six months later, Sanders ran for Governor of Vermont on the Liberty Union ticket.

This time, he only received one percent of the vote, but the campaign taught him another important lesson: you don't have to win elections to implement change.

His Democratic opponent, Thomas Salmon, won the election and went on to implement two of the more popular policies Sanders had campaigned for: property tax reform and a state dental insurance program for children from lower-class families.

As we'll see in the next blink, these early political experiences prepared Sanders for his mayoral election campaign.

> _"Although I was the candidate of a minor party, people were listening to what I had to say and they often supported my positions."_

### 4. It was as mayor of Burlington that Sanders first demonstrated his political potential. 

In 1976, Sanders ran for governor once again, and this time, he took six percent of the statewide vote along with a significant 12 percent of the Burlington vote.

Five years later, Sanders decided to run for mayor of Burlington, Vermont.

Thanks to the support of many activist groups, as well as backing from the city's police union, he won the election (by 14 votes out of 10,000 cast), and became the only socialist mayor in the country. The city council, which was primarily made up of Republicans and Democrats who were willing to do anything to oppose him, gave him a good deal of trouble.

Nevertheless, Sanders was able to instate changes as mayor, because, rather than fighting them, Sanders focused on achieving things without the council.

For instance, after realizing that the city was throwing away money on costly insurance contracts for local businesses with political affiliations, Sanders decided to open the contracts up to competitive bidding, saving thousands of taxpayer dollars.

With this extra money in hand, he funded projects that benefitted the community and brought people together, such as popular summer concerts and sports programs for disenfranchised youth.

Eventually, Sanders was able to consolidate this power, allowing him to launch a political revolution in Burlington.

In the 1981-2 city council election, Sanders and his progressive allies campaigned hard and won three more seats, increasing their minority to five out of 13. This finally gave them the ability to veto bills from their Republican and Democrat rivals.

However, things were more difficult when it came to Vermont's state legislature. With their opposition to his progressive tax reforms, Sanders was forced to seek alternative methods to generate revenue.

But here he found ways to succeed as well: He implemented a one-percent room-and-meal tax on the city's restaurants and successfully made private hospitals and universities pay more for public emergency services.

### 5. As a Representative in the US Congress, Sanders proved that independents could get things done. 

By 1988, Sanders had served four terms as Mayor of Burlington and decided it was time to take his progressive message to Washington, DC.

That year he ran for Vermont's one and only seat in the House of Representatives, but he came up short — 2nd place, with 38 percent of the vote.

Undeterred, he ran again two years later, this time winning the election with 56 percent of the vote and becoming the first politically independent congressman in over four decades.

Once there, Sanders helped start the Congressional Progressive Caucus (CPC) in an effort to bring together a group of congresspeople with similar legislative objectives.

Others were already pursuing a progressive agenda, such as the Congressional Black Caucus, but Sanders knew that an effectively progressive caucus couldn't be limited to just one ethnic group. The CPC, which Sanders formed with a number of left-leaning Democrats, had 52 members by 1994 and was on its way to becoming one of the largest caucuses in the Democratic Party.

They rallied against the Republican Contract with America, which aimed to defund social welfare and gut overall spending.

But Sanders also showed that it was possible for progressives to find shared ground with conservatives.

As the only independent member of the House, Sanders remained unfettered by party politics. This allowed him to work with both Democrats and Republicans and bring them together to form otherwise unlikely partnerships.

This is how Sanders was able to build a cross-party coalition to fight against George W. Bush's PATRIOT Act.

Sanders, together with the House Democrats and dozens of Republicans, added an amendment in a proposed bill aimed at removing the government's ability to spy on people's library records.

This challenge to the president's authority was so serious that Bush threatened to veto the entire bill. And although Bush eventually got his way, Sanders continued to build cross-party coalitions on topics such as civil liberties and privacy.

### 6. Sanders continued to fight against establishment politics after being elected to the US Senate. 

In 2006, after eight terms in the House of Representatives, Sanders was ready to take another shot at the Senate. He caused quite a stir that year when he contested a historically Republican seat and managed to win it with over two-thirds of the vote.

As a senator, Sanders repeatedly voted against both Republicans and Democrats by opposing policies that favored what he calls the "billionaire class."

We can see this in the events that followed the 2008 Wall Street crisis. Both parties supported a rescue package that would, as Sanders put it, award billions of US taxpayers' dollars to the very bankers and financiers that caused disaster in the first place.

Instead, Sanders insisted that the people who created the mess should pay the price and use their own billions to bail themselves out, or else go bust.

And when Republicans and Democrats joined forces to extend the Bush-era tax cuts, Sanders made a widely-covered 8-hour Senate filibuster speech about why it was wrong to continue offering tax breaks to the richest one percent of America.

However, Senator Sanders wasn't only a force of opposition, he could also be a strong voice for reform.

On health care, for example: Sanders achieved major victories while campaigning for a universal, single-payer health care system, similar to what exists in other Western nations.

During the passage of the Affordable Care Act (ACA), Sanders managed to secure $12.5 billion to fund community health centers after lengthy negotiations with allies in the White House and Senate.

Such victories often follow a pattern: Sanders will promise to vote on a bill, such as the ACA, as long as he can include an amendment that he believes will help forward his progressive agenda.

We can see this again in the Dodd-Frank Wall Street Reform and Consumer Protection Act: Sanders believed the bill wasn't strict enough, so he promised a favorable vote as long as it included an amendment allowing the Federal Reserve to receive its first-ever complete audit.

> _"[Bernie Sanders] is passionate, and he is not afraid to go it alone, but nor is he a purist who enjoys being right"_ \- John Nichols

### 7. Sanders’s presidential campaign is the start of a revolution aimed at America’s working and middle-class. 

In autumn 2014, Sanders began to contemplate a bid for the presidency. There were many questions to consider, chief among them: would he run as an independent or a Democrat?

In the end, Sanders decided to run for president as a Democrat since it would be the best way for his message to reach as many people as possible.

This message touches on many issues that Sanders has been championing for decades.

One central goal is to start a universal health-care system. Sanders believes every American — not just those that can afford it — has a right to be healthy.

Sanders also believes everyone has a right to education, so he plans to remove all tuition fees for public colleges and universities.

And finally, Sanders wants to make sure the US government works for the people as a whole, not just the top one percent of earners. As president, Sanders would make sure the working and middle classes have someone who represents their interests.

But winning the Democratic nomination isn't his only goal; Sanders also wants to start a political revolution in the United States. He believes the US political system needs a radical overhaul and hopes to nurture a movement that will change the way Americans view politics.

After all, a political revolution isn't just about winning votes. It's about educating and organizing people so that their voices are heard. This is what he wants for the millions of young Americans who have, hitherto, been uninterested in politics.

To reach these people and raise awareness of the real issues Americans face, Sanders hopes to organize a grassroots campaign with the help of social media and new technologies.

Whether or not he becomes the next president, he hopes his campaign will lead to a much more informed and politically active working and middle class.

> _"Sometimes political revolutions occur — in cities, in states, and perhaps even in nations."_

### 8. Final summary 

The key message in this book:

**Bernie Sanders believes a political revolution is possible in America. In fact, it occurred in Burlington, Vermont, during his term as mayor, and he took it to the rest of the state after being elected to the House and then to the Senate. Sanders has always been fighting for the working and middle class by campaigning for democratic socialist principles such as universal healthcare and free university tuition. In his autobiography, he recounts his journey as an outsider politician and asserts that a political revolution might yet be possible in the United States.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Hard Choices_** **by Hillary Clinton**

_Hard Choices_ offers a first-hand account of the trials and impressive diplomatic successes of the early years of the Obama administration. In this telling memoir, former Secretary of State Hillary Clinton places you at the administration's negotiating table where key policy decisions were made.
---

### Bernie Sanders with Huck Gutman

Bernie Sanders is a US politician who describes himself as a democratic socialist. After four terms as mayor of Burlington, Vermont, Sanders served as a congressman. Currently a senator, he is running for the office of President of the United States.

Huck Gutman, a professor of English at the University of Vermont, is an American academic and political advisor.

