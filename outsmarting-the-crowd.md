---
id: 574087eed37f7e00034566ef
slug: outsmarting-the-crowd-en
published_date: 2016-05-23T00:00:00.000+00:00
author: Bogumil K. Baranowski
title: Outsmarting the Crowd
subtitle: A Value Investor's Guide to Starting, Building, and Keeping a Family Fortune
main_color: 26C028
text_color: 177318
---

# Outsmarting the Crowd

_A Value Investor's Guide to Starting, Building, and Keeping a Family Fortune_

**Bogumil K. Baranowski**

_Outsmarting the Crowd_ (2015) is a stellar beginner's guide to investing. These blinks will give you all the knowledge you need to get started investing. Just don't expect to get rich overnight: good investing is all about patience, discipline and rationality.

---
### 1. What’s in it for me? Find out what it takes to become a great investor. 

There are countless books out there dishing up tips on how to become a successful investor. And the basic message of all these books is "buy low, sell high" — the buzzword of investing. But do you actually understand what it means?

No worries if you don't because these blinks will clue you in on that and much more. They outline the way to a successful start in investing, explaining what company stocks are and what you need to invest in them.

You'll also learn why successful investors are picky and never gamble with their money. And you'll learn why investing won't make you rich overnight.

You'll also learn

  * why a dip in the value of your shares may be a golden opportunity;

  * why markets are like trains; and

  * why buying stocks is a bit like buying an umbrella.

### 2. To own stock is to own part of a business in exchange for funding its operations. 

So, you're ready to start investing in the stock market? Well, the most logical place to begin is by understanding what stocks are, because, despite what some might think, they're not just pieces of paper with a price tag attached.

In fact, every stock represents part of a business, and when you buy stock you're actually purchasing a share of the company. So in the same way that entrepreneurs can own a business outright or split ownership with other partners, as a stockholder, you can own shares of a company.

But while entrepreneurs and partners work day in, day out to manage their business, as a shareholder you don't have any responsibility for managing the company and can sell your shares whenever you want.

So stocks are effectively pieces of a company, and many companies make their stock available to the public. This is a strategic decision that depends on the size and financial needs of a company. Every business relies on financing, but some entrepreneurs use their own savings while others come up with the initial capital by asking family and friends like the founders of Google did.

As a company grows, its funding often needs to grow with it. Eventually, the company can become so big that it only has two options for how to raise the massive amounts of capital needed to run its daily operations and make investments:

First, they can borrow money from a bank just like an average person would do to buy a car or a house. Naturally, this money, along with the interest it accumulates, needs to be repaid.

The second funding option is to go public. That means splitting ownership of the company into shares and selling them on the stock exchange. This option is different from a loan because the money never has to be repaid by the company. Instead, shareholders own stock as long as they want until they sell it to other investors, hopefully turning a profit and sometimes collecting dividends in the process.

Now that you know _what_ stocks are, it's time to learn how to analyze their value and become a successful investor.

### 3. Investing is about rationality and the right timing, so keep your emotions out of it. 

If you've ever read the financial news, you've probably been bombarded with information, both enthusiastic and catastrophic. You see headlines like "Google shares hit record high" or "Oil prices collapse." Some investors become quickly distressed trying to keep up with the media craze.

But sound investing is about rationality, not emotions. Say you own stock in Starbucks and read in the paper that the company's valuation fell by 10 percent in just one day. Your emotions may urge you to sell your shares to avoid getting caught on a sinking ship, but don't listen to them.

If you approach the situation rationally instead, you'll see that a 10 percent loss is an opportunity to buy more shares at a lower price. After all, you know the company has serious potential.

But rationality isn't all you need. Investing also requires great timing. It's essential to know when to disagree with the masses and buy when everyone is selling or sell when everyone is buying.

Good timing usually presents itself when extreme emotions are driving financial markets. For instance, during the 2000 dot-com bubble, investors were slobbering over internet companies like Pets.com and WebVan. However, it was quickly revealed that these companies had no business models, produced zero revenue and were bound for bankruptcy.

Or take the 2008 global financial crisis when financial markets fell apart due to mass panic. During this time, even sound and profitable companies saw their shares devalued. In both this event and the dot-com bubble, there were lucrative opportunities by thinking differently from the crowd: namely, by selling in 2000 when enthusiasm was at its highest or buying in 2008 when panic struck.

> "_The time to buy is when there's blood in the streets._ " — Baron Rothschild

### 4. Successful investing is about discipline, patience and only using money you don’t need to spend. 

You might have dreams of getting rich overnight, but even the most successful investors like Warren Buffett didn't make their fortunes so quickly.

In fact, being a good investor is all about cultivating patience and discipline, so take it slow. Rushing investment decisions is a sure way to fail. The truth is, it often takes months, even years, for an investment to turn a profit.

For instance, in recent market history, Facebook ran into difficulties soon after going public, and the stock dropped substantially only to quadruple a few years later.

But discipline can be tricky. Part of it is about letting go of the fear that you're missing out on an investment opportunity. For example, if markets rose by 20 percent this year and you failed to invest, you might feel like you missed out.

Such feelings are natural. But in reality, there are so many publicly traded stocks that financial markets are virtually infinite spheres of opportunities. In fact, financial experts often compare missing an opportunity on the stock market to missing a train: another one is sure to come!

Finally, discipline also means not gambling with money you might need. Fear, which can drive people to make irrational choices, is the investor's worst enemy. But it can be difficult to keep your fear from getting the best of you if you invest money you need to pay your mortgage.

So avoid making bad choices based on irrational fears and only invest money you know you won't need for the next three to five years. Keep the rest safely planted in your savings account.

> "_You see that the market moves up 5 percent, 10 percent, 20 percent, and you think you missed it. To avoid that kind of thinking, have a regular investing plan_."

### 5. Know your areas of expertise and deepen your knowledge continually. 

Knowing when to break from the crowd is essential to good investing, and the most effective way to outsmart other investors is to build up your knowledge every day. But before you do that, you'll need to know where your investing competencies lie.

Warren Buffett, one of the most successful investors on Wall Street, often cites the concept of a _circle of competence_. The idea is that it pays to know what you're good at and to stick to it.

Let's say you know a lot about pharmaceutical companies. Your knowledge gives you an investment advantage in this field. But you might also be drawn to stocks in industries you barely know anything about. It's important to avoid these and invest only in what you know. Remember, over time your circle of competence can grow if you keep learning about new investment opportunities and other industries.

In fact, continued learning will give you a profound advantage in investing. It's pretty simple really: knowledge gives you an edge because most people these days don't have it. The Pew Research Center's report on reading shed light on this phenomenon: it found that 23 percent of Americans didn't read a single book in all of 2014. Compare this number to just 8 percent in 1978.

Knowing a bit about accounting may help you read a company's financial report, but you need to be knowledgeable about the world around you to know what those numbers mean in the current economic and political landscape. So reading regularly is imperative to succeeding as an investor.

You also need to remember that part of the investing process is learning from your mistakes. So if you sell a stock too late and lose money the best thing you can do is analyze the situation! By doing so, you can determine exactly where you went wrong and be less likely to do it again.

### 6. Sound investment decisions depend on simplicity and selectivity. 

It might seem ingenious to come up with a complex investment strategy, but the truth is, real wisdom is about keeping things as simple as possible. So just focus on what really matters.

After all, there's a nearly infinite number of criteria to consider when assessing a company: brand image, financial performance, the management team's charisma — and the list goes on.

To stay focused, stick to the _Pareto Principle_, also known as the _80/20 rule._ Here's how it works:

In the nineteenth century, the Italian economist Vilfredo Pareto observed that 80 percent of Italy's land was owned by just 20 percent of the population. Based on this observation, he deduced that in many cases, 80 percent of the effects result from just 20 percent of the causes.

And the same logic can be applied to investing: stick with the indicators that matter most to you and forget about the rest of them. After all, only a few will ultimately cause a stock to appreciate.

Selectivity is another way of being focused. So you shouldn't buy a little bit of everything but choose stocks that fit your profile.

In short, you need to apply _filters_. When Warren Buffet considers an investment he uses the following four:

First, do you understand the business? You shouldn't invest in a company selling a new technology you don't know anything about just because it seems innovative.

Second, does the company have long-term potential? After all, some industries are fads and others, like food or health care, are more essential.

Third, do you trust the management? This one is simple because a CEO that previously bankrupted several companies is a red flag.

And, finally, is the price right? In other words, if you think the stock is overpriced, move on and look for another opportunity.

### 7. Carefully choose stocks in competitive companies that anticipate change. 

There are literally thousands of companies whose stocks are traded on the market. To separate the winners from the losers, investors need to know how to ask the right questions.

When looking at a stock, it's best to act like a five-year-old and never stop asking questions such as: Why do people like or need the company in their lives? Why do they consume its products? Is the company selling a revolutionary innovation or is it a trendy start-up that's going to fade away?

You should also ask: Why is _now_ the right time to buy? After all, major corporations like Nike, Exxon and Microsoft have existed for decades, so why should you invest in them today? Well, maybe they just tapped into a new and promising market or have nailed down a profitable long-term trend.

Another strategy is to seek out companies with a genuine _competitive advantage_, that is, one or more attributes that help them outperform competitors. One such attribute is _pricing power_, a company's ability to easily raise prices while retaining its customers.

Just look at Apple. When the company launches a new iPhone, they barely even consider the prices of their competitors. Instead, they set their own prices with the knowledge that consumers desire Apple products and will pay a hefty price to get them.

Finally, you should steer clear of companies that can't anticipate change. In fact, there are plenty of ways to sniff out a company's danger of incurring heavy losses in the near future due to this flaw. Two such indicators are cheap competition and the rise and fall of technology. Take Kodak, which fell out of fashion with the rise of digital photography, causing the demand for film cameras to plummet.

Then again, some companies can better adapt to changing trends. For example, Coca-Cola is no longer just a soft drink company but has expanded its product line to include other drinks that meet the ever-changing needs of consumers.

### 8. Markets can plummet and soar on a dime so your best bet is to stay in for the long haul. 

Today, financial markets are prone to drastic drops and spikes. These changes can be caused by _mood swings_ prompted by the overexcitement and exaggerated disappointment of investors.

That's because, today, investors are taking in too much information, gossip and rumors from the media. They overreact to this information overload by buying and selling too quickly. As a result, investors today hold on to their stocks for exceedingly short periods of time.

For instance, in the 1960s, people held stocks for an average of eight years. Today, people will generally sell them after just six months. This rapid buying and selling also fuels the drastic fluctuations of financial markets.

So, keeping your cool is key, and you shouldn't pay attention to daily fluctuations in stock prices. Say you just invested a lot of money in stocks. You might be tempted to check the price movements of your stocks on a daily basis.

But it's rare that prices go up every day, and even small decreases could frustrate you, causing you to sell too soon. Instead of checking prices on a monthly or even yearly basis, you should give the companies you're invested in the time they need to implement business strategies and grow in the long term.

Then, once you've immunized yourself to the market's mood swings, you can begin profiting from those who haven't. That's because buying stocks is a little like buying an umbrella.

If you walk around New York City on a rainy day, you see loads of vendors selling low-quality umbrellas for exorbitant prices. But if you walk the same streets on a sunny day, the same umbrellas will be selling for half the price.

Now apply that to the way distressed sellers respond when the market isn't working in their favor. Smart investors can jump on the opportunity presented by the panic of others who want to sell at all costs. That's because such moments are the best time to buy stocks at bargain prices.

### 9. Smart investors put security first – so buy cheap, lower your expectations and diversify your stocks. 

Investing might seem like it's all about making more money, but the truth is, good investors make it their priority not to _lose_ money. The best way to do this is to buy cheap.

When you buy a high-priced stock, you expose yourself to potentially huge losses if the price plummets. Obviously, this damage is reduced if you go with cheap stocks.

Imagine stocks as students. There are reliable, straight-A students, which are analogous to your expensive stocks, and everyone knows that they are great. Then there are good students who have temporary problems: these are your cheap, but valuable stocks.

Maybe they were absent a few times or failed a test. Still, these stocks have massive potential because their imperfections make them much cheaper than their real value.

So buying cheap is key, but it's also important to remain conservative with your expectations. That's because you can't predict the future and shouldn't bother trying. Instead of hoping for the best possible outcome, you should strategically construct scenarios that don't rely on a company defying all odds. That requires being realistic and determining whether a company can turn healthy profits even in imperfect conditions.

Just take Tesla. Some speculators might bet on the company's next car being a huge success, but a wise investor would only buy shares of the company if the company has a plan to build a healthy business based on modest, but long-lasting success.

And finally, when embarking on your investment journey, be sure to diversify your stocks. After all, the less experienced you are, the less you'll know about the specifics of any given sector.

Therefore, the greater the diversification, the smaller the impact if one of your stocks crashes because the rest of your portfolio can curb the damage. For instance, if you invest solely in banks, your entire portfolio will crumble in the event of a banking crisis.

### 10. Final summary 

The key message in this book is:

**If you're new to investing, you might think that the stock market is a way to get rich overnight. But the truth is that investing is about patience, discipline and rationality. By planning strategically and sticking with your investments, you can build up the portfolio and the wealth you've always dreamed of.**

Actionable advice:

**Accept your mistakes.**

The great thing about investing is that you don't have to be always right. Even if you're wrong with a few investments, you can still turn a profit with the rest of your portfolio. So accept your mistakes and know they're not deadly. You can even use these failures as inspiration to learn and do better the next time.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Intelligent Investor_** **by Benjamin Graham and comments by Jason Zweig**

The _Intelligent Investor_ offers sounds advice on investing from a trustworthy source — Benjamin Graham, an investor who flourished after the financial crash of 1929. Having learned from his own mistakes, the author lays out exactly what it takes to become a successful investor in any environment.
---

### Bogumil K. Baranowski

Bogumil K. Baranowski is a New York investment professional who manages a private investment fund at Tocqueville Asset Management.

