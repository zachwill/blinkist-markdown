---
id: 556c763c6431340007150000
slug: overcoming-mobbing-en
published_date: 2015-06-05T00:00:00.000+00:00
author: Maureen Duffy & Len Sperry
title: Overcoming Mobbing
subtitle: A Recovery Guide for Workplace Aggression and Bullying
main_color: D4EEEF
text_color: 647070
---

# Overcoming Mobbing

_A Recovery Guide for Workplace Aggression and Bullying_

**Maureen Duffy & Len Sperry**

_Overcoming Mobbing_ (2014) offers a practical guide to the problem of "mobbing" in the workplace. Based on clinical practice and research, it offers valuable insights into the conditions that allow for workplace mobbing, and gives tips on how victims of mobbing can best recover.

---
### 1. What’s in it for me? Learn about a dangerous form of workplace abuse and how you can prevent it. 

Have you ever had the misfortune of working with an office bully? If you have, you'll know how awful they can make people feel. A bully in the office can make going to work a miserable experience.

Although office bullies can make life hell, they can be defeated. A well-run organization will have support channels that victims can turn to, procedures for removing both the bully and their poisonous activities from the workplace.

Even though bullies can easily be confronted, there is a more damaging form of workplace abuse that often goes unchecked. And if it's allowed to continue, _workplace mobbing_ can destroy careers, and even lives. So what is it and what can you do about it? These blinks will tell you.

In these blinks, you'll discover

  * why workplace mobbing can force people out of the careers they love; and

  * why you need to become a _speak no evil company_.

### 2. Mobbing is a form of organizational abuse, designed to remove victims from the workplace. 

Despite its implications, "mobbing" is a term seldom heard in corporate culture. It's not a day-to-day term. It's usually not mentioned in workplace training. And yet, a recent survey found that 35 percent of American workers have experienced workplace abuse at some point during their working lives.

But how exactly is mobbing different from bullying?

Well, both involve the abuse of an individual. However, whereas bullying involves the targeted abuse of one individual by another (usually the boss), mobbing is an often-covert form of abuse (usually perpetrated by a group) that the organization itself supports. The express purpose of the abuse is the removal of the victim from the workplace.

Bullying works differently. Say you have an awful boss who constantly criticizes your work, hounding and undermining you until you begin to hate your job. In this case, you could go to HR, and disciplinary actions would be taken against your boss.

Now contrast this with mobbing. Imagine that you speak out against a company policy, thereby unwittingly infuriating your boss. Unbeknownst to you, he goes to the department head and brands you a "trouble maker."

Together, they work to undermine you, and even recruit a coworker who dislikes you. They spread malicious gossip about you and tell people that your work is garbage. Gradually you become a total outsider; your days become intolerable. Because the head of the department is against you too, you have nowhere to turn, and so you leave the company.

Although there isn't a typical profile for victims of mobbing, it's likely to be someone who speaks out or seems different.

Whistleblowers, for instance, who draw attention to unethical or inefficient business practices are likely to be the ones who suffer from mobbing. People who are culturally different from the rest of the team, or who seem like outsiders, are also often singled out. This includes people like recent immigrants or people with non-normative sexual identities.

### 3. There are three factors which can both lead to mobbing and influence the victim’s response to it. 

In 1991, a recently fired postal worker named Thomas McInvale walked into Royal Oak Post Office and shot dead four of his former managers before killing himself. But what could have motivated him to do that? Mobbing.

By looking into this tragic case, we uncover three factors common to all incidents of mobbing. They also illuminate why McInvale snapped.

First is _work group_ dynamics. Mobbing occurs when team cohesion breaks down, and certain team members begin to be seen as outsiders holding the rest back.

At McInvale's postal service job, the relationships between management and employees were dreadful. Management allegedly treated the staff inhumanely, and McInvale was unfairly singled out by them, arbitrarily fired for doing something they would usually turn a blind eye to.

Next is _organizational dynamics_, which can intensify mobbing. While there are many factors in organizational dynamics that can influence the likelihood of mobbing, we will focus on two: _structure_ and _external environment_.

The organizational structure of Royal Oak Postal Service was one of strict top-down hierarchy, with workers completely subordinate to management. This hierarchy was maintained through harsh discipline, and employees who didn't toe the line were subject to constant abuse.

This rigid structure and harsh disciplinary environment worked in concert with the economic conditions that made up the postal service's external environment. Technology in the industry and competition from FedEx and UPS meant that US Post had to increase efficiencies and cut costs, making job insecurity and job-related stress very high.

Finally there's _individual dynamics._ People with certain personality traits, such as high anxiety or a quick temper, might have difficulty when confronted by workplace abuse. McInvale, for instance, was a high-strung former marine who had trouble handling stressful situations at work. Ultimately, his personality, combined with the other stressful factors in and around the organization, led him to snap.

### 4. Mobbing has a devastating impact on the victim’s health and career. 

Being bullied is bad enough, as the feeling of isolation makes us unhappy and distraught. However, the effects of mobbing extend far beyond our feelings. Being constantly undermined by coworkers can be career-halting, and can leave victims with a gaping hole in their lives.

Many people's lives are centered around their careers, so much so that it can be the focal point of their entire personal identity. Having their career taken from them, therefore, can inflict very serious injuries.

Some of these injuries are physical. Often, victims of mobbing suffer high levels of stress and misery, which can lead to heart problems, high blood pressure, fatigue and insomnia.

Many serious psychological illnesses, such as post-traumatic stress disorder (PTSD), are also associated with workplace mobbing. Victims feel trapped in a toxic environment filled with abuse. They desperately want to leave, but at the same time need to stay at the company to make ends meet and build up their resume. These contradicting stressors sometimes lead to PTSD.

Adding to this, depression and being ostracized by coworkers (many of whom once seemed like friends) can make victims of mobbing feel alienated and worthless. Sometimes, it causes them to lose all faith in their abilities. While mobbing is rarely their fault, many victims nonetheless blame themselves, rather than the organization, for their suffering.

Workplace mobbing can likewise damage the victim's career. Many victims feel that they are unable to return to work again after being mobbed; they question their ability to work and, even if they do recover, they may lose confidence in their work performance, which keeps them stuck on the same rung of the career ladder.

Many victims, in fact, leave the organizational workplace altogether, preferring entrepreneurship or self-employment.

### 5. Mobbing not only affects the victims, but also their families, coworkers and the companies they work for. 

While mobbing centers around the abuse of a single person, that doesn't mean that this one person is the only sufferer. In fact, mobbing affects the people around the target, and at great cost to the company where mobbing is taking place.

For starters, mobbing affects the personal and work relationships that surround the victims of mobbing.

Spousal and parent-child relationships are just one of many examples. The stress that comes from mobbing is often passed on to the victim's loved ones. After all, if you have to devote all your energy to overcoming difficulties in the workplace, how much energy will you have left for your spouse or child?

Furthermore, bystanders who witness mobbing also often suffer. Just witnessing instances of mobbing can lead to physical and psychological damage similar to that suffered by the victim. Incredibly, according to one study, witnesses of workplace mobbing reported higher levels of stress than emergency workers and first-responders!

Finally, workplace mobbing affects the company where it takes place. The company not only loses all the skills and experience that the ousted mobbing victim brought to the table, but may also experience higher turnover rates as other employees become dissatisfied with the toxic working environment.

Replacing staff is a costly affair, especially if the process of hiring new workers is ongoing due to continual mobbing. And as staff become more dissatisfied and stressed, the amount of hours lost to sickness, absenteeism and lost productivity rises with it, further burdening the company.

In fact, some estimate the total monetary cost to US companies due to workplace abuse at somewhere between $180 million and $250 billion a year.

Clearly, workplace mobbing has terrible consequences for everyone involved. Our final blinks will look at ways victims of mobbing can get their lives back.

### 6. Recovery is possible if you make it your primary goal. 

If you are a victim of workplace mobbing, you don't have to feel that your situation is hopeless. While recovery can be slow, it's not impossible. On your road to recovery, there are a few things that you ought to keep in mind.

First, remember that grief is a _necessary_ response to loss. Mobbing can take away all the things you once held dear. Be sure to take the time to grieve what you've lost, and don't force yourself to snap out of it straightaway.

Second, you'll need to prioritize networking. After suffering from social exclusion, it's tempting to shun networks and society altogether. But you must avoid this kind of thinking. Not only is a strong social community generally good for you, it also provides the support you need after your traumatic experience.

Take care to continue nurturing your relationships with loved ones, and go out and find new networks of friends. Why not try volunteering, joining an art class or finding some other social activity you might enjoy?

Of course, it's also important to seek professional help from therapists, medical professionals and career counselors.

But to ensure that you take the clearest route out of that mobbing malaise, you'll need a good plan.

First, build a support network of people you trust.

Then, select the five life areas (such as trust, career, relationships, etc.) that have suffered the most as a result of mobbing. Then prioritize fixing these areas.

Talk to your support team about your experiences with mobbing and listen to their perspective. Their advice and perspective will help you gain insights into what exactly happened, and they will have ideas on how to move forward in the future.

Finally, as you move forward, follow an iterative process by asking yourself: Am I moving forward in the right way? If not, what can I do to adjust my trajectory? Every case is different, so be sure to remain flexible.

### 7. Putting people before profits can lead to a healthy, mobbing-resistant workplace. 

Everyone wants to work in a healthy environment, and companies play an important role in making the workplace healthier and more resistant to mobbing. It's therefore crucial that your company understands and acknowledges workplace mobbing, as well as the measures one takes to address and prevent it from happening.

Traditionally, organizations were evaluated primarily in terms of their profitability and productivity. However, in today's world, the good reputation of a company also hinges on the degree to which they care for their employees. Companies therefore need to look beyond profits as their metric for workplace success.

That's not to say that profits don't play a role. Indeed, it's in the organization's financial interest to prevent mobbing, as healthy workplaces are more profitable than unhealthy ones.

If you need convincing, consider that some of the most successful companies, among them Google and Starbucks, rank high in overall revenue performance and in their dedication to the health and well-being of their employees.

In order to create a healthy work environment, you need to become a _speak no evil_ company.

In contrast, places where mobbing thrives are either _see no evil_ companies _,_ which ignore mobbing and bullying and focus only on profits, or _hear no evil_ companies, which have established guidelines against abuse but focus on individual bullying rather than workplace mobbing.

Speak no evil companies value people (both customers and employees), and place their primary focus on their well-being throughout the organization.

Catholic Health Service is one such company. They always rank highly in both patient happiness and employee satisfaction. According to their value statement, they put patient care first, employee care second and community third in their list of priorities. Profits don't even appear on the list!

As a result of their people-focused business ethos, abuse within the organization is low and most staff retire after 30 years of service or more.

### 8. Final summary 

The key message in this book:

**Workplace mobbing is a worrying phenomenon that doesn't just ruin the lives of those at whom it's directed, but the lives of those who witness it or are close to the victim, too. However, that doesn't mean victims of mobbing are helpless! With the right techniques we can overcome the damage done by mobbing.**

**Suggested** **further** **reading:** ** _My Age of Anxiety_** **by Scott Stossel**

This book offers a candid, valuable insight into the world of the clinically anxious. The author takes us through his personal struggle with anxiety while presenting us with scientific, philosophical and literary work about the condition and the treatments available for it.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Maureen Duffy & Len Sperry

Maureen Duffy is a therapist and expert in workplace mobbing and bullying, and an affiliate with Qualitative Research Graduate Program at Nova Southeastern University. She also co-wrote the book _Mobbing: Causes, Consequences and Solutions._

Len Sperry is Professor of Mental Health Counseling at Florida Atlantic University, and works as a consultant for various businesses on the prevention of mobbing in the workplace.

