---
id: 5a37c15eb238e1000765baff
slug: oversubscribed-en
published_date: 2017-12-22T00:00:00.000+00:00
author: Daniel Priestley
title: Oversubscribed
subtitle: How to Get People Lining Up to Do Business with You
main_color: EB2F31
text_color: D12A2B
---

# Oversubscribed

_How to Get People Lining Up to Do Business with You_

**Daniel Priestley**

_Oversubscribed_ (2015) explains how to create a business that generates more demand than it can supply. Used by Apple to create a passionate and loyal customer base and by boutique brands to manufacture desire and earn huge profits, the business model of oversubscription both gets attention and keeps it. In these blinks, you'll learn how it works.

---
### 1. What’s in it for me? Learn how to increase demand for your product or service. 

Today's consumer markets are so flooded with endless options that it can be hard for new products to take hold. That's where the concept of oversubscription comes in. Instead of competing with giants, you have to carve out your own market and create a loyal customer base that will be clamoring for every product you release — so much so that demand will exceed supply and you'll become oversubscribed!

From focusing on innovation to maintaining great customer relations, there are a lot of different ways to get your brand to stand apart. The following blinks will explore how some brands have managed to keep ahead of the competition and, of course, how you can emulate their successes.

In these blinks, you'll learn

  * that a small but dedicated group of fans is enough to keep you afloat;

  * why music festivals sell early-bird tickets; and

  * what made the iPod such a huge success when it was released in 2003.

### 2. Successful businesses create more demand than supply and find niche markets to generate desire. 

Every year there will be some expensive new gadget or device that people really don't need, yet they'll desire it nonetheless.

There's nothing mysterious about this since it all comes down to popularity and scarcity: When everyone else has a certain thing, you'll want to have it, too. And when something is so popular that it's hard to come by, that's when something becomes _really_ desirable.

You may have learned the laws of supply and demand in grade school, but these days, the secret of success is in the laws _demand and supply_.

When there is more demand and buyers than there are products and supply, this causes a business to become _oversubscribed_. This might sound bad, but it's actually a good thing. Indeed, it's a powerful means of generating profit.

One of the best examples is the iPhone. Whenever Apple releases a new version, it's guaranteed to sell out before the phone even goes on sale. Now, this isn't a problem for Apple. They want people to see long lines of iPhone enthusiasts waiting for days to be the first ones to buy the latest model. It's free promotion that shows how huge the demand is.

However, demand can shift over time. For Apple, their iPhone needs to remain desirable even as companies like Samsung get increasingly competitive.

One of the biggest challenges to staying oversubscribed is knowing how to respond to a market that becomes flooded with similar products once there's an obvious demand.

In the 1980s, plastic surgery was an oversubscribed business, with a handful of doctors that could charge enormous fees and still be fully booked. But after thousands of other doctors joined in, prices had to be greatly reduced in order to remain competitive.

So a great way of remaining oversubscribed is to carve out your own niche market with loyal customers.

Moet & Chandon, the makers of Dom Pérignon champagne, is a good example. While there are a lot of champagnes out there, Moet & Chandon have managed to build a luxury brand that is regarded as the best of the best. So customers who want to impress know exactly where to turn.

### 3. Disrupt the market by being more innovative and convenient, or less expensive. 

Finding a niche market is easier said than done, as is inventing a popular new product. Nevertheless, one of the best ways to start an oversubscribed business is to come up with a truly innovative and unique product.

Being truly unique and inventive means you have no competition, which is what happened when the iPod came out. Since no one else had ever offered a user-friendly, well-designed MP3 player, there was no market for the iPod before it came out. So when the iPod was released in 2002 there was no competition; it had the market all to itself.

When competition does arrive, you can make sure your customers stick with you by building strong relationships.

A traditional, yet reliable way of doing this is through the use of contracts. When you're searching for a phone carrier, for instance you consider a load of offers, carefully comparing each plan. Once you select a carrier and sign a contract, say, for 24 months, you are committed to a relationship with that carrier and the others no longer matter.

Another path to oversubscription is to make a more convenient version of something that already exists.

A classic example of this is Amazon, the online one-stop shopping center that put everything under one roof. Anyone with an internet connection can casually browse their seemingly endless marketplace, read customer reviews and buy everything from tires to baby toys with just one click.

For similar retailers like Woolworths or K-Mart, this kind of convenience is hard to compete with.

A third way to achieve oversubscription is to provide the cheaper option.

For a successful example of this, you can follow the methods of UNIQLO. This global clothing company was able to sell quality clothes at a low price by making exclusive deals with suppliers. This allows it to buy fabrics at wholesale prices and pass the savings on to the customer.

In the next blink, we'll look at ways of growing your customer base without breaking the bank.

### 4. Manufacture desire by creating exclusivity and a special customer experience. 

Which sounds more desirable to you: a book that all your friends are reading and enjoying or a book that is constantly being advertised on TV, at the movies, on the subway and online?

Generally speaking, people respond more positively when products aren't being aggressively pitched to them.

Think of those street vendors who spend all day shouting at passersby about how much of a bargain their products are. How many of these street vendors have you purchased from?

Now, think about a place like Gallery Lafayette, a luxury shop in Paris that doesn't have to spend any time or money on aggressive ads. At this shop, there's an exclusive Chanel counter where one customer at a time can buy the latest handbag. Naturally, this means there's always a long line, but people happily stand in it for a chance to get a moment of special treatment at the counter.

Offering this kind of exclusive, one-on-one service can make people feel special, which is another powerful way to acquire loyal customers.

At the Gallery Lafayette, the long line doesn't just broadcast the shop's popularity; it also generates excitement and makes people curious about the tailored treatment that a wait in line will lead to. If someone's never experienced it before, they'll keep imagining what it's like to get one-on-one attention until they eventually become a customer.

When you provide a great customer experience, you'll then be able to use a questionnaire to gather customer stories you can use as testimonials that will grow your customer base even more. A great way to do this is to ask your customers the right questions, such as: How did our products or service change your life for the better? Once you have a story, you can pass it along to potential customers through social media or even get the satisfied customer to agree to make a YouTube video.

When others hear the story, they'll be rushing to be the next in line.

### 5. Forget about traditional marketing and make something that people will talk about. 

Everyone likes to think of themselves as independent thinkers — as people who make their own unique and meaningful decisions. But the truth is, we're all heavily influenced by what those around us are saying and doing, and any long-lasting, successful company will try to use this to its advantage.

Nowadays, this means going beyond traditional advertisements.

It's not enough to put an ad in a magazine that says, "Our high-quality cameras take brilliant photographs" — because anyone with an internet connection can simply go online and compare your camera against every other camera on the market. And, immediately, the customer reviews are going to be more important than whatever slogans you've thought up for your ad.

Since buyers now have expert reviews and a wealth of customer options to sort through, the best thing you can do is get other people talking about your product or service. Once you begin to get honest, glowing reviews from customers, you'll be on the way to renown as a reliable, trustworthy brand.

These days, getting people talking about your business on social media, YouTube and other online publications is the ultimate marketing tool.

This is because customers place far more trust in other customers than in corporate slogans and catchy jingles. And this is a good thing since people are happy to talk about the things they like and it doesn't cost anything for a happy customer to post a glowing review on Twitter with your company name as the hashtag.

Once you get those priceless tweets and start to be known as a trustworthy brand, it'll be easy to expand into other products, services and platforms.

If your underground music blog builds up a loyal following, you can then start a podcast or a YouTube channel and feel comfortable that people will recognize your brand and continue to trust it. After this successful expansion, you could then go on to start a music label and people would expect that your expert taste would carry over into releasing quality music.

In the next blink, we'll take a closer look at how to get your customers excited.

### 6. Use updates to build anticipation and get customers subscribed. 

There's a good reason that the collectables industry is worth billions of dollars: people love to have one-of-a-kind items and are excited to own something that only a few others have even touched.

Your business can tap into this kind of excitement and build up valuable anticipation through what's known as _business-to-customer signaling_.

This is a way of giving your customers a heads-up and letting them know that something special and unique is about to happen, like the release of a new limited-edition product.

One business that's great at building up hype on a regular basis is the Glastonbury Music Festival in England. Year-round, people can sign up for updates and a chance to get advance tickets to its annual three-day outdoor concert. Every year, the lineup of musicians is expertly teased, building-up excitement until, finally, 350,000 subscribers are frantically trying to buy 120,000 early-bird tickets that sell out in minutes.

This is a very literal example of oversubscription in action, and it's greatly helped by expert signaling that gets customers jumping at the chance to be part of something unique and limited.

Signaling also works the other way around, with potential customers telling the business what they're excited about.

By giving people the opportunity to receive updates on different products, you'll soon find out which ones they're most interested in. This works for your business by providing valuable customer insight as well as a good idea of future sales, and the updates will benefit the customer by giving them advanced notice on when they should have their money ready.

So, as you can see, offering customers a chance to subscribe for updates is a win-win. The business engages with customers, strengthening their relationship with them and building up excitement for future releases; and the customers get to feel special by getting advance notice on the exact things they're interested in.

In the final blink, we'll look at more valuable ways of engaging with your customers.

### 7. Engage your audience with entertaining and informative content. 

Once your customers are subscribed, it's like having a captive audience, and what you do with that audience will determine how successful you'll be.

Sending out a regular newsletter is a great way to engage with your customers and keep them loyal, but you have to have the right kind of content to keep them engaged. You could send out funny notices with silly gifs and jokes, but the better route is to send valuable information that also teaches and continues to demonstrate your expertise.

Chances are your customers are busy and don't want to waste time. So, if your newsletter doesn't provide valuable information, they may quickly end up putting your emails in the trash or tagging them as spam.

It's important to remember how much time most people spend researching before they buy something, especially when it's a big item. For things like cars or deciding where to go on vacation, people spend an average of seven hours researching online before making a purchase.

Through a newsletter or blog, you can be an important part of that research time by providing statistics, how-to-guides, expert insight and other valuable information. The more time you can engage a potential customer, the more likely they'll be to use your product or service when the time comes.

Information can be entertaining as well. If you're providing a car-share service, your blog or newsletter might contain a funny YouTube video that also provides information on how car sharing is good for the environment.

So keeping customers engaged and entertained is good for everyone, but it's best to focus on providing either information or laughs. Otherwise, customers may get confused.

A good rule of thumb is the _80-20 Rule_ : Imagine you're providing an in-store workshop to raise the profile of your business in the local community. During the workshop you might tell a couple jokes, but the comedy shouldn't take up more than 20 percent of the time. If people wanted jokes, they'd go see a stand-up comedian.

The same rule should apply to your content. People should know what to expect and look forward to. Remember, your goal is to create a strong and easily identifiable brand, not to keep people guessing.

So, now that you know some of the secrets to success, it's time to get people talking about your business!

### 8. Final summary 

The key message in this book:

**Scarcity is a good thing! When customers see your products or services as being exclusive and limited, they'll want them even more. One way to distinguish your business is to offer something that no one else offers — whether it's exceptional convenience, unbeatable prices or innovative products. To hold on to your customers and build strong relationships, keep them engaged with valuable content so that they'll keep you in mind and continue to enjoy your products and services.**

Actionable advice:

**Here's an exercise to find out what's next for your business:**

Imagine that money wasn't an issue and you could do whatever you wanted. Think about what would excite you the most and get you incredibly engaged with your business. Maybe it would be a huge, interactive social-media campaign across multiple platforms. Whatever it is, start thinking about ways you could refine it and make it fit your actual budget. Chances are your customers would also find it fun!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Your First 1000 Copies_** **by Tim Grahl**

In _Your First 1000 Copies_, Tim Grahl outlines a step-by-step guide to his connection strategy for authors. He reveals how you can create a platform to find your audience and keep them in the loop so they'll buy your next book. Packed with vital information on how to keep readers interested in your work with good online content and how to sell without being sleazy, _Your First 1000 Copies_ is a must read for any aspiring or established author.
---

### Daniel Priestley

Daniel Priestley is a an award-winning entrepreneur who's been building and buying businesses around the world since he was 21 years old. He is also a sought-after speaker who provides expert business advice, as well as a best-selling author of numerous books, including _Entrepreneur Revolution_, which he cowrote with Glen McCready.

© Daniel Priestley: Oversubscribed copyright 2015, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

