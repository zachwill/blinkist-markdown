---
id: 552463b03066650007270000
slug: overwhelmed-en
published_date: 2015-04-08T00:00:00.000+00:00
author: Brigid Schulte
title: Overwhelmed
subtitle: Work, Love and Play When No One Has the Time
main_color: FDF333
text_color: 636014
---

# Overwhelmed

_Work, Love and Play When No One Has the Time_

**Brigid Schulte**

_Overwhelmed_ (2014) explores how our outdated attitudes to gender, parenthood and race are making us all stressed and frantic. Author Brigid Schulte shows how such old fashioned ideas are holding us back from living happier, more fulfilled lives. Yet, it doesn't have to be this way. There are alternative ways to live, we just need to learn how to implement them.

---
### 1. What’s in it for me? Tear down old-school ideas about work and home and relieve your stress. 

We live in enlightened times, right? Well, yes and no. Although attitudes toward gender, race and sexuality have evolved in recent years, there are still areas where people remain decidedly unenlightened.

One such area concerns society's approach to gender. Incredibly, most Western societies still adhere to the idea that a woman should raise children and look after the household while a man should concentrate his energies on his work.

This outdated, backward point of view is making us all lead unfulfilled, overstressed lives. In these blinks, you'll get a detailed look at how and why we are so stressed out and crucially, discover exactly what we can do to relieve daily pressures today and for the future.

In the following blinks, you'll discover

  * why many CEOs think that a man who cares for his children is a poor employee;

  * how being stressed out can make part of your brain shrink; and

  * why African tribes have never had a problem finding adequate child care.

### 2. Parents are stressed, with no free time and too much to do. Mothers are especially overwhelmed. 

We all know what it's like to feel overwhelmed. Balancing your work and your private life is hard enough even when you have no dependents, but more so if you're a parent with mouths to feed and diapers to change.

There's no question that parents today are stressed, having to juggle long working hours while raising a family. This balancing act places a great deal of pressure on an individual's already limited time.

In one 2008 study, the Families and Work Institute asked American workers about their daily lives and found that half of the respondents felt they had too many tasks for a typical workday. Some two-thirds felt they didn't have enough time for themselves or their spouse; and three-quarters reported they didn't have enough time for their children. 

This sense of being overburdened is particularly prevalent among women. Since the 1970s, the percentage of employed women has risen dramatically, which has led to circumstances where, to carve out and maintain a job that generates enough income, many mothers need to work full time.

Another study performed in 2004 asked mothers with children under six if they could say they often had any free time. None of the women in the study said they did! Yet a lack of free time isn't just a problem for mothers; only 5 percent of fathers queried in the study said they had any free time.

Unsurprisingly, a lack of free time has led to high rates of stress and anxiety in the United States.

The American Psychological Association reported in 2011 that Americans as a whole are chronically overstressed. Earlier, a 2007 study by the World Health Organization found that Americans, while living in one of the world's richest countries, are on average the world's most anxious people.

So life is stressful — but is stress that big of a deal? It is, and we'll learn why in the next blink.

### 3. Too much stress can physically harm our brains, and cause us to lose control of our emotions. 

Is stress always harmful? In short, no. Feeling stressed or anxious for short periods of time has actually benefitted humans throughout our evolution, sharpening our senses in times of threat.

However, _permanent stress_ caused by feeling overwhelmed on a daily basis is not beneficial. In fact, permanent stress actually damages our brains.

Emily Ansell, an assistant professor of psychiatry at the Yale Stress Center, discovered evidence of this effect as part of a groundbreaking study. She found that people who felt constantly overwhelmed had a prefrontal cortex (the part of the brain associated with sophisticated behaviors, such as planning, self-control and reasoning) that _was actually smaller_ than of those who led less stressful lives.

Moreover, the amygdala — the most ancient or "primitive" part of the brain associated with feeling fear, aggression and anxiety — correspondingly _grows in size_ in people who are constantly stressed.

Ansell predicts that such changes to the brain are likely to be associated with a diminished ability to control one's emotions, and an increased risk of displaying addictive or self-destructive behaviors.

Yet it's not just the individual who suffers from being overwhelmed. High levels of stress experienced by so many people can have an aggregate effect on society.

A stressed workforce, for example, is far less likely to be productive or conscientious.

Bruce McEwen, the head of neuroendocrinology at Rockefeller University in New York, has found that overstressed medical students are less capable of making effective, complex decisions and are more likely to make careless mistakes. 

Acclaimed psychologist Mihaly Csikszentmihalyi proposes that living with constant stress, which is connected to not being able to think clearly or orderly, makes it much more difficult to reach a state of "flow thinking," where one is able to focus deeply on a task and thus potentially find creative solutions to complex problems.

### 4. The industrial age inspired greater productivity; yet since then, workers have never slowed down. 

We know that stress is bad and anxiety can hurt us both emotionally and physically. But how did we as a society get to this point of being so overwhelmed?

The effects of the Industrial Revolution play a part in our transition to a more stressful world. With the advent of industrial production in the late 1700s and early 1800s, productivity, or getting the most work done in the shortest time, became a central goal.

This meant that workers had to work harder for increasingly extended periods of time. Since then, such working habits have more or less stayed the norm, at least in the United States.

In 2001, the Organisation for Economic Co-operation and Development (OECD) found in its "Better Life Index" study that Americans work the longest hours in the industrialized world, with this lead increasing throughout the 1990s.

In addition to long working hours, American workplaces are rather inflexible when compared to other OECD countries, which can cause tension between work responsibilities and home life.

For example, American workers often can't request flexible working hours or part-time work; what's more, part-time workers lose out on benefits and insurance coverage, unlike in many European countries. The American workload has also increased since the beginning of the information age.

In short, we haven't evolved to cope with the constant influx of information that needs to be sifted through and understood each day. In fact, one study by researcher Jonathan Spira found that the average American worker can spend over half a working day merely processing emails!

Figuring out what to focus on is, in and of itself, a major source of stress and unproductivity.

Time management guru David Allen says that trying to decide what to pay attention to during a working day can lead to _decision fatigue,_ which in turn creates a _gnawing sense of anxiety_.

This constant stress breaks down an individual's willpower, making it more difficult to curb interruptions. This in turn eats away at productivity, as it takes time to recover from each interruption.

> _"Stress is no more and no less than the inability to control and shape the forces that shape our lives" — Neuroscientist Huda Akil, University of Michigan_

### 5. Dad works; Mom cares for the children. Old stereotypes still persist in our modern age. 

Although it may seem like we live in a more modern world, many of us are still, often subconsciously, influenced by the beliefs or stereotypes that influenced our grandparents' generation.

One such stereotype revolves around the ideal male worker, who focuses on his career from early adulthood until retirement, and the ideal mother, who alone cares for children and the home.

While we may not openly admit it, people who deviate from these stereotypes are often perceived as somehow abnormal.

For example, a 2012 survey by WFD Consulting queried more than 2,000 supervisors, managers and executives and revealed an entrenched _caregiver bias_. That is, more than half of those surveyed thought that men who were committed to their personal and family lives could not be equally committed to their jobs.

The perpetuation of such roles places particular pressure on women, who find themselves torn between expectations at work and society's view of their role at home, which in turn results in feelings of guilt and stress.

In a 2008 study in the _Journal of Applied Psychology_, for example, working mothers were viewed as selfish and uncommitted mothers.

The power of gender norms also encourages certain behaviors in men which cause stress, and often, men as well are penalized when they don't adhere to societal expectations.

Men too face a "flexibility stigma" when they seek out more flexible working hours, to care for children or family members for example. Sociologist Joan Williams found that co-workers and managers judge a man as uncommitted and lazy if he diverges from the stereotype of the "ideal worker." Consequently, such men are often overlooked for promotions.

So what can we do to crush such stereotypes? The next blinks will show you how.

### 6. Do we need to work 100 hours to be productive? No. Flexible working hours are the future. 

So how do we do away with inflexible, long working hours to something more humane and less stressful?

Let's look at some examples of companies that have found flexible, successful work strategies.

Software design company Menlo Innovations was founded by Rich Sheridan, a man who spent years working unhappily in stressful, traditional workplaces.

To create an environment more conducive to productivity and employee well-being, Sheridan focused on the idea of flexibility. To curb long hours working toward unreasonable goals, the company concentrated on setting realistic targets and, to make sure these goals were met, created a culture where the staff shared regular, constructive feedback — freeing employees to work when and how they saw fit.

The result? Menlo employees express considerable satisfaction with their work-life balance and the company as a whole has been praised for its dynamism and creativity. What's more, Menlo has been named by the American Psychological Association as one of the most "psychologically healthy workplaces" in the United States.

Another interesting example is Clearspire, an online law firm. Clearspire harnesses the power of technology to allow employees to work from home.

Evidence has shown that parents who are allowed to telecommute from an "online office" are more productive and far less overwhelmed. Working from home gives employees more autonomy over the organization of work and family time.

Parents can then attend important family events and have more time for leisure activities, and importantly, are able to work when they're most productive.

The idea of judging productivity by the sheer number of hours worked is finally giving way to a more trusting work atmosphere, where employees such as those at Menlo are given clear, structured performance goals and the freedom to meet those goals however they see fit. 

And importantly, Menlo as a company has achieved financial and reputational success in doing so!

### 7. Gender stereotypes are not innate. Through good public policy, they can be overcome. 

So you might think, if gender stereotypes still hold traction in society, perhaps they explain the way things actually are — shouldn't we just accept them and move on?

We shouldn't. The truth is that gender stereotypes are _socially constructed_ and not biologically determined.

American anthropologist Sarah Hrdy's research into child-rearing among the !Kung tribe of the Kalahari Desert in Africa, far from affirming the concept of a mother as the sole caregiver, showed that children are brought up collaboratively in the tribe, a practice called _alloparenting_.

Likewise, the "grandmother" hypothesis _,_ or that humans when compared to other primates live a considerably long time past child-bearing and child-rearing age, strongly argues against the ingrained stereotype of the nuclear family and the mother as a "natural" caregiver.

Looking outside the United States, it is easy to see how negative gender stereotypes can be overcome.

Denmark is a prime example, having spent the past 40 years, through active and interventionist state policy, attempting to address gender biases. Progressive state policies include one-year parental leave during a child's first year of life for mothers _and_ for fathers; setting maximum working hours; and guaranteed, high-quality child care.

Consequently, such policies have inspired significant social change. For example, there is an almost total gender convergence with regard to time spent on housework, highly uncommon by world standards. Women in Denmark also enjoy the highest amount of leisure time in the world.

Moreover, 60 percent of Danish men report being happiest in relationships where home and childcare responsibilities are evenly distributed, a significantly higher number when compared to the United States.

Denmark is setting a solid example, as the country is consistently ranked (by the United Nations World Happiness Report, for example) as having the world's happiest people; more competitive economy per hours worked than the United States; and 80 percent of Danish women in paid employment.

So with good public policy and supportive workplaces, we can change stereotyped gender roles, which in turn reduces unnecessary stress for men and women.

### 8. Admit that you can’t do everything today. Spend time practicing mindfulness or meditating. 

We now know what we need to do to overcome the societal stressors that make us overwhelmed.

Now let's focus on how to overcome our own problems, individually.

We should first understand that as individuals, we can't do everything. Instead, we should only do what is within our capabilities.

Being swamped with an endless list of chores often leads to an acute sense of ambivalence as to what we should be doing right now, which causes stress and leads to unproductivity. To avoid this, we need to acknowledge that it's impossible to do everything.

Psychotherapists David Hartman and Diane Zimberoff recommend that people try to maintain realistic expectations of what they can achieve in any given day, and prioritize by concentrating on the most important tasks at work, as completing these tasks will make us feel satisfied and productive.

Meditating and practicing mindfulness are other methods of overcoming stress and anxiety.

Professor Mihaly Csikszentmihalyi has observed that the brain, when "left to itself...turns to bad thoughts, trivial plans, sad memories and worries about the future." He stresses that "entropy, or disorder, confusion, decay, is the default position of the consciousness."

One way to tackle entropy is by engaging in mindfulness and meditation, which involves learning to experience the body and the mind in the present moment, and simply observe your thoughts.

Even a few minutes a day of mindfulness can help you find a peaceful center free of unnecessary stress.

In one study, neuroscientists at Harvard University found that after practising mindfulness and meditation for only eight weeks, the prefrontal cortex — that part of the brain responsible for planning and self-control — literally enlarges. And the fear center of the brain actually contracts!

### 9. Final summary 

The key message in this book:

**We needn't lead our working and private lives in accordance with outdated ideas and old-fashioned beliefs. Instead, we can change our personal lives, our workplaces and our society at large to create less stressful and generally happier lives.**

Actionable advice:

**Find a quiet place and meditate every day.**

Combat feelings of being overwhelmed by practising mindfulness or meditation techniques regularly. They require just a few minutes of your day! Meditation is a proven method of finding a center and listening to your mind and body that can help you gain perspective and peace, regardless of your work or social situation.

**Suggested further reading:** ** _The Life-Changing Magic of Tidying Up_** **by Marie Kondo**

_The Life-Changing Magic of Tidying Up_ isn't just a guide to decluttering, it's a best seller that's changed lives in Japan, Europe and the United States. The _Wall Street Journal_ even called Marie Kondo's Shinto-inspired "KoMari" technique "the cult of tidying up." Kondo explains in detail the many ways in which your living space affects all aspects of your life, and how you can ensure that each item in it has powerful personal significance. By following her simple yet resonant advice, you can move closer to achieving your dreams.
---

### Brigid Schulte

A journalist with _The Washington Post_, Brigid Schulte writes on work-life issues and poverty, and has closely examined the concept of work and leisure time in America, exploring the question of how we all can lead "The Good Life." She has also written for _Style_ and _Outlook_.

