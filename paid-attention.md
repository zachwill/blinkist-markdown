---
id: 559bc89d3966610007990000
slug: paid-attention-en
published_date: 2015-07-09T00:00:00.000+00:00
author: Faris Yakob
title: Paid Attention
subtitle: Innovative Advertising for a Digital World
main_color: FEFF33
text_color: 666614
---

# Paid Attention

_Innovative Advertising for a Digital World_

**Faris Yakob**

How can you get people interested in your brand in an age of ad-blockers, vanishing attention spans and colossal consumer choice? _Paid Attention_ (2015) discusses the fast-changing media landscape, and maps out strategies for success that reach beyond banner placement and pop-ups.

---
### 1. What’s in it for me? Discover how advertising is changing and what you can do to keep up. 

On your walk to work today, how many adverts did you see? Probably hundreds: in newspapers, on billboards, mixed in with the podcast you listened to. How many of them caught your attention? Probably none.

There is a plague of adverts, and this makes it very hard for any to catch your eye. For advertisers and marketers this is proving a huge problem: the old way of advertising just doesn't work any more.

These blinks will show you how advertising is changing to cope with these new conditions. If you work in advertising, then reading these blinks will help you keep up with a profession in flux.

In these blinks you'll discover

  * why, in terms of content, we now do in two days what previously took us 10,000 years;

  * why Red Bull sponsored a man who jumped from outer space; and

  * why market research is next to useless.

### 2. Due to rapidly increasing demands on people’s attention, the traditional model of advertising just doesn’t work. 

Most of us view advertising as a sophisticated industry. And although that's true to a certain extent, it's also worth remembering that the concept behind it is simple: advertising is ultimately about grabbing attention. If you can't do that, you won't be able to sell anything to anyone.

But in the modern world, this goal has become increasingly complicated due to the rapid proliferation of content. There are more demands on our attention than ever before!

As Google CEO Eric Schmidt put it, by 2010, the amount of content we created _every two days_ was equal to the amount humanity had produced from the dawn of civilization up until 2003.

Basically, we've shifted from a media environment defined by scarcity to one defined by abundance. And for the advertisers, it's incredibly hard to cut through so much competition to reach an audience.

Making things even more difficult, people's attention spans are decreasing — which makes the competition that much fiercer.

So, what can advertisers do? For a start, they have to change their strategy from the _AIDA model_, which follows these four steps: "A" — attention is attracted; "I" — interest is raised; "D" — desire begins; "A" — action is taken, the product is purchased.

At every one of these steps, some people are lost, but the ones who remain will have a greater likelihood of making a purchase. In other words, traditional advertising is about buying attention from the greatest amount of people possible, at the lowest possible cost. This approach used to give marketers the best chance of funneling as many people as possible through to the end of the process.

But in the modern world, this model simply doesn't work. Attention is just too scarce — you'll never attract enough people to make it worthwhile.

That's why the following blinks outline a new approach to advertising for today's media landscape.

### 3. If you want to reach consumers, you have to create an emotionally resonant brand. 

The modern consumer is overwhelmed by choice. So how does anyone decide what to buy? The truth is, most of us make purchasing decisions according to _brands_.

At heart, a brand is an emotional link between the customer and the company. Ultimately, brands help people build their identities around products. In other words, you are what you buy!

Since the connection is an emotional one, the best brands stand for something we admire, love or cherish — often many different emotions at once.

Take Coca-Cola, which stands for the joy and fun of summer, the happiness and family togetherness of Christmas, the excitement of a great party.

That leads us to another important aspect of brands: brands are social, giving people a common ground for connection. Think of a shared affinity for a certain brand as shared hobby, an interest that brings people together.

But how are brands formed? Well, it all comes down to the company behind the brand, and how it builds on social understandings. After all, a good brand is the image of the company behind it.

Google, for instance, has a brand that represents the future and the pursuit of knowledge. Its core internet search business is key to its brand identity, as is its famous motto: "Don't be evil."

And yet, no company can have sole ownership of its brand. Because the modern-day brand is also formed by people talking with friends and creating a collective idea around it.

To understand how this works, think about money: if we hadn't all collectively agreed that money represented something (a currency), paper bills would have no value.

That's how it goes for brands. Companies can try to convince us of their brand identity, but if we don't agree with them, the brand will be worthless.

### 4. Instead of relying on market research, find out what your customers want by connecting with them personally. 

Has a market researcher ever stopped you in the street, asking about your purchasing habits?

Although market research is an $11 billion industry in the United States, it's actually a massive waste, because asking people what affects their shopping behavior is pointless. Most of our purchasing decisions happen on an unconscious, emotional level.

You may find that hard to believe. After all, most of us rationalize our purchases, telling ourselves we make decisions on the basis of cost or quality. But in reality, our subconscious leads us to act in ways we can't fathom.

Remember, brands have an emotional appeal. Accordingly, our connection to companies tends to be emotional — not rational. And those emotional connections can never be adequately explained by market research.

But if market research is moot, what can companies do to understand customers? Well, the answer lies in customer service, which is about focusing on individual consumers to understand their needs and problems.

And in the social media age, this is crucial. Because today, it's easy for just one customer with a bad experience to destroy your brand image.

Consider Dave Carroll, whose guitar was damaged on a United flight. He spent months trying to resolve the issue through customer service, but nothing got sorted. Then he wrote a song expressing his frustration and posted it to YouTube, where it went viral — quickly amassing over twelve million views. It was the worst possible thing for the United brand.

But it's a teachable moment for us: instead of spending money trying and failing to find out what people want, simply reach out to your customers (especially anyone who's angry with you) and find out firsthand. Your customers will be happy — and you'll also build up a great brand image.

### 5. Good advertising offers customers something of value in exchange for their attention. 

In earlier blinks, we've discussed the importance of connecting with consumers to understand their needs. Well, it's equally important to transmit your own message to customers. In other words, good communication is at the heart of advertising.

But what does that actually entail? Some marketers approach the question of good communication from a rational framework. They believe that giving people a simple choice between two (or more) items and arguing for the advantages of their own superior product is the simplest way of effectively transmitting their message.

The problem with that approach? Emotions are what drive our decision-making. That becomes crystal-clear when we look at research on people who've lost their _amygdalae_ (the area of the brain involved in emotional response). Without those vital neurons, humans simply can't make decisions any more.

Not to mention, even people with functioning amygdalae don't like making decisions: studies have shown that giving people more choice actually makes them unhappier!

This means that good advertising must surpass the simple product-comparison approach. Instead, advertisers have to offer the customer something in exchange for their attention.

And that's a crucial point. After all, you're delivering commercial messages your audience never asked for — one of the many messages bombarding your customers every day. Remember that the modern consumer feels ever-more angry and frustrated with each new disruption.

Well, marketers can circumvent this by offering consumers something of value. Customers don't resent _all_ messages — they just want messages they can use, somehow. Accordingly, clever advertisers will find a way to add some benefit or use to their messages, ultimately building brand loyalty.

Red Bull excels at this approach. The energy drink company invites customers to design their own cans and exhibit their creations in Red Bull's The Art of the Can exhibitions. Alternately, musicians can apply for the Red Bull Music Academy. The reason all these ventures are so successful? Because they deliver something of value to consumers.

> _"Counter-intuitively, people know that they love choice but find that they hate making decisions."_

### 6. In today’s media landscape, authenticity and good content are crucial for successful advertising. 

Surely you've heard this famous motto of media theory: "the medium is the message." Hmmm...does the medium really count more than the content? Well, maybe in the past. But certainly not in today's world.

Decades ago, it didn't matter what your advert said — all that mattered was how it was delivered to consumers. If you had an advertising slot on one of the few TV stations, your advertising message was likely to succeed regardless of content.

But as we mentioned, that's no longer true. Today, the digitization of media has fragmented audiences and advertisers alike. If you put your ad on television, chances are people will either fast-forward through it or not even see it, because they've decided to watch the program online instead.

And that's exactly why today the medium is far less important than the content. But then, what's the key to good content?

To start, your content has to be authentic, because if there's any dissonance between what is communicated and what is experienced, it will erode trust in your brand.

Consider Coca-Cola: if they tried to push a message about the healthiness of their product, few would believe it. And then all the happiness and warm feelings they've built around their brand for years would be undermined. _That's_ why advertising has to be authentic.

And yet — despite the growing importance of authenticity — scale still matters. You need to have an authentic message _and_ reach a lot of people in order to be a truly powerful brand.

Look at Red Bull: when Felix Baumgartner jumped 24,500 meters freefall for the brand, the stunt was broadcast on more than 40 TV stations. Plus, eight million people watched it live on YouTube. Thus, Red Bull's famous motto — "it gives you wings" — was literalized for a massive audience. It was a brilliant piece of marketing.

### 7. As long as your advertising strategy reflects the changing media landscape, the future will be bright. 

Now that we understand some of the media shifts underway, we can figure out how to develop an advertising strategy to reflect those changes.

So instead of launching huge campaigns, go for social copying instead. Even if you don't know that exact term, you've seen this phenomenon in action: whenever people yell "Wassssup" at each other, they're copying a Budweiser ad — and ultimately spreading the beer company's message far beyond the initial audience. The big advantage of social copying? It's much more authentic than a standard TV commercial.

Social media is another increasingly important aspect of contemporary advertising. And to excel on social platforms, there are only a few simple rules: Be nice, and take your time to build relationships.

Many companies find social media difficult, because they have to give up control over communications. But you trade that control for attention. And as long as you're nice and authentic, no one will have any reason to complain.

This leads to an important point: advertising in the digital world requires a fundamental shift. Businesses have to engage with their customers in a new way in the democratized online space.

Just look at what's happening with television, with the shift to social: people are increasingly commenting on shows in real-time, to enhance the viewing experience. That means customers are no longer passive couch potatoes — they are active and engaged.

Accordingly, successful advertising will probably either have to be reciprocal (solving problems for people) or imitative (creating behaviors that can be copied). And ultimately, the ideas that actually earn attention in the now infinite media space will be those that encourage consumer participation.

### 8. Final summary 

The key message:

**In today's media landscape, attention is a precious resource. That's why traditional approaches to advertising no longer work. Instead, marketers have to adjust to new consumer expectations by prioritizing communication and authenticity.**

**Suggested further reading:** ** _Facebook Marketing_** **by Greg Brooks**

_Facebook Marketing_ (2014) is a comprehensive guide of the topic, covering both the basics like creating simple ads, and advanced power-user techniques like building custom audiences.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Faris Yakob

Faris Yakob is the award-winning co-founder of Genius Steals, a global strategy and innovation consultancy. _Paid Attention_ is his first bestselling book.

  

Original: ©Faris Yakob 2015. This summary of _Paid Attention 2015_ is published by arrangement with Kogan Page.

