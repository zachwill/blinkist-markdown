---
id: 5740a2c42edd1b00032e6b57
slug: pandemic-en
published_date: 2016-05-24T00:00:00.000+00:00
author: Sonia Shah
title: Pandemic
subtitle: Tracking Contagions, from Cholera to Ebola and Beyond
main_color: 2286A5
text_color: 1D728C
---

# Pandemic

_Tracking Contagions, from Cholera to Ebola and Beyond_

**Sonia Shah**

_Pandemic_ (2016) explores the fascinating world of pathogens and diseases and how they can spread from a bat in China to five other continents in a single day. How do these diseases evolve, and how does modern society help contribute to their success? And most importantly: what can we do to stop the next pandemic?

---
### 1. What’s in it for me? Delve into the mystery of pandemics. 

At school, we all learned, with a shudder, how the Black Death ravaged Europe. Thanks to modern medicine and good hygiene, a pandemic like that could never devastate the modern world! Or could it?

Many epidemiologists believe that a global pandemic is going to hit in the near future. So what can we do? Well, to protect our own society and help others, we need to understand how outbreaks emerge and how they grow to catastrophic proportions.

And that's where these blinks come in; step by step, they trace the evolution of several major epidemics and highlight the sometimes surprising circumstances that enabled them.

You'll find out

  * how the founding of a bank contributed to a devastating cholera epidemic;

  * about a virus that spread to five continents in the course of a single day; and

  * that, in days of old, New Yorkers ingested a lot of human waste.

### 2. As humans spread across the globe, previously harmless animal pathogens adapted to our bodies and made us sick. 

Have you ever stopped to wonder, is there a place on Earth humans haven't inhabited? Over the past few centuries we've expanded to almost every region on this planet. You can even find us in inhospitable places like the wetlands and Antarctica.

Well, sometimes this grand expansion comes with serious consequences.

Take, for example, the Sundarbans, a large mangrove forest in Bangladesh and India that was left uninhabited by Mughal emperors who saw it as a dangerous and evil land. In a way, this turned out to be true, since at the time the area was swarming with cholera germs carried by tiny flea-like creatures called _copepods_.

But around 1760, the East India Company took over the area, cutting down the forest to cultivate rice. By the end of the nineteenth century, humans occupied 90 percent of the Sundarbans and were unwittingly working and bathing in water full of cholera-bearing copepods.

This massive and continuous exposure enabled the cholera bacteria to adapt to human bodies and make us their new host. Over time, the bacteria developed small "tails," allowing it to bond together, build a sticky film and colonize in our gut.

We saw this happen again in 2003, with the _severe acute respiratory syndrome_ (SARS) epidemic, when a bat virus learned to adapt to humans in a "wet market" in Guangzhou, China.

The vendors in these markets sell a wide variety of live animals, such as turtles, snakes and bats. The virus that would eventually cause SARS started out as a horseshoe bat virus, one that usually can't harm humans and most animals.

But since this market had so many wild animals penned up in such a small space, the virus got the continuous exposure it needed to adapt itself to other animals, and eventually to people.

So we know how an outbreak starts. In the next blink, we'll see how this initial contact can lead to a global epidemic.

> _"From cows, we got measles and tuberculosis; from pigs, pertussis; from ducks, influenza."_

### 3. Pathogens use our own transportation system to spread around. 

The means of human transportation have evolved rapidly, from boats to trains to planes, and we now have the luxury of crossing thousands of miles in just a few hours. Unfortunately, this is great for germs as well.

Indeed, without the help of our transportation systems, the ability of pathogens to spread would be quite limited.

Let's take another look at cholera. In its infancy, cholera wasn't even able to travel from one human to another. So, to get around, it developed a toxin that caused diarrhea, which proved to be an effective way to flush infectious bacteria out of one sick person and into the proximity of other, healthy people.

But diarrhea is not an ideal mode of transportation, even for cholera, since the disease could only travel to people living very close to one another.

Luckily for it, the nineteenth century saw the development of many new forms of transportation, including travel by sea and canals. And since the bacteria _Vibrio cholerae_ is a waterborne pathogen, this was ideal: once cholera made it into a canal system, it could spread across huge distances.

Today, air travel is our primary way of connecting the world, making it even easier for pathogens to get around.

We can see this in the 2003 outbreak of SARS. When the initial victims in Guangzhou arrived in a hospital, doctors were unsure of the cause. The physician in charge traveled to Hong Kong, where he checked into a hotel and infected 12 more people, including a flight attendant.

The flight attendant then traveled to Singapore before falling ill and checking herself into a hospital. Her physician was due to fly to New York, but he only made it to Frankfurt before succumbing to the disease.

Other infected people traveled to Vietnam, Canada and the United States. Within one day, SARS had spread to five continents.

But mass transit isn't the only way we're assisting these germs; as we'll see in the next blink, another problem is our waste-management system.

### 4. Despite advancements in human waste management, feces can still lead to dangerous outbreaks. 

Today, people in many different industries are trained about proper hygiene and sanitation techniques. But it took a long time for us to get to this relatively clean environment.

In the eighteenth and nineteenth centuries, people were surrounded by filth, creating perfect conditions for cholera epidemics.

In New York, for instance, human waste could be found in alleys and on sidewalks, and the rate of contact was so high that it is estimated the average person ingested around two teaspoons of it every day!

Adding to the problem was Manhattan's "water lots" — marsh lands along the coast that were turned into housing developments. Twice a day, rising tides would flood through the area, carrying feces and germ-infested water into streets, cellars and even leaking into wells of drinking water.

Due to a drought in 1832, the small amount of drinking water that was available became increasingly contaminated, leading to a deadly cholera epidemic.

Thankfully, western countries now have adequate systems to handle human waste. But the problem still plagues gigantic industrial farms that handle huge amounts of animal waste.

Between 1959 and 2007, the size of US hog farms increased by 2,000 percent, and chicken farms grew a staggering 30,000 percent. As you can imagine, the amount of waste produced on these farms also increased tremendously. This has resulted in manure pools, where pathogens thrive, evolve and contaminate the air, soil and water on the farm.

And even if you don't live nearby, you could still be exposed to the dangerous pathogens that form there, which, through discarded water and manure, can contaminate the produce that ends up at your local supermarket.

This is what happened in 2011, when thousands of Germans ate contaminated fenugreek sprouts from Egypt and were afflicted with bloody diarrhea caused by a unique, Shiga toxin-producing strain of the common gut bacterium E. coli, known as STEC.

Dangerous waste is still an issue, and the next blink explains how big cities also contribute to the spread of disease.

### 5. Pandemics thrive in large crowds. 

Many people are attracted to the world's large and vibrant cities. Since the beginning of the nineteenth century, New York City has been home to people from all over the world seeking work and a better life. But this hasn't always turned out so well.

Dramatic increases in the population meant people were forced to live in terrible conditions. And by 1850, the New York City slums were six times more crowded than modern-day Tokyo or Manhattan. These kinds of conditions led to two massive cholera outbreaks, one in 1832 and another in 1849.

More recently, in 2014, several major West African cities struggled to contain a disastrous Ebola outbreak. And it is no coincidence that all of these outbreaks affected crowded areas the worst.

Large crowds hold three advantages for pathogens.

First, the more people there are, the more quickly germs can spread.

Many pathogens travel through social contact, like shaking hands, so the more crowded a place is, the higher the rate of contact. Therefore, once a pathogen moves from a sparsely populated area to a crowded city, its transmission rate spikes.

Second, pathogens can survive longer in large crowds.

In densely populated cities, there are simply more people to infect, which means it takes longer for an epidemic to run its course.

For example, prior to 2014, all Ebola outbreaks had occurred in smaller cities and lasted only a few months. But, as the 2014 outbreak hit overcrowded slums in West African cities, the rate of infections continued to grow even ten months after it began.

Third, in crowds, pathogens are free to be aggressive.

Under less crowded circumstances, pathogens can't afford to be aggressive and kill their victims quickly. Otherwise, it wouldn't be able infect other people. But, since germs can spread rapidly in crowded cities, pathogens can also get more aggressive and rapidly sicken and kill the infected.

But it's not just the crowds of modern city life that benefit the spreading of disease. In the next blink, we'll find out how corrupt politics also offer an advantage.

> _"Filth is just a symptom. The real problem is crowding."_

### 6. Political deceit can enable pandemics. 

While New York City has experienced water shortages, it's never been low on corrupt politicians. And another leading cause of outbreaks is a poorly run public-health system.

In fact, the New York City cholera outbreaks in 1832 and 1849 were greatly assisted by a power-hungry senator.

In the late 1700s, Manhattan's fresh water supply was so shockingly insufficient there wasn't enough to extinguish the house fires that frequently popped up around the city.

To solve this problem, physician Dr. Joseph Browne and engineer William Weston proposed the construction of a $200,000 waterworks.

However, Republican state senator Aaron Burr thwarted their plan and set out to build a waterworks himself.

But, unfortunately, Burr didn't care about the water; what he really wanted to do was create a new bank since the banking industry was currently in the hands of his political opponents, the Federalists.

So he devised a deceptive plan. In order to prove to the state that his project would benefit the community, in addition to the bank, he would also build a water company.

His plan was approved, and Burr raised $2 million from investors. But he only invested $172,261 of it into the waterworks, saving the rest for his bank.

This resulted in terrible waterworks that supplied the city with contaminated drinking water for over 50 years, contributing to the cholera outbreaks of 1832 and 1849.

Nevertheless, Burr came out on top: In 1801, he became Vice President to Thomas Jefferson.

Even today, political deceit enables epidemics.

In 2002 and 2003 the Chinese government made the outbreak of SARS a state secret, threatening physicians and journalists with prosecution if details were released.

Eventually, a resident of Guangzhou mentioned it to an acquaintance online, and the information got out. But even then the government blocked the Word Health Organization (WHO) from investigating cases and continued to deny the outbreak.

As a result, WHO couldn't intervene and the virus spread uninterrupted.

But as we'll see in the next blink, even in the absence of corruption, medicine still has difficulty fighting outbreaks.

### 7. Time and again, medical progress has been held back by strict beliefs. 

You've probably heard of Hippocrates, the father of modern medicine, and the famous Hippocratic oath that doctors take. But despite his influence, his teachings haven't always been helpful.

In the nineteenth century, physicians commonly dismissed treatment for cholera patients because it didn't adhere to Hippocratic teachings.

At this time, a few physicians, including William Brooke O'Shaughnessy, had already found an effective way to treat cholera patients: replenishing lost fluids and minerals with an intravenous solution.

O'Shaughnessy even proved that his method worked by administering it to over 200 prison inmates suffering from cholera. Less than four percent died, far fewer than those who didn't receive the treatment.

So why did the elite medical establishment dismiss these achievements?

Because, during the 1800s, medicine was strictly based on Hippocratic teachings which stated that epidemic diseases like cholera were spread through poisonous and foul-smelling gases called "miasmas." According to this diagnosis, a simple replacement of fluid and minerals wasn't a viable solution, and O'Shaughnessy's methods were therefore ignored. Consequently, many patients died who could easily have been saved.

While we live in more enlightened times now, we still encounter problems through what is known as the _reductionist paradigm_ : reducing complex problems by trying to trace them to a singular cause.

A couple examples: cholesterol as the sole cause of heart disease; pathogens causing all infectious diseases.

It is often the case that physicians will focus on a symptom and ignore the larger context, which means they fail to look outside their area of expertise — or to a patient's environment or lifestyle — for other clues.

For instance, by the time of the 2014 Ebola outbreak, the disease had already been affecting apes for some time. But since veterinarians and physicians rarely share information, this connection wasn't discovered until it was too late.

As we'll see in the next blink, knowing how to avoid an outbreak means looking beyond the obvious.

> _"Hippocratic medicine, in other words, was a boon to_ Vibrio cholerae _."_

### 8. Fixating on foreign pathogens can distract us from other dangerous diseases at home. 

It's human nature to be frightened of exotic diseases. But it's also easy to get carried away, and miss the dangers that lurk at home.

When Ebola broke out in 2014, the whole world took notice and was terrified. Nearly two-thirds of Americans feared the epidemic would enter and spread throughout the country.

But these fears are often irrational.

Even government agencies overreacted: people returning from one of the affected African countries faced quarantines and weeks of mandatory twice-a-day health checks.

And some of these cases seem absurd. One teacher was quarantined because she went to a conference in Dallas, ten miles away from a hospital caring for an Ebola patient. And other countries such as Australia and Canada banned all travel to and from West Africa.

Of course, indifference to scary new pathogens like SARS or avian flu is a bad idea. But while we're reacting dramatically to these potential new threats, we're remarkably unconcerned with more common pathogens.

Take Lyme disease, for example. Since its origin in 1975, Lyme disease has spread across the entire United States, and, despite it's being a very harmful disease, there is no widespread fear.

Every year, almost 300,000 people are infected with Lyme disease, and even when it is properly diagnosed, it can be very difficult to treat. There are many severe and long-lasting symptoms, including suicidal thoughts, chronic pain and paralysis. And yet most people don't even care about protecting themselves against the ticks that transmit the disease.

The fact that we disregard Lyme disease makes it even more dangerous.

Experts agreed that the odds of an Ebola epidemic in the United States were quite low. But despite the facts, people feared it more than a pathogen that was already among us, one that affected more than a quarter of a million people in the United States alone.

> _"Fear is a response to the unexpected."_

### 9. Final summary 

The key message in this book:

**Civilization has brought many new pathogens with it and researchers think that a new pandemic will emerge in the decades ahead. In order to survive this we need to be aware of the origins and causes of these new pathogens. Crowds, waste management and a lack of communication between different fields of medicine are only a few factors that contribute to the spreading of new diseases.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Hot Zone_** **by Richard Preston**

_The Hot Zone_ offers an intense, often terrifying account of the real-life events surrounding one of the world's most deadly diseases, Ebola. The book explains not only how the virus spreads, but where it came from and how it may continue to play a role in the future.
---

### Sonia Shah

Sonia Shah is an author and journalist whose work has appeared in _The New York Times_, _Scientific American_ and _The Wall Street Journal_. Her TED Talk, "Three Reasons We Still Haven't Gotten Rid of Malaria," was watched by over a million people around the world. Her other books include _Crude: The Story of Oil_ and _The Body Hunters_.

