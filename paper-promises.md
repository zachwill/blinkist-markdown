---
id: 54e36d08303764000a600000
slug: paper-promises-en
published_date: 2015-02-18T00:00:00.000+00:00
author: Philip Coggan
title: Paper Promises
subtitle: Money, Debt and the New World Order
main_color: B4343F
text_color: B4343F
---

# Paper Promises

_Money, Debt and the New World Order_

**Philip Coggan**

_Paper Promises_ offers a sobering take on the nature of money, the recent global financial crisis and what our attitudes about debt will mean for future generations.

---
### 1. What’s in it for me? Learn how we accumulated tons of financial debt before the financial crisis. 

How far in the red are you? Without glancing at credit card bills, student loan statements or mortgage commitments, you wouldn't be alone if you said you had a good amount of outstanding debt.

But what happens when you don't pay back your debt, as agreed? Yep, bad things happen. You're fined, or slapped with a bad credit rating — or if things really go bad, you develop a criminal record.

Yet just like you and your friends, companies and even countries in recent years have compiled tons and tons of debt. Yet as you'll discover in the following blinks, these entities didn't sweat the details of paying those debts back.

The result? One of the biggest financial crises in history. You'll learn exactly how we got to this point and why such irresponsible borrowing behavior was allowed to happen in the first place.

In these blinks, you'll discover

  * how many zeros there are in a trillion;

  * why it's easier to buy a sheep with cash than with a loaf of bread; and

  * why you may be buried in debt, but your grandparents are not.

### 2. Bartering a loaf for a lamb was how trade was done in early times. Today, money moves markets. 

If money is just ink on paper, why is it that we worship it so? The answer rests in how we use it.

Money's primary function is as a _medium of exchange._ Before we used money, deals were settled through bartering. If you were a baker, for example, and you wanted a pair of pants or a leg of lamb, you could find a shepherd and trade. If you gave him 100 loaves of bread, he'd give you a lamb.

But what if the shepherd had no need for bread? You'd then have to find something he _did_ want, perhaps a new knife. You'd then have to find a blacksmith (who needs bread) to make the knife, which you could trade for the lamb.

Trying to find the right thing to trade could lead to some intricate, time-consuming bartering. Money cuts through this confusion: if you want something, you simply exchange it for money, which the seller can then use to buy something else. The transaction is simple, and direct.

Money also functions as a _unit of account_, or a means of measuring a product's value.

To trade an item, we need to understand its worth. Bartering makes this difficult. Could you say accurately how many lambs one knife is worth? Or does it depend on the needs and resources of the shepherd and the blacksmith?

Money makes valuation easy, as beforehand its value is agreed upon.

Finally, money can be used to _store value_, for example, by maintaining the value of a specific product.

Most of the world's countries keep the value of their money more or less stable to foster investment and prevent inflation, in which prices rise but the value of money decreases.

Understanding money's many functions is essential to understanding the way it can influence the economy and society.

### 3. Borrowers show confidence; lenders offer trust. A heady combination led to far too much lending. 

With an understanding of how money functions, we can now look at one of its major roles in modern society: the concept of _debt_.

People often lend and borrow money to and from one another based on trust. In fact, the word "credit," another way to describe debt, stems from the Latin word _credere,_ meaning "to believe."

No matter whether you're a creditor or a borrower, mutual trust and confidence in the transaction serves as the foundation of debt. The borrower needs to be confident in her own ability to pay back the loan; the creditor likewise needs to have to trust in the borrower to do so.

What's more, both borrower and creditor need to be confident that they will profit in some way from the arrangement. Student loans provide a good example. A student applying for a loan trusts that he will get a good education and a well-paying job after college, while the bank (lender) trusts that the student will finish his education and will be able to pay back the loan, _with interest_.

There has been plenty of trust between borrowers and lenders in recent decades. The result of this is that most Western countries carry debt that is at least three or four times higher than reported annual income.

How could this state of affairs have ever happened?

In the last 40 years, Western countries' economies have grown steadily, while living standards and productivity have likewise increased. As societies became more confident in their ability to earn and thus easily pay back loans, they borrowed even more from banks and other countries, which trusted the borrowers to pay the loans back and on time.

Slowly but surely, the debt that was amassed surpassed levels ever seen in the global financial system!

> _"Modern money is debt, and debt is money."_

### 4. Confidence keeps vertigo away from the debt market. Once it hits, however, everything falls apart. 

If you go to Times Square in New York City, you might notice a particular clock. This clock doesn't tell the hour but instead records the accumulation of debt by the U.S. government in real time.

When the clock was built back in 1989, it was equipped with enough digit places to display an amount in the trillions, or a one followed by 12 zeros.

By 2008, the clock needed to be updated. By then, the amount of debt the United States had accumulated had hit a total of _$10 trillion_, which required more digits than the clock could handle.

So how did the United States, as well as other countries, fall so deeply into the red?

Over time, countries racked up more and more debt, without stopping to think how the debt would be paid back. If you ask a mountain climber how she manages to climb so high without fear, she might say, "Keep looking up, never look down."

Countries essentially did the same thing. As a nation grew comfortable in its general ability to pay back debt, it would start to borrow beyond its means. Yet as the mountains of debt kept growing, countries simply stopped looking down. A nation would concern itself only with borrowing more to maintain operations, not with how to pay the debt back.

Eventually, everyone looked down, and confidence in the debt-burdened system collapsed.

Why shouldn't a climber look down? If you do, you quickly realize how far you can fall, and your confidence will crumble. This is exactly what happened with the financial system. Sitting on mountains of debt, countries suddenly looked down and were shocked at the unbelievable amount of money they owed each other.

Faced with this realization, confidence disappeared. Countries stopped borrowing, and, more crucially, stopped lending. And as a result, the financial system ground to halt and credit dried up.

> _"The massive debts accumulated over the last 40 years can't be paid in full."_

### 5. The collapse of Lehman Brothers created a domino effect that dried up lending across the globe. 

The sudden realization of the degree of indebtedness caused countries and banks across the world to panic and halt lending. But how did this actually result in a global financial crisis?

The golden years of borrowing had created a bubble in the financial system. As countries, companies and banks borrowed more and more, they created an artificial boom in the global economy.

In contrast to previous boom periods, which resulted from events such as high worker productivity or improved business practices, the boom preceding the global financial crisis was fueled by cheap credit. As long as people continued to lend to one another, the trend would only continue.

Investment banks like Lehman Brothers, for example, borrowed huge amounts from other banks. They used this debt to buy and trade other forms of debt, such as mortgage bonds, from which they profited greatly.

And as their profits increased, they saw no reason to slow their borrowing to keep things going up.

As soon as the confidence in borrowing waned and the flow of credit dried up, however, the bubble burst and the financial system collapsed.

Once everyone saw just how much debt Lehman Brothers had accumulated, no one wanted to lend to the firm anymore. Then when the U.S. government refused to keep the company solvent with a bailout, Lehman was forced to file for bankruptcy protection.

This bankruptcy created a domino effect worldwide. Banks and insurance companies that had lent money to Lehman suffered massive losses, and as a result, they too stopped lending.

And it wasn't just the financial industry that suffered. Countries, such as Greece and Spain, had funded years of growth with massive borrowing. Once the crisis hit, these countries too had no way to pay back their debts.

The financial crisis was both massive and devastating. But how much did the crisis' effects change our practices of borrowing and finance in general?

The next blink will look at what the future holds for borrowers and lenders.

### 6. While the 2008 financial crisis was a big one, there have been many debt crises throughout history. 

After the 2008 financial crisis, many people believed the end of the world was nigh.

But really, those that predicted the end times had no grasp of economic history. The truth is, large financial crises happen all the time, whether in ancient periods or modern times.

Before King Louis XIV died in 1715, France experienced a serious financial crisis as the king had accrued debts worth nearly a billion livres, the currency of the time.

France tried to lessen the crisis by printing paper money while attempting to rekindle confidence in its new banking system. The idea was to trick investors into thinking that banks were more credible than they actually were.

And just like in 2008, the French bubble quickly burst, and many people lost everything, their homes and their livelihoods, and were forced to live on the streets.

One of the most common ways to fight a debt crisis is through inflation, in which the local currency drops in value. If your currency becomes half as valuable, then you effectively wipe away half of your debt burden that is denominated in that currency.

Many economies in the past, including ancient Greek despots, took this approach. So why didn't every country hit by the financial crisis do this in 2008?

One reason is that countries like Greece, which uses the euro, can't devalue its currency independently to bolster its economy, as many other European countries use it as well.

### 7. The world is still shaking off its debt hangover. What does this mean for future generations? 

Post-crisis, it's easy to understand how people could be pessimistic. Is there any silver lining to look for in the global financial system for the future?

There is, but we have to deal with the consequences of changing global demographics.

Researchers estimate that the world's population will grow by 50 percent within the first half of the twenty-first century. This growth will occur mostly in the developing world, while birth rates in developed countries will fall.

This unbalanced growth will inevitably lead to problems. For one, developing countries' economies will struggle under the weight of a massive population. At the same time, the low birth rates of developed countries will lead to a decline in productivity, due to a shrinking population.

To ensure the future economic health of the globe, we need to work on these and other problems.

Economically struggling countries, like Greece for example, not only need support but also must change social assumptions. The country requires fundamental reforms to fix the root of economic woes, rather than relying on further borrowing to fund growth.

By working to foster real growth — by improving infrastructure, fighting corruption and increasing productivity — Greeks could actually develop a strong economy for the long term. Growth would be based on a stable foundation, and thus unlikely to collapse.

The world is in dire need of a change in direction if we are to manage the problems caused by our debt-ridden way of life!

### 8. Grandpa kept cold cash under his mattress, while we collect credit cards and run up huge debts. 

Most young people today carry some sort of debt, in the form of an overdrawn checking account, a housing loan or even a weighty student loan.

The ease with which we accumulate debt is a relatively new development. Our great-grandparents, survivors of the Great Depression in the United States, avoided banks and would have rather been seen eating garbage than borrowing money. Even Baby Boomers still fear debt as bad or unusual.

Indeed, the way that societies and cultures have dealt with financial matters has changed.

If you owed money, it was usually seen as the mark of having lived an extravagant or wasteful life, of living beyond your means. Most people simply didn't need credit, as they could rent a home and work at the same company their entire life for a steady income.

But the advent of lowered interest rates, entrepreneurial activity and a decline in traditional working practices made people more willing to take on debt and build a business, finance an education or own a home outright.

Whereas older generations feared debt, the younger generations see it as an accepted way of life, not something to be ashamed about.

Yet this accumulation of debt, above and beyond normal levels, not only can cause problems between generations but also lead to problems for us all in the future.

### 9. Final summary 

The key message in this book:

**Wanton borrowing and overconfidence in financial systems led to a massive, worldwide financial crisis. Even in the face of this crisis, there is still no shortage of debt, for individuals and for governments.**

**Suggested further reading:** ** _House of Debt_** **by Atif Mian and Amir Sufi**

When we bailed out the banks during the Great Recession, we didn't actually address the real factors that caused the economic downturn. The actual problem lay with excessive mortgage lending to those who couldn't afford it, which led to heavy debts and, eventually, huge collapses in consumer spending. To avoid the consequences of this boom-and-bust cycle in the future, the authors propose new ways of restructuring debt and stimulating the economy.
---

### Philip Coggan

Philip Coggan is a columnist at _The Economist_ and has written a number of books, including _The Money Machine_ and _The Economist Guide to Hedge Funds_. For 20 years he has also worked at _The Financial Times_, most recently as investment editor.

