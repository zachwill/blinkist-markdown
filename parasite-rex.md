---
id: 5406d3343933310008080000
slug: parasite-rex-en
published_date: 2014-09-02T00:00:00.000+00:00
author: Carl Zimmer
title: Parasite Rex
subtitle: Inside the Bizarre World of Nature's Most Dangerous Creatures
main_color: DB2E41
text_color: C2293A
---

# Parasite Rex

_Inside the Bizarre World of Nature's Most Dangerous Creatures_

**Carl Zimmer**

_Parasite_ _Rex_ offers an up-close-and-personal look at the fascinating and often misunderstood world of parasites. By introducing you to some of the great discoveries in parasitology, you'll discover that parasites aren't only important parts of our delicate ecosystem but also responsible for our own evolutionary complexity.

---
### 1. What’s in it for me? Learn why parasites deserve our sympathy and admiration. 

It's an unfortunate truth that we humans reserve a particular level of disgust for parasites. To be considered a parasite is to be counted among the lowest creatures possible: blood-sucking mooches who get a free ride off the hard work off their hosts.

But this view of parasites is as irrational as it is simplistic, and for a long time even actively hindered our scientific understanding of parasites. In fact, it's in your best interest to come to terms with the existence of parasites, since you very likely have some living inside you right now!

If anything, rather than turning our noses up at parasites, we should be _thankful_ for the way they balance our delicate ecosystems and all the wonderful diversity they've brought to our species.

These blinks will give you an intimate look into the fascinating world of parasites, from their stages of life and metamorphoses to the strategies they use in order to survive on our hostile planet.

In these blinks, you'll find out

  * how some parasites turn their hosts into zombies;

  * why many parasites castrate their hosts; and

  * why you should thank a parasite for its role in the evolution of sexual intercourse.

### 2. Parasites often pass through many life cycles before they reach their final form. 

Almost every animal on the planet will one day play host to a parasite. And yet, despite their ubiquity, parasites remained a mystery for scientists for thousands of years.

Back in the days of the ancient Greeks, parasites were thought to have generated spontaneously within and by the bodies they are inhabiting. It seemed like the most plausible explanation at the time since parasites had only ever been observed within the bodies of their hosts, never actually as they entered the body.

The ancient Greeks' erroneous beliefs about parasites were due primarily to a lack of information: the life cycles of parasites are highly complicated and unlike anything humans knew, which made them more elusive.

This all changed in the 1830s, when Johann Steenstrup began studying the mystery of flukes: scientists knew that flukes laid eggs, but nobody had seen a baby fluke in its host.

In his experiments, Steenstrup showed that the fully grown, leaf-shaped flukes found in the livers of sheep (or another host) were actually the final stage of a single animal's complex life cycle.

Streenstrup observed that the eggs laid by adult flukes within their hosts actually escaped the hosts' bodies and later hatched in water. Once hatched, they appeared to be covered by fine hairs and swam around in the water until they penetrated a snail.

Once inside the snail, these parasites transformed yet again into something Streenstrup thought resembled a shapeless bag, swollen with embryos of yet more flukes.

These embryos, called the King's yellow worms, then transform yet again into missile-tailed _cercariae_, i.e., swimming parasite larvae. The cercariae then look for another host — another snail or a vertebrate host like a sheep — inside of which they finally develop into mature flukes.

Armed with the knowledge of these strange transformations, scientists could finally discard the notion that parasites generated spontaneously.

### 3. Despite what some scientists believe, parasites are highly complex creatures. 

The thought of parasites fills many of us with absolute disgust. In fact, for a long time, even scientists considered them too lowly to study.

This idea was epitomized in 1879, when the zoologist Ray Lankester claimed that parasites seemed to shun evolution by developing backwards.

Lankester looked at organic life as a Christmas tree-shaped hierarchy, with us glorious human beings — the most developed of the species — at the very top. The branches below represent the development of other species.

Lankester's diagram also included drooping branches to represent species that appeared to have climbed down the tree, i.e., _degenerated_. He believed that degenerated life forms, like parasites, might have been more complex in the past, but became less developed as time went on.

His favorite example was _Sacculina_ _carcini_, a parasite that, when outside the host, appears to be a barnacle. However, once inside a suitable host, the parasite "degenerates," losing its legs, tails, and even its mouth!

Lankester's view on parasites led to an underestimation of their complexity, thus sparking an opinion of parasites that hindered research by scientists such as Michael Sukhdeo.

Sukhdeo dedicated decades of his life to studying the mechanisms parasites use to navigate within their hosts' bodies, and was particularly intrigued by the question of how they're able to reach the organs where they finally make their homes and lay their eggs.

He came to the conclusion that a parasite's movement — as is the case among animals in general — depends on _programmed_ _behavior_, i.e., genetically inherited stimulus-response reactions, even more so than that of free-living creatures.

In spite of his groundbreaking work, Sukhdeo had to endure the disrespect of researchers who were more interested in animal behavior, focusing on vertebrates because parasites seemed to be too primitive to have behavior at all.

Yet, while he might not have been around to enjoy it, Sukhdeo had the last laugh: over time parasites have adapted clever and complex ways to fool their hosts' immune systems, survive and thrive.

So now that science appreciates parasites as a complex life form worthy of study, the following blinks will show what we've learned about how they survive and thrive.

> _"Every living thing has at least one parasite that lives inside it or on it."_

### 4. Parasites have developed complex ways to interact with their hosts’ immune systems. 

Just like animals fight off invaders in their territory, our bodies' immune systems defend us against invading organisms, such as bacteria, viruses and parasites. And sometimes, these invaders fight back.

In fact, each and every parasite has its own strategy for overcoming the immune system of its hosts.

Consider, for example, _Toxoplasma_. Although its _final_ _hosts_ — i.e., the organisms that house the parasite in its final developmental stage — are cats, it uses both cat prey and humans as pit stops along the way.

Cats are quite picky, preferring to eat only the freshest of dead mice, and since cats are the parasites' final destination, they have to keep their hosts alive long enough for cats to kill and eat them.

To complicate matters further, Toxoplasma doesn't have the power to determine which _intermediate_ _host_ — i.e., the organism that houses the parasite before it develops into its final shape in another host — will swallow its eggs.

It might be a cat that eats a mouse that's hosting parasites; _or_ it could be a human who, likely accidentally, has ingested small amounts of cat feces that contain parasite eggs.

Either way, Toxoplasma is programmed to not kill mice, and thus won't kill _any_ hosts in order to keep them attractive prey for cats.

Consequently, Toxoplasma wants the intermediate host's immune systems to function. However, to the host's body, they are invaders and should thus be eliminated by the host's immune system.

Although it seems paradoxical, Toxoplasma releases a molecule within its hosts that actually triggers the production of immune system T-cells. In doing so, the parasites help keep the host's immune system aggressive against germs and other invaders, but this also poses a danger to Toxoplasma.

In order to survive, Toxoplasma will hide itself inside of its hosts cysts and thus out of harm's way.

> _"Parasites make up the majority of species on Earth. According to one estimate, parasites may outnumber free-living species four to one."_

### 5. Parasites can use genetically engineered viruses as weapons. 

If you've ever had a cold, then you've lived through a viral invasion that overcame your body's natural defenses. Sometimes, if a virus is resilient enough, it can be a truly terrifying experience. Indeed, some viruses, such as smallpox, are so deadly that their application in biological weapons has frightened us for decades.

But humans aren't the only ones who have considered turning to nature to find ammunition. Parasites do, too.

Perhaps the best example for a parasite's use of biological weapons can be found in the mosquito-sized wasp called _Cotesia_ _congregata_, which uses the tobacco hornworm — a bug that resembles a fat green caterpillar with black boots on its feet — as a suitable place to raise its offspring.

When it's time to lay her eggs, the mother wasp finds a nice, fat hornworm and injects a soupy mix into its body. This soup, among other things, contains both her eggs as well as viruses, which are produced from the genetic material of the female wasp.

Without these viruses, the eggs would be destroyed by the hornworm's natural immune defenses. However, thanks to their mother's skills in biological warfare, the viruses within the soup destroy the immune system of the host, thus allowing her eggs to grow within the hornworm's body in relative peace.

The hornworm, however, is not left entirely defenseless. After a while, its body develops antibodies against the viruses and thus recovers after a few days from the infection. Unfortunately for the hornworm, the wasp larvae have grown large enough by that time to defend themselves against the hornworm's immune system.

Eventually, unable to fight the growing larvae within its body, the hornworm stops eating and dies.

### 6. Some parasites can take complete control over their hosts. 

If you're a science fiction fan, then you've definitely seen some film or another where a mysterious force, perhaps a virus or an alien being, takes control of the mind of an unsuspecting human, which it uses to its own foul end.

But this isn't just the stuff of science fiction: parasites, too, can actively influence their host's behavior for the sake of their own parasitic interests.

Remember the parasitic wasp from the previous blink? It not only infects hornworms with viruses that neutralize its immune system but also takes control over its behavior and metabolism.

As wasp larvae develop within the hornworm, the hornworm grows up to twice its normal size, and this has everything to do with how the larvae influence their host.

When a hornworm eats something, like a leaf, the wasp larvae living inside it change how the hornworm digests that leaf. Usually, the hornworm would convert the leaf primarily into a storable form of energy, like fat. But when it hosts wasps, it converts the leaf into sugar instead, as it's a quicker source of energy that the parasitic wasps can use for immediate growth.

Moreover, these wasps also change their host's physiology. Male hornworms are born with large testes. But once infected by a parasitic wasp, the testes will shrink and eventually disappear! Why? Because the hornworm's sex organs don't help the wasp's eggs to develop, and — at least from the parasite's perspective — that energy could be used more efficiently.

While it might seem horrifying, castration is a common practice in the parasite world. In fact, many species of parasites developed such strategies independent of one another. For example, sacculina castrate crabs and flukes castrate the snails they invade.

Once taken over by parasites, hosts lose all control of how their energy is used, becoming little more than zombies serving their parasitic masters.

These highly complex and slightly scary creatures sometimes have a huge influence on their hosts. As you'll see in the following blinks, their influence even extends far beyond that.

> _"Parasites can castrate their hosts and then take over their minds."_

### 7. Entire ecosystems are influenced by the behavior of parasites. 

When most people think of an ecosystem, they imagine a natural community of predator and prey creating a harmonious circle of life. They don't often think of parasites. And yet, the role of the parasite in its environment cannot be denied: they can even shape entire ecosystems!

Take the parasite _Euhaplorchis_ _californiensis_, for example. Euhaplorchis eggs are released in bird droppings, which are then consumed by horn snails. These eggs then hatch inside the snails, where they grow into flukes and castrate the snail.

Afterwards, they multiply and eventually transform into missile-tailed cercariae. Then, the cercariae swim out of the snail and into the California killifish.

Once inside, they work their way towards its brain, forming a thin layer on top of it where they patiently wait for a shorebird to come and eat the killifish.

The shorebird, then, becomes their final host.

With all these different interspecies interactions, the Euhaplorchis becomes a major player within its salt marsh ecosystem, as ecologist Kevin Lafferty discovered.

For starters, the castrated and infertile snails aren't killed by the flukes. In fact, they continue to live and eat algae in order to feed the parasitic flukes within them, and as such become competition for the uninfected snails in their ecosystem.

Lafferty determined that without the fluke parasite, the snails would double in population. The increased population would mean less algae, and snail predators, like crabs, would thrive.

Furthermore, the shorebird population also seems to depend on the parasites in order to catch their favorite prey — the killifish.

By pumping out powerful signals, the parasites cause the killifish behave in strange ways that make it easier for the birds to catch them. In fact, experiments have shown that the birds were _thirty_ _times_ more likely to attack a killifish infested with parasites, which cause the fish to flail or become paralyzed.

Just imagine how difficult it would be to survive if it were thirty times harder to find a meal!

### 8. Parasites may have pushed the course of evolution. 

While it's undeniably impressive that parasites are able to have an impact on entire ecosystems, that pales in comparison to the ways in which they've shaped our entire evolutionary development.

It appears that parasites' attacks on their hosts led to the evolution of the very first immune systems. However, whenever a host develops a new protection against parasites, parasites evolve methods to evade it.

We can see an example of this in a study conducted by A. R. Kraaijeveld involving two kinds of fruit flies and the parasitic wasps that affect them.

Both kinds of fruit flies developed immune system responses to the wasps. However, wasps that attacked one kind of fruit fly were able to respond to its new defenses because their ancestors had experience with the changes in the fly's defense system.

However, the new defenses developed by the other kind of fly were too unfamiliar to the wasps, which were thus unable to find a way to circumvent them.

Under these conditions, Kraaijeveld proved that, over time, hosts develop heritable strategies against the parasites that prey upon them.

Although the claim that parasites _caused_ the development of immune systems is just a hypothesis, Kraaijeveld's experiment provides a foundation to explain how our immune system came to be: hosts and parasites evolved alongside one another, each trying to develop new ways to thwart the other.

Sometimes, however, this kind of evolution doesn't actually lead anywhere. Although their strategies change, the relationship between parasite and host remains the same: either attack or evade.

Biologists call this sort of evolution _The_ _Red_ _Queen_ _hypothesis_, named after Lewis Carroll's _Through_ _the_ _Looking_ _Glass_. In the book, the Red Queen takes Alice on a long run that leads to nowhere, telling her: "Now, _here_, you see, it takes all the running _you_ can do, to keep in the same place."

> _"In this new kind of evolution there is no progress forward or backward."_

### 9. Parasites triggered the development of sex. 

Although the theory of evolution can account for quite a bit of the natural world, it has a hard time accounting for _sex_, i.e., reproduction that depends on copulation between two different variations within the species. Bacteria and plants, for example, don't have sex, but mammals do. So why on Earth did species evolve to reproduce through sex and not stick to good, old fashioned cloning?

As it turns out, parasites might hold the answer to that question.

In the early 1980s, William Hamilton developed the theory that parasites were responsible for the existence of sex, believing that hosts found that sex was a better survival strategy than cloning when it came to fighting parasites.

Cloning makes a parasite's life quite easy: all it has to do is develop _one_ strategy to overcome its host's defenses, and it's set for generations to come.

Unlike cloning, however, sexual reproduction shuffles the genetic material of the parents, half from father and half from mother. The more variance in genetic material, the harder it is for a parasite to find the right plan of attack to pass it on to the next generation.

The question of sex intrigued another scientist named Curtis Lively, who was able to prove Hamilton's theory.

In order to study the circumstances that caused the evolution of sex, Lively needed to find a species that reproduced both with and without sex.

That species ended up being a snail in New Zealand called _Potamopyrgus_ _antipodarum_. The majority of its population are identical clones with no distinct sexes, though a fraction of them are divided into male and female snails that reproduce via sex.

Lively discovered that the snails that reproduced sexually lived in habitats with more parasites than the asexual ones. In other words, the more parasites, the higher the evolutionary pressure for sex since the shuffling of genes acts as a defense mechanism.

And so, it seems that parasites deserve quite a bit of our thanks: for building our immune systems, balancing our ecosystems, and fostering the need for sexual intercourse.

> _"Parasites have been a dominant force, perhaps the dominant force, in the evolution of life."_

### 10. Final summary 

The key message in this book:

**Parasites** **aren't** **the** **lowly** **creatures** **they're** **often** **made** **out** **to** **be.** **While** **they** **might** **be** **disgusting** **in** **our** **eyes,** **their** **amazing** **adaptability** **has** **led** **to** **some** **of** **the** **most** **important** **evolutionary** **advancements** **in** **the** **animal** **kingdom,** **from** **the** **development** **of** **immune** **systems** **to** **sexual** **reproduction.**

Actionable advice:

**Good** **hygiene** **is** **key** **to** **staying** **parasite-free.**

Parasites are everywhere. In fact, you probably have some living in you even as you read this. However, that doesn't mean you should abandon your attempts to keep yourself as parasite-free as possible. The best way to protect yourself from parasites is through good hygiene practices, so wash yourself often and stay away from the pig pen!

**Suggested** **further** **reading:** **_Spillover_** **by** **David** **Quammen**

_Spillover_ takes a look at where the world's most deadly diseases come from, explaining how humanity is at risk from viruses and bacteria hiding in animal populations. It also shows that the closer we get to the natural habitats of wild animals, the greater our risk of coming face to face with deadly foes: pathogens.
---

### Carl Zimmer

Carl Zimmer is an author and columnist whose writings focus on the frontiers of biology. He has received many awards for his work in science writing and science journalism, and makes regular appearances as a speaker at universities, museums and festivals.

