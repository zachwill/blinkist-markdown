---
id: 53eb58763634330007120100
slug: partners-and-rivals-en
published_date: 2014-08-12T00:00:00.000+00:00
author: Wendy Dobson
title: Partners and Rivals
subtitle: The Uneasy Future of China's Relationship with the United States
main_color: EE3830
text_color: D4322A
---

# Partners and Rivals

_The Uneasy Future of China's Relationship with the United States_

**Wendy Dobson**

In _Partners_ _and_ _Rivals,_ Dobson lays bare the relationship between the two biggest powers in global politics: the United States and China. She describes the consequences of China's meteoric rise to power, and the inevitable tensions it has created. But she also offers advice that both the United States and China would do well to follow — for the good of the whole world.

---
### 1. What’s in it for me? Learn more about the most important international relationship in the world. 

To many people in the West, China is still a cultural mystery, lying in a gray zone between socialism and capitalism. Its economic success and political aspirations frighten some and impress others, but one thing is sure: China is now a major world player, and is bidding to become top dog.

But despite its meteoric rise to power, China is still suffering from growing pains. If you look behind the curtain of it's economy, you'll find out that there are still a number of domestic issues that need to be taken care of before China can fully assume its global leadership position. These are not only remainders of a centrally planned economy, but also institutional challenges from China's rising civil society.

In this book you'll learn more about the two global superpowers, and how their relationship can potentially bring the world closer together — or tear it apart.

In these blinks, you'll discover:

  * how some small rocky islands can threaten world peace,

  * why China desperately needs more babies, and

  * how the United States and China could improve their relationship — if they have enough good will.

### 2. Over the last few decades, China has become a new global superpower. 

When the financial crisis hit the world in 2008, whole countries were plunged into despair and frustration — but not China. In fact, the collapse of the developed economies allowed China to rise to new heights of economic and political power.

And now, China is about to become the world's most powerful economy.

The 2000s were China's decade. During that period, the United States, Japan and Germany all went through slumps, their annual growth rate seldom rising above two percent, while China's economy continuously grew at a staggering rate of ten percent.

Even before the financial crisis, specialists were predicting that China would overtake the United States as the world's biggest economy by 2041. But due to the crumbling financial markets and debt explosion of the 2008 collapse, it is now believed that China will overtake the United States by 2026.

China is also taking a frontline position in world trade. Its $2 trillion trade with its top six trading partners takes up eight percent of total international trade. And here begins our US–China saga: the United States is the country's most important market by far, with as much as 18 percent of all US imports coming from China.

Now China is turning this economic weight into political influence.

In response to the financial crisis, the entire global community came together to seek solutions. With that purpose in mind, the Western-dominated G7 was replaced by the G20 summit of the biggest 20 economies. This enabled China, which had previously been sidelined, to take a leading role in directing global economic policy. And recently China has increased their influence over the International Monetary Fund (IMF) — one of the most important financial institutions in the world — currently holding the third highest voting rights in the organization.

> _"Singapore's former prime minister Lee Kuan Yew sees China as the biggest player in history."_

### 3. Although China has grown to a position of dominance, it needs to reform its economy if it wants to stay there. 

China cannot allow itself to celebrate its powerful position for too long. Its quick rise to power has covered up structural problems that must be addressed if its economy is to keep growing.

One problem is that Chinese policy makers have overlooked the creation of a strong domestic market.

China's success has been built on producing extremely cheap products for world export. Consequently, wages are kept low to keep the economy competitive — so despite the good health of its industry, the people of China have seen little of its wealth.

This situation is dangerous because if global demand drops, the Chinese people's low wages will mean that the domestic demand might not be able to make up for the loss — and the economy would stop growing. Therefore, Chinese policy makers should help increase the common worker's disposable income. This could be done by increasing investment in state education or providing free health care.

Beyond creating a strong domestic market, if China wants to keep its position of power, it will also need to boost the productivity of its economy.

Why?

Because despite their corporations' recent successes, old practices are keeping things back.

For example, Chinese State-Owned Enterprises (SOEs) have become surprisingly successful and globally competitive — think for instance of the world's fifth largest revenue maker, oil giant SINOPEC. However, SOEs are also the vehicle of large-scale corruption and cronyism: In fact, many failing SOEs are being kept alive by subsidies so their profits can be siphoned off by politicians — subsidies that would be better spent elsewhere in the economy.

Furthermore, China's financial sector needs to become more flexible if it is to properly serve the country's economy. Currently, it is too hard for companies to trade stocks and acquire fresh capital. And banks are mainly supporting larger or state-owned companies, which hinders the growth of innovative small and medium-sized enterprises.

### 4. China’s one child policy and its resistance to urbanization are threatening long-term growth. 

In most countries we are free to have as large a family as we desire — but not in China.

In fact, since 1979, China has combated overpopulation with a _one_ _child_ _policy_. This means that if a Chinese family has a second child, they suffer negative social consequences like risking job promotions, and jeopardizing their children's education.

This outdated policy is now threatening China's growth by limiting the economy's competitiveness.

One of the prime reasons why China had such low labor costs in the past was its abundance of young workers, which would constantly keep wages down. But as a result of the old one child policy, for the last few years this flow of workers has declined, putting pressure on wages.

Furthermore, the United Nations has predicted that in the next 20 years China's productive population will decrease from 68 percent to 61 percent, which will push wages even higher. Simultaneously, the percentage of elderly dependent people will rise to over 25 percent.

However, solutions exist to fix these issues.

In fact, policy makers have already begun to phase out the one child policy, which should produce more available labor. But if other forces make labor scarcer anyway, it'll need to be used more efficiently. One way of doing this is to promote _urbanization_.

Currently, only 50 percent of China's population is urban, far less than similarly large and ageing economies like Japan and Germany. One major reason for this slow urbanization is the _hukou_ system, a relic of command economy times. This system allows only licensed farm workers to migrate to the cities. As a consequence, unlicensed migrant workers are forced to live on the margins of urban societies, which subjects them to many social problems such as isolation and poor housing. But if these people were free to move to the cities, they would find work more easily — and help push up the country's falling productivity.

> _"China will grow old before it is rich."_

### 5. To prevent the country from falling into crisis, China requires serious institutional reforms. 

The Chinese government has done everything it can to ensure its people don't learn about the country's problems — especially the mass corruption and growing inequality. But despite their best efforts, Chinese citizens are starting to discover the country is not as socialist as it claims.

In fact, there is growing unrest in large parts of China, both on- and offline.

On social networks and popular micro-blogging pages, China's massive internet audience — the country now has more than _twice_ as many internet users as the United States — are finding out what the government isn't telling them, and they're growing more and more angry. On average, there is a social network-based protest every three minutes in China. And outside the virtual world, protests are especially strong in the countryside. They are mostly targeted against local governments who take land away from farmers to sell to investors.

So how do politicians respond? Either by using police violence, or by offering citizens compensatory payments — which have been increasingly used in recent years. In fact, the public budget for buying off demonstrators' anger — the _wei_ _wen_ — actually exceeded military spending in 2011!

If China wants to restore stability, it needs to reform its justice system.

One problem is deeply entrenched: throughout history, the Chinese paradigm of the relationship between the ruler and the law has been that the ruler created legislation, but would stand above it. This attitude came to a peak during the communist period after World War II, and although it's improving now, China's elites still feel they are above the law.

But things are starting to change: the new leader of the Communist Party, Xi Jinping, has recently responded to the growing unrest about inequality before the law. As a consequence, the Chinese media has recently broadcast the trials of corrupt high-level party members.

> _"Citizens have become increasingly able and willing to challenge the state."_

### 6. China has taken an ambivalent role in the international political system. 

Being a world power doesn't just mean being a large-scale trader — it also means taking on the responsibility of shaping decision-making in the world of international governance. But so far, China has been slow on the uptake.

In fact, China has preferred to stand back from international politics.

Generally speaking, China does show interest in international cooperation. But during the financial crisis, the country was quickly overwhelmed by the rising expectations that it was going to help bail the world out. As a consequence, China has done more observation than direct action. For example, with the exception of a small governance reform of the IMF, China hasn't pushed for a large-scale restructuring of institutions to better reflect the growing importance of Asian economies.

This reluctance to participate in international politics might be caused by China's highly critical view of the existing governance system.

Traditionally, China does not believe in higher level global institutions or international law that constrains Chinese interests. It takes an independent and nationalistic approach to international politics and favors case-by-case decisions over inflexible laws. In particular, the concept of individual human rights conflicts with the core Chinese concept of placing the collective before the individual.

Finally, China's performance in Asia shows that they are still new to international politics.

Rather than getting involved in the global scene, China has expressed ambitions to become a regional leader in Asia. There have also been multiple attempts to create a free-trade zone in East Asia, together with Japan and South Korea. Yet all these aspirations have failed because of the re-emergence of old national issues. One major issue is the border conflict in the South China Sea: China and Japan disagree over the ownership of a few rocky islands, which embody the struggle for dominance in the region.

In the following blinks, you'll learn more about the future of US–China relations.

### 7. Current US–China perceptions are dangerous for global peace. 

Anyone who has heard shouts of "USA, USA" at a sports event will know that many Americans believe their nation to be the greatest and most powerful in the world. But this view is definitely not shared in China.

Beyond mere nationalism, this is because the Chinese know themselves to be a rising power.

The leadership in Beijing has made it clear to all Chinese people that their recent success means China's time has come. This is particularly significant because the Chinese have traditionally taken on a victim mentality, stemming from their humiliation in the nineteenth century when the old kingdom was defeated and exploited by the West.

Now China's rise is bringing up old aspirations for respect once more. Many analysts believe that the nation's aggressive territorial claims in the South China Sea over rocky islands and sea borders relate to this emotional longing for greatness.

But this new pride has many Chinese worried: they fear the United States will be a bad loser.

Many Chinese people are convinced they will soon take over the United States' status as the world's largest power — and they don't expect the United States to give up hegemony that easily. For example, they took President Obama's 2011 speech in Australia about reorienting US security policy towards the Pacific extremely seriously, and see it as a first sign of hostility.

China's rise to power has not only got the Chinese worried: to many Americans, China also appears unpredictable and very dangerous. Furthermore, the United States has traditionally felt their democratic and open society is morally superior to autocratic states like China. Accordingly, Americans are struggling to face China at an equal level, and respect its ambitions.

The first step to cooperation is understanding and respect. But it currently appears that neither China, nor the United States, are prepared to take the first step.

> _"For many Americans, a fundamental divergence of interests is inescapable and détente impossible."_

### 8. The United States and China need to take major steps toward creating a cooperative relationship. 

In the US presidential race of 2012, Republican candidate Mitt Romney took advantage of a highly popular topic: US fear of China. His rhetoric of interdependence, trade, and cyber warfare demonstrated how high tensions had already risen. But if this tension is allowed to keep escalating, it will only lead to disaster.

Instead, for the good of the whole planet, China and the United States have to increase mutual trust.

One place to start is by ensuring that the current expansion of the Chinese military doesn't escalate into a Cold War-style arms race. The countries could avoid this by making their security goals transparent, and discussing their common and conflicting interests. They could even actively practice cooperation by performing joint military manoeuvres.

Another crucial element for furthering cooperation is the deepening of high level relations.

A few lower-level diplomats getting friendly around a nice meal won't build much trust. What is needed is for Xi Jinping and Barack Obama to regularly meet, tackle serious problems behind closed doors, and present an atmosphere of cooperation to the world — only then can real progress be made.

One topic they could discuss is deeper and broader interdependence.

In fact, many optimistic people believe that this interdependence is already large enough to prevent a military conflict: they believe a war is unthinkable due to the _entanglement_ of both economies. But to ensure peace, this entanglement should be strengthened by continuously promoting mutual direct investment, and increasing trade relationships.

One way to achieve this would be to rethink the the Trans-Pacific Partnership (TPP) — the international plans to establish a US-dominated free-trade zone in the Pacific. Up until now, China has been kept out of the negotiation process — which leaves wide open a great opportunity to invite China into the conversation, and strengthen US–China ties.

> _"The US leadership is more positive toward the relationship than the Chinese choose to recognize."_

### 9. The two superpowers should work together to solve major global and regional issues. 

Can you think of one major world problem that could be fixed by any single country? Climate change maybe? Unrealistic. Or terrorism? Problematic. If we ever want to solve international issues, cooperation is the necessary key to success. And this is especially true for superpowers like the United States and China.

For example, a peaceful East Asia depends on US–China cooperation.

In fact, international stability depends on the United States and China establishing a peaceful situation in Asia. They can play a key role in defusing the ongoing tensions between China, Japan and other regional players that are a potential threat to world peace.

So what can be done?

The United States, instead of digging trenches, could work to bridge divides between the major powers in East Asia. It has strong links with China, Japan, South Korea and even India, and could try to bring all the rivals together to discuss regional solutions. But there's a problem: China still prefers to sit down and negotiate with every partner face to face, using its direct leverage to make them adhere to Chinese demands. And if there's going to be serious change, both the United States and China have to get involved.

The United States and China are also the key players in the arena of global issues.

The United States and China are currently the top two emitters of CO2 — which means that if they can't agree on a common way of dealing with climate change, global warming will win the battle. They could better tackle the problem together if they aligned under a cooperative global leadership.

Another major global issue that the United States and China are deeply involved in is international cyber security. While both countries are actively using cyber espionage, they also have a shared interest in an international convention to regulate cyber conflict and mitigate its effects — especially in regard to rogue non-state hackers and cyber terrorists threatening both countries.

### 10. Final summary 

The key message in this book:

**The** **rise** **of** **China** **to** **the** **position** **of** **top** **global** **superpower** **is** **inevitable.** **The** **central** **question** **now** **is** **whether** **the** **United** **States** **and** **China** **will** **be** **able** **to** **navigate** **the** **troubled** **waters** **of** **a** **new** **international** **hierarchy.** **By** **increasing** **their** **interdependence,** **and** **banding** **together** **as** **global** **leaders** **to** **deal** **with** **international** **issues,** **there** **could** **still** **be** **hope** **for** **future** **US–China** **relations.**

**Suggested** **further** **reading:** **_The_** **_Great_** **_Degeneration_**

The Western world seems to be in crisis. It is faced with huge levels of public and private debt, and the economies of the rest of the world are fast catching up. After 500 years of total global dominance, the era of Western powers could be coming to an end.

_The_ _Great_ _Degeneration_ by Niall Ferguson aims to tackle why this is the case. It suggests that a decline in Western institutions is partly to blame. Only by arresting this decline through radical reform can the West recover.
---

### Wendy Dobson

Wendy Dobson is an acclaimed Canadian economist and co-director of the Institute for International Business at the University of Toronto. She has served as Canadian Associate Deputy Minister of Finance, and has published multiple books on Asia and the international economy.

