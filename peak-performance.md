---
id: 5a3000fbb238e10006672dda
slug: peak-performance-en
published_date: 2017-12-15T00:00:00.000+00:00
author: Brad Stulberg and Steve Magness
title: Peak Performance
subtitle: Elevate Your Game, Avoid Burnout, And Thrive With The New Science of Success
main_color: FDCF33
text_color: 7D6619
---

# Peak Performance

_Elevate Your Game, Avoid Burnout, And Thrive With The New Science of Success_

**Brad Stulberg and Steve Magness**

_Peak Performance_ (2017) employs success stories, case studies and various examples of athletes, artists and intellectuals to give you a crash course in performance. These blinks explain why performance became so integral to society in the first place and how you can be your best.

---
### 1. What’s in it for me? Become the best you can. 

Olympic athletes, master violinists and top CEOs all have at least one thing in common: they're at the peak of their performance. And in a world where competition for jobs or advancement gets fiercer by the minute, peak performance is more crucial than ever.

But how do you get there?

In these blinks, we look at why optimum performance is more important today than ever before and how you can find the formula for the balance that will get you to the pinnacle of performance.

In these blinks, you'll find out

  * that taking a day off from the gym for rest is more important than we might think;

  * why you should stop multitasking right now; and

  * which routines will get you on your way to peak performance.

### 2. Technology has globalized the job market and ramped up competition. 

Just a few decades ago, getting a job was a piece of cake. As long as you stood out from the few people in your geographical area who had also applied for the desired job, you had a good chance of landing it. But today, that's clearly not the case.

At this point, the job market is akin to a full-blown world war. After all, technological innovations have made it possible to do a number of jobs from anywhere on the globe. As a result, the number of people vying for a limited number of jobs has increased, which has made the competition fiercer than ever.

To make matters even more difficult, an unprecedented number of people are committing themselves to breaking world records these days, a fact that makes standing out from the pack that much harder. For instance, in 1954, when the British athlete Sir Roger Bannister ran a mile in less than four minutes, lots of people thought his accomplishment represented the outer limits of human performance. But today, more than twenty Americans break that four-minute barrier annually.

And finally, computers, robots and other forms of artificial intelligence are putting the squeeze on the job market. Just take the e-commerce and cloud-computing company Amazon, which is making human employees redundant by using technology. Since the firm operates entirely online, there's no need for cashiers or salespeople; Amazon doesn't even need to pay rent on, or invest in, storefronts.

The rise of this online monolith has naturally resulted in the bankruptcy of some brick-and-mortar competitors, like the bookseller Borders, which previously employed over 35,000 people.

To take things to an even higher level, Amazon is now looking into using drones to deliver its products, which may eliminate the need for human workers altogether.

The point is, machines are getting smarter every day and coming for an ever-greater percentage of jobs.

### 3. Impossibly high standards are driving people to take performance-enhancing drugs, to ill effect. 

What would you do to get a leg up at work? Well, for many people, intense competition is leading to some desperate measures. In fact, performance-enhancing drugs have become integral to academic, athletic and even professional life.

For instance, researchers estimate that 30 percent of students take the "study drug" Adderall, a toned down version of the street drug known as "speed." Adderall is used to treat attention deficit hyperactivity disorder, or ADHD. But according to the Center for Disease Control, just five to six percent of the population actually suffers from this condition. Most students take this drug to increase their attention span, concentration and focus.

Because of these benefits, Kimberly Dennis, an MD and the medical director of a substance abuse center, has seen a marked increase in Adderall use among professionals between the ages of 25 and 45. One person from this age bracket named Elizabeth is the founder of a health-technology company. She told the _New York Times_ that she began taking Adderall because working hard wasn't enough and the need for sleep was preventing her from putting in longer hours.

But it's not only students and professionals who take such drugs. Around 40 percent of top athletes use banned drugs to increase their performance — and only about two percent of them get caught.

Such a performance-at-any-cost way of thinking is, however, unsustainable. In the end, it just results in burnout.

For example, a 2014 survey of over 25,000 companies across 90 nations found that the biggest challenge in these firms was overwhelmed employees. This issue has gotten so bad that 21-year-old Moritz Erhardt, an intern at the Bank of America, died of an epileptic seizure brought on by a lack of sleep after working for 72 hours straight.

What's worse is that the people responsible for providing health care to such people are also suffering. Studies show that 57 percent of medical residents and 46 percent of physicians meet the criteria for burnout.

### 4. The secret to sustainable success is balancing stress and rest. 

We've all heard the classic saying "work hard, play hard." But maybe it's time to add "rest well" to this adage. After all, giving your body and mind time to rest and recover makes them stronger, which enables you to put in more effort in the future.

Just take Deena Kastor, a collegiate runner. She had never won a major race and so, hell-bent on achieving one, she went to train with the legendary coach Joe Vigil. Under his tutelage, she far surpassed her prior levels of success. When asked how she'd done it, she said the major impact was _between_ training sessions — not the training itself.

More specifically, Kastor gets 10-12 hours of sleep every night, plans her diet out in great detail and receives a weekly massage and stretching session. Such a rigorous recovery program has enabled incredible growth.

So why is rest so crucial?

Well, one reason is that suppressing or resisting needs, rest included, is a stressful mental task that makes all others much more difficult. For instance, in the mid-1990s, the social psychologist Roy Baumeister became interested in why people feel tired after approaching a complex problem and why humans run out of willpower in general.

To study the question, he got 67 adults together in a room that smelled of freshly baked chocolate chip cookies. Then, the cookies themselves were brought into the room. Half of the people in the room were allowed to eat the cookies, while the other half were told to eat radishes instead. Naturally, for this second group, resisting the cookies was difficult.

Then, after both groups had eaten, they were asked to solve a problem that appeared solvable but which was in fact impossible. The radish eaters gave it 19 attempts and gave up in eight minutes; meanwhile, the cookie eaters tried 33 times, committing over 20 minutes to the task. Simply put, the radish eaters had worn out their mental muscles resisting the cookies, while the cookie eaters were still full of energy.

### 5. Stress can stimulate growth and adaptation, but a positive mindset is also key. 

In 1934, the endocrinologist Hans Selye did an experiment in which he injected ovarian extracts into rats. The rats reacted strongly, and he believed that he had discovered a new sex hormone. But he soon realized that he was wrong. In reality, the rats reacted exactly the same way to _any_ injection. The experiment just shows that anything that shocks or causes pain and discomfort can trigger stress.

Interestingly enough, however, Selye observed that the rats adapted to the stressors over time. In fact, stress can actually be positive and spur growth.

A great example is exercise. When you work out a muscle — say, by lifting weights — it causes micro tears in the muscle fibers, triggering a stress response. In effect, the body becomes aware that it's not strong enough to lift such a weight, and so it transitions into an anabolic stage, building up the muscle to withstand greater stress.

And this doesn't just apply to muscles. Pushing the brain to new heights can also build up its response to stress. For example, in one study, students who were forced to struggle through complex problems without any help outperformed those who received immediate assistance.

And managing stress is often merely a matter of how you think about it. If you see it as a positive experience, you'll be much more able to withstand it.

Just take a 2010 study that found that Americans who view stress as "facilitative" have a 43-percent lower chance of dying prematurely.

It could be assumed that the only reason such a group views stress favorably is because they rarely experience it. However, when researchers compared the total number of stressful events for this group with those of people who viewed stress negatively, they found that they were nearly identical.

The only other explanation is that the attitudes people hold toward stress can determine how it impacts them and, therefore, the length of their lives.

### 6. Multitasking is inefficient and it’s best to focus on one task at a time. 

Do you love multitasking and consider it the most efficient way to get things done? Well, if you do, it's time to change your ways.

That's because countless studies have found that both the quality and quantity of work suffers when people do multiple things at once. Just take research from the University of Michigan that found that multitasking can consume up to 40 percent of your productive time. Other research has found that multitaskers are worse at filtering out information, are slower to identify patterns and have diminished long-term memory.

A better approach is to do one thing at a time. For example, in the 1990s, the psychologist K. Anders Ericsson began researching how people become experts. To do so, he went to Berlin, Germany, to study violinists at the prestigious Global Music Academy.

Ericsson asked each violinist to write a diary of their practice. He then studied the diaries and found that every one of them practiced for 50 hours a week. However, each of them used that time differently. The people who went on to become international soloists spent more time than the lower performers trying to master one specific goal, and they achieved greater focus as a result.

Or consider Dr. Bob Kocher, a successful venture capitalist in health care who also works as a health care economist and Stanford professor. How has he accomplished so much?

He compartmentalizes. In other words, he divides tasks into categories and gives each activity or goal his singular focus, a practice that can clearly be seen in his daily interactions. The moment you enter a room with Dr. Bob it's clear that he's fully with you. He's entirely undistracted by his email, his phone or anything else. He gives you the same amount of attention as he would the president of the United States, or to any other task or person.

### 7. Rest is important for peak performance, but not all types of rest are equal. 

Have you ever wondered why so many people have their best ideas while walking, showering or sleeping?

It's in large part because such restful times allow the brain to step out of the linear thought process. After all, when you're actively working, your conscious mind reigns supreme. It functions in a linear "if-then" kind of way. If this, then that. The issue is, you often fail to solve problems when you get stuck and begin obsessing about them.

On the other hand, when you stop trying, your subconscious mind kicks in. This allows your brain to pull random information out of storage, information that was otherwise inaccessible. This is where creativity is born.

But rest is also essential to peak performance. Let's go back to the prior story of that British athlete, Roger Bannister. In 1954, when he set out to run the four-minute mile, he took a new approach. He didn't push himself up until the last minute before the race. Instead, two weeks before the race, he abandoned training altogether, and set out for the mountains to hike. Compared to his normal routine of constant running, this was more rest than he'd had in a long time. When the big day finally did arrive, he finished the mile in record time.

So rest is key, but some forms are better than others. For instance, even the shortest walk can have major benefits as long as you take it in the right locale.

This idea is supported by a Stanford study in which participants were asked to take a walk outdoors, indoors or not at all. They were then tasked with coming up with creative uses for a number of common items. Those who'd walked outdoors experienced the most obvious increases in creativity, but even those who'd walked indoors had 40 percent more ideas than those who'd opted to stay put.

### 8. Great performers abide by a few simple tricks. 

Whether it's a writer getting ready to type out a story or an athlete preparing for a race, a great performer will never just hope to be at the top of her game. Instead, she'll actively cultivate an environment that will help her do her best.

That's why having a solid routine is paramount. Just take Matt Billingslea, the drummer for pop star Taylor Swift. Before every sold-out show, he takes 30 minutes to hop rhythmically from side to side. He then does some exercises, making wide circles with his arms and activating his core and back muscles. Finally, he shifts his focus to his mind, taking deep breaths and visualizing each move while getting into the zone. This routine helps Billingslea stay focused and perform at a top level, despite the pressure of playing in front of hundreds of thousands of people.

But a routine isn't the only way you can reach peak performance. Setting the right priorities can also help. Just take Michael Joyner, a physician and researcher at the Mayo Clinic. He's published 350 articles on human performance and has a few performance secrets of his own.

First, by living close to his workplace he saves time on his commute. Second, he pre-packs his gym bag so he doesn't waste time picking out workout clothes. And finally, he reserves his energy and willpower for activities that add value, such as a difficult work task that requires complete focus.

So it's important to set yourself up by establishing the proper routines and priorities.

Finally, there's one more trick to peak performance: your mood. For instance, an experiment at Northwestern University gave participants a questionnaire designed to assess their emotional state. The participants were then split into two groups based on their mood. Those with a positive perspective were much more likely to solve challenging intellectual problems.

To figure out why, the researchers used an MRI machine to watch the brains of the participants as they solved the problems. They discovered that those with a positive outlook had increased activity in a brain region known as the _anterior cingulate cortex_, which handles decision-making, emotional control and problem-solving.

### 9. Having a purpose can help you transcend self-imposed limitations. 

It's every kid's dream to be a superhero with incredible strength and dazzling powers. And, as it turns out, the realization of such a dream might not only belong to science fiction.

That's because fatigue and the physical limits of your body actually only exist in your head. In other words, you're capable of actions that go beyond your normal abilities.

Just consider Tom Boyle, an ordinary American. He and his wife once saw an 18-year-old boy ride his bike head-on into a Chevy Camaro on a suburban road in Tucson, Arizona. They sped to the scene and Tom immediately set to work lifting the front end of the car with his bare hands, freeing the boy's legs in the process. When the boy still couldn't get clear of the car due to his injury, Boyle screamed to the driver to drag the boy free.

In other words, Boyle broke the Olympic deadlift record even though there was nothing physically extraordinary about him. He was just a simple paint-shop supervisor whose drive to save a young boy gave him superhuman strength.

What's more amazing is that you can draw on such an ability regularly by finding a purpose that transcends yourself.

For example, University of Michigan public-health professor Victor Strecher had a daughter named Julia. Julia suffered from heart trouble and eventually required two transplants. Every time his daughter's health suffered, Strecher was reminded of the value of life and was driven to make the most of his time with her. When, on her nineteenth birthday, she passed away, Strecher entered the bleakest period in his life. This deep melancholy finally broke on Father's Day when he hallucinated his deceased daughter telling him to move forward.

This inspired him to shift his research to understanding the power of purpose, a decision that led to his becoming one of the best teachers in his field. To what does he attribute his success? He says he sees his daughter's face in every student and teaches them as if they were Julia.

And if that's not a prime example of the power of purpose, it'd be hard to say what is.

### 10. Final summary 

The key message in this book:

**Improving your performance isn't a simple matter of working as hard as you can. In fact, to truly be as productive as possible, and avoid burning out in the process, you've got to take care of yourself, focus on one task at a time, identify your purpose and know what truly compels you.**

Actionable advice:

**Help yourself by helping others.**

Assisting other people activates the reward and pleasure centers of your brain. In other words, lending someone a hand will not only make you feel better; it will also help you associate positive emotions with this act of generosity. So try giving back — and see how it positively impacts your energy and motivation.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _High Performance Habits_** **by Brendon Burchard**

_High Performance Habits_ (2017) explores the six habits that can turn an ordinary person into an extraordinarily productive one. Performance coach Brendon Burchard draws on the data and statistics from one of the largest studies of the world's most productive people ever conducted to explore their habits and find out what makes them tick.
---

### Brad Stulberg and Steve Magness

Brad Stulberg and Steve Magness are both experts on human performance.

Stulberg worked at McKinsey and Company and has coached some of the top business executives in the world.

Magness holds a Master's degree in Exercise Science and has coached Olympic athletes.

Together, their writing has been featured in publications such as the _BBC_, the _New Yorker,_ the _Wall Street Journal_ and _NPR._

