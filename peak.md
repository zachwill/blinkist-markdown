---
id: 56af46e6377c160007000011
slug: peak-en
published_date: 2016-02-01T00:00:00.000+00:00
author: Chip Conley
title: Peak
subtitle: How Great Companies Get Their Mojo From Maslow
main_color: F2A630
text_color: 8C601C
---

# Peak

_How Great Companies Get Their Mojo From Maslow_

**Chip Conley**

_Peak_ (2007) reveals how hotelier Chip Conley became inspired by a book on psychology and applied it to his own successful business philosophy. These blinks show us how we can find lasting success and happiness by shifting away from fixating on profit and instead focusing on creating positive relationships with customers and investors.

---
### 1. What’s in it for me? Learn why being a relationship-oriented person will make your business prosper. 

What's the right way to do business? If you're an entrepreneur or starting your own business, you've probably asked yourself that question at some point.

Surely, there are many (many!) ways to go about it. But it is important to know: What needs and values — beyond the basic ones for the survival of your business — must you address to make your customers happy? And to make your employees and investors happy enough that they want to work hard for the business?

The author sets out to answer that exact question. Based on Abraham Maslow's celebrated Hierarchy of Needs and the author's own experience with running a successful hotel business, these blinks will guide you through the steps necessary to make your business thrive by emphasizing relationships.

In these blinks, you'll discover

  * why the author believes that relationships are the most important currency in both life and business;

  * why meeting your customers' unrecognized needs will make them advocates for your business; and

  * why you should consider all your investors philanthropists.

### 2. During the 2001 economic crisis, the author discovered a new way of conducting business. 

Have you ever found yourself downhearted and unable to see the light at the end of the tunnel? Sometimes all it takes to pull yourself out of a time of crisis is a good book. This is exactly what happened to the author, successful hotelier Chip Conley.

Like many businesses, the author's chain of twenty boutique hotels in the San Francisco Bay Area suffered badly during the recession in 2001. 

The impact of the bursting dot-com bubble hit this region especially hard due to the many internet startups based in the area. Many businesses lost customers and money, Conley's hotels among them.

One day, struggling to cope with the grim state of his business, Conley left the office to take a walk around town. He soon found himself in a bookstore, browsing the psychology section and recognizing a book he vaguely recalled from his days in college.

That book was _Toward a Psychology of Being_ by Abraham Maslow, a classic in human psychology. The book focuses on self-actualization and finding _peak experiences_, or those high points of bliss in everyday life and work.

The author immersed himself in the book, reading for several hours, soon realizing what he had to do: reconnect with the dream that inspired him to start his business in the first place. 

He would return to the initial idea of creating a business where his customers, employees, investors, and even he himself, would find joy and fulfillment.

### 3. Abraham Maslow’s psychology points out that success is not only a matter of money. 

What do you hope to get out of life? Some people will gravitate toward money and material things. Others toward less tangible things like loving relationships, creative and pleasurable work, or making the world a better place.

Abraham Maslow is a psychologist who proposes that there is more to life than simply survival and wealth. To illustrate this, he created a _Hierarchy of Needs Pyramid_.

The pyramid puts basic human needs, such as food, water and shelter, at its foundation. Once these are met, new needs like social connection and appreciation emerge until we reach the top of the pyramid. Here, Maslow places the need for _self-actualization_, i.e., reaching our full potential and finding deep meaning in our life and work.

Companies have a long history of ignoring higher needs like social connection, as it is intangible, i.e., it can't be physically measured like financial profits. But recently a shift has been taking place and society is becoming more aware of the importance of intangible needs. 

Take the South Asian country of Bhutan, where the king has decided to measure the prosperity of his country by looking at its Gross National Happiness index rather than the Gross Domestic Product.

This same shift is beginning to happen in the business world as well, where we can see companies looking beyond the basic needs of customers and employees.

For example, the intangible value of customer loyalty is now being sought after by companies as a way to measure new business through word-of-mouth recommendations. 

And the needs of employees have also become important, as can be seen at outdoor apparel company Patagonia, which is shifting away from strict employee schedules and timetables for everyday work. As long as they make sure their work gets done, employees are free to enjoy breaks and outdoor activities on their own schedule.

### 4. According to Maslow, relationships are the most important currency in both life and business. 

Have you ever wondered why some business managers seem less cheerful and happy than the people who work underneath them? One theory suggests that they might be lacking the benefit of a positive relationship with their customers.

When you look at long-term statistics, you see that good relationships lead to a successful business.

Economist Fred Reichheld explains in his book, _The Loyalty Effect_, that with a sustained increase in customer loyalty of just 5 percent, company profits can increase by as much as 25 to 95 percent. This goes to show that it is worth the extra effort to focus on customer satisfaction over generating short-term profits by cutting costs or compromising on quality.

Strong customer loyalty can even be better than money in the bank when it comes to helping a business through tough times.

For instance, following the 2001 economic crisis, the author was able to take advantage of the customer loyalty his hotels had built up over the years. His business sent out letters to their most frequent guests asking for support and to recommend their services to friends and family.

The same long-terms benefits of good relationships apply to investors as well.

Businessman John Bogle explains the importance of creating good relationships with investors in his book _The Battle for the Soul of Capitalism_. It shows that, when investors' only motivation is to make profits, they usually only hold on to their stock in a company for about a year. But if an investor is motivated by a personal relationship and shared values, the average holding time increases to six years.

So if you want customers and investors to stand behind your business through good times and bad, make sure you develop strong personal relationships.

### 5. Happy employees are good employees. 

How many people do you know who say they love to go to work? Chances are, not many. After all, workplaces tend to be sterile, noisy and unpleasant. 

But some companies are starting to realize that it doesn't have to be this way. Have you ever wondered what it would be like to work at a place you didn't want to leave?

Companies like Google and Pixar are finding success by creating a new and exciting workplace with good food, fun events and nice parties. And this works out well because, at the end of the day, a big paycheck isn't as great a motivator as you might think.

Sadly, most companies still look to pay raises as the most effective way to motivate and hold onto employees. But studies have shown that focusing on money isn't the best way to go. Underpaid employees will remain unmotivated and employees that are already well paid don't become any happier when they receive more.

What employees do want is to feel appreciated.

This is reflected in the book _Managing with Carrots — Using Recognition to Attract and Retain the Best People_ by Adrian Gostick and Chester Elton. It quotes a study revealing that, when companies put an employee recognition and appreciation strategy in place, they are twice as successful in generating revenue than those without such a strategy.

Another way of motivating employees is to help them see and experience the good that comes from their hard work.

The author makes this clear to his employees by inviting them to stay at one of his hotels, free of charge, four times a year. This gesture itself is a terrific perk. But, more importantly, it gives the employees a first-hand experience of the service they are providing their customers.

### 6. Satisfied customers are loyal customers. 

So how do you go about creating customer loyalty? Ask yourself: Why do I keep going back to my favorite store? 

It often comes down to good customer service. We tend to return to places where we feel welcome and supported in finding what we need.

Therefore, it stands to reason that, if you can figure out how to satisfy your customers' expectations, they'll be more likely to remain loyal.

The author's Joie de Vivre hotel chain improved its chances at customer satisfaction by creating an online matchmaking tool. Customers answer a few questions and the service matches them to the hotel that best suits their personality and needs.

This shows that when you can find out the specific desires of your customers, you'll be better able to satisfy them. And there's been a growing trend in customers wishing to be seen as individuals with unique tastes that need to be met. 

Take the successful optometry store, Paris Miki. The customer provides a word, like "retro" or "quirky," and a computer system uses the specific shape of the customer's face to recommend the best frames and lenses.

To go the extra mile in customer satisfaction, you can consider fulfilling the deeper desires of a client, even if it doesn't necessarily fall into your business model. By tapping into your customer's subconscious desires, you can provide an even more memorable and enriching experience.

At the San Francisco restaurant Café Gratitude, the waiter, after presenting customers with the daily specials, performs the odd ritual of asking them thought-provoking questions such as, "Which important person in your life would you like to acknowledge today?" Or "What are you grateful for today?"

By showing a willingness to share more profound and human ideals with its customers, the restaurant also satisfies their desire for a unique experience.

### 7. Involved investors become long-time investors. 

An investment partnership can be like dating. For the relationship to be more than just a one-night stand you should make sure to find out what your investor is looking for.

And finding out what they want should start at the business level.

Don't assume every investor is a philanthropist. Nearly every investor wants to make a profit. And while some will have good intentions and a willingness to invest in a good cause, others will simply be in it for the money. It's important to figure out their expectations so you can act accordingly. 

The author, for example, sends out an annual survey to investors in an effort to make sure he knows what they're thinking and where their priorities are.

Again, developing a strong personal relationship with investors is crucial for establishing trust. 

During the 2001 financial crisis, the author turned to his investors for help. And due to their relationship, he was able to reach out to them using humor despite the grim situation. 

Along with the annual financial report, the author sent out T-shirts to them that read, "I bought a hotel in San Francisco and all I got was this lousy T-shirt!"

Furthermore, history shows us that investors will have your back and be more loyal if they believe in what your company stands for.

This attitude dates back centuries to when members of religious orders would refuse to invest in slave trading or companies that dealt in alcohol or tobacco. Some investors really care where they put their money, and we still see it today with mainstream banks supporting investment opportunities in humanitarian and eco-friendly endeavors.

### 8. The Final summary 

The key message in this book:

**As in life, the beauty of business is building heartfelt relationships with all the members of a company. A good company should give its employees, customers and investors the opportunity to not only fulfill their basic needs but also to feel appreciated, that they belong and that they can make the world a better place.**

Actionable advice:

**Be a dreammaker.**

Whether you're an employee looking to find more enjoyment in your work, a boss wanting to create a better workplace or a dissatisfied customer wondering how to find the service you need, take the time to sit down and dream. What would your ideal world look like? Perhaps you can find ways to make those dreams come true. 

**Suggested further reading:** ** _The Rebel Rules_** **by Chip Conley**

In _The_ _Rebel_ _Rules_, Chip Conley gives us the lowdown on success stories of entrepreneurial rebels. Conley makes sense of the seeming oxymoron of the rebel businessman, who embodies two apparent opposites — business savvy and refusal to play by the rules. He argues that business rebels, i.e., people who connect with their inner passions and dare to use their vision and instinct, aren't only more successful in business but happier in general.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

### 1. What’s in it for me? Discover how outstanding performers practice correctly to achieve greatness. 

Do you ever feel a pang of envy when you watch world-class musicians or Olympic athletes perform? Do you think it's unfair that a higher power, or fate or simply genetics endowed these people — and not you — with such exceptional talent?

These blinks will show you that after decades of scientific research, we now know that no one is born a genius, or blessed with supernatural talent. Instead, talented people work hard to develop the skills they make seem second nature, dedicating endless hours to practicing correctly.

So how do you need to approach practicing to become a top performer? And what part does your brain play in this process? Well, let's get into it.

In these blinks, you'll learn

  * how an "ordinary" guy learned to memorize 82 digits;

  * why being a cab driver changes your brain's structure; and

  * why Austrian composer Wolfgang Amadeus Mozart wasn't a genius.

### 2. With diligent practice, everyone can develop specialized skills from a young age. 

It is said that Austrian composer Wolfgang Amadeus Mozart could identify any musical note, regardless on which instrument the note was played. But few people know how he was able to do this.

Have you ever wondered how Mozart achieved such tremendous musical ability, or how some people memorize thousands of digits in pi as if doing so was as easy as knowing your phone number?

We used to think such genius was the product of innate talent or a special, spiritual gift. But in reality, anyone can acquire highly specialized skills. It just takes practice.

Consider perfect pitch, the ability that Mozart had to identify any musical note without a known tone for reference. This skill is rare; only one in 10,000 people can do it. Perfect pitch is considered an example of an _innate ability_. Yet a recent study found that having perfect pitch isn't innate at all.

In 2014, Japanese psychologist Ayako Sakakibara set out to teach 24 children between the ages of two and six how to identify the 14 different chords on a piano. Several times a day, every day for months, she taught the children chords.

As the children progressed, Sakakibara tested them on individual notes. At the end of the experiment, all the children could correctly identify notes when played. In other words, they had _acquired_ perfect pitch.

It stands to reason that with the right training, any person can learn perfect pitch, too.

This would require diligent instruction and practice, however, starting at the age of six. The point is, under favorable conditions, perfect pitch is something that almost anyone can master.

Humans can develop highly specific skills regardless of the skill in question, be it music or otherwise, because of the way the brain responds to practicing. You'll learn how in the next blink.

### 3. London’s taxi drivers literally have “bigger” brains than you do, a product of diligent training. 

London is a vast, sprawling city with thousands of streets that intersect at the oddest of angles. With a population of more than 8.5 million, the city is also home to hundreds of restaurants, housing estates, shopping malls, office buildings and all sorts of establishments catering to its many inhabitants.

And if you want to be a taxi driver in London, you've got to know your way around this maze!

Sounds daunting, right? While some tasks — like memorizing all of London's streets — might seem impossible, humans can learn many difficult skills given the brain's ability to adapt.

Just as your body grows stronger when you lift weights, your brain is "plastic," meaning it changes when you train it.

Neuroscientist Eleanor Maguire at the University College London performed an experiment to test this idea in 2000. As part of her work, she compared the brains of London taxi drivers with those of non-drivers.

Maguire found that cab drivers had a larger _posterior hippocampus_, the part of the brain that helps you navigate space and memorize locations. Also, drivers who had been in the trade the longest had the largest hippocampi.

To be sure the data she gathered was scientifically relevant — that is, that cab drivers didn't just start with large hippocampi, which helped them excel at the job — she scanned the brains of two groups of people. Her two groups were people about to start taxi driver training, and ordinary non-drivers as a control.

At the start, she found no difference in hippocampi size between the two groups. Four years later, however, when she performed the test again, she found that the hippocampi of the now-trained taxi drivers were larger than those of her control subjects.

This study implies a connection between navigation skills and the size of the brain's posterior hippocampus. When trained, this part of the brain can clearly grow, becoming capable of executing tasks it couldn't handle before deliberate training.

So now we know that practice can change the human brain. But does practice affect thinking or the way we perceive situations?

### 4. The images stored in long-term memory help people excel at a variety of complicated tasks. 

When you read the words "Mona Lisa," you probably think of the famous painting of a mysteriously smiling woman by Leonardo da Vinci. When this happens, your brain "sees" the picture.

This is called a _mental representation_. Mental representations are structures that are stored in the brain that correlate to particular objects, images, movements or anything that brain might have processed and stored for later retrieval.

In a broad sense, mental representations allow you to sidestep your short-term memory, an ability that can be quite useful.

Short-term memory is great for certain things, like remembering the words you just read to understand this sentence. But to memorize an entire language, you need the power and capacity of your long-term memory.

Simply put, without the ability to store all the rules of a language in your long-term memory, you wouldn't be able to communicate with the ease that you do.

Here's where mental representations come into play. These well-organized patterns of information, stored in long-term memory, allow you to respond quickly to the situation in which you find yourself.

For example, these representations remind you that the Mona Lisa is a famous painting, which is why you don't need to constantly relearn the significance of the painting every time you come across a reference to it.

Mental representations also play a role as a performance booster. We know it takes years of practice to become an expert in a field, whether driving a taxi or playing chess. Only through practice can you develop detailed representations of the situations or movements that matter to your performance.

Let's consider the sport of baseball. Plenty of fans have a handful of mental representations stored away, but they're nothing compared to those of professional baseball players. Because of all the practicing they do, professional players have developed sophisticated representations of all the potential trajectories of a baseball.

This means that when a batter receives a pitch, he can predict within a split second how fast it will approach, whether it's a curveball or slider, and know exactly how to swing his bat.

In this way, practice really does make perfect. But not any type of practice will do. Next, you'll learn which type of practicing is required to develop specialized skills.

> _"What sets expert performers apart from everyone else is the quality and quantity of their mental representations."_

### 5. Becoming a skilled performer means practicing purposefully, with set goals and constant feedback. 

Do you think that a professional musician, athlete, scientist or entrepreneur developed their skills and talents overnight? It's unlikely. Instead, they probably used _purposeful practice_ to achieve success.

This technique pushes you to build skills through several mechanisms, including setting clear, specific goals, being focused, leaving your comfort zone and receiving constant feedback.

To better understand how purposeful practice is achieved, consider an experiment by the author in the 1970s. A Carnegie Mellon University undergraduate named Steve was asked to memorize an ever-expanding string of numbers as the string was read out loud to him in one-second intervals.

Steve did not already have any particular facility with numbers or memorizing. In general, most people can memorize around seven digits without much trouble, and initially, Steve proved no different.

But with lots of training and practice, Steve could memorize chains of numbers that were 82 digits long.

How did he achieve this? First, Steve had a clear and specific goal: to memorize more numbers than he could previously. Second, throughout the experiment, he remained focused.

Steve also was pushed outside his comfort zone, as he was constantly encouraged to move past his current level of performance. If he managed to memorize 28 numbers in one session, for example, the trainer would start the new session by having him recite another string of the same length.

Then immediately afterward, the trainer would ask Steve to memorize a string of 29 digits — a higher performance bar.

And finally, Steve also received feedback on his performance, as the trainer would always inform him how he had done.

This last mechanism is crucial. After all, how can you improve if there's no one to tell you how you're doing? Regardless whether you're practicing sonatas or German grammar, it's essential to know if you're learning it correctly.

Through purposeful practice, you can learn all manner of specialized skills. But purposeful practice is just a step toward a greater goal, which you'll learn about in the next blink.

### 6. Informed practice guided by expert knowledge separates good performances from stellar ones. 

Now we know that purposeful practice is essential to becoming a great performer. But what sets apart great performers from people who seem to touch genius?

They achieve this through _deliberate practice_, or purposeful practice that's informed. For your practice to become deliberate, two things need to happen.

First, the practice must be applied to a field that's well-developed, meaning that there are already more experienced practitioners in the world whose level of performance clearly differs from those who are just starting out. Second, deliberate practice requires a teacher or coach who can train a student using the practice activities necessary to improve.

The musical tradition has developed over centuries, and obviously, there are differences in the quality of musical performances. Also, it's not uncommon to find piano teachers who are expert performers in their own right. These musicians have developed sophisticated training techniques that help their students acquire the necessary skills to become experts themselves.

In essence, deliberate practice must not only be based on a field with established experts but also involve guidance on how the student can become an expert. Thus you can take advantage of the specific techniques that teachers have used to achieve excellence to find excellence in your practice.

By adopting a teacher's knowledge, a student is given a template to follow, eliminating the need to start from scratch or waste time figuring out basic facts common to the field.

For instance, let's say you want to become a star high jumper. With a great coach, you won't have to figure out things like the best place to start your jump, or how to achieve maximum distance.

But let's pause for a moment. If nearly any skill can be learned through intentionally guided practice, what does that say about the concept of talent?

> _"Deliberate practice is purposeful practice that knows where it is going and how to get there."_

### 7. Contrary to public opinion, deliberate practice and not talent is the key to becoming extraordinary. 

Now you know that highly specialized skills can be developed through diligent practice. But aren't some abilities the result of sheer genius or a sort of talent bestowed by a higher power?

Many people tend to think this is the case, believing that outstanding performers owe their skills to innate talent, especially in cases where landmark performances are inexplicably accomplished early in a performer's life.

What other explanation could there be for such unusual, extraordinary abilities?

Many of Mozart's biographers, for example, have pointed to the fact that he began composing at the age of six, and that he wrote a symphony just two years later. Innate talent seems like a convenient catch-all to explain how a small child could accomplish so much in such a short time.

Yet there's no evidence that innate talent exists. Rather, the incredible abilities of top performers, even a genius like Mozart, seem to be the result of deliberate practice.

In fact, Mozart might not have accomplished that much when he was young. Evidence now suggests that the first compositions ascribed to him when he was eight years old match the handwriting of his father, Leopold Mozart, a composer who trained his son from a young age.

While Mozart went on to become a legendary musical figure, he didn't write any significant musical pieces until his teenage years, after nearly a decade of honing his skills through deliberate practice.

The same holds for many other performers. Talented people don't take shortcuts. They've all practiced deliberately for years, building their brains' capacities by creating advanced mental representations.

### 8. Final summary 

The key message in this book:

**Innate talent just doesn't play much of a role in performance. Instead, the key to performing your best in whichever field you choose is to practice deliberately. By devoting yourself to a methodological training schedule, you can master almost any skill.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Unlimited Power_** **by Anthony Robbins**

_Unlimited Power_ (1989) is a powerful, useful guide to overcoming fear, uncertainty and the feelings of unworthiness that can plague your life. With a few mental and physical exercises to help generate positive thoughts and improve body language, you can achieve the goals in life that truly matter to you.
---

### Chip Conley

Chip Conley established Joie de Vivre Hospitality in 1987, when he was just 26 years old. It has since grown into a prize-winning boutique hotel company. He is also a popular author and public speaker. His previous books include _The Rebel Rules_ and _Marketing That Matters_.

©Chip Conley: Peak copyright 2007, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

