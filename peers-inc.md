---
id: 5918c9c5b238e100066a4e7f
slug: peers-inc-en
published_date: 2017-05-17T00:00:00.000+00:00
author: Robin Chase
title: Peers Inc
subtitle: How People and Platforms Are Inventing the Collaborative Economy and Reinventing Capitalism
main_color: 2A6B35
text_color: 2A6B35
---

# Peers Inc

_How People and Platforms Are Inventing the Collaborative Economy and Reinventing Capitalism_

**Robin Chase**

_Peers Inc_ (2015) provides an insider's look at how the modern sharing economy is changing the way companies and consumers do business. It also explains how this economy may be a cure for the planet's many ills, from rising temperatures to dwindling resources.

---
### 1. What’s in it for me? Sneak a peek at the new collaborative economy of peers. 

Have you ever used Uber or Airbnb? Even if you haven't, you've probably heard of them. These companies — platforms for peers who want to trade services with each other — have taken the world by storm, and engendered no small amount of controversy. These platforms belong to what is sometimes called the collaborative economy, where horizontal networks bypass traditional top-down supply-and-demand economics.

After years of simply buying the things we want, we have reached an impasse where many of us own more than we need and the planet's ecology is crumbling under the pressure of consumption and the production needed for that consumption. Here we will take a look at the many different platforms for peers and how they can be used to combat these global challenges.

In these blinks, you'll find out

  * how excess capacity is the foundation for a collaborative economy;

  * why the people you open your platform to make all the difference; and

  * how strict governmental regulations can encourage the emergence of peer platforms.

### 2. A successful sharing platform makes it easy to share resources. 

You're probably familiar with the expression "sharing is caring," right? Well, it's not just a lesson for preschool kids anymore; today, it's valid for the modern business economy too.

The concepts of sharing and collaborating are the backbone of a successful new business model that relies on _excess capacity_.

Excess capacity refers to anything owned that isn't being used all the time, or to its full capacity. This might be a car, a second apartment or even tools and appliances that are just sitting in storage.

When looked at critically, owning is an inefficient concept. It comes with costs and hassles, and the phenomenon of everyone owning one of everything is pushing the planet's resources to the limit. Clearly, it's time to find a better way.

The benefits of a sharing economy can perhaps best be seen by looking at car ownership. You have to pay for parking, insurance and maintenance. Plus, every car adds to the traffic jams and toxic emissions that already plague most major cities.

So why own a car when you can share it? This is the question that the author, Robin Chase, asked in 1999. Chase had realized that people didn't use their cars to their full capacity and launched Zipcar to help them share this resource.

But having an excess capacity of something is only the root of a good collaborative model. There also needs to be a platform to make sharing easy and efficient.

After all, people borrowed cars from friends long before Zipcar was around. But coordinating that exchange wasn't always easy. Zipcar's platform offers a simple way to find out when every nearby car is available.

In conclusion, a successful sharing platform will do two things: identify the excess capacity and provide the necessary hardware and software to make it easy to share that resource. In this way, the business becomes a valuable service that does something that would take individuals a lot of time and money to do themselves.

> _"In all cases, leveraging excess capacity comes at a far lower cost than buying raw material."_

### 3. A successful sharing platform needs to roll out gradually to avoid pitfalls and react to harmful trends. 

You may have heard about the 16-year-old girl who accidentally invited every Facebook user to her birthday party with over 1,000 showing up. You don't want to make this kind of mistake by inviting everyone to participate on your platform on day one when you're not prepared to deal with the masses.

Platforms need to be organized, standardized and simple to use, and finding the perfect balance often happens through trial and error. To make this process as painless as possible, while still maintaining a certain level of quality, you need to slowly expand the number of users over time.

This is how companies find out what changes need to be made to offer the best possible experience.

For instance, it took some time before eBay realized the benefit of using ratings and reviews. Without these incentives, most sellers didn't think it necessary to offer quality products, provide prompt delivery or respond to customer complaints.

Another development that you'll be able to spot is how certain users will try to exploit your service.

After some time, you'll likely identify "power players" — people who will understand how your platform works and use it to their advantage against your average users.

Airbnb provides a perfect example. After launching their service, designed for the average user to rent out a spare room, they saw a rise in real-estate professionals using the platform to rent entire apartments or houses.

Having professionals use your service isn't always a problem, but when they change your established rules to fit their own agenda, it can be a threat to the goal of your service. This is why Airbnb chose to remove these listings from their site.

So give yourself time to identify harmful trends that might emerge and be sure to establish new rules to prevent power players from taking over.

### 4. Different types of platforms benefit from different types of funding. 

If you've ever left your wallet at home, you know that borrowing a couple dollars for a cup of coffee can be painfully embarrassing. Borrowing on a large scale can be even more daunting, and launching a Kickstarter campaign to ask millions of strangers to fund your peer platform can seem like a downright scary proposition.

Be that as it may, _crowdfunding is_ one of the three major ways to get your project off the ground, with the other options being _private-sector_ and _public funding_. So which one is right for you?

Well, private funding requires you to aim for maximizing shareholder value, which can sometimes be at odds with the goal of your platform. Private funding is also risky, since it can leave you without a controlling majority of the company, robbing you of the decision-making power to develop your platform as you see fit.

For example, the software-development platform Github managed to avoid private funding for their first four years. This way, shareholder concerns weren't a priority when making difficult decisions.

If, on the other hand, your platform deals with basic utilities, you should look to the public sector for funding. Services like GPS, WiFi and the internet were all financed and developed through taxpayer dollars and then made available to the public for free. If you think about it, the electricity net is also a shared platform, as anyone can install solar panels on their roof and start selling the excess capacity. So if your platform can be seen in a similar light, it makes sense to seek public funding.

Last but not least, crowdfunding can also be a great way to retain your control of your platform, because you don't need to give out equity to the people funding it.

Just consider the example of Caroline Woolard, who used the crowdfunding platform Kickstarter to launch _Trade School,_ a service that allows people to start and attend free classes throughout New York City on topics ranging from modern art to professional communication. Kickstarter allowed Woolard to raise enough money to cover most of her costs.

> Since 2009, Kickstarter has connected developers with $1.4 billion in funding.

### 5. Governments have a long history of helping peer platforms – even when they’re being overly restrictive. 

As we saw in the previous blink, public funding can help you develop your platform. But in what other ways has the US government helped create peer platforms?

Let's look at the history of the Global Positioning System, or GPS.

This technology was originally developed by the United States during the Cold War era to help deter nuclear attacks.

Then, in 1983, the Soviet Union shot down a Korean Airlines plane that had accidentally flown into their airspace. It was clear that this tragedy could be avoided if the pilots had been able to navigate using GPS. And so the US government decided to allow anyone to use their GPS, effectively making it a peer platform. From there, it has evolved into an invaluable tool today, used by countless services.

On the other hand, sometimes strict governmental regulation can also lead to new platforms.

Take Uber and the taxi industry, for instance. In the United Kingdom, cabbies are required to pass a rigorous test to prove their knowledge of London's tricky roadways, without the help of GPS. As a result, it can take almost three years of preparation for them to pass this test.

This kind of strict governmental oversight happens in many other parts of the world too and is partly what led Uber to create a viable and attractive alternative to taxis.

In the next blink, we'll look at how big companies can influence the sharing economy as well.

### 6. Large companies also benefit from collaborative models. 

You've probably heard the saying "adapt or die" applied to the business world? Well, large companies too are trying to adapt to the sharing economy trend.

The UK supermarket behemoth Sainsbury's embarked on a bold collaboration experiment when it opened its doors to 155 external corporate sustainability experts, asking them to review its sustainability strategy. The group even included one of their direct competitors. Thanks to this independent peer review, Sainsbury's was able to objectively pinpoint both the areas it excelled in as well as those that needed improvement.

In France, the home-improvement store Castorama decided to incorporate an online platform where employees, customers and local craftspeople could share useful skills and tips. Within a few months, more than 2,400 people signed up and began to shape an active community.

It's hard to know exactly how much revenue a community platform like this can bring in, but both of these examples illustrate how more and more big businesses are moving toward collaboration.

And it seems to be working for them. In 2014, the management consultancy Deloitte examined the 500 most valuable companies in the United States to determine what the most successful business model was. Sure enough, they found that companies using the collaborative model were valued two to four times higher than those using other business models.

Deloitte calls these companies "Network Orchestrators" and believes their success is due to the strong peer networks they build, which allows them to interact and boost each other's value.

These kinds of results will surely attract more and more businesses to try the collaborative model, and it's only a matter of time until those refusing to join it will find themselves struggling to stay afloat.

### 7. Peer models can help to combat climate change. 

In the previous blinks, we've seen how sharing platforms can address a wide variety of needs. But only when we consider the current global situation can we see just how useful this model really is. 

Climate change is one of the most pressing issues facing the planet today. If we're going to avoid the catastrophic effects of rising temperatures, emissions need to be lowered immediately so that they might reach net zero by 2050 — meaning that the amount of emissions released roughly equals the amount of renewable energy created.

Governments have been either slow or unwilling to address climate change, though they could help by adding a carbon tax to products and services based on how much pollution they create.

But this alone is insufficient, and peer models can help.

Climate change presents so many problems and has so many causes that a collaborative approach is needed to reverse it. It's going to take intelligent people from all over the world to formulate a workable solution.

And with effective peer models, we could change sectors like agriculture, food distribution and public transportation, all of which are vital to reducing emissions.

One example of something already in action is India's G-Auto, founded by Nirmal Kumar.

Like Uber for automotive rickshaws, G-Auto reduces the need for drivers to cruise around looking for customers, thereby reducing CO2 emissions and traffic jams.

There is still a great amount of excess capacity in many realms around the world to take advantage of. And the more efficiently and effectively we use our resources, the better we'll be at combating climate change.

### 8. Final summary 

The key message in this book:

**When we switch from a buying and owning mentality to a sharing mentality, everyone wins. Those who have an excess capacity of a resource make money; those who use that excess capacity save money. And the planet is exposed to fewer emissions. But there are still more resources with excess capacity out there, just waiting to be used.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Raw Deal_** **by Steven Hill**

_Raw Deal_ (2015) reveals the ugly truth behind the new sharing economy and the harm that companies like Uber or Airbnb are inflicting upon societies around the world. There's a major crisis on the horizon, and it will affect not only these companies' exploited employees. We're all at risk, and we'll need to choose our next steps wisely to prevent an economic collapse.
---

### Robin Chase

Robin Chase is an entrepreneur and cofounder of Zipcar, the world's largest car-sharing company. She has served on the board of the Massachusetts Department of Transportation and worked on a federal transportation advisory committee. In 2009, _Time_ magazine named her on its list of the world's 100 most influential people.

