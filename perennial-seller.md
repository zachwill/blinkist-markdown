---
id: 5a256f59b238e10008f0f445
slug: perennial-seller-en
published_date: 2017-12-07T00:00:00.000+00:00
author: Ryan Holiday
title: Perennial Seller
subtitle: The Art of Making and Marketing Work that Lasts
main_color: 761918
text_color: 761918
---

# Perennial Seller

_The Art of Making and Marketing Work that Lasts_

**Ryan Holiday**

_Perennial Seller_ (2017) explains how to ensure that great creative work also succeeds in the market. These blinks not only demonstrate how to generate success for a particular project, but also how to secure continued long-term success for yourself as a creative individual.

---
### 1. What’s in it for me? Create something that will outlive you. 

We'd all like to have our little place in history, wouldn't we? Even if you're not the next Steve Jobs, you probably still wish to leave some kind of lasting legacy. And you probably hope to do this by creating something. Of course, this creation shouldn't have but a brief existence; it should be eternal. The book you've written, the app you've developed, the work of art you've created — whatever you've produced, you definitely want it to last.

But in a world that is all about being new and fresh, how do you create a product that is more than a flash in the pan? In other words, how do you make a perennial seller?

These blinks will teach you how. You'll learn how to take all the steps, from the moment you have that first creative idea right through to what it takes to continue to survive — and even thrive! — in an industry that's predicated on newness.

You'll also learn

  * what blasting music out of a car's loudspeakers has to do with perfecting your product;

  * how YouTube can help you with perennial success; and

  * what connects Iron Maiden with an insurance mailing list.

### 2. Creating a perennial seller requires hard graft, and great ideas alone will get you nowhere. 

You know that old maxim about product success being 20 percent creativity, 80 percent marketing? Well, you can safely ignore it, because it's pure bushwa.

If you want to develop a perennial seller — a product that always sells, year in and year out — you're going to need to put a lot of work into the creative process. Marketing is not a magic wand. To be successful, your creation will have to be a world-beater from the get-go. And there's no escaping the hard work that creation entails.

No company knows this better than Microsoft. It's forced some real stinkers on the market, hoping that marketing would make up for product shortcomings. Just think of its failed MP3 player, Zune, or its dismal search engine, Bing.

In contrast, Microsoft Office has only gotten better over the years, and is now a classic 25-year perennial seller, thanks to its sound software design.

But, of course, if you're going to market a product you'll need to develop one in the first place. Just having lightbulb moments will get you nowhere.

To drive this point home, consider how many aspiring writers you've met who haven't actually published a thing. You know the type; he talked your ear off at this or that party, claiming he would soon produce a masterpiece, though he hadn't written anything yet. It's one thing to have an idea for a film's plot, or a concept for a start-up, but it's quite another to actually get going.

Ideas alone are worth nothing. Take the writer and comedian Sarah Silverman's advice. You needn't wait for someone's approval to get going: just write!

So exactly how do you turn an idea into something real? Just what kind of hard work is needed? Let's turn to that now.

> _"Lots of people want to be the noun, without doing the verb."_ — Austin Kleon, poet

### 3. Motivation, and a readiness to make sacrifices, are crucial to creative success. 

There are loads of people who say they'd like to play professional football. Why do they want to do this? Many believe that "it would be fun." But that kind of flabby rationale really isn't enough to fuel success. It's not concrete, and it's not going to lead to the kind of motivation that produces actual results.

The same holds true with creative work. A strong sense of purpose is the surest way to success.

When you combine your work with a purpose, you'll have the motivation to push through any barriers that stand in your way. Let's imagine you're a writer with a strong purpose to make the world a better place. When your articles or books on this subject get rejected, you won't simply give up. You'll keep on writing and submitting your work because you know that your purpose is a worthwhile aim.

Of course, there are other situations that might drive you and inspire your sense of purpose. It may be that your or your family's survival depends on your success. Desperation can be extremely galvanizing, a fact that dovetails nicely with the other thing you'll need to succeed: a willingness to make sacrifices.

Take athletes as an example. No doubt they'd prefer to spend a beautiful summer day chilling with their friends, sipping cold beers. But where will you actually find them? In the gym, lifting weights, or out on the track, running laps.

Likewise, creatives like writers and artists must seal themselves off and shut out distractions to actualize their vision.

This is the case for you, too. You need to make those sacrifices to ensure that your efforts pay off. But exactly what sort of hard work is involved? Let's look at that now.

### 4. To be a successful artist, become your own CEO and track down an ace editor. 

Somewhat paradoxically, there's more to being creative than creativity alone. Novelists don't get published by simply jotting down a masterpiece and mailing it off to some grateful publisher.

It's not that simple.

If you want your work to be successful, you'll need to be a CEO as well as an artist.

Many creatives would prefer to be just that and leave the editing, promotion and marketing to someone else. But unless you're already rolling in dough, that's not going to happen.

The competitive nature of the market can't be underrated. For instance, at least 300,000 books are published each year in the United States. To make your book stand out, you'll have to do more than be a sequestered, tortured genius. You're going to have to act as your own CEO, devising commercial strategies for your product.

Though the responsibility for success ultimately rests on your own shoulders, however, an outside critique or alternative voice is always helpful. Such a voice usually comes in the form of an _editor_.

An editor can't be some stranger pulled off the street. She's got to be a professional in the field, someone you can trust to be honest with you. Friends won't cut the mustard.

Writers and screenwriters ought to turn to actual editors. Musicians may want to seek the advice of sound engineers or composers. Athletes should rely on expert coaches and judges.

This last creative step — the editing step — is often neglected. But you'll thank yourself later for taking it. Nowadays, Harper Lee's _To Kill a Mockingbird_ is considered an ageless classic. But the novel began its life as an imperfect manuscript submitted to an editor, Tay Hohoff. Hohoff's criticism was brutal. She demanded that Lee rework the text to make the narrative more structured and coherent. Without this editorial effort, you wouldn't know who Harper Lee is.

So we know that editing and perfecting a product is critical. But how does that process actually work?

### 5. Testing your product to perfection is essential. 

You might think that if your product is close to being finished, you can relax a little more. Well, banish the thought. As you near the finish line, you should get more obsessive still by testing and improving your creation.

In fact, for your work to be successful, you'll have to test and rework it over and over.

Creatives and producers often do this by engineering some surprising methods for quality control.

Just take composer and producer Max Martin. He's written songs for the likes of Celine Dion and Adele, and, over the years, he's developed a neat trick called the "LA Car Test."

Martin likes to cruise down the Pacific Coast Highway while blasting whatever song he's testing out. This gives him a chance to experience the song as a typical radio listener would. If it works while he's cruising down the road, it's probably going to work for the masses.

Here's another practice that you can take advantage of before releasing your creation to the public. Step back, ask yourself what you originally aimed to achieve and whether your product really represents this.

This technique is called the _One Sentence, One Paragraph, One Page method_.

When your mind is clear, take out a fresh blank piece of paper. Now write down precisely what your project constitutes, in a single sentence. Now do that again, but in a single paragraph. And again, in a single page. This procedure will impose coherency on your creation.

Imagine that you've written an autobiography, but incorporated fictional elements. Now, when you're describing the project using the One Sentence, One Paragraph, One Page method, you may find it difficult to say whether you've written fiction or nonfiction. This sort of observation will force you to refine the content and make it more consistent.

Until you can describe your work concisely and convincingly, your primary task will be product enhancement and constant improvement.

### 6. Marketing your creation is your job alone, and there is one essential rule to remember. 

It's easy to feel swamped by the amount of culture and art out there. As a creative person, you may feel intimidated and daunted. How exactly are you supposed to make yourself stand out from the crowd?

The answer is marketing. But remember: this requires hard work and independent effort. You can't rely on others to do the work for you.

The reason for this is simple. After all, you're the only one who's really invested in your product. Paying somebody else to push your creation isn't going to work, because no one is going to care about what you're doing as much as you do.

Many artists have come to this same conclusion. For example, author Ian McEwan jokes that half the time he acts as his own employee, a miserable salesperson of the actual author's creations.

And, unfortunately, marketing a product these days is far from easy. At no time in history has it been easier for consumers to compare prices and search for competing products.

With this in mind, there is one thing you should never forget when marketing your work. Nobody will give a fig about your creation unless you push it. You've got to work to show consumers why they should care.

You are, in short, a marketer of your own products. And to market well, you'll need a healthy dose of humility.

For example, one of the author's clients had the chance to present his new product on several successful podcasts.

Such opportunities are one in a million. But the client was initially unenthusiastic. He wanted to do a single conference call, with each podcast on the line, and be done with it. Fortunately, the author reminded him of the importance of humility and persuaded him to go on each podcast, which proved to be a marketing coup.

So marketing is important. And the good news is, it's not as complicated or dry as you might think.

### 7. There is no one right way to do marketing, though word of mouth should always play a role. 

If the bad news about marketing is that you always have to start from a position of audience ignorance, then the good news is that there are plenty of strategies to spread the word.

In fact, sometimes the most creative approaches are the most successful. There are no hard-and-fast rules when it comes to marketing, as author Steven Pressfield well knows.

In 2011, Pressfield wrote _The Warrior Ethos_, a book that examined how humans deal with and overcome adversity. It was entirely self-published, with no external support. To market it, Pressfield made a special military edition and printed 18,000 copies, which he distributed to friends and army contacts.

It took a lot of effort to publish and deliver the books, but it was well worth it. In the following months, the book's sales figures went through the roof. Even today, they are stabilized at around 13,000 copies sold per year.

But this is no fluke. The success of _The Warrior Ethos_ was based on stellar marketing practices — mainly word of mouth.

A study by the consultancy firm McKinsey outlines that word of mouth accounts for 20 to 50 percent of purchasing decisions. In today's consumer environment, emphatic recommendations from close friends are marketing magic. It's been shown that people are up to fifty times more likely to purchase a service or product if a close friend enthusiastically recommends it, as when compared to an acquaintance mentioning it in passing.

Word of mouth is also the best strategy for ensuring your product remains a long-term best seller. With that in mind, target your marketing efforts accordingly to increase word-of-mouth mentions of your product. Social media is a godsend here.

Marketing is essentially all about creating word of mouth, whether you're trying to get your product off the ground or sustain income.

### 8. A platform is great for holding people’s attention, and it will also ensure creative independence. 

Once you've put your product out, the temptation is to feel that you'll be forever defined by this one achievement.

But if you want continued success you'll have to develop your presence in the public eye, independent of any one single creation.

Therefore, you'll need a platform where you can alert people to the latest goings-on.

A platform combines all the skills, connections, communication channels and followers necessary to ensure the success of your creative work and nurture your own personality as a creator.

A platform is especially useful for surviving in difficult times.

Winston Churchill is a good example here. He had been a successful politician early on in his career, but his rivals kept him on the fringes between 1931 and 1939 because of his radical stance against Indian independence.

However, that downtime didn't ruin him. He had another platform he could rely upon thanks to the fact that he'd built a second career. Churchill wrote books and articles, and he sometimes appeared on the radio, so that, even while in political exile, he continued to exert influence and make money.

The other benefit of a platform is that it means you can make your success independent of other people. It's entirely your own. Just look at the success of Casey Neistat, who spent years as a promising filmmaker and even premiered two films at Cannes.

However, Neistat decided to forge his own independent following. He created a platform profile on YouTube and now has millions of subscribers and hundreds of thousands of views each day. His audience is his own. He has managed to cut out the middleman, and can now personally decide whether his work gets broadcast or not, which also means his profits are entirely his own. Now that's artistic freedom par excellence.

### 9. Creating a mailing list is critical for survival and you can even start before finishing your first product. 

Imagine that one day, on a whim, the media and the industry lose interest in your work and decide to blackball you.

Until recently, you couldn't really have done much to combat such a turn of events. But now you can.

A mailing list is by far the most important survival insurance for a creative of any stripe. At its heart, a mailing list is a direct line of communication to people who know you or value your work. But it takes time to build up that support base. Start early. There's no point waiting until your career is waning.

That's exactly how the band Iron Maiden stayed on top of the game. Heavy metal had had its mainstream halcyon days in the 1980s. But those soon faded, giving way to grunge.

But Iron Maiden had built up a mailing list, long before anyone thought of doing so. Consequently, they never lost direct access to their large fan base. They still play massive sold-out concerts to this day despite heavy metal's having gone out of vogue.

In fact, you needn't even have a product ready before you start compiling your email list. Ideally, you want fans before your product even drops.

That sounds a bit back to front, but it works. All you need is a strategy to find your would-be fans.

Actually, that's precisely how the author managed such great sales on his first book. He knew he wanted an email list of interested people ready before publication. So he created a monthly newsletter with book reviews. That way he could also publicize his own book, once it was published.

Thanks to this scheme, when his first book was released, the author already had five thousand people on his mailing list. Today, he has 80,000 followers.

Okay, so now you've got the lowdown on developing your creative work. You know the importance of product and platform creation, and how crucial marketing is. It will take a lot of hard work, but it also isn't rocket science. You can create it, if you really want to.

### 10. Final summary 

The key message in this book:

**When it comes to creative work, there's no escaping the hard work that's needed. In fact, you're actually going to have to work perhaps harder than you would in any other field. Not only do you have to actually do the creative work; you also have to be your own CEO, marketing director and PR professional. But with the right strategies, none of this is impossible.**

Actionable advice:

**Network indiscriminately and passionately.**

As a creative person, you'll always be largely dependent on people's support. Therefore, treat everyone you meet as if they had the ability to get you a spot in the _New York Times_ — because you never know. Someday, you may meet someone who can do just this, if not something even more helpful.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Creativity, Inc._** **by Ed Catmull with Amy Wallace**

_Creativity,_ _Inc._ explores the peaks and troughs in the history of Pixar and Disney Animation Studios along with Ed Catmull's personal journey towards becoming the successful manager he is today. In doing so, he explains the management beliefs he has acquired along the way, and offers actionable advice on how to turn your team members into creative superstars.
---

### Ryan Holiday

Ryan Holiday is an author and media specialist who left college to become assistant to Robert Greene, the author of the _48 Laws of Power._ He later became head of marketing for American Apparel and now owns his own consulting agency. His other best-selling books include _Ego is the Enemy._

