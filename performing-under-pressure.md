---
id: 559bd2a63966610007370100
slug: performing-under-pressure-en
published_date: 2015-07-10T00:00:00.000+00:00
author: Hendrie Weisinger & J.P. Pawliw-Fry
title: Performing Under Pressure
subtitle: The Science of Doing Your Best When It Matters Most
main_color: E2C449
text_color: 7D6C28
---

# Performing Under Pressure

_The Science of Doing Your Best When It Matters Most_

**Hendrie Weisinger & J.P. Pawliw-Fry**

_Performing Under Pressure_ (2015) offers a guide to building confidence and overcoming high-pressure situations to achieve your loftiest goals. You'll discover why it's difficult to perform when the stakes are high and learn practical strategies to help you conquer performance stress.

---
### 1. What's in it for me: Learn why pressure is your enemy and how you can vanquish it. 

Are you one of those people who leaves things to the last minute? Are you convinced that deadline pressure actually makes you work better? You just might be mistaken.

Scientific evidence shows that while pressure may keep us from procrastinating, it nearly always impedes our creativity and makes us reach for the most conventional solutions. Pressure can even distort our thinking or make us freeze up altogether.

It turns out that the most successful people are those who have learned to perform well despite the world breathing down their necks. But how can you master the techniques needed to do the same? These blinks will help you to overcome pressure and make yourself a productive powerhouse.

In these blinks, you'll discover

  * what your ability to deal with pressure tells you about your love life;

  * why saying "I need a car" can make you unhealthy; and

  * how a "COTE" of armor can help you to deal with pressure.

### 2. Focus on the now to conquer stress. Focus on success to conquer feelings of pressure. 

Think about taking an important exam. Are you afraid, eager? Do your palms sweat, or nerves twitch?

The sensations you felt in that moment are typical of high-pressure situations. Unfortunately, pressure and stress can hamper your performance when it's most essential that you succeed.

Pressure moments can affect performance in everything, from your day job to your relationships.

Harvard Business School professor Teresa Amabile's research into creativity in the workplace discovered that pressure negatively affects both creativity and productivity. Although some workers felt more creative when under pressure, the fact of the matter is that while they were certainly completing tasks, their performance was actually on a much lower creative level.

Pressure affects more than work. The secret to a successful relationship isn't _chemistry_ between partners but a pair's ability to interact in pressure moments.

For example, couples who criticize each other, saying things like "You're too selfish," put a tremendous amount of pressure on a relationship. As a result, the partnership suffers as each individual inevitably feels dissatisfied with the other.

Stress, on the other hand, occurs in different scenarios.

While it's common to experience _pressure_ in situations where success is crucial, like during a college entrance exam, we tend to experience _stress_ in situations with too many demands and too few resources, such as a work day with lots of back-to-back meetings.

But what's the connection between pressure and stress?

Stress affects us just like pressure by decreasing our creativity. But stress is a different phenomenon and therefore begs a different solution than does pressure.

The trick to dealing with stressful situations is _reduction._ For instance, focusing on only what you're doing _right now_ will help you manage stress.

But in a pressure situation, you should focus on the final result, whether it means safely landing a helicopter or winning a basketball game. In short, this means in pressure moments you should focus on _success_ and adapt your behavior to constantly advance.

### 3. Pressure can reduce your ability to perform in an instant, just through the way you think. 

Have you ever started a presentation with the idea that every word you said would be perfect? Of course — we all put pressure on ourselves to do so. But it's essentially counterproductive.

Why?

Because doing so leads to a specific type of failure caused by pressure, called _choking._ Here's how it works.

Pressure tends to mess with your performance system, things such as physical arousal, thoughts and behavior. Disturbing any one of these can undermine your whole system.

For instance, say you're giving a speech and can feel the pressure building up: your heart races, your mind goes blank and you can barely read the words in front of you. You're choking!

Pressure can also affect your memory. For example, in pressure moments you tend to actively monitor your own performance, thereby consuming your brain's resources.

What happens when you do this?

Your procedural memory, a crucial function that helps you perform complex tasks without thinking, is blocked. Imagine that a friend has been rehearsing for a piano recital for weeks. But when she gets on stage, she starts paying close attention to her every move and all of a sudden, can't recall a single note.

In other words, she chokes. But how does this happen at all?

A simple thing, such as your perspective on a situation could trigger pressure — and even lead to depression. Here's how this works.

Your _cognitive appraisals_, which determine how you see the world, are affected by pressure leading to a phenomenon known as _cognitive distortions_, an effect so strong that it can produce anxiety and depression.

For instance, thinking that you "need" something, like a new car, puts pressure on yourself and risks making you ill. To control a distorted reality, it's essential to take control of your words.

Therefore, instead of thinking "I _need_ a car," think "I _want_ a car." The pressure of the sentence, and of the situation overall, will be immediately reduced.

> _"It's not the event that causes our reactions but rather how we interpret the event."_

### 4. Handle pressure situations with the right strategies. A good rule of thumb: Chill out a bit! 

So we know that pressure is bad, but how can you deal with it?

There are simple ways to minimize pressure and reduce your worries.

For instance, an easy strategy to adopt is to not take things too seriously. This works by reducing pressure and therefore your fear of failure.

Imagine you're heading to an important interview and really feeling the pressure. Your friend asks you, "What's the worst that could happen?" and tells you to relax.

While you probably have the urge to snap at her, she might have a point. Because being able to see your situation as normal reduces the pressure you've built and leaves you free to do your best.

After all, worrying too much about failure can be a distraction from what you have to do. So make it your goal to keep your head clear and focused.

But how do you do this?

Instead of worrying about what the interviewer might ask you, focus on what you need to do to shine. Do plenty of research and rehearse what you're going to say.

Another trick to dealing with pressure works by helping you remember your values amid high-stakes scenarios. Here's how it works.

In pressure moments, you can be so eager to do what's expected of you that you forget your personal values. To ensure this doesn't happen, you should prepare for such moments by listing your values in order of relative importance.

For example, success might be very important to you, but fairness more so. Say your boss expects you to strike a deal with a new company, but over the course of negotiations you discover that the agreement would require you to underpay your employees to meet the company's rock-bottom price expectations.

Since you know that fairness is more important to you than success, it will be that much easier to avoid that pressure to accept the company's unfair conditions.

### 5. Confidence and optimism are the first key attributes to handling and defeating pressure. 

To do your best when it counts, it's essential to develop attributes that help you thrive in pressure moments.

Consider these qualities your "COTE" of armor: Confidence, Optimism, Tenacity and Enthusiasm.

_Confidence_ is key to keep you pressing ahead when the going gets tough.

As your self-confidence increases, your anxiety reduces and it becomes easier to perform well under pressure. In fact, several studies have demonstrated that people with higher confidence perform better, work harder, persist longer and even consider themselves smarter and more attractive than their peers.

It's easy to increase _your own_ confidence simply by striking a high-power pose.

Begin by physically opening your body, lifting your arms up, standing straight and pulling your shoulders back. Stand like this for a couple of minutes. This confidence-building posture is enough to lower your stress hormones and boost your testosterone, giving you more courage!

Like confidence, _optimism_ will help you move forward despite pressure.

If you look at things positively and have good expectations for the future, you'll be more likely to do things that are uncomfortable now, like taking risks or working hard.

Debra is a young woman who, after a terrible car accident, suffered from multiple fractures and months and months of a difficult, painful recovery.

Yet Debra was still grateful to be alive each and every day. Her positive attitude was so powerful it gave her the strength to keep working and got her back on her feet.

How can _you_ develop an optimistic attitude?

Try starting your days by appreciating every little thing around you. Things like a cozy bed, fresh air and your loving family. By cherishing the small things, you'll learn to see the positive side in every situation.

Now that you know about confidence and optimism, it's time to complete your "COTE" of armor with tenacity and enthusiasm.

> _"Affirm yourself, be positive every day, commit to your best, celebrate."_

### 6. Tenacity and enthusiasm complete your “COTE” of armor. Pressure doesn’t stand a chance! 

Confidence and optimism are great for getting you moving when things get tough, but to keep moving forward despite constant pressure, you'll also need tenacity.

_Tenacity_ comes into play when there is a goal for which you want to strive.

Because when you're working to achieve something you want, you'll put up with challenges and frustrations you'd never otherwise tolerate.

For example, say you're scared of speaking in public, but want to inspire your classmates to care about animal rights. You'd likely be willing to stomach the fear of delivering a speech that would make them all give up their ham sandwiches!

Tenacity can get you to do things you wouldn't otherwise think yourself capable, but enthusiasm will help you stay passionate and creative in spite of pressure.

_Enthusiasm_ gives you energy to keep working and doing your best. It can be like a virus in that it is quickly catching, positively affecting those around you.

Even in situations where pressure has almost killed your creativity, enthusiasm can kick it back into gear!

For instance, when under pressure to produce results, it's common to narrow your scope and work toward any solution. But if you're passionate about what you're doing, you're likely to broaden your mind in search of something truly original.

So the next time you face a high-pressure moment, such as giving a speech before the board of directors, be sure to act enthusiastically so you will feel enthusiastic. If you need an enthusiasm boost try laughing, recall a positive memory or listen to upbeat music.

### 7. Final summary 

The key message in this book:

**Pressure affects the human brain, changing the way you think and behave often in the worst moments. To improve your performance under pressure, it's necessary to learn the right strategies to become more confident, optimistic, tenacious and enthusiastic — and thus successful!**

Actionable advice:

**Focus on an object to keep your thoughts in the present.**

Focusing on the present moment is a great way to conquer high-pressure situations. You can increase your general presence of mind by choosing any object, picking a spot on that object and focusing on that spot. As you focus, pay attention to your breathing and how it feels as you breathe in and out.

**Suggested further reading:** ** _Four Seconds_** **by Peter Bregman**

_Four Seconds_ (2015) gives precise examples of how to rid yourself of self-defeating habits at work, at home and in your relationships. A four-second pause helps slow down hasty, unhappy reactions and is the first step to reworking the way you communicate with others and receive feedback from them. You really can be prepared for anything if you just take a breath first.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Hendrie Weisinger & J.P. Pawliw-Fry

Hendrie Weisinger is a psychologist and pioneer in the field of pressure management as well as a bestselling author.

J.P. Pawliw-Fry is a performance coach who advises Olympic athletes and business executives. He is the president of the Institute for Health and Human Potential, a global research and learning company that trains organizations and leaders to best perform under pressure.

