---
id: 594efa76b238e10005bb48c9
slug: personality-not-included-en
published_date: 2017-06-27T00:00:00.000+00:00
author: Rohit Bhargava
title: Personality Not Included
subtitle: Why Companies Lose Their Authenticity and How Great Brands Get it Back
main_color: ED6D30
text_color: A14A21
---

# Personality Not Included

_Why Companies Lose Their Authenticity and How Great Brands Get it Back_

**Rohit Bhargava**

_Personality Not Included_ (2008) explains how to build an awesome public face for your company. These blinks teach you how to develop a strong personality for your business, explain why that's so important and help you clear the barriers between you and business success.

---
### 1. What’s in it for me? Learn how to build a strong personality for your company. 

Imagine you're on your way to the airport in a taxi. Your driver strikes up a conversation and, because he turns out to be an entertaining person, you make a note of his number and promise to call him the next time you need a ride to the airport. 

As you get to the airport and board your flight, the airline attendants present a friendly face, but everything they say seems to come directly from their corporate handbook. In short, they don't show personality.

Whose praises will you sing to your friends and colleagues: the taxi driver or the flight attendants? The driver, obviously. To build a successful business of any kind — even a one-man taxi firm — your company needs personality.

These blinks will teach you how to prevent your business from being faceless. You'll learn how to create a strong and convincing personality for your company and why this helps you acquire customers.

You'll also learn

  * what the three numbers on every Dole banana tell you;

  * why your auto mechanic wants to give you technical insights while repairing your car; and

  * why you should listen to what your customers _don't_ say.

### 2. Companies used to aim for a faceless image, but authentic connections are more effective. 

You can travel to just about any city in the world and know that the McDonald's you find there will be nearly identical to the branch in your hometown. In this way, McDonald's is a prime example of a faceless company — their restaurants have no individual identity.

This approach is a vestige of a past in which, if a company wanted to give off an air of success and trustworthiness, it erased any personality. As a result, the more success it experienced, the more layers were built between the corporation and its customers.

There was no expectation that any customer could talk directly to a staff member with power or responsibility because any company whose management had that kind of time couldn't be big, successful or dependable.

That's all changed. Now success depends on forging a bond with your customer, although many companies remain faceless and disconnected.

A disproportionate focus on rules and policies is a common mistake. Just consider airlines. The author once got bumped from a flight — even though the plane still had an empty seat — because the airline had a policy of not assigning new seats within 30 minutes of departure. Worse, if he had called his travel agent and booked a ticket for the same seat, he would have been able to stay on the flight.

If you've ever had a similar experience, you know how frustrating it is! Only a faceless company would treat its customers this way.

Instead, to create a true bond with your customers, you've got to give your company a voice. Take BzzAgent. They're a word-of-mouth marketing company that started a blog to host articles by their employees detailing their excitement and personal stories about moving offices. This simple idea helped BzzAgent's customers get to know the company while holding their attention and growing the firm.

### 3. A spokesperson can highlight the human side of your brand – or destroy it. 

Companies tend to have spokespeople, but have you ever wondered why?

Well, employing a spokesperson is a fantastic way to put a human face on your brand and prove that you don't just exist to make money. To accomplish this, a spokesperson talks about their brand in public. As the founder of a company, you can be your own spokesperson.

After all, you see and understand your company's vision better than anybody else, and the effort you've put in to realizing it will garner you respect. By openly talking about your passion, you can inspire others while building the credibility of your company.

But there's more than one sort of spokesperson. The other type is the accidental spokesperson, the most vivid and real voice your brand can find.

The most famous example is Jared Fogle, who became a spokesperson for Subway after he was profiled in a local newspaper for losing 100 pounds over just three months by eating Subway sandwiches. The company's advertising agency took full advantage of the opportunity by shooting an ad with Jared that became a national campaign.

But it's important to be careful when choosing a spokesperson since they can just as easily build a negative or unreliable image of your company. This is especially important to keep in mind with celebrities. Stars might be the most popular choice because they come with a ready-made fanbase, but the risk of celebrities alienating your audience by appearing fake is also sky high.

Campaigns built around celebrity spokespeople often fail because they don't foster a real connection between the celebrity and the company or product they're representing.

In 2000, the British TV chef Jamie Oliver became a spokesperson for the Sainsbury's supermarket chain. But the partnership blew up when Oliver admitted, on national TV, that he thought stores like Sainsbury's were like factories and he preferred shopping at local markets. Clearly, his endorsement wasn't at all heartfelt.

### 4. Instead of relying on marketing stunts, use a simple tool to create a strong brand personality. 

So, grabbing the attention of customers and personalizing your brand is key, but how do you go about it?

Well, many companies create a lot of noise around their brand through sensational marketing ideas. But this focus on stunts won't build a lasting personality for your firm.

Just consider the American fast-food chain, Taco Bell, which pulled a prank on April Fool's Day in 1996. They issued a press release saying they had bought the Liberty Bell in Philadelphia, renaming it the Taco Liberty Bell. The prank went down in pop-culture history, but its weak connection to the brand didn't do much for the company's personality.

In other words, even if a stunt succeeds, the effects aren't long-lasting enough to stick to the brand's name.

Instead, use the _UAT Filter_. This tool is composed of three elements: being _unique, authentic_ and _talkable_.

To accomplish the first goal of being _unique_, you have to find something to make yourself stand out. A good example is the small town of Sighișoara, Romania. Sighișoara brought in millions of tourists after dubbing itself the birthplace of Vlad Țepeș, a prince of Wallachia who served as inspiration for the Count Dracula stories.

The second element, _authenticity_, is derived from your company's values. These are qualities that aren't linked to profit, like honesty and philanthropic commitment.

And finally, _talkability_ is the hook that gets people to share information about your brand with their friends and family. It can be anything from a quick slogan to a jingle or a disco ball in your store. It's just got to be catchy and memorable.

### 5. Giving your business a backstory will foster an emotional connection between customers and product. 

Everybody loves a good story, which makes credible narratives an awesome tool for building authenticity for your brand. After all, a solid backstory will create an emotional connection between your customers and your product.

Just take Dole. Each Dole banana comes labeled with a three-digit farm code that customers can look up online to learn how that banana was grown, complete with personal reports from actual farmers. Through this tactic, Dole has built a meaty backstory with which customers can interact.

To build your own backstory, draw upon five models.

The first is the _passionate enthusiast_ who turned her passion into a successful business. Storyville Coffee is a good example. The company was started by coffee enthusiasts whose mission was to "save the world, one cup at a time" through their organic coffee.

Second comes the _smart listener_ who built her business by identifying the needs and preferences of customers. Stacy Madison used to sell little pita sandwiches on the sidewalk. By carefully tailoring her product to her customers' tastes she created pita chips that were so tasty they soon appeared in every grocery store in the country.

The third model is that of the _inspired inventor_ who followed their vision to launch a game-changing product. There's probably no better example than Steve Jobs and the iPhone.

Fourth comes the _likable hero_ who overcame all the odds. Step forward Tesla founder Elon Musk and his dream of electric cars. The auto industry didn't believe in the technology's potential, but Musk handily proved them wrong.

And finally there's the story of _little guy vs. big guy_, in which a small company battles a huge competitor. Consider Snapchat and their incredible refusal of a $3 billion buyout offer from Mark Zuckerberg's Facebook empire.

### 6. Four barriers stop your company finding its personality. 

Have you ever stayed at a job you hated just because you were scared of change? Fear like this can block you from making progress — and it has the same effect on businesses.

Most companies are scared of lateral thinkers who come up with ideas that challenge or change their current practices. So, instead of encouraging their employees to think outside the box, they urge them to follow instructions and fall in line.

It's essential to avoid this kind of thinking because the image of your company relies entirely on how your employees do their jobs. If your employees are mere automatons, this will be reflected in your company's image.

Instead, you should work to overcome the four barriers that hold your business back.

The first is the _danger of success_. Success can cause arrogance that's damaging to your brand and can also sap the ambition that drives your work, replacing it with complacency.

The second barrier is the _uncertainty factor_, in which uncertainty results from a poor understanding of the impacts of your actions and a reluctance to try new things.

After that comes what's called the _tradition barrier_, which is faced by some companies that stick to an old-fashioned outlook. They can't adapt their personality to the modern business world.

And finally, the fourth barrier is _finding a precedent_ or giving in to the temptation to copy another company's marketing plan and apply their strategy to your own business. What works well for one firm might be a disaster for yours. That's not to say you can't build on the precedent set by others, just that it's essential to find unique paths to continue your journey.

> _"A culture built on fear leads to individuals and companies that are afraid of change."_

### 7. Building personality depends on identifying the moment when you connect with your customers. 

Fostering a truly special experience with your customers requires proper attention to detail but it also means jumping at every chance you get to meaningfully connect with them. Such opportunities are called _personality moments_, and they allow you to convey your brand's image, making your customer love your product and come back for more.

These moments are driven by small things — minute gestures that are easy to write off as insignificant or to miss altogether. The oil change shop, Oil Can Henry's, created a personality moment by giving customers the opportunity to watch mechanics service their cars.

A personality moment doesn't necessarily depend on personal interaction with a customer; it can also happen before or after direct contact, when customers unpack your product, for example, or read about it. Each tiny detail of these experiences paints an image that is subconsciously delivered to consumers.

But before you're able to create such a charged moment, you first need to grab your customers' attention. To structure this process, keep in mind that every idea you'll ever have to get a customer's attention will fall into one of three categories: shock, sex, or relevance.

Volkswagen snagged the attention of their customers by depicting images of people narrowly avoiding accidents, thereby showcasing the incredible safety features of their cars. The ability to know precisely when you've captivated your customers is key to efficiently using personality moments.

Just take rock band, Sister Hazel. When their fans were lined up in the cold, waiting to get into a show, the band ordered them pizza and played them a few songs. This was the perfect way to take advantage of a moment when they could bond with their audience by providing something relevant — hot food and a personal preview of the gig!

### 8. Listening to and respecting your customers will build your company’s personality. 

In any workplace, respect is the cornerstone on which you build support for what you believe in. But respect isn't just essential between coworkers; it's also important to show respect to your customers.

It's critical when developing the personality of your business. To get others to show you respect, you've first got to show it to them.

Just listen sincerely and value the opinions of others. When you make others feel heard, they'll listen to and value your opinions in return.

Once you've done that, the second rule to gaining respect is to "listen first and talk second." After all, listening to the perspectives of others will force you to pay attention to what they believe, enabling you to compose responses that take into account their views as well as your own.

It's hard work, but you have much to gain. Not only are you promoting your company's personality, but you can also use the information in various ways.

One great strategy to identify points in need of improvement is to listen out for what your customers are _not_ telling you. Get all your employees together and have them list the rumors they hear about your business, or keep track of online conversations and pay attention to what's said about your brand.

Tracking this information stream will give you insights into the true opinions of your customers, things they might be unwilling to openly share with you. You can also build personality by engaging customers in dialogue through the comments on blogs or online forums.

This doesn't all have to happen online. Try participating in events relevant to your company and talking about what you do.

### 9. Final summary 

The key message in this book:

**Building a successful business means creating a personality that connects customers to your brand. To do so, you need to let go of the fear of being different and forge meaningful connections with your customers.**

Actionable advice:

**Beat the competition by thinking like them.**

Keeping up with the competition is a constant struggle; a good strategy is critical. Look at your business from your competitor's perspective and identify your strong points. From there, you can focus on these areas of expertise and try to improve them further or approach them from a different angle.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Non-Obvious_** **by Rohit Bhargava**

_Non-Obvious_ is all about detecting important trends, an essential part of running a business. You can't accurately predict the future by looking at obvious, surface-level influences — the important ones are hidden underneath. These blinks offer advice on identifying meaningful changes, while outlining some prominent trends we're likely to see in the future.
---

### Rohit Bhargava

Rohit Bhargava is the founder of Influential Marketing Group and an expert in building brand influence. He has written five bestselling books on the topic.

