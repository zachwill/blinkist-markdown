---
id: 541029183635620008460000
slug: personality-en
published_date: 2014-09-09T00:00:00.000+00:00
author: Daniel Nettle
title: Personality
subtitle: What Makes You the Way You Are
main_color: 219EA5
text_color: 176E73
---

# Personality

_What Makes You the Way You Are_

**Daniel Nettle**

Anyone can see that people have different personalities, but what exactly causes this? This book examines the factors that influence personality based on the research of numerous psychologists. You'll learn about the core traits that determine a person's overall character, the different strengths and weaknesses they have, and how to get the most out of your own personality.

---
### 1. What’s in it for me? Learn what makes our personalities tick. 

Why are some people anxious all the time and others relaxed? Why do some people seek out danger while others prefer to stay home? Why are some people more generous, outgoing or self-disciplined? The range of human personality we see around us can seem elusive and difficult to understand.

Fortunately, psychologists have studied personality for generations and can offer a great deal of insight on the matter. Drawing on the work of many psychologists and psychological studies, this book explores what it is that makes us _us_. You'll learn about the factors that create and influence our personalities, and how they determine our decisions and behavior.

You'll also learn

  * how being worried or anxious can be beneficial at times;

  * why personality is a product of evolution;

  * why it's impossible to have a "perfect" personality; and

  * how understanding your own personality can help you lead a more fulfilling life.

### 2. Our personalities derive from our genetics and the environment we grew up in. 

Have you ever wondered why some people never seem anxious, while others have such bad anxiety they can't sleep?

Put simply, it's because people have different personalities. But where do those personalities come from? Is it something biological or do you get it from your environment?

The answer? It's a combination of both.

About 50 percent of your personality comes from your genes. We can see evidence of this in the animal kingdom. For example, researchers once bred guppies from different geographical areas in an artificial environment free from predators. Later, they introduced a predator to see how they'd react.

Although none of the fish had faced a predator before, they reacted the same way they would've in the wild: namely, the fish originally from areas with a _low_ number of predators exposed themselves to the danger more than the fish from areas with a _high_ number of predators, which were better at protecting themselves and thus surviving.

This showed that their way of reacting to predators wasn't something they learned — it was genetic. And, like these fish, humans also have certain personality traits that are biologically ingrained.

The other half of our personality depends on our environment and is mostly developed in our childhood. Children learn to adapt very quickly in order to survive. So when we're in situations where we learn something important as children, it's likely to influence our personality as adults.

For example, if a person is the oldest of several children, they often have a strong sense of responsibility. Firstborns often feel the need to take care of their younger siblings, which can influence their personalities throughout their lives. They might continue seeking leadership positions when they're adults, for instance.

So although we do get quite a bit of our personality from our genetic code, our environment shapes it just as powerfully.

> _"Essentially unpredictable childhood misadventures can have important, long-term effects on personality into adulthood."_

### 3. Your personality is stable and it determines how you live your life. 

So you know that our childhood environment shapes our personality, but what about the environment we live in as adults? Does our personality continue to adapt to it?

Well, not really. Regardless of what experiences we go through as adults, the personality we develop in childhood stays with us for the most part.

One group of researchers interested in learning about the stability of personality gave participants the same personality questionnaire three times over the course of 12 years. Surprisingly, there was an extremely high correlation between their answers on the first test and their answers 12 years later. In fact, the answers were so similar that the same correlation was found when the researchers gave the test three times over the course of only _six_ _days._

Clearly, personality is very deeply rooted. And it has a huge impact on every aspect of your life.

Imagine you're walking down a dark and unknown street at night. Some people feel terrified; others feel fine. Why?

Well, if you developed an anxious personality in your childhood, you'd hate to be in that situation. You'd probably feel frightened, keep looking over your shoulder, and walk quickly to get out of there as soon as possible.

But if you developed a more outgoing personality when you were younger, this situation might not be a problem for you. You might even enjoy the adventure of discovering a new area.

We live our lives based on our decisions — whether they're about the city we want to live in as adults or the street we want to walk down at night — and we make decisions based on our personalities. This means that our life paths are usually closely tied to our personalities.

### 4. The variation between people’s personalities helps preserve our species. 

In school, nearly everyone learned about Darwin's theory of evolution: the organisms best adapted to their environments are more likely to survive. But what can Darwin's ideas tell us about personality?

When you look at personality from an evolutionary perspective, it's clear that the wide range in human personality is no accident. If there was only one, single personality that best fit to survive, all humans would've evolved to have it.

The animal kingdom is full of various species that have evolved to be experts at one thing. Consider the male peacock's extravagant tail: it helps the animal survive and reproduce.

And yet, there is no one human personality that determines our success in the way that the size of a male peacock's tail does. Instead, we have tremendous variation in our personalities. Why?

Because certain personality traits are not always useful to our survival. Some traits are helpful in certain situations but dangerous in others.

For example, climbers of Mount Everest were once asked to complete a questionnaire about themselves. Not surprisingly, they considered themselves good at coping with fear.

However, it's well known that when people climb Mount Everest, there's a mortality rate of one in ten. So these people's bravery (some might call it recklessness) means they probably ignore other serious threats in their lives.

People with personalities like this often seek out dangerous jobs. They might be firefighters or policemen, for instance. Without those jobs, society wouldn't function properly. But if _everyone_ sought to climb Mount Everest or fight fires, our species would die out, as too many individuals would take unnecessary risks to their lives.

So to balance this out, we also need cautious personalities to ensure another generation after us. We as humans work together and depend on each other immensely for survival, and it's to our benefit to have such a wide collective variety of personality.

### 5. The big five: meet the five core personality traits. 

How many types of personality are there, and how can we define them?

Psychologists have long sought an answer to this and, after many generations of research, they have established five main personality traits, often called the _big_ _five_. Our personalities are determined by what levels of each trait we have.

Let's look at the first one: _Extraversion_. Extraversion is about positive emotions. If someone has a high level of Extraversion, their brain rewards them when they achieve something by putting them in a good mood.

In one study of Extraversion, participants were shown movie clips that provoked either positive or negative emotions. Their moods were then measured. Those who'd been identified as extraverts had a much higher boost in their mood when they watched the positive clips than the other participants. They internalized the positivity, and were in a more positive mood afterwards.

As their mood is affected so much by what they see and think, extraverts tend to look on the bright side of things. So it's no surprise that they're usually adventurous and outgoing.

The next important trait is _Neuroticism._ Neurotic people worry about things often, and most of the time it's needless. Do you know anyone who panics when they read about epidemics or meteorites in the news? Or maybe people who want to move to a new house when there's been one break-in in their neighborhood? People who do such things are probably quite neurotic.

Although Neuroticism may seem disadvantageous, it can actually be very useful for survival. People who expect the worst are more likely to prepare for danger.

In our distant past, neurotics would've been the people who hoarded extra food in case they needed it in the future. That might well have kept them alive during a food shortage, while the more optimistic people starved.

So, possessing the different traits, like Neuroticism and Extraversion, can be very valuable in different situations.

### 6. The big five, part two: Conscientiousness, Agreeableness and Openness. 

The next of the big five traits is _conscientiousness._ Conscientiousness is the ability to set a goal for yourself and then work towards achieving it. It's a great predictor of success in your career. What do successful managers, salespeople and lawyers have in common? They're all very dedicated to their goals, i.e., they have a conscientious attitude.

Imagine you're studying for an exam and your friends invite you to go out. Having the self-discipline to say no is being conscientious. You know it's more important to work towards a bigger goal than to get immediate gratification.

The next trait is _Agreeableness_. This is when you overlook your own needs to help others.

Agreeableness is a uniquely human trait — animals don't display it. This was proved through a study of humans and chimps. Researchers sat two people opposite each other. One had two levers to pull: one that gave both people food, and another that gave only them food. Most participants pulled the lever that gave both people food, believing that to be the better choice. Chimps, however, pulled the levers randomly. They only cared about getting food themselves, not about whether their partners did.

Most likely, Agreeableness developed as a survival mechanism: our ancestors realized they could survive better if they supported each other in groups.

The final trait, _Openness_, is the least explored. Having a high level of Openness makes people creative, imaginative or eccentric, but little is known about how this works.

Some psychologists think that Openness is similar to intellect. People with a high level of Openness usually go out and explore the world, hoping to learn more about it.

In any event, psychologists still have much to learn about the final trait.

> "People who score high in Agreeableness … are quick to forgive, and slow to anger, even with people who are in fact blameworthy."

### 7. Each personality trait has advantages and disadvantages. 

The more we try to understand personality, the more a certain question is bound to emerge: Is there such thing as a "perfect" personality?

Well, no, there's not. We can't say there's a perfect personality because each of the five traits has its own pros and cons.

Let's look at Neuroticism. You might think it's annoying to be around a neurotic person, but try to imagine a world without Neuroticism. One of the great advantages of Neuroticism is that neurotic people tend to want to improve things, which can be beneficial to society.

Neurotic people realize when something is wrong, and they usually work to make it go away. Imagine a neurotic person concerned about climate change. It might be someone who devotes his life to developing environmentally friendly cars out of his fear of pollution. Without Neuroticism in our society, the world might never change or improve.

However, Neuroticism certainly has its downsides. Neurotic people are more likely to suffer from depression. They not only _worry_ more often than others but are also more affected by negative emotions.

For example, a person with a high level of Neuroticism will feel nervous when they see someone who looks angry. They'll wonder whether they've personally done something wrong to cause that anger, and they probably won't be able to concentrate on anything else until that person is gone.

If a neurotic person slips into nervousness that easily, he might end up being anxious all the time. Having a permanent or near-permanent state of anxiety often leads to depression.

So, Neuroticism can clearly be both an advantage or a disadvantage. The other four traits all work in the same way — there is no perfect formula of the big five. And there's certainly no perfect personality, as all traits can be an asset or a hindrance, depending on the situation.

### 8. Understanding your personality can help you improve yourself and find your niche. 

So why is this knowledge of personality traits useful, exactly? After reading about them, you've probably already started thinking about yours. Examining your own personality isn't only fun — it can be very beneficial to your life.

Understanding your own personality can give you a new perspective on many things. We often see the world in only one way, which can limit our understanding. Imagine a very wealthy person, for instance. On the one hand, her life is enviable: she doesn't have to be concerned about how much she spends on a pair of shoes or her child's education. On the other, she might struggle to make true friends because many people want to use her for her money.

Sometimes, the most obvious way of seeing something isn't the best way.

Similarly, if you want to achieve something, you might get a new perspective if you step back and think about your personality. How can you use your personality traits to reach your goal?

Imagine you want to raise awareness about animal testing. Unfortunately, you have a low level of Extraversion and a high level of Neuroticism, so you have trouble with public speaking. You certainly won't be very useful if you try to go canvassing or give speeches about the issue.

Luckily, there are still other important ways you can help the cause. You might join a research or management team, for instance. Try to use your personality traits to your benefit — you'll feel happier _and_ work towards your goal in a more meaningful way.

When you understand your personality traits better, you'll be better at finding your niche. If you intimately know your traits and their various strengths and weaknesses, you'll ultimately become a happier person.

> _"Now more than ever, then, it should be possible to find a niche where the traits you have actually give you an edge."_

### 9. You don’t have to change your personality traits – only the way you deal with them. 

Think about your personality for a moment. Are you happy with it, or do you sometimes wish you could change it?

Well, don't bother — you can improve yourself even if your personality traits stay the way they are.

Because you can't control your traits — though you can control how you manage them. Even though your personality largely determines your decisions, you still have the ability (and _responsibility)_ to choose your behavioral patterns.

For example, being a person who's not particularly agreeable isn't an excuse to ignore the emotions and needs of others. You have to work hard to help people when they need it, even if it doesn't come naturally to you.

Sometimes it's useful to find strategies for coping with difficult aspects of certain personality traits.

The trait people struggle to cope with the most is Neuroticism. It can be difficult to work towards your goals when you worry too much about failing. Here, it might be useful to find a consistent way to manage your negative thoughts or fears.

Many people who struggle with Neuroticism find it soothing to do yoga or other forms of physical exercise. This can distract them from their worrying. There are also medications available, so you should let your doctor know if you think you might benefit from them. If you have problems with Neuroticism, it's up to you to find the best way to ease your anxiety.

So, you don't have to _change_ your personality traits. In fact, that's not even really possible. What you _can_ do is learn about yourself, and learn the most effective ways to manage your traits — it can profoundly improve your life.

> _"You don't have to change yourself. You just have to change your self's outlet."_

### 10. Final summary 

The key message in this book:

**Personality** **results** **from** **a** **combination** **of** **nature** **and** **nurture,** **it** **dictates** **a** **great** **deal** **of** **our** **lives,** **and** **all** **personality** **traits** **have** **their** **own** **strengths** **and** **weaknesses.** **There's** **no** **reason** **to** **change** **anything** **about** **your** **own** **personality.** **If** **you** **commit** **to** **learning** **about** **yourself,** **you** **can** **make** **the** **right** **decisions** **and** **live** **life** **to** **the** **fullest.**

Actionable advice:

**Know** **yourself.**

Your personality is outside your control. Your actions aren't. When you closely examine the traits that make _you_ who you are, you'll know what you need to overcome to be a better person and how to work towards living a happier and more fulfilling life.

**Suggested** **further** **reading:** **_Quiet_** **by** **Susan** **Cain**

_Quiet_ focuses on the strengths and needs of introverts and extroverts. The book shows situations in which both personality types feel comfortable and the ways in which they can use their own potential to the fullest.
---

### Daniel Nettle

Daniel Nettle is the author of many successful books, such as _Happiness:_ _The_ _Science_ _Behind_ _Your_ _Smile_ and _Strong_ _Imagination:_ _Madness,_ _Creativity_ _and_ _Human_ _Nature_. He studied psychology, philosophy and anthropology at Oxford University and University College London, and his work focuses mostly on evolution, cognitive development and human behavior.

