---
id: 5d9b3e9c6cee0700074e2289
slug: persuasive-copywriting-en
published_date: 2019-10-08T00:00:00.000+00:00
author: Andy Maslen
title: Persuasive Copywriting
subtitle: Cut Through the Noise and Communicate With Impact
main_color: 74CCCA
text_color: 3A6665
---

# Persuasive Copywriting

_Cut Through the Noise and Communicate With Impact_

**Andy Maslen**

_Persuasive Copywriting_ (2019) is a valuable guide to the world of copywriting, with tips on how to get the attention and keep the interest of customers, as well as generate those all-important sales for your client. Author Andy Maslen takes time-tested techniques that have proven reliable for generations and shows how these are being successfully applied in an online world that's increasingly focused on content marketing.

---
### 1. What’s in it for me? Learn the secrets of good copywriting from an experienced pro. 

Writing good copy is a skill that will continue to be valuable as long as people use words to sell things. So, even though copy has moved from direct mailings and the pages of catalogs to emails and company websites, the basic principles of copywriting are still being used today.

As these blinks explain, the dos and don'ts of copywriting still apply in a world where companies are increasingly focused on content marketing. After all, the point of content marketing is to get people to take a desired action, whether it's to leave you their contact information or buy your product. And this is precisely what copywriting is all about.

So, whether you're a seasoned pro looking to brush up on some fundamentals, or a newcomer eager to learn, you're in the right place.

In these blinks, you'll learn

  * why targeting your reader's ego is never a bad thing;

  * how reading novels can improve your copy; and 

  * why it's sometimes okay to ignore the grammar police.

### 2. Good copy should appeal to the reader’s emotions, not the writer’s emotions. 

If you're a copywriter these days, you've surely heard people go on about how much has changed since the focus has shifted away from paper and ink towards social media and content marketing.

But while the places where copy is used may have changed, people remain fundamentally the same. Today's customers have many of the same emotions, fears and desires that their predecessors had 50 or even 100 years ago. And what good copy has always done, first and foremost, is tap into a customer's emotions. So in this respect, copywriting hasn't changed that much.

The reason good copy will evoke certain emotions in a reader is because emotions are at the center of our motivations. Every copywriter wants a customer to take a certain action, whether it's to make a purchase, provide a donation or sign up to a newsletter. 

Therefore, the aim of every copywriter should be to evoke the emotion that will motivate the customer to take the desired action. To put it another way, you want to engage in _empathetic copywriting_.

Empathetic copywriting isn't about being obvious and forceful; it's about persuading readers who are considering how to spend their time and money. What the copy shouldn't focus on are the emotions you, the writer, are experiencing.

For example, let's say the copy is about a 10 percent discount offer. What you don't want the copy to say is, "We are overjoyed to offer you an amazing 10 percent discount…" The reader didn't open an email or click on a link because they were curious about your emotions, nor is the reader necessarily going to believe that any sort of discount offer is truly amazing. Visiting the Grand Canyon or witnessing the aurora borealis is amazing, so when you use this word to describe a sales offer you're essentially diminishing its meaning. By doing this, you're disrespecting the intelligence of the reader and doing a disservice to your copy.

So, instead of exaggerating your emotions with words like "overjoyed" and "amazing," focus on the emotions you want your reader to feel by using more honest and effective words like "exclusive," "members only" or "one-time-only." By giving your reader the sense that she's being made part of something exclusive, you're tapping into her emotions in a way that will lead her to take your desired action.

### 3. Creative copy requires a lot of reading and writing, as well as an abandonment of rigid thinking. 

Getting your reader's attention with a snappy headline for something like an exclusive offer is only part of the challenge for a copywriter. You also have to hold that attention and get him to read beyond the headline. One of the best ways to do that is to use creativity when writing your copy.

Oftentimes, copywriters will use puns and maybe be a little cute or silly in their writing. But while humor can be a great tool, creativity is ultimately more than just making puns. True creativity is about solving problems with imagination and improvisation.

Sounds easier said than done, right? Well, there are ways to get your creative juices flowing.

One of the best ways is to keep writing — but don't just limit yourself to copywriting. Write the way you want, about whatever subjects you enjoy. Your writing may take the form of book reviews, character sketches or journaling. The more you write for yourself, the more you'll develop your voice and strengthen your creative muscles.

You should also think about reading more fiction. Novels are an endless wellspring of creative writing that can inspire useful ideas, and they often provide key insight into human emotions — the very thing your copy should be focused on.

Broadening your literary horizons can also help erode any rigid thinking that may be getting in the way of your creative thinking. You may find yourself writing copy to sell products that you have no personal connection to, and trying to reach customers you have very little in common with. So opening your mind to the inner lives of characters in novels who are nothing like you may prove inspiring.

Of course, only you know what kind of rigid thinking or biases you might have. So it doesn't hurt to ask yourself some tough questions like, "Do I feel uncomfortable dealing with what's unfamiliar?" Or, "Do I tend to be inflexible and block out anything that challenges my beliefs?"

If you're aware of some rigid thinking, one of the best things you can do is challenge yourself by changing up your routines, being open to new ideas and rigorously questioning your beliefs.

### 4. Writing for mobile and social platforms, or for content marketing purposes, requires special considerations. 

No matter what kind of copy you're tasked with writing, your client should give you a _brief_, which provides you with guidelines about the copy you're expected to write. These guidelines should include what's being promoted, what kind of benefit the product or service provides, what kind of brand voice needs to be used, and what kind of person the prospective reader is. 

If these things aren't included, do yourself a favor and ask for this information straight away. If you don't, you're likely to either struggle needlessly with the copy or turn in something that will need to be rewritten.

If your brief is to come up with copy for social media or mobile platforms, there are some specific things to take into consideration. 

As mentioned before, the aim is essentially the same: writing copy that connects with the reader's emotions and gets them to take a desired action. But there are aspects to social media and content for mobile devices that require a slightly different approach.

For starters, you need to think about how your content is going to look on the reader's screen. Most of us were taught that paragraphs are a collection of sentences centered around the same idea. And while this hasn't necessarily changed, a paragraph that looks normal on a printed piece of paper can look like a never-ending stream of words on a smartphone screen.

So try not to make your readers scroll any more than is necessary by being upfront with the point of your copy. And keep your paragraphs snappy, so that readers aren't confronted with a wall of text. 

Also, the tone of your copy will naturally differ from assignment to assignment, but social channels are generally best served by copy written in a personable and natural tone, rather than a stuffy or dry tone. Ultimately, you need to reflect the identity of the organization you're writing for, but no matter what the product or service is, if you're on social media you'll likely want to be casual and engaging.

### 5. Effective ways to engage readers include the use of curiosity, secrets and storytelling. 

So, you may be wondering, is there a formula for writing engaging copy? Well, the truth is there are countless approaches to capturing and retaining a reader's attention, but there are some fundamental ideas about how that copy should be structured.

One of the oldest formulas out there is known as AIDA, which stands for Attention, Interest, Desire and Action. You want to gain the reader's attention, hold her interest, and tap into her desires to convince her to take action.

In much the same vein, the author has his own formula called TIPS, which stands for Tempt, Influence, Persuade, Sell.

Tempting is essentially giving the reader an emotional reason to click your link or scroll down and continue reading. This often happens by writing something that piques the reader's curiosity, like telling the start of an intriguing story.

An example of a bad headline is "Over 2,500 PCs Rated & Reviewed!" A curiosity-inspiring version would be "Looking for a new PC? Pick our brains and you won't go wrong." This latter example is clear about who the intended reader is (someone looking for a new PC), and suggests an intriguing dialog between you and the reader.

Another engaging technique is to share secrets. If you suggest that you're in possession of exciting insider information, few readers will be able to resist leaning in and asking to hear more. 

This can work for everything from business tips to gardening tools. "Five leadership secrets you'll kick yourself for missing at business school!" "Top gardeners swear by this garden shed. But they won't tell you about it!" As you can see, it isn't even necessary to use the word "secret." 

Telling a story is another effective way to grab and hold a reader's attention. So keep the four elements of storytelling in mind: the protagonist, the problem, the description of what happened, and the resolution.

When used well, a story can take an enticing headline and keep the reader engaged until you deliver your pitch. Take this headline: "Keyed, crashed and left to rot in a barn. Then one man decided to do something amazing."

The copy may have been written to sell car parts, but the story is what will get the reader emotionally invested, to the extent that she may be convinced to make a purchase or sign up for a newsletter.

### 6. Using flattery and the wisdom of ancient Greece can yield good results. 

Some of the strongest emotions you can use in copywriting are those that stem from a person's ego. As the saying goes, "flattery will get you everywhere." So don't be afraid to use language that subtly inflates your reader's ego.

Let's say your product is an airline membership that can save people time and money. You could write something that begins "As a valued customer, we'd like to offer you…" But how many times have you read those words?

You know who your desired reader is, so there's no need to be so generic. Instead, you could write something specific and flattering, like, "As someone who spends many hours in the air…" 

If the pitch is intended for HR managers, you could write, "Exclusively for senior HR managers like yourself…"

Directly appealing to a person's ego is a powerful way to get that emotional connection and make the reader feel special. This works well when combined with a pitch that makes your service or product sound like an exclusive privilege, or an opportunity for your reader to indulge in a spot of luxury.

Believe it or not, much of this advice can be traced back to the days of Aristotle, the ancient Greek philosopher. He suggested that there were three kinds of effective persuasion: ethos, pathos and logos — otherwise known as character, emotion and argument.

Character is the reason why the reader should care about what you're saying. Are you an expert, and do you have secret knowledge or an intriguing experience to share? If so, lead with this and make the reader want to learn more.

Pathos, or emotion, is that connection to curiosity or desire that you can tap into. Readers are curious, and they desire better solutions to their problems, so use these compelling emotions. 

Logos, or argument, is about facts. Has your product been perfected over decades of fine-tuning? That's a compelling reason for your reader to consider buying it. 

Knowing your readers, and what their problems and desires are, will allow you to connect with them and engage their emotions in a useful way.

### 7. Avoid the common traps of copywriting and don’t pay too much attention to the grammar police. 

Good copywriting should always be pleasurable to read. And many of the techniques behind pleasurable writing, such as rhythm, pace, musicality and imagery, are developed over time. The more you read and write, the more you'll recognize and be able to utilize these valuable techniques.

But here's a good tip: use repetition to hammer home a point. For example, you could write, "Are you a copywriter? Then you're going to love what I have to offer."

But a better version might be, "Are you a copywriter? Are you a copywriter who's going places? Are you a copywriter with guts and ambition? Then you're going to love..."

Writing pleasurable copy is also about avoiding the common pitfalls that can make content cringeworthy.

The first is to avoid cliches, even if they're intended to be playful. There's always more than one way to say something, and you should always aim for an engaging way that hasn't been used a million times before.

Avoiding cliches can also help you to avoid misusing certain words, like "proverbial" and "literally." Is the prospect of early retirement the _proverbial_ carrot on the stick or the _metaphorical_ carrot on the stick? Since the carrot on the stick isn't featured in any ancient proverb, you should use "metaphorical."

Likewise, "literally" continues to be misused in place of its opposite, "figuratively." So, don't write that someone was "literally sweating blood," because that sounds horrifying, not playful.

That said, don't get _too_ distracted by the grammar police when writing your copy. Despite what overeager commenters may say, there are very few hard and fast rules when it comes to copywriting. What really matters is what's effective. 

Let's say there's a new electronic pen on the market with the tagline "Write Clever." Is it unusable because it should read "write cleverly?" We know which slogan the English teachers might prefer, but which one do you think is stickier?

Of course, grammar is important in writing, but often, what people call bad grammar is really an issue of style. Sentences ending in prepositions and nouns being used as verbs aren't the hate crimes against English that some make them out to be. In fact, such bending of the non-existent rules can be effective, and that's ultimately what counts.

### 8. Final summary 

The key message in these blinks:

**Good copywriting is empathetic, as it's all about understanding your reader's emotions and using those emotions to be persuasive. There are many techniques you can use to gain a reader's attention and emotional engagement. These include inciting curiosity and exploiting our natural interest in storytelling. It's also often a good idea to appeal to the reader's ego and his attraction to secrets and exclusivity.**

Actionable advice:

**Give life to your writing with these two tips.**

Dramatizing the benefits of your product or service can be an effective way to bring your writing to life. If you're pointing out low prices, testimonials or statistics, add flavor through dramatic enactments. For instance, if you're writing about a popular new e-book, instead of just quoting sales numbers, paint a picture of your reader on her morning commute, her train packed with people reading the product on their e-readers.

Using actual pictures is also a great way to make your copy more engaging. If you're trying to impress the reader with statistics, why not grab her attention with an image that illustrates these figures? If you're passing along a testimonial or telling a biographical story, use a photo of the person in question to make your story come more vividly to life.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _How to Be Heard_** **, by Julian Treasure**

Persuasive copywriting is all about finding ways to make your writing more attractive and convincing, but what if you have to come up with a strong verbal argument? If you want to be able to deliver a sales pitch as well as you can write one, then we recommend heading over to the blinks for Julian Treasure's 2017 book, _How to be Heard_.

If you've ever felt like people tend to tune out whenever you speak up, then it's time to learn how to captivate your audience's attention from word one.
---

### Andy Maslen

Andy Maslen has been a sought-after teacher of copywriting techniques for many years and is the CEO of the Andy Maslen Copywriting Academy. He's also the managing director of the corporate communications agency Sunfish. His clients have included _The Economist_, the _New York Times_, BBC Worldwide and the London Stock Exchange.

