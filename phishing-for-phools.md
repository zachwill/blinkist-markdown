---
id: 569e2df3f16f340007000055
slug: phishing-for-phools-en
published_date: 2016-01-20T00:00:00.000+00:00
author: George A. Akerlof and Robert J. Shiller
title: Phishing for Phools
subtitle: The Economics of Manipulation and Deception
main_color: F8F3B1
text_color: 5E5C43
---

# Phishing for Phools

_The Economics of Manipulation and Deception_

**George A. Akerlof and Robert J. Shiller**

_Phishing for Phools_ (2015) reveals the ways in which modern free-market systems, so often praised as the epitome of rational exchange, are fueled instead by willful deceit, with the goal of pushing you to act against your self-interest.

---
### 1. What’s in it for me? Don't be a phool! 

As part of your daily activities, you've probably purchased groceries, picked up some pills at the pharmacy, perhaps even sat down to watch a program on television or read a magazine. 

Yet did you know that with every step or action, you were being manipulated? 

No matter what you do, if you live in a "free market" system, your potential for being _phished_ is great. This practice of convincing you to take action that may be against your best interests is a potent tool used by advertisers and supermarkets — even politicians. 

These blinks show you how "phishermen," whether retail sellers or pharmaceutical executives, try to hook you into their net by spreading temptation and deceptive information — thus making you a _phool_!

In these blinks, you'll also learn

  * how phishing played a huge part in the 2008 financial crisis;

  * why Sunkist oranges are manipulating your mind; and

  * how companies hire "scientists" to boost their bottom line.

### 2. Far from being rational, free markets are full of irrational temptations for the “phool” consumer. 

While we like to think of free-market economic systems as places where individuals make mutually-beneficial trades based on rational decisions, the reality is much different. 

In today's free market, people are constantly being _phished for phools_. But what do we mean by this?

_Phishing_ is a process of getting a person to do something that is in the interest of the "phisherman" but not necessarily beneficial to that person. 

Someone who has been successfully phished is then called a _phool_. And contrary to popular opinion, markets that are based on supply and demand with little to no government interference — essentially free-market systems — are actually ideal phishing grounds.

Yet most economic textbooks will tell you that most buying decisions in such markets are indeed rational. A typical example goes like this: you go to a supermarket to buy apples and oranges. The catch is that you have only a limited amount of money to spend. 

The amount of apples and oranges you purchase depends on both the price of the fruit and on your personal preference for either apples or oranges.

But does this example reflect reality? Do we really make our buying decisions based on a rational assessment of a particular good's price? 

Certainly not. Free markets constantly create temptations to exploit consumer weaknesses.

Think about your local supermarket. Where are the eggs and milk located? In all likelihood, they've been placed strategically at the back of the store. 

As milk and eggs are common items that most people purchase, every customer is forced to walk through the whole store to find them — all the while being reminded of the other things on the store shelves that could be purchased. 

We're also similarly manipulated by our own desires when making purchasing decisions. 

For example, companies that sell cake mixes appeal to an individual's subconscious desire to make something "homemade." Instead of just including egg in the mix, they require the buyer to add a fresh egg himself, playing on the illusion that with this addition, the cake was made from "scratch."

> _"The free-market system exploits our weaknesses automatically."_

### 3. Reputation mining, one way to phish for phools, played a central role in the 2008 financial crisis. 

During the 2008 financial crisis, inflated housing prices in the United States finally popped, triggering a market crash that wreaked havoc in the US financial sector. 

The causes of the crisis have been extensively discussed. What has hardly been noted, however, is the role played by a kind of phishing called _reputation mining_.

To better understand reputation mining, let's create a scenario. If you have a reputation for selling beautiful, ripe avocados, then you have an opportunity to sell faithful customers a mediocre avocado at a price they would normally only pay for a perfectly ripe one.

By leveraging, or _mining_, your reputation, you will have phished your customers for phools by manipulating them into doing something that is in _your_ interest — but not theirs.

This is exactly what happened in the years leading up to the 2008 financial crisis. 

Established credit rating agencies in the United States, such as Moody's and Standard & Poor's, had built up a solid reputation by reliably _rating bonds_ — or determining the security of loans granted to corporations or governments. 

To illustrate how this scenario played out, imagine these bonds were regular avocados.

In addition to rating bonds, in the early 2000s rating agencies also began to evaluate more complex financial products — let's call them a new, exotic variety of avocado.

Banks had little incentive to offer beautiful, ripe avocados of this new exotic variety. They could offer mediocre or even rotten avocados — or complex financial products with high chances of default — and take them to the rating agencies, which would then mine their reputation by giving the products top ratings. 

But why would they do that? Well, rating agencies charged _banks_ for ratings, so the banks were able to pressure the agencies by leveraging their bargaining position: low ratings meant no future business. 

So when investors finally discovered that some of the exotic financial products were actually "rotten," their value plummeted, triggering the financial crisis.

> _"A great deal of phishing comes from...supplying us with misleading or erroneous information."_

### 4. Advertisers phish when they create stories that get into our heads, playing on our emotions. 

Advertising is the best place to observe phishing in its purest form. But phishing also occurs whenever you pay for purchases with "plastic," like a credit card. 

Savvy advertisers know that the human mind processes information as a narrative. Far from being free-form or abstract, our thoughts often run on in the form of a conversation, in which one voice speaks and another responds. 

You can think of most advertising as an attempt to either engage you in conversation or to graft stories onto the conversation in your head — all with the goal of getting you to buy a product.

For example, advertising agency Lord & Thomas created the "Sunkist" orange as an attempt to influence consumers by creating a simple story: that company oranges were "sun kissed." 

The warm, joyful words "sun" and "kissed" are intended to evoke positive feelings, creating an appealing narrative in a consumer's mind. 

Yet phishing doesn't just happen when you're deciding what to buy. Sometimes you might be phished when deciding how to pay for your purchases.

The choice to use cash or credit may seem a straightforward one. When you have cash in your wallet, you use cash; when you're short of cash, you pull out a card. 

Yet it's not so simple. Consumers can actually be influenced by subtle cues that push a person to pay a certain way. And research suggests that if you pay with a card, you're likely to spend more than if you were limited to paying with cash.

A study conducted by psychologist Richard Feinberg revealed that tips left by people paying by credit card were actually 13-percent greater than tips left by cash customers. Another study showed that people with credit cards purchased more items in a department store than people who don't have a card and paid only cash.

Store owners who encourage purchasing with "plastic" are in essence phishing for phools, as this sets up a scenario in which a customer might spend more than she would if she could only pay with cash.

### 5. Politicians phish by not giving voters sufficient information on policy; big pharma phishes in a similar fashion. 

Phishing isn't just reserved for store owners and advertisers — you can also find evidence of phishing in the healthcare industry and even in a democracy. Basically, in any exchange where access to information may affect how you decide, you'll find a phisherman. 

Indeed, a voter is a highly phishable individual, as he or she is seldom fully informed on a given issue or candidate.

Take the US Emergency Economic Stabilization Act of 2008, as just one example. This law was used to bail out a significant portion of the US banking system, as well as automotive companies General Motors and Chrysler, both on the edge of bankruptcy following the peak of the financial crisis. 

Yet even those with insider information couldn't have foreseen that the law would have been used in this fashion. Not even the authors of _Phishing for Phools_, who had procured a copy of the act, were able to identify the specific language in the act that authorized the financial bailout of certain banks and auto manufacturers.

It would seem that being truly, fully informed about any piece of legislation is an impossible task. But when a voter lacks information, he becomes an "information phool" and can be easily persuaded to agree with laws or policy that run counter to his interests. 

Similarly, consumers of pharmaceutical products who lack critical information about the pills they take are at risk of being phished.

Pharmaceutical company Merck, for example, in 1999 brought its new painkiller Vioxx to market. A study — financed by the company — suggested that Vioxx was a wonder drug. Yet later, independent studies revealed that Vioxx had serious side effects and could potentially cause heart attacks.

More generally, a significantly higher portion of articles published in medical journals — when sponsored by the pharmaceutical industry — offer a favorable spin on research or products than when the article or research is funded by outside sources.

In this way, pharmaceutical companies phish for phools by spreading biased information about the safety and effectiveness of drugs, increasing revenues without necessarily improving consumer health.

### 6. Innovations open paths for phishing, too – and the tobacco industry is the master of them all. 

When people think "innovation," they often think "progress." After all, economists say that innovation lies at the heart of economic growth. But not all innovations are for the best.

Quite a few innovations are designed specifically to take advantage of our vulnerability to phishing. 

For instance, United Airlines discovered a remarkable phishing concept: boarding class. On large planes, the order in which passengers board depends on a status designated by the airline (such as Premier Platinum, Gold or Silver).

People are easily attracted to these kinds of superficial rankings, thus allowing United Airlines to phish customers by enticing them to collect miles — by purchasing more tickets — to attain these coveted "elite" positions.

Another one-sided innovation was the cigarette-rolling machine, invented by James Bonsack in the 1880s, a device which greatly reduced the costs of cigarette production. 

While this innovation was great for manufacturers, the mass production of inexpensive cigarettes led to increased rates of nicotine addiction and lung cancer.

The tobacco industry in particular takes advantage of our vulnerability to phishing. From the 1920s to the 1940s, smoking was considered sophisticated, sexy and cool — associations the tobacco industry helped create.

When scientists began to present evidence that cigarette smoking could cause cancer, the tobacco industry used phishing to create doubt. 

Tobacco companies hired their own "scientists," who would strongly voice the opinion that the link between smoking and cancer was yet "unproven." Crucially, the confused public had a tough time differentiating one "scientist" from another.

Such disinformation allowed the tobacco industry to obscure the real effects of smoking and continue taking advantage of people's vulnerability to nicotine addiction.

> _"...if phishing for_ phools _is important anywhere, it is in the four great addictions: tobacco, alcohol, drugs, and gambling."_

### 7. Smart laws and standardization can help prevent consumers from being phished. 

Nobody likes being duped. So how can you prevent becoming a phished phool? Here are a couple of ways you can protect yourself as a consumer.

First, standardization is a great tool in the fight against phishing by making it more difficult for companies to spread misleading information.

Wheat standardization is one real-life example. Wheat comes in many different varieties, and is subject to many different types of imperfections. One bag of wheat may have an usually high number of damaged kernels, for example, making the product less desirable. 

However, as we've learned in a previous blink, companies can leverage their reputation and sell subpar bags of wheat through reputation mining. Or, more to the point, they could simply give an imperfect bag of wheat a misleading "perfect" label.

Yet the US Department of Agriculture has established rigid systems for classifying, grading and labeling wheat, all with the goal of protecting consumers from phishing producers. Additionally, federal inspections of facilities and products are performed regularly. 

Second, federal and state laws can also provide protections against phishing. 

Consider that all US states have enacted some form of the Uniform Commercial Code, which imposes an obligation of "good faith" in commercial contracts to prevent interested parties from being duped by a contract's fine print. 

Moreover, the code distinguishes between "consumers" and "merchants," and holds typical consumers less responsible for inspecting the fine print than the presumably more sophisticated "merchants."

So how does this law work in reality? Imagine that you think you've purchased a toaster, but the fine print on the packaging says that the toaster is actually an ice cream maker. 

Of course you didn't read the fine print and are dismayed when you unpack your "toaster" at home! 

Laws like the Uniform Commercial Code protect you from being told by the toaster company that you should have known better, since the "information" was in the fine print.

### 8. Final summary 

The key message in this book:

**Traditionally, Economics 101 textbooks omit a widespread phenomenon in free-market systems, the manipulation of people into doing things that aren't in their self-interest — better known as phishing for phools. Whether you're making purchases or listening to politicians, the risk of being duped is real and ever present.**

Actionable advice:

**Make a budget according to the 50-30-20 rule.**

Divide your take-home pay into three parts. Fifty percent is reserved for "must-haves," such as food, rent and toilet paper; 30 percent goes to "wants" (new shoes, movie tickets, fine dining); and finally, 20 percent is for savings. If you stick to your budget, you're less likely to be phished, as strict limits on spending makes it easier to resist temptation.

**Suggested** **further** **reading:** ** _Social Engineering_** **by Christopher Hadnagy**

_Social Engineering_ (2011) reveals the secret methods hackers and con artists use to manipulate their targets and scam their victims. The book provides detailed step-by-step depictions of how criminals plan a scheme, and gives you all the tools you need to prevent yourself from being duped.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### George A. Akerlof and Robert J. Shiller

Nobel laureate George A. Akerlof is an economist and professor at Georgetown University. He was awarded the Nobel Prize in economics in 2001.

Nobel laureate Robert J. Shiller is the Sterling professor of economics at Yale University. He was awarded the Nobel Prize in economics in 2013. Shiller is the author of the bestselling book, _Irrational Exuberance_.

