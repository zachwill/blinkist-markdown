---
id: 5649fe576634310007490000
slug: physics-of-the-impossible-en
published_date: 2015-11-18T00:00:00.000+00:00
author: Michio Kaku
title: Physics of the Impossible
subtitle: A Scientific Exploration of the World of Phasers, Force Fields, Teleportation and Time Travel
main_color: D12A3A
text_color: D12A3A
---

# Physics of the Impossible

_A Scientific Exploration of the World of Phasers, Force Fields, Teleportation and Time Travel_

**Michio Kaku**

Just how unrealistic is the technology we see in sci-fi novels and television shows? In _Physics of the Impossible_ (2008), renowned physicist Michio Kaku takes a mind-bending look into how far away we really are from such fantastical notions as starships traveling faster than the speed of light or teleporting to different planets.

---
### 1. What’s in it for me? Prepare for science fiction to become reality. 

Many of today's technologies were once only the fantastical inventions of science fiction. Did you know that nineteenth-century science fiction author Jules Verne already fantasized about devices like fax machines, a worldwide communications network and rocket ships to the moon? We take these technologies for granted today, but Verne's contemporaries, even the most prominent physicists, laughed them off, convinced that they were impossible.

Even if you're watching science fiction movies that aren't all that old, you may find that some of the technologies depicted would be considered crude today, like oversized computers with lots of blinking light bulbs. While we don't travel through other galaxies wearing catsuits quite yet, in some regards, today's science already has surpassed yesterday's fiction. 

And what about technologies that seem impossible today? Which of them will be realized in the future and how? That's what you'll learn about if you read on. 

In these blinks, you'll find out

  * why, soon, you won't have to be a psychic to read your lover's mind;

  * whether you should start saving money for a handheld laser gun; and

  * what it takes to travel faster than light.

### 2. There’s real science behind the ideas of force fields and invisibility cloaks. 

Remember the force fields used in _Star Trek_? Formidable energy barriers that protected starships from rockets and enemy fire? The stuff of fantasy, right? Actually, force fields are known to classical physics _._

As we know, many objects can exert an influence over other objects in their vicinity without direct contact with them. For instance, a magnet attracts or repels things that lie within a certain field around it. In the nineteenth century, a British scientist named Michael Faraday came up with the concept of _force fields_, invisible areas or lines of force that envelop a magnet. Later, the concept expanded to include other forces, such as the earth's _gravitational field_. 

Sure, these aren't the force fields we know from science fiction, but they could help us create them. It might even be possible to develop force fields that deflect rockets.

Here's how: When gas is exposed to extreme heat it becomes _plasma,_ an electrically charged mass that's neither solid, liquid nor gas. This plasma could then be molded by magnetic and electrical fields to form an invisible sheet or plasma window. This _force field_ could then be reinforced with a lattice of _carbon nanotubes_ : nanoscopic cylinders made of thin, rolled-up sheets of carbon. Carbon nanotubes are stronger than steel and could deflect rockets.

But what if you wanted instead to deflect someone's attention, say, with an invisibility cloak? This, too, isn't impossible!

Our ability to see depends on the light that objects reflect. The more light that passes _through_ a material rather than being reflected by it — such as what happens with a gas or liquid — the less visible the material. But there's another way that things can become invisible: in 2006, scientists at Duke University developed composite materials known as _metamaterials_ that contain small particles which _deflect_ as opposed to _reflect_ light waves. Any objects enveloped in such material are virtually invisible.

> _"If at first an idea does not sound absurd, then there is no hope for it." - Albert Einstein_

### 3. Phasers and death stars might come to exist outside the world of Star Wars. 

From the Greek mathematician Archimedes to the _Star Wars_ movies, we've been enthralled by the idea of using lasers or rays of light as weapons. But how realistic is it to think we'll someday fire handheld phaser guns or harness the planet-destroying force of George Lucas's Death Star?

With advances in nanotechnology, a handheld laser gun could become possible in the not-too-distant future. In fact, the military already uses lasers in their missile defense systems. 

However, to produce a compact laser gun with the ability to fire multiple times, we'd need a portable power source for the lasers and we're not quite there yet. And although nanotechnology may one day enable us to create miniature batteries that could store enough energy for a functioning laser gun, we would also need to find a stable material that could withstand that much energy.

So, while you might be fortunate or unfortunate enough to witness the first handheld laser gun in your lifetime, there's one thing you'll thankfully probably never encounter before you die: planet-demolishing super-lasers.

Millions of years could pass before we find a way to power such super-lasers, but they don't actually go against the laws of physics.

To this day, we know of one kind of beam with an almost inconceivable power: the gamma-ray burster. Gamma ray bursters are enormous extragalactic blasts of radiation thought to have emerged during the creation of a black hole.

It is theoretically possible that we could predict a gamma ray blaster and manipulate it to aim in a certain direction, but it would be millions of years in the future.

### 4. In theory, teleportation is a real possibility, but it’ll take ages before we can teleport humans. 

Remember the last time you were stuck in a traffic jam, daydreaming about "beaming" yourself to your destination? This idea of _teleportation_ involves transferring matter, energy or information from one location to another without actually moving it through physical space, and it's not as outlandish an idea as you may have thought.

According to quantum theory, teleportation occurs spontaneously all the time. That is, electrons are constantly taking "quantum jumps," meaning they can suddenly vanish and reappear elsewhere within the atom, or even appear in many different locations simultaneously. That is to say, it's theoretically possible that one day you could get to where you need to — a meeting, a party or Mars — instantaneously.

There's also a remarkable phenomenon called _quantum entanglement_ which happens when you separate two electrons that vibrate synchronously. Even when they're literally miles away from each other, whatever you do to one electron changes the state of the other. That is, the information of one is transmitted to the other.

Even more fascinating is that scientists have already succeeded in teleporting objects. By harnessing quantum entanglement, physicists have actually teleported trillions of atoms entangled with a beam of light over long distances. 

What many don't know is that when we "teleport" an atom, what is really teleported is the information about its state, such as the way it spins. The atom itself is not physically transmitted. In a sense, teleporting an object from point A to point B requires reconstructing it at point B based on the teleported information.

At extremely low temperatures, most atoms become entangled, so scientists are experimenting with one of the coldest materials in the known universe — a Bose–Einstein condensate — in hopes of teleporting increasingly large objects.

But teleporting humans could take us a little longer given that, currently, teleportation can only take place under extreme conditions. Also, teleporting something as complex as a human body could well require the calculations of a quantum computer, and that kind of technology is still in a pretty rudimentary phase.

### 5. Mind-reading and moving objects with your mind might become possible. 

Who wouldn't want to read minds? For over a century, scientists have been investigating the claims of psychics and looking into the technological possibilities that could enable mind-reading and other kinds of _extrasensory perception_ (ESP), including _psychokinesis_, that is, the ability to move objects with the mind. 

While there's no solid evidence that people can read minds, technology is making leaps and bounds in reading brain activity. However, since the electrical signals emitted from the brain are so weak, they aren't readable and, even if we could attach an antenna to them, we have no way to unscramble the signals.

Currently, though, MRI technology is helping scientists identify human brain patterns. So far, they're already compiling a "dictionary of thought" that can translate these patterns into human emotions.

Yet, while MRI technology is edging nearer to creating handheld gadgets capable of reading tiny magnetic fields, it's still a far cry from deciphering the plethora of individual thoughts a human mind can produce, or from mapping the hundreds of billions of neurons in the human brain.

What about psychokinesis? The closest we've come to recreating this phenomenon is through the process of biofeedback, communicating directly to a computer through the electronic transmission of brain waves.

Modern technology has made it possible to use implanted chips to read brain waves and translate them into commands. This has allowed paralyzed people to not only control devices but perform complex tasks and even play video games.

By the next century, such technology may be developed to a point where we can control nanotechnology through biofeedback and perform tasks in a way that would seem like pure magic today.

### 6. Science is still struggling to develop truly smart robots or computers. 

From Rosie in _The Jetsons_ to the Terminator, we have been fascinated by the idea of robots and artificial intelligence for generations. But while modern computers can perform mind-boggling calculations in the blink of an eye, there are some very basic things they can't do.

Like speaking a language, for example. Whereas machines can be programmed to combine symbols and form grammatically correct sentences, they can't comprehend the meaning behind words, and many theorists claim they never will. So far, computer scientists have failed to program common sense into machines and pattern recognition remains poor in computers.

The idea may seem executable: pin down the rules of common sense, and translate them into an algorithm that can be fed to the computer. For instance, one rule could be "fire can be dangerous."

But the problem is there are _millions_ of such rules. 

Consequently, decades of programming haven't seen any victories in endowing computers with common sense. Similarly, programming machines to recognize patterns hasn't worked out well, either. Say you want to navigate a room: you need to recognize obstacles and find a suitable route around them. While humans can instantly do this, robots see only lines and curves and they lag far behind in making sense of them.

One new approach to creating artificial intelligence, however, allows robots to learn from experience, just as animals and humans do. When we're infants, we don't memorize rules about common sense. Rather, our common sense comes from experience: when we touch water we find out it's wet. In this way, we learn to navigate rooms by trial and error, stumbling and bumping into walls as we go.

Drawing inspiration from this, MIT professor Rodney Brooks developed bug-like robots that learned to walk based on experience. This worked so well that some of his robots are now on Mars, collecting data for NASA.

> _"The Newton of AI probably has not yet been born."_

### 7. We haven’t found extraterrestrial life yet, but scientists are looking. 

Are we the only intelligent beings in the universe? We've asked this question for centuries, but are we any closer to finding an answer?

Scientists are certainly making steps toward finding other intelligent life-forms: our telescopes are getting better and our capacity to interpret what they show us is improving, too. So much so that a new extrasolar planet is discovered twice a month. Also, as we continue to send out satellites, the likelihood of finding signs of life elsewhere in the universe increases.

While 30 years of insufficient evidence have elapsed, scientists at the Search for Extraterrestrial Intelligence (SETI) project are still broadening the scope of their search in hopes of picking up interstellar signals. Science is also finding more and more criteria for habitable planets, which allows us to scout for life in the most promising places.

One thing we know is that life on earth is impossible without water and this may be true for life forms on other planets, too. Therefore, the presence of water has been a key indicator in determining whether a planet could sustain life or not.

Over the years, though, other factors have been found to be indicative of a planet's habitability. One such factor is a large moon, as this is necessary to stabilize a planet's axis. Without it, a planet is massively unstable, which can induce extreme weather conditions and render it uninhabitable.

Also, we can look for the presence of a Jupiter-like planet in a planet's galaxy, as this would help keep it safe from roaming asteroids.

Yet, despite our efforts, we have very little evidence of the existence of extraplanetary beings. In fact, 95 percent of UFO sightings can be chalked up to natural phenomena such as atmospheric anomalies, hoaxes or sightings of secret aircraft projects.

The remaining five percent, though, such as the CIA-documented sightings of UFOs over Iran in 1976, have yet to be explained.

### 8. Space technology is advancing, but there are still huge challenges ahead. 

In a few billion years, the sun will swell and engulf the Earth. If humanity is to survive, we'd better figure out a way to leave the solar system. So how could we do this?

It starts with finding the right fuel source to send a starship across the universe. It's no easy feat, but science is already exploring different options. 

Two possibilities are ion and plasma engines. Plasma engines heat hydrogen gas to a million degrees Celsius, at which point it becomes plasma, and then emit the plasma in a powerful jet. Similarly, ion engines eject ion beams to propel the spaceship. NASA already used an ion engine to fuel Deep Space 1 in 1998.

Another way to propel a spaceship could be by use of solar sails, which use the low but steady pressure of sunlight. But for a starship big enough, the sails would have to span hundreds of miles. And creating such a sail and casting it into space is, unfortunately, not plausible with our current technology.

Next, there are ramjet fusion rockets. By triggering a thermonuclear reaction in hydrogen gas, they would yield an enormous amount of energy. This could enable a starship to travel at 77 percent of the speed of light, meaning it would take a crew a mere 23 years to reach the Andromeda galaxy. One problem would be that the space needed to store the fuel would necessitate a ship so vast it would need to be constructed in space. However, the keen minds at NASA are already looking into the possibilities of a "space elevator" to transport building materials into space.

Another challenge is to make space travel less dangerous. For starters, we need to protect ourselves from radiation, as we're wide open to deadly levels of it without the Earth's magnetic field and atmosphere. Then there's the issue of weightlessness. Without the constant pull of gravity, our muscles atrophy and our bones deteriorate. In fact, after a year in space, astronauts are so weak that they're unable to walk.

> _"The finer part of mankind will … never perish — they will migrate from sun to sun as they go out." - Konstantin E. Tsiolkovsky, Soviet rocket scientist_

### 9. Einstein stated that the speed of light is the limit of human travel – but he may be wrong. 

Einstein theorized that the speed of light was the ultimate speed in the universe but physicists have discovered two possible loopholes in his thinking. 

The first is _warping space_. If you imagine space as a piece of paper, and you want to get from one end of the page to the other, then warping space is like folding the piece of paper so the two ends meet. Einstein's equations state that the warping of space-time can be calculated, given a certain amount of mass or energy. But, if you start calculating with _negative_ mass or _negative_ energy, you can come up with results that are faster than light, and thus contradict Einstein.

One scientist who worked on negative mass and negative energy calculations is physicist Miguel Alcubierre. He proposed the _Alcubierre drive_ and it would work as follows: First, you'd need enough negative energy to create a bubble in space-time to envelop a spaceship. Then, whenever that bubble moved from point A to point B, it would compress the space ahead of it and expand the space behind it. If the highest velocity for passing a non-compressed route from point A to point B was the speed of light, you could reach point B faster than light once the route was compressed.

So how would we find this negative energy? It has been measured in a laboratory, but so far only in miniscule quantities.

Another loophole in Einstein's theory are wormholes in space. Einstein's theory allows for shortcuts — known as _wormholes_ — between two points in space-time _._ If we were to utilize these, we may be able to cross immense distances as we pass freely back and forth through them.

Unfortunately, we would need a colossal amount of negative energy to do this. One the size of Jupiter, in fact. And it would only be enough to open up a one-meter wormhole. To add to this, we'd also be exposed to lethal amounts of radiation.

> _"It is impossible to travel faster than the speed of light, and certainly not desirable, as one's hat keeps blowing off." - Woody Allen_

### 10. Time travel may be a challenge and it leads to paradoxes, but it doesn’t violate the laws of physics. 

The notion of time travel has made for captivating science fiction. And although eminent physicist Stephen Hawking has contested the possibility of time travel since we haven't yet encountered a time traveler, there isn't anything about the idea that violates the laws of physics or quantum theory.

In fact, humans have already traveled into the future, albeit in a very minor way.

Time travel adheres to Einstein's special theory of relativity in that it states that the faster a rocket moves, for example, the slower time will pass for the passengers inside it. So, if you're moving through space quickly enough, you can travel into the future _relative_ to those on Earth. This is the case for Russian astronaut Sergei Avdeyev, who orbited Earth for 748 days and holds the world record for traveling .02 seconds into the future.

But what about going _back_ in time? This poses a tremendous challenge, but is still possible. According to Einstein's theory, time and space are intimately connected. Therefore, if a wormhole connects two points in space, it could also connect two places in time. Much like traveling faster than light, traveling back in time would require wormholes and enormous quantities of negative energy to create openings in space-time.

Of course, time travel comes with some confounding paradoxes. For instance, if you traveled into your past and killed your parents before your birth, you wouldn't exist. But if you didn't exist, you couldn't go back to kill your parents before your birth, so you'd have been born! But, this paradox may be resolved if we assume that a parallel universe emerges whenever you go back in time. That is, the past you came from would be a different past from the one you travel to, although it would appear identical and your parents would be genetically identical to the parents you were "actually" born to. 

Most of these technologies are on the fringe of our current knowledge, and should we ever realize them, we'd need to rewrite some fundamental physical laws.

> _"A paradox is truth standing on its head to attract attention." - Nicholas Falletta_

### 11. For centuries, the perpetual motion machine has been the stuff of dreams for inventors. 

What did both Leonardo Da Vinci and Nikola Tesla consider the holy grail of invention? The perpetual motion machine — a device that can produce more energy than it consumes.

As our population expands, satisfying our energy needs is becoming a crucial subject for many of today's scientists, but we could combat our problems with a perpetual motion machine.

The laws of thermodynamics indeed state that it's impossible for anything to produce more energy than it consumes. But there are loopholes in this law and scientists are continuing to delve into the possibility of extracting energy from nothing, or from a vacuum. Nikola Tesla experimented with this notion, using what is known as _zero-point energy_, but without much success.

However, today this theory has reemerged thanks to _dark energy_, which is found in total vacuums. Data analyzed from satellites orbiting the Earth have concluded that 73 percent of our universe is made up of this dark energy.

Despite physicists' experiments proving dark energy's existence, they have so far been unable to explain or calculate this energy.

What's more, actually harnessing this energy is quite a different matter. Up to now, only the tiniest quantities have been found on Earth — nowhere near enough to power a perpetual motion machine. Yet, if dark energy could be used to power such a machine, it would spark an unimaginable shift in our world. 

We're already constantly pushing the boundaries of technology, and things that we once deemed impossible are already being used on a daily basis: the internet is a prime example. However, making a perpetual motion machine wouldn't just change our world, it would necessitate the rewriting of physics as we know it.

### 12. Physics might be close to finding groundbreaking answers. 

Einstein spent the latter part of his life chasing one ultimate goal: a _theory of everything._ That is, one unifying theory that could explain all the fundamental forces of the universe, including gravity, electromagnetism, and weak and strong nuclear force. This theory would encompass all physical laws and enable us to explain the origins of the universe. But he was not successful.

Today, however, thanks to the continued exploration of space and to quantum physics, we are closer than ever before to developing a theory of everything. 

Though we're still missing some pieces of the puzzle, advancements in technology, such as satellites with radiation detectors that can measure radiation from a mere 300,000 years after the big bang, mean we're moving closer to revealing the origins of our universe.

These radiation detectors are even making progress towards tracking neutrino radiation, elements so elusive they were once thought to be impossible to find. Tracking these could take us within _seconds_ of the big bang and, with all this information, we could potentially be led to a testable theory of everything. That testable theory could be _string theory_.

String theory isn't a new concept: it has been worked on since 1968 with the aim of tying Newton's laws of gravity and Einstein's theory of relativity to quantum theory, thus devising one all-encompassing theory.

In string theory, particles can be modeled as "strings" that vibrate and interact with each other. One particular state of vibration corresponds to a quantum mechanical particle called the graviton. In this way, string theory can tie the laws of gravity to the laws of quantum theory. It can also explain the hundreds of subatomic particles that have emerged from particle accelerators over recent years, as these are all simply different vibrations of the string.

The team behind the Large Hadron Collider are hoping to soon discover superparticles, which would indirectly test and support string theory as the unifying theory. If this happens, this would call into question once more what is possible or impossible in the world of physics.

> _"The ground of physics is littered with the corpses of unified theories." - Freeman Dyson_

### 13. Final summary 

The key message in this book:

**The far-fetched concepts behind our favorite science fiction books and movies are not only entertaining: they serve as a window into the possibilities of tomorrow and remind us that, in the world of science, a so-called impossibility is simply a challenge to be overcome.**

**Suggested** **further** **reading:** ** _Physics of the Future_** **by Michio Kaku**

_Physics of the Future_ lays out predictions of future technology based on the works and opinions of experts on the cutting edge of physics, genetics, biology and computer science. The author explores some of the hurdles we will have to overcome in order to develop these future technologies, and what fundamental changes we can expect their presence to make on our society.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Michio Kaku

Acclaimed physicist Michio Kaku holds the Henry Semat Chair in Theoretical Physics at the City University of New York. He has presented the popular BBC series _Time_ and _Visions of the Future_, and is the best-selling author of _Hyperspace_ and _Parallel Worlds_.

