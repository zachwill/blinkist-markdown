---
id: 50d7294be4b045383aa45e1e
slug: pitch-anything-en
published_date: 2013-03-01T00:00:00.000+00:00
author: Oren Klaff
title: Pitch Anything
subtitle: An Innovative Method for Presenting, Persuading, and Winning the Deal
main_color: C1272E
text_color: C1272E
---

# Pitch Anything

_An Innovative Method for Presenting, Persuading, and Winning the Deal_

**Oren Klaff**

_Pitch Anything_ (2011) introduces a unique, new method for pitching ideas. Through psychology, neuroscience and personal anecdotes, Klaff explains the tactics and techniques needed to successfully pitch anything to anyone.

---
### 1. You must tailor your pitch to the audience’s croc brains. 

Everyone should learn to pitch ideas well. In every profession, from dentistry to investment banking, there comes a time when you must convince someone of something. Unfortunately, there is a gap between what we are trying to tell our audience and how they perceive it. To understand this gap and overcome it, we must look at the evolution of the human brain.

Basically, the human brain has evolved in three separate stages, resulting in three distinct parts: the primitive reptilian part, the _croc brain_, developed first. It's a simple device primarily focused on survival and it can generate strong emotions, like the desire to flee a predator. Next, the _midbrain_ developed. It allows us to understand more complex situations, such as social interactions. Finally, the sophisticated _neocortex_ evolved, facilitating reasoning and analysis to understand complex things.

When you pitch, you use your neocortex to put into words the ideas you are trying to convey. Unfortunately, your audience doesn't at first process these ideas with their neocortices. Instead, it is the audience's primitive croc brains that receive the ideas and they ignore everything that is not new and exciting. Worse still, if your message seems abstract and unfathomable to the croc brain, it might perceive the message as a threat. This will make your audience want to flee to escape the situation.

This is why you must tailor your pitch to the croc brain. Since croc brains are simple, your message should be clear, concrete and focused on the big picture. You also need to ensure the croc brain sees your message as something positive and novel, which deserves to be passed on to the higher brain structures.

### 2. To secure your target’s attention, you must create desire and tension. 

The one critical thing you need throughout your pitch is the _attention_ of your target. To successfully attain this, research has shown that you must evoke two sensations in your pitch: _desire_ and _tension_. Desire arises when you offer your target a reward, and tension arises when you show them they might lose something, like an opportunity, as a result of this social encounter. On a neurological level, this effectively floods your target's brain with two neurotransmitters: _dopamine_ and norepinephrine.

Dopamine is a chemical associated with anticipating rewards — desire. One such reward would be the pleasure of understanding something new, such as solving a puzzle. Thus, to increase the level of dopamine in your target's brain, you must introduce _novelty_ through a pleasant surprise, like an unexpected yet entertaining product demo.

Norepinephrine, on the other hand, is the chemical responsible for alertness and it creates tension in the target. If your pitch convinces them that there is a lot at stake here, their brains will be flooded with norepinephrine.

To create tension, you must create a bit of low level conflict with a _push-pull strategy_. This means first saying something to push the target away, like, "Maybe we aren't a good match for each other." You then counter this by pulling the target back toward you with something like, "But if we are, that would be terrific."

This push-pull dynamic creates alertness in the target, as they sense that they might lose this opportunity. Depending on the situation, you may use very powerful push-pull statements, especially if you sense your target's attention beginning to wane.

### 3. To control a meeting, you must first establish frame control. 

Different people will see any given situation from a different perspective or point of view based on their intelligence, ethics and values. These perspectives are called _frames_, and they dictate how we perceive social situations such as meetings and sales pitches. Frames also determine who controls those situations.

When two people meet, their individual frames crash into each other. Only one frame can survive such an encounter — the stronger one. For example, let's assume a cop pulls you over for speeding. He has a strong moral-authority frame and you only have a weak "I'm so sorry officer"-frame. It is clear that when your frames clash, his frame will prevail. This means he will control every aspect of the encounter: from its duration to its content and tone.

You will often face a similar clash of frames in a business environment; for example, a customer may be focused on the price of your product while you are focused on its quality. You will both try to get the other to focus on what you think is important.

If it is your frame that survives this clash, you will have _frame control_ in the situation meaning your ideas and statements will be accepted as facts by the customer. This is a crucial advantage in any pitch. Without frame control, you are unlikely to convince anyone of anything.

### 4. You will often encounter the power frame, time frame and analyst frame, hence you must know how to counter them. 

In a pitch or sales meeting you will often encounter certain archetypes of frames, and it is important you choose strong with which to counter them.

Typically, your target will use the _power frame_ which exudes arrogance. You must not do anything that validates the other person's power. Instead, use small acts of defiance and denial to bust the frame; for example, by yanking your presentation material away from the target if they do not seem to be taking it seriously.

Another oft-used frame is the _time frame_, where your customer asserts control over time: "I only have ten more minutes." This is meant to push you off balance, but you can always counter with: "That's fine, I only have five."

A particularly lethal frame is the _analyst frame_, denoted by a fixation on details and numbers. If your opponent is in this frame, they will likely insist on drilling down into minor technical and financial details, effectively bogging down your pitch.

In such situations, give a direct but high-level answer to the question asked and get right back to your pitch. Analysis comes later. Before more questions come up, counter the analyst frame with your own _intrigue frame_. This basically means you tell a compelling personal story and leave it unfinished as a cliffhanger: "… so there we were, in a pitch black, falling airplane with no idea what was going to happen. Anyway, back to the pitch …" This redirects the focus of the room onto you and makes the discussion personal once again.

### 5. Use prizing to make the target seek your acceptance. 

The most important frame you should be able to use is the _prize frame_, as it works in a variety of situations against many opposing frames.

Typically when you're selling something or pitching an idea, your target will tend to see their money as the "prize" of the meeting, something you have to fight for. You must reframe the situation so that _you_ are the prize and they would be lucky to do business with you.

Because people tend to want things they can't have, prizing yourself will make your target work for your acceptance instead of the other way around. BMW does this with a special-edition M3. The company demands prospective buyers sign a contract assuring they will take proper care of the car, otherwise they cannot buy one.

In a pitching situation, never engage in behavior that makes it seem as if you are chasing the target, for example by agreeing to last minute schedule changes or trying to prematurely close the deal by saying things like, "So, what do you think so far?" Such behavior only reinforces the impression that the target is the prize. Instead, get your target to explicitly qualify themselves to you; for example, you could say, "I am very particular about with whom I work. Why should I do business with you?" This usually catches them off guard and they start trying to impress you.

### 6. Stack frames to trigger hot cognitions. 

Contrary to popular belief, we are more prone to making choices instinctively than through rational analysis. In fact, we often make a decision about something before we even fully understand it and only later come up with reasons for that decision. These gut calls are called _hot cognitions_, whereas the decisions arrived at through rational reasoning are known as _cold cognitions_.

After you've introduced your big pitch idea, you want to trigger hot cognitions within your target. These will make him or her want what you have to offer in mere seconds, instead of analyzing your pitch for days to reach a rational, cold decision. You trigger the hot cognitions by _stacking frames_, meaning you introduce multiple frames in quick succession.

The first frame is the _intrigue frame_ : you tell your target a compelling story, a personal narrative where a dilemma is solved. At the crucial juncture, you stop telling the story, leaving your target on the edge of their seat, ensuring their full attention.

Next, you pile on the prize frame, where you flip the tables on the target: instead of trying to impress them, make them qualify themselves to you. You could say something like, "This deal has so many investors after it, I have to choose who to take on board."

After this, you stack on the _time frame_ by adding time pressure to the pitch: "Unfortunately, this is a limited-time offer, and the train, so to speak, is leaving the station on Monday." This will make the target feel like they are losing an opportunity, at which point they will want it even more.

By triggering all these hot cognitions in the target, you will leave them drooling for what you have to offer.

### 7. Don’t be needy – make the target chase you. 

Neediness, otherwise known as validation-seeking behavior, is a sign of weakness and it can be absolutely fatal to your pitch. If you act needy, the audience will sense you are weak and their primitive croc brains will classify your proposal as a threat — to their money. This can easily push you into a vicious cycle where the audience becomes more and more distant due to your neediness, which in turn makes you anxious and even needier!

To negate neediness, you can use a simple three-step formula based on the movie _The Tao of Steve_, where the protagonist, Dex, follows a pseudo-Taoist philosophy to pick up women.

First, try to eliminate your desires, at least in the eyes of the target. If they have something you desperately want, this will translate as neediness in you. To negate this, make it clear to the target that you do not need them.

Second, focus on the things you do well, your strengths. Demonstrate something that showcases your excellence. Dex, for instance, was great with children and made sure the target of his affections saw this. Similarly, you must demonstrate excellence in front of your target.

Third, withdraw. At the crucial moment when your target expects you to chase them for their money, withdraw instead by saying something like, "I'm not totally convinced we're a good match for each other." This will make them chase you, much like the women in the Tao of Steve chased Dex.

### 8. To pitch effectively, you must attain situational alpha status. 

_Status_ plays a vital part in any social encounter. In any meeting, a dominant member known as an _alpha_ emerges, while others take subordinate _beta_ -positions. It is very difficult to be persuasive from a beta-position; hence, you must grab alpha status.

Though some elements of status, like your reputation or wealth, are quite stable, _situational status_ can vary immensely; for example, while a successful surgeon has considerably higher social status than a golf teacher, the teacher is still the alpha during a golf lesson.

Often your pitch targets will lay so-called _beta traps_ to force you into the situational beta position; for example, being made to wait in the lobby is a classic beta trap.

You must try to ignore these traps and avoid doing anything that enforces your opponent's alpha status. Instead, use small acts of defiance and denial to grab the situational alpha status for yourself as soon as you can.

Say a customer has made you wait in the lobby. Once in the meeting room, you could begin examining some papers on the table in front of you. When the customer peeks at them, you could yank them away and say something like, "Nope, not until I'm ready." If done in a good-natured, half-joking manner, this enforces your alpha position.

Once you have alpha status, you must then steer the discussion into a direction where you are the expert, much like the golf professional talks about golf, not heart surgery, when teaching the surgeon. To solidify your status, force your opponent to say something that reinforces your alpha position with a good-natured jest, like, "Remind me, why on earth _should I_ work with you guys on this?"

### 9. Keep your pitch short and simple. 

Before you begin any pitch, let your target know you will keep the presentation short. This will put them at ease. When Watson and Crick presented their Nobel Prize-winning idea of the DNA helix, they only needed five minutes. If you know what you are doing, you can pitch anything in twenty.

Start your pitch by introducing yourself. This does not mean rattling off your entire résumé but just outlining your greatest successes, like projects where you really did something impressive.

Most people will be tempted to jump right to the "big idea" for which they're trying to get financing. But before you get to it, you should address one crucial concern in your target's mind. Namely, you must explain why now is the right time to invest.

Rather than a long and complex analysis, simply outline the _economic, social and technological forces_ which make your deal unmissable right now. Economic forces that benefit your pitch, for example, could be your target customers becoming wealthier and interest rates going down, the social forces could be the rising consumer concern for the environment, and the technological force could be the development of the electric car. You must present these forces in a way that shows a window of opportunity has recently opened but will not remain open forever.

The three forces set the stage and paint a backstory for your big idea, which should also be kept brief and simple. Use an established "recipe" for this:

"For [target customers] who are dissatisfied with [current offerings on the market]. My product is a [new idea] that provides [solution to key problem] unlike [competing product]. My product has [key product features]."

That's it. The time for details comes later.

### 10. Final summary 

The key message in this book is:

**In any social encounter where you aim to be persuasive, it is vital that you seize control of the situation and ensure the target sees your pitch through the frame of mind you have chosen. At the same time, you must cater your pitch so that on a neurological level, the target's brain works for you, not against you.**

**Suggested further reading: _POP!_ by Sam Horn**

This book shows you how to craft messages and ideas that _stick_ in the minds of your audience because they are Popular, Original and Pithy. Author Sam Horn offers a highly adaptable and systematic approach to creating pitches, titles and taglines that grasp people's attention and win them over to your brand, product or idea.
---

### Oren Klaff

Oren Klaff is an investment banker who has raised over $400 million of capital over the past 13 years, and continues to do so at a rate of two million dollars a year. He has accomplished this with his unique pitching method, which he developed over years of trial and error after he tired of the usual weak sales techniques that did not produce results and seemed more like begging.

