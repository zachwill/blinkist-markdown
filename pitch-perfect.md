---
id: 53e20ab26339360007290000
slug: pitch-perfect-en
published_date: 2014-08-05T00:00:00.000+00:00
author: Bill McGowan and Alisa Bowman
title: Pitch Perfect
subtitle: How to Say it Right the First Time, Every Time
main_color: FFD333
text_color: 806A19
---

# Pitch Perfect

_How to Say it Right the First Time, Every Time_

**Bill McGowan and Alisa Bowman**

_Pitch_ _Perfect_ presents advice and principles that can help anyone to communicate more effectively. It teaches you how to present an argument or story with confidence, in a memorable way, and how to make your points with more precision. The author introduces seven principles that will help you to use exactly the right tone in both your professional and private life.

---
### 1. What’s in it for me? Find out how to get your message across effectively, at work and at home. 

Whenever you've been nervous before a presentation, how many people told you to imagine your audience in their underwear?

Plenty, right? But this is terrible advice! It will only distract you from the task at hand — focusing on conveying your message effectively.

Yet this is not the only popular-yet-ill-advised technique out there. There are, in fact, many that will hinder you from getting your message across to an audience or individual.

These blinks will help you to avoid these traps, and to improve your communication so that you can climb the corporate ladder. Also, with good communication skills, you'll find that you have a lot more time to focus on actually executing your ideas, and not just clarifying or answering questions about them.

And not only will these communication skills help you to improve your public speaking, they'll also help you in your personal life — whether you're in the thick of an argument with a loved one, or you wish to have more engaging conversations with friends.

You'll also discover:

  * which communication habits keep people from getting promotions,

  * how to stop that embarrassing shaky voice when you're giving a speech,

  * how to keep your listeners hanging on your every word,

  * why Steve Jobs was a master at giving presentations,

  * how to be a better conversationalist, and

  * how poor communication skills cost one famous business leader his job.

### 2. In many situations, you have just one chance to deliver the right message. 

Whether in your professional or home life, there are moments when you need to be _pitch_ _perfect_ : to use the right tone to deliver the right message to the right person at the right time.

At work, for instance, having effective communication skills and knowing when to use them means that you're more likely to be promoted. There are only a few moments in your career which determine whether or not you impress your superiors and thus climb the corporate ladder.

For example, imagine your boss asks for your opinion regarding any internal obstacles to the company's growth.

On the one hand, if you're hesitant to express your opinion, and you avoid direct eye contact, you'll lose your chance to demonstrate your knowledge. On the other hand, if you speak bluntly and reveal that the weak structure of the business itself is threatening its growth, then you could offend your boss. Either way, you can kiss that promotion goodbye! Indeed, one study found that a main obstacle to promotion is poor communication skills. This includes racist comments, offensive jokes, crying, cursing, avoiding eye contact, and other mishaps.

But if you make your communication pitch perfect on the very first attempt, you'll experience fewer misunderstandings and requests for clarification, and you'll have more time to focus on executing the idea itself.

For example, say you want your child to finish his homework, but you cannot convince him on your first attempt. Each new attempt costs both you and your child time that could be spent in far better ways.

Pitch-perfect communication is fundamental to getting the results you want. Approximately two thirds of proposed ideas are rejected — not because they're innately bad ideas, but simply because they are expressed poorly.

For example, if you've invented some new website technology that you'd like to be used in the next big project at your company, it's best you don't talk about how you came up with the idea but instead tell people how the company will benefit from using it.

> _"During pivotal moments of our lives, results are often determined not by what we do, but instead by what we say."_

### 3. Start with a concise, compelling statement to make listeners want to know more. 

Whenever you're giving a presentation or starting up a conversation, do you sense that you could lose your audience at any moment?

Guess what? You're right.

You have just _30_ _seconds_ to convince your audience to continue paying attention. If you do manage to hook them in at the outset, they'll keep listening, but otherwise their minds will drift, they'll check their smartphones — or they'll leave!

Clearly you want to avoid such a situation. But how?

First, it's crucial that you avoid talking about your agenda beforehand. Instead, you should dive right in and get started on the presentation proper. If you spend the first minute talking about what you're _going_ to say, you'll sound just like every other speaker, and your audience will assume you have nothing new to say.

For example, consider what you would do if you had to endure a preamble where a presenter introduces every point that she'll discuss later. You wouldn't be at the edge of your seat.

For this reason, it's best to begin a presentation with your first argument, or with a brief, attention-grabbing story that fits your message.

Which bring us to the second point: your opener must be effective, which means that you should put your most compelling material right up front.

Of course, there's no recipe for the perfect opener, but they tend to be short, suspenseful, and most importantly, surprising. To hook your audience, you could start with a story or a provocative statement or question. Or you could do something unexpected.

For example, in 1984, Steve Jobs began a presentation with "Hi, I'm Steve Jobs," when everyone already knew who he was. The audience laughed and applauded — then Jobs started right in with his first argument. By surprising the audience, you have a better chance to grab your listeners' attention.

Finally, you should test out your presentation in a low-stakes situation. This kind of low-pressure environment might be the dinner table with friends (not co-workers) or your teenage children. Can you hold their attention? Or are they slouched over their smartphones?

### 4. Use visual images to illustrate a story and keep listeners hanging on every detail. 

Whether you're giving a presentation or participating in a conversation, you can make what you say far more vivid by using some simple storytelling techniques.

These include generating _anticipation_ in the audience — ". . . and then, out of nowhere, something completely unexpected happened" — and lacing the story with personal details that make the audience care.

In choosing the visual details, you dictate what your audience will picture. For example, imagine an economist who wants to explain inflation in Brazil in the 1980s.

How could she make her explanation vivid?

She could include a story about Brazilians transporting sacks of suddenly worthless cash on their shoulders to their bank to trade them for the new currency.

However, this kind of storytelling can be difficult — especially when you have to include statistics. Turning stats and numerical data into easily grasped images is challenging, so if you find you cannot accomplish this, try instead to use _analogies_.

By using analogies you can emphasize what those statistics actually mean to you, and your audience can put that into a certain context. For example: "We increased our sales 100 percent last year — that's like Michael Jordan scoring twice the number of baskets!"

Of course, capturing an audience is not only about _what_ you say; it's also about _how_ _you_ _say_ _it_. You have several tools at your disposal here: you could vary your pitch, pace and projection in order to get a key point to stick in your audience's mind.

For example, Steve Jobs wanted to stress that his company had become the most successful in the world. To accomplish this, he ended his argument by slowing down his pace, and introducing longer pauses: "Apple is the number one . . . mobile devices company," then a longer pause, followed by a near-whisper, " . . . _in_ _the_ _world_."

### 5. Boil down your argument to its key message to make it as rich and brief as possible. 

Whether it's because they want to appear smart, drive home a point or simply have poor planning skills, many people talk for way too long. Unfortunately, these people just won't succeed in getting their message across.

If you're one of these people, you should instead adopt the "pasta-sauce principle": try to boil your argument down until the message is rich and brief.

This is a useful principle because most people's attention span is very short. Indeed, one study revealed that when people are listening to speeches, they tend to be receptive for just 18 minutes, max.

So if your audience is capable of digesting only small pieces of information, it's counterproductive to overfeed them with an XXL portion.

It's for this very reason that Twitter has become so successful: in a world where information overwhelms us, a Tweet delivers a single, concise, focused message. Indeed, by comparison, even an email or blog post can seem too long.

One way to ensure your presentation is not too long is to develop decisive openers and endings, as this will afford you a certain flexibility.

So practice both your start and finish, and learn them by heart. This will help to calm your nerves and build your confidence. It will also ensure that you know exactly what constitutes your main message, and how you want to close it. Also, decisive openers and endings enable you to expand or shrink the main part of your presentation to be flexible to time constraints.

Another way to keep things short is to ensure you avoid recapping your presentation at the end. When you begin to repeat to your audience what they have just heard, they will easily become distracted and stop paying attention.

Instead, at the end, you could offer your audience pertinent advice. This might involve suggesting ways for them to apply the information you've given them to their actual, individual lives. For example, "The next time you develop a speech, make sure that it is not longer than 18 minutes and that it is rich and concise."

> _"Your communication should leave people feeling satisfied, not stuffed and bloated."_

### 6. Slow down when you’re uncertain about your next sentence. 

Many people say what they are thinking, exactly as they're thinking it. This means that they often speak very rapidly. However, it is much better to think before speaking, because every mistake that escapes your mouth cannot be taken back.

But why is it that people speak rapidly and at length?

We tend to speak for longer, and at a faster pace, when we are defensive or anxious. Some people believe they sound more convincing when they do this, when in fact they come across as uncertain and neurotic.

One reason for this behavior is that, when people are tense and nervous, their thoughts accelerate, sending them into overdrive.

And when you speak too fast and for too long you sometimes say things that can damage your reputation.

Perhaps you recall former BP chairman Tony Hayward's final comments during the tragic aftermath of the oil spill in the Gulf of Mexico? He said, "There's no one who wants this thing over more than I do. You know, I'd like my life back."

Why would he say something so reckless? Perhaps it was because he was tense, or defensive. Either way, letting that single, egocentric statement slip at a press conference resulted in his dismissal from BP.

In contrast, by speaking slowly and succinctly you demonstrate confidence and avoid the risk of saying something imprudent.

This means that you won't find yourself in a position where you have to take back any statements — as people often do when they speak before fully considering what they'll say — or reformulate your thoughts differently again and again.

Instead, by giving your brain enough time to put your thoughts in order, you will come up with an effective structure for your ideas. In turn, this makes your thinking more focused and your arguments more persuasive, which leads to greater confidence.

> _"The object is to keep your mouth a safe car length from your brain."_

### 7. With good posture and the right wardrobe you appear more assertive. 

You're at a conference, and a presenter approaches the stage dressed in a rumpled shirt, faded old jeans, and with unkempt hair. As he begins to speak, you can't help but notice that he's waving his hands around a lot.

Already you're more focused on the speaker's appearance and gestures than on what he has to say.

For that reason, it's important that when you speak, you stand upright and use only small gestures to make your point.

Posture is important because it affects our confidence and, consequently, our behavior. It's therefore crucial that while you're presenting, you stand rather than sit, as standing lowers your stress levels by 25 percent.

This is because your back is far straighter when standing, resulting in increased self-confidence, which in turn means that what you say will probably have a greater impact on your audience.

Also, though gesturing can assist you in stressing your key points, you should limit it: don't move your hands constantly as you speak, or gesticulate wildly. Instead, you should do it sparingly and keep those gestures small. After all, you don't want your audience's focus to be diverted from what you're saying to your slouched posture or distracting hand movements.

Similarly, you should avoid wearing an outfit that will distract the audience, and ensure the rest of your appearance is inconspicuous.

Appearance matters: a dirty shirt or yellow teeth will certainly grab the audience's attention. And if they're distracted by how you look, it's unlikely you'll get your message across.

Therefore, make sure you don't wear anything which makes your audience think: "What the hell _is_ that?"

For example, you might recall that a certain female chef and author used to show a lot of cleavage on her TV appearances. This wardrobe choice had an adverse effect: many female viewers were threatened by her. They certainly didn't want to invite into their living rooms a person who their husbands would want to invite into their bedrooms!

To assuage the fears of the women in her audience, so that they'd ultimately purchase her books, she made the choice to wear more conservative attire.

### 8. Earn trust by displaying great interest in your listeners. 

We all know people who seem to talk non-stop and never listen to anyone else. It may not be their intention, but such people can make others feel insignificant.

Therefore, if you want to be an effective communicator, not just on stage but also in conversations, it's crucial that you avoid this behavior.

Instead, you should show an interest, as this will lead to far better conversations.

If your conversation partner can see that you are mentally engaged in what they're saying, they'll feel more confident in talking with you. To appear and act in this manner, you should let your partner speak without interrupting her, and always display a short moment of reflection before you respond.

By doing this, your partner will tend to like you more, because she'll feel that you're listening with genuine interest, and this, in turn, will make her feel respected and heard.

This is especially pertinent to conversing with business clients, as they want to be heard and will appreciate your undivided attention.

Finally, by displaying an active interest in them, you can draw out important information about your clients.

For example, imagine that a customer enters your sports equipment store, and you soon learn that she received a kayak for her birthday. Here's your chance: by asking her further questions — how passionate she is about kayaking, what other sports she enjoys, and so on — you'll be able to better address her needs and will be more likely to sell her something.

But it's not enough to show an interest; the best conversationalists are also generous and modest.

By being generous — i.e., asking for the other's input, trying to understand their stories and considering their opinions — you'll find others will want to engage in discussions with you and seek your input in return.

And being a modest and humble conversationalist — i.e., asking others about themselves before talking about yourself — will benefit you greatly. For instance, an entrepreneur wanting to discuss his business idea might approach you, introduce himself and quickly start explaining his idea.

But this isn't the best approach. The entrepreneur would do better to start a conversation with you, and wait until you ask him about his business idea. An advantage to this approach is that he'll have a clearer notion of how to relate his idea to your own interests.

### 9. Make sure that the focus of discussion plays to your strengths. 

In some ways, a discussion is like a car ride: you can either be sitting in the backseat while someone else decides where you'll go, or you can take over the driver's seat yourself.

But there's a danger to letting someone else direct the conversation. When others dictate the content of a discussion, you can't play to your strengths.

Imagine you're about to close an important deal with a client, but your co-worker begins to ruin things by talking about his failing marriage. What can you do to get the conversation back on the right track? If you notice that your client is blushing, or becoming angry, it's crucial that you somehow redirect the topic of conversation to a more suitable one. If you don't, you risk sabotaging the deal.

So, how can you turn this situation back to your advantage? Simple: you must _gently_ change the topic.

Subtlety is crucial here because, as one study showed, the perceived trustworthiness of speakers decreases when they abruptly change topic.

For this reason, it's vital that you at least _slightly_ touch on the broader topic you want to (re)introduce. If you're asked a question or statement you don't want to directly answer or respond to, you could _mirror_ a small piece of information contained within the question or sentence in your response. This will enable you to shift easily from one topic to another.

For example, imagine you and your wife are sitting together with another couple whom you know from your college days, and the husband takes out his phone and shows everyone a Facebook photo of an old classmate in a bikini.

"Have you seen this? Cindy's in incredible shape now," he says.

You notice that the topic of conversation is embarrassing his wife, and yours, so you want to subtly redirect it.

How?

You could respond: "Actually, I think our generation starts taking better care of themselves right after graduation. My parents never even started jogging until they were past 30."

By doing this, you make a slight detour to discuss generational differences in terms of health and sports.

### 10. Try to avoid being spontaneous, and make sure you prepare. 

If you think that just because you're a great speaker you'll rock the stage, you're wrong! Even the best have to prepare in order to save themselves from embarrassment.

If you have an off-day, or are simply not attentive enough, you can quickly fail at your moment in the spotlight.

All it takes is one small, imprudent remark to hurt your reputation and divert your listener's focus away from your intended message. And once the damage is done, it can't always be repaired.

For example, pro-wrestling manager Abraham Washington once commented on wrestler Titus O'Neil's performance live on air, stating that O'Neil "was like Kobe Bryant at a hotel in Colorado . . . He's unstoppable."

Washington was fired. Why? Because, at that time, Kobe Bryant was held under suspicion of committing a rape in a Colorado hotel.

Had Washington taken enough prep time, he might never have found himself making such an ill-considered remark. A little preparation is like a career insurance policy: perhaps you won't ever need it, but you'll certainly be glad to have it if you do.

But what happens if you have to speak at short notice? Well, you can prepare for that too.

Gather together a few stories that you know will be interesting to most of your listeners. For instance, during small talk, many people ask about your family and kids, where you're from or what you do for a living.

So try to tell them an entertaining story, but not one that's too personal or potentially embarrassing. Also, you could add some curious facts: for example, "I work as a salesmen for a robotics firm and have made it to 40 states already."

Finally, make sure you're always informed about _hot-button_ _issues_ — topics that always come up in conversation because they're inherently interesting or the industry talks about them constantly. For example, if you have a web company and present your strategy to potential investors, you should be prepared to handle questions about online privacy, today's current hot button topic.

> _"Some people bristle when I tell them that for all their love of spontaneity, spontaneity doesn't love them back."_

### 11. Get to the venue ahead of time to socialize with your listeners, and take a deep breath before speaking. 

Whenever you have to give a speech or presentation, it's not enough to just be on time. You have to arrive _long_ _before_ time.

Arriving early is not just about setting up beforehand and making sure the projector is working. It serves other important purposes too.

It gives you the chance to get a feeling for the room and your audience. You can stand onstage, at the lectern, test the microphone, or sit in the audience's chairs. Doing this will increase your confidence.

Furthermore, it allows you to get to know the first people to arrive. Engaging in brief, light conversation with these early birds will help you to relax and, perhaps, to get to know their interests.

This way, during your speech, you can look at the friendly faces you already know, or weave in a couple of stories that you're sure will chime with their interests.

Doing this will ensure your audience stays interested, which will ultimately help you to convey your message.

While arriving early will help to keep at bay any anxieties you might have, it's also important to take a deep breath before you begin speaking. If you don't, you might be confronted with a shaky, nervous voice when those first words leave your mouth!

So, take a slow, deep breath, as a steady voice will help you to sound more convincing. Also, begin by speaking slowly, so that you'll have enough breath for the rest of your speech.

The importance of this is illustrated perfectly by former Secretary of State Alexander Haig, who in 1981 was filling in for President Ronald Reagan after the assassination attempt by John Hinckley.

When the press secretary couldn't say who was running government at that moment, Haig ran from the Situation Room to the pressroom and, with shaking arms and wobbling knees, blurted out "I am in control here."

Unfortunately, his voice and body language conveyed the opposite.

### 12. Be empathetic and avoid being inappropriate. 

As we've seen, good communication isn't always business-related. If you're the best man at a wedding and have to give a speech, you might reconsider telling that story about your old roommate running naked across campus.

In this situation, you should avoid bad jokes and embarrassing stories, because anything that's potentially humiliating, or which might change the guests' perceptions of the bride or groom, is inappropriate.

So you should ensure beforehand that everything in your speech reflects the broader personality of the person you're talking about, as it's best to have a short, solid speech that doesn't make anyone feel uncomfortable.

This is especially advisable if the person you're talking about has certain obvious limitations, such as an illness or disease. In such cases, you should always focus on the positive aspects.

For example, Sarah was born with Williams Syndrome, and she's often asked to explain what this is.

Sarah tells them that it's a condition that inhibits her ability to process things and remain organized, but — to focus on the positive aspects — it can lead to musical proficiency, giving her greater musical potential than most people have.

Of course, it's sometimes hard to avoid saying the wrong thing. But one surefire way is to practice being empathetic — that is, taking into consideration what your audience appreciates, their situation, and how _you_ would want to be treated if you were in their position.

This can be the case when you are on your first date, meet your boyfriend's parents for the first time, or have to explain to your boss exactly how you would change the company.

For example, when your friend tells you they've lost someone dear to them, don't try to compare their loss to one of your own, as pain is not comparable. You're far more likely to make your friend feel better if you share a touching story or pleasing memory you have of the deceased.

### 13. Final Summary 

The key message in this book:

**Pitch-perfect** **communication** **means** **saying** **it** **right** **the** **first** **time,** **to** **the** **right** **person,** **with** **the** **right** **message.** **Among** **many** **valuable** **pieces** **of** **advice** **are** **seven** **principles** **that** **will** **help** **the** **reader** **to** **effectively** **handle** **various** **difficult** **communication** **challenges.** **Always** **communicate** **succinctly** **and** **precisely,** **without** **being** **controversial** **or** **inappropriate,** **and** **always** **be** **prepared.**

Actionable advice:

**Don't** **be** **abstract.**

Build stories and visual images into your conversation or speeches to ensure your listeners understand and remember your arguments.

**Think** **ahead.**

Always prepare, whether you're a great speaker or a novice. Ensure that you are breathing calmly before starting your speech.
---

### Bill McGowan and Alisa Bowman

Bill McGowan is CEO of Clarity Media Group and an Emmy Award-winning correspondent. He has conducted hundreds of interviews as a journalist and has coached celebrities — such as New York Giants quarterback Eli Manning and executives like Sheryl Sandberg — on their communication skills. Alisa Bowman is a journalist, blogger, book collaborator, and author and co-author of several books, including _Project:_ _Happily_ _Ever_ _After_.

