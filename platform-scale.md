---
id: 5717a37b3f35690007554e19
slug: platform-scale-en
published_date: 2016-04-26T00:00:00.000+00:00
author: Sangeet Paul Choudary
title: Platform Scale
subtitle: How an emerging business model helps startups build large empires with minimum investment
main_color: EE594B
text_color: BA463B
---

# Platform Scale

_How an emerging business model helps startups build large empires with minimum investment_

**Sangeet Paul Choudary**

A new kind of business has emerged during the last decade. Facebook, Airbnb, YouTube, Twitter have exploded in popularity — but what do these companies have in common? They're all platforms: they gather millions of users and achieve billion-dollar valuations, but instead of products, they offer something very different. _Platform Scale_ (2015) offers an insightful analysis of the mechanisms that drive this new platform business model, and how it achieves skyrocketing growth.

---
### 1. What’s in it for me? Understand the workings behind the platforms you use every day. 

We have all heard the story of how Mark Zuckerberg and some friends created Facebook while still in college. Today it is probably one of the world's best-known brands and a place where people spend hours at a time. Other platforms like Instagram, Twitter, Uber and Etsy have achieved similar massive growth. So how have these companies been able to scale up and become so big, so quickly?

The answer lies in how these _platforms_, or type of networks, work. They actively engage the people who use them to become a part of not only the consuming side but the production side too. So let's delve into the trappings, tricks and methods of platforms and see how they are able to scale up.

In these blinks, you'll find out

  * what "stand-alone mode" means and why it can be a good way to start a platform; 

  * why Instagram succeeded where Hipstamatic failed; and

  * how dating platforms like CupidCurated guide the user experience.

### 2. Business design is shifting from the old model of pipes to the new model of platforms. 

The internet is revolutionizing the world: the way we communicate, the way we consume media and even the way we do business. In fact, the old model for doing business is slowly fading away.

For decades, the standard method for building a business involved using _pipes_. You can easily imagine the pipe model by visualizing a business at one end of the pipe offering products or services, and the customers at the other purchasing what's on offer. 

This model worked well for many years in traditional industries, and its sway was so strong that early digital business used a similar model. Take the first version of Amazon, which was simply an internet take on a classical marketplace. Software companies like Windows worked in a similar manner: they designed products and pushed them through the pipe to their consumers.

Since then, the rapid development of the internet has forged a new model. Two forces, in particular, have played a crucial role. First, we are increasingly interconnected by mobile devices that allow us to _always_ be online. Second, goods are no longer produced in one single location. This _decentralization_ allows companies to produce and sell in different places.

In consequence, pipes are fading into the background, and _platforms_ are moving into the limelight.

Platforms are virtual environments where users can connect and exchange value with each other. They allow producers and consumers to interact, which means that producers are no longer limited to the old paradigm of producing goods, holding inventory and marketing products. Instead of creating the products themselves, businesses build platforms and let the users generate the value: like sellers and buyers on eBay, drivers and passengers on Uber, and video creators and viewers on YouTube.

### 3. Behind every platform, there is a simple and flexible core idea. 

Platforms revolve around interaction, so it's crucial that the platform is simple enough for the users to instinctively understand and use. So how can we understand the simplicity that lies behind a complex platform?

If you look at the workings of popular platforms and their massive ecosystems, you can see the few small, simple core ideas that form the foundation of their services. Take WhatsApp, for example, whose entire platform is based on sending SMS messages for free. Or consider Uber, an app that allows you to call a taxi with your smartphone.

As you can see, these ideas are amazingly simple — but why is simplicity so crucial?

Because simplicity attracts users.

If using a platform is complicated in any way, it immediately turns off a potential user, but if it allows a user to do something valuable simply, the reverse happens. For example, Twitter's 140-character rule enables users to easily and quickly create content that can potentially reach millions of people. And Instagram enables people to create beautiful images with little skill thanks to its wide range of powerful filters.

A platform's simplicity also allows it to be flexible to the needs of its users, who often take it in directions the creators never imagined.

Take Moodswing, which lets users express how they feel, like a Twitter for moods. At first there was little interest, but soon people started using it to express two moods in particular: depression and insecurity.

Moodswing noticed the shift of direction and saw the possibility of making the platform useful for a specific audience. Instead of being a space for everyone to express moods, Moodswing became a platform for its users to connect directly to people who could help them, like psychology students and psychologists.

So if you're creating a platform, don't stick too rigidly to your original idea. Let your users surprise you, and you might discover new unthought-of uses for your creation.

### 4. A successful platform is designed so users can interact and get what they want. 

Creating a platform is just like building something with Legos: you start with a large base on top of which you add the bricks and blocks. Design starts with an understanding of these very elements: _the core value units_ of the platform.

The core value units are the services or items that the platform offers. Take Etsy, an e-commerce website where subscribers can sell their homemade items. The homemade creations are Etsy's core value units.

If the platform has few core units, it also has little value — which is a big problem. That's why it's so important to create a platform that is attractively designed, so people will want to become users, even though it's initially empty.

So, what does a well-designed platform look like? It focuses on making the interactions between its users on the platform as smooth as possible.

Interactions take two different forms: one is _creation_, where a producer creates something valuable and increases the supply of content on the platform. This may be a video on YouTube, or a tweet on Twitter. The other form of interaction is _curation_, which allows users to sort out good content from bad, making the platform more attractive for everybody. This happens when YouTube videos are rated, or tweets are retweeted. 

However, a well-designed, expanding platform faces a major issue: very quickly there's so much content that users struggle to find what they need.

A good platform needs _consumption filters_ to keep things orderly. They work by attaching data to the core value unit, which makes it easier to connect with the consumer's needs. For example, a YouTube video (core value unit) has information that says what it is (a title and a description) and how good or bad the content may be (the rating). So if you're looking for a highly rated video of cute cats, YouTube will make this as easy as possible to find.

### 5. An optimized platform uses efficient systems to encourage users to contribute content. 

Can you remember how you first used the internet? Chances are you were only using Google to look up content that someone else had created — but now things have changed.

In fact, before the emergence of platforms, internet communities obeyed the _90–9–1 rule_ : 90 percent of subscribers consumed content, nine percent curated and only one percent created. But today, successful platforms have pushed the trend toward a much higher percentage of producers — which in turn has become the key to platform success.

To increase the number of producers, platforms need to offer two things: tools for creating content, and channels to broadcast it. Both elements are crucial to becoming a competitive platform — as the story of Instagram shows.

Many people don't know that before Instagram, there was Hipstamatic — the first platform that allowed users to apply filters to pictures. But Hipstamatic didn't make it easy for its users to spread their pictures. So when Instagram arrived and created a strong community around its tools, it quickly took over in popularity — and won the digital photography app race.

Another way of optimizing the production of content is to minimize unnecessary _frictions_.

Frictions are the barriers that make users less motivated to contribute to the platform, like overly demanding security measures that send producers through lengthy background checks before they can get access, or interfaces that are so poorly designed that users get lost trying to upload their content.

But sometimes friction can be a good thing — especially when it comes to security. Take a platform like Craigslist as an example. Anyone can post an ad without any prior checks. While this flexibility boosts the popularity of the website, it's also why some people call it a shady version of eBay. In the end, a balance between too much and not enough is the best policy.

### 6. Interaction failures cause problems for platforms, but they can be tackled. 

Imagine you put up an ad on eBay, and no one responds, or that you upload a video to YouTube without getting a single view. This is what's called an _interaction failure_.

Sometimes the content — for example, the video or the ad — is just plain bad, but it can also mean that the platform has failed to match supply and demand. And when that happens, users lose interest, because the platform is not helping them connect. 

So what causes an interaction failure?

One major factor is how easy it is for users to _multi-home_, which means taking advantage of several platforms that offer similar services. Say you're a car driver. You can use both Uber and Lyft to find passengers without incurring any additional costs. This means you can easily access more customers, but it also means that each platform offers fewer potential clients. And while this prevents any one platform from reaching a winner-take-all position, it also makes it harder to offer efficient interactions on all the platforms. 

You can see this particularly clearly when it comes to instant messaging applications, like Messenger, Viber or WhatsApp. They all offer nearly identical services, but if you want to reach your full circle of friends, you have no choice but to install each individual app.

In order to overcome interaction failures, platforms need to develop accurate metrics. For a platform like Upwork that helps freelancers find jobs, this metric could mean the number of freelancers who don't find any work within a certain number of days, or job requests that don't get filled within a given period of time. By tracking down the important metrics, you can discover the issues that undermine interactions and the popularity of your platform.

### 7. There are ways to avoid the initial risk of having a platform without content. 

When a platform first launches, it's empty. Being empty, it can't attract users. And without users, it remains empty! 

Luckily there are several ways to avoid the vicious circle of an empty platform.

If you're entering an asymmetrical market — which means one part of the target group is easier to attract than the other — you can use some kind of bait to form a core base of users.

Take dating websites and apps: a lot more men sign up than women. But when men see there are few women, they quickly leave. So dating apps have learned to offer incentives like free subscriptions to get the women on board, and the women keep the men involved.

Another strategy is to target a market where registered users are given incentives to bring others to the platform. For instance, on Kickstarter, project creators want to attract as many donations as possible, so they spread the word about their campaign to as many of their contacts as they can.

Another way of avoiding an empty platform is to start it in _stand-alone mode_. This simply means that the platform is used without its full functionality.

Take OpenTable, a booking management system for restaurants. At first it could only be used to handle table reservations. But when enough restaurants had become users, the function that allowed consumers to book tables was opened up. This meant OpenTable could manage its growth step by step, rather than launch immediately and wait for consumers to show up and be disappointed by an empty platform.

A final way of creating traction for an empty platform is to fake the initial supply of content. The first users believe the platform is active and keep participating until the platform takes off, when faking ceases to be necessary. 

This is a typical strategy of dating platforms. Many of them create dummy profiles of women, which brings life to the platform, and incentivizes the first actual users to stay online. When real women slowly start subscribing, the fake profiles can be gradually removed, leaving a flourishing platform behind.

### 8. Virality is necessary to spread the word about a platform. 

Have you ever spent hours looking at the latest cute cat videos that are all over your social media? What you've experienced is _virality_, and it's a crucial element in creating a successful platform.

Many people hunt for the mysterious source of virality, but you can build it into a platform by using an _engine_ strategy. This contrasts with another way of growing a start-up: using _bumps_.

Let us explain.

A _bump strategy_ publicizes a company with traditional tools: advertising, PR, events, and so on. This strategy requires a significant investment to create exposure — a bump — and when the investment stops, so does the exposure.

In contrast, an _engine strategy_ doesn't rely on bumps of publicity to achieve growth: it harnesses the power inherent in the platform to increase exposure. Whenever a new user signs up, it revs up the platform's popularity, which automatically feeds into itself. 

For example, every time someone publishes a picture, Instagram immediately becomes more visible. By embedding virality into the app itself, the company didn't need to use traditional forms of marketing to become popular. In fact, before it was sold to Facebook, it was run by a mere 13 people.

This means that it's vitally important that you don't treat virality separately from the design of your platform. Indeed, many start-ups believe that virality simply means users send invites to their circle of friends. But members of start-ups with growth engines don't send each other invites — they share core units of content. YouTube users don't waste time sending an email invite; instead they directly share videos. This proves that virality isn't the same as word of mouth. Word of mouth happens when users recommend a product to others, but virality is the direct consumption of the product itself.

If you want to reach the heights of virality, you'll need a high percentage of producers because the best evidence of a platform's value is the number of people contributing to it. That's one reason why WhatsApp is so successful: it's impossible to be a passive user because you receive and send messages. And when you find yourself contributing to the platform without thinking, it has already proven its relevance to your life.

### 9. Platforms can suffer from reverse network effect, but this can be managed by increasing curation. 

When a platform starts growing exponentially, there's always a lot of excitement. But it may reach a stage where growth backfires and turns into a threat. 

Why?

Because platforms with a lot of users can suffer from the _reverse network effect_, where the size of the network gets in the way of offering a quality service. 

Many dating websites suffer from the reverse network effect. Women overwhelmingly receive the most messages, but when the platform is small, that isn't too much of an issue. But if the platform takes off, it's typical for thousands of new men to suddenly join. The women then get flooded with often lewd messages — so they leave for another service. LinkedIn faces a similar situation: it aims to easily connect users with one another, but it can't allow professionals to be bombarded with requests from people they don't know.

In order to maintain a high quality of interactions, platforms have to constantly improve _curation_.

Well-curated platforms limit the kinds of behavior that hurt platform scaling. For example, the dating site CupidCurated forces men to create a profile that needs to be approved by female users before they can join the service. That filters out people who just want to send provocative messages. And LinkedIn stops users from adding people they don't know by hiding distant connections — unless users are prepared to invest in the premium subscription, which gives unlimited access. This way the start-ups can maintain interaction quality and generate revenue — killing two birds with one stone.

### 10. Final summary 

**The key message in this book:**

The traditional model of manufacturing and selling goods is slowly being taken over by a new way of doing business: building platforms that both encourage interactions between producers and consumers, and allow everyone to create value.

**Actionable advice:**

To avoid launching with an empty platform, be your own first producer and progressively open the platform to other contributors. When Apple retailed the first iPhone, the App Store was entirely managed by Apple, which offered only a few basic apps. However, this was enough to answer consumers' needs and get enough traction to convince other developers to come on board as producers.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _Traction_** **by Gabriel Weinberg and Justin Mares**

_Traction_ explains why the success of every start-up depends not only on its products, but on the customer base it builds. Weinberg and Mares present proven methods for gaining customers, and help you choose the best for each growth phase of your company. With a bit of _Traction_, you'll win — and develop — the audience your product deserves.
---

### Sangeet Paul Choudary

Sangeet Paul Choudary is the CEO of Platform Strategy Labs. He has written for several leading publications such as TechCrunch, Forbes, _Wall Street Journal_ and _Wired_.

