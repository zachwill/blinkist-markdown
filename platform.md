---
id: 53f3582a3433390008000000
slug: platform-en
published_date: 2014-08-19T00:00:00.000+00:00
author: Michael Hyatt
title: Platform
subtitle: How to Get Noticed in a Noisy World
main_color: A02025
text_color: A02025
---

# Platform

_How to Get Noticed in a Noisy World_

**Michael Hyatt**

The book offers detailed information on how to create compelling products, then establish an online platform to get visibility for them. These blinks will walk through the process the author, Michael Hyatt, went through to build a successful career, by mastering social media channels like Facebook and Twitter. This book will show you how you, too, can monetize your social media platform and even turn it into a career.

---
### 1. What’s in it for me? Learn how to create the ideal online platform. 

People today have easy access to literally millions of websites. Just a few decades ago, consumers might have only had a choice between a few products in a certain line. Today, they can instantly access thousands. How can you make your product stand out in this sea of information?

The key is to build an online _platform_. This is crucial whether your product is a book, T-shirt or even an idea. Establishing a strong online platform not only increases your visibility; it can be extremely rewarding, and even change the course of your career. This book will guide you through the process of building your platform, and eventually reaping the benefits it sows.

You'll also learn:

  * what gives a product the "wow" factor, 

  * why having a tribe of followers can help sell your products,

  * what kind of advertising your followers might appreciate, and

  * how the author, Michael Hyatt, built his career around his blog.

### 2. Create a wow product that people will be excited to use and share. 

What's the secret to creating a successful product? The answer can be elusive, but all successful products have one significant thing in common: they are _compelling_.

What makes one product dull, and another one compelling? A compelling product is one that people will fall in love with and tell their friends about. To ensure people fall in love with it, it's important that no one in the creation process is satisfied with mere mediocrity. Instead, make sure all the creators, from the designers to the manufacturers, put their heart and soul into crafting the perfect product.

This approach is part of what's led to Apple's widespread success. Apple focuses on developing only a few products at a time, to guarantee the designers focus on perfection. When the new iPhone was presented in 2007, Steve Jobs himself was entranced by it, and called it a "magical experience."

Once you have your compelling product, you need to add some _wow_ to it. Customers should vividly remember the first time they use or see it. The _wow_ feeling is usually associated with surprise or anticipation.

Apple put a lot of effort into their product's design, so that customers feel excited just from looking at them. Customers are so enthralled that they start to anticipate what future Apple products will look like, generating a devoted customer base.

Having a _wow_ experience is the best way to ensure people will love your product. Aim for creating something that people can be excited about, and will want to share with their friends.

### 3. A platform is an assortment of social media sites that can be used to market your product. 

Is your product a T-shirt? A book? An idea? Whatever it is, there's only one modern way to market it in our modern world: you have to build a _platform_.

So what is a platform? Traditionally, a platform is something on which you stand so people can hear or see you better. A social media platform serves a very similar purpose.

To build a social media platform, you have to use as many social media sites as possible, reaching the maximum number of people. Like a physical platform, this makes you more visible and gives you a position from which you can be heard.

Hyatt, the author, is a strong example. His blog has over 400,000 visitors per month; his Twitter has over 100,000 followers; and he has even more likes and fans on Facebook. His platform has helped him get noticed, and his blog was even ranked among the Top 800 Business Blogs in the world.

Having a strong platform like Hyatt is crucial in our modern world. Today, people have easy access to an incredible amount of information, so it's easy for things to go unnoticed. Just a few decades ago, there might have been only a few brands of soap, for instance, or a few telephone companies for people to choose from. Now consumers can browse through thousands of quality products online. How can you stand out in this sea of information? By having a strong platform that engenders fans and followers.

### 4. Establishing a successful platform takes time and preparation. 

Building a good platform requires a lot of help and preparation.

There are millions of websites, so it will take some research and experimentation to find out how to make yours more appealing to customers. It's a good idea to hire professionals such as web designers. They'll know how to design a website that's more likely to attract and retain followers.

Keep working to build your platform, even if you don't see immediate success; it takes a lot of time, commitment and patience.

For instance, let's say you want to start your own blog. Before you even begin writing, you'll have to consider what range of topics you'll cover, how often you'll update and how much you'll interact with your followers. If you still don't see success after your first few entries, don't get discouraged. Continue updating and personalizing your blog, and keep promoting it on other social media channels. Your work will start to pay off eventually.

Even Michael Hyatt had to spend years growing the platform he has today. There were times when his blog lost followers and his traffic decreased, but he remained devoted and kept updating, which ultimately paid off.

Persist to build your platform, even if it's a struggle. It's a long process that will be worth it in the end. However, if possible, try to prepare your platform as best you can before your initial launch, because you'll only get one chance to make a first impression.

### 5. Use the tools that blogging sites offer to create a unique experience for your fans. 

It's ok if you don't have experience designing your own website or blog; most people don't. The good news is that nearly all host websites offer a range of tools that let you customize your site and create something unique. You'll be surprised at how much you can create yourself, even if you don't have web design experience.

These tools on various blogging sites make it so that anyone can design a website. When creating a new site, most people will be afraid to deviate too much from the template, but doing so is very important to make your site stand out. Most host sites provide toolboxes that make the process of individualization much easier.

Disqus, for example, provides great tools to let you manage your visitors' comments. Hyatt even developed his own tool called Get Noticed! that provides different layouts for bloggers to use and customize.

Use these tools to create a design that makes the viewers feel comfortable. Make sure the information on your site is easy to read and navigate. Test its usability by interacting with the site yourself: is it easy to post comments, access older entries, and navigate the different links?

It's also a good idea to look at some already well-established blogs, to see what elements you like, or what you'd like to do differently. You might find Hyatt's blog a useful ([ _www.michaelhyatt.com_](<http://www.michaelhyatt.com/>)) example of a design that's stylish and practical.

So spend time crafting a site that's comfortable to use, but also stands out. If you can provide a unique and pleasant experience to your fans, your platform is much more likely to garner success.

### 6. Use a consistent design throughout all your sites. 

People like to feel at home when visiting the different platforms of their favorite brands. Wouldn't it be disorienting if the visuals on a company's blog were completely different on their website?

Always try to make your fans feel comfortable when they're visiting your sites. If the layout or design across your different sites is consistent, your customers will have a feeling of familiarity when they interact with them.

You'll probably put the most effort into the design of your homepage — the first page where you can start to build a fanbase. If your homepage already has a great design, why change it? When you expand to other sites, try to use the structure you already developed.

Hyatt, for example, capitalized on the efforts he put into making his blog when he expanded into other social media. His homepage was his blog, in which he invested a lot of effort. He even had professional photos taken for it, and he hired professional web designers. Once the blog became a success, he simply implemented a similar design across his other sites.

Replicating the design elements is not only practical, but also creates consistency. Many of Hyatt's fans praised him for staying true to his design and policy choices, because it made his different sites so easy to use.

When you establish your design in this way, it also makes your sites more recognizable. People will notice your sites across different platforms more easily if the design is familiar to them, and they'll feel more comfortable using them, allowing them to engage with your content, rather than getting hung up on navigating.

### 7. Expand your reach by guest blogging or sharing your ideas via networks like Twitter. 

Now that you've established your own blog and attracted your first fans, the best way to keep increasing your outreach is through constant updates and communication.

This is especially true in the Web 2.0 era, in which users don't only consume, they also contribute content themselves. In recent years, social networks like Facebook and Twitter have grown exponentially, allowing people from all over the world to connect and communicate.

You can use this connectivity to your advantage. Twitter, for example, allows you to add tags to your posts, so all interested parties can be notified. Imagine you're hosting a speaking event. You could create your own hashtag like "#Speak2014" that would allow the participants to easily see any tweets relating to the event, building publicity.

Guest blogging is another good way to increase your influence and connections. You can guest post on others' blogs and have others post on yours. If you feature guests on your blog, their fans will follow them over to your blog, and they might become regular visitors. If _you_ are featured as a guest on another blog, you'll get immediate exposure to their fanbase, and some might follow you back to your own.

Hyatt has used this strategy to expand his platform. He features guests on his blog regularly, and encourages his fans to join the discussions started by guests. This interactivity is beneficial for him, his guests and the fans too.

Fostering this kind of communication with and among your fans is essential to securing and expanding your platform. The web we use today is defined by communication and interconnectivity — take advantage!

### 8. Build your own tribe and let its members help promote your website. 

What does the word tribe mean nowadays? Traditionally, it's a group of people who live together. Today, it has a different definition. A tribe today is a group of like-minded people who share a common interest.

Having a tribe means having constant traffic to your blog. If you want to be a successful blogger, don't aim to have people visit your page just once — you need _subscribers_, who'll form the core of your tribe.

Your subscribers make your blog interesting and alive. They'll comment and discuss your work, which will attract more followers in turn. People will take a greater interest in your blog if they see that other people are interested in it.

Hyatt values his long-term subscribers the most for this very reason. They not only engage with him regularly, they encourage their friends to get interested as well. Your tribe members can do some of your best advertising.

However, don't forget that your tribe isn't obligated to advertise for you. They won't spread your message unless they get something in return. So even after you've secured a steady tribe, keep giving them interesting content — don't take them for granted. Be thankful that people actually want to listen to you, and don't become smug about it. If you do, you might lose your tribe.

Also, if you want your tribe to do something for you, like fill out a quiz or poll, make sure you don't stop providing for them. It's a good idea to employ the 20-to-1 rule. This rule means that before you ask your tribe for something, you should give them 20 good posts, quotes or facts. Following this rule ensures that you're still delivering great content to them, even while they're giving content back.

Ultimately, encourage your tribe to get involved in your work, leading to greater satisfaction on both sides and expanding your recognition and followers.

> _"Tribe-building is the new marketing."_

### 9. You can monetize your platform and use it to build a sustainable career. 

Now that you've put so much work into establishing your platform, you deserve to reap some benefits! Although it may seem unlikely at first, your online platform can become a fruitful source of income.

Blogging and writing can actually be a sustainable career. There are many ways to make money from your website or blog. For example, you can host small advertisements that fit the style and topic of your blog.

Remember that not every ad has to involve half-naked women or flashy game invites. Choose your sponsors carefully — your fans might even appreciate being able to see products that fit their interests or lifestyles. If you're a business blogger, for example, you can feature ads for marketing books or motivational speakers.

You don't have to think of your platform as separate from your career. In fact, using your platform as an _extension_ of your career can garner a lot of recognition. It can even lead to new career opportunities.

Hyatt, for example, started his blog while he was CEO of _Thomas_ _Nelson_ _Publishers_. Since then, he's become a published writer and motivational speaker, and he is now considered one of the most famous business bloggers of all time. Though he had a great position before, his career really took off when he started blogging.

So try to use your blog to bolster your career. Putting a lot effort into your passion and then seeing your hard work pay off is one of the best feelings in the world. You should strive for that goal every day as you build your platform — it can be immensely rewarding.

### 10. Final summary 

The key message in this book:

**With** **dedication,** **anyone** **can** **build** **their** **own** **online** **platform.** **You** **can** **use** **this** **platform** **to** **promote** **yourself** **or** **your** **product** **and** **interact** **with** **your** **fans.** **If** **you** **focus** **on** **creating** **a** **compelling** **product,** **promoting** **it** **on** **well-designed** **sites,** **and** **cultivating** **a** **tribe** **of** **followers,** **you** **can** **establish** **a** **platform** **that** **might** **even** **change** **the** **course** **of** **your** **career.**

Actionable advice:

**Use** **the** **same** **design** **across** **the** **different** **sites** **in** **your** **platform.**

It might be tempting to create unique web designs for each of the sites you use, but doing so can actually hinder you. It's not only more work for you; it's less comfortable for your fans. If you can establish a consistent visual style, your followers will feel more at home when they visit your pages, and your products will be more recognizable. So stick to a similar color scheme, typography, and page layout throughout your different sites to maximize the appeal and usability of your sites.
---

### Michael Hyatt

Michael Hyatt is one of the top business bloggers in the world. His blog, which receives over 300,000 visitors a month, is ranked among the top 800 blogs in the world. He is the former CEO and current Chairman of _Thomas_ _Nelson_ _Publishers_, the seventh largest trade book publishing company in the US. He's also published several successful books and is an established motivational speaker.

