---
id: 53a1ad776335360007370000
slug: plato-at-the-googleplex-en
published_date: 2014-06-17T00:00:00.000+00:00
author: Rebecca Newberger Goldstein
title: Plato at the Googleplex
subtitle: Why Philosophy Won't Go Away
main_color: 2480B5
text_color: 1F6E9C
---

# Plato at the Googleplex

_Why Philosophy Won't Go Away_

**Rebecca Newberger Goldstein**

_Plato_ _at_ _the_ _Googleplex_ examines contemporary issues through the lens of Plato's philosophical questioning. The book explores the life and times of Plato as well as how his philosophy and thoughts on love, education and ethics can be a model for us today.

---
### 1. What’s in it for me? Learn how Plato’s philosophy can help you wrestle with today’s big questions. 

By most measures, our modern society has little in common with that of Ancient Greece. Where we have universal citizenship, the Athenians had slaves and only men could be citizens; where we have many distinct branches of scientific inquiry, many of today's natural scientific disciplines were not even around then. All in all, we live in a completely different time than the birthplace of democracy.

And yet, philosophy in general, and the thoughts of Plato in particular, continue to live on and influence our lives. These blinks will present Plato's timeless insights and even give you an idea of how he personally might handle some of society's major dilemmas.

In addition, you'll see how Plato's philosophy can help you to better think and act for yourself, and gain a deeper and stronger control of your mental faculties.

In these blinks, you'll learn

  * how to not act like an unruly horse,

  * why the wisest person in the world claims to know the least,

  * why you can't google the answers to all of life's problems and

  * why acting on instinct can get you killed!

### 2. Plato’s ideas have far outlived their time and still affect our lives today. 

If you've ever been bored in a history or philosophy class, you've probably wondered what relevance Plato, an ancient Athenian philosopher who lived 2,400 years ago, holds in our twenty-first century lives? What, if anything, can we learn from him?

Although many of the facts surrounding Plato's life and times undoubtedly seem barbaric from our modern perspective — such as the possession of slaves and sexist attitudes — the philosophical insights found in his _dialogues_ nonetheless raise fundamental questions about who we are and how we should live.

Plato's writings pose many questions that don't have easy answers, and are still just as open to interpretation as they were thousands of years ago.

_The_ _Symposium_, for example, includes dialogues on the meaning of love and the responsibilities that come with it. In _The_ _Republic_, too, we find discussions on what kind of political organization is best. These questions hardly have cut-and-dry answers. In fact, we struggle with them to this day!

What's more, Plato's writings provide us with these timeless questions in a way that doesn't require us to dig into the past to find answers relevant for today.

In addition to posing timeless questions, Plato also set the foundations for exploring the meaning of life by examining our own lives and the lives of those around us.

In Plato's _Apology_ — _apologia_ means "defense" in ancient Greek — the character of Socrates, who in real life had been accused of impiety, corrupting the youth and bringing false gods into the city, proclaims that "the unexamined life is not worth living." In essence, we should strive to have new experiences and question norms in order to find the best way to live.

But we don't even have to read Plato's works in order to appreciate his relevance. In fact, our contemporary methods of trying to make sense of the world around us owes itself completely to Plato's philosophy.

> _"My Plato is an impassioned mathematician, a wary poet, an exciting ethicist, a reluctant political theorist."_

### 3. Plato is great not because he was right, but because he was unafraid to question our assumptions. 

Given that we have access to more contemporary works on the wonders of natural science, physics and sociology, why should we read Plato if we want to learn more about the world?

First, Plato helps us teach others by asking questions that allow people to arrive at answers on their own. Also called the _Socratic_ _method_, his method involves asking questions to a conversation partner in order to learn more about the subject under consideration and simultaneously prompt the conversation partner to develop her own conclusions.

For example, in the _Meno_ dialogue, Socrates helps someone who's never taken a math class to answer a difficult geometrical question — simply by asking the right questions!

In fact, this form of teaching is still used in law schools today. Professors will ask the class about a case under review by questioning the arguments used, the historical precedence, etc. After receiving an answer, he will then ask for further clarification in order to help students discover more about the nature of arguments themselves and thus become a more effective lawyer.

Plato didn't just use questions to arrive at answers; he also questioned those who claimed to have knowledge in a certain subject. In his dialogues, Plato had the characters who claimed to have knowledge provide an account of it so others could learn from them. But if they couldn't provide an explanation, they would end up proving their ignorance.

For example, when Socrates was accused of impiety in _The_ _Apology_, he first asks for an explanation of "impiety" from an expert before beginning to defend himself. In fact, Socrates was even named in _The_ _Apology_ as the "wisest person in the world" because he would always abstain from claiming any knowledge and instead choose to rely upon true experts.

### 4. Plato’s philosophy led to a transformation of ancient Athenian values that carries into the modern world. 

We're all affected by our specific cultural and historical contexts and Plato was no exception to the rule. However, he didn't simply accept the values and ways of his society; instead, he chose to question and transform the things he didn't agree with.

In his dialogues, Plato tried to understand, problematize and transform the ancient Athenian values that he saw as complicated. For instance, Plato lived during a time that the author calls the _Ethos_ _of_ _the_ _Extraordinary_, where individuals were expected to continually strive for extraordinariness. Plato didn't question this habit, but he did challenge the definition of extraordinariness.

For example, at the time, Athenians placed value in brute strength and emulating the gods. They looked to figures like Achilles, the great warrior from Homer's _Iliad_, for inspiration: his ferocity, strength and agility in battle embodied the gods they worshiped.

Plato, however, thought that an extraordinary life was created by continually improving our reasoning and being responsive to those around us. For him, excellence didn't require any gods or feats of strength, and was instead based on a continuous dialogue: the questioning and contemplating of accepted beliefs.

Plato believed that truth, beauty and goodness were all important values, and he showed the importance of each for becoming a well-rounded person. In fact, these values continue to survive to this day.

We can see this in the various institutions that gather and exercise wisdom: mathematics and the sciences deal with questions of truth and knowledge; our courts and lawmakers strive for justice and the moral good. And then there are all the artists, museums and galleries that confront us with questions about what beauty means today. 

Now that we've learned why Plato's philosophy is still meaningful today, the following blinks will offer insights into how Plato might have answered some of today's toughest questions.

> _"Philosophical progress is invisible because it is incorporated into our points of view."_

### 5. Plato would be suspicious of whether Google’s algorithm can solve our ethical problems. 

Where's the first place you go whenever you need a quick answer to a burning question? Chances are you pull out your smartphone and _google_ it. That certainly works for most questions, but what about humanity's deeper questions? Can we google things like answers to ethical or moral dilemmas?

Plato would be very skeptical that this is possible, as he drew distinct lines between expert knowledge and mass information.

Imagine that Plato attended an Authors@Google event where they discussed the merits of acquiring knowledge via _crowdsourcing_, or the soliciting of vast numbers of people to obtain answers — in this case, by using Google's search algorithm to solicit websites.

Plato would probably pose this insightful and decisive question: Is it better to ask a horse trainer about matters concerning horses or to appeal to information from the masses?

What does he mean by this? Well, while Google does provide us with lots of information, expert knowledge is less likely to shine through there versus going straight to the source of that knowledge.

Furthermore, Plato would argue that Google couldn't provide us any substantial knowledge on what the "best" life is.

According to the author, Google's search engine relies on the flawed assumption that it provides the questioner with the "best" answer. In actuality, the answers rely on the number of references a website has, not the value of the knowledge itself.

So while the highest-ranked answer to a problem worked for the person providing the solution, that doesn't necessarily make it the right answer for everyone.

While googling certainly provides us with a large number of responses, it cannot address the personal and particular aspects of a given issue. This isn't a major problem when googling recipes but it is when it comes to ethical issues, such as the death penalty or abortion, that can't be solved with a search engine alone.

### 6. For Plato, a well-rounded education means getting a solid foundation and then pursuing your own, individual interests. 

What's the best way to raise and educate children? Should we be strict, accepting nothing less than perfection, forbidding all extracurricular activities that don't immediately serve their education? Or is it better to be more lenient, allowing them to discover for themselves with fewer constraints?

According to Plato, our education should be strict and disciplined, highlighting mathematics, music, athletics and philosophy. However, this discipline should take our individual needs into consideration, and thus can't be applied universally.

Plato makes this quite explicit in _The_ _Republic_, which explores what the best and most just state would look like. In it, Socrates's character states that, since every child is not the same, education cannot be the same for each child either.

While certain disciplines should indeed be studied by everyone, we shouldn't have to continue with subjects that don't interest us. Instead, our educational development should correspond to our own interests and strengths.

Furthermore, crafting a good education relies on an early identification of the talents and interests of the student. No two individuals are the same and not everyone has the same faculties and interests, so after providing the common, necessary skills for everyone, education must cater to individual strengths in order to allow each individual to succeed and benefit the community.

Education, therefore, must provide everyone the same educational foundation while also being flexible enough to accommodate our individual needs, interests and strengths.

### 7. Plato offers a notion of love that incorporates all kinds of relationships. 

What comes to mind when you imagine "love"? Is it merely sweet whispers and romance or do you think it's something greater?

Plato believed that love was a multitudinous concept. For example, you've probably heard the term _platonic_ _love_ at some point, traditionally understood to describe the non-sexual love between friends. Plato's intention, however, was actually to expand the notion of love rather than divide it into categories, by showing how love, friendship and sex are intimately intertwined with one another.

In his _Symposium_, various speakers deliver speeches on what love is, ranging from love between friends to romantic love to a love of wisdom, i.e., philosophy. Socrates' position incorporates all of these varying views on love, suggesting that love actually develops in various stages.

According to Socrates, our love for others begins with our senses, later advancing to our rational faculties. For example, our amorous relationships may start with the excitement of the senses, i.e., our physical attraction to someone. Then, as we get to know that person, we become attracted to and eventually love those traits we can't physically see, i.e., their personality.

In this way, our initial sensual attraction can lead us to share ourselves with others through discussions of ideas.

Plato goes on to explain that love isn't just a relation between two people but a binding force necessary for establishing a community, whether that's a familial, educational or political community. While it might start with an individual, love should eventually extend to others and establish a love for the community.

In fact, _The_ _Symposium_ shows us that love is the force of attraction between people and the necessary requisite for _all_ relationships. All the various expressions of love, no matter what their degree of intimacy, lead us to eventually establish a community.

### 8. Our rationality is crucial to improving our lives. 

What is the best way for us to live our lives? Which values should we aspire to live by? No matter how we answer these questions, Plato would argue that _reason_ should be our guiding principle.

For Plato, a life left unexamined is not a life worth living, and it is reason that makes this examination possible. In his _Apology_, he writes, again through the voice of Socrates, that we can only justify our lives once we understand who we are and why we behave the way we do.

In order to gain this valuable understanding, Socrates questions others' beliefs and actions, demanding that they _give_ _reasons_ for their behavior. He believed that by reflecting on our thoughts, actions and interactions, we gain insight into who we are and where we want to go. Without this, we merely drift about aimlessly.

Conversely, being ruled by our emotions and personal ambitions can blind us to how we should act.

Plato uses the character Alcibiades — whose passions and emotions ruled him in a way that made him act selfishly and eventually got him executed — in order to demonstrate the dangers of following your gut feelings.

Like Alcibiades, when we react on the impulse of our emotions, we lose our reason and thus behave like a bucking horse. However, when we consider our actions rationally without responding immediately, we're more likely to make the right decision that has a clear aim.

Imagine, for example, that you hear about a work colleague's stupid mistake that might cost your company valuable business. Your gut reaction might be to express anger, but this could easily make a bad situation worse.

However, if you reflect upon what's happened and carefully consider how to act, you stand a chance at actually improving your situation: understanding your colleague's mistake can help you prevent it from happening again in the future, which will create a more constructive work environment.

> _"We should never rest assured that our view, no matter how well argued and reasoned, amounts to the final word on any matter."_

### 9. Plato’s questions of free will and personal identity continue to be of vital importance today. 

Bold headlines in science journals report the latest discovery: _We_ _are_ _nothing_ _but_ _a_ _bundle_ _of_ _well-organized_ _neurons_ _and_ _electrical_ _charges_. Surely you've seen something similar in the news.

In fact, some neuroscientists assert that chemical reactions of our various organs — with the help of our environment — completely determine our behavior, and thus conclude that personal identity and free will are mere illusions.

But the case isn't closed yet!

Were Plato alive today, he would undoubtedly expose the unquestioned assumptions underlying the neuroscientists' research and scrutinize their role as scientists, concluding that questions of personal identity and free will are blind spots in their studies.

For example, Plato would remark that the neuroscientists don't provide an account for what made them study the brain or how they draw their conclusions: If it's true that we're nothing more than a bundle of neurons, completely controlled by our environment, how can we explain the scientific curiosity that initiated the _choice_ to conduct research in the first place?

In the _Phaedo_ dialogue, Socrates explains how our nature and environment _enable_ us to act a certain way, but this doesn't mean that they _cause_ us to act that way. In other words, while we may be conditioned by external factors, we're never actually determined behave a certain way.

Furthermore, although we're composed of physical stuff — in this case bundles of neurons — this doesn't encompass the entirety of who we are. It's wrong to assume that we're nothing more than the nerves themselves simply because we haven't found a personality behind a bundle of nerves.

For Plato, it would take more than merely examining biological or chemical facts to disprove the presence of a personal identity or free will. We should, however, use these facts to inform our understanding of ourselves.

### 10. Final summary 

The key message in this book:

**Plato's** **philosophy** **is** **not** **only** **relevant** **to** **ancient** **Athenians** **and** **scholars** **in** **the** **ivory** **tower.** **Everyone** **can** **benefit** **from** **learning** **about** **Plato's** **philosophical** **wisdom,** **which** **addresses** **issues** **from** **love,** **personal** **identity** **and** **education** **all** **the** **way** **to** **the** **building** **of** **a** **state.**

**Remember **to** **ask** **questions.** **

There is no knowledge outside your grasp if you ask the right questions.
---

### Rebecca Newberger Goldstein

Rebecca Newberger Goldstein is a bestselling and award-winning author of both fiction and nonfiction works, including _The_ _Mind-Body_ _Problem_, _Properties_ _of_ _Light_ and _36_ _Arguments_ _for_ _the_ _Existence_ _of_ _God:_ _A_ _Work_ _of_ _Fiction_. She is also a recipient of the MacArthur Fellowship, a.k.a. the "genius grant", and a fellow of the American Academy of Arts and Sciences.

