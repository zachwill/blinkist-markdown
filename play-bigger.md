---
id: 58a31e5db2613e0004fe6812
slug: play-bigger-en
published_date: 2017-02-17T00:00:00.000+00:00
author: Al Ramadan, David Peterson, Christopher Lochhead and Kevin Maney
title: Play Bigger
subtitle: How Pirates, Dreamers and Innovators Create and Dominate Markets
main_color: 89315C
text_color: 89315C
---

# Play Bigger

_How Pirates, Dreamers and Innovators Create and Dominate Markets_

**Al Ramadan, David Peterson, Christopher Lochhead and Kevin Maney**

_Play Bigger_ (2016) details strategies you can employ to break into any market successfully. Creating a new product isn't easy but getting people to buy it is even harder. This book shows you how to pick the right market for your product by creating it yourself and making people take notice.

---
### 1. What’s in it for me? Discover how to create a new market category by playing bigger. 

Are you ready to shake up the market? Do you have a game-changing idea on the order of an iPad or Uber, something that will solve the problems consumers didn't even know they had?

Then these blinks are your ticket to becoming a category king. You'll learn how to play bigger by inventing your market category and simultaneously dominating it.

In these blinks, you'll also learn

  * why you should seek market solutions first and not problems;

  * why practicing patience is a rule when it comes to new ideas; and

  * why storytelling is a requirement for any company toolbox.

### 2. Category kings find and develop solutions to problems that no one else has noticed. 

At the beginning of the twentieth century, if you had asked people how personal transportation could be improved, they would have told you to make a faster horse.

Henry Ford, however, imagined an ambitious alternative — the Ford Model T: the first affordable, mass-produced automobile.

Such a game-changing innovation is called a _new category._

New categories offer consumers a vision of a new way to live and solutions to problems that few others have even considered.

When you need a taxi, for instance, you often scramble to hail one from the street or search high and low for a taxi stand. Then Uber came along and said, "Why can't the taxi come to you, no matter where you are or what time of day it is?"

A person who thinks like this, who comes up with game-changing solutions, is called a _category king._

Companies can be category kings, too. A company, after all, is the entity that develops the services and products that become essential in our lives. Old ways soon become obsolete, and we wonder how we ever survived before the new category came along.

We often think this way, because it's only when a category king offers the perfect solution that we realize we had a problem in the first place! To become a category king, you have to not only show people the solution but also show them the problem.

Uber is a perfect example of a category king. Its innovative taxi service was all the more appealing because it highlighted the many flaws of traditional taxi companies.

Before Uber, customers had to track down a cab themselves, guess how much the final ride would cost, and fumble with bills and coins. With Uber, customers can order a cab through a smartphone, get an automatic price estimate, pay by credit card and even track the progress of the cab online!

### 3. It can take a long time for people to change their minds. Category kings need to have great timing. 

When the automobile was first invented, consumers didn't take to it quickly. People thought cars were unsafe, and it was some time before this prejudice would wane.

This can be true for any new category. When a company comes up with a revolutionary idea, it often has to wait for public opinion to catch up.

It's one thing for companies to identify and develop a category, but it's another thing entirely to make potential customers realize a problem needs solving.

Picture a middle-aged IT manager who works in the corporate sector and knows the traditional ins and outs of his industry. When an innovative platform such as Salesforce.com comes along — a service that helps clients more efficiently connect with customers — it may take him years to accept it.

This isn't surprising. Neuroscience research tells us that if an idea is completely ingrained in a person's mind, it can take between six to ten years for it to be replaced with a new, better idea.

So people need time to change their minds. What's more, to help this process along, a company should clearly understand the problems and needs of potential customers.

In 2002, Microsoft's Bill Gates developed the Tablet PC to address what he saw as a technology problem. Cell phone screens were too small to use comfortably for any reasonable length of time; laptops weren't portable enough. Gates thought he'd nailed a solution with the tablet computer.

However, the Tablet PC as a product bombed.

Yet just a few years later, Apple's Steve Jobs identified the same problem and created the iPad — a tablet computer.

So what was the difference between the perceived solutions? People didn't need a personal computer in a tablet form, but an internet device on which they could surf and deliver presentations.

Jobs correctly identified this need and met it with the lightweight, intuitive iPad.

### 4. You can discover opportunities for new categories using market and technology insights. 

So how do you go about pinpointing what's missing in today's world to find a market ripe for revolution?

More often than not, you'll stumble upon a gap in the market by sheer luck. To make your idea a reality, however, you're going to need _market insight_.

Market insight means you're intimately familiar with how the market in which you plan to operate works. You can then use that knowledge to bring your plan to fruition.

Do you know how the electric guitar came to be invented, for example?

Les Paul used to play his guitar and harmonica in a parking lot. One day, a person came up to him and said that he couldn't hear the guitar over the sound of the harmonica.

This was a lightbulb moment! Paul realized he needed a louder guitar. And while it was a random comment that set Paul on his path, his knowledge of the market made him confident that his problem was shared by others, and was thus worth solving.

There _'s_ another way to discover a gap in the market, and that's through _technology insight_.

It's usually scientists or engineers who have technology insight, inventing a new product or process often without deep knowledge of a market's needs. It's only afterward that an application is found.

When the founders of VMware created virtual computers that could run virtual software, for example, they had no idea which problems their invention could solve.

It was only once the company did some research that VMware's founders discovered their solution could be useful for companies that wanted to test software safely.

And just like that, a new category was discovered!

### 5. Stories can help customers understand your business better and help motivate employees. 

The need for stories doesn't disappear once we become adults and outgrow bedtime fairy tales.

A good story still has the power to influence how we feel and what we think.

According to a 2010 study by Professor Paul Zak at Claremont Graduate University, stories affect people more deeply than facts alone. Stories lead to better understanding, cooperation and motivation because listening to a story raises oxytocin levels in the brain. Oxytocin is the chemical that makes us feel empathy.

So how can _you_ make use of the power of storytelling?

You can present your product and the problem it's going to solve in the form of a story. Doing so will allow you to convey your _point of view_ (POV) effectively — how you define your company's place in the world and how it finds solutions to problems.

Your story could be about your company's mission, where it fits into the lives of your customers and how your new category is going to solve their problems.

Coverity is a software company that finds bugs in aircraft computer code. Coverity's story is that there are more than 100 million lines of software code in a jet airliner, and to keep the code running properly — thus keeping planes in the air — they need careful testing.

In this way, the work Coverity performs suddenly seems essential — a matter of life and death!

It's not enough for the CEO to have a clear idea of the company's point of view, however. Every employee needs to have a sound understanding of it, too.

After creating your POV, your entire company needs to learn and internalize it to guarantee its effectiveness. A training course for employees can help ensure your story is the guiding principle at all levels.

> _"Everything a company does … should align with the point of view. If the POV is clear and employees buy into it, the company will practically manage itself."_

### 6. The introduction of a new category in a market must be as dazzling as a strike of lightning. 

When a fork of lightning flashes across the sky, it lights up the night with its brilliance.

In the same fashion, designing a _lightning strike_ for your product can help gain the attention of a market.

Your strike could take the form of an event that attracts everyone from potential investors, stakeholders and analysts to the media and future customers.

An extraordinary event can also take potential competitors by surprise.

Sensity, a tech company working in the LED lighting industry, staged such an event at Lightfair International in 2003. At this annual convention attended by lighting manufacturers and customers, Sensity unveiled a groundbreaking discovery: that light fixtures can be digitized with LED lights.

This revolutionary idea allows lights to collect information on motion, sound, air quality and even weather data. Sensity dubbed its system the Light Sensory Network. The company even briefed a reporter from the _Wall Street Journal_ about the new category a few days before the fair, and the resulting newspaper article helped to build anticipation for what was a highly successful event.

To be effective, then, you have to direct your lightning strike at your target market.

Sensity was clever in its choice of market. It could have made its pitch to the sensor industry, but the impact would have been minimal. By choosing the lighting industry, the company was able to show how conventional lighting was not keeping up with technological innovation.

Like Sensity, you should single out a specific market, as a blanket approach won't work.

A carefully planned strategy to strike a single market will pave the way for your company to be crowned a category king!

### 7. Final summary 

The key message in this book:

**To play bigger, you need to create a new market category. To do this, you have to captivate your customers with a story about how your product will meet needs they didn't even know existed.**

Actionable advice:

**Select a team, or even an outsider, to realize your project.**

Once you've come up with a new category, you have to assemble a dedicated team to execute your project. And while company founders are committed, in most cases it's the team who is responsible for day-to-day operations and who will have the final say in naming the new category and defining the company's POV. If you want a fresh perspective and an alternative approach, you could also hire an external consultant to lead your project.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Hooked_** **by Nir Eyal**

_Hooked_ explains, through anecdotes and scientific studies, how and why we integrate certain products into our daily routines, and why such products are the Holy Grail for any consumer-oriented company. _Hooked_ gives concrete advice on how companies can make their products habit-forming, while simultaneously exploring the moral issues that entails.
---

### Al Ramadan, David Peterson, Christopher Lochhead and Kevin Maney

Al Ramadan is a data scientist, category designer, and the cofounder of Play Bigger Advisors, where he coaches technology firms to help them dominate in their respective market categories.

