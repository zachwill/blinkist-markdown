---
id: 57cdc4542b2b0d0003fa299f
slug: playing-the-whore-en
published_date: 2016-09-08T00:00:00.000+00:00
author: Melissa Gira Grant
title: Playing the Whore
subtitle: The Work of Sex Work
main_color: E4AF66
text_color: 7D6038
---

# Playing the Whore

_The Work of Sex Work_

**Melissa Gira Grant**

_Playing The Whore_ (2014) busts the myths still surrounding the topic of sex work and explores how and why society continues to shame the chosen profession of the sexually liberated. Unfortunately, society's attitudes and laws often endanger, rather than protect, those who work in the sex industry. Discover why that is and why it's time to change our perspective on one of the oldest professions in the world.

---
### 1. What’s in it for me? Challenge your views on prostitution and sex work. 

Have you ever heard the word "whore" employed as a compliment? Probably not. Often called the oldest profession in the world, prostitution has always been deemed amoral, something you would only do if there were no other choice. But this attitude relegates sex workers — real people who, like all of us, deserve respect and consideration — to the periphery of society.

Our moralistic views on sexuality have left sex workers behind. They're forced to live in the underbelly of a society that grants them no rights, putting them at even more risk than that intrinsic to the profession.

Let's take the sex worker's perspective for a change, and see what it's like to play the whore.

In these blinks, you'll find out

  * how the police often pose the biggest risk for prostitutes;

  * why legislation passed to protect sex workers has the opposite effect; and

  * that current US police methods limit the possibility for sex workers to have safer sex.

### 2. Rather than protecting prostitutes, the police often make a sex worker’s life more dangerous. 

Society often sees things as black and white. It's often assumed, for instance, that the police will work to keep all people safe from harm and that all prostitutes are criminals. But reality is rarely so clear-cut.

Often, the work of police ends up making the lives of prostitutes _less_ safe.

According to a 2003 survey by the Sex Worker's Project, an organization that offers legal and social support for sex workers, over two-thirds of sex workers in New York City are harassed by police, usually on a daily basis.

On top of that, 30 percent of sex workers have received violent threats from police officers and most feel that they can't depend on the police to help them out when clients become violent.

For example, after a prostitute was gang-raped, the police refused to investigate and, due to her profession, didn't consider her worthy of protection.

This attitude results in many emergency phone calls from prostitutes being ignored by police, and, as a result, many sex workers have simply given up trying to call for help.

And the statistics on the mistreatment of sex workers by police only get worse from there.

In 2005, the Sex Worker's Project found that 14 percent of the interviewed prostitutes in New York City were victims of police violence. Further, 16 percent reported that police officers had attempted to initiate sexual activity.

These problems aren't restricted to New York City.

In West Bengal, a survey of 21,000 sex workers revealed that the overwhelming amount of violent attacks on prostitutes were committed by the police, not clients.

This debunks one of the most common myths of prostitution: that clients present the greatest risk. In actuality, the police often pose much more of a threat.

> _"If I call them, they don't come. If I have a situation on the street, forget it."_ \- Carol, a New York City sex worker

### 3. Attitudes toward sex work improved in the 1970s, and positive work continues on an international level. 

If you've seen Anne Hathaway's Oscar-winning portrayal of Fantine in the film _Les Miserables_, then you're familiar with how prostitution was once seen as a person's last resort. When Fantine does become a prostitute, after losing her job, she also becomes an outcast from nineteenth-century French society.

Thankfully, things have improved since then.

In the wake of the sexual liberation of the 1960s, prostitutes saw many positive changes.

Pop culture from this time presents some of the first positive portrayals of prostitution: In 1971, Jane Fonda won an Oscar for the movie _Klute_, in which she played an independent and empowered call girl.

Not long afterward, a prostitute published her experiences in a book that made it to the top five of _The New York Times_ Best Seller list. It was aptly titled _The Happy Hooker_, though imagining such a person is difficult, even today.

All of this coincided with the birth of the sex worker's movement and, in 1973, the launch of the first US prostitutes' rights group, led by Margo St. James.

Two years later, in Lyon, France, prostitutes organized a sit-in protest at a church to fight against the unfair prison sentences that some of their colleagues were facing. (Street prostitution is still illegal in France, but, today, discreet sex workers are no longer arrested.)

These days, one must turn to international bodies to see the active work being done to fight for the rights of sex workers.

For example, UN Secretary General Ban Ki-Moon and several United Nations committees, including the UN Human Rights Committee, have repeatedly called for the decriminalization of sex work.

Their attitude is supported by the International Labor Organization, which officially recognizes sex work as legitimate employment and helps protect workers from discriminatory practices, such as forced HIV testing.

They also require that structured benefits like social security and health insurance be in place, just as they would be for any other profession.

> _"All countries should work toward decriminalization of sex work."_ \- The World Health Organization

### 4. When making laws or discussing the morality of sex work, those in the profession are unfairly sidelined. 

If you were tasked with improving the working conditions of IT consultants, the first thing you'd do would probably be to seek out the opinions of some IT consultants. Sex workers, however, are rarely asked for their opinions.

There is an ongoing debate about the morality of prostitution, but, when sex workers speak up, their input is routinely dismissed.

The debate is led by intellectuals, politicians and moral leaders, people who are generally more concerned with their own image and rhetorical ability than with the lives of sex workers.

For example, anti-prostitution activist and sociologist Kathleen Barry was a prominent figure at the first world conference on human trafficking, in 1983. But she refused to consider the position of Margo St. James, founder of COYOTE, a prostitution rights group. (The letters stand for Call Off Your Old Tired Ethics.)

Absurdly enough, Barry believed that prostitutes couldn't have a valid opinion on sex trafficking since they would have a biased and positive view of their profession.

This lack of representation can also be seen when laws are made regarding sex work.

In 1999, Sweden passed a new anti-prostitution law without consulting any sex workers. Before it went into effect, it was illegal to sell sex in Sweden, but the new law targets customers, making it illegal to buy sex, an act now punishable by imprisonment or fines.

While some celebrated this as a victory for feminism — a shift of the blame from the female sex workers to the male clientele — the new law is criticized by those in the profession.

After all, clients are now justifiably scared of being arrested, which makes them harder to come by. It also makes the initial act a tense affair, with clients in a rush not get caught and prostitutes having to make snap decisions about whether a client is safe, or intoxicated and potentially dangerous.

Intended to protect prostitutes, the law actually makes sex work riskier.

> _"Forgive sex workers if they do not want the attention of those who refuse to listen to them."_

### 5. The illegality of prostitution has led to a dangerous hypocrisy that puts the lives of sex workers at risk. 

In the brothels of yesteryear, conditions weren't great for prostitutes: their lives were oppressively controlled and they were blatantly exploited. But, in addition to all this, they were often also protected by a matron or headmistress who managed their affairs.

Today, however, especially in countries where prostitution is illegal, prostitutes receive little protection from their employers.

A typical high-class escort agency will often make sex workers sign a contract promising that they won't have sex with their clients.

This is the kind of dangerous hypocrisy that happens when prostitution is criminalized. Since the agency needs to make sure it can't be held criminally accountable, it shifts the responsibility for what might happen with the client onto the sex worker.

In order to maintain this innocent appearance, agencies are unable to develop policies to protect their workers, such as teaching them how to negotiate the terms and conditions of a date.

As long as prostitution is illegal, this kind of moral hypocrisy can even result in women in possession of condoms coming under suspicion.

It may sound ridiculous, but as recently as 2012, police in major cities such as San Francisco and Washington, D.C., have used condoms as evidence that a woman might be a sex worker.

If police were suspicious of a woman's activities, they could take her into custody and search her home and belongings. And if they found enough condoms, they could justify this as evidence of prostitution.

Ironically, this only discourages actual prostitutes from using condoms, making their work even more dangerous by putting them at risk of HIV and other sexually transmitted diseases, not to mention unwanted pregnancies.

According to police policy that is still in use in New York City, it also means that any woman who has regular, protected sex with one or more partners could be suspected of prostitution.

> _"Sex workers refuse condoms from outreach workers, and from each other, as a way to stay safe from arrest."_

### 6. While society would prefer sex work to remain invisible, the court of law defends advertising it. 

Many people turn a blind eye when confronted by a homeless person asking for spare change. Similarly, prostitution is something a lot of people would prefer not to see.

In this way, society is more concerned about how visible prostitution is, rather than how safe the prostitutes are.

For example, the humanitarian group Equality Now recently petitioned _The Village Voice_ to stop publishing ads for escort services featuring the naked bodies of women.

_The Village Voice_ responded with a compromise: it now requires sex workers to post ads using a simple headshot photo that clearly shows their face rather than a sexualized or full body picture.

Since sex workers generally prefer to be anonymous, this makes advertising problematic. Even if they find a way to satisfy this demand, interested clients are far less interested in a headshot photo than a seductive nude picture.

This sums up much of what people are truly concerned about when it comes to sex work: they don't want to be confronted with the realities of sex in our society; they somehow feel that making it visible will corrupt the societal morals.

And yet, according to the rule of law, sex workers have a right to advertise.

In fact, many states tried to ban such ads but were rejected by the courts: In Washington state, a bill that sought to suppress sex ads was eventually annulled. According to the judges, the bill was worded so broadly that, had it passed, all online speech could have been construed as illegal.

Likewise, in Tennessee, a law to suppress the advertisement of sex trafficking was rejected. It too was deemed overly general, since the term "sex trafficking" could have been used to target any offer of sex services.

> _"Many of us here at_ The Voice _wish these ads would just go away."_ \- _Village Voice_ editor

### 7. The moral judgment of women’s sexuality is linked to sex work and women are also responsible for slut shaming. 

If you've seen the TV series _Sex and the City_, you've seen how pop culture might embrace a sex-positive attitude toward women. This is but a minor advancement, however, since society still tends to pass negative judgment on women with an active sex life.

This moral judgment of women is closely associated with people's feelings toward sex workers.

Just consider those who suggest that women who've been raped were "looking for it" by dressing or behaving in a certain way.

This attitude was reinforced in 2006 when Toronto police officer Michael Sanguinetti lectured a group of university women on how they should dress so as not to be victims of sexual violence.

According to this logic, wearing a sexy outfit makes you a whore, and whores shouldn't be surprised if they get raped.

This, of course, is extremely offensive logic, and in 2011 women organized the first SlutWalk protest in Toronto. They advocated for sexual freedom and the right to wear the clothes of their choice without shame.

However, the sad truth is that many women also reinforce this moral judgment, categorizing women as either wholesome or whorish.

For example, if a woman is called a slut, her friends might argue, "No, she's definitely not a slut." These friends may mean well, but such a defense implies that there are other women who indeed _are_ slutty.

What they forget, and other people fail to realize, is that someone who is labeled a slut or whore may actually be a sexually liberated woman, a person who rejects the oppressive idea that women must be virtuous.

Rejecting such old-fashioned ideas is just one way to be sexually liberated. If someone decides they want to use their sexuality to earn money, there is no reason that this decision shouldn't also be respected.

### 8. Sex workers are often much more empowered and independent than we think. 

As we've seen in the previous blinks, it can be difficult to shake outdated modes of thinking. And this includes the idea that sex workers are victims forced to work in an unhealthy environment.

The reality of sex work is sometimes the exact opposite. In some places, it's the workers who have the power over their clients.

This is how things operate at a discreet residential mansion with close connections to the business center of a major US city.

Here, a very specific kind of sex worker awaits clients with open chains and handcuffs, in rooms that are often referred to as "a dungeon."

According to the strict rules of business, clients specify the services they wish to receive, but the sex workers have the final say in approving or vetoing these services. A receptionist then assigns the client to the sex worker who is willing to fulfill his or her desires.

In these scenarios, the sex worker is in the dominant role, while clients are the submissive ones, often literally tied down or restrained.

What's more, many clients also enjoy being coerced into the role of houseboy and made to tidy up the mansion. Inverting the patriarchal, as much S&M does, women dressed in leather and stilettos order their houseboys around, offering appreciated punishment when they fail to do a good job.

Further contradicting misconceptions is the fact that many sex workers are young and independent entrepreneurs. In fact, one of the author's friends uses her apartment as a base of operations to run a porn website and make short videos.

While she often models for her own videos, she also uses online forums to recruit friends and acquaintances to take part. And as her membership increases, she can expand her business, bringing in more models, taking more expensive photographs and filming more elaborate scenes for the site.

As we can see, the world of sex work is far from being a black and white affair.

> "_When prostitutes win, all women win."_ \- Black Women for Wages for Housework, 1977

### 9. Final summary 

The key message in this book:

**Being a whore is not an insult; it's a right. Whether one chooses to do it for pleasure or money, being a sex worker is a valid choice that a sexually liberated person can make. Unfortunately, due to unfair legislation, misguided moral judgment and gender inequality, sex workers don't enjoy the same human rights that most of us take for granted.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Melissa Gira Grant

Melissa Gira Grant is a writer, journalist and former exotic dancer. She advocates for human rights, especially the rights of sex workers, and does voluntary work for gender equality organizations and sex work groups. Her articles have been published in _The New York Times_ and _The Guardian_.

