---
id: 5406e3ae3933310008710000
slug: playing-to-win-en
published_date: 2014-09-02T00:00:00.000+00:00
author: A. G. Lafley and Roger L. Martin
title: Playing to Win
subtitle: How Strategy Really Works
main_color: None
text_color: None
---

# Playing to Win

_How Strategy Really Works_

**A. G. Lafley and Roger L. Martin**

_Playing_ _to_ _Win_ explains how to choose the right strategy and ensure that your company becomes a market leader. The reader will learn how to build a competitive, logical strategy from the ground up, and to win. Using examples from their experience at Procter & Gamble (P&G), the authors demonstrate that the answers to five questions based on the current market situation are at the heart of a winning strategy.

---
### 1. What’s in it for me? Let the CEO of Procter &amp; Gamble teach you how to develop a winning strategy. 

In the world of business, strategy is a term that's frequently used but not always fully understood.

In _Playing_ _to_ _Win_, A. G. Lafley (CEO of Procter & Gamble, or P&G) and Roger L. Martin (strategic adviser), explore the concept of strategy, and develop an accessible system which businesses can adopt to help them make a series of related and informed choices — the basis of any winning strategy.

Lafley and Martin argue that at the core of all business strategy is a process of making five specific decisions, known as the _strategic_ _choice_ _cascade_. These blinks will walk you through that process.

Along the way, you'll discover

  * why choosing a playing field where there's already a strong competitor can help you to win there;

  * how reverse-engineering can make important decisions easier; and

  * why hiring a star chef can injure your restaurant's image.

### 2. Strategy means making the necessary choices to win. 

Anyone who runs a company knows that the most important question they should ask themselves is, "How do I win against my competitors?"

The answer? You have to _play_ _to_ _win_.

This means "thinking big" when considering how to grow your business. Being good at what you do is simply not enough. What matters most is becoming the leader in your field and accomplishing more than your competitors.

For example, the main difference between the highly regarded Mayo Clinic and an average hospital is that the former doesn't aim merely to provide a good service, but to be the leader in the field of medical research in general.

But how does a company play to win?

_Strategy_.

Strategy is essential to winning. It involves making choices that enable a company to beat out the competition, and building every aspect of the business around those choices.

However, many people prefer to deal with urgent matters as they arise, rather than taking the time to conceive an overall strategy that would enable them to _win_. This is a shortsighted approach. A strategy helps you to deal with urgent problems in a way that stops them distracting you from the broader goal.

Imagine a saleswoman working in a large store. For her, winning means being the best salesperson on the floor, and making her customers happy. However, every day she's frustrated by the same problems — the cutthroat competition among salespeople in the accessories department, and having to explain to customers the minute differences between the handbags on sale.

In her case, a strategy would help her to find permanent solutions to those everyday problems.

Her strategy could entail deciding in which section of the store she would be at her most competitive. If she were to move to the footwear department, for example, she could escape the frustrations of the accessories department, and prepare herself for specific customer needs with the technical knowledge she'll need in order to succeed there.

The strategy (moving to footwear) would solve the problem of having to deal with urgent matters daily (e.g., the cutthroat competition).

Companies, too, are confronted every day with choices that have to be made. In the following blinks, you'll learn that establishing a company strategy requires a _strategic_ _choice_ _cascade:_ that is, five specific choices, with each choice determining the context for the next one.

> _"What matters is winning."_

### 3. You need to set your winning aspirations. 

The first element of the strategic choice cascade is deciding what your _winning_ _aspirations_ are. Your winning aspirations should take into account your company's ideal future: What are its main goals? What is its statement of purpose?

But why do you need to determine your winning aspirations? In short, aspirations motivate you to win.

Winning, of course, can mean many different things — from reaching a high stock-market value to gaining a loyal customer base. By establishing specific aspirations, you give yourself clear goals to work toward, and these in turn guide the other choices you need to make along the way.

For example, in their statement of purpose, P&G declared that its aim is to improve the lives of its customers, which, in turn, will result in high sales and profits.

As we'll see, setting down specific aspirations will render all the decisions that follow much easier to make. All you have to do is to ensure that each decision is linked to your aspirations — in P&G's case, toward improving customers' lives. Then profits become the indicator that the company is succeeding.

Of course, it's not always easy to decide on the specific aspirations of your company. The best advice is that you should choose aspirations that inspire you and your team to win.

A winning aspiration requires that you keep your company, customers and competition in mind. For example, if you're selling lipstick, you're not in the cosmetics business — you're in the business of making women feel beautiful. You should try to fulfill the desires of the consumer and compete with the very best companies.

Here, a winning aspiration would be not only to make a profit, but to boost the confidence and elegance of women around the world, and become a leader in the global beauty industry.

### 4. Choose your playing field. 

Once you've chosen your winning aspiration, it's time to make the strategic choice of _where_ _you_ _will_ _play_.

The _playing_ _field_ of a company can be defined through a number of criteria: the countries or regions in which the company will compete (geography); the kinds of products or services offered (product type); the consumer groups to target and needs to meet (consumer segment); and the delivery of goods or services (distribution channels).

Of course, you can't win in every field. No company is able to fulfill the wishes of every consumer on the globe. A company that serves every consumer in every country, producing and distributing the products by itself, simply does not and cannot exist.

Even a giant like Apple needs to narrow its playing field: in 2009, just two percent of Apple's revenues came from China, because Apple had made the decision not to play in Asia.

It follows, then, that you have to make a choice about where you'll compete.

As you choose each parameter that defines the playing field — geography, product type, and so on — you narrow your competition. This helps you specialize your tactics and make the choices that are essential to winning in that specific field.

For example, in 1984, P&G introduced a new product, Liquid Tide, and entered the field of liquid detergents. Although there was already an established brand in this field — Wisk — this also made it easy for P&G to identify its main competitor, adjust its own tactics in response to those of Wisk, and compete in the field.

> _"If everything is a priority, nothing is."_

### 5. Choose how you are going to win on your playing field. 

Having chosen the field you want to compete in, you have to consider how to win in it.

The playing field, and the way to win in that field, are closely related.

We've already seen how no single company can please everyone. So, once you've decided which group you want to please, you need to think about what this group specifically needs.

For example, consider Olive Garden and Mario Batali, two Italian restaurant chains positioned on two different playing fields.

It wouldn't make sense for Olive Garden, with their standardized menus, to begin offering a fine dining experience. Nor does it make sense for Mario Batali, whose customers expect first-class Italian dining, to suddenly stop hiring prominent chefs.

To win in a specific field, you have to decide what kind of advantage you can use to defeat its key players.

Basically, there are two kinds of advantage you can win: producing a product comparable to that of your competitors but at a much lower cost, or offering a product that is distinctively better than that of your competitors but doesn't cost you more to produce.

At the beginning of the twenty-first century, P&G wanted to enter the diaper business in emerging markets. The company settled on Asia because of the market's vast size. However, P&G's Pampers diapers were too expensive to compete there.

So, the company designed a diaper that didn't cost more than an egg, but was a higher quality than its competitors' diapers.

The result? Pampers became the market leader in China.

### 6. Determine which core capabilities you need to win. 

Now that you've determined _why_, _where_ and _how_ you will play, you should consider what you must do to achieve your goals.

This means getting to know your _core_ _capabilities_ _–_ that is, the actions and competencies that support your choice of a playing field and the way to win in it.

Your core capabilities will depend on where you're playing, and include specific knowledge, experiences or abilities that set you apart from your competitors. Without knowing your core capabilities, you'll never work out how to win in your playing field in the way that you've chosen.

Take Pampers, for example. Because they'd already experienced success with their diapers in the United States, they knew their core capabilities were in innovation, customer and market research.

So, when Pampers entered the Asian market, they had to figure out how to use those capabilities to their advantage.

P&G realized that to deliver better diapers, they'd have to ask customers what they needed from a diaper, and then create it for them.

That's exactly what they did: rather than stripping the original diapers of as many costs as possible, and thus offering an affordable but lower quality product, Pampers researched the expectations of Asian consumers and designed an entirely new diaper to meet their needs.

Since your company's core capabilities support your choice of both playing field and the way of winning in it, you have to build a unique set of capabilities so that your competitors can't win in the same way that you do.

Along with innovation and market knowledge, P&G's core capabilities include creating and building brands, partnering and scaling.

It's the sum of all those capabilities that ensures other companies can't cut into P&G's market share: even if another company has the same global scalability, for example, no other company invests as much in product innovation.

### 7. You have to pick appropriate management systems. 

The last element of the strategic choice cascade is deciding what management systems you need.

_Management_ _systems_ are any measures that support the chosen strategy. They can be of various types and depend on the other strategic decisions.

Any decisions made at the upper levels of the company reach the lower levels only via management systems.

So, let's say you've already organized a meeting with the top management of your company and you have made decisions about your winning aspirations, playing field, way of winning and even the capabilities you need.

How do you make sure that everyone in the company follows those decisions? By creating management systems that communicate your choices throughout the company and ensuring that everyone implements them on daily basis.

The author argues that success comes down to making everyone at the company understand the most important strategic choices. As chairman of the board, president and CEO of P&G, the author translated the company's aspiration to improve consumers' lives into a simple motto: "Make the consumer the boss."

He knew that conveying the company's choices to all employees in a simple statement was the best way to ensure they carried out the strategy.

However, management systems aren't just useful for ensuring that employees at all levels are on the same page. They also help to turn the other strategic decisions into sustainable realities.

Management systems help you to grow the capabilities you need to win on your chosen field in your chosen way, thus fulfilling your winning aspirations.

P&G's winning aspiration was to improve their customer's lives. They chose a global playing field and wanted to win by offering quality products. For this, they needed deep consumer understanding as a core capability. And to ensure this capability, they developed a management system: _investing_ _in_ _new_ _methods_ _for_ _consumer_ _research_.

This management system supported all choices from the strategic choice cascade.

You've now learned how, by making five specific strategic choices, you create a strategy. But how do you make those choices? For example, how do you know that playing globally is better than playing locally for your company? Or that winning by enticing middle-aged women is better than winning by enticing teenagers?

### 8. To make the necessary choices, analyze the relevant data. 

It's all very well to know _what_ you must decide on. Actually _making_ that decision is another matter entirely.

How can you be certain that one choice is better than another?

By using the _strategy_ _logic_ _flow_. The strategy logic flow involves investigating four factors that define the market.

First, examine how _segmented_ the industry is. For example, the fine fragrance industry can be divided into women's and men's fragrances. The men's segment is the more attractive one, because the competition isn't as fierce.

Second, investigate _what_ _customers_ _want_. If you consider the laundry detergent business, some customers want a technically superior product that gets rid of difficult stains, while others care more about its scent.

Third, consider your own _capabilities_ _and_ _cost_ _structure_ in comparison to the competition. For instance, are you more likely to win by being a cost leader or by offering a better product?

Finally, consider your _competitors'_ _response_ to your strategic decisions. Do you think they're going to adopt the same strategy as you?

If you don't consider the entire strategy logic flow, the success of your choices will be left to chance.

For example, let's say you want to sell fruits in Brazil. You consider that the industry can be segmented into different fruits, and that citrus fruits are not an attractive segment because there are already many competitors.

You think about what kind of fruit the Brazilian consumers want, and where they usually buy it, and you also think about your capabilities and costs.

Unfortunately, however, you neglect to consider your competitors' response to your strategic decisions. Even though you might've chosen where to play and how to win, your success can be jeopardized by a well-established competitor who can suddenly lower the cost of his own product.

### 9. To make good choices, ask the right questions. 

Traditional strategy-building processes — that is, analyzing masses of data and developing options from it — are ineffective. This is because processing enormous amounts of data is extremely slow and expensive.

For example, if you compile all of the data on the four factors from the strategy logic flow, you'll be overwhelmed with information and facts, not all of which will be relevant to your decision making.

But by using a _reverse-engineering_ process, you can make good strategic decisions simply by asking the right questions.

Here's how to do it.

To reverse engineer, first consider all your options. Second, using the four factors as a framework, ask yourself: under what conditions would any one of these option be a winning choice?

Let's look at an example. Say you've developed the choice of selling either kiwis in Brazil or oranges in the United Kingdom. In this case you should ask: What would make selling kiwis in Brazil a great choice for me?

With regards to the four factors, the answer would be: an underdeveloped Brazilian kiwi industry, kiwi-loving customers, your ability to offer delicious or cheap kiwis, and weak competition that allows you to enter the market.

Then you go through the same process for the option of selling oranges in the United Kingdom.

The next stage of the reverse-engineering process is to ask which of those conditions seem uncertain, and perform tests to determine this. For instance, if you're not sure whether or not Brazilians like kiwis, you'll have to research the market.

At the end of the process, you simply deduce which of the options offer the most stable set of conditions. By using reverse engineering, the winning choice — whether kiwis or oranges — will become apparent to you. You just have to choose the one for which the conditions seem more likely to be true.

> _"In the end, building a strategy isn't about achieving perfection; it's about shortening your odds."_

### 10. Final summary 

The key message in this book:

**No** **business** **can** **succeed** **without** **an** **effective** **strategy.** **Five** **specific** **decisions** **guide** **that** **strategy:** **your** **winning** **aspirations;** **your** **playing** **field;** **how** **you** **win** **on** **that** **playing** **field;** **the** **capabilities** **you** **can** **deploy;** **and** **the** **management** **systems** **you** **need** **to** **fulfill** **your** **goal.** **To** **make** **the** **right** **decisions,** **you** **must** **gather** **information** **on** **four** **factors** **and** **process** **it** **by** **asking** **the** **right** **questions.**

Actionable advice:

**Make** **five** **decisions** **and** **create** **a** **winning** **strategy.**

Imagine you run a cosmetics company that wants to introduce a new lipstick brand for teenagers. How would you use the lessons from these blinks to win?

_Determine_ _your_ _winning_ _aspirations_. As a cosmetics company, your aspirations might be to improve the self-esteem of young women around the world, and to be recognized as a market leader.

_Choose_ _where_ _to_ _play_. This means, for example, deciding on the continent(s), countries and regions in which you'll have the best chance at winning. Perhaps the demand among teenagers for your particular lipstick will be greatest in Europe and North America.

_Decide_ _how_ _you_ _will_ _win_. This is about determining the kind of advantage you could gain over the existing competition. Perhaps you'll decide to market your high-quality lipstick at a lower price point than competing brands, thus targeting younger people more effectively than them.

_Choose_ _your_ _winning_ _core_ _capabilities_. The success of your lipstick will depend on the unique capabilities of your company. For example, perhaps your cosmetics company has a particular, competitive strength in product development: each season, a new range of lipstick colors is added to the line.

_Choose_ _a_ _management_ _system_. This means choosing the system that best supports the strategy you've established based on the above decisions. For example, if your competitors tend to stick with the exact same range of colors for seasons at a time, you'd gain a massive advantage over the competition by investing strongly in your company's product development.

**Suggested** **further** **reading:** **_Good_** **_to_** **_Great_** **by** **Jim** **Collins**

_Good_ _to_ _Great_ presents the findings of a five-year study by the author and his research team. The team identified public companies that had achieved enduring success after years of mediocre performance and isolated the factors which differentiated those companies from their lackluster competitors. These factors have been distilled into key concepts regarding leadership, culture and strategic management.
---

### A. G. Lafley and Roger L. Martin

A. G. Lafley is chairman, president and CEO of P&G. Additionally, he works as a business and strategy consultant. Roger L. Martin is a strategy, innovation and integrative thinking adviser, and also dean of the Rotman School of Management.

