---
id: 57c42b488a12b60003822272
slug: political-order-and-political-decay-en
published_date: 2016-09-02T00:00:00.000+00:00
author: Francis Fukuyama
title: Political Order and Political Decay
subtitle: From the Industrial Revolution to the Globalization of Democracy
main_color: 7894C6
text_color: 5A6F94
---

# Political Order and Political Decay

_From the Industrial Revolution to the Globalization of Democracy_

**Francis Fukuyama**

_Political Order and Political Decay_ (2014) contrasts the history of democracy in America with its current condition to reveal the fundamental flaws of our modern democracy. From a declining middle class to selfish lobbyists and unadaptable institutions, these blinks explain just a few sources of political decay in the United States.

---
### 1. What’s in it for me? Understand why democracy is in peril. 

As the 1980s came to a close and the 90s got underway, the authoritarian communist regimes of Europe's Eastern bloc were crumbling. The end of the Cold War was in sight, and it looked as though Western democracies had won the day. At the time, Francis Fukuyama declared that this moment marked "the end of history," and the turn of phrase, as well as Fukuyama himself, quickly became famous.

But was it really the end of history? And has democracy truly flourished since the communist regimes fell?

Here we will take a closer look at how democracies around the world are struggling, with a focus on the United States. By looking at the ways in which American democracy is in serious decline, you'll get a glimpse into how the future of democracy could look, and which institutions are necessary to give democracy its best chance of survival. In an era of rising populism, these insights are crucial to securing the future of democracy.

In these blinks, you'll find out

  * how the 12,000 registered lobbying firms in Washington are affecting democracy;

  * why a declining middle class poses a serious problem; and

  * how the US Forest Service is a clear example of American institutions in decay.

### 2. Democracy is the cornerstone of American politics. 

The word _democracy_ often gets thrown around, whether in political discussions, philosophical debates or cultural criticisms. It's also the central theme of these blinks, so let's put the concept of democracy into context before going any further.

Simply put, democracy is a government for the people, by the people. In 1789, the US Constitution brought together democratic ideals of equality and fair representation in a radical way.

Unfortunately, the values embedded in the Constitution were ignored for much of the country's early history, and the United States had a weak and deeply corrupt political system right up until the nineteenth century. Goods and services were bought and sold in exchange for political alliances and, unsurprisingly, it was the rich and influential who wielded the most political power.

But toward the end of the nineteenth century, things began to change; the American federal government began to transform. By the mid-twentieth century, it had become an independent, effective and value-driven political actor.

This transformation began with the Progressive movement, led by politicians such as Theodore Roosevelt, who broke up big business conglomerates. This work was pushed along by the politics of the _New Deal_, which provided US citizens with healthcare and a general pension.

Industrialization had also altered traditional social structures and was a driving force behind social changes. From African-Americans to suffragettes, a host of newly empowered political actors began to shake up the old and corrupt system.

By 1989, it looked like democracy was on top. The author, in his seminal text _The End of History_, argued that the fall of Communism marked democracy's triumph, and its global expansion was the inevitable path that the future would take.

In fact, the number of democracies across the globe increased to nearly 120 in 2010 from just 35 in 1970; that's around 60 percent of the world's countries. But as democracy spread, it encountered its fair share of challenges along the way — and this is even true of democracy in the United States. But before we dig deeper into these issues, let's learn about what makes a democracy work.

### 3. A large and strong middle class is essential for a stable democracy. 

Since the time of Aristotle, philosophers have argued that the middle class is essential to healthy states and democracies. But what is the middle class? It can be rather complex to define.

In political science, _middle class_ refers to a measurement of social and educational standing. To illustrate this, think of a poor person with low social standing and a limited educational background who gets a new, better-paying job.

From a political scientist's perspective, he would climb into the middle class; but when he loses his job, he would sink back down into poverty. Chances are, he won't be able to mobilize a political protest against his descent back into poverty — he'll be too busy trying to survive each week.

Next, imagine a middle-class person with a university education struggling to find a job. Because of their continued unemployment, they sink to a lower social level. By contrast, this person is much more likely to engage politically and protest their slide into poverty.

Now, what would happen if the middle class grew massively, dwarfing other social classes? You'd have a lot more protesting voices if anything went wrong. And that's precisely what pushed the spread of democracy — the global rise of the middle class.

International studies show that middle-class people place greater value on democracy and individual freedom. They also tend to be more tolerant of alternative lifestyles than people from lower classes are.

American economist William Easterly's research reveals that better rates of economic growth, education, health and civil stability are linked to a large middle class. This, in turn, is tied to middle-class values of self-discipline, a strong work ethic and emphasis on long-term saving and investment.

Denmark and France have their middle classes to thank for their transitions into democracy in the nineteenth century. Driven by their middle classes, Sweden, Germany, Britain and many other nations became fully democratized by the early twentieth Century. In the Western world, the middle class has been nothing less than the bedrock of democracy.

> _"No bourgeois, no democracy."_ \- Barrington Moore Jr., Harvard University theorist

### 4. America’s middle class is struggling with small paychecks and declining jobs. 

In 1970, the top one percent of wealthy families in the United States took home nine percent of the country's Gross Domestic Product. By 2007, this amount skyrocketed to 23.5 percent. What does this mean for the middle class?

The reality is that there's been a hidden trend of decline in middle-class incomes since the 70s. The entry of women into the workforce around this time saw an increase in average household incomes — but this masked the fact that paychecks were actually getting smaller.

Another factor disguising income stagnation was the use of cheap, subsidized credit as a substitute for income redistribution. For politicians, this seemed like a great idea. And although a government-backed housing boom occurred as a result, it eventually culminated in the financial crisis of 2008.

There's yet another element making life difficult for the middle class and, oddly enough, it's something that benefitted them in the nineteenth and twentieth centuries: technology.

Back then, major technological innovations created countless jobs for low-skilled workers in the coal, steel, chemical, manufacturing and construction industries. You only needed to have passed the fifth grade to get a stable job on a Henry Ford assembly line, which broke down the process of building a car into simple, repeatable steps.

While these technologies drove the rise of a large middle class and, in turn, democracy, technology today has had a rather different impact. Innovations in automation have eliminated vast amounts of low-skilled, but well paid jobs. At the same time, new, higher-paying jobs are emerging, which reward workers with advanced skills.

Back in the nineteenth century, math whizzes didn't have too many chances to profit from their talent. Fast-forward to today and they take home much larger proportions of national wealth as software engineers, bankers or geneticists.

### 5. Lobbyists use their wealth to influence government policy, making the public feel voiceless. 

Have you come across the notion of _repatrimonialization_ yet? This term describes the domination of democratic institutions by rich and powerful individuals who pursue their interests at the expense of the rest of the population. Repatrimonialization is one of the most detrimental influences on American democracy and government. So how is it manifested in practice?

One way repatrimonialization emerges is through _lobbying,_ a legal form of exchanging political power for money. Though political bribery is technically illegal today, gift exchange is condoned. The idea is that individuals that receive gifts will feel morally obliged to return the favor. This indirect form of bribery is the basis of the entire American lobbying industry — and it's no small industry either.

Lobbying and interest groups have expanded dramatically in Washington, DC. In 1971, there were 175 registered lobbying firms. In 1981, this number exploded to reach 2,500. And by 2013, a whopping 12,000 firms were happily spending more than $3.2 billion on lobbying. It's these firms that distort American public policy across many different areas, and one of the most important of these is the tax code.

While nominal corporate tax rates in the United States are higher than most other developed nations, the amount that American corporations ultimately pay is far lower. Why? Because they've negotiated special exemptions and benefits for themselves through lobbying.

As lobbying continues to grow, repatrimonialization is causing a crisis of representation. The power of lobbyists and other calculating and cunning activists causes the public to feel unrepresented and voiceless.

Take the National Rifle Association (NRA), one of the most influential groups in Washington, which advocates for gun rights. It has an unrivaled influence on politicians and policy, and it maintains this influence at the expense of the average citizen's safety.

### 6. The rise and fall of the US Forest Service highlights the weaknesses of political institutions. 

In order to get a better idea of how political institutions can grow corrupt, let's look at the rise and fall of one American government organization: the US Forest Service.

The US Forest Service (USFS) was founded in 1905. At the time, it was a textbook example of American state building during the Progressive Era between the 1890s and 1920s. University-educated foresters and agronomists made up the USFS staff. They were chosen based on merit and technical expertise, unlike most other public offices at the time, which were typically awarded based on _patronage_ — that is, through personal connections and favors. The meritocratic and autonomous USFS embodied some of the best ideals of American democracy.

Today, however, the USFS is infamous for its dysfunctional bureaucracy. Where did things go wrong? It started out with a conflict in public expectations. The USFS originally had one mission: sustainable use of American forests. But over time, the USFS took on the responsibility of containing forest fires.

This created a serious dilemma. Homeowners with properties threatened by forest fires pressured the agency to protect their real-estate interests. Simultaneously, environmentalists were insisting that the organization follow the "let burn" policy, in accordance with new findings that suggested forest fires were necessary elements of ecosystems.

These conflicting groups used the USFS's access to Congress and the courts to serve their own interests. Slowly, the small, unified agency grew into a sprawling, clumsy one as conflicting groups pressured and steered the organization to serve their respective interests.

In turn, bureaucrats at the USFS became more focused on boosting their jobs and budgets by doing what these groups demanded, rather than fulfilling their mission of protecting the forests. This downward spiral is emblematic of what happens across government agencies in the United States on a massive scale.

### 7. Institutional inability to adapt to change is a key source of political decay in the United States. 

So, what does the story of the US Forest Service tell us about government institutions? Well, the strength of these very institutions has everything to do with how they respond to change.

Immunity to change is an intrinsic characteristic of institutions; in fact, it is often a strength. Political scientist Samuel Huntington described the "stable, valued, recurring patterns of behavior" within institutions that allow them to facilitate collective human action.

Without clear, stable rules, members would have to reestablish behavioral norms at every turn. This would be time-consuming and would invariably lead to conflict. Instead, individuals accept the constraints of institutions in order to benefit from their stability.

Institutions are what have helped humans achieve a greater level of social cooperation than any other animal species. From education systems that include public schools and universities to the transportation and energy infrastructure that connect us, most basic human needs are met by institutions.

But institutions are also capable of hindering progress in human societies. How? By failing to adapt to new circumstances.

Democratic decay is often attributed to the inability of institutions to adapt effectively to rapidly shifting circumstances, especially as new social groups with new political demands rise up and challenge the status quo.

But political decay can lead to, and is arguably necessary for, political development. Why? Because the old must break down before the new can emerge. However, if old institutions are incredibly resistant to change, or highly inefficient at incorporating different points of view, they'll never make room for new institutions to emerge.

Therein lies just one of many problems at the heart of American politics today. There are no easy fixes for these challenges. But striving to better understand the complexities of power struggles is central to understanding not only the politics of the United States, but the world as a whole.

### 8. Final summary 

The key message in this book:

**Democracy is a cornerstone of American politics. But democratic ideals, admirable as they are, continue to face many obstacles. The declining middle class, a crisis of representation caused by aggressive lobbying and the inability of institutions to adapt to new circumstances are just some of the issues that American democracy must grapple with today.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Origins of Political Order_** **by Francis Fukuyama**

In _The Origins of Political Order_, Francis Fukuyama delves into the history of modern state-building. Instead of focusing on Ancient Greece or Rome like many earlier scholars, he traces political histories in China, India, the Middle East and Europe. Using a comparative approach, Fukuyama explains how diverse political and social environments allowed Europe to develop many different political systems.
---

### Francis Fukuyama

Francis Fukuyama is an American political scientist, best known for his seminal text _The End of History and the Last Man,_ as well as several other books including _The Origins of Political Order_ and _America at the Crossroads_. He has taught at both Johns Hopkins University and George Mason University, and is currently the Olivier Nomellini Senior Fellow at Stanford University's Institute for International Studies.

