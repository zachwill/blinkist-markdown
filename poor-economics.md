---
id: 5316ff393764650008220000
slug: poor-economics-en
published_date: 2014-03-05T10:49:51.558+00:00
author: Abhijit V. Banerjee and Esther Duflo
title: Poor Economics
subtitle: A Radical Rethinking of the Way to Fight Global Poverty
main_color: A476A7
text_color: 8A638C
---

# Poor Economics

_A Radical Rethinking of the Way to Fight Global Poverty_

**Abhijit V. Banerjee and Esther Duflo**

_Poor Economics_ (2012) is __ investigating some of the biggest challenges poor people face. This book provides the reader with an understanding of why there still is so much poverty in the world, and why many of the measures usually implemented do not help. Based on these insights, the authors offer a number of concrete suggestions to demonstrate how global poverty might be overcome.

---
### 1. What’s in it for me? Find out how the problem of global poverty could be solved. 

The battle against global poverty has been fought by grandees from many fields — from politicians and economists to famous musicians and Hollywood actors. Yet, as we all know, the problem remains.

And _Poor_ _Economics_ attempts to provide several answers to why.

As the authors argue, we are currently looking at the problem in the wrong way. We spend most of our time on general theories about poverty, neglecting the perspective of the poor themselves. Instead, we should be examining the complexity of their lives to get a clearer idea of which kinds of aid are effective and which are not. To that end, we should first seek to understand how and why poor people make the often self-destructive decisions they do, and how we can get them to make better ones in the future.

By looking at the problem of poverty from this angle, _Poor_ _Economics_ shows how we might begin to answer such perplexing questions as: Why do poor people refuse to take medicine that will make them healthier? Why do women in poorer regions bear more children than those in richer ones? And, why do people in poverty spend what little money they have on inferior, non-nutritious food?

### 2. Economists seek general solutions to global poverty, but these don’t deal with the problem effectively. 

It's a sad fact: poverty causes millions of deaths every year. Equally sad is the fact that we have yet to find an effective solution to this devastating problem.

One of the reasons might be that economists dealing with the problem tend to look at it in the wrong way, focusing too much on "big" economic questions like, "Do developing countries need more or less help from outside in order to grow?"

Such general questions tend to generate ineffective debate.

One camp argues for more development aid to get these countries on track, as they're not able to do so on their own. Economist Jeffrey Sachs claims that if rich countries would spend $195 billion per year on development aid, in 20 years (i.e., by 2025), poverty would be eradicated.

Then there's the other camp, which emphasizes the negative consequences and ineffectiveness of outside interventions, claiming that it's more helpful to leave developing countries alone.

Clearly, it's difficult to come up with a conclusive answer to such a general question. Moreover, debating it does nothing to solve the problem of poverty.

Of course, some countries appear to prove the effectiveness of development aid. For example, following the Rwandan Genocide, Rwanda received a lot of money and their economy subsequently boomed. However, the situation's complexity renders it impossible to know for certain whether Rwanda's success was due to the aid or to other factors.

Furthermore, a single case isn't enough to prove a theory's accuracy: data from over 100 countries suggests that those that had received aid didn't grow anymore than those that didn't.

Both of these arguments are manifestations of an economic reasoning that looks at the problem of poverty in the wrong way. And, in the end, such arguments will not help to solve it.

Rather than spending time and energy on considering these big questions, it's more useful to look more closely at specific measures and to evaluate why they do or do not work.

### 3. Looking seriously at how poor people make economic decisions is vital for eradicating poverty. 

Until now, the economic complexity of poor people's lives hasn't received much scientific attention. Perhaps since most poor people live on less than $1 a day (13 percent of the world's population in 2005), economists seem to believe that the poor have no complex economic decisions to make.

But that's not true.

Poor people are forced to be very rational when it comes to economic matters. Due to their lack of financial resources and awful living conditions, they have to reflect far more on economic questions than people living in developed countries.

Any economic decision or investment that poor people make could have devastating consequences — consequences that might threaten their very existence — so naturally they have to consider each decision very carefully.

But if scientists would take the economic decisions of poor people more seriously, it would help us to understand what measures are actually required in the fight against poverty.

For instance, in many cases, poor people reject or neglect to use the help offered by NGOs and governments, and don't think long term. Consider, e.g., the fact that poor people don't exploit the availability of the free vaccinations that could greatly improve their lives. And they often spend their money on rather useless things, like coffee, instead of saving it for something more important.

These decisions, however, are not irrational or random, but influenced by an array of different factors, like economic considerations, social beliefs and psychological effects.

Therefore, it is only by gaining a deeper appreciation and understanding of the conditions under which poor people make their decisions that we will begin to learn precisely what kind of help is needed, and how to get them to make better decisions for themselves.

### 4. Non-nutritious food – not hunger – is what reinforces poverty. 

Most of us would associate poverty with hunger without thinking twice. Yet the link between the two is not as obvious as it seems.

One popular view held by economists and politicians is that hunger is one of the main reasons for poverty's persistence. Indeed, this seems reasonable since nobody can be productive if they're hungry.

But that view is wrong. Although many people suffer from hunger today, being hungry can't be seen as a general cause for contemporary poverty.

In fact, data from more than 18 countries shows that most poor people don't have to spend all their money on getting enough calories — which means they could spend more on food if they needed to.

And even with the money they do spend on food, they often choose to buy more expensive, tastier foodstuffs, rather than to maximize their calorie intake.

Furthermore, the problem isn't in the quantity of food available: if poor people had better- _quality_ food, they'd be more capable of improving their health and productivity.

Most poor people, particularly children, don't get enough high-quality food, which means they lack many micronutrients, like iron or iodine. This nutritional lack in poor people's diets has consequences for children's development and adults' economic life. In fact, one study in Indonesia showed that men and women who took iron supplements for a couple of months were able to work harder and earn more money.

Since poor people are often unaware of the importance of nutritious food, it would help if governments and NGOs provided nutritious food or food supplements, and created food rich in essential nutrients. In India, e.g., they've introduced iron-fortified salt.

It's crucial that we leave behind the common belief that poor people just need cheaper food. Instead, we should focus our efforts on providing them with high-quality food they'll enjoy.

### 5. A reliable and proactive healthcare system is necessary for improving the health of the world’s poor. 

Staying healthy is extremely important for poor people. Just one sick family member can have a disastrous effect on the whole family, as it can cause a drop in income and generate additional costs.

The following three things are required to help raise health levels in poor countries:

First, better provision of medical help is needed. Although most developing countries have a public health system, poor people rarely use them. The reason is simple: the system doesn't work.

Indeed, many medical stations are often closed, and most medical center staff are unmotivated. One World Bank study showed that the average absence rate of medical center staff in different developing countries was 35 percent.

Therefore, governments must make public health systems more reliable so that poor people have access to effective healthcare.

Second, people have to be educated about health issues. Even if a more reliable health system were in place, people might not end up making use of it because of cultural beliefs and a possible lack of information.

For example, research in Udaipur, India, shows that poor people do not believe that the cheap drink solution they can get from medical stations actually helps treat diarrhea; they believe that, in general, medicine works only when injected into the bloodstream.

Third, and finally, sometimes people need the right incentives to care for their health, as medical facts don't always motivate people enough.

For example, while people know the importance of vaccinations, they often don't complete the treatment. Like most people, they simply don't care about things that don't offer them an immediate benefit.

And so, the right incentives need to be found and employed. One project conducted in India showed that the rate of completed vaccinations increased dramatically from 6 to 38 percent once a set of plates was promised for every vaccination.

The better we understand what prevents people from using medical support, the more we'll know about how to change that behavior.

### 6. Developing countries need better school systems that focus on educating all children. 

Another very important factor for overcoming poverty is education.

In recent years, many developing countries have addressed this by providing educational facilities for their children.

However, developing countries must ensure that children do not just attend school but are actually _educated_. Although it's a positive development that governments have provided all children with at least an elementary education, the fact that public school staff often don't care about the success of their teaching is a problem.

Consider, again, the above-mentioned study by the World Bank, which shows that teachers in public schools are often absent. That research revealed that 50 percent of Indian teachers were not actually in their classrooms when they should've been. The sad result of such carelessness is that children in India are barely educated.

In addition to that, elitist thinking in developing countries must be questioned in order to educate more children. Since, e.g., Indian society values elitist thinking, its education system neglects to teach its children the most basic skills. Instead, the system is very demanding academically, and is thus directed towards only the most academically minded students. The result is that a huge number of children without an academic disposition are ignored.

Finally, parents must take care to not let their incorrect economic assumptions prevent the majority of their children from receiving a basic education. Polls in developing countries show that parents believe it's more rational to spend their entire education budget on the most promising child than it is to support all of their children's education needs.

Their calculation is wrong: parents themselves need to learn that it makes more sense give a greater number of their children a basic education than it does to have only one highly educated child.

Ensuring that more children are educated is a complex but crucial challenge for developing societies to undertake if they are to fight poverty effectively.

### 7. The key to population decrease is better social protection of the poor and the empowerment of women. 

Because of the strong connection between population growth and poverty, many countries have attempted to regulate that growth. As you might imagine, however, fighting population growth is a complex issue.

For one, as long as there's no social security system or strong incentives to save money, having many children is a rational choice for poor people.

This is because old people depend on their family to take care of them. So, the more children people have, the more likely they are to have support when they're old or sick. Indeed, most people in developing countries receive financial support from their children or even live with them — as is the case in China, where over 50 percent of old people live with their children.

Therefore, one of the solutions to population growth is to improve the social protection of the poor.

Another way to gain control over a country's growth problem is to empower its women. Most women consciously decide to have children. That's because, compared to staying at home and serving their family, the prospect of getting married and starting a family is the more attractive option.

It's highly unlikely, therefore, that the population problem can be solved by measures like more education or providing information about how to use contraceptives.

Yet, empowering women, and strengthening their role in marriage and society, would lead to fewer children.

Interviews with both men and women reveal that women usually want less children than men do, suggesting that an empowered women would choose to have fewer.

A good example of the link between women's empowerment and population control can be found in Peru, where the government gave land to former peasants. In the documents where the woman of a couple was also mentioned, that couple had fewer children because naming them implied their power.

Measures against population growth must be considered carefully. We will only be able to fight it if we understand where it really comes from.

### 8. Financial innovations, like microcredits, improve poor people’s lives, but they’re not a long-term solution to poverty. 

One popular, though controversial, instrument for helping the poor is _microcredits_.

Microcredits are useful in supporting poor people and their businesses in developing countries. Without them, poor people would have to approach banks, which have little interest in giving affordable credits to poor people (because of the small amounts involved and the risk of not being paid back), or loan sharks, to whom they'd have to pay very high interests.

The introduction of microcredits in developing countries provides a viable alternative for poor people. Special financial institutions lend small amounts of money to poor people so that they can make an investment to get a business started (e.g., rent a shop) or to buy equipment (e.g., a wheelbarrow) which can help them be more productive.

The result is that microcredits actually work: studies show that families who got microcredits were more likely to invest in their own businesses and to take care of their economic future.

However, the impact of microcredits is limited to the short term.In fact, most of the small businesses funded by microcredits don't survive for very long at all.

There are two main reasons for this.

First, while microcredits are a good way to start a business, promising businesses must grow and make additional, larger investments. However, there are no institutions from which the poor can loan a larger amount of money at a reasonable interest rate. Therefore, other financial instruments are needed for businesses to be economically viable and grow.

Second, many small businesses get started only because there's no other alternative for poor people to make money. And, as there's not really a demand for those businesses, most don't survive for long. For example, there's no need for five small shops to sell the same groceries on the same street.

Therefore, it's crucial to point out that, in the end, good and secure jobs are still the best way to fight poverty.

The right financial instruments can support poor people and developing economies to grow, but these instruments can't be used to hide the fact that the problems are more complex.

### 9. Insurance against misfortune protects against poverty, so governments should incentivize the poor’s coverage. 

In contrast to most poor people around the world, those in rich countries are lucky, as they're insured against many risks (e.g., losing their job or sickness).

So if there were more affordable opportunities to get insurance in developing countries, many poor people's fears could be reduced.

For instance, poor people have to deal with a lot of insecurity in their lives. They're often peasants and greatly depend on the weather and price fluctuations; or they have several jobs and will be hit hard if they lose just one of them.

To combat this, in many villages and neighborhoods in developing countries, poor people have created networks to help those in need. However, compared with full-fledged insurance systems, this kind of assistance is limited. For instance, only smaller financial problems are covered by this rudimentary security, so they don't help improve any real hardship.

In the cases where getting properly insured is a possibility for poor people, governments should intervene by providing both poor people and private insurers with incentives.

Although it isn't possible to insure all the risks poor people face, there are some risks that can be insured, e.g., a meager crop yield or other weather-related risks.

But the poor barely even consider taking out insurance policies offered by private companies and the reasons for that are numerous: it's expensive, poor people are skeptical of it or they simply don't think long term, like most people.

Therefore, the poor clearly need incentives to get themselves proper coverage, so the governments have to step in to get the market going. For example, when weather insurance was subsidized in Ghana, almost all poor people signed up.

Once the poor see that the insurance system works for them, subsidies could be reduced and a free market could exist.

### 10. A successful fight against poverty will require small, local actions alongside big institutional reforms. 

By now it should be clear that there is no easy solution to the question of how to end poverty. And yet, many experts recognize that we must begin by focusing on major institutional changes.

One popular view held by economists and political scientists is that we will not be able to fight poverty effectively as long as there are unstable political regimes. For those experts, corruption is a huge problem as it means, e.g., that corrupt officials often steal money allocated to projects dedicated to fighting poverty.

Therefore, these experts concentrate on the question of how we can best achieve big institutional changes in these countries — e.g., by implementing a new, more democratic political regime — believing that this is essential to fighting poverty effectively.

However, while the question of large-scale, fundamental institutional changes is central to the battle against poverty, we shouldn't forget that there's room for local improvements.

That's because small and local actions can be extremely effective in countering the political shortcomings of governments (e.g., corruption). 

For example, one 1996 study in Uganda showed that only very few of the subsidies the government said it gave to schools were actually received by them. The Ugandan people were outraged and demanded reforms. Ultimately, the treasury reacted and, when the study was repeated in 2001, on average 80% of the money intended for the schools actually reached them.

In this case, all that was required to fight corruption was information from economists and public protest.

Therefore, we must not think that we have to wait until big institutional changes occur, as there are many simple, local measures that can lead to the political improvements necessary to combat poverty effectively.

### 11. Final Summary 

The key message in this book:

**The fight against poverty is very complex and there is no easy solution. But we must not give up: a better understanding of the lives of poor people, and a close examination of the measures we take against poverty, will help us to see what's essential to winning the fight against poverty.**

Actionable advice:

**Be skeptical about "big" solutions to poverty**.

Keep in mind that measures against poverty will be only effective if we understand the circumstances of poor people's decisions. One common, and general, misunderstanding is that the poor are incapable of thinking rationally about money. This isn't true at all: they are just as rational as the rest of us. Remember that the next time you want to help alleviate someone's poverty.

**Never forget how important education is.**

In the developed world, we often take our education for granted. We shouldn't: it's the most vital tool against poverty. Being educated — about economics, science (e.g., contraception), numeracy and literacy — can help poor people make better decisions and lead better lives.

If you're looking for organizations that try to combat poverty in the ways the book suggests, the authors provide a list of organizations on their website (pooreconomics.com).
---

### Abhijit V. Banerjee and Esther Duflo

Abhijit V. Banerjee and Esther Duflo are both professors of economics at the world-renowned Massachusetts Institute of Technology. For more than 15 years, the two have researched questions of global poverty, spending several months in the field to better understand what needs to be done to combat poverty effectively. This book is the result of their work.

