---
id: 53dfdb613838370007390000
slug: pop-en
published_date: 2014-08-05T00:00:00.000+00:00
author: Sam Horn
title: POP!
subtitle: Create the Perfect Pitch, Title and Tagline for Anything
main_color: ED4B2F
text_color: BA3B25
---

# POP!

_Create the Perfect Pitch, Title and Tagline for Anything_

**Sam Horn**

This book shows you how to craft messages and ideas that _stick_ in the minds of your audience because they are Popular, Original and Pithy. Author Sam Horn offers a highly adaptable and systematic approach to creating pitches, titles and taglines that grasp people's attention and win them over to your brand, product or idea.

---
### 1. What’s in it for me? Create pitches, titles and taglines your audience can’t forget. 

People nowadays are overwhelmed with all kinds of advertising pitches that compete for their time and attention. In this information-saturated age, if an ad isn't presented in a unique and instantly appealing way, it'll remain lost in the noise.

There is a way, however, to pierce through the chaos and get your message heard. POP! shows you how to make your messages so compelling that your audience simply _can't_ _resist._

You'll learn the secret ways of wrapping your brand, idea or product in memorable pitches, titles or taglines that'll get people's attention — and won't let go.

In the following blinks, you'll discover:

  * how to get anybody's attention with your message — and keep it,

  * how to be HUMORiginal,

  * how to learn to paint your own word pictures, and

  * how to develop systematic methods to boost your creativity.

### 2. The key to a sticky message is to make it POP!: Purposeful, Original and Pithy. 

Every single day, from the moment we connect to the internet or walk out onto the street, we are bombarded with ads that fight for our money and our attention. But have you ever wondered why some ads are more successful than others?

The answer is simple: the ads that grab your attention are the ones that POP! — or that are Purposeful, Original and Pithy.

Let's examine these elements one by one.

First comes _Purpose_. Whether you're trying to spread an idea or sell a product, your message has to instantly communicate your purpose — and how it positively affects your audience.

In fact, the best wordplay in the world won't POP! unless it does two things: it must precisely articulate the essence of your message and show your audience what's in it for them.

Then comes _Originality_. You've got to remember that no matter how brilliant your product or message is, someone else is probably trying to do something similar. Therefore you need to distinguish yourself by every means possible, and in particular, by _being_ _original_ in what you do.

Keep in mind that people's yearning for something fresh is so strong that even if the essence of what you're doing is not new, it still has a chance of taking off if you can make it _original_.

Finally, there's _Pith_. The word pith means the essence of something — a concept that's crucial, as the human brain can actually only hold about seven bits of information in short-term memory! This means that you _have_ to be pithy and convincing in a second, or your idea will soon be forgotten.

It's no coincidence that the top slogans of the twentieth century, as selected by Advertising Age magazine, are all less than seven words: De Beers' "Diamonds are forever," Nike's "Just do it," and Avis' "We try harder."

### 3. By answering the big W’s, you create a foundation of core words that form your purpose. 

When you think about it, it seems obvious: the first step to good communication is always to find out exactly what you're trying to say.

This means that if you want to make a message POP!, you'll need to have precise answers for the big W's: _what_ you are offering _,_ _who_ you're aiming at and _what_ you want to achieve _._ You also need to consider _what_ value you're offering your customer and _who_ your competitors are.

When you're answering these questions, keep one guiding principle in mind: you need to examine the essence of your offering from every possible angle, to create a detailed bank of _core_ _words_. Core words are the key descriptive phrases that articulate the essence of your product or idea.

And although it could take ten minutes to two hours to answer these questions to your satisfaction, it's time well spent. Remember that core words are the foundation from which all POP! messages are built. You'll be using them regularly to create powerful messages in future exercises.

As your product and business evolve, you should repeatedly evaluate your answers to the big W's and your core words to make sure your branding is as fresh as your latest creations.

> _"I have six honest servants... Their names are what, why, and who, and when and where and how."_ — Robert Louis Stevenson

### 4. Play with your core words and put yourself in your customer’s position. 

As they say, there is nothing new under the sun — which means being original is close to impossible. And yet people still demand originality if they are going to give you their attention.

So what can you do?

You have to do your best with the most powerful tool you have — the core words that embody your idea — and catch people's attention with an original phrase. This means you need to push your creative potential and create a one-of-a-kind name.

And if you're successful, the media will help you out for free.

Why?

Because the media is always on the hunt for the next big thing to report. This means that if you can come up with a catchy word or phrase, you not only get the attention of your customers but also get free advertising through media coverage.

So, how can we create an original phrase?

One way is to use "_Spell_ _Chuck,_ " and discard the normal way of spelling things. For some reason, beauty salons seem to be very good at this: Consider _Shear_ _Genius,_ _Curl_ _up_ _and_ _Dye_ and _Cut_ _and_ _Dried_.

Another way of creating an original phrase is to run your core words through the alphabet, from A to Z, and keep modifying the first syllable until you find something that corresponds with your idea or product.

For example, the word entrepreneur can be transformed into a variety of trademarkable terms — a spiritual business owner could be a "Zenpreneur," for example.

You can extend this wordplay approach and try playing with the _last_ syllable, or add a prefix (e.g., re-, multi-, pre-) or suffix (e.g., –ly, -ology, -ish) to your core words. For example, consider the new word _re-newlyweds_ that describes couples who have renewed their wedding vows later in their marriage and are "wed" once more.

### 5. Tap into your audience’s love of music. 

We all love music: it fills us with energy, it takes us to the highest highs, the lowest lows and back again. But did you know that you can use musicality to increase the memorability of your messages?

One great way of using musicality to increase memorability is by using _alliteration_ : words that start with the same sound can have a pleasant ring to people's ears.

For example, what do you feel when you say these words: _Bed,_ _Bath_ _and_ _Beyond;_ _Dunkin´_ _Donuts;_ or _Rolls_ _Royce_? Isn't it pleasant to say and hear them? Aren't they intriguing and memorable?

This is the magic of alliteration.

Review your core words and try to find new words that form a series of alliterations. You can also run the first syllables of each word through the alphabet to potentially create an interesting neologism while you're at it.

Another way of increasing memorability is summed up in a Duke Ellington song title: "It Don't Mean a Thing If It Ain't Got That Swing."

Indeed, rhythm and rhyme are powerful tools of easing comprehension, and can make even longer strings of information memorable. Take the alphabet song, for example: A, B, C, D, E, F, G — pause — H, I, J, K, L, M, N, O, P. Many of us still use the song to remind ourselves which letter fits where!

A good way of applying rhythm to your own idea is to try and use an _iamb_. In an iamb, or iambic meter, we accent every other beat in a sequence, as in "Ta DUM, Ta DUM, Ta DUM." Try reading the following commercial jingle out loud to understand iambic meter: "Double your pleasure, double your fun, with Doublemint, Doublemint, Doublemint gum."

If you still doubting that rhymes help you remember, try this on for size: if you were asked to think of two instinctive human responses to danger, what would they be?

I'm betting your answer was, "Fight or flight!"

### 6. Relate to your customer’s world through celebrities and popular media. 

As we saw in a previous blink, to ensure your message sticks, you have to make it personal. So if you want your message to POP!, you need to relate your message to your _customer's_ world, thoughts and everyday life.

How?

Start by asking yourself: what do your current or future customers say or think when they experience the need your idea fills? What could a possible "Eureka!" or "At last!" moment feel like, when the customer's problem is finally properly addressed? By placing yourself in your customer's shoes, you'll realize exactly what they need — and how to tell them that you've got it.

Another powerful method of making your message personal is to relate it to a celebrity or popular piece of media.

For example, consider the questions: who is also doing what I do, or what also represents certain core aspects of my idea that everyone knows about?

Amy Rosenthal did this brilliantly when she wanted to spread ideas about being a loving mother and juggling many responsibilities at the same time. She understood that young babies react very strongly to our temperament, which makes it very important to maintain calm. So, she asked herself, who is internationally known for being calm, peaceful and tranquil?

The Dalai Lama!

She then ran the name through the alphabet and voila, there it was, _Dalai_ _Mama,_ two words that instantly communicated what Amy wanted to say.

Another more systematic approach to making your message personal is to relate it to a popular piece of media. Review your core words once more and use your favourite movie, song and book search engine to find a match with a popular piece of media.

> _"Without a first-person story, it's all rhetoric."_

### 7. Be HUMORiginal – but avoid clichés like the plague. 

Everyone loves a good laugh, and most of us would love to be funnier — especially when we're trying to create memorable messages. But can humor really be taught?

Probably not — but there are some methods you can use.

First, pay close attention and learn from the humorous moments around you. Family celebrations are often an excellent source of humor, as are TV shows and movies. Remember, inspiration is better than information, but if you do borrow a joke, remember to credit the original or it will be perceived as a rip-off.

You can also raise your sensitivity to the timing and context of your humor, which is just as important as the content. Watch carefully how comedians deliver their lines, and try to be surprising and out of the blue with your jokes. But when it comes to creating your memorable messages, remember to pay attention to your purpose — don't be funny for funny's sake, and always relate your jokes back to your core words to keep on message.

The problem with jokes, however, is that they can easily lose their novelty factor. And there's nothing more deadly to your likeability than using clichés. However, rearranging clichés to make them fresh is fair game — and can even be powerful.

Here's a systematic approach to rearranging clichés. First, enter your core words into a cliché search engine, like ClichéSite. Then write down everything you read that makes you giggle or creates a quirky connection to your core words. Then, substitute or rearrange the key elements of the original cliché with your core words to turn an eye-roller into an eye-opener.

A good example is business writer Steven Pearlstein's powerful title for an article about a group of managers who were concerned about not having access to their BlackBerries, due to pending litigation. The title?

"Big Firms Caught with Their Patents Down."

### 8. Visualize your message with pictures and metaphors. 

As they say, an image is worth a thousand words — which means that if you can create an image with only a few words, you've increased the power of those words a thousandfold!

But before you start creating visualizations for your idea, you need to ask yourself what attributes your product has, and how those attributes are embodied in the real world.

Let's clarify with an example: What are the attributes of superglue? Answer: It's really strong. And what is something real, that is really strong? The brand Gorilla Glue answered this question with an 800-pound gorilla, which became a powerful symbol in their ads.

You can use your answers to create _meaningful_ _metaphors_ that empower your message. The best method for creating personalized and unique metaphors is to focus on the three A's: Avocation (e.g., a hobby), Achievement and Adversity.

To do this exercise, you need to take a fresh piece of paper and on it, draw three vertical lines. Use one column for avocations you are passionate about, one for your proudest achievements and one for all the adversities you have faced so far.

Now look for similarities and connections between the columns. Can you create analogies and use past experiences to create stories that better explain what you're trying to say?

A good example of the power of this method is how a business executive used his flying lessons as an example to curb micromanaging in his company. He told his managers how his flying teacher never took the yoke out of his hands — not even in critical situations — but instead gave general instructions and let him figure it out. "Let them fly the plane" thus became an expression in the company for "stop micromanaging."

### 9. Be counterintuitive and catch your audience by surprise. 

"TV IS GOOD FOR YOUR KIDS!...no, it's not." Bet that message got your attention, didn't it?

The reason why is because it's _surprising_ _and_ _counterintuitive_, which is often enough to catch your audience's attention and get your message across. Like author John LeCarré once said, "'The cat sat on the mat,' is not the beginning of a story. 'The cat sat on the dog's mat,' is."

To create your own message that surprises, start by looking at your core words and the answers to your W's from an earlier blink. Now ask yourself: Where do I dare challenge common sense and suggest that people change their minds, instead of offering blind allegiance? Where do I even dare to be politically incorrect? And most importantly: How does my product address a topic no one wants to talk about?

Another surprising way of raising curiosity is the _half-and-half_ _technique_.

This technique was used to great effect on the popular FOX TV show, _The_ _O.C.,_ in 2004. One episode featured a Christian family and a Jewish family that blended the holidays Christmas and Hanukkah together to celebrate a brand new feast: _Chrismukkah_. The episode created a buzz that lasted for months, and the show itself generated a record number of viewers.

If you want to produce a similar reaction to your idea, try this: Take out a piece of paper and write one half of your core words on the left side and one half on the right. Now try and blend each word from the left column with the right, and write any combination that strikes you on a different piece of paper. Write as many as possible, keep playing — you never know what'll come up!

### 10. Keep your audience’s attention with the power of stories. 

We've now seen many powerful ways of creating memorable messages. But if you want to make your message come alive, you'll have to wrap it in a story.

Why?

Because when you give people a meaningful story to which they can personally relate, they switch from simply processing information to emotionally relating to your message.

One way of doing this is to create a powerful history behind your idea. Think back to when you came up with your idea, product or brand. What did you feel, say or do? Where did your ideas come to you, who inspired you? Interview everyone who worked with you and create the authentic story behind your cause.

For example, when SpeakerNetNews co-founder Rebecca Morgan told the story of how she came up with her new book, she quickly had everyone's attention. She was visiting a mall when she was approached by a salesperson with samples of orange-spiced chicken. When she repeatedly refused, the salesperson became more and more persistent, to the point that Rebecca decided never to buy anything there again.

But when she left the mall, she smelled the delicious flavor of chocolate-chip cookies being sold on another counter. No one had to approach her — she couldn't resist the sweet smell and bought a whole batch of her own free will. That's when she came up with the title for her new book: "Chocolate-Chip Cookie Marketing: The Anti-Hard-Sell Approach."

Another powerful way of sustaining your audience's attention is by tapping into the stories that all of us know.

To do this, start thinking of famous phrases out of a story or movie. (Consider: "I'll be back" from Terminator, or "Here's looking at you, kid" from Casablanca.) Then review your core words and see if you can create your own derivation of this kind of classic phrase. If you succeed, the memorability of your phrase will drive word-of-mouth marketing for you.

> _"The world isn't made of atoms, it's made of stories."_

### 11. Final Summary 

The key message in this book:

**If** **you** **want** **your** **message** **to** **stick,** **you** **have** **to** **make** **it** **Purposeful,** **Original** **and** **Pithy.** **Know** **your** **core** **words** **and** **use** **rhythm,** **rhyme** **and** **stories** **to** **communicate** **your** **purpose** **directly** **to** **your** **audience.** **Remember** **to** **keep** **your** **message** **fresh,** **to** **the** **point** **and** **surprising.**

Actionable advice:

**Learn** **more** **about** **pop** **culture** **to** **harness** **the** **collective** **unconscious.**

The more you know about pop culture, the more you know about how people are thinking. Stay up to date on the latest trends by checking out the most popular memes, news stories and YouTube videos. Then you can combine your core words with the freshest entertainment out there.
---

### Sam Horn

Sam Horn, communication expert and founder of Intrigue Agency, is widely known for her award-winning speeches and highly interactive workshops on effective communication. An expert on branding and innovation, she has written books such as INTRIGUE and Tongue Fu!, and worked as a pitch coach for Springboard Enterprises, helping entrepreneurs design and deliver effective pitches.

