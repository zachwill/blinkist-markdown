---
id: 5c06a4e26cee070008ff3da2
slug: populism-en
published_date: 2018-12-06T00:00:00.000+00:00
author: Cas Mudde and Cristóbal Rovira Kaltwasser
title: Populism
subtitle: A Very Short Introduction
main_color: 7F191B
text_color: 7F191B
---

# Populism

_A Very Short Introduction_

**Cas Mudde and Cristóbal Rovira Kaltwasser**

_Populism_ (2017) investigates one of the political buzzwords of our age, which is often encountered in the media and thrown around by political opponents. Populist leaders attempt to mobilize the frustrations of the masses by claiming to speak for "the people." By placing blame for social and economic problems on a supposed "elite," populists attempt to gain political success.

---
### 1. What’s in it for me? Discover the inner workings of the political buzzword of our times – populism. 

There's no doubt you've heard of populism. This term is attached to the rise of right-wing parties in Europe and left-wing movements in Latin America. And then, of course, there's the populist conservative insurgency that led to the election of Donald Trump. But how can one term describe such a wide range of political parties?

First of all, populism is less a political ideology than a political strategy. By mobilizing masses of people against so-called corrupt elites, populists are able to harness widespread grievances in society and attain political success. With populism on the rise, understanding the internal mechanisms of this political strategy has become more important than ever.

In these blinks, you'll learn

  * why populism must attach itself to _host ideologies;_

  * how populist Silvio Berlusconi used accusations of sexual misconduct to his advantage; and

  * how the financial crisis of 2007-2008 led to a rise of populism around the world.

### 2. Populism is a political phenomenon in which the people’s interests are juxtaposed against those of the elites. 

Over the last few years, the term populism has increasingly appeared in political and media discourse. Although populism is a growing political phenomenon, it is often misunderstood and treated more as a buzzword than an actual political strategy. It is used to describe political movements from both the left and the right — from Bernie Sanders' left-wing 2016 presidential campaign to the right-wing nationalist ascendancy in Europe. But what really is populism?

We can attempt to define populism as a political worldview in which societies are split into two contrasting camps — the _people_ and the _elite._ Furthermore, populism maintains that policy should be determined by the _general will_ of the people. A populist conception of the people can have different forms — either as _sovereign, common_ or the _nation –_ or a combination of the three.

A populist understanding of sovereignty implies that the people themselves — and not the elite — should hold political power. Meanwhile, the portrayal of a common people revolves around a shared socioeconomic class. Finally, in the understanding of the people as a nation, the people constitute a national community. The corrupt elite stand in opposition to these differing constellations of the people. This can refer to a ruling political class, mainstream media or the wealthiest members of society.

Within the populist worldview, different components of the elite are often working to prop each other up. For example, the wealthy are seen as supportive of political elites for their own vested interests against the general will of the people. This is the final core element of populism, consisting of what the eighteenth-century philosopher Jean-Jacques Rousseau termed the "volonté générale," or general will. Rousseau used this term to describe the people's ability to come together in order to bring about change that is beneficial to them as a group.

And it is through this general will that the charismatic, populist leader comes in. This leader must be able to identify the general will of the people and also forge a community of individuals who share it.

### 3. While local contexts are key in the development of populist movements, host ideologies are equally important. 

On both sides of the Atlantic, left- and right-wing populist movements command increasing attention in our public discourse.

But while many populist movements share a number of characteristics, they stem from a myriad of individual political contexts. This means they often appear very different from each other. Their distinctions are made even more apparent when examining the different _host ideologies_ that populisms must attach to — from the socialism of left-wing populism to the nationalism of right-wing populism.

In twenty-first-century America, for instance, the importance of host ideologies comes into focus when examining the two populist movements that came out of the Great Recession — the left-wing Occupy Wall Street and right-wing Tea Party movements. At first glance, the two groups have much in common — they both railed against the government bailouts resulting from the 2007-2008 financial crisis, and sought to defend the American people, often labeled "Main Street," against the corrupt elite represented by "Wall Street." In the case of Occupy and the Tea Party, though, this is where their similarities end — and where the importance of host ideologies comes in.

Occupy, for example, promoted an inclusive, left-wing populism with a focus on defending all disenfranchised Americans — the "99 percent" — against what they saw as a corrupt financial system propped up by Washington elites and Wall Street financiers, or "the 1 percent."

On the other hand, the populist Tea Party movement relied on an exclusionary and conservative host ideology. While they were also against the government bailouts, they had a completely different idea of who constituted the elite. According to the Tea Party, Wall Street alone was not to blame for the people's misfortunes. They also took issue with the cultural elite symbolized by Hollywood, liberal academia and Democrats. These groups were all to blame for the propagation of un-American ideas that led to big government policies like bailouts.

Geographic contexts and local issues play a central role in the characteristics of populist movements. In an increasingly globalized world, however, the importance of host ideologies means that populist movements in different regions can share a lot. In Europe, political responses to the financial crisis of 2007-2008 led to the formation of populist movements similar to Occupy Wall Street. In an economically devastated Greece, for example, the left-wing populist party Syriza came to power by mobilizing the disenfranchised Greek people against economic elites. And while the enemy elites weren't Wall Street, blame for the people's economic woes was similarly placed on the European Central Bank and the International Monetary Fund.

### 4. Populist leaders often use a personalist approach to mobilize supporters and attain political success. 

What comes to mind when you think of a populist leader? Most likely, it's the image of a strong, charismatic male. With some notable exceptions, nearly all successful populist leaders over the last decades have been male and have, through charisma, mobilized masses of people against elites. When successful, they have gained political power in the process. Think of Trump's successful 2016 campaign, or Beppe Grillo's Five Star Movement in Italy.

One way that such leaders mobilize the masses around their agendas is through _personalist leadership._ Such leaders claim to personify masses of disenfranchised people who are oppressed by elites. Thus, only they, the leader, can free the masses from this servitude. Nevertheless, in the liberal democracies of today, this mobilization strategy still necessitates a _personalist political vehicle,_ or pseudo-organization that allows populist leaders to participate in elections.

Peruvian populist Alberto Fujimori demonstrated this style of mobilization in the late 1980s. At the time, Peru faced a formidable communist insurgency and a crippling economic crisis. Enter Fujimori, a Japanese Peruvian who was able to mount a successful populist campaign around his personalist leadership. Not only did he rail against the Peruvian elites for causing a multitude of crises, but he also used his Japanese heritage to emphasize his outsider status. With this, he claimed he was one of the people. His campaign slogan? "A President Like You." His personalist political vehicle took the form of Change 90, a party he founded specifically for the purpose of winning the presidency in 1990. Once elected, however, the weakness and inexperience of Fujimori's personalist political vehicle meant he was eventually forced to bribe MPs to support his government's legislative agenda. When this was discovered, he met his political downfall and resigned from office.

In Bolivia, a personalist leadership approach was more successful. This was because its populist leader, Evo Morales, sought election by branding himself a man of Bolivia's indigenous people and coupled this with a strong political party, Movement Towards Socialism (MAS). MAS and Morales also both had links to well-organized populist social movements that fought against neoliberalism and discrimination in the 2000s. Since his election in 2006, Morales' personalist approach — as well as his strong political party and grassroots social movements — led to his re-election in 2009 and 2014.

> Personalist mobilization strategies tend to work best in states with presidential systems where the people can directly elect a powerful leader who claims he alone can combat corrupt elites.

### 5. To demonstrate their connection with the people, populist leaders employ a number of key characteristics. 

It's not easy to become a populist leader who is able to harness the will of the people and claim to truly represent them. In order to convince the masses that they are not part of the elite, populists attempt to represent a number of carefully crafted characteristics. The first and most obvious characteristic of a populist leader is that they must present themselves as a _strong leader._

Such a leader should portray themselves as independent of established political organizations and, therefore, be able to say whatever he wants. He is then free from the restrictions that usually apply to mainstream politicians. In this way, a populist leader can demonstrate the strength of his political intention — he is willing to break away from the political norms, showing he is one of _the people_.

But words are often not enough, which is why a strong populist leader must also present himself as a _man of action._ Even in the face of contrary expert opinions, populist leaders will not shy away from making what they portray as urgent, difficult decisions. As populism often arises in times of political uncertainty, populist leaders will take advantage of an insecure situation to take bold action and upend the status quo.

As many populist leaders are men, they often emphasize their _virility_ to show their masculinity, strength and, of course, to relate to common people. For example, after being accused of illegitimately fathering the child of a much younger woman, Italian populist leader Silvio Berlusconi admitted that it might be true, proclaiming that lots of women want to have his children. To further prove their masculinity, populist leaders often use a _stammtisch discourse,_ or the sort of language one might hear at a bar. By using simple, coarse and often sexist language, they can claim to be "one of the boys," further emphasizing their everyman credentials.

Finally, populist leaders use _charisma_ to show the strength of their own personal characters. Originally described by German sociologist Max Weber as a politician's "gift of grace," charisma is all about presenting oneself as being completely devoted to the people's cause. In other words, populist leaders represent themselves as a savior-like figure that will guide the people out of a crisis and into redemption.

### 6. After populist attitudes are activated, they can be difficult to reverse. 

As Western countries experience the rise of populism, it becomes more important than ever to identify how these attitudes are activated in the first place. The most obvious reason behind the activation of populist attitudes is, of course, the perceived failure of established political parties. After the economic downturn resulting from the 2007-2008 financial crisis, such attitudes are currently widespread.

Once an electorate feels abandoned by the politicians they elected to represent them, populists are able to easily emphasize the failure of the status quo. For example, when Greek social-democratic Prime Minister Georgios Papandreou enacted austerity policies after the financial crisis, his voters felt betrayed by the party they had brought to power. This created a fertile breeding ground for populist movements, eventually leading to the election of Syriza, backed by its left-wing anti-austerity agenda.

Also, mainstream media is key to populism. In Finland, for example, right-wing populist party True Finns was able to gain a vote share of 19 percent in the 2011 general election. This is remarkable for a country only moderately affected by the financial crisis. However, after a corruption scandal that engulfed established parties was widely reported in the media, True Finns took the opportunity to gain media attention. By doing so, they were able to attach their anti-immigration stance to the corruption scandal, thereby creating a narrative of political crisis. These views were then widely disseminated through Finnish media.

So how can populist activation be moderated — or reversed? In the case of Finland, the corruption scandal was aggravated by the establishment's response — or lack thereof — that did not offer its electorate an accountable investigation process. Such moves only support the view that establishment parties belong to a homogenous elite which works for its own interests. Such elite unresponsiveness to populist sentiments adds firewood to the populist bonfire.

What establishment parties must therefore do is be more open and honest about their failures. While they promote their successes, they must also act transparently and accept responsibility when things go awry. In addition, establishment politicians must attempt to offer answers to the questions that populist leaders pose. Instead of ignoring populist sentiments, mainstream parties should address, through both their rhetoric and actions, the grievances of the voters that populist leaders use to seize power. This can also be achieved by including populist parties in governing coalitions, which can help prove to voters that established parties are sincere about enacting the general will of the people.

### 7. Final summary 

The key message in these blinks:

**Populism is a political phenomenon that pits the will of the people against the interests of an elite. Different shades of populism attach themselves to various political host ideologies, for example socialist left-wing populism and nationalist right-wing populism. One strategy that populists use for electoral success is the personalist approach, which emphasizes their personal, often non-elite credentials to seem relatable to the masses. And because populism is a phenomenon sweeping the world, it's important we try to fully understand its roots, mechanisms and effects — and ways to mitigate any potential repercussions.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _How Democracies Die_** **by Steven Levitsky, Daniel Ziblatt**

_How Democracies Die_ (2018) examines the fundamental principles of democracy, with a look at historical cases — particularly in Latin America — where democracies have turned into dictatorships or autocracies. The authors examine how these democratic downfalls have happened, whether it could happen again in the future, and what could be done to prevent this dangerous and often lethal outcome. Attention is also given to the presidency of Donald Trump, to question his motives and determine whether he qualifies as an American autocrat.
---

### Cas Mudde and Cristóbal Rovira Kaltwasser

Cas Mudde is a Dutch political scientist and Stanley Wade Shelton UGAF Professor at the University of Georgia. He is also the cofounder of the European Consortium for Political Research's Standing Group on Extremism & Democracy. Other titles he's authored include _Populist Radical Right Parties in Europe_ and _Racist Extremism in Central and Eastern Europe_.

Cristóbal Rovira Kaltwasser is an associate professor at the School of Political Science of the Diego Portales University in Chile. His main areas of research are comparative politics and the relationship between populism and democracy.

