---
id: 55fff05d615cd10009000022
slug: portfolios-of-the-poor-en
published_date: 2015-09-21T00:00:00.000+00:00
author: Daryl Collins, Jonathan Morduch, Stuart Rutherford, Orlanda Ruthven
title: Portfolios of the Poor
subtitle: How the World's Poor Live on $2 a Day
main_color: 647955
text_color: 4E5E42
---

# Portfolios of the Poor

_How the World's Poor Live on $2 a Day_

**Daryl Collins, Jonathan Morduch, Stuart Rutherford, Orlanda Ruthven**

_Portfolios of the Poor_ (2009) details the creative financial strategies that the world's poorest people use to get by. These blinks explain how people with no educational background whatsoever manage their finances.

---
### 1. What’s in it for me? Learn that the lives of the world’s poorest aren’t exactly what you’d think. 

Have you ever given any money to OXFAM? Maybe you've donated to Save the Children or Christian Aid? Chances are, a lot of you will have, at some point in your life, given to an organization focused on helping the poorest people in the world.

When we see images of hungry, impoverished people in the media, we can't help but feel like they need our help, which may prompt us to give to NGOs and charities. However, this might not be the best way to help.

These blinks explain why the developed world's view of global poverty is inaccurate. Extreme poverty does not turn people into charity cases, unable to help themselves. In fact, those who live in poverty are incredibly smart financially, and have many complex networks for raising capital and making investments.

In these blinks, you'll discover

  * how those without writing skills are able to keep track of financial commitments;

  * why living on $2 per day isn't what it seems; and

  * why merely having a bank account can be a way out of poverty.

### 2. Though lacking a stable income, those living in extreme poverty have strong, effective money management skills. 

When we hear about people living in extreme poverty — subsisting on a meager $2 a day — it's easy to assume that they're leading the toughest lives out there. And this is only the first assumption we tend to make about the world's poorest people. 

We also often assume that people with little money spend any and all cash as soon as they get it. But that's not true. People living on an average of less than $2 a day usually set aside a small amount of savings — a nest egg for covering unforeseen expenses and tiding over periods without income. 

For instance, Hamid, who lives in Bangladesh, always keeps a little cash on him for emergencies, stashes enough money at home for food and also puts aside the necessary amount for making improvements to his home. 

But since most of the world's poorest people are illiterate, they keep track of and manage their finances through oral communication with their friends and families. For example, a husband might tell his wife that he's trying to save money for their children's school supplies, and that he's going to take a job at a local store to earn some income and borrow the remainder from their neighbor. Now that she knows his plan, the man's wife can remind him to stick to his commitments.

In fact, the ability to manage money in such ways is absolutely key for extremely poor people. Because their income is usually irregular, paying back loans is a real difficulty. For instance, farmers earn practically all their yearly income during two or three peak months of harvest and virtually nothing for the rest of the year. Naturally, it's difficulty for them to stay on top of monthly payments during periods of low-income, a fact that makes good money management vital to their success.

### 3. Social bonds and cooperation are crucial for the poorest people to make due with what they have. 

When your friends fall on hard times, they can count on you for support. And if you find yourself in trouble, you know they'll return the favor. Well, the same thing is true for the poorest people in the world. Despite the poverty they live in, everyone in these communities joins together to help out when someone is in need. 

In fact, both official and unofficial social contracts bolster the financial strength of the poor. One official agreement that's common in communities where people live on less than $2 a day is collective investment — a joint effort to bring in more income for everyone. For instance, certain banks, like the Grameen Bank, founded by Muhammad Yunus, focus on lending money to poor communities. Access to such credit streams enables those in poverty to raise the capital necessary for small businesses and to collectively grow their wealth. 

But there are also unofficial agreements made between those in financially dire straits. For example, it's commonplace for the destitute to receive informal credit from a shopkeeper or friend who is lucky enough to have a regular income. These unofficial loans help people in poverty maintain decent living standards even when they fall on hard times. 

However, financial reciprocity and collaboration are just two of the ways the poorest people help each other. Since money is a rare commodity in these communities, community members also offer each other practical, non-financial aid. For instance, a woman might borrow a bit of salt or a cup of rice for dinner only to repay it later or offer another gift, such as a piece of cheese. 

Not just that, but tight-knit friendships within poor communities entail all manner of non-material support. This could be anything — from babysitting to providing emotional support to caring for the ill.

### 4. People in the developed world misunderstand poverty in other countries, and this makes it hard for them to help. 

When people in wealthy first-world countries see images of and hear testimonials about extreme poverty in foreign countries, it's common for them to have a compassionate response and want to do something practical to help out. As a result, people and groups across the world are lending a hand in the effort to end global poverty, generally by working with international organizations like NGOs.

Despite their good intentions, however, groups like this can actually perpetuate misunderstandings about the world's poorest people. 

That's because the standards for remediating world poverty are usually set by international organizations, which mistakenly apply them as if all poor people shared the same reality. For instance, as we know from earlier blinks, the poorest people in the world tend to have highly irregular incomes — a fact that makes standards like "a dollar per day," which refers to an average value, unhelpful for understanding and aiding the poor. 

So, while many poor people might make more on average than one dollar per day, their irregular incomes are the root cause of their financial problems. 

In fact, such standards are often applied indiscriminately to all poor people across the world. It's obvious that such a strategy can't produce an accurate analysis because of the huge differences between different regions. For instance, the same amount of income, say $2 a day, means completely different things depending on where a person lives. 

So, while someone living in Bangladesh might earn the same $2 a day as someone in New York City, the things they can afford are dramatically different, since the latter is so much more expensive than the former. 

Not just that, but attempts to convert global living standards and incomes to make them accurate relative to one another tend to result in failure. Fact is, it's just tremendously difficult for international organizations to accurately measure poverty across diverse economies, societies and cultures.

### 5. National organizations can drastically improve the situation of the world’s poor by offering reliable and professional financial services. 

So, international organizations have a difficult time comprehending the realities of poverty in different countries. The good news is that national institutions and groups, like the Association for Social Advancement, or ASA, and BRAC, both based in Bangladesh, can provide both assistance and more effective solutions. 

How?

Well, national organizations take into account the need in poor communities for safety and reliability by offering opportunities for the world's poor to securely invest their limited capital. For instance, in countries like Bangladesh, India and South Africa, banks forge agreements with the government to make options for financial growth more accessible to the poorest citizens. 

One way they do so is ensuring that every person has a personal bank account. This simple step provides an important defense from robbery, which poses major problems in poor countries, while also preventing the money from being lost and allowing the account holder to make official investments. 

But that's not the only way national organizations help the poor; they also do so by monitoring the prices of financial products. For instance, in countries with extremely poor populations, national organizations work to ensure that prices abide by two conditions: that they're low enough for the majority to afford and that they're high enough to guarantee real growth for small businesses. 

A major advance that national organizations have brought about while doing this financial work is professionalism. 

It may seem odd, but professionalism is not a self-evident fact for financial collaborations in poor countries. As we saw in a previous blink, many people in such countries rely on unofficial financial deals that often involve neighbors or local business owners who don't use signed contracts. 

But it's a different story altogether when people make official investments through registered organizations. Financial partners are held accountable through written contracts and are subject to the law — facts that make all of these cooperative enterprises run much smoother than their unofficial counterparts.

### 6. Final summary 

The key message in this book:

**By collaborating, making wise investments and thinking creatively, people can always find a way to survive and make the most of even the tightest economic situation. By learning from the strategies of the world's poorest people, you can understand how to help them.**

Actionable advice:

**Travel to a poor country.**

Though it depends on how willing you are to be exposed to global poverty, seeing first-hand the realities of global poverty may give you a complex and accurate understanding of people's lived experiences. You won't just learn how to better support poor people, but, by understanding their lives, you'll learn how to better appreciate your own life, too. 

**Suggested** **further** **reading:** ** _Poor Economics_** **by Abhijit V. Banerjee and Esther Duflo**

Investigating some of the biggest challenges poor people face, this book provides the reader with an understanding of why there still is so much poverty in the world, and why many of the measures usually implemented do not help. Based on these insights, the authors offer a number of concrete suggestions to demonstrate how global poverty might be overcome.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Daryl Collins, Jonathan Morduch, Stuart Rutherford, Orlanda Ruthven

Daryl Collins is the senior associate at Bankable Frontier Associates in Boston. She was responsible for organizing the latest version of the financial diaries in South Africa and holds both a B.Sc., and an M.Sc in Economics from the London School of Economics.

The other three authors, Jonathan Morduch, Stuart Rutherford and Orlanda Ruthven, are all experts in economics with international experience, primarily in microeconomics.

