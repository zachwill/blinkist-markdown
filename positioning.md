---
id: 5385e7276162380007920000
slug: positioning-en
published_date: 2014-05-27T13:48:06.000+00:00
author: Al Ries and Jack Trout
title: Positioning
subtitle: The Battle for your Mind: How to be seen and heard in the overcrowded marketplace
main_color: EA2F48
text_color: D12A40
---

# Positioning

_The Battle for your Mind: How to be seen and heard in the overcrowded marketplace_

**Al Ries and Jack Trout**

_Positioning_ has become one of the most renowned, best-selling books about marketing strategies in the last few decades. It describes a revolutionary marketing concept developed by the authors in the 1970s after it became clear that classic advertising was no longer effective due to an increase in media and competition. _Positioning_ focuses on how to position your product in the market to become an industry leader.

---
### 1. What’s in it for me? Find out how to create a successful brand for your startup or employer. 

Have you ever wondered how brands like Apple or Coca-Cola got so extraordinarily successful? Or how Burger King thrived in the fast-food market even after McDonalds had already established a fast-food empire? 

In this book, you'll learn about the power of marketing and how to create a successful brand, even if customers are skeptical and resistant to new products and changes in marketing. 

In these blinks, you'll discover

  * how to get into the mind of unwilling and skeptical customers,

  * why market leaders sometimes lose their position to competitors,

  * what positioning is and how to successfully position your product in the market and

  * the different traps in marketing and how to avoid them.

Note: This book was originally published in 1982; the following blinks refer to the latest version published in 2001.

### 2. We’re so overwhelmed with information that our brains filter out most brands we encounter. 

Due to the increased prevalence of media and advertisements, we're constantly overloaded with information, leading us to filter and block out most information we encounter.

The reality is that we're exposed to an over-communication of information. 

Consider that, since the rise of the media, the exposure of US citizens to marketing and advertising has increased exponentially. The United States consumes 57 percent of the world's advertising and the average US family watches around 7.5 hours of television daily, meaning they see countless ads every day. 

In fact, even before we leave the house, we're bombarded with over a thousand words on the backs of cereal boxes telling us about what we're eating. 

It's no surprise that this barrage of information and advertising needs to be filtered somehow. So, as a way to cope with all this, we automatically rank brands and products and place them on a metaphorical ladder in our mind. 

What does that mean? Well, the brand with the most effective marketing strategy takes the top spot on our ladder, making us want to buy only from them. We relegate all other brands to the lower rungs, meaning that we're unlikely to spend money on their products. 

Two brands that have been highly successful in taking our top spots in their field are the mobile phone operating systems Android and iOS. They were so good at marketing that most of us are familiar with _only_ these two operating systems. As a result, Android and iOS together make up almost the entire mobile operating system market. 

So what can we take away from all this?

Well, if you want to be noticed in what has become an overcrowded marketplace, your strategy needs to stand out against all other advertisers. Otherwise customers will simply forget about your product.

> _"As a nation we have fallen in love with the concept of 'communication'. We don't always appreciate the damage being done by our over-communicated society."_

### 3. Be the first fish in the pond! 

If your concept is the first in the customers' minds, they will most likely adopt it. For instance, Coca Cola was promoted as the most successful soft drink in the market and has stayed in that position for decades.

Our tendency is to always remember what we see or hear first in a category, while additional products in the same category are harder to recall.

Apple, for instance, was able to secure a "first" position in the market as the Mac was the first computer aimed at higher income earners. Even today, Apple has managed to keep this position and sell its products at a higher price point than other companies.

Polaroid also reaped the benefits of being the first of its kind. Even when the product itself became outdated, being the first to explore new markets gave it a huge advantage over subsequent brands selling similar products.

Typically, the first brand to enter the market enjoys the competitive advantage of selling twice as much as the second one.

Another advantage of being first is that you can shape the customers' image of the product the way you want it. This means you can introduce a completely new line of advertisement without competing against other products, which in turn solidifies your market position.

The fact that Coca-Cola was the first cola product to be introduced to customers meant that 7-Up and Dr Pepper were always compared to the original: Coca-Cola. That's because follow-up products are seen as "me-too" products and will always have to fight against their predecessors.

So, if you want to stick in peoples' minds, it pays to be the first.

### 4. To successfully position your product, you need a memorable, trendy marketing approach. 

So you created a product you want to sell. Now how do you get people to remember it? The key is knowing your market and finding a marketing angle that sticks.

First of all, to determine the most effective positioning for your product, you need to think about the current market trends.

Marlboro, for example, chose to market itself as a product with a masculine side to it, which resonated well with the spirit of the time. But have you ever heard of the company Lorilland? Probably not. It also tried to brand its cigarettes with a macho theme twenty years after Marlboro. But by then, brands emphasizing masculinity were no longer trendy. As a result, Lorilland's products failed to achieve the same iconic status as Marlboro's because they weren't catering to the current trends in society.

Contrast this with Apple, which introduced itself as an alternative product to the standard PC, with better design and aimed at a wealthier demographic. Apple saw the demand for such a product, which made them massively successful.

Once you've defined an effective marketing approach, stick to it. Ask yourself how you want to develop your position in the long term, i.e., over the next five to ten years, and don't fret about the short-term problems regarding the approach. Abandoning the positioning that brought you success too quickly can lead to plummeting sales.

This can be seen in the car rental company Avis after it became famous as the "No. 2" that "tried harder." Once the company was taken over, they changed their tagline to "Avis will be no. 1." In the years after using that tagline, Avis lost a considerable amount of their market share.

It's thus clear that in order for your product to sell, you have to understand current trends. Once you've secured a good position using an effective marketing line, don't change it!

> _"To be successful today, you must touch base with reality. And the only reality that counts is what is already in the prospect's mind."_

### 5. If you can’t be first, find a niche or use your competition. 

Ideally, you want your product to be the first in the marketplace. However, if someone else has already beat you to it, you'll need to know how to successfully market a _follow-up_ product. This isn't easy, but it's possible.

If your product is a follower of an established brand, it will have to fight to be recognized. Even if your product is better, customers will still not trust secondary products.

Therefore, you must find a niche in the market. For example, you may notice that there is no product on the more expensive end of the market.

One brand that did this well was Michelob, the first brand to fill the market gap of premium-priced beer.

Another approach to establish your brand's position is to use the strengths and weaknesses of established competitors to your advantage.

A competitor's strength can be used to create an "against" position. Take Avis, the rental car company mentioned in the previous blink. Avis was a follower of market leader Hertz, so it utilized this by taking an "against" position with its slogan "Avis is only No. 2 in rental-cars, so why go with us? We try harder."

After this campaign, Avis increased its profits immensely: not because of a pity effect from "trying harder," but because it contrasted itself with Hertz, which gave it a clear, distinct position in customers' minds.

Another positioning tactic is to reposition your competitor: launch a campaign to move them to a different spot in the market so you can take their old spot.

Tylenol was particularly savvy at this. They repositioned market leader Aspirin by stating that Aspirin could lead to nausea or trigger asthma, which, in the consumers' minds, opened a gap in the "safe pain killers" position in the market. Tylenol promptly filled it and went on to become the largest brand in analgesics.

### 6. Don’t ride on the coattails of well-known brands, whether it’s your own or your competitors’. 

When a brand becomes hugely successful, it's tempting to cash in on it by marketing something similar, or "taking a free ride." But although this seems like an easy way to get noticed, it doesn't mean that customers will automatically trust your product.

Long-time market leader Coca-Cola tried this by introducing Mr. Pibb, a follow-up product of a rival company's Dr. Pepper, and failed miserably. Even Coke's position as a leader in soft drink products couldn't make Mr. Pibb successful.

The likely reason behind this failure is that, to the customer, a well-known brand can only stand for one good product. Promoting more than one product under nearly the same name means one of those products will have to fail in order for the other to succeed.

Another thing to watch out for is falling into the "line-extension trap." That is, taking the name of your established, successful product and attaching it to other products.

The soap company Dial made this mistake. Its product Dial Soap was a massive success in the United States, yet when the company tried to leverage this success by marketing its new deodorant as Dial Deodorant, it was a dismal failure.

It's important to understand that what sticks inside the customer's mind is not the product itself, but its name and what it stands for. By trying to extend that meaning onto another product, you go against the image that you've successfully created in the customer's mind. As a result, customers become skeptical of your product.

If you do manage to maintain a great, powerful brand name, you may well be rewarded with immortality: your brand could become a generic term for the product in question. Take Band-Aid as an example: when most people cut their finger, they say "I need a Band-Aid," not "I need an adhesive bandage."

### 7. Your name must be understandable and memorable. 

It goes without saying that your brand name should be impactful and memorable, as this will be the name your customers hang on their metaphorical product ladder. But what exactly makes a good brand name?

An old strategy was to choose a fancy, uncommon word for your brand name, like the young men's magazine _Esquire_ did.

But history has shown that this is a bad idea: it's far better to come up with a name that most people can understand. In the case of _Esquire_, it found itself losing market share to _Playboy_ because everybody knew what a playboy was but far fewer people knew the correct meaning of the word "esquire."

Your product can also have a coined, non-descriptive name like Kodak or Xerox, but only if it's first in the market, in which case the name doesn't matter that much.

A strong first-mover position can also negate other name-related issues. For example, Coca-Cola is commonly referred to as "Coke," but, thanks to its stronghold on the market, it doesn't have to fear the word's negative connotation (i.e., of the drug cocaine).

If you're not the first in the market, you have to at least come up with a brand name that's simple and easy to remember.

Generic names, such as _Newsweek_, are actually effective because they describe the product, and it's short and simple.

One thing to avoid when coming up with a brand name is falling into the "no-name" trap by using acronyms, for example. Customers need to understand your name, which makes acronyms less easy to remember.

Of course, a company like International Business Machines could get away with the acronym IBM — but only _after_ it became very well-known.

Generally, though, customers will only remember a name they can hear and understand it quickly, so that's what you should strive for.

### 8. Final summary 

The key message in this book:

**In** **order** **to** **successfully** **market** **a** **product,** **you** **have** **to** **have** **a** **good** **product** **name,** **avoid** **marketing** **traps** **and** **utilize** **your** **competitors.** **If** **you** **can't** **be** **the** **first** **in** **the** **market,** **you** **must** **use** **your** **own** **positive** **qualities** **and** **specialties** **to** **find** **a** **niche** **for** **your** **product** **rather** **than** **being** **a** **"me-too"** **product.**
---

### Al Ries and Jack Trout

Al Ries and Jack Trout are the co-founders of the marketing company Trout & Partners. Together they coined the term "positioning" as a way to utilize a position in the marketplace to sell a product. Ries first worked in advertising at General Motors and has published multiple books including _The_ _22_ _Immutable_ _Laws_ _of_ _Branding_. Trout and Ries also co-wrote the marketing classic _Marketing_ _Warfare_.

