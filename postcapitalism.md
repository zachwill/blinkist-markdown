---
id: 56e0376cc3a283000700000b
slug: postcapitalism-en
published_date: 2016-03-11T00:00:00.000+00:00
author: Paul Mason
title: PostCapitalism
subtitle: A Guide to Our Future
main_color: FE8F65
text_color: 99563D
---

# PostCapitalism

_A Guide to Our Future_

**Paul Mason**

_Postcapitalism_ (2015) offers a close examination of the failures of current economic systems. The 2008 financial crisis showed us that neoliberal capitalism is falling apart, and these blinks outline the reasons why we're at the start of capitalism's downfall, while giving an idea of what our transition into _postcapitalism_ will be like.

---
### 1. What’s in it for me? Learn why capitalism is broken and what will come next. 

Capitalism as we know it today posits that the best way to achieve affluence is to let people pursue their self-interest through the workings of free markets.

But in these blinks, it will be argued that this kind of capitalism has reached its limit. Just consider modern information technology, which produces new information goods such as MP3s or ebooks; these types of goods create serious tensions within a traditional capitalist economy, because they can be copied and shared at almost no cost.

These blinks explore the ways in which capitalism is broken, and why it quite probably won't last too far into the future. In fact, there is a strong case to suggest that a _postcapitalist_ society is possible — a different type of society that can be realized through collaborative efforts.

In these blinks, you'll also discover

  * how governments sink deep into debts they can't repay;

  * why Wikipedia conflicts with neoliberal market mechanisms; and

  * how Karl Marx overestimated the proletariat.

### 2. Neoliberal capitalism is broken, largely because of fiat money and financialization. 

The modern form of capitalism in place throughout the Western world is called _neoliberal capitalism_, or _neoliberalism_. According to neoliberalism, the best way for a society to grow and be prosperous is for it to allow its individual citizens to pursue their self-interests in a free market. 

However, neoliberalism is failing. According to the Organization for Economic Co-operation and Development, or OECD, economic growth will slow over the next 50 years, while inequality will rise by 40 percent in Western countries. Why? 

One major contributing factor is the increased use of _fiat money_. Fiat money is currency that isn't backed by gold or silver: it derives its value solely from the state that issues its notes. 

Neoliberal economies use fiat money to counter financial crises. For example, the European Central Bank printed €1.6 trillion during the 2015 crisis, which itself originated in 2009 when certain member states in the Eurozone, such as Greece and Spain, weren't able to repay their debts. 

The neoliberal practice of countering financial crises with fiat money can cause follow-up crises years later, because fiat money allows governments to take on more debt, even if they're unlikely to be able to repay it later on. 

_Financialization_, which began in the 1980s, is another major cause of neoliberalism's downfall. Financialization is the process by which stagnant incomes in the workforce are compensated with credit given by banks. 

Credit cards, overdrafts, mortgages, student loans and automobile loans are now a regular part of everyday life in the neoliberal world. Over time, this bank credit means that more and more of a society's money is imaginary, since the money is generated purely by the interest owed upon these loans and credits.

> _"Capitalism is an organism: it has a lifecycle — a beginning, a middle and an end."_

### 3. The two other major reasons for neoliberalism’s downfall are global imbalances and information technology. 

Fiat money and financialization have caused serious problems in the current capitalist system, but there are two other major factors contributing to the downfall of neoliberalism.

The first of these is _global imbalances_, or the differences between a given country's imports and exports of goods, services and investments. The main _deficit countries_, namely the United States and most of Europe, have to import more than they can export. The countries with a significant surplus are Germany, the Arab oil producers, China, Japan and most of Asia. 

These imbalances force deficit countries into increasing debt, which eventually led to the 2008 financial crisis. Greece amassed a huge amount of debt but had no way to earn enough money to pay it off. It then went into the downward spiral of austerity, which caused thousands of people to lose their jobs and homes. 

The 2008 financial crisis was a major wake up call showing that neoliberalism was unsustainable and that something needed to be done about global imbalances.

The information-technology revolution is another factor that is causing neoliberalism to fail. Neoliberalism is founded upon property ownership, but property ownership matters less and less in our information-based world. 

Take _Wikipedia_, for example. It's not possible to determine the value of Wikipedia by assessing its profits, revenue or the value it accumulates. Furthermore, Wikipedia doesn't really _belong_ to anyone; rather, it belongs to _everyone_. 

Information goods like Wikipedia fundamentally conflict with neoliberal market mechanisms — and we're only going to see more of these goods in the future.

In summary, neoliberalism has failed due to four main reasons: fiat money, financialization, global imbalances in trade and the information-technology revolution.

### 4. Capitalism has historically moved in cycles – but its current changes are unprecedented. 

Have you ever heard of Nikolai Kondratieff? He was a major figure in twentieth-century economics.

Kondratieff spent eight years as a political prisoner in Moscow and was executed in 1938, all because he proposed that, instead of collapsing in crises, capitalism adapts and changes according to the present circumstances.

Kondratieff argued that economic cycles move in historical waves. According to his theory, each cycle has an upswing of about 25 years, followed by a downswing of 25 years. The upswing is driven by the development of new technology and high investment, and the downside typically ends with economic depression, high unemployment, bankruptcy and the unavailability of credit.

That's why Kondratieff didn't believe in Karl Marx's theory that capitalism had reached its "final" end crisis. He believed that capitalism was just moving through another one of its cycles.

Based on Kondratieff's theory, the Austrian economist Joseph Schumpeter developed his own theory of the "Kondratiev cycles." Later economists developed the theory further. Many agree that there have been four such cycles in the history of capitalism: 1790 to 1848, 1840 to the mid-1890s, the mid-1890s to 1945, and 1945 to 2008.

The beginning of the fourth cycle was shaped largely by the invention of the transistor. Years later, in 1973, the Arab oil embargo pushed Western economies into a recession. So, the period from 1945 to 1973 didn't see any recessions, but we've had six recessions since 1973, culminating in the 2008 financial crisis.

There seems to be a disruption in this pattern, however. In the 1990s, at the end of the fourth wave, the elements of a fifth wave also started to appear, thanks to the introduction of new network technology, mobile communications and a more connected global marketplace.

This wave seems to have been stalled by the failure of neoliberalism. It may be that capitalism's adaptability has finally reached its limit.

### 5. Information technology is leading us toward a postcapitalist economy. 

In the 1990s, the rise of personal computers and information sharing started to change dramatically. People developed buzzwords like _knowledge economy_, _cognitive capitalism_ and _info-capitalism_, concepts that had no place in the capitalist world.

The idea of "information technology" completely goes against the way supply and demand function in a capitalist economy. Our economy is becoming increasingly based on _information_ products like software, ebooks, online news articles and songs on iTunes. 

An economy based on information works very differently from than one based on physical goods, however. Information goods can be copied and shared at little or no cost, which has major implications for the market.

Supply and demand rests on the idea of _scarcity_ : a physical product's value depends on how limited, or _scarce_ it is. Before information goods, all goods were finite and scarce, so they all functioned according to the same economic principles.

Information goods, on the other hand, are potentially unlimited, making supply and demand irrelevant. iTunes sells songs, but there's no limit to the amount of songs they can sell and those same songs can be easily copied illegally too.

Information technology also allows for new, non-capitalist modes of production. Since the 1990s, we've started to develop a network economy in which money no longer determines the value of a product. Wikipedia has over 24 million registered users who contribute to it, it's free, it doesn't make a profit and it's not anybody's property. 

There is no way an economic system with free goods and no property rights can be capitalist. Information technology undermines the fundamental principles of how capitalism operates, and is leading us toward a postcapitalist society.

> If Wikipedia were run as a commercial site, its revenue could be $2.8 billion a year.

### 6. Karl Marx’s labor theory is consistent with information goods, but Marx had a flawed view of the proletariat. 

According to capitalist theory, if everyone can listen to music online for free, it shouldn't have any value. However, Karl Marx developed another theory that can account for the value of online music.

The value of information goods like online music can be measured using Marx's labor theory. Marx's labor theory states that a commodity's value is determined by the number of _socially necessary_ hours to produce it. "Socially necessary" hours aren't merely the number of hours a person works, but the average amount of labor time needed to produce the good. 

In Marx's labor theory, the value of the machines, energy and raw materials is transferred to the final product, or _finished labor._ Information goods, in other words, can be thought of as a form of finished labor: their value depends on how much effort went into producing them.

Marx was right about the value of labor but he was mistaken about the working class. He thought that only the proletariat could push society out of capitalism by rebelling against it. But capitalism has been around for 200 years, and the proletariat have never been a long-term, serious threat to it. 

Even so, capitalism has acquired new enemies in recent years: people who show their dissatisfaction with the current system by camping out in city squares, occupying financial districts or blockading fracking sites. 

It's very clear that more and more people around the world are growing seriously dissatisfied with the current economic system and the problems it has caused, particularly global warming, which threatens humans' very existence. So how do we transition out of capitalism and into something better? Find out in the next blink.

> " … [The] agent of change has become, potentially, everyone on earth."

### 7. The transition from capitalism to postcapitalism will be a lengthy process shaped by new technologies and growing global problems. 

Before capitalism, Western society had feudalism. How did one transform into the other?

The transition from feudalism to capitalism was lengthy and complex. In feudalism, which flourished from the ninth to the fifteenth century, lords granted their land to _vassals_ for a fee. 

Like capitalism, there were four main reasons for the downfall of feudalism. 

The first was a series of terrible famines that started in the 1300s, when agricultural productivity couldn't keep up with the rapid population growth. 

Second, new economic powers arose with the growth of banking in the fifteenth century. 

Third was the conquest of the Americas, which began around 1500 and brought further trade and prosperity.

And finally came the development of the printing press in 1450, which was a main catalyst for the scientific revolution.

Despite all of these contributing factors, the transition from feudalism to capitalism still took several hundred years. Although it has been spurred by a very different series of developments, the transition from capitalism to postcapitalism will also be a lengthy process shaped by technology and global shifts.

As we've seen, capitalism is being undermined by information technology, while other major global changes are also heralding its demise: energy depletion, climate change, aging populations and migration. 

Climate change, for instance, will cause serious weather problems like hurricanes, floods and droughts if we continue to burn fossil fuels at the current rate. Capitalism can't react to climate change because of the supply of and demand for oil.

Developed countries are also going to have problems with their workforce in the future, as there will be many more elderly people than young people. In addition, elderly people will probably have to live on much less than they do now. 

In conclusion, the transition from capitalism to postcapitalism will be rough at times, but it will happen.

> _"But if we are bold enough to imagine we can rescue the planet, we should also imagine rescuing ourselves from an economic system that doesn't work."_

### 8. The state could take deliberate steps to move toward a postcapitalist economy. 

You can think of the advancement toward a postcapitalist society as a collaborative project, like Wikipedia. The goals of the postcapitalist project would be the eradication of carbon emissions, a market of products with no cost of production and close to zero labor time. What role would the state play in moving toward this?

First, the state could reform the market to make it favor sustainable, collaborative and socially just projects. It could provide tax incentives for solar panels or local energy systems, for example. 

The tax system could also be adapted to favor the creation of non-profit organizations and collaborative production. Company regulations could be changed to make it easier to create living-wage jobs instead of low-wage ones.

Second, the state could stop privatization, dismantle monopolies and alleviate debt. If a state was _very_ serious about transitioning into postcapitalism, it could immediately stop privatization, which outsources public services like health, education and transportation. 

A state could also deliberately break up monopolies, which allow companies to charge unreasonably high prices for their goods. Anytime a monopoly needed to be broken, they could simply take it apart and transfer its services to public ownership. 

States have to address the issue of government debt, too. This problem will only get worse over time and governments must find ways to pay these debts off.

Finally, states could give all of their citizens a basic income. The idea would be to give everyone of working age an unconditional basic income that's funded by taxation. It would replace unemployment benefits and give people time to volunteer, develop new ideas or even edit articles on Wikipedia. There's no telling what innovations would arise. 

Projects like Wikipedia show the power of collaboration. Collaboration is likely the key to the new economic era.

> _"What happens to the 1 per cent? They become poorer and therefore happier. Because it's tough being rich. … The 99 per cent are coming to the rescue. Postcapitalism will set you free."_

### 9. Final Summary 

The key message in this book:

**Capitalism has ruled the world for the past two hundred years, but its end is now in sight. Thanks to the Internet and new technologies, our economy is increasingly based on information goods that aren't governed by the laws of supply and demand. That, combined with major shifts like global warming and migration, will force states to start moving toward postcapitalism.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Future of the Professions_** **by Richard Susskind and Daniel Susskind**

_The Future of the Professions_ (2015) examines how modern technology and the internet have revolutionized our society. These blinks in particular address how technology has changed the way society views the work of experts, the so-called professionals. The role of such experts is evolving quickly; here you'll discover just what the future of professions will look like.
---

### Paul Mason

Paul Mason is the economics editor of _Channel 4 News_. His other books include _Meltdown: The End of the Age of Greed_ and _Why It's Kicking Off Everywhere: The New Global Revolutions_. He also writes for news outlets including _The Guardian_ and _The_ _New Statesman_.

