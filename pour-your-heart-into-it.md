---
id: 54eafb5e393366000acf0000
slug: pour-your-heart-into-it-en
published_date: 2015-02-27T00:00:00.000+00:00
author: Howard Schultz and Dori Jones Yang
title: Pour Your Heart Into It
subtitle: How Starbucks Built a Company One Cup at a Time
main_color: B0593B
text_color: 964C32
---

# Pour Your Heart Into It

_How Starbucks Built a Company One Cup at a Time_

**Howard Schultz and Dori Jones Yang**

_Pour Your Heart Into It_ takes you on the enthralling journey of how Starbucks came to be a success story. It sheds light on its beginnings, reveals how the quick growth rate was possible and sets out to explain how businesses can learn from its strategies and principles.

---
### 1. What’s in it for me? Get business advice from a coffee master. 

What makes a great cup of coffee? According to Starbucks CEO Howard Schultz, his coffee deli became so hugely popular because it has always had extremely high standards for their products and work environment.

The story about Starbucks shows that the success of a business depends on the authenticity of its product, and that trust, brand renewal and strong principles are essential to keep a business growing.

In these blinks you'll learn

  * how low fat milk threatened Starbucks' very high standards;

  * why spending more than you have might actually not be such a bad idea; and

  * how an experiment in 1988 made coffee-flavoured ice cream possible.

### 2. Success in business comes from selling an authentic product. 

If you bring up Starbucks in conversation, chances are pretty much everyone will know what you're talking about. You _know_ Starbucks as a successful business, but have you ever wondered how it came to be?

The secret to Starbucks' success is, for founder Howard Schultz, no secret at all. Plain and simple, the success of Starbucks lies with the brand's authenticity, achieved by selling _only_ the highest quality coffee.

Back in 1981, Starbucks was just a small retail store. It sold exclusively dark-roasted Italian style coffee, which may be ubiquitous today, but was still a novelty back then. By roasting the coffee dark, the coffee grows in strength, gains a more powerful aroma and has the distinct taste of authentic Italian coffee.

Starbucks never compromised its beans, and became known for its premium-quality, dark-roasted flavor profile that its founders were so passionate about. Because of those early business decisions, Starbucks had an undeniable feeling of authenticity about it since the start.

The company has held true to its authentic coffee profile, in tough times to the present day. In 1994, coffee prices shot to an all-time high on the world market, from $0.80 to $2.74 in a matter of days, after a frost that destroyed much of Brazil's coffee plants.

During this coffee crisis, a lot of shareholders promoted the purchase of cheaper beans, to keep the price of a cup of joe stable. But Starbucks was unwilling to compromise quality and continued to sell the highest quality coffee they could, instead reducing other costs to account for the higher price of beans. When the world coffee market recovered, Starbucks had an even stronger customer base because its quality remained consistently high.

> _"We don't manage the business to maximize anything except the quality of the coffee."_

### 3. In business you have to be stubborn to succeed. 

From what we've learned in the first blink, Starbucks adheres to uncompromising business practices. So what kind of person does that make its founder? You could probably describe Howard Schultz as _stubborn._ In fact, that quality is what led Howard to become a part of Starbucks in the first place.

After tasting the great coffee at its store in 1981, Schultz knew he wanted to become head of marketing in the Starbucks retail stores. The original founder, Jerry Baldwin, was interested. But his partners thought Schultz had too many new ideas and was too eager to create change. So Baldwin declined to hire Schultz. 

At first, Schultz was devastated. But as he was so determined to join Starbucks, he called back the following day to tell Baldwin he was making a huge mistake. And, after sleeping on it, Baldwin, won over by Schultz' persistence, offered him the job.

Starbucks' initial reluctance to hire him wasn't the only obstacle. Later on he took leave from the company due to disagreements about brand development, and opened his own coffee shop, Il Giornale. Raising money for that venture over the course of a year, he pitched to 242 accredited investors. A whopping 217 said no.

In the face of rejection, he grew more stubborn. He continued to elicit investors until he was finally able to open Il Giornale. And, his coffee shop eventually became so successful that he bought Starbucks and developed it into a billion dollar company!

We can take away two lessons: (1) Business will always present obstacles; (2) A stubborn attitude is key to overcoming those stumbling blocks. Still, there's much more to success! Read on.

> _"In the fifteen years since then, I've often wondered: What would have happened had I just accepted his decision?" - Schultz_

### 4. It’s vital to build trust with your employees. 

We've seen that it pays to be stubborn when dealing with people at the top of the food chain. But with everyone else vital to your business, your focus should be entirely different: build a relationship of trust with your employees.

The Starbucks management team has worked hard to nurture that trust. It's reflected in the company motto: "Treat people like family and they will be loyal and give their all." The Starbucks family receives a great benefit package and even part-time employees are offered stock options — _Bean Stock_ — so they are partners, not just employees. Employees are also encouraged to voice their opinions at the quarterly _Open Forums_, a source of valuable feedback.

As a result, there is a great bond of trust between management and employees. Starbucks workers are in fact so pleased by their company that they have refused to be represented by unions since 1992, making the following statement: "You trusted us, and now we trust you."

It makes sense that if your employees trust you, they're likely to stay with you longer. But did you know this can also improve your _customer_ bond?

For example, Starbucks has a lower turnover rate than other comparable retail stores — up to 65%, compared with up to 400% per year. Low turnover means the business is saving time and money on training new hires.

But, it also helps the business in a more personal way. Many regular customers can expect that their baristas will recognize them and greet them with, "Same as usual?" That familiarity gives the customers a strong connection to the Starbucks brand, a connection that keeps them coming back. We can see that for Starbucks, trust is an invaluable commodity.

### 5. Not only does a good business need strong values; it needs to stick to them no matter what. 

We've all got those friends that seem to be changing their values on a whim, chasing the latest fads and trends. If they're an acquaintance, we'll tolerate that kind of behavior, but how would you like it if your business was changing its core principles from week to week? Neither employees nor customers would put up with it for very long, which is why it's crucial that a business stands by its values.

As we know, the core value of Starbucks is authenticity. So, taking this principle seriously has sometimes meant denying customer requests.

For example, when flavoured coffee beans became available, many customers asked for the ones they could find in other stores. However, Starbucks refuses to produce them to this day, because they don't want to pollute their high quality coffee beans with flavoring chemicals.

This goes to the point where a Starbucks' employee would grind another company's beans for you, but they would not grind flavoured coffee beans in their grinders, as some traces of chemicals could be left behind and pollute the other coffee.

In this way, values are so important at Starbucks that it's sometimes necessary to have lengthy discussions on matters that seem, at first, totally trivial. When Starbucks began to receive customer requests for low fat milk, fights broke out. Again, authenticity drives the Starbucks visions, and it was widely considered that a Caffe Latte could only be authentic using whole fat milk.

But, on the other hand, another core value at Starbucks and in any sensible business is to fulfill the customers' wishes. So Starbucks had a dilemma on their hands. Not willing to compromise either value, the company conducted extensive testings on low fat products in order to make sure that the authentic taste would not be sacrificed.

The low fat Caffe Latte was introduced only once they were sure it tasted just as good as their original whole fat Caffe Latte. To those who aren't particularly enthusiastic about coffee, this may seem like a lot of effort for such trifling issues.

But you've got to admire the way Starbucks fiercely upheld its values as the company grew and changed. And, as we'll find out in the next blink, there certainly were a lot of changes in store.

### 6. It pays to invest above the curve. 

We all know it's wise to be cautious with our money, be sparing and only spend what we can afford. But in business, it's a different game where it's often necessary to take big risks. The truth is, if you want to be successful, you've got to take the plunge and invest.

Starbucks' impressive and rapid growth wouldn't have been possible if it hadn't started "investing above the curve," which means investing in themselves even _before_ they needed it.

Today Starbucks has 21,000 stores. But, even when they only had 20, Schultz already knew he wanted to make it big. He soon realized that his investments were most needed in one area: infrastructure.

With plans to grow to 300 stores in the near future _and_ to continue roasting its own beans, Starbucks needed roasting facilities that were up to the task. Their current roasting facilities simply wouldn't stand the test. So, it needed to build a new one.

The company also needed to attract a high-performing management team, one that had already worked with huge enterprises, with an ability to handle fast growth. In addition, it needed a sophisticated computer program, custom-built to deal with the company's thousands of sales in hundreds of places.

Altogether, these changes required a large and risky investment. Many investors were concerned by losses between 1987 and 1989, and pressured the business to change strategies. But, it became apparent that the losses were a result of investment that had not yet paid off, and that the storefronts were in fact operating at profit. So once again, Schultz stuck to his guns and the investments paid off over time. By 1990, Starbucks was making a profit _and_ had a firm foundation for further growth.

Starbucks wasn't alone in its early losses. Most companies struggle in their initial phases. But if the reason for your losses is because you are investing above the growth curve, then you can be sure that your company will profit and expand in years to come. That strategy applies to both business plans and people, as the following blink discusses.

> _"In a growth-company, you can't play catch-up."_

### 7. Invest in people who are smarter than you and then let them do their job. 

Let's face it: we can't do everything ourselves. Still, superiors often feel threatened by their brightest employees, rather than tapping into their skills as an asset. Schultz, on the other hand, was smart enough to give the reigns to those with expertise in their specific areas.

For example, when confronted with the challenge of creating a sales computer program for hundreds of stores, Schultz hired a programmer who had worked on the McDonald's sales program. Having worked in a company that was far bigger than Starbucks, she was well-suited.

Instead of micromanaging her every step of the way, Schultz trusted her, offered her a clean slate and let her develop the Starbucks sales program. That program is still used worldwide today.

This shows just how valuable it is to trust that your specialist employees know what they're talking about; after all, it is _their_ specialty. In 1989, a new manager, Howard Behar, joined Starbucks. He began to rapidly change the corporate culture and openly disagreed about many tactics — not so dissimilar to Schultz in his early days at Starbucks!

At first, working with Behar was uncomfortable for some Starbucks managers. But the clever ones learned to adapt and recognize his criticism for its constructive value. With his help, Starbucks made the shift from a product-oriented company to a company that was centred on people.

Behar was responsible for the Open Forums that give Starbucks employees the opportunity to provide their feedback. If management had rejected Behar because they felt threatened by him, they wouldn't have been able to benefit from his great new ideas, ideas that make Starbucks the successful company it is today.

Openness to new ideas isn't just vital within a company; sometimes new ideas come from other sources, leading to revolutionary collaborations. In the next blink, learn how Starbucks optimized collaboration!

### 8. Working with others can revitalize your brand, or even revolutionize your product. 

If you want to get ahead in business, the old "if it ain't broke, don't fix it" mantra won't cut it. _Brand renewal_, on the other hand, will boost your success; the key is to change things _before_ they stop working.

Starbucks managed to apply brand renewal to a product as old and traditional as coffee. And, the company applied renewal at a scientific level. Don Valencia, a biomedical scientist, began experimenting with coffee in 1988. His research led him to develop a coffee extract, which he then refined until it was completely indistinguishable from real, freshly-brewed coffee.

He took his idea to his local Starbucks shop, where the managers were impressed, as were those higher up in the Starbuck hierarchy. Starbucks onboarded the coffee extract, and Valencia's invention made it possible to create products such as coffee-flavored ice cream and ready-to-drink bottled beverages. These products could be sold in supermarkets, making Starbucks popular to a wider market.

Another way to create brand renewal in your company is by working with another company on a _joint venture._ In 1994, Starbucks began working with Pepsi to distribute cold coffee drinks, making it possible to sell Starbucks in places where there weren't any Starbucks stores.

But it wasn't an easy match. At first, the companies couldn't have been more different: Starbucks was a small business in which people worked on several different projects at once, whereas Pepsi employees were used to focusing on one project at a time.

But instead of challenging each other, both companies chose to view these differences as complementary, and with a positive outlook they began to look for win-win solutions. This way, they were able to put the bottled Frappucino on the market, which was so popular that stores were already selling out soon after its release.

Having the courage to work with others to renew your product can pay off greatly, and can even propel you to an international market. But as a company expands, it must be true to its founding spirit — the principles behind its success. Starbucks strives to keep its original culture alive, as we'll see in the next blink.

### 9. It’s important for a company to stick to its principles and ensure its employees are happy. 

It's a classic story for big chains to sell their soul in order to stay ahead in the game, but the same isn't true for Starbucks. Instead, the company takes the initiative to stay intimate with its employees — no easy feat when there are about 25,000!

By offering _Bean Stock_ (stock options) Starbucks generates a sense of partnership among its employees. It tends to offer higher hourly wages than the industry standard as well other benefits, such as health insurance for employees and their partners. Schultz communicates personally with each store via email and voicemail, and each region holds quarterly meetings for store managers so that management remains in touch with the base.

A survey in 1996 revealed that corporate culture was exceptional: 88% of the employees were satisfied with working at Starbucks and 89% were even proud to work at Starbucks!

Not only does Starbucks strive to take care of its employees; it also strives to stand by environmental principles. For example, its disposable paper cups are a predominant environmental issue. Single use disposable paper cups generate an unseemly amount of waste, not to mention that sometimes hot beverages necessitate two cups.

While disposable cups continue to be a necessary part of the business, Starbucks developed cup sleeves, so that hot beverages no longer need a double cup. The sleeves are made of half the material as a cup, and protect people's hands from the heat, just the same. The stores also sell reusable cups to encourage people to reduce waste. And they conduct Green Sweeps, in which employees pick up trash around their neighborhoods.

Every big company risks losing the small business culture from which it began, but, as Starbucks has demonstrated, it's possible to continue to strengthen the brand, authenticity, and employee- and customer-loyalty.

### 10. Final summary 

Key message in this book:

**If you want success to mirror that of Starbucks, give your customers something authentic, whose values they can trust! If you want your business to grow like Starbucks, find ways to renew your brand and create a community that cares for its employees.**

Actionable advice:

**Be stubborn!**

The next time you are having trouble with a task like raising money for your project, keep in mind that Schultz was turned down 217 of the 242 times he initially elicited investments for his coffee shop business. If you really want something, persistence is the key to achieving it.

**Suggested** **further** **reading:** ** _Delivering Happiness_** **by** ** _Tony Hsieh_**

The central theme of the book is the business of literally delivering happiness while living a life of passion and purpose. _Delivering Happiness_ tells the story of Tony Hsieh and his company Zappos, demonstrating how thinking long-term and following your passions can not only lead to profits but also a happy life for your employees, your customers, and yourself. The book describes an alternative approach to corporate culture that focuses on the simple concept of making people around you happy, and by doing so increasing your own happiness.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Howard Schultz and Dori Jones Yang

Howard Schultz is an American businessman and Starbucks Chairman and CEO. From a modest background and the first person in his family to go to college, he grew the small coffeehouse into a billion dollar company. In 2007 he won the FIRST award for Responsible Capitalism.

Dori Jones Yang is an American author. She studied in Princeton and John Hopkins University.

