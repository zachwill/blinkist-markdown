---
id: 5485c5396331620009220000
slug: power-en
published_date: 2014-12-08T00:00:00.000+00:00
author: Jeffrey Pfeffer
title: Power
subtitle: Why Some People Have It And Others Don't
main_color: CB6C29
text_color: 99511F
---

# Power

_Why Some People Have It And Others Don't_

**Jeffrey Pfeffer**

_Power_ (2010) is a _realpolitik_ guide to leading a successful career. It offers unusual insights and advice you wouldn't normally find in other career literature, with tips and techniques you can start using now to achieve long-term success.

---
### 1. What’s in it for me? Learn how to move yourself into positions of power. 

Many of us spend life thinking that we're somehow unworthy of power or at least thinking that we shouldn't go out of our way to get to high positions. Instead, we keep our head down, work hard and hope that we'll eventually move up the greasy pole.

These blinks show how this way of thinking is completely wrong. They show that if you want to get power you have to work for it. They provide tips on how you can stand out among the competition and make your way to the top.

In these blinks, you'll discover

  * why if you are a nail, standing up is best;

  * how one Democrat tamed the Republicans; and

  * the true value of a smile.

### 2. Many of us mistakenly believe that power and success is earned by people who follow the rules. 

Every day you work hard at your job: you come in early, find a way to accomplish all your tasks and responsibilities and you stay late into the night, burning the midnight oil.

With all that you do, you're convinced that a promotion is right around the corner, just as soon as the boss notices all your hard work.

But actually, you could be waiting a long time, because a broad range of studies have shown that there's not much of a link between job performance and promotions. For example, a study on Fokker — a Dutch aircraft manufacturer — found that white-collar workers were only 12 percent more likely to be promoted when they received a performance rating of "very good," as opposed to merely "good."

And this leads us to a common misunderstanding: many people mistakenly assume that positions of power and prestige are earned by people who deserve them. In other words, most of us think that the world is a fair place, a mindset that was first described by psychologist Melvin Lerner as the _just-world hypothesi_ s. We think that successful people are the ones who follow the rules.

This viewpoint prevents us from adopting techniques used by people who have achieved their power through nefarious means. When we see someone who got to the top by using a slightly underhanded or ruthless approach — maybe by taking a little too much credit for a team project or by being rude with coworkers — we tell ourselves, "He'll get what's coming to him!" Since we take it for granted that their bad behavior will somehow be punished, we don't bother to learn from their success.

But that mindset is way too limiting, because we're missing out on a wide range of useful tips and techniques just because we don't like the people who use them.

### 3. To develop your leadership qualities, start with an honest assessment of your strengths and weaknesses. 

When you see the President, you probably think to yourself: "Now that's a natural leader! It looks like she was _born_ to do this job." This reflects a common mindset: we often believe that some people are naturally suited to power, as if they have some genetic gift for it.

But, in fact, anyone has the potential to become a great leader: you can learn leadership!

So what does an education in leadership consist of? Well, it starts by understanding what kinds of qualities are inherent to good leadership.

And the most visible quality associated with leadership is confidence. Think about it this way: You cannot hope to become a great leader if you don't think you can be one. Rather, you need confidence to move toward your goal. Whatever you're trying to achieve — whether it's climbing Mount Everest or getting promoted at work — you cannot possibly hope to succeed if you don't think you're capable of it.

But confidence isn't the only thing that characterizes leadership; so does energy. Since the path to power is marked by early mornings and late nights of hard work, you need lots of energy to succeed.

There's another important quality inherent in strong leadership, too: empathy, the ability to understand what other people want. Throughout your entire career, you'll have to stand out to get noticed by the decision-makers at the top. Thus, it helps to know what they're looking for: do the decision-makers want a sensible and reliable person, or would they rather find someone with great communication skills and the capacity to lead?

Confidence, energy and empathy are only a few of the qualities associated with leadership — there are many others, such as resiliency and self-awareness. And, ultimately, reflecting on these qualities and honestly considering which ones you embody (and which you don't) is the first step to increasing your personal power.

Because in order to be successful, you need to have a clear grasp of both your strengths and your weaknesses. Once you know your weaknesses, you can train yourself to overcome them. And in the upcoming blinks, you'll find out how to do just that.

> _"You don't change the world by first taking a nap."_

### 4. If you’re in pursuit of power, it’s important to land in the right department. 

When it comes to building a career, most of us intuitively understand that a multinational bank probably offers better future prospects than a local grocery store.

And yet, the differences that exist between companies also exist within companies themselves, in different departments. And if you want to be on the path to power, it's important to land in the right department.

Want proof? One study analyzed the career paths of 338 managers in a public utility that employs 3,500 people. The researchers found that managers who began their career in the more high-powered departments enjoyed higher salary growth and were also more likely to land in powerful departments at other companies when they changed jobs.

So how can you figure out which department is most powerful? There's no universal answer that works across companies, so you have to evaluate on a case-by-case basis. There are three factors to consider.

The first is pretty simple: it's a matter of relative pay. More powerful departments tend to offer higher salaries.

Physical proximity to top leadership is another important indicator of power. For example, the location of departments in the Pacific Gas and Electric Company's headquarters changed to reflect a shift in power. Over time, the law and finance units moved up in the building, so that they were closer to senior management, while the engineering department moved down. And during this period, the representation of law and finance employees who moved into senior management positions rose accordingly.

The third key way to gauge departmental power is to look at the composition of important committees, like the board of directors. More powerful departments will have higher representation in these committees.

### 5. Stand out by asking questions and occasionally breaking the rules. 

There's an old Japanese proverb, "the nail that sticks out gets hammered down." The author turns this proverb on its head. Put yourself in the shoes of a manager trying to decide who should get a promotion: If you're looking at a wooden plank with dozens of nails hammered into it, and there's one nail sticking out, which nail would you choose?

Simply put: When it comes to career advancement, being more visible gets you noticed. But how can you achieve this?

Start by asking powerful people for help. For example, you could invite your boss to a lunch meeting and then ask her what steps you should take in order to get a promotion. The boss will likely remember that you were confident and daring enough to ask her that question.

Many people are afraid of making these kinds of moves because they feel that people will ignore or reject them.

This principle was demonstrated in a study conducted by business school director Frank Flynn and doctoral student Vanessa Lake. Participants had two tasks: (1) To ask other people for a small favor (to fill out a questionnaire), and (2) To guess in advance how many requests they would have to make before someone agreed to do the favor. Unbelievably, participants overrated the number of people they thought they would have to ask by a factor of three.

As the study suggests, you shouldn't be afraid of asking for what you want and being visible. But what should you do when that's not enough?

You also have to be memorable; you have to stand out from the crowd for doing things differently.

Just think of world-famous sports stars like Cristiano Ronaldo. Ronaldo is a great soccer player, but there are many great soccer players. Ronaldo doesn't break the rules of the game more often than others but he stands out because he breaks social rules by being overly narcissistic.

> _"You can't select what you can't remember, and that includes... candidates for leadership positions."_

### 6. If you want other people’s help, you have to find a way to help them first. 

Most of us can't achieve power on our own; we need other people's help to get to the top. But how can we get help?

First, you need to provide something of value in exchange. In other words, you need to have _resources_ : social support, money, career advice — any kind of asset people want or need.

Sharing the resources you have will compel others to help you, because the exchange involves an element of _reciprocity_ ; after all, there's a social obligation to return favors.

For example, imagine you're telling a colleague that you're planning to move next weekend. And let's say the colleague offers her help: if you choose to accept her assistance, you'll probably have the feeling that you owe her something in return. And even if you don't accept her offer at all, you still might feel obligated to offer her help next time she seems to need it.

And, in fact, your ability to help others is a fantastic resource. In the workplace, there are plenty of boring menial tasks people seek to avoid. Helping a colleague with something like this won't cost you very much, and will earn you a lot of gratitude.

Let's say your boss decides that your division should spend the day outside the office doing team-building activities. If you volunteer to help organize the day, your boss will be extremely grateful.

Here's another resource you can leverage: treat people fairly and politely.

Don't believe us? Consider Democrat Willie Brown, who was essentially ruled the California Assembly for 16 years. Although he promoted legislation that supported gay equality and the legalization of small amounts of marijuana, he received support from many ideologically-opposed Republican legislators.

The reason for this unusual support? Brown had previously chaired a committee in which he treated Republicans with respect and fairness. He won respect, and thus his colleagues were willing to support him on certain matters, even when they personally disagreed.

> _"Having resources is an important source of power only if you use those resources strategically to help others whose support you need."_

### 7. Carry yourself in a way that exudes power. 

When you turn on any political talk show, you'll see politicians quarreling with each other about whatever issue happens to be most controversial at the moment. Try to think of a moment when politicians from different sides of the aisle were able to agree about something.

Can't think of anything? Well, there is one thing: politicians agree that the way they speak and behave can communicate authority and influence people.

This is an important principle all powerful people have internalized: the way you carry yourself influences the way people interact with you.

And how you present yourself goes deep: your emotions don't simply influence the people around you; _they're contagious_. When you walk through a corridor smiling, for example, you'll be greeted with smiles in return.

A study on this very topic and its use in marketing found that people actually become happier themselves when they encounter others smiling. So, in this case, if they see an ad for a product containing someone smiling, the happiness will rub off on the customer — and she will have more positive feelings about the product.

In short, if you want to become powerful, it helps to exude power. There are several ways you can do this convincingly.

First of all, you should always display dominant behavior when interacting with others — for example, by displaying anger when you disagree with something. This was shown to be a successful strategy by psychologist Larissa Tiedens, who found that people who easily express anger are perceived to be strong, competent and smart.

Another way to exude power is to take your time when speaking. This will prevent you from wasting words or contradicting yourself — which will make you seem competent. For example, the most successful politicians speak slowly and deliberately. They think about what they are going to say and make sure it's clear and consistent.

### 8. Having a good reputation will carry you a long way on the path to power. 

At the 2001 California Medical Association, Albin Avgher, PhD, presented a theory of human communication. Throughout his talk, Avgher expounded on some ideas and theories that went against the accepted practices of other physicians and attorneys in the audience.

Nevertheless, most people stayed in their seats and listened attentively: They were truly interested in what this expert had to say. That is, they were interested until Avgher admitted that he wasn't actually Albin Avgher, PhD, but rather a comedian named Charlie Varon — and that everything he had just said was completely made up.

This anecdote highlights how much reputation matters if you want to get ahead. Because once people form a judgment about you, they'll consistently find ways to back it up. In other words, they'll concentrate on the things about you which are consistent with their judgement and ignore everything that isn't. This process is called _cognitive discounting_.

So since Varon had been introduced as a PhD, the audience members immediately assumed that he was an expert with a valuable opinion. This judgment led them to trust everything he said, even when it went against their prior knowledge.

But it doesn't end there: people modify their own behavior when they interact with you based on their assumptions. So if Varon had never gone on to disclose his real profession, the audience would have questioned him — though not by challenging his theories, but rather by asking less critically for supporting evidence about what they could learn from his research.

As we've seen, reputation is crucial. How can you ensure that you have a good one?

Well, nothing beats a first impression. If you fail at first, you might get stuck in an environment where people have a bad opinion of you. It might be easier to change companies than try to change anyone's already established beliefs.

> _"Many behaviors are ambiguous... how people interpret what they see depends on their expectations."_

### 9. On the path to power, you’ll inevitably encounter some amount of conflict and failure. 

Although most people are _conflict-averse_ — that is, we try and avoid confrontation — the best leaders use a different tactic. These people realize that they're bound to meet others with different goals and different values. And since they want to succeed, leaders figure out how to face these people head on and win them over.

But of course, this approach isn't about going out and picking a fight with just anyone. There are a few important ground rules to keep in mind.

First of all, in order to become powerful, choose your fights wisely and don't engage in unnecessary conflicts. There's no reason to get worked up when someone parks in your favorite spot or forgets to put the milk back in the fridge. Instead, feel free to fiercely defend yourself when someone stands in the way of your main goals — by harshly criticizing your work, for example.

And then when you do get involved in a conflict, always offer your opponents a graceful way to retreat. After all, you don't want to make permanent enemies — people who feel compelled to attack you again and again — so act respectfully, even when you disagree with someone.

This approach was the key to California Democrat Willie Brown's political power. After winning a tough campaign fight, he helped his former rivals secure other kinds of desirable posts. Doing so kept them out of his way.

Another important aspect of dealing with conflict has to do with how to move forward when you lose a battle. It's natural to feel the desire to retreat when you experience an embarrassing failure, but don't give in to the temptation.

For inspiration, consider the story of Steve Jobs. When Jobs was fired from Apple, the company he co-founded in 1985, he considered leaving Silicon Valley. But instead he decided to start over. He started two companies, NeXT and Pixar, which went on to great success. He later called the whole experience, "the best thing that could have happened to me."

### 10. Final summary 

The key message:

**Although many of us mistakenly believe that power and success is earned by people who follow the rules, that's not the most effective way to get what you want. Instead, you should develop your leadership skills, find ways to stand out from your competitors and exude confidence.**

Actionable advice:

**If you want to establish a good reputation for yourself, ask for help!**

Too often, our attempts to promote ourselves are perceived as arrogance by other people. But if you can get someone else to promote you (by hiring a PR firm, for example), you can avoid this potential drawback.

**Suggested further reading:** ** _The_** **_Prince_** **by Niccolò** **Machiavelli**

_The Prince_ is a 16th century guide on how to be an autocratic leader of a country. It explains why ends like glory and power always justify even brutal means for princes. Thanks to this book, the word "Machiavellian" came to mean using deceit and cunning to one's advantage.
---

### Jeffrey Pfeffer

Jeffrey Pfeffer is a professor at Stanford University, specializing in organizational behavior.

