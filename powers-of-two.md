---
id: 5429211f6665620008300000
slug: powers-of-two-en
published_date: 2014-09-30T00:00:00.000+00:00
author: Joshua Wolf Shenk
title: Powers of Two
subtitle: Finding the Essence of Innovation in Creative Pairs
main_color: B92564
text_color: B92564
---

# Powers of Two

_Finding the Essence of Innovation in Creative Pairs_

**Joshua Wolf Shenk**

Despite the myth of the "lone genius," behind every creative type there is often a creative partner. _Powers of Two_ explores the idea of the creative pair, examining the relationship of creativity and the brain, while drawing heavily on examples of celebrated creative duos such as The Beatles' John Lennon and Paul McCartney, Apple Computer's Steve Jobs and Steve Wozniak, and _South Park's_ Trey Parker and Matt Stone.

---
### 1. What’s in it for me? Find out why the idea of a “lone genius” is a myth; it takes two to create. 

What do we mean when we say that two creative people have "chemistry?" Why has the notion of the lone genius — an individual from whose mind great works of art spring forth, fully formed — dominated our understanding of the creative process for so long?

These questions are at the heart of _Powers of Two_, a timely meditation on the value of creative partnerships. With the advent of the internet and the collaborations we witness online every day, the idea of the lone genius as the greatest source of creativity is finally being exposed for the myth that it really is.

In an exploration of the history of successful creative collaborations, you'll learn what the most celebrated partnerships have in common, and come closer to an understanding of what defines and inspires creative chemistry — and what causes its ultimate demise.

In the following blinks, you'll also discover

  * why competition and conflict are essential to a productive creative relationship;

  * why giving up our individuality in a partnership can actually increase our self-confidence; and

  * why creativity depends on a balance of solitude and engagement with others.

### 2. Creativity emerges from a balance of self-reflection and dialogue with others. 

How do great composers and artists come up with their masterpieces? The common idea is that the most celebrated geniuses of our time work as loners, locking themselves away in their studios until their masterwork is complete.

This is the myth of the lone genius. 

It derives from the Enlightenment era of the seventeenth and eighteenth centuries, a time when human nature was generally understood to be solitary and self-contained.

At that time, the belief that an individual's mind is the seat of creativity was a product of the political, economic, cultural and religious beliefs of everyday life. For instance, the notion that the world itself was created by a single, divine being led artists to consider their individuality as the fundamental driver of their creative force.

This idea continued into modern times — that is, until the advent of the internet. 

To the same degree that the internet has changed our social and professional lives, it has also transformed our ideas about creativity, and overturned the myth of the lone genius. 

The countless musical mash-ups, film parodies, collections of art or photography we view online every day have opened our eyes to the abundance of creativity that can result when two or more people collaborate or merely take inspiration from one another.

We now know that, in most cases, creativity comes not only from indulging in a long stretch of "alone time" but rather from a balance of self-reflection _and_ social interaction. 

To stimulate your creativity, you have to enter into some kind of creative exchange with another entity — whether another artist, a muse or even with your inner voice. The most important factor is that this "dialogue" is a balance of self-reflection (talking with your inner self) and interaction with others. 

The Dalai Lama is an example of someone who excels at both being alone and engaging deeply with others. Every morning, he wakes up at 3:30 a.m. and meditates. Then, at sunrise, he begins receiving visitors and spends the remainder of his day captivated by the company of others. 

This combination of solitude and social interaction enables him to lead a creative and engaged life.

### 3. The best creative relationships balance the similarities and differences of two people. 

What brings people to begin a creative relationship in the first place?

People often come together because they have things in common. These similarities form a kind of familiar foundation, on which both people feel comfortable. Using this foundation, and provided there is enough personal chemistry, the pair can then build a relationship that elevates them above and beyond their capabilities as individuals.

But if you are a creative type, how do you meet creative partners? 

These often life-changing meetings can happen in environments that seem commonplace, such as cafes, offices, parties or even weddings — what sociologist Michael Farrell calls _magnet place_ s. For example, _South Park_ creators Matt Stone and Trey Parker met at school, a classic magnet place. 

Yet similarities alone aren't enough to stimulate the development of a creative relationship. 

All creative partnerships require at least a few fundamental differences between collaborators. While common ground is the soil from which a creative relationship can grow and blossom, _differences_ between partners introduce an element of surprise and novelty in the creative process.

Indeed, a fruitful relationship doesn't require a perfect harmony of ideas and personalities. Your most effective collaborator may be in fact a person who challenges you to leave your comfort zone, provoking you to see your ideas from a different point of view.

One of the most famous creative pairings of the last century — John Lennon and Paul McCartney — were in many ways an unlikely pair. While McCartney came from a loving family and had a solid musical education, Lennon had spent his childhood living with his aunt and experienced a life filled with torment and breakups.

These differences, however, were the engine of their collaborative creative force: Lennon learned much from McCartney's musical proficiency and McCartney from Lennon's daring. The result was an intense period of creativity, during which they co-wrote over 180 songs.

### 4. As part of a creative pair, you must be present; and have confidence, trust and faith in your partner. 

One of the most famous and influential creative partnerships of the twentieth century was between dancer Suzanne Farrell and choreographer George Balanchine. Their relationship can teach us a lot about the creative process. 

The interaction of all creative pairs travels through three initial stages: _presence_, _confidence_ and _trust_. The final stage, however, that cements the creative relationship, however, is _faith_.

_Presence_ is the foundation of authentic interaction. To be present with someone means to be truly aware of who they are and be willing to accept them into your "space." Once both partners establish this presence, they can be honest about how they feel with each other — their weaknesses and strengths, joys and sorrows — and the creative process can begin in earnest.

After several gruelling rehearsals with Balanchine's dance company, Farrell and Balanchine established presence with one another. Because Farrell had opened herself up emotionally to Balanchine, he was able to create choreography perfectly suited to her talents.

_Confidence_ is the next stage. When partners are confident, they have an equal amount of respect for each other. This confidence can rest on shared (even mundane) qualities, such as reliability and punctuality. 

_Trust_, in contrast to confidence, is more holistic: it means having the belief that the other person will defend and protect you and your ideas, no matter what. In this stage, the creative partners surrender themselves to each other, confident that they're _together_ on the "right path."

When it came to ballet, Farrell let Balanchine be the sole judge of her capabilities. If he thought that she could perform a complex sequence of steps, she trusted his judgment and pushed herself, even if she first had doubts. 

The very last stage that takes a creative relationship to its ultimate level is _faith_. Trust transforms into faith when the partners know instinctively that the boundaries between them have dissolved and that each partner can trust blindly the guidance of the other. This is the moment where the creative bond becomes unbreakable, and where the magic of collaboration finds its most fertile ground.

> _"Two people do more than get to know each other or come to love each other. They absorb each other."_

### 5. The ritual is the foundation on which creative partners build their relationship. 

Creative relationships can involve partners making some pretty strange choices. Take artist Marina Abramovic and her partner Ulay, who chose to live together in a Citroën van for several years. 

Behind their decision was the belief that spending their lives together in such a confined space would force them to take their relationship to a higher level.

This kind of process is known as _the ritual,_ and it's the basis of some of the most successful creative partnerships. 

For pairs, the most basic ritual is the regular meeting, in which they leave their individual private spaces and create a shared environment. 

In this space, the pair develops its own private language. Over time, each partner can even take on the speech patterns and body language of the other — a phenomenon that psychologists call "social contagion." 

For example, investor Warren Buffett and his partner Charlie Munger are often referred to as "Siamese twins." They wear almost identical clothes, they walk and talk in the same manner and they each have a similar sparkle in their eyes. 

At this point, you may wonder whether such close bonding requires each partner to give up their sense of self. If a person becomes so involved in a relationship, wouldn't they lose a sense of their own individuality? 

Though it may seem counterintuitive, in fact the opposite is true: the more of your individuality you surrender as part of the relationship ritual, the _stronger_ you become as an individual. 

As singer and poet Patti Smith wrote in her book _Just Kids_, addressing her creative relationship with photographer Robert Mapplethorpe, the more time the two spent together, the more and better they knew themselves as individuals.

In other words, as you give up more of your own privacy, you gain more self-confidence. As a result, your ideas and ambitions are encouraged to grow too, and you might find yourself doing the best work of your life.

> _"Every pair has its own dance, a choreography of thrusts and parries … that shapes its way of moving across life's stage."_

### 6. There are different types of creative pairs, and different ways that partners influence each other. 

All creative relationships are different. In some partnerships, only one is the "star," or the spokesperson for the pair, while the other remains in the shadows. In other partnerships, however, like Lennon and McCartney's, each member is equally famous and revered.

The star-shadow pairing is known as an _asymmetrical_ partnership, where one partner "absorbs" the other. So, even though both play an equally crucial role in the relationship, only one partner gets the credit.

This often happens in teacher-student relationships. For example, Suzanne Farrell will be forever known as Balanchine's dancer, even though she had a major influence on most of his choreographies. 

An equal pairing is known as an _overt_ partnership. In this variation, both partners have the same status in the production of their work and both share in the public spotlight. 

But there's another type of creative pairing, in which each partner has a separate public identity. This is known as a _distinct_ partnership. This pairing doesn't collaborate in the traditional way, but instead serves as an advisor to, and a muse for, the other.

Patti Smith and Robert Mapplethorpe, for example, never collaborated directly, but they did rely on each other for guidance and inspiration. Because of the strength of their distinct partnership, they individually produced great works, such as Smith's poetic tribute to Mapplethorpe, _The Coral Sea,_ and Mapplethorpe's famous portrait for Smith's album, _Horses._

Within each of these relationship variations, there are different types of creative partners.

There's the _dreamer type_, who has enormous strength of character and great ideas, but may also start things they can't finish. 

Then there's the _doer type_. Doers are productive, efficient and reliable. However, they also struggle to be original and to initiate creative projects.

In many cases, creativity begins when dreamers and doers partner up. Though each type might struggle individually, together they complement each other perfectly and are able to accomplish great things.

### 7. It’s necessary to establish distance between partners for the relationship to run smoothly. 

As we've seen, many great creative relationships bloom from an intense period of closeness and bonding. But it's equally true that time spent apart is as important as time spent together.

Indeed, highly functional couples say that the secret to a good relationship is ensuring that both partners have time and space to themselves.

Of course, there's no explicit guideline for the amount of space creative partners will need. It depends on their personalities, their goals and their lifestyles.

Some people need to shut themselves off from social interaction to be creative. This isn't to suggest that they prefer isolation; they just need time and space to recharge.

This process is somewhat akin to meditation, as it involves removing yourself from outside influences to calm your mind and unleash your creativity.

However, beyond a certain point, distance isn't good for creativity. To function well, creative people tend to need a mix of both autonomy and intimacy.

A good example of this is the highly innovative partnership between poets Jane Kenyon and Donald Hall. Kenyon and Hall lived together, but at the same time enjoyed what they called a "double solitude." In effect, this meant that whenever they'd meet in the kitchen to have a cup of coffee, they wouldn't say a word to each other, but they _were_ aware of each other's presence.

But how exactly do autonomy and intimacy serve creativity?

When we're alone, we can access our unconscious minds. Many creative people have observed that they tend to be most creative when involved in a semi-automatic activity, like walking or swimming. These activities take a certain amount of conscious attention, but leave the unconscious part of their minds free to be used as a creative resource.

Psychologist Greg Feist believes that the most effective creative method is to separate the stages of generating ideas from the process of evaluating and elaborating on those ideas. In other words, first, work in solitude, and then, present your work to your partner and develop your ideas in collaboration.

### 8. Conflict and competition between partners are essential to the creative process. 

Although a harmonious creative relationship may be pleasant, the fact is that a more competitive relationship tends to lead to better creative work.

Certainly competition motivates us to do a great job. It's in our nature to strive to do better than our neighbors. This is no bad thing, as rivalry can motivate us to work harder and inspire us to improve ourselves. Lennon and McCartney were in constant competition with each other. So, when John would write a hit like "Strawberry Fields Forever," Paul responded by writing a song like "Penny Lane."

Sometimes the sense of competition can be so subtle that both partners are unaware of it. When novelist Sheila Heti was asked if she was in competition with her partner, painter and filmmaker Margaux Williamson, she responded that it was impossible since they worked in totally different fields.

But Heti also said that if Williamson had a more productive week than she had, she would immediately try to do a better job in her writing.

Naturally, the constant battle for power that such competition entails can bring conflict between partners.

But this conflict can be healthy: often, when two people are fighting for the upper hand, they'll also inadvertently drive the creative process forward.

For example, in an attempt to be the more powerful team member, a partner can become tyrannical and make the other partner fearful. This turns that partner into a "subordinate" who will then work harder to satisfy the powerful partner.

Curiously, this process can still lead to good creative work.

One example of such a relationship is that between famous movie director Alfred Hitchcock and actress Tippi Hedren. While shooting _The Birds_, Hitchcock controlled Hedren's every move. He dictated what clothes she could wear, her diet and which visitors she could see. The result was an incredible performance by Hedren, and a successful, popular film.

And although Hedren was traumatized by her experience, she admitted that she'd learned more in the three years it took to make _The Birds_ than she could've learned in 50 years working with a less-controlling director.

> _"Accept that your partner is a pain in the ass. Accept that you are a pain in the ass, so the two of you are made for each other."_

### 9. The same reason can account for both the beginning and end of a relationship. 

Though it's a cliche, the saying "opposites attract" happens to be true. And this is why the things you admire about your partner are often also what bring about the end of the relationship.

Early on in a relationship, we are drawn toward a partner based on a certain quality or a certain feeling which we like and which inspires us. Yet, as time goes on, this quality may intensify, sometimes to the point of unbearableness.

In one study, sociologist Diane Felmlee asked several people why they began a relationship and why they ended it. Interestingly, some 30 percent of respondents gave essentially the same answer for both questions.

One person found his partner initially "sweet and sensitive," but later "too nice." Another thought her partner was "strong-willed" when she met him, but later "domineering." Yet another was charmed by her partner's "sense of humor," and later annoyed because he made "too many jokes."

Another reason that relationships come to an end is that success gets in the way.

Psychologists have found that people with money tend to isolate themselves, choosing to be selfish and free of dependents. Often, when too much emphasis is placed on money, we can lose touch with ourselves and with those around us.

Wanting to avoid falling into this well-known trap, comedian Dave Chappelle chose to put a stop to his career. And Chappelle had good reason to be concerned: the first season of _Chappelle's Show_ was the best-selling TV series on DVD ever.

After the second season, Chappelle's creative partner, Neal Brennan, made a deal with Comedy Central to extend the show for two more seasons for an unprecedented $50 million.

However, after filming just a handful of episodes for the third season, Chappelle left the set and then the country, without telling even his closest friends and family.

In his defense, Chappelle later said that "success takes you where character cannot sustain you."

> _"The opposite of love is not hate: it's indifference."_

### 10. Even when a relationship ends, sometimes it can be hard to truly let go. 

Most people would like to think that a relationship can end in the same way as a theater play: once the curtain falls, the stage goes dark and everything is forgotten.

Unfortunately, such a clean ending is impossible.

Often relationships don't end exactly when we want them to. Moreover, even when they _do_ end, we can't truly let go of them.

Take the example of Lennon and McCartney. Before The Beatles eventually split up in 1970, the duo had been going through a long period in which their relationship was incredibly strained.

Tensions within the pop group were making it increasingly difficult for the pair to collaborate well. But rather than separating and perhaps ending the relationship on good terms, they chose to plough ahead.

Eventually, the strain became too much to bear, and the partnership dissolved acrimoniously.

But the animosity didn't end there: both Lennon and McCartney simply couldn't let go of their relationship, and continued to compete with each other in their years as successful solo artists.

Tragically, for some people, the end of a relationship isn't a new beginning; it really _is_ the end.

After Vincent van Gogh shot himself in the stomach in 1890, his brother and partner, Theo, gradually went insane.

First he quit his job. Then he moved into a new apartment, spacious enough that he could display Vincent's paintings, as in a museum. Ultimately, he became violent, was placed in an asylum and not soon afterward, died.

Even those partnerships that come to a less dramatic end can leave an indelible mark on the people involved.

Suzanne Farrell and George Balanchine worked together until Balanchine was too sick to continue. The connection between them was so profound that, after his death in April 1983, Farrell reported that she felt orphaned. In the following years, Farrell drifted away from dance, eventually cutting off all contact with the New York City Ballet.

### 11. Final summary 

The key message in this book:

**For centuries we have believed that creativity comes exclusively to artists who work in solitude and isolation. But behind every artistic creation exists a creative relationship. The myth of the lone genius is demolished by the power of two, because to create something meaningful there must be an exchange of ideas or emotions between two creative minds.**

**Suggested** **further** **reading:** ** _Creativity, Inc_** **. by Ed Catmull with Amy Wallace**

_Creativity, Inc._ explores the peaks and troughs in the history of Pixar and Disney Animation Studios along with Ed Catmull's personal journey towards becoming the successful manager he is today. In doing so, he explains the management beliefs he has acquired along the way, and offers actionable advice on how to turn your team members into creative superstars.

**Got feedback?**

We'd love to hear what you think about our content! Just drop us an email to remember@blinkist.com with _Powers of Two_ as the subject line, and share your thoughts!
---

### Joshua Wolf Shenk

Joshua Wolf Shenk is a curator, essayist and author. His article for _The Atlantic_, "What Makes Us Happy?" was the most-read online article in the history of the magazine. His first book, _Lincoln's Melancholy_, was voted one of the best books of 2005 by _The Washington Post_.

