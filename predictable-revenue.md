---
id: 556c6ba832366300079c0000
slug: predictable-revenue-en
published_date: 2015-06-03T00:00:00.000+00:00
author: Aaron Ross & Marylou Tyler
title: Predictable Revenue
subtitle: Turn Your Business Into a Sales Machine with the $100 Million Best Practices of Salesforce.com
main_color: 29CFBD
text_color: 15695F
---

# Predictable Revenue

_Turn Your Business Into a Sales Machine with the $100 Million Best Practices of Salesforce.com_

**Aaron Ross & Marylou Tyler**

_Predictable Revenue_ (2014) breaks open the secrets of the hugely successful SalesForce.com. You can't bid for more investment if your future sales are a mystery, so follow the steps in these blinks to anatomize and optimize your salesforce and create real — and forecastable — leads that keep on coming.

---
### 1. What’s in it for me? Bring in more revenue – consistently and predictably. 

How can you plan the next five years for your business if you don't know how much financing you'll have? You might count up what you've made so far, or guess how much your investors might contribute, get a bit creative. But then you're stuck, because there's no way to know how much you'll be making in the future. How can you plan anything if you have great results one month and no profit the next?

That's why you need predictable revenue. Predictable revenue means constantly increasing sales that you can reasonably expect and count on. With predictable revenue, you can plan far into the future for your company, and rely on the security your competitors are groping to find.

As an added bonus, when you next present your results to potential investors, you'll double your credibility by projecting how much revenue you'll bring in.

These blinks explain the secret to predictable revenue, and it's not just about increasing the number of salespeople!

In these blinks, you'll find out

  * why you shouldn't obsess about closing deals;

  * how you can turn a free trial into a sale; and

  * the differences between various sales leads.

### 2. You can’t increase your revenue with an outdated approach to sales. 

In sales, it's commonly believed that increasing revenue is simply a matter of hiring more salespeople. But is it so simple? Hiring more salespeople is an outdated approach that won't guarantee more revenue.

What you really need is quality, not quantity. In other words, a sales team that constantly generates new leads and customers. If you have a constant flow of new leads, you can convert them into a constant flow of new customers — that's predictable revenue.

The problem is, sales have changed. Nowadays, sales are more about attraction than promotion.

The old approach to sales was like someone poking you and saying, "Going to buy yet? Going to buy yet?" until you gave in — just to get that annoying person off your back. Older techniques relied on a lot of control and manipulation. What's more, sales people didn't really care what happened after they closed the deal, nor if their new customers were happy.

This simply won't cut it today. With the advent of the internet, customers can look up your company online, inform themselves more and learn about your product's quality. Any unscrupulous behavior can come back to bite you in the form of online reviews that are public and permanent.

If salespeople can't afford to be pushy anymore, how can they secure customers? By being respectful and offering something that will attract prospects because it represents real value.

If your revenue needs a boost, your company's sales technique should be the first thing you investigate. Instead of simply hiring more salespeople, why not work with what you have, and ensure tasks are being completed to the highest caliber.

One way to do this is to split the tasks within the sales department. It's just as important to have a good lead generation team as it is to have a team that can work with those new leads _and_ take care of the existing customers.

So what does a good lead generation system look like, and how can you implement it? Find out in the blink that follows!

### 3. The first step to effective sales: Understand the three different leads. 

So you want to learn how a good lead generation system works. Well, the first step is understanding exactly what a lead is!

A _lead_ is a sales prospect who is clearly interested in what you have to offer, either because they attended a seminar, downloaded a white paper, or informed themselves in some other way. Once they've left you some personal contact data, they are leads.

Leads can be converted into new customers by your sales team. But before you convert them, you have to understand the differences between them. There are three categories of leads.

The first category are _seeds_. Yes, just like the seed of a plant, these leads stem from the hard work that goes into building up your brand image, winning new followers and spreading the word. Seeds will often find you through internet searches, PR, social media and content marketing. Though it may take a while, if you care for these seeds, they're very likely to become customers.

_Nets_ are the second kind. They are won through classic marketing programs designed to reach a large audience. Such programs include email marketing, television advertising and some forms of internet marketing, such as ad placement on external websites.

One big advertisement on TV could get you thousands of thousands of nets in just 30 seconds! But these nets might not convert into customers as often as seeds.

The third type are _spears_, and attracting them involves _targeted outbound efforts_. This means building a _target_ _profile_ of the kinds of people or firms you're targeting, then pursuing them one after the other. In other words, you'll need to hire people to chase after the spears.

> _"The most common mistake is lumping all the types of leads into one bucket of 'leads' and then making future projections based on past results."_

### 4. Use referrals and free trials to generate steady inbound leads. 

Some leads are easier to get than others — they're the ones that approach you before you approach them, perhaps by following your site or signing up for your newsletter. These are _inbound leads_, a category that includes seeds. Inbound leads make excellent prospects, so how can you attract more of them?

A great way to attract inbound leads is through _referrals._ Happy customers who recommend your products or services convey the trust they have in you to other people, who are then more likely to convert to customers.

If you sell your products and services to other businesses, you can influence this stream by giving referrals too; after all, people are more likely to return favors you do them. Additionally, people who come to you through referrals should be able to get in touch with you easily. You don't want to lose these valuable leads just because they didn't know how to contact you.

Another effective approach to generating inbound leads is through _free trials_. Ten years ago, many software companies didn't offer free trials, fearing that competitors would copy their ideas. Nowadays it's an incredibly popular technique to gain new customers and sell products. Indeed, for service and software companies, this is an ideal lead generation method.

But what if you're not a software company? Well, there are still ways to offer free trials. These could be free consultations, online training videos or sample products.

Of course, there are many more marketing methods that generate inbound leads — SEO, email newsletters, affiliate marketing and social media are also highly effective tools. Choose just a handful and focus on using them as effectively as possible. Once you're confident with those techniques, you'll be able to expand your approach toward a broader base of inbound lead generation.

### 5. Conferences can also generate valuable leads. 

It's true that conferences and trade shows have a bad reputation in sales. However, they can be more effective than you've been told. Though you might assume guests feel overwhelmed with the options and free giveaways available, there are ways to make your product stand out.

First, you'll need to have an event sales team that takes responsibility for the entire process, from preparation to execution, to evaluation and follow through.

In order to prepare for the event your team should create a list of companies that will attend. Then it should select the companies that match your target group and research them.

You'll need to find out who the decision makers are at the fair, as these are the people with which you'll want to connect. Even these interactions can be prepared for, with cheat sheets that outline key points about the targets. This will make it much easier to start conversations.

A strong strategy is just as vital as preparation. So make sure the members of your sales team use their cheat sheet to proactively approach target prospects. Remember, your time is limited, so companies that don't fit your target profile can be omitted from your leads list. Otherwise your salespeople will just be wasting time making follow-up calls.

A thorough evaluation is also important as it will boost your chances of success at future conference events. So how can you assess the success of your event? You could, for example, measure the number of leads within the following two to four weeks. Or you could examine the actual sales results over the next two to six months.

Now that we've learnt all about generating leads, it's time to think a little more broadly. What makes a strong sales organization overall? Read on to find out.

### 6. Boost productivity with specialized sales teams. 

How do you build a great modern sales organization? Take that daunting task step by step. The first step is specialization: This means splitting responsibilities and tasks into different sales roles.

Without specialization, you risk inefficiencies. There are several different tasks involved in sales, so why force your team members to take on all of them? You'll boost productivity if you divide your sales team into four core functions.

The first of the four is _inbound lead qualification_. This group consists of _market response representatives_ who take care of the marketing leads coming through your website or hotline. These leads usually arise from marketing programs, search engine marketing or referrals.

The second core function is _outbound prospecting_. In this section, _new business representatives_ develop sales opportunities from lists of target accounts that are "cold" or inactive.

This team is most effective when it implements _Cold Calling 2.0_, a method that will be explained in the next blink. They don't close deals but qualify and create sales opportunities to pass on to _account executives._ Account executives work within the third core function, the process of _closing deals_, which we associate most strongly with sales.

The fourth and final function is _account management_, where current customers are cared for with offers for new deals and products, encouraging them to keep buying from you.

Perhaps you can easily imagine four separate teams to cover these functions in a large company, but what about small businesses?

Well, even endeavors with very few people can, and should, specialize their sales team. For example, the first person you hire should be a person who can close deals. The second person should be a salesperson who is dedicated to generating leads for the first.

> _"One of the biggest productivity killers is lumping a mix of different responsibilities into one general 'sales' role."_

### 7. Implement “Cold Calling 2.0.” 

In the last blink, we touched on the method that is vital for sales development representatives: _Cold Calling 2.0._ So what is it? Think of it simply as prospecting for new customers.

Cold Calling 2.0 begins with the creation of an _ideal customer profile_. Think about it: contacting the wrong people is just a waste of time! It's your ideal customer who has a higher chance of buying your product and bringing you lots of revenue. Ask yourself: which industries need your products? Who is already using similar products, or looking to invest in new products like yours?

Next, consider how you might build a database with these kinds of prospects. Sometimes your company might already have an established list, sometimes you can buy one. Check these options before building your list from scratch.

Once you have this list, you're ready to go. But you needn't start with cold calls straight away. Instead, open with emails. You can use phone calls to follow up with prospects who respond to your emails.

The best way to use email is by sending mass messages to the prospects in your database, ensuring they look like single emails from one salesperson. Don't send too many emails every day, but send fewer emails on a regular basis. If you generate more than five to ten responses per day it will be hard to handle them.

Once you acquire leads, you'll need to pass them on to account executives. It's vital to recognize that the passing-over of leads is just as important as their generation. So you'll need a system to ensure that this process works smoothly between departments without losing any leads. Of course, you should also ensure that the relationship is a fair and respectful one.

### 8. Use best practices to strengthen your sales approach. 

The techniques we've learned about in these blinks will put your company well on its way to becoming a sales leader. However, your sales approach wouldn't be complete without crucial best practices to provide a solid foundation for your methods. And the best place to start is with the attitude your teams have toward sales themselves.

Many sales teams place the most importance on closing deals, but they're wrong. In fact, salespeople often feel so pressured to close deals that their performance is compromised. By thinking only of closing, they'll often fail to develop a positive relationship with prospects. Instead, your team should think beyond closing. How?

By building a simple _success plan_ for your clients. A success plan paints a picture of the gains the client can achieve with the help of your company. Try writing down exactly how your products or services will make your client's life better once they use them.

Clients are more likely to become loyal customers if they know you care about their business, so communicate this vision to them.

Another best practice is the three-hour-and-15-minutes sales process. It's a simple, three-step approach that goes like this: in the first 15 minutes, a salesperson should make light conversation with the prospect, to find out if it will pay off to talk further. This will become clear if you talk about their expectations right away.

The next stage is the first hour: a qualification/discovery call. In this conversation you talk to those responsible for new deals. Both sides need to find out if there is a fit. If so, you both plan a group working session that brings in all key people and decision makers.

The group working session covers the final two hours, and that's where you develop a collaborative vision together. At this stage you should describe clearly how your products will help the clients to become successful.

These best practices should be combined with the other methods outlined in these blinks. In this way, you'll be able to both increase your revenue, and make it something your company can always count on.

> _"Customers don't care at all whether you close the deal or not. They care about improving their business."_

### 9. Final summary 

The key message in this book:

**Today's world requires a new approach to sales. Salespeople must truly understand lead generation, from the different types of leads to the most effective approaches for generating them. With a specialized team that ensures every step of a sale is performed to a high caliber, and an organization that is committed to best practices, you can expect powerful and reliable revenue.**

Actionable advice:

**Cultivate positive energy.**

To maintain positive energy in your sales team, allow them a short break every 90 minutes, and ensure they take a full lunch break with co-workers. Increasing your employees' workload may yield results in the short term, but will only destroy their enthusiasm in the long term. To avoid burn out or high turnover, make their positive energy your priority.

**Suggested further reading:** ** _Double Double_** **by Cameron Herold**

_Double Double_ (2011) gives you the keys to unlock powerful growth in your business. These blinks will coach you, from effectively defining your company's vision to boosting employee performance to ensuring your resilience as a leader.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Aaron Ross & Marylou Tyler

Aaron Ross is the managing director of consultants Predictable Revenue, Inc. In his previous work at SalesForce.com, he and his team were responsible for new sales techniques that revolutionized the principles behind sales and cold calling. He's also the author of _CEOflow: Turn Your Employees into Mini-CEOs_.

Marylou Tyler is the CEO of Predictable Revenue, Inc. After 25 years in the sales business, she's racked up clients like Apple, Deloitte and Mastercard.

