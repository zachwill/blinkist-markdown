---
id: 564078413866330007370000
slug: predictable-success-en
published_date: 2015-11-11T00:00:00.000+00:00
author: Les McKeown
title: Predictable Success
subtitle: Getting Your Organization on the Growth Track – and Keeping it There
main_color: 26B6BF
text_color: 176D73
---

# Predictable Success

_Getting Your Organization on the Growth Track – and Keeping it There_

**Les McKeown**

_Predictable Success_ (2010) is a step-by-step guide that lays out the different stages of organizational development that occur on the path to sustainable success. The author provides tips and strategies for every phase of the process, showing you what it takes to achieve — and then maintain — predictable success, year after year.

---
### 1. What’s in it for me? Get yourself to the sweet spot in the business cycle – and stay there. 

Like Dorothy's famous journey in _The Wizard of Oz_, a company's path to success is paved with danger. Many companies start out aiming to achieve greatness, yet few actually make it. Most fall back somewhere along the yellow brick road, sapped by lack of customers, lack of money or lack of management. 

These blinks show you how your business can traverse this perilous path and reach a level known as _predictable success_, where you can plan your objectives and know you'll reach them. The blinks will also show you how to stay there. As hard as it is to get to where you want to be, it's just as tough to stay relevant and dynamic. So, if you want to reach — and hold onto — that sweet spot, read on.

In these blinks, you'll learn

  * how to navigate the dangerous business rapids known as _whitewater_ ;

  * how to grow as a business but still allow employees the space to innovate; and

  * why a CEO with a full calendar is a sign of business weakness.

### 2. Any company can consistently meet its objectives – regardless of size, age or finances. 

Business is tough: for many companies, success — or failure — is often a matter of sheer luck. No matter how good your idea may be, it's impossible to predict how it'll fare upon execution. 

Want to change that? Wouldn't it be nice to achieve _predictable success_, when you consistently meet the goals you set for yourself? That might sound impossible, like a privilege solely reserved for companies that have been around for decades. But the truth is, neither age nor size nor finances determine a company's predictable success. 

Consider Little & Co. The credit-card payment processing company hit the top of the Inc. 500 just five years after it was founded, putting it in the same league as the 120-year-old SC Johnson.

Finances don't determine predictable success either. Microsoft might have billions of dollars in reserve, but it doesn't consistently meet its goals; meanwhile, the small, self-funded graphic design firm the author works with has never fallen short of its annual targets. 

So, if age and cash don't lead to predictable success, what does? The answer is management. Managers with strong visions and unrelenting commitment to a set path will help their companies achieve predictable success. These leaders stay calm even when external or internal problems arise, working to assess the problem and determine the optimal response. 

Flowing water is a great metaphor for the management style you need. When a stone falls into running water, the water is utterly indifferent to the motives of the stone. It simply responds accordingly, with minimal drama; when the stone is pushed downstream, the water quickly returns to its calm, still state. 

In the following blinks, we'll show you how companies learn to embody running water and achieve predictable success.

### 3. There are three stages on the path to predictable success, beginning with “early struggle” and “fun.” 

No company can achieve predictable success right from the get-go if it doesn't follow a process. And though each stage is filled with challenges, if a company plays it right they'll be unstoppable. 

First, a company will hit the _early struggle_. This initial stage requires you to address two simple problems:

  1. Are there enough buyers for my product?

  2. Do I have enough cash on hand to pay my bills?

Of course, most companies fail to find an answer to these questions, which means they never advance past the first crucial stages. 

To join the elite 20 percent of businesses that move further along the path to predictable success, follow this simple rule: do your best to have three times as much cash as you think you'll need. 

The author learned this lesson while helping Pizza Hut set up new franchises in Ireland: even though the franchises were convinced they had sufficient customer base to meet their cash-flow demands, they quickly ran out of money. So, to reiterate: figure out how much money your operations require, then triple that sum. 

Next, once you've identified a viable market and stabilized your cash flow, you'll be able to move into the second stage: _fun._

In this period, your company will likely grow very quickly. Sales will flood in, bringing large amounts of revenue. Since this happy period follows the difficulties of the early struggle, it may seem like you've finally made it. But as a result, you may be tempted to overspend just to keep the good times rolling. 

Don't do it! Remember, you still have to pass through the third stage before you can finally achieve predictable success.

> _"Whatever your business plan says you need in funding, triple it."_

### 4. During the third stage – “whitewater” – demand for your product outpaces supply. 

So, in the second stage you're having fun. Your business is doing well and you have plenty of customers. Seems great, right? Well, here's where things get tricky. 

When an organization grows too quickly, decision-making and execution become more difficult. During this third stage, _whitewater_, mistakes become very common. Prepare yourself: you'll have to devote much of your time and resources to trying to solve these problems. 

In whitewater you'll be unable to meet the accelerating demand for your product or service. And when you can't keep up, you might get sloppy (for example, mixing up orders) or be forced to cancel on customers. In either case, you'll end up with unhappy customers and negative reviews, ultimately slowing down your growth. 

But it doesn't have to be that way. Any company can get out of the whitewater through effective management: make sure your sales and operations teams are working together closely. If your sales team knows how much their operations counterparts can handle, they can better manage customer expectations.

The author once advised a woodworker named Ian. During their first consultation, Ian was in deep whitewater: an onslaught of customer orders had undermined the quality of his work. As a result, he was losing clients and seeing his profits decline.

The author instructed Ian to finetune his communication and feedback system, to better align his sales and operations. And that's how Ian managed to get out of the whitewater stage and achieve predictable success.

Of course, maintaining predictable success in the long-term can be tricky. But in the next few blinks, we'll show you exactly how it's done.

### 5. The three stages that follow predictable success can send a business into freefall. 

Congratulations, you've achieved predictable success! Time to breathe easy? Unfortunately, there are still challenges ahead. 

Immediately after you've arrived at predictable success, you may encounter the _treadmill_. This phase occurs when a company becomes overly dependant on established systems, thus losing its creative streak. 

At this point, many founders are tempted to leave their companies because their influence is waning and their energy diminishing. 

That's exactly what happened to Derek, the founder of a successful PR agency. Once his firm hit the treadmill stage, his work became increasingly bureaucratic and he could no longer identify with the company he started. As a result, he sold the majority stake to a national advertising conglomerate and moved on to new projects. 

After the treadmill period, companies may fall into _the big rut_ — the penultimate stage of organizational development. This occurs when businesses lose sight of their mission, shifting their focus away from the customer and wholly onto the company itself. 

Organizations heavily populated by bureaucrats are particularly susceptible to the big rut. The author experienced this firsthand when he met the owners of a chocolate factory that had been around since 1908. Management was narrowly focused on upholding the tradition and staying faithful to the "way it's always been done," at the expense of innovation and progress. That's a surefire way to lose ground to upstart rivals!

Also, it can lead to the final stage of organizational development: the _death rattle_, which signifies the end. As a rule, companies that aren't creative will eventually fall behind when new innovative competitors disrupt the market. 

Consider what happened when the music industry shifted from CDs to MP3s: plenty of big players lost out when they failed to adapt to changing consumer demands. 

Don't let that happen to your business! Because once the death rattle has sounded, there's no way back from the brink — the company must fold. 

We've seen the challenges that face organizations before and after predictable success. In the upcoming blinks we'll learn how to maintain the perfect balance.

### 6. Simplify decision-making and unite employees around a collective vision to move from whitewater to predictable success. 

Every stage on the path to predictable success has its challenges. But the most difficult shift occurs when your company moves from whitewater to predictable success. 

At this point, you may see your organization's decision-making process become overly complex and bureaucratic. Management needs to work against this impulse, simplifying the company's decision-making at every level. 

For example, when hiring new staff, management should ask employees who will be working closely with the new team-member to define the parameters of her role and guide the search process. So, if you're looking for a new head of marketing, ask people in the marketing department to describe what they would need and expect from a person in that position. 

That way, you're more likely to find someone who's the right fit for the organization. What's more, by allowing people at every level to make decisions, you simplify and speed up the process. No one wants to wait around for overworked managers to make the call.

Another problem occurs when an organization scales too quickly: alignment and self-accountability are lost. In the whitewater stage, tried-and-tested company values like, "We believe in total quality" or "The customer always gets what the customer wants" may start to seem like pure rhetoric, to the detriment of quality and customer satisfaction. 

As a result, employees in various departments will rewrite the vision to suit their needs. Customer service staffers may start to placate the customers or simply ignore them, while meanwhile the manufacturing department might try and stem the problem by cutting the manufacturing rate. 

To deal with this, management has to work with employees to design a new company culture that everyone believes in. It's important that you come up with a _realistic_ vision, one which will empower employees. Doing so will recalibrate everyone's self-accountability and align departments within the company, pushing the organization to predictable success.

### 7. After achieving predictable success, install effective systems and institutionalize risk-taking. 

As we've learned, it's difficult to maintain predictable success. Luckily, there are two steps you can take to avoid the treadmill, the big rut and the death rattle. 

The first step requires you to install two systems.

The first system turns your organization into a highly efficient decision-making machine. The author learned this lesson from one of his clients, who had implemented a very specific process: "data, debate, decide or defer." They started by collecting the relevant information, openly debating what to do for as long as necessary, and then finally either making a decision or deferring the subject to a subgroup. This was an efficient and effective process. 

The second system centers on your employees. It means your firm focuses on company- and people-driven visions, drawing on the skills and expertise of staff rather than forcing management-generated strategies onto them. 

Now comes the second step for maintaining predictable success — and it's a tricky one. You must institutionalize risk-taking and innovation. Management often fears that "institutionalizing" risk will lead to mistakes and misuse. But the thing is, risk-taking is crucial for any business; it's the only way you can learn and innovate. So managers have to find ways to support it. 

But here's why it's tricky: you need to be able to allow some risk-taking in order to make your company more flexible, but you also need to protect your organization from unnecessary risk — especially if it doesn't align with your overall mission and vision.

The key is to limit risk-taking to the core business. Don't make any "wild bets" on extraneous products or services. Also, don't ever engage in any unethical activities. 

By following the predictable success framework and these two further steps, your organization will set itself up for positive long-term outcomes.

### 8. To avoid decline and to maintain predictable success, be flexible with systems and prioritize good HR. 

If you've achieved predictable success but feel that you might be backsliding, there are a couple of things you can do to get back on track. 

Most importantly, make sure you're using your implemented systems effectively — not being used _by_ them. After all, having a perfectly organized calendar doesn't make for an effective CEO. A CEO who doesn't have a single moment of free time in the next five weeks has no time to innovate, dream or create. 

The author would advise this CEO to break free from his system and create regular "office hours," allowing employees to drop in for an impromptu discussion. That would boost creativity and create a beneficial feedback loop between management and employees. 

Prioritizing recruitment and training is another way to maintain predictable success and to avoid slipping into either treadmill or decline. 

Motivate new employees and help cultivate the right mind-set by emphasizing "why" over "what." Open up a dialogue and explain the company's processes. That way, the employee understands the reasoning behind the systems, thus potentially finding ways to improve them. 

You should also focus on success by encouraging innovation and creativity; if an employee fails to comply with a system, don't linger on it if his decision works. That's why your evaluation system should be more qualitative than quantitative: give employees actionable steps to improve their performance, not numerical scores. 

Think of yourself as a coach with her team after the game: watching a clip of an effective, correct play will motivate the team; screening ten fumbled plays will only discourage them. 

All in all, by following all these steps, your organization can maintain predictable success, avoiding both the treadmill and the decline.

### 9. Final summary 

The key message in this book:

**Every organization can consistently meet its goals, regardless of size, age or finances. It isn't a question of luck, but rather a matter of carefully following a step-by-step process.**

**Suggested** **further** **reading:** ** _Scaling Up_** **by Verne Harnish**

You had the idea, you drafted the business plan, you raised the cash, you launched your new venture and you became a success. But now you need to grow. _Scaling Up_ (2014) reveals the most useful tools for doing just that. Use the Scaling Up system of checklists, levers and priorities to establish a strong company culture as your business expands through the right strategic and financial decisions.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Les McKeown

Les McKeown is the president and CEO of Predictable Success. He has personally founded over 40 companies, including an incubation consulting company that has itself launched hundreds of businesses worldwide. His clients include the US Army, Harvard University, Microsoft, VeriSign and more. McKeown has been featured on CNN, ABC, BBC and in _USA Today_ and the _New York Times_.

