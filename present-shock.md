---
id: 56e6f52c9854a5000700015d
slug: present-shock-en
published_date: 2016-03-18T00:00:00.000+00:00
author: Douglas Rushkoff
title: Present Shock
subtitle: When Everything Happens Now
main_color: EEEE30
text_color: 6E6E16
---

# Present Shock

_When Everything Happens Now_

**Douglas Rushkoff**

These blinks are all about the mental and emotional state we all live in thanks to our rapidly changing technological culture. _Present Shock_ (2013) explains the roots of this problem and what it means for our mental well-being.

---
### 1. What’s in it for me? Grasp what’s so confusing about our current culture. 

Do you know that nagging feeling that you're missing out on something really important, even though you're constantly checking social media and news sites? Or are you done with being online all the time and dream of running away to live in some sequestered cottage? 

We've surrounded ourselves with a world of digital information, and now these media are setting the pace for us, which can be overwhelming. After all, we're not cyborgs yet, just creatures bound to natural rhythms.

In these blinks, you'll find out how digital media are transforming our personal lives, our culture and even our perception of time — and learn about the conflicts that arise in the process. 

You'll also discover

  * why planning and patience are no longer our strong suits;

  * that we all have multiple personalities nowadays; and

  * why you can blame the remote control if your biography resembles that of Bart Simpson rather than Luke Skywalker.

### 2. We’re disoriented and stuck in an eternal present. 

Imagine you're practicing tennis with a ball machine, when the machine suddenly starts shooting balls out faster and faster until you can't keep up. That's how cultural and technological change has progressed for the last few decades.

Around 1970, the futurist Alvin Toffler predicted that we'd soon reach a rate of progress that was so fast we would enter a state called _future shock_. 

In the twentieth century, we had a future-oriented view of technology. People obsessed over revolutionary inventions and business models they thought were on the way. Everyone was excited by the possibilities afforded by new technologies, like cell phones that let us talk to our friends, relatives or co-workers at any time. 

The pace of change kept increasing, however. Computer processing speed doubled every two years. 

That's why Toffler predicted that we'd reach a point where we wouldn't be able to keep up, mentally or emotionally. We'd experience _future shock_ — a kind of culture shock that happens within your own culture.

That "future" is already here. People increasingly feel lost in the modern world and we're no longer motivated by the technological optimism of the twentieth century. Future shock has turned into _present shock_. 

We're surrounded by change and lack a clear sense of direction, so we've given up on planning for a better tomorrow. Instead, we want everything _now_. 

This feeling manifests in many ways. Few traders look for long-term investments, for instance, preferring deals with instant benefits instead. 

Just think about the investors who already had Facebook shares the first day the company went public. Many sold them the very next day because they hadn't risen enough.

> _"When people stop looking to the future, they start looking at the present."_

### 3. We now prefer fragmented stories instead of linear stories, which adds to our disorientation. 

Stories used to be laid out with a clear beginning, middle and end. Think of _Snow White_ or _Star Wars,_ for example. This is no longer the case, however.

People used this classic structure because it helped them make sense of things. After all, stories are instruments of thought: they give order to the world around us. 

That's why stories followed a linear structure for centuries. They started with a hero the audience could identify with, like Luke Skywalker. At first, Luke is just a regular guy living with his family. 

Then something unusual happens to the hero, prompting them to go on some kind of journey. For Luke, it happens when his family gets murdered and he sets out to become a Jedi.

The tension is resolved in the end when the hero wins or loses. _Star Wars_ concludes when Luke saves the rebels and avenges his family. 

People soon learned to distrust these stories, however, thanks to the politicians and advertisers who used them to manipulate us. 

Consider a person who enlists in the military, for instance. They would think of themselves as a hero going off to save their country, but once they get there, they might realize the war is raging for very different reasons and they've been deceived.

The soldier would return home skeptical or resentful toward traditional stories. So many people have had experiences like this that we now prefer a more fragmented form of storytelling. 

Our technology enables this fragmentation even further. We can zip through the channels with a remote or switch to a new YouTube video whenever we like — we don't have to stay on a continuous narrative.

### 4. Digital technology distracts us, and distorts our identity and perception of time. 

Your smartphone enables you to do something your ancestors could never do: be in more than one place at a time. That's great in some ways, but it also makes life more confusing.

People conceived of time differently in the pre-digital era. They considered time to be linear and thought they could only be in any one given physical space — where their body happened to be!

A person's day would play out like a series of linear stories. Each story had a specific setting, whether it was home, the office or the mall. When you were in a particular setting, you were completely absorbed in it and not connected to the other environments in your life. 

The modern world is different. Today, each person has a collection of digital selves that take place in other spaces, though the spaces are no longer physical.

We can call our digitally fragmented identities _digiphrenia_. You might have a Facebook profile, some well-crafted online gaming avatars or even an anonymous handle you use to troll message boards. Each "self" is unique and serves a separate purpose.

And these identities can be much more extreme. Consider a drone pilot who goes to work and kills people from the comfort of his office chair, then returns home safely to his family. This takes its toll: drone pilots are more likely to develop PTSD than pilots physically flying in Afghanistan.

We're also taken out of our present environments by constant streams of what seem to be real-time information, like Facebook or Twitter notifications. Your phone might even vibrate each time one comes in, repeatedly distracting you from the outside world.

These updates take you out of the moment. The live feed might seem like a part of the present but each notification refers to an event in the past, probably in a different place. And how often are these updates _really_ important?

> _"Whatever is vibrating on the iPhone just isn't as valuable as the eye contact you are making right now."_

### 5. We’ve lost sight of the importance of timescales. 

Young people imagine that life lasts forever. Yet when you zoom out, the whole of human history seems to have lasted just a moment. The Big Bang happened about 13 billion years ago but modern humans have only been around for 200,000 years. 

Does this mean there are multiple kinds of time? Not exactly. But there are several _timescales_, according to the writer Stewart Brand. 

Brand wrote that different kinds of changes happen simultaneously, but at varied speeds. Just imagine six concentric rings rotating around each other. The outermost ring is the slowest and the innermost is the fastest. 

The outermost ring is for very slow developments like geological changes. It can take eons for a glacier to carve out a valley.

Cultural changes are also slow but happen faster than geological shifts. A culture usually lasts for about a millennium, like the ancient Mayan or Roman civilizations. 

_Governance,_ or systems of power, change within the same civilization. A civilization might change from a monarchy to a republic, for instance. 

Within a given culture, _fashions_ change even faster. Commerce and trade are driven largely by a society's evolving tastes.

All of these changes can be in process at the same time but they play out on various timescales. And changes on one timescale can influence another. When a culture declines, for example, it may be easier for a tyrant from another culture to take it over and alter the governance. 

Our state of present shock, however, has muddled our understanding of timescales. We get confused about changes that should occur on slower timescales because we're so fixated on the present. 

That's why politicians, who _should_ operate on the timescale of governance, often focus too much on immediate results like polls, which change very quickly like fashions. Sometimes they do this deliberately but sometimes they have no choice, as it's all their voters can understand.

### 6. Final summary 

The key message in this book:

**In the last century, we imagined what technology could do for our future. Today, we use it to focus on the present. We're bombarded with information that takes us out of the moment and prevents us from thinking about the future, and we prefer story structures as fragmented as our daily lives. We even understand time differently. Our technology has changed our lives fundamentally — it has put us in a state of present shock.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Who Owns the Future?_** **by Jaron Lanier**

_Who Owns the Future?_ explains what's wrong with the current way the information economy works, and why it's destroying more jobs than it's creating.
---

### Douglas Rushkoff

Douglas Rushkoff is a prominent media theorist. He's written several books including _Life Inc_ and _Program or Be Programmed_. He also contributes to the _Guardian_ and CNN and has written and hosted documentaries such as _The Persuaders_ and _Digital Nations_.

