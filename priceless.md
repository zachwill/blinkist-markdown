---
id: 558922be3466610007400000
slug: priceless-en
published_date: 2015-06-24T00:00:00.000+00:00
author: William Poundstone
title: Priceless
subtitle: The Myth of Fair Value (and How to Take Advantage of It)
main_color: 2F613B
text_color: 2F613B
---

# Priceless

_The Myth of Fair Value (and How to Take Advantage of It)_

**William Poundstone**

_Priceless_ (2010) explores the psychological reasons behind the value and price we give to things. Through numerous experiments and case studies in pricing, the author explains how prices influence our purchasing decision and exposes companies that use pricing to increase profit.

---
### 1. What’s in it for me? Understand how prices work, and buy smarter. 

When was the last time you bought something, took it home and immediately thought, "I've made a huge mistake and wasted my money"?

Humans aren't very good at prices. We have a hard time placing monetary values on things, so we let other factors influence us. The problem is, we don't have much of a filter. Not all of these values are relevant — a number we hear on the radio can affect how much we're willing to spend on dinner!

That might sound crazy, but these blinks include many examples and studies that prove it's true: When it comes to prices and evaluation, we need help.

In these blinks, you'll learn

  * how evolution made us more afraid of losing what we have than excited about gaining what we don't have;

  * why you're more fair when people are watching you; and

  * how we forget prices almost immediately.

### 2. Prices are relative – we can’t estimate them without a reference point. 

What should a jar of peanut butter cost? Maybe you know because you often buy it. But what about a pearl oyster? You probably have no idea.

Although we're sensitive to price differences, we aren't always able to estimate an item's fixed price, or its _absolute monetary value_. But are the prices we see in a supermarket fixed? If they were, you'd be happy to pay $10 for a bottle of red wine from one vineyard, no matter what a similar bottle of red wine from the vineyard next door costs. As you know, it simply doesn't work that way. Why? Because prices are relative — they depend on each other.

Here's a question for you: Would you be able to guess the exact weight of an object just by holding it in your hand? What about if you held a lighter object that you knew the weight of first? People find the second task much easier, as psychologists have known since the 1800s.

It's just the same for prices: Consumers can say _which_ product should cost more, but can't estimate what a product should cost without a reference point. For example, if before an auction you asked bidders what their top bid would be, most of them wouldn't know for sure. Why? Because it depends on the first bid, and how much others are willing to pay.

If you aren't an auction bidder but just a regular consumer of supermarket peanut butter instead, you'll have a better chance at estimating how much you'd pay for a jar. By buying the product frequently, you'll learn and remember the average price. But could you remember the price of a jar of sun-dried tomatoes you bought for a dinner party last year? Perhaps not! Our memories of prices are limited, and it's nearly impossible to remember all the prices you see in the supermarket.

### 3. Prices don’t always make everyone feel the same. 

Would finding a $20 bill make you twice as happy as finding a ten? Perhaps, or perhaps not. Double the amount of money won't always make you twice as happy. It all depends on your financial security.

If you're doing well financially, $10 becomes meaningless in comparison with the money you already have. This phenomenon is called _the wealth effect_. A thousand dollars is less desirable to a millionaire, while $100 would be a miracle for someone who has nothing. You'll react more strongly when you lose or gain money if it affects your financial status.

So when philosophers asked Harvard students what would make them twice as happy as $10, the surprising answer was $40. All the students were financially secure due to their wealthy families and were happy to receive $10 just for the novelty of it. But, once that excitement wore off, it took more than twice the amount of money to please them.

Another factor that determines how you'll react to a price is _utility_, the value you assign to products.

So if you absolutely love chocolate, you're willing to pay much more for it than someone who is only mildly fond of it. Instead, they prefer to put all their money toward their stamp collection, while you wouldn't dream of spending more than a dollar on a stamp.

### 4. Price decisions are influenced by many factors we rarely even recognize. 

The likelihood of winning a lottery is almost nil, but millions of people try their luck anyway. Ever wondered why?

Due to lack of time and information, we often make decisions based on _heuristics._ Heuristics are mental shortcuts our brains use to make quick judgments based on prior experiences. This decision making can be described as _boundedly rational._

Psychologists illustrated boundedly rational thinking in experiments with gamblers. For example, instead of choosing the gamble with the highest average value, players only account for basic factors in their decisions, such as looking for the highest payoff, or else they act on intuition.

When other people try to pressure us into decisions, we may choose to go with our gut feelings instead. But what if our very instincts are being influenced too?

The hormone oxytocin promotes trust and generosity. When produced during childbirth and breast-feeding, it establishes a powerful emotional bond. Generosity is something every salesperson would love more of in their clients.

In fact, a new product has been invented for this very reason: Liquid Trust is a spray containing oxytocin for use in perfumes and greeting cards. Scientists maintain that oxytocin can only be successfully administered via injection or by directly spraying it into the nostrils, so Liquid Trust probably doesn't work. But it's not unlikely that your gut decisions are being influenced more than you think!

### 5. Insignificant numbers can influence our purchasing decisions. 

Do you remember the temperature of the weather forecast this morning? If you don't, you might be surprised to hear that it can still influence your next purchase.

When we have to estimate prices or numbers without any given scale, we construct them from hints in our environment. These hints are called _anchors_. In this way, even the most insignificant numbers we chance upon can shape our subsequent decisions without us realizing. And anyone influenced by a higher anchor will estimate higher.

In one experiment participants were handed questionnaires with post-it notes attached to the front with a number written on them in a colored ink. Some of them were asked to copy out the number onto the questionnaire. Others just had to write down what color ink was used.

The participants were subsequently asked to estimate the number of physicians in a phone book. Those who had painstakingly copied out the figures of their questionnaire ID number guessed a number that was as much as 55 percent higher on average than the guess of the group that just had to write down a color!

But what if we're aware of an anchor, and consciously using it as a reference point? Our behavior is impacted to a greater extent.

Another experiment asked participants to estimate the weight of a second object after feeling the weight of the first. The first object was of course the anchor. When the anchor weight was lighter, the second object felt heavier than it did with a heavier anchor weight.

The more attention a number gets, the more influence it has. Just consider when you compare two prices directly. The higher the anchor prices of the surrounding products, the higher price you'll be willing to pay.

### 6. People tend to make fair offers, but only when it is possible to track the pricing process. 

If you had to share a cake with a stranger, how big would your piece be? You'd probably make an equal split, because you want to be fair, right?

When we make offers, we anticipate the reaction of our trading partners and adapt our behavior accordingly.

People are willing to pay more if they think the price is fair. Companies therefore make prices fair to ensure people buy their products. For example, if an umbrella firm applied surge pricing to their products during a storm, it'd be unfair and customers would seek out other manufacturers who weren't raising their prices.

In an experiment called Ultimatum Game, people were asked to split $10 with a stranger. They could offer any amount to the other person and keep the rest, as long as the trading partner accepted the offer. If he did not accept, nobody got any money. Most of the participants expected the other person to refuse an unfair split, so they adapted their offer to meet the reaction they expected from the trading partner: They offered a 50/50 split.

But what if people are sure nobody can track the pricing process? Then they tend to get greedy.

People care more about being fair when they know they're being observed or when the customer/partner knows the pricing process.

In the Dictator Game, the "dictator" gets $20 to split with a stranger. In this experiment, 76 percent made an even split. But when the Dictator Game was altered so that the receiver would not know how much money the dictator kept, most of the dictators got selfish: about 60 percent kept all the money they were given.

As the partner could not be angry about an unfair offer — because he was unaware of what the dictator kept — the dictator was not afraid of negative consequences and was more likely to make unfair deals.

### 7. Flat rates and insurance policies allow companies to benefit from our fears. 

Many of us have flat rates for our phones. While it may be a popular option, is it really the most sensible one?

All humans fear loss, and the fear of losing money means we pay more for risk coverage. Scientists found that people regret a loss of $10 more than they delight in a gain of $10.

This phenomenon is called _loss aversion_, and there's an evolutionary explanation for it. Losing an animal's tracks on the hunt while hungry could be life threatening. But when you already have enough meat to feed yourself, choosing not to hunt for more isn't such a risky decision.

Fast forward to the twenty-first century and consider how most of us are afraid of breaking our precious phones. Although it rarely happens, we're willing to pay a lot of insurance for it, _just in case._ Ultimately, we're paying for insurance that we won't use, while the insurer makes an easy profit.

And what about flat rates? Think about it: one big loss is less painful than several small losses. Compare a $90 parking ticket with three $30 ones. You're not just paying $30 three times, you're paying $30 plus the pain of a financial loss three times. By contrast, paying $90 once is just one hit.

We experience every purchase as a loss of money to some extent. Because of this, we're willing to pay for flat rates so that we can feel free to text and chat. A 2009 study showed that cell phone users in San Diego paid a super high average of $3.02 a minute for calls. This number arose because many people had a flat rate, but didn't talk as much as the flat rate plan permitted.

### 8. Anchor prices are used to increase profits. 

Ever bought a watch that was expensive, but still felt you were getting a good deal? Not to burst your bubble, but the store probably sold a higher-priced watch that made yours seem cheaper.

Many of our buying choices change when we see lower- or higher-priced options. We assume the cheapest product is poor quality, while more expensive products seem like a rip-off. Buying products in the middle of the price range seems like the safest choice. But is it?

Often we're so busy comparing prices of products across the board that we forget to accurately assess the quality. Take Budweiser, for example. This premium beer is sold by Anheuser-Busch. In order to gain market shares, Anheuser-Busch introduced the more expensive and higher quality brand Michelob.

Previously, beer drinkers had seen Budweiser as a premium option. But with a new product taking over the top end of the price range, Budweiser became a mid-priced beer. As those seeking to buy the median option switched to Budweiser, Anheuser-Busch gained a considerable number of new customers.

So if you can't sell your product because it's too expensive, there's a better solution than cutting prices. Why not introduce a similar but more expensive product as an anchor?

This is the strategy that the Williams-Sonoma chain used. Their breadmaker made very weak sales with its steep price of $279. But when they added a bigger model for $429, the first breadmaker became the cost-effective option, and its sales nearly doubled! Luxury pricing turns previously expensive products into a bargain.

### 9. Final summary 

The key message in this book:

**Our psychological setup makes it quite a struggle to look at prices rationally. Seemingly irrelevant factors can have huge influences on what we buy and how much we pay for them, and many businesses know just how to take advantage of this. But by remaining aware of our tendencies and the businesses' sales techniques, we'll be better able to think before we buy.**

Actionable advice:

**Use the magic of nine.**

If you want to sell something, make end the price on the number nine. It works like a magic number: When people see nine they think discount, and everyone loves bargains and discounts. So try using the number nine in the next thing you price!

**Suggested** **further** **reading:** ** _The 1% Windfall_** **by Rafi Mohammed**

_The 1% Windfall_ (2010) introduces the often-overlooked strategy of price setting and shows how companies can grow even further by making smart pricing decisions. How can a firm not only survive but also thrive amid stiff market competition or even inflationary periods or a recession? These blinks will help you find the path to attracting the customers you want and keeping those you have.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### William Poundstone

William Poundstone is the author of various nonfiction books, including _Are You Smart Enough to Work at Google?_ and _Fortune's Formula_.

