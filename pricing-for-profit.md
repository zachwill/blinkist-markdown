---
id: 56f12addf77893000700002d
slug: pricing-for-profit-en
published_date: 2016-03-25T00:00:00.000+00:00
author: Peter Hill
title: Pricing For Profit
subtitle: How to Develop a Powerful Pricing Strategy for Your Business
main_color: F3A731
text_color: 8C601C
---

# Pricing For Profit

_How to Develop a Powerful Pricing Strategy for Your Business_

**Peter Hill**

_Pricing For Profit_ (2013) is a practical strategy guide for pricing. These blinks use real world business examples to paint a comprehensive picture of how to optimize your pricing and maximize your profits.

---
### 1. What’s in it for me? Learn to price for profit. 

Maybe you were taught that if you want to make your business grow, you should do everything _but_ increase your prices. Perhaps you hear the voice of some business advisor echoing in your head: "Don't raise it, for crying out loud, it'll just scare your customers away. They'll run right into the arms of your competition."

But what if that weren't true? What if it could in fact be a good idea to ask for more in return for your product or service?

In these blinks, the author briefly introduces you to the four common paths to growing your business, and then goes on to explain how and why bringing the price of your products up a notch could be just the right choice for you.

In these blinks, you'll also discover

  * which price-setting methods to avoid;

  * why price tags are a good idea; and

  * why your focus should be on value.

### 2. A price raise is the simplest and most effective way to increase profits. 

Most business owners are skeptical of the notion that raising prices will boost growth. But in fact, there are four common ways to grow your business and a price hike is a pretty good choice.

The four most popular strategies for growth are increasing your customer base, increasing the value or rate of transactions, achieving greater operational efficiency, and, finally, increasing your prices. So, while many businesspeople think that hiking prices will lead to fewer customers, that's not quite the reality. 

For instance, one survey that compared the reasons customers ceased purchasing a product found that 68 percent of those surveyed stopped because of what's known as _perceived indifference_. This means that they felt they weren't being cared about enough, as evidenced by things like poor customer service. Not just that, but only 10 percent said they stopped purchasing because of higher prices. 

So, why wouldn't you increase your prices when it's such a simple way to grow? 

After all, even making small price increases can significantly impact your company's growth. For instance, investing in greater marketing efforts takes longer and costs more than a simple price raise. That's because marketing investment might require lots of new print materials, staff or technological purchases. 

On the other hand, just a minor price increase can mean huge benefits at a fraction of the cost. For example, say you're selling 5,060 used phones a year at an average of $198.45 per phone. If you raise your price per phone by just 2 percent, or $3.969, your annual profit will rise from $1,004,157 to $1,024,240. That's about $20,000 added profit!

That being said, there are some important considerations to make when setting a price, which is exactly what we'll explore next.

### 3. Setting prices means understanding your product’s value to customers. 

When it comes to setting prices, there are a variety of strategies, but one factor is key: value to the customer. The most common methods for pricing are flawed because they ignore this essential aspect.

For instance, _cost plus pricing_ works by taking costs and adding a percentage to arrive at the selling price. However, this means that in reality there's no connection between what it costs to produce something and what it sells for. 

So, say it costs $15 to make something that you sell for $20. You find a way to reduce your costs to $13 and therefore decrease your selling price to $18. The issue with this approach is that your customers still value the product at $20, which they've been willing to pay hitherto. 

Another equally flawed approach is to _price below your competitors_. The problem with this strategy is that you can't know how much value consumers place on your competitors' products. It could be that they value your goods more and are willing to pay a price that reflects that. So, both these strategies miss the point, which is value to the customer. 

A better strategy is to base pricing on what a product is worth to consumers. That's because customers don't ask themselves what a product costs to make; they ask how much value it brings to them. So, when setting a price you have to consider the added value of your product and how it makes the lives of your customers easier. 

Say you're selling a hair dryer that costs a little more than most of the others on the market, but comes with a five-year guarantee. While the hair dryer might cost the same to make as a less costly one, your long-term guarantee is an added value and will be perceived as such by customers, so you can charge more than the competition. 

But this raises a bigger question: how exactly does value work?

### 4. Customers decide what to buy based on the relationship of price to value. 

How do you choose which TV to buy when you're overwhelmed by the selection at your local electronics store? Well, most people make purchase decisions based on how much they value any given product. 

For instance, take a quality pair of headphones that sell for $100. If you value them at $100 because of your affinity to the brand, you will consider the price fair. If you valued the same headphones at $130 you'd consider $100 a bargain, and if you valued them at $70 you probably wouldn't buy them, opting instead for a cheaper model or one with features you value more. 

So, to be able to charge a given price, you first have to make sure your customers know why your product is valuable. One way to do this is by talking to customers about it. You can simply give them a price range and discuss the different options.

For instance, if you offer a service like hairdressing, you might give customers a price range between $50 and $150 depending on different options. By showing your customers the costs of extras, you communicate the added value of these options. 

That means a basic haircut could cost $50, but additional hair care and styling would run closer to $120. Given these options, your client will choose what they're willing to pay for according to the value they place on different services.

### 5. Proper presentation of prices is essential to winning customers. 

When you're focused on setting the right prices it can be easy to forget how you're going to present them, but this consideration is crucial. In fact, failing to think about how your prices are presented can even cost you customers. 

That's because if your prices aren't clear, potential customers might not make a purchase. This makes sense when you consider the risks customers face when buying something for the first time. Doing so is essentially a leap in the dark. For this reason, it's essential that your prices be clearly stated. 

When you walk into a high-end boutique you see lots of beautiful garments with no prices on display. So to get the price of an item you have to check the tag inside it, something you might not do because you don't want to come across as cheap.

When faced with such a scenario, plenty of people just walk out of the store. 

So, it's important that prices are clearly displayed to potential customers. For example, you can make your prices stick out by writing "sale" in a different color. 

Another strategy to help your customers orient themselves is to play around with word choice. Displaying an item's price is a good start, but it doesn't communicate its value. 

If you're in the grocery store and see two bins of oranges with different prices and no explanatory information, you won't know why you should pay more for one than the other. So it's important to be specific.

You can use a sign that says something like "today's special price" to communicate the bargain value of an item and compel customers to purchase it. If you passed by the same bins of oranges and noticed a sign above the cheaper ones that read "today's special price," you'd be more likely to purchase them.

### 6. Final summary 

The key message in this book:

**Contrary to popular belief, raising prices is the simplest and most effective profit-boosting tool. Arriving at a price scheme that is right for you and your business means understanding the value of your product to customers, and communicating it well.**

Actionable advice:

**Use the number nine.**

When setting prices, the number nine can be extremely helpful. That's because the number signals a value just below a threshold. When consumers see a price with a nine at the end they consider it a bargain. Whether it's 0.99, 1.79 or 99.99, try out this strategy yourself and see how your customers respond. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Priceless_** **by William Poundstone**

_Priceless_ (2010) explores the psychological reasons behind the value and price we give to things. Through numerous experiments and case studies in pricing, the author explains how prices influence our purchasing decision and exposes companies that use pricing to increase profit.
---

### Peter Hill

Peter Hill is an accountant with over 30 years of experience, a popular speaker and a host of seminars on pricing and profit for clients like Lloyds and HSBC.

