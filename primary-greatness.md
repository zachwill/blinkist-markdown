---
id: 5a9b350eb238e1000791093a
slug: primary-greatness-en
published_date: 2018-03-06T00:00:00.000+00:00
author: Stephen Covey
title: Primary Greatness
subtitle: The 12 Levers of Success
main_color: 31BAF5
text_color: 1D6D8F
---

# Primary Greatness

_The 12 Levers of Success_

**Stephen Covey**

_Primary Greatness_ (2015) explains that anyone can live a fulfilling life. It's as simple as prioritizing the things that are truly important. Instead of trying to appear successful by accumulating status, fame and money, you should focus on finding true success by developing your inner character — a task that the 12 levers of success make simple.

---
### 1. What’s in it for me? Find success that isn’t fleeting. 

We often mistake symbols for success. That man is driving a Maserati: he must have found success. That woman is wearing Louboutins: she must have found success. But this take is entirely superficial.

As you'll learn in these blinks, actual success can't be seen or measured in material trappings. It's an inner quality, something that anyone can build — no fancy cars or red-soled shoes necessary.

So how can you build up these vital internal qualities? You must listen to the advice of self-development behemoth, Stephen Covey, and concentrate on working on the 12 levers of success. These blinks explain what these levers are and how you can utilize them yourself. Follow this advice and it won't be long before you're enjoying a fulfilling and peace-filled life that's driven by primary greatness.

In these blinks, you'll also find out

  * why importance trumps urgency;

  * how to find your purpose; and

  * why you should imagine the worst.

### 2. Inner character is the driver of true success, and it can be developed. 

What does success look like? Is it Armani suits, Rolex watches, cherry-red Ferraris and a daily glass of champagne on the deck of a Long Island beach house? Sure, these might be some of the symbols of wealth — but they're certainly not signs of success.

True success can't be seen. It comes from within and is generated by what the author calls _primary greatness_, which naturally comes to those who embody admirable qualities such as integrity, honor, persistence, selflessness and a commitment to a purpose that transcends the personal.

S _econdary greatness_, in contrast, _can_ be seen. It's the appearance of success — the money and the fame and all the attendant status symbols, from cars to suits to houses.

However, whereas primary greatness leads to fulfillment and inner peace, secondary greatness dead-ends at fleeting pleasures and financial security.

Now, if you haven't spent your life fostering stalwart inner attributes like honor and selflessness, don't fret. It's been scientifically proven that such traits can be developed.

In Michigan, back in 1965, the Perry Preschool Project began following the lives of 123 inner-city preschoolers. They were divided into two groups — test and control — and the test group learned, at an early age, to delay gratification by engaging in boring and low-reward tasks.

Fast-forward 50 years, and those in the test group were much more successful. Compared with those in the control group, more of the test-group members had graduated and found employment, and fewer had been arrested. All this because, in preschool, they'd built some primary greatness by developing attributes like integrity and persistence.

So what can you do to develop your character?

Well, as you'll soon learn, there are _12 levers of success_, a set of principles that will serve as the foundation for your primary greatness. These levers are integrity, contribution, priority, personal sacrifice, service, reciprocity, diversity, responsibility, loyalty, learning, teaching and renewal.

These 12 levers are the result of the author's extensive research. He's taught and learned from thousands of people worldwide, and distilled his findings into the essentials for primary greatness.

### 3. Self-affirmations and an abundance mind-set will set the stage for primary greatness. 

Inner peace, a sense of fulfillment, long-lasting prosperity — the benefits of primary greatness are myriad and magnificent. But how exactly do you move toward acquiring it? Well, there are a few handy techniques that will prep your brain and make it more receptive.

The first is to use self-affirmations.

Self-affirmations are positive messages delivered in the first person and the present tense from yourself, to yourself. They affirm the life you want to live and nudge you toward making your aspirations into a reality.

Let's say you're a procrastinator, and you want to get better at taking the reins and getting things done. Your self-affirmation might be something like, "It's gratifying to take control and guide my own life by proactively planning my time and following through on my plans."

Once you've cooked up some suitable self-affirmations, you can improve their efficacy by doing two things:

First, relax. When you're relaxed, your brain waves slow down, making you more receptive to sensory input — and you want to be as receptive as possible when reciting your affirmations.

Second, recite your affirmations every day. The more you say them, the more likely they are to manifest.

The next technique is about your general outlook on life, and it requires you to shift your mind-set entirely. Instead of obsessing about scarcity, you've got to start focusing on abundance _._

Most of us have a _scarcity mind-set_ — that is, we view the world through a win-lose lens, believing that more for others means less for us, and neglecting opportunities for cooperation and shared benefit. 

But this is often detrimental to success. Just think of a ball hog on a basketball team. By taking too many shots and refusing to pass, such a player not only robs his teammates of the chance to score, he also lowers morale, flouts the code of fair play and, more often than not, costs his team the game.

Now, if that player shifted to an _abundance mind-set_ — where the world is viewed through a win-win lens, and there's always enough to go around — he might take a huddle with his teammates and realize that, by passing the ball and allowing his fellow players to pose a threat, they can weaken the defense and win the game.

### 4. Integrity, the foundation of primary greatness, aligns your values, beliefs and actions. 

_Integrity_. It's a word that gets thrown around a lot, but, if asked, would you be able to define it succinctly? Well, since it's the first of the 12 levers of success and the foundation of primary greatness, it's important to have a clear definition.

So the author broke it down into its component parts, and he found that integrity is a combination of two traits: _humility_ and _courage_.

If you are humble enough to recognize where you can improve and courageous enough to undertake the work of improvement, you'll be well on your way to becoming a person of integrity.

To get a sense of what integrity looks like in action, let's take a look at a situation from the author's life. Once, a close friend of the author's hurt him deeply — but this friend realized what he'd done and gave a heartfelt apology. The author, impressed, asked him how he'd managed it, and the friend said that, after an in-depth conversation with himself, he'd realized he had two options: listen to his ego and give a lukewarm apology or listen to his conscience and give a meaningful one. 

It took humility to recognize that he should be penitent and courage to act on this recognition — and it was this combination that resulted in the friend's displaying true integrity. This act of integrity had a very powerful effect, inspiring the author not only to accept the apology, but to forgive the initial trespass.

So, having integrity will positively affect those around you; but it will also shape you in beneficial ways — namely, by aligning your values, beliefs and actions. This alignment is called _congruence_.

If you've got congruence, you'll find it much easier to gain the trust of others. This is, in part, because you'll simply seem more authentic. By aligning your actions with your beliefs and values, you'll shed all hidden motives and secret agendas, and walking your talk will simply become second nature.

### 5. Foster the levers of contribution and prioritization by finding your purpose. 

People need purpose. Whether it's writing novels or raising a family, a purpose imbues life with meaning, giving its possessor a sense of direction and duty. And yet only between five and ten percent of people take the time to define their purpose.

Purpose is crucial because, without it, you won't know how to contribute to the world, and _contribution_ is the second lever of success. To identify your purpose, start by asking yourself three questions: What is the world lacking? What do I excel at? And how could I contribute by doing something I like?

Keep in mind that your purpose might be right in front of your nose. Often, we aim too high and fail to notice how we can contribute right here, right now.

For example, consider the protagonist of the film _Mr. Holland's Opus_. He dreams of becoming a brilliant composer, but he's forced to work as a music teacher, a job he initially dislikes. But the years go by and he begins to love his students. In the end, he doesn't become a famous composer. Instead, he pours his passion into teaching and contributes by helping thousands of students.

So remain open to the possibility that, without realizing it, you may have already found your purpose — all you have to do is identify it.

Once you've figured out how to contribute to the world, you can focus on the third lever of success: _prioritization_.

Prioritizing is largely a matter of differentiating between important but non-urgent tasks and urgent but non-important tasks. Important, non-urgent tasks take the bigger picture into account, and they should always be prioritized.

Imagine you're a doctor and, while performing heart surgery, a nurse runs in and says there's an urgent call for you. Well, it doesn't matter _how_ urgent the call is, because, in the big picture, the patient's survival is of the utmost importance. Importance always trumps urgency, so you wouldn't take the call.

This example also demonstrates how a higher purpose — in this case, the purpose of saving the patient's life — naturally leads to prioritization of what's most important.

### 6. The levers of sacrifice and service encourage personal connections, which are essential to success. 

Maybe you left some dishes in the sink, assuming your partner would wash them. Maybe you "accidentally" cut someone off in traffic. Or maybe you just didn't take the time to call your mother.

Yes, being selfish is normal. However, if you want to form meaningful and productive relationships, you've got to fight those selfish tendencies and nourish the fourth lever of success — _sacrifice_.

Sacrifice means putting aside your ego and focusing on the greater good. It's about emphasizing the outcome of collaborative work, as opposed to claiming credit for contributing the most, and, like integrity, it requires humility.

For example, a company's president and vice president once took a business trip together. On the second day of the trip, the vice president awoke to a surprising sight: the president was shining his shoes.

He was startled to find the president engaged in this humble activity; it seemed the opposite of presidential. However, it also helped the vice president see the president's human side, and, in the end, it brought the two men closer together.

Sacrificing your ego isn't terribly difficult — and it can go a long way toward creating a lasting bond.

In that regard, it's similar to _service_, which is the fifth lever of success. Service is about doing things for other people, and it's the perfect way to strengthen the relationships that sacrifice helped you create.

One way to serve is to speak as though you're only addressing one person, regardless of how large your audience is. This will show others that you're engaged with them, that you're doing them the service of being present, and, in turn, it will help them engage with you, as the following anecdote demonstrates:

The star of a play was once having trouble capturing the attention of his audience. Luckily, a friend of his had the presence of mind to abandon his seat, approach the stage and mouth the words "talk to me." The actor understood and began addressing the audience as though it were a pal, a person with real feelings and thoughts. Just like that, the audience recognized this service and reengaged.

> _"I don't care how much you know until I know how much you care."_

### 7. The levers of reciprocity and diversity result in productive relationships. 

Now that you've got an idea how to establish and strengthen relationships, let's take a look at how to make them productive. Interestingly enough, one of the best ways to build a beneficial relationship with others is to improve how secure you feel about yourself.

A self-assured character is, in fact, the foundation of the next two levers of success — _reciprocity_ and _diversity_.

Self-assurance — that is, a deep feeling of inner security — enables you to remain open to unfamiliar ideas and new opinions. And this openness will imbue you with empathy, something without which reciprocal relationships can't exist. 

If you're insecure, you'll be closed off. Different ideas and new perspectives will feel threatening, because to those who lack self-assurance, a threat to a belief system feels like a threat to personal identity.

Only after establishing some solid self-assurance should you start focusing on _reciprocity_, the sixth lever of success. But, when you do activate this lever, there are two things to focus on:

First, concentrate on bonding. The firmer the social bond between two people, the less likely they'll be to act selfishly.

Second, keep communication channels open. Sharing and collaborating on problems, and discussing possible solutions, will increase the reciprocity of any relationship.

Self-assurance is also the foundation of the seventh lever of success, _diversity_, which is all about encouraging a range of opinions, skills, personalities and ideas. These things drive innovation and positivity — but they'll be under threat if you and your team aren't self-assured.

Remember, insecure people think difference is threatening, so, in order to oust limited thinking, you've got to encourage people to embrace difference and openly express themselves. In other words, you've got to help them feel self-assured.

The author points out that, when there's no diversity of opinion within a team, there's essentially only one opinion — and that's bad for creativity and innovation. New and diverse ideas will breed new and diverse projects and products; if you welcome difference, you're bound to make one.

### 8. Stay accountable with the levers of loyalty and responsibility. 

We've all experienced it. There you are, at the bar with friends or eating lunch with colleagues, and somebody starts bad-mouthing an absent pal or coworker. Such behavior is common, but it's also horribly toxic.

The eighth lever, _loyalty_, combats such toxicity by holding you back from ever disrespecting other people, regardless of whether they're present or absent.

Loyalty and respect are important because they remove the negative interactions that often lead to negative labeling, which has a very tangible and toxic effect.

Let's say a colleague of yours insults you and, in consequence, you label her in your mind as "hateful." Even if you never let on that you find her hateful, she will, on some level of consciousness, pick up on how you actually feel, and her actions will begin to align with your expectations.

Just as you should avoid labeling people, you should never say anything about anyone that you wouldn't want to say to their face. Further, if you're ever present when people are talking negatively about someone else, you should combat the negative labeling by mentioning a few positive things about the absent person. Speaking up for the voiceless is loyalty at its finest.

Of course, we can't always be perfect. Whether it's gossiping or some other slip up, people make mistakes. And that's where the ninth lever, _responsibility_, comes in.

Truly taking responsibility entails fully acknowledging your mistakes and offering an apology that isn't hedged around with excuses, defensiveness or explanations. If you really want to move past a mistake, you've got to take the full blame; that's the only way to remove negative labels and receive forgiveness

And remember: once you're forgiven, be sure that your actions chime with the apology you made. This will show that you were sincere, and that your apology wasn't merely an empty expedient.

> _"The ultimate test of primary greatness is to be loyal to people who are absent."_

### 9. The lever of teaching complements and reinforces the lever of learning. 

Imagine that, for the next ten years, you learn nothing new. Well, the truth is that, if you stopped learning, you'd become irrelevant: your skills and knowledge would get you nowhere in the world of ten years hence. So how can you keep growing and stay up-to-date?

The best way is to keep _learning_, which is the tenth lever of success.

Horst Schulze, the cofounder of the Ritz-Carlton hotel chain, knows this well. Despite the cost, Schulze provides a daily training program for employees, because he firmly believes in the importance of learning. By helping his employees grow, Schulze also grows his company — because a skilled workforce is the backbone of his chain.

On a more personal level, there are many things you can do to keep learning.

You could, for instance, design a personal curriculum. If you're interested in business, read _Harvard Business Review_ or _Fortune_ magazine. There are academic journals for every field, so just identify your interests and dive in. And, to broaden your horizons and make you wiser, don't neglect the classics. A little Shakespeare before bed can only do good.

You could also attend your very own online university. Watch TED talks; enroll in one of the many massive open online courses (MOOCs); make use of Khan Academy.

And if you really want to learn something, be sure to use the eleventh lever of success — _teaching_.

As the author learned from Dr. Walter Gong, a San Jose State University professor, teaching is the best way to learn. During dinner, Dr. Gong always had his children teach him what they'd learned that day in school — and all three of them went on to earn PhDs from a top university.

Teaching works so well because, when you know you'll have to make someone else understand the material, you're forced to really learn it.

That's why every student should learn like a teacher!

### 10. Balance your physical, mental and social health to achieve the last lever – renewal. 

So we've been focusing a lot on the component parts of primary greatness. But if you really want to a serve a higher purpose, and live a fulfilled and peaceful life, you've got to take care of yourself. And that's where the twelfth lever, _renewal_, comes in.

Self-care is a matter of balancing your mental, physical and social health.

One way to inspire physical- care habits is to imagine you've _already_ had a major health setback — that, say, you've had a heart attack or undergone surgery for cancer. Just imagining such a health crisis should motivate you to break any unhealthy habits you might have, be it overwork or smoking cigarettes.

To improve your mental health, imagine that, in three years' time, all your professional knowledge will have been made obsolete by technological advances. This should spur you to prioritize learning and the advancement of your skills.

To keep yourself socially healthy, imagine that everyone in your life is always listening to the things you say about them. This should keep you from saying regrettable things and being judgmental. This doesn't mean you should be uncritical — just that you should be kind in your criticism, and not say things about someone that you wouldn't say to them.

If you regularly engage with these hypothetical situations, you'll be able to adjust your mind-set.

It's much more effective to put energy toward your physical, mental and social health than to try to pinpoint and alter every single negative behavior, since a more general approach influences multiple behaviors at the same time.

Physical, mental and social health are also closely connected, and focusing on all of them is the best, most holistic, form of self-care.

For instance, if you neglect your skills and knowledge, you may get fired from your job, which might lead to your feeling like a victim and blaming others. This would affect your social relationships, leading to stress, which would compromise your physical health. 

A holistic approach to self-care is the best defense against burnout and the negative chain reaction that neglect of one area can cause.

### 11. Final summary 

The key message in this book:

**A life centered on making connections with and in the service of others is far more rewarding than a life spent hunting for self-serving pleasures such as status and money. And the best way to find real fulfillment and inner peace is to establish your primary greatness. You can do this with the help of the 12 levers of success: integrity, contribution, priority, personal sacrifice, service, reciprocity, diversity, responsibility, loyalty, learning, teaching and renewal.**

Actionable advice:

**Keep a "journey to primary greatness" journal.**

Keep a journal to track your progress toward primary greatness and use it to collect your learnings along the way. Jot down your higher purpose and all of your self-affirmations. Use it to record all the situations where you have chosen secondary over primary greatness _._ This will help grow your self-awareness as you travel toward s your ultimate goals.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The 7 Habits of Highly Effective People_** **by Stephen R. Covey**

_The Seven Habits of Highly Effective People_ (1989) is the enormously influential self-help phenomenon that can teach you the principles of effectiveness. Once you make these principles into habits, you'll be well on your way to more success, both in your personal and your professional life.
---

### Stephen Covey

Stephen Covey is the author of the internationally best-selling book _The 7 Habits of Highly Effective People_, which has sold over 25 million copies. Because of his work toward developing business and education, he was recognized as one of _Time_ magazine's 25 most influential Americans.

