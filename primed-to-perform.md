---
id: 584465c8a6ba9500046bed78
slug: primed-to-perform-en
published_date: 2016-12-07T00:00:00.000+00:00
author: Neel Doshi and Lindsay McGregor
title: Primed to Perform
subtitle: How to Build the Highest Performing Cultures Through the Science of Total Motivation
main_color: C32734
text_color: C32734
---

# Primed to Perform

_How to Build the Highest Performing Cultures Through the Science of Total Motivation_

**Neel Doshi and Lindsay McGregor**

_Primed to Perform_ (2015) is a guide to motivation. These blinks will show you how to light a fire under your employees and colleagues — that is, motivate them to perform — in a way that brings meaningful results and is long-lasting. You'll learn that in doing so, you'll build a corporate culture that values high performance, which in turn will help you fulfill your goals.

---
### 1. What’s in it for me? Prime your employees to perform to the best of their abilities. 

What motivates you to work? Cold cash? Great perks? Well, would you work in a job you totally hated, for example, if you were compensated very well? Probably not.

So what exactly motivates you — and in turn, everyone else who suffers through the 9-to-5?

Business leaders know that if you want to create a high-performing culture within an organization, you have to discover what motivates your teams. Because if you can't tap into the ideas or feelings that inspire your employees, they won't perform — and your company won't succeed.

These blinks draw on more than 20 years of business know-how, explaining the various shades of human motivation, how it works and how you can harness its power to build a successful company.

In these blinks, you'll learn

  * why you should inspire your employees with the motive of play;

  * why adaptive goals beat tactical goals; and

  * why Toyota decided to involve employees in the entire process of building cars.

### 2. Understanding what motivates people can help you form a high-performance corporate culture. 

What gets you out of bed every morning, ready for another productive day at the office?

When you're working to build a high-performance culture at your company, this should be the first question on your mind.

In fact, motivation can be broken down into three categories: _play, purpose_ and _potential._

Play motivates you to take an action simply because it is fun to do so. You might be curious and enjoy experimenting, or simply eager to learn or adapt.

This is why people spend time enjoying a hobby, solving crossword puzzles or listening to music. But in turn, if you're trying to do something difficult like losing weight, play motivation can help; you might enjoy trying new recipes or researching vegetarian restaurants to reach your goal.

Purpose motivates you to do something because you value the outcome and impact of your actions, even though the process itself may not be enjoyable. The long shifts and stressful days of being a nurse may be tough, for example, but deep down you value caring for people.

Potential motivates you when you value the indirect outcomes of a certain activity. In essence, you think that your actions will lead to something important, such as fulfilling a long-term goal.

If you were a paralegal, for example, you might not enjoy the day-to-day work, but you see the job as a necessary step toward applying to law school.

So now you know the three types of motivation. The more closely these three are connected to the work you do, the more strongly they'll influence your overall performance.

Of the three, play is the most powerful motivator as it's closest to the work itself. This means the more you see your work as play, the better you'll perform!

But don't ignore purpose and potential — they're still strong motivators!

### 3. Emotional and economic pressures keep people working, but for all the wrong reasons. 

Now that you know how people are motivated, it might seem like the more of these motives you provide for your team, the more dedicated they'll be. However, that's not quite how it works.

When motives aren't directly connected to work, they _reduce_ performance. More specifically, _emotional pressure, economic pressure_ and _inertia_ are indirect motivators that can have this effect.

Emotional pressure happens when emotions related to your self-perception or judgment of others cause you to take certain actions. Such emotions might be guilt, disappointment or shame.

For instance, you might practice piano because you're scared of disappointing your mother, or stay at a job you dislike simply because your position and title boost your self-confidence.

Economic pressure motivates by encouraging you to seek rewards or avoid punishment. Many workers clock overtime hours to earn a bonus, get a promotion or simply keep a job.

Inertia motivates you to do something today simply because you did it yesterday. In cases of inertia, it's difficult to identify why you're even doing the work in question.

An executive might keep working late because he can't think of a reason to go home; a college student might keep studying just because it's the path he's already begun.

Such motives can be problematic. The more important they are for your reasons for working, the more damage they can cause.

Emotional pressure is the weakest of indirect motives; economic pressure is stronger; and inertia is the most debilitating to overall performance. Inertia makes you get work done, but deprives you of knowing _why_ you do it, day in and day out.

### 4. Real success depends on balancing the multiple types of performance metrics. 

Performance isn't just about accomplishing tasks effectively. In this rapidly changing business world, performance is also measured in a company's ability to adapt. This is called _adaptive performance_.

This describes a company's flexibility, or its ability to break with a set plan to seize new opportunities, markets or other surprises. But how does your company become adaptive?

A working culture that encourages creativity and problem solving will adapt to change effortlessly. Adaptive performance can be boosted through direct motives of play, purpose and potential.

Professor Adam Grant conducted a study in which he asked students to generate ideas for how a band could earn income. One group, primed with play and purpose motives, was told that the exercise would be fun, and that band members need money to support their families.

Another group was told the exercise would be boring and that the band was financially secure and just playing music for kicks. So what happened? The first group produced ideas that were considered 30 percent more creative by independent music experts.

Adaptive performance isn't all that matters. _Tactical performance_ describes a person's ability to execute a plan. This works by helping you focus your energy on essential goals, such as increasing sales.

Tactical and adaptive performance are complementary, and both are necessary for success. But while both forces are crucial to overall performance, companies often focus just on the tactical, as it's easier to measure. As a result, adaptive performance tends to fall by the wayside.

It's easy to chart the rise and fall of sales, for example. But it's harder to measure whether the company entered a new market quickly enough. Even so, companies need to pay more attention to their adaptive performance. It's time to change!

Managing your company's culture is just like managing its finances — it's an ongoing process that requires careful diagnostics. Luckily there's a tool that can help you measure your company's culture and its performance.

It's called ToMo — and you're about to learn all about it!

### 5. Total Motivation, or ToMo, is a useful tool to measure a company’s adaptive performance. 

Are you ready to boost your company's adaptive performance?

This is where _Total Motivation_ or _ToMo_ comes in. This tool measures a company's adaptability by using the six motives we've reviewed in previous blinks.

Here's how ToMo works.

First, you need to calculate your current ToMo by analyzing the six motives. The authors have designed several methods for doing so. The simplest method consists of six statements which responders can agree or disagree with, such as "I continue to work at my current job because the work itself is fun to do" and "I continue to work at my current job because without this job I would be worried I couldn't reach my financial objectives."

Next, you use this data to identify the areas where your company's ToMo can be improved, marking the areas in which adaptive performance is most essential. These areas tend to be those with direct customer engagement, that influence product quality or that require creativity.

Once you've done this, you choose the optimal individualized strategy for your company's situation. To do so, take a look at your research and determine how you can boost the three positive motives while reducing indirect motives in your areas of focus.

Next, you set your desired ToMo to start building a company culture that motivates your employees using the ideas you identified in the previous step.

Here's a benchmark metric that might help you. Research has found that companies renowned for their great corporate culture tend to have a ToMo score — which ranges from -100 to 100 — about 15 points higher than the industry standard.

Finally, generate a plan to implement your desired company culture in areas that produce specific outcomes. After all, you've got limited investment capital, and it's essential that the money you spend on building a corporate culture increases your economic performance.

ToMo is strongly correlated to customer experience, an area that often has a direct connection to profits, customer retention rate and cross sales. One study found that the difference in revenue generated by a salesperson with positive ToMo and another with negative ToMo was 28 percent!

### 6. Building a high-performance culture starts at the top. A leader needs to encourage positive ToMo. 

Increasing your company's ToMo can help employees perform better and become more adaptive. But to do this effectively, you need leaders who demonstrate and encourage such behavior by always pushing the direct motives of play, purpose and potential.

How can a company leader do this?

To encourage play, leaders should inspire curiosity and experimentation within the workforce. By doing so, they can pique employees' interest and get them involved in work. Employees, freed from rigid instructions or heavy-handed codes of conduct, will be happy to work on projects that excite them.

To stimulate purpose, leaders should emphasize shared values and common goals among employees. Be sure to explain the positive outcomes your company will inspire and how customers will benefit.

To inspire potential, leaders need to help connect employee work to personal goals and needs. Focus on employee strengths, and on teaching and coaching them; treat everyone as a valuable individual. The idea is that a leader shows employees that their investment in the company is an investment in themselves.

But this isn't all a good leader needs to do. Leaders also need to discourage indirect motives, such as emotional and economic pressures. It's up to a leader to assure employees that the company has set reasonable goals that they can feel confident pursuing. Setting reasonable goals can remove unnecessary emotional pressure, allowing employees to focus on more positive motivation. 

A leader also needs to translate tactical goals into adaptive ones. For example, if a tactical goal is to increase market share by 30 percent, the adaptive equivalent would be to learn five new strategies for boosting market share in general.

This works. In a study, one group of students was given a tactical goal and another an adaptive goal. The tactical group lost eight percent of its market share while the adaptive group grew its business by 59 percent!

### 7. The way a job is designed is the most powerful and often overlooked source of ToMo. 

Most jobs are based on tactical performance, meaning people have specific instructions on how to perform. It's rare for a position to be centered on inspiring adaptive performance and total motivation. This is a shame; here's how to institute change.

To inspire an employee to deliver an adaptive performance, his job description needs to show him how he can make an impact, inspire him to have fun in his work and help him prioritize the work by himself.

So the first thing you should do is ensure employees witness the effects of their work.

Accomplishing this is as easy as familiarizing an employee with the entire process of his department. In other words, show him how his actions, as well as those of the company, produce an output. As a result, the employee will self-identify ways to improve his performance.

Toyota, for instance, rotates employee jobs in its factories so that everyone understands the car-building process from start to finish. Understanding this helps employees improve their performance and shows them how their work interacts with and bolsters that of others.

As a leader, it's also essential to encourage the play motive. You can do so by opening up opportunities for employees to generate new, improved ideas. Clerks at Whole Foods Market, a grocery chain, are allotted time to meet local producers, speak with customers and visit competing stores so they can generate new ideas for the company.

You should always make sure employees can prioritize their tasks. Prioritizing isn't just about deciding when to do something — it's about deciding which tasks require the approval of a higher-up.

This is essential. If employees can prioritize on their own, they'll be more likely to try out new ideas, which can be rapidly tested. On the other hand, if employees can't see the line between their realm of authority and actions in need of approval, they'll be unlikely to act on their ideas.

### 8. Common identity and career advancements put the finishing touches on your culture of ToMo. 

There's no single source of Total Motivation. Rather, it's a combination of aspects that includes a company's attitude toward its visions and values, as well as its promotion hierarchy.

A good strategy is to build a _common identity_ within your company that changes jobs into _callings_.

A common identity is formed when a group holds a _common objective,_ a _code of behavior_ and a _heritage._

A common objective should bring your team together, inspire them and explain a group's purpose. A behavioral code should empower people to solve problems and make decisions. Instead of listing values like honesty or integrity, your code should define exactly how decisions are made. For instance, it might answer the question, "Should customer experience be prioritized over sales targets?"

Your company's heritage is about living by company values. This is done by sharing real examples in which the company lived up to its identity.

But building a common identity isn't all that's necessary. You'll also need to avoid tactical performance increases by individualizing promotion ladders. For instance, some companies think that competition among employees is good, but in such situations, charting a career path can look like a tournament with employees competing for slots.

Studies show that when people are competing for promotions they seek out easy, low-risk tasks that make them look busy. They avoid problem-solving and opportunities for creativity. As a result, people focus more on landing a promotion than on producing results, choosing tactical performance over adaptive performance.

This can be avoided through individualized career ladders. To implement such a strategy, it's essential to not automatically reward top performers with managerial positions.

Instead, the managerial ladder should be reserved for those who enjoy coaching; the expert ladder for those who like mastering technical skills; and the customer ladder for people who love working with clients and care about their satisfaction.

> _"If you want to build a ship, don't drum up people to collect wood and don't assign them tasks and work, but rather teach them to long for the endless immensity of the sea."_ — Antoine de Sain-Exupéry

### 9. Final summary 

The key message in this book:

**The ability of workers to perform has everything to do with their reasons for working. Effective leaders need to help employees excel by showing them the pleasure, purpose and potential of their work.**

Actionable advice:

**Avoid the reward and punishment approach to motivation.**

To decrease the cobra population of Delhi in the 1800s, the government said it would pay a bounty for dead cobras. The policy, however, led to the creation of cobra farms, where people would raise snakes then kill them, to collect the bounty! When the government caught on, the farmers released the snakes en masse, flooding the land with cobras — and making the initial problem even worse. This story illustrates why as a leader, you should avoid the reward and punishment approach to motivation. Doing so neither cuts out economic pressures nor does it avoid _maladaptive performance_, when people seek out the shortest route to winning an award or avoiding punishment.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Motivation Manifesto_** **by Brendon Burchard**

_The Motivation Manifesto_ (2014) explains the fundamental driving forces of human nature and how these either help us realize or keep us from our life's goals. In a few easy steps, you'll learn how to inspire and increase your own levels of motivation to live a happier life.
---

### Neel Doshi and Lindsay McGregor

Neel Doshi and Lindsay McGregor have more than 20 years of experience helping companies develop high-performance cultures. They are also the co-founders of Vega Factor, a company that helps firms create high-performing, adaptive cultures.

