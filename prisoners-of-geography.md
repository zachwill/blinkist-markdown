---
id: 5a817f43b238e100072f7092
slug: prisoners-of-geography-en
published_date: 2018-02-16T00:00:00.000+00:00
author: Tim Marshall
title: Prisoners of Geography
subtitle: Ten Maps That Tell You Everything You Need to Know About Global Politics
main_color: D87835
text_color: A65C29
---

# Prisoners of Geography

_Ten Maps That Tell You Everything You Need to Know About Global Politics_

**Tim Marshall**

_Prisoners of Geography_ (2015) explains how, all over the world, political decision making is greatly influenced by geography. Even choices that may appear arbitrary are in fact driven by the Earth's mountains, valleys, rivers and seas.

---
### 1. What’s in it for me? Discover how geography determines the world’s balance of power. 

Did you know that the land you stand on has shaped the society you're living in? If this sounds a tad abstract, think of it this way: the geographic features and resources around you have strongly influenced your country's economy, as well as how it has fared in the many wars that have been fought throughout history.

These blinks look at six of the most fascinating and influential geographies around the globe. More often than you may think, the decisions made by world leaders have a lot to do with the lay of the land. Sometimes, these leaders and the people they represent turn out to be prisoners of geography.

In these blinks, you'll find out

  * about the one part of the world Vladimir Putin obsesses over the most;

  * why China refuses to let go of Tibet; and

  * why the African continent has struggled to capitalize on lucrative trade routes.

### 2. Russia is an aggressive presence in the Baltics because it fears invasion from the West. 

There's no denying the fact that Russia is enormous. Covering a sprawling 6 million square miles and containing eleven different time zones, Russia is by far the world's biggest country.

So what keeps Vladimir Putin, the president of Russia, tossing and turning at night? It's one particular stretch of land that somewhat resembles a slice of pizza.

Beginning in Poland, this particular pizza-slice-shaped wedge extends southeast to the foot of the Ural Mountain range, and northeast to Russia's capital city of Moscow.

What keeps Putin particularly worried is that this area of land is part of what's called the _North European Plain_, which stretches from France across Belgium, the Netherlands, Northern Germany, Poland and ends at the Russian Urals. As the name suggests, this area is flat and makes the European gateway to Russia vulnerable and difficult to defend.

Any country within the North European Plain could conceivably send an army across the flatlands and directly into Moscow. As Putin knows all too well, this is exactly what has happened to Russia throughout its history.

During both world wars, this is the path the Germans took in their military campaigns. But that's not all — since 1812, invaders from the Northern European Plain have attacked Russia an average of once every 33 years!

For generations now, Russia's strategy for neutralizing the threat from the North European Plain has been to control Poland and all the Baltic states that lie between it and Russia, which include Lithuania, Latvia, Estonia and Belarus.

These are the nations that make up the meat of that pizza slice, so to speak. While the wedge stretches 2,000 miles from north to south at its easternmost section, it's only 300 miles wide around Poland and the Baltic states. If Russia can station a strong defensive front here, it can more easily hold off potential Western invaders.

Unfortunately, this means the Baltic states are likely to continue having a rough go of it.

### 3. China’s fears of Indian invasion and water shortages keep their grip on Tibet strong. 

If you're familiar with the history of Tibet, you know it's been the site of an ongoing struggle for freedom from Chinese rule. It's also one that has involved regular, though always shocking, acts of Tibetan monks setting themselves on fire to protest Chinese oppression. In 2008, 21 Tibetans died after protests turned especially violent.

There's been no shortage of humanitarian pleas calling for an end to the occupation of Tibet. So why does China refuse to let go of Tibet?

The answer to this question lies just west of Tibet: India.

China and India are far and away the two most populous countries on the planet, so there's tension over how ugly any major conflict between them could become. It is perhaps fortunate that there is a natural buffer zone between these two world powers in the form of the Himalayan mountains, which run along the western border of China. But this is unfortunate for Tibet, since it's smack dab in the middle of this buffer zone.

Tibet's homeland is the Tibetan Plateau, which is right next to the Himalayas, on China's side of the divide. So, in the theoretical event of India sending an invading army over the Himalayas, they could occupy the Tibetan plateau and have a commanding position, looking down upon mainland China, from which they could launch their attack.

This is China's primary strategic reason for occupying Tibet. If they don't control it, they're leaving the door open for India to take it and thereby leaving themselves extremely vulnerable.

However, there is another motive behind China's actions: water.

Tibet has long held the nickname "China's water tower," since three of China's main rivers — the Mekong, the Yellow River and the Yangtze — all have their sources in Tibet. So, not only would India have a superior offensive position in Tibet, if they occupied this area they could also cut off China's main water supply.

As far as China is concerned, whether or not India would ever _want_ to deprive China of water is irrelevant. The fact is that they _could_, and this is enough of a threat to China's prosperity for them to continue occupying Tibet.

> _"The Chinese see Tibet not through the prism of human rights, but that of geopolitical security."_

### 4. Guns and geographical good fortune make the United States invulnerable. 

If you were to think of the world in terms of real estate, which country do you think a realtor would hold in the highest regard? If you factor in things like good neighbors, water supplies and state-of-the-art security features, most real estate agents would put the United States of America on the top of their list.

Unlike most of the countries mentioned in these blinks, the United States has few concerns about traditional invasions.

The geographical position of the United States is unique in that it makes the country practically invulnerable to any invading army. Its only neighbors are Canada and Mexico, and they're not just friendly, they're also big enough that any invading army attempting to reach the United States by going through these countries would have to establish impossibly long supply lines.

Perhaps the best protection the United States has working in its favor are the Pacific and Atlantic Oceans, which border the country's west and east coasts, respectively. This effectively cuts off the east and the west from attacks, since any invading force would have to contend with an entire, volatile ocean before even reaching its target.

As for the state-of-the-art security system — well, how does 100,000,000 loaded guns sound?

The United States' lenient gun laws have resulted in every small town having the potential to take up arms and immediately defend itself from invasion without any help from the federal government. This is a country with the right to bear arms written into its social fabric, so guns are within easy reach for millions of Americans.

Any invading force would have a new set of armed civilians in every Springfield and Sunnydale they come across.

### 5. Geography has blessed northern Europe and blighted its south. 

Thanks to the Enlightenment and the Industrial Revolution, Europe has hugely contributed to the modern world, for better or for worse. This isn't just happenstance; it's due in large part to Europe's temperate climate, generous rainfall and fertile soil, all of which helped build thriving societies.

Due to geography, however, some areas of Europe have thrived more than others.

In 2012, when the Eurozone crisis was at its peak, nasty stereotypes began appearing in German media with increasing frequency, in attempts to explain why some regions of Europe were experiencing such a severe economic downturn.

In particular, these generalizations portrayed northern Europeans as industrious hard workers, and southern Europeans as a bunch of lazy slackers. But rather than being attributable to work ethic, the true reason for the struggles of some southern European countries lies in geography.

The same Northern European Plain that haunts Russia has provided France, Germany, Belgium and the Netherlands with fertile soil and a wealth of productive crops. As a result, they're the nations associated with hard work and money. And with a surplus of crops and goods to trade, northern Europe became home to bustling cities of commerce and major urban hubs.

In contrast, countries in southern Europe have far less arable land.

Greece, for example, doesn't have enough fertile land to be a major agricultural exporter, and this also means that the country can only develop a small handful of those major cities of commerce that are so abundant in the north. It's in these cities, like London and Hamburg, where you'll find the highly educated and skilled workers that drive forward modern economies.

Unfortunately for southern European nations like Greece, geography still plays a major factor in its well-being and political future.

### 6. Geography has given Africa beautiful but impractical waterways. 

The relationship Africa has with its oceans and waterways is a complex and frustrating one, and this isn't just as a result of its vast deserts. Africa is home to some of the world's most stunning beaches and coastlines, as well as legendary rivers. But as we'll see, African countries have been dealt a difficult hand when it comes to being able to use these waterways for commercial gain.

For starters, Africa's picture-perfect coastline is all but useless when it comes to setting up harbors.

Unlike much of the jagged coastlines found along Europe and the United States, where the ocean sharply declines into deep waters that are perfect for docking boats, Africa's shoreline is mostly smooth and shallow. This makes it impossible for cargo ships to load and unload goods for importing and exporting.

While this remains a problem, human ingenuity is starting to find a way around Africa's problematic geography. Tanzania and Angola, for instance, have formed partnerships with China to start creating man-made deepwater harbors. In Tanzania, they're using sheer brute force to expand the port of Bagamoyo so that it will eventually be able to load and unload 20 million containers of cargo each year, making it Africa's biggest port.

But Africa's challenges don't end with the coastline. Moving inland, it's Africa's rivers that pose another geographical obstacle that has hindered trade.

The Zambezi River is one of the continent's longest and most spectacular rivers. Its 1,600-mile waterway runs through six countries and is punctuated with whitewater rapids and breathtaking waterfalls, such as Victoria Falls. These stunning features may be great for adventurers, but they severely limit the river's usefulness for transportation. Simply put, cargo ships and waterfalls don't mix.

The impracticality of Africa's rivers as useful trade routes has made both trade and contact between the continent's different regions very limited. This has, in turn, significantly hindered economic development across the continent and prevented major trade routes from forming.

> _You could fit the United States, Greenland, India, China, Spain, France, Germany and the United Kingdom into Africa and still have room for most of Eastern Europe!_

### 7. Geography has gifted North Korea with hills, while there’s flat land in South Korea all the way to Seoul. 

With the seemingly endless threats it poses to neighboring countries, North Korea is becoming a major headache — especially for South Korea. You may be wondering how this difficult arrangement has lasted so long and, once again, the answer lies in geography.

Even though South Korea has twice the population and 80 times the economic power of North Korea, not to mention having a superpower like the United States on its side, South Korea has remained the vulnerable one.

This is because of the hills and elevated terrain located along North Korea's side of the border, which is located only 35 miles away from Seoul, South Korea's capital city, where half of the country's 50 million citizens live.

Within these hills, military experts estimate that North Korea has 10,000 weapons stashed, ready to fire 500,000 rounds into the city of Seoul within 60 minutes. So, if a conflict were to occur, South Korea knows that it would have to immediately contend with millions of civilians fleeing south from Seoul, while at the same time trying to create a strong defensive line in that area. It doesn't take a strategic genius to recognize that this is a recipe for chaos.

Another geographic feature working against South Korea is that the 35 miles of land separating Seoul from North Korea are flat, making the hills along the North Korean side of the border even more dangerous.

Therefore, if North Korea were to launch a surprise attack, their army could move quite easily over the flat terrain and into the heart of the enemy's capital city, landing a devastating blow. On the other hand, if South Korea were to launch a surprise attack, it would immediately hit a series of geographical speed bumps that would slow down ground troops and make them vulnerable to attack.

This is partly why these two opposing nations have remained in a political deadlock for over 50 years.

> The United States has about 30,000 troops stationed in South Korea.

### 8. Final summary 

The key message in this book:

**Societies are inevitably shaped by the land upon which they exist. Natural resources and geographic features can provide safety and prosperity or leave a country's citizens exposed and struggling. Geography has been a determining factor in the wars humans fight, as well as the speed of our economic development. Although modern technology now allows us to bend the rules of geography, it still remains crucial to understanding why nations have turned out the way they are today.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _A Short History of Nearly Everything_** **by Bill Bryson**

_A_ _Short_ _History_ _of_ _Nearly_ _Everything_ offers an enlightening summary of contemporary scientific thinking relating to all aspects of life, from the creation of the universe to our relationship with the tiniest of bacteria.
---

### Tim Marshall

Tim Marshall is a British journalist and the former foreign affairs editor for Sky News.

