---
id: 5a9b37b1b238e1000791094a
slug: problem-solving-101-en
published_date: 2018-03-07T00:00:00.000+00:00
author: Ken Watanabe
title: Problem Solving 101
subtitle: A Simple Book for Smart People
main_color: FFC633
text_color: 806319
---

# Problem Solving 101

_A Simple Book for Smart People_

**Ken Watanabe**

_Problem Solving 101_ (2009) is a short and snappy guide to problem-solving. Originally written to help kids become better problem solvers, it can help anyone who wants to improve their ability to resolve issues, no matter how big or small.

---
### 1. What’s in it for me? Become a problem-solving master. 

Whether you want to be more successful at business or better at learning, it can be more than a little frustrating when you know what you want but find that the path to your goal is littered with obstacles, making the target appear impossible.

These blinks are about never having that frustrating experience again. Based on simple advice originally written for Japanese school children, the advice contained within will help anyone get a grip on the problems keeping them from succeeding. Read on to learn how to break down the problem-solving method into four simple, easy-to-follow steps.

In these blinks, you will learn

  * the no-brainer first step toward solving any problem;

  * the usefulness of a yes/no tree; and

  * how to achieve even the biggest of dreams.

### 2. Problem Solving 101 starts with breaking your problem down to its core. 

We come across problems every day, like grammar difficulties in English class or tension with colleagues or friends. No matter the scale of the problems we face, not all of us are well-equipped to solve them.

That's where the four essential steps come in. They can help you solve any problem.

The first step is to identify what your problem is really about. To do so, you have to take the problem and break it down into smaller parts.

For example, let's imagine you're struggling with poor grades in math. At first glance, the problem looks huge and almost impossible, so you wonder whether you should stop playing soccer with your friends to devote more time to studying. But before you ring your friends to tell them you're off the team, think about the problem in another way.

You should ask yourself which categories are causing you trouble. Is it geometry, algebra and/or fractions? If only geometry is bringing down your overall marks, then only geometry should be your focus of increased study. But before you start studying, break your problem down even further.

Take a narrower look at geometry and single out the specific problems you have with the subject. Is it measuring cylinder volume or trapezoid areas, or is it the Pythagorean theorem? Once you've pinpointed your weak spot, you can begin to solve the problem much more effectively by focusing all your attention on it — and you will probably have time to play soccer with your friends, too!

So now you know exactly _where_ your problem is. However, you haven't solved it yet. Next up, work out what exactly is _causing_ your problem.

> _"Problem-solving isn't a talent that some people have and others don't. It's a habit."_

### 3. To discover what’s causing your problem, brainstorm the potential reasons and test your ideas. 

Now that you've drilled down to find your actual problem, you have to work out _why_ it's a problem. In other words, what's causing it? This is step two of the problem-solving method.

To determine the cause of your problem, first list all the possible causes you can come up with.

For example, let's say there's a children's band called Apples and Oranges. They hold monthly concerts in their school, but few people attend, and they want to know why. So the band members list all possible causes: Maybe people haven't heard about the concerts? Maybe they don't like pop music?

But these are only hypotheses. To know for sure whether one hypothesis really _is_ the root cause of your problem, you need to test it out. A great method of testing is to use a tool called a _yes/no tree_ to answer a few yes/no questions that help clarify your hypothesis.

Apples and Oranges start creating their yes/no tree after listing the possible causes. The first yes/no question is: Do people know about the concerts?

If the answer is "no," the band have probably identified a lack of awareness as the root cause of low attendance numbers.

If the answer is "yes," the tree unfolds into more branches, and the band have to answer another yes/no question: Did the group of people who knew about the concert actually attend? If no, perhaps they're not fans of pop music? If the answer to that is also "no," then that's the cause. But if they are fans, then dig a bit deeper. Maybe ask why more people came to the first concert than the second one.

Keep asking yes/no questions until you have all the answers to support your hypotheses. Then you'll have your root cause and be ready for the next step: analyzing that cause so you can form an action plan.

### 4. To select the best possible solution, brainstorm ideas and conduct careful analysis. 

So you've completed step two and found the cause of your problem. The next step is to perform the analysis that will help you generate the best solutions.

Start by asking yourself: What information do I need to analyze the situation? Going back to the Apples and Oranges example, the band shared surveys and conducted interviews with a small sample of students at their school. From the information they gathered, they realized that they needed to both raise awareness and entice people to come to their concerts.

Now you've collated and analyzed all the information; it's time to develop possible solutions.

So, Apples and Oranges listed advertising via the student newspapers and email. They also considered playing in classrooms and placing adverts on the radio. All these possible solutions were written down and laid out in a table. The band made two columns: one entitled _"Raises awareness?"_ and the other _"Gets people to attend?"_ Then they arranged each solution under one column or another, according to how and where they were impactful.

With many possible solutions, you need to prioritize and determine which will be included in your _action plan_.

Apples and Oranges wanted to prioritize the solutions that were both impactful and easy to implement. Take the idea of performing in classrooms. They knew it would have a great impact, but it would also require a lot of effort to set up and then dismantle equipment each time.

Since a significant portion of the students listen to the school radio, having the radio station announce their concerts and preview their music would be both simple to implement _and_ have a great impact. So they decided to make radio announcements part of their action plan of concrete steps to solve their problem.

Now that we know the steps involved in problem-solving, let's see how they can be applied to everyday situations. Then we'll return to the action plan and learn about the next step.

### 5. To realize big dreams, set smaller goals to gauge what it will take to reach them. 

Many of us like to dream big. Some of us think about becoming professional figure skaters or famous actors. Often, however, because these dreams seem so big and impossible to reach, we never act upon them, and they stay dreams forever.

But it doesn't have to be that way.

If you want to successfully hunt down a big dream, you need to break it up into smaller goals. Make sure the goals are obvious and set them one at a time.

For example, Eric Squirrel wants to become a director of big-budget animated films, but he has absolutely no skills in computer animation. He doesn't even own a computer. His first goal is, therefore, to get hold of a computer. Specifically, he wants a used Apple computer which costs $600, and he'd like to buy it within the next six months without taking out a loan to pay for it.

Eric's goal is small, clear and specific, which makes it easier for him to reach. Like Eric, you too need to set small, clear and specific goals.

The next task is to work out how you can reach that goal.

Consider the size of the gap between your current situation and the goal you're trying to meet. Eric Squirrel knew that he wanted a $600 computer within half a year. But by calculating his savings and future earnings, he discovered that he would only have $352 by that time. The gap between his current situation and goal was $248.

So, how could Eric close that gap? We'll find out in the next blink.

### 6. To reach your goal, list all the possible solutions and turn the best one into a hypothesis. 

What do you do when you notice a hole in your sock? You work out how best to sew it up. Similarly, you must create ways to close the gap between your current situation and your goal.

You can achieve this by making a list of all the possible solutions before choosing the best one, which will become the hypothesis for a potential solution.

In Eric Squirrel's case, he starts brainstorming loads of ideas, like saving cash, asking his boss for a raise and playing the lottery. Then, to choose the best idea, he forms a _logic tree_, very similar to the yes/no tree used by Apples and Oranges to discover why no one was coming to their shows.

Eric starts from his main goal of using his own money to buy a $600 used computer within six months. He draws two branches: one about limiting his spending;the other about raising his income. He extends those two branches to new, more specific branches. So for the "reducing his spending" branch, he draws new branches that suggest exactly how he will save money. One suggestion is cutting back on entertainment purchases, such as games and CDs.

When the tree is complete, Eric eliminates the branches that seem neither achievable nor particularly effective, like winning the lottery and making investments. In the end, the good and feasible ideas are left.

From these he will form his hypothesis: he can reach his goal of buying the computer by getting a better paid job, selling some of his DVDs and not buying any more games or CDs.

Now it's time for Eric Squirrel to put his hypothesis for a solution to the test. Next up, step four: make an action plan.

### 7. Once you have your solution hypothesis, you can analyze it and start taking action. 

So, you've nailed down your problem, you know the root cause and you've created a hypothesis to solve it. You're pretty much ready to take action.

But first, you need to analyze your hypothesis to work out how exactly to implement it.

Start the analysis by collecting relevant information. In Eric Squirrel's case, he begins by collecting the receipts of games and CD purchases. He also reaches out to friends about job offers and researches the selling price for used DVDs.

After collecting the information, you're ready to analyze it. To calculate his expenses, Eric goes over his purchase receipts of the past few months and concludes that he spent $10 on a video game, $3 on a comic book, $1 on candy, and so on. He then measured his priorities against the cost of purchasing the different items. Eric has a big sweet tooth, and since he only spends $1 on candy, he decides it's not worth cutting back on. But he makes the decision to stop buying CDs for a while, as he spends quite a lot of money on them. He also analyzes the pay from another job, and how much he can earn selling DVDs. After all that, he can conclude that it's possible to reach his goal of purchasing the used Apple computer.

With the analysis done, it's finally time for the fourth step: execution.

Remember, a plan is worth nothing if you don't execute it. So be sure to execute it well.

And be prepared to make adjustments along the way! Imagine if Eric couldn't sell his DVDs. Well, he would simply have to apply his new knowledge about problem-solving. Perhaps he can walk his neighbor's dog instead, and earn a bit of extra cash that way.

You should do the same. If a hiccup appears, modify your plan and keep executing it until you achieve what you have set out to do and solve your problem.

### 8. Final summary 

The key message in this book:

**Everyone faces problems, from small ones like a geometry sum to big ones like fulfilling a lifelong dream. Luckily it only takes four steps to solve any problem: break it down, see what's causing the problem, analyze the cause, and, finally, execute an action plan.**

Actionable advice:

**Use** ** _criteria_** **and** ** _evaluation_** **to choose between different options.**

If you're having trouble deciding between two or more options — for example, which school to attend — the tools of _criteria_ and _evaluation_ may come in handy. First, list the criteria you want to use to evaluate the options, such as the quality of the school's science program, or the accessibility of the school. Once you've listed the criteria, decide on the importance of each one, using + and -. If it's not that important, mark it with a -. If it's very important, indicate so with ++ or even +++. This will enable you to find the best solution for you.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _How to Fly a Horse_** **by Kevin Ashton**

_How to Fly a Horse_ (2015) delves into the process of creation and ultimately discovers that the very act itself is far more ordinary than we often think. In fact, in building upon the creative work of generations of thinkers, anyone can create, as long as they have the grit and determination to do so.
---

### Ken Watanabe

Ken Watanabe is a Japanese author. He was formerly a consultant at McKinsey & Company and is now based in Tokyo where he manages his own education, media and entertainment company, Delta Studios.

