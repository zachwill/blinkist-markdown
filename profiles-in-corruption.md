---
id: 5e70dcd66cee070006e0cdef
slug: profiles-in-corruption-en
published_date: 2020-03-20T00:00:00.000+00:00
author: Peter Schweizer
title: Profiles in Corruption
subtitle: Abuse of Power by America's Progressive Elite
main_color: F33131
text_color: BF2727
---

# Profiles in Corruption

_Abuse of Power by America's Progressive Elite_

**Peter Schweizer**

_Profiles in Corruption_ (2020) challenges us to question and confront the moral integrity of the politicians at the forefront of the modern progressive movement in America. Derived from a range of sources, from financial reports to corporate documentation, eight profiles of the biggest names in left-wing politics tell us a harrowing story of illicit exchanges, cover-ups, and double-crosses.

---
### 1. What’s in it for me? Pull back the curtain on the shady dealings of the progressive movement’s biggest names. 

Many of us view our favorite politicians through rose-tinted glasses. We want to believe that those pushing the issues important to us are honest and principled. But how often do we question whether that's really the case?

In the following blinks, you'll read profiles of eight American politicians who have abused their power in a variety of ways. For some, it's a matter of saying one thing in public and doing another in private. For others, it's using their positions to collect favors for friends and family. And for the rest, it's a more disturbing tale of crime cover-ups and financial deals with shady individuals.

The author, Peter Schweitzer, has focused specifically on progressive politicians because they are the primary figures pushing for government expansion, which could further increase their power. However, they are certainly not the only politicians who have been swayed by money, connections, and status.

In these blinks, you'll discover

  * how Joe Biden uses his contacts;

  * why Cory Booker is no action hero; and

  * how Senator Amy Klobuchar treats her staff.

### 2. Kamala Harris leverages her friendships with powerful people to gain power for herself and cover up their misdemeanors. 

Kamala Harris, California's current junior Senator, is sometimes referred to as the "female Barack Obama." Superficially, it's easy to see why, with her confident, charismatic persona and mixed-race background.

Prior to her Senate career, Harris was best known for serving two terms as California's Attorney General. And while she claims that her success comes down to her grit, determination, and skill as a lawyer, her rise to prominence actually depended heavily on favors from a corrupt California political machine.

**Here's the key message: Kamala Harris leverages her friendships with powerful people to gain power for herself and cover up their misdemeanors.**

In 1994, Kamala Harris began dating a politician named Willie Brown. At the time, Brown was the Speaker of the State Assembly of California. The relationship led to a number of favors for Harris, including new political appointments and big boosts to her paycheck.

In 2000, Brown became the Mayor of San Francisco. Just three years later, he leveraged his wealth — and the labor of his supporters and employees — to help Harris get elected as San Francisco District Attorney.

And Brown's help didn't stop there. Perhaps his greatest gift to Harris was his long list of political connections. These included supporters, donors, and sponsors who would help boost Harris's various campaigns and fill her coffers. In return, she'd help them out when they got into trouble.

Throughout her tenure as attorney general, Harris showed her willingness to be tough on issues like violent crime and the environment. However, when a political supporter of hers was involved in a case, she became curiously silent. One of the most disturbing instances of Harris' selective prosecution was a child abuse scandal involving the Catholic church.

From 2002 to 2003, stories of child abuse and molestation began spreading from Rome to the United States. Eventually, the scandal made its way to San Francisco, California. There, Harris's predecessor, Terence Hallinan, began pursuing legal cases against the San Francisco Archdiocese. The Archdiocese eventually — and with a great deal of reluctance — provided Hallinan with internal records of abuse complaints spanning seventy-five years.

When Harris took office as D.A., she inherited the case from Hallinan. The problem? While Harris was conducting her campaign prior to the election, donations from the Catholic Church and its lawyers flowed towards her in unprecedented amounts..

Though victims' groups urged Harris to release the records and expose the names of the alleged abusers, which would help encourage more victims to come out, Harris refused. Even more damning? She never pursued a single documented case against an abusive priest during her time as D.A.

### 3. Joe Biden repeatedly leveraged his power to give his family huge financial boons and other forms of aid. 

Throughout his long career in politics, Joe Biden has painted himself as a champion of the working class. His image is one of a gritty, blue-collar guy from Scranton who fights relentlessly for unions and workers across the country.

But that's not all. Another big part of Joe Biden's persona is his absolute refusal to discuss his family members' business activities with the press. So what secrets is he hiding?

**The key message here is: Joe Biden repeatedly leveraged his power to give his family huge financial boons and other forms of aid.**

Hunter Biden, Joe's son, has recently been in the news thanks to President Trump's impeachment trial. While most of the attention has been on the president, we shouldn't overlook Hunter's shady yet lucrative business deals in countries like Ukraine, China, and Kazakhstan. 

In December 2013, Hunter hitched a ride with his father on a private plane to Beijing. While the Vice President was conducting a series of meetings with Chinese officials, Hunter was being introduced to future business partners in China.

About ten days after the trip, Hunter Biden's company, Rosemont Seneca Partners, finalized a $1 billion deal with the Chinese government. 

It's obvious why this was a major problem. Aside from Hunter leveraging his father's position to give his business a huge advantage, protecting his son's interests could have influenced Joe to be more lenient in his attitudes and policies toward China.

Hunter isn't the only Biden who was able to ride the wave of Joe's success. James and Frank Biden, Joe's younger brothers, have both benefited from their brother's lofty position.

Let's take Frank. In 1999, he and a group of friends were speeding in a rented Jaguar sedan. That led to a car accident that killed a young single father crossing the street.

Frank was sued and ordered to pay each of the victim's two daughters $275,000. However, he never did — and worse, no one, not even private investigators, could seem to find Frank. As a last-ditch effort, the family wrote a letter to Joe in 2008 hoping for help and sympathy.

But his response was extremely disappointing. The letter, written by Joe's assistant, expressed sympathy for the family, but claimed that Joe could do nothing to help. It also claimed that Frank possessed no financial assets that would allow him to satisfy the judgment — which was an outright lie.

> _"From that beginning, Joe's political career was a family affair."_

### 4. Cory Booker casts himself as an action hero, but his true character and intentions are much less pure. 

In the midst of a relentless snowstorm, Mayor Cory Booker of Newark, New Jersey received a phone call from a woman who was trapped in her house and running out of diapers for her baby. Could the mayor do anything to help her?

"I'm on it," replied Booker. He rushed to the woman's house to deliver the diapers, the press in tow. 

On the news, Booker was praised as a pseudo-action hero who knew how to manage his city during a snowstorm. But a nagging question remained — why did the woman need to resort to calling the mayor when the streets should have been plowed in the first place?

**The key message here is: Cory Booker casts himself as an action hero, but his true character and intentions are much less pure.**

The snow plowing debacle is far from a one-off. Take the time in 1999 when Booker pledged to sleep in a tent in one of Newark's more dangerous areas as a form of protest in solidarity with residents. But what really happened? Witnesses often saw Booker leaving the tent late at night and then returning early in the morning. He was trying to make it seem like he had stayed in his tent all night when he clearly hadn't.

It's been clear throughout his time in politics that Booker cares more about his image than he does the actual lives of Newark residents. This becomes even more apparent when we take a look at which organizations and causes Booker has chosen to support **** — as soon as they gave him financial aid in return.

At one golfing event, for example, nine contractors gave a combined $19,500 to one of Booker's nonprofits. What happened over the next twelve months? Of course, the contractors' companies received $21.5 million in city contracts.

Moreover, Booker's interest in money has led him to neglect his job. When he was mayor, he could often be found traversing the country on lucrative speaking tours. One year, he was away from the city for three full months. While Booker was collecting millions from his speaking arrangements, the Newark police department was engaged in a brutal regime which ultimately sparked a federal intervention in 2016. 

In his speeches, Booker stands up to cronyism and corruption. Unfortunately, his words aren't matched by his actions.

### 5. Elizabeth Warren has a troubled history of lying about both her race and the contracts she took on as a bankruptcy lawyer. 

Elizabeth Warren has been at the forefront of progressive politics since 2013, when she took up her position as Massachusetts senator. Prior to her political career, she was a Harvard professor specializing in bankruptcy law.

Warren's scholarly persona is a direct result of her academic success. But how much of that success can be tied to her claimed status as a Native American?

**The key message here is: Elizabeth Warren has a troubled history of lying about both her race and the contracts she took on as a bankruptcy lawyer.**

One year before Warren's appointment to the University of Pennsylvania faculty, she began listing herself as a "minority" Native American law professor in the directory of the Association of American Law Schools. But in 2018, she took a DNA test which showed she was only, at maximum, 1/32nd Native American.

By declaring herself a Native American, both the University of Pennsylvania and Harvard Law School could tout her as a minority faculty member. Harvard even boasted about Warren's status as the law school's "first woman of color." Curiously, the same year she was given a tenured position at Harvard, she chose to stop listing herself as a minority.

Although Warren has a reputation for fighting for people of color and working-class Americans, she has advised on a number of legal cases that prove she's willing to take the opposite stance when it suits her financially. 

In the mid-1990s, Warren was highly influential in both writing and advising Congress on bankruptcy laws. The result? Laws that allow large corporations to avoid liability and shield themselves from lawsuits by declaring bankruptcy. That was a huge win for the corporations — and for Warren, who could offer her advisory services to companies affected by the new laws.

One company that sought Warren's consultation in the 1990s was LTV Steel. The company was trying to overturn a court ruling which forced it to pay $140 million in retirement benefits to former employees and their dependents.

On television, Warren gave an interview in which she attacked the company for treating its employees "like paper towels." But off-screen, Warren had been paid $10,000 to help LTV avoid some of its legal liabilities.

It's easy to see why Warren wouldn't want her constituents to know about these cases.

> _"[F]or herself, [Warren] was comfortable with the sort of economic behavior for which she would later famously condemn others."_

### 6. Sherrod Brown is willing to engage in dishonest and corrupt practices when they benefit him or his family. 

Ohio Senator Sherrod Brown is known as the guy who was progressive before it was cool. Unlike many of his liberal compatriots, Brown hails not from one of the coasts, but from America's heartland. Proudly displayed in his office are a miner's safety lamp and a United Workers beer stein.

Publicly, Brown presents himself as a regular, working-class guy. But the reality couldn't be more different. Brown has, in fact, never held a full-time job outside of politics. And his inspiration to fight for working people? Tolstoy's _Resurrection_, which he read at Yale.

**The key message here is: Sherrod Brown is willing to engage in dishonest and corrupt practices when they benefit him or his family.**

It's clear that Brown is out of touch with the people he professes to give a voice to. But that's not all. He also seems uniquely willing to protect union leaders __ who donate to his campaigns — he doesn't offer this same protection to union _workers_, though.

Major unions like the AFL-CIO and the Teamsters have poured money into Brown's campaigns. And when the interests of union leaders have clashed with those of workers, Brown has consistently sided with the leaders. For instance, Brown worked to protect union leaders from having to institute transparency policies which would track how union members' dues are spent.

And it's not only union leaders who have benefited from Sherrod's time in office. Charlie Brown, Sherrod's brother, has directly benefited from some of Sherrod's congressional bills and campaigns. 

Charlie and his business partner, James Turner, run several health advocacy non-profits out of their law firm — ones with official-sounding names like Citizens for Health and Consumers for Dental Choice. But these firms push unscientific claims such as childhood vaccines causing autism and mercury in dental fillings poisoning people.

So where does Sherrod come in? As Charlie deals with legal cases involving drug and health companies, Sherrod introduces congressional bills and campaigns which directly benefit his brother. 

One example was Consumers for Dental Choice, or CDC, a research institution formed in 1996 which would lobby for "mercury-free" dentistry. Soon after Sherrod formed the organization, Charlie began working with class-action attorneys to file lucrative lawsuits seeking damages worth millions of dollars from dental companies around the country.

Seemingly, Sherrod is more interested in helping himself and his family than those he's supposed to represent.

### 7. Bernie Sanders and his wife, Jane, have spent years engaged in questionable financial activities they’ve worked hard to cover up. 

You could easily argue that Bernie Sanders has had more of an impact on modern left-wing politics than any other member of Congress. Many see him as a beloved grandfather figure, an aging warrior for the 99 percent.

But despite Sanders' outward veneer of fighting for average Americans, he has, in the past, referred to working-class jobs as "moron work." And this is just part of a troubling pattern of actions that stand directly opposed to his impassioned words at speeches and rallies.

**The key message here is: Bernie Sanders and his wife, Jane, have spent years engaged in questionable financial activities they've worked hard to cover up.**

Anyone who has watched a Sanders speech knows that he campaigns as a democratic socialist railing against the mega-rich. So in theory, he should be against real estate projects backed by wealthy contractors which result in neighborhood gentrification, right?

Apparently, the answer is _no._ After he took office as mayor of Burlington, Vermont in 1980, Sanders held a string of secret meetings with developers to make plans for a highly exclusive waterfront condominium project — much to his supporters' chagrin.

Another issue Sanders has spoken out against is the military-industrial complex. Why, then, did he side with a Burlington armaments plant and order the arrests of the protestors picketing there? Perhaps because the plant and the military spending that occurred during the Reagan administration were huge boons to the Burlington economy.

In terms of financial questions surrounding Sanders, this is just the tip of the iceberg.

Just before the 2000 election, the Sanders family formed a consulting company called Sanders & Driscoll LLC, which also operated under two different trade names. While it's impossible to know how much money these companies were responsible for raising, it's clear that they served as financial conduits for Sanders' election campaign.

Additionally, Jane Sanders has listed herself both as a "self-employed antique dealer" and "media buyer" on her husband's financial disclosure forms. Strangely, though, there are no antiques business licenses registered to Jane's name in the state of Vermont. And since media buyers are not required to disclose their commission anywhere on campaign filing forms, we only know that she made more than $1,000 from that line of work.

This is all just the tip of the iceberg in terms of the financial mysteries surrounding Sanders. Another question, for example, involves the cash Sanders and his wife used to purchase their third home, a half-million-dollar vacation house in Vermont. Sanders is keen to attack wealthy people for their lavish lifestyles and excesses. But when he participates in these things himself, he does whatever he can to sweep them under the rug.

### 8. To her staff, Amy Klobuchar is verbally abusive – but she’s perfectly willing to go soft on those who can benefit her political career. 

Minnesota's senior senator, Amy Klobuchar, has been making headlines. Not just because of her 2020 presidential campaign, but for the way she allegedly treats her staffers and interns.

Staffers have reported Klobuchar calling them in the middle of the night, throwing binders at them, and sending them to her house to do her dishes. One female staffer even reported that Klobuchar forced her to dry shave Klobuchar's legs under her desk while she was on the phone.

Klobuchar's shockingly high staff turnover rate of 36 percent seems to corroborate these stories of abuse.

**The key message here is: To her staff, Amy Klobuchar is verbally abusive — but she's perfectly willing to go soft on those who can benefit her political career.**

Not only does Klobuchar mistreat her staff, but she also punishes small companies and individuals while avoiding the big fish who have given her money.

In the early 2000s, Klobuchar went after Northwest Airlines pilots who were being investigated for tax evasion. The pilots argued that they lived and paid taxes in other states and would simply arrive in Minnesota for their flight assignments.

Klobuchar ultimately charged seven pilots with tax evasion, declaring the pilots white-collar crooks. However, she acted very differently with another crook in her jurisdiction even though this crook was crafting the then-second-largest Ponzi scheme in American history.

The man at the center of the scheme was Tom Petters — a friend of Klobuchar's and one of her biggest financial backers. In 1998, Petters and his employees donated $8,500 to Klobuchar's campaign for Hennepin County Attorney. By the end of Klobuchar's 2006 senate campaign, Petters Group Worldwide had contributed more than $120,000 to her.

As early as 1999, when Klobuchar was working as county attorney, evidence of Petters' crimes had begun to surface. Officers from Klobuchar's office seized documents which offered evidence that Petters and two of his partners were engaged in an illegal financial investment scheme.

Klobuchar charged both of Petters' partners — Hettler and Kahn — with fraud. But she did not press charges or even _investigate_ Petters.

Then, in September 2008, the FBI raided Petters' office and home. In a conversation the FBI recorded, Petters noted that Klobuchar had called him a few days after the raid. 

While Klobuchar wasn't directly involved in Petters' scheme, she was complicit in allowing it to go on as long as it did.

### 9. Los Angeles Mayor Eric Garcetti plays a central role in his city’s corruption, particularly when it comes to real estate. 

If there is one thing you'd expect a mayor of Los Angeles to have, it's an army of celebrity supporters. The current mayor certainly embodies this cliche. Salma Hayek, Jimmy Kimmel, and Will Ferrell have all thrown their support behind Mayor Eric Garcetti at one time or another. Adding to his star-studded image are his multiple appearances on television shows and personal friendship with rapper Jay-Z.

Garcetti may be dancing with the stars, but behind the scenes, he's been greasing the wheels of a highly corrupt Los Angeles political machine.

**The key message here is: Los Angeles Mayor Eric Garcetti plays a central role in his city's corruption, particularly when it comes to real estate.**

Due to LA's complex zoning laws, the political system has devolved to one in which contractors pay money to the city government in exchange for approval for their projects. Essentially, in order for projects to move forward, the city council and mayor must grant companies exceptions, even if they don't comply with environmental regulations or other restrictions.

One way that companies can contribute money to California politicians is through a loophole called _behested payments._ To take advantage of the policy, a politician must first set up a nonprofit. Then, the politician can personally ask for a donation to the nonprofit from a particular company. This, of course, results in preferential treatment for those willing to shell out.

In just eight years in office, Garcetti took in $31.9 million in behested payments. Despite criticizing big money in politics, he doesn't seem to mind when that money ends up in his own pockets.

Take Samuel Leung, a developer who wanted to construct a residential property in an industrial area of Los Angeles. In 2013, Leung donated $60,000 to a pro-Garcetti organization. And in February 2015, Garcetti and another local politician were sent multiple checks from a gardener, a chef, and a handyman. The problem? All the checks appeared to be written in the same handwriting — Leung's.

Ultimately, Leung's project was approved. But he's also been indicted on bribery and money-laundering charges, which he denies.

Sadly, Garcetti's story is nothing new in politics. Time and again, we've seen politicians look the other way at crimes committed by their friends and donors, collect favors, and construct elaborate cover-ups. All of this, of course, happens at our expense — the very citizens and voters keeping them in power.

### 10. Final summary 

The key message in these blinks:

**Progressive politicians want us to believe that they stand up to wealthy corporations and individuals who seek to exploit average working- and middle-class Americans. Unfortunately, though, progressive elites have shown themselves to be all-too-willing to cozy up to the wealthy if it happens to benefit their own careers. The result is a disturbing tale of corruption and dishonesty.**

**Got feedback?**

We'd love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Brazillionaires_** **by Alex Cuadros**

Sadly, the United States isn't the only country where corruption has a solid foothold. In the South American country of Brazil, bribery and favoritism can claim a long and storied history. Billionaires and the wealthy corporations they control exist alongside those who struggle to attain even the most basic necessities.

To explore the shade and sunshine of Brazil's economy and politics, check out our blinks to _Brazillionaires._
---

### Peter Schweizer

Peter Schweizer is an investigative journalist, political consultant, and senior editor-at-large of Breitbart News. He is a five-time _New York Times_ bestselling author whose former titles include _Secret Empires_ and _Clinton Cash_, which prompted an FBI investigation into the money flowing to the Clinton Foundation.

