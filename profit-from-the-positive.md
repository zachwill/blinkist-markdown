---
id: 543392a234373600081a0000
slug: profit-from-the-positive-en
published_date: 2014-10-08T08:30:00.000+00:00
author: Margaret Greenberg and Senia Maymin
title: Profit from the Positive
subtitle: Proven Leadership Strategies to Boost Productivity and Transform Your Business
main_color: 6EBF46
text_color: 42732A
---

# Profit from the Positive

_Proven Leadership Strategies to Boost Productivity and Transform Your Business_

**Margaret Greenberg and Senia Maymin**

_Profit from the Positive_ explains how leaders can increase productivity, collaboration and profitability by using the tools of positive psychology to boost their employees' performance. It gives clear examples of how small changes can make big differences.

---
### 1. What’s in it for me? Learn how to become an effective leader using the tools of positive psychology. 

Have you ever wondered whether you're a good leader? Have you asked yourself how to push your team or wondered whether performance reviews really lead to better work?

_Profit from the Positive_ provides a complete system that will help you become a better leader and improve your team's performance. You'll learn how to get more work done without working more hours, how to control your emotions and improve a bad mood, how to avoid making hiring mistakes, and more.

The best part? You don't have to be an expert, have a budget, or get your boss' permission. Anyone can use these tools, whether you manage four or 40,000 people.

In addition, you'll learn

  * how putting a pen in your mouth can make you happier;

  * why a bad mood can be as infectious as a cold; and

  * why asking someone to rate their own weirdness can save you lots of money.

### 2. To improve productivity, plan ahead and try to pick up positive habits. 

If you're a manager, you're probably always on the lookout for new ways to increase both your own and your employees' productivity. Let's take a look at a few simple tricks for achieving that.

First of all, always take the time to formulate a plan before leaping into action.

The importance of this can be seen in a study by psychologist Peter Gollwitz, where he told his students to write a report on how they spent Christmas, and hand it in on 27 December. While half the students just got the assignment, the other half were also asked to identify exactly when and where they would complete it. It turned out that this "plan" made a huge difference: 71 percent of the second group submitted their reports on time, compared to just 32 percent who'd made no concrete commitment to getting it done.

This insight is easy to apply in other areas too. For example, saying, "Could you bring that report to the conference room after the 10 a.m. meeting?" is more effective than, "Could you hand it in tomorrow?" because the former creates a specific plan for the other person to follow.

Another way to improve productivity is tricking yourself into getting started.

Let's say you've got a long, daunting to-do list and you're having trouble getting started. Just pretend you've already begun! Jot down a few additional tasks that you've already completed, and then cross them out. This simple act will make you feel like you've already done something, which will motivate you to continue.

Finally, if you really want to be more productive, create positive habits and routines.

Consider Deborah. She used to check her email all the time, interrupting her other work. To improve her productivity, she decided to adopt a new positive habit: checking her emails only four times a day, at specific times, and not just whenever a new email arrived in her inbox. This allows her to better focus on her work, saving her time in the long run.

> _"We know what we should be doing, but sometimes we can't seem to get out of our own way."_

### 3. When things go wrong, be resilient and learn from your mistakes. 

To be a true leader, it isn't enough to be productive — you also have to be resilient. Because the truth is, we all experience setbacks and disappointments. What matters most is how quickly we bounce back.

So don't quit just because something went wrong in the past. Rather, be grateful for these mistakes and learn from them. Use these experiences to help you confront current challenges.

If your company loses a client, ask yourself: How did that happen? How can we avoid making the same mistakes with our new client?

Once you've asked these questions about the past, move forward by putting your focus on the present and future. What do you hope to learn from this new project? What mistakes might you make? How can you avoid them?

Finding the cause of the failure will help you learn from the experience. One excellent technique for analyzing what went wrong and preparing for new projects is called the _Me-Always-Everything Framework._ In this framework, you analyze failure from three different perspectives:

  * _Me_ : Ask, did I cause this failure? Or did external events cause it? Was it a combination of both?

  * _Always_ : Does this situation always happen to me? Or is this a one-off setback?

  * _Everything_ : Will this event spill over into other domains in my life? Or is this an isolated situation?

Another key to resilience is to change your perspective on a bad situation. If you're feeling negative, shifting your perspective to a more positive mind-set may spur you to action.

For example, consider the _severity perspective_ : How bad is this compared to other situations you've already faced? Remembering bad situations you've mastered will allow you to draw strength from them.

You might also consider the _extremes perspective_ : What's the worst thing that could happen? What's the best? And then, finally, what is the most likely to happen?

Thinking about these aspects will help you see things in a more positive light.

After all, to err is human. Simply learn from your mistakes and move on!

> _"Did you know that in tough times being a learner is more effective than being an expert?"_

### 4. Your mood will impact both your own productivity and your team’s performance. 

We don't always realize it, but our moods have a powerful effect on everyone around us.

That's because emotions are catching, as explained by _social contagion theory_. Basically we humans are hardwired to mimic each other's facial expressions and moods. According to the theory, just one person in any team of five can "infect" the other four.

So when you're in a bad mood at work, it spreads like a cold, which can seriously damage productivity.

On the other hand, positive feelings are equally contagious. Studies show that when bank tellers are in a good mood, they produce greater customer satisfaction by passing on their upbeat emotions.

This phenomenon also has a status component. People higher up in the organizational hierarchy are more contagious, meaning that an office leader's positive or negative mood can spread to others in the team in as little as seven minutes, quickly impacting everyone's performance.

This was demonstrated in a study of 53 sales managers: When leaders were in a good mood, their teams performed better, generating higher sales.

So although it's extremely important for leaders to stay positive, we all get the blues from time to time — that's natural. But it's important to overcome your bad moods. Let's look at four different techniques you can try.

  1. Label it: Naming what you're experiencing can minimize negative emotions.  

For example, saying "I'm anxious" allows the feeling to dissipate more quickly.

  2. Take a long, deep breath: Slow rhythmic breathing produces an immediate calming effect.

  3. Get off your tush: Walk around. If you can, go outside. Being around nature helps reduce stress.

  4. Embodiment: Put your body into a stance that mimics a different psychological state. This will effectively trick your mind into adopting the same state. So, for example, try to smile. Just mimicking this physical gesture can improve your mood.

These techniques can help you infect your staff with only good moods!

> _"Control your emotions, not your employees."_

### 5. To be a strength-based leader, know what you’re good at and hone in on solutions, not faults. 

Now that you embody productive, resilient and contagious leadership, there's another attribute you need to master.

You need to become a _strength-based leader_ — someone who focuses on strength, not weakness.

So for instance, when analyzing your business, ask questions about what your company is doing right:

  * Where is this change or process being implemented well? What can we learn here that could apply to other areas?

  * What makes this employee or competitor successful?

  * Why does this team or location constantly exceed expectations?

This approach allows you to focus on solutions, not faults. It's also a fantastic way to handle challenges your business may be facing.

For example, when you're facing an obstacle, say: "Let's brainstorm solutions. What are some short-term possibilities? Long-term ones?" This will help everyone focus on solving the problem.

Of course, managing a team is stressful. Some managers, in an effort to avoid having problems dumped on their desks, tell employees, "Don't bring problems if you can't propose a solution."

But this is flawed logic. If employees aren't communicating with you about serious problems, you might learn about them too late.

Another important aspect of strength-based leadership is that you and everyone on your team actually have to know their own strengths.

Research bears this out: A sales team was asked to complete a 40-minute strength assessment followed by a one-on-one discussion of the results. Another sales team didn't. The team that completed the assessment later generated 17 percent more sales than the other, showing that focusing on strength really lifts performance.

So how can you uncover your own strengths? Just ask yourself three questions:

  * What am I good at?

  * What kind of work energizes me?

  * When am I at my best?

Ultimately, being a strength-based leader doesn't mean ignoring problems, just focusing on solutions.

### 6. To avoid hiring mistakes, take your time and don’t undervalue personal skills. 

Every organization needs new people every now and then. The trouble is, how do you find the right people?

This question is a serious one because hiring mistakes are expensive: factoring in everything, each mistake costs a company around five times the hire's salary!

So how can you ensure you make the right hires?

Perhaps the most important tool you have at your disposal is the _interview_, so you need to prepare it well. Don't make the typical mistake of talking too much. Let them do the talking so you also get a feel for the candidate's personal skills. They are often more important than technical ones.

For example, if you're looking for a customer service person, it's better to hire someone with a positive attitude and no experience, than someone with a lot of experience but a bad attitude. After all, you can always teach someone new skills, but it's much harder to change their attitude.

In the same vein, it's important to only hire people who fit your company's culture.

Google developed a term for inspiring employees: _Googly_. A Googly employee is a smart, creative, a good problem solver and fun.

You, too, need to figure out what kind of employee profile would fit your own company.

Of course, to be able to do this, you have to first understand your company's culture, starting with your organization's core values. Only then can you tailor your interview to find suitable people.

Let's look at Zappos, which celebrates individuality among its employees. When interviewing job applicants, the CEO always asks: "On a scale from one to ten, how weird on you?" This question helps identify employees who would enjoy the individualistic company culture. This leads to higher employee loyalty and less staff turnover.

Finally, give the hiring process enough time: the typical one-hour interview may not cut it. For example, at Rackspace, prospective employees stay at the office for nine or ten hours. That way they truly get to know the company — and vice versa.

### 7. Identifying and cultivating your employees’ strengths will boost productivity. 

Here's something you may find surprising: Focusing on what people do well, rather than their weaknesses, will boost productivity.

This means once you've assessed your own strengths, you should start focusing on what your employees do best.

This was demonstrated in a study that tried to pinpoint aspects of effective leadership. One of the metrics was the intensity of leaders' focus on their employees' strengths, and researchers found that managers who scored in the top quartile for this metric achieved 50 percent better project performance than managers who paid less attention.

Connecting with your employees about their strengths doesn't have to be a big event. Make helping them tap into their skills part of your everyday interaction.

These conversations don't have to happen solely on a one-on-one basis, either. When personal strengths are part of daily conversations and team meetings, employees will get to know their colleagues' core abilities. This will create an environment in which differences are understood and valued, because they're seen as contributions to the overall power of the team. This helps avoid conflict.

To start creating this kind of atmosphere, host a workshop where you go around the room and ask team members to share one or two of their strengths and discuss how they've used them recently.

And once everybody knows each other's strengths and weakness, you can collaboratively plan how to position the team for success on upcoming projects, leveraging the sum of those strengths.

By identifying and cultivating your employees' key skills, what you're really doing is getting people to perform their best.

### 8. Performance reviews are an opportunity to motivate your team and preview the year ahead. 

Performance reviews can be an uncomfortable experience for managers and employees alike. But they don't have to be.

Instead of going into a performance review to evaluate your employee, see it as a time to _re-energize_ them.

Start by using the language of strength. For example, say something like: "Ann, you're an exemplar of the _achiever_ strength. You're focused, hardworking and always deliver. I'd like you to keep using your _learner_ strength to learn new processes."

Also, make sure you don't set vague goals. When you tell employees, "Just do your best," they're more likely to slack off, since they don't know how to improve.

Instead, try something like: "You're an emotional person. I like that side of you. Please write down your worries and feelings about this new project and send them to me by 2 p.m."

These kinds of specific goals will help you connect with your employees, but to really motivate people, it's important to also set challenging goals.

For proof, just ask Gary Latham, who researches goals at the University of Toronto. In multiple studies of different occupations, Latham found that those who set challenging and specific goals performed better than those who didn't. That's because when you accomplish a goal, you feel pride. "Challenging goals facilitate pride in accomplishment," explains Latham.

And to make the most of the goals you set, review them more than once a year. That way, you're motivating your employees again and again.

Finally, go beyond the conventional performance review and make it a _preview_.

Ask your employees to preview their performance for the year ahead. Have them imagine you're meeting a year from now to discuss their work. Ask them to visualize what a successful year would look like and what they have specifically done to achieve their goals.

Then, make the conversation especially memorable by asking your employees to write down their preview.

So while performance reviews may get a bad rep, they're actually a great chance to push your team.

> _"Think energize, not evaluate!"_

### 9. For a productive meeting, get everyone to participate and pay attention to openers and closers. 

Imagine a world in which your employees actually look forward to meetings. It might sound impossible, but you can make it happen.

How?

First of all, pay special attention to _openers and closers_. The way you start and end meetings matters because it's unrealistic to assume that everyone has arrived in a good mood and ready to focus.

Consider this: a staggering 91 percent of surveyed respondents admitted to daydreaming during meetings.

You don't have control over anyone's mind-set, but kicking off with a positive opener will boost the overall mood.

To do this, recognize an accomplishment, acknowledge a specific success or tell a funny story. Another way to improve moods at the start of a meeting is by offering a small gift.

For example, in one study, two sets of doctors were asked to make a complex diagnosis for a patient. One group was asked to first read the medical profession's ethical code. The other group received a piece of candy instead. The sweets helped: The second group of doctors were three times as accurate as the first.

And once you've nailed the beginning, don't neglect the ending, because people always remember the last few minutes of a meeting. Take time to discuss next steps and end things on a positive note.

Another key to running a productive meeting is ensuring everyone has a chance to contribute. Of course, managing the conversation is challenging for any meeting facilitator. To get people to speak up or to stop talking, try this:

  * Before the meeting, draw a circle on a sheet of paper. Label the circle with each participant's name. As each person contributes, put a checkmark next to their name so you can follow who participates and who doesn't.

  * If you notice someone interrupting, step in. Acknowledge the point and move on by asking for another perspective. Tell your team you don't need superstars — you want everyone to participate.

Follow these steps and your employees might actually look forward to meetings.

### 10. You don’t need anyone’s permission to use these tools. Simply start small, avoid jargon and expect resistance. 

By now you've read about quite a few tools and ideas for effective leadership. But how can you actually apply them to the real world?

First of all, start small. You don't need anyone's permission to use these tools; you don't need your boss or the HR department to get on board.

Just pick three techniques you can start doing today and go for it! For example, why not start today's meeting with a piece of cake and a compliment?

Another key to implementing these tools successfully: Use language everybody understands. Avoid jargon — it'll make people lose interest in your suggestions or think you're arrogant, and either way, they'll just ignore you.

Remember Gollwitz's study, which described how specific plans help us achieve goals? Well, never refer to it by that name because no one's going to know what that means. Instead, coach your employees in layman's terms: tell them they can achieve their goals by identifying where and when they'll complete certain tasks.

Here's something else you might remember: Social contagion theory, which explains how emotions circulate. You don't need to use the term in order to benefit from the insight. Pay attention to the mood you project when you walk into work. If it's not positive, change it.

Finally, a crucial point for anyone who wants to apply what we've learned to the real world: Don't resist resistance.

When we try to introduce something new — whether at work or at home — our ideas are often met with resistance. That's totally natural.

If you can expect resistance then you won't be surprised by it. Instead, you can view it as a promising sign of change. When people in our lives resist our ideas, it typically means that they have their own pain points that need to be addressed. So give them some time to rethink their attitude.

That's it! Now you're prepared. So make a brief plan and trick yourself into getting started. Good luck!

### 11. Final summary 

The key message in this book:

**Productive leaders create positive habits, like checking emails only at specified times, and always make plans before starting projects. They're also good at controlling their emotions as well as focusing on solutions and strengths, rather than problems. And even when things go wrong, productive leaders are resilient and learn from their mistakes.**

Actionable advice:

**Feeling grumpy and irritable? Try the pen smile.**

Simply hold a pen gently between your teeth for a couple of minutes. Why will that work? Well, the pose is similar to the position our mouths are in when we're smiling. Holding the pen activates our smiling muscles, inducing the brain to think "Oh, this is funny." This simple physical act produces a feeling of happiness, which consequently mitigates our bad mood and makes us more productive.

This easy trick can help ease your negative mood, putting you in a more positive mental state.

**Suggested** **further** **reading:** ** _How_** **_to_** **_Be_** **_a_** **_Positive_** **_Leader_** **by Jane** **E.** **Dutton**

_How_ _to_ _Be_ _a_ _Positive_ _Leader_ examines cutting-edge research from the field of positive organizational behavior, in which companies aim to foster both a positive attitude to work and high performance among employees. The research is complemented with vivid examples from real organizations.
---

### Margaret Greenberg and Senia Maymin

Margaret Greenberg is an executive coach to Fortune 500 firms and founder of The Greenberg Group consultancy. Senia Maymin has a Stanford PhD in organizational behavior and has consulted for clients including Google and Intel.

