---
id: 5681b981704a880007000055
slug: project-animal-farm-en
published_date: 2015-12-31T00:00:00.000+00:00
author: Sonia Faruqi
title: Project Animal Farm
subtitle: An Accidental Journey into the Secret World of Farming and the Truth About Our Food
main_color: 8B9038
text_color: 595C24
---

# Project Animal Farm

_An Accidental Journey into the Secret World of Farming and the Truth About Our Food_

**Sonia Faruqi**

_Project Animal Farm_ (2015) is all about one of the most harmful industries of our time: the modern mass production of meat, eggs and milk. It delves into the horrible conditions farm animals are kept in and the dangerous effects of factory farming on humans, animals and the environment.

---
### 1. What’s in it for me? Discover the harsh reality of mass-produced dairy and meat, and learn about alternatives for the future. 

Humans have kept animals for meat, dairy and eggs for a substantial part of our history. When we think of farms now we conjure up images of green pastures, rolling hills and animals roaming free, but does this resemble reality?

The short answer is no. Today's farms are more likely to resemble the factories of early industrial times, both for the animals and the people who work there. So what has changed and what will the future look like?

These blinks take us on a journey into the world of modern factory farming and animal keeping. We learn the harsh truth, how it affects us and what the alternatives are.

In these blinks, you'll discover

  * how laying 300 eggs per year affects a hens anatomy;

  * why an hour in a modern factory farm can be lethal to people with asthma; and

  * why Roger Harley was offered $250,000 to stop farming.

### 2. Mass production, on the rise for decades, is harmful to both humans and animals. 

Have you ever wondered where the meat in your supermarket comes from? And why are mega packs of meat so cheap? You probably know it's the result of mass production, but how exactly does that work?

Mass production is a worldwide phenomenon. In the last few decades, there's been a global shift away from small pastoral farms to large factory-style plants that allow us to produce more meat than ever before. 

The food industry has become globalized. People in China now eat chicken breast produced in the United States and American farmers feed their cows on food grown in Brazil. 

However, this newly globalized mass production causes a number of problems for both humans and animals. 

Workers in these mega plants have to come face to face with some of the cruelest conditions for animals on the planet. They have to learn to mentally shut themselves off just to continue their work. In fact, many of the employees at factory farms become vegetarians. 

Factory-farm employees also face physical dangers at work. They're exposed to high levels of ammonia from the animals' urine and feces, which often results in a chronic or even incurable cough.

If you have asthma, you can die if you're exposed to these fumes unprotected for just one hour. 

Of course, these plants are also awful for the animals, who don't have the option to leave like the human workers do. They live out the entirety of their short lives in constant misery.

### 3. Chickens on factory farms are kept in tiny cages, mutilated and left in fear. 

Many people break out in tears when they enter an egg factory for the first time. Why is that?

Well, the chickens in these factories are kept in tiny battery cages that are lightless and dirty. When Sonia Faruqi, the author, was researching egg factories, she found that four to eight chickens were sometimes stuffed into cages the size of a microwave. The cages are stacked on top of each other and rarely or never cleaned, with up to 300,000 animals in one factory. 

The chickens also live in a constant state of filth. They're often surrounded by dead birds because workers are too lazy to remove corpses, and their cages are kept dark because it makes the birds slightly calmer. Most birds in egg factories never experience the sun. 

And what happens to the mental state of these birds? 

The birds go mad from the conditions they're forced to live in. They try to peck each other to death, so many farmers cut their highly sensitive beaks off with hot knives.

But the animals still peck each other raw even after their beaks are mutilated. They sometimes even start eating each other alive. 

Chickens are also genetically modified to produce more eggs and meat, with horrible consequences. They're bred to lay as many eggs as possible or have massive breasts. Hens die because their insides get pushed out of their bodies from the pressure of laying over 300 eggs per year, and some birds can't walk because their chests are so large their bodies can't carry their weight. 

And though poultry conditions are terrible, pigs have it even worse.

> _"Animal use has become synonymous with animal abuse."_

### 4. Pig factories are more like torture dungeons than production plants. 

The pig is one of the world's most intelligent and social animals. They love to interact with others and can build strong, emotional relationships. So how are they treated in the factory-farm system?

The meat industry views pigs purely as tools of production, not as living beings, and it shows. Pigs are bred to give more meat and have bigger litters than usual; today, they're fatter than ever and have piglets in more rapid cycles. 

Larger pigs are kept in tiny crates where they're unable to move, and are often internally probed and medicated by humans. A sow can be up to six feet long and weigh over 500 pounds, but these intelligent animals, who love to move around, are kept in filthy crates so small that they have to stand because there isn't enough room to lie down. 

When sows are due to farrow, some farmers stick their entire arm into the womb to speed up the birthing process. This rarely helps and only leaves the sow in more pain. 

Sows are also often heavily medicated to kill bacteria and increase their appetites. In fact, 70 percent of the antibiotics used in the United States are used on animals that don't need them, which has increased antibiotic resistance. Their babies often go unmedicated, however, even when they _do_ need it. 

Like chickens, piglets are often mutilated to prevent fighting. Many factories cut their piglets' tails off at a young age so they can't be bitten by their littermates. Some farmers even castrate young piglets without anesthetic. A European declaration of goals aims to have this practice discontinued continent-wide by 2018 because of the intense pain it causes the animals.

> _**"** The crates could also be called coffins."_

### 5. Slaughter houses are traumatic, dangerous and deadly for both animals and humans. 

We've looked at the conditions for live animals, but how do their tragic lives end?

Unfortunately, the kill process is also very difficult for them. Slaughter is often done incorrectly and goes unchecked by inspectors. 

To be properly slaughtered, animals are supposed to be stunned first, then killed within the following 15 seconds. This rarely happens, however. 

Most workers aren't properly trained to kill animals painlessly. They also tend to care little about stunning them, so they kill them when they're still conscious. 

Some workers even inflict this suffering just to save themselves a few minutes and get home earlier. They may do that by not stunning them properly or by cutting into them while they're still alive. 

Inspectors are supposed to check the slaughter process every few hours and report or penalize any violation of protective laws, but they are often employees of the very slaughterhouses they're inspecting, and are therefore incentivized _not_ to report anything. 

Animal slaughter also has a big impact on the butchers. Workers in factory farms are constantly surrounded by blood, cruelty and death. Many don't have job prospects other than killing for a living, and their workplace environment can leave a lasting impression on their psyches. 

It's common for employees to quit after only a month in the slaughter business. Those that manage to get through the first year often suffer from severe mental problems and have to protect themselves against the dangers of their work. 

Nader, a man the author spoke to, has worked most of his life in slaughter. His body is covered in scars from work accidents, he has a number of psychological problems and he can't stand straight without a back brace because a large metal hook once fell on him, leaving him permanently debilitated.

### 6. “Free-range” farms aren’t tightly regulated in the United States and Canada. 

When you go into the supermarket, you'll find eggs with different labels. Some are organic and others are _free-range_, but what does "free-range" actually mean?

In the United States and Canada, "free-range" simply means that the animals have some kind of access to the outdoors. Outdoor access isn't clearly defined, however, as the law lacks rules about what that access is supposed to look like. 

Factory farms also have very loose rules, which sometimes aren't even adhered to. Lots of plants have small, dark backyards that the animals are rarely allowed to roam, or don't want to enter because of the state of them. 

Yet this kind of vague outdoor access is still considered enough for the factories to be termed "free-range."

When the author visited one turkey farm, she discovered that the animals were deprived of outdoor access for months because of a broken fence. None of them had ever seen the sun, and yet these were still "free-range" turkeys. 

Laws regulating organic production aren't sufficient either. Animals on organic farms in the United States and Canada need to be allowed outside at least 120 days a year, but this is still much worse than many European farms, where organic farmers are required to let their animals out every other day. 

Standards for organic farms in Canada are flimsy too. Cows there can still be tethered by the neck and kept in small cubicles, restricting their movement and forcing them to stand on their own feces for most of their lives. That practice has been banned even in the United States, but Canada still lags behind.

### 7. Organic dairy cows spend most of their lives being prodded by electric sticks and are rarely allowed outside. 

We've seen that the United States and Canada have weak regulations when it comes to free-range farms, but sadly the laws for organic farms are still far from rigorous. 

Organic dairy cows are treated better than cows in factories, but their treatment is still sub-par. You might assume that organic farmers want their animals to live happily because they allow the cattle to go outside for 120 days; the reality, however, is quite different.

Most cows are only allowed outside for exactly 120 days, so the farmers can meet the minimum requirement. This is an improvement on the conditions of animals kept in small pens for their whole lives, but it's still not enough to keep them happy. 

And when the animals are kept _inside_, they're still subjected to terrible living conditions. Dairy cows spend most of their lives in tiny stalls where they can't turn around. If they try to move, they're shocked with electric sticks. They're also often tied by their necks for days and separated from each other. Their excrement collects in gutters in their stalls, which aren't cleaned for days or weeks. 

The animals aren't able to communicate or have any physical contact with each other either. This is particularly hard for calves, who are as social as puppies. 

In Europe, calves are kept separate from each other for their first eight weeks, but they can't be separated from other calves after that. In the United States, calves can be kept in isolation for up to six months. 

Artificial insemination is also a common practice for ensuring that the cows keep producing milk. Cows are artificially inseminated more often than other animals and there's a big market for bull semen, as a single bull can father thousands of calves. That dramatically decreases the species' genetic diversity — a further danger for future generations.

### 8. Some farms do care for their animals, but they’re constantly under threat from big companies and ineffective public policies. 

In recent years, a new movement of small farms that want to care for their animals properly has emerged. Such farmers are against the horrible conditions animals are subjected to in factory farms.

Animals on these new farms are allowed to live the way they want. Roger Harley's farm in Canada, for example, seems like heaven on earth compared to a factory farm. He's an organic pasture farmer and his animals have enough space to roam for acres. 

Harley's animals are much happier and healthier because of this. They can withstand disease and cold much better than animals kept in large factories. His farm also operates without artificial insemination, genetically enhanced breeding, force-feeding or cruelty. So why aren't there more farms like this?

Well, publicly fighting for animal welfare means that organic farms are constantly under threat from larger companies. In fact, Harley has been approached and asked to discontinue his efforts several times. He was even offered $250,000 per year just to keep quiet. 

The goliaths of the agricultural industry are constantly waging a war against smaller pastoral farmers like Harley. 

The process of giving out "organic" farm licenses is also completely dysfunctional. "Organic" licenses are so vague that they're often little more than a sham.

Wealthy farmers hire lawyers to find out the minimum they have to do in order to be certified as organic. Inspectors that give out certifications are also often employed by the farms themselves, since the state delegates that responsibility to private organizations rather than a government agency.

### 9. Factory farms are breeding grounds for diseases that are dangerous for both animals and humans. 

You've probably heard of avian flu or swine flu, which are both side effects of meat production. 

The terrible conditions the animals are kept in are breeding grounds for disease. Animals are caged, mistreated, genetically modified and constantly under stress — a paradise for viruses!

The diseases that circulate in factory farms have also grown resistant to antibiotics because of the unnecessary medical treatment doled out to animals. The genetically modified animals are essentially a laboratory for viruses to develop themselves, and the viruses sometimes become deadly enough to kill millions of animals — and potentially humans too. 

Animals on factory farms are also more susceptible to disease because of their unnatural genetic make up. 

This is why biosecurity is a top priority on factory farms. When someone enters a plant, they have to go through some kind of procedure to get rid of the germs they may be carrying. In some Western farms, that could just mean taking a shower, but in Asian countries like Malaysia, Indonesia or Singapore, the process can be much more complex. 

In some places, farmworkers have to be quarantined and monitored for a month before they're allowed to enter the farm. They're essentially caged in the same way as the animals!

There's a good reason for this high level of concern: there have been several epidemics in the past. Just a few years ago there was an outbreak of _H1N1_, or _swine flu_, which killed humans and animals alike. 

There have also been several outbreaks in chicken, pig and cow farms that have resulted in millions of animal deaths. In Indonesia, people even burned sick chickens alive to prevent their diseases from spreading.

### 10. Meat consumption has reached unprecedented new highs and continues to rise, thus contributing to obesity and environmental pollution. 

Meat was a rare commodity a hundred years ago, but today most Americans eat it on a daily basis. This has profound implications for our future. 

Meat consumption is increasing in many parts of the world. The wealthier people get, the more meat they eat. In Singapore, for example, the average meat consumption is 160 pounds per person per year — and it's still on the rise. 

In China, the average person eats about 115 pounds of meat per year. Actually, Chinese meat consumption is over half the total meat consumption of all developed countries!

In the United States, the average person eats about 175 pounds of meat per year, though the American population is only a quarter of the Chinese population.

This rise in meat consumption is causing a number of problems. The mass production of meat is very bad for the environment: greenhouse gases produced on factory farms are one of the biggest causes of global warming. 

Caged animals also produce poisonous manure, which many farms carelessly dump into rivers or oceans. In fact, a single factory farm can create as much harmful waste as an entire city. 

Humans who eat meat every day also face more personal consequences, especially because high meat consumption is one of the causes of obesity. Several countries in the world currently face problems with obesity. In the United States, one-third of adults are obese; in Malaysia, one-seventh. 

We could reduce this by living healthier lifestyles and shifting to more plant-based diets, but unfortunately, the opposite is happening.

### 11. We could improve the agricultural sector by using smaller farms, having more compassion for animals and involving more women in the industry. 

We've seen several examples of bad farms, but what's the answer to all this? What style of farming should we switch to?

There _is_ a way to make use of farm animals without causing them any suffering. The author found one viable answer when she visited The Lemon Dairy and Egg farm in Belize.

The Lemon and Dairy Egg farm is a small, family-run establishment that allows cows and chickens to pasture freely alongside each other. The farmers love and respect their animals: they name them, build relationships with them and care for every aspect of their lives. They truly respect their animals and don't have any motivation to mistreat them. 

The farm is small, but it could easily expand. It's still possible to manage thousands of chickens without keeping them in tiny cages and torturing them. 

There should also be more women involved in farming and the agricultural industry in general. That might sound discriminatory or sexist, but it's been proven that women tend to be more sensitive and compassionate than men. They also consume more organic products and eat less meat. 

In the agriculture business, a bit of compassion for animals would go a long way, and improve things for humans and the environment too. Unfortunately, most factory farmworkers are men. 

Cal-Maine, the largest egg producer in the United States, for example, only has one woman on its 20-member leadership team.

Despite the lack of female representation in the agricultural business, most food is actually purchased by women. If women are the ones purchasing most of the industry's goods, shouldn't they have more influence in the way their food is produced?

### 12. We have to change our consumption behavior, return to pastoral farming and regulate the industry more tightly. 

We need to change a number of things about the food industry to ensure a healthier and more sustainable tomorrow. 

First, we need a serious shift in our behavior as consumers. These days, most people base their purchasing decisions entirely on price. They don't think too much about the lives of the animals that end up in their shopping carts. 

That means a lot of people inadvertently support the factory-farm business, often without knowing how destructive it is. Consumers need to educate themselves and be more mindful about the origin of the products they buy.

You can also cut down your meat consumption. Try having _meatless Mondays_, where you abstain from meat on the first day of the week. Replace meat with healthy vegetables whenever you can. 

We also need a return to smaller-scale pastoral farming. It might seem like pastoral farms are too small to feed the world, but there was a time when humans _only_ used pastoral farming! Our modern technology and knowledge can be used to expand pastoral farming rather than factory farming. 

An increase in pastoral farming would ensure that farm animals live happier and healthier lives. It would also be better for the environment and prevent the spread of disease. 

Agricultural laws and regulations need to be stricter too. There are currently big loopholes in laws that allow factory-farm workers to get away with multiple atrocities. We shouldn't torture animals just to save a few cents. 

Food labels also need to be regulated more tightly so that they're more meaningful. Vague terms like "free-range" are doing us little good. 

All in all, we have a lot of work to do if we want to make the world a better place for ourselves and our animals, but we have no other choice if we want a better future.

### 13. Final summary 

The key message in this book:

**Modern factory farming is unsustainable and harmful to livestock, human consumers and Earth itself. We need to drastically reform our agricultural industry by returning to smaller pastoral farms. By doing so we would improve our physical health, reduce the risk of infectious diseases, provide better lives for the animals we depend on and reduce the emission of greenhouse gases.**

Actionable advice:

**Find out how your food was produced.**

The first step is checking food labels more closely. Learn more about the specifics of "organic" labels in your country. If the label doesn't provide any information, reconsider. Think about what kind of company your money is supporting. 

**Suggested** **further** **reading:** ** _Eating Animals_** **by Jonathan Safran Foer**

_Eating Animals_ offers a comprehensive view of the modern meat industry and demonstrates how the entire production process has been so completely perverted that it is unrecognizable as farming anymore.

The book explains the moral and environmental costs incurred to achieve today's incredibly low meat prices.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Sonia Faruqi

Sonia Faruqi is a Dartmouth graduate and former Wall Street investment banker who left her corporate job to help reform the food industry and improve the lives of people and animals everywhere.

