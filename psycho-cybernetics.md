---
id: 599c37d9b238e10006a126cd
slug: psycho-cybernetics-en
published_date: 2017-08-25T00:00:00.000+00:00
author: Maxwell Maltz
title: Psycho-Cybernetics
subtitle: A New Technique for Using Your Subconscious Power
main_color: CD2931
text_color: CD2931
---

# Psycho-Cybernetics

_A New Technique for Using Your Subconscious Power_

**Maxwell Maltz**

_Psycho-Cybernetics_ (1960) is about human self-image, how it's crafted and how it can drastically affect your happiness and success. These blinks describe how to use machine principles to feed your mind the right data and steer yourself toward a fulfilling life.

---
### 1. What’s in it for me? Leave your past self behind. 

What kind of person are you? Are you perpetually tardy, type-A personality, or maybe a poor writer? Where did those beliefs come from? A judgmental teacher, a circle of friends who label you a certain way, or maybe from yourself?

We all have an image of the type of person we are. Some of these are positive, and some are detrimental to moving forward and making changes. We use stories to understand the world, but sometimes we make ourselves the villain without thinking twice about the consequences of that narrative. So get out your editing pen; it's time to make some changes.

In these blinks, you'll learn

  * how we're all affected by hypnosis;

  * how you can practice the piano in your head; and

  * why you might not be a poor math student after all.

### 2. We act according to the image we create of ourselves. 

Each person has a _self-image_ — a mental blueprint that describes the kind of person she is. This all-important conception of self contains the beliefs she holds about herself based on past experiences, successes and failures.

Such perceptions are crucial since people act like the person they believe themselves to be. If you think of yourself as a failure, you'll quite likely fail. Conversely, if you consider yourself successful, you'll find ways to succeed.

But why are these life-determining self-images formed in the first place?

Well, it's usually for a logical reason. A person might consider herself a failure because she had a bad year in school after her parents got divorced. While some people can easily recover from such an event and be back on the honor roll in no time, others find themselves trapped in a self-image that tells them they're the kind of student who gets Fs. In this way, whether good or bad, the formation of a self-image is crucial to everything that follows in a person's life.

Here's another example: the author knew a man who suffered from his 'ugly' looks; he felt his nose was too large and that his ears stuck out. He suffered because he felt other people were judging him for his peculiar looks.

In reality, however, people weren't judging him for his appearance. Rather, it was his own negative self-image that made him cripplingly insecure and miserable. In this case, his perception that others were spiteful toward him made him spiteful back. However, this was a loop that could be broken if he turned his negative self-image around.

We'll learn more about these loops in the next blink.

### 3. We’re hypnotized by negative beliefs; the power of rational thinking can lay them to rest. 

You probably think of hypnosis as a stunt for an oddball circus act. But every human is hypnotized in one way or another.

After all, the power of hypnosis comes from belief, and you already know how powerful beliefs can be. Just consider a friend of the author, Dr. Alfred Adler, who did poorly at math in school, leading his teacher to believe he had no talent for the subject.

Adler accepted this as a fact, and the grades he earned proved his teacher right. But one day, he suddenly understood how to solve a complex equation. When he showed the solution to his teacher, he realized that he had no trouble grasping arithmetic. His confidence soon increased, and he became a better math student.

What changed?

Adler had been hypnotized by a false belief about himself. Studies have proven that Adler's experience is typical of many low-achieving students.

People experience both positive and negative beliefs that hypnotize them in similar ways, and the goal is therefore to overcome the preconceptions that hold us back. To do so, you need only harness the power of rational thinking.

Your negative beliefs aren't a result of facts or experiences, rather the conclusions you draw from them. Therefore, rational thinking — which is logical and conscious — can be used to control your unconscious mind.

To start uprooting the beliefs that create your sense of inferiority, ask, "why?" Doing so will help you overcome irrational and false assessments like, "I failed yesterday; therefore I'll fail today." Instead, when faced with such a thought, you'll be able to see that each day presents an opportunity to learn from the past.

### 4. The human body contains a success mechanism that’s driven by imagination. 

People have often theorized about the body as a machine — a concept that isn't too far from the truth. While humans aren't machines per se, we each have a machine that we can use when necessary.

The author calls this phenomenon _psycho-cybernetics_, a conception of the human brain and nervous system as a _servo-mechanism_ or automatic response that processes negative feedback to guide its course. This theory operates according to the principles of _cybernetics_, the study of machines and mechanics.

By applying psycho-cybernetics, we can uncover new insights into why and how humans behave the way they do. One such insight is that humans have a built-in mechanism for success.

Consider how a baby might try to grab a rattle. He can't call on any _stored information_ based on prior experiences. Therefore, his hand has to swipe back and forth until he can reach the object. Once he succeeds in grabbing the toy or, in other words, achieves a _successful response_, he'll store the memory for future reference. Over time, he can refine his skill at grasping, slowly remembering his successes and forgetting his failures.

Similarly, this kind of success mechanism is at play as we work toward our goals. Naturally, activating this mechanism is beneficial and, to begin doing so, we need to use our imagination.

The human nervous system can't tell the difference between experiences that we imagine and ones that actually occur. As a result, it reacts according to what we believe or imagine to be true.

Research by Dr. Theodore Xenophon Barber at American University in Washington during the 1950s found that hypnotized subjects easily underwent surgery without anesthesia after being told they could not feel pain.

Or take Artur Schnabel, a world-famous concert pianist who rarely practiced on an actual piano. Instead, he would simply refine his art in his head. There are also comparable examples of a darts player and a golfer who improved their skills through mental practice.

### 5. We can stimulate creativity and make ourselves happy. 

Whether it's writer's block or a stalled brainstorming session, it's common to hit obstacles when pursuing a creative project. Most creative practitioners know that when such a roadblock presents itself, the only thing they can do is relax and wait for inspiration to return.

This wisdom makes perfect sense since humans have a highly specific creative mechanism. It only activates when we become interested in solving a particular problem, consciously consider the dilemma and gather every piece of relevant information.

Once we've struggled with the problem sufficiently, we reach the point at which further thinking only holds us back. Then our built-in creative instinct kicks in. As a result, we have our best ideas when we're not actively working.

Take the famous inventor, Thomas Edison. Whenever he came up against an issue he couldn't resolve, he would take a short nap. After these brief respites, he often found the solution he sought.

So, while creativity is unreliable, we can reboot it by altering our actions and thoughts. And the same goes for happiness. Here's how:

Most people think of happiness in terms of the future. We think we'll be happy once we're married, or get a better job. But happiness must be practiced in the present. We first need to recognize that happiness is an entirely internal feeling, a product of our thoughts and the attitudes we hold about them.

If we become unhappy when a driver honks at us, it's because we chose to respond with annoyance and frustration, which saps our happiness. We can develop a habit of happiness by fixating on positive points and letting go of negative ones.

Edison provides another good example. He once lost his multi-million-dollar laboratory in a fire and had no insurance for compensation. Nonetheless, he made the decision to avoid unhappiness and instead began rebuilding first thing the next day.

### 6. Seven elements make up a successful personality. 

Success isn't something you simply find any more than failure is something you "arrive at." Rather, failure and success are the result of forces buried deep within your personality.

Both are produced by unique sets of elements. First up you'll learn about _SUCCESS_, an acronym that explains the core elements of a successful personality.

First comes a _Sense of direction_. After all, every human is a goal-seeker and, just like a mountain climber, you need an objective to pursue. When you reach the proverbial top, you should simply forge ahead to the next peak.

The second trait is _Understanding_. Most failures are a result of _mis_ understandings, as people often distort sensory perceptions with fear, anxiety or desire. Just imagine you see two colleagues in conversation. Right as you walk by, they look up and stop talking.

You might easily conclude that they were gossiping about you, but that's unlikely. It's just one example of how feelings can alter your perceptions.

The third necessary trait for success is _Courage_. After all, only by taking courageous actions can you make your dreams a reality. You need the nerve to back up your ideas by taking calculated risks. If you delay taking action until you're entirely certain, you'll never accomplish anything.

The fourth characteristic is _Charity_ since successful people are attentive to the problems, needs, dignity and respect of every human.

Fifth comes _Esteem_ because, if you think "I can't do it," you'll never move ahead. As a result, a low self-opinion is never a virtue, but rather a threat to your success.

Sixth is _Self-confidence_. Such a feeling is built upon experiencing success, which makes it essential to remember your past triumphs and forget your failures, which threaten to wreck your confidence.

And finally, _Self-acceptance_ is the last pillar of success. Without coming to terms with yourself, your faults and weaknesses, you'll never be able to attain what you desire.

These seven elements compose a successful personality. Next up you'll learn the seven traits that cause a person to fail.

### 7. Being aware of what causes failure is key to achieving success. 

The factors that lead to failure can be readily summed up by an acronym of the word _FAILURE_ itself.

First comes _Frustration_, which arises when you feel you can't realize an important goal. This produces a self-fulfilling prophecy as you think of yourself as incapable and are therefore more likely to fail.

Next is _Aggressiveness_. This energy _can_ help you achieve your goals, but in a failure-type personality, all that aggression is directed toward self-destructive tendencies like worry and rudeness, rather than a worthy aim.

Third comes _Insecurity_ or feeling like you don't live up to requirements. Interestingly enough, insecurity is often caused not by a real lack of ability, but by a distorted system of measurement. In other words, if you compare yourself to an imaginary, likely impossible ideal, you'll never feel self-assurance.

Fourth up is _Loneliness_, the feeling of being alienated from others. Lonely people cut themselves off from the roads that could lead to a healthy social environment, fail to act and struggle to achieve.

The fifth trait of failure is _Uncertainty_ or avoiding mistakes by never making decisions at all. It's a failure to act and achieve goals, precipitated by the false premise that you shouldn't act if you're not certain you'll succeed. Since no certain path can be found, no decision is made, and you're safe from failure — or success, for that matter.

Sixth comes _Resentment_, an attempt to make personal failure more digestible by blaming it on unfair treatment.

And last is _Emptiness_, a symptom of failure itself. This feeling results from the sense that life is boring and no pursuit is worth attaining.

If you notice any or all of the above traits in yourself, it's time to make a change. Next up you'll learn precisely how to go about it.

### 8. Attend to your emotions and unleash your real personality. 

When you incur a physical injury, your body forms a scar to protect the damaged area. In this way, scar tissue is nature's way of preventing further injuries to the same place.

Similarly, people tend to form emotional scars to protect hurt feelings. However, such scars don't just protect you from the person who initially hurt you; they can also separate you from all other people. After all, they construct an emotional wall that keeps out friends and foes alike.

That being said, you can overcome this by offering yourself an _emotional face lift_. This process is like doing surgery on your emotions, but rather than a scalpel, forgiveness is your primary tool.

Just like a sharp surgical implement, genuine forgiveness can cut away old hurts, allowing healing to occur. But to truly forgive, you must forget the feeling that you were wronged and even forget the act of forgiveness itself.

Then, once you've removed your emotional scars, you can let loose your real personality, which comes from within. Many people have inhibited personalities that prevent them from exercising their creative potential and expressing their true selves.

For instance, stuttering is often a result of inhibition. This speech impediment is caused by an excessive tendency to self-monitor, which generates too much negative feedback.

It's a good example of how inhibiting a fixation on what others think and want from you can be.

To get past this debilitating feeling, it's necessary to practice _disinhibition_. Interestingly enough, while people like to say you should think before you speak, the exact opposite is required. By speaking before you think, you can banish self-conscious thoughts like "should I have done that?" This is the first step on your path to truly unlocking your creative personality.

### 9. Use these two simple techniques to find peace of mind in a world full of interruptions. 

Imagine you're at home, quietly reading a novel. Your phone starts ringing, and you instinctively jump out of your favorite chair, rushing to answer the call.

In the blink of an eye, you went from a peaceful state of mind to one of unrest. And who can blame you? The ring tone is a signal that you've learned to obey. But the truth is, you don't have to answer the phone; you could just as easily ignore it.

Therein lies the first technique for achieving peace of mind: start avoiding the different _bells_ that ring around you. This term refers to all the disruptive stimuli that populate your environment. Bells are things to which you have been conditioned to respond by sheer force of habit.

For instance, parental warnings result in most children fearing strangers. While such a tendency might serve a reasonable function in kids, it gets carried over into adulthood, causing lots of people to feel uncomfortable around others they don't know.

Strangers have become bells to which the learned response is fear and avoidance.

However, you can overcome these conditioned responses by practicing relaxation instead. In other words, if you want to, you can learn to let the bell ring.

A good way to ease into this practice is to delay your response, say by counting to three before answering the phone. From there you can slowly extend the time until you eventually ignore the bell altogether.

Another way to achieve peace of mind is to construct a mental quiet room that offers you a retreat from the world. You can furnish it with whatever you find relaxing — maybe a beautiful landscape, or your favorite piece of art. Then, whenever you have a few moments to spare between appointments or while riding the bus, calm your mind in your quiet room.

### 10. Final summary 

The key message in this book:

**Humans may not be machines, but we can think of our mental processes as mechanized. By using cybernetic principles to understand this machine thinking, we can overcome negative ideas about ourselves, enhance our self-image and live a fulfilling, successful life.**

Actionable advice:

**Use a mental picture of heaviness to relax.**

Sit down in a comfortable chair or lie flat on your back. Imagine that your legs are made out of concrete and envision yourself sitting or lying in place with these heavy extremities. As you sink further down into the floor or the mattress from the sheer weight of your legs, you start to release the tension in your body. For a total of 30 minutes a day, repeat this exercise with your arms, hands, neck and any other body part you want to relax.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Magic of Thinking Big_** **by David J. Schwartz**

_The_ _Magic_ _of_ _Thinking_ _Big_ unveils why believing in ourselves is a pivotal key to success, and how we're each capable of achieving any of the goals we've always dreamed of. The author's methodology is supported by his work as a professor and leadership counselor, as well as by his innumerable interactions with people and businesses that have seen both sides of the success-failure coin.
---

### Maxwell Maltz

Maxwell Maltz (1889–1975) was an American cosmetic surgeon. In addition to the bestselling _Psycho-Cybernetics_, he wrote an autobiography, numerous self-help books, a novel and a play.

