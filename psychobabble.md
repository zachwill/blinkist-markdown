---
id: 53f485e03433390008120100
slug: psychobabble-en
published_date: 2014-08-19T00:00:00.000+00:00
author: Stephen Briers
title: Psychobabble
subtitle: Exploding the Myths of the Self-Help Generation
main_color: D8832B
text_color: 8C551C
---

# Psychobabble

_Exploding the Myths of the Self-Help Generation_

**Stephen Briers**

_Psychobabble_ explains how the self-help industry is misleading people, and why the human mind can't be swayed by catchy self-help mantras and lucid pop-psychology diagrams alone.

---
### 1. What’s in it for me? Find out why so many self-help manuals fall flat. 

Pop psychology and self-help books claim to have all of the answers to all of your problems.

Are you struggling at work? Fake it till you make it! Do you suffer from depression? Just change your attitude! Is chemotherapy not curing your cancer? Just imagine all of those cancerous cells imploding on themselves! It's that easy!

Considering how "easy" it is to cure our woes with self-help manuals, and considering how many people actually read self-help material, you'd think that by now we'd live in a paradisical utopia. And yet we don't. Why?

That's because self-help often advocates amazingly simplistic philosophies, which then produce woefully inadequate strategies for our complex lives.

It's no wonder that all too often neither their basic assumptions nor their ultimate solutions stand up to scientific scrutiny.

And not only are the pompous promises found in pop-psych books ineffective, they're sometimes downright harmful. They raise our expectations to the point where we can no longer be content with anything that is less than perfect, which is simply not possible in an intricate world with so many influences and actors.

In these blinks, you'll discover

  * why you might make better decisions when you wake up on the wrong side of the bed;

  * why spotting negative thoughts is a lot like finding a flea;

  * how useless it is to try to keep a train from flying off the rails; and

  * why, no matter what, we can't all be winners.

### 2. Self-esteem is overrated. 

It seems obvious that self-esteem is critical for our happiness. After all, it's good to feel good about ourselves, right? But how important is high self-esteem _really_? Is it an integral part of every successful and healthy person, as self-help books want you to believe?

In pop psychology books, poor self-esteem is blamed for a whole slew of problems. For example, authors attribute underachievement at school or work to the student's or employee's underdeveloped belief in himself and his abilities.

In the same vein, some popular self-help books link marital problems to one or more partners' lack of self-respect.

Many texts suggest that even school bullies resort to tyrannizing other kids and extorting their lunch money merely as a means to improve their own painfully low self-esteem. The assumption is that, by dominating their peers, bullies can enhance their own lack of self-worth.

Yet for many behavioral problems, it turns out that self-esteem just isn't an issue.

For example, research has shown that there is no link between a teen's self-esteem and problematic behaviors like stealing, excessive drinking and promiscuity.

Going back to the playground, bullies have been shown to be even surer of themselves than their peers, rather than the pitiable, doubt-stricken souls we might otherwise imagine them to be.

Moreover, neither a person's job performance nor their relationship skills are affected by their level of self-esteem. Employees with a high self-esteem, for example, don't necessarily outperform others in either their jobs or in laboratory tasks.

What's more, psychological programs that aim to boost students' self-esteem do nothing to improve their performance in school. In fact, when struggling college students received messages intended to bolster their self-esteem, they actually did even _worse_ in the exams that followed.

It would seem that people with high self-esteem can have problems, too. If we're interested at all in the taxonomy of our problems, then we'll have to look for something better than the catch-all "self-esteem."

### 3. Behaving too assertively is just as bad as behaving too meekly. 

Here's a question: in what way are a bullied school kid and an aspiring executive the same?

Answer: both are likely to be told to be more assertive.

_Meek_ people — that is, gentle-natured people who will not fight or compete if they can avoid it — can profit from assertiveness training, enabling them to better stand up for themselves.

Sometimes, however, behaving more assertively can be overkill.

In fact, high assertiveness comes with considerable social costs. For instance, highly assertive people are considered by their peers to be less friendly, and also enjoy less popularity.

And because extremely assertive people tend to strive to achieve their goals irrespective of the interests of those around them, they are more prone to insult or annoy others and even inadvertently isolate themselves from their peers.

Imagine, for example, a highly assertive kid who insists on playing soccer even though all his friends would rather play detective. He may eventually get what he wants, but it's equally possible that he just pushes his friends away.

This is just as true in the business world as it is on the playground, as was demonstrated in one study where participants were asked to assess their managers' leadership abilities. They found that extremely assertive managers tended to be rated as poorer readers — just like their meeker counterparts.

In addition, assertiveness can disrupt the flow of any social group. Indeed, the group just can't get anything done if everyone is trying to push through their own agendas. If no one is willing to give others the upper hand, to compromise, cooperate or yield to an authority, then things will quickly devolve into drawn-out power struggles, which waste the group's time and energy.

Just imagine that you're in a group working on a short film: if no one is willing to make compromises, then you may never even get past the planning stage and actually start filming!

Regrettably, a strong ego and a strong will are not the solutions for every problem. This complicates our most important task: managing our fates. The following blinks show how this works.

### 4. Meditation or changing your mindset will do little to change your outcomes if you have cancer. 

At one point or another, we've all heard some tear-jerking story about cancer patients who, by visualizing imploding malignant cells or working their way through childhood trauma, find themselves tumor-free with a long, healthy life ahead of them.

But did those people really "think away" their illness? And what about the countless other people who finally succumbed to their illnesses: did they bring it upon themselves by thinking the wrong thoughts?

The reality is quite straightforward: psychotherapy and meditation don't affect disease progression or survival rates in cancer patients.

This is evidenced by several well-designed studies that set out to find _any_ demonstrable effect psychotherapy might have on the survival rates of those suffering from cancer. According to most of the studies, there was absolutely no effect on survival.

It should be noted, however, that therapy did significantly improve the patients' quality of life.

Likewise, meditation had no effect on the progression of the disease. It did, however, help to reduce the pain brought on by the disease and the treatment.

All of this is not too surprising really, since a cancer patients' attitude does not influence their prognosis. For example, studies show that a patient's chances of recovery are completely unaffected by her mood or attitude.

Likewise, large studies have found no evidence that a fighting spirit helps patients to reduce recurrence rates or even survive longer.

While it would certainly be wonderful if we could overcome our maladies by sheer thought alone, statistically speaking, this happens at best only rarely. It's more likely to be coincidence.

### 5. We overestimate the impact of parenting on a child’s personality. 

Have you ever seen one of those horror movies where an excited, good-natured mother gives birth to an evil child? Thankfully, this can't happen in real life: a child's personality isn't predetermined, and depends mostly on his upbringing — or does it?

Actually, our personalities are strongly influenced by our genetic makeup. Identical twins, for example, share many personality traits even if they are raised apart. In fact, their personality traits, such as their their degree of self-control, correlate 75 percent of the time when they are raised apart and an astounding 85 percent of the time when they are raised in the same home.

These statistics suggest that only a fraction of the variations between them depend on how the twins were raised in their separate homes.

However, it is quite clear that a child's development can be impaired by abuse or neglect.

But it's not always the case that parenting influences a child's character. Sometimes it's the other way around! This can lead investigators to overestimate a parent's influence over their child's development.

This is because parents adapt their parenting style to a child's inborn traits. For example, it's possible that naturally compliant children are punished less than their headstrong siblings simply because they'll behave well anyway.

Scientists might then erroneously conclude that a non-authoritative parenting style leads to compliance in children.

Thus, even when there are correlations between parenting styles and a child's behavior, you can't necessarily determine the cause, i.e., whether the child or the parent has more influence over the child's outcomes.

In addition, when environment does actually affect a child's development, there's more to that environment than just the family. Parents, therefore, are not always responsible for these developments.

For many hours a day, kids are exposed to other children, teachers and media.

We cannot hold parents completely responsible for the impact of extrafamilial environments, as there is much that lies beyond their control. For example, even in a carefully chosen school, you can't necessarily avoid the ridicule of bullies.

> _"The degree to which we are affected and influenced by other people is actually quite terrifying."_

### 6. You’re weaker than you think. 

If you've ever flipped through any self-help manual you probably found "advice" that reads something like this: With the right mindset and lots of practice, you can accomplish _anything_!

What a load of rubbish!

The fact is that there are some goals that are simply out of your reach. While it's true that everyone has untapped potential, there are also limitations that even superhuman willpower can't transcend.

Firstly, what if you were just born with the wrong body type? A girl who doesn't have a petite frame will never become a Bolshoi ballerina, no matter how hard she tries.

Secondly, what if you were born without any natural talent in a particular field? Becoming the world's best would be out of the question.

But let's say that you want to try anyway: Do you have the determination necessary to practice for the 10,000 hours that experts consider the minimum requirement for outstanding performance?

What's more, it's logically _impossible_ for everyone to be better than all competitors in a given field.

Imagine, for example, that five girls all want to win a race. So they all follow the advice from pop-psych books — put in 10,000 hours of practice and develop iron determination. But _by_ _definition_, only one person can win a race! Four of them will be losers.

Finally, you aren't 100 percent in control of yourself — there's always the influence of other people. While self-help books will tell you that no one can _make_ you feel a particular emotion, in actuality, we're hard-wired to respond to others.

In fact, our brains are outfitted with a specialized set of neurons called _mirror_ _neurons_, that react to and reproduce people's body language. For example, when another person is crying, you'll feel like crying yourself.

In addition, we react to other people's expectations of us without even knowing it. When investigators randomly pointed out average pupils as "gifted" to their teachers, these students began objectively outperforming their peers without any idea that they were labeled as "gifted _."_

> _"We can't all be the fastest runner or the most successful entrepreneur or the cleverest in the class."_

### 7. Overestimating your control over life events may prove harmful. 

Imagine an obsessed man who runs after every car he sees driving down his road, just to make sure it doesn't hit a pothole. Sounds crazy, right? Of course! It is utterly exhausting and impossible to stop a car hitting a bump in the road.

But when you think about it, trying to control your life is just as futile.

In fact, there are a number of downsides to believing that you determine your fate.

For example, if you perceive all your daily choices as having life-altering consequences, then you may feel reluctant to make _any_ spontaneous decisions, as any mistake might be fatal in the long run.

Apart from provoking severe anxiety, this outlook will also stifle your creative process, since creativity involves freely spontaneous and non-evaluative thought.

Furthermore, if you believe that you can determine everything, then you'll consequently feel responsible for every single mishap in your life. Indeed, someone who believes that it's up to him to determine how any situation unfolds will work incessantly to make sure he gets everything perfect.

But he still can't pull it off, and this can put him under immense pressure.

For example, if someone feels responsible for her spouse's feelings, then she'll obsess over the fact that her loved one is feeling down. She'll brood over possible mistakes she made and become obsessed with correcting them, taking drastic measures like painting the bedroom in their favorite color just to make them smile.

Moreover, sometimes it's just bad to believe that people can fix all their problems.

For example, as we saw, people can't conquer cancer with thoughts alone. To suggest they can is to make them feel unjustly responsible for their own suffering.

Indeed, if you believe you could heal yourself but continue to experience symptoms, then you might feel like an utter failure. What's worse, you might also fail to seek timely medical help.

Instead, we should follow the advice of the Greek philosopher Epictetus: "There is only one way to happiness ... cease worrying about things that are beyond the power of your will."

But if we can't control our lives as much as we'd like to, we can at least be the masters of our own minds — right? These final blinks will answer that very question.

> _"People who believe they are in control automatically feel under the pressure to get it right all the time."_

### 8. Neuro-Linguistic Programming is ineffective and based on fallacies. 

If you want to make positive changes in your life, why not try to change your mind itself? Why don't you try Neuro-Linguistic Programming (NLP) to reprogram your brain?

What is NLP exactly? NLP is a practice based on the belief that all high-functioning people have specific thinking habits, and that anyone can become higher-functioning if they learn to think like a successful person.

Alas, NLP doesn't work. In fact, studies show that NLP is ineffective both in enhancing human performance _and_ as a therapeutic tool.

For example, one foundational belief in NLP is that we can replicate an expert's performance by emulating her expert knowledge and imitating her thinking habits. Not so!

Expert knowledge isn't just a compendium of rules, but rather a set of highly developed skills. And even the simplest of skills, like riding a bike, can't be acquired without practice.

Another NLP technique is changing the way you talk by calling setbacks "feedback" rather than "failures" in order to raise your morale. This is pretty misguided. If you want to become a skilled singer and your neighbors offer you $1,000 _not_ to sing for one week, then you could optimistically take this failure as feedback that you don't yet sing very well, and you'll have to practice much harder to reach your goal of serenading them.

But what if you are a lifeguard, and tragically fail to rescue a drowning child. Can you consider this death mere "feedback" on your skills as a lifeguard? No!

That's because _feedback_ is for ourselves alone, our _failures_ affect others, too. Your neighbors had to listen to your terrible singing, after all.

Real failure hurts. It's not just some useful information to help you succeed. It should motivate you to reconsider your values, and not merely adjust your strategy in pursuit of your goal.

> _"Feeling bad about ourselves is often life's way of letting us know we could do better."_

### 9. Another therapeutic approach, cognitive behavioral therapy, is also far from infallible. 

Have you ever wondered what causes your feelings and behavior? Cognitive behavioral therapy, or CBT, maintains that both are caused by our thoughts.

Consequently, behind every problematic behavior or negative emotion there's bound to be some unhelpful thought. Thus to overcome your problem, you'll first have to find, explore and alter that thought.

However, CBT's assumptions are deeply flawed.

It is often our emotions that precede our thoughts, _not_ the other way round. For example, if you're arachnophobic and spot a huge, hairy, disgusting tarantula on the seat next to you, you'll first be flooded with panic before your brain even produces the word "tarantula."

CBT also wrongly assumes that what filters through from our subconscious takes the form of a proposition. However, it could just as well be an abstract image.

For example, rather than thinking "I'm completely helpless" in times of desperation, you might instead fleetingly visualize yourself as having no hands — it's quite difficult to reason with an image.

Furthermore, CBT assumes that we should simply change negative thoughts that are irrational. In reality, those thoughts belong to a whole network of other thoughts and memories. Uprooting them is like taking apart our lives, so we're sometimes reluctant to change them.

Also — just like fleas — if you spot one there are probably hundreds. When a depressed person thinks "I'm useless," it's not just connected to a network of other thoughts, it's also more likely to be connected to _negative_ beliefs and painful memories. Challenging one negative belief means challenging potentially hundreds, and this is no easy task.

Consequently, CBT isn't always effective.

In fact, while studies show that patients exhibiting symptoms of anxiety _do_ moderately improve through CBT, roughly three quarters still experience significant symptoms, and the statistics are even worse for those suffering from depression.

### 10. Positive thinking can be counterproductive. 

Is it possible to be a pessimistic curmudgeon and still live a good life? And can the power of positive thinking lift someone out of depression?

Studies show that people who already feel bad may feel even worse if they are confronted with positive affirmations. In one study, after repeating the positive mantra "I am a lovable person," participants with a low self-esteem actually felt worse. But why?

The researchers believe that these counterintuitive results occur because the participants don't actually believe the mantras, and they thus trigger contradictory thoughts, such as, "That's not true; I'm not lovable at all."

Of course, this isn't an argument against positive thinking _per_ _se_, but rather against parroting positive affirmations that you don't believe — a tactic advocated by too many self-help manuals.

Conversely, people in bad moods are actually less prone to mental errors. This was evidenced by one study in which subjects watched movies aimed at lifting or dampening their moods, and were then asked to complete several mental tasks, such as judging the truth of rumors.

Compared to their happy counterparts, those with dampened spirits were better focused, less gullible and made fewer mistakes.

Positive thinking itself can even be dangerous, if overdone. Then it becomes denial, and denial can breed problems.

This is because sometimes the most realistic take on a situation is a negative one. Just think of the sinking _Titanic_ : until the bitter end, many passengers held on to the belief that the ship was unsinkable.

While this denial might have been good for passengers trying to cope with their impending doom, denial in other situations can prevent you from dealing with a problem that requires action.

For example, if you experience the early symptoms of diabetes, you really ought to see your doctor rather than pretend that nothing is wrong.

Positive thinking, while not inherently bad, isn't always healthy, helpful or rational. Sometimes it's the grumpy pessimist who is more realistic than all those Pollyannas out there.

> _"Are we really so vain or insecure that we need to believe that we can do and be anything?"_

### 11. Final summary 

The key message in this book:

**The** **self-help** **industry** **promises** **simple** **solutions** **to** **all** **our** **problems.** **However,** **many** **of** **its** **suggestions** **don't** **stand** **up** **to** **closer** **scrutiny.** **In** **fact,** **the** **prevailing** **message** **that** **"we** **could** **do** **better"** **might** **make** **us** **anxious** **that** **being** **a** **regular** **person** **with** **reasonable** **success** **just** **isn't** **good** **enough.**

Actionable advice:

**Wait** **until** **you're** **in** **a** **foul** **mood** **to** **make** **important** **decisions**.

In spite of the claims that positive thinking will make _everything_ in your life better, it turns out that there are some times where it's better to be a grouch. In fact, studies show that we make our best decisions when we're down in the dumps, as sad people are more conscientious and less gullible than their happy counterparts.

**Suggested** **further** **reading:** **_The_** **_Antidote_**

_The_ _Antidote_ is the intelligent person's guide to understanding the much-misunderstood idea of happiness. The author emphasizes that positive thinking isn't the solution, but part of the problem. He outlines an alternative, "negative" path to happiness and success that involves embracing failure, pessimism, insecurity and uncertainty — what we usually spend our lives trying to avoid.
---

### Stephen Briers

Dr. Stephen Briers is a clinical psychologist who has authored several best sellers, including _Psychobabble_, _Superpowers_ _for_ _Parents_ and _Brilliant_ _CBT._

