---
id: 533ae8093430340007210000
slug: purple-cow-en
published_date: 2014-04-01T08:11:53.000+00:00
author: Seth Godin
title: Purple Cow
subtitle: Transform Your Business by Being Remarkable
main_color: 71329B
text_color: 71329B
---

# Purple Cow

_Transform Your Business by Being Remarkable_

**Seth Godin**

These blinks explain why traditional marketing no longer works, and why to be successful you need to build _Purple Cows_, remarkable products and services that stand out of the crowd. They also explain how you can reach your target market once you've found your own Purple Cow.

---
### 1. What’s in it for me: find out why not being exceptional means death for a company. 

If you look at a field of cows, guess what you'll see? Probably, the view will be dominated by black and white, with perhaps a few splotches of brown thrown in if you're lucky. But probably none of these will really grab your attention.

But now imagine that you spot, against all odds, a Purple Cow in the field! It's safe to say you'd take notice, because a Purple Cow really is something remarkable!

There's a lesson in there for all companies: if your products are like the conventional black and white or even brown cows, you can't hope to attract anyone's attention. Instead, you need to create something exceptional: a Purple Cow.

In these blinks, you'll discover

  * why your customers are your best marketers;

  * why the Leaning Tower of Pisa attracts over 100 times the amount of visitors than the Pantheon does; and

  * why being ridiculed can be a good thing.

### 2. Advertising is losing its power because it is so abundant. 

You may have noticed that there is a tremendous change underway in the dynamics of marketing.

In the past, if you turned on the television or read a newspaper, you would probably only see a few products being advertised. This rarity made those products seem automatically trustworthy.

Today, however, countless products are being advertised in countless ways, so the consumer is overwhelmed with advertisements. This has made capturing the consumer's attention almost impossible.

This is due to the fact that people are simply too busy to recognize and focus on all the advertising around them. An illustrative anecdote of this is provided by the author: Once at a nice hotel, he asked a few people around him reading a newspaper to name two companies with full-page advertisements in the paper. No one could remember even two.

This obliviousness stems from the fact that consumers ignore marketing unless it meets their exact needs. After all, you wouldn't really take notice of car ads unless you needed a new car.

This also means that traditional advertising in mass media is becoming less effective.

Say your company buys a television ad. A mass of people will see it, yes, but only a fraction of that mass actually wants your product. And only a fraction of those people is willing to actually pay attention to your advertisement. Finally, only a fraction of this last subset is actually willing to buy your product.

Imagine you're launching a new brand of pain relief medication and want to advertise them. The trouble is, there's already a multitude of pain relief brands available. This means you need to overcome three obstacles:

First, you need to find consumers who want a pain reliever. Second, from this group, you need to find those customers who aren't happy with the current options. Finally, you need to find the people who will actually listen to what you're saying and buy your product.

These trends make it increasingly challenging to really reach your target customers and market your products.

### 3. To make your product or service stand out in the modern world, it has to be remarkable. 

As we saw in the previous blink, the dynamics of advertising are changing.

Looking back at the history of advertising, we can discern three distinct periods: before, during and after advertising. Let's take a closer look at each.

_Before_ _Advertising:_ This is the oldest form of advertising, where consumers recommended the products and services that solved their problems to each other through word-of-mouth. For example, people might have talked about the best vegetable seller on the market square, driving more and more customers to her stand.

_During_ _Advertising:_ In this era, advertising worked like magic; the more you advertised, the more your sales would increase. Companies could buy ads to increase sales and profits, which could then be spent on yet more advertising in a virtuous circle.

_After_ _Advertisement:_ This is the era in which we find ourselves today. We have reverted back to word-of-mouth promotion. These days, however, social networks like Twitter and Facebook mean that word of great products and services can spread very quickly.

In this new era, there is so much choice in products out there that it is not enough to satisfy the customer's needs — to truly get attention, you need to be _remarkable._

To get your product or service to stand out in the market and be immediately noticeable, you need to make it a _Purple_ _Cow_, something different and eye-catching. The marketing that goes into this is called r _emarkable_ _marketing._

A great example of a Purple Cow is the new Volkswagen Beetle. The original Beetle had been popular and profitable for over fifteen years, so the relaunch was a challenge.

It turned out to be a great success though because its distinctive shape and attractive colors made it stand out from the other cars in the street. Also, great reviews and word-of-mouth spread its fame even further.

> "_The Purple Cow is not a cheap shortcut. It is, your best (perhaps only) strategy for growth_."

### 4. Today, taking risks is a safer strategy than avoiding risks altogether. 

No doubt you've seen a lot of remarkable products and services out there, so you know there are many different potential ways to achieve this.

Most companies, though, are reluctant to explore ideas for becoming remarkable, because they are afraid of trying something new.

To succeed, you must overcome this fear, because there is no place for boring products in the modern market. In a crowded marketplace, not standing out is equivalent to being invisible.

Consider the Buick: It has always been a boring car, where the manufacturer has clearly opted not to take any risks with the design. Consequently, it has never sold particularly well.

To illustrate the importance of standing out, whatever your field, consider the example of Andrew Weil. He attended Harvard Medical School, where the goal for most students was to become the best doctor around. But Weil chose a different path than his peers: he challenged the medical establishment by combining conventional and alternative medicine — a field often discredited by the medical establishment. The bold choice paid off, and he has helped hundreds of thousands of people through his clinics, writing and presentations.

Another popular — but poor — strategy for businesses is "follow the leader"; emulating the actions of the leaders in the field. But this strategy will never make you a leader yourself.

This is because the leaders likely attained their success by taking risks and creating something exceptional. If you merely imitate this, you will not be remarkable.

Companies who choose this strategy inevitably fail in the long run, because their market is bound to change sooner or later, at which stage they'll be trapped as they won't have any experience in trying out new things.

Just consider the music industry: For a long time, record companies just mimicked each other, producing near-identical products, with similar labelling, packaging, etc. But when the technological landscape changed, spawning, for example, online music stores, the record companies felt trapped because they had no experience in dealing with change.

> _" The old rules don't work so well anymore. Marketing is dead. Long live marketing."_

### 5. Focus on the customers who are willing to both try something new and spread the word. 

So now you know that you need a remarkable product. But once you have one, how do you tell the world about it?

Basically, to find the right method, you must first know that there are five groups of people who will likely use a product.

First, there are the _innovators_ : people who are eager to try new things and to be at the cutting edge.

Next, there are the _early_ _adopters_. They are interested in new products because they want whatever advantage they might provide.

Then, there are the _early_ _and_ _late_ _majorities_. These people are the pragmatists who adopt proven products because everyone else is using them too. These groups constitute the majority of the customer base.

Finally, there are the _laggards_, who are reluctant to adopt anything new and will only do so when they absolutely have to.

Traditional marketing wisdom suggests that because they constitute the largest segment, you should target the early and late majorities first. But in fact, this is a mistake, as these people are reluctant to adopt new products before they are widely used.

Instead, you should target the early adopters; they are likely to discover new products and, even more crucially, they can create buzz around the products.

This means you should design your product so it attracts early adopters and helps them spread the word.

As an example, consider digital cameras: they are more convenient and cheaper to use than traditional film cameras. So how would you market them?

First, you should target your marketing at technology enthusiasts and professional photographers, because they constitute the early-adopter segment. This means they will appreciate your product's benefits and convince others to use it as well. They could win you customers from the other segments.

To understand how you can encourage people to spread the word about your product, ask yourself questions like "How effortlessly can people recommend your product to others?" and "Does your target group talk about products they like?"

> "_Don't try to make a product for everybody, because that is a product for nobody_."

### 6. Marketing is about inventing the product, not just selling it after it’s been made. 

Ask yourself: What do you think "marketing" really entails?

Traditionally it was thought that marketing is about communicating the value of products and services once they've been produced. But in fact, this is just advertising. Marketing comprises much more.

Marketing is actually about _inventing_ _the_ _product_. Successful products have been built from day one so that every aspect of the product — design, production, pricing and sales — is influenced by marketing considerations, meaning how to make the product such that people will talk about it.

For example, the CEO of the airline JetBlue asked the Head of Marketing to be involved in product design and employee training from the start.

What's more, marketing means finding a competitive advantage — an edge — that will differentiate you from competitors.

To discover your edge, you must try out various ones, carefully comparing where your own and your competitors' products differ in aspects like pricing and promotion.

Because the heart of marketing lies in your actual product, you should come up with a simple and easy slogan that conveys the main message behind your product.

For example, consider the Leaning Tower of Pisa. It's a world famous tourist attraction because its main message is so easy and simple to spread: it's a leaning tower. This is its "slogan."

Meanwhile the Pantheon in Rome, while beautiful and historically more significant, lacks such a slogan and therefore only gets one percent of the annual visitors that the Leaning Tower of Pisa gets.

Another example of a great slogan, though a wordless one, is the signature blue box in which Tiffany & Co. jewelry comes in. It radiates a message of elegance and quality, and every time someone gives it as a gift, they are spreading the word about the brand.

### 7. Market to the people who are looking for your solution to their problems, and measure the efficacy of that marketing. 

As you've already no doubt surmised, the majority of advertisements today are ineffective: the people who see them either aren't interested in the product or they're not part of the early adopter segment who are open to new products and eager to spread the word about them

So you may well ask: What's so special about the advertisements that do work? And how can you tell if yours do?

The key is to target your ads effectively.

This is because, while earlier marketers could choose whom they wanted to reach, these days the sheer abundance of advertising means it is _the_ _customers_ who have to choose to listen to you.

This means your advertisement has to target people looking for a solution — your solution — to their problems, and this is achieved by putting your ad in a place where they are looking for it.

For example, Google Ads works well in this respect because people type in search terms that reflect what they are looking for, and the ads are selected to offer a solution to whatever problem they express.

Whichever way you advertise your product, you will still need to measure the effectiveness of your marketing. This is the only way in which you can find out what works and what doesn't so that you can optimize your actions.

For example, the clothes retailer Zara keeps a close eye on how its products are received in stores and out in the world, and adjusts its selection of clothes every three or four weeks accordingly.

Remember, you can only improve that which you can measure, so in addition to thinking about the effectiveness of an action, you should always also consider how much it will cost you to measure it.

### 8. Many companies fear the criticism, ridicule and change that being remarkable would bring. 

Many companies find it hard to be remarkable, because they are afraid. Let's examine some of the things they're afraid of:

First, they're afraid of criticism — being remarkable means attracting criticism, because you will stand out of the crowd.

For example, the new CTS Cadillac has been widely criticized because it is ugly, despite the fact that it has sold well and that those who bought it love it. Creating something exceptional simply attracts criticism — it's inevitable!

But criticism doesn't mean failure. In fact, if you _don't_ attract any criticism, you should worry, because it means you're playing it safe, and as mentioned earlier, this is a road to failure.

Second, many companies are also afraid of being inadvertently offensive or looking ridiculous if they are too bold.

Indeed, being different can cause people to make fun of you. But even if someone does ridicule your product, their ridicule itself can become famous, thus further spreading the word of your product.

But being offensive or outrageous isn't something you should knowingly strive for, as it only works on occasion. What's worse, it can create a scandal, which will create negative word-of-mouth — not what you want.

Finally, perhaps the biggest fear most big companies have concerning creating Purple Cows is that it would require them to radically transform their existing infrastructure, for example, their factories.

After all, once you understand what will be the thing that makes you remarkable, you will need to do whatever it takes to achieve this, and this often means big changes.

A prime example of overcoming this fear can be seen in Brad Anderson, the CEO of electronics retailer Best Buy. To differentiate his company from competitors, Anderson totally changed the way the company worked, starting with selling what customers wanted to buy rather than what the company wanted to sell. This paradigm shift resulted in big changes to the infrastructure the company had heavily invested in earlier.

### 9. Final summary 

The key message in this book:

**In today's crowded marketplace, there's no room for "ordinary" products or services anymore; either you're remarkable or you die. To become remarkable, you need to boldly take risks and not worry about criticism. Then, when you start spreading the word of your remarkable product or service, you need to target the people who are both willing to try new things, and eager to spread the word to others.**

Actionable advice:

**If you find that your company is lagging behind the leader in the industry, resist the urge to just emulate what they are doing.**

Instead, take a pen and paper and make a list on how you might close the gap between you and the leader, but only include items where you do something _differently_ from the leader.

**If you're a product designer, take a course in marketing.** I **f you're a marketer, take a course in product design.**

In both cases, also spend time on the factory floor to understand how the product gets made. Creating Purple Cows demands that you invent the product with the marketing perspective already in mind.

**Suggested further reading: _Permission Marketing_ by Seth Godin**

_Permission Marketing_ confronts the conflicts and challenges that modern marketers face in the digital age and offers a viable alternative. It explains how the advertising landscape is filling up and how this makes traditional advertising ineffective. The author suggests that smart marketers no longer simply interrupt consumers but invite them to volunteer their time and become active participants in the _marketing process._
---

### Seth Godin

Seth Godin is an entrepreneur, author and marketing guru. He has created such popular websites as Yoyodyne and Squidoo, and his previous notable works include _Permission_ _Marketing_ and _Linchpin_, both also available in blinks.

