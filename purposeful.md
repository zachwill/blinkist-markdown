---
id: 5bf9e4b86cee070007cab48e
slug: purposeful-en
published_date: 2018-11-28T00:00:00.000+00:00
author: Jennifer Dulski
title: Purposeful
subtitle: Are You a Manager or a Movement Starter?
main_color: 6FC7B9
text_color: 36615A
---

# Purposeful

_Are You a Manager or a Movement Starter?_

**Jennifer Dulski**

_Purposeful_ (2018) is an exciting guidebook to getting your big ideas for change off the ground. Drawing on her experience at Change.org, author Jennifer Dulski shows how popular movements get started, how they build a following and, finally, how they enact real change in the world. Dulski offers plenty of real-world examples, from how leaders inspire loyalty to the best tactics for getting powerful decision-makers on your side.

---
### 1. What’s in it for me? Learn what it means to be the leader of your own movement. 

There's more than one kind of movement. At the most basic level, a movement is what happens whenever a group of people unite around a single vision. Therefore, a movement could be based around a powerful political idea or an exciting business concept, and the leader of a movement could be either a passionate activist or an inspired entrepreneur.

Dulski is well positioned to offer advice for the potential leaders of any movement since she was the former president of Change.org, the launching pad for many of today's most popular campaigns for change. Dulski has firsthand experience with many of the issues that leaders might face, including how to gain a following and how to keep those followers motivated once they're on board.

Many people today are motivated to see change happen in their lifetime, and they're looking for the kind of leader who will inspire them to mobilize and fight for what they believe in. So now's the time to give action to your words and bring your ideas to life.

In these blinks, you'll find

  * the story behind Nike's first ad that featured a hijabi woman;

  * how to cope with trolls and haters; and

  * why a "festival of failure" is a good thing to be part of.

### 2. To start an exciting movement, you need to be purposeful in your actions. 

There's no shortage of great leaders and managers working today. But the real shining stars are the ones who go the extra mile and use their leadership skills to ignite a genuine movement.

People are attracted to movements because they offer a sense of purpose, something we all look for in life. When we see others driven by an exciting purpose, it's easy for that excitement to spread and catch on. Before you know it, large numbers of people are uniting and bringing about change.

Manal Rostom is one of those shining stars who used purpose to ignite change. Rostom is an Egyptian Muslim woman, and for years she's worn a _hijab_, a religious veil or headscarf that covers the neck and hair. But one day, Rostom began to feel a growing public resentment toward the garment, with many media outlets publishing anti-hijab articles. It got so bad that Rostom wasn't even allowed to enter some public spaces in Dubai because of her hijab.

Fed up with the negative perceptions, she decided to start a Facebook group called "Surviving Hijab" as a place where Muslim women could share their experiences and support one another. It turned out to be a popular idea: in just a few months, the group attracted 40,000 members, and today there are around 500,000.

With such a positive response, Rostom saw an opportunity to go a step further. As an athletic woman, she often received annoying comments from people wondering how she could run while wearing the extra layers of clothing. Rostam was sure there were other women who could benefit from a change in the public perception that hijabi women can't pursue sports.

So, in 2015, she contacted the head of Nike's Middle East division, Tom Woolf, and asked why there weren't any Nike ads in the Middle East featuring hijabi women. After a productive meeting, it was decided that Rostom would become the first woman to wear a hijab in a Nike ad. But it didn't end there. Later that year, Rostom was also made the coach of Dubai's Nike women's running club. And then, in 2017, Nike released a whole line of athletic wear called _Nike Pro Hijab_.

In addition to being a leader, Rostom is a movement-starter, and it all began with a clear purpose: to show the world that faith isn't something that gets in the way of a woman's accomplishments. As it turned out, many people shared this feeling — so much so that a world-changing movement formed.

### 3. Anyone can start a movement, so stop doubting yourself and get started. 

When you think of world-changing people, you might picture Martin Luther King, Jr. or Nelson Mandela. But you don't have to win a Nobel Peace Prize in order to make a big difference. In fact, even small actions can have a ripple effect that leads to significant consequences.

Such was the change that came from the actions of Turkish activist Erdem Gündüz.

In 2013, authorities in Istanbul removed tents located in Taksim Square that were being used by activists and victims of police violence. To protest this removal, Gündüz simply stood still in Taksim Square. He was soon known as the "Standing Man," and after a couple of hours, others joined until hundreds were standing by his side in silent protest.

Gündüz stood still for eight hours, and this relatively small action had the major consequence of triggering an international movement that transcended the borders of Turkey. People worldwide were inspired to stand in solidarity against police violence.

As we can see from Gündüz standing still, or from 42-year-old Rosa Parks refusing to give up her seat to a white man in 1955, one person making a small gesture can have a big impact. But you don't have to be oppressed in order to stand up against injustice. In fact, it doesn't matter who you are or where you're from — you _can_ make a difference in the world.

Still, many of us get stuck behind mental roadblocks, so it's important to find a way around such barriers and not get stalled. If you come from a privileged background, you shouldn't feel like you haven't earned the right to speak out about the issues facing certain communities — you can still speak up on behalf of others.

Don't get hung up on where to start, either. Just take the first step and get the wheels of change in motion — that's what's important.

Sarah Kavanaugh didn't let self-doubt stop her from taking that step. One day, she was disturbed to discover that the popular sports drink Gatorade contained _brominated vegetable oil_, or BVO, an ingredient that was taken off the FDA's "Generally Regarded as Safe" list back in 1970. Kavanaugh took her first step by going to Change.org and starting a petition to make Gatorade safer for people to enjoy.

Before long, the petition had 17,000 signatures and was receiving media attention. It was enough to get the change Kavanaugh was after: Gatorade's parent company, Pepsi, eventually removed the unsafe ingredient.

> _"Even the biggest movements in history were sparked by relatively small but critically important actions of key people along the way."_

### 4. Have a well-defined vision and use storytelling to get people on board. 

If having purpose is the first ingredient to change, the second essential ingredient is a well-defined vision.

While purpose should answer the question of _why_ you want to take action, vision answers the question of what the future looks like after action has been taken and change sets in.

Neil Grimmer decided to take action after he couldn't find healthy organic baby food in his local shops. So he decided to start his own baby-food company, Plum Organics. In this case, Grimmer's _purpose_ was for parents to have healthy food for their babies. His _vision_ was a future where the shelves of grocery stores were well stocked with organic baby food.

A well-defined vision is like a compass; it keeps you on track throughout the process of creating your movement. This is why you should have a clear vision before you start.

Once you've started, a good way to get people on board is to use storytelling to communicate your vision. With the aid of storytelling, you can demonstrate how your vision will impact others while getting your purpose across more persuasively than by simply stating your desired goal.

Certainly, the tragic story behind the "Kari's Law" Act was a major reason the legislation managed to get passed in 2017.

It all began four years earlier, when Kari, the mother of three young children, was killed in a hotel room by her husband. Kari's kids ranged from age two to age nine, and they were in the adjoining room while Kari was being attacked. The oldest child tried to use the hotel phone to call the police but wasn't able to get through because he didn't know that he needed to dial nine in order to get an outside line.

As Kari's father, Hank Hunt, explained when introducing the legislation behind Kari's Law, had there been no need to dial nine, his daughter might still have been alive. This was his purpose, and his vision was to ban any hotels or other business from requiring clients to dial extra numbers when making phone calls.

Hunt's harrowing story deeply connected people to his vision, as anyone who heard it immediately wanted to help. It's a big reason why Kari's Law is now a federal law in the United States.

### 5. Be respectful toward decision-makers and map out multiple contacts who can help you. 

If you want to change the world, you may think that playing it nice won't get you very far. But you shouldn't underestimate the power of a positive attitude, especially when trying to convince people in power to help you. When you approach them respectfully, people will be far more receptive to your ideas, as well as more willing to work with you.

This positive attitude means you should avoid thinking of decision-makers, such as politicians and CEOs, as "targets" that need to be forcibly convinced to do what you want them to. For example, attacking someone on Twitter is unlikely to provoke the change you're looking for. In fact, it's likely to kill your chances of working with that person now or in the future.

Remember, many decision-makers are elected officials and public servants, whose job it is to help. So don't get off on the wrong foot by thinking they're inaccessible to the public and will never listen to your story.

There are a number of respectful and effective ways to get the attention of decision-makers, such as peaceful demonstrations, organized phone calls, boycotts and signing petitions. Any of these methods could very well lead to a fruitful relationship with those in charge.

To make your efforts even more efficient, you should map out all the relevant influencers, including people and organizations, that could possibly help your cause. Sometimes, the obvious person isn't available or receptive, so it helps to have options.

Jennifer Tyrrell was an effective agent of change because she pursued multiple avenues after she was let go by the Boy Scouts of America (BSA) when they discovered she was gay.

Looking to change this discriminatory policy, Tyrrell started a petition on Change.org, but it was soon clear that dealing with BSA executives directly wasn't effective. So she began reaching out to people on the BSA board of directors, like the CEOs of AT&T and Ernst & Young.

Unlike the conservative BSA executives, these CEOs were known to support LGBTQ rights, and after Tyrrell urged them to speak out against BSA's policies, they did just that. For good measure, Tyrrell also reached out to other LGBTQ-friendly companies that partnered with BSA, like UPS and Intel. And sure enough, they spoke up as well.

Eventually, in 2013, Tyrrell's vision was realized when BSA voted to allow gay people into their community.

### 6. Keep supporters on board by keeping them engaged with purpose, personal growth and strong connections. 

Getting people on board is only the start of a successful movement — you also have to know how to keep those people motivated and engaged for the duration of the movement.

To sustain people's motivation, you need to remind them of three important things about your movement: its members' shared _purpose_, their continued personal _growth_ and their strong interpersonal _connections_.

First of all, while purpose attracts people to your movement, it's also a big factor for keeping them motivated. When people share a purpose, it makes them feel united and as though they're part of something important, even if the people come from widely different backgrounds. So don't pass up a chance to remind people of that shared purpose.

After Neil Grimmer started his Plum Organics baby-food business, he would remind his team about the shared purpose at company meetings every Monday morning, making it a strong, motivating start to the workweek.

Next is to make sure people are growing and continuing to learn. When people recognize that your movement is helping them grow as individuals, they'll feel more of an incentive to continue fighting for your cause — so be sure to keep challenging them.

While the author was working at Change.org, she started an initiative called the _90/10 model_ for decision-making. It meant that every employee is expected to make 90 percent of the necessary decisions for them to be successful in their jobs. Yes, this put a lot of responsibility in employee hands, but it also challenged them to rise to the occasion. And this ensured that they were always learning and growing on the job.

Finally, it's important to create an environment where you and your team members can forge strong connections. Genuine relationships create an atmosphere of trust, which allows people to feel safer to take risks and be open and honest.

This environment benefits everyone. According to a recent study at Google, the highest performers are those who feel psychologically safe.

This can apply to online-based movements as well: Facebook has found that its most successful groups are ones with active administrators who create a safe and rewarding environment. They're welcoming to new members, they ban bad behavior and they continually add new and relevant content. By making members feel comfortable and safe, such administrators set the stage for people to make strong, genuine and lasting connections.

> _"...your movement can't continue unless the support from your team does."_

### 7. Flood negative feedback with positivity and use it to your advantage. 

No one likes to be on the receiving end of harsh criticism, but when you're starting a movement, criticism is practically inevitable. No matter your purpose and vision, you're bound to encounter someone who either opposes your idea or disagrees with your approach.

These days, there are two kinds of criticism: there's the positive and constructive feedback, and then there are the "trolls" and "haters" who offer nothing constructive and only criticize in order to drag others down.

So what should you do when the inevitable happens and you're faced with an obnoxious troll? You stay positive by keeping your supporters in mind.

The author spoke to plenty of petition starters while working at Change.org, and nearly all of them relied on the positive feedback of supporters when they were forced to deal with the comments of haters and trolls. By reading and rereading the positive remarks, you can remind yourself of your legion of supporters and not lose sight of the fact that haters don't speak for everyone.

It's also worth keeping in mind that sometimes criticism can be used to your advantage.

Kara Goldin did exactly that when she was considering the next step for her unique beverage company. In 2005, Goldin founded Hint Water, which came as a response to being bored with normal water but still wanting to avoid sugary drinks. Hint Water gets its name from the hint of fruit flavor it has, even though it contains no sugar or artificial sweeteners.

When Goldin was exploring the option of selling her business to one of the larger beverage corporations, she was told by a high-ranking executive that they weren't interested in Hint Water because Americans love sweet drinks and sweet drinks _only_.

Goldin could have been discouraged by this patronizing, closed-minded remark, but instead she saw it as an opportunity. If beverage executives were under this impression and only interested in selling sweet drinks, then she could market Hint Water as a refreshing alternative and have very little competition. After all, Goldin was an American who didn't _only_ love sweet drinks, and she was pretty sure there were others like her.

Goldin's courage to move forward on her own paid off handsomely. Hint Water became part of a big health and wellness movement in the United States, and its value has been estimated at $100 million.

### 8. Before you succeed, you’ll likely fail, but this experience makes you stronger. 

Life doesn't move in a straight line. It's filled with ups and downs, especially when you're trying to start a movement. So don't be surprised if you stumble when trying to come out of the gate with a new idea. And remember, you're not alone — every entrepreneur and leader has taken a fall at one time or another.

Once you understand that obstacles are bound to appear, you can then be ready to react as best you can.

The author refers to rebounding from downfalls as Rocky Moments — as in Rocky Balboa. Every one of these moments has two parts. First comes the failure, which is when you're down and any possible success seems distant and unlikely. But if you continue to fight, you'll eventually come to the second part, when you turn things around and triumphantly get back on your feet.

Failure doesn't mean you're finished. In fact, you can emerge smarter by having understood what went wrong and being equipped to overcome future challenges. Therefore, you should take a close look at the reasons for the failure and share your insight with others. By being open about mistakes with your peers, you'll be in a position to learn from their mistakes and avoid those potential pitfalls.

At Change.org, there's a regular company-wide event called the "Festival of Failure," where people come together to share their worst experiences and downfalls. There are also smaller versions of this concept that take place between different divisions, like software engineers who share bad code experiences and discuss what went wrong and what they learned.

So, when you encounter a Rocky Moment, remember that there are others out there who not only can offer support, but can help you learn from the experience.

Finally, let's consider the case of Olga Rybkovskaya, who reached out for help during a difficult time in her movement. Rybkovskaya was using Change.org to petition the Russian Ministry of Health to change a law that prohibits families from visiting intensive-care patients in the hospital.

She knew success wouldn't be easy since it would require the drawing up of new policies and guidelines. But rather than despair at the difficulties of this challenge, Rybkovskaya took a chance and reached out to the lawyers who'd signed her petition. Amazingly, she was able to get 30 lawyers to volunteer and help create the documents she needed.

Olga wasn't alone in her movement, and neither are you. If there's change you want to see in the world, now's the time to take action.

> _"It ain't about how hard you hit. It's about how hard you can get hit and keep moving forward." –_ Rocky Balboa

### 9. Final summary 

The key message in these blinks:

**Anybody can start a movement. All you need is a collaborative spirit and the right strategic approach. It starts by defining your vision and moving past any doubts about the validity of your voice or ideas. If at first you fail, don't despair. Instead, learn from your failure and use that knowledge to keep moving forward. With determination, you can make the social change you and others desire for the future, whether it's starting a new business or changing an unjust law.**

Actionable advice:

**Ignore irrelevant criticism!**

Sometimes you'll receive criticism that is helpful but delivered in a mean and unconstructive way. Even if that's uncool, it's still worth considering it as feedback. However, if the criticism is irrelevant, such as a comment about your appearance, just ignore it. It's not worth your time and energy, and it won't help you to reach your goal.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Imagine It Forward_** **by Beth Comstock with Thal Raz**

_Imagine it Forward_ (2018) charts the successes and setbacks of one of America's most prolific businesswomen, Beth Comstock. Combining anecdotes from her tenure at General Electric with surprising insights and indispensable practical advice, these blinks explore the life and times of this remarkable change-maker and innovator.
---

### Jennifer Dulski

Jennifer Dulski has been an executive manager for both ambitious start-ups and giant internet companies like Yahoo. Before her current role as head of the Group and Community division at Facebook, she was the COO and president of Change.org. Her writing on modern trends in leadership and entrepreneurship has been featured in such publications as the _Huffington Post_ and _Fortune_ magazine.

