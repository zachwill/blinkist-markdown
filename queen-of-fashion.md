---
id: 57dd860df9916500030ea79b
slug: queen-of-fashion-en
published_date: 2016-09-23T00:00:00.000+00:00
author: Caroline Weber
title: Queen of Fashion
subtitle: What Marie Antoinette Wore to the Revolution
main_color: 98692C
text_color: 805825
---

# Queen of Fashion

_What Marie Antoinette Wore to the Revolution_

**Caroline Weber**

_Queen of Fashion_ (2006) reveals the untold ways in which Marie Antoinette, with her iconoclastic sense of fashion and her rebellious behavior, challenged the status quo of the eighteenth-century French court. Her daring originality was a way for her to share her voice and personality, and her story tells us a great deal about the revolutionary politics that can be found in the history of both fashion and France.

---
### 1. What’s in it for me? Take a runway look at how Marie Antoinette got ahead in fashion. 

"If they don't have bread, let them eat cake." This famous quote has been wrongly attributed to Marie Antoinette, the last queen of the French royal house of Bourbon, and even though she never said it, it sums up the image of Antoinette that we've inherited — of a queen remote from everyday people, spending her time obsessing over fashion and lavish parties.

But can such a simplistic image really be correct?

Marie Antoinette was born on November 2, 1755, the youngest daughter of the emperor and empress of the Habsburg Empire in Vienna. As the youngest daughter, she probably wouldn't have expected to have a marvelous marriage, yet she ended up becoming a part of the most elaborate and luxurious court in Europe. In this environment, fashion and appearance weren't only for show; they also had political repercussions — something this "queen of fashion" knew well.

In these blinks, you'll find out

  * how Antoinette transformed a hairdo into high politics;

  * what the Flour Wars were; and

  * why wearing a simple garment isn't always the best choice.

### 2. As a teenager, Marie Antoinette was taught the political importance of fashion and appearances. 

As a child in eighteenth-century Vienna, Marie Antoinette lived a rather carefree life. She was allowed to play and dress as she wished and, aside from being required to attend and dress up for formal occasions, the young Austrian princess was mostly left to herself.

This all changed in 1770, when a marriage was arranged between Marie, then 15 years old, and Louis-Auguste, the heir to the French throne who would soon be known as Louis XVI.

Once this arrangement was made, Marie Antoinette underwent a preparatory makeover; the rules of the French court at the Palace of Versailles were completely different from those of Vienna, and, as a Bourbon princess, she would be expected to dress and act accordingly, both day and night.

The makeover was demanding: Marie's teeth were straightened and she was trained to perform the _Versailles-glide_, the graceful walk of the French royalty. This meant learning to take tiny steps, with both feet always touching the floor.

Needless to say, Marie could no longer wear Austrian clothing, so her mother spent what in today's money would equate to between two and three million dollars to buy her a new wardrobe in all the best French styles.

Through it all, Marie was also being taught how politically important her marriage was and that her appearance played an essential part in its success.

After all, this was not a marriage of love. Rather, it was a union to help ensure there would be peace between France and Austria, two nations with centuries of feuding that had recently reached an agreement with the 1756 Treaty of Paris.

While the treaty was brokered by Marie's new grandfather-in-law, Louis XV, others in her new family opposed the marriage: Louis XV's mistress, Madame Du Barry, as well as his sisters, Mesdames Adélaïde, Sophie and Victoire, remained distrustful of the Austrian empire and unhappy with the idea of a foreigner like Marie Antoinette becoming queen.

### 3. Circumstances made it difficult for Marie Antoinette to meet the many expectations of the French court. 

Once she arrived at Versailles, Marie Antoinette's every action was carefully planned, controlled and watched over. Louis XV enforced strict rules of court conduct. He thus maintained control by making the royals appear immaculate in the eyes of the public and those around them.

Each gesture and action of the king and his family was carefully orchestrated so as to appear sacred.

Attendants at Versailles assisted one in all one's doings — from removing a cloak to using the toilet — and thus elevated trivial activities to the realm of sacred ritual. These attendants were also high-ranking nobility; they'd been trained to see their role as a privilege.

Everyone had a role to play, including the queen, who was expected to bear children and successors to the throne and not to interfere with other matters.

Marie Antoinette's predecessor and grandmother-in-law, Maria Leszczyńska, had excelled in this role; although Maria was looked down upon when her life at Versaille began, she followed the rules and paid her dues, which led her to becoming a highly respected queen.

Following the rules meant that Maria didn't interfere when her husband indulged his whims by bestowing gifts, palaces and titles on his various mistresses.

Marie Antoinette, however, ran into a problem with her obligation to bear a successor.

Louis-Auguste was an awkward and quiet husband who never really wanted to be king, and the role only fell to him because his father and two brothers died unexpectedly. He also had little interest in his responsibilities, and for seven and a half years he failed to consummate his marriage.

As with most things in the palace, this was public knowledge and the subject of much gossip, including rumors that Marie Antoinette had the "German vice," a term used for homosexuality at the time.

### 4. Marie Antoinette rebelled against royal strictures and became a fashion icon. 

Before long, Marie Antoinette realized that she didn't much care for the restrictive ways of Versailles. She soon began rebelling against the royal court, rejecting the codes of conduct and doing as she pleased.

In the early 1770s, Marie began leaving the palace altogether and making regular trips to Paris.

She took great pleasure in the freedom of escaping the demands and formalities of Versailles, travelling anonymously and attending masquerades and the theater alongside common folk.

Marie's rebellious escapades shocked and delighted the French people, who would read about the latest sightings of the future queen in gossip papers. Sometimes these reports included rumors of scandalous meetings with stylish young men and women. 

All this attention also put Marie Antoinette on the path to becoming a beloved fashion icon.

One of her signature looks was the creation of her stylists, who would become known as the "ministers of fashion." It was called the _pouf_ hairstyle — essentially a tower of hair held in place atop one's head. It was infinitely customizable, and could be decorated with anything from feathers to jewels to vegetables!

Marie wasn't afraid to make a political statement with her fashion choices. She once showed support for France's opposition to the British in the American Revolutionary War by decorating her pouf with the French warship, _Belle Poule_.

With fashion statements like these she soon gained a large public following, spawned thousands of imitators and redefined what the Queen of France was supposed to look like.

Further, Marie Antoinette's willingness to forgo traditional attire in favor of eye-catching fashion is often credited with launching the modern fashion magazine industry. 

But, more importantly, it proved that she was a queen who could do whatever she pleased.

### 5. The precarious French economy and the Flour Wars would change the public’s view of Marie Antoinette. 

The public's infatuation with Marie Antoinette's lavish lifestyle didn't last forever. As the 1770s wore on and the French economy became increasingly troubled, people grew critical of Marie's extravagant ways.

But France's economic downturn didn't have anything to do with Marie's spending; it was mostly due to the country's ongoing investment in the American Revolutionary War.

Regardless, the public's opinion was shadowed by her expensive habits, which were seen as highly inappropriate and a threat to the country's well-being.

The situation worsened following the death of Louis XV and her husband's subsequent coronation as France's King Louis XVI. At the celebration, the newly crowned queen caused a scandal by wearing an excessively flirty gown with an abundance of fancy accessories.

She was such a spectacle that jokes were made about how people couldn't see the king due to the towering pouf on her head, which is also an apt metaphor for how Marie eclipsed the king's power.

Jokes and rumors were also spreading that the king had to make structural renovations to Versailles in order to accommodate Marie's hair. While this kind of gossip was baseless, three women actually did die from burns they received after getting their poufs stuck in chandeliers!

Shortly after the coronation in 1775, the Flour Wars began and cemented the negative public opinion of the queen. 

The Flour Wars began when a series of bad harvests led to a shortage of grain, bread and flour. And since flour was a primary ingredient in Marie Antoinette's hair powder, people stormed the gates of Versailles in protest.

At this point, her hair was not only seen as disrupting the order at Versailles; it was blamed for using up resources that could have gone to the starving people of France.

This flour shortage is likely the reason for Marie Antoinette being falsely attributed with the famous quote, "Let them eat cake."

But there was no mistaking that her once awe-inspiring fashions were now seen as being wasteful and disrespectful.

### 6. As queen, she continued to make rebellious fashion statements, and the criticisms continued as well. 

By the late 1770s, Paris was no longer Marie Antoinette's favorite getaway spot. With the public having turned against her, the queen needed to find a new place to escape.

So she set up a small private sanctuary away from Versailles called _Petit Trianon_.

This allowed her to free herself of the recognizable trappings of the French court, including the ceremonial attire and all the fussy rituals.

In fact, Marie was in charge of all the rules at Petit Trianon. All decrees were signed "By Order of the Queen" and they included how to tend to the gardening and who was invited to the parties.

This was a radical departure from tradition, since these kinds of orders normally only came from the king. Nor did this departure go unnoticed. Many viewed Petit Trianon as proof of her willingness to infringe upon the king's authority.

The queen's proclivity for setting rule-breaking fashion trends was also given free rein at Petit Trianon.

The _gaulle,_ or _chemise a la reine_, was an especially shocking new look that was even seen as being masculine. A simple garment layered with loosely draped muslin and tied at the waist with a simple sash, it was easy to wear and allowed for people to move freely about. It was perfect for the playful picnics and garden parties the queen and her friends enjoyed at Petit Trianon.

She loved the gaulle so much that she wore one for a portrait of herself that was painted in 1783 by Élisabeth Vigée-Lebrun and hung in the Louvre.

The public was shocked by how casually the queen was attired in the painting. The lack of fancy accessories and makeup led people to regard it as a portrait of Marie Antoinette in her underwear and held it as evidence that the queen was unworthy of their respect.

The uproar resulted in Marie replacing the portrait with a more traditional one, but, at this point, there was little she could do to prevent what lay ahead.

### 7. Momentum against the queen’s fashions and the entire royal order reached its pinnacle with the French Revolution. 

The backlash against the queen and her fashions was as multi-layered as the gaulle she loved to wear. The public turned on her for complex, political and even contradictory reasons.

After all, even when the public criticized Marie, they remained eager to imitate her style.

As with the pouf, the gaulle was widely copied by everyone, from princesses to prostitutes.

Someone impersonating the queen by wearing her signature dress even took part in a heist known as the Affair of the Diamond Necklace. And even though Marie Antoinette had nothing to do with the crime, people still blamed her, suggesting that it was her fault that a criminal could be mistaken for a queen.

More criticism came from the fact that the dress was being made from materials imported from Britain and Austria, which was seen as an insult to France and the nation's money and jobs.

According to one critic, 75 percent of Lyon's silk workers lost their jobs in the 1780s because the queen's preference for foreign fabrics caused French silks to fall out of fashion.

So, when the French Revolution finally broke out in 1789, the queen was a prime symbol of the royalty that was being replaced.

In her final years, Marie Antoinette adopted a more conservative and traditional style, but the public still held her in contempt. They called her a chameleon and accused her of simply trying to hide her true nature.

This later transformation came too late, however. In the eyes of the public, she had trespassed against what was expected of a woman in her position too many times.

So, after the revolutionaries stormed Versailles, Marie Antoinette was accused of being an enemy of France and condemned to death by the guillotine.

In her final moments, however, Marie Antoinette made a last defiant statement with her fashion. She wore a white gown that she'd kept hidden in her jail cell, which symbolized the lily of the Bourbon empire. It was thus attired that she walked toward the guillotine — her final, but no less glorious, public appearance.

### 8. Final summary 

The key message in this book:

**Marie Antoinette radically used fashion and appearance to assert her power in a time and place where women — even queens — were often powerless. She used her appearance to give herself a voice and break free from the restrictive bonds of tradition, even though her message wasn't always received in the way she had hoped.**

Actionable advice:

**Use style to communicate!**

Next time you're getting dressed, consider what each accessory and article of clothing communicates, and then reflect back on Marie Antoinette's use of fashion. It's fascinating how meaningful appearance was just a few centuries ago. Often, it was a matter of life and death. Though people don't often get beheaded for their get-up these days, the way you attire yourself still says quite a lot.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Innovating Women_** **by Vivek Wadhwa & Farai Chideya **

_Innovating Women_ takes a critical look at today's technology industry, which, for all its success, remains incredibly old fashioned in its gender imbalance. Statistics and case studies help us scrutinize major players in the technology industry, while personal stories give insights into the lives of talented female innovators working hard against the odds.
---

### Caroline Weber

Caroline Weber is a specialist on eighteenth-century French culture. Before she became an associate professor of French and Comparative Literature at Columbia University's Barnard College, she taught at the University of Pennsylvania and Yale University. Her writing has appeared in _Vogue, Bookforum,_ the _Washington Post_ and the _New York Times_.

