---
id: 58eb9bd4a54bbb000464bb21
slug: quiet-power-en
published_date: 2017-04-12T00:00:00.000+00:00
author: Susan Cain, with Gregory Mone and Erica Moroz
title: Quiet Power
subtitle: The Secret Strengths of Introverts
main_color: E82E37
text_color: CF2931
---

# Quiet Power

_The Secret Strengths of Introverts_

**Susan Cain, with Gregory Mone and Erica Moroz**

_Quiet Power_ (2016) explains why adolescents struggle with introversion and explores how an aversion to socializing can make it challenging to form friendships, complete schoolwork and fulfill social obligations. These blinks offer a number of different techniques that introverts can use to make these situations bearable and turn their so-called weakness into a straightforward advantage.

---
### 1. What’s in it for me? Understand introversion and learn how to live with it. 

We live in an extroverted world. We admire those who know how to draw attention to themselves — pop stars, movie stars, models and divas. At school, those who regularly raise their arm are usually the ones who get the better grades, and in the working world, it's often those who shout the loudest who ascend the career ladder fastest.

How are young introverts supposed to get by in this extroverted world? What's the surest recipe for success that doesn't betray an introverted sensibility?

Drawing heavily on interviews that the author conducted with more than 100 children and teens, as well as their parents and teachers, these blinks teach you that introversion isn't a hindrance to success. You'll learn exactly what introversion is and what you can do when others pressure you to act in ways that make you uncomfortable.

You'll also learn

  * how to measure introversion with lemon juice;

  * how to make group discussions enjoyable for introverts; and

  * why, at a party, frequent trips to the bathroom can be a good idea for introverts.

### 2. There are common misconceptions about what it means to be introverted or extroverted. 

Some people like to see the world in black and white. They categorize people as being either attention-seeking extroverts or attention-avoiding introverts. But human beings aren't that simple.

The terms "introvert" and "extrovert" were introduced by the Swiss psychologist Carl Jung — but, in truth, people needn't fall into either category.

There is a long continuum of personality traits. Introversion and extroversion are just two categories situated at the extreme ends of this continuum. There is also a middle ground, which is commonly referred to as "ambivert."

For example, you might know someone at work or school who keeps to themselves and is therefore considered an introvert. Yet this person might never shut up when they're around close friends and family. Likewise, someone who's pegged as an extrovert and the life of the party might cherish their downtime and appreciate solitude.

So there is no simple definition of an introvert. But if one had to name a primary characteristic shared by most introverts, it would be a rich inner-life.

This doesn't mean they dislike the company of others; it simply suggests that, by nature, they tend to look within themselves and gravitate toward safe and quiet environments where they can recharge their batteries.

In fact, this is a good description of the author. While growing up, she was often asked by her friends, teachers and family members why she was always so quiet. The simple answer is that this was just her nature. She felt unhappy in loud, crowded and gregarious environments like summer camp, and she craved quiet places where she could read and commune with her thoughts.

> _"There's a word for 'people who are in their heads too much': thinkers."_

### 3. Introverts are sensitive and often feel like something is wrong with them. 

If you're an introvert, then you probably try to avoid noisy parties. Indeed, you probably steer clear of crowded restaurants and bustling streets. But you might not be sure exactly why you do this.

Introverts' nervous systems are more delicate than the average nervous system — and this makes introverts more sensitive to the sights, sounds and tastes around them.

As a result, introverts have more intense reactions to their environment and to occurrences within that environment.

Psychologist Hans Eysenck studied this very phenomenon in 1967. In order to determine whether the bodies of introverts react differently than the bodies of extroverts to stimuli, he measured the amount of saliva that each group produced when lemon juice was dropped on their tongues. The results showed that introverts clearly produced more, suggesting that they have more sensitive nervous systems.

Introverts also often think that something is wrong with them and that they should be more outgoing.

This is due to how much the world celebrates extroverts and those who embrace the spotlight. Yet roughly one-third of the world's population finds the spotlight unappealing — and there's absolutely nothing wrong with these people.

At many schools and colleges, it's the loud and outgoing students who are considered popular, even if they aren't particularly friendly, smart or talented. This can cause introverts to wonder whether they need to change something about themselves in order to be appreciated and fit in.

However, being an introvert is not a character flaw or something that needs to be fixed. As we'll learn in the following blinks, it's a quality that has its own unique advantages.

### 4. Introverts can excel in the classroom when teachers provide the right environment. 

Teachers have good reasons for promoting classroom discussions: it allows the students to hear different opinions and points of view, and it gives the teacher a way to gauge how many students have actually done their homework.

But expecting students to verbally participate in class puts introverted students at a disadvantage; it's an expectation that ignores an important fact: good and motivated students may be afraid to voice their opinion in front of others.

One student, Brianna, describes how her teacher would provide each student with three sticks before class. The students then had to sit in a circle and discuss the topic of the day. A stick could be disposed of every time you made a contribution to the discussion. If you had any leftover sticks at the end of class, the teacher could give you a lower grade for the day.

This kind of incentive doesn't lead to meaningful discussion; it leads to students saying meaningless gibberish just to get rid of sticks. And if there's one thing introverts hate, it's talking just for the sake of talking.

Introverts need to explain their genuine fear of public speaking to their teachers so that those teachers can learn and make the proper adjustments.

This way, arrangements can be made and small discussion groups can be used rather than big ones.

Another useful technique that teachers can use is the "Think/Pair/Share" approach. It works like this:

Teachers designate a certain amount of time for isolated thinking (something introverts are great at) and then have students pair up and discuss their thoughts. After some time, the teacher can bring the full class back together for these thoughts to be shared.

Talking one-on-one provides a friendlier environment for introverts. It gives them a way to participate in class, and, by the end of the class, the teacher can be sure that all the students have engaged with the ideas under discussion.

Classrooms aren't the only challenge that introverts face, however. In the next blink, we'll discuss how introversion can affect friendships.

### 5. A fear of loneliness shouldn’t make introverts compromise on real friendship. 

Some people have the gift of the gab. They can walk into a room and effortlessly win over strangers with their charm. Naturally, this can lead to a large circle of friends and acquaintances, which is something introverts rarely have.

Introverts usually have a small, select group of friends that they are quite close with.

At school, this small circle of friends may act as a protective bubble, since introverts often feel self-conscious, as though their every move is being watched. But surrounded by a few good friends, they will relax and feel more comfortable and at ease.

Gail is a teenager from Ohio who is cautious about whom she considers a real friend. She only uses the word for people she trusts and feels she can share her true feelings with. This selectivity leaves her with three friends.

For her, this is enough — but such a small circle of friends may not feel large enough to you. If you're hoping to forge some new friendships, be sure to keep one thing in mind: don't compromise or maintain unhealthy relationships.

It can be difficult for an introvert to make new friends, a fact that can lead to situations similar to one that Georgia (a young introvert) found herself in. Worrying that she'd always be alone and never be considered cool enough by the cliques in her school, she formed friendships with some pretty unfriendly people — a group of girls that bullied her for things as trivial as having a different taste in music.

It's better to let go of unhealthy relationships and risk solitude to be able to eventually find real friends.

Which is exactly what Georgia eventually did. Unable to put up with the bullying, she cut off all contact with these girls, despite the lonely and isolated feelings that followed. And then, one day in science class, she met a kind-hearted girl who, in the end, turned out to be a real friend.

### 6. Good friends and a quiet spot can help introverts through social events. 

One of the most painfully stressful experiences for an introvert is attending a loud and crowded party packed with strangers. But there inevitably comes a time when going to one of these parties becomes unavoidable.

Fortunately, there are tips to help you get through it.

A good way to ease yourself into the situation is to spend time with one or two friends beforehand and go to the party as a team. After all, it's far less distressing to enter a party with a trusted friend than it is to go solo.

This is especially true for big school events like the prom. Carly, a teenage introvert, was dreading having to go to her junior prom. But she met up with a group of her friends prior to the event, and, as they ate dinner and had fun taking pictures together, Carly felt the tension fading away. These pre-prom activities made the prom itself a less frightening proposition.

It's also important to remember to breathe and give yourself space to reboot.

Noise and constant chatter are exhausting to an introvert, so finding a quiet spot that allows you a few minutes of quiet relaxation can do wonders to restore your strength.

Like many teenagers, Jenny was excited to one day be able to throw a big party at her house. But, as an introvert, Jenny also found the idea rather frightening.

She was able to get through the night and even enjoy herself, though, by occasionally stepping away and spending a few moments in the safe haven of her bathroom. It gave her a chance to take some deep breaths and recharge her batteries, which enabled her to be a good host.

Being an introvert doesn't mean you can't enjoy a good party. And, as we'll learn in the next blink, it doesn't mean you can't be a good team player, either.

### 7. With intense focus and inward visualization, introverts can be successful team players. 

In the popular imagination, introverts tend to be the kind of people who don't work well with others. Yet, with the right approach, introverts _can_ be valuable team players.

So if you're an introvert, don't give up your dreams of playing a team sport. Often, you can grow and improve your skills outside of the team setting.

Jeff loved playing lacrosse, though he didn't realize his passion until he was 13 years old. This meant he had some catching up to do in order to reach the same skill level as his peers, so he began his own vigorous training schedule.

Jeff had always enjoyed playing games by himself, so now he began practicing and greatly improving his lacrosse skills by bouncing a ball against a wall thousands of times each and every day. By putting in hours of work on his own, he was more than ready when it was time to play on the field with his teammates.

There are other quiet approaches to team sports, including the important practice of visualizing your success, which taps into the great focus and imagination that many introverts have.

Unlike his teammates, Jeff wasn't eager to pump himself up before games by shouting, jumping around and bumping chests. Instead, Jeff would find a quiet spot to sit with his headphones on and visualize the different ways he would break through the opposing team's defense.

And it worked like a charm. As Jeff's visualization technique got better and more focused, so did his performance and his team's winning streak. Eventually, Jeff was breaking records and receiving awards for his excellent performance.

So don't let introversion keep you in the shadows of life. With a good attitude and the right approach, there are plenty of ways you can use your gift of focus and introspection to put yourself ahead of the game.

### 8. Final summary 

The key message in this book:

**There's no avoiding the fact that young introverts face challenging situations in life, especially when it comes to school and social situations. However, with the right tools and frame of mind, introverted youngsters can not only persevere but flourish. By harnessing their potential, along with the focus and thoughtfulness that comes with introversion, a quiet power can be unlocked and put to great use.**

Actionable advice:

**Prepare the things you want to say.**

If you already know that tomorrow will bring yet another class discussion (with your teacher breathing down your neck), prepare what you want to say by planning a couple of key sentences beforehand. This gives you the opportunity to shape the discussion and show that you are motivated to contribute.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Quiet_** **by Susan Cain**

_Quiet_ focuses on the strengths and needs of both introverts and extroverts. These blinks describe the situations in which both personality types feel comfortable and the ways in which each can use the potential of their personality to the fullest.
---

### Susan Cain, with Gregory Mone and Erica Moroz

Susan Cain is a former corporate lawyer and the cofounder of Quiet Revolution, a mission-based company that works to unlock the great potential of introverts. Cain is also the author of the best-selling book, _Quiet: The Power of Introverts in A World That Can't Stop Talking_.

Erica Moroz is a journalist and frequent contributor to the _American Reader_, a literary magazine.

Gregory Mone is the author of several children's books with subjects that range from Santa Claus to the Titanic.

