---
id: 57025b4f8fb8ff0007000051
slug: quirkology-en
published_date: 2016-04-07T00:00:00.000+00:00
author: Richard Wiseman
title: Quirkology
subtitle: The Curious Science of Everyday Lives
main_color: 60C6EC
text_color: 377187
---

# Quirkology

_The Curious Science of Everyday Lives_

**Richard Wiseman**

_Quirkology_ (2007) takes a uniquely scientific look at some common questions that are often dismissed as trivial: What kind of impact does astrology have on our lives? Is the number 13 really unlucky? Can a joke truly be harmful? And more!

---
### 1. What’s in it for me? Learn about the enticing science of human quirks. 

Did you know that, right before Christmas, children's drawings of Santa Claus are typically a lot larger than ones drawn in January? It's true. In fact, people do a lot of weird things, and there are numerous studies dedicated to revealing them. One day, these studies might constitute an entire field in the social sciences: _Quirkology_.

But, until then, these blinks will provide you with the basics. Does astrology work as an investment tool? What's really in a name? Are jokes always a laughing matter? If you've ever asked yourself such questions, then these blinks are for you.

Read on and also discover

  * how safe it really is to be around a black cat;

  * what the way you write the letter "Q" says about your ability to lie; and

  * why, sometimes, astrology _can_ tell you something about a person's character.

**This is a Blinkist staff pick**

_"These blinks are fantastic! They look into academic studies of the weird and wonderful to explain many of our quirks and peculiarities. For example, did you know you can tell a liar by the way they draw a "Q"? And that black cats are nothing to be feared? These blinks are just packed with fascinating anecdotes and examples."_

– Thomas, English Editorial Lead at Blinkist

### 2. Astrology doesn’t work – or, at least, not as expected. 

Do you check your daily horoscope? Are the predictions always mysteriously accurate? Well, that notwithstanding, science demonstrates that astrology actually does a poor job of predicting the future.

In one experiment, the author tried to determine if astrology could help make predictions about the stock market. He asked three individuals — an astrologer, a normal investment specialist and a four-year-old girl — to pick successful financial investments.

The results spoke for themselves: the astrologer was the least successful, losing 10.1 percent of the money he invested. The regular financial expert did only slightly better, losing 7.1 percent. And, surprisingly, the four-year-old girl, who invested at random, fared the best, losing only 4.6 percent.

Furthermore, when the participants were asked to make a second round of investments, the child made a profit of 5.8 percent, while the astrologer remained at a loss of 6.2 percent. 

You may be thinking: Okay, so astrology can't predict events, but it can still tell us about who we are, right?

Actually, the stars don't affect us. What changes our behavior is our belief in what astrology says.

For example, renowned psychologist Hans Eysenck performed a test on over 1,000 students to determine who were extroverts and who were introverts. It turned out that students with an "extrovert" astrological sign (such as Aries and Leo) scored as extroverts on the test. The same held true for students who were born under "introvert" signs, like Virgo and Scorpio.

But Eysenck suspected that the students' knowledge of what their sign supposedly means had an influence on their answers. So he performed another test, this time with children, and the results showed that there was no relationship between personality and astrological sign. 

As we can see, astrological predictions are often wrong, and even when they aren't, this is because we tend to unconsciously change our behavior in order to make them seem right.

### 3. Again and again, scientific investigation disproves our superstitions. 

You undoubtedly know that the number 13 is supposed to be unlucky. From Friday the 13th to the ill-fated Apollo 13 space mission, the belief that the number 13 brings bad luck has been around for ages. But is there any truth to such superstitions?

Scientifically speaking, absolutely not.

To test one theory, Mark Levin, a high school student from the United States, set out to determine if there was any truth to the old tale about black cats causing bad luck.

He based his test on whether or not a black cat could affect your luck when guessing the results of a coin toss. Mark split the test into two rounds. During the first, participants guessed heads or tails in a series of coin tosses. Before the second round, Mark had cat trainers lead a black cat before the participants. The cat, however, had no effect on the luck of the participants', whose ability to call heads or tails neither improved nor worsened.

So much for fearing black cats. But what about full moons?

Within the medical profession, it's long been thought that a full moon increases the incidence of patients admitted to the emergency room.

Well, researchers in the United States looked into this claim by checking a year's worth of records from trauma centers and comparing them against the lunar cycle. Alas, they found no such increase to justify the superstition. So, don't worry, there's no need to fret about that next full moon. 

There's actually been a long history of people debunking superstitions like these.

Back in the 1880s, war veteran William Fowler started New York City's _Thirteen Club_. On the 13th day of every month he would invite guests to a dinner where people would test every possible superstition: guests would open umbrellas indoors, spill salt on tables and cross forks. Naturally, guests reported no bad luck occurring as a result.

So, since many of these myths have already been disproved, why do they persist? In the next blink we'll look at the science behind our complex relationship with the truth.

> _"The most important thing … is the destruction of superstition. Superstition interferes with the happiness of mankind."_ - Robert Ingersoll, politician, at the Thirteen Club in 1886

### 4. Science shows that it’s easy to lie and get away with it – but there’s a clue to uncovering deceit. 

As a child, did you find magicians fascinating? Maybe you still enjoy a sense of wonder when watching a card trick or a disappearing act. This is perfectly fine, as long as you recognize magicians for what they truly are: talented liars.

Unpleasantly enough, lying is a universal human action — despite what respondents to a survey conducted by the author said.

Only eight percent of these respondents admitted to having ever told a lie. And it's probable that some of this eight percent were deceiving themselves.

As a continuation of the study, the author asked these same people to note each lie they told in a journal. The results showed that, on average, adults told at least two notable lies every day.

And, unsurprisingly, some people are better liars than others.

Here's an easy way to tell if your personality type predisposes you to being a good liar: Without giving it much thought, imagine drawing an uppercase Q on your forehead.

If, in your mind, you drew the Q with the tail pointing toward your left eye — making it easier for other people to read — then you are what is called a _high self-monitor_. Such people are highly aware of the effect they have on others, and they're usually better liars.

But most people aren't very good at detecting lies to begin with.

This was demonstrated in another experiment, in which the author asked people to say whether an actor was lying when he said he preferred the film _Gone With the Wind_ to _Some Like It Hot_. They couldn't do it.

Nonetheless, this experiment showed that there are signs that point toward the truth. For instance, the actor gave more details and spoke at greater length about the movie he truly preferred. If someone remains quiet and vague about a subject, your suspicions may be well-founded.

So, we have an easier time lying than detecting the truth. It's not uncommon, however, for our behavior simply to be irrational.

### 5. Human beings aren’t nearly as rational as we think. 

Have you ever done something and asked yourself later, "What was I thinking?!" Maybe you went on an irresponsible shopping spree or got in an argument for no reason at all. Is there a scientific explanation for such lapses?

Decision-making is based on context, which can result in some pretty irrational actions.

In one study, researchers asked people to make a decision based on two scenarios:

In the first, participants were asked to imagine that they'd gone to a store to buy a £20 calculator; right before making the purchase, however, they learn that the calculator will be on sale the next day for just £5.

In the second scenario they were asked to imagine buying a computer for £999 — and then learning that, the next day, it will be on sale for £984.

In both scenarios the price difference is the same: £15. But 70 percent of participants said they would wait for the sale to buy the calculator but not the computer. Irrationally, people only regard a savings of £15 as important when compared to the overall price.

Irrationality isn't limited to shopping, however; it happens when we meet new people as well.

It turns out that a person's name can lead to a lifetime of irrational reactions.

For example, studies have shown that teachers grade a student's work more favorably if they have a popular name like "David," as opposed to a less common name like "Hubert." Something similar can happen in college, where students with less popular names end up socially isolated.

Psychologists in the 1960s even determined that people with unusual names tended to be more psychologically disturbed than people with more common names.

But for those who can overcome the irrational responses, people with odd names are found to have a greater chance of finding fame.

After all, a name like Benedict Cumberbatch is far more memorable than someone named Joe Smith. And being memorable is an important step toward fame.

> "_Life is infinitely stranger than anything the mind of man could invent._ " - Arthur Conan Doyle

### 6. Science offers some interesting insights into human altruism and selfishness. 

Have you ever found yourself at the side of the road with a broken down car, hoping someone might stop and help you out? Maybe the experience left you thinking, "How would I react to a stranger in need of help?"

Psychologist Richard LaPiere tried to get to the bottom of such thoughts in a study he conducted in the 1930s.

LaPiere traveled through the United States with a young Chinese couple, visiting 66 hotels and 184 restaurants. Wherever they went, they were treated with courtesy and respect. 

In the second part of his study, La Piere's assistant sent out questionnaires to each of the businesses, asking them if they would accept Chinese customers. Troublingly, over 90 percent said no. Of the other 10 percent, most answered that they were unsure of how they'd react and only one woman replied that she would welcome the customers. It turned out she had enjoyed a recent visit from a nice professor and a young Chinese couple!

So it turns out people are often friendlier than they think they'll be. This is because it's nearly impossible to predict how we'll behave until we know all the details of a situation.

Examining selfish behavior, the author conducted another experiment. A cashier at a large corporate store was told to give the unwitting participants too much change after they'd made their purchases. While keeping the extra money would be considered selfish and wrong, most people did just that. Some even wore a big smile as they walked away!

Then, the author conducted the same experiment at a small, independent local shop. But here the customers reacted differently: they gave the money back. The customers identified with the cashier of the small shop and felt wrong about cheating him out of his money.

### 7. Our favorite jokes can have rather toxic side effects. 

Have you heard the funniest joke in the world? No person still living ever has. It was used by the military (in a famous Monty Python skit) to make the enemy laugh themselves to death. Okay, but if there really _was_ a funniest joke, what would it be?

When the author searched for the funniest joke in the world he discovered that many people enjoy jokes that make them feel superior.

In his search, the author created Laugh Lab, an online site where people could enter their own favorite joke and rate the submissions of others. The results were clear: The highest-rated jokes were those that made us feel superior by allowing us to laugh at the foolish behavior of others.

And though not as dire as those in the Monty Python skit, the consequences of such superiority jokes can be very negative.

These jokes often hinge on stereotypes. So, in 1997, psychologist Gregory Maio conducted a study in Canada on the effect these jokes can have on those who are made fun of.

He formed two groups, one of which read ordinary jokes not aimed at any group of people; the other group was given jokes making fun of people from Newfoundland.

Afterward, the two groups were questioned about their feelings toward different people, Newfoundlanders among them. The results showed that group members who'd read jokes about Newfoundlanders had much harsher opinions about them than group members who hadn't.

These kinds of jokes can also negatively affect the confidence of the people they target.

In another experiment, two groups of blonde women were given an intelligence test. Prior to the test, one group was given jokes denigrating the intelligence of blonde women. Naturally, this group scored lower on the test than the group that didn't receive the jokes. 

So, think twice before you decide to spread another superiority joke. Not only do they encourage discrimination, they are also harmful.

### 8. Final summary 

The key message in this book:

**Don't believe everything you hear. A lot of myths, from astrology to superstitions about bad luck, turn out to have no bearing on our lives whatsoever. On the other hand, some things, like your surname or a joke** ** _can_** **have a surprisingly strong impact on your life, affecting the thoughts you have about yourself.**

Actionable advice:

**Defy your superstitions.**

Invite twelve friends to a party on the thirteenth day of the month and perform all the bad luck activities you've been warned against. Who knows, like the members of the legendary Thirteen Club, defying superstitions might just prolong your life! 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _59 Seconds_** **by Richard Wiseman**

_59 seconds_ (2010) lays out some handy tips and insights backed by scientific research. Apply them today, and experience the change you want in your life.
---

### Richard Wiseman

Richard Wiseman is Professor for the Public Understanding of Psychology at the University of Hertfordshire. He specializes in magic, astrology, deception and the paranormal. His other books include _Rip It Up_, _The Luck Factor_ and _59 Seconds_.

