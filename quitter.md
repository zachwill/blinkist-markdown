---
id: 54b3b5df6466330009750000
slug: quitter-en
published_date: 2015-01-16T00:00:00.000+00:00
author: Jon Acuff
title: Quitter
subtitle: Closing the Gap Between Your Day Job and Your Dream Job
main_color: D82B31
text_color: BF262B
---

# Quitter

_Closing the Gap Between Your Day Job and Your Dream Job_

**Jon Acuff**

Finding out where you really want to be in life requires patience, hard work and planning, but anyone can do it with the right dedication. Filled with many personal anecdotes from a 12-year span of job-hopping, _Quitter_ shows you the smartest way to quit your day job for your dream job.

---
### 1. What’s in it for me? Learn how to move from your current job to your dream job. 

We all have our own idea of where we really want to be. Some of us will have even made tentative plans to reach that place. Unfortunately, hardly any of us make it to the Promised Land, and many Americans are stuck either in jobs they hate, or in jobs they simply don't care about.

That's where these blinks can help. Based on Acuff's own journey from a series of dead-end jobs to his goal of being an author, they explain how you, too, can make the jump to your dream job happen. Read on and take your first step to the future you really want for yourself.

In these blinks you'll discover

  * why finding your dream job starts by not quitting your current one;

  * why it's better to work hard in the morning than at night; and

  * why some people never seem to have enough.

### 2. Finding your dream job doesn’t start with quitting your current one. 

A survey in the United States in 2011 showed that 84 percent of employees planned to look for a new job that year. That's a huge number, isn't it? You might've even been one of them!

Yet how do you go about finding this dream job? By quitting your old one? Well, no. Don't quit your day job. If you quit right away without planning what to do next, two things could happen.

First, you might replace your old boss with new mini bosses that were previously hidden. That means your electricity bill, your water bill, your health insurance — in short, bills. When they rule your life, there's not much chance of being your own boss.

Second, your personal relationships can be put under pressure. Worrying about money leads to an unhealthy and neurotic mind-set. You might find yourself shouting at your partner for spending too long in the shower and causing the water bill to go up, for instance.

There's a wiser way to find your dream job: keep your day job. Well, at least initially.

Why? You run less risk of getting sucked into a opportunity that isn't actually good enough. You don't have to feel forced into accepting any old offer just because you have no others.

Jon Acuff's first book contract, for example, was terrible. A publisher offered to buy the book for nothing, keep 100 percent of the profits and then sell copies back to him so he could then sell them via his blog. Luckily, he had work at the time, so he could reject the offer. Imagine if he'd been unemployed and desperate. It probably would've been a completely different story.

Of course, staying in your day job also helps you maintain a disciplined and healthy lifestyle, whereas being unemployed can lead to procrastination and unhappiness. And we want to avoid a negative state of mind.

> Fact: Ryan Gosling got a job making sandwiches in a deli to keep structure in his life after the success of the film _The Notebook._

### 3. Hinge moments can help you rediscover your dream. 

Some of us have a vague idea of what we'd like to be doing in life, like, "I'm an accountant _,_ but I want to be an artist."

Yet not everyone can see their path with such clarity. Some of us want to pursue a dream without even knowing what it is.

So how can we recognize our dreams?

Instead of asking the overwhelming question, "What do I want to do with my life?" ask yourself a more manageable question, like, "What have I done in my life so far that I really loved doing?"

Dreams don't suddenly appear to us as surprises. Usually, we realize what they are when we revisit them again after a first encounter.

Bono, the anti-poverty campaigner and lead singer of U2, realized his dream this way. He first visited Ethiopia in the 1980s. He struggled to understand the poverty and hunger faced by the people there, and he wanted to try to help.

About a decade later, he realized what he could do to make a difference. So in the 1990s, he started working with the likes of Desmond Tutu and Nelson Mandela to fight against poverty.

So if you've forgotten what that special something was, look back through your life for what the author calls _hinge moments_.

A hinge moment often initially appears to be insignificant or not obvious. It could be happy or sad. Ultimately, it's a moment that pushes you in a direction you might not have been expecting to take.

When Acuff was a young student, for example, his third grade teacher asked him to write a book. He wrote it even though the other students were doing a different project. His book was then laminated and bound to make it look real. He later identified this as a hinge moment: he realized that writing was something he loved, and could do forever.

> _"Finding out what you love doing most is about recovering an old love or an inescapable truth that has been silenced for years, even decades."_

### 4. View the risks you take through a telescope. 

Naturally, following your dream does involve some hazards.

Acuff encountered many risks when pursuing his dream of becoming a writer. He quit his job, moved his family to another state and left his friends behind — a high price if things didn't work out.

Being aware of and coming to terms with these risks is crucial if you want to find your dream job. Luckily, there's a good way to ensure these risks don't get the better of you.

There are three main ways to view risks, only one of which enables you to overcome them easily.

First, you can look at them through a _magnifying glass_, which blows them out of proportion. This makes them seem huge and impossible to overcome.

Alternatively, you can look at them through a _kaleidoscope_, in which all your fears and worries intersect, and become complex. This also makes them seem massive and overwhelming.

The right way to look at them, however, is through a _telescope_. This is where you see the risks associated with your dreams from a safe distance. They are small and manageable and certainly not too large to put a stop to your dream.

One common risk is perfectionism. Don't become a perfectionist. It's better to be 90 percent perfect and share your ideas with the world, instead of hanging on and waiting till you're 100 percent perfect.

### 5. Use your day job to find your dream job. 

We saw earlier that you shouldn't quit your day job if you want to successfully chase your dream job, but you might've been thinking, "I hate my job. I can't keep at it any longer."

If that's the case, try thinking of how you can get your terrible day job to direct you to your true calling. Your day job can actually give you the time you need to realize your dream.

For instance, when Acuff worked as a copywriter for AutoTrader.com, he had time to develop his dream of becoming a writer. His day job allowed him to make mistakes that didn't cost too much. He could try new things with less risk.

If you're having a tough time remembering what you like about your day job, try looking for parallels between your day job and dream job. You can almost always learn things in your day job that will benefit you when you have your dream job.

In Acuff's case, he learned to write with clarity and precision, which was applicable for his future work as a writer, blogger and speaker.

It's also wise to look for purpose. Ask yourself — how can your dream job impact your day job? This will bring more purpose to your day job, and you'll probably start to appreciate it more.

Acuff's dream job was to connect with people and have a positive impact on their lives, but he forgot about the people who surrounded him every week at work. When he started reaching out to his colleagues, he could implement a bit of his dream, and bring new purpose to his daily work.

> _"The effort you invest in work will return big dividends to your dream."_

### 6. Allow yourself to practice and develop. 

Let's say you make a careful plan for finding your dream job, and you resolve to stick to it no matter what. Will this strategy work?

Nope.

Don't let _the plan myth_ paralyze you. Your success in a particular field _isn't_ dependent on you having everything planned.

Making a rigid plan won't lead you anywhere, unfortunately. Strictly following a plan means that you concentrate on small, insignificant steps instead of seeing the bigger picture. And of course, you can't possibly plan for everything, can you?

A soccer player can plan for some things in training, but can they plan for the ball to come in at an angle? For the defender to jump too early? For a breeze to lift the ball? For the glare from the sun to make the goalie miss it? There will always be unpredictable occurrences in the game. You simply can't plan for every eventuality.

So instead of having a fixed plan, grow through practice. Practicing involves giving yourself small but necessary tasks, such as visiting stores like the one you want to open, or reading blogs like the one you want to start. You'll gradually research your interest, without too much pressure.

When you're practicing, you haven't succeeded yet — you're not a famous writer or a renowned entrepreneur. On a global scale, you're pretty much invisible, but that anonymity can be great: you can make mistakes without everyone watching! You have the time to develop creatively.

Sergey Brin, the founder of Google, said, "We knew that Google was going to get better every single day as we worked on it . . . So we were never in a big hurry to get you to use it today. Tomorrow would be better."

So make the most of your time — don't rush. You'll be grateful in the end.

> _"Don't see invisibility as a punishment or a failure. See it as a gift."_

### 7. No dream will come true without hard work or hustle. 

Many of us imagine that one day, our dream will somehow fall into our laps. It will appear out of nowhere, like something from Cinderella. Unfortunately, things aren't this easy in real life.

If you want to succeed you need to learn to _hustle_. "Hustling" is slang for the effort you exert to make something happen. You should concentrate on using it at certain times.

It's better to work hard, or hustle in the morning. This is because the excuses haven't woken up yet. Few people balance their checkbook, return a call or find something good on TV at 5 a.m. It gives you a natural boost for the day and you feel good about making a positive start by having achieved something.

The opposite is true for night owls, however. Night owls face _short-timer's disease_. They're less likely to stay focused during the day. They're counting down the minutes until they get back to their dream job at night.

Hustling also requires you to monitor your progress, without measuring your success against your competitors. Nothing good comes from measuring yourself against others — if you're beating them, you'll get cocky and lazy; if they're beating you, you'll get discouraged and depressed. However, it _is_ important that you measure yourself.

Would you start a diet without measuring your body fat beforehand? In the same way, you need to have a gauge to see how your dream is developing. Just be sure not to let those numbers control your dream.

For instance, Acuff was fascinated by Google Analytics, which tracks activity on a website. He became so engrossed in it that he started defining his self-worth by the number of people who read his blog. Not good.

> _"Quantity leads to quality. The more you practice your dream, the better you get at your dream."_

### 8. Define success realistically, and by your own standards. 

We all want to be successful, right? But should we chase success above all else? Success can actually be dangerous, if we aren't prepared for it.

If you suddenly become famous, you'll find your life changing. You'll get more attention. People will start telling you whatever you want to hear, to get on your good side. This might make you arrogant, and less aware of your mistakes.

The 2008 financial crisis was a dramatic example of this phenomenon. Author Malcolm Gladwell blamed the crisis on overconfident leaders, who believed that they could control the economy and ignore any risks. Their arrogance resulted in a major error.

To ensure that you don't let your success affect you like this, keep a _realistic vision of success._ We often tell ourselves we'll finally be successful when we've got "enough." But what exactly is "enough"?

A football player, for example, might say it'll be enough when he wins the championship. But then he'll spend the next year expecting to be rewarded for being a star player — his "enough" will never be "enough."

It's much better if we're more realistic. Instead of never having "enough," define how much "enough" means to you, and concentrate on achieving it.

Acuff uses a service that allows him to email himself in the future. He tells himself what "enough" is.

In one email, for example, he defined "enough" as having a wonderful family and a good job. When the email reached him a year later, he was going through a period of intense success. His previous definition of enough helped him realize that he didn't actually need anything more. Chasing his dream would've only hurt him.

So remember to have a realistic perspective on whatever you want to do. Define what "enough" is for you, so you won't lose sight of your goal.

> _"That to me is the key to being successful as success. Instead of chasing 'enough,' you have to define it."_

### 9. Know when you’re ready to finally quit your day job! 

Once the practice and planning is over, it's finally time to quit your day job. Your dream job should comfortably fill your empty schedule. But before you drop everything, make sure you've got a few things in order!

First, give your dream a test run so you can fully know that your plan will work out. Acuff, for instance, didn't start working for Dave Ramsey, author and broadcaster, until June 2010, even though he offered Acuff a job in 2008. Right workplace, wrong job.

Next, make sure you have a strong support network. This is crucial in achieving your dream. When Acuff moved to Nashville for his new job he had a supportive family, which made it easier for him to make the decision. He drew security from his support network — his wife, his brother and the fans following his blog.

It also doesn't hurt to make a list of any potential risks. You won't be able to think of all of them, but at least you'll be prepared for certain possibilities, such as how your new job might affect your family.

Also, think of some guidelines to put in place for your new life. This will help you make a smoother transition from your old life to the new one.

For example, when Acuff began his new job with Ramsey, he agreed to speak at all of his events. However, he'd already pre-planned some events before starting the job, so he ended up being away for seven weekends in a row, which upset his wife.

If he'd thought of some guidelines to put in place, such as no back-to-back speaking events, he may have avoided the stress he put on the family. Avoiding negativity is crucial.

### 10. Don’t become too negative – it can cause you to fail. 

There are three main ways in which you could prevent yourself from closing the gap between your day job and your dream job. Let's examine them.

Many people view their lives as somehow removed from their jobs. They see a clean distinction between work and personal life. But this isn't always so easy to achieve. Why? Because in certain ways, your work defines who you are.

Imagine an accountant who says, "I'm not really an accountant." What they really want to say is, "My current job doesn't define who I am." If they had a job they identified with, they'd be more than happy to say, "I am a novelist."

Have you ever noticed how musicians often begin sentences with, "As a musician..."? This is because they are happy identifying themselves as musicians. Music is their passion. They are following their dream.

People often think of their day jobs as just something to fund the rest of their lives. But that diminishes their value. Your job probably also provides social interaction, opportunities to help others, a sense of accomplishment, and the chance to practice working toward your dream.

So don't just fund your life — get out there and live it!

Lots of people assume that their work will never be fulfilling, but life doesn't have to be like this. Of course it won't always be fun, but work is really a good way to bring change to the world, or at least your own world.

While working his day job during the recession, Acuff partnered with readers of his blog to raise $60,000 in 25 days. Two kindergartens in Vietnam were built with the money. Remember, a day job doesn't necessarily mean you can't accomplish fulfilling things.

> _"Start where you are. Start with what you have. Start today."_

### 11. Final summary 

The key message in this book:

**Your dream job isn't going to fall into your lap. You need to plan it before it can take shape. So don't quit your day job immediately — use it to help you transition into making your dream come true. Write your own definition of success, and make a flexible plan. With dedication, preparation and positivity you'll be able to close the gap between your day job and dream job.**

Actionable advice:

**Hustle in the morning.**

Most people will experience a boost for the rest of the day if they do something active and productive in the morning. So push yourself to get up early, and complete some small tasks. You'll feel more energized for the rest of the day, and be more productive over all.

**Suggested further reading: _Dream Year_ by Ben Arment**

Many people today are frustrated with their day-to-day jobs, because deep down they know that they're not really living the life they desire. _Dream Year_ helps you identify your dreams, and offers practical advice on how you can go about starting your own business to pursue them.
---

### Jon Acuff

Jon Acuff is a self-professed "serial quitter" who once had eight different jobs in eight years. He finally closed the gap between his day job and dream job in 2010, after he joined the Dave Ramsey team to become a full-time author. He's since gone solo. His other works include _Gazelles_, _Baby Steps and 37 Other Things Dave Ramsey Taught Me About Debt_ and _Stuff Christians Like_.

