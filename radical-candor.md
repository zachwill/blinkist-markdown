---
id: 59e67f5db238e100073fefb5
slug: radical-candor-en
published_date: 2017-10-18T00:00:00.000+00:00
author: Kim Scott
title: Radical Candor
subtitle: Be a Kickass Boss Without Losing Your Humanity
main_color: FEC233
text_color: 806119
---

# Radical Candor

_Be a Kickass Boss Without Losing Your Humanity_

**Kim Scott**

_Radical Candor_ (2017) offers valuable tools that any team leader or manager can use to establish the best possible relationship with their employees. You'll find an insightful approach to management that creates a working environment where great ideas emerge, and individuals can reach their full potential. It's time to stop doubting yourself and become the kickass boss your employees will be proud to follow.

---
### 1. What’s in it for me? Be the kind of leader who cuts through the bullshit. 

People have different views of what makes a good leader. Some see the sensitive and quiet type as their ideal boss, and others hold the drill-sergeant in high esteem.

But in these blinks, the case is made for the radically candid boss — the boss who can be both caring and direct and combine bluntness with empathy.

You'll find out why radical candor is something you need in your workplace, how to practice it on a day-to-day basis, and how it'll help you handle the inevitable challenges of leadership.

You'll also learn

  * why a fear of hurting people can create the most suffering;

  * when and why it's OK to fire someone; and

  * how ten minutes of silent listening can benefit your leadership skills.

### 2. Radical candor develops strong and beneficial relationships between managers and employees. 

Are you the kind of manager who arrives at the office feeling joyful and excited about the opportunity to shepherd your team through another great day of work?

This may not be an accurate description of your current routine, but it's a good example of how things could be once you start using _radical candor_.

At the heart of radical candor are two principles: a manager should personally care about their employees and challenge them in their work.

While it's true that there are no simple solutions guaranteed to work for everyone, these principles are a great way of developing trusting relationships in the workplace. And even though different people have unique personalities and reactions, radical candor can help any manager improve their relationship with just about any employee.

So, the first step is to become more than just a coworker by establishing personal relationships that prove you genuinely care. This means opening up, sharing and talking about more than just business. Contrary to what some may think, a good working relationship is a highly personal one.

However, a good manager must also be willing to challenge their employees when they're not meeting expectations.

This can be difficult for managers. Being candid and direct doesn't always feel like friendly and caring behavior. But as we'll explore further, challenging your staff to be their best is indeed the hallmark of a boss who truly cares.

We'll also look at the many benefits radical candor has for performance and office culture.

By being honest and direct with your employees, you'll find that new lines of communication readily open up. Staff will be quick and willing to accept your feedback, and they'll feel free to offer feedback regarding your work as team leader.

This attitude will spread throughout your workforce. When you're radically candid with your staff, they'll soon become radically candid among themselves, which makes for a healthy, productive and efficient work environment.

So, now that you know the basic principles let's dive in and have a closer look.

> _"Why 'radical'? I chose this word because so many of us are conditioned to avoid saying what we really think."_

### 3. Radical candor is a delicate balance between being direct and honest while not offending. 

If you have experience as a manager, then chances are you know what it's like to have someone react aggressively or defensively to constructive feedback.

Even when your intentions are positive, there's a delicate skill to providing guidance and criticism while coming across as helpful, rather than mean. And this is exactly what radical candor can achieve.

When the author, Kim Scott, was first starting out as a manager for Google, she had a boss who was brilliant at delivering feedback with radical candor.

Once, after Scott delivered a successful presentation, her boss took her aside to give her some constructive criticism. But her boss didn't just blurt it out; she began by honestly complimenting Scott's performance and the way she handled the questions from the audience.

Only then did she bring up the criticism, which was about Scott's habit of saying "um . . . " too often. At first, Scott didn't think this was too important — after all, the presentation went well. But then her boss elaborated: When someone says "um" too often, they can sound dumb, which would be a shame, especially when they're as smart as Scott.

This kind of comment is radical candor at its best. She made her point clear while managing to compliment Scott at the same time, and the author was soon working with a speech coach to become even better at her job.

This example shows us how radical candor gets results by being open, honest and direct.

First of all, Scott's boss didn't let the overall success of the presentation discourage her from pointing out how it could have been even better. And she didn't wait to provide feedback, either, which allowed Scott to immediately improve her performance.

Perhaps most importantly, her boss was honest and direct about both the positive and negative aspects of the work, and she didn't sugar coat anything. This ensured that Scott got the point and didn't mistake it for a personal attack.

> _"[On radical candor:] It's not mean, it's clear."_

### 4. Radical candor avoids the pitfalls of overly aggressive, lazy and fearful management. 

Anyone who's seen the movie _The Devil Wears Prada_ has seen a good example of a toxic workplace. The film features a terrifying and abusive boss played by Meryl Streep and based on Anna Wintour, the editor of _Vogue_ magazine.

This depiction of leadership is quite the opposite of radical candor; being honest and direct does _not_ give you the right to be obnoxiously aggressive.

Remember, radical candor is about expressing care for your staff, as well as being open and direct. Ultimately, when a boss humiliates an employee with their criticism, they create enemies, destroy morale and cause people to quit their job.

However, if you have to choose between being an unpopular asshole who tells it like it is or being a friendly boss who can't be critical of your staff, choose being unpopular. In the long run, it will be better for business and everyone involved if you are clear and honest.

But, of course, radical candor is the best of all options.

Other behavior traits that a successful boss should avoid are _manipulative insincerity_ and _ruinous empathy_.

Manipulative insincerity is another result of not caring and not challenging your employees to help them grow as professionals; it often stems from the manager being lazy.

For example, if an employee gives a poor presentation, a manager with manipulative insincerity may respond by saying it was adequate. They'll do this because it's easier than getting into a discussion about what needs to be improved.

A manager with ruinous empathy, on the other hand, will avoid conflict not out of laziness, but rather out of a misplaced fear of hurting an employee's feelings. Yet the result of ruinous empathy is that the employee will only suffer more due to a lack of improvement.

When a boss refuses to be critical of an employee whose performance is deteriorating, that employee will only grow more incompetent until the manager is forced to fire them.

So if you truly care about an employee, being honest is always in their best interest.

### 5. Bosses shouldn’t manufacture a false meaning for a job; they should provide professional development. 

In a perfect world, everyone would have a fulfilling job for which they have genuine passion. But in reality, some jobs are tedious, and it isn't the manager's role to make them appear otherwise.

Being radically candid means that you're honest and don't sugarcoat things to motivate your employees.

Having a job with meaning and a noble purpose can inspire employees to perform to their best ability, but it's not up to you to manufacture that meaning and purpose when they're not there. Doing so can backfire and make matters worse.

When the author was a manager in Google's customer support department, she tried to manufacture meaning and convince her staff that their work was of vital importance to Google's creative workers. But at least one employee saw right through her phony pep talk. And he was absolutely correct when he told her that it would be better to tell the truth and admit that a lot of necessary jobs can be tedious and unfulfilling.

There's no use dodging the fact that some jobs have only one reward: earning enough money to pay the bills and live comfortably. So be honest with your employees and tell them that there is still satisfaction to be had in doing a job well, and make sure that good work is acknowledged. There's no need to feel you must solve your employees' existential dilemmas.

However, you can provide them with perspective on their careers and the tools to help them grow and develop as professionals.

To do this honestly, you'll need to spot the difference between a _superstar_ and a _rockstar_.

Superstar employees need to be challenged and permitted to grow quickly so they can move up the corporate ladder and reach their full potential.

Rockstar employees offer a steadier presence, and they will be great as long as they're given the stability and time to excel at the job they're given.

Up next, we'll look at a different employee, the _falling star_.

### 6. Firing someone is a troubling experience, so be sure to take every consideration before it happens. 

If you've been fired, there's a good chance you felt some animosity toward your ex-boss. This is a natural reaction, but it can blind us to the fact that this likely wasn't the outcome that the boss wanted, either.

Any manager will tell you that firing people is one of the hardest parts of the job.

Firing someone is tough because you're making someone else's life incredibly difficult, even if it's only temporarily. And in many cases, it's not only the employee who's suffering but their spouse and children too. Not only are you taking away a source of money, but entire families can lose health insurance, and the whole incident can cause all sorts of marital strife.

This is why a manager needs to ensure someone is fired for the right reason. It's also important to remember that other employees are aware when someone is let go, so the incident should still reflect you as being an honest and caring boss.

For this to happen, you need to consider three main points before a firing takes place.

First of all, every effort should be made to help the employee improve their performance. This shows that you care and that firing is your last resort. As always, you should use radical candor to support their good work and be honest and direct in how they can improve, while avoiding any personal attacks.

Second, you need to consider the effect a bad employee has on the overall team. If the person is an annoying and demoralizing presence, then it makes sense to let them go.

The third aspect to consider is an outside opinion. If you have any doubts, it's good to bring in an impartial third party and get their opinion on the matter.

When all of this is taken into consideration, you should have a clear understanding of what needs to be done.

No one wants to be involved in a firing, but there's also a good chance that the departing employee needs to find a job that better suits his skills.

### 7. Managers shouldn’t tell people what to do; instead, they should practice collaborative leadership. 

There's a good way and a bad way to approach team leadership. The bad approach is to think of it as an opportunity to boss people around. The right approach is to see your team as a great opportunity to collaborate with talented individuals.

Nevertheless, many managers mistakenly believe their job is to tell people what to do, which will only lead to problems since leaders make mistakes.

Steve Jobs knew he could be wrong, so he relied on his team to speak up and challenge him when they disagreed with his ideas.

On at least one occasion, Jobs was furious when an employee gave up and let him win an argument because it later turned out the employee was right. Jobs had to remind the employee that he was hired to ensure Jobs didn't make these kinds of mistakes, and not back down from a fight.

So, how do you lead a team without giving orders? It might sound like a paradox, but the answer is _collaborative leadership_, and there is an art to practicing it.

The first step is to really listen to what the people in your team have to say. When you do this, your team will feel safe to speak their minds and have the kind of discussions that lead to brilliant ideas.

The second step is to give your team the time and space to develop their ideas. Otherwise, they can end up being shot down before they ever have a chance to be clearly understood.

The third step is to allow for healthy debate so that the best ideas are presented and agreed upon.

The final step is for you, the manager, to persuade other company executives that your team's idea is worth pushing forward. Then it's up to you to execute the idea and make sure everyone gains valuable insight from the results.

This process gets repeated over and over again.

### 8. Depending on your personality, you can listen quietly or loudly. 

There are a lot of tutorials on how a boss should speak, yet knowing how to listen is just as important.

The first thing you should know is there are two ways of listening, and you should practice the one that suits your personality.

On the one hand, there is _quiet listening_, which suits leaders who prefer to let others do the talking.

Apple CEO, Tim Cook, is one such leader. He's notorious for using painfully long stretches of silence to get others talking.

If you're practicing quiet listening, you should spend at least ten minutes of an hour-long conversation patiently listening to what the other person has to say.

This is especially useful for people who want honest opinions. Too often, managers will interrupt people and share their own opinions. As a result, those who are talking will change their message to match the manager's opinion.

So, for quiet listening to work, you must provide the encouragement and freedom for your team to feel comfortable saying what's truly on their mind. Only then can you settle in for a good, quiet listen.

_Loud listening_, on the other hand, is good for leaders with a more confrontational personality.

This was how Steve Jobs listened. He would make a strong statement and insist that others provide an equally strong response to keep the discussion going. Since loud listening pushes people into talking, it is an effective way to draw shy employees out of their shells.

However, you will need to instill confidence in your teammates if you want them to speak up. For this to happen, you'll need to respond positively to their opinions, even if you don't agree with them, so that they'll continue to speak up and challenge your ideas.

Listening to your employees is key to promoting an effective and creative team. And as we'll see in the final blink, it also puts you in a position to help move their careers forward.

### 9. To support your employees, have honest discussions that reveal their true motivations. 

While there's nothing wrong with working a job just to pay the bills, you're bound to have people in your team who have big dreams they're hoping to realize.

Managers should support the dreams of their staff and help them approach those dreams in a realistic fashion.

To do this, you must first talk (and listen) to your employees so that you understand their aspirations, and they know that you are personally invested in helping them get on the right path.

When Russ Laraway was the director of sales at Google, he used _career talks_ as a way to help keep his team motivated.

One of his employees was Sarah, and when he asked about her aspirations, she initially responded with some hesitation and said she hoped to one day be a boss, like Russ. But Russ sensed that Sarah wasn't being completely honest with him, so he asked her if she had any other visions for her future. Sure enough, Sarah described her dream of owning a farm that grew spirulina, an algae-like super food.

Russ was now able to talk to Sarah about her upbringing and identify her main motivators, such as helping the environment, working hard, and being a financially independent leader.

With this in mind, they could now focus on developing Sarah's management skills rather than her analytical skills to make sure her current job was preparing her for running that spirulina farm.

To understand your employees' dreams and identify those important motivators, use one of three kinds of conversations.

The first is _the life story conversation_ where the employee tells you everything leading up to the present day, and you try to find their motivating factors.

The second is _the dream job conversation_, where they describe their biggest career desire.

The third is _the 18-month plan conversation_, where they look into the immediate future, and you identify everything that can be done to keep them on the right track.

By following these guidelines, it won't be long before you have a team of highly motivated individuals working together and achieving great things.

### 10. Final summary 

The key message in this book:

**By using radical candor, you can become the best possible boss. Radical candor helps you listen and express genuine care for your employees. It also allows you to directly challenge your staff members in a constructive way to which they'll be receptive. This makes leadership a collaborative and personal process that brings out the best in everyone.**

Actionable advice:

**Have a growth plan for your employees.**

Don't just think of your staff as people who are only there to do what you assign them to do. Think of them as individuals on a career path and work with them to develop a plan that keeps that career on track.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _How to Be a Positive Leader_** **by Jane E. Dutton and Gretchen M. Spreitzer**

_How_ _to_ _Be_ _a_ _Positive_ _Leader_ examines cutting-edge research from the field of positive organizational behavior, in which companies aim to foster both a positive attitude to work and high performance among employees. The research is complemented with vivid examples from real organizations.
---

### Kim Scott

Kim Scott is an experienced CEO who's worked for a variety of Silicon Valley companies including Twitter, Dropbox and Google. She's also a former faculty member of Apple University and the current CEO of Candor, Inc., a company she co-founded to provide more resources for managers and bosses in need of support.

