---
id: 5922e745b238e10007b6af1b
slug: radical-collaboration-en
published_date: 2017-05-26T00:00:00.000+00:00
author: James W. Tamm and Ronald J. Luyet
title: Radical Collaboration
subtitle: Five Essential Skills to Overcome Defensiveness and Build Successful Relationships
main_color: 3FC7B0
text_color: 277A6C
---

# Radical Collaboration

_Five Essential Skills to Overcome Defensiveness and Build Successful Relationships_

**James W. Tamm and Ronald J. Luyet**

_Radical Collaboration_ (2004) offers invaluable methods to help you build effective and high-functioning collaborative relationships, as well as strategies to manage any kind of conflict that you might run into. At the heart of these methods are five skills that can turn anyone into a better teammate and turn any organization into an efficient and productive partnership.

---
### 1. What’s in it for me? Find out how to become a better collaborator. 

When reading about the likes of Bill Gates, Steve Jobs or Elon Musk, it's easy to think of success as a one-person show. But probe a bit deeper and you'll see that success depends heavily on being able to form great collaborations with others.

And this critical aspect is more important today than ever before. Just consider a study published in the Harvard Business Review in January 2016, which revealed that over the last 20 years, the amount of time both managers and employees spend collaborating with others has doubled!

So how do you make sure you get the most out of your collaborations? These blinks will show you, by outlining the five most important strategies to get your collaborative efforts off the ground right away.

In these blinks, you'll learn

  * how to step into the green zone;

  * why you should put your body where your mouth is; and

  * how to reach an agreement using straw designs.

### 2. Collaboration requires good intentions and an unselfish attitude. 

The modern business world is highly interconnected, with teams spread across the globe that, with the use of online communication, can nevertheless collaborate with one another as if they were neighbors. As such, it's more important than ever to have great collaborative skills.

So without further ado, let's look at five essential skills that every great collaborator should have — skills so useful they'll not only improve your business relationships, but your personal ones as well.

Every great collaboration needs to start with the right motives, which is why the first skill is _collaborative intention_ which refers to having the right mindset for collaboration.

To make sure you have the right mindset, you'll want to make sure to avoid the _red zone_ and stay in the _green zone_.

The red zone is a place of defensiveness, where people are mostly driven by a sense of self-interest and the will to outshine everyone else. When you're in the red zone, you're not thinking about creative resolutions or how to find win-win situations; instead, your selfish desires will create conflict.

Where you want to be is in the green zone, where your mind is focused on cultivating successful, long-term collaboration, and everyone is driven by open and cooperative values. People in the green zone are opposed to selfish gains and are instead driven to find solutions where everyone comes out ahead.

However, it's all too common for people to mistakenly think they're in the green and unconsciously sabotage a project by being in the red.

This is why it's important to always be honest with yourself and reflect on your own attitude while being open to feedback from your colleagues.

A helpful exercise is to ask your teammates for ten words that best describe your attitude or style.

What you want to be on the lookout for are words like "defensive," "closed," "anxious" or "competitive," as these are telltale signs that you're in the red zone. You can also ask your colleagues to inform you when you've mistakenly made a huge deal out of a small incident, as this will also help you stay firmly in the green.

> _"Defensiveness is a poison pill to good relationships. In conflict, defensiveness is like blood in the water to a shark."_

### 3. By using the first truth first tool, you can make sure important messages are received and understood. 

If you have any experience with relationships, you know that they require a healthy sense of trust, honesty and openness in order to work well.

The same holds true for a successful collaboration, which is why the second essential skill for collaboration is to _be truthful_. To this end, one strategy, known as the _first truth first tool_, is especially useful.

This tool is a way for teams to facilitate open and honest discussions about problems the moment they arise, so they can be fixed before they turn into major disasters.

It's a common mistake for supervisors and managers to put off talking to an employee for fear of hurting their feelings or creating an awkward situation.

With the first truth first tool, you can cut through these fears by being open and speaking the first truth first. This might mean saying, "We have an issue that isn't easy to talk about, but please be aware that this doesn't change how much we value your contributions to the team."

But there are additional factors to keep in mind if you want to become more aware of what exactly you communicate. In particular, it's essential to deliver a clear message free of any body language or behavior that could distort your intentions.

If you were to tell your loved one, "Yes dear, I love you," while rolling your eyes, the result would be a very confusing mixed message.

Likewise, a strong collaboration requires clear communication, free of contradictory behavior. This way, people won't waste time trying to decipher your intentions and won't doubt that you're being truthful.

So pay attention to the tone of your voice and your body language to make sure they match the intention of your message.

For example, if your message is to motivate people to be more punctual, don't dilute your message by showing up whenever you feel like it. If you want a unified team brought together by trust in one another, it's important that the leader be on the same page as everyone else.

### 4. Good listening skills are a vital part of effective collaborations. 

A strong and trustworthy collaboration is a two-way street: not only do you need to speak honestly, you need to listen with genuine intent as well.

In fact, being a good listener is beneficial to a relationship in more ways than one.

When someone is certain that you're listening to what she is saying, she will tend to be more relaxed and willing to share what's truly on her mind, as well as being more careful about the words she uses.

So, being a good listener doesn't just lead to a more deeply connected relationship, it also grants you access to a greater amount of more accurate information.

There are two things you can do to become a better listener, the first of which is to have a _tell-me-more attitude_.

This is a way of giving the speaker your full attention, which helps her feel safe about opening up and sharing her message confidently. After all, no one wants to share meaningful thoughts or feelings with someone who's only partially paying attention.

Having a tell-me-more attitude is about sincerely listening to the speaker and not trying to cross-examine her or interrupt her with probing questions. When someone is truly listening, he'll use phrases like "Tell me more about that," and "Is that so? Please go on," in an effort to hear more about what the person has to say.

A good listener will also make the speaker feel understood by letting her know that he has received and processed her message.

But truly understanding something is more than just repeating back what she has said — it's letting her know that you have an emotional reaction to her message as well.

In addition, an appropriate response will show that you've paid attention to the other cues the speaker gave you, including the tone of her voice and body language.

> _"There is more to listening than just hearing words."_

### 5. Make your choices wisely and take responsibility for your actions. 

Are you the kind of person who believes that his fate is predetermined? If so, you might not give the choices you make in life the proper consideration they deserve.

This isn't the best characteristic to have for a healthy collaboration, since the choices you make can also affect your teammates and the people with whom you maintain relationships.

So it's important to remember that only you have the power to make your own choices and take control over your life — and you make these choices all the time.

Where you work and who you work with, as well as how you spend your time, are all choices that you make every single day.

Making the right choices also goes hand-in-hand with accepting responsibility. So if you're not happy with your job, rather than blaming your boss or your coworkers, realize that you ended up where you are because of the decisions you made.

No one wants to work with someone who blames his boss for his workload and doesn't do anything to fix it, so this is not the ideal attitude if you hope to have a good relationship with your coworkers and become a great collaborator.

Instead, you need to have the third essential skill for a great collaborative relationship, which is _self-accountability_.

This is achieved by recognizing that you have choices to make in both work and relationships. If you're unhappy with the workload you're being assigned, don't just stew in negative emotions; instead, talk to your boss and be open about what steps can be taken so that you can be at your most productive.

### 6. Know how you fit in by being aware of yourself and your teammates. 

If you're looking for insight into team compatibility, there's no better place to start than the FIRO theory, which stands for _Fundamental Interpersonal Relations Orientation_.

This theory points to three traits — _inclusion, control_ and _openness_ — as the primary influences over how compatible we are with others.

These traits are determined by our desires and fears: we all want to feel appreciated and well liked by others, while at the same time we fear being humiliated, rejected and ignored.

Depending on how strong these feelings are, they can dramatically affect our relationships.

For example, if you have low self-esteem or carry a fear of being ignored, this can influence your desire for inclusion. And along with this desire to be included, you might also harbor a fear of including others, or a lack of openness.

This brings us to the fourth essential skill: _being self-aware_.

Being self-aware implies having an understanding of your own fears, desires and feelings, and how they differ from others. Being in touch with this information will make it easier for you to know how you can best fit in with the rest of your team.

So ask yourself, "How do my levels of inclusion, control and openness compare with people I'm working with?" As you come to find the answer, you'll have a better understanding of how compatible you are with those people.

Let's say you're a CEO with a strong preference for inclusion. This would make you highly compatible with other people who favor inclusion, but you may need to strike a different tone with those who prefer being alone.

In the end, being flexible is key to any successful collaboration and to forging productive relationships with your teammates.

### 7. The first steps to conflict resolution are to calm everyone down and understand the problem. 

No matter how flexible you are, there are bound to be conflicts when working together with others.

This brings us to the fifth and final essential skill in forming and maintaining collaborative relationships: _managing conflicts_.

One especially effective way of handling conflict is to use the _interest-based_ approach, which is a step-by-step method for taking into account everyone's interest in a given dispute.

The first of these steps is to create an environment that makes everyone feel included. By checking in with everyone regularly, you can find out which team members have a stronger need for inclusion than others, and can calm down anyone who's feeling on edge.

Once you understand where people stand, you can act accordingly. So if someone in particular has a strong desire to be included, you might decide to interact with that person face-to-face, instead of using e-mail.

The next step is to clearly define the problems and issues at hand.

What you want to avoid is spending days, weeks or even months negotiating only to find out that you didn't understand what was really being argued in the first place.

This can easily happen when complex issues are involved. Executives might think that employee attendance is down due to wage disputes, while human resources might think the issue is solely a result of low morale. The fact of the matter is that both of these reasons may contribute to the problem, so it's the role of the mediator to define the problem precisely.

But these are just the first two steps in the interest-based approach. Let's take a look at the next two steps in the following blink.

> _"If your relationship doesn't bump up against some conflict every once in a while, you're either in complete denial or overly medicated."_

### 8. Make sure you understand everyone’s interests, including your own, and prepare a contingency plan. 

In the third stage, after you have figured out the interests of all the parties involved in a conflict, it's time to take a look at your own interests.

Your interests differ from your fixed position. Whereas your fixed position is the outcome you desire from a situation, your interests are flexible and you should focus on them.

Let's say you're trying to sell a car for $8,000. You have another friend who needs a car, but the best offer they can offer is $7,000. Your position is that you really want to sell the car — and that is fixed. But your tools in making the deal happen are flexible; the sale price, for example, doesn't necessarily have to be $8,000. Perhaps you can negotiate for $7,500 if you throw in a bike rack and a spare tire?

It's also important to come up with a contingency plan in case a compromise can't be reached.

By having a solid contingency plan in place well in advance, it will serve as an important point of reference when trying to decide what's in your best interest.

For example, if you and your neighbor are trying to resolve a property dispute involving the removal of a tree, you can know ahead of time that the contingency plan will be to settle the matter in small claims court.

Having this in mind gives you a frame of reference that will help you properly consider the offers coming from your neighbor. Even if only getting half the money reimbursed for the removal of the tree isn't ideal, it's probably better than the potential time and money it will take if the matter goes to court.

But you'll only know this by having the contingency plan in place, to begin with.

### 9. To find a solution, break down the issues to find different options you can narrow down. 

Let's look at the final phase of the interest-based approach to conflict management.

The end result of this approach is to find creative solutions and compromises for your conflict, which you'll have a better chance of doing once you break your conflict down into smaller and more precise issues.

When a labor union goes on strike, negotiations between the union and management will often hit an impasse on the issue of wages. But when you break down the wage issue into smaller parts, you can see how it becomes easier to overcome.

If you start by looking at the hourly wage increase the union is pushing for, you could make a series of suggestions on how to get that money to the workers in different ways. Maybe the money could be paid out in the form of year-end bonuses? Or perhaps the money could be distributed in chunks when certain performance goals are met?

By the end of negotiations, and upon finding out what everyone's stake in the conflict is, you should have a fair amount of potential solutions on the table. Now is the time to narrow them down and decide which one works best — and one of the best ways to do this is to use a _straw design_.

This works by systematically reviewing and discussing each solution and then getting one party to draw up a preliminary "straw design" of an agreement. "Straw" refers to the nature of the agreement, since it's flexible and not concrete.

Once the draft has been prepared, it can then be distributed and everyone can provide feedback, which can then be considered for revisions.

In the case of the labor strike, the suggestions of year-end bonuses and performance-based rewards can be made into different drafts and distributed for another round of feedback. This process is then repeated until, little by little, they get closer to and eventually reach a final agreement that pleases everyone.

### 10. Final summary 

The key message in this book:

**In a globally interconnected world, it's more important than ever to have good collaboration skills. Therefore, it's in your best interests to identify the skills that are crucial to establishing and sustaining collaborative relationships. An especially important aspect of this is knowing how to resolve a conflict, which can be done by taking the pulse of every party involved and finding a solution that everyone can agree with — including you.**

Actionable advice:

**Invite all stakeholders to a brainstorm!**

The next time you're trying to come up with creative solutions for a conflict, try brainstorming with everyone involved. Brainstorming is an effective technique that gives you a greater number of ideas to choose from. So, ask all sides in a conflict to take a minute to write down a few ideas. This will let you avoid a situation where one party takes a more dominant position by offering more suggestions than the other.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Collaborative Intelligence_** **by Dawna Markova, Ph.D. and Angie McArthur**

_Collaborative Intelligence_ (2015) is a guide to developing your own personal form of intelligence by utilizing your unique ways of thinking. These blinks will teach you how to identify and build on your strengths as well as those of others, while adjusting your communication accordingly.
---

### James W. Tamm and Ronald J. Luyet

James W. Tamm is an expert in conflict resolution with decades of experience creating collaborative work environments. He is also a former law professor and California judge who now heads the consulting firm Business Consultants Network Inc.

Ronald J. Luyet is the cofounder of the Green Zone Culture Group, which helps companies build their own collaborative work environments. He is also a senior member of the Business Consultants Network and coauthor of the book _Where Freedom Begins: The Process of Personal Change_.

