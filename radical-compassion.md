---
id: 5e4d27f46cee0700079a1696
slug: radical-compassion-en
published_date: 2020-02-20T00:00:00.000+00:00
author: Tara Brach
title: Radical Compassion
subtitle: Learning to Love Yourself and Your World with the Practice of RAIN
main_color: None
text_color: None
---

# Radical Compassion

_Learning to Love Yourself and Your World with the Practice of RAIN_

**Tara Brach**

_Radical Compassion_ (2019) is a practical guide to letting go of painful emotions and embracing the world with more love, forgiveness, and compassion. Mindfulness expert Tara Brach presents RAIN, a simple four-step meditation system that helps practitioners cultivate inner strength to overcome life's many obstacles.

---
### 1. What’s in it for me? A tried-and-true method for overcoming adversity. 

The world can be a turbulent, troubling place. Our media is often a constant stream of bad news, and our personal lives can be similarly fraught with stress, anxiety, and uncertainty. In these blinks, you'll learn how to deal with these challenges by using RAIN, the philosophy and practice of mindfulness expert and clinically trained psychologist, Tara Brach. 

A combination of eastern spiritual practices and modern brain science, RAIN meditation practice is a path to peace in four steps: Recognize, Allow, Investigate, and Nurture.

In these blinks, you'll learn the skills and mindset necessary to hone your own RAIN meditation practice. Filled with practical tips and engaging stories from a lifetime of assisting others to live in the moment, Brach's guide will help you reap the rewards of a more mindful life. 

In these blinks, you'll learn 

  * what meditation can do for anxiety;

  * how to have tea with a demon; and

  * why remembering death isn't always grim.

### 2. The key message here is: You can create the mental space you need to gain that perspective, by using the RAIN meditation technique. 

With chores, meetings, bills, and traffic, daily life can sometimes seem like an endless parade of things to do and problems to solve. Getting through the day can feel like being lost in a dense forest: around each corner is just more undergrowth, with no exit in sight.

In these moments, when you feel like there is no way out, carve out a clearing. Create a bit of space to breathe. You'll find that when you do, you'll finally be able to see a path forward. It sounds easy enough, but how is it done?

**The key message here is: You can create the mental space you need to gain that perspective, by using the RAIN meditation technique.**

So what is the RAIN technique? Well, RAIN is an acronym for a four-step meditation practice. It's designed to give you distance from life's problems, and allow you to live in the moment. The first three steps were developed in the 1980s, by senior Buddhist teacher Michele McDonald. Since then, Tara Brach has honed and perfected the practice, adding a final step to awaken self-compassion.

When you're feeling stressed, anxious, or overwhelmed — what Brach calls "being in a trance" — practicing RAIN can help you let go of those negative emotions and be present in the moment.

The RAIN meditation practice begins with R, which stands for Recognize. That's because, here, you first have to recognize you're in a trance. Do this by noticing red flags such as compulsive behavior, ruminating on anxious thoughts, or rushing to get things done.

The next step is A for Allow. Here, don't try to change anything just yet. Simply take a breath, and allow yourself to experience what is happening without judgment.

Once you are centered, it's time for the third step: I, or Investigate. It asks you to direct your attention inward to uncover why you are feeling this way and what you may need to feel better. Are you feeling fear of failure? Are you feeling torn by multiple priorities? What type of reassurance would help you move on?

The final step is N, for Nurture. Close your meditation by sending a message of support inward to yourself. Gently relax your body as you think of a positive phrase like, "You will be alright," or "We can do this."

Following these four steps may only take a few minutes, but can yield powerful results. RAIN meditation helps you turn your attention away from the outside world and toward your inner self. In the next blink, we'll look more closely at steps one and two.

> "_Simply put, RAIN awakens mindfulness and compassion, applies them to the places where we are stuck, and untangles emotional suffering._ "

### 3. When you experience negative emotions, don’t lash out in anger. Respond with kindness. 

More than 2000 years ago, the Buddha crossed northern India teaching a message of compassion and freedom. He didn't journey alone, though. Mara, the god of hatred and greed, followed him the length of his travels.

But, interestingly, each time Mara appeared, the Buddha didn't fight him off. Instead, he would approach the demon and calmly say, "I see you Mara. Come, let's have tea." In doing so, the Buddha displayed the first two steps of RAIN: Recognizing and Allowing.

**The key message here is: When you experience negative emotions, don't lash out in anger. Respond with kindness.**

Mara appears in our own lives in the form of painful experiences like failure, fear, or jealousy. When these feelings develop, we've learned to confront them by saying "no." We try to fight them, we assign blame, or we withdraw.

RAIN, teaches us to do the opposite. Through it, we learn to accept these negative feelings without judgment, allowing us to consciously respond to them rather than merely reacting.

Take the story of Roger, a powerful executive at a technology company. The stress of his job caused him to be irritable, mean, and cruel to his loved ones. Brach sought to help him with the first two steps of RAIN meditation.

First, she asked Roger to close his eyes and focus on an incident that had set off his temper. She asked him to Recognize how he felt in the moment of anger. He felt tense, heated, ready to explode. Next, she asked him to Allow himself to feel these sensations — rather than reacting immediately, he had to sit with his rage and just let it be.

As he took a moment to experience the anger, Roger felt it slipping away. These two steps may seem small, but when you Recognize and Allow negative feelings, you open a space to respond with kindness instead of hostility.

Making this process a habit is important. Many research studies show that consciously practicing mindfulness is a learned skill, and one that can be improved over time. You may not always get it exactly right but, with consistent effort, your brain will adapt. Recognizing and Allowing will become a regular part of your thought patterns.

The same is true of the next two steps, Investigating and Nurturing.

### 4. We must break down our defensive barriers to find our inner love and compassion. 

In the 1950s, an order of monks had to move their massive, beloved statue of the Buddha. It got damaged in the process, and the outer surface of clay cracked and ruptured. What the monks saw astonished them: the cracks revealed that underneath, the Buddha statue was made of solid gold.

The monks believe the outer shell of clay was added hundreds of years earlier, to protect the treasured Buddha during a time of war. They also believe there is a lesson to be learned from this story.

Like this statue, we too develop hard outer shells to secure us in times of crisis. And, just as with the golden statue, these tough defense mechanisms can sometimes obscure our most valuable traits.

**The key message here is: We must break down our defensive barriers to find our inner love and compassion.**

The second two steps in RAIN meditation can help us with this process. These are Investigating and Nurturing. To show us how this works, Brach tells the story of Sophia, one of her student's daughters.

As a junior in college, Sophia experienced a very tough breakup. She was depressed and heartbroken, and so took a break from school. She was in such despair, she couldn't return. To help her overcome this struggle, Brach led her in RAIN meditation.

After first Recognizing and Accepting her feelings, Sophia needed to Investigate why she felt so sad and lost. She turned her focus inward, and realized that deep down, she felt like a small child, afraid of being abandoned. Her introspection uncovered the root of her profound sadness.

To reassure this part of herself, Sophia had to Nurture that inner child. She did this by placing her hand on her heart and telling herself comforting words. Phrases like "I am here for you," and "I care." As she spoke, Sophia healed the psychic wounds of the breakup and was eventually able to return to school.

Another way you can practice Nurturing, and discover your own hidden gold, is to call on your future self. Before starting on this journey, relax. Imagine yourself 20 years in the future. Share your current problems with this future you and receive their advice and care. You'll find they may give you the support you need.

Now that you are more familiar with RAIN meditation, it's time to consider how to apply it to some of life's most common problems.

> "_There's a calling within each of us to connect with the gold and live from the gold_."

### 5. In order to grow we must leave behind negative beliefs about ourselves. 

Lazy, unlovable, a failure. Harsh words like these are painful and dispiriting. Unfortunately, many people hear these judgments all the time — and not from some bully or outside antagonizer. They hear this criticism from themselves.

**The key message here is: In order to grow we must leave behind negative beliefs about ourselves.**

We all have some destructive thoughts about ourselves. Worse still, they can be very hard to ignore or leave behind. This is because our brains are much better at remembering painful events, like the times we've failed at something or when we've been hurt. Millions of years ago, this bias was a useful survival instinct. Now, it keeps us reliving the worst experiences of our lives while overlooking the good ones. 

When we constantly ruminate on our perceived flaws, they become part of our identity. The negative stories we tell ourselves about being worthless or unworthy, ultimately, hold us back from achieving happiness and connecting with others.

Practicing RAIN can help you shed these mental blocks. 

When you're thinking about a past mistake or telling yourself, "There is something wrong with me," remember the first step in RAIN. That is, to recognize that you're engaging in negative thinking. Next, allow yourself to feel the negative emotions associated with that thought. Pay attention to the sensations it brings to your body. ****

Then, investigate the belief. Ask yourself, "Is this really true?" You'll notice that, often, your worst thoughts don't map onto reality. Take the time to question the truth of that negative self-belief. You're likely to find that they're exaggerations, distortions, or even complete fabrications. 

Once you grasp this truth, ask the question: "What will happen if I let this belief go?" Letting go of a long-held notion about yourself, even a negative one, can be scary. It can feel like shedding your skin, and leave you feeling exposed or vulnerable. But, what it also does is create space for growth.

The poet Mark Nepo calls this step of opening up yourself to growth "the exquisite risk." It's a risk, because we step away from the familiar, but it is exquisite because we allow inner beauty, sensitivity, and compassion to be revealed.

> "_Our negative self-beliefs can keep us constricted, small-minded, disconnected from our heart, and suffering._ "

### 6. It’s really important that we nurture our inner goodness, so that we can overcome shame. 

Here's a familiar story: a wealthy father has two sons. The younger one asks for his inheritance early. He leaves home and proceeds to spend all of it on an extravagant lifestyle. A few years later, broke and penniless, the son must return home with nothing but his shame.

The father doesn't turn him away, though. Instead, he rejoices. He welcomes him back with forgiveness and open arms.

At times, we've all been this prodigal son; a person ashamed of our thoughts and actions. But RAIN meditation teaches us that we must also be the father — we have to learn to meet our own shame with love and kindness.

**The key message here is that it's really important that we nurture our inner goodness, so that we can overcome shame.**

What is shame? Well, it's a feeling that develops when we believe our faults are so great that others will no longer accept us into their community. This deep-seated fear of being banished can be so distressing and destructive, that it can lead to self-hate.

Cultivating radical compassion for ourselves is a strong antidote to the power of shame, and we can do this through Nurturing — the final step in the RAIN meditative practice. 

Brach demonstrates this through the example of another student, Sean. Sean lost his job during the 2008 financial crisis. After sixteen unsuccessful months of looking for work, he was still unemployed. Without a job, Sean felt like he was failing his family. He felt like a burden. He felt shame.

To overcome this painful feeling, Brach had Sean practice Nurturing. She asked him to think about the support group he attended for other men in his situation. He concentrated on the respect they shared for each other, and the community they'd formed. By focusing on those positive feelings, Sean was able to nurture his inner self-worth and find the strength to forgive himself.

Sometimes, in order to nurture your inner goodness, you might find you have to look for compassion outside yourself. Talk to loved ones, commune with nature, or visualize the presence of a revered spiritual leader like the Dalai Lama or Kwan Yin, the bodhisattva of compassion. Feel the warmth of their love.

Wherever you find compassion, it's important to be open to its healing potential. When practicing RAIN, always take a few moments to feel the love of others and remind yourself that no flaw is too mighty to overcome.

### 7. To gain control over feelings like fear and anxiety, we must confront them directly. 

A child once suffered from chronic nightmares. Every night, he dreamed he was being chased through the woods by a giant unseen monster. One night, however, having had enough of these nightmares, he resolved to face his fear. In the dream, he stopped and turned to look at the monster. As soon as he did, the creature disappeared and the nightmares ceased!

This story offers an important insight into how negative emotions work. When we try to avoid feeling them, they often just become more powerful still.

**The key message here is: To gain control over feelings like fear and anxiety, we must confront them directly.**

We can all agree that experiencing real fear is unpleasant. Our hearts pound, our muscles clench, and our stomachs tie up in knots. In fact, these sensations are so uncomfortable that many of us become afraid of the very idea of being afraid! 

Fear builds fear in a vicious cycle. RAIN can help break this awful pattern. It teaches us how to process anxieties in a more healthy way. The story of Brianna — one of Brach's students — shows us how. 

Brianna was a competent worker, but she found attending meetings with her company's new CEO had become a major source of anxiety. Why was that? Well, she was intimidated by him because he was gruff, rude, and critical. When Brianna tried to ignore these feelings, she became distracted and couldn't think clearly. 

After learning RAIN, she applied the practice of Allowing herself to feel her fear. Before each meeting, she'd take a few deep breaths, feel her muscles tighten, but tell herself, "It's okay, this feeling belongs here." Soon, she no longer experienced her fear as an unstoppable force, but instead saw it as a passing sensation.

This technique works well for, say, mild anxieties. But what about larger, more overwhelming fears? In these instances, where the emotion is too much to bear alone, seek outside forces to help nurture your strength. 

Find a quiet, comfortable place. Sit and close your eyes. Recognize your feelings of fear and allow them to exist. Then, imagine a benevolent entity that you trust. This may be something like a divine presence, Mother Nature, or Jesus. Picture yourself holding your fears and handing them over to your spiritual companion. You may even act this out physically. 

As you hand off your fears, you will feel as if a weight is lifted. You won't be carrying them alone.

> "_Through the practice of RAIN, we can discover this heartspace — a loving presence larger than our small, frightened self — that includes anxiety or fear without becoming possessed or consumed by it_."

### 8. Investigating the roots of desire can help us nurture inner wholeness. 

Max, a wealthy and powerful CEO, came to Brach with a problem. He called it FOMO, or Fear of Missing Out. Rather than enjoying the successful life he'd built, he was always preoccupied with the next thing: a vacation, a new tech gadget, or an upcoming project. He felt he couldn't be happy.

To Brach, FOMO sounded like a modern term for a very old affliction; having too much desire. To help Max with his endless pursuit of more, she offered him a classic koan from the Zen poet Ryokan: "If you want to find the meaning, stop chasing after so many things." 

**The key message here is: Investigating the roots of desire can help us nurture inner wholeness.**

Everyone has desires; it's part of being human. We all want to feel connected, to feel alive, and to feel that our existence has meaning. We easily lose sight of these spiritual desires, though, by becoming fixated on more tangible substitutes. We go after money, objects, or ways of controlling others. These pursuits are ultimately unsatisfying.

By practicing RAIN, focusing on step three — Investigating — we can uncover the root causes of our immediate desires. When we do, we can then nurture those needs in a more wholesome manner.

Fran, another of Brach's students, engaged with this very process. Fran suffered from an eating disorder and often binged on junk food when she experienced stress. This was especially bad when she felt judged by her parents. Fran was advised to investigate her feelings. When she did, she found she used eating to soothe her inner child when feeling hurt.

After this discovery, she decided to instead nurture that inner child with messages of love and support, rather than with ice cream and cake.

Not all desires are bad, but sometimes it can be hard to uncover what you truly want. A special meditation practice can help with this journey.

Let's start by relaxing and calming your body until you feel a sense of openness. Now, turn your focus inward. Ask yourself gentle questions like, "What does my heart really long for?" or "What matters most in life?" As you do this, be sensitive to the words and images that your mind conjures. How do they make you feel? Notice which ones call out to you and bring a feeling of warmth or peace. These are your true desires.

RAIN really is a powerful tool for understanding yourself! What it also does, though, is help in your relationships with others, something that the next blinks will explore.

### 9. Holding onto anger and resentment can prevent your wounds from healing. 

Imagine yourself at the end of your life. When you look back at the people and relationships who defined your decades in this world, what do you see? Were your interactions based on love and openness, or anger and judgment?

These were the questions that faced Charlotte, one of Brach's older students. After facing a period of poor health, she realized that too many of her relationships were still defined by past arguments and animosity. Moved by this perspective, she sought to open her heart to forgiveness.

**The key message here is: Holding onto anger and resentment can prevent your wounds from healing.**

Anger and hurt are an unavoidable aspect of human relations. These feelings are normal, especially when they're brief and passing sensations. But, if we let them linger, that's when they become a problem.

Keeping anger close will only harden your heart and stifle your relationships. Staying angry with someone prevents you from seeing them as complex, full persons. You'll only see the negative aspects of their personality. This is called making an Unreal Other.

Brach's student, Stefan, faced this problem with his parent. For most of his life, Stefan's father would look disapprovingly at his son's sensitive personality and artistic interests. Now, as an adult, Stefan couldn't forgive him. Even when his father was on his deathbed, Stefan still saw him as the stony and uninterested man he once was.

Fortunately, Stefan's sister intervened. She encouraged him to look beyond this two-dimensional version of their father. Yes, he was sometimes cruel, but he had a loving side as well. With this more fully rounded image of his father in mind, Stefan was able to reconnect with him before it was too late.

Has anger created an Unreal Other of someone in your life? Start unlearning that anger with this brief exercise. Think back to the incident that made you mad. Now, concentrate on their face and ask yourself: How were they feeling? Were they hurt? Were they scared? Was this the best version of themselves?

Practices like this will remind you of the goodness in others — and remembering their essential goodness will help you forgive. Forgiveness isn't a one-time event, but a process that unfolds over time. You may not feel better right away, but making this type of mindfulness a habit will keep your heart open to others.

> "_What could be more beautiful than dedicating ourselves to putting out blankets of forgiveness, letting those we've pushed away feel us welcoming them home_?"

### 10. The best way to help those you love is to focus on their basic goodness. 

The Babemba people of South Africa have a curious custom for when an individual does something cruel or selfish. After someone acts out, the community places them in the center of the village. Then, one by one, each member of the community speaks to them.

Do they berate them for their mistakes or call out their faults? No. In this exercise, every person in the village lists all the times the accused has been kind, loving, or just. Rather than looking for the bad, they choose to remember the good. This is how the community heals.

**The key message here is: The best way to help those you love is to focus on their basic goodness.**

Basic goodness is an important concept. It doesn't refer to the surface-level attributes that are considered _good_ by specific societies. Things like having a good job or excellent manners are nice qualities, but basic goodness is deeper. 

Basic goodness refers to the more immersive universal qualities that make someone special. Qualities like love, creativity, kindness, and awareness. To build strong relationships and resilient communities, it's important to recognize and nurture these qualities. This is called mirroring.

When Brach thinks of mirroring, she remembers the story of Jono, a young man who was a bit of a late bloomer. By his mid-twenties, Jono still hadn't moved out of his home or found a career. His parents were worried he would never reach his potential.

Brach advised the parents not to pressure Jono, but to instead remember what they loved about him. So, his sensitivity and his artistic flare. When Jono exhibited these qualities, his parents followed Brach's instruction and practiced mirroring. They reflected back Jono's excitement and passion, and within a few months, bolstered by their support, he began making films for a local non-profit.

To help with your own mirroring practice, try a special type of RAIN meditation called Seeing the Secret Beauty. To do this, sit in a calm, comfortable place. Reflect on someone you care about. Recognise the positive qualities they have, and take special care to Allow yourself to feel the warmth they awake in you. Nurture this feeling by imagining your love for them opening up in all directions.

This strategy works no matter who you have in mind. It could be a loved one you are struggling with or a stranger who caused you pain or discomfort. You can even practice it on yourself. Try to see your own basic goodness from the outside looking in.

In the next blink, we'll expand this practice even further, and show how extending radical compassion to others can help heal some of the biggest rifts in our society.

### 11. RAIN can help break down the barriers that separate us from people who we perceive as different. 

A man takes a flight from Lagos to South Africa. As he boards, he notices that the two pilots are black. At first, he thinks nothing of it, but when the plane hits rough turbulence, he gets scared. He wonders, "Do these pilots have the necessary skills to land safely?"

A quick conclusion we could draw from this story is that this man is racist. But, the person described here, was, in fact, Archbishop Desmond Tutu, who won the Nobel Peace Prize for his anti-apartheid activism. It was clear he had unconsciously absorbed some of our society's worst stereotypes, and was practicing Unreal Othering.

**The key message here is: RAIN can help break down the barriers that separate us from people who we perceive as different.**

Remember, Unreal Othering is when we reduce people to a false, one-dimensional image, and stop seeing them as fully realized, complex individuals. When this is applied to minority groups or other oppressed peoples, it comes through as racism, classism, or gender discrimination.

One way Unreal Othering is perpetuated is through implicit bias. This is the term scientists use to describe the unconscious thought patterns we may hold against people unlike us. Luckily, by practicing RAIN, we can Recognise those patterns and begin to eliminate them by Nurturing a more holistic view of the other.

One moving example of this process in action is the 2016 documentary _Look Beyond Borders_. In this film, wealthy Europeans were paired with refugees from Syria and other parts of the Middle East, a group in society who face serious discrimination. 

In the documentary, the pairs sit facing each other. They gaze into each other's eyes and take turns asking questions and sharing personal stories. Soon, these strangers begin to leave behind their biases and see each other as individuals who share much in common. Many of the encounters end with tearful hugs.

Not all encounters with others can be so tightly managed. Still, you can nurture your radical compassion with your own RAIN practice. When you are relaxed, reflect on someone who may be outside your immediate community.

Allow yourself to experience the feeling they arouse in you. Then, Investigate why you feel this way. What do you know about this person? What experiences and struggles might they have to deal with? What makes them vulnerable? With this in mind, Nurture your own acceptance of these differences with love and openness.

The stereotypes and biases that divide our society are deeply rooted. Fortunately, they aren't permanent. With careful attention to how we approach others, it's possible to unlearn some unconscious bigotry that may limit our growth.

> "_Once you've enlarged your self-awareness, it's time to make the effort to connect with people from different races to understand their realities and the suffering they are living with_."

### 12. Living with an open heart requires being truly present in your daily life. 

One of Brach's fondest memories comes from a meditation retreat she attended with Zen master Thich Nhat Hanh. At the end of the retreat, Hanh instructed each pupil to embrace a partner. As the pairs held one another, the master offered this reminder: both of you will die.

This wasn't a morbid gesture. It was a loving reminder that life is fleeting and each and every moment is precious.

**The key message here is: Living with an open heart requires being truly present in your daily life.**

Being truly present isn't always easy. More often than we realize, we are caught up in the rush of living. We stop paying attention to ourselves, our loved ones, and our surroundings. We fall into the trance.

But we can avoid this fate by following a few Remembrance practices. Each is designed to pull you out of the trance and put you back into the moment.

The first is called Pause for Presence. Use this technique when you're feeling particularly rushed. When you feel your stress rising, stop what you're doing. Pause. Take three to five deep breaths, and with each breath, feel your body relax.

The second technique is called Say Yes to What's Here. This Remembrance is all about accepting the world as it is. When you're feeling pain, anxiety, or discomfort, don't push it away. Instead, turn your attention inward and whisper, "yes." This will help you accept the moment in both its highs and its lows.

A third technique is Turn Toward Love. Practice this Remembrance when you're feeling low in spirit. In these instances, remind yourself of the goodness in the world by meditating on the kindness of others, by sending yourself a supportive message, or by imagining the embrace of a spiritual figure.

The final Remembrance is Rest in Awareness. This is best practiced in the moments you feel at peace. In these times, pay attention to what your body is sensing: Feel the earth below you; hear the wind or the rain outside; notice the beauty of what you see. Moments of peace are precious, and we should take care to be fully aware of them.

When you make a habit of these Remembrance practices, you'll be more tuned into the world around you. Regularly combining this nurturing of presence with the RAIN meditation techniques, will help you meet the challenges in your life. As you feel love and extend your radical compassion to the world, every day will feel more like a gift.

> "_Finally, remember that even when our lives seem most lonely, we're never on this path alone_."

### 13. Final summary 

The key message in these blinks is:

**Our daily lives often pull our attention in many directions. We are blinded by our stress, anxieties, and responsibilities. However, through practicing the four steps of RAIN meditation — Recognize, Allow, Investigate, and Nurture — it's possible to see beyond these negative feelings and open our hearts and minds to the beauty that surrounds us.**

Here's some more actionable advice:

**Identify your Resource Anchors.**

When practicing RAIN, it's helpful to remember the persons, places, and things that make you feel empowered. These are called resource anchors. When beginning your RAIN journey, make a mental list of the resource anchors in your life, so that they're fresh in your mind to call upon in times of need.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Radical Acceptance_** **(2004) by Tara Brach**

You just learned how RAIN meditation can help you cultivate a more present, aware, and loving mindset. Continue your path toward enlightenment with _Radical Acceptance_ (2004).

This book-in-blinks provides even more Buddhist-inspired insights from mindfulness expert Tara Brach. Learn how to overcome career, personal, and interpersonal challenges through Brach's intimate stories and guided meditations.
---

### Tara Brach

Tara Brach is an internationally-recognized therapist, teacher, and mindfulness expert. She holds a PhD in clinical psychology from the Fielding Institute and founded the Insight Meditation Community of Washington, DC. Her bestselling books include _Radical Acceptance_ (2003) and True Refuge: Finding Peace & Freedom in Your Own Awakened Heart (2013).

