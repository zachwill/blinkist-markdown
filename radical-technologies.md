---
id: 5d34a6e26cee07000859bcd5
slug: radical-technologies-en
published_date: 2019-07-22T00:00:00.000+00:00
author: Adam Greenfield
title: Radical Technologies
subtitle: The Design of Everyday Life
main_color: None
text_color: None
---

# Radical Technologies

_The Design of Everyday Life_

**Adam Greenfield**

_Radical Technologies_ (2017) brings us up to date on the most transformative and influential technologies currently being developed, including 3D printing and the most powerful algorithms. Author Adam Greenfield explores the potential repercussions of these technologies and provides insight on both the positive impacts these technologies may bring, as well as the dangerous pitfalls that deserve our attention.

---
### 1. What’s in it for me? Learn more about today’s most influential technologies. 

It may be hard to believe that the first iPhone appeared in 2007. That wasn't very long ago, yet it's difficult to imagine life without having access to dozens of apps in your pocket. Indeed, so many technological leaps and bounds have happened in such a relatively small amount of time that one has to wonder what the next 20 years have in store for us.

Already, we have many emerging technologies that promise to radically change the way we live. These include 3D printers, self-driving cars and other forms of automation that could either make life a whole lot easier or a whole lot more complicated, depending on how things go.

Author Adam Greenfield surveys today's top game-changing technologies to see what kind of potential they have for making things both better and worse. As is often the case, the intention for a radical technology will often be good, but there's also the potential for things to get quickly out of control if we don't pay close attention.

In these blinks, you'll find out

  * why smart cities may not be so smart after all;

  * how a smartphone game led someone to a dead body; and

  * how 3D printing could revolutionize consumerism.

### 2. Smartphones are powerful tools, but they also reveal the dangers of becoming reliant on technology. 

If there is one object that symbolizes the technological revolution taking place in society today, it is undoubtedly the smartphone. There are many reasons why the smartphone has become so ubiquitous, but one of the most significant is how it has radically changed the way we navigate our world.

For millennia, people have relied on two-dimensional maps to assist them when traveling through foreign landscapes. These maps could be lifesavers, but they were far from perfect. Indeed, the effectiveness of a map depended a great deal on an individual's ability to recognize how the abstract two-dimensional lines and symbols represented their three-dimensional surroundings. Just finding your current location could be a considerable challenge.

All that changed once smartphones came on the scene. 

Soon, every owner had a map that not only tracked their current position and movements in real time, but also offered the ability to zoom in and out while providing high resolution aerial and street-level images. Now, no matter where in the world you may have found yourself, with a few taps of your finger you could instantly pinpoint your exact location. Not only that — as you moved, your digital map could continuously update the suggested route to your destination.

As a result of this technological development, we've become increasingly dependant upon smartphones to orient ourselves, especially in complex urban environments. But ultimately, this technology is itself dependant upon a vast technological infrastructure that is out of our control.

After years of using these smartphone maps, we've naturally grown less and less familiar with how to read conventional two-dimensional maps. This can become a serious problem when battery power is depleted, signals are lost, and the internet is nowhere to be found.

Therefore, the remarkable sense of orientation gained by having a high-powered map in your pocket is only as good as the intricate and fallible system of satellites and servers that allow the map to function. If one piece of this system fails, so too can your ability to navigate.

This raises an important question: While such radical technologies may be powerful and convenient, how much of our human agency should we be giving up by switching over to them? We'll continue to examine this question in the blinks ahead.

### 3. Making important decisions based purely on data can have disastrous consequences. 

Of course, maps and phones aren't the only things being made "smarter" by technology. In fact, a lot of people are speculating about the possibility of _smart cities_. This would involve putting everything from traffic lights to security systems in the hands of data-driven algorithms.

While some believe that such technological innovation could make urban infrastructure more efficient, we need to be cautious, as our flawed history with data-driven solutions demonstrates.

One of the more notable examples of this took place around 1973 when the RAND Corporation began exploring ways of using data analysis to improve the infrastructure in New York City. RAND's approach was partly inspired by Robert McNamara, the former US secretary of defense, who relied heavily on statistical analysis when forming strategies and making decisions while in office. 

For instance, RAND looked at the statistical record for New York City fires to find the best way to redistribute fire stations throughout the city. Unfortunately, their efforts proved faulty since RAND based their system on the amount of time it should take to reach a location instead of considering the time it would actually take to start fighting the fire.

These kinds of mistakes can have deadly consequences. And so can issues of biased analysis.

RAND's reliance on data ultimately caused them to close down fire stations in highly vulnerable neighborhoods and open new ones in sparsely populated, low-risk neighborhoods. This led to concerns that the research may have been biased, since it was readily apparent that the neighborhoods with new firehouses tended to be where the wealthy white people lived. 

Meanwhile, some of the poorest neighborhoods in the South Bronx, Manhattan and Brooklyn suffered fires that could have been less devastating had their firehouses not been shut down.

So, while there's potential for algorithms to be put to good use in our communities, the fact is, this technology can be harmful if it isn't using exactly the right kind of unbiased and relevant data. Therefore, we must proceed cautiously before handing over control of our cities to this sort of artificial intelligence.

> _"The claim of perfect competence that is implicit in most smart-city rhetoric is incommensurate with everything we know about the way technical systems work."_

### 4. Augmented reality has already proven to be quite popular, but also problematic. 

On a summer's day, in 2016, Shayla Wiggins left her house in Riverton, Wyoming, and proceeded to walk through town. She eventually ended up strolling along the river bank, where she encountered a most disturbing sight: the corpse of a dead man.

Stumbling upon a dead body is newsworthy enough, but what made this story particularly interesting is that, when it happened, Wiggins was in the middle of playing the augmented reality game, _Pokémon GO_.

Augmented reality is another radical technology that has lately only increased in popularity, thanks in part to video games like _Pokémon GO_. Essentially, augmented reality works by placing you in a virtual environment that reproduces your real-world surroundings while also adding new, digitally created elements.

In the case of _Pokémon GO_, these additional elements take the form of fantastical creatures, which users are tasked with finding and collecting. So as players walk around, looking at their surroundings through the interface of the game, their reality is periodically augmented by these animated characters. 

But augmented reality isn't limited to video games — it can also be used to provide valuable information about what people see. For example, if you're visiting a new city, an augmented reality interface could recognize the buildings around you and provide information on the style of architecture, when they were built and who designed them.

Now, despite these fun and informative uses for augmented reality, there have been issues with this technology as well. For starters, there are privacy issues surrounding the data collection that goes on in these programs as people open up a window to their environment. 

But what's also been questioned is the way augmented reality can turn culturally sensitive or even potentially dangerous areas into virtual playgrounds.

In particular, _Pokémon GO_ has become somewhat notorious for having its collectible creatures pop up in places like the World Trade Center's National September 11 Memorial and Museum. Players have also gone chasing after virtual characters in the demilitarized zone between North and South Korea.

There are clear benefits to the entertainment and information that augmented reality can provide, but this is another radical technology that needs monitoring and regulation to limit the potential problems it raises.

### 5. Digital fabrication could revolutionize society and the entire way we do business. 

Have you ever dreamed of owning a box that could produce whatever food your heart desired? Want a peanut butter and jelly sandwich? Just press a few buttons and voilà!

This may sound like something out of Star Trek, but since the advent of 3D printing technology, we've gotten a step closer. Currently, the Replicator MakerBot 2 is capable of printing nearly any object that is smaller than 11 inches, as long as you don't mind that object being made out of a greasy artificial polymer.

Despite the fact that we're still in the early days of 3D printing, it nevertheless holds the promise of one day being able to produce anything we wish for, from food and medicine to clothes and building materials. As such, digital fabrication has the power to revolutionize our economy.

In fact, there's a good chance that this particular radical technology could precipitate the end of the capitalist era. 

After all, capitalism puts wealth and power into the hands of those with the means of production, and 3D printers have the potential to give more and more people the means with which to produce all manner of intricate and valuable objects. So, as the technology becomes cheaper and more accessible, capitalism as we know it could be upended.

For most of our history, the value and price we have put on material goods has been dependent upon how scarce the material is or how much craftsmanship and labor went into its creation. For instance, something made out of plastic in a factory has always been far cheaper than something handmade out of platinum. 

But since all manner of materials and intricate designs could be made possible through digital fabrication, our old economic models based on scarcity and craftsmanship may cease to apply.

And let's not forget the revolutionary impact it could have on our entire system of production, shipping, storage and retail. 

Basically, 3D printing could make all of it obsolete. 

Global supply chains, including all the ships, trucks and warehouses, would be a thing of the past as all products could be printed to order on site. The ramifications of this would be immense since it would free up vast amounts of real estate that could be put to better use as community facilities or, better yet, reclaimed by nature.

> _"Put simply, an established practice of distributed fabrication is freedom from want."_

### 6. Bitcoin has addressed concerns around the power of banks and fragile economies. 

There's a good chance you've heard of digital currency such as bitcoin, but even those familiar with it might be hard pressed to explain how it works. To really understand how this revolutionary form of money came to be, however, we should first explain how currency in general works.

Prior to bitcoins, we were already using a form of digital currency whenever we used online banking to move money around or pay with something by using a credit card. In such cases, no paper money or coins are being exchanged — it's really just a bunch of ones and zeroes being moved around online.

But what sets bitcoin apart is that these more traditional online systems still rely on an intermediary — the bank — to ensure that the transaction is valid and that it is done accurately and securely. 

In a way, this is a vulnerability since it requires that a lot of trust be placed into the banking industry — in protecting both your money and the personal information of you and whomever you're doing business with.

Security isn't the only issue, either. A bank can also decide to prevent a transfer from being made, as the Bank of America did in 2010 when it blocked all money being transferred to the WikiLeaks platform. 

Then there's the issue of instability around the value of your money. In the case of most traditional currencies, this means that your money is often closely tied to the economic health of the country or institution that issued the currency. 

This fact is often held up by anarchists and libertarians as yet another way governments can encroach on our personal freedoms. 

But it's also a fact that many people have had to deal with when an economic crash has greatly reduced their life savings. In Argentina, for instance, when a steep economic collapse took place in 2001 and 2002, the value of their currency, the peso, plummeted by 70 percent. 

So, obviously, there are some good reasons for wanting a currency that would remain unaffected by the whims of a bank or the economy of a country. In the next blink, we'll take a closer look at how Bitcoin has attempted to pull off this feat.

### 7. Bitcoin transactions are verified in a rigorous and thorough process. 

Proponents of bitcoin describe it as a best-of-all-worlds currency, since it's free of governmental regulations while also creating a shared financial record that is available for every user to see. In other words, it strives to be both transparent and highly secure. 

But how exactly does it achieve this? 

Well, for starters, the bitcoin system records every transaction by giving each "coin" a uniquely identifiable mathematical code. This code is essentially a cryptographic signature that verifies its identity and authenticity. 

When the transaction is made, the coin's signature becomes linked to the unique digital signature of the user who now holds the coin. Since the system keeps track of each transaction and is open to all users, anyone at any time can follow the entire history of a coin's ownership from the first to last transaction.

To make sure that every transaction is valid, each one must receive the cryptographic signatures of the parties involved. When this happens, something called a _hash value_ is produced, which essentially acts as a certificate for the transaction.

Now the transaction is ready to be finalized, and for this to happen, the hash value gets sent through the entire network of users, or _nodes_, as they're called in the system. 

The transaction gets checked against the historical information stored in each node to make sure there is no conflict with any previous transaction. Since the current transaction contains the history of the coins involved, as well as the history of every party involved, the smallest discrepancy or attempt at forgery would result in a false hash value and the transaction not going through.

Once the transaction is found to agree with every node, the system then attempts to link the new transaction to the last transaction that was made — thus making it the most recent transaction in a chain of transactions. 

This link between two transactions is called a _block_, and the information in this block is then checked against the information in every node for final verification. If everything matches and is considered valid, the transaction then becomes a permanent part of the blockchain, which is also the name of the system that controls the bitcoin currency.

As you can see, the blockchain program is constantly and thoroughly verifying itself and making sure that nothing has been tampered with. So, to put it mildly, it's difficult to scam the bitcoin system.

### 8. Automation technology is on the rise and could lead to unemployment for many. 

If you had to guess what the most commonly held job for the past few decades was, what would it be? Well, according to statistics taken throughout the United States between 1978 and 2014, the most common job was that of a truck driver.

This may not be too surprising, but what makes this statistic interesting is the fact that truck driving is one of the many professions increasingly at risk of being taken over by automation.

The term automation is used to describe any programmable process, be it through software or robotics, that can replace manual labor. And these days, truck drivers, farmers, factory workers and plenty of white-collar jobs are all under threat of being completely replaced by rapidly developing automation technology.

Both Google and Uber have been making headlines with their work on developing driverless cars, but the truth is that transportation trucks are more likely to be the first vehicles to go driverless in a widespread fashion. This is due to the fact that such vehicles often travel along highways on straight routes with very few stops involved. 

This makes a typical truck driver's route ideal for automation.

While truck drivers are far from being the only workers who may soon be looking for a new job, there is some debate over whether or not automation will lead to fewer jobs in the long run.

When the experts at the Pew Research Center got together in the summer of 2014 to discuss the possible impact of automation on employment, they were pretty much evenly divided on the subject, with half of them believing that automation would create enough jobs to make up for the ones that became redundant.

If we look at recent history, however, we find that new technologies haven't always led to new jobs. 

In a frequently cited 2013 study from the University of Oxford, researchers Carl Frey and Michael Osborne looked at 702 existing job categories from around the world, discovering that 47 percent of them were vulnerable to being automated in the decades ahead. Meanwhile, they also found that only 0.5 percent of the US population found job opportunities in the new tech industries that have emerged since 2000.

At the very least, this should make us pause to think about the consequences that automation may have on unemployment rates worldwide.

### 9. Algorithms are not as complicated as we think, though their impact is tremendous. 

When it comes to revolutionary new technologies, a lot of people are talking about algorithms. As a result, some of us who aren't as tech savvy are left scratching our heads and wondering what the heck an algorithm is.

Granted, the name does sound rather complicated but, in essence, an algorithm isn't much more complicated than a cooking recipe — it's basically a set of instructions on how to transform one thing into another. Only, in this case, those instructions are highly specific and structured in a very precise and sequential way.

And though you may not realize it, there's a good chance many of your everyday interactions involve algorithms. If you use an online service for shopping, streaming music or watching TV, the recommendations you see are the result of an algorithm. The same goes for the people you get matched up with on dating apps — even restaurants use algorithms for seating people more efficiently.

It's also worth noting that algorithms aren't limited to software. Any variable list of procedures, where one action leads to another, is technically an algorithm. For example, the emergency room staff that has to assess a wide variety of incoming patients is likely doing so with the help of a laminated checklist. And even though this is performed manually, this is also a type of algorithm.

Algorithms are one of the most radical technologies used today since their impact on our lives is truly enormous. Some algorithms are running networks and systems that are so powerful that even tiny changes can result in huge consequences.

Consider Google's search engine, for instance. This service is so popular that businesses can thrive or suffer based on the results. So imagine if, one day, a change was made, and your business suddenly went from being the first result to the fiftieth. Your steady supply of new customers would be cut off overnight, while some other business became swamped with attention. 

That's a level of power that we'll need to keep an eye on.

What's certain is that all of these radical technologies are far from being passing fads. They're here to stay. And as they become more developed and refined, they'll have an ever-increasing impact on our everyday lives. 

This means that there will be more questions about how we can best use these technologies in an ethically sound way. If we succeed in addressing those issues, they may help us flourish in ways we've only ever dreamed about.

### 10. Final summary 

The key message in these blinks:

**Radical technologies such as bitcoin, digital fabrication and automation all hold the potential to profoundly transform the world we live in. However, their revolutionary power comes with substantial risks regarding how they're developed and used. If left unchecked or unregulated, these technologies could lead to a number of problems, including devastating levels of unemployment or life-threatening misinformation. On the other hand, if used ethically and purposefully, these tools could help us thrive in an unprecedented way.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Soonish_** **, by Kelly Weinersmith and Zach Weinersmith**

Fewer subjects are more fascinating than pondering the technological advances that await us in the not-so-distant future. So much has happened in the past couple of decades, so the next few are sure to be full of surprises! 

This is what makes _Soonish_ the perfect set of blinks to read next. You'll find fascinating insights into some of the most cutting-edge technologies around, as well as what lies in store for space exploration. In many cases, what was once the domain of sci-fi is starting to look a lot more like reality, and it raises a lot of tricky questions that we'd be better off asking now rather than later.
---

### Adam Greenfield

Adam Greenfield is an American author and urban studies specialist who is currently based in London. Training in the design of systems and networks, he worked for Nokia before founding his own urban systems design firm called Urbanscale in New York City. He also teaches at London's Bartlett School of Architecture.

