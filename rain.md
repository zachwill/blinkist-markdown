---
id: 56a6ad35fb7e87000700001a
slug: rain-en
published_date: 2016-01-27T00:00:00.000+00:00
author: Cynthia Barnett
title: Rain
subtitle: A Natural and Cultural History
main_color: 8DB2D3
text_color: 5A7287
---

# Rain

_A Natural and Cultural History_

**Cynthia Barnett**

_Rain_ (2015) tells the story of one of the most valued, destructive and inspiring natural forces on our planet. These blinks trace a journey from rain worship in ancient cultures to the use of weather forecasting throughout the ages — and even the scientific explanation behind raining frogs.

---
### 1. What’s in it for me? Learn about the fascinating world of rain. 

"Into each life some rain must fall/But too much, too much is fallin' in mine," sings Ella Fitzgerald, and touches figuratively on humanity's very conflicted relationship with rain. 

We both need it — it's the source of all the water on the planet — but at the same time we've often tried to avoid it. Give us rain, but not too much, since then our mood will suffer, or, even worse, the rivers will overflow, the land flood and the crops die.

In these blinks the nature and culture of rain are explored, as well as our complicated relationship to it — something that drives us to worship, condemn, forecast and even seek to control rain. 

You'll also learn

  * how rain caused the Great Famine and witch hunting;

  * how swindlers used to get rich on good rain; and

  * why rain can be colored, and even include falling frogs or fish!

### 2. Rain can give life and destroy it too. 

Where would we be without water? Well, we wouldn't be anywhere at all! Life on earth would not have developed if it weren't for water. Today, water sustains our living world. It's no wonder then that humanity has always worshipped rainfall. 

Native Americans created dances to summon the rain. The Prophet Muhammad performed rain prayers, casting his arms skyward and turning his cloak inside out. In Judaism, rain is prayed for every year on the eighth day of the harvest festival, Sukkot. In 2011, following a three-month drought, a Christian governor of Texas declared the three days from April 22 through 24 as official prayer days for rain.

A lack of rainfall has, of course, been a serious threat for communities throughout history and around the world. However, too much rainfall is just as dangerous! Heavy rain brings mold, rot and mosquitoes with it, which in turn spread diseases. 

Torrential rain can even wipe out crops and cause widespread famine, which is what occurred in Europe during the Great Famine of 1315–1322. Heavy summer rain prevented grain from maturing, which meant no crops could be seeded in autumn. The rains continued through spring and the sowing of oats, barley and spelt was impossible. 

These rains not only caused some three million people to starve to death, they also led to a wave of brutal witch-hunting. Witches were accused of bringing on the hail and thunderstorms that had damaged the crops. Whether it's the source of life or the bringer of death, rain is certainly powerful!

> _"Rain can be a Janus; at once the face of salvation and despair."_

### 3. Umbrellas and weathermen have been around since the early stages of human history. 

Today we have weather satellites orbiting our planet so we can figure out when it'll rain. When it does, we've got waterproof jackets to keep us dry. Of course, observing and sheltering from the weather was much harder in centuries past! 

Weather predictions, however, are no new invention. The Ancient Greeks gave us the earliest recorded attempts at the scientific study of rainfall. In his scientific treatise _Meteorologica,_ Aristotle portrayed rainfall as one element of a sun-driven cycle that also determined the behavior of air, land and sea. Today, scientists consider sun, sea, wind and terrain as the main factors behind weather. Aristotle wasn't far off at all. 

By the fourth century B.C., civilizations had realized that the more they knew about rainfall, the better they were able to predict it. In India, bowls were used as rain gauges, while in Palestine rainfall data was recorded in written documents, a project carried out by generations for over 400 years. 

It wasn't until the nineteenth century that the first national weather network was developed in North America. Telegraph lines connected thousands of local weather observers, who collated and reported their findings. 

The way we protect ourselves from the rain has a long history too. Turns out that nearly every culture created it's own methods for staying dry. For example, the humble umbrella is recorded in just about every early civilization. 

An eighth century tomb in the ancient city of Gordion in Turkey contains the earliest known umbrella. Egyptians too developed their own umbrellas. Assyrians even created a collapsible version 3,000 years ago.

But none of these umbrellas were made of waterproof fabric. Waterproof material emerged in the eighteenth century thanks to Scottish chemist Charles Macintosh. The macintosh raincoat is named after him, and without his inventions we'd never have the Gore-Tex fabric we know and love today.

> The towering cumulonimbus cloud was listed number nine on an 1896 list of cloud types. This is why we say we are on "Cloud Nine."

### 4. Curious rains caused American farmers to move their crops to the desert and to rely on swindlers. 

During the 1870s and 1880s, the dry, deserted regions of Dakota, Nebraska and Kansas were subject to some very curious weather. Rain increased, grass grew and soil became fertile. Farmers began to move further west as a result. Luckily for these homesteaders, the rains followed suit. 

The further the farmers traveled, the better the conditions seemed to be. Rain was said to be "following the plow" and these ambitious farmers were thrilled. But not for long. 

The miraculous rains were strange exceptions to the normal climate patterns. Soon enough, the land dried up. The farmers were left with no rain but vast lands to cultivate. Desperate times called for desperate measures, and farmers looked to so-called _rainmakers_. 

These rainmakers were simply fraudsters who had convinced their customers they could summon a downpour. One of the leading rainmakers of the 1890s was Frank Melbourne, also known as "the Rain Wizard." 

Melbourne's first rainmaking demonstrations in Canton, Ohio in 1891 were soon the talk of the region. It wasn't long before Melbourne began to rake in the profits. He was able to charge as much as $500 for a "good rain" that spanned up to a hundred mile radius. 

Though his methods were shadowy and incomprehensible (involving a crank and gases that nobody ever saw in action), he inspired the trust and admiration of farmers. Often, it did rain on the days he performed his rainmaking. These dates were identical, however, to the days when rain was forecast.

### 5. From creative arts to cosmetics, rain is a timeless source of inspiration. 

Which country has the most authors in the world? And which city has one of the highest average cloud coverages and chances of rain? Iceland and its capital, Reykjavik. Is there a connection?

Well, it's not impossible! Rain is a common source of inspiration for popular musicians, artists, writers and filmmakers. Just think of Morrissey, lead singer of The Smiths, who spent his teenage years in rainy Manchester. He declared that "teenage depression was the best thing that ever happened to me." Who's to say that the gloomy weather didn't have a hand in it?

Numerous poets and writers have also looked to rain for inspiration. The sheer number of poems that include the word "rain" attests to this. Woody Allen also found himself inspired by wet weather. He once remarked that "if you look at all my films over the years, you'll find it's never sunny. … I love the idea of rain. I just think it's so beautiful."

Allen was right — rain is beautiful, and comforting too! This is something hundreds of retailers have capitalized on, marketing their cleaning supplies and beauty products with rain motifs. 

From Refreshing Rain laundry detergent and dishwashing liquid, Renewing Rain fabric softener, to Rain Clean toilet bowl scrubber and Midnight Rain bubble bath, the relaxing, refreshing qualities of rain are a perfect fit for our household chores and pampering.

> _"Rain may not be the sole cause of the anguish or the art. But no doubt, it can create a mood and inspire a melody."_

### 6. Frog rains and colored rains aren’t just meteorological myths! 

On June 12, 1954, Sylvia Mowday experienced something startling. She had been caught in a rainstorm while walking through a park north of Birmingham, England. This was no ordinary rainstorm, however. Thousands of frogs were pelting down from the sky. Meteorological myth? Let's look at the evidence. 

Episodes of raining frogs, toads and fish have all been described throughout history. From Ancient Greek literature to medieval chronicles, to accounts from French soldiers fighting Austrian troops in 1794, strange rains have bewildered people time and time again. 

Meteorology today points to tornadoes and waterspouts that are able to lift up and carry water containing fish or amphibians over land, where the animals are dropped to earth during rainfall. 

Frog rains aren't the only kind of freak storm humans have witnessed. Red rainstorms, for example, are well documented. We can explain red-colored rain using satellite images that show how the red dust of the Sahara is carried thousands of miles over the Atlantic Ocean and onto land. 

Perhaps even freakier than red rainfall is the black rain that fell over the British Isles during the nineteenth century. The emissions pumped into the atmosphere by hundreds of British factories had simply been picked up by the rain and returned to Earth!

### 7. Final summary 

The key message in this book:

**Humanity has relied on rain as a source of life since its very beginnings. Watching, measuring, praying for and predicting rain have become central activities in every civilization, while bizarre storms and destructive weather have frightened, mystified and inspired us.**

**Suggested** **further** **reading:** ** _Animal, Vegetable, Miracle_** **by Barbara Kingsolver, with Steven L. Hopp, and Camille Kingsolver**

_Animal, Vegetable, Miracle_ (2007) offers insights gained during the author's year-long sojourn in the countryside. They lived only on seasonal and local food, and their experiment reveals the right time to eat each vegetable and the importance of investing in the local food made by local farmers.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Cynthia Barnett

Cynthia Barnett is an environmental journalist who has reported on water from the Suwannee River to Singapore. She is the author of _Mirage_, which won the gold medal for best nonfiction title in the Florida Book Awards, and _Blue Revolution_, singled out as one of the top ten science books of 2011 by the _Boston Globe_.

