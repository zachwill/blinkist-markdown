---
id: 57483c67c678450003db710f
slug: raise-your-voice-en
published_date: 2016-06-03T00:00:00.000+00:00
author: Brian Sooy
title: Raise Your Voice
subtitle: A Cause Manifesto
main_color: 624C9B
text_color: 624C9B
---

# Raise Your Voice

_A Cause Manifesto_

**Brian Sooy**

_Raise Your Voice_ (2014) is a guidebook to getting your message across as a nonprofit organization. These blinks explain everything you need to know to build a stellar communication strategy that wins over supporters and keeps people enthusiastic about your mission.

---
### 1. What’s in it for me? Get your message heard loud and clear. 

According to some estimates, Nike spent over $3 billion on marketing and advertising in 2014. This huge budget allowed the sportswear giant to project their brand and story across multiple channels. So how can a small nonprofit like a local charity possibly compete with such full-throttle spending?

Well, of course it can't, but this doesn't mean it can't make itself heard. These blinks help explain how nonprofits of every size can communicate in a way that amplifies their message above the competition and reaches the attention of their potential supporters.

These blinks also show you

  * why everyone, from board member to volunteer must broadcast the same message; and

  * why your design should have a mission.

### 2. If you want to be heard, communicate with clarity. 

Can you even estimate how many brand messages you hear each day? It's probably in the hundreds, if not thousands and, for a resource-strapped nonprofit, making your voice heard in this cacophony can be nearly impossible.

Your only hope is effective communication, and that means speaking with a single, clear voice. That's because the best communication is clear communication. But what is clarity exactly?

It's when every part of your organization echoes the same message. For instance, if you work for Greenpeace, your message is "environmental care" and this has to be central to everything else you say. That means your campaign against nuclear power needs to connect to environmental care in just the same way as your work against political corruption.

But it's also essential to focus on the critical things you want to get across about your organization: your purpose — why you exist, your mission — what difference you'll make, and your goals.

For example, Greenpeace's message should emphasize their mission to end practices that are detrimental to the environment and to communicate their ultimate goal of a cleaner, safer, more peaceful world. Messages like this give people a good sense of what an organization does and how helping Greenpeace will help the world.

However, it's not just your publicity and advertising that need to speak with clarity, all your staff from the chief executive to your volunteers need to speak the same message too. So, ensure that everyone who speaks for your organization can clearly state the mission in just one sentence.

If you don't, a tide of conflicting messages will erode the clarity of your message.

And remember, clarity doesn't just mean being reasonable, informative and speaking your mind. Clarity is also about inspiration and speaking to people's emotions.

While inspiration and clarity may begin with your organization's board, it shouldn't stop there. Because once your board is committed, you can use them to gain commitments from every staffer down the ladder and eventually to reach your audience.

> _"The audience wants assurance that the organization is credible, and want to believe the cause is worth investing in."_

### 3. Non-profits need an engaging story and a design that matches. 

For your nonprofit to succeed, it needs to stand out from the countless other organizations grappling for public attention. But how?

By attending to your _positioning_ — that is, the way you're perceived.

To best position yourself you need to focus on _human-centered communication_, which means having a conversation with your audience and including them in your message. To do so, you'll need to give them the things they want to see and hear — messages that will inspire them to engage and respond.

Engaging your audience is key to building strong, long-lasting relationships. Here's how to do it:

First, you need to design a powerful narrative around your message. The best strategy for doing so is to carefully mix _pathos_ or emotion with _logos_ or rationality and _ethos_ — also known as credibility.

For instance, if your cause is emotional, like helping sick people, then you should add _pathos_ to your communication, maybe through real-life stories. Or, if you advocate a more rational cause like voting reform, then your message will benefit from adding more _logos_, for example, by using statistics.

But remember, no matter what your message, you'll always need _ethos_. Because without it, nobody will trust you. So be sure to make your expertise clear in whatever you do.

Then, once you've built an engaging story, you need to get it across. This is where _mission-driven design_ can help. This is basically a tool that ensures your design and marketing reflect your organization's purpose and mission, while also tending to your content and communicating your key points.

Aligning your design and marketing efforts with your mission will guarantee you a single voice while making people believe in your cause, your values and your culture. As a result, it will make your audience want to connect.

For instance, if you're an environmental organization with the mission statement "To protect our local wildlife," your design shouldn't include a jungle scene from some far-off place. Your flyer should be printed on recycled paper and you should choose a local animal for your emblem.

By making these choices, your communication will be whole and your design mission-driven — which means that anyone who comes into contact with your materials will know exactly what you're about.

> _"Mission-driven design moves your organization beyond branding to design with purpose."_

### 4. Use the cause manifesto to make your communication strategic and inspirational. 

So, you're ready to follow through with your mission-driven design and make sure your message is heard loud and clear. But how?

To implement this strategy you'll need a _cause manifesto_, composed of four principles. Let's examine the first two here.

The first is all about your purpose and how you communicate it strategically. To accomplish this you need to plan your goals and how you'll connect with your audience, all of which depends on a good, clear strategy.

The best plan here is to stick to one cause, one mission, one target audience and one purpose. In the example of the local wildlife protection nonprofit from before; your audience is the local population. That means you shouldn't communicate in complicated business-speak but use their own language instead.

And don't talk about peak oil if your local issue is plastic bags. In other words, be strategic and focused. Finally, give your supporters a purpose or a goal they can believe in, like clearing a specific area of plastic bags by a certain date.

The second principle of the cause manifesto is about inspiring your audience through _pathos_ and _logos_, adjusted to appeal specifically to the audience to which you're reaching out. To do this effectively you need to offer a vision of the future and how you want to impact it. For instance, you may want to make your local environment healthier with no more animals dying because they swallow plastic bags or get trapped in them.

But you'll also need to be insightful, giving plain evidence of the possibilities. That means using clear data to outline your purpose. For example, you might show how many bags you'll remove and by what date.

And finally, you need to engage your audience in an experience. For instance, you could organize a "plastic bag pick up day" and give out prizes to whoever gathers the most trash. People will be enticed to join in and learn about your purpose.

So, now that you know the first two points of the cause manifesto, it's time to learn the last two.

> _"To communicate effectively, you have to eliminate possibilities in order to create opportunity."_

### 5. Relating to and inspiring your audience are essential for effective communication. 

The third principle of a cause manifesto requires using _relational principles_ in your communication. This means it should be full of content and make people interact with your organization.

There are lots of ways to accomplish this, like by making shareable content or showing your audience how grateful you are to them. By doing so you'll build a relationship that encourages them to become active as new ambassadors or advocates.

For instance, consider the local wildlife protection nonprofit again. This organization could keep its donors motivated by sending each one a card with a diagram that depicted how much forest they had cleaned up. Then, they could post the same image to their website and on social media so that donors could share it with their friends and put their good deeds on display.

Also, if it's possible, holding a yearly event for your donors and supporters is a great idea. This is a chance to show them how grateful you are for all their help and to ask for their feedback.

The final component of the cause manifesto is that your communication needs to stick to aspirational principles that guide the tone of your voice. That means being positive. After all, optimism is a whole lot more motivational that pessimism.

So, whatever you say, be sure to put a good spin on it. For instance, rather than talking about what dire straits the environment is currently in, talk about the opportunity you have to improve it and what a major role your supporters play in doing so.

But to be inspirational, your communication also has to be powerful. That makes it essential to show how your cause is meaningful and has the power to transform the world.

For example, our local wildlife nonprofit might explain the importance of their work by pointing to the connections between local wildlife, local environmental health and healthy living. It could then be explained to supporters how they can help protect wildlife by simply cleaning up after themselves when outdoors.

> _"The trust a supporter gives an organization is more important than the donation that accompanies it."_

### 6. Final summary 

The key message in this book:

**Your nonprofit's cause isn't a commercial brand, and that means that in order to get the word out you'll need to communicate your identity and use your personality. The key is to focus on your audience and use their perceptions to engage them in the work you do.**

Actionable advice:

**Use design and data to make your story visible.**

A strong image can evoke intense emotions and lead people to action. And if your cause appears too abstract, a little data can help. That's because communicating through data will show that you are engaged and trustworthy, will inspire your audience and lead them to support you.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Marketing Above the Noise_** **by Linda J. Popky**

_Marketing Above The Noise_ (2015) guides you through today's marketing world, helping you separate useful advice from useless noise. Advising against jumping on the bandwagon and following all those hot new marketing trends, these blinks demonstrate that tried and true approaches to marketing are the best way to win over — and hold onto — customers.
---

### Brian Sooy

Brian Sooy is an entrepreneur and principal of Aespire, a design and communication firm that helps nonprofit and philanthropic organizations.

