---
id: 5800df32230ff80003a1b856
slug: raising-cain-en
published_date: 2016-10-21T00:00:00.000+00:00
author: Dan Kindlon, Ph.D. and Michael Thompson, Ph.D.
title: Raising Cain
subtitle: Protecting the Emotional Life of Boys
main_color: D4372C
text_color: BA3027
---

# Raising Cain

_Protecting the Emotional Life of Boys_

**Dan Kindlon, Ph.D. and Michael Thompson, Ph.D.**

_Raising Cain_ (1999) explains how boys have to navigate a society rife with misguided ideas about masculinity and filled with cruel classmates who are ready to pounce on any sign of weakness. Discover how these conditions can create emotionally stunted and suicidal young men, and find out what can be done to help remedy their situation before it's too late.

---
### 1. What’s in it for me? Understand the world of raising boys. 

As the saying goes: "Boys will be boys." This simplistic tautology implies that boys will always be troublesome, causing disruptions in school and fighting among each other, and this is no cause for concern.

But what is it like from a boy's perspective? From early education to relationships with their mothers, boys do differ from girls in many ways. Here, we will look at how we as a society groom boys into men and the problems that derive from that process.

In these blinks, you'll discover

  * why older boys should be in classes with younger girls;

  * how the "boys will be boys" attitude makes for a cruel, emotionally guarded world; and

  * what impact father and mother figures have on boys.

### 2. Society has an enduring and misguided image of masculinity that leads to bad behavior among adolescents. 

You're probably familiar with sayings like "Real men don't cry" or theories stating that women prefer the "strong and silent" type of man over someone who shows emotion.

Ideas like these represent a traditional stereotype of masculinity that is still very dominant in our society and continues to influence boys who are growing up today.

Especially troubling are the ubiquitous images of tough, violent and emotionally detached male heroes.

These figures have been around for generations, going to back to the movies of John Wayne and James Dean. And today they can still be seen in video games like _Grand Theft Auto_ or _Hatred_, or in popular action movies where the protagonist is a cold and remorseless man who solves his problems with a gun.

Parents may try to protect their children from these images, but they are hard to escape when even television commercials and ads celebrate similar male stereotypes. Everywhere on TV, the idea of a real man is someone who drives fast cars, drinks bourbon whiskey and objectifies women.

Influenced by these images, boys try to live up to the male ideal by proudly binge drinking, getting into fights, driving drunk and having casual sex.

There have even been studies that directly link such bad behavior to the pervasive image of macho masculinity.

In a _National Survey of Adolescent Males_ conducted in 1995, heterosexual boys between the ages of 15 and 19, from all across the United States, were interviewed about their behavior.

The boys were first asked whether they agree or disagree with certain statements related to masculinity in society, such as "A guy loses respect when he talks about his problems."

In follow-up questions, the boys were asked about their behavior in regards to sex and drugs. The results showed that the more a boy agreed with the stereotypical ideas about masculinity, the more likely he was to take drugs and coerce someone into having unprotected sex.

### 3. Boys develop certain skills at a later age than girls, which can cause them to underperform and act out in school. 

Teenage boys are not the only troublesome bunch. If you've ever witnessed a schoolteacher trying to read to a class of young children, you'll often see the girls quietly paying attention while the boys get restless and goof around with each other.

Boys are nurtured to be more adventurous and less well-behaved, but a primary reason they have such a hard time following the rules in elementary school is that they mature at a later age than girls.

Psychologist Diane Halpern cites biological differences as a main source of trouble for young boys. For instance, the _synapses_ that connect our neurons, and help our nervous system function, develop at a faster rate in girls than in boys.

This is why girls tend to perform better in school and are quicker to pick up basic skills, such as reading, counting and identifying objects and colors.

As a result, boys make up between 60 and 80 percent of the children who suffer from learning disabilities. This disadvantage can make them feel inadequate, which may lead to their acting out during the first years of schooling.

Some researchers suggest that a possible solution might be to rearrange classes and put eight-year-old boys together with six-year-old girls, so that the children will be at the same developmental level.

Another skill that boys are slow to develop is impulse control. This can lead to problems in school as they may exhibit hyperactivity and even struggle to remain seated.

In fact, boys are between two and four times more likely than girls to be affected by hyperactivity. For instance, a study of thousands of kindergarten children in Tennessee showed that four percent of all boys showed symptoms of hyperactivity, compared to just one percent of girls.

Montessori kindergartens have found a solution to this problem. These schools specialize in self-motivated education, and they've started an exercise corner for children to jump rope and burn off excess energy, reducing disruptions in the classroom.

> _"I love all children, except for boys."_ \- Lewis Carroll

### 4. Schools are a cruel environment that can cause boys to become guarded and to shut down emotionally. 

If you've spent any time around adolescent boys, you know how eager they are to watch the same TV shows, read the same comic books or wear the same clothes as the other kids in school. But these boys aren't just following trends; this behavior is a survival strategy, a way to cope with the environment they're forced to grow up in.

After all, the everyday world of adolescent boys can be cruel, especially if you're unpopular.

Both the authors are psychotherapists who work with adolescent boys in schools, and they've heard enough disturbing tales to know that cruelty is quite common in these boys' environments.

On the birthday of one unpopular boy, his classmates teased him by intentionally forgetting his name as they sang "Happy Birthday," and the cake they prepared was actually a block of ice covered in frosting.

One of the most vicious places can be the school locker room, where it's not uncommon for bullies to urinate on another boy's clothes or in his shampoo bottle.

The bathroom is another place to beware of bullies. Some boys avoid using them all together as they're afraid of being shoved into a urinal or otherwise assaulted and taunted.

This culture of cruelty leads many boys to live in a state of constant fear, eventually becoming emotionally guarded.

If you were to walk down the hallway of a school, you might think that most boys appear to be friendly toward one another, but they're all living in fear.

Every boy is aware of how vulnerable he is; even the popular kids and the bullies know that all it takes is one embarrassing incident or display of emotion, and they'll find themselves at the bottom of the pecking order.

This is why so many boys participate in these cruel rituals. Even if they don't feel any animosity toward the target, it's better to be the perpetrator than the victim.

So, this hostile environment teaches boys to constantly have their guard up, hide their emotions and to trust no one.

### 5. Having the good example of an emotionally involved father can lead to a better life for young men. 

Traditionally, raising children was considered the job of the mother. Even when it came to psychological research, studies were mainly done on the mother-child relationship while generally ignoring the importance of the father's role. 

This discrepancy was pointed out in a 1998 article in the _New York Times_ ; that same year, research into the father-child relationship started appearing.

In May of 1998, the journal _Demography_ devoted an entire issue to exploring the importance of the father's role in the family.

The published results showed that boys who had fathers that played an active role in their lives performed better in school, were more psychologically stable and ended up with better jobs later in life.

And when the researchers looked into how much money individuals earned at the age of 27, the most significant determining factor was how often their fathers had attended school parent-teacher meetings.

While an involved father is important for any child's development, this is especially true for boys.

Support for this can be found in a 1998 study by the University of North Carolina. It found that when a father is emotionally involved and close to his family, the risk of his children committing acts of delinquency or vandalism is significantly reduced. And, of course, boys are the most common perpetrators of these acts.

The same study also showed that involved fathers tend to give special attention to their sons, and that this relationship can be especially important to the emotional well-being of young men.

Since boys are slow to mature and in need of experienced guidance around sexual relationships, fathers can be important role models, helping young men develop empathy and showing them how to establish healthy relationships with women.

> _"Fathers need to celebrate the accomplishments of their sons, to honor them for who they are._ "

### 6. Physical comfort between mothers and sons can be awkward, but young males need this contact. 

Were you ever embarrassed when your mom dropped you off at school and tried to give you a hug or a kiss in front of other students? This is a pretty natural reaction for boys. As they get older and explore the world around them, things like physical intimacy with their mother can get uncomfortable.

But while adolescent boys tend to shy away from a mother's embrace, mothers can also feel awkward about this kind of physical contact, even if their sons are still looking for tenderness.

For example, the authors' share a story about a patient who was concerned that her thirteen-year-old son was still climbing into her bed, along with his younger brothers and sisters, to give her cuddles.

Mothers can fall victim to masculine stereotypes as well: some fear that giving too much physical comfort will make boys overly sensitive or "turn" them gay. But fears like these are completely unfounded.

In fact, a mother's comforting touch is important to a son, no matter how old he is.

Child psychologists recognize a nurturing touch as being important to a child's development, and according to neuroendocrinologist Robert Sapolsky, it continues to be important as they grow into young men.

One of the benefits of a nurturing hug is that it can greatly reduce stress. And the awkwardness that might come as a child grows up can be overcome.

A mother by the name of Hope ran into her awkward-hug moment when her son Aaron was ten years old. One day, Aaron stopped leaning in for a hug before heading off to school. Instead, they just smiled and waved goodbye.

But Aaron ended up missing the nurturing contact as much as his mom. Later that week, Aaron came home and asked his mom for an "energizing" hug after school.

And this became their new routine, Aaron gets a comforting and de-stressing hug on his own terms and everyone is happy.

### 7. When boys become isolated and depressed, it’s important for loved ones to help. 

When you think of a typical angsty teenage boy, you might imagine an adolescent who retreats to his room, preferring to play video games rather than socializing. Seeking isolation in this way is not uncommon.

Young men often find it difficult to communicate and come to terms with their own emotions, which can lead them to shut down and isolate themselves.

Take Martin, for example, who suffered from severe loneliness as a young man.

Growing up, Martin received no emotional support from his parents; he had a harshly critical father who was never satisfied, and a mother who was emotionally distant and preferred spending time with his sisters. Plus, at school, Martin only had acquaintances and no real friends or social life.

All this led Martin to seek the solitude of his room, where he could nurture his own feelings of self-loathing. Sometimes he would simply sit by the window and stare out at the rest of the world from inside his own lonely bubble.

Young men often have emotional difficulties like this and they need help from their loved ones in order to properly address them before they get worse.

As we saw in previous blinks, young men tend to consider asking for help a sign of weakness and are reluctant and embarrassed to talk about their emotions. So, it is up to a loved one to recognize these unhealthy symptoms and provide help.

Sadly, this often doesn't happen, as the stereotypical image of the depressed and withdrawn young man has become so commonplace that many consider it a normal phase that every adolescent goes through.

It's true that there is a normal amount of withdrawal and desire for privacy and independence in adolescent boys. But it's important to notice when a young man also starts to retreat from his friends and spend long periods of time stuck in a gloomy mood.

When this happens, a parent or loved one should consider therapeutic assistance.

### 8. Boys are at risk of teenage suicide, so any distress signals must be taken seriously. 

Another commonly held misconception about boys is that they're all strong, resilient and capable of getting through anything.

But the statistics of teen suicides tell a different story: they show that there is a clear gender divide that puts boys at a dangerously high risk.

While more girls _attempt_ suicide, it's boys that account for the most casualties.

According to the 1997 American National Health Statistics, there were 1,890 suicides by people between the ages of 15 and 19, and 1,625 of these fatalities were boys. That's 86 percent overall.

And it doesn't get much better when we look at people ages 10 to 14. Of the 330 suicides on record, 253 were boys. That's 77 percent.

These statistics are certainly a cause for concern, but what can we do to help?

First of all, it's important to pick up on any distress signals and take them seriously, because the sad truth is that boys tend to be ignored even when they do call out for help.

Keith was one such boy. The author was a counselor at a school where Keith had passed a note around his class that said he was going to jump from a bridge that afternoon.

The author was told about the note by two of Keith's classmates, so he pulled Keith out of class to talk with him. Even though the troubled boy denied his intentions, the author insisted on informing his parents.

The first reaction from Keith's parents was that it was simply a bad joke on Keith's part and they didn't even want to bother coming to the school. And when they were finally able to show up, they still refused to see the note as a call for help, even though Keith's father had his own history of depression!

Tragically, Keith was moved to a different school where he committed suicide. Only then did his parents finally understand the pain their son was experiencing.

### 9. Emotional problems make it hard for men to have healthy relationships, but this can change later in life. 

Everyone knows the cliché of a manipulative, cold-hearted man seducing women and then never calling them, despite promises to do so. But even such callous behavior doesn't mean the man isn't still an emotional human being.

The problem is that young men have a difficult time processing their emotions, and this often gets in the way of their having healthy relationships with women. 

Let's look at the typically troubled love life of Jerry, who was thirteen when he first fell in love. At the time he was quite the romantic, and he and his girlfriend would seek out secluded spots for making love.

This lasted for three years until one day, the girl told Jerry that she'd slept with someone else.

Jerry was unable to process his emotions surrounding the heartbreak he was feeling. Instead, he got drunk and went after the boy who'd slept with his girlfriend. But he was much bigger and Jerry only managed to further humiliate himself by getting beaten up.

After that painful defeat, Jerry pretended he didn't care and never wanted to see the girl again, continuing to leave his emotions unprocessed.

This kind of delusion would continue to taint his future relationships and, out of fear of getting hurt again, he would only engage in casual sex.

Fortunately, men tend to mature and learn how to deal with their emotions when they reach their thirties.

This is what happened to Geoff, who spent his youth as a notorious womanizer.

As a young man he would listen and talk to women only as a way to get them into bed. Otherwise, if he found himself developing feelings for a woman, he would withdraw or push her away by offending her or refusing to talk at all.

For most men, this kind of attitude changes when they hit their thirties.

Just as criminologists know that many delinquents put an end to their illegal activities in their thirties, most men will stop emotionally and sexually manipulating women around this age.

Even Geoff turned out alright. He eventually became a loving husband and emotionally available father.

### 10. Final summary 

The key message in this book:

**Men are not selfish and unfeeling people, even though this might appear to be the case, especially as they work their way toward adulthood. Unfortunately, boys grow up in a hostile environment where classmates and bullies are ready to attack any signs of emotion, weakness or vulnerability. This makes them feel the need to hide their feelings and emotionally, or even physically, cut themselves off from the rest of the world.**

Actionable advice:

**Allow boys to have an internal, emotional life.**

Encourage them to speak about their feelings. This will help them to better understand their own emotions and to communicate openly rather than reverting to defensiveness and aggression.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _How to Talk So Kids Will Listen & Listen So Kids Will Talk _****by Adele Faber and Elaine Mazlish**

_How to Talk So Kids Will Listen & Listen So Kids Will Talk_ (1996) is a practical, clear guide that helps parents improve communication with their children. Faber and Mazlish present realistic scenarios that can have parents tearing their hair out in frustration, and then provide precise tactics for not just coping, but turning around the situation and creating more harmonious parent–child interactions.
---

### Dan Kindlon, Ph.D. and Michael Thompson, Ph.D.

Michael Thompson, PhD, is a family psychologist based in Cambridge, Massachusetts. He is a _New York Times_ best-selling author who has offered his counseling skills to thousands of schools around the world. His other books include _Homesick and Happy_ and _The Pressured Child_.

Dan Kindlon, PhD, is a leading child psychologist who has taught at Harvard University for over 15 years. He specializes in treating learning and behavioral disorders in children and young adults. His other books include _Tough Times, Strong Children_.

