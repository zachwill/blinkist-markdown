---
id: 58bd12f7c7c53e00041d65e9
slug: rare-en
published_date: 2017-03-07T00:00:00.000+00:00
author: Keith Veronese
title: Rare
subtitle: The High-Stakes Race to Satisfy Our Need for the Scarcest Metals on Earth
main_color: 3C619B
text_color: 3C619B
---

# Rare

_The High-Stakes Race to Satisfy Our Need for the Scarcest Metals on Earth_

**Keith Veronese**

_Rare_ (2015) sheds light on the common but elusive chemical elements beneath the earth's surface — elements that play an increasingly important role in the development of modern technology. Get a better sense of what's really driving the geopolitical struggles between the world's superpowers, and what a group of rare earth metals has to do with the future of our energy sources, gadgets and military technology.

---
### 1. What’s in it for me? Learn about the little-known minerals that play an integral role in our lives. 

Do you know what _lanthanum_ is? How about _cerium_ or _praseodymium_? If you're like most people, then you probably feel a bit baffled at this point. So what are these things? Well, they're _rare earth metals_, a group of minerals that, when taken together, encompass 17 chemical elements. Each has an extremely odd name, and although none of them is well known, each plays a crucial role in technology, both current and prospective.

With unique properties that make them all but indispensable today, rare earth metals are present in smartphones, nuclear plants and advanced war technology. These blinks explore the history of these metals, as well as their many uses and the possible impact they'll have on our geopolitical future.

In these blinks, you'll learn

  * why thallium sulfate is called "inheritance powder";

  * what importance beryllium holds for the US Air Force; and

  * how China's access to rare earth metals will prove pivotal in the future.

### 2. Rare earth metals are actually quite common, but they’re only found in tiny concentrations. 

What is rare is always a matter of context and perspective. For example, in nineteenth-century China, it was rare to see someone riding a bicycle; today, however, there are about 430 million bicycles in China, and cyclists are a daily sight.

Similarly, the actual rarity of rare earth metals is a matter of perspective.

The name "rare earth metals" is shorthand for a category of 17 different chemical elements, all of which have odd names that are difficult to pronounce, like cerium, praseodymium, scandium and yttrium.

When these elements were first discovered, people believed they were as scarce as diamonds or gold — hence the name. But in truth some of these metals can be found just about anywhere along the earth's crust.

In fact, if you were to examine a random sample of soil, you'd probably find as much europium, neodymium, holmium and ytterbium as copper, cobalt or nickel.

So it's not that rare earth metals are hard to find; it's that they're only found in very tiny quantities and it is extremely difficult and costly to separate them from their surroundings. This is why these elements continue to be considered rare, despite the fact that they're all around us.

These metals exist in such negligible quantities that you could go through tons and tons of soil and come away with only a gram or even just a milligram of one type.

Therefore, in order to acquire enough of these elements to be useful, massive amounts of rocks have to be demolished, separated and sifted in a complicated and ultimately wasteful procedure. To remove the unwanted parts of the rocks, chemicals are used that actually destroy a portion of the elements that are being extracted.

This means that, at the end of the day, you actually get even less of the rare earth metal.

### 3. The first rare earth metal, discovered in the eighteenth century, wasn’t put to use until much later. 

You could say that pinpointing a rare earth metal in a handful of soil is like searching for a needle in a haystack, so it stands to reason that their discovery was the result of chance rather than intention.

That first discovery happened in Sweden, in the late eighteenth century.

This wasn't the work of a scientist or a geologist but of an army officer named Carl Axel Arrhenius who came across a strange black rock in a quarry near Ytterby, Sweden.

Arrhenius named the rock _ytterbite_, after the location, and, three months later, after failing to determine its composition, he decided to hand it over to chemist Johan Gadolin.

Gadolin found that a third of the black rock was composed of a completely unknown chemical compound, which he in turn named _yttria_.

Gadolin spent the next few years analyzing the rock, and during this time he found that yttria was actually a compound of several different elements. Among these was a new element, bound up with oxygen atoms, that he named _yttrium_.

In chemical terms, the presence of the oxygen atoms meant that yttrium was an oxide, but it also meant that it would be incredibly difficult to separate the element from these oxygen atoms.

Because of this difficulty, centuries passed before rare earth metals were put to use.

Other elements, like gold, can be extracted from oxide manually, but this isn't the case with elements like yttrium, so new methods had to be developed.

By the twentieth century, however, scientist had developed a method of bathing the rocks in concentrated acids and bases. The different liquid solutions allowed different elements to be extracted from the rock, a process that eventually rendered pure yttrium.

Once isolated, this particular element was used, among other things, to make various screens — for radar and televisions and computers — where it helps create the color red.

### 4. Rare earth metals help stabilize nuclear power plants, but they also create nuclear waste. 

You've probably heard of dangerously radioactive elements such as uranium, and you probably have an idea of how they've been put to use. But did you know that rare earth metals are the real heroes of nuclear power plants?

Unlike radioactive elements, rare earth metals are used to keep the nuclear power plants safe.

Their ability to absorb neutrons makes them an ideal ingredient for the fuel rods that power the nuclear reaction. They help keep the reaction gradual and manageable instead of sudden and devastating.

This process even has a bonus side effect. As the fuel rods partially disintegrate from the radioactive heat, a significant amount of the rare earth metals, rhodium, ruthenium and palladium, are created and extracted. 

But of course this system isn't without its dangers.

All fuel rods eventually diminish to the point where they're no longer useful — and the question of what to do with this nuclear waste is as tricky as it is controversial.

The primary method is to store the waste in facilities that are located far from any human settlements and reinforced with lead to keep the radioactivity contained. However, this method has been the subject of much scrutiny and criticism, with many wondering how safe and secure these facilities really are.

Recently, a method of recycling the materials has also been used, where the radioactive compounds are separated and neutralized, and then put to use elsewhere. But reprocessing nuclear waste isn't as easy or as safe as it might sound.

Plutonium, for instance, once it's been neutralized and separated from the nuclear waste, is a potentially major security hazard. If anyone managed to get their hands on enough plutonium powder, they could conceivably build their own nuclear bomb.

### 5. Some rare earth metals, such as the discrete poison thallium sulfate, can be potent murder weapons. 

If you like to read a good murder mystery, you might be familiar with the 1961 Agatha Christie novel _The Pale Horse._ The plot hinges on a number of mysterious deaths that are revealed (spoiler alert!) to be poisonings caused by the metal _thallium_.

Unfortunately, rare-earth-metal poisoning is not just a thing of fiction. 

Christie even received a fair amount of criticism for providing would-be murderers with an untraceable way to administer death.

As it turned out, thallium became such a popular poison that it was dubbed "inheritance powder" due to the number of heirs and heiresses that benefited from a relative's early (and suspect) demise.

In its natural state, thallium doesn't dissolve in water. But in the compound _thallium sulfate_, it becomes a white crystallized salt, and the metal is given an electromagnetic charge that allows it to bond with water molecules. 

All that is needed to kill an adult is one gram dissolved in a drink. And since it was the main ingredient in commercial rat poison until 1972, thallium sulfate wasn't so hard to come by.

But this isn't the kind of poison that has someone gasping and clutching their throat after one sip of tainted coffee. What makes thallium sulfate the true poison of choice is how discreet it is.

After a victim ingests it, the compound breaks apart, allowing the thallium metal to enter the bloodstream. The atoms of thallium, which closely resemble those of potassium, are allowed entry to the cellular channels, and this leads to a slow deterioration of health that, within a few weeks, results in death.

This slow sickness has symptoms that can easily be confused with many other deadly illnesses, making it an ideal murder weapon that still gets used today.

In 2011, after further inspection of what appeared to be the suicide of a man in Boston, it was revealed that he'd actually been poisoned with thallium by his ex-wife, Tianle Li, a pharmaceutical chemist.

### 6. Most rare earth metals are found in China, where they accumulated about 400 million years ago. 

Many of the political and economic discussions these days include remarks about the growing power of China. But what often goes unmentioned is China's key resource: rare earth metals.

Remarkably, the vast majority of all the planet's rare earth metals are in China. 

The country is so rich in these precious elements that geologists compare it to the wealth represented by Saudi Arabia's oil reserves.

In other words, the rest of the world has to beg for scraps and is utterly dependent on China to manufacture the numerous products that require these metals.

Naturally, unstable political relations only make access to these raw materials all the more difficult.

Between 2012 and 2015, China tried to restrict the exportation of rare earth metals, but their attempts were overruled by the World Trade Organization in 2015. 

Much of China's rare earth metals are concentrated in one area: the Bayan Obo Mining District in the region of Inner Mongolia.

Originally developed to extract iron, these mines date back to the 1930s. But after chemists discovered the importance of rare earth metals, the focus shifted to the extraction of much more valuable resources, such as tantalum and niobium.

Okay, but how did this area end up with such a high concentration of these elements?

For an answer we have to go back over 400 million years ago, long before humans walked the earth, to the mid-Proterozoic period. At this time, the earth was much more like Venus, with searing heat, swamps and an atmosphere that contained very little oxygen.

Though we can pin down the arrival time of the rare earth metals, no one is certain exactly how or why they appeared.

There is a theory, however, that this large deposit in China is the result of a tectonic shift that brought up volcanic magma and precious metals from the earth's core.

### 7. Afghanistan, long a mere pawn in military games, could be helped by rare earth metals. 

Afghanistan is also home to a significant amount of rare earth metals — a fact that may help the country liberate itself from its violent history. Positioned in the geopolitical hotbed between the Middle East, Russia, China and India, Afghanistan has long been an arena where foreign forces wage their wars.

Much of today's turmoil dates back to the 1980s, when the United States supported Afghan rebel factions who were fighting against the Soviet Union.

The US involvement was part of a multi-million dollar CIA operation called Cyclone, which provided weapons — including portable anti-aircraft missiles — and training to tens of thousands of Afghan soldiers between 1979 and 1989.

The CIA wanted to make sure the Soviet Union couldn't rule over Afghanistan, and so a war between the United States and the Soviet Union was waged on Afghan turf.

Ironically, the United States would return to Afghanistan a decade later, at the turn of the twenty-first century, this time to fight the militant Islamists they had helped arm and bring to power.

Afghanistan may yet find a path to prosperity, however.

The best hope for Afghanistan to move past its history as a war zone and become a nation with a stable economy may well be its supply of rare earth metals.

The 2010 US Geological Survey reported a large amount of rare earth metals, as well as iron and gold, residing just beneath the surface of the Afghan soil. The value of these resources has been estimated at somewhere between one and three _trillion_ dollars.

Such wealth, stemming from natural geologic resources, could potentially rebuild the economy and the education system, as well as bring communities out of poverty. Rare earth metals could be Afghanistan's way to finally escape being the battlefield on which other powerful nations play their war games.

### 8. Rare earth metals are crucial for military and energy technologies. 

Could you list the elements present in your smartphone or video game system? More and more, these technologies rely on rare earth metals (which is probably why most people couldn't tell you what's going on behind the screens). But rare earth metals are mainly coveted for less pedestrian reasons. 

Indeed, they're primarily used in new military technologies. 

The US Department of Defense has said that the rare earth metal _beryllium_ plays a central role in their plans for US security, while also acknowledging that supplies are scarce.

Currently, a beryllium-aluminum alloy is used to make the frames of five families of fighter jets, including the popular F-35 Joint Strike Fighter, which many countries rely on for military security.

Beryllium's lightness makes the alloy a superior material, perfect for optimizing aerodynamic dexterity.

Within many aircrafts and drones, you'll find beryllium being used to build the electrical circuits. It can also be found in radar technology and the devices used to detect bombs and other explosives.

The military also takes advantage of beryllium's ability to make glass resistant to the vibrational pressures common to most combat zones; it's used, for instance, in the mirrors that tank drivers use to navigate.

And of course there are many non-military uses for rare earth metals as well — particularly in the area of new energy technologies.

Electric cars are heavily reliant on the rare earth metal _lanthanum_. In fact, every Toyota Prius requires nearly twenty pounds of lanthanum, most of which is used to increase the storage capacity of the car's battery. Naturally, without such a powerful battery, the Prius wouldn't be such a success.

More controversially, there's thorium — a radioactive rare earth element that is being developed as an alternative to traditional uranium for use in new nuclear power plants.

Though research is ongoing, future nuclear facilities could potentially be powered by a thorium-fluoride salt that is predicted to be more stable and much safer.

### 9. Outer space could be the future source of rare earth metals, but the question of celestial ownership remains undetermined. 

Like other natural resources, there is a finite amount of rare earth metals, so the central question is: What will we do when our supplies run dry?

While some experts are searching for renewable resources as a replacement, others are looking to the stars.

No one knows for sure just how large the supply on other planets may be, but there's a good chance that it's quite big indeed.

After all, rare earth metals were first formed when stars were dying in outer space.

When a star is born, its core contains only basic elements like hydrogen, helium and lithium. But as the star gradually heats up, atoms like iron begin forming.

And as the iron continues to build up, the star loses energy and starts to cool down, which is when the death spiral begins. It's during this time that neutrons collide with atomic nuclei in a way that produces rare earth metals and other complex chemical elements.

But the final frontier of outer space poses many questions, such as who is going to lay claim to these precious metals. 

The United States may have been the first to plant a flag on the moon, but it remains to be seen whether this entitles them to all the moon's resources.

The United Nations tried to resolve potential disputes in the 1966 Outer Space Treaty. It states that outer space should be treated like deep oceanic waters and not be considered the property of any one nation; rather, it should belong to all nations.

Notably, none of the primary outer space explorers — Russia, China, the United States — has signed this treaty. Each of them is obviously waiting to see if they can claim sole ownership of certain celestial territories — and certain valuable metals.

### 10. Final summary 

The key message in this book:

**Rare earth metals are a group of complex chemical compounds that are going to play a large role in the military and civilian technologies of the future. The race for resources is on, and China has a crucial advantage.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Making the Modern World_** **by Vaclav Smil**

_Making the Modern World_ (2014) is a guide to humanity's material consumption through history and into the future. These blinks explain the major material categories of our time and how we can effectively manage them as we move forward.
---

### Keith Veronese

Keith Veronese has written for Gawker Media and Alpha Brand Media. In 2011, he received his PhD in chemistry from the University of Alabama, in Birmingham. He is also the author of _Plugged In: Comic Book Professionals Working in the Video Game Industry._

