---
id: 552bd5116466340007020000
slug: rational-ritual-en
published_date: 2015-04-13T00:00:00.000+00:00
author: Michael Suk-Young Chwe
title: Rational Ritual
subtitle: Culture, Coordination, and Common Knowledge
main_color: B7D584
text_color: 607046
---

# Rational Ritual

_Culture, Coordination, and Common Knowledge_

**Michael Suk-Young Chwe**

_Rational Ritual_ (2001) offers a profound, game theory-based analysis of the role that rituals, ceremonies and media events play in society. Throughout the ages, these rites have been used to create "common knowledge" that allows people to solve problems such as which ruler to obey and which products to buy. Essential reading for budding Robespierres or Steve Jobses alike.

---
### 1. What’s in it for me? Find out why religious services and Super Bowl advertising have the same goal. 

What do choosing to buy a certain brand of beer at the supermarket and deciding whether or not to go to an anti-government protest have in common?

In both cases, you're more likely to do something if you know that others are acting the same way. And since the same is true for everyone, we're constantly trying to coordinate our own actions according to those of others. Indeed, such dilemmas are called coordination problems, and in these blinks, you'll understand how we solve them using "common knowledge."

Once you understand the vital role of common knowledge, you'll begin to see how many events and rituals from royal coronations to the Super Bowl actually aim to generate common knowledge.

You'll also discover

  * why most countries in the world drive on the right-hand side of the road;

  * why beer, soft drink and car brand advertisements dominate the Super Bowl; and

  * why you're more likely to hear about career opportunities from acquaintances than close friends.

### 2. Certain situations known as coordination problems are solved through common knowledge. 

Imagine you live under a repressive regime — hopefully this is a hypothetical situation for you.

One day, you receive a message that there's a protest being held against the government. Would you attend?

Your decision will probably depend on whether or not you thought other people would attend: the more people show up, the less likely you are to be arrested or beaten, so you'd be more inclined to go.

This is an example of a _coordination problem_ : a situation where each person's willingness to participate in a joint action increases if others participate too. In a coordination problem, every person cares about what the other person knows and thinks.

The scale of coordination problems can be seen when we look a little deeper into the protest example, given that every potential protester faces the same dilemma as you, only wanting to participate if others do too.

Therefore in order to make the decision to participate, it's not enough that a person knows about the invitation. Each person must also know that every other person knows about the invitation. In fact, each person must know that every other person knows that every other person knows, and so forth. Put simply, the invitation must be _common knowledge_.

A simple way to define common knowledge is through the difference between the CC (carbon copy) and BCC (blind carbon copy) fields in an email message. Everyone whose address is in the CC field will not only receive the message, but also see who else received it, whereas recipients in the BCC field will only receive the message. This is why using CC generates common knowledge: everyone knows what everyone else knows.

As we'll see in the following blinks, this concept of common knowledge is surprisingly powerful, and can explain a great many phenomena.

> _"...the meaning of a wink depends on it not being common knowledge."_

### 3. Public rituals create common knowledge, which is why authorities rely on them to sustain their power. 

Have you ever wondered how individuals decide to submit to a social or political authority like a government? This is also a coordination problem: each person is more likely to support the authority if others do too.

We have seen that common knowledge is key to surmounting coordination problems, which is why authorities use _ritual_ s in order to create common knowledge supporting their power.

One example of this can be found in the royal progress, an old ritual where monarchs paraded around their realms from town to town. The idea was to assemble large crowds of peasants along the route to witness the symbols of the monarch's dominance. But a more important function was that the individual peasants would see the crowds and know that others had seen the same signs and symbols. In other words, common knowledge was created.

Of course, when the authority changes, the rituals and political symbols used by the old regime must be reinvented, and communicated to the public as the new norm.

This could be seen in the aftermath of the French Revolution, where countless rituals like revolutionary festivals and oath-swearings were held. The new regime even went as far as to establish new units of measurement: the metric system. They also changed the conventions of travel, dictating that people should travel on the right-hand side of the road. This was meant to be a democratic shift, because peasants on foot traditionally used the right side to better spot oncoming traffic.

So why did the revolutionaries bother with such trivialities? Well, getting people to accept these new conventions was a relatively easy coordination problem, and solving it also helped deal with another: acceptance of the new regime. The peasants may not have known how many supported the new regime, but at least they would know that many had consented to using their conventions of measurement and travel.

### 4. Advertising also generates common knowledge. 

Would you buy a fax machine if you were the only person in the world who had one? Or sign up for a credit card that no one else has?

Probably not, because both products are basically useless without a sufficient mass of people using them. In the case of the fax machine, you'd have no one to exchange faxes with, and in the case of the credit card, no vendors would accept your Visa if you were the only person wielding it.

As you can see, in a sense, choosing which products to buy is often a coordination problem: you're more likely to buy something if other people buy too. Beyond the obvious examples already mentioned, this also applies to many "social" goods like beer or clothes or movies, where people tend to prefer popular brands.

This means that advertising for such goods can be seen as common knowledge generation, because advertisers want you to not only see the advertisement, but to know that many other people have seen it as well.

By far the best venue for such common knowledge generation through advertisements is the Super Bowl, the most popular regular program on network television. This is evidenced by data on Super Bowl advertisements: social goods like beer, cars and clothing dominate the scene, whereas "nonsocial" goods like batteries and motor oil are virtually absent.

What's more, advertisers seem to be willing to pay a hefty premium for advertising social goods.

Looking at data for television advertisements in general, it becomes apparent that advertisers for social goods pay a much higher cost per thousand people reached than advertisers for nonsocial goods.

They also reach larger audiences than nonsocial brands, because the ads tend to be aired on popular shows.

The reason? Such shows are more effective at generating common knowledge, because everyone knows that more people are watching.

> _"Ever since the Macintosh's introduction at the Super Bowl in 1984, it has been the premier showcase for the introduction of new products."_

### 5. Though weak links are better for communication, strong links are better for coordination. 

Next, let's turn our attention to _strong links_ and _weak links_ that exist between individuals in a group.

In simple terms, strong links are shared by close friends, whereas acquaintances share weak links. When you look at how a person's strong links and weak links form networks, the difference between the two becomes apparent.

A network of weak links grows very quickly as you add more tiers: if you were to count the acquaintances of your acquaintances, you'd already have a very large network. In fact it has been suggested that anyone in America can be connected to any other American through no more than six weak links.

On the other hand, the close friends of your close friends tend to be your close friends too, so a network of strong links grows much more slowly as more tiers are added.

The breadth of a weak link network means communication tends to be much faster than in networks of strong links, as knowledge has more scope to travel far and wide quickly.

Despite this, studies indicate that strong links are more important for coordinating action than weak links. For example, one study found that whether or not an individual volunteered for the 1964 Mississippi Freedom Summer project to register black voters depended strongly on whether they had a strong link to another participant, but weak links to other participants had no correlation.

This seeming contradiction is explained by the fact that strong links are better at generating common knowledge. If you and I are close friends, I probably know what your close friends know and vice versa, so common knowledge exists in our network. But I probably don't know what your acquaintances know, which is why weak links tend to be better for hearing about job opportunities, but strong links are better for coordinating joint actions.

### 6. The panopticon prison design shows how hindering common knowledge generation can be crucial for maintaining power. 

Another interesting concept related to common knowledge generation is the _panopticon_ prison design. It was created and vehemently lobbied for by eighteenth-century philosopher Jeremy Bentham.

In a panopticon, the prisoners' cells are arranged in a circle around a central guard "lodge." Though the panopticon's success as a prison design has been limited, it is still useful as an analogy for power.

A panopticon has three characteristics that place power in the hands of the guard:

First, visibility is _centralized_ : the guard can see all prisoners from her central lodge. The purpose here is mainly to save costs, as fewer guards are needed.

Second, visibility is _separated_ : the prisoners cannot see each other. This prevents communication and coordination among the prisoners, thereby lessening the likelihood of an orchestrated riot.

Third, visibility is _asymmetrical_ : the prisoners cannot see the guard thanks to her lodge's smoked glass windows and blinds. This saves costs because prisoners can never know when they are being watched, so they must behave as if they were being watched constantly. In effect they are surveilling themselves.

What's more, the asymmetry also helps prevent the generation of common knowledge, just as separation does. Without asymmetry, if the guard were dead or asleep, each prisoner could see it, and know that other prisoners could see it as well. This would create common knowledge about the situation, possibly leading to a riot.

It should be noted that Bentham also foresaw a ritual aspect to the panopticon: his original designs place a chapel above the guard's lodge. The idea was that prisoners could partake in religious services without moving from their cells. In this sense, the prisoners were not only the objects of surveillance, but also an audience for rituals of power and authority.

### 7. Both rational choice and cultural practices determine how coordination problems are solved. 

Have you ever felt that there are two sides to your mind: an irrational one that often makes poor decisions, and a rational one that scolds you for these decisions like a father would a son?

You're not alone. Ever since Aristotle, claims have been made that our thoughts divide neatly into rational and irrational.

But this distinction is not so clear-cut: studies of people who have suffered prefrontal brain damage show that not only do they become emotionally unresponsive, but also unable to make rational everyday decisions. Clearly the rational and emotional are connected.

Similarly, when examining coordination problems, which at first sight appear to be purely rational exercises related to real, tangible issues, we find that the common knowledge required to solve the problems actually depends on a host of intangible, culture-related issues.

Let's examine this idea in more detail through _game theory._ Consider the simple coordination problem of deciding which side of the road people should drive on.

According to game theory, there are two _equilibria_, meaning strategies that no rational individual will deviate from: either everyone drives on the left or everyone drives on the right. In both cases, traffic is relatively safe and no sane person attempts to switch sides.

But since both these equilibria are equally good, how does one decide between them?

In such cases, the answer typically lies in common knowledge: it has been communicated to everyone to drive on the right-hand side of the road, and everyone knows that everyone else knows this, so they adhere to that equilibrium.

And how do we communicate the information necessary to create common knowledge? Through all kinds of rituals and societal practices that enforce social unity: ceremonies, media events, and so forth. In this sense, our culture influences the rational choices we make to solve coordination problems.

### 8. Final summary 

The key message:

**To solve coordination problems, we need common knowledge: information that we know, and know that other people know, and so forth. The creation of this common knowledge can be seen as one of the purposes of cultural practices, rituals and media events like the Super Bowl.**

**Suggested further reading:** ** _Beautiful_** **_Game_** **_Theory_** **by Ignacio Palacios-Huerta**

_Beautiful_ _Game_ _Theory_ shows us how applicable economics is to our daily lives by looking at the fascinating world of professional soccer. By examining compelling statistics and studies on shoot-outs, referee calls, and ticket sales, _Beautiful_ _Game_ _Theory_ offers some interesting insights into the psychology of behavioral economics.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Michael Suk-Young Chwe

Michael Suk-Young Chwe is a professor of political science at University of California, Los Angeles, where he teaches game theory. He's also the author of _Jane Austen, Game Theorist_.

