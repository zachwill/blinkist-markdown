---
id: 58321c312b89ea0004096a4e
slug: raven-en
published_date: 2016-11-23T00:00:00.000+00:00
author: Tim Reiterman with John Jacobs
title: Raven
subtitle: The Untold Story of the Reverend Jim Jones and His People
main_color: 971E3A
text_color: 971E3A
---

# Raven

_The Untold Story of the Reverend Jim Jones and His People_

**Tim Reiterman with John Jacobs**

_Raven_ (1982) reveals the untold story of Jim Jones and his Peoples Temple in Jonestown, the site of a mass murder in which 917 people lost their lives on one fateful day in 1979. These blinks give you a closer look at the Jones, shedding light on how he rose to power and why his followers met such a horrific end.

---
### 1. What’s in it for me? Dive into the dark history of one of America’s most notorious religious cults. 

Our story begins with its ending already well-known: On November 18, 1978, some 917 people — mostly Americans — died in what many called a mass suicide, or even murder.

These individuals were members of the Peoples Temple, their bodies strewn across the settlement in Guyana, South America. Their leader was a man named Jim Jones.

The Reverend Jim Jones was a cult leader, a charming personality to his followers who nonetheless hid a dark side. These blinks delve into the character of this cult figure, exploring where he came from and how he rose to such power among not just the counterculture but also mainstream politics in the 1970s.

Importantly, you'll discover how one man's fanaticism could drive hundreds of adults and children to a horrific death, the largest mass killing in the US before the terrorist attacks of 9/11.

In these blinks, you'll learn

  * how Jones's early character showed he was no regular personality;

  * which schemes helped Jones grow his congregation; and

  * why hundreds of people followed Jones to death.

### 2. As a child, Jim Jones was both sympathetic and sadistic; he craved above all power and control. 

Jim Jones was born in Indiana and raised by mostly absent parents, who left their eccentric child to his own devices.

Since Jones's father was unemployed and alcoholic, his mother worked long hours to provide for the family and was absent most days from home.

Despite his mother's best intentions, the Jones household was a place neither warm nor caring; Jones later remarked that he lamented not having a close-knit family and was long plagued by feelings of being different and alone in the world.

To cope, Jones sought community, visiting neighborhood churches. He carried a Bible with him everywhere he went. He was also sympathetic toward others whom he saw as marginalized.

The family's house sat near the local railroad tracks, and many homeless people lived rough in the area. Jones would often give food to these needy individuals.

As Jones grew up, however, he also developed an overwhelming need to exert power and control over others.

He read obsessively about historical figures such as Mahatma Gandhi, Josef Stalin, Karl Marx and especially, Adolf Hitler. He pondered their strengths and weaknesses, and examined what led each man toward success or failure.

At one point, Jones converted a nearby barn loft into a church in which he would conduct services.

In a disturbing sign of his controlling nature, he also one evening decided to lock up two of his friends in this space overnight. On another occasion, Jones went hunting with his best friend, Don. While deep in the forest, Jones aimed his gun at Don and threatened to shoot him, unless he stopped walking.

Jones also showed his sadistic streak with animals; one time he removed the leg of a chicken and tried to attach it to a duck.

As we'll see in the next blink, Jones was also learning how to mask his darker nature with a persuasively charming personality.

### 3. Jones found his true calling in the church pulpit, where he crafted a charismatic public persona. 

As he grew up, Jones's behavior mixed acts of kindness with a deep capacity for cruelty and need for control.

When he turned 18, Jones married Marceline Baldwin, a woman who would remain his wife until the bitter end, despite years of suffering under his control.

As just one example of his dark nature, one day, as he was reading the newspaper, Jones told Marceline about a horrible car accident. As he read the article, Jones said one of Marceline's best friends was a victim. Marceline was overcome with grief; moments later, Jones admitted he was just kidding.

But his lie wasn't just a bad joke. Jones saw other people in Marceline's life as competition, robbing him of her undivided worship and attention. He manipulated his wife's emotions to see how much she loved her friend, as compared to how devoted she was to Jones.

Over time, Jones's desire for control would become more strategic.

By this time, Jones had become an atheist, but in 1952, he became curious about the Methodist church which Marceline attended. There the pastor gave sermons on how to end poverty and unemployment and why we should better care for the elderly, protect free speech, reform prisons and fight for racial equality.

These talks sparked something in Jones, as he'd been looking for a way to inspire and lead people to action. Now, he saw his true path.

That summer, Jones joined the Somerset Methodist Church in Indianapolis as a student pastor. He also spent hours attending local revival meetings and healing services to better learn the persuasive cadence and oratorical skills of evangelical preachers.

Cleverly, Jones would dig up private information on congregation members, which he would reference as part of his sermon, in an attempt to appear like a divine prophet.

The ruse worked. His congregation soon overflowed, with followers both faithful and passionate.

### 4. Jones’s message of racial and social equality in the late 1960s made him popular and influential. 

Jim Jones as a preacher was on a path to make sure he stood out from other faith leaders. He certainly was charismatic, but he also drew people to his congregation with a message of equality.

In 1956, Jones left the Methodist church, claiming church leaders threw him out because of his push for more racial integration. He then started his own church, the _Peoples Temple of the Disciples of Christ_.

To show he practiced what he preached, he and his wife became Indiana's first white couple to adopt a black child. He called his now multiracial family the Rainbow Family; he would later adopt a Korean child and a Native American child.

Many members of the Peoples Temple were drawn to Jones's message of diversity and equality, with the feeling that they were part of a larger social movement.

In 1965, Jones and his followers moved to California, where his ideas appealed to an even broader group of young, idealistic people attracted to his socio-political sermons.

These new members were often college-educated and politically aware, and they admired Jones's criticism of capitalism, his support for socialist politics and his call for equality in matters of class, race and sexuality.

These followers weren't disenfranchised or naive; confident in their identity, Jones's new acolytes didn't fit the stereotypical image of people easily brainwashed.

In the late 1960s, racial tensions in the United States were high, and racial integration was a hot topic. The Peoples Temple, with its diverse and integrated congregation, was uniquely appealing to many forward-thinking individuals. 

Jones also established his credentials as a humanitarian civic leader, running food programs and services for teens, ex-cons and senior citizens.

His power was spreading to the world of politics as well. As his congregation grew, they became an important voting bloc that in the 1970s helped elect San Francisco Mayor George Moscone and other liberal Democrats.

### 5. Jones used deception and sneaky schemes to lure more individuals to join his Peoples Temple. 

On its surface, getting younger people to become more politically active and promoting social equality seems admirable. Yet in Jones's case, his motives were murky at best.

Jones couldn't just let his message of equality work on its own. To grow his congregation and hold on to those he had already brought in, Jones resorted to manipulation and underhanded tricks.

By the mid-1970s, the Peoples Temple had grown from 150 followers to a congregation of some 3,000 members. Most people were working class; between 70 to 80 percent were black.

Jones called his members the "troops" of the Peoples Temple. Followers did everything from setting up chairs for meetings to collecting thousands of dollars in donations and _tithes_ — a rule from the Bible that says a tenth of a believer's income should go to the church.

Jones even had members gather information on other members, snooping around neighborhoods and digging through trash bins to see what members ate, how much they paid the electric company and even what prescription drugs they might be taking. Much of this snooping took place in poor black neighborhoods such as the Fillmore district in San Francisco or Watts, outside of Los Angeles.

While Jones had his base in San Francisco, he and his "troops" traveled up and down the West Coast, and even across the United States, to deliver sermons and establish new bases of believers.

The tricks in Jones's sermons became more elaborate. He even had members of his "inner circle," mostly white men from privileged backgrounds, use theatrical makeup and disguises during his talks.

At an event in Seattle one evening, a temple member dressed up as a disabled elderly lady in a wheelchair. After five minutes of being treated with Jones's "godly" gift, she was miraculously "cured" and able to walk again.

Jones was so powerful at this point that members were willing to sign over life savings, homes and other property to the Peoples Temple.

### 6. As Jones’s grip on his followers tightened, he convinced his cult to move the church to Guyana. 

Through organized deception and manipulation, Jones had become the all-powerful ruler of his small universe, and he had no intention of letting this power slip away.

To ensure his total control, Jones used some dark and cruel methods.

He worked to emasculate other men and weaken family bonds by claiming that everyone in his congregation was homosexual, except him. He also sexually exploited both men and women, and ordered his followers to speak favorably about his sexual prowess.

If he felt certain members were too independent and needed to be humbled, Jones would have them sign false confessions saying that they had sexually molested their children — binding them ever closer to the "protection" of the temple.

Public beatings were also a common form of discipline. Jones conditioned his followers to accept the idea of imminent death by routinely staging suicide rehearsals and mock sieges.

There were, however, a few people who broke free from Jones's spell. In 1977, the author met with some defectors from the temple. As a journalist, he was ready to expose many of Jones's dark secrets.

Yet before he could, Jones assembled his congregation and moved the church to the South American nation of Guyana. Clearly, he feared that a revealing exposé would threaten his tight hold over the cult.

Since Jones had manipulated members to become entirely dependent upon him alone, it wasn't difficult to convince his congregation to sever ties with the outside world and relocate to South America. While some members brought their families along, others simply left, leaving no word to loved ones.

They were told that a new utopia awaited them. The Peoples Temple in Guyana, in "Jonestown," would be a harmonious, communal outpost, free from discrimination.

In reality, the members lived in overcrowded barracks and had little food.

Temple members slaved to keep the settlement livable. Those who stepped out of line faced cruel and unusual punishments, such as spending time in a sensory deprivation box.

### 7. An official visit inspired Jones to call for “revolutionary suicide,” committing his followers to death. 

California Congressman Leo Ryan flew to Jonestown on November 14, 1978, after hearing some concerns from constituents who had friends or family members in Jones's church.

Ryan and his team were received warmly by church members when they arrived. At first glance, nothing seemed to be amiss in Jonestown.

But then one of Ryan's men received a note on the sly from two church members that said, "Dear Congressman…Please help us get out of Jonestown."

The revelation set off a chain reaction, as additional scores of followers also asked to leave. All the while Jones remained cool, saying that anyone who wanted to leave with the congressman was free to do so.

Having collected a small group of defectors, Ryan returned to the Port Kaituma airstrip with his team and prepared to leave. Yet just as they started out, armed church members opened fire on the group.

Congressman Ryan was shot and killed, as were three journalists and one defector. Others were wounded, including another journalist and the author, Tim Reiterman.

Jones knew that word of Ryan's death would soon become public, and with that, the Peoples Temple would come to an end. But Jones was determined to end things on his terms.

He gathered all his followers together and gave a speech, warning them that his enemies would soon arrive to avenge Ryan's murder and destroy their paradise.

To protest this injustice and all the world's inequities, Jones told his congregation it was time for the death for which they had long prepared. Their "revolutionary suicide" would be facilitated by Kool-Aid laced with cyanide. This fruit drink, now deadly, was distributed to everyone at the compound.

Some 917 people died in Jonestown on November 18, 1978 — including about 300 children.

Although the event is considered a mass suicide, the author calls it murder. Many people, especially children, were poisoned by injection; there was little chance of escape, as the compound was surrounded by armed guards.

Known later as the Jonestown Massacre, this horrific event marked the end of Jim Jones (who also died in the event) and his Peoples Temple.

### 8. Final summary 

The key message in this book:

**Jim Jones, a manipulative, charismatic preacher, attracted hundreds of followers with a promise of social equality and then controlled them with threats and psychological games. His megalomania ultimately led to the death of over 900 people. This tragic event needs not to be forgotten, as each generation has its own charming yet dangerous fanatic personality.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The True Believer_** **by Eric Hoffer**

_The True Believer_ (1951), published in the aftermath of World War II, is an exploration of mass movements and the means by which they attract followers. These blinks will take you on a walk through history — showing how, under certain circumstances, be they right or wrong, anyone can become a _true believer_.
---

### Tim Reiterman with John Jacobs

Tim Reiterman is an award-winning journalist who spent over a year at Jonestown as a reporter for the _San Francisco Examiner_. He was present when a US Congressman, three reporters and a defector from the Peoples Temple were killed before the group's mass suicide. He also worked as a writer and editor for the _Los Angeles Times_ during its prize-winning coverage of the Los Angeles riots. He is a teacher at the UC Berkeley Graduate School of Journalism.

