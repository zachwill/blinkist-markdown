---
id: 58f4abd01507cf00046b9987
slug: raw-deal-en
published_date: 2017-04-21T00:00:00.000+00:00
author: Steven Hill
title: Raw Deal
subtitle: How the "Uber Economy" and Runaway Capitalism are Screwing American Workers
main_color: FC3237
text_color: C9282C
---

# Raw Deal

_How the "Uber Economy" and Runaway Capitalism are Screwing American Workers_

**Steven Hill**

_Raw Deal_ (2015) reveals the ugly truth behind the new sharing economy and the harm that companies like Uber or Airbnb are inflicting upon societies around the world. There's a major crisis on the horizon, and it will affect not only these companies' exploited employees. We're all at risk, and we'll need to choose our next steps wisely to prevent an economic collapse.

---
### 1. What’s in it for me? Discover why companies like Airbnb and Uber are setting the stage for an economic disaster. 

Whether it's booking a room for your vacation or finding a ride home after a night out, our smartphones and the internet have made many tasks easier than ever. Rather than visiting travel agents or waiting to hail a taxi, we only need a few clicks or taps and can be on our way.

Many of the companies that are thriving today — such as Airbnb or Uber — are part of the "sharing economy." Companies in the sharing economy don't really do very much; instead, they prefer to connect buyers directly with sellers.

For example, Airbnb helps those looking for a room get in touch with those who have one to spare. It sounds like an ideal and egalitarian system — but it isn't. There is a dangerous side effect to the sharing economy, one that, if left unchallenged, could bring down the entire US economy.

These blinks explain why the economy is facing oblivion and how we can hope to fix it before it's too late.

In these blinks, you'll also discover

  * why Uber doesn't "employ" its drivers;

  * why manual laborers shouldn't fear robots; and

  * why cheap window cleaners could contribute to economic collapse in the United States.

### 2. The sharing economy promises freedom but doesn’t deliver. 

You may not realize it, but every time you use Airbnb or Uber, you're taking part in a new economy, known as the _peer-to-peer_ or _sharing economy_. While it might sound like an exciting new development, many are raising important questions about how good this new business model really is for the economy and society as a whole.

Those in favor of the sharing economy call it revolutionary, and believe it could create a kinder, gentler strain of capitalism. And since it removes the middleman and cuts through the red tape of government regulations, supporters see it as a path to greater freedom in the marketplace.

Let's take a closer look at Airbnb. While it has by now grown into an enormous multinational corporation, it was first seen as an exciting new business venture that could disrupt the market by offering an alternative to overpriced hotels.

It even received praise from across the political spectrum, with liberals seeing it as an example of self-sufficiency and sustainable practices, while conservatives supported its decentralized business model.

But as we can see now, neither is true. The benefits of a sharing economy are mixed at best, and what we're left with is raw capitalism and impersonal, faceless transactions.

Airbnb might have begun with the idea of letting people make some extra money by sharing a spare room, but the company's greed has since destroyed that concept.

Airbnb is now flooded with professional renters, and landlords are evicting regular tenants in order to make more money through temporary rentals. Even though Airbnb is aware of this, they obscure this fact by putting forward a wholesome image of sharing.

They also disregard laws that try to protect tenants.

Many cities prohibit rentals for less than 30 days so that tenants have some security. But Airbnb dodges these regulations by positioning itself merely as a booking agent that can't be held responsible for how anyone might use their services illegally. Airbnb has also avoided the responsibility of paying hotel taxes in a similar fashion.

But as we'll see in the blinks ahead, there are many more serious problems with the sharing economy.

> _"That's not 'sharing.' It's just raw, naked capitalism."_

### 3. By defying rules and regulations, companies put clients at risk and create unhappy employees. 

Airbnb isn't alone in abusing the sharing economy and bending laws to maximize profits.

Uber is another great example of a company that is ruthless in its search for loopholes in laws designed to protect clients and employees.

This is why, in an effort to avoid paying social security contributions, Uber calls its drivers "contractors" instead of employees. Uber also has very lax security protocols, which has allowed known criminals to register and become drivers — sadly there have been a number of shocking rapes and assaults committed by Uber drivers.

But much like Airbnb, Uber avoids responsibility by claiming it isn't a taxi company at all; rather, it positions itself as a tech firm that simply connects customers with drivers. Therefore, it can't be held responsible for whatever the drivers might do or any danger their customers might find themselves in. This also allows Uber to avoid the taxes and license fees that traditional taxi companies must pay.

All of this means Uber can undercut the costs of other companies while costing states and cities huge amounts of revenue. And while there are ways to force companies like Uber to fall in line with existing laws, there needs to be support from politicians and the public.

But to make matters more complicated, Uber's drivers often try to improve their wages by only working in areas and at times where rates and demands are at their highest. This is known as "surge driving," and while it results in Uber paying their drivers more, it also leaves a lot of areas with little or no access to the service.

Since the public doesn't understand why the drivers are resorting to surge driving, they blame them for Uber's problems and not the company itself. And since politicians have yet to see Uber's abuse of existing legal loopholes as a priority, regulations continue to be weak and ineffective.

As a result, we're left with crooked companies and exploited workers.

### 4. Businesses in the sharing economy save money by firing employees and hiring unprotected contractors. 

If you're a permanent employee at a company or organization, you can count your lucky stars that you have a stable job with steady income and perhaps even benefits. After all, today's job market is becoming increasingly reliant on independent contractors, which is not a good thing.

There's a name for this growing trend in the United States: the _1099-economy_, since these contractors fill out a 1099-MISC form at tax time, rather than the standard W-2 form.

But more importantly, this growing community are all paid by the hour and receive no health benefits, retirement plan or social security. As a result, businesses can save money by outsourcing their work to these freelancers.

Some companies have even fired their employees in order to rehire them as contractors with a wage reduction and no benefits, which is what happened at the LGBT magazine _Out_.

Other companies unevenly split their workforce between a few well-paid employees with benefits and a majority of freelancers. Google does this, and the practice even extends to its bus drivers, who are contracted to commute employees to and from their offices.

While this is a popular way of doing business around the world, it's at its worst in the United States.

Other countries have put laws in place to protect contractors and temp workers. In Germany, for instance, they are protected by the same wage laws as regular workers, while in Brazil, companies are required to hire a temp as a regular employee after three months of work.

None of these laws apply in the United States, which is why you can easily find "perma-temps" who have worked for years at the same company, but always with the status of a temp and always earning less than other employees.

These workers can often feel trapped in this situation; they have a desk and an e-mail address just like a regular employee, but they have neither the insurance and security of their coworkers, nor the freedom of other freelancers.

This arrangement isn't good for anyone, since it undermines the middle class, who need secure and well-paid jobs to continue being the consumers that create a strong economy.

When secure jobs disappear, so does a secure economy.

### 5. A sharing economy leaves workers fighting for scraps and resorting to illegal labor. 

Have you heard about TaskRabbit? It's another great example of the dangers inherent in the sharing economy: if you need someone to mow your lawn, you can post the job on TaskRabbit and give it to the person who makes the cheapest offer.

Some might say this is a good way to provide work to the unemployed, but a more truthful evaluation is that it's exploiting those in need.

This new economy often gets sugarcoated with language like "sharing" or "peer-to-peer employment," but a more accurate description would be a "share-the-crumbs-economy."

Services like TaskRabbit wouldn't work without a depressed labor market in which a lot of people are willing to do just about anything for a little bit of money. It's only under poor economic conditions that people would offer and accept cheap labor on an auction site.

Ultimately, when you auction off your labor and compete with people around the world for meager jobs, you're going to end up getting underpaid.

Even when a local job like cleaning windows is up for grabs, workers will underbid each other to win a chance to get whatever crumbs they can. On the off chance you find a decent-paying job, it still doesn't factor in expenses, and when you consider the time and money spent commuting and searching, the job will always be worth much less than advertised.

The nature of these jobs adds to what is known as the "informal economy," which is untaxed, uncontrolled by government and often illegal.

Whether it's a nanny that is getting paid under the table or black-market and criminal activities, they all contribute to this underground economy.

These jobs are not only untaxed and precarious — if the employee gets ripped off, there's no recourse for them to seek justice, leading to a greater chance of exploitation.

In 2012, underground economic activity in the United States totaled roughly $2 trillion. This is double what it was in 2009 and equal to 13 percent of the country's overall gross domestic product.

### 6. Automation isn’t as harmful to blue-collar jobs as the threat of deregulation. 

It's often believed that machines and robots are putting blue-collar jobs at risk. But these aren't the jobs that are in real danger — it's those with high-skilled jobs that should be most concerned.

Automation is expensive, so it makes sense to have it perform the tasks that cost companies the most, which are middle- to high-skilled jobs. This is why there are robotic algorithms now scanning law documents to find important concepts, as well as sorting medications and even composing music.

In the end, this may actually start to bring jobs back to the United States that were previously being outsourced — only now, they will be done by machines. For instance, the interpretation of MRI results was a common job to outsource to India, but it may soon be cheaper for local automated services to perform.

A more dangerous threat to blue-collar workers is the deterioration of labor regulations.

It's no secret that labor unions have gradually been losing their power, but the new unions that are springing up are threatening, rather than strengthening, labor rights.

One such organization is the Freelancers Union, which is mainly active in New York. While this union does work to open health clinics for freelancers, its overall vision is to go back to pre-1930s conditions. As stated by founder Sara Horowitz, they hope to get rid of the government and state regulations protecting employee rights that were set by the New Deal, and instead let the private sector set the rules.

However, this goes against the interests of freelancers and all employees in general.

History has clearly shown that without government regulations, there are no secure labor rights, and salaries and benefits are guaranteed to plummet. And with freelancers already underemployed and underpaid, this would only harm them further.

### 7. Declining wages and a lack of job security are pushing the US economy to the brink of collapse. 

With all these developments, the traditional model of the US economy is taking one crushing blow after another.

What people fail to understand is that the US economy performs best when its workers are happy.

When employees get paid a decent wage, it allows them to buy the products that others in their society produce. And it's this demand for products that keeps the economy going.

Henry Ford understood this system back in the early 1900s, which is why he made sure his employees made enough money to buy the cars they were manufacturing. He knew that a prosperous American worker was crucial to a strong economy.

It was this well-functioning consumer society that made the United States the dominant economic power throughout the twentieth century. And when the economy took a downturn in the 1930s, the government did what it could to help: it increased government benefits for employees and created jobs for the unemployed so that American goods continued to be bought and sold.

This is why, by lowering the workers' wages, the sharing economy is endangering the very fabric of the consumer system.

Though the United States continues to produce goods and services that are worth $16.8 trillion, lower pay and job insecurity means that fewer and fewer people can actually afford to buy them.

This creates an imbalance, with a surplus of production on one hand and a serious lack of demand on the other; if this gets bad enough, the economy could soon implode. The author has a name for this eventuality: the _economic singularity._

To understand the effects of this, you can look at the economic depression that Japan has been going through for over a decade now. But it is avoidable. If the United States were to take steps similar to those Germany has taken and create policies that protect employees and freelancers, it could maintain consumer demand and keep the economy healthy.

In the final blink, we'll take a closer look at the choices the United States can make to prevent an economic singularity from taking place.

### 8. We need a new social contract that protects both workers and businesses. 

The United States now stands at a crossroads: in one direction there's the _new_ New Deal, which values the employee and protects their prosperity, and in the other direction there's the _Raw Deal_, which follows the sharing economy and its inevitable decline.

If we want to prevent another economic depression, we need to agree on some fundamental issues.

First, we have to put an end to working with quasi-independent contractors, a system that blurs the line between freelancers and employees.

There is really no reason employers shouldn't be willing to pay social security for contractors. It would cost less than $2.00 per hour for each worker if the employer took advantage of the subsidies currently offered by Obamacare.

Now, some employers might think this is still too much — but compared to the long-term consequences for the economy, it's well worth it.

Companies like Wal-Mart get away with grossly underpaying their staff because their employees receive extra help from the government. But this adds up to an annual bill of $6.2 billion that US taxpayers have to cover, and if we continue going in this direction, the amount will only increase.

Although things are looking dire, it's not too late. The US economy can be saved, and there are places around the world that can be a source of inspiration.

European countries like Germany, the Netherlands and Sweden have implemented policies that help both employers and employees.

Instead of cutting jobs, when a business is facing tough times, all of its employees scale back their hours and wages to reduce costs until things turn around. This is the kind of practice US business should consider before resorting to layoffs and making a select few work harder.

If people need to feel secure enough to spend money and keep the economy afloat, they can't worry that they'll be out of a job the moment things get tough.

If businesses can once again recognize the importance of job security and offer a reasonable living wage, there is still hope that we can put the economy back on the right track — and everyone will win as a result.

### 9. Final summary 

The key message in this book:

**The sharing economy has been hyped as the new path to prosperity. But this is a false promise that advances a dangerous business model that not only endangers the livelihoods of workers and customers but the entire existing socioeconomic structure as well. This is why it's time for some important regulatory changes.**

**Actionable Advice**

**Forget about the sharing economy and go for the** ** _solidarity economy_** **.**

If you're interested in what's good for society in the long run, look to the solidarity economy. Platforms like Couchsurfing or Yerdle have created giant online flea markets where users never have to pay with actual money. They also have shops where you can borrow physical items instead of buying them yourself.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading: The Inevitable** **by Kevin Kelly**

_The Inevitable_ (2016) is your guide to understanding the technology trends that are gaining momentum today and will undoubtedly shape the future. These blinks delve into the ideas and motivations that are driving technology and what it all means for the world of tomorrow.
---

### Steven Hill

Steven Hill is a senior fellow at the New America Foundation. His writing has appeared in the _New York Times_, the _Guardian_ and the _Huffington Post_, and his best-selling books include the widely acclaimed _Europe's Promise_.

