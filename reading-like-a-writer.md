---
id: 577548db3588860003e82440
slug: reading-like-a-writer-en
published_date: 2016-07-07T00:00:00.000+00:00
author: Francine Prose
title: Reading Like a Writer
subtitle: A Guide for People Who Love Books and for Those Who Want to Write Them
main_color: 8B264B
text_color: 8B264B
---

# Reading Like a Writer

_A Guide for People Who Love Books and for Those Who Want to Write Them_

**Francine Prose**

_Reading Like a Writer_ (2006) shows us how to read literary masterpieces with open eyes. These blinks explain the patterns of writing that make fiction memorable, powerful and authentic, helping us slow down our reading and find more enjoyable experiences in every book.

---
### 1. What’s in it for me? Become a better reader and writer. 

What does it take to write a superb novel — or any novel at all? Do you need to attend a creative writing course or closely follow the formula outlined in a writing manual? Not necessarily.

These blinks will introduce you to another approach, and an immensely rewarding one if you happen to love literature: why not let great writers teach you?

This isn't just about reading masterful novels and hoping that their mastery will somehow rub off on you; rather, it's about close, inquisitive reading. As you'll discover, artful writers take great care to tend to even the tiniest details in a novel, like the choice of a single word. Once you take on their perspective, wondering why exactly they chose this word, opted for that kind of narrator or arranged a sentence quite the way they did, you've already start thinking like a true writer.

In these blinks, you'll also find out

  * how to create an irresistible character;

  * how to become a great liar; and

  * why bad writing is like asthma.

### 2. Rediscover the art of close reading to get inside a great writer’s head. 

Have you ever watched a child read? Many mouth every word they read to themselves silently. They're incredibly thorough, and make much better close readers than we adults. Why?

Well, as we grow older, we also grow impatient. We're able to read faster and tend to read more superficially. This leads to skim-reading, when we're always on the hunt for quick info, a surprise or a laugh. We devour words like junk food!

Skim-reading certainly has its merits in certain situations where efficiency is the goal. But when it comes to reading literature, skimming is simply counterproductive. It causes us to miss the most important things, like a word that evokes powerful associations, or a subtext that shimmers between the lines.

Great literature, after all, has many layers of meaning. No matter how deep you dig, you'll always discover more. By slowing down your reading, you'll make your experience of the text far more rewarding. Paying closer attention to words will also bring you closer to the great masters who wrote them, allowing you to learn from them and improve your own writing in turn.

So how exactly do you relearn the habit of taking it slowly when reading? It can prove rather difficult without the right incentive. But, sometimes, all it takes is one clue to keep your eyes glued to the page.

The author recalls how her English teacher suggested she pay close attention to motifs of seeing and blindness while reading Sophocles' Greek tragedy _Oedipus Rex_ and Shakespeare's _King Lear._ Soon enough, she discovered that both texts were filled with curious patterns, parallels and connections, as if the two authors had hidden them there for her to uncover. Whole new dimensions of meaning emerged in the texts, changing her reading experience entirely.

By taking a closer look at certain patterns of literary texts, you'll learn to slow your reading down and discover what every sentence really has to offer. But how do you know which pattern to follow? We'll have a look in the next blink.

### 3. Word choice plays a powerful role in plot and character development. 

According to the _Oxford English Dictionary_, there are currently 170,000 words in use in the English language. A writer certainly has a lot of choice! But she is also faced with the challenge of finding that one perfect word among 170,000 that expresses exactly what she wants to say.

As a result, every word in every great work of fiction is there as the result of meticulous decision making. To better grasp the genius of a writer, consider what word they could have used instead, why they didn't and what effect their chosen word has. First sentences of novels make great illustrations of this. Consider the opening of American writer Flannery O'Connor's short story, _A Good Man Is Hard To Find_ : "The grandmother didn't want to go to Florida."

Even the very first two words, "the grandmother," are worthy of deeper investigation, specifically as a solution to the problem of how to introduce a character. O'Connor didn't introduce her as "Rosie" or "Mrs. Thompson" or even "an old lady." She remains nameless, labelled only by her position in the family. From the beginning, it's indicated that this is a tale of family affairs, but not a sappy, sentimental one. The unnamed grandmother becomes a symbol for all grandmothers, and much more — but you can read the book to find out!

Word choice is just as important when it comes to a character's speech. Think about your loved ones, and the particular way they speak. Our backgrounds, experiences, attitudes and emotions are all reflected in the words we choose to express ourselves in real life. An author has to capture this as he dreams up his own characters on the page. Sometimes even the use of incorrect words can be a perfect choice for certain characters.

Take the first sentence of James Joyce's _The Dead_, for example. We're told that "Lily, the caretaker's daughter, was literally run off her feet." The word "literally" isn't correct in this context — but it's something that the character Lily would say herself. Through this one word, it's hinted that we're looking at the world from _her_ perspective, which in turn is shaped by her social environment and background.

### 4. Beautiful sentences are clean, clear, musical and rhythmic. 

What makes a sentence beautiful? That's no easy question to answer. Normally, we just know one when we see one. Though there are no hard and fast rules for fantastic phrasing, we can investigate examples to find some common features. Some of the most stunning sentences are also the simplest, using only the words they need to communicate powerfully with the reader.

German writer Heinrich von Kleist was a master of clear, clean sentences and the opening to his story _The Earthquake in Chile_ provides us with a great example: "In Santiago, the capital of the kingdom of Chile, at the very moment of the great earthquake of 1647 in which many thousands of lives were lost, a young Spaniard by the name of Jéronimo Rugera, who had been locked up on a criminal charge, was standing against a prison pillar, about to hang himself."

Note that despite the amount of information packed into this sentence, it's not overcrowded. And it makes us hungry for more, opening up many questions for the reader: Why is this young man locked up? Why is he suicidal? And what's the situation with this catastrophic earthquake?

As well as being clear, great sentences have their own music and rhythm. Joyce's writing illustrates this strikingly. Consider the last sentence from the short story _The Dead_, where a character leaves a party in Dublin and heads to a snowy graveyard where one of his relatives is buried:

"His soul swooned slowly as he heard the snow falling faintly through the universe and faintly falling, like the descent of their last end, upon all the living and the dead."

Through the repetition of words beginning with "s" and the word "faintly", as well as the half-rhymes that bind the words "descent," "end" and "dead" together, this sentence falls as gently as the snow it describes.

### 5. Paragraphs allow the reader to breathe. 

Let's imagine you're a detective. And not just a detective with any old mission. Like the protagonist in American writer Rex Stout's story _Plot It Yourself_, your task is to figure out if three supposedly plagiarized manuscripts are written by the same person. Where would you look for clues? Word choice? Phrasing? Punctuation, maybe?

Well, the best clue would be found in none of those! The most revealing aspect of an individual's writing style is the way they _paragraph_. As the detective says: "A clever man might successfully disguise every element of his style but one — the paragraphing." Paragraphs, it seems, are more important than you'd first expect — they're a fantastic tool for creating emphasis. But how?

Well, in paragraphs, stress is placed on the first few and final few words. Because we're all different, with different ideas of what is worth emphasizing, paragraphs are highly individualized. There are, again, no hard and fast rules. But we can make use of a few guidelines to make the most of paragraph power.

Nobody likes to pick their way through exhaustingly long paragraphs. Help your reader focus by breaking these paragraphs into smaller, more digestible chunks — but not too short! A string of short paragraphs can also be quite distracting. So how do you get the balance right?

Well, it helps to see paragraphing as a kind of literary breathing. With every new paragraph, we're able to breathe in, slowly exhaling as we read and breathing out fully with the last word. This gives us time to reflect and keep a good rhythm. Short paragraphs might feel a little too asthmatic, while long ones can leave us breathless (and not in a poetic way!).

If that analogy doesn't work for you, paragraphs can also be imagined as shots in a film, which is how French novelist Stendhal would use them. As he introduces a small French town as the setting for his novel, _The Red and the Black_, he begins with a bird's eye view, and slowly zooms in a little more with each new paragraph. We see the town from above and then approach the busy main street crowded with people, before zooming in on a certain person — who becomes our protagonist!

### 6. The best narrators are the ones that intrigue you. 

Every story can be told from countless perspectives. But while stories from the perspective of an animal or a brick in the wall of a family home might make for interesting stylistic experiments, first- and third-person narration remain the most popular and flexible options.

These two narrative styles each has its own strengths and challenges. Once you've decided on your narrator, his attitude and a tone that reflects it, it's up to you to work through these lenses.

If you're writing through a first-person narrator, you have to get the reader intrigued by her personality. This doesn't necessarily mean that your narrator has to be likable — but she's definitely got to be interesting enough for your reader to follow her story over the course of the text.

Take Vladimir Nabokov's character Humbert Humbert, the narrator of his notorious work _Lolita._ Humbert is sexually obsessed with a young girl, and someone you probably wouldn't want to hang around with in real life.

But as readers, Nabokov ensures we stick around. Who couldn't help being enticed by delirious mutterings like these: "Lolita, light of my life, fire of my loins. . . . Lo-lee-ta: the tip of the tongue taking a trip of three steps down the palate to tap, at three, on the teeth. Lo. Lee. Ta." We might not like or trust Humbert, but we do want to know more about how his strange mind works.

At the other end of the spectrum, we have the third-person narrator, a perspective that sees and knows all. As this narrator is a background figure, a fly on the wall who shines a spotlight on characters, her voice can be detached, objective and unobtrusive. But sometimes, even a third-person narrator hints at a curious personality.

Consider American writer and playwright Jane Bowles' novel _Two Serious Ladies_. The strange blend of formal and childish tones used by the narrator gives the impression of a gifted yet slightly kooky child behind the story: "Christina . . . took off her shoes and stockings and remained in a short white underslip. This was not a very pleasant _sight to behold_, because Christina at this time was very heavy and her legs were _quite fat_." Unless you've met a child who wrinkles her nose and complains of "an unpleasant sight to behold," this narrative voice will prove quite intriguing indeed.

### 7. Action, thoughts and dialogue reveal a character’s unique personality to the reader. 

In literature as in life, there are three key things that will define you: what you do, what you think and what you say. To these, some might also add how you look. But, to be honest, bookworms like us don't care much for superficiality!

While the ability to create living, breathing and unforgettable characters is common to all great writers, the path they take to create these characters will always differ. Some writers use a character's actions to define his personality. Heinrich von Kleist is one of these types; your understanding of his characters is gained by observing what they _do_, such as how they react to shameless injustice, uncomfortable proposals or even life-threatening earthquakes. The inner thoughts of the character are rarely explicitly expressed.

Jane Austen, in contrast, brings her characters to life by sharing their most private concerns and musings with us. Whole paragraphs share a character's ruminations on her own wondrous generosity, which a reader can easily see through as the thoughts of a complete narcissist.

We also get to know characters better through the way they talk to each other. Brilliant dialogue can reveal a character's fears, his passions or even his hidden agendas, through what he says and how he says it. Let's take a look at another scene from Bowles' _Two Serious Ladies_. After a party, protagonist Miss Goering is invited over by young Arnold, just as his conservative and imposing father steps in:

"Well, lady", he said to her, "are you an artist too?"

"No," said Miss Goering. "I wanted to be a religious leader when I was young and now I just reside in my house and try not to be too unhappy. I have a friend living with me, which makes it easier."

This strangely terse yet unfiltered summary of her own life gives the reader a profound sense of just how confused and desperate Miss Goering's existence is. A few words are enough.

> _"In the particular is contained the universal."_ \- Samuel Beckett

### 8. Details are what makes characters and stories come to life. 

Do you consider yourself a good liar or a bad one? If you aren't sure, here's a quick way to spot the difference: while bad liars scramble to stack up facts and arguments to give themselves greater credibility, a smart liar knows that what's really going to keep their tall tales unnoticed is a _realistic detail_.

Great writers are, in many ways, not so different from great liars. Both make up stories that are untrue yet convincing, and both know how to make themselves convincing by using a few specific, realistic details. Simply adding a trivial, yet very particular aspect of your story will make listeners feel that you simply must be telling the truth — otherwise, why would you think to mention something like that?

A detail like this could be, for instance, the brand of a car. In _Access to the Children_, a story by William Trevor, the protagonist is "a fair, tallish man in a green tweed suit that required pressing," and a divorcee with the right to visit his children one day a week. What kind of car does he drive? Perhaps you won't be surprised: a ten-year-old Volvo!

Another kind of detail that makes your tale more authentic refers to physical gestures. Like speech, body language can reveal a character's innermost thoughts and feelings, and can even become a trait that makes him stand out from everyone else.

In a passage in Russian novelist Ivan Turgenev's _First Love_, a beautiful young woman catches the narrator's eye and he is about to fall in love with her. Four other men already have plans to woo her, so how does she respond? By tapping each one of them on the forehead with a bunch of delicate gray flowers. This bizarre yet charming gesture serves to highlight this young woman's uniqueness.

Keep an eye out for little details like these in the books you read. They will give you an insight into how authors create an imaginary world that feels real enough for you to get lost in, with characters that are as unique as the people in your life. Nothing could be more important for both readers and writers!

### 9. Final summary 

The key message in this book:

**Next time you open up a novel, slow down! Savor every single word in order to gain insight into the worlds of meaning hidden behind them. By paying close attention to patterns and details in speech, dialogue, narration and even in the phrasing and word choice of individual sentences, you'll be able to see a novel from the perspective of the literary talent that wrote it.**

**Actionable Advice:**

**Write something your mother wouldn't like.**

Lots of worries and restraints can keep us from writing freely. "What if my novel is ridiculed by critics?" you may wonder, or "What if my mother doesn't like it?" The truth is, to write good literature, you have to stop worrying about what other people might think. In their time, many great works of art were seen as frivolous or strange. So, the next time you don't dare to write down your ideas, try to write something your mother wouldn't like; this will build your courage and free you to find your voice.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _How to Read a Book_** **by Mortimer J. Adler and Charles van Doren**

Since _How to Read a Book_ was first published in 1940, the blank sheet of paper that faces you when you start an essay or report has been replaced by the blinking cursor in a blank Word document. No matter: this classic bestseller, revised in 1972, is still a great guide to tackling a long reading list, extracting all the relevant information and organizing your own conclusions. Be the boss of books with this effective approach to reading and understanding texts of all kinds.
---

### Francine Prose

Francine Prose is a best-selling author and essay writer. She's also won several literary prizes and was a finalist for the National Book Award in 2000 for her novel, _Blue Angel_.

