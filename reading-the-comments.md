---
id: 57b2e333381bcc00031e0626
slug: reading-the-comments-en
published_date: 2016-08-19T00:00:00.000+00:00
author: Joseph M. Reagle Jr.
title: Reading the Comments
subtitle: Likers, Haters and Manipulators at the Bottom of the Web
main_color: 40544B
text_color: 40544B
---

# Reading the Comments

_Likers, Haters and Manipulators at the Bottom of the Web_

**Joseph M. Reagle Jr.**

_Reading the Comments_ (2015) delves into the social phenomenon of online commentary. These blinks explore how online commenting became the force it is and examine commenting's positive and negative influence on communication at large. Importantly, these blinks encourage you to think about the implications of online comments for the modern internet user.

---
### 1. What’s in it for me? Better understand the often negative, damaging world of online commentary. 

The internet has given us many outlets for our opinions as sites offer channels to post and share comments. Today everyone has a voice, and we can let people know what we think immediately, nearly anywhere in the world — an amazing development.

Or is it? While there are many benefits to online commentary, the freedom a user enjoys online comes with a dark side, too. These blinks explore the advantages _and_ disadvantages of this new way of communicating.

In these blinks, you'll discover

  * why the sandwich technique is the tastiest way to give feedback;

  * why a Linux user board is meaner than a fan fiction review board; and

  * why people prefer their Facebook profile to a mirror.

### 2. People love offering commentary and the internet has given the opinionated free rein. 

Thanks to the internet, online commentary is booming. Users can convey their opinions in many different ways, from awarding stars to writing in-depth reviews.

But our passion for commenting wasn't born online. It has precedent in history.

Take the Michelin guide. Established in the early twentieth century, the guide not only helped drivers navigate highways safely, but was also the first to offer a star-based rating system for hotels, gas stations and other services along the way.

Consider, too, printed book reviews. As unprecedented numbers of books and articles were published following the Enlightenment period, periodicals such as _London's Monthly Review_ and the _New York Times Book Review_ cropped up to assist readers in deciding which books they should be reading.

While experts penned these early guides and reviews, other review platforms that allow anybody to share a thought or opinion have been around for decades.

The _Zagat Survey_, for example, leveraged the wisdom of the crowd to rate the quality of restaurants, hotels and even music. Founded in 1979, Zagat reviews were a hit long before Yelp!

When you share your opinion about another person's work, however, you also offer a form of commentary: it's called giving _feedback_. We'll explore the implications of this sort of commentary in the online world in the next blink.

> _"We live invested in an electronic information environment that is quite imperceptible to us as water is to fish."_ _–Marshal McLuhan, media theorist_

### 3. Many online communities are based on commentary in the form of positive or negative feedback. 

Feedback is a form of commentary marked by the intention to _help._ Feedback can be challenging to deliver, as different audiences require different approaches. But once you've found the right balance in your feedback, you'll have found a beneficial way of offering commentary.

You're no doubt familiar with the concept of _constructive feedback_. Effective, constructive feedback encourages individuals to keep chasing a goal, boosts self-esteem and offers actionable recommendations on improving performance. This is in contrast to feedback which simply criticizes, condemns or complains.

A simple way to ensure that feedback remains constructive is to use the _sandwich technique._ Begin by praising the work, then share your critique, then again offer praise.

Constructive feedback for an essay in "sandwich" form might sound like this: "The beginning really had me keen to know more. The second paragraph, however, was a little confusing. But the ending definitely grabbed me!"

Not everyone is looking for praise, though. While a novice might respond better to praise, an expert might prefer more negative feedback, in the form of pure critique.

Why would this be the case?

In general, beginners are looking for feedback to help them stay committed to a goal. Experts, on the other hand, are more concerned with finding ways to progress and improve.

This difference is reflected in the tone with which feedback is offered in online communities. Communities that bring together amateurs, such as fan fiction communities, usually trend toward offering friendly or positive feedback on work. Communities of experts, however, such as a group for Linux developers, are clearly geared toward offering concrete, often negative, feedback.

No matter which camp you find yourself in, there are a few rules of thumb for giving feedback that can help.

Educational psychologist Valerie Shute discovered two important things after studying over 100 articles about formative feedback. First, she found that feedback should focus on the task of the learner and not on the personality of the learner; and second, that specific, clear feedback is best communicated in writing rather than via conversation.

> "_Because there are few who can endure frank criticism without being stung by it, those who venture to criticize us perform a remarkable act of friendship."_ _–Michel de Montaigne, essayist_

### 4. Internet commentary is noisy, messy and sometimes difficult to interpret out of context. 

Although the internet has expanded the number of channels through which we can express ourselves, most channels are noisy.

They're noisy because they're self-organized and context-driven. As a result of this, online popularity and good quality posts don't always go hand in hand.

Consider social news site Slashdot, for example. Users on Slashdot can rate comments on posts from -1 to +5, the latter being the best score. Comments with the lowest ratings are considered poor and are made invisible.

This system often backfires, however, as most users tend to rate comments that have already been rated, disregarding other potentially valuable comments that might also deserve a rating.

Another difficulty with online commentary is that individuals use vastly different subjective scales when evaluating comments or events. This discrepancy makes what seems a simple review system far more difficult to interpret than you might think.

Some users might feel a product that works well deserves three stars, but no more. Other more lenient users might offer four or five stars, even if they aren't 100 percent satisfied with the product.

Also, online review systems force users to review a product's range of features with a single rating, which makes that single rating tough to decipher.

Finally, online commentary can be confusing for readers lacking the context for a post. In this case, it's often hard for a reader to discern the commenter's intention. Some users might post a note about a previous comment and assume that, since everyone has been following along, other readers will immediately "get" the reference.

If a comment is retweeted, reposted or forwarded without the necessary context, however, it's all too easy to get the wrong idea about what a person originally wanted to convey. In such a situation, this can even lead to other readers taking offense.

But it's not the users' fault that most online communication channels are so noisy. Often, channels are being manipulated to raise the level of noise. Find out how this is done in the next blink.

### 5. Buyer, beware! Online reviews for products or services are frequently manipulated. 

Online reviews are a solution to a real problem, allowing potential customers to learn about the quality of a certain product or service before spending any cash.

Good online reviews can be powerful, boost customer numbers and, in turn, increase revenue for a company. Because of this, online reviews are increasingly manipulated.

And who's surprised?

In one study, researchers observed 610,713 online reviews for 4,490 books. They discovered that the probability of seeing a positive review followed by a negative review was 72 percent. This was 2.6 times greater than the chance of seeing a negative review followed by another negative review.

The researchers concluded that 10.3 percent of online reviews were thus subject to manipulation.

Cornell University sociology professor Trevor Pinch found that companies, too, can be dishonest when it comes to product reviews. A seller of SLR cameras, for example, might copy a positive review for a competing camera, then post that same review for its own product!

And it doesn't stop there. Buying fake reviews has become an illicit market of its own.

In 2010, entrepreneur Jason Rutherford launched GettingBookReviews. His service allows companies to pay for positive reviews, sometimes exaggeratedly so, on major book sites, including on Amazon.com.

On community site Craigslist, you can find many company job postings asking for freelance help in writing positive product reviews for cash.

Some sites have already taken steps to crack down on manipulation, but fraudulent reviewers seem to keep getting smarter.

The CAPTCHA system has been long effective at keeping rating bots out of comment sections, but even this system can be hacked and must be updated frequently to ensure security.

> _"When a measure becomes a target, it ceases to be a good measure."_ _–Campbell's law_

### 6. Cyberbullies and trolls are legion and want nothing more than to cause trouble and incite arguments. 

Any user can choose to comment anonymously online. You have the freedom to write to whomever you want and however you want.

This anonymity can lead to communication that might never happen if the people in question were speaking face to face. Faceless people, sadly, are often tempted to behave immorally.

The ancient Greek philosopher Plato in his writings examined whether people would continue to behave morally if they could act anonymously. He illustrated this dilemma in the story of Gyges, a shepherd who finds a ring that makes him invisible. He eventually uses this ring to commit a series of immoral acts.

Psychologist Tatsuya Nogami in a recent study asked 100 university students to flip a coin twice and were told that they would receive a reward if they got two tails. The researchers did not observe the students as they flipped, however.

Although statistically, flipping a coin twice and getting two tails should only happen 25 percent of the time, a rather suspicious 46 percent of students reported that they had indeed flipped two tails.

Anonymity not only can make people greedy but it can also make people act cruelly.

Fantasy writer Foz Meadows defines bullying as an imbalance of power, in which stronger individuals undermine the opinions of and belittle a weaker person. Many people use online anonymity to engage in _cyberbullying_ — deliberately targeting the opinions and reputation of other online personalities.

Cyberbullying attacks are rife on Goodreads, an online platform where budding writers can share work. Many authors have been the targets of attacks, launched seemingly for fun. These authors might receive numerous negative reviews in a single day, as bullies team up to destroy their online reputation.

Then, of course, there are _trolls_ — users who enjoy posting inflammatory comments to both start fights and attract attention online. Linguist Susan Herring describes how trolls send messages that appear sincere but are designed to provoke a response that leads to a pointless argument.

> _"As online discussion grows longer, the probability of a comparison involving Nazis and Hitler approaches one" –Godwin's law, attributed to cyber lawyer Mike Godwin._

### 7. Competition for flawless selfies or “likes” can either help or harm a person’s self-esteem. 

Of course, you know what a "selfie" is. Referring to a picture taken of yourself and posted to social media, _selfie_ was the international word of the year in 2013.

One of the pleasures of having an online presence is the freedom to create a powerful self-image. The flipside, however, is that "selfie" culture has spawned a boom in narcissistic behavior.

Psychologists Amy Gonzalez and Jeffrey Hancock explored how looking in the mirror might compare to looking at a Facebook profile. They asked half of their participants to look at themselves in a mirror, and instructed the other half to visit their Facebook profile.

The researchers found that participants who looked in a mirror reported lower self-esteem, while those who looked at their Facebook profile reported higher self-esteem.

Why the difference?

Gonzalez and Hancock attributed Facebook's ego-boosting power to a user's ability to control or change his or her profile. Interestingly, participants who modified their profile during the experiment reported even higher self-esteem than participants who only looked at their profile.

Authors Jean Twenge and Keith Campbell believe that the internet has created a narcissism epidemic. It's not difficult to see their point, as most blogs seem to be nothing but exercises in vanity, while valuable, informative content remains buried under digital reams of self-promotion.

While posting flattering photos of yourself or receiving copious "likes" from friends might boost your self-esteem, those same posts and likes could be lowering your friends' self-esteem at the same time.

Psychologist Alex Jordan found that people often overestimate how happy other people are, as we simply cannot know what's going on behind closed doors. We base social comparisons, which deflate our self-esteem, on external appearances — an action all too easy when looking at shiny, happy photos of peers online.

People of course only share their best moments online, not the depressing or mundane ones. Celebrities in particular engage in fierce competition, spending huge amounts of money and effort on promoting a flawless, enviable social image.

Spend hours looking at these over-produced, airbrushed images and you're more than likely to feel unhappy about your life!

> _"If we only wanted to be happy it would be easy; but we want to be happier than other people, which is almost always difficult, since we think them happier than they are."_

### 8. Final summary 

The key message in this book:

**Online comments offer society a fascinating new mode of communication. Comments can produce both positive and negative results; good comments include constructive feedback and helpful product reviews, while bad comments run the gamut from manipulative advertising, to cyberbullying, to online narcissism. Regardless of quality or tenor, online commenting is a psychological and social phenomenon that deserves greater attention.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _This is Why We Can't Have Nice Things_** **by Whitney Phillips**

_This is Why We Can't Have Nice Things_ (2015) explores the subculture of trolling: where it came from, who does it, why they do it and what exactly it is they do. The book examines the blurred line between a malicious online attack and revealing social commentary, and shows how trolling and mainstream culture have come to form a close bond.
---

### Joseph M. Reagle Jr.

John Reagle is the author of the acclaimed title, _Good Faith Collaboration: The Culture of Wikipedia_. He is also an assistant professor of communication studies at Northeastern University.

