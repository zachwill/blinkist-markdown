---
id: 537230ab6264390007150000
slug: ready-for-anything-en
published_date: 2014-05-13T13:52:07.000+00:00
author: David Allen
title: Ready for Anything
subtitle: 52 Productivity Principles for Work and Life
main_color: FCC132
text_color: 7D6019
---

# Ready for Anything

_52 Productivity Principles for Work and Life_

**David Allen**

In _Ready_ _for_ _Anything_, productivity expert David Allen presents a variety of techniques that will help you clear your mind and your desk, deal with the things that hold you back, and prepare for all the creative, challenging and rewarding projects that will come your way. Additionally, the book serves as an introduction to the general principles of Allen's groundbreaking productivity system.

---
### 1. What’s in it for me? Learn how to make space in your life for creative and rewarding projects. 

Will you ever find the time and energy to work on the projects you love? Or will you have to resign yourself to the fact that the demands of everyday living will always get in the way of having a creative and fulfilling life?

Based on management consultant David Allen's popular productivity newsletter, _Ready_ _For_ _Anything_ covers a lot of ground, providing insight into the causes of many time- and productivity-management issues. While much of this is a gloss on Allen's best-selling _Getting_ _Things_ _Done_, _Ready_ _For_ _Anything_ also sets out to answer an important question: How can you make time and space in your life for creative, meaningful work?

The following blinks will offer a few answers to that question. In them, you'll find out:

  * why memorizing a shopping list can limit your creativity

  * why it's good advice to feed your dog before planning a major art gallery heist

  * why it's sometimes good to expect something bad to happen

  * why micro-managing your working life might actually be an obstacle to doing the work itself

  * and how it's possible for you to write a magnum opus, even though you only seem to have a few moments to yourself each day.

### 2. Having too many things on your mind will hamper your creativity. 

Unlike those tasks that don't demand much from your brain — like filling out forms, writing a shopping list or responding to a routine email — creative work requires lots of "mental horsepower."

Why is this?

First, creative work is a mental balancing act. At any one time, a creative act requires that you keep in mind — and evaluate — many different considerations simultaneously.

For example, when a screenwriter is in the process of writing a script, she has to reckon with many different things. She must come up with a convincing story and a compelling narrative. She will need to invent effective visual metaphors for the characters' emotional states — like, using heavy rainfall to symbolize sadness. She'll have to ensure that the script doesn't require too many different shooting locations, as that will increase production costs. And she'll also have to do a lot of background research, especially if her story is set in an ancient era, or something similar.

Balancing all of these factors simultaneously requires a lot of mental effort. Moreover, that effort depends on us having a kind of mental "storage space." However, for most of us, this space is at a premium.

All of the information that will be relevant in the short- to mid-term is stored in what we'll call a kind of _psychic_ _RAM_. The information that we store there ranges from the subtotal of a bill that you're calculating to the fact that you need to buy milk. Crucially, creative thought also depends on this _psychic_ _RAM_.

Unfortunately, though, the capacity of psychic RAM is limited. Attempting to keep in mind a lot of information at once will result in your RAM becoming too full to process any new ideas. Which means, of course, that your creativity will suffer.

If we cannot be creative while our heads are filled with mundane details, memos, facts and so on, then it follows that we must find a way to clear our minds of such information.

### 3. To maximize your creativity, don't keep all your promising ideas and plans in your head. 

As we've seen, storing too much information in your psychic RAM will hinder your creativity. But there are also other reasons to stop trying to store all your ideas in your head.

First, if you don't set down an idea when it comes to you, it may very well be unavailable when you need it.

This is because our psychic RAM is rather limited, so older information might be deleted to make space for new information. Indeed, the best idea you've ever had could be nudged out of your RAM by a newer, far more mundane one — like, "must buy milk."

Also, information stored in your RAM is badly organized, so you might find yourself unable to retrieve that great idea when you need it.

For instance, imagine you're preparing for your finals, and a fantastic business idea pops into your head. Once the exams are over, when you finally have time to consider the idea, you might not have access to it because it's become buried under all the information you needed for the exams.

But by _writing_ _down_ your ideas, you no longer depend on your psychic RAM, and those ideas will be far easier to retrieve when you need them.

Yet, there's more to writing ideas down than keeping them safe: writing also helps us to define and evaluate them. There's a vast difference between an idea and its real-world application. When you first have an idea — say, for a business — it's usually not very clear, but rather a jumble of vague thoughts and possible conclusions, like "save costs" or "interns."

The process of writing the idea down in complete sentences forces us to structure it more clearly and helps us to see any faulty conclusions we may have made. For example, the written statement "By hiring interns, we can save 20 percent on our costs" is far easier to evaluate than the embryonic idea that exists only in the mind.

### 4. Being aware of your job, goals and current tasks will enable you to make good decisions about the work to come. 

Imagine that you get the opportunity you've been waiting for: the chance to shoot a documentary. Then reality kicks in — unfortunately,your plate is rather full with teaching Media Science at a university.

How do you decide whether you can fit making that film into your schedule?

Simply being aware of the tasks you've committed to will help you in your decision. Writing down any current duties and projects — like future classes and seminars you've agreed to teach — helps you to see clearly how much time and energy you're already investing, and thus whether there's enough left over to give to that film project.

Furthermore, if you're aware of your commitments, you're in a position to consider the possibility of dropping certain tasks, or delegating them to someone else so that you can make space in your life for an attractive new project. Perhaps you could even allocate some of your classes to a keen research student so you can make the documentary.

But even if you've listed all of your commitments, and are now fully aware of them, you'll still have to decide which activities are more important than others. To prioritize in this way, you need to attain clarity about your job description and goals. Then you can determine the importance of the tasks in relation to a particular _reference_ _point_.

In our case, a good reference point is your job: How important is a particular task in relation to your job description? If your job is Media Science lecturer, and the semester has just begun, clearly teaching will be closer to your job description than making a documentary.

Another reference point could be your aspirations: Is it your ultimate goal to become a filmmaker? If so, then perhaps you should seriously consider quitting teaching and taking the opportunity that's in front of you.

### 5. The best plans prepare for difficulties and interruptions ahead of time. 

No matter your area of work, every project you plan will involve its own set of risks and difficulties, such as bad weather, or an investor who pulls out, and so on. However, it is often possible to prepare for things that might go awry.

In fact, the best time to deal with potential obstacles is while a project is still in the planning stages, and not when it's already underway. During this time, there are many opportunities to prepare for things that may go wrong.

For example, imagine that you're planning a large garden party and you anticipate rain on the big day. In such a case, you'll have plenty of time to arrange a party tent, or come up with fun indoor activities, just to be safe. Thus, no matter the weather, your party is set to go smoothly.

On the other hand, if you _hadn't_ used the planning stages to deal with potential problems, and a thunderstorm catches you by surprise, you'll have far fewer options — for instance, you wouldn't have a tent on standby.

Furthermore, on any project, delays and interruptions are to be expected. So, if your timetable doesn't allow for such interruptions, every unforeseen event will result in your project running behind schedule.

Just imagine a stage designer who knows his team can build a dummy ship in eight hours under normal conditions. If he schedules this task for the very day of the premiere performance, there will be no time reserve to fall back on should a co-worker call in sick or the paint not dry in time.

Aside from the fact that working this way puts his team under a lot of unnecessary pressure, the designer risks not completing his work in time for the premiere, which creates problems for the theater and everyone involved.

The lesson? A good timetable will accommodate such delays and prevent major problems from arising.

### 6. The right kind of organization will help you to get things done – and do creative work. 

At first glance, it may seem that productivity-management rules and routines simply aren't conducive to creative work — but they actually are.

If your work is well-organized, you can achieve more in less time. For example, if you make many to-do lists, but never really organize the tasks correctly, you'll end up overwhelmed: just imagine how you'll feel when confronted with a desk plastered with various forms and files.

Just to get away from that desk, you'll do whatever non-essential tasks you can — like replying to unimportant emails — but these will just eat away at your time and energy, ultimately making you less productive.

However, simply by organizing your work into a system that places all your to-do tasks in one location where you can keep track of them, you won't feel overwhelmed. Instead, you'll feel in control and able to devote time to each task without wanting to escape by doing other, less important tasks.

Also, the more organized you are, the more time and available psychic RAM you'll have to be creative.

Creative work, like any other work, takes a lot of time. So if your time is wasted because of chaotic working habits, you'll find less opportunities for creativity. Such bad habits also result in your psychic RAM filling up with non-essential information, leaving no mental space for creativity.

To avoid these obstacles, you should immediately complete those tasks that you're sure won't take longer than two minutes. Doing this frees your mind and enables you to spend time on more important things.

For example, say you intend to steal the Mona Lisa — a particularly creative task! If you're going to plan such a heist so that it goes smoothly, you'll first have to make sure you've answered any urgent emails, and fed your dog, as you're going to need to free up some psychic RAM for all that planning!

> _"When mental space has too many distractions, unmanaged agreements and loops, flow is limited."_

### 7. Putting too much effort into organizing your life can keep you from getting things done or being creative. 

As we've seen, getting organized can be extremely beneficial. However, managing every aspect of your life down to the smallest detail can be counterproductive. While being organized is certainly necessary, it's also crucial that you remain flexible.

Why?

First, instead of increasing your productivity, an overly elaborate system will consume all of the energy and time you need for the actual work.

While you're focusing on getting organized, you're not focusing on the very work that such organization is designed to serve. For instance, imagine that you want to become more active and healthy, and thus develop a very detailed plan to fit a certain amount of running and strength-building exercises into your tight schedule.

If your system depends upon meticulously recording your pulse rate and increases in muscle mass, you may end up spending more time on updating and maintaining that training plan than on exercising.

Second, trying to tightly control and manage every aspect of your work and life can make you inflexible and impede your creativity.

If your management system is too detailed and rigid, you might fail to seize a great opportunity. For example, you might end up turning down an invitation to a conference that would be highly beneficial for your career, just because you've committed to a schedule that has no built-in flexibility to respond to unexpected events.

A rigid system might also damage your creativity. For instance, some fiction writers organize the practicalities of their work very strictly. The result is that all of their novels adhere to basically the same plot, with the only difference being the details of characters and settings — for instance, perhaps one novel is set in Paris while the other in Prague.

> _"Many people are trying but can never work hard enough because working hard enough is not really what they need."_

### 8. To reach your long-term goals, translate them into short-term action steps. 

Imagine an English teacher who has created many interesting characters and has a great idea for an intriguing story. However, the act of transforming these ideas into a novel seems like far too ambitious a project, given that there's just too little time: too many lessons to plan, students to teach and assignments to correct.

Sound familiar?

In our everyday schedules, there's simply no time slot large enough to accommodate a big project. Writing that novel, for instance, will take a massive amount of time, and during our teacher's typical workday there are many important tasks that must be dealt with immediately, such as answering parents' phone calls, supervising students and, of course, teaching classes.

However many small moments of free time exist throughout the day — between those tasks, and after the working day is done — there certainly won't be enough time to author the next Great American Novel.

So how can someone like our teacher achieve her goal?

The answer is that single "action steps" towards that long-term goal can be easily slotted into one's schedule.

To get started, our English teacher could break down her novel project into action steps, such as finding a title, creating a rough structure, or even writing just one page per day. Compared with attempting to tackle the whole project at once, each of these steps will take only a very brief amount of time.

Once she organizes her project and her time into manageable steps, our teacher will find that, suddenly, the work of writing her novel fits snugly into the slivers of free time she has between teaching, grading and lesson planning. And, eventually, completing each of those manageable tasks will result in our teacher finishing the first draft of her magnum opus!

### 9. If a team isn't getting things done, improving team communication may help. 

So far, we've learned how to make our individual lives more efficient. Now, in this final blink, we'll see how to increase efficiency when working as part of a team.

Ideally, a team should be far more productive than a group of isolated employees. Sometimes, however, teams fail to cooperate productively. That's when it might help to be on the lookout for communication problems.

This is because fluent, reliable communication is essential to productive teamwork: in order to coordinate its activities effectively, a team must exchange information effectively. For example, team members should keep each other informed about when they'll be able to complete their assigned tasks so that the team can plan ahead and distribute new assignments.

If one or more team members don't communicate effectively, the working pace for the entire team can be slowed down.

For instance, if someone working on an advertising campaign urgently needs information from his colleague about the customer's musical preferences in order to meet the agreed deadline, but his colleague takes several days to respond to his emails, the lack of communication will mean that the team isn't functioning as productively as possible.

One solution for such a team is to train members in their individual communication responsibilities.

Often, identifying communication problems in a team requires simply looking at the email inboxes of each member. When many emails are labeled "urgent" or "high priority," this indicates that certain members are trying to make themselves heard. This is usually a reaction to their emails not receiving a response within an acceptable timeframe.

The best method for overcoming this problem is to make clear to each team member how valuable clear and prompt communication is to the team as a whole. They should be made to understand how important it is to share information regarding when a particular task will be done, or indeed whether it can be done at all.

### 10. Final Summary 

The key message in this book:

**If you want to be more creative and productive in your personal or professional life, then you have to prepare yourself. This involves using techniques to free up your "psychic RAM" as well as developing the mental flexibility to effectively shift your focus to wherever it's needed at any given time.**

Actionable advice:

**To get things done in a team, put someone in charge.**

In a team, if the responsibility for a project is distributed equally among team members, no one person will feel that they're in charge. Consequently, nothing will get done — not on time, at least. But if you ensure that one team member is made responsible for the whole team's outcome, the chances that important tasks will be actioned increase dramatically.

**Be organized but flexible.**

Don't try to manage every little aspect of your work, as this won't make you more productive. Instead, it will consume too much of your precious time and energy — resources that are better used elsewhere. Of course, you do need to have an organizational plan, but it should be flexible at the same time.
---

### David Allen

Productivity consultant David Allen is considered one of the leading experts on organizational and personal productivity. Best known for creating the highly popular "Getting Things Done" time-management method — and authoring the bestselling book of the same name — Allen is also ranked among the top 5 executive coaches in the world.

