---
id: 599817dbb238e10006a125cb
slug: real-artists-dont-starve-en
published_date: 2017-08-21T00:00:00.000+00:00
author: Jeff Goins
title: Real Artists Don't Starve
subtitle: Timeless Strategies for Thriving in the New Creative Age
main_color: 779D43
text_color: 516B2E
---

# Real Artists Don't Starve

_Timeless Strategies for Thriving in the New Creative Age_

**Jeff Goins**

_Real Artists Don't Starve_ (2017) is a wake-up call to those who think being an artist isn't a realistic or viable pursuit. Here, you'll find ample proof to debunk the myths portraying artists as starving and solitary figures that must suffer for their art. There's a New Renaissance going on these days — find out how you can be a part of it!

---
### 1. What’s in it for me? Thrive, don’t starve, as an artist. 

There's something gripping about the image of the poor poet, leading a bohemian life and surviving on the bare minimum. But even though the idea of the so-called starving artist might seem romantic, the reality of having to fight to make ends meet is a tough grind.

By contrast, being able to live off and even thrive from one's art is a dream that most aspiring creatives share, and these blinks will explore how this lofty dream can become a reality.

These blinks will outline — and debunk — some of the popular myths about what makes a true artist, and will explain how any artist can prosper. From the fallacy about the natural-born genius to the misconception that artists have to relentlessly promote themselves, you'll learn how to separate genuine, helpful advice from damaging cliches in the artistic world.

You'll also discover

  * how 30 minutes of creative writing a day turned a lawyer into a best-selling author;

  * why even 122 rejection letters should never stop you from continuing your work; and

  * how close Rowfl the dog was to not joining the Muppets.

### 2. Anyone can be an artist, and real artists know how to steal. 

For ages, we've been holding on to popular misconceptions about what it means to be a "real" artist.

In the media and in our minds, true artists are depicted as starving, naturally talented and deeply original. Unfortunately, these types of ideas can scare some people away from ever entering the arts and fulfilling their creative potential.

So let's start with two of the biggest fallacies about artists, namely that they're born with natural talent, and that their artwork must be completely original.

First off, you don't need to be born with natural talent to become an artist; what you need to do is put in hard work and show persistence and determination. With these tools in hand, even a corporate professional can become an artist. Just consider the case of lawyer turned best-selling author, John Grisham.

He began by finding just 30 to 60 minutes each day to write one page of his book. Three years later he had finished his first novel, _A Time to Kill_. And even though 40 different publishers would come to reject it, he persisted and got to work on his second book, _The Firm_.

Eventually, his perseverance paid off, and both books became bestsellers. They did so well that Grisham was able to quit his day job and devote all of his time to writing. His books even launched a new literary genre, the legal thriller.

While you do need to be dedicated to your craft, you don't need to reinvent its form or be wholly original to succeed. On the contrary, what you need to know is how to steal.

Like Picasso said, "Good artists copy, great artists steal."

This doesn't mean being lazy or mimicking someone else's work; there's a code of conduct among artists that allows for taking something that's been done and expanding upon it or pushing it further.

A great example is _The Muppets_. The show's creator, Jim Henson, took the puppetry of the puppeteer Burr Tillstrom, along with the humor and jokes of other artists, like the comedian Ernie Kovacs, and combined them to create something new and exciting.

Next, let's look into what makes an artist thrive.

### 3. Thriving artists are both humble and stubborn. 

While the image we have of the "starving artist" is a fallacy, this doesn't mean that being an artist is easy.

There's bound to be moments of rejection, and the best way to get through these tough times is to put your stubborn side to good use.

Luckily, if you feel a calling toward the arts, you probably have a stubborn side — a part of you that refuses to give up or play by someone else's rules. Psychologist Paul Torrance believes that most creative types are misfits and rule breakers who had trouble fitting in at school.

So, while you should expect to hit difficult times along the way, you can overcome these hurdles by remaining true to your stubborn self.

When F. Scott Fitzgerald was trying to launch his writing career, his apartment was littered with 122 rejection letters. Yet Fitzgerald kept at it and didn't stop until he got his first two books published, which eventually brought him critical acclaim and financial reward.

Fitzgerald would later experience a major drop in his confidence and self-image, however, particularly when critics turned on him and panned his third novel, _The Great Gatsby_. Eventually, alcoholism and a heart attack would lead to his death at just 44 years of age.

His story is especially tragic since, three years after his death, _The Great Gatsby_ would come to be reevaluated as a modern classic, once again demonstrating the importance of persistence.

Another much-needed quality is humbleness, which is necessary for an aspiring artist to learn what they need through a fruitful apprenticeship.

One of the reasons the starving artist myth has prevailed is because artists who think they know it all have ended up broke.

However, while the starving artist thinks they already have all the talent it takes, the thriving artist expands their skill set by seeking an apprenticeship under a true master — as Michelangelo did when he studied under the renowned Renaissance painter Domenico Ghirlandaio.

For any such apprenticeship to work, you must show humbleness and a willingness to be taught; no mentor wants to teach someone who thinks there's nothing left for them to learn.

### 4. Artists often succeed by joining a scene and cultivating collaborators and patrons. 

Another side to the starving artist myth is that they live isolated lives and work in solitude, only to emerge when they complete their masterpiece.

It's a romantic notion, but not an accurate one, since genius doesn't benefit from existing in a vacuum.

What is far more likely, and beneficial, is for artists to create vibrant communities and nurturing environments where they can support one another.

We've seen this throughout the years in many contexts, such as in post-WWI Paris, when Ezra Pound, Ernest Hemingway and James Joyce, among many others, lived and worked in the same neighborhood.

More recently, in areas like Silicon Valley, creatives in the tech field have come together to invent amazing software that one lonely programmer in a basement would probably never have come up with.

To cultivate collaborators, you have to know where to look. So study your field to become familiar with the best artists and find out what they do well.

We see this a lot in today's hip-hop music scene. While some have been critical of artists like Beyoncé and Kanye West because their albums have had dozens of writers collaborating on them, this isn't a sign of inferiority; rather, it's a sign of visionaries who can bring together great talents to create a powerful work of art.

Similarly, Michelangelo worked on projects that were bigger than he could handle himself, so he worked like an engineer and a foreman, directing people to complete individual tasks.

Michelangelo also knew the value of a patron, which a thriving artist must also seek out.

There isn't one kind of patron; they could be a critic or tastemaker, or anyone who can help move the artist's career forward.

Famously, Elvis Presley was catapulted to stardom with the help of Sam Phillips, the owner of Sun Studios in Memphis, Tennessee. A 19-year-old Elvis was able to calm his nerves in front of Phillips long enough to belt out one perfect tune, "That's All Right Mama," and Phillips made sure it was played on the radio that very night.

Two years later, every American teenager knew Elvis's name.

### 5. The thriving artist shares and interacts with their audience. 

Finding an audience is crucial to being a thriving artist and not a starving one. But, luckily, there is more opportunity than ever for an up-and-coming artist to get noticed.

While a lot of artists despise the idea of marketing and advertising and find it all rather sleazy, these days there are ways to share your art and build a following in a more natural and organic way.

One of the most common ways is through a regularly updated blog that offers a peek into your creative process.

As an example, let's look at the trajectory of Stephanie Halligan, an artist who always dreamed of being an animator. Despite these dreams, Halligan was $34,579 in debt after college, so she took a job working as a financial consultant for low-income families.

This led to Halligan starting a blog called the _Empowered Dollar_ in 2012, which was a way for her to pass on the best and most effective tips she'd learned. But she still felt an artist's calling, so after two years of the blog growing its audience, she began to add her own cartoons to accompany her new posts.

Suddenly, for the first time in years, Halligan felt alive — and this feeling grew as she began to sell prints of her cartoons to her readers. Soon, she was starting a new art blog and taking on clients, such as banks and universities, who liked her motivational cartoons.

But an artist can use the internet for more than just sharing their art with people.

Many artists today find ways of interacting directly with their audience online. New musicians can test out new songs and release demos, while artists can share works in progress in a variety of different ways, such as through the website Bandcamp. 

This is essentially like practicing in public, and it's the best way to not only build an audience, but to get better as an artist as well.

You can practice guitar in your bedroom for years and only improve so much; once you start playing in front of an audience, you'll start playing better than you have before. This is the simple rule of the audience.

### 6. Avoid working for free and fight to keep ownership of your work. 

One of the worst habits an artist can get into is working for free. This often happens when someone tries to tell you that a project will be "good exposure" or a "valuable opportunity." But much like the unpaid internship, it's often just a way for someone to exploit your talents and take advantage of you.

To be a thriving rather than a starving artist, you need to adopt a mindset in which you tell yourself that you are an artist, and that your work is worth money.

Placing value on your creative work is also a way of valuing the entire artistic profession, which is something Michelangelo fought for. He insisted that artistry should be considered a noble profession and that artists should be called by their last names, which was customary for only the aristocracy at the time.

More recently, the novelist Steven Pressfield has stated that you become an artist once you start saying that you're an artist. So, you can start printing those business cards that say you're a painter, writer or graphic artist — and make sure you charge accordingly.

Another thing to be aware of if you want to thrive as an artist is ownership.

Countless artists have signed deals when they were just getting started that gave up ownership of their art for a lump sum of money — and they virtually always live to regret it.

Luckily, _The Muppets'_ creator Jim Henson didn't make this mistake. When he was just starting out, the Purina dog food company offered him $10,000 for the rights to his Rowlf the Dog puppet. His agent thought it was a great deal, but Henson wisely turned it down. If he hadn't, he wouldn't have been able to use Rowlf as a beloved character when he launched his television show _The Muppets_ years later.

So, the bottom line is, if you want others to respect you, you have to take ownership and always charge money. This way, you can take part in the New Renaissance, which we'll have a look at in the last blink.

### 7. The New Renaissance is here, with artists that have a diverse set of skills and don’t starve for their art. 

Often, someone who dabbles in a variety of vocations is seen as being less talented than someone who concentrates on just one skill.

But it wasn't this way during the Renaissance. In fact, the term "Renaissance man" described a well-rounded craftsman, someone who could work in many different trades.

This is the kind of person we're starting to see more of today — a kind of New Renaissance artist.

Since there have never been more ways to share the art we create and get our hands on the tools we need to make art, we are seeing the dawn of a New Renaissance.

Much like during the original Renaissance, today's artists are those who are restless in their pursuit of more than just one craft, as this makes both life and work a richer experience.

Mark Frauenfelder is one such artist. He's a mechanical engineer by trade who cofounded the website _Boing Boing_, which began as a zine for creative-minded people in the late 1980s. Since then, it has become a popular website, and Frauenfelder has gone on to become an associate editor for _Wired_ magazine and the publisher of a book on magic tricks. He is also an artist in his own right, with work displayed at exhibitions across the United States.

Frauenfelder knows that people have traditionally looked down on those who don't fit neatly into one category, but he's found success by following his artistic inspirations, wherever they may take him.

For Frauenfelder, this mentality helped ensure that he never became a starving artist. He has always seen the goal of making money as a way to become your own patron; this way, you can use the money you earn to make more art and keep the cycle going.

So, get out of the starving artist's mind-set and enter the psyche of a thriving artist by studying and stealing from the great artists. Then, find an apprenticeship to learn from a master, and if you venture out on your own, be persistent until you find your audience.

And finally, always remember: never work for free.

### 8. Final summary 

The key message in this book:

**The romantic image of the starving artist is a myth that needs to be debunked. With so many tools and great ways to be creative and share one's art with the world, there is no reason to adopt the mindset of a starving artist. Rather, to be a real, thriving artist, start by studying your field carefully and learning from someone you admire. With persistence and practice, you'll be able to make great works of art.**

Actionable advice:

**Find a valuable apprenticeship by doing your homework and following the master's lead.**

If there's an artist you truly admire, study them and learn everything you can from them before you even introduce yourself. Create something that is influenced by them and your admiration for their work, and which shows that you are eager to learn. Be humble and be teachable. Then, introduce yourself to the artist and let them know that you want to learn everything they have — and are willing — to offer.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Art, Inc._** **by Lisa Congdon and Meg Mateo Ilasco**

_Art, Inc._ is a practical guide for artists who want to earn a living from doing what they love. Art, Inc. presents the business side of the art world in a palatable way, sharing useful advice and practical guidance.
---

### Jeff Goins

Jeff Goins is a former musician who went on to start a writing career, which has since become a thriving business aimed at helping others turn their artistic dreams into reality. His other books include the bestseller _The Art of Work_. You can find out more about his teachings and keynote speeches at goinswriter.com.

