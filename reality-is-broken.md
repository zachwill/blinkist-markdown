---
id: 53f47bbe3433390008960000
slug: reality-is-broken-en
published_date: 2014-08-19T00:00:00.000+00:00
author: Jane McGonigal
title: Reality is Broken
subtitle: Why Games Make Us Better and How They Can Change the World
main_color: EDE22F
text_color: 6E6916
---

# Reality is Broken

_Why Games Make Us Better and How They Can Change the World_

**Jane McGonigal**

_Reality_ _is_ _Broken_ explains how games work, how they influence our everyday lives and what potential they have to improve our lived reality. Full of examples of different game styles and their effects on gamers' dispositions, it not only offers a broad perspective on what games are but also shows how game designers can use them to solve some of the world's most pressing problems.

---
### 1. What’s in it for me? Learn why playing games can actually improve your life – and everyone else’s. 

Games, especially video games, are time and time again the topic of media curiosity, where their focus is often drawn to the realistic violence and gore found in many video games.

However, the spectrum of games today extends far beyond obscene violence, and of the millions and millions of us who game, virtually none will confuse fiction for reality and engage in acts of violence.

Quite the contrary: games offer us immense value, helping us to develop essential skills that easily translate into the "real world." In fact, the benefits of gaming are so strong that we should _all_ spend more time engaged with them and outfit our daily routines in accordance to gaming principles.

Even things as mundane as doing chores can be turned into an exciting and engaging game!

But gaming principles aren't just a way to make mundane tasks bearable — with a little creativity, we can use them to reach loftier goals centered around furthering human development and solving serious worldwide problems.

In these blinks, you'll find out

  * why the myth of the "lone gamer" is so bogus;

  * why people are drawn to games;

  * why the author considered hiding the toilet brush from her husband; and

  * how one game developer was able to coax its players into trying to help solve a major world problem.

### 2. Games provide more motivation and rewards than “real life” does. 

Today, video games are more popular than ever before. Across the world, millions of us use our phones, PCs, or various consoles to play.

In fact, a whopping 97 percent of youths play one game or another. But, what draws us to video games in the first place?

Many experts believe their popularity is the result of mere _escapism_, a chance for us to distract ourselves from the pressures of reality for a few hours.

However, this view is too simplistic. The _real_ reason why people are playing more video games is that games can provide people with important social needs that might not be available in "real life".

For example, chat rooms, _wikis_ (websites and games developed by users), and communities of gamers provide gamers with a level of social bonding that reality isn't offering them.

Games also provide people with a degree of _justifiable_ _optimism_ : players feel good because they know that they can win — that the game can be beat. Every game is solvable by design, which gives people hope.

And even if they do fail, they can take solace in having fun trying. Reality, in contrast, does not provide them with anything similar.

In addition, games provide people with a genuine happiness.

Indeed, a world without fun or excitement leaves people deflated and depressed. Games, with their missions, secrets and "easter eggs," can augment our otherwise dull lives with a bit of excitement, thus helping to prevent us from becoming unhappy.

What's more, games also can provide us with _fiero_, that wonderful feeling of triumph over adversity.

You've surely experienced this rush yourself after beating a boss or completing a difficult mission in a game — it's that moment when you jump from your chair throw your arms over your head and let out a celebratory scream!

> _"The opposite of play is not work. It's depression."_

### 3. Playing games is a sustainable way of providing ourselves with better lives. 

Imagine this scenario: Every day, you work for hours in a job you hate. If that wasn't bad enough, your life is made even more miserable by a daily commute for two hours along the busiest highway in the city. Sounds like fun, right?

Of course not. That kind of life would make anyone depressed!

However, there's a way to make things more bearable: video games. Fortunately for us, there is no micromanaging boss, long commute, or boring tasks in _World_ _of_ _Warcraft_ or _Call_ _of_ _Duty_.

Indeed, games help make life bearable, but also go a step further: they fill us with positive emotions by making us _truly_ happy.

Although many believe that happiness comes from chasing extrinsic rewards like money or fame, they'll never actually achieve happiness that way.

True happiness comes from within, from intrinsic rewards that cannot be _found_, but must be _made_. Gaming, moreso than chasing money, provides us with these rewards. Each time you accomplish a goal or a mission, you feel rewarded — you feel happy because you have accomplished something, in spite of other troubles you may face in life.

And the great thing about games is that they can provide this gratification instantly and endlessly, making it an _infinitely_ _renewable_ _resource_.

While most things that make us feel good aren't sustainable, playing games is. Each and every time we pick up the controller and load up our favorite game we experience positive emotions and intrinsic rewards.

And this gratification is instantaneous. In real life, whenever we accomplish something, we're usually forced to wait for feedback or acknowledgment of our achievement.

Games, however, are different: the feeling of attainment is instant.

For example, when you make a good pitch at work, you have to wait for your clients or bosses to answer your proposal. But if you capture the enemy's base in a game, then you know immediately that you've achieved your goal.

### 4. Multiplayer games help build important social bonds between players. 

The popular image of a video game fanatic is that of a lonely, sexless youth, living a life of exile in a dark bedroom. The only time he sees another human being is as he passes his mom as he creeps from his room to the refrigerator to warm up another microwavable meal.

Yet, this perception of gamers as isolated and lonely is far from accurate. In fact, since the development of the internet and online multiplayer games, video games are increasingly becoming an important tool for social bonding.

For one, games actually help people, particularly those who are shy or introverted, to make friends and bond.

In real life, these people find it hard to start and maintain healthy relationships. In games such as _World_ _of_ _Warcraft_, however, they are able to engage their online friends through a type of indirect communication that is more comfortable for them.

What's more, the evidence for this type of social bonding is clear:

Even though many prefer to play solitary missions, _World_ _of_ _Warcraft_ players still pay to join an online multiplayer world where others also play, and where they feel a sense of community. In fact, players benefit from just being around other players through a phenomenon called _ambient_ _sociability._

Additionally, video games also provide people with the feeling that they are part of something much larger than themselves.

But how can this be? Playing video game offers no "real" value, right? Not quite. A lack of material value doesn't exclude the presence of _meaning_.

And in online multiplayer games, meaning or purpose is _created_ by the feeling that you are part of a team, a community striving for a shared goal.

This is even reflected in the way people play games. For example, after beating a game, many "train" others to also be able to succeed at it. In other words, they feel a responsibility to help others in their community, just like you would in "real life."

Now that we've learned about the positive benefits of gaming, the following blinks will explain how to create these benefits with intent and purpose.

### 5. Even the most mundane routines can be turned into an epic experience through gaming. 

Earlier we discussed how tedious and hollow real life can be. But it's not always because something is actively getting you down. There are some things about real life that, while neutral, are just boring. How often do you get excited about going to the grocery store or pharmacy?

Games, however, are very different. Games are not mundane at all! In fact, playing a game is an _epic_ experience, i.e., one that eclipses our everyday experiences in terms of intensity or size.

They are able to do so by creating epic environments. For example, games like _Halo_ take place in alien worlds full of bizarre and awe-inspiring cities, landscapes and spacecraft.

Moreover, games also involve epic projects, where each mission or task adds up to an inspiring end goal. This could be anything from saving the world from a zombie or alien infestation to building your own business or criminal empire.

What if we were able to use the epic nature of games to add some color and excitement to our everyday routines? What if, for example, you could turn your mundane chores into an epic quest?

This is exactly what the game _Chore_ _Wars_ aims to do. For each task you complete, you earn points, whether you've washed the dishes, ironed your shirts or cooked dinner. And _why_ do you collect points? So you can battle with your family to see who can get the most!

This way, even the most boring of jobs becomes an exciting and engaging quest for glory. In fact, the author became so invested in this game that she almost found herself hiding the toilet brush from her husband so that she could complete the chore before he could and nab the points!

Looking beyond chores, we can really turn _anything_ into a game with enough creativity. In doing so, we can make our lives more fun and enriching.

> _"Something epic is of heroic proportions."_

### 6. We can use gaming principles to “hack” our happiness. 

The next time you're at a bookstore, have a look at the "self-help" section. Chances are it will be enormous. Thousands of authors have written exhaustively on the secret to happiness, many of them even drawing on extensive medical research.

Nevertheless, as a society, we are growing more depressed. In fact, the World Health Organization has called depression the greatest threat to global health.

Why, despite our knowledge about the mechanisms of happiness, do we increasingly feel blue?

Well, just because we know what will make us happy doesn't mean we can make the changes necessary to lead happier lives.

There are a number of causes:

First, we are naturally cynical towards any claims of the secret to happiness, treating these theories with disdain, mistrust and indifference.

Even if we _do_ jump onto the bandwagon, we still struggle to see things through to the end. You've almost certainly done this yourself: just think of the last diet or exercise plan that didn't make it past the first month.

Luckily, however, we can actually use games to change this self-defeating disposition by "hacking" games mechanics such that they help to spread genuine happiness.

Consider, for example, that displaying kindness to strangers is a guaranteed way to make yourself happy. Knowing this method, the author developed a happiness hack called _Cruel_ _2_ _B_ _Kind._

Players are sent an email or text containing three secret weapons to "kill" others with kindness. For example, one weapon could be something like a beaming smile, or a heartfelt compliment. Armed with these "weapons," they then take to the streets to use them.

And since players don't know who else is playing, they might "hit" strangers with a smile as well, making them feel good too and helping to spread happiness throughout their community.

Because it's disguised as a game, people react more positively to these tasks than they would if they found them in a self-help book or a psychology lecture.

### 7. Long-time gamers develop a number of skills as a result of gaming. 

Did you know that, by the time they have reached the age of 21, the average US American will have spent over 10,000 hours playing video games?

It is a generally accepted approximation that if you practice a skill for that long, you eventually become truly extraordinary at it! So what abilities have gamers mastered in their 10,000 hours?

Perhaps the most central skill gamers gain is _collaboration_ — in other words, cooperating and coordinating with each other to produce something. In a game, that means agreeing on the common rules and boundaries and deciding when to play in order to create a great gaming experience.

Indeed, modern games offer many ways for players to collaborate with one another in order to accomplish their shared goals.

For example, many games are outfitted with a _co-op_ mode in which players work together to achieve a common objective. For instance, in the game _Rock_ _Band,_ players play as a musical group on a stage. Or in _Call_ _of_ _Duty_ _2:_ _Modern_ _Warfare,_ gamers coordinate strategy in order to fight battles together.

So how does this cooperative skill manifest itself? Gamers develop three _collaborative_ _superpowers_ as a result of their modern gaming:

Firstly, although they might be shy in real life, gamers turn into extreme extroverts in the online world, always ready and willing to contact others within their network to have discussions and hatch plans.

It's not just that they're eager to reach out to one another: gamers also develop a "sixth sense" for knowing who is best to collaborate with in a particular situation.

Finally, gamers develop what is known as _emergensight_. In highly complex and chaotic environments, where it's difficult to predict what will happen next, gamers thrive. Having played so many games, they are experts in reading signals from their environments and fellow players, and changing their tactics on a dime.

So how do we bring these skills into the real world? These final blinks will look at how gamers can use their collaborative skill to surmount the world's most pressing problems.

### 8. Gaming principles can broaden collective knowledge and help us find solutions to global problems. 

You've probably spent some time scrolling through articles on Wikipedia. You might have even contributed to an article or two.

In fact, these small contributions add up: one estimate suggests that Wikipedia is the product of 100 _million_ hours of work.

But what is it that inspires people to invest their limited time in Wikipedia?

Well, the most successful collaborative projects on the internet, such as Wikipedia, function much in the same way as collaborative _massive_ _multiplayer_ _online_ _role-playing_ _games_ (MMORPGs), in which players create a character who interacts with millions of other people's characters online.

Just like in MMORPGs, where players can explore secret passages, dungeons, hidden kingdoms, etc., Wikipedia readers and editors have their own kind of exploration in the form of unusual articles.

For example, when this book was written, there were 137,356 "lonely pages" on Wikipedia, i.e., pages which aren't linked to other articles and can therefore only be found through direct searches.

Consequently, only the best sleuths can find them. Those secrets, so to speak, provide users with the motivation to search and contribute to the largest encyclopedia known to humanity.

What's more, we can also use the collaborative power of gaming to find solutions to some of the world's biggest problems.

One of the biggest obstacles in developing solutions to serious global problems is that we think our voice or idea is too small to make a difference. Games, however, can help us get a more hopeful perspective.

Consider, for example, the game _World_ _Without_ _Oil,_ in which players described what would happen if the world ran out of oil. Would there be food shortages? Riots? Revolutions? What could we do to hold society together?

The game unleashed a wave of new ideas and thoughts because it got people to contemplate a potential future calamity in an engaging way.

By bringing people together through games, we have the opportunity to harness otherwise unused resources and generate participation. With this strategy, even the biggest problems become solvable.

> _"Good games are productive. They're producing a higher quality of life."_

### 9. We must try to get everyone involved in games, since they are a mechanism of world prosperity. 

Because games can change our perceptions and skills, we can use them to create a better future.

For starters, the collaborative nature of games help us define lofty goals and tackle seemingly impossible social missions together.

One way they accomplish such ambitious goals is by fostering an outlook that sees the world as a complex web of interconnected and interdependent parts, called _ecosystems_ _thinking_. Gamers are aware that small actions can add up to larger ramifications, which can help them to appreciate, for example, how changes in our environment such as resource exploitation can have massive side-effects.

Gamers also learn _pilot_ _experimentation_, in which they test and develop strategies through small-scale experimentation before finally scaling up the most successful ones. This slow, evidence-based approach is ideal in tackling complex problems.

These skills are not trivial. In fact, they are so important and beneficial for society that McGonigal hopes a game designer will win the Nobel Prize in the future.

However, if we want to reap the greatest rewards from games, we must increase the number of games that might help people develop solutions to the world's problems.

We've already seen how _World_ _Without_ _Oil_ motivated people to contemplate the unsustainable use of fossil fuels. Unfortunately, the game only ran for a short time and with a relatively small number of players. If we want to change the world through games, we need to get _everyone_ on board.

The question is no longer how to get people to play games, but getting them to play the kinds of games that actively aim to improve society or solve pressing issues. If we can garner participation, we have a much better chance of dealing with whatever problems the future might bring.

> _"New games … make it possible for gamers to change, or even save, real people's lives as easily as they save virtual lives."_

### 10. Final summary 

The key message in this book:

**Games** **are** **not** **the** **mindless** **activities** **you** **do** **between** **bursts** **of** **productivity.** **In** **fact,** **games** **are** **highly** **beneficial** **to** **our** **social** **well-being:** **they** **connect** **us** **with** **people** **all** **over** **the** **world** **and** **foster** **collaboration.** **Knowing** **their** **advantages,** **there** **are** **steps** **we** **can** **take** **to** **integrate** **games** **more** **fully** **into** **our** **everyday** **lives** **in** **order** **to** **increase** **our** **happiness** **and** **help** **solve** **difficult** **problems.**

Actionable advice:

**Don't** **feel** **guilty** **playing** **video** **games!**

Although many would have you believe that playing games is merely a form of mindless, brain-rotting escapism, gaming actually helps you develop crucial skills and social bonds. The scientific approach to experimenting in-game, as well as the collaboration required for successful online gaming, translate very well into "real life."
---

### Jane McGonigal

Jane McGonigal is a creator of "alternate reality games" as well as Director of Games Research and Development at the Institute of the Future in Palo Alto, California. Armed with a PhD in Performance Studies, she explores the science of game development and game perception as well as the actual creation of games.

