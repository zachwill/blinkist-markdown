---
id: 5c2360466cee070007b45d42
slug: reality-is-not-what-it-seems-en
published_date: 2018-12-27T00:00:00.000+00:00
author: Carlo Rovelli
title: Reality Is Not What It Seems
subtitle: The Journey to Quantum Gravity
main_color: ECD82F
text_color: 6E6416
---

# Reality Is Not What It Seems

_The Journey to Quantum Gravity_

**Carlo Rovelli**

_Reality Is Not What It Seems_ (2014) offers a quick overview of the long journey modern science has taken from the cosmic observations of ancient Greece to the heady theories of quantum mechanics. These blinks offer an easily digestible take on the many twists and turns that have occurred in the history of modern physics, as well as an overview of the tricky questions physicists continue to grapple with today.

---
### 1. What’s in it for me? Take a historical journey down the winding road of modern physics. 

Since at least the fifth century BCE, there have been scholars using scientific tools to better understand our world and how the universe works. Prior to the turn of the twentieth century, quite a few developments took place, but it's only from 1900 onward that the pace has really picked up.

Thanks to Albert Einstein's theory of general relativity and the field of quantum mechanics, as well as amazing technological developments, the past one hundred years have brought us one remarkable discovery after another. These blinks are both a helpful catalog of the biggest revelations as well as a handy report card on where we now stand and the mysteries that still keep physicists up at night.

In these blinks, you'll find

  * which ancient Greek had some prescient ideas about atoms;

  * how the two pillars of modern science are in conflict; and

  * which "grains" make up the space around you, according to quantum gravity.

### 2. Modern science began with the scholars of ancient Greece and the experiments of the late Middle Ages. 

For thousands of years following the first human civilizations, our ancestors explained everyday natural occurrences by invoking things like supernatural spirits and deities. That finally began to change around 500 BCE, thanks to the scholars of ancient Greece. They understood that reason, observation and mathematics could be used as tools to explain the world around them.

One such scholar was Anaximander, a philosopher who used those rational methods to explain how rain fell from the sky. It wasn't the work of a benevolent god, he explained. Rather, evaporation caused water to accumulate in the sky and then fall back to earth.

Not long afterward, another scholar named Democritus theorized that everything in the world was made up of tiny building blocks called _atoms_. Democritus also reasoned that there must be a finite size to atoms — a point where you can no longer divide these tiny grains of matter. This theory was rooted in the idea of _spatial extension_ : that matter must have size and occupy space. Therefore, atoms must also have a certain indivisible size.

More advancements came in the third century BCE, prompted by philosophers such as Plato and Aristotle, who both contributed to the idea that mathematics could be used as a tool for understanding our universe.

Then there was Ptolemy, born in 100 CE. He created formulas to calculate the movements of planets, thereby allowing us to predict their future positions.

Over a thousand years later, during the Middle Ages, Renaissance scholars such as Copernicus and Galileo returned to the ancient tools of mathematics and reason. This allowed Copernicus to revolutionize astronomy by proving that the orbits of celestial bodies could be better calculated once the sun, not the Earth, was considered the center of the solar system.

Likewise, in the sixteenth century, Galileo was the first to gaze upon the mountains of Earth's moon, the rings of Saturn, and the moons of Jupiter, thanks to the newly invented telescope. Galileo also tested his hypotheses with rigorous and repeatable experiments, thereby helping to create what came to be known as the scientific method.

One such hypothesis was the belief that all objects fall at a constant speed. However, what Galileo's tests revealed was that it wasn't speed, but rather acceleration, or the rate of _increased_ speed, that was constant among falling objects.

This discovery marked the very first mathematical law for earthly bodies: that every second, the speed of any falling object on Earth will increase by 9.8 meters per second.

### 3. Newton’s theory of universal gravitation was upended in the twentieth century by Albert Einstein. 

Around a hundred years after Galileo's calculations, in the seventeenth century, the scientist Isaac Newton came to see Galileo's work in a new light. Newton was experimenting with a theoretical test of a "little moon" orbiting just above the Earth's surface, and he came to realize that the force affecting the curve and speed of the orbiting body was likely the same force behind Galileo's falling objects.

Newton knew that there must be some widespread force at work — one that caused both the moon to orbit the Earth and objects to fall to the ground. And this is how he began to develop his landmark theory of universal gravitation.

Newton's theory presented a new picture of the universe, one where, within the vastness of space, bodies are drawn toward one another by the constant force of _gravity_. This theory marked a giant leap forward in scientific understanding, as it marked the first time we singled out a force that connected the laws of Earth with those of the celestial bodies in the heavens.

While Newton may have been a genius, he also knew that this was an incomplete picture. There were still mysterious forces at work that were yet to be discovered.

Sure enough, in the nineteenth century, one such force would be revealed by the British scientists Michael Faraday and Clerk Maxwell. These bright minds discovered _electromagnetism_ : the force that binds together the atoms that form molecules, as well as the electrons that are within atoms.

But perhaps even more important was the concept of the _field_. Faraday and Maxwell suggested that there was an invisible web — or field — throughout space that enables electromagnetic forces to act.

The concept of the field really took hold in 1905, thanks to Albert Einstein's theory of special relativity, which was intended to bring Newtonian physics together with the more recent theories. With his radical new theory, Einstein showed how different observers could experience the laws of time and space differently based upon their unique conditions.

Suddenly, even time was no longer a universal absolute. And while this was a huge revelation, Einstein was just getting started.

> _"The present is like the flatness of the Earth: an illusion."_

### 4. Einstein’s theory of general relativity tied together matter and space and suggested an expanding universe. 

In 1905, Einstein's theory of special relativity raised many eyebrows and gained him a lot of notoriety as a bold young voice in the scientific community. But ten years later, his theory of _general relativity_ was admired by many as a masterful, even beautiful, work of scientific theorizing.

General relativity was brilliant because it brought together all matter and space as being subject to the same laws of the _gravitational field_ — just as, years earlier, the electromagnetic field brought together electric and magnetic forces.

By introducing a gravitational field, Einstein was again redefining the concept of space and what it's made of. Space had long been synonymous with emptiness, but Einstein was suggesting that this wasn't the case. According to general relativity, space _is_ the gravitational field and it's constantly affecting all matter.

In the Newtonian world, questions about space and matter were considered separately. But now Einstein elegantly explained how mass could bend the space around it, thereby creating a "curvature" in space. This caused bodies to be pulled toward each other, like two marbles in a funnel. And so, Newton's seventeenth-century concept of gravity was effectively brought into the twentieth century.

Einstein's theories also applied to the very origins of the universe, leading to the concept of how it all started from a "big bang."

Scientists have long pondered the question of whether the universe is finite or infinite. But Einstein suggested a middle ground, stating that something could be both finite and limitless. Take Earth, for example. If you set off in one direction, you could continue traveling along the surface of the world forever; you wouldn't hit a dead end. So, in a way, it is limitless, yet still finite, with a certain amount of surface to the planet.

This led Einstein to theorize whether or not the geometry of space isn't similar. Perhaps, if you set off in one direction, you might eventually find yourself back where you started.

But a finite universe would mean that, due to gravity, all the bodies within it would be pulled toward the center, which would result in the eventual inward collapse of all matter. Since this hasn't yet happened, Einstein figured, the universe must be expanding outward, as the result of an event that set everything in motion. And this is how Einstein arrived at the idea of what became known as the big bang theory, the initial event that created enough force to counter the gravitational pull.

### 5. The theory of quantum mechanics has unveiled three fundamental aspects of the world. 

Einstein's theory of general relativity wasn't the only game-changer to emerge in the twentieth century. Physics was also revolutionized by quantum theory, otherwise known as _quantum mechanics_.

While general relativity seeks to explain the cosmic laws of space and matter, quantum mechanics is our best explanation of what's going on down at the microcosmic level of atoms and particles.

However, even though this field of physics has achieved great experimental success, it still holds many mysteries that physicists continue to grapple with.

Essentially, quantum mechanics has cast a light on three fundamental parts of our world: _granularity_, _relationality_ and _indeterminacy_.

It all began back in 1900, when German physicist Max Planck started calculating the energy in electrical fields. To make things a little easier for himself, Planck took a mathematical shortcut and assumed that all energy in the field was distributed in small packets — he called them _quanta_ — rather than at a continuous rate, as was generally assumed. Much to Planck's surprise, his calculations were spot on.

It soon became clear that quanta were more than just mathematical trickery. In 1905, Einstein realized that light was also made up of similar small packets. A few years after that, Danish physicist Niels Bohr found that an atom's electrons can only have particular amounts of energy, as opposed to the continuous spectrum of energy that had been assumed.

So, what Planck, Einstein and Bohr had all discovered is the fundamental theory behind quantum mechanics, which is that the universe is _granular_, since both energy and light are made up of tiny, finite packets.

The next fundamental aspect of quantum mechanics is that the world is _relational_, which is a discovery attributed to the German physicist Werner Heisenberg.

In the 1920s, Heisenberg found that electrons don't always have a precise position in space — thereby upending another commonly held belief among physicists. Instead, Heisenberg found that an electron's position can only be determined if it is interacting with something else. Taken a step further, this also means that an electron can only _exist_ based on its relation to another object.

This leads us to the third aspect of quantum mechanics: _indeterminacy_.

As the name suggests, indeterminacy means there's only a probability and never a certainty with which we can predict physical events, such as the position of an electron. Such is the unusual world of the subatomic environment.

> _"Light falls on a surface like a gentle hail shower."_

### 6. The theory of quantum gravity raises doubts about the common notions of space and time. 

Today, there's a paradox facing modern physics. The two pillars of twentieth-century physics — the theories of general relativity and quantum mechanics — are at odds with one another.

To begin with, according to general relativity, space is curved and everything is continuous. But in the world of quantum mechanics space is flat and everything is granular and comes in tiny packets.

Understandably, physicists are eager to reconcile these differences and to find a theory that can somehow bring these two worlds together. And this is precisely what the modern field of quantum gravity aims to do.

Basically, quantum gravity makes two fundamental claims. The first is that space is not continuous, but rather also granular.

This brings us back to Democritus, and the question of what can or cannot be infinitely divisible at the atomic level. According to quantum gravity, and the Soviet physicist Matvei Bronštein, space is _not_ infinitely divisible, just as Democritus had theorized about matter and atoms.

While Bronštein developed his theory back in the 1930s, subsequent research has indeed supported his claim by suggesting that the smallest size space can be divided to is 10 -33 centimeters. This measurement even has a name, _the Planck length_, and it's a billion times smaller than the nucleus of an atom.

Since space is turning out to be more like matter than we previously thought, other names like the "atoms of space" or "quanta of space" have come to refer to its granular nature.

The other fundamental claim of quantum gravity concerns our very basic notions of time.

Over a century ago, Einstein's theory of special relativity disabused us of the notion that we're all subject to a great cosmic clock. The fact is, in some places in the universe, time passes slower or quicker than in others.

For example, the higher up you are, the faster time will run. If you place one clock on a table and another on the floor, the one closer to the earth will run slightly slower.

Since time is no longer a universal and reliable scientific measurement, modern physicists, including those in the field of quantum gravity, no longer use it in their fundamental equations. So it's not only safe to say that events no longer happen _in time_ ; you could even say that time _doesn't exist_ in modern physics.

> _"Space appears continuous to us only because we cannot perceive the extremely small scale of these individual quanta of space."_

### 7. Final summary 

The key message in these blinks:

**The study of physics has come a long way. The scholars of ancient Greece were the first to use reason, and then in the Middle Ages the scientific method was developed with the use of rigorous and repeatable experimentation. From this tradition, Newton developed a theory of the world where time and space were absolute. But the Newtonian tradition was eventually turned on its head in the twentieth century with Einstein's theory of general relativity coupled with quantum mechanics. Today, physicists aim to reconcile the differences of general relativity and quantum mechanics, and, as a result, they've opened up a wondrous world, where time is relative, space is granular and electrons may both exist and yet not exist.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _The Tao of Physics_** **by Fritjof Capra**

Now that you've had a good primer on the current state of modern physics from the perspective of a Western scientist, it's time to look at physics from a more spiritual, Eastern perspective.

You may think of these two worlds as completely unrelated, but as _The Tao of Physics_ (1975) reveals, there are some surprising commonalities between Eastern mysticism and the science of quantum mechanics and general relativity.

If you've ever been under the impression that spirituality and modern science don't go together, these blinks will show you another way, where the two can actually go hand in hand.
---

### Carlo Rovelli

Carlo Rovelli is a theoretical physicist who has made significant contributions to the field of physics and our understanding of space and time. He currently directs the quantum gravity research group of the Centre de Physique Théorique in Marseille, France. His other books include _Seven Brief Lessons on Physics_ and _The Order of Time_.

