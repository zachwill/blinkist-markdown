---
id: 5db595af6cee0700087a2bc4
slug: rebel-ideas-en
published_date: 2019-10-28T00:00:00.000+00:00
author: Matthew Syed
title: Rebel Ideas
subtitle: The Power of Diverse Thinking
main_color: E56B2E
text_color: B25324
---

# Rebel Ideas

_The Power of Diverse Thinking_

**Matthew Syed**

_Rebel Ideas_ (2019) explains why cognitive diversity is the fundamental ingredient for finding solutions to difficult problems, and how we can harness it to create positive change at work, in politics and when tackling global issues.

---
### 1. What’s in it for me? Learn how to optimize your chances of success in the face of complex challenges. 

Imagine you had access to an ultimate, cutting-edge ideas generator. How would it revolutionize your business, cause or research? And what might happen if you plugged your brilliant generator into someone else's, to combine their power? 

The truth is, we all have access to a generator like this. Perhaps you think that your brain can play the role of Ultimate Ideas Generator. While it's undeniable that intelligence is essential in solving complex challenges, there's another fundamental ingredient we must seek out, find and integrate into the mix if we want to be successful.

This ingredient has extraordinary powers. It can make blind-spots vanish, broaden our expertise exponentially, and tease out every infant idea and help it flourish.

As you'll learn in these blinks, that ingredient is _cognitive diversity_ — diversity of the minds we engage with. Once you've learned how to harness its power, you'll be a monumental force. 

In these blinks, you'll learn

  * that meetings are often fatal for brilliant ideas;

  * why thinking like a migrant can make you be more innovative; and

  * how spotting German swear words helped Britain crack codes.

### 2. Working with people who are similar to us undermines our potential for success. 

Imagine you're a CEO recruiting a new financial manager. The final two candidates are equal in merit and experience. One shares your views on company policy, while the other has voiced some concerns about your five-year plan. Who should you hire?

We tend to surround ourselves with people we identify with, in appearance, beliefs and perspective. This subconscious habit, known as _homophily_, occurs because it's validating to have our own ideas reflected back to us by the people around us, whether it's friends, family or colleagues. But the truth is that homophily significantly inhibits the success of a team.

The problem with homophily is that it creates _collective blindness_. Even if a team is made up of highly intelligent individuals, if they all think in similar ways, they won't be aware of what they're not seeing. These blind-spots often aren't the result of failure on any one individual's part. They can arise from incidental factors we can't control, like the culture we grew up in or who our university professors were.

We can see the devastating consequences of homophily if we look at the CIA's past recruitment patterns. Prior to 9/11, the CIA had a long tradition of predominantly hiring officers who mirrored existing staff: white males from the middle and upper classes who had studied liberal arts at college.

This homogeneity meant that, despite having thousands of personnel with a formidable budget at their disposal, CIA agents suffered from collective blindness. They overlooked important clues about Osama bin Laden's growing influence. Their lack of understanding about Islam, for instance, led them to dismiss him as primitive because he lived in a cave, had a long beard and wore a simple cloth robe. They failed to recognize that he had deliberately modeled himself on the Prophet, and that a cave is a deeply religious symbol to Muslims. Their blindness meant they underestimated the threat bin Laden posed, contributing to the horrifying tragedies that took place in America on September 11, 2001.

So, how do we overcome homophily if it's part of human nature? In the blinks that follow, you'll discover how to step beyond collective blindness by embracing the rebel within.

### 3. When solving complex problems, diversity is just as important as intelligence. 

Consider this hypothesis: if the fastest relay team can be created by cloning Usain Bolt, then the most accurate team of economists can be created by cloning the world's most competent forecaster. Makes sense, doesn't it?

Although the statement above seems logical, it's incorrect. Tackling complex problems demands more than just intelligence and skill. It demands diversity. And this diversity shouldn't be limited to demographic attributes, like gender, race, age, sexual orientation or religion. It must be a diversity of the mind, or _cognitive diversity_, something we wouldn't achieve by cloning our genius forecaster, since we'd be replicating the same brain.

Cognitive diversity in itself is complex and achieving it takes more than just a perfunctory review of demographic information. To build a diverse team, we must consider our candidates' specific skill-sets. For instance, two economists may have different genders and nationalities. But if they use the same economic model to solve problems, they'll arrive at the same conclusions. However, if we recruit economists who use different models, they'll draw on different data, ask different questions and make different assumptions. And this diversity of approach will increase the team's likelihood of making correct economic predictions, since important factors won't be overlooked.

When we build a team that is cognitively diverse, we increase what's known as _group wisdom_, meaning that the broad range of perspectives in the team give it thorough coverage. This collective intelligence doesn't just arise from academic knowledge, however. True group wisdom requires a deep understanding of human behavior.

We can find a brilliant example of group wisdom at work in the recruiting process used by Alastair Denniston at Bletchley Park during the Second World War. Alongside mathematicians and intellectuals, Denniston hired demographers to crack German codes. The team noticed that each encryption began with three letters that weren't part of the message. These letters revealed the cipher settings on the German encryption machine. After considering where a signal operator's mind might go in a moment of extreme pressure, the team realized that the letters were taken from either the beginning of a German girl's name — probably a girlfriend — or a swear word. Without a diverse team that possessed group wisdom, this important detail may have been overlooked.

Denniston's team was successful not only because it was demographically and cognitively diverse, but because it was fostered in the right way. Next, let's explore what a diverse team needs to thrive.

### 4. Without effective communication, the benefits of diversity are lost. 

Have you ever been in a meeting with your stomach in knots because you disagree with what your boss is saying but don't feel comfortable objecting? Or perhaps you have a brilliant idea to contribute, but between your manager and your assertive colleague, it's impossible to get a word in edgewise.

These scenarios are common in even the most inclusive workplaces because of _dominance hierarchy_. In any group situation, either a leader will be appointed or one will emerge. And because humans have been operating under hierarchical social structures for thousands of generations, we often don't even notice them. But these structures are a menace to cognitive diversity because they silence non-leaders, just like you were silenced in your meeting. Unless leaders foster open communication, ideas like yours are lost.

Can we overcome dominance hierarchy, then, by communicating on a more individual basis? Not necessarily. The death of twenty people on a flight to Portland in December 1978 tragically illustrates this. The plane crashed not as a result of landing gear failure — which was the pilot's initial concern — but because it ran out of fuel. The pilot's onboard engineer was aware of the problem but he failed to articulate its significance. The work culture, where the pilot was referred to as "sir" and questioning his authority was unfathomable, meant the engineer didn't speak up until it was too late.

Would doing away with leaders altogether solve the problems caused by dominance hierarchy? Unfortunately, without leaders we end up in a state of conflict and indecision. But by creating _psychological safety,_ leaders can harness the benefits of cognitive diversity by establishing an environment that encourages ideas sharing.

Surprisingly, it isn't that difficult to create psychological safety in the workplace. For example, Leigh Thompson of the Kellogg School of Management believes that using a simple method like _brainwriting_ — where employees contribute ideas by writing them down anonymously and then voting on the best idea — overcomes many of the challenges created by dominance hierarchy. Brainwriting ensures that all individuals have a voice and that ideas are considered based solely on their merit. 

Psychological safety is the most important factor in driving success, since generating ideas is a crucial step in arriving at optimal solutions. Not every idea will be brilliant, but it may lead to something ground-breaking.

### 5. To be truly innovative, a diverse team must be made up of individuals who are diverse within themselves. 

What do Ariana Huffington, Sergey Brin and Elon Musk have in common? Well, they're all successful entrepreneurs based in the US but they have something else in common too. They were either born outside the US or their parents were. And being an outsider primes the human brain for innovative thinking.

Innovation can occur slowly over time — as it does when a species evolves — or it can happen quickly when two concepts collide in an exciting and unexpected way. People who aren't frequently outside their comfort zones are less likely to experience these bursts of inspiration, because their minds have settled into what's familiar. This is why many influential innovators and entrepreneurs are migrants, or children of migrants, since they're not accustomed to the quirks and characteristics of their new homeland.

In navigating two or more cultures, the minds of migrants are trained to identify new possibilities. They see how their own ideas might fuse together to form great solutions and, importantly, they see how they could combine their own concepts with the concepts of others in original ways. This mindset drives innovation. 

So, how do we think innovatively without moving to another country? The answer lies in what we consciously expose ourselves to. We can diversify our thinking by ensuring that we don't become a slave to one area of interest, crossing conceptual borders if not geographical ones. This was a practice Charles Darwin adopted. Alternating his research between botany, zoology, geology and psychology gave him a fresh perspective and allowed him to draw ideas together across fields.

Similarly, it's important that the network of people we engage with is as diverse as possible, otherwise we might not encounter that crucial piece of information that would make our idea a success. History is full of examples where an idea didn't reach its full potential because it didn't have access to the right network. Just take the mathematician and engineer, Hero of Alexandria. In the first century AD he invented a steam-powered engine. But it seems that news of the engine didn't reach the right cart designers, who may have helped Hero fully utilize his innovation.

Continually engaging with diverse concepts and people working in other fields stimulates our brains, so we can generate great ideas. When we give these ideas the chance to fraternize with a broad community, we create the potential for ground-breaking innovation.

> "_The growth of the future will be catalyzed by those who can transcend the categories we impose on the world."_

### 6. To nurture our cognitive diversity, we must engage with other points of view in meaningful ways. 

Imagine you arrive at a party where you only know a few workmates. You're excited by the prospect of meeting new people. But when you leave a few hours later, you realize you didn't speak to anyone you didn't already know. You had a great night, but you didn't expand your social circle.

It's part of human nature to seek out people we identify with. Ironically, the broader our social options, the less likely we are to form diverse friendship groups. And while it's natural to gravitate towards people with similar interests and values, it means we're at risk of ending up in an _echo chamber_.

Echo chambers form when our own beliefs are constantly reiterated by the people around us, in person or online. And surprisingly, if opposing points of view worm their way into our echo chamber, they don't encourage us to question our own positions; they actually polarize us further.

This characteristic of the echo chamber — known as the _epistemic wall_ — was examined by mathematician Emma Pierson in 2014. She analyzed tweets concerning the death of a black man, Michael Brown, who was shot by a white police officer, Darren Wilson. Two distinct groups formed — those who saw Brown as a victim and those who saw Wilson as a scapegoat. When the groups interacted, tweets became personal attacks, with Wilson's defenders accusing Brown's chief defender of being a hate-spreading communist with a mental illness. This undermining behavior made rational conversation and consideration of a different perspective impossible.

How do we escape the echo chamber then, so we can soberly explore other viewpoints? The answer lies in forming meaningful connections with individuals, even if we are fundamentally different. This was the experience Orthodox Jew Matthew Stevenson had with fellow college student Derek Black. After learning that Black was an active advocate of white supremacy, rather than shunning him, Stevenson invited Black to Shabbat. Recognizing that Black was a product of his family environment, Stevenson worked to establish trust with him through their shared academic interests. Eventually, Black realized that he'd been indoctrinated by his family — who were his echo chamber. This led to him abandoning his political beliefs, despite the ramifications this had on his family relationships.

The friendship between Stevenson and Black highlights the transformative power of meaningful connections. Using empathy and trust, we can work together to dismantle echo chambers.

### 7. When we impose specific standards on people, we reduce the likelihood of successful outcomes. 

Imagine you're at a friend's house and someone accidentally spills a jug of juice on you. Your friend lends you some clean clothes but they don't fit. This is strange because you're both the same height and a similar weight.

Clothing is just one of many daily encounters we have with standardization. Since few of us can afford a bespoke wardrobe, we purchase clothes in predetermined sizes. But body shapes are diverse, even within a specific size range, which is why your friend's clothes might not fit you.

Standardization is practical in many scenarios — like clothes manufacturing — but it becomes a liability when it is forced upon people. This is because it denies our individuality and doesn't provide scope for us to play to our personal strengths.

The benefits of providing this scope were observed at Google in 2014, after a team of psychologists held a workshop with sales and administrative staff. The staff were encouraged to reflect on their talents and find ways to apply them to their work practices, rather than simply following an established system. Staff who attended the workshop went on to outperform their colleagues, reported being happier, and were an impressive 70 percent more likely to transition to a new role of their choice or be promoted.

When it comes to health, denying our diversity can have serious ramifications. For example, obesity is one of the world's most serious health problems. There are myriad diets available to combat it, but often the results are limited or short-term.

This was exemplified by computational biologist Eran Segal, who conducted an experiment on the topic in 2017. He measured the blood sugar response of healthy participants whose only source of wheat was either homemade sourdough or commercially manufactured white bread. He found that, on average, there was no difference between the two groups. However, there were significant differences in the individual results. This is because our stomachs house unique communities of bacteria which affects how each of us process what we eat. Diets don't take this into account, so unless we tailor our nutrition to our bodies, we won't necessarily become healthier even if we're eating a balanced diet.

Humans are unique, inside and out. A "one size fits all" approach may result in everyone being dressed but few will be dressed well.

### 8. Before we can harness the benefits of diversity, we must overcome our biases and be willing to listen. 

There's something very special about learning to read as a child. For many of us, books were an excuse to snuggle into a warm lap while an adult brought meaning to squiggles on a page. Slowly, those squiggles became letters, then words, then sentences, leading to that wonderful moment when we could read independently.

This ability to learn new skills from others sets our species apart. But if we really want to build our _collective intelligence_ — the combined intelligence of a group — we must overcome bias. 

Back in the 1970s, members of American orchestras were predominantly male because men were considered better musicians than women. Cecilia Rose of Princeton and Claudia Goldin of Harvard challenged this assumption by suggesting that musicians audition behind screens. Once this practice had been adopted, the number of female performers increased from five to nearly forty percent, proving that many talented women had been disregarded because of gender bias, and that the selection process had not been objectively merit-based.

Ageism is another bias that can limit success, something fashion brand Prada found out the hard way. Between 2014 and 2017, their slow engagement with digital channels led to a huge profit slump. By contrast, Gucci's sales grew by 136 percent between 2014 and 2018, courtesy of its digital strategies. This astonishing outcome was the result of a _shadow board_ — a group of young people who advised Gucci's executives on key decisions. Recognizing the valuable insights that its digitally-native younger employees could offer gave the company its edge.

Shadow boards highlight the importance of sharing wisdom and individual perspectives. Since complex challenges are too difficult to tackle alone, we must work in teams to solve problems effectively. Sharing ideas within a cognitively diverse team creates a mutually beneficial environment, where recipients gain wisdom and givers become connected within a supportive network.

When we put aside our biases and surrounded ourselves with minds filled with knowledge and experience that differs from our own, we create a team with broad understanding. This, ultimately, optimizes our chances of success.

### 9. Final summary 

The key message in these blinks:

**In the past, success has been the result of intelligence, skill and commitment. This is still a winning combination of qualities when we're faced with challenges that we can tackle alone. But the problems we face today are too complex for individuals to handle in isolation. To solve them, we must work as teams. And how we construct those teams will determine whether or not we attain our goals. Demographic diversity isn't enough to ensure we gain a thorough understanding of each hurdle we encounter; our teams must be cognitively diverse, with each team member contributing their knowledge and perspective in an environment that encourages communication. Only then will we avoid the dangers of homogenized thinking and arrive at truly innovative solutions.**

Actionable advice: ****

**Encourage cross-pollination at your workplace.**

Brilliant ideas often happen when employees have chance encounters with colleagues from different departments. Conduct an audit to see if you can find ways to encourage this type of incidental mingling, by reconfiguring cubicles which are closing people off from one another, locating facilities like bathrooms in places where different departments will intersect, or arranging seating so that diverse departments are positioned close together. It may seem inefficient or illogical but it'll help innovation thrive!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Black Box Thinking_** **, by Matthew Syed.**

As you've just learned, every team charged with solving complex problems will benefit from diversity on its journey towards success. But even the most intelligent and cognitively diverse team can fail. 

But failure doesn't have to be a wholly negative experience. In our blinks to _Black Box Thinking_, you'll learn how our typical reaction to failure — namely self-justification instead of honest acknowledgement — prevents us from reaching our full potential. Through case studies and anecdotes, Syed shows us that, despite the associated pain and shame, failure is one of our greatest assets.
---

### Matthew Syed

Matthew Syed is the author of five bestselling books including _Bounce_, _Black Box Thinking_ and _You are Awesome_ and an award-winning journalist for the _Times_. He also co-hosts the podcast _Flintoff, Savage and the Ping Pong Guy_ and is the co-founder of Greenhouse, a charity which uses sport to empower children.

