---
id: 5775431a3588860003e82425
slug: reclaim-your-brain-en
published_date: 2016-07-07T00:00:00.000+00:00
author: Joseph A. Annibali, M.D.
title: Reclaim Your Brain
subtitle: How to Calm Your Thoughts, Heal Your Mind and Bring Your Life Back Under Control
main_color: 2BA2D9
text_color: 1C688C
---

# Reclaim Your Brain

_How to Calm Your Thoughts, Heal Your Mind and Bring Your Life Back Under Control_

**Joseph A. Annibali, M.D.**

_Reclaim Your Brain_ (2015) is about how imbalances and quirks in the human brain can lead to serious problems such as anxiety, depression or addiction. These blinks explain the biological roots of these problems and what you can do to overcome them to get your brain back in check!

---
### 1. What’s in it for me? Take control of your life by better balancing your brain. 

Do you sometimes feel that your brain is buzzing, or that you're far too excited to focus on one single task? Do you feel driven by impulses, such as buying things you can't really afford? Or do you suffer from bouts of depression?

If you answered yes to any of these questions, then chances are your brain is out of balance. The human brain is a miraculous organ, yet it is easily disturbed ‒ anything from poor sleep, or overstimulation to painful experiences can upset its fragile balance. And basically, an unbalanced brain spells trouble.

In these blinks you'll learn holistic ways to rebalance your brain and address all the things that might ail you, from random moments of chaotic thoughts to crushing self-doubt.

In these blinks, you'll also discover

  * what Mr. Spock and your brain have to do with addiction;

  * why some students can do homework with a TV on at full volume; and

  * how to turn off your brain's amygdala without surgery.

### 2. A healthy brain has a good balance between the prefrontal cortex and the limbic system. 

The human brain is probably the most complex organ in the world. Fortunately, you don't need a PhD to grasp a basic understanding of how it works!

You just need to know two key areas: the _prefrontal cortex,_ or _PFC_, and the _limbic system_.

The PFC, which is the brain's most recently evolved region, serves as the control center. It's responsible for complex tasks like organization, impulse control, motivation and problem solving.

The PFC is particularly important in managing negative thoughts and emotions, because it helps you analyze them. That's why a well-functioning PFC is crucial to living a positive life. If a person's behavior was guided _solely_ by their PFC, on the other hand, they'd be like the Star Trek character Mr. Spock: perfectly logical and fully in control of all emotions or urges.

The second key brain area is the _limbic system_, the emotional center and one of the oldest parts of the brain. The PFC is tasked with keeping it under control.

The limbic system, also called the _primal brain_, is further divided into four main parts: the _basal ganglia_, the _anterior cingulate_, the _amygdala_ and the _thalamus_. It's responsible for our most basic survival needs, like escaping predators.

So when you encounter something threatening, like a lion, your _amygdala_ sets off an alarm that tells you to fight it or run away: a _fight-or-flight reaction_.

These emotional reactions helped our ancestors survive, but they can go too far in the modern world. If your amygdala takes over your PFC, you won't be able to calm down or think rationally. Negative emotions like anxiety and fear will take over instead.

So in order for your brain to function well, you need a healthy balance between your PFC and limbic system. Let's take a closer look at how that can go wrong.

> "If the limbic system is too strong and/or the PFC is too weak, you have an individual who is ruled by her passions and urges."

### 3. Every brain needs a unique amount of stimulation in order to work well. 

Imagine a company that's headed by an exhausted and overworked CEO, who can't be fired or replaced. How could the company ensure the CEO stays awake? How about hiring a mariachi band to play in their office all night? That would definitely stimulate them!

That's sort of how various factors stimulate different brain regions. A person with ADHD, for example, has an underactive prefrontal cortex. It needs to be stimulated, just like the CEO needs to be stimulated by the mariachi band to stay awake. That's why people with ADHD seek out certain kinds of unusual stimulation.

The author had a very bright client named Jeremy, for instance, who was thrilled to get into med school for the intellectual challenges it would present. Once classes started, however, he found that most weren't exciting, and consisted only of boring, repetitive exercises.

Jeremy's brain wasn't sufficiently stimulated, so whenever he tried to cram for exams he had to turn up the radio and television to keep himself awake - not a great way to study! People like Jeremy have to find ways to make such tasks less boring, or their brains won't be at their top performance.

Some people, on the other hand, have brains that react _too_ strongly to their surroundings. Any minor stimulus might trigger a fight-or-flight response, making them afraid of everything.

Some people have amygdalae so reactive they register even ordinary kinds of stimulation, like being around a talkative person, as threats. Such people regularly need to get away to a calmer environment, where they can be alone and recharge.

And over-stimulation or under-stimulation of certain brain regions doesn't just affect the way a person studies or reacts in social settings. It can also lead to dangerous addictions, as we'll see in the next blink.

> "Every moment of every day ... you are bombarded by sensory input that stimulates you - sometimes a little stimulation, sometimes a lot."

### 4. Brain imbalances can lead to addiction, but motivational interviewing can help. 

Have you ever craved alcohol when you felt overwhelmed, knowing it would calm you down? This impulse might seem benign, but it could over time turn into a debilitating addiction.

Addictions often result from brain imbalances. The author had a client named Jill who suffered from an overactive limbic system that left her constantly anxious and stressed. To cope, she smoked marijuana.

Another client named Bart had a weak prefrontal cortex so he had difficulty with self-control. He gradually became addicted to gambling and had accumulated $100,000 in debt before he finally sought professional help.

If you're struggling with addiction like Jill or Bart, a method called _motivational interviewing_ can help.

Motivational interviewing works by boosting your motivation and willpower and restoring balance to your brain. Give it a try by following the same steps that Bart followed while in therapy:

First, rate your motivation on a scale from one to ten, where one is not motivated and ten is very motivated.

Then ask yourself the following questions: What would your life be like if you made a major change? What would be the advantages and disadvantages of that change?

Now, rate your motivation again. You might find that it has increased, even if just a little!

Next, make a list of your strengths. If you need inspiration, try to think back to any time that you successfully made a life change.

Finally, figure out a small, first step toward your desired change. Every journey starts with a single step; it's okay if it's a small one. Bart's first step, for example, was to stay away from the racetrack, and say "no" to his brother-in-law's invitations to go.

### 5. Mindful meditation can calm you down and helps to rebalance your brain. 

You might be wondering: How can I rebalance my brain on my own? The good news is that you don't always need drugs, but can balance your brain with mindfulness.

_Mindfulness_ is a state in which you are conscious of your thoughts, your emotions and your place in the world — without judging yourself. You can practice mindfulness by taking the time to _meditate_.

Research that uses magnetic resonance imaging (MRI) scanning technology has found that meditation has the effect of quieting the brain's amygdala. That's why after you meditate, you feel calmer.

Meditation also increases activity in your brain's prefrontal cortex (PFC), making you feel happier and more at peace. In sum, meditation makes you feel better because it helps to restore balance to your brain.

Yet this isn't the only reason mindfulness can help you calm down. Mindfulness also allows you to distance yourself from unpleasant thoughts.

Imagine you're anxious about an upcoming surgery. Anxiety can be overwhelming in situations such as this. But if you mindfully focus on your anxiety by telling yourself, "Okay, I'm having anxious thoughts right now," you're one step closer to overcoming the anxiety, as you're examining yourself like an unbiased observer.

So when you need to calm down, try this simple mindfulness meditation.

First, sit in a quiet place. Close your eyes and focus on your breathing. Choose a word like "om" or "love" and repeat it every time you breathe out.

If your mind starts to wander, thinking of a recent conversation or even what you're going to have for dinner, try to let go and refocus on your breathing and your special word. You'll calm down and be able to listen to yourself more deeply.

> _"You should sit in meditation for 20 minutes a day, unless you're too busy; then you should sit for an hour._ " — Zen saying

### 6. Negativity is rooted in how the human brain is wired and manifests itself in many ways. 

When was the last time you received an unexpected phone call and immediately assumed the worst? This isn't unusual, as the human brain is hardwired to be more sensitive to negative stimuli.

The brain is divided into two hemispheres, each specializing in different tasks. The left hemisphere oversees logic and language, while the right hemisphere is more responsible for sensory experiences, such as looking at a photograph.

Scientists have found that the right hemisphere of the brain is more "negative" than the left. When a person suffers a stroke located in the brain's left hemisphere, for example, the victim's personality often becomes more negative, as the right hemisphere becomes more dominant. Conversely, people who suffer a stroke in their brain's right hemisphere tend to become happier, often even manic.

Interestingly, the right hemisphere of a child's brain develops first, so in general, humans develop a more negative view of the world from the start. By the time the left hemisphere catches up, we've already collected a range of negative early memories; these lay the foundation for a negative view of life.

Such negative thinking manifests itself in many ways, such as _black-and-white thinking_ and _mental filters_.

Black-and-white thinking means you think in extremes; your math skills are either terrible, or brilliant. This sort of false dichotomy ignores the "gray" or middle possibility: that your math skills are just fine.

Why is black-and-white thinking negative? It leads you to consider yourself a failure anytime you can't live up to the highest standards. If you don't ace that test, for example, you want to give up entirely.

Negative mental filters make you focus on the negative aspect of a situation, blinding you to any other aspects. If you have big ears, for instance, you might convince yourself that you're unattractive and conclude that you'll never find love.

But maybe you're really good-looking, despite your ears! Mental filters make you see only one part of the whole picture.

### 7. Challenge negative assumptions by writing down your thoughts and personal stories. 

We all tell ourselves stories about who we are and what the world around us is like. Unfortunately, these stories are often negative and inaccurate.

Negative thinking has a big impact on how we view ourselves. The author had a client named Carl who struggled with issues of self-esteem and had problems with his job as an accountant. No matter how hard he tried, he was constantly disorganized, losing documents or forgetting to charge clients on time.

Because of this, he saw himself with a negative filter and convinced himself that he was worthless and incompetent at his job.

Interestingly, Carl turned out to have attention deficit hyperactivity disorder (ADHD). He wasn't lazy or bad at his job, but he did need medication to help him realize his full potential. His disorder was the problem; but Carl instead blamed himself.

While medication helped to balance Carl's brain, he still needed therapy to rewrite the negative stories he convinced himself of over the years.

So how exactly does a person "rewrite" negativity?

The first step is to _write down your stories_. Thoughts rush constantly through your mind and can disappear before you're able to fully process them. Slow your thinking by writing down your thoughts!

Once you've put all your thoughts on paper, you can start to think harder about your assumptions.

It's crucial that you challenge any negative stories you've written down. Carl wrote, for example, that he was a terrible student and "didn't try hard enough." When the author pressed him, however, Carl realized he'd really been a hardworking student who usually got B's and B+'s.

Carl was able to think of himself more positively once he challenged his negative thoughts and memories.

Writing your thoughts down, examining them critically and rewriting your own story are all important commitments toward improving your mental health.

### 8. Improve your closest relationships by challenging the roles you and your partner play in them. 

If your personal life plays out like a narrative essay, with beginning, middle and end, then relationships represent dramatic plot points in your overall story.

You and your partner have to play the right roles in this story if you want to be happy. People naturally play roles with their partner, which is fine — as long as the roles aren't based on inferior or superior statuses.

Few people seek out an inferior role in a relationship, so partners often try to dominate each other. They compete for a superior role: the critic instead of the criticized, the victimizer instead of the victim.

Yet if one partner always has the upper hand, the other partner may feel resentful. Relationships based on these sort of emotions are stressful and unhealthy.

In a healthy relationship, on the other hand, partners have equal roles. These roles don't necessarily have to be similar, as long as they're equal. Partners shouldn't have to fight over dominance.

If you find yourself unhappy in your role, change it! Once again, mindfulness can help you here.

Ask yourself why you've taken on this particular role in your relationship. Maybe you're playing the victim because you feel guilty when you're assertive, for instance. If this is the case, psychotherapy might help you deal with your issues.

Or perhaps your partner habitually puts you down. You react defensively, creating a constant critic-criticized dynamic. If you fall into this sort of pattern, talk with your partner. Discuss how you can both change roles to make things better.

Playing a role like victimizer or victim can easily become a habit. Mindfulness helps you defeat such habits as it gives you a better understanding of your situation, allowing you to assess your behavior and regain self-control, instead of acting impulsively.

Over time, being mindful will empower you to overcome unhealthy behavior in your relationship.

### 9. Final summary 

The key message in this book:

**Maintaining a healthy brain comes down to keeping a balance between the more primal limbic system and the more recently evolved prefrontal cortex. If one area is overactive or underactive, it can cause problems such as ADHD, anxiety or addiction. This is where mindfulness can help! When you learn to focus your thoughts and view emotions like an outsider, you can control your behavior, make positive long-term changes and ultimately reclaim your brain.**

Actionable advice:

**Get your brain checked out.**

Sometimes the underlying cause of a brain imbalance is an injury. Even seemingly harmless actions like a fall when you were younger or too many soccer headers can have lasting effects. So if you're suffering from certain behaviors that you can't explain, visit a neurologist. You never know what they might find.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Brain that Changes Itself_** **by Norman Doidge**

How can stroke victims who become paralyzed start using a fork or buttoning their shirts again? Well, contrary to what was believed for so long, the brain is not hardwired. It can change, regenerate and grow. Drawing on real-life cases of scientists, doctors and patients, _The Brain that Changes Itself_ (2007) shows us how, rather than relying on surgery and medicine, we can alter our brains through thought and behavior.
---

### Joseph A. Annibali, M.D.

Joseph A. Annibali is the chief psychiatrist at Amen Clinics, located outside Washington, D.C. Annibali is also a leading expert in the use of single-photon emission computerized tomography (SPECT) brain imaging in treating neuropsychiatric problems associated with Lyme disease.

