---
id: 5725fee1f2d1b1000391f2ad
slug: red-notice-en
published_date: 2016-05-05T00:00:00.000+00:00
author: Bill Browder
title: Red Notice
subtitle: A True Story of High Finance, Murder, and One Man's Fight for Justice
main_color: C32731
text_color: C32731
---

# Red Notice

_A True Story of High Finance, Murder, and One Man's Fight for Justice_

**Bill Browder**

_Red Notice_ (2015) is a gripping, true story of one man's experience with fraud, corruption and violence in post-Soviet Russia. After discovering rampant fraud in Russia's investment market, the author found himself in a nightmare: first, he was declared a national security threat, then he feared for his life.

---
### 1. What’s in it for me? Enter the brutal world of Russian business in this real-life investment thriller. 

Some people's lives are made up of the stuff of blockbuster films. Such is the story of Bill Browder, one of the biggest foreign investors in post-Soviet Russia.

Browder rebelled from his intellectual, communist-leaning family to pursue a life of high finance, tackling with MBA in hand the wild-west investment landscape of the former Eastern bloc. He determinedly and quickly rose to a position of enormous financial power — and met head on local oligarchs who didn't take kindly to a foreigner encroaching on their plans for economic domination.

In these blinks, you'll learn how a fight for business transparency got Browder labeled a threat to Russia's national security. What's more, you'll realize that in the Russian "free market," there are plenty of people willing to kill to hold on to what they have — whether gained legitimately or not.

In these blinks, you'll also discover

  * how Browder's savvy thinking during Russia's privatization earned him $125 million;

  * why Browder had to spend some 15 hours in limbo in a Russian airport;

  * how one lawyer who discovered the truth died for confronting corrupt officials.

### 2. Born into a communist family, Bill Browder rebelled by embracing capitalism and getting an MBA. 

As Bill Browder boarded a plane on November 13, 2005, he didn't think that day would be different from any other. He often flew from his home in London to Moscow, where he worked.

But this time was different. When Browder landed in Moscow, the Russian customs officers wouldn't let him through. Instead, they detained him for 15 hours, without food or water, and sent him back to London without explanation.

This event baffled Browder. As the founder and CEO of Hermitage Capital, an investment company worth $4.5 billion, Browder was essentially the largest foreign investor in Russia.

What happened? To find out, let's start at the beginning.

Bill Browder was born in 1964 to an intellectual family with communist sympathies. Browder's father, Felix, was an accomplished mathematician; his grandfather Earl Browder was a labor union organizer from Kansas. Earl became the leader of the Communist Party of the United States and was invited to Moscow for an official visit in 1926.

Browder, however, found it difficult to embrace the left-wing beliefs of his family, so he rebelled — and chose to become a capitalist, instead.

Even after receiving an MBA from Stanford, Browder was still fascinated with his grandfather's legacy, an interest that inspired him to look for business opportunities in the former Soviet sphere.

In 1989, Browder joined the Boston Consulting Group and moved to London, as the focus of the company's London office was Eastern Europe. Later, in 1991, he joined Maxwell Communications Corporation, one of the only Western firms investing in Eastern Europe at the time.

His experience with Maxwell allowed him to explore nearly every country of the former Soviet bloc, and in doing so, he was a key player in many regional investments.

Before too long Maxwell was revealed to have been involved in financial fraud. Browder quickly left, and in 1992 took up an associate position with the Eastern European investment banking team at Salomon Brothers in London.

> _"My grandfather had been the biggest communist in America, and … I decided to become the biggest capitalist in Eastern Europe."_

### 3. The heady days of privatization in Russia presented a lucrative opportunity for Western investors. 

As Browder started his job at Salomon Brothers, he was given one simple rule: Generate five times your yearly salary in 12 months, or be fired.

As Browder pondered how to meet such expectations, he noticed that nobody was investing in Russia.

So he decided to plant the seed that Salomon Brothers invest in Russian businesses. This turned out to be an incredible opportunity, for both the investment firm and Browder.

As Russia made its transition from state-controlled communism to free-market capitalism, the government was giving away much of the state's property to private individuals. This was done through a process called _voucher privatization_. Russian citizens thus received certificates that represented potential shares in state-owned companies.

Browder knew that these vouchers were highly undervalued and could be easily purchased. On his recommendation, Salomon Brothers bought $25 million worth of vouchers, which it used to reinvest in Russian companies.

This idea proved successful. By 1994, Browder's investment portfolio was worth $125 million — ensuring that Browder had not only made five times his salary, but five hundred times!

Browder was suddenly a financial superstar. But once again, he felt it was time to move on. For him, the culture at Salomon Brothers was one of envy and greed — and he didn't want to be part of it.

Based on his already stellar reputation, Browder founded an investment company in Russia called Hermitage Capital. He attracted big investors, including Edmond Safra and Beny Steinmetz, to help start a fund for investing in Russian companies.

By 1996, Browder had raised $25 million. He moved to Moscow, where his winning streak continued.

As just one example, Hermitage invested $11 million in Sidanco, a large oil company in western Siberia. With its huge oil reserves comparable to other Western oil giants yet low production costs, Sidanco was a profitable investment.

But things were about to change. Browder was on a crash course with one of the most important business leaders in Russia.

### 4. Bill Browder fought Russian oligarchs and won, then a market crash bottomed out his fund. 

By the summer of 1996, Browder was riding high. Hermitage Capital was a success, having grown some 65 percent since its founding. But things were about to come crashing down.

Sidanco, the Siberian oil company in which Browder had invested heavily, was controlled by Russian billionaire and business oligarch Vladimir Potanin. In early 1997, the company announced that it would triple the total number of company shares and sell them at a price 95 percent lower than the going market rate.

What did this mean? All of Browder's shares in Sidanco would dramatically decrease in value, and his Hermitage fund would lose $87 million.

Sidanco's move was illegal, and Browder soon discovered that Potanin was deliberately trying to squeeze him out of the business. Potanin essentially didn't want a foreigner to get rich through his company.

Browder decided to fight back. He brought the story to the media, reaching out to newspapers and launching a legal investigation backed by the Russian Federal Securities and Exchange Commission.

A move this bold was unprecedented, as few Russians would dare to confront publicly someone as powerful as Potanin. People who crossed oligarchs often met with unsavory ends.

Potanin eventually reversed his decision to issue Sidanco shares. Thus, Browder won his battle, and he continued to do well with his investments in the oil giant.

In 1998, however, Browder's victory was overshadowed when the Russian stock market crashed on the heels of an economic crisis in Asia.

Browder had hoped that Russia could avoid contamination, but he was wrong. In the crash, his Hermitage fund lost $900 million, or 90 percent of its value.

Browder wasn't about to give up. He was determined to make up his losses, and in doing so, he set himself up for another run-in with Russia's richest and most powerful men.

> _"We're going to fight these bastards, that's what. We're going to war."_

### 5. By identifying pervasive corruption, Browder wasn’t lauded but declared a threat to national security. 

Despite his losses, Browder stayed on in Moscow. He was determined to recover his investments for his clients, no matter what.

This set him running headlong into another confrontation with Russia's powerful oligarchs. Browder soon discovered that many of these men were stealing from companies in which he was investing.

Browder had invested in Gazprom, a large Russian natural gas company. This firm managed an enormous gas field called Tarko Saley. Yet it turned out that Gazprom's executives had essentially stolen ownership of Tarko Saley, which contained some four hundred billion cubic meters of gas, worth billions of dollars.

Again, Browder made the details of his discovery public. He also brought to light the rampant corruption he uncovered in other companies that were part of his investment portfolio. As he shared his findings with international media outlets, he also pointed fingers at other corrupt Russian oligarchs.

His insistence that corruption be stopped helped Browder recover his investment losses.

The revelation at Gazprom pushed the company shares to new lows, trading at more than 90 percent under their original value. The issue was that the public had believed corrupt officials had stolen some 90 percent of the energy company's assets.

Yet Browder's investigations showed that the stolen assets amounted to only 10 percent. Once this was clarified, Gazprom shares jumped and Browder started recouping his losses.

By the end of 2003, Browder had bounced back from his 1998 losses, and his fund's value had risen more than 1,200 percent!

But again, his victory didn't last long. Browder's fight put him on a collision course with Russian President Vladimir Putin, as the oligarchs essentially formed Putin's power base.

This was the wall Browder ran into on November 13, 2005. The 15-hour detention, the inexplicable return to London? The Russian Foreign Ministry had decided Browder was a threat to national security.

> _"Many people have asked why the oligarchs didn't just kill me for exposing their corruption. It's a good question."_

### 6. Browder’s Moscow offices were raided by the police, and three of his companies were stolen. 

After discovering he was listed as a national security threat, Browder knew he had to act quickly. His first move was to get his core staff and his clients' money out of Russia as soon as possible.

Yet this move caused his clients to panic and bail out of the Hermitage fund completely. The fund eventually folded.

Matters only got worse. On June 4, 2007, Russian police raided Browder's Moscow offices, kept open on the small chance that Browder's visa would be reinstated.

Some 25 plainclothes officers, under the leadership of Lieutenant Colonel Artem Kuznetsov, showed up and grabbed everything they could, including computers, corporate stamps and seals.

The raid was justified in that officials claimed Browder owed the state a large amount of tax.

Browder's tax lawyer, Sergei Magnitsky, began an investigation into the state's claims. He found that not only did Browder's companies not owe any money but also that Browder had actually overpaid by $140,000.

More importantly, Magnitsky discovered that after the raid, ownership in three of Browder's companies had been transferred, without Browder knowing, to a man named Viktor Markelov.

It turned out that Markelov was a convicted murderer and known accomplice of Artem Kuznetsov — the officer in charge of the raids on Browder's offices!

How was this possible? Well, all that was needed to make the ownership transfer legit was Browder's official corporate stamps and seals, taken during the police raid.

But what was the reason for such subterfuge? It turns out that corruption in Russia ran deeper — and was even more brutal — than even Browder could imagine.

> _"When the Russian government turns on you, it doesn't do so mildly — it does so with extreme prejudice."_

### 7. An elaborate scheme to defraud the state resulted in the untimely death of Browder’s lawyer. 

Luckily Browder had moved most of his cash out of Russia before his companies were stolen from underneath him. But this begged the question — since the companies essentially had no assets, what exactly were the thieves up to?

Their plan, it turns out, involved using Browder's companies to defraud the Russian state of $230 million.

Artem Kuznetsov, the officer who led the raid on Browder's offices, was the ringleader, and he was in partnership with Pavel Karpov, the lead investigator for Browder's case.

Here's how it worked. First, they used Browder's seized official stamps and seals to forge agreements, making it appear that his companies owed $1 billion in liabilities to other companies. Then in a court hearing (about which Browder wasn't even aware), bribed lawyers confirmed that Browder's companies did indeed owe the sums indicated.

Interestingly, the amount of money that was being asked for exactly matched the earnings of the three companies, putting their combined profits at zero.

This allowed the fraudulent new owners to request a tax refund of $230 million from the state, the amount that Browder's companies had legitimately paid in taxes. In a final move, the two criminals bribed tax officials to ensure that the refund was approved.

But Sergei Magnitsky, Browder's lawyer, was keenly aware of what was going on.

In an attempt to expose the fraud, Magnitsky testified against Kuznetsov and Karpov. Browder worried that Magnitsky's actions might have serious repercussions and urged him to leave Russia. He refused.

In retaliation, Kuznetsov and Karpov had Magnitsky arrested. He went to prison a healthy man, but 12 months later, he died on November 16, 2009, after being denied medical care and tortured.

### 8. Tortured and denied medical care, Magnitsky’s cruel death led to the passing of the US Magnitsky Act. 

Shortly after Magnitsky's death, the Russian General Prosecutor's Office stated that the incident involved no violations of the law.

But the sad truth surrounding the death of Sergei Magnitsky paints a different picture.

Magnitsky was kept in an unheated cell, with no windowpanes to keep out the cold winter air. The toilet was open to the sleeping area, and raw sewage often bubbled up, covering the floor.

Magnitsky was also refused any contact with his family, a further act of psychological torture.

As Magnitsky's physical health weakened, he and his lawyer wrote more than 20 requests to every branch of Russia's penal, law enforcement and judicial system, asking that Magnitsky receive medical attention.

Some requests were simply ignored while others were officially denied. The Interior Ministry eventually posted an official statement saying that Magnitsky had never filed any complaints.

On the eve of November 16, 2009, Magnitsky was taken to an isolated cell, handcuffed to a bed rail and beaten to death by eight guards in full riot gear.

Seeking justice for Magnitsky's death, Browder appealed to politicians and human rights organizations.

Eventually, he received some support from the US government. On December 14, 2012, the Magnitsky Act was signed into law by President Barack Obama. It imposes financial and visa sanctions on Russian officials known to have been involved in Sergei Magnitsky's murder.

Browder still feels grief and guilt over Magnitsky's death. But the passing of the Magnitsky Act provides a sense of justice, a glimmer of hope at the end of a tragedy.

> _"What started out as a bill about Sergei had morphed into a historic piece of global human rights legislation."_

### 9. Final summary 

The key message in this book:

**Bill Browder was the largest foreign investor in Russia following the collapse of the Soviet Union. While embroiled in a fight with Russian oligarchs, he also became a target of corrupt Russian state officials. Browder was forced to leave the country while his lawyer, Sergei Magnitsky, was murdered in prison. These events led Browder to push for legislation in the United States to penalize those responsible for Magnitsky's untimely death.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Man Without A Face_** **by Masha Gessen**

A biography of Russian President Vladimir Putin, _The Man Without A Face_ shines a clear light on one of contemporary history's more shadowy political figures. The book charts Putin's almost accidental rise to Russia's highest office, starting from his benign beginnings in the state secret police. His vindictive personality, overwhelming greed and disdain for democratic norms continue to transform Russia today.
---

### Bill Browder

Bill Browder, founder and CEO of Hermitage Capital Management, was the largest foreign investor in Russia until 2005. He holds an MBA from Stanford Business School and before founding Hermitage, he was vice president at Salomon Brothers.

