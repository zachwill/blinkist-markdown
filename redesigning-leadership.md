---
id: 55c1280a3439630007390100
slug: redesigning-leadership-en
published_date: 2015-08-07T00:00:00.000+00:00
author: John Maeda
title: Redesigning Leadership
subtitle: Simplicity: Design, Technology, Business, Life
main_color: 407966
text_color: 407966
---

# Redesigning Leadership

_Simplicity: Design, Technology, Business, Life_

**John Maeda**

_Redesigning Leadership_ (2011) takes the jargon out of management and replaces it with a human touch. Being a great leader means taking to the shop (or office) floor, making contact with your employees and — most importantly — keeping their focus and respect. The author offers simple ways to be a manager who retains their principles — and heart.

---
### 1. What’s in it for me? Learn how to become the best leader you can be. 

Nowadays a manager, CEO or head of an organization can run everything without ever leaving the comfort of their office chair, managing by the click of a mouse. You're only as slow as your broadband.

But while the logistics of being the boss are undoubtedly easier in the modern world, that doesn't automatically make us better leaders. In fact, in our rush to use technology we have lost the personal, human touch required to get the most from our employees.

And if you want to inspire those employees as a real person rather than a management borg, you have to go further: the key to respect lies not in your job title, but in your dedication to your principles. These blinks are a guide to being an approachable, in-touch boss who gets things done.

In these blinks you'll learn

  * why the best leaders get out of their offices and get their hands dirty;

  * why you need to attract wannacomes to your meetings; and

  * why Obama had to apologize to an art history professor.

### 2. Creative and successful leaders manage by being mobile. 

How often do you see the head of your department walking around the workplace? Rare occurrence? Department heads usually stay in their offices unless they have to come out to lead meetings.

That means they don't connect with their employees very much — they just keep in touch through periodic meetings and reports. But you'll never truly understand your business and how it can be improved until you form strong connections with the people you work with. You need to get out of your office and interact with the people under you. In other words, you need to manage by walking around.

The author did this when he first got his post as the president of Rhode Island School of Design (RISD). He wanted to know everything about the school, so he left the office and got to know the people in the departments around him. He brought them food and even helped the new students carry their luggage when they moved in, among other things.

This style of personal management doesn't mean taking over other people's jobs, however. If you get too invested in working right alongside your employees, you might end up accidentally taking someone's work from them, or making all the important decisions yourself, so your team members have nothing to do but the menial tasks. That will make them feel useless, as they won't be contributing much to the team. Leaders need to lead, after all!

A manager has to find the balance between working with their employees and giving them their space.

### 3. Communicate clearly and give your messages a personal touch. 

Leaders have a variety of ways to communicate with their teams nowadays. If you can't meet your team in person you can always connect with them on Skype or by email. We can communicate with more people over wider distances, but this also presents problems. As a leader, how do you ensure that your team members fully understand the messages you're sending out?

There are many strategies for clear communication, but the best is to use examples to back up your points.

Facts are easier to understand when they're presented with an example — it makes the information more real to your audience.

Say you're sending an email about the necessity of taking responsibility for one's actions. You'll need to explain what you mean and define your terms. As an example, you can point out that another employee will have to stay late if the others don't do their work properly. That will help drive your point home.

These skills are important but remember that it's still always best to talk face to face. There's no substitute for the warmth of personal communication.

It's not possible to meet in person with everyone all the time, however. So give your virtual messages a human touch when you can't make it.

The author once had to send an email to a campus at his college. He wanted to meet all the recipients in person but he knew that wasn't possible, so he gave the email a nice personal touch: he attached a scan of a handwritten letter.

The special touch worked. A lot more people responded to the email than usual — a bit of warmth made a big difference.

### 4. Get your team members together regularly in short and efficient meetings. 

Imagine your company has just created a product and they need to assemble a new team to manufacture, market and sell it. You've just been appointed leader. What's the next step?

The first step to making people feel like they're part of a team is to get them all together in the same room. Teams don't feel especially real when they're first formed, but when the team members actually meet they'll start feeling like they're part of a group. After all, they're united because they're working toward the same goal.

This meeting won't be the last, of course. A team has to meet several times over the course of a project to discuss their strategy or work out any problems that arise.

There are three types of people who come to meetings: _wannacomes_, _havetacomes_ and _wannaeats_.

Wannacomes do actually want to be there, havetacomes don't, and the wannaeats are just there for the food. The best meetings are always those that are dominated by the wannacomes. If you want to make sure they'll turn up, focus on keeping your meetings as snappy as possible.

That means when you write up the agenda, you should only include the really important issues that need to be discussed. Wannacomes might not attend meeting about things they're interested in if it means they'll have to listen to pointless discussions about things they're not interested in too.

So keep your meetings brief. Short running times for meetings ensure that everyone stays on topic and keeps productive. And, more importantly, it'll be more appealing for everyone who's invited — the havetacomes and the wannaeats might just turn into wannacomes.

### 5. Stick to your principles and show your human side. 

Being a strong leader is about getting the job done — it's not about morals or principles, right? Wrong.

You don't automatically become a leader because you have a great idea. You also need a solid method for pushing it forward into reality.

The story of Jerome Wiesner, the former president of MIT, illustrates this. When he was younger, Wiesner served as a physicist for the Manhattan Project that developed the atomic bomb during the Second World War.

After Wiesner saw the destruction of the war, he devoted his life to "fostering world peace and integrating the arts into the sciences at MIT."

Wiesner's ideals turned him into a leader. He was so devoted to them that he started overseeing actions to realize them. He ended up becoming a leading voice in the global discussion on nuclear arms.

So good ideas are important but they don't make you a leader. _Ideals_ do.

And your principles shouldn't just guide you at work — if you want to be a great leader, they must guide the rest of your life too.

The author, for example, gets a number of benefits from being the leader of a college. He has a private car with a chauffeur, a mansion and membership in a private dining club.

He doesn't use any of these privileges, however. Why?

Well, in a meeting during the financial crisis, he was once asked to imagine a parent struggling to pay for the child's fees. He was asked how she might feel if she saw a faculty member getting those kinds of flashy benefits.

The author has used this as a guideline for himself since then. He's careful to think about how his image comes off. He knows that using his privileges as the president of RISD would go against what he believes in.

Don't forget that at the end of the day, leaders are just people. They're influenced by their environment just like everyone else. So show your team members your human side! They'll respect you if you live by your ideals. Leading by doing is always the most effective method.

### 6. Take the time to earn your team’s respect. 

As you might imagine, a great leader needs a great team. So you need a team that supports you, but how do you earn their support?

Great leaders always truly respect the people they're working with. The best leaders usually go out of their way to show their staff that they appreciate them.

Ikko Tanaka, one of Japan's greatest designers, embraces this idea. He's the mind behind several successful Japanese brands, including MUJI. He once invited the author to join him and his entire staff for dinner.

When the author arrived, he was surprised to discover that Tanaka had prepared all the food himself. He also spoke about how thankful he was to have his staff because he knew they were the ones behind his success. He wanted to show his thanks and keep their respect.

You can also keep your team's respect by acknowledging and apologizing for any mistakes you make.

It's rare to find leaders that do this publicly. In fact, a high profile figure once apologized to the author for being hostile during a heated public meeting, but he asked him not to tell anyone about it.

Don't do this. Admit your mistakes publicly — it shows your humility.

President Obama, for example, has apologized to the public several times. In February 2014, he even apologized for making a joke about art history majors. Ann Collins Johns, an art historian at Texas University, was so upset that Obama sent her a handwritten letter of apology, which she accepted.

You can't always be right, even when you're a leader. If you admit to your mistakes, people will respect you a lot more.

> _"Respect is constantly earned, and shouldn't be assumed because of your position."_

### 7. Final summary 

The key message in this book:

**When you become a leader, don't forget that you're a still a human being too. So live by your ideals, connect with your employees in personal ways and communicate as clearly as you can. Keep your meetings on point and remember that respect isn't guaranteed — it's earned. Only a great leader can inspire a great team.**

Actionable advice:

**Don't take your team for granted.**

Give your team members freedom - they'll perform better if they're given space. So trust their creativity and don't forget to show them your appreciation. Teams should be founded on respect.

**Suggested further reading** ** _: The Laws of Simplicity_** **by John Maeda**

_The_ _Laws_ _of_ _Simplicity_ consists of a set of "laws" formulated by the author to try to grasp the meaning and essence of simplicity. Along the way, it provides useful advice on how to introduce simplicity to our daily lives, business and product design.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### John Maeda

John Maeda is an author, computer scientist, graphic designer and the former president of the Rhode Island School of Design. In 1999, _Esquire_ named him one of the 21 Most Important People of the Twenty-First Century.

