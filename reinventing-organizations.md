---
id: 54d9dde5336661000a290000
slug: reinventing-organizations-en
published_date: 2015-02-12T00:00:00.000+00:00
author: Frederic Laloux
title: Reinventing Organizations
subtitle: A Guide to Creating Organizations Inspired by the Next Stage of Human Consciousness
main_color: 23AEAD
text_color: 187A7A
---

# Reinventing Organizations

_A Guide to Creating Organizations Inspired by the Next Stage of Human Consciousness_

**Frederic Laloux**

_Reinventing Organizations_ discusses why companies around the world are getting rid of bosses, introducing flat hierarchies and pursuing purpose over profit. And ultimately, by adopting a non-hierarchical model, these organizations _thrive_.

---
### 1. What’s in it for me? Learn about an organizational structure that will change the way we work. 

How do you decide what you should do everyday at work? Most people, if not self-employed, are guided by the whims of their superiors.

Even if you have a certain degree of independence in the office, what a manager says, goes!

Why is this the case? Most of us still work in hierarchical organizations. There's a CEO at the top, followed by layer upon layer of management, and the employees at the bottom.

This rather feudal type of system today is outdated. But how can we change it? Which system works better? In these blinks, you'll discover the organizational method that will come to dominate the workplaces of the future. So to get a head start, read on!

In the following blinks, you'll discover

  * why you should be allowed to bring your dog to work;

  * why one company pays new employees $3,000 to leave; and

  * why the main job of CEOs in the future will be to not exercise power.

### 2. Human organizations have transformed in stages over the course of history, and continue to evolve. 

Think how much we have advanced in the past 10,000 years. From scattered groups of hunter-gatherers, humans now live in booming, crowded cities organized in nation-states.

A similar transformation also occured on the level of organizations. And in fact, psychologists have identified concrete stages, organized by color, describing how this occurred.

Our ancestors existed in the RED stage. During this period, organizations were small and violent, based on fear and an "I want it, so I take it" philosophy. A leader of a RED organization constantly needed to assert his power and dominance over the rest of the group, as if he showed any sign of weakness, someone else would take his place.

The development of agriculture led to the AMBER stage. During this period, planning became increasingly important but the rigid hierarchies of the past remained.

For instance, the Catholic Church was founded on dogma and strictly guarded hierarchies, oriented toward consolidating and holding power at the top. This structure was considered God-given, which is still the case today; while heretics are no longer hanged, the pope's status is still unquestioned.

ORANGE organizations came next, somewhat looser to foster innovation and creativity. Many of these organizations operate according to the principle of _management by objectives_, meaning that leadership doesn't care _how_ you do something, as long as objectives are met.

Although the ORANGE model is common at big, multinational companies, GREEN organizations have also emerged. GREEN companies break hierarchies down even further, centering work around a strong shared culture.

For example, at Southwest Airlines, staff are encouraged to make the company a fun place to work. To that end, one stewardess brings her hobby to work, playing the harmonica to entertain customers on board.

But the process doesn't end here. There's an even more progressive stage of organization, the TEAL organization...which you will learn about in the next blinks.

> _"We cannot solve our problems with the same thinking we used when we created them." — Albert Einstein_

### 3. “Flatten” your organization and get rid of bosses to put decision making on everyone’s shoulders. 

New kinds of companies are emerging worldwide, indicating that we're entering the next stage of organization: the TEAL stage, which takes self-management to a higher level.

In TEAL companies, bosses are eliminated and replaced by flat hierarchies, granting every employee the power to make decisions independently.

Why are so many companies adopting the TEAL model? One reason is that self-management makes organizations more effective and more profitable.

Consider the Dutch home nursing company Buurtzorg. Rather than having a manager responsible for making decisions, management tasks are distributed across all nurses. This allows each person to independently decide which priorities to follow.

This approach is actually hugely efficient. According to a 2009 study, Buurtzorg ends up spending 40 percent less time with each client.

And especially when you consider the fact that Buurtzorg nurses spend time having coffee and chatting with clients, and not simply counting the minutes of each visit and treating customers like "products," the findings of the study are impressive.

It's also worth noting that the study estimated that by adopting the Buurtzorg model as a home care standard, the Netherlands could save up to $2 billion Euros every year.

As you can see, the TEAL model is effective, but what is it that makes it effective?

Removing bosses and middle management stimulates employees' personal abilities, leading to a more motivated and professional workforce.

If you don't have managerial hierarchies, people simply can't dump decisions on their colleagues. They have to face problems themselves, even when it's tough. So although this system demands much more of every employee, it's also hugely rewarding.

To that end, nurses at Buurtzorg were surprised by how much more motivated and energetic they felt without having bosses making all the decisions and ordering staff around.

### 4. In the new organization, employees follow an advice process to ensure efficient decision making. 

If everyone is their own boss within an organization, wouldn't that lead to chaos?

Well, no. Ultimately, the TEAL model relies on an _advice process_ to ensure efficient decision making.

Anyone within a TEAL organization can make decisions, as long as they first seek advice from affected parties and those with expertise. It's important to note that even if one of these advice-givers opposes the idea, the final decision lies with the person seeking advice.

For example, when Shazad Qasim, a financial analyst at U.S. electrical company AES, told the CEO that he wanted to research the possibility of setting up a power plant in Pakistan, the CEO was skeptical.

But since the company followed the advice process, Qasim pursued his idea anyway. And eventually, Qasim himself, and not the CEO or the executive board, decided to invest $200 million in the new plant. Two years later, the plant was a phenomenal success, worth $700 million.

Another important feature of TEAL organizations is that employees don't have strict job titles but rather a number of roles. And as long as they follow the advice process, employees are free to create new roles for themselves.

Formally, this occurs according to an organizational model called _holacracy_.

In a holacratic company, roles and collaboration are discussed in regular company-wide "governance meetings." These meetings typically follow a strict process and are guided by a facilitator, to ensure no single person can dominate decision making.

During these meetings, anyone who feels the need for a new role or some other change can propose the idea and have it addressed fairly.

Although this might seem like a rigid process, research shows that most employees find it liberating and highly efficient.

### 5. Dogs at work? Meditation time? New companies make room for who you are as a whole person. 

Why do our workplaces so often feel like depressing factories? And why is there often a disconnect between work and the rest of our lives?

Wouldn't you like to work at an organization which allows you to bring your whole self to work? And wouldn't you like to bring your dog to work, too?

TEAL organizations promote a more personable atmosphere that makes room for not just the worker but the whole person. For instance, the Colorado-based spiritual publishing company SoundsTrue allows dogs in the office (as long as owners follow a "three poops and you're out" rule).

That might seem like a small perk, but employees say that allowing dogs in the workplace has had a calming effect on the company, which has in turn led to better decision making. It's also help build a sense of community among the company's 90 employees.

As part of an emphasis on "humanized" environments, TEAL organizations also actively encourage and create space for _reflection_.

Heiligenfeld, a German medical rehabilitation company, is a great example of this principle. Once a week, employees meet for one hour to communally reflect on a relevant topic, including conflict resolution, company values, risk management, mindfulness and dealing with failure.

This practice is a huge success. When Heiligenfeld was named Europe's "Best Workplace" in the healthcare sector, many employees specifically mentioned these sessions as a positive aspect of working at the company.

But although these reflection sessions are effective, reflection doesn't have to be an organized thing. To that end, many TEAL organizations simply encourage employees to spend 15 or 20 minutes silently meditating at their desks.

> _"Extraordinary things begin to happen when we dare to bring all of who we are to work."_

### 6. Design a recruitment process that supports your company culture and promotes honesty. 

Job interviews can be uncomfortable. It's easy to feel nervous when you have to present the best image of yourself! Interviewers too are in the hot seat, convincing you to consider a job.

Yet is this really the best way to match job seekers to jobs? Isn't it time to try something new?

TEAL organizations approach recruitment differently, with processes designed to give both the applicant and the employer a true sense of the other party.

Interviews, for instance, are handled not by human resources personnel (who are motivated by recruitment target numbers), but rather by future teammates, who can be more honest about the real nature of the workplace.

Online retailer Zappos actually offers $3,000 to any new hire who decides to leave the company during the first four weeks of orientation. This ensures that only people who truly want to be part of the company stay on as employees.

And the numbers speak to the effectiveness of this approach: So far, only 2 percent of new hires chose to take the money.

The Zappos model highlights another important feature of the TEAL hiring process, as such companies spend much more time onboarding new employees. In fact, company CEOs will often personally participate in every stage of the new-hire training process.

After all, since TEAL organizations prioritize self-management, new hires typically need more time to adjust to the idea of full autonomy with respect to decision making.

And to ensure that employees can make decisions which serve the company as a whole, TEAL trainings emphasize company values, communication processes and dealing with failure.

### 7. Purpose trumps profit in the new organization; sharing best practices helps everyone win. 

Greed is good. This infamous Wall Street motto pretty much sums up the dominant philosophy of most companies. Profit trumps everything; and a ruthless competitive attitude is a must.

Yet things are starting to change. Today, more and more companies are starting to value purpose and cooperation over profit and competition.

Unlike other traditional organizations, where most employees (and CEOs, even) typically don't know what the company's mission statement says, TEAL companies are driven by an overarching purpose.

Outdoor clothing retailer Patagonia, for example, aims to raise environmental standards in the clothing industry. Patagonia is so committed to its mission that it has even produced ads with the slogan, "Don't buy this jacket," with the idea that most people already own too many things. 

Patagonia also tries to reduce waste by only producing clothing that's highly durable, and offering customers a service for repairs.

In addition to a mission-driven approach, TEAL organizations are also happy to share their knowledge with industry competitors.

For instance, the CEO of home care provider Buurtzorg regularly accepts invitations from competing companies to share insights and explain the company's operations.

Buurtzorg doesn't see its operational strategy as a secret formula that needs to be protected at all costs. Instead, the company believes that the more you give, the more you get back.

And since the true purpose of Buurtzorg isn't simply to make money but rather to help people live more healthy and meaningful lives, it makes sense to share knowledge with companies that want to contribute to their overarching mission.

> _"Profit is like the air we breathe. We need air to live, but we don't live to breathe."_

### 8. CEOs no longer call the shots, but foster an environment of trust to keep the hierarchy flat. 

In a self-management structure, many of the decisions typically made by CEOs are distributed throughout the workforce.

In short, there are no shots to call, no targets to set and no one to fire. So what's left for the CEO?

CEOs of TEAL companies instead focus on maintaining flat hierarchies by promoting trust.

Think about it like this. At traditional companies, when something goes wrong, management responds by imposing new controls. However, TEAL company CEOs deal with setbacks by promoting trust and trying to prevent old practices and hierarchies from sneaking into the organization.

When the CEO of Philadelphia-based non-profit Resources of Human Development (RHD) discovered that an employee who managed company cars had secretly given one to her son, some employees demanded more supervision and control over employees in general.

But the CEO didn't think that one unfortunate incident should restrict employee freedom. So he persuaded the team to keep trusting each other and reject new controls.

In addition to promoting trust, TEAL company CEOs also serve as role models for self-management. Even within a flat hierarchy, the CEO is still the face of the company in many respects. Thus it's important she embody the principles of self-management and resist an urge to rule from above.

For instance, when RHD's CEO thought it was the right time for the organization to start helping convicts with drug addiction and other issues, he organized a meeting for employees to discuss the idea. At the end of the meeting, employees agreed to select a "point person" to take the lead. And from there, the project took on a life of its own. 

So as you can see, the CEO managed to advise the company without overstepping the bounds of self-management and telling employees what to do.

### 9. Change doesn’t happen quickly. Slowly introduce elements of self-management to your firm. 

Turning a more traditional company into a TEAL organization isn't about tweaking the small details. Rather, it's about changing the very heart of the operational structure, introducing a radically new mind-set.

It's a challenging task, and you have to be ready for it.

Convincing people of the advantages of TEAL practices, and _especially_ of self-management, isn't easy. In fact, the chief of AES says that whenever the company opens a new factory, employees are skeptical of the organizational structure, coming up with all kinds of objections to self-management.

Other CEOs and leaders who have introduced self-management to their organizations have had similar experiences, noting that lower-level employees, or those typically with little influence in a traditional organization, are the most positive about the prospect of implementing TEAL practices.

However, middle and high-level managers really struggle in accepting a loss of power.

Another challenge of introducing a TEAL organization to a workplace is getting people used to the idea of bringing their whole selves to work. That's because employees who have spent their lives working in more traditional companies find this principle intimidating, even frightening.

So ultimately, the best way to win them over is to introduce some of the associated practices (like daily meditation) gradually.

For instance, start by replacing performance reviews, which typically look at numbers and targets, with discussions about the employee's passions and strengths, and how the employee could benefit the company by bringing these passions to the workplace.

You could also try some non-hierarchal group approach methods, like _Future Search_, based on the idea of inviting all employees — sometimes even partners, clients and suppliers — to voice their passions and concerns to help shape the future of the organization.

These kinds of meetings are typically very energizing for the company, and demonstrate the advantages of the TEAL model.

### 10. Final summary 

The key message in this book:

**All around the world, more and more companies are eliminating traditional organizational structures and introducing flat hierarchies. And by emphasizing self-management, embracing employees' whole selves and prioritizing purpose over profit, these companies are creating a new model for an effective, happy workplace.**

**Suggested further reading:** ** _Scaling_** **_Up_** **_Excellence_** **by Robert I. Sutton and Huggy Rao**

_Scaling_ _Up_ _Excellence_ is the first major business publication that deals with how leaders can effectively spread exemplary practices in their organization. Readers can expect to learn about the latest research in the organizational behavior field, lots of instructive industry case studies, and many helpful practices, strategies and principles for scaling up.

The authors help leaders and managers understand major scaling challenges and show how to identify excellent niches, spread them and cultivate the right mindset within their organizations. They also set out scaling principles that guide leaders in their daily decisions.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Frederic Laloux

Frederic Laloux is a former strategy consultant for McKinsey & Company. Today, he advises companies on how to adopt new organizational structures and practices.

