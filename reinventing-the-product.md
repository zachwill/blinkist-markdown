---
id: 5e435bd66cee0700068c4e5f
slug: reinventing-the-product-en
published_date: 2020-02-13T00:00:00.000+00:00
author: Eric Schaeffer and David Sovie
title: Reinventing the Product
subtitle: How to Transform your Business and Create Value in the Digital Age
main_color: None
text_color: None
---

# Reinventing the Product

_How to Transform your Business and Create Value in the Digital Age_

**Eric Schaeffer and David Sovie**

_Reinventing the Product_ (2019) takes an in-depth look at what it takes to compete in today's increasingly digitized marketplace, outlining all the steps a company needs to take to pull itself out of the past and into a future where the marketplace is ruled by smart, digitally connected products.

---
### 1. What’s in it for me? Learn what it takes to bring your company into the digitally connected age. 

Some businesses still have a twentieth-century way of doing things. They want to sell the same kind of products people were buying 50 years ago and operate just like they did two generations ago. Unfortunately for them, the days of traditional product-making are no more. For some 200 years, making and selling a product was a straightforward process of sourcing raw materials, taking care of labor and manufacturing, and then selling your product. 

Today, the point of sale is just the beginning of an ongoing transaction between businesses and their customers. As authors Eric Schaeffer and David Sovie see it, businesses today need to offer not just any products, but _Product X.0s_ — digitally-connected smart products that deliver valuable customer experiences.

Over the course of these blinks, you'll learn more about what that means and why it's important.

In these blinks, you'll learn

  * what the outcome economy is all about;

  * why you should be thinking about platforms; and

  * how many pizzas it should take to feed your biggest team.

### 2. Any business eager to grow in today’s marketplace must heed certain digital imperatives. 

Let's say you want to hang up a piece of art in your living room. You could buy a drill and all the materials to do it yourself. If you're handy, that might be the way to go. But it could also be time consuming and expensive. A better alternative might be to hire a company that already has all the materials and promises to hang your new painting quickly and perfectly. These types of companies represent the future. Rather than selling products, they sell _outcomes_.

This move from selling products, like hammer and nails, to selling services and outcomes, hasn't been easy for many legacy businesses used to doing things as they've always been done. These businesses need to consider certain digital imperatives if they're to experience growth and prosperity in the years ahead.

The first of these imperatives is the need to transform the core of your company to match the digital era. From engineering and production, all the way to sales and support functions, your company's many facets need to be digitally connected with each other — and with customer feedback — every step of the way.

This includes having a digital-ready workforce that's ready, willing, and able to be fast and flexible. Today, prototypes can be quickly tested with software designed to identify problems before they happen. Getting a product from idea to the marketplace is quicker than ever before. And once a product is in customers' hands, it's not uncommon for software updates to start going out the very next day. Your workforce needs to be able to work in harmony with this kind of constantly-evolving ecosystem.

In the same vein, your company needs to be aligned with the outcome economy, and move away from being product- and point-of-sale-minded. Your entire business model should be focused on creating experiences and nurturing long-term customer relationships, rather than looking at the point of sale as the end goal.

This also means making sure your company is managed with the understanding that multiple pivots may be required to stay afloat. If changes need to be made, your company needs to be built in a way to make them quickly and smoothly.

### 3. To thrive in the modern world, businesses need to shift their focus from product features to customer experience. 

If you need to take your company out of the dark ages and into the modern, digital world, there are several major shifts that need to take place. One of the most important ones is to shift your focus away from products as we've traditionally understood them and toward the ongoing journey of the customer experience.

Let's say you make cameras. Traditionally, camera-makers may have spent the majority of their time and effort thinking of new features they could put on the camera in order to make it stand out in the marketplace. But today that is just one step to consider.

A customer's experience begins with exploring and looking for the type of camera that suits them best, so it's important to know what your customer is searching for, including the types of cameras they're after and the kind of reviews they're looking at.

Next, you should also consider how easy it is for the customer to find and test your product — what is the in-store experience like? 

Once a sale is made, there are many more experiences to consider: What's it like to open the box once you've had it delivered? What's it like to actually use your product and to share that experience with others? If you're selling cameras, how easy is it to share photos? What's it like trying to get advice on ways to achieve the best possible experience with your product? 

Finally, you need to consider all the possible accessories and software updates that will allow customers to continue adding value to their purchase while you continue to strengthen your relationship with them. 

Think of Tesla cars and how the company is constantly updating the software that runs the car, and how that continual stream of improvements can make a years-old Tesla more valuable than it was when it was first purchased. This is the kind of shift in business plan that today's companies need to make in order to compete with the best.

### 4. Businesses should start thinking of their products as services and become platform-oriented. 

Netflix, Tesla, Uber, Apple, and many more of today's most successful businesses all sell products that are essentially services, and this is a trend that will only increase as more businesses look at ways to keep customers engaged in the long term.

There are a few key factors when transforming into a _product-as-a-service_ company. The first two are identifying a clear and viable strategy for your product and shifting your operating model to fit today's digital ecosystem. For example, companies that used to sell cars are now shifting into developing ride-share programs, and companies that sold medical testing devices are shifting to selling software that analyzes medical data.

The third is changing the way you approach research and development. This is important because, in the experience economy, it's less about a customer _owning_ a product and more about the ongoing relationship that comes with them _using_ that product.

For some legacy businesses, research and development is more of an afterthought than a main priority, but in today's increasingly digital economy, R&D must be front and center. Software and technology are constantly changing, so you need to have teams devoted to staying abreast of the latest developments, testing new possibilities, implementing changes, and monitoring feedback data. The service you're providing needs to be constantly improving.

One area that requires an even bigger kind of shift is platforms. The simple fact is, most of today's top companies belong to a platform of some kind — either one they've created or an existing one they've joined.

For most app-based companies, the main platforms are Apple's App Store and Google's Play Store. Huge tech-based companies like Amazon and Alibaba also have platforms that allow them to gain both value and exposure by bringing businesses together as part of a powerful system.

In many cases, platforms give you a seamless way to provide regular updates to customers who are subscribed to your service. In other cases, a platform may be a way for your company to create a mutually beneficial partnership with other companies.

For example, many car companies, including BMW, Renault, and Nissan, are creating platforms for their vehicles that integrate weather services from one company, artificial intelligence like virtual assistant technology from another, and streaming entertainment from yet another. The potential in building your own platform and bundling technologies together this way is clear, but if doing so isn't within your capacity, considering existing platforms can also be a great fit.

### 5. Today’s businesses should also shift their attention to AI and building an agile operating model. 

You've probably heard about recent strides in the development of artificial intelligence, or AI, especially when it comes to the advanced algorithms allowing services like Apple's Siri and Amazon's Alexa to learn the preferences and anticipate the needs of users.

But AI is about more than just digital assistants. That's why embracing AI is also important for all businesses.

These days, AI has four human-like capabilities: it can _sense_ things, as it does when performing facial recognition; it can _comprehend_ things, such as language and verbal commands; it can _act_, as it does when making recommendations; and it can _learn_. All of these capabilities are finding their way into one industry or another, and any company that chooses to ignore AI will likely be left behind.

Currently, around 70 percent of manufacturers worldwide identify AI as a key part of their future innovation and growth. Yet only 19 percent of those companies have presented a clear and specific roadmap for utilizing AI. However, AI has clearly contributed to increased value and accelerated growth for the companies that have already added it.

Take the smart speaker, for example. This was a non-existent product just a few years ago, yet in 2018 alone there were around 56 million smart speakers sold worldwide. Meanwhile, experts are suggesting that the use of voice assistants and smart, digitally connected household appliances will only continue to rise in the years ahead. 

This brings us to one last big shift for companies: to stop thinking linearly and to focus on building an agile operating model. The marketplace and technological landscape will continue to change rapidly in the future and having traditional top-down hierarchies will make it difficult for teams to act quickly and make the decisions that need to be made.

Amazon founder Jeff Bezos advocates for small, empowered, product-minded teams with a focus on experimentation. As he sees it, twice as much experimenting will lead to twice as many ideas. Amazon also has a "two pizza" rule — if your team can't be fed by two pizzas, it's too big.

### 6. Digitizing your core business should be a key step on your roadmap to success. 

Now might be a good time to put everything together into a clear roadmap for transitioning into a successful product-as-a-service company. There are seven key action points on this roadmap.

First is to define your vision and the value you're offering. What kind of experience is your product providing? Be clear about this and know precisely the kind of connected product you're selling.

Second is to digitize your core business. This means enacting all the shifts, pivots and structural changes we've already gone over. This can sound like a costly step for legacy businesses that are used to doing things in a traditional top-down way. Fortunately, the authors have found that implementing a digital transformation program usually ends up reducing costs, meaning that the transition effectively pays for itself.

Third is to outline your product experience and how it relates to your business operations. For example, how much data are you getting from the way customers interact with your product? Do you have enough storage for that data and a plan to utilize and learn from it? And, perhaps most importantly of all, is your data secure enough?

Fourth and fifth are creating a workplace and a workforce designed to accelerate your new business and enable a smooth execution of your vision. Remember, a digitally-focused workplace culture embraces rapid testing, learning, and pushing forward new ideas. Instead of thinking of your business as a factory for manufacturing, the authors suggest thinking of it as a "_product and experience innovation center._ " For this to happen, remove the blockers that keep teams isolated and make sure everyone is working with the same data and goals.

Action point number six is to track your results and be willing to constantly adjust course. Smart connected products provide an endless stream of feedback. Teams and programs need to be set up to analyze this data and act on it. 

This means a certain production cycle, which starts with the refining of a product idea and then moves on to building, testing, and learning from the product's initial iteration. Next comes planning, executing, and assessing the product. At each stage, the product continues to evolve through the tracking of results.

Finally, there's no time like the present, so start your pivot _now_.

### 7. Faurecia is just one company that’s gracefully pivoted to success in today’s digitally connected age. 

Theory is all well and good, but what does a company that's successfully transitioned to producing smart connected products look like?

One such company is Faurecia. It supplies automotive companies with products such as car seats and vehicle interiors, and in 2015 it found itself in debt and in need of change. Around 2016, the company put new leadership in place and read the writing on the wall: it was time to go all-in on digital systems integration and intelligent automotive cockpits. After all, the clear trend in cars was digital systems that connected everything from the ignition to onboard entertainment.

Faurecia ditched everything else it'd been working on and shifted its funds to include a €100 million innovation budget. The changes allowed the company to reduce its development time from 36 months to just 22.

In 2017, Faurecia began to piece together its own platform ecosystem by purchasing Parrot, a French company specializing in providing car-specific infotainment. It also entered into a joint venture with Coagent, which brought in some much-needed software expertise. Then came a partnership with Accenture, a company that specializes in the _internet of things_ — the kind of smart connected products that used to be more or less monofunctional; car seats, for example.

With a focus on R&D, and its own platform ecosystem in place, Faurecia began to create digitally connected automotive cockpits that offered a range of valuable experiences. These come with a voice-responsive assistant that can adjust the air conditioning, upload videos, switch to a new music playlist, make a phone call, and even set a new appointment in your MS Office calendar. 

What's more, the system learns the preferences of individual drivers. That means it knows what settings to provide depending on who sits down in its biometric car seats. This is just the tip of the iceberg, though; the system also has facial recognition and a number of safety-monitoring features that automatically respond to drowsiness and other potential signs of danger.

It's expected that the market for intelligent cockpits like Faurecia's will be €35 billion by 2025. Forecasters are saying that, thanks to its digital pivot, the company will gain a 15 percent piece of this market and experience an annual three percent growth in its stock value over the next five years.

This is what digital reinvention looks like — and today's the day to take the first step.

### 8. Final summary 

The key message in these blinks:

**We're currently experiencing the beginnings of the outcome economy, which is where many of the biggest companies in the world are doing business. They're creating products that act more like services and are being sold based on the experiences they provide. These products-as-services are all digitally-connected smart products that exist as part of a platform. They provide entertainment, information, transportation, shopping experiences and the services of AI-assisted software that can make life a little easier. Any business can pivot, change their business operations, and begin to manufacture smart connected products today.**

Actionable advice: 

**Think of your product in terms of intelligence and experience quotients.**

How intelligent is your smart connected product? Is it utilizing the last AI capabilities? How much data is it sending back for analysis? Chances are your smart product could be smarter, which means having a higher _intelligence quotient_, something that will probably make it a better product all around.

Likewise, think about how your product can provide a higher _experience quotient_ — in other words, a better experience for the customer. If you can make this happen, you'll be making a better product.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _The Road to Reinvention_** **, by Josh Linkner**

If you need more advice on how to revitalize your business, or remain unconvinced of the need for drastic measures, then it's time to check the blinks to Josh Linkner's 2014 book, _The Road to Reinvention_. Linkner is a venture capitalist and tech entrepreneur who provides ample evidence of how the world's best businesses were all major disruptors who reinvented their industries.

No one wants to end up left in the dustbin of history because they refused to adapt to the times, so find out how you can get your business on the road to reinvention.
---

### Eric Schaeffer and David Sovie

Eric Shaeffer has spent 30 years with Accenture, a company that helps industrial organizations grow through digital innovation. He currently works as a Senior Managing Director and uses his background in engineering to help clients around the world transform their businesses by embracing digitally connected innovation. He is also the author of _Industry X.0_ (2017).

David Sovie is also a Senior Managing Director at Accenture, where he specializes in helping businesses shape and execute their plans for technology-based reinvention. With a background in electrical engineering and an MBA from Harvard Business School, Sovie assists and supports businesses worldwide in their efforts to embrace new digital technologies.

