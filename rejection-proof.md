---
id: 5592601765613800078e0000
slug: rejection-proof-en
published_date: 2015-07-02T00:00:00.000+00:00
author: Jia Jiang
title: Rejection Proof
subtitle: How I Beat Fear and Became Invincible Through 100 Days of Rejection
main_color: 91BF43
text_color: 577328
---

# Rejection Proof

_How I Beat Fear and Became Invincible Through 100 Days of Rejection_

**Jia Jiang**

_Rejection Proof_ (2015) teaches you how to face rejection head-on and embrace the journey of self-improvement, both inside and out. Using Jiang's "rejection toolbox," you'll come to a better understanding of rejection, learn how to hear "yes" more often and discover the benefits of getting rejected.

---
### 1. What’s in it for me? Learn to overcome your fear of rejection and become more successful. 

If you've ever been stood up by a date or left off an invite list, you know how painful rejection can be. One thing is certain, however: you're not the only person who's ever been rejected.

Rejection in business or in life is common. Whether you're passed up at a dance audition, turned down for the baseball team or ignored by potential investors, it's all too easy to let rejection drain your spirits and make you give up.

These blinks will show that letting rejection rule you is a big mistake. If you look at rejection from a different perspective, you'll discover that there's a lot to learn from it. And it isn't just about learning how to be more successful; it's about growing as a human being.

In these blinks, you'll learn

  * why the author decided to go on a 100-day rejection journey;

  * why evolution has made us so scared of rejection; and

  * how basketball star Stephon Marbury became a legend in China.

### 2. To make your dreams come true, you have to face many obstacles, including rejection. 

Picture this scenario. You've a great, six-figure job at a Fortune 500 company, a wonderful spouse and own a 3,700-square-foot house.

Sounds pretty fantastic, right?

This was the author's life, yet he was still unhappy because he was too scared to realize his true calling: to become a world-class entrepreneur. 

It's not so hard to get yet another job or promotion, but not pursuing your lifelong dream is a heavy burden to bear.

The author's wife realized that he was unhappy, and advised him to take six months off to set up his dream company and work as hard as he could to make it a success. If by the end of the period the company still had no investors, he could always go back to his old career.

This deadline approach is effective, as it minimizes risk and forces you to set fixed goals and time limits. After a trial period, you can either keep going, or call it off.

To better your chances of success, though, you need another trick up your sleeve. You have to find your own way of handling _rejection_.

The author's vision was to create a personal development app that helped people achieve their goals. As a newbie entrepreneur, the author assembled a team of skilled engineers to build the gamification app of which he had dreamed.

But still, he needed an investor to get his company off the ground. He and his team rehearsed their sales pitch for weeks, yet in the end, the investor unfortunately said "no."

The author found he wasn't very good at dealing with rejection. So to deal with his fear head-on, he resolved to embark on a 100-day rejection journey, blogging and recording video to chronicle his experience.

Though the author's journey was unorthodox, the lessons he learned during his challenge are certainly applicable to anyone who wants to better understand and cope with rejection.

### 3. To deal with rejection, you have to understand why we as humans fear it. 

To begin to deal with rejection, you have to first understand the psychology behind the phenomenon. For example, why is rejection still a social taboo? And why does it feel so painful?

Our fear of rejection is rooted in our biology and reinforced by our instinct to stay alive.

When you feel physical pain, your brain releases opioids — the body's natural painkillers — to help you cope. In a recent experiment, researchers at the University of Michigan Medical School wanted to see if the brain would release opioids if the stimulus was instead a rejection.

Interestingly, the researchers discovered that the brains of people who experienced a rejection indeed released opioids, just as they would had the subject suffered actual _physical_ trauma.

It's probably no surprise that being rejected sits on the top-ten list of people's greatest fears. But why is that? The distress associated with rejection actually has an evolutionary foundation.

Fear has always been necessary for the survival of the species. Being ostracized by our peers or protectors would leave us to face dangerous animals or environments alone, potentially leading to injury or death. No wonder we're terrified of rejection!

Yet to truly understand rejection, it's also necessary to distinguish it from failure.

While we might see failure as unfortunate yet understandable due to factors outside our control, rejection instead feels personal, since it's another person who is saying "no," and frequently doing so face-to-face. 

The nature of rejection involves an unequal exchange, between the rejector and the rejected. And of course it's the rejected person who takes the biggest hit. Unlike failure, it's hard to blame rejection on external factors, such as the market or the economy.

It's true that fear might have saved our ancestors' skins, but today it's more handicap than precaution. So to understand the positive aspects of rejection, we need to reframe our concept of it.

### 4. You have to learn to rethink what rejection means. It’s not a judgment on who you are. 

Think of all the amazing, potentially life-changing ideas and opportunities that you didn't act on for fear of rejection.

To overcome your fears, you have to see rejection not as an objective final verdict about you but as the subjective opinion of others.

You must realize that different people will respond to the same request in many different ways. It's not the whole universe evaluating your skills and abilities, but a diverse array of people with varied backgrounds, personalities and ambitions.

Rejection is just one of many human interactions in which at least two parties are involved and because of this, every interaction is going to be unique and will often lead to a different outcome.

As part of his rejection journey, the author decided to explore rejection in the context of applying for a job.

He decided to try to find a job for one day by personally introducing himself to different companies. In his first two attempts, he was rejected by office managers who brusquely informed him that he couldn't just drop by and apply for a job.

In his third attempt, however, the author met a friendly manager who offered him a job at her technology firm on the spot.

So you see, facing rejection is a matter of simply trying again, even if it takes several attempts. In fact, you should bear in mind that rejection always has a number.

Though it seems odd at first, it's possible to get a "yes" just by talking to enough people. Because people's opinions vary, eventually you are bound to find a person who can fulfill your request.

Take best-selling author J.K. Rowling, for instance. In 1995, she submitted her first Harry Potter manuscript to 12 British publishers and all 12 rejected it. Yet, when Rowling's work was eventually picked up by publisher Bloomsbury, the book went on to sell over 100 million copies.

Rejection, then, is subjective. Avoid treating other people's opinions of you as the final word on your character and what you have to offer!

### 5. There is a lot to learn from rejection. Don’t run away when people say no; learn why, and improve. 

Sometimes after a rejection, we want the ground to swallow us up. At the very least, it's tempting to make a swift exit from the scene.

But wait! There is value in resisting the urge to run. Now is your chance to find out the reason behind the rejection.

It's a good idea to stick around and ask why you were turned down. This makes a rejection easier to deal with, and helps you formulate a more successful strategy next time.

This is similar to "retreat or rout" tactics in military warfare. A retreat is a withdrawal to regroup, and is usually temporary. A rout, on the other hand, is a disordered and confused running away, a breakdown of discipline and morale.

Therefore, an orderly retreat is far more desirable. Retreating means asking questions about a rejection to help you regroup and adapt your approach, according to the answers you receive.

In addition to asking questions and tweaking your approach, frequently changing the audience, environment and circumstances for your pitch to will increase your chances of getting a "yes."

This effective approach involves posing your request to different people, rather than trying to repeatedly hammer home one idea to the same person.

Take the case of basketball star Stephon Marbury. Getting to the top wasn't easy, and he had lots of trouble along the way. In 2007, Marbury sank into a deep depression when his father died and his shoe company _Starbury_ failed.

After switching teams and arguing frequently with coaches and team members, he was essentially forced out of the professional league. But Marbury found a new calling, and took his career to China.

In the new, different environment of Beijing, Marbury was an instant success, so much so that he became a local legend and is now even the subject of a theater musical.

> _"We might not have the freedom to control our situations, but we have the freedom to find meaning in every experience."_

### 6. To make people accept your pitch and make rejection less likely, you have to set the stage for “yes.” 

Before getting out there and gathering as many "yes" responses as you possibly can, you first need to learn the fundamentals of decision making.

First, being authentic and disclosing the reason behind your request will greatly increase the chance of receiving a "yes."

In 1978, Harvard psychologist Ellen Langer carried out a study that proved the effectiveness of providing a reason behind a request. In her experiment, she approached people waiting in line to use a copying machine and asked if she could skip the line to make her copies first.

She wanted to see if her reasons and the way she phrased the request had an immediate effect on the person's response. She found that it did. If Langer offered an explanation, even a trivial one, to jump the line, the percentage of people willing to let her do so rose significantly.

As well as being open about the motivation behind your request, acknowledging other people's doubts will also, surprisingly, put you in their favor.

In a 2009 survey of consumer taste preferences, Domino's Pizza ranked at the bottom. Domino's knew it desperately had to change, so the company completely overhauled its menu and pizza recipes.

When it came to the advertising campaign to launch its new image, however, Domino's was bold and openly slammed the former quality of its pizzas, as based on actual customer complaints.

In addition to understanding other's doubts, be sure to pitch to the right audience. Failing to do this can yield underwhelming results.

One social experiment conducted by _The Washington Post_ and violinist Joshua Bell aimed to find out how many people would recognize the musical prowess of a world-class musician if he played incognito in a New York subway station.

Guess how many people stopped to listen to Bell's 45-minute performance? A grand total of six.

Talent and brilliance don't matter if you're targeting the wrong audience in the wrong setting. Rejection is based on a host of factors that aren't necessarily about _you_.

> _"Neither rejection nor acceptance is the objective truth about the merit of an idea or even a product."_

### 7. One part of overcoming your fear of rejection is to appreciate the positive aspects of “no.” 

Although hurt feelings might be inevitable, a rejection doesn't have to be a completely negative experience. Each rejection has an upside, if you're willing to look for it!

You can use rejection to motivate and increase your ability to work on even greater goals.

Take NBA basketball legend Michael Jordan. When Jordan was inducted to the Basketball Hall of Fame in 2009, most people expected a run-of-the-mill acceptance speech.

But Jordan's talk was everything but ordinary. Jordan itemized every personal rejection he had ever received in his career, and what it had done to motivate him. He recounted everything from not being selected for varsity by his high school coach to journalists who said he'd never see the kind of success as players like Magic Johnson.

Jordan concluded that each rejection had actually fed him and his goal of becoming the top-ranking player in U.S. professional basketball.

In addition to being highly motivating, rejection might just be the mark of a radically creative idea, especially when the rejection is heavily influenced by a "herd mentality" or conventional thinking.

Throughout history, we have seen countless examples of rejection signifying that someone is well ahead of the curve in terms of discovery and innovation.

Galileo is a remarkable example of this. In 1610, Galileo published his work _Sidereus Nuncius (Starry Messenger)_, offering his heliocentric theory in which the sun sat in the center of our known universe.

Galileo's discoveries were vehemently opposed by the Catholic Church, and Inquisition authorities banned the idea of heliocentrism, ordering Galileo to refrain from defending it.

But as we know now, his ideas turned out to be not only correct but also revolutionary.

### 8. Experiencing rejection will help you develop empathy for others and clarify your goals. 

People face rejection everywhere and every day, from amateur entrepreneurs being shown the door to beggars in the street addressing passers by who won't even look at them.

So what can we learn from daily rejection? Rejection and suffering can be viewed as tools to better understanding people.

As part of his rejection challenge, the author spent time with beggars and panhandlers and experienced firsthand what it felt like to be rejected constantly and often cruelly. It was the most taxing part of his journey, but through it, he became more empathetic.

One of the people the author spent time with, a man named Frank, related an incredible story. He had served in Vietnam until he suffered an injury, which prevented him from fighting further.

Frank waited a long time for the government to upgrade his veteran disability benefits so that he could cover more than just the costs of his food and shelter. The author decided to share Frank's ordeal in his book to help him get the money he needed to make a better living.

Rejection doesn't just foster empathy; it can also teach you how much your dream really means to you.

Before comedian Louis C.K. became famous, he struggled for eight years performing stand-up comedy in an obscure Boston nightclub.

One night, the director of Saturday Night Live was in town to scout for new talent. He selected every comic who auditioned, _except_ Louis C.K.

Although the rejection stung, Louis C.K. saw his failed audition as an opportunity to really think about whether he wanted to pursue a career in comedy. He eventually got his break and was hired by late-night host Conan O'Brien as a writer for the show.

We define ourselves through our reactions to our experiences. Using rejection wisely, we can understand others better and can choose to continue our mission or find a new calling.

### 9. Greater success means staying true to who you are and being detached from the outcome. 

Overcoming rejection encourages you to take more risks in life. But it's not just about external outcomes. You have to look inside and value yourself before you seek approval from others.

The trouble is, we're not brought up to think like this. As children, we are taught that we have to fulfill other people's wishes, especially the wishes of our parents.

But this incessant approval-seeking bleeds into other areas of our life too, and we end up wanting _everyone_ to accept us, in our social circles and in business.

Yet constant approval-seeking leads us astray from living fulfilled lives and staying true to who we really are.

Your goal should be to feel comfortable in your own skin. That is, acceptance by others is _not_ the purpose of your rejection journey.

Once you value yourself, enjoying long-term success also means being able to detach yourself from the outcome.

Concentrating on factors within your control, such as your effort and behavior, will promote greater success in the long term. The benefit of focusing on controllable variables means you won't waste your energy on fretting about the unknown future.

Take John Wooden, the legendary UCLA basketball coach who won ten collegiate championships over 11 years. He never rallied his team by ranting about winning or losing. All he cared about was the effort his players put into the game.

Wooden was smart, as focusing on players giving their all and not agonizing over results led the coach to groundbreaking success with his team.

### 10. Final summary 

The key message in this book:

**Rejection happens every day, but how you interpret rejection is up to you. As you begin to embrace rejection and learn from it, it can help you refine your goals and become one of the strongest driving forces behind living the life you really want.**

Actionable advice:

**Switch up your audience or environment.**

The next time you get rejected, change your environment or pitch your request to a totally different group. A change of location or listener could well lead you to the results you want.

**Ask "why?" before you walk out.**

Dont leave the site of a rejection without asking why you were turned down. Getting to the bottom of why you were rejected will give you clarity and help you to improve your approach.

**Accept yourself first.**

Being authentic and confident should be your number one priority. Believing in yourself will see you through the tough times and help others see your strengths.

**Suggested** **further** **reading:** ** _Choose Yourself_** **by James Altucher**

Author James Altucher explains that after the 2008 global economic crisis, you can't wait to be chosen; you have to _Choose_ _Yourself._ This means you have to take full responsibility for your own success and happiness by reclaiming control of your aspirations and dreams. To do this, the book gives you both tools and effective practices to stay physically, mentally, emotionally and spiritually healthy.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Jia Jiang

Author and entrepreneur Jia Jiang is the founder of the popular blog and YouTube series, _100 Days of Rejection_. Jiang is also the creator of an online course and community that teaches others how to overcome their fear of rejection.

