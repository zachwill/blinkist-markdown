---
id: 558070616232320007670000
slug: religion-for-atheists-en
published_date: 2015-06-19T00:00:00.000+00:00
author: Alain de Botton
title: Religion for Atheists
subtitle: A Non-believer's Guide to the Uses of Religion
main_color: B82A38
text_color: B82A38
---

# Religion for Atheists

_A Non-believer's Guide to the Uses of Religion_

**Alain de Botton**

_Religion for Atheists_ (2012) sheds light on the often-overlooked positive aspects of organized religion. By considering religion in absence of a belief in divine beings, we find many valuable social initiatives and philosophical lessons from which even the most cynical among us might benefit.

---
### 1. What’s in it for me? Discover what we can learn from religion even if we’re not religious. 

Religion plays an integral role in human culture. Whether it's a Buddhist monk seeking enlightenment through meditation, a Catholic going to confession in church or a Muslim praying in a mosque, religion gives meaning and purpose to people's lives all over the world.

But what role does religion play in the lives of those who identify as secularists or nonbelievers? Is there anything they can learn from it?

Many atheists would say no; science and logic is all we need. As these blinks will show you, however, they're wrong. Religious traditions and practices can show us not only how to rebuild the sense of community that has been lost in the secular world; they can also teach us to look beyond mere facts, thereby putting our knowledge in perspective.

In these blinks, you'll learn

  * how religion fosters community;

  * why restaurants should be more like churches; and

  * how religion teaches us to let go of the ego.

### 2. Even if we don’t believe in a god, there are still things we can learn from religion. 

What do we think of when we talk about religion? A white-bearded God seated on a throne in heaven? An ornately decorated church? Maybe a painting by Botticelli? Such knee-jerk associations reflect how religion and divinity are often assumed to go hand in hand. But there's far more to it than that.

If you're someone who doesn't believe in deities, you might assume you've got nothing to learn from religion. On the contrary, however, there is no reason why you can't make the most of the cultural, social and philosophical aspects that have evolved out of it.

Just think of morality: ideas of how we might live better lives and treat others well need not be explicitly religious. However, "the bad odor of religion," as Nietzsche referred to it, has made morality something for secular people to avoid, all because of its association with religious practices.

Secular society should reclaim some of the rituals and festivals that give depth to human experience. After all, early Christianity drew from belief systems outside its own to create powerful new traditions. Christmas was created by combining the midwinter solstice — a typical pagan celebration — with the birth of Jesus. And the ideas of Epicurus, the Greek philosopher, which dictated that men inclined to a philosophical way of life should build their own communities, led to the development of monasteries.

So apparently there's more to religion than gods, divine beings and other supernatural presences. But as a nonbeliever, you might still be skeptical about exactly what religion could possibly add to your life, and how. So read on!

### 3. Religion provides a great example of how to bring people together. 

When was the last time you saw your relatives? Chances are it was during a holiday — probably one rooted in religion. Traditionally, organized religion brought people together, and it still does today; as secularization spreads and the population grows, however, our world will suffer more and more from a lack of community.

Think about the last stranger you walked past — perhaps you felt that you had nothing in common with her. Now consider religion, which counteracts such feelings of alienation by bringing vast numbers of people together, no matter their differences.

The Catholic mass brings diverse individuals together under one roof to pray, sing and listen. As you might know, mass was originally a meal shared by the congregation. It was like going to a restaurant and being seated randomly. The idea is that it's difficult _not_ to feel a sense of community when you're breaking bread with a stranger!

Other rituals developed by organized religion may aid us in building community, too. Making amends, for example, is a cornerstone practice of many religions. In Judaism, the year begins with Yom Kippur, the day of atonement. Yom Kippur allows people to reflect on the year that has passed, to recognize how they have wronged others, and to ask for forgiveness.

By creating our own secular rituals for saying sorry, perhaps we could find new ways to move forward and leave old grudges behind. A day of atonement might even be a welcome occasion for a bank holiday!

Community built through rituals and gatherings, a fundamental element of many religions, is something we too can benefit from without having to believe in God or gods. But, as the following blinks reveal, community isn't the only thing we can learn from religion.

> _"Joyful immersion in a collective spirit is an unlikely scenario in the majority of modern community."_

### 4. A little extra moral guidance never hurt anyone! 

Remember your teenage years, when you had to constantly remind your parents that they couldn't keep bossing you around? As children, we're constantly faced with instructions on how to act — advice we start to reject as we grow older.

Still, we're often faced with morally complex situations, and often, at such times, we could use a little advice. But who do we turn to?

Those in religious communities never have to look too far for advice about their decisions. Religion is not afraid to act as a moral compass. In Christianity, humanity is seen as frail and very much in need of guidance. This stems from the concept of original sin. Because we're all flawed from the outset, we shouldn't hesitate to create rules that will help us treat each other well.

Judaism also imposes moral regulations. The _Mishnah_, an old collection of Jewish oral traditions, has some passages that are framed as laws, but are actually just pieces of advice that come in handy when asking your parents for permission to travel or deliberating whether or not to invite widows for dinner every spring. It even has instructions on how often a man should be intimate and loving with his wife.

Religion often shows the traits one should aim for by creating images that embody them. In the Scrovegni chapel in Padua, the Florentine artist Giotto painted a mural that depicts seven human figures, each representing a different moral virtue. In this way, the congregation get's a clear picture of which virtues are worth striving for.

Saints, in a similar way, embody goodness and kindness, two key virtues. The saints serve as role models to devout Catholics, and in addition to paintings of them, many believers keep small, sculpted models of them in their homes.

In our secular society, we often value freedom more than acts of kindness and goodness. And yet, maybe there is something we've missed, and that we could regain from religion?

> _"Men of independent means should make love to their wife everyday. Sailors, once every six months." — The Mishnah_

### 5. Religious education teaches us true life skills. 

For most of us, all those days spent in the high-school classroom are something we'd rather forget. Seems like a shame, doesn't it! Perhaps our educational approach could do with a makeover.

Modern education prescribes that students should study specific disciplines with specific materials — study paintings to learn about art history, books to learn about literature and classical music to learn about music theory.

But why not focus on underlying, universal themes, rather than structuring teaching around specific subjects and materials? In fact, this is just what religious education often does. Themes such as humility and love are explored in memorable ways, using a range of different sources and materials, such as anecdotes, hymns, historical texts and speeches.

Most of us remember that feeling of staring at a convoluted piece of math homework and wondering, "When am I ever going to use this in real life?" Religious education does it differently, by placing equal emphasis on knowledge _and_ on how to make use of it in everyday life. Maybe alongside literature or philosophy classes, modern schools should create a department of personal relationships or a faculty for being alone!

Schools today could also learn something from the way lessons are taught within religious education. Every teacher faces the problem of bored students. Perhaps they should look to African-American preachers for inspiration! With the powerful use of call and response, the messages of sermons impact churchgoers and stick with them.

> _"Anna Karenina and Madame Bovary should be assigned in a new class on understanding marriage."_

### 6. Compassion is the best remedy for the pressure of social expectations. 

Have a look at your Facebook or Instagram feed. Do you see many pictures depicting the hardships or mundanities of life? Not so much. Most of us like to share things that make us seem accomplished and successful. But such posturing only serves to promote unrealistic lifestyle expectations.

Misery and sorrow are a natural part of life, and their presence actually allows us to appreciate positive feelings for what they're worth. Striking a balance between misery and hope is a powerful aspect of many religious traditions. Take the Wailing Wall in Jerusalem. Every morning, adherents of Judaism congregate along the wall to pray and deposit handwritten notes — containing their wishes, disappointments and yearnings — at the base of the wall.

Perhaps to counteract all the advertising posters that tantalize us with false hope, we need our very own Wailing Wall; a place where people could feel comfortable sharing their anguish and unfulfilled dreams, even anonymously, would remind us that we're not alone in our occasional misery.

As we learn to accept ourselves and our disappointments, compassion becomes a particularly helpful tool. While our secular society rarely focuses on caring for others (or one's self), religious ideas might be able to help us out. Think of all the religions that feature maternal figures. Maria, mother of Jesus; Isis of Egypt; Demeter of Rome — each is a sort of divine mother. These women symbolize tender love, and bring human vulnerability to the fore in a way that celebrates it, rather than dismissing it.

In fact, early Christianity held that the soul was much like a child. Today, Mother Mary's caring presence is prayed to in churches from Kuala Lumpur to Venezuela, all for the sake of our inner child-souls!

We find yet another caring maternal figure in Chinese Buddhism: Guanyin, a bodhisattva with a female form, who visits in times of sorrow, embracing and relieving the miserable. So the next time you're feeling low, remember to allow yourself to be cared for!

> _"Man's greatness comes from knowing he is wretched" — Blaise Pascal_

### 7. We’re tiny compared to the vastness of the universe, so keeping perspective is vital. 

How can you describe what it feels like to witness a beautiful sunset, or to listen to the laughter of children? Well, for one thing, it certainly puts that terrible pimple on your forehead into perspective! The vastness of the universe can make our own individual troubles seem insignificant in much the same way.

Religions have always been particularly good at reminding us of humanity's place in the universe and keeping us humble. Take the Book of Job in the Bible. Job lives happily, until God takes everything away from him. Why would He do this? Simply because God perceives that Job feels entitled to his great life.

The story tells us that we shouldn't ask _why_ we are subjected to something, but learn from it and be humbled. Similarly, the philosopher Spinoza thought of God as the "cause itself" and the origin of the universe. He proposed that we should imagine submitting our lives to the laws of the universe and follow them, even if it goes against our instincts and wishes.

With the importance of perspective in mind, we could also revise the way we look at scientific endeavors. Instead of simply a means to expanding our knowledge, modern science should strive to put our knowledge in perspective.

We all know that our world revolves around the sun. But even our huge sun is no match for Eta Carinae, the largest star in the universe. At 400 times the size of our sun and 4 million times as bright, it puts our section of the galaxy in perspective, and definitely inspires humility, no matter what beliefs you hold.

### 8. Art and architecture allow us to share life’s lessons in a memorable way. 

How many paintings of Jesus on the cross have you seen in your life? Probably a lot. Same goes for the number of churches. Christianity and many other religions make bold use of art and architecture. But what purpose does it serve?

Art has the power to convey deep concepts and let us understand them in a profound way. In religious art, the same themes are worked on over and over again, giving us a deeper understanding of both the themes and ourselves.

Depictions of Jesus as a child are one of the most common themes from which we get an understanding of the child within us all. The Buddhist art of mandalas — abstract art made of sand — is about the harmony of the cosmos and the ever-changing nature of life. 

When the Church had more power, it was the Church, not the artists, who chose which themes were important enough to depict. The idea that all artists should be great thinkers wasn't realistic to the Church. Today, however, artists are celebrities. But what if they gave up their moment in the spotlight to dedicate themselves to helping the rest of us understand some of life's big questions?

A painting you connect with can make time stand still. Certain spaces have the power to do this, too, a possibility harnessed by religious architecture. Architecture shapes the spaces we work and live in, and, in this way, shapes our thoughts, too.

Catholic architecture is predicated on the principle that good architecture engenders good people. In fact, way back in 1536, during the Council of Trent, a papal decree outlined standards for how a church should look — from the towering cross as the main focal point, reminding congregants of Christ's sacrifice, to the raised pulpit, putting the priest, or the voice of God, above the congregation.

Why such specificity?

The decree stated that cathedrals were "articles of faith" upon which people would reflect warmly long after their visit.

### 9. Religion reveals the importance of rituals for wellbeing and for institutions that we can rely on. 

How did you feel during your last visit to the doctor? Do you feel confident that you'll be treated similarly the next time? For many of us, seeking medical help is both uncomfortable and a hassle. But what if it was an opportunity to find comfort in our bodies and minds?

For hospitals to become institutions we can rely on, some changes need to be made. And religion has mastered the art of building dependable institutions. Let's take a look at how they did it.

You can count on consistent quality at all branches of a strong institution — whichever one you attend. Rituals in the Catholic Church, for example, are strictly regulated so that they are the same all over the world. In confession, the priest's tone of voice, procedure and even the location of the confession box are regulated by specific standards.

Religious institutions have understood that the strongest way to communicate ideas is through both mind and body. In Zen Buddhism, for example, the tea ritual — _chanoyu_ — encompasses both mind and body. The tea is stirred slowly, helping the mind let go of the ego, and the simplicity of the setting where the ceremony is performed removes unwholesome concepts, such as status.

By using specific dates for rituals and holidays, religion makes sure adherents stick to the same ritual, thus strengthening the institution. The first cherry blossoms every year are celebrated by Jews all over the world who read poems and pray to the divine for creating something so beautiful. And, in Zen Buddhism, the observation of the fifteenth night on the eighth month is a time to gather and watch the moon, something that unites Zen Buddhists worldwide.

Even for people who don't belong to a religion or don't believe in divine beings, the firm institutions of organized religion give followers a valuable sense of security — something that wouldn't go amiss in our secular institutions, either!

### 10. Final summary 

The key message in this book:

**In a thoroughly secular society where atheism continues to grow in popularity, it's easy to dismiss religion as simply irrelevant. But by building compassionate communities, providing lifelong personal and moral education and reminding us of our place in the wider universe, religion has a great number of strengths that anyone can appreciate, no matter their beliefs or background.**

Actionable advice:

**Use the branding power of religion as inspiration for company branding!**

The Catholic Church is aware that the strengths of meet-and-greet are the same all over the world. As has MacDonalds. Mickey D's has a 300-page book that covers absolutely everything, from ensuring that workers smile warmly to where the name tag should be placed to the exact amount of mayonnaise a burger requires.

**Suggested further reading:** ** _God is Not Great_** **by Christopher Hitchens**

_God is Not Great_ traces the development of religious belief from the earliest, most primitive ages of humankind through to today. It attempts to explain the dangerous implications of religious thought and the reasons why faith still exists today. It also helps explain why scientific theory and religious belief can never be reconciled.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Alain de Botton

Alain de Botton is a Swiss philosopher, TV presenter and author currently based in the UK. His book _The Architecture of Happiness_ received rave reviews and was featured in the movie _500 Days of Summer_. De Botton is also a fellow of both the Royal Institute of British Architects and the Royal Society of Literature.

