---
id: 540f03773934640008030000
slug: renewable-en
published_date: 2014-09-09T00:00:00.000+00:00
author: Jeremy Shere
title: Renewable
subtitle: The World-Changing Power of Alternative Energy
main_color: 89B224
text_color: 4F6615
---

# Renewable

_The World-Changing Power of Alternative Energy_

**Jeremy Shere**

_Renewable_ takes a look at today's most promising renewable energy sources, examining their history and future potential. These blinks explain why it is inevitable that humanity will switch to mainly renewable sources of energy, regardless of opinions on controversial topics like climate change or peak oil.

---
### 1. What’s in it for me? Learn why renewable energy sources are the inevitable future of humanity. 

Where do you think the world will be getting its energy in the year 2075? Will your coffee machine, toaster and vacuum cleaner still be powered by coal and nuclear plants? Will cars still run on gasoline?

Even the most ardent supporter of fossil fuels would probably agree that it would be unlikely — quite simply, because the supply of fossil fuels on planet Earth is limited and being rapidly depleted. A profound shift toward renewable energy sources like biofuels, and wind and solar power is inevitable. The only question is how quickly the shift will happen.

In these blinks you'll discover

  * why most renewable energy sources are not something new, but actually have long histories;

  * how the wind energy industry is becoming so attractive that even "Dr. Shrink" is interested in it; and

  * how your morning coffee might be powering your car one day.

> _"Ironically, the only time most of us think about electricity is when it's suddenly not available."_

### 2. Fossil fuels are a finite resource and using them continually will jeopardize the climate and our energy security. 

In today's energy debate, _peak_ _oil_ and _climate_ _change_ are perhaps the two most hotly contested topics.

Peak oil refers to the point in time when the maximum amount of petroleum is extracted from the ground. After this point, it's inevitable that the amount extracted will decline. The debate rages over how soon this point will be reached, with some parties claiming it's right around the corner and others trying to assure us it won't be reached for centuries.

At the moment, though, there is convincing evidence that global oil production will hit its peak no later than the end of this century and begin to decline thereafter.

Irrespective of the exact timing, peak oil _will_ be reached at some point because the amount of fossil fuels on earth _is_ limited. This means we must become less dependent on them so that the transition away from them will be smoother when fossil fuels do become scarce.

The second hot topic today is climate change, where the debate revolves around whether human activity is at its root or not. The author himself believes that there is a pile of evidence the size of Mount Everest supporting the fact that humans are accelerating climate change, but he does not wish to convince anyone of that with this book.

Instead, he emphasizes the necessity of transitioning our energy sources away from fossil fuels. If humanity continues to cling to fossil fuels even as their supply dwindles, it will require the use of increasingly risky extraction and energy production methods. These will surely have an environmental impact that will further accelerate climate change.

But regardless of what one might think of the timing of peak oil or the causes of climate change, the fact is that fossil fuels are finite and humanity must transition away from them for the sake of energy security.

### 3. Today, the timing is better than ever for increasing our use of renewable energy sources. 

Though renewable energy sources are a particularly hot topic today, they've been around for decades — some even for centuries. Windmills, for example, are an age-old way of harnessing wind power.

So why are renewable energy sources so relevant today?

Because never before have we had the knowledge, technological capabilities and political atmosphere to take advantage of them as we do now. This makes the shift towards renewable energy sources more likely than ever before.

Thanks to technological advancements, the price of producing renewable energy is falling to competitive levels. In the solar industry, the entire process from production to sales to installation is becoming far more efficient, making solar panels a lot more affordable.

And although the oil industry still wields tremendous political influence, governments around the world are increasingly supporting the development of renewable energy production forms.

In the past, increasing reliance on renewable energy sources was often actively discouraged by government policies. For example, early attempts to use ethanol as a fuel source failed because the US government taxed it at the same rate as alcoholic beverages, which heavily discouraged its use.

Today, many governments are in favor or renewables, though there is still considerable variance between the ways in which countries and administrations address them. Some merely remove the obstacles impeding their development while others actively support their proliferation.

But even with just limited support, it looks as though wind and solar energy as well as biofuels will become increasingly important in the future.

### 4. No single renewable energy source can replace fossil fuels – we need a combination of renewables. 

Often, advocates of a particular renewable energy source or energy technology tend to see it as the silver bullet to neatly solve the energy crisis and satisfy the world's demand for energy.

But reality is a bit more complicated than this.

Fossil fuels have many advantages that no single renewable energy source can compete against. Most importantly, they can be used around the world and around the clock, whereas all renewables have some restrictions. Geothermal energy, for example, is limited to only certain areas on Earth, while solar power can only be utilized during the daytime.

With these restrictions in mind, it's likely that every country and region will come up with its own _mix_ of renewable energy sources depending on their own environmental conditions and energy demand.

Typically, the broad advantages of fossil fuels are enough to make any single renewable energy source look bad in comparison, but a combination of them would more than compensate for this — and foster greater independence from fossil fuels and the countries that produce them.

The author envisions our daily energy needs in 2075 being met by a broad combination of renewable energy sources: our roof will feature solar panels and tiny wind turbines that power our appliances; a smart-meter mounted on our wall will tell us exactly how much energy each appliance consumes and allow us to power down everything while we're away. What's more, before we set off for work in the morning, we'll put our garbage — banana peels, coffee grinds, peach pits, etc. — into a device that compresses and processes it directly into biofuel for our car.

Such a clean, sustainable future could well emerge if we find a working strategic mix of renewable energy sources. But getting there will require the various actors who are currently focused on developing their own renewable sources and devices to work together.

> _"Like it or not, for several reasons ... coal will remain king for the foreseeable future. But energy is not a zero-sum game."_

### 5. The present and future of all major renewable energy sources are explained by their history. 

Renewable energy is nothing new: ideas and methods for utilizing it have been around for decades — and in some cases even centuries. This means we can learn from previous mistakes and obstacles.

Historically, the most common hindrances to using renewable energy sources over fossil fuels have been prohibitively high costs, inadequate technological capabilities and disadvantageous political decisions.

For example, in the former British Empire at the beginning of the twentieth century, solar power was considered a viable energy source in regions that lacked coal deposits, such as Egypt. Solar engines worked by reflecting the sun's rays onto boilers where water turned into steam that then powered turbines.

Unfortunately, this promising development ended abruptly with the start of World War I and later with the large-scale extraction of oil from the vast reserves in the Middle East. Thus, solar power lost its competitiveness, which it is only beginning to regain today as fossil fuels become more expensive and solar devices become cheaper.

Of course, the fact that renewables have a long history does not guarantee that they will be widely used in the future. It does mean, however, that countries all around the world already have some prior experience with them. This is a good starting point for increasing future use.

Because wind has always been a globally available resource, wind energy has a long history all over the world. This makes for a broad and deep pool of experience, which will likely accelerate further development and proliferation.

Geothermal energy is geographically limited to certain places, like Iceland, and its history is much shorter. Consequently, there hasn't been the same accumulation of expertise as with wind energy. Nevertheless, since geothermal energy is crucially important in places that have access to it, expertise is rapidly being accumulated there.

### 6. The potential of a renewables can’t be based on its current status alone. 

When newspapers write about the future of renewable energy sources, the stories often feature images of huge solar or wind farms extending as far as the eye can see. The purpose of these images is to show the impracticality of replacing conventional energy sources like fossil fuels and nuclear power through renewables.

But this is misleading. If we just look at the crude numbers, solar power only accounts for around 1 percent of US energy production, it's true. But its promising growth is hidden by the fact that energy generation as a whole is growing very rapidly.

What's more, in certain areas like California, solar power is far more important than it is nationally.

And, of course, we should be more focused on the _potential_ of solar energy, not its current status. From a historical perspective, even today's solar power generation capabilities would have seemed fantastic and unimaginable, so who knows what the future will bring — especially as technological breakthroughs like improved solar panel efficiency are being made constantly.

Looking at the latest numbers alone will always favor conventional energy sources, so it's vital for us to rally political support for renewable energy sources. The only way to do this is by encouraging an optimistic view of the potential of renewable energy sources.

One promising new technology involves using the power of waves and tides to generate energy. At the moment, this technology accounts for very little of global energy production, and prior attempts to utilize it more broadly have not entirely succeeded. But if we only think of these "near successes" as failures, it will lead to a lack of political support for further development, which could end up depriving us of a major energy source in the future.

In short, with renewables, we have to go beyond the numbers and past disappointments and focus on future potential.

### 7. Fossil fuels are only a brief folly: we must return to the mindset we once had about renewables. 

When fossil fuel-based energy production first became widespread, people were well aware of the finite nature of the resource. In fact, fossil fuels were never intended to be the primary energy source of the world in the long-term.

But as more and more oil wells were discovered and improved, drilling techniques allowed more oil to be extracted, people began to forget that oil is finite. And, naturally, the oil industry does all it can to encourage such amnesia.

Biofuel has a long history: some of the very first cars ran on alcohol! It is clearly more sustainable than oil, but the oil industry has succeeded in playing down this fact to the public. Up till now, it has only been in times of oil crises and price spikes that people have briefly turned their attention to biofuels.

In the face of the finite nature of fossil fuels, we need to go back to the renewable energy sources that were common before fossil fuel use became so widespread. We must also re-establish the mindset that people had in those days: constantly adapting renewable energy sources to new circumstances.

For example, windmills have always been rapidly adapted to new circumstances by changing their design. In the Great Plains of the United States, farmers found that European-style windmills were far too expensive and ineffective, so they modified them to better suit their needs and came up with a self-regulating design.

This kind of thinking is currently being applied to hydropower. Today, around 16 percent of global electricity consumption is produced with hydropower based on dams and turbines. But, thanks to the long history of using water as an energy source, scientists are also working on new forms of hydropower based on waves and tides: _hydropower_ _2.0._

> _"A civilization in which renewable energy carries a lion's share of the load is not only possible and necessary but also inevitable."_

### 8. As renewables become competitive with fossil fuels, a big business is springing up around them. 

As mentioned in the previous blinks, one of the biggest hurdles to the large-scale adoption of renewable energy sources has been their high cost compared to fossil fuels.

But today, renewable energy sources are for the first time becoming competitive cost-wise. This development is driven by two trends:

First, the price of fossil fuels is rising due to an increased demand stemming from population growth and rising living standards.

Second, the price of renewable energy is rapidly declining thanks to technological advancements. Solar panels and wind turbines are getting cheaper and more efficient.

No matter where in the world you go, you'll see that one form of renewable energy or another is becoming increasingly competitive with fossil fuels. Renewable energy is no longer just a small niche promoted by a few green activists; it's becoming a big business that attracts big players.

Even giant oil companies like BP, Exxon and Chevron are jumping into the fray, researching the potential of alternative energy sources. One area where they, together with a few innovative start-ups, are particularly active is in studying the potential of algae as a biofuel source. Their goal is to be the first movers in this promising, up-and-coming market so they can secure themselves a big slice of the pie.

The author saw this development first-hand when attending the annual _Windpower_ _Conference_ _and_ _Exhibition_ in Dallas, Texas. He was shocked by how big an industry had sprung up around wind power, calling the conference a veritable "Disney World of Wind," with lots of businesses from diverse industries in attendance. There was even a company specializing in shrink wrapping called _Dr._ _Shrink_ that was hoping to make a killing in the business of wrapping turbine blades for shipment.

### 9. We need support from today’s politicians to smooth the transition into renewables. 

As mentioned in the previous blink, renewable energy sources are becoming more and more competitive with fossil fuels. But even with this development, we cannot sit back and wait for the day when renewables fully triumph over conventional energy sources. It'll take a long time to make this transition, which is why renewables require political support _now_.

Though a change toward renewables is already in motion around the world, it is very slow. Many people and companies are still forced to decide between energy that's green but expensive, or cheap but fossil-fuel based, and this slows down the transition. If the United States were to throw its political and economic weight fully behind renewables, it would accelerate the shift immensely.

It's necessary to kick-start the change now so that the long transition can begin while there are still fossil fuel reserves to smooth the process.

That the world will shift to renewable energy sources is inevitable, the only question is whether that shift will happen in a fairly controlled, smooth, financially sensible way, or in a panic caused by an urgent lack of fossil fuels. The deciding factor will be the political will for change that we can muster up today.

One example of a time-consuming shift will be adapting the complicated energy infrastructure that is specifically tailored to fossil fuels like oil pipelines and power grids. Undertaking a massive change to enable renewable energy sources to utilize this same infrastructure will demand a lot of time, investment and political support, and it's important that fossil fuels still power the world throughout this process.

### 10. Final summary 

The key message in this book:

**The** **history** **of** **renewable** **energy** **reaches** **much** **further** **than** **most** **people** **assume:** **humanity** **has** **been** **using** **the** **power** **of** **the** **wind** **and** **the** **sun** **for** **centuries.** **Reverting** **back** **to** **renewables** **like** **these** **is** **inevitable,** **because** **planet** **earth's** **supply** **of** **fossil** **fuels** **will** **run** **out** **sooner** **or** **later.** **To** **make** **this** **transition** **in** **a** **smooth** **and** **controlled** **way,** **we** **need** **to** **start** **increasing** **political** **support** **for** **renewables** **today.**

**Suggested** **further** **reading:** **_Green_** **_Illusions_** **by** **Ozzie** **Zehner**

_Green_ _Illusions_ deflates the hype surrounding new alternative energy sources. It also explains why, if we truly care about the environment, we should focus on changing our own excessive consumer behavior.
---

### Jeremy Shere

Jeremy Shere is a science writer and journalist who also teaches at Indiana University in Bloomington.

