---
id: 55ed2e8611ad1a0009000077
slug: resilience-en
published_date: 2015-09-10T00:00:00.000+00:00
author: Eric Greitens
title: Resilience
subtitle: Hard-Won Wisdom for Living a Better Life
main_color: CB4036
text_color: B2382F
---

# Resilience

_Hard-Won Wisdom for Living a Better Life_

**Eric Greitens**

_Resilience_ (2015) encourages us to confront pain and hardship, using them to create meaning and find joy. The author doesn't promise miracles, since there's no easy cure for losing a loved one or facing other tragedies. He only promises the effectiveness of hard work, describing how to overcome the fear of failure and take responsibility for your own life. If you can do that, the challenges you face will only make you stronger, wiser and more courageous.

---
### 1. What’s in it for me? Discover how pain and suffering is necessary for happiness and greatness. 

Why do we hurt, feel self-doubt and constantly struggle throughout life? What does it take to overcome all the obstacles? The bottom line is that hardship is a part of life, but only by facing up to it and learning to tackle all of life's challenges will we achieve our full potential. So how do you do that? With resilience.

Author Eric Greitens, in his correspondence with fellow Navy SEAL veteran Zach, tries to find the qualities, practices and training needed to become resilient against the harder parts of life. His search took him through history, philosophy and beyond. Ultimately, it seems the ancient struggle we face as humans has a solution that's equally ancient.

In these blinks, you'll discover

  * how resilience helped create the first modern novel, _Don Quixote_ ;

  * that people failing to take responsibility for their personal actions helped Nazi Germany; and

  * how Ancient Greeks used repetitions to create resilience in both mind and body.

### 2. You need resilience to courageously overcome life’s challenges. 

Are you living up to your full potential? The unfortunate truth is, many of us fail to access all our energy, intelligence and compassion because we get bogged down in disappointment and failure. 

But we can learn to overcome hardship by harnessing the power of _resilience_. Because here's the thing: none of us can escape pain, fear or doubts. But we can refuse to let difficult experiences bring us down! The trick is to see them in the right light. Overcoming obstacles is difficult, but doing so will make you stronger. Simply put, pain builds courage. You just have to be resilient. 

That's what the author learned after spending many years working with veterans. Those brave men and women lost both limbs and friends, not to mention purpose. But by facing the loss and overcoming their pain, they transformed it into something positive.

Consider the story of Redmon Ramos. After losing his leg by stepping on a landmine in Afghanistan, the former marine realized he had two choices: he could either pity himself or confront his pain. He chose the latter option and, as a result, became much stronger. With the help of prosthetics, Ramos eventually entered the US Paralympic Warrior Games and brought home several medals. 

There's a lesson here: the goal isn't to bounce back from a hardship, but rather to move through it. History is full of similar stories — stories of people faced with seemingly insurmountable challenges and managing to use their misery to their advantage. 

Consider Miguel de Cervantes, a Spaniard imprisoned in 1602. To pass time in his cell, he invented a story about an old man who believed he was a knight. Eventually, that tale evolved into the first modern novel, a work of literature beloved to this day — _Don Quixote_.

We can all follow Cervantes' lead and meet life's challenges with courage and resilience.

### 3. Anything worth having is worth struggling for. 

We all want fulfilling work and happy relationships. But it's easier to want those things than to get them. And that's because anything worth having requires lots of hard work and struggle. 

But how do you know what's worth struggling for? Well, it comes down to having a purpose. And that's not something you can casually find. Rather, you have to forge a purpose by taking action and trying new things. 

For instance, take a job in a new town where you don't know anyone. Or try traveling abroad for the first time. Of course, these kinds of experiences can be painful; leaving your comfort zone is scary.

But remember, challenges won't hurt you — in fact, we crave them! 

Consider the research done by Mihaly Cziksztentmihalyi, professor of management and psychology at Claremont Graduate University. Cziksztentmihalyi showed that the best moments of our lives aren't passive or relaxing; rather, they're active — when our body or mind is pushed to its limits. During this joyful state, called _flow_, we lose ourselves completely in whatever we're doing.

The crucial point here is that flow arises during moments of difficulty. Meaning, if we're not being challenged — if everything in our lives is provided for — our minds deteriorate. 

Just think of how the body crashes when you don't meet its basic needs, like sleep, food and water. A similar thing happens when our mind doesn't get what _it_ needs — a meaningful challenge. 

For instance, a 2014 Gallup study showed that after 52 weeks of unemployment, the incidence of depression shot up to 20 percent. Why? The explanation is simple: lack of purpose made people feel unneeded. 

In other words, the goal of life isn't to avoid struggle, but rather to choose meaningful challenges. Because although we can subsist without worthwhile work, we can't flourish. Not to mention that, in the long run, lack of purpose is as dangerous as deprivation of sleep.

### 4. To summon resilience, start by taking responsibility for your own actions. 

We've all been through dark, painful periods characterized by relentless struggle. But how do you summon your resilience and move forward during these moments? Well, start by looking in the mirror. 

Because ultimately, resilience is about taking responsibility for your actions. It's a matter of being able to accept what you cannot change so that you can redirect your focus to things you have control over.

This is an age-old strategy for dealing with life's challenges. In medieval Spain, for instance, at a time when Jews were severely oppressed, Jewish philosopher Solomon ibn Gabirol wrote that wisdom and peace lie in "being reconciled to the uncontrollable."

And yet, although you're not responsible for everything that happens to you, you _are_ responsible for how you deal with it.

That's what the author realized while visiting a refugee camp, where he saw thousands of men and women struggling to come to grips with a tragic reality. And while some were beaten down by misery, others stood proud; instead of sitting around and mourning, the resilient ones taught children, organized sports games and more. The author saw that even at their most powerless, some people found an inner strength that couldn't be taken from them.

On the other hand, history shows that there are terrible consequences for refusing to take responsibility for your actions. 

Consider the work of philosopher Eric Hoffer, who studies mass movements and fanaticism. When trying to understand why people would voluntarily acquiesce to tyranny, Hoffer encountered a young German who explained that he joined the Nazi party to be "free from freedom." 

For that young man and for others, passing off the heavy burden of responsibility might seem attractive. But that impulse has abetted some of the most notorious acts of tyranny and brutality the world has ever seen.

### 5. Repetition allows us to form positive habits that help us reach our goals. 

Imagine a ballet show or a movie: the seeming effortlessness of the final product makes it easy to forget how much work went into it. In the same way, we often don't realize that good habits and strong character don't just fall from the sky, but are diligently built through practice and repetition.

Look at it this way: every time we battle our fears, we become more courageous. And repetition ultimately builds resilience, which we can use to overcome hard times. 

In fact, the Ancient Greeks were aware that repetition was crucial for forming human behavior. They were also aware that in order to develop a strong character and spirit, you have to train both your mind and your body. Likewise, repetition was a crucial part of physical training in Athens; the exercise regimen was structured around repeatedly lifting stones and running around and around in the sand.

So all in all, if you want to evolve in a different direction, start by changing your habits. Because every time we repeat an action, we're reinforcing a habit. If you want to become a kind person, get in the habit of being kind daily. 

That's the thing about kindness — it's a habit anyone can cultivate. It's a matter of treating your inner kindness as a source of strength, even if you encounter someone who makes you angry or stressed. You'd be surprised to see what effect this can have. Instead of raising your voice and getting upset, just stay calm, kind and compassionate. Chances are, the other person will start acting kindly as well. 

Unfortunately, this same principle applies to our negative habits. If we're mean every day, pretty soon we become mean. And of course, hard times make it easy to fall back into bad habits. Battle this by training even harder to build strong, positive habits.

> _"There are some things which cannot be learned quickly, and time, which is all we have, must be paid heavily for their acquiring." - Ernest Hemingway_

### 6. Deal with your pain by finding meaningful challenges. 

Sometimes pushing yourself by studying hard or training ferociously is painful. However unpleasant such pain may be, it's easier to bear, because we've sought it out, and, of course, it's ultimately constructive. But what about those other times — after the death of a loved one, for instance — when the pain finds us. Such tragedies may feel unbearable, but we still have to confront them. 

Through his years of work with devastated veterans, the author encountered some former soldiers who tried to numb their pain with alcohol, television or reclusiveness. But the author believes that the best way to ease the pain of veterans is to challenge them. This might involve tutoring a child or training a football team — anything that helps them rebuild a sense of purpose. 

Consider the story of Tim, an Iraq War veteran who was struggling to put his life back together after returning home. The author asked Tim one simple, life-changing question: "How are you going to serve again?" Tim said it was the first time since returning home that someone had asked something of him. And so today, Tim owns Patriot Commercial Cleaning, a business that employs other veterans to help them rebuild their lives and re-enter civil society. 

Ultimately, you can't live a full life without purposeful work and strong social bonds. And that's not only a problem for veterans; living without purpose is painful for everybody. To overcome that pain, you have to find something meaningful. 

That's what the author learned from the years he spent working with veterans and also as a humanitarian aid worker: people need to serve a purpose higher than themselves, _especially_ when things are hard. In other words, outer surface leads to inner growth.

It's like fighting fire with fire: to help someone overcome their pain and conquer the challenges they're facing, put a new challenge before them.

### 7. Resilience is about learning to accept failure. 

Remember when you learned to ride a bike? You probably fell a few times and scraped up your knees before mastering it. 

That's the case for every challenge: at first, you fall. Failure is just the reality and high-achievers have figured out how to live with it.

In fact, those who excel fail more often than those who don't, because they've learned to accept failure as part of the improvement process. Accordingly, they're willing to try more things and put themselves out there, leading to even more failure. Crucially, these people don't fail passively, instead finding a way to learn from each mistake.

Many of us fear failure, so accepting it is easier said than done. For instance, many of the author's former SEAL friends used to be great at boxing, but now they're unwilling to step inside the ring. Why? Because after tasting success, they couldn't deal with the prospect of losing. 

Nevertheless, it's possible to overcome the fear of failure at any age. And if you can do that, you'll be able to try new things, have adventures and grow. 

To that end, no matter how old you are, try to see yourself as a beginner. Case in point: after returning home from Iraq, the author — a former boxing champion and Navy SEAL — considered taking up the martial art tae kwon do. But he kept putting it off, wondering whether there was any point to learning a new martial art, since his boxing game was already tops. But when he finally went for it, he got his ass handed to him. Still, he greatly enjoyed learning and realized that defeat is only temporary. And in the meantime, it helps build strength, willpower and motivation. 

All in all, failure is a common part of everyday life. And it's the only way to improve. So, if you're not failing constantly, you're probably not trying hard enough.

### 8. Final summary 

The key message in this book:

**People get better when things get hard. By using our inner strength, we can make the best out of a bad situation. It's a matter of building up courage, strength and wisdom. In other words, it's about becoming resilient.**

Actionable advice:

**Exercise hard.**

Find a way to push yourself physically. A truly challenging exercise regimen should push you to the breaking point. Pushing yourself regularly will not only help you feel better in your body, but also awaken your spirit and free your mind. 

**Suggested** **further** **reading:** ** _The Road to Character_** **by David Brooks**

_The Road to Character_ (2015) explains how society's focus on fame, wealth and status eclipses moral virtues and internal struggles. These blinks will show you how to reclaim qualities such as kindness, bravery, honesty and commitment.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

### 1. What’s in it for me? Get the blueprints for facing rejection when pursuing your dreams. 

Have you ever wondered why some people become successful and others don't? Well, who hasn't? The problem is, we often assume that successful people have something we lack, be it extraordinary talent, money and connections or simply a lot of luck. And sure, these things might help — but they're not what success is made of. Rather, success is the result of resilience: the ability to face criticism and rejection when pursuing your goals.

In these blinks, we'll look at how you can build your resilience and achieve your dreams. Along the way, you'll also gain some techniques that'll help you achieve your goals, whatever those may be.

You'll also find out

  * why passion trumps talent;

  * that fear is fuel to pursue your dreams; and

  * why you need to learn how to deal with your inner critic.

### 2. To make your dreams come true, you need to be comfortable with moving past your fears. 

Many of us had big dreams when we were young. We wanted to be athletes, musicians, astronauts or even a president or prime minister. Yet as the years go on, most of us settle for modest and unassuming jobs.

The reason most people give up on those early dreams is fear. The bigger the dream, the bigger the fear of taking the leap required to make it a reality.

For example, the dream of being prime minister or president means entering the world of politics, which requires being comfortable speaking in front people, getting votes and the prospect of giving a speech before the members of congress or parliament. This all requires a lot of preparation, not to mention familiarity with the issues that are affecting people's lives.

Naturally, it's overwhelming just to think of all the dedication and hard work this would take. What if you get behind a podium and freeze up? Or worse, what if you say the wrong thing? There are so many possibilities for it all to go horribly wrong.

So there are two options: you can either give up or you can face your fear. And, of these two options, one is clearly a whole lot easier. Most people give up.

Most people want to experience the dream without the fear, but this just isn't possible. The only way to reach great heights is to stare down your fears and conquer them.

Okay, so to get yourself started, try this visualization exercise:

Let's say your dream is to be a novelist. Close your eyes and picture yourself taking the steps to make this dream come true. As you do this, you'll feel that natural fear emerge, but instead of avoiding it, sit with it and accept it. Now, imagine yourself working through that fear and writing your book and, after long last, typing the words "The End."

As you picture yourself finishing your goal, you'll feel a surge of positive energy that will keep you on the path to your dream. And the fear and excitement of the undertaking will provide you with adrenaline, the fuel you'll need to step up to the challenge that lies ahead.

> _"Do something concrete that will take you one step closer to your goal. Even if it's a small action, notice how it reduces the fear."_

### 3. Having the passion to realize your dreams requires great commitment. 

If your dream is to be a Navy SEAL or a CIA agent, then it's fair to say you have a life-threatening dream. If you're chasing these kinds of goals, being prepared to die to make your dream come true is par for the course.

But you need this passionate commitment even if your dream isn't a real threat to your survival.

One thing's for sure: making a dream come true is hard work, requiring you to overcome obstacles and face levels of rejection and criticism that would break most people's spirits. When these low points arrive, you'll need to have a death-defying passion if you hope to continue.

If you're just testing the waters to see if you can put a talent to use, you won't last long. And if you have as much passion as the average person, you'll collapse before you reach the first hurdle.

So the first step is making sure that your dream is something you are truly passionate about.

To make sure you're properly prepared, here are some pointers:

One of the most powerful dreams a person can have is to fight for justice and equality. There are few motivations as potent and as satisfying as helping others. So if your dream helps other people and is philanthropic in some way, you'll have a better chance of keeping it alive when times get tough.

But it needn't be a noble cause. People have accomplished amazing things by devoting themselves to things like setting a new world record in exploration or extreme sports. For others, all it takes is passion for starting their own successful, money-making business.

Whatever it is, it must be a dream that you hold very dear, because you will face rejection. Your passion is what will keep you going.

### 4. Persevering in spite of rejection is the key to reaching your dreams. 

You know that dreadful feeling you get in the pit of your stomach when you receive a message that begins with the words, "We regret to inform you…"

Rejections can be utterly crushing, but it's important to remember that feelings of despair are completely normal.

When you're rejected, of _course_ you'll take it personally and feel like the world is conspiring against you — like you're the only one who's getting picked on while everyone else is coasting along on easy street. But this isn't an accurate picture of how things are.

The author has helped hundreds of people achieve their dreams over the years and each one felt that he was alone in experiencing rejection while everyone else was succeeding.

The reality is, you're not the only one trying to publish a book, get a record deal or launch a start-up. And since so many people are trying to do the same thing, the odds are that a lot of people will be getting rejected every day.

But this doesn't mean you should give up. In fact, the key to success is having resilience and persevering in the face of rejection. This is what separates the wannabes from the superstars.

Stephen King's first novel, _Carrie_, was rejected a dozen times, but King didn't throw in the towel. He kept going and continued submitting the book until he found a publisher that agreed to take a chance on him. King isn't alone — William Golding went through one rejection after another before he got his classic novel, _Lord of the Flies_, published.

In short, there's no sense in taking rejection as a sign of failure when it's actually a normal part of the journey.

### 5. To get closer to your dreams, overcome your fears and keep challenging yourself. 

If you have a fear of sharks, the last thing you'll want to do is go deep-sea diving and surround yourself with great whites. But if you're trying to _overcome_ your fear of sharks — well, diving right in might not be such a bad idea.

The emotion of fear can be overcome through the process of _desensitization_.

Psychologists began advocating desensitization when they discovered that fearful patients were getting good results after being repeatedly exposed to the source of their fear.

Whether it's meeting new people, heights or public speaking, repeated exposure will gradually turn that fear into something you can tolerate. This is important to remember in the pursuit of your dream, since you might think that you can never be an actor if you have crippling stage fright. If you continue to play roles, gritting your teeth and pushing yourself through it, by the tenth play you'll be wondering what you were so afraid of.

Desensitization requires practice and a willingness to continually challenge yourself. The more you do this, the fewer fears and obstacles you'll have separating you from your dreams.

To get yourself started, create a list of everything you'd want to accomplish in life if there wasn't anything standing in your way. Now do the visualization exercise again and imagine starting toward these goals and getting repeatedly rejected — but then, for one or two of your dreams, imagine being accepted. If just two out of ten dreams come true, the eight other rejections would be totally worth it, right?

Now that you have healthy perspective, you can get started on your dreams for real and begin sending in those applications for opportunities that trigger scary feelings. And when the inevitable rejection letter comes your way, you can celebrate it, knowing that you're on the right path.

In the next blink, we'll discuss how to deal with rejection's close relative: harsh criticism.

### 6. To be a better self-critic, distance yourself from your work so you can take a fresh perspective. 

Have you ever given up on a dream, or even a hobby, because you thought you weren't talented enough?

We're often our own harshest critic, but that criticism doesn't always need to be negative. To help your criticism be effective, however, you need to take a step back, distance yourself from the work in question and look at it from a new perspective.

Leonardo da Vinci recommended this. He would set up a mirror so that he could look at a reflection of his latest painting. This offered da Vinci, quite literally, a new perspective; seeing the reverse image in the mirror allowed him to be objective and judge a painting as though he were seeing it for the first time.

Even walking away to look at your work from a distance can allow you to see things in a new light, and it doesn't only apply to paintings.

The author will regularly change formats during his writing process, starting with handwritten drafts, then moving to the computer screen and finishing with printouts that he'll use to make edits. Each one of these new perspectives offers him a fresh look and a chance to notice improvements he didn't see before.

Athletes and performers all use video and audio recordings after a game or show to pick out weaknesses that otherwise might go unnoticed.

Taking some time away from your work is another recommended way of gaining perspective.

Put down the pen or paintbrush and let your work rest for an hour — or even several days, if you can. When you return to it, you'll find that you can now judge it with greater accuracy.

Many writers follow this advice and will even set up different periods of time for writing and editing.

The poet Maya Angelou worked in two different phases: the first phase was during the day, when she would try to write at least twelve pages over a six-hour period. The second phase started hours later. From 8:00 p.m. to 10:00 p.m., she'd edit the day's work into the three or four pages that were worth keeping.

### 7. Your inner critic can work with you, rather than against you. 

There are many self-help books that demonize creative people's _inner critic_, the voice within us that loves to criticize our own work. This voice often gets cast as a destructive force that can prevent us from ever being creative.

But your inner critic doesn't have to get in the way. In fact, it can be a great friend and ally.

After all, your inner critic is the one who pushes you to keep working and reworking until you've reached the best possible outcome.

For the musician and producer Mike Monday, the difference between a good producer and a great producer is the ability to recognize the difference between high-quality and low-quality work. Drawing this distinction wouldn't be possible without your inner critic.

The reason the inner critic gets a bad rap is because it can get overcritical before you've even begun, and this can convince you to not even bother trying. But if you befriend your inner critic, you can prevent this scenario from playing out.

For example, if you notice that your inner critic is getting too negative, ask, "What do I need to do differently?" This forces the voice to be constructive rather than unhelpful, and you can use this feedback to make your work better.

Likewise, if the inner critic starts speaking up too early, when you're only in the planning phase, tell it something like, "I hear you but I'm going to ignore you for now. We'll talk later when there's work to look at. Don't worry, I won't submit anything without going over it with you."

Also, make sure your inner critic voices opinions and isn't trying to convince you of absolute truths. The criticism should be focused on precise problems, and it should always be respectful.

If your inner critic is breaking these rules, don't just take it — talk back! Tell it that, if it isn't constructive, you won't listen at all.

### 8. Reaching your dreams requires a healthy attitude toward success. 

Close your eyes and think about the word "success." What feelings and images does the word conjure up?

It's helpful to understand your preconceptions about success, because different people relate to it in different ways. And how you think about it will affect the way you pursue your dreams.

People generally perceive success in three different ways, and only one of them is positive.

The first way to think of success is as something that's superficial and morally reprehensible. This perspective recognizes the money and the luxury, but it also sees greed and corruption.

The second way of viewing success is as something that's far out of your reach. It's like a mountain summit that only a select few can reach — but certainly not you. As you can see, this isn't a motivating point of view.

Then there's the healthy way of seeing success: as the act of achieving something great, whether it's creating inspirational art, inventing a life-changing product or empowering people to be better. With this perspective on success, you can keep yourself on the right path.

So, if you're thinking negative thoughts about success, now's the time to change those ideas.

When the author was a young man, he was an idealist, an aspiring poet who saw corporations and businesses as evil. Yet as he grew up, he realized he needed to be a successful businessman in order to sustain himself with freelance work.

To help get the right perspective, he looked to Shakespeare as an example. Here was a great author who was also a savvy businessman, helping create a thriving theater company and investing his money wisely. The author began to understand that business and art needn't be at odds with each other, and this made his life much easier.

Next, we'll debunk some misconceptions about success. After that, you'll be ready to start on your own path to your dreams!

### 9. Anyone can pursue their dreams, so start experimenting to expand your confidence. 

If you've ever believed that you're not smart enough, brave enough or rich enough to pursue your dreams, it's time to think again.

The only quality necessary to go after that dream is a willingness to work hard.

For some reason, many people believe there are specific qualities that a person must possess to pursue certain professions, but this is a misconception.

We'll often think a politician or actor must be an outgoing extrovert, but this is far from reality. The truth is that you don't even need that much confidence before you begin pursuing your dream.

When the author was 24 years old, he was told that he could be a therapist. At first, he was certain this was a mistake. Doesn't a therapist need to be old, with a lot of experience? As it turns out, no. What's really needed is curiosity and an inquisitive mind.

So the author started reading about hypnotherapy and, soon enough, he was enrolled in school and taking some courses. Eventually, he had his own successful hypnotherapy practice.

When you start expanding your ideas about what's possible, your confidence will also grow. And you can start expanding by experimenting.

If there are things you want to do, but you don't feel capable of doing them, write each thing down on a piece of paper, along with the reason you think you can't do it. Is it impossible because of something you lack? Do you not have enough empathy, courage, intelligence or physical strength? Whatever it is, write it down.

Now you're ready to start the experiment.

Imagine you have one of the particular qualities you believe you lack, pick something off the list and do something related to that impossibility.

Perhaps you think you could never be an Olympic-level mountain biker. Well, how about you start by fixing up that bike sitting in your garage? The more small steps you take, the more confidence you'll gain and the closer that dream will be.

It's time to move past the fears of rejection and criticism and to start enjoying the adventures that life can provide. Get out there and start living!

### 10. Final summary 

The key message in this book:

**Resilience is not an elusive quality that some people have and others lack. It's a practice that you can build by refusing to give up, despite the rejection and criticism you encounter. We all have the capacity for resilience. And, by cultivating it, you can discover your true passion and pursue it, even when the odds seem to be stacked against you.**

Actionable advice:

**Keep track of your progress.**

If you're a writer, keep track of the number of words you write a day. Decide for yourself how many quality words or pages you need to write per day in order to feel good about your efforts. It doesn't matter what you do — what's important is to track it. Remember, success comes one step at a time.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Gifts of Imperfection_** **by Brené Brown**

_The Gifts of Imperfection_ offers an accessible and engaging walk through the ten principles that you can follow to live a more fulfilling life, defined by courage, connection and compassion towards others. Filled with relatable anecdotes and actionable advice, the book is a useful resource for readers both young and old.
---

### Eric Greitens

Eric Greitens is a former Navy SEAL and humanitarian aid worker. He is the founder of Center for Citizen Leadership, a nonprofit organization that helps veterans adjust to life back home. Greitens is the author of _Strength and Compassion,_ which chronicles his experiences as a humanitarian aid worker.

