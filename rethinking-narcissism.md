---
id: 56e023e126ea840007000001
slug: rethinking-narcissism-en
published_date: 2016-03-09T00:00:00.000+00:00
author: Craig Malkin
title: Rethinking Narcissism
subtitle: The Bad – and Surprising Good – about Feeling Special
main_color: 2DA2E3
text_color: 1E6B96
---

# Rethinking Narcissism

_The Bad – and Surprising Good – about Feeling Special_

**Craig Malkin**

_Rethinking Narcissism_ (2015) provides fresh perspectives on what we typically understand as arrogance or vanity. These blinks situate narcissism both historically and culturally, explaining the spectrum of narcissism and its different forms; they also provide helpful strategies for recognizing and dealing with the narcissists you might know.

---
### 1. What’s in it for me? Learn the whys and ways of self-love. 

We've all got one of them in our life: the guy whose only topic is himself; the gal whom everything is about. All conversations revolve around them. They are — always — the most important person in the room.

Of course, this drives all their friends crazy. They've probably been told multiple times to think more of others. And yet, they seem incapable of altering their behavior. 

Why are some people like this? Well, they simply love themselves too much. They're narcissists. 

And self-aggrandizement isn't the only symptom of narcissism. It's a malady with many forms and gradations. These blinks explain what narcissism is, where it comes from and, more importantly, what you can do to prevent it.

These blinks also show you

  * how people can be narcissistic in silence;

  * why parents get blamed, at least partly, for causing self-love; and

  * what an emotional hot potato is.

### 2. Narcissism has been debated since ancient times. 

You're probably familiar with the myth of Narcissus — the extraordinarily handsome hunter, who, after shunning the mountain nymph Echo, fell in love with his own reflection in a lake and, unable to draw himself away, perished at water's edge. Clearly, self-love has been a controversial topic since ancient times.

Back in 350 BC, Aristotle asked whom the good man should love more: himself or others. Ultimately, Aristotle decided that the good man is he who loves himself most. If, a few centuries earlier, you'd asked Buddha the same question, you'd have gotten a very different response. He claimed that the self is nothing more than an illusion, and that it was best to love others. 

But it wasn't until the early twentieth century that the word _narcissism_ appeared for the first time, courtesy of Sigmund Freud. Freud claimed that, in order to establish meaningful relationships with others, a person must first fall in love with themselves. In a famous paper, _On Narcissism: An Introduction_, published in 1914, Freud theorized that infancy is the stage at which we fall in love with ourselves. 

As young children, we develop self-love after witnessing all the things we are capable of. According to Freud, this is a healthy and necessary step in our development. Without it, we'd fail to discover our own importance and would subsequently struggle to reach out to others. In this sense, self-love was a positive thing for Freud. But when it came to his view of human nature overall, he was decidedly pessimistic. 

Freud argued that humans are driven by aggressive and sexual instincts. Decades later, Heinz Kohut, an Austrian psychoanalyst, opposed this idea. Kohut believed that humans are driven by the need to develop a healthy self-image. So narcissism is central to Kohut's theory. The love, admiration and consolation of those around us is what makes us feel special, allowing us to grow into confident, self-loving individuals.

### 3. We can use a spectrum to draw the line between healthy and unhealthy narcissism. 

As humans, we're neither purely good nor purely evil. We're a mix of strengths and weaknesses, so it helps to avoid black-and-white thinking when assessing a person's character. Instead, imagining a spectrum along which different personality traits lie gives a far more accurate understanding of individuals. 

Using a spectrum is useful when thinking about narcissism, too. At one end, we have _abstinence_ — a zero on the spectrum — and, at the other end, there's _addiction_, a ten on the spectrum.

A person whose narcissism is at zero lives a life of abstinence; they have no desire to feel special. These people are selfless in the most frightening sense, believing themselves unworthy of love or care from others. 

But people with a ten on the narcissism spectrum also don't have it so easy. These individuals live in constant need of validation; they push people away with their arrogance and feel deeply disturbed when those around them don't recognize their importance. 

Obviously, life at both zero and ten is pretty unhealthy. Yet living between two and three, or between seven and eight, isn't particularly fantastic, either. Those at two and three on the spectrum may occasionally feel the desire to feel special, but very rarely, often repressing it when they do. Those between seven and eight, in contrast, find it difficult to manage their need to feel special, and tend to be rather selfish. 

If you land somewhere between four and six, however, you're quite stable. Life at five, though, gives the best of both worlds. If you're a five on the narcissism spectrum, you can act on desires to be successful without being consumed by them, and can put your own needs aside when others ask for your support. Moderation is key. But is it really attainable?

### 4. Narcissism fluctuates and comes in three different forms. 

If you worry that you're a little close to one of the extremes on the narcissism spectrum, don't panic! Narcissism _fluctuates_ — that is, we move up and down the spectrum depending on the situations we're confronted with. 

Just think of what it's like to be a teenager. Adolescence is a stage of life when people leave the safety of their parents and seek out their own identity. Narcissism is often necessary; it's what gives young adults the self-esteem they need to make it out of the nest and into the world. 

As well as manifesting to different degrees, narcissism also comes in different forms. There are _extroverted_ narcissists, _introverted_ narcissists and _communal_ narcissists. Let's decode this a little. 

Extroverted narcissists are probably the ones you're most familiar with. These narcissists struggle to resist seeking attention from their surroundings. They'll take any opportunity to show off their wealth, good looks or talent — parking that glossy Porsche where everyone will notice it, turning up to a casual occasion dressed to the nines or taking center stage at every meeting. 

Introverted narcissists, on the other hand, are easier to miss. The thought of being criticized or judged terrifies them, so they avoid attention at all costs. But in their secluded worlds, these narcissists believe they're far superior to others, who are simply too stupid to realize they're in the presence of a gifted being. 

Finally, the communal narcissists are those that you might have recognized, but struggled to put a finger on. Ever met someone who was just a little too proud of all the charity work they've done? Well, communal narcissists pride themselves on being _givers_ ; they think they're far more caring, understanding and generous than others, placing themselves on a pedestal for every good deed they do. 

We now know more about narcissism's many guises. But how does it arise in the first place? In the next blink, we'll explore the interplay between nature, nurture and narcissism.

### 5. Our genetics as well as our upbringing greatly influence our proclivity for self-love. 

Our genes don't just determine our physical features and medical predisposition. At birth, we're already biologically programmed with behavioral tendencies, including narcissism! Some of us may have an innate tendency to be cautious and withdrawn, while others seem to be reckless and loud from an early age. Narcissism, or a lack thereof, can simply be part of our nature. 

But nature isn't the only contributing factor. Nurture — that is, how our parents raised us — has a powerful influence on our ability to love ourselves. 

Take, for example, the story of Jean. Her parents were stern and distant, constantly reminding her to keep her feet on the ground; she was told not to be too proud of her achievements and to stop dreaming of things she'd never achieve. 

So Jean stopped dreaming, fearing that her dreams would cost her the love of her parents. By her teen years, Jean had stopped expecting love and care from anyone. In her adult life, she struggled. She married a philanderer and her family ties loosened. It's a sad story — but such patterns aren't uncommon for people living with a three on the narcissism spectrum. 

Chad's upbringing, on the other hand, was the exact opposite of Jean's. His parents never stopped telling him how great he was, and that he was definitely destined for stardom. But all this praise was empty, and came with neither true empathy nor understanding. Chad grew up to be a vain, arrogant and deeply sad and isolated adult — a destiny that awaits most people with a seven or eight on the narcissism spectrum.

### 6. Narcissism can be spotted if you pay attention to warning signs. 

There might be someone in your life right now who's behaving in ways you just can't fathom. Well, perhaps narcissism is behind it. A complicated character trait, narcissism can be difficult to pin down. But there are a few indicators that can help you spot a narcissist. 

Ever given someone some feedback and been rewarded with a slew of caustic remarks about your many shortcomings? You may have encountered a narcissist's _emotion phobia_. Narcissists lack true self-confidence and are very sensitive to criticism. Unfortunately, the only way for them to feel better about themselves is to make other people feel worse.

Narcissists are also likely to play _emotional hot potato_ — that is, they try to get rid of emotions that make them feel vulnerable by projecting them onto others. Say a friend of yours has ignored your texts, and then later demands to know why you've been ignoring him. He's trying to shoulder his guilt onto you. 

Often, narcissists will go to great lengths to make you experience what they're feeling. Your partner, who tends to be grumpy when under pressure, might insist that you're the one who is constantly angry. You're not, but she's so adamant about it that you ultimately do get quite mad. You've just been served your partner's anger: _bon appétit_! 

This brings us to the focus of our final blink: relationships with narcissists. Now that you know how to recognize narcissists, how should you act around them? Is there anything you can do to make it easier for yourself — or, for them?

### 7. Keep your partner’s narcissism in check by being vulnerable. 

If you're about to give up on your narcissistic boyfriend or girlfriend, take a step back. Not all hope is lost! Narcissism is often quite easy to combat. It just takes a little vulnerability on your part. 

Studies conducted by psychologists from the University of Surrey and the University of Southampton showed narcissists a video where a woman describes her frightening experiences with domestic abuse. The heart rates of the narcissistic participants rose considerably, a sure sign of empathetic feelings. 

When narcissists see that others are in need or suffering, their arrogant, selfish or cold behavior often melts away and is replaced with compassion and understanding. How can you use this to your advantage when dealing with a narcissist? It's simple enough. If they make you feel unhappy, don't hide it. 

Of course, that's easier said than done. We tend to be experts at masking our true feelings. If our loved ones exhibit narcissistic behavior in the form of insensitivity or condescension, we tend to react with anger and frustration. 

But if we can catch ourselves before starting an argument, we have a chance to turn the situation around. Imagine that you're discussing a career move with your partner, and they tell you flatly that you should apply to jobs that require fewer qualifications and carry less responsibility. You might feel quite hurt, but don't respond with anger. 

Rather, calmly explain how their actions make you feel and why. Explain that their opinion matters to you, and that what they said makes you feel as if they see you as incapable or unintelligent. This will trigger feelings of empathy, love and compassion in your partner. Try it out, and watch the narcissists in your life transform before your eyes.

### 8. Final summary 

The key message in this book:

**Narcissism, a trait that's plagued humanity since ancient times, comes in all different shapes and sizes. Some cases are healthy; others aren't. Unhealthy levels of narcissism can be spotted by looking out for telltale signs, and you can even help your loved ones curb narcissistic behavior by showing them you need their love and care.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _The Narcissist You Know_** **by The Joseph Burgo**

"Narcissism" has become a buzzword and a snap diagnosis, but how much do we really understand about this condition? _The Narcissist You Know_ (2015) unpacks the myths and the truths. Narcissism isn't just a serious psychiatric disorder, it's part of life — we all share some tendency toward it. By analyzing a wide range of narcissists — many of them celebrities — Joseph Burgo reveals the hidden shame that lies behind all the pain.
---

### Craig Malkin

Craig Malkin is a clinical psychologist. He's written articles about relationships for top publications, including _Time_ and _Psychology Today._ Malkin is also the director of YM Psychotherapy and Consultation, which offers workshops in couples therapy.

