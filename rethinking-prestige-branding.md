---
id: 588636d15806db0004d7ff30
slug: rethinking-prestige-branding-en
published_date: 2017-01-24T00:00:00.000+00:00
author: Wolfgang Schaefer and J.P. Kuehlwein
title: Rethinking Prestige Branding
subtitle: Secrets of the Ueber-Brands
main_color: FDD463
text_color: 7D6931
---

# Rethinking Prestige Branding

_Secrets of the Ueber-Brands_

**Wolfgang Schaefer and J.P. Kuehlwein**

_Rethinking Prestige Branding_ (2015) is your guide to the radically transformed world of prestige brands. These blinks explain how the practice of building a prestige brand has changed, what customers want today and what you need to do to make your brand a coveted object.

---
### 1. What’s in it for me? Learn how to build a modern prestige brand in today’s market. 

You might think that energy drink company Red Bull and luxury fashion house Hermès have nothing in common. Yet if you look closely, these two world-famous brands follow the same seven marketing principles — steps that have ensured their continued success.

So what are these seven principles? And how can they help brands — including yours — outperform the competition? These blinks will explain how you can transform you brand into an "ueber" brand.

In these blinks, you'll learn

  * how to turn your brand into a hero;

  * what a "velvet rope" is in the context of branding; and

  * why a face cream that comes with complicated instructions is prestigious.

### 2. Exclusivity and high prices don’t signify prestige anymore, as technology has leveled the playing field. 

As the global economy changes, business strategies are changing along with it. So what do companies need to know to build a prestige brand today?

One strategy that doesn't work as well as it once did is _high prices_. Brands were once able to distinguish themselves — and their customers — through extravagant price tags. The idea was that a product would become a status symbol when only a few, hyper-wealthy individuals could afford it.

Now practically anyone can buy or borrow the prestige of a brand. Online flash sales offer designer clothes at a discount while luxury cars can be rented by the hour.

As a result, such luxury items have lost their once exclusive status.

The new status driver is knowledge. In our information age, people idolize superior intelligence. Just consider the rising status of rich IT professionals, once seen only as "nerds."

This means that your brand can attain prestige by being "in the know," for example, by employing novel technologies or staying ahead of new fashion trends.

A knowledgeable brand can, in turn, help customers appear knowledgeable, as privileged information is sometimes necessary to "get" a brand. A good example is a skater who distinguishes herself from posers by only wearing "real" skater gear.

Another crucial aspect of establishing brand prestige today is the need to be honest and ethical in your business dealings. Using child labor to assemble clothes or polluting the environment are wrong; consumers today demand better of the brands they support.

With online technology, customers can now rapidly share information about various brands, and bad news travels quickly. Information about a company's unethical behavior can destroy a previously stellar brand image.

Consumers have used social networks to shame brands publicly over false promises or exploitative working conditions — some have even organized brand boycotts.

Companies that promote social responsibility, however, can use the same channels to connect with customers over the environmental and social issues they care about, earning their respect.

In our next blink, we'll explore the secrets to prestige branding in the digital age.

### 3. Prestige brands define excellence themselves and provide the authenticity that consumers now seek. 

In 1873, German philosopher Friedrich Nietzsche theorized about a new type of person that he called the _Ueber-Mensch_. This person is independent, ahead of the masses; he lives by his own rules and values.

Prestige brands today function like this theoretical person. _Ueber-Brands_ adhere to self-defined principles and ideals rather than striving to excel in generic, pre-established categories, such as being the fastest car on the market, the most expensive fur coat or another restaurant with three Michelin stars.

In short, instead of striving to meet someone else's terms, a brand defines excellence for itself.

So what exactly makes a brand an "ueber" brand? A good example to look at is Apple.

Apple isn't interested in creating the most technically advanced products on the market but rather seeks to make technological works of art that integrate seamlessly into everyday life.

Such a mission is a part of a larger corporate trend in which many leaders, including top CEOs, are wanting to do something more meaningful than merely generate profits and satisfy shareholders. These leaders care not only about their products but also about their customers and the impact company operations have on the environment.

Unilever CEO Paul Polman, for example, says that he works for his customers and isn't driven solely by the traditional goal of boosting his company's share price.

And customers too have changed their tune, seeking more meaning and authenticity in the products they purchase.

Mass-produced, interchangeable products are no longer valued; people yearn for things that are individualized and honest. Consumers might prefer soap handmade with real flower petals to the standardized and synthetically scented soap available at any corner drugstore.

Simply put, today's customers want more substance. So how can Ueber-Brands satisfy this consumer hunger for meaning and uniqueness?

### 4. Establishing an Ueber-Brand means defining and promoting your company’s unique mission. 

Becoming a prestige brand today requires more than just making beautiful, well-designed products. Your company also needs to follow the seven secrets of prestige branding.

Here's the first secret. To stand out from the competition, your company needs to adopt a unique mission, a goal that you stick to at all costs. Today's consumers want the process of buying to have more meaning so to set yourself apart from the competition, you need to establish your inspiration and explain the drive behind why your brand exists.

Of course, most companies are in business for the money — but a mission statement that says "to yield insane profits" is neither unique nor endearing.

Step away from the profit-focused past and create a goal that means something, even if it alienates some of your customers.

German supermarket chain Edeka offers a good example of how a well-defined mission can boost a brand. The brand decided that it wanted to become a supermarket chain devoted to quality food.

The company carefully redesigned its shops to reflect this new goal and trained employees to know more about Edeka's products.

In the process, Edeka risked losing customers who shopped there looking for low prices or specific goods. In the end, however, the chain emerged with loads of new and devoted, quality-conscious customers.

So how can your company do the same for your brand?

There are two ways to pick and promote your mission. The first is to commit to a social, environmental or political goal. Outdoor gear company Patagonia, for example, made a point of demonstrating its commitment to sustainability in outdoor wear by producing durable products made of discarded clothing and recycled material.

The other way is to redefine a category or set a standard, such as Starbucks did with coffee. This coffee chain staked its claim in the market by creating European-styled cafe spaces for enjoying coffee as well as a unique language to describe its drinks and sizes that would become synonymous with its brand.

> _"If you're not pissing off 50 percent of the people, you're not trying hard enough."_ — Yvon Chouinard, founder of _Patagonia_

### 5. Ueber-Brands balance exclusivity and belonging while seducing new customers to join the club. 

When a product enters the mass-consumption market, it tends to lose its prestige status. So how does ice cream brand Ben & Jerry's, for example, maintain its "hipness" while being available in nearly every supermarket?

Ben & Jerry's is an example of the second secret to Ueber-Branding: balancing exclusivity with inclusivity.

To reach this sweet spot, the company employs advertising that addresses a desirable in-group of cool, beautiful and successful people. This group constitutes the company's "design target."

After attracting this target group another, much larger set of consumers naturally follows suit. These are people who want to belong to the target group but aren't quite hip or beautiful enough to belong.

Importantly, this group believes that consuming prestige brands will "make" them cool.

We can think of this divide metaphorically as if the two groups are divided by a "velvet rope," like movie stars are from fans at the Academy Awards.

In the case of Ben & Jerry's, the company's design target is composed of hip, fit, slightly politically left-leaning people who care about the environment and particularly are turned off by "normal" flavors of ice cream. The "fans," in contrast, are less hip or sophisticated and seek the brand out as a badge of association.

It's also essential for an Ueber-Brand to attract customers in subtle yet seductive ways, steering clear of the hard sell. While discounters and mass brands push blatant sales pitches, doing the same would hurt the prestigious, desirable image of an Ueber-Brand.

Of course, Ueber-Brands need to advertise, too. Their goal, however, is subtlety. This technique is called "un-selling," and it's the third secret of prestige branding.

Clothing company Abercrombie & Fitch, for example, rarely displays its products in its advertisements. Its ads might simply inform a prospective buyer of the brand's proud heritage; it might instruct a consumer on how to care for such a precious product. In doing so, the company conveys the message that owning an Abercrombie & Fitch product is a worthy aspiration.

### 6. Cultivate an inspiring story around your brand to draw in customers and keep them hooked. 

Ever since our human ancestors shared hunting lore around a cooking fire, telling a good story has become part of the fabric that unites people and society. This love of stories, along with a desire for meaning, are powerful tools to boost your brand.

Notebook company Moleskine, for example, tells a great brand story of preserving the fading tradition of hand-written notes amid the pressures of a digital age.

Using a Moleskine notebook thus can be framed as an act of resistance in a world dominated by technology. Moleskine also reminds its customers of great writers, like Ernest Hemingway, who scribbled ideas in a notebook kept in a pocket. The implication is that, when you buy and write in a Moleskine, you're following in the footsteps of literary greats.

Storytelling also allows a brand to construct a myth or story that people can believe in, especially as the pull of religion has diminished in society at large. A brand can work to fill this void by presenting its history as myth, painting the company as a "hero of our times."

Telling a great story or creating a powerful myth is the fourth secret of prestige branding. Here's how to best use it.

You don't need to create a story all on your own. In fact, your audience will happily connect the dots for you. What you need to do is provide all the essential aspects of an engaging story.

Begin by presenting a character with whom consumers can identify. Such characters tend to be based on archetypes or universal roles like "the hero" or "the rebel." Once you've defined a character, develop her story by describing how she overcomes a challenge.

The luxury fashion brand Chanel, for example, tells the story of the company's founder, Coco Chanel. This woman, born to an unmarried clothes washer and raised in a convent, went on to tap her natural talent for design and became one of the most famous fashion icons of the twentieth century.

Telling a story like this can entice as well as inspire customers, as they come to perceive your brand as heroic and associate an inspiring story of triumph with your company.

> _"Stories are like a drug, they shut our brains off for a while or at least they calm them down."_

### 7. To truly become a prestige brand, your product needs to deliver on the expectations you create. 

So now you've come up with a compelling story and new customers are intrigued, eager to learn more about your company and your product. What characteristics should your product have to ensure a customer leaves with one or several in hand?

Crucially, an Ueber-Brand needs to offer "ueber" products; that is, products that fulfill the promise made by the brand's myth.

You'll never establish a prestige brand without making a product that meets or exceeds the expectations you've created through your myth.

So the fifth secret of prestige branding is thus: your product is essential to maintaining your prestige brand as it manifests your brand's story.

The brand story of sports car company Porsche is based on performance. There's certainly no question that Porsche sports cars are some of most high-performing automobiles in the world.

Conversely, a product that fails to meet expectations will disappoint customers who were brought in by the company's myth. This discrepancy will turn off a potential customer quickly.

Yet exceeding expectations isn't just achieved by creating a high-quality product. You also need to present your product in the market in an appealing fashion.

You can do so by setting your product up like a "holy grail." In other words, make your product appear vastly superior to others. Beverage company Nespresso nailed this; it displays its espresso machines in special boutiques, the machines perched on illuminated pedestals, like precious works of art.

Another strategy is to create a ritual around the consumption of your product. For instance, before a consumer can apply Crème de la Mer, a high-priced facial cream, the user needs to carefully melt just the right amount between two fingers. The company's website offers how-to videos, describing its "enhanced application techniques."

### 8. Realize your mission and myth while moderating growth to preserve your prestige brand. 

Do you think that a hip, unconventional brand makes its employees work in gray cubicles from nine to five? Hardly. The sixth secret of a prestige brand is to align your day-to-day operations with your mission and myth.

Your company's mission and myth should first be reflected in your corporate culture. Otherwise, the public will sense your disingenuousness, and your myth will die.

Such a focus is key for brands that want to be cool but aren't quite there. Creating unconventional work environments will indeed attract creative, independent employees who will help you realize the public image to which you aspire.

Without such a strategy, your brand will be seen as inauthentic, and no doubt will be outed as the conventional imposter that it is.

You also need someone on board who can translate your brand mission into profitable business decisions. Many Ueber-Brands are led by two-person teams, an artsy visionary and a business buff. Gucci is a great example, a fashion company that boomed once businessman Domenico De Sole was paired with designer Tom Ford.

The final secret of prestige branding is to focus on smart, measured growth. To survive in a competitive world, a company needs to grow. But an Ueber-Brand knows that growing slowly at first is a small price to pay for long-term development and the perfection of a product, mission and myth.

Sticking to this strategy will eventually pay off. Your company will see exponential growth, and your well-told myth will make your brand wildly attractive to consumers.

But don't forget balance. As a prestige brand, you'll need to control growth to keep your brand suitably rare, unique and most importantly, true to your mission.

Some companies seem to act counterintuitively to protect their mission. Outdoor clothing company Patagonia, for example, to stress its mission of sustainability, asked its customers to buy fewer clothes!

### 9. Final summary 

The key message in this book:

**Shifting economic conditions have transformed prestige branding into "ueber" branding. This practice is similar to its predecessor in scope and importance but works in a different way. At the center of an "ueber" strategy are a brand mission and myth that work to draw in customers and grow a company.**

Actionable advice:

**Make clear what your company doesn't do.**

The credibility of your brand doesn't just stem from what you do but from what you don't do. As a company, you need to be clear about your boundaries. If your brand mission is to produce high fashion in an ethically responsible manner, you should never make fur coats — even if they're the rage of the coming season.

In addition to making clear what you don't do, let your clients know why. You might lose sales in the process, but doing so will signal to your core clientele that you care about the company mission. In turn, your clients will be even more loyal.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _What Great Brands Do_** **by Denise Lee Yohn**

In _What_ _Great_ _Brands_ _Do,_ author Denise Lee Yohn draws on her over twenty-five years of experience in brand-building to demystify the branding process. You'll discover the main principles that have made brands such as Nike, Apple and Starbucks such iconic household names.
---

### Wolfgang Schaefer and J.P. Kuehlwein

Wolfgang Schaefer is chief strategy officer for advertising agency SelectNY. He has years of experience in developing global brand strategies for European, American and Asian markets. He is also the cofounder of Ueber-Brands Consulting.

JP Kuehlwein is brand director for Procter & Gamble and a lauded business leader, strategy expert and brand builder. He is a cofounder of Ueber-Brands Consulting.

