---
id: 57d65fe308c9270003b191d5
slug: retromania-en
published_date: 2016-09-15T00:00:00.000+00:00
author: Simon Reynolds
title: Retromania
subtitle: Pop Culture's Addiction to Its Own Past
main_color: FBE43E
text_color: 7A6F1E
---

# Retromania

_Pop Culture's Addiction to Its Own Past_

**Simon Reynolds**

_Retromania_ (2011) takes a critical look at the state of modern pop music and asks what happened to all the innovative energy behind the pop of the past 50 years. Why hasn't there been another groundbreaking innovative musical movement like the punk-rock explosion of the 1970s or the hip-hop boom of the 1980s? Find out what's keeping today's artists from creating the next great rock 'n' roll revolution.

---
### 1. What’s in it for me? Learn why music really was better back in the day! 

You've probably heard some nostalgic grouch complaining about the sorry state of today's music. "We used to play real instruments," such people like to say, or "In my day, music was innovative." Most of us shrug off these sentimental laments, but what if they actually hold some truth? What if things really _were_ better back in the day?

Examining the music of this millennium's first decade shows that the complainers are driven by more than just nostalgia and conservatism. Music used to be more progressive; it used to really push the envelope. Maybe it's the music of the aughts that's filled with nostalgia, conservatism and mania for the past. All you have to do is open your favorite streaming service and you'll be overwhelmed with evidence supporting the arguments these blinks make.

In these blinks, you'll find out

  * how bands like the Fleet Foxes wouldn't exist without 1960s music;

  * why dubstep might be the only original music of the 2000s; and

  * that technology keeps us in the past rather than pushing innovation.

### 2. Since the year 2000, both experimental and mainstream popular music have failed to innovate. 

Do you think the quality of music has taken a nosedive in the new millennium? Perhaps you've gone to recent concerts by so-called innovative bands and been disappointed by their atonal chords and lackluster sound experiments.

The problem with modern experimental music is that its methods are stuck in the past, and the musicians are struggling to come up with something new and exciting.

This wasn't the case in the 1990s. Then, music fans were constantly being surprised by innovative artists. The techno and rave movements exploded onto the scene, helped along by impromptu and drug-fuelled parties illegally hosted in abandoned warehouses.

But the turn of the century seems also to have marked an unfortunate turn in music history: the new millennium has brought no new music.

You can get a good sense of this stagnation by going through music magazines or websites such as _Wire_, _Pitchfork_ or _Fact_. Their articles and reviews make clear that nothing new is happening.

Even the most experimental genres derive from ideas that have been around for at least 20 years, whether it's the minimalistic repetition of _drone_ or _nois_ e, the unstructured freewheeling of improv music or the disharmonies of atonal music.

And so the mainstream music of the 2000s pulls out all the stops in an attempt to appear groundbreaking.

Take the Black Eyed Peas, for example. In order to appear cutting edge, they lean heavily on futuristic and sci-fi scenarios in their videos and apply robotic effects when the lead singer is delivering lyrics such as, "I'm so 3008, you're so 2000 and late."

But the Black Eyed Peas aren't as innovative as you might think. Their beats recall those that Missy Elliott used near the end of the 1990s, and their use of Auto-Tune — a device that corrects a singer's pitch with unnatural accuracy to give it an electronic sound — was pioneered by Cher in her hit song "Believe," back in 1998.

> "_Play me some music that is characteristic of the late 2000s as opposed to the late 1990s"_ \- Jaron Lanier, American musician and composer

### 3. Major new pop movements bloomed between the 1960s and the 1990s. 

While there have been exciting advances in music throughout history, pop music really took off in the 1960s.

Indeed, the progress music made can be charted by tracing the major innovative shifts that occurred from the 1960s to the 1990s. 

While the 1960s are perhaps most famous for the advent of British _beat groups_, such as The Beatles and The Rolling Stones, this represents but a sliver of what was going on. The decade also gave rise to _folk rock_, _psychedelic music_, _soul_ and a new form of Jamaican pop music called _ska_.

The 1970s were arguably even better, with bands like Led Zeppelin, Blondie and Black Sabbath introducing an exciting era of _glam rock_, _funk_, _heavy metal_, _disco_, _punk rock_ and _reggae_.

In the 1980s, there were innovative mainstream artists like David Bowie, Madonna, Prince and Michael Jackson, each flourishing alongside new genres like _rap_ and _house music_. This decade also witnessed the emergence of _goth music_ and _synth pop_ bands like Depeche Mode.

And the 1990s is when we saw the rise of _rave music_, _grunge_ and _experimental rock_ bands like Talk Talk.

But this parade of innovation comes to a sudden halt when we hit the 2000s.

In fact, most of the new music from the 2000s merely offers a slightly different take on pre-existing genres.

For example, _emo music_ is basically punk rock from the 1980s with slight changes to melody, style and presentation.

Possible exceptions could be made for _grime_ and _dubstep_, two sub-genres of EDM, or electronic dance music. While these styles build on the broken beats of 1990s _jungle_ and _garage_ remixes, they do manage to create a new sound, rather than just rehashing previous styles.

Tinchy Stryder and Magnetic Man even made it into the pop charts. Beyond that, however, grime and dubstep haven't gained much mass appeal.

> _"What was lacking in the 2000s was movements and movement."_

### 4. The 1960s, music’s “golden age,” still have a strong influence on today’s artists. 

If you're a music fan, you probably feel some nostalgia or longing for the revolutionary energy of the 1960s, even if you weren't around to witness it.

A glimpse at the music of the 2000s betrays what an influence the 1960s — and especially the late 1960s — has had on modern artists. 

During these innovative years, many artists moved away from psychedelic rock and, in an effort to make more authentic music, returned to their roots in folk and country.

In the 2000s, we have the _freak folk_ and _new Americana_ movements. Extremely derivative, this music is produced by artists donning beards and plaid, which are meant to signal authenticity and maturity.

Perhaps Fleet Foxes is the band most representative of this trend. Their songs, such as "Ragged Wood" and "Blue Ridge Mountains," evoke the 1960s by blending country and folk music. Their videos also use imagery that suggests a return to the land.

Fleet Foxes's self-titled 2008 album was quite successful, and the members openly acknowledged that, while growing up, they were greatly inspired and influenced by the music their parents played for them.

Another reason the 1960s continue to remain strongly influential is because those years symbolize a golden age when people believed in something.

For example, Sandi Thom's 2006 UK chart-topping hit "I Wish I Was a Punk Rocker (With Flowers in My Hair)" perfectly sums up the nostalgia felt by a generation of young people who feel rudderless and without a cause. They regard the 1960s as a time when music was connected to independence, when it had a meaningful impact on society.

Some fans felt so strongly connected to the song's message that they burst into tears upon hearing it.

This reaction could be due to the song's lacking the kind of passion that can be found in punk rock or the songs of the 1960s. So, rather than feeling inspired and uplifted, audiences are left feeling sentimental and mournfully resigned.

> _"The yearning for this totally blurred memory of the past is delicious, but the songwriter recognizes that there is no going back."_ \- Fan of Sandi Thom

### 5. When young artists fail to let go of the past, they also fail to be innovative. 

With today's music streaming services, you have the complete catalogs of The Beatles, The Stones and even Irving Berlin or Louis Armstrong available at the touch of a finger.

This is great for fans of music from previous generations. However, part of the problem with music in the 2000s is that today's younger artists are reluctant to let go of music's glorious past.

Therefore, the popular musical genres from the 1960s to the 1990s are still popular today and continue to attract young artists, which results in a lack of innovative music.

This wasn't always the case. The young generations of the past routinely rebelled against their parent's music or simply wiped the slate clean and introduced a whole new movement.

We saw this happen with kids in the 1980s when hip-hop came along. Suddenly, everybody was a _b-boy_ or a _fly girl_, and kids began DJ'ing, breakdancing, rapping, getting into graffiti culture and starting new fashion trends.

This movement came out of nowhere, and yes, it did draw on elements from the past, but it also moved beyond them and defined a completely new form of music.

But these days there is more emphasis placed on recycling and remixing past music than introducing something new.

On one hand, it's normal to look to the past for inspiration. The 1970s drew inspiration from the music of the 1950s and the 1980s often referred back to the styles of the 1960s.

But then we have the 2000s, where we find musicians simply content to recycle or combine old styles.

In the new millennium, we've seen a 1980s electro-pop fad randomly followed by a post-punk phase, or periods where garage-punk or neo-psychedelic bands take over. But none of it is advancing in a clear direction or creating a new movement.

For example, Lady Gaga's hit song "Fashion!" not only sounds very similar to David Bowie's 1983 song "Let's Dance" — the title is actually taken from another Bowie song!

> _"Nothing on the game-changing scale of rap or rave came through in the 2000s."_

### 6. Modern music’s focus on technology has inspired super-hybrid musicians. 

These days, music and technology are more entangled than ever. Indeed, your own listening habits probably rely heavily on your computer.

In all likelihood, the 2000s will be remembered as an era of technological innovations in music rather than as one of musical innovations. 

In the past, bands like The Beatles, David Bowie, Bob Dylan and The Sex Pistols were responsible for musical innovation; these days, however, the major musical innovations include things like the iPod, YouTube, Spotify and Pandora.

The changes in the music scene now have less to do with music styles and more to do with accessibility, interconnectedness and how quickly you can tap into a new global musical network. 

The innovation in today's music lies in the technology that allows for a limitless archive of music and for people to seamlessly navigate through all musical eras and genres.

This technology and the amount of music it makes available also greatly influences today's musicians.

Just consider Gonjasufi, a musician from Los Angeles whose work is influenced by a merging of music from Mexico, Ethiopia and America.

His work is further influenced by technology that allows him to take scraps from diverse genres and stitch them together into strange musical quilts: the basic structure of his music takes elements from hip hop, but there are melodic layers informed by 1960s punk and psychedelic music as well as the trance and rave scenes.

The effect of bringing all these influences together creates a timeless kind of music that seems to transcend the particularity of genre. By merging music from different decades and countries, Gonjasufi has made something that's difficult to classify.

Nonetheless, critics have coined a term for artists that create this kind of music: _super-hybrids_.

### 7. Post-production now takes precedence over production – and this reflects our post-industrial world. 

Just as technology has changed the way we listen to music, it has also altered the way music is made.

In the world of modern pop music, the focus has shifted from how music can be produced to how it can be manipulated in post-production.

This means that today's musicians are less concerned with playing and composing music and spend more time reworking, editing and remixing existing music.

The epitome of the post-production musician is the DJ. He entertains a dancefloor audience by taking existing music and reassembling it in clever or surprising ways and adding tempo and volume changes, all without really creating anything new.

This is by definition a form of post-production: DJs use material that was produced earlier, most of which is pop music created in the 30 years following World War II. Much of the time, they're using the creative work of black musicians who made soul, funk, disco and R&B music. 

With this in mind, you can even look at the shift from production to post-production in music as a reflection of how Western economies have developed. 

After all, many of the great eras in musical production coincided, spatially and temporally, with industrial production.

Motown music, for example, took off at the beginning of the automobile boom in Detroit, Michigan; Motown is actually an abbreviation of Motor Town.

Heavy metal is another example. It was born in the West Midlands of the United Kingdom, which was the center of British automobile manufacturing at the time.

Similarly, we can look at DJs and the dominance of post-production in music as a reflection of today's white-collar workforce, who spend the majority of their time in offices. Instead of manufacturing and building, there's more scrolling, clicking, programming and designing.

In this sense, modern pop music is the perfect reflection of our post-industrial world.

### 8. Final summary 

The key message in this book:

**It's not a false impression: the current music scene lacks innovation and relies too strongly on re-creating and remixing the styles of bygone musical eras. We need a new revolution so that the music of the 2000s can be remembered for something more than just the iPod.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Steal Like an Artist_** **by Austin Kleon**

_Steal Like an Artist_ (2012) will help you unlock the secret to creating great art: theft. No artist creates their work in a vacuum: all art is influenced by the art that came before it. _Steal Like an Artist_ teaches you how to "steal" from the work of your heroes, and use it to create something new and unique. It also provides important advice on using the internet to launch your career, so others can enjoy your creativity!
---

### Simon Reynolds

Simon Reynolds is a British music critic and journalist. After working for the magazine _Melody Maker_ in the 1980s, he became a regular contributor to the _New Yorker_ and the _Guardian_ and wrote several books on the history of music.

