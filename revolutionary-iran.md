---
id: 5bf79ebb6cee070007cefd5c
slug: revolutionary-iran-en
published_date: 2018-11-23T00:00:00.000+00:00
author: Michael Axworthy
title: Revolutionary Iran
subtitle: A History of the Islamic Republic
main_color: 6AA0B8
text_color: 4C7385
---

# Revolutionary Iran

_A History of the Islamic Republic_

**Michael Axworthy**

_Revolutionary Iran_ (2013) tells the story of modern Iran, from the early twentieth-century origins of the 1979 revolution through to reactions to Ahmadinejad's second presidential victory, in 2009. The book also dispels misconceptions and examines internal politics and cultural debates within the country.

---
### 1. What’s in it for me? Discover the modern political history of the majestic, yet volatile, Iran. 

The nation of Iran is an ancient one. For many centuries, it was called "Persia" in the West. The country has been the home of several empires, too, from the sixth century BCE right up to the twentieth century. These autocratic dynasties ruled over Iran for hundreds of years, but democracy, along with a host of other political ideologies, influenced the nation in the early twentieth century. Change was on the horizon, but it wasn't entirely clear where that change would take the country.

For most people, it's hard to understand exactly how Iran ended up as a simultaneously democratic and theocratic Islamic state after the 1979 revolution. And it's even more perplexing these days, with Iran increasingly being held up as a pariah state by the Trump administration. In circumstances like these, it's a great idea to be informed about Iranian history, and few people are better equipped to shed light on the subject than Michael Axworthy.

These blinks examine the formative period of Iranian democracy so you can get a clear understanding of the twentieth-century history of this thoroughly modern, and globally significant, nation.

In these blinks, you'll learn

  * how a poetry reading signalled a full-blown revolution was on its way;

  * methods to keep your grip on power if you're leading a revolution; and

  * a bundle of details about the Iranian constitution.

### 2. The foundations of the 1970 Iranian revolution were laid at the start of the twentieth century. 

Iran, though an ancient nation, was no less vulnerable than younger countries to the upheavals and revolutionary atmosphere of the early twentieth century. And so, in 1905, when faced with economic instability, Iran felt the first stirrings of civil unrest. Demonstrators soon took to the streets.

Things soon escalated when, in July 1906, police killed a theology student at a rally. This led to further protests and the deaths of 22 more people. The situation culminated in mass strikes and vocal denunciation of the country's monarch, Mozaffar od-Din Shah.

It was clear that the Shah would have to cave. On August 5, he accepted one of the protestors' main demands and issued an order that an Iranian national assembly should be established. In October 1906, the _Majles_ convened for the first time.

The Majles set about swiftly drafting a constitution, which was ratified by the Shah on December 30, just five days before he died of a serious illness. It confirmed that the Shah's sovereignty was given by the people, not by God, and that this power had been given in trust. It also declared Shi'ia Islam as the state religion and established a committee of Iranian clergy, _ulema_, to review legislation passed by the Majles.

This legislation represented a political victory, but the revolutionary momentum that had ushered them into law was already dwindling. 

By 1908, the _ulema_ were turning against constitutionalism; they regarded new reforming legislation as a threat to their traditional authority. The Shah's successor, his son Mohammad Ali Shah, was equally skeptical of the Majles. In 1908, set on a return to absolute monarchy and with the backing of the _ulema_, Mohammad Ali staged a military coup.

Though he met with success in Tehran, he failed elsewhere. The revolutionaries successfully countered, and, in 1909, the Shah was forced into exile. His young son Ahmad succeeded to the throne. The constitutionalists were back in control.

But these changes didn't solve the country's deeper problems — political polarization and a dangerously out-of-control revolution. The country was descending into complete disorder; assassinations were common on both sides.

Then, in December 1911, conservative factions in the cabinet staged what amounted to a successful coup and the Majles was dismissed. It seemed to everyone that the revolution had been stopped dead in its tracks. However, the seeds for later revolutionary changes had been sown.

### 3. The founding of the Pahlavi dynasty had significant repercussions. 

In 1908, a British venture managed to identify the first commercially viable oil source in the Middle East — southwest Iran. Thanks to this knowledge, the British became the dominant foreign power in the country by 1920.

But not all was rosy. The British were also overstretched and had an eye on getting out. It was in this colonial climate that the British senior commander, General Ironside, promoted an Iranian soldier, Reza Khan, and gave him a brigade to lead. Ironside was clear: if Khan were to stage a military coup in Tehran, the British would let him do it. And in 1921, Khan did exactly that.

Khan seized the moment and successfully took Tehran and the rest of the country. He established a new regime, but it took a few false starts before he found a version he was happy with. That is, Khan initially set up a new government with himself as prime minister. He even attempted to make Iran a republic in 1924. But, less than five years later, even though he had no royal or aristocratic heritage, he had himself crowned as Shah and adopted the name Pahlavi. Thus, a new dynasty was founded.

Generally speaking, few had objections to his coronation. Both constitutionalists and the _ulema_ identified him as the man to usher in reforms to assist in the nation's development.

Khan, whose reforms tended to be both nationalist and secularizing, managed to bridge the two groups. For instance, he reorganized the legal system by elevating secular judges and issuing European-style civil and penal codes.

Khan's significant reforms didn't stop there.

To start with, in the 1920s, Khan put 40 percent of state expenditure toward the creation of a modern army. He then used it to brutally suppress dissent outside urban areas. As Iranians badly wanted stability, most were happy to accept Khan's ruthless strategy.

However, one edict that did prove controversial was the Shah's ban on traditional dress and headwear, including the veil. He much preferred Western-style clothing and so ordered them to wear it.

Perhaps most significantly, there was a huge education drive. By 1938, school attendance was up to 450,000 from 55,000 in 1922. The Shah also founded Iran's first university proper in Tehran, and even made scholarships available for Iranian students to study abroad.

Interestingly, Tehran's university had a theology faculty founded on a new outlook. Instead of teaching religion as an absolute truth, it was now understood in more relative terms.

The Pahlavi dynasty marked a dramatic transformation for Iran. But it was to be relatively short-lived.

### 4. By the late 1970s, conditions for an Iranian revolution had reached a head. 

By the 1970s, Iran had undergone tremendous growth, in large part due to the booming oil industry. But cracks were beginning to show and inequality and corruption were endemic. The new Shah — Reza Shah's son, Mohammad Reza Shah — was facing international pressure; his regime's human-rights record was abysmal.

As a result, the regime began to relax its grip. And, consequently, this move away from repression created openings for political opposition.

To begin with, a few political prisoners were released, and the Shah met Amnesty International representatives to discuss how to improve prison conditions.

These small changes emboldened activist groups like the National Front and the Writer's Association. They emerged from hiding. Before too long they were calling for the restoration of the 1906 constitution and the establishment of a constitutional monarchy.

The changing mood was signaled by the Goethe-Institut's decision to host ten evenings of poetry readings in Tehran. These began on October 10, 1977. The atmosphere was charged, speeches were given between readings criticizing the Shah's regime and one speaker even called for a minute of silence on behalf of the writers who had been censored in the previous 50 years.

The audience grew and grew, despite the risk they knew they were running. By the end, thousands of people were attending, even traveling from remote parts of the country. Ideas that were first pushed by radicalized sections of society had become mainstream by the end of the 1970s.

On September 4, 1978, open-air prayers on Eid-e Fetr, the celebration marking the end of Ramadan, morphed into an impromptu demonstration. By the end, the crowd — all shouting in favor of the exiled religious leader Ayatollah Khomeini — was between 200,000 and 500,000 strong and filled almost the entire length of Tehran.

A truly popular movement was now in the ascendant. In these tinderbox conditions, all that was needed was a spark.

The spark soon came. Just a few days later, soldiers fired automatic weapons straight into crowds at a follow-up demonstration. Around 80 people were killed. Rather than stopping the movement, the massacre only succeeded in ensuring that demonstrations multiplied on into the autumn.

Iran had grown mighty rich during the twentieth century, but now it was clear that societal change was necessary to address animosities that had never truly been resolved. Iranian society was about to be utterly reshaped.

### 5. The 1979 Iranian revolution swiftly toppled the monarchy and established the Islamic Republic. 

In 1979, Iran was once more ripe for revolution. The previous year had been characterized by protests; demonstrators had railed against economic inequalities, political repression and the Shah's autocratic behavior.

The return of Ruhollah Khomeini to Iran, on February 1, 1979, indicated that change was imminent.

Khomeini was a popular _ayatollah,_ a high-ranking member of the clergy, who had been exiled in 1964 for repeatedly criticizing the government and the Shah. He still retained a following in the country and continued to voice his opinion on Iranian politics.

Khomeini also provided a model for the foundation of the Islamic Republic, based on the principle of _velayat-e-faqih_. He outlined this in his book, _Hokumat-e Eslami_, or "Islamic Government." In essence, his book argued that the only legitimate law was Islamic law as interpreted by Emams, the descendants of the prophet Mohammad.

As a result, he declared the Iranian Constitution illegitimate, chiefly because it allowed for a monarchy, which, he claimed, Islam was opposed to.

No doubt, these were radical arguments, but in 1970 most people in Iran were unaware of quite how far Hokumat-e Eslami went. For them, Khomeini represented just two main principles: the removal of the Shah and the creation of an Islamic government. What's more, Khomeini never actually specified what that government would look like.

The Shah saw which way the wind was blowing. On January 16, before Khomeini's return, he fled Iran.

On February 5, within days of his arrival, Khomeini held a press conference to announce a new prime minister as well as his plans to establish an Islamic state. First, a referendum would be held to confirm popular support for the issue. Then a constituent assembly would be called to write a new constitution. Following that, there would be elections for the creation of a new Majles.

There was only one issue. The old government, led by the Shah's prime minister, Shapur Bakhtiar, had not yet fallen. Fighting broke out between it and anti-government factions. The army sensed where things were headed though, and, on February 11, declared itself "neutral." Bakhtiar had no choice but to go into hiding.

The date is still celebrated today as the anniversary of the final victory of the Islamic revolution. But events had not yet run their course. The end of the monarchy was a mighty victory, but stability had not yet returned.

### 6. The events that immediately followed the revolution shaped the country for years to come. 

The fall of the Shah had happened relatively quickly, but the disorder that followed was to last for years. In particular, the few months after the revolution would define the very essence of the new Islamic Republic.

Many people were thrilled at the changes within the country, but the situation was nonetheless chaotic and strenuous.

Strikes that had preceded the revolution meant supplies of staple goods, including food and cooking fuel, were low. Shortages were common. What's more, gangs of young men roamed the streets inciting violence. They were after supporters of the old regime, and they sometimes targeted foreigners, too.

People just didn't know where to look for answers: the old government had been replaced with numerous new independent centers of power. These included local revolutionary committees that essentially operated off the leash.

Uncertainty, a multipolar political system and occasional extrajudicial violence — these three features of the Islamic Republic's first months are still present today.

If confusion reigned in the rest of the country, it was at least clear that Khomeini and his allies knew what they were doing when managing their own ambitions.

When Khomeini returned to Iran in 1979, many people were dubious about his real intentions. Some thought he would seize power and establish a religious dictatorship.

But Khomeini wisely avoided autocratic decision-making. He collaborated with his inner circle and cultivated support from a large range of groups. He even was happy to allow secular politicians positions of responsibility in the government.

It was all very clever, as it prevented the sort of rolling revolutions that had characterized the 1917 Russian and the 1789 French revolutions. In those, the first wave of revolutionaries was removed by a second, and the situation only deteriorated from there.

In contrast, Khomeini, thanks to this initial caution, was able to hold onto the religious, cultural and social power that he had so cannily assumed right up until his death in 1989.

### 7. In the early 1980s, the Islamic Republic faced severe internal strife. 

In the post-revolution aftermath, Iran was still trying to find its footing. At the same time, there were other major challenges to be dealt with.

Neighboring Iraq sensed Iran's weakness and invaded in 1980. An eight-year war followed. Meanwhile, internal struggles in Iran could not be quelled. Already by 1981, Khomeini was at serious odds with the Islamic Republic's first president, Abolhassan Bani-Sadr. Bani-Sadr was stripped of his post and forced into hiding.

Even greater problems lay ahead. To begin with, factional struggles took a turn for the worse. Violence became endemic.

On June 28, 1981, a huge bomb exploded at the Islamic Republican Party's headquarters. Though not the ruling party at the time, the IRP represented the interests of Khomeini's followers. The bomb was believed to have been placed by the People's Mujahedin of Iran, an extremist political organization whose ideology combined Marxism and Islam.

The roof of the building collapsed, and more than 70 people were killed.

In response, the IRP came down hard. There were arrests, while executions and armed street confrontations became increasingly common.

According to Amnesty International, there were 2,946 deaths from executions, torture in prison and street clashes in the year after Bani-Sadr was removed from office.

Also at this time, contradictions in the founding ideology of the revolution were coming to the fore, both within the IRP and between left and right more generally.

The revolution itself had been an amalgam of impulses across the political spectrum that united, briefly, over a few issues. Both left and right were genuinely committed to improving living conditions for the lower classes, as well as education and health services.

But there was also a strong conservative strain of thought that resulted in Khomeini's restrictive rulings, such as limiting the status of women in society.

This internal conflict bubbled to the surface in debates over economics. For instance, a consequence of the revolution and the Iran–Iraq War was the greater control the state exerted over various economic sections. Factions on the left were in favor of this development, but the merchant-bazaari class were generally extremely skeptical.

Iranian politics and society was under extreme strain. And things were about to get even more tumultuous.

### 8. The Iranian constitution was altered once again in 1989. 

February 1989 marked the tenth anniversary of the 1979 revolution and Iran's leader, Khomeini, was seriously ill. His grip on the country had been strong, and it was clear that his passing would usher in a new phase of the Islamic Republic.

Khomeini had been struggling with cancer and heart disease for some time, but his sudden deterioration required that a successor be found at once. What's more, there were other serious matters to resolve. For instance, Iran had three leaders in positions of authority: a supreme leader, a president and a prime minister. it was an awfully crowded field and political factionalism only caused further gridlock.

Shortly before he died, Khomeini convened an assembly for revision of the constitution to ensure such difficulties could be resolved.

For example, the constitution as it stood demanded that the supreme leader be a _marja_ — a member of an extremely closed and respected clerical class that act as guides to the _ulema_ and other Muslims.

But Khomeini's chosen successor, Ali Khamenei, was not a _marja,_ and so the constitution would have to be changed.

On July 8, the assembly completed its revision of the constitution. The amendments they advocated were far reaching.

Primarily, the revisions abolished the office of prime minister, gave some of the president's powers to the supreme leader and reduced the power of the Majles.

On July 28, the ratification was put to the people in a referendum. Meanwhile, a new president was elected, Akbar Hashemi Rafsanjani, who had also been favored by Khomeini.

Constitutional revision had been a sensible decision on Khomeini's part. He realized that it would be impossible for anyone else to govern as he had. After all, his authority essentially stemmed from personal charisma and political skill.

Without him, there was a real danger that factionalism would instigate civil unrest. The point of the reforms, then, was to give his choices for leader and president strong legal authority to ensure the country's stability.

Iran had a new governmental structure. But, among ordinary Iranians, there was a ferment of discontent. Was the republic really working for them?

### 9. In the late 1990s, a new intellectual movement emerged to underpin political and social reform. 

Since the very foundation of the Islamic Republic, in 1979, conversations about political reform were common. But it wasn't until the late 1990s that one particular ideology became increasingly influential.

A new religious movement was on the rise. Its reformist arguments held that, in the existing system, there was room for religion and politics to complement each other better.

Abdolkarim Sorush, a teacher and writer, argued that religion should be considered alongside other forms of knowledge. In other words, Islamic law should not be seen as a separate sphere but should actually be part of nature and science.

At the same time, he also reasoned that religion and politics should be kept separate.

In particular, he noted that religion had been corrupted by politics. This idea resonated with many ordinary Iranians. They had long since become disillusioned with the religious authorities, whose position had been secured by the revolution. To them, it seemed that the revolution had rid the country of a corrupt monarchy, only to replace it with an equally corrupt monarch.

When it came to bad practice, the question of human rights loomed large.

For example, thinkers like the cleric and journalist Yusefi Eshkevari showed how the _ulema_ had interpreted the Koran to justify the subjugation of women, when, in fact, the Koran supported no such oppressive interpretations. Rather, the _ulema_ 's subjective reading served as a tool that could be used to entrench existing conservative structures.

Tellingly, many proponents of the reformist movement were persecuted by the government. But all this did was enrage activists, create publicity and, counterproductively, ensure the movement would continue to grow.

By 2003, reformist ideas had filtered into mainstream politics. Citizens utterly frustrated with the Iranian political system now felt they could dream of a progressive country.

But it wasn't to be. Faced with extreme government corruption, disillusioned citizens either gave up voting or actively boycotted elections. In 2003, turnout for the local council elections in Tehran flopped to an unprecedented low of just 12 percent. Conservatives won and appointed Mahmud Ahmadinejad as mayor of the city.

When reformists chose to repeat the same tactics in the 2005 presidential election, all it did was ensure the continued rapid rise of Ahmadinejad. An uninformed, confrontational hard-line nationalist was now at the country's helm.

### 10. The 2009 Iranian presidential elections produced a shocking and weird result. 

Ahmadinejad's election to president in 2005 came to be seen by many Iranians as a mistake.

Consequently, public support and enthusiasm coalesced around opposition candidate Mir Hosein Musavi. And it only grew as the 2009 presidential elections neared. But, in a shocking result, the incumbent Ahmadinejad was re-elected by a healthy margin on June 12, 2009.

Many people suspected that something was off, and when the election results were combed over, these suspicions seemed to be confirmed. Irrespective of where the districts were, whether in cities or out in the country, the distribution of votes between candidates was weirdly similar. There was no local variation at all, as one might expect.

Street protests unlike any that had occurred before in Iran's history swept the country. Three days after the election, an estimated million or more took to the streets demanding the removal of Ahmadinejad. The diversity and size of the crowds alone indicated the level of dissatisfaction.

What's more, the election had taken place in an already charged atmosphere: people were shocked at the harsh beating and arrest of women demonstrating for equal rights in March 2009.

The protests also signaled the start of the Green Movement, which was named after the green colors used during Musavi's campaign.

As might have been expected, the regime was less than impressed. It cracked down on protests rather than listening to demonstrators. When demonstrations continued on into the autumn, the regime retaliated. Eventually, reports of deaths and torture of people in police custody leaked out.

Then, on February 11, 2010, in anticipation of a massive protest on the anniversary of the 1979 revolution, the government sealed off areas designated for official celebration to everyone except its supporters.

The same tactics had proved effective in February 2011, but this time the regime went further. They felt threatened enough to put Musavi and another opposition candidate under house arrest. They remain there to this day.

At the time of writing, the regime and its leader were still in power. But their legitimacy had been damaged. Since 2009, swathes of reformists, politicians and journalists have left the country, preferring exile to living in a state that seemed to be on its way toward totalitarianism.

Iran remains a fascinating state, always in an uneasy balance between Islamic principles and democratic republican ideals. Conflict since the revolution has never ceased, but the aspiration to find a middle way lives on.

### 11. Final summary 

The key message in these blinks:

**If you want to understand Iran, you've got to keep in mind religion's role in its history. But it's not the only thing influencing things. Arguably, the upheavals and changes that the country has undergone leading up to the Islamic Republic and since were also a product of a deeper tension. The very notion of how a country should be governed has surfaced again and again in the history of Iran. It is a question that is still being worked out in the Islamic Republic today.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Necessary Revolution_** **by Peter Senge, Bryan Smith, Nina Kruschwitz, Joe Laur, Sara Schley**

_The Necessary Revolution_ (2008) sheds light on the environmental and social challenges faced by people living in today's world. Drawing on stories from real people and real communities, these blinks introduce the mentality we must adopt to fight for sustainability.
---

### Michael Axworthy

Michael Axworthy is the author of two other books dealing with Iranian history, _The Sword of Persia_ and _Iran: Empire of the Mind._ From 1998 to 2000, he was the head of the Iran Section of the British Foreign Office. He became a research fellow at the Institute for Middle Eastern and Islamic Studies at the University of Durham in December 2001, and he is now Senior Lecturer and Director of the Centre for Persian and Iranian Studies at the University of Exeter.

