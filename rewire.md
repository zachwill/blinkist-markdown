---
id: 54bd2b7f3465370009660000
slug: rewire-en
published_date: 2015-01-22T00:00:00.000+00:00
author: Richard O'Connor
title: Rewire
subtitle: Change Your Brain to Break Bad Habits, Overcome Addictions, Conquer Self-Destructive Behavior
main_color: FCF54D
text_color: 63611E
---

# Rewire

_Change Your Brain to Break Bad Habits, Overcome Addictions, Conquer Self-Destructive Behavior_

**Richard O'Connor**

Rewire is about why we sometimes fall into self-destructive behavior, and how to move past it.

It delves into the brain activity behind addictions, and outlines strategies for rewiring yourself for improved self-control over your bad habits.

---
### 1. What’s in it for me? Learn how to steer clear of destructive behaviors by improving your brain functioning. 

We've all been there: you do something almost unconsciously, and then you realize how dumb it was and you wish you could take it back. Sometimes it's something harmless, like a spelling error, but sometimes it has grave consequences, like smoking that one cigarette for old time's sake. It can lead to a chain of destructive behaviors: not just more cigarettes, but also anger, self-hate, and guilt for falling off the wagon.

The truth is that these kinds of mistakes are common in all of us. Actually, it's part of what makes us human.

In _Rewire_, Richard O'Connor explains why we make these mistakes, and how our brains are wired so that we don't only make them; we make them often! Luckily, these blinks show how it's possible to _re_ wire our brains, to reverse our habits if we choose to be happier and healthier.

In these blinks, you'll learn

  * why we do things we regret;

  * why we tend to blame our faults on "just being human;" and

  * one way to overcome addiction.

### 2. We have two “selves” that dictate our actions – and we can learn to control them. 

We've all been through this: you're faced with two choices, you know which one is right, yet you choose the wrong one. Why do we do that?

It's because we all really have two _selves_ — a _conscious_ self and an _automatic_ self. They both influence our decisions.

The automatic self acts without our direct control. It's what's influencing you when you mindlessly eat a bag of chips while watching TV.

The conscious self uses rational thought and reason. When you decide to try octopus for the first time, your conscious self is in charge.

Usually, when you do something you regret, it's because your automatic self is in control, and your conscious self isn't considering the consequences. You eat those chips without thinking about it.

So if you want to overcome any bad behavior, you have to train your automatic self to stop slipping. Strengthening your _conscious_ self to be more dominant is also useful, but training your _automatic_ self is more effective.

Brains can physically change. You can direct the ways your brain develops, and how it affects your behavior.

Our brains are constantly building new cells, and new networks between them. In fact, it's recently been discovered that learning actually causes the growth of new cells; our behavior affects brain cell growth, thereby brain function.

When you do any action repeatedly, your nerve cells grow more and more connections with each other. For example, "go to the gym" ( _nerve A_ ) will connect with "stay at the gym until my workout is done" ( _nerve B_ ).

As you work out more and more, it'll become a habit. Nerves A and B will increasingly bond together.

So when we develop good habits, we replace bad habits. You need to change your automatic habits from negative to positive ones.

> _"Each time we engage in a bad habit, we make it more likely we'll do it again in the future."_

### 3. Certain habits can lead you into self-destructive behavior. 

Unfortunately, it's not easy to _unlearn_ the bad habits in our automatic system.

Our automatic self has many habits — some that benefit us, and some that hurt us. Habits form when repeated patterns of behavior become normal, and form new pathways in the brain.

We learn our good and bad habits subconsciously. A bad habit might be watching TV and eating chips after work, instead of going to the gym. A good habit could be brushing your teeth.

Some habits, like smoking, are clearly harmful. Other times, however, habits that we _think_ are good are actually destructive. S _elf-serving biases_ lead to deceptively destructive habits.

Self-serving biases affect the way we see ourselves and the world. We claim that we're responsible for our good characteristics, but blame our faults on the outside world.

For example, imagine a man who tactlessly checks out every woman he meets. He might justify his behavior by saying, "I'm just a man with human desires." That reasoning makes it seem like it's not his fault. But if he performs well at work, he'll think that's because of his own dedication and perseverance, not because he is fortunate enough to be in a supportive work environment in a growing industry.

Interestingly, most happy and confident people benefit from the self-serving bias of believing they _earned_ their happiness and confidence. This can be detrimental, though, if it distorts reality.

If someone becomes overconfident in her abilities, she may stop trying hard. We tend to ignore anything that goes against our self-serving biases. Our beliefs keep reinforcing themselves.

If you hear that a person is unintelligent before you meet him, you're more likely to interpret him that way when you actually meet. The opposite may be true if the person is introduced as a professor.

These sorts of biases operate unconsciously, so they don't usually get corrected by the conscious self. Instead, we fall into making the same mistakes without having chosen to do so.

### 4. Repressed emotions cause us to become self-destructive. 

You know how a boiling tea kettle needs to let out steam? Emotions work the same way. They're also chemical reactions, they build up, and they have breaking points.

Negative emotions like fear, anger and guilt can push us into self-destructive behavior. When we try to repress those feelings, they just come out another way.

Emotions are simply reactions to things, like how pulling your hand away is a reaction to touching a hot stove. So emotions can't be "wrong," since they're not meant to be based on reason. However, sometimes we think they're wrong, which causes us increased guilt, anger or frustration.

Thinking your emotions are wrong can result in self-destructive behavior. Self-destructive behavior is really a result of our two minds not communicating. Our conscious and automatic selves sometimes give us conflicting advice, which can cause us to hurt ourselves.

Sometimes the conscious self knows an action is wrong, but the automatic self does it anyway. This isn't always necessarily bad. Anger, for example, can be a good thing when you're protecting yourself or your loved ones. It can instill you with bravery.

If you have too much repressed anger, however, you may underestimate risks or overlook dangers. This can lead you to make rash decisions. If you're angry at work, for instance, you might quickly write a rash email. However, if someone's breaking into your house, anger may give you the courage to make the rash decision of getting a bat!

Stifled anger will build up. There's a good chance you'll eventually take it out on someone you love, by hurting or deserting them. Doing so can make you feel guilty, which can, in turn, make you hate yourself. And hate is another dangerous repressed emotion.

> _"If there is one single common element to self-destructive behavior, it's fear."_

### 5. Self-destructive behavior can also result from a need for attention, general unhappiness, or a loss of motivation to fix problems. 

There's another reason we're sometimes self-destructive: we're really crying for help.

Sometimes the unconscious self starts causing us trouble, by seeking attention in hopes that someone will come to the rescue.

People often struggle to ask for help, because they fear being rejected. Instead, they ask in subtler ways.

Richard O'Connor, the author of this book, once encountered a 16-year-old boy who was having problems with marijuana. He dropped his stash in front of his mother twice, and when O'Connor pressed him about it, the boy broke down crying and said he just wanted a caring mother. He'd wanted his mother to see it and reprimand him.

As we've seen, self-destructive behavior is often tied to strong emotions. This isn't always the case, however. Sometimes people simply don't act against their self-destructive tendencies.

We can see this in people who are truly _defeated_. There are two kinds of defeated people:

First, there are those who've never had the motivation to fix their harmful behavior, because they've never thought to put forth the effort. They assume that being miserable is just part of life, and don't try to change it.

Then there are those who've tried _too often_ to fix themselves. They've disappointed themselves or others so many times that they get burnt out on trying. They also lack the motivation necessary for self-improvement, but for different reasons.

If you're in the second category of defeated people, try setting more realistic expectations for yourself. Don't think you'll quit smoking right away — you're only setting yourself up for disappointment if you fail. Instead, set more manageable goals, like smoking less.

### 6. Addictions and undertow are two other forms of self-destructive behavior. 

How many of you have thought, "Yes, I've finally quit smoking!" only to find yourself smoking again two weeks later? Addiction and _undertow_, another kind of self-destructive behavior, often function in this pattern.

Undertow blocks your recovery when you're getting close to success. It results from bad habits that are etched into our brains. One simple mistake can make us slip back into the pattern our brains expect.

Imagine an alcoholic who's been sober for a year. He accidentally takes the wrong glass at a party, has a sip, and then goes on a drinking binge.

This person will then feel intensely guilty, and may even hate himself. He could lock himself up at home, neglect his work and go on being self-destructive.

One way to steer clear of that pattern is to associate your relapse in judgement with something negative like fear or disgust. When that former alcoholic accidentally sipped the wine, he could've instinctively spit it out. We have to rewire the patterns in our brain to avoid getting swept up by the undertow.

Addictions are inherently self-destructive, because it means that something has taken away our self-control. It could be a substance like a drug, or a habit like gambling.

When you get something you want, your brain releases dopamine, which makes you want it more. We can see this hormonal response in lab animals: when they're trained to give themselves small shocks in order to avoid getting a larger shock, they'll keep giving themselves small shocks even when the threat of the bigger one is gone.

This is why Alcoholics Anonymous's 12-step program for dealing with addictions is so effective. The first thing you have to do is admit that you're powerless. Then you can look at yourself like an outsider, assess the problem and start to tackle it.

> _"Neurons that fire together wire together."_

### 7. We can break bad habits by practicing mindfulness and self-control. 

So how can we completely steer clear of bad habits?

The first step is to practice _mindfulness_. Mindfulness means looking at yourself in a calm, objective and compassionate way. It means stepping back from your habits, so you don't act on them immediately.

You can work on mindfulness by meditating. _Mindful meditation_ is about aiming for mindfulness, not enlightenment or zen. It's a request of yourself, for you to listen to your feelings, without judging yourself.

Find a quiet, comfortable place where you can meditate every day at the same time, for about 30 minutes. Close your eyes, focus on your breathing, and let other thoughts come and go. Don't worry about doing it exactly right.

If meditation is hard for you, try monitoring your thoughts and feelings by keeping a journal.

Ultimately, your goal is to increase your self-control and strengthen your willpower.

If you are continuously dedicated, self-control will become part of your automatic self, not just your conscious self. You'll rewire your brain.

This is what the AA saying, "Fake it till you make it," is referring to. Breaking an addiction is difficult at first, but gradually it'll take less effort to practice self-control, as it becomes more automatic.

"Fake it till you make it" applies to other things as well. Even pretending to be kind will make you kinder. You'll feel good about yourself, and keep doing kind things until it becomes a habit.

There are also ways to increase your will power. Eat healthier so your brain will function better. Always keep parenting yourself, and lightly punish yourself if necessary. If you can associate a negative stimulus with your temptation, you'll start to feel it less and less.

Once you've gained more mindfulness and self-control, it will also help you in the long run to start building stronger relationships with positive people. Being surrounded by other healthy people — with healthy minds — means your self control can give way to automatic positive habits.

> _"Get to know yourself with compassionate curiosity."_

### 8. Final summary 

The key message in this book:

**You succumb to your bad habits when your automatic self repeats behavior without your conscious self choosing what's better for you. But by practicing mindfulness and staying dedicated, you can rewire your brain so your bad habits disintegrate. Keep meditating, watching yourself and "faking it" until you make it.**

Actionable advice:

**Know yourself.**

The first step in destroying your bad habits is to look at yourself like an outsider, and acknowledge self-destructive behavior. Meditate, and tune into your thoughts and feelings; the more you understand them, the easier your journey will be.

**Suggested further reading: The End of Stress by Don Joseph Goewey**

_The End of Stress_ offers a unique look into the severe damage caused by stress on both your health and happiness, and offers simple tips and tricks that you can start using today to undo the damage. Ultimately, it reveals how adopting a peaceful mindset will set you on the path to increased productivity, creativity and intelligence.
---

### Richard O'Connor

Richard O'Connor is a psychotherapist. He's also the former executive director of the Northwest Center for Family Service and Mental Health in Connecticut, USA.

