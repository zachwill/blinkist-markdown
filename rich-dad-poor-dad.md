---
id: 528232a7333464000c300000
slug: rich-dad-poor-dad-en
published_date: 2013-11-13T14:28:58.000+00:00
author: Robert T. Kiyosaki
title: Rich Dad, Poor Dad
subtitle: What the Rich Teach Their Kids about Money – That the Poor and the Middle Class Do Not!
main_color: 6F356F
text_color: 6F356F
---

# Rich Dad, Poor Dad

_What the Rich Teach Their Kids about Money – That the Poor and the Middle Class Do Not!_

**Robert T. Kiyosaki**

_Rich Dad, Poor Dad_ (1997) __ combines autobiography with personal advice to outline the steps to becoming financially independent and wealthy. The author argues that what he teaches in this _New York Times_ bestseller are things we're never taught in society, and that what the upper-class passes on to its children is the necessary knowledge for getting (and staying) rich. He cites his highly successful career as an investor and his retirement at the early age of 47 as evidence in support of his claims.

---
### 1. Fear of society’s disapproval prevents us from leaving the “rat race” and growing wealthy. 

Most of us know what the phrase _rat race_ refers to, but if asked, how would we define it?

One definition is "The endless routine of working for everyone but yourself." This means you do all the work, while others — the government, bill collectors and your bosses — take the majority of the reward.

We usually talk about the _rat race_ as something we're all a part of. At the same time, we also talk about it as something we hate. So why do we keep racing?

Because most people's lives are dominated by their fear of society's disapproval..

For example, consider the mantra "Go to school, study hard, get a good job."

We still teach this mantra, even though it's outdated advice founded on the ideas of our parent's past. Back then, you were likely to land a job right out of college, work for the same company for decades, and retire with a cushy pension. Today, this is no longer a guaranteed recipe for a life free of financial struggles or poverty.

The truth is that you can study hard, get into a good school and graduate into a high-paying job without ever seeing financial growth, because you're still stuck in the "rat race." Your bosses — not you — are getting rich from all _your_ hard work.

Nevertheless, we still believe in and follow the above mantra out of fear of violating the expectations that have been drilled into us since birth. The result? We may be avoiding poverty, but we're certainly not growing any wealthier.

**Fear of society's disapproval prevents us from leaving the "rat race" and growing wealthy.**

### 2. Fear and greed can drive financially ignorant people to make irrational decisions. 

When it comes to money, everyone — wealthy or not — experiences two basic emotions: _greed_ and _fear_. If you have money, you are likely to focus on all the new things it can buy (greed). If you don't have it, you worry you might never have enough (fear).

People who are ignorant about how to manage their finances are especially prone to letting these emotions drive their decision-making.

For example, let's say you just received a promotion and a hefty pay raise.

You could invest the extra money into something like stocks or bonds, which would earn you money over time, or you could gratify yourself with new purchases, like a car or house.

If you're a financially ignorant person, this is where emotion takes the wheel.

The fear of losing money is so powerful it prevents you from investing in stocks or other assets because of the perceived risks, even though such investments would bring you wealth in the long-term.

At the same time, greed inspires you to spend your increased salary on a better lifestyle, for example by buying a bigger house, which seems a much more real and safer option than buying shares in a company.

However, this upgrade also means a bigger mortgage and higher utility bills, which effectively negates your raise.

This is how fear and greed hinder the financially ignorant from becoming wealthy in the long term.

So how can you counter these powerful emotions?

By building up your financial knowledge about things like investments, risk and debt. This will place you in a better position to make rational decisions — even in the face of greed and fear.

**Fear and greed can drive financially ignorant people to make irrational decisions.**

### 3. Despite being vital for both personal and societal prosperity, we receive no training in financial intelligence. 

Most people think that to become rich, it's enough to be talented and capable. But in fact, the world is full of such people, and most of them are poor. What they are missing is _financial intelligence_, a comprehensive aptitude for financial subjects like accounting, investing and so forth.

Unfortunately, we're raised without this intelligence. Our school systems are set up to train people in a variety of useful subjects, but financial intelligence is not one of them.

Children aren't taught about subjects like saving or investing, and as a consequence are clueless about topics like compound interest — as clearly evidenced by the fact that, today, even high schoolers often max out their credit cards.

This lack of training in financial intelligence is a problem not only for today's youth but also for highly educated adults, many of whom make poor decisions with their money.

For example, politicians are generally regarded as the brightest, most well-educated people in a society, but there's a reason why countries end up in staggering national debt: most of the governing politicians have little or no financial intelligence.

Ordinary people, too, can be astonishingly bad at handling their money matters, as evidenced by their lack of retirement planning. For instance, in the United States 50 percent of the workforce are without pensions and, of the rest, nearly 75 to 80 percent have ineffective pensions.

Clearly, society has left us poorly equipped in terms of financial knowledge, and so it is up to the individual to educate him- or herself.

When we find ourselves seeking wealth in times of great economical change, it becomes even more necessary to independently pursue a good financial education.

**Despite being vital for both personal and societal prosperity, we receive no training in financial intelligence.**

### 4. Financial self-education and a realistic appraisal of your finances are the building blocks of growing wealthy. 

You can start the journey toward personal wealth at any point in your life, but the earlier you get going the better — if you begin at 20, you're far more likely to become rich than if you begin at 30.

Regardless of age, the best way to get started is by appraising your finances, setting yourself goals, and then acquiring the education necessary to reach them.

First, take an honest look at your current financial state. With your current job, what kind of income can you realistically expect now and in the future, and what kind of expenses can you sustainably handle? You may find, for example, that the new Mercedes you've been drooling over simply isn't affordable.

After this, you'll be able to set realistic financial goals. You could say, for example, that you want that Mercedes to be within your reach in five years' time.

The next step is to then start building your financial intelligence. Consider this an investment into the greatest asset available to you: your mind.

You can do this in any number of ways, but one good approach is to shift focus: work for what you learn, not what you earn.

For example, if you're afraid of rejection, try a short spell working for a network marketing company. While you might not get an amazing salary, you'll gain a lot of sales skills and self-confidence, which will be very useful in the future.

You can also improve your finance education in your spare time. Enroll in finance classes and seminars, read books on the topic, and try to network with experts.

If you base your financial foundation on these building blocks, there's a good chance you'll become wealthy one day.

**Financial self-education and a realistic appraisal of your finances are the building blocks of growing wealthy.**

### 5. To become wealthy, you must learn to take risks. 

Insanity is defined as doing the same thing over and over again and expecting different results. By this logic, if you're looking to change your current financial state, you'll need to start handling your finances differently.

The biggest change you most likely need to make is learning to take risks. All financially successful people have taken risks to get where they are, and they are successful because they manage rather than fear these risks.

Taking risks means not always being balanced and safe with your money, which is what you're doing when you put it in basic checking and savings accounts at the bank.

Instead of playing it safe, try investing your money in stocks or bonds. While these are considered more risky than typical bank accounts, they have the chance of generating much, much more wealth — sometimes (as with stocks) in a very short period of time.

Or, if you don't want to commit yourself to the stock market, there are a variety of other investments that will help grow your wealth in the long run, like real estate or so-called _tax lien certificates_. With tax lien certificates, interest rates range between 8 percent and 30 percent.

Of course, the higher the potential for return, the higher the risk. With stocks, for example, there's always the slight chance you could lose your entire investment. But if you don't take the risk in the first place, you're guaranteed not to make any big returns.

So you see that taking those bigger chances and handling the bigger risks they present is necessary in order to start making a bigger income.

**To become wealthy, you must learn to take risks.**

### 6. The road to wealth is long, so you must keep yourself motivated. 

The journey to wealth is long and trying. It's easy to lose heart when you hit a hurdle such as seeing the price of a stock you invested in suddenly tumble. In order to achieve your financial goals, you'll need to find ways to stay motivated even in the face of setbacks.

One method to boost motivation is to create a list of "wants" and "don't wants" for your personal reference.

For example: "I do not want to end up like my parents" and "I want to be free of my debts within three years."

Pull out these lists any time you need a reminder of why you must persevere on your journey to wealth.

Another good way to stay motivated is to spend money on _yourself_ before paying your bills.

Though somewhat counterintuitive, this way you'll see exactly how much extra money you need each month to satisfy both of your objectives: fulfilling desires like buying that vintage guitar you've had your eye on, and meeting your bill collectors' demands.

This doesn't mean you should rack up lots of credit card debt, but do keep "paying" yourself first; the extra pressure of paying off your bills afterward will inspire you to find creative ways to make enough money to satisfy both.

This method will also sharpen and develop your financial self-discipline, which is a key trait of all financially successful people.

For outside inspiration, research the life stories of wealthy people like Warren Buffett or Donald Trump. Reading about how they overcame struggles to achieve triumphs will help keep you ambitious.

Put these tips into practice and you'll be sure to find that staying motivated on the road to wealth isn't that difficult.

**The road to wealth is long, so you must keep yourself motivated.**

### 7. Laziness and arrogance can drive even financially knowledgeable people to poverty. 

Even after strengthening your financial intelligence, personality pitfalls may still threaten you and your money.

Laziness and arrogance are two such pitfalls, because they can work against you in less-than-obvious ways.

We often think of laziness as slouching around and doing nothing, but in fact laziness does not necessarily mean inactivity; it can also be avoiding things that should be done.

For example, imagine a businessman who works over 60 hours a week. To the outside observer, he is not lazy at all. However, by working such late nights, he has alienated his family. He has already seen the signs of trouble at home, but, rather than addressing them, he buries himself in work. In short, he is being lazy: he is avoiding what he should be doing, and will likely suffer the consequences in the form of a costly divorce.

Similarly, arrogance can be a devastating weakness _._ Contrary to the usual definition, in the case of financial ruin it can be defined as "ignorance plus ego"; a combination of poor financial knowledge and an ego too proud to admit it.

Arrogance is a particularly dangerous flaw when you make investments. For example, some stock-brokers will try to feed the arrogant side of you to sell you more shares and maximize their own commission. They're like dishonest used-car salesmen; they boost your ego with the positives of an investment while keeping you ignorant about its negatives.

So even if you become a financial genius, keep these personality pitfalls in check. This way, you are much more likely to avoid financial ruin.

**Laziness and arrogance can drive even financially knowledgeable people to poverty.**

### 8. Only invest in assets, which put money in your pocket; and avoid liabilities, which take money out. 

Knowing the difference between an _asset_ and a _liability_ is necessary to ensure you're making strong investment decisions.

Quite simply, an asset is something that makes you money, while a liability costs you money.

Clearly, then, it's more likely you'll become wealthy if you mostly invest in assets.

Assets include businesses, stocks, bonds, mutual funds, income-generating real estate, IOU notes, royalties from intellectual property, and anything else with value that produces income, appreciates over time, and can be sold readily.

When you invest in assets, your dollars become employees working to create income for you. The more "employees" you commit, the better. The goal is to get your income as high above your expenses as possible, and then to reinvest the excess income into your assets, employing even more dollars to work for you.

Unfortunately, many investors continually mistake certain liabilities for assets. 

For example, a house is often considered an asset, but it's actually one of the biggest liabilities you can have. Buying a house often means working your entire life to pay off a 30-year mortgage and property taxes.

This works against you in two ways: First, you're guaranteed to have a massive expense taken away from your income every month (a tell-tale sign of a liability) for the next 360 months. Second, those 360 payments could have been invested in potentially more lucrative assets, like stocks or real estate you rent to tenants.

Ensuring that you know the difference between an asset and a liability means you'll be able to soundly judge what to invest your money in and what to avoid.

**Only invest in assets, which put money in your pocket; and avoid liabilities, which take money out.**

### 9. Your profession pays the bills, but your business is what will make you wealthy. 

Most people consider their profession and their business to be one and the same thing. When it comes to personal finances, though, there's a difference:

Your _profession_ is whatever you do 40 hours a week to pay the bills, buy groceries, and cover other living costs. Usually, it gives you a specific title such as "restaurant owner "or "salesman."

Your _business_, on the other hand, is what you invest time and money in to help grow your assets.

Because a profession only covers your expenses, it's unlikely that this alone will make you wealthy. To achieve wealth, you must build a business _while_ working at your profession.

Take, for example, a chef who's gone to culinary arts school and knows all the tricks of the trade. Although her profession — cooking — provides enough money to pay rent and feed her family, she's still not growing wealthy.

So she invests in a business: real estate. Whatever extra money she has each month, she puts towards buying income-producing assets — apartments and condos she can rent to tenants.

Alternatively, consider a car salesman who invests each month's leftover income into stock trading.

In both cases, the professions provided enough income to survive on a monthly basis. However, by putting their extra income into their businesses, these people are also growing their assets and making strides toward wealth.

Your profession often funds your business initially; therefore, it's wise to keep your day job until your business starts to show sustainable growth.

When that starts to happen, your assets — and not your profession — become your main source of income.

And that, indeed, is the sign of true financial independence.

**Your _profession_ pays the bills, but your _business_ is what will make you wealthy.**

### 10. Understand the tax code to help you minimize your taxes. 

Everyone knows that taxes detract from personal wealth, but most people don't bother to find out how they can minimize the taxes they pay. There are many ways this can be legally achieved.

One way to reduce taxation is to invest your money through the coverage of a _corporation_. If you invest through your own corporation, the money you make is taxed much more leniently than if you invest in your own name.

In the United States, corporations come with other benefits, too. For example, debts and liabilities are placed in the corporation's, not the owner's, name, which insures against limited losses on investments gone awry.

When you're an employee, you earn, get taxed, and then try to live on what's left. When you're protected by a corporation, you earn, invest or spend as much as you can, and then get taxed on what's left.

It's no surprise, then, that corporations can help people get rich very quickly.

There are other ways you can minimize your taxes, too; it's just a matter of educating yourself on the many loopholes and benefits of the tax system.

For example, because of Section 1031 of the Internal Revenue Code of the United States tax system, if you sell your current real estate assets in order to buy more expensive ones, the government delays taxing your new real estate until you liquidate the property.

This means your capital gain increases, while the government refrains from taking anything from you until later.

By becoming aware of how the "system" works in your country, you may be able to legally reduce how much money the government takes from you.

**Understand the tax code to help you minimize your taxes.**

### 11. Final summary 

The key message in this book is:

**Because we're not trained in financial intelligence in school, it's up to us as individuals to develop this trait by ourselves. We are only likely to become rich or financially independent once we have both a strong financial IQ and a firm, ambitious mindset to support it. In the end, what you invest in your mind is what brings you success, because your mind is your most important asset in any financial situation.**

**Actionable advice:**

**If you want to see results, start right now.**

Although this book lays out the paths to financial independence and wealth, these ambitions can only be fully realized if you start moving toward them now.

This means researching to find the best books on your area of interest (e.g. real estate, or the stock market). Where can you pick them up locally? Which ones are best for beginners? Also, try to discover the "who's who" of the markets you want to join. Do they have websites you can follow? When is their next publication coming out? Other general websites, like Investopedia.com, have great information for beginners who aren't sure where they should start putting their money. In any case, staying actively informed will help keep you afloat and give you a better understanding of your markets.

**Make a column sheet that tracks your monthly expenses and income, as well as your current assets and liabilities.**

One of Kiyosaki's main points throughout the book is to ensure you have an income greater than your expenses.

The only way to do this is to keep an eye on your money. Use a program like Microsoft Excel to create a worksheet you can update on a monthly basis. Chart your income, which includes any money coming your way each month, and compare it with your expenses, which include bills, rent, lifestyle expenses and taxes taken out of your paycheck, as well as any other costs. Also, start keeping track of how much your current assets are generating for you each month as well as how much your liabilities are taking away. This will help you gauge what you can afford to cut out from your life in order to start widening the gap (in a good way) between your income and expenses.

**Introduce yourself to people who do what you want to do.**

By networking with people who are already active in the markets you're interested in, you can form valuable connections that will benefit you in the long run.

For example, find someone in a local tax office who knows a lot about tax lien certificates. Offer to take them out to lunch — your treat. Make sure, though, that they understand you want to learn from their experience and knowledge, and that you aren't just asking for help to get rich. If you're honest about your intentions and willing to listen, chances are most experts will be glad to give you a few pointers.

**Suggested further reading: _The Millionaire Fastlane_ by MJ DeMarco**

The world is full of people working hard with dreams of getting rich — but only a handful succeed. As _The Millionaire Fastlane_ shows, there's a simple reason for this; conventional roads to wealth don't work. But thankfully, there is one way that works, and if you're eager and willing to put in some effort and make some sacrifices, it's far quicker than you might think.
---

### Robert T. Kiyosaki

_Robert Kiyosaki_ is an investor and entrepreneur with an estimated net worth of over $80 million. His _Rich Dad_ brand has published more than 15 financial self-help books, which have sold over 26 million copies worldwide.

