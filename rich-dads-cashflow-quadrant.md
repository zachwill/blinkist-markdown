---
id: 5e79efc16cee0700068c94e2
slug: rich-dads-cashflow-quadrant-en
published_date: 2020-03-27T00:00:00.000+00:00
author: Robert T. Kiyosaki with Sharon L. Lechter
title: Rich Dad's Cashflow Quadrant
subtitle: Guide to Financial Freedom
main_color: 6A2C6F
text_color: 6A2C6F
---

# Rich Dad's Cashflow Quadrant

_Guide to Financial Freedom_

**Robert T. Kiyosaki with Sharon L. Lechter**

_Rich Dad's Cashflow Quadrant_ (1998) is a guide to financial freedom. In the second book of the Rich Dad Poor Dad series, authors Robert T. Kiyosaki and Sharon L. Lechter describe how some people achieve financial success without working as hard as the rest of us. In this blend of instruction and autobiography, they explain how you might have the wrong idea about attaining financial freedom and set out ways in which you can turn that around.

---
### 1. What’s in it for me? Learn how to take your first steps towards financial freedom. 

Have you ever dreamed of handing in your notice, getting out of the rat race, and living a life of plenty? You're hardly alone. But for many of us, those dreams will remain just that: dreams.

Not for author Robert T. Kiyosaki. He took those fantasies and turned them into the affluence and freedom he enjoys today. His own father had been an overworked government official who fell into debt and financial ruin. After Kiyosaki watched this happen to his father, he vowed he'd never let it happen in his own life. And so, he learned to create wealth wherever he was.

In these blinks, you'll learn the core values that Kiyosaki has applied to his own life, through decades of successful investing and business creation. Rather than specific techniques, you'll come to understand the broad principles that've guided him throughout his success. With any luck, you'll pick them up yourself.

In these blinks, you'll learn

  * about the four distinct categories of cash-flow;

  * what the Information Age means for our jobs and pensions; and

  * how fear affects the way we look at money.

### 2. The different ways in which we earn money can be divided into four quadrants. 

Let's imagine you drew a simple 'plus' sign on a piece of paper. What do you see? Two lines — one horizontal and one vertical, right? These lines divide four white spaces. And those four spaces are called quadrants.

**The key message here is: The different ways in which we earn money can be divided into four quadrants.**

So what are these four quadrants? They're each labeled with a letter: E, S, B and I. On the left-hand side of the plus sign are the _E_ and _S_ quadrants. The E stands for "employee" **** and the S for "small business or self-employed." And on the right-hand side, we have the _B_ and _I_ quadrants. The B stands for "big business owner" and the I for "investor."

Now, depending on how you make your living, you belong to one of these four quadrants.

Over your lifetime, you might earn money from just one, a couple, or all of these quadrants. Take the example of a medical doctor, working in the United States today. She could decide to earn a living as an _E_ (an employee). She can do that by joining the staff of a large hospital or insurance company, working for the government in public health, or becoming a military doctor. In other words, by taking a nine-to-five job.

That same doctor could also choose to be an _S_ (self-employed) and start a private practice. She would set up an office, hire staff, and build a private list of patients. It's still hard work, but she'd have more control.

As a third option, this doctor could decide to become a _B_ (big business owner). She could own her own clinic and hire other doctors. In that case, she'd probably employ someone else — a business manager — to run the organization. So, she'd own the clinic but not have to work in it herself. On the other hand, she could continue as a working medical professional _and_ own a business that's _un_ related to medicine.

As a high-earning doctor, she'd probably have investable income, too. So, while practicing medicine, running her clinic, or overseeing her business, she could also become an _I_ (investor). She could do that by investing in shares or property.

So far, so simple, right? This is the basic structure of our society. All of these quadrants require different capabilities and personal characteristics. Some people find their place in life and are happy in whichever quadrant they end up in — there is no "right" or "wrong" choice.

But what if you want to be financially free _?_ What if you want to escape the world of work and drudgery? Then you'll need to move from the _E_ and _S_ quadrants into the _B_ and _I_ quadrants. In short, from _working_ to _owning_. We'll see why in the next blink.

### 3. Studying and working hard won’t get you to financial freedom. 

When Robert Kiyosaki was growing up in Hawaii, he had two father figures. One was his biological dad, who worked for the government. The other was his friend Mike's dad, a self-made businessman and investor.

He came to know them, respectively, as his poor dad and his rich dad. As he observed their lives, he began to learn fundamental lessons about work and money.

**The key message here is: Studying and working hard won't get you to financial freedom.**

On the surface, Kiyosaki's biological father was successful — he was well-educated and well-respected. He'd been an excellent student and had worked his way up to Head of Education in the Hawaiian government. However, as a government employee, he was overwhelmed with work — his calendar was crowded with appointments, and he was always traveling. As a result, he had very little time to spend with his family, or to enjoy his other passion, reading books.

And despite his prestigious government role, he also had little money. As clever and worldly a man as he was, he still believed the central myth of our time: that working hard without investing can give you financial security. This was because, despite his fine scholarly education, he'd had a poor _financial_ education. In fact, he didn't think there was such a thing as a financial __ education and openly mocked anyone who took classes on investing or real estate.

On the other hand, Mike's dad, the businessman and investor, always seemed to have both time and money. In fact, he seemed to have more time for the author than his biological dad did.

Mike's dad was everything that the author's "poor dad" despised: he didn't have a formal education, but he knew a lot about things like "dividends" and "land valuation." He'd decided long ago that, rather than spending all of his time in the _E_ and _S_ quadrants, he would try to escape the toil of the nine-to-five.

So what did he do? Early in life, and bit by bit, he bought property. Eventually, he'd built up a hotel empire. So, while he received the scorn of educated people, he also began to receive a passive income from all of his hotels. Maybe others would mock him. But now he had all the time in the world. He could even read all of the books his critics would love to read, _if only they had the time._

> _"My educated dad was a voracious reader of books, so he was word-smart, but he was not financially literate."_

### 4. Working hard and working smart are two different things. 

One day, while the author was at Mike's house, his "rich dad" sat him down and told him a story. In many ways, it was the beginning of his financial education.

Kiyosaki didn't know it then, but this story would explain the essential difference between those in the _E_ and _S_ quadrants, and those in the _B_ and _I_ quadrants.

**The key message here is: Working hard and working smart are two different things.**

The story went like this: Once upon a time, there was a quaint little village. **** The only problem was, it didn't have a water supply. To remedy this, the village elders decided to contract two men, Ed and Bill.

The first contractor, Ed, set to work immediately. He decided to carry two steel buckets to a nearby lake, fill them up, and carry them back himself. It took him hours to fill up the village's water tank on his own. By the end of the day, he was completely beat. But at least he was making money from his labor.

The other contractor, Bill, vanished for a while, which pleased Ed because now he had no competition. But Bill didn't stay idle. Instead of buying two steel buckets to carry the water, Bill had written a business plan, created a corporation, found investors, employed a president, and gathered a construction crew.

Within a year, Bill's team had built a stainless steel pipeline which connected the village to the lake. Then, in no time at all, Bill expanded his pipeline to other villages. The water he provided was cleaner, cheaper, and more readily available than Ed's. Before long, he was making money from a whole system that he'd created. Soon, he no longer had to work at all. Poor Ed, on the other hand, was working himself into an early grave just to break even.

In the end, Bill went on to sell his pipeline business and retired into great wealth. Ed, however, could only look on sadly **** as his children decided not to take over his water-bucket venture and moved away to the city.

The simple story of Ed and Bill is the fundamental difference between the left and right-hand quadrants: working _hard_ **** and working _smart_.

### 5. In today’s economy, we can’t rely on the government to look after us. We need to take our financial security into our own hands. 

Today, we live in what is roughly defined as the "Information Age." This age began around 1991. It marked the end of the "Industrial Age," as new technologies allowed companies to move their money around the world at lightning speed.

It was also the beginning of the end **** for secure, unionized jobs and good government pensions. But many of us still haven't adapted to the times.

**The key message here is: In today's economy, we can't rely on the government to look after us. We need to take our financial security into our own hands.**

In fact, many of us are still inclined to think in the same way that our grandparents did. That is, if we work hard and pay our taxes, we expect the government to be responsible for us later in life. Sadly, this kind of arrangement is impractical today.

For instance, it's estimated that in 2020 there will be more than 100 million Americans expecting some kind of government support. These will include federal employees, military veterans, teachers, other government employees, and retirees anticipating Social Security and Medicare payments. And they're right to expect support: **** they paid into the system and were promised something in return.

However, it looks unlikely that these promises will be fully kept. The cost is simply too high. And if the government raises taxes to pay for these promises, the ultra-rich will just escape to countries with lower taxation.

So the old wisdom — that you work hard and then the government looks after you — is wrong. So, what should you do? To achieve financial security throughout your life, you'd better move into one of the right-hand quadrants, _B_ or _I: Big business ownership_ or _Investment_.

Once again, the experience of his two father figures taught Kiyosaki a vital lesson here. His biological father worked hard as a government official and expected security in later life. But then a political disagreement cost him his job, and he was blacklisted from the Hawaiian government. He was floored. Without experience in the _B_ and _I_ quadrants, he tried a few business ventures, but they failed, and he sunk deep into debt. Bitterly, he realized there was nothing to catch him as he fell.

On the other hand, Kiyosaki's "rich dad" had created a system of passive income, a continuous pipeline of cash, that flowed into his bank account. He'd entered the _I_ quadrant early in life, and it shielded him through the rest of his days.

### 6. The four quadrants attract different types of people. 

From listening to his "rich dad", Kiyosaki learned about the core differences between people in the different quadrants. He realized that their personality types were fundamentally different in their relation to work and money.

**The key message here is: The four quadrants attract different types of people.**

Firstly, let's look at the E quadrant. These are the employees of organizations. Someone who naturally gravitates towards this quadrant will often use words like "security" and "benefits." They need to feel certainty, **** in the form of a contract, a regular paycheck, and employment benefits. And they're often driven by _fear –_ fear of risk and of financial destitution. Employees can be janitors _or_ the presidents of companies. It's not what they do that defines them, but the _contractual security_ that they've chosen.

Secondly, there's the S quadrant. These are the self-employed or the owners of small businesses. These people like to be "their own boss." When it comes to money, they don't like having their income determined by others — if they work hard, they expect to be paid well. Conversely, if they don't do a decent job, then they understand they'll make less. They're often perfectionists — they believe that _nobody_ can do a better job than they can. For them, independence is more important than money. And they're also driven by fear: in this case, the fear of losing their independence.

Thirdly, we come to the B quadrant, the large business owners. In many ways, they're the opposite of those in the S quadrant. They like to surround themselves with smart people from all of the other categories. Their chief talent is _delegating_. **** Henry Ford was a classic example. He wasn't the most talented financial analyst or mechanical engineer, but he _was_ brilliant at hiring others to do those things for him. Those in the B quadrant are able to leave their whole operation running while they do nothing. Instead, they _oversee_ a system that continues to make money for them.

Lastly, we arrive at the I quadrant, the investors. This quadrant is often the realm of the ultra-rich. And what characterizes them is their ability to take _calculated_ risks. Like gamblers, they aren't afraid of volatility. But unlike gamblers, they like to research their risk, so it's no longer such a dangerous prospect. A great investor like Warren Buffet is able to embrace the danger of a financially volatile world, while also making sure to understand, better than anybody else, all of the risks involved. This is the key trait for anyone who wants to attain financial freedom.

### 7. The surest way to achieve financial freedom is to move into business ownership so you can invest. 

If you're reading these blinks, you're probably interested in financial freedom. And who isn't? It means having the time to do what you love — whether that's traveling the world, collecting art, or scuba diving with manta rays.

But how do you reach that point? Well, if you look at how many of the very wealthy have lived, you'll see that they've taken a similar route.

**The key message here is: The surest way to achieve financial freedom is to move into business ownership so you can invest.**

If you look at how people like Bill Gates, Rupert Murdoch, and Warren Buffet became rich, they were in the B quadrant first, and then they moved to the I quadrant. The reason is quite simple. To become rich, it's important to accumulate capital. And the surest way to do that is through investing — in shares, funds or property.

But to invest effectively, you'll need a ready flow of capital and time. And the best way to attain that is to set up a business that can earn money while you sleep. Like Kiyosaki's "rich dad" and his hotel empire, this means setting up a whole system that can operate even when you're not present.

And unless you have that ready supply of cash, you're limited in your investments. Whereas when you invest large sums of money astutely, you can move from the financial security of business ownership to the true financial freedom of large-scale investing.

There's another good reason to follow this path. If you can make it in business, you'll be well equipped to become a great investor. Instinctively, you'll understand which business models will yield better, long-lasting investment returns.

Without this business acumen, you're possibly setting yourself up for investment failure. Many people in the _E_ and _S_ quadrants attempt to move swiftly into the _I_ quadrant, but because they don't have experience of what makes a great business system, they make terrible investments. And without a large organization of their own, they'll have limited funds to invest. That makes their investments all the riskier.

Now, maybe you have no desire to set up or run a large business, but you'd still like to invest. If this is your path, then you should at least learn about the different types of investors. We'll do just that in the next blink.

### 8. There are five different classes of investor. 

For most people, becoming an investor is a daunting prospect. It feels like learning to surf at a beach known for its tiger sharks. However, with a bit of education, a novice can become a confident investor.

So what separates a beginner from, say, Warren Buffet?

**The key message here is: There are five different classes of investor.**

Firstly, there is the _Zero-Financial-Intelligence Level_. Sadly, most of us fall into this category. These are the people who have _nothing_ to invest, even if they're temporarily affluent. That's because they get into more debt than their income allows for. For them to become investors, they must first balance their basic finances.

Secondly, there is the _Savers-Are-Losers Level_. As the name suggests, this is also a financially illiterate level. The accepted wisdom has been that just saving money means financial security later on. But interest levels are very low in the modern economy, so there is very little return if you just park your money in a bank account. And as we saw with the 2008 financial crisis, those who'd saved their money in bonds — that is, government loans, packaged into retirement funds — often had their savings wiped out. Saving **** is not enough.

The third level is the _I'm-Too-Busy-Investor_. These are people who simply hand over their money to a financial advisor. Though these investors are often more successful than the first two groups, they still run a great risk. As many found out after the 2008 crash, their "trusted expert" was nothing of the sort, and they lost great chunks of their investments. This is because they entrusted their money to people who weren't successful investors themselves. Rather, they were just employees at financial advice firms.

Fourth, we come to the _I'm-A-Professional Level_. This is the first real kind of investor. These are people who educate _themselves_ on investing, say, in stocks or real estate. And they do their own in-depth research. Because of this, they're more focused in their investments, _and_ they develop a financial education that'll serve them well throughout their lives. In our precarious world, we'd all do well to reach this level of knowledge, whatever else we do. A financial education is itself a great investment.

Finally, we have the _Capitalist Level_. This is the Warren Buffet level. It entails two steps. First, you become a successful _B,_ or business owner. Then you plow your capital into riskier investments. It's the best route to great riches, but it's also the steepest mountain to climb. If building a business empire isn't your ambition, then the "I'm-A-Professional" investor level is more achievable!

> "_My poor dad often said, 'Investing is risky.' My rich dad often said, 'Being financially uneducated is risky.'"_

### 9. Money provokes irrational feelings which we must overcome. 

We're not always rational. Sometimes, the "irrational" is what makes us human, like when we fall in love. However, with money, our irrationality can be a _real_ problem.

**The key message here is: Money provokes irrational feelings which we must overcome.**

Many of us have illogical fears about all kinds of things. For instance, when Kiyosaki was in the Air Force as a young man, he attended a class on how to survive in the wild, in case his plane was shot down.

One day, his class had to learn how to eat snakes. When the teacher brought out a harmless garden snake, one of the students — a young, macho pilot — jumped up, screamed, and ran out of the room. His phobia of snakes overpowered him, and no matter how much he tried to control it, he couldn't.

As with snakes, money can also provoke deep phobias. Investing has the power to turn otherwise rational people into gibbering wrecks — wrecks who, just like that pilot, jump up, scream, and run out of the room. The truth is, many people find it hard to think logically about money: **** it's an emotional subject because it's so _vital_ to our well-being. Just look at how the financial markets behave. They don't move "rationally" at all, but leap and fall erratically, driven by fear and greed.

But when it comes to money, it's important that your logic triumphs over these feelings. In fact, investing in property or the stock market really doesn't have to be so risky and frightening. Just like the board game Monopoly, the rules are actually very simple. When you're put off by the fear of loss, just remember the best way to win at the game: secure as many profitable "houses" as you can. In the real world of money, those "houses" could be anything from rental properties to stocks, but the same logic applies.

It's a real gift to be able to look at money rationally, and then learn how to gain it. Kiyosaki learned this early on. As he strove for financial success, he and his wife found themselves homeless, living out of their car.

But rather than give in to justifiable fear and abandon their dreams, they knew that, if they kept to their path, they'd succeed. So they slowly and deliberately built a lucrative business. Four years later, **** they were millionaires.

### 10. To achieve financial success, take baby steps and keep the long-term in mind. 

Most of us know the saying "A journey of a thousand miles begins with a single step." Well, when we're talking about money, it's better to say "A journey of a thousand miles begins with a _baby step_."

To achieve financial freedom means embarking on a long journey. So, you should be wary of trying to get rich in the short-term. The truth is, nobody became rich and, crucially, _stayed_ rich, through purely short-term thinking. These days, we're surrounded by _Get Rich Quick_ schemes — those endless courses and books that promise immediate wealth. Many of them sound wonderful. But in reality, the only person they're going to make rich is the person selling them. And the person selling them very often took a long time to accrue __ their wealth.

**The key message here is: To achieve financial success, take baby steps and keep the long-term in mind.**

Too often, we want immediate gratification. Everything seems to be geared toward getting what we want as soon as we want it. But as the saying goes, Rome wasn't built in a day. In fact, many of the greatest investors began small and worked their way up. Remember, even Warren Buffet began selling chewing gum door-to-door. So, you must start small and set achievable goals. Don't overextend yourself or your finances in the rush to get rich.

And thinking long-term means approaching the future pragmatically. The future, sadly, means less job security, as the Information Age promises to be very volatile. So even if you have a wonderful, well-paying job now, don't get addicted to earning money that way. It's unlikely to last forever.

With that in mind, your best way to achieve financial freedom is through long-term, _compound_ investments. What's a compound investment? It means beginning your investments _now_ — however small — and putting your dividends or profits back into your investments. Now, your investment is larger, so your profits will be bigger too. In the end, your profits get bigger and bigger. As Albert Einstein said: "Compound interest is the eighth wonder of the world."

To achieve financial freedom, you should also invest in another thing — your financial education. It can be just as crucial to your long-term safety. A good understanding of the stock market or the real estate sector can be much more useful than a risky investment portfolio. Then, even if the future gets rocky, you'll have the tools to get back on your way to financial freedom!

### 11. Final summary 

The key message in these blinks:

**There are four distinct ways of earning money** — **as an employee, being self-employed or running a small business, owning a big business, or investing. Though there is no one "right way" to live your life, if you want to attain financial freedom, then it's best to move into big business ownership and investing. And even if you don't want to become a business owner, investing is still vital, as it will provide you with a passive source of income throughout your life. Because money is so vital, it's often a very emotional subject. But as soon as we're able to approach financial risks objectively, we're able to take control of our lives.**

Actionable advice:

**If you can, invest judiciously today.**

No matter your age, it's a good idea to begin investing _now_. Though there is risk attached to investing, there is often just as much risk attached to your job, or the certainty of your pension. In fact, if your investment is well-researched — whether it's in stocks or property — then it's likely to provide you with security later in life.

**Got feedback?**

We'd love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Second Chance,_** **by Robert T. Kiyosaki**

You've just learned how certain types of income provide financial freedom, and how others don't. For instance, if you're a hardworking teacher, you might love and value your job, but you'll never attain financial freedom with that job alone. Our economy is simply rigged against you. Whereas, a real estate owner can live easily off their investments.

In another of his books, _Second Chance_, Robert Kiyosaki reflects on why so many of us still believe that we can get ahead by working or studying hard. Not even a well-paying job or a PhD means financial safety these days. The only way to achieve that is through investing and capitalizing on assets. Get the blinks to _Second Chance_ to find out how.
---

### Robert T. Kiyosaki with Sharon L. Lechter

Robert T. Kiyosaki is the best-selling author of _Rich Dad, Poor Dad_. He's the founder of Rich Global LLC and the Rich Dad Company, an education company that provides personal finance and business education through books and videos. He's also an investor and radio personality.

Sharon Lechter is an American businesswoman and leader dedicated to improving the financial education of teens and young adults. She's a spokesperson for the National CPA Financial Literacy Commission; the founder of the financial education organization, Pay Your Family First; and a creator of _Thrive Time for Teens_, an award-winning financial board game.

