---
id: 55c8ab0d3864640007150000
slug: righteous-indignation-en
published_date: 2015-08-13T00:00:00.000+00:00
author: Andrew Breitbart
title: Righteous Indignation
subtitle: Excuse Me While I Save the World!
main_color: F2303A
text_color: BF262E
---

# Righteous Indignation

_Excuse Me While I Save the World!_

**Andrew Breitbart**

_Righteous Indignation_ (2012) outlines the influence of the political Left in the United States and how this group's influence has destroyed American media. The book shows exactly how this situation came to be and explains what those on the Right can do to fight back.

---
### 1. What’s in it for me? Learn how conservatives can fight the influence of the Left in American media. 

Quarreling between members of the right-wing and the left-wing in American politics is as old as the system that established the two sides in the first place. In each party's extreme ranks, however, the "other" is seen as the epitome of political evil.

Yet what late conservative pundit Andrew Breitbart proves in these blinks, is it isn't just that the influence of the Left in the United States is bad. What kept him up at night was specifically _how_ left-leaning political personalities were able to use the media and even Hollywood to achieve their political goals.

Conservatives can arm themselves with this knowledge to fight back against the wave of Left influence, take back the media and ultimately, save America.

In these blinks, you'll learn

  * why actor Martin Sheen called former President George W. Bush a "moron";

  * how to tease journalists to ensure your story gets the most attention; and

  * how a conservative should always be prepared to shoot a video.

### 2. The political Left is harmful to society and controls Americans through its media influence. 

Does the political Left scare you? With the group's inconsistencies, hypocrisy and lack of transparency, it should. There's nowhere in America where the Left's influence is more pronounced, however, than in the media.

For example, the events surrounding Clarence Thomas's nomination to the Supreme Court in 1991 illustrate just how amoral and dishonest the political Left can be. Liberals accused Thomas of sexual harassment; in fact, all he really did was rent porn and invite women on dates.

As an individual, Thomas embodied the American dream: the son of a sharecropper who was about to become a Supreme Court justice. Why would the U.S. Democratic Party want to tarnish the impressive achievements of such an American?

The author was shocked by the events surrounding Thomas's confirmation, and it galvanized him to rethink his political beliefs and the state of political action overall in America. The realization that the media wasn't neutral, that it had actively sided with the Left's cause against Thomas, was too much.

He realized too that Democrats weren't guided by long-established principles but instead by the desire for political gains. After all, Thomas was a Republican and felt the U.S. Constitution shouldn't be concerned with issues such as abortion. In contrast, Democrats were pro-choice and wanted a pro-choice justice to uphold a woman's right to choose.

It's clear that the political Left controls the media in America. And if you control the media, you control society — the Left does this by using its influence with filmmakers, radio and print journalists to spread its propaganda. Taken together, this influence is called the _Democrat-Media Complex_.

The Democrat-Media Complex controls everything, and even helped President Obama get elected. In the run-up to the 2008 election, influential talk show host Oprah Winfrey featured Obama on her show _twice_.

As conservatives, we have to fight against this. The only way to respond to the Democratic Party's unfair media practices is to counter its message through _new media_, such as video, Twitter and Facebook.

While en route to a Tea Party gathering, the author learned the value of video firsthand. Democrats attacked his car with eggs then called the police, claiming that _he_ had orchestrated the attack. Fortunately, the author had filmed everything and could set the story straight.

### 3. The Democrat-Media Complex was built on the ideas of Marxist-leaning thinkers and social activists. 

So where did the Democrat-Media Complex come from?

This state of affairs had its start with a group of intellectuals associated with what was known as the Frankfurt School. Around the time of World War II, many thinkers influenced by the European school's progressive, Marxist-influenced ideas came to the United States.

Philosopher and political theorist Herbert Marcuse was an influential member of the Frankfurt School, and some of his ideas, such as the slogan, "Make love, not war," later appealed to the 1960s generation, youth who readily turned against the morals and values of their parents.

Marcuse was also eager to dismantle the capitalist system by mobilizing marginalized ethnic and sexual minorities in America. By this point, universities in the United States were already offering courses on gender and queer studies, for example!

Marcuse was a Marxist, and saw these victimized groups as making up a new proletariat — as he didn't think a worker's revolution would fly in America. Thus minorities would be the group to take down capitalism.

Later on, community organizer Saul Alinsky built on Marcuse's ideas and was influential in bringing Frankfurt School ideas to the forefront of American society. He essentially distilled the complex philosophical thought to make it more practical and more applicable for collective action.

Alinsky in particular used ridicule as a weapon of social protest, to better cross cultural boundaries. One of his more infamous actions was to suggest African-Americans eat beans, then buy tickets to the symphony and fart through the performance, to shock the typical upper-class, white audience.

Alinsky's book, _Rules for Radicals,_ served as a manual for the powerless _have-nots_ in society, looking to displace the _haves_. His famous rule, "pick the target, personalize and polarize it," summed up his _politics of personal destruction_ : present your opponent as entirely evil, and yourself as entirely good.

> _"We can't win the political war until we win the cultural war."_

### 4. The Left-controlled media and Hollywood are powerful – and thus dangerous. 

So how does the Left leverage its celebrity and media contacts? Famous Hollywood actors and the media often walk in lockstep with regard to leftist propaganda.

The assaults on former President George W. Bush are a perfect example. Hollywood led the way in demonizing Bush, and the media followed. The frenzy began even before Bush took office, as of course a Democrat-controlled media wouldn't like a Republican president.

While Bush unequivocally won the Florida election in 2000, bumper stickers on cars around the country proclaimed "Not My President" or "Selected, Not Elected." Propaganda at its finest! Even the author's babysitter's car boasted such a bumper sticker, even though the babysitter wasn't overtly political.

Hollywood stars and influential musicians, including George Clooney, Robert Redford, Susan Sarandon and Madonna, also openly criticized Bush. Actor Martin Sheen, who played an American president on the popular TV program _The West Wing_, called Bush a "moron" on the BBC.

These attacks show just how much power Hollywood and the media have — and because of this, they're very dangerous. They're propaganda tools of the Left.

This state of affairs is what inspired the author to expose the Left's injustices and strengthen the Right through new media. Yet he went about it in a curious fashion, by helping columnist and political commentator Arianna Huffington launch _The Huffington Post_, a web-based platform for Left politics.

The author's reasoning was thus: First, he wanted to spotlight the Left's illogical thought processes by giving the movement a public platform. Second, he wanted to illustrate how similar _The Huffington Post_ would be to other media outlets, such as _The_ _New York Times_, which claims to be neutral.

Importantly, even though _The Huffington Post_ is openly left-leaning, its front page is nearly the same as _The New York Times_ on most days.

The author eventually grew uncomfortable with his involvement, however, and decided to come out honestly as a Republican.

> _"Hollywood is more important than Washington…pop culture matters."_

### 5. New media is the Right’s antidote to the Left's destructive hold on mainstream media. 

The Left has controlled mainstream media so far, but new media — Twitter, Facebook, blogs and web-based news portals like _breitbart.com_ — offer the Right a chance to catch up.

The Right needs to use new media to attack the Left. Recent technological developments can help loosen the Democratic Party's grip on the media.

The 2009 scandal involving the now-defunct Association of Community Organizations for Reform Now (ACORN) is a key example of the power of new media. Activists James O'Keefe and Hannah Giles went "undercover," posing as a pimp and prostitute looking for ways to hide illegal activities and avoid taxes, and secretly filmed interviews with ACORN activists who tried to help them to do so.

You can't argue with a video of real events!

The footage wasn't just released in one place, either — the ineffective strategy of a mainstream media outlet like CNN. When the author got the story from O'Keefe, he used the _drip-drip-drip from everywhere_ strategy _,_ sending it to Fox News, YouTube and other online media platforms over an extended period of time.

The video went viral. New media works!

There are other strategies the Right must use to weaken the Left's influence.

First, prominent Republicans should be open about their secrets. The Right holds more moral standards than does the Left, so it's easier to criticize the Right for its moral hypocrisy. The Right has to come clean, so the Left can stop spreading harmful stereotypes. Doing so will also weaken the negative stigma associated with the Right.

Be selective about the news you read, and contribute to the media yourself — don't let the Democrat-Media Complex do it for you!

If you uncover an important story, like O'Keefe and Giles did, spread it to as many places as you can. That will ensure your journalistic revelations aren't smothered in the grip of the Left-controlled media.

### 6. Final summary 

The key message in this book:

**The political Left has successfully controlled media outlets and Hollywood for decades. Conservatives need to stand up and fight this situation by harnessing the power of new media like Facebook and Twitter.**

Actionable advice:

**Carry a recording device with you always.**

Bring a camera anytime you're at an event and could witness something that might be denied or ignored by the Left-leaning media. No one can argue with real video or audio footage!

**Suggested** **further** **reading:** ** _Manufacturing Consent_** **by Edward S. Herman and Noam Chomsky**

_Manufacturing Consent_ takes a critical view of the mass media to ask why only a narrow range of opinions are favored whilst others are suppressed or ignored.

It formulates a propaganda model which shows how alternative and independent information is filtered out by various financial and political factors allowing the news agenda to be dominated by those working on behalf of the wealthy and powerful. Far from being a free press, the media in fact maintain our unequal and unfair society.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Andrew Breitbart

Andrew Breitbart was a conservative author and political commentator, known for his online news and video sites, _breitbart.com_ and _breitbart.tv._ He was instrumental in helping launch _The_ _Huffington Post_. Breitbart died in 2012.

