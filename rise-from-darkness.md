---
id: 587232706267350004ccacda
slug: rise-from-darkness-en
published_date: 2017-01-09T00:00:00.000+00:00
author: Kristian Hall
title: Rise from Darkness
subtitle: How to Overcome Depression through Cognitive Behavioral Therapy and Positive Psychology
main_color: 3F8EC7
text_color: 2F6A94
---

# Rise from Darkness

_How to Overcome Depression through Cognitive Behavioral Therapy and Positive Psychology_

**Kristian Hall**

_Rise from Darkness: How to Overcome Depression through Cognitive Behavioral Therapy and Positive Psychology_ (2015) details techniques anyone can use to help overcome depression. These blinks explore the causes of depression and map out the near- and long-term strategies readers can use to develop a recovery program.

---
### 1. What’s in it for me? Discover simple yet effective strategies to improve your state of mind. 

Do you feel that you spend too much time focusing on the negative things in life? Do you wish you were a happier person? You're not alone. Bouts of depression affect people all over the world.

Psychologists have developed effective, life-changing techniques that people who suffer from depression can use and benefit from. The best part? You don't have to visit a therapist, and some techniques take less than a few minutes to follow.

These blinks will show you the right path to radiating happiness in no time!

In these blinks, you'll learn

  * how to keep negative thoughts from spiraling out of control;

  * why it's important to show thankfulness daily;

  * why self-hypnosis isn't just hype (and why you should try it!)

### 2. The way our brains filter information can make situations seem a lot worse than they are. 

Every image of the world you perceive is first turned upside-down by the lenses in your eyes and then flipped again by the visual cortex in your brain.

Your visual cortex acts as a filter — your brain indeed has many filters — but sometimes these filters can do more harm than good.

In short, _mental filters_ affect how we perceive the world and are the reason why everybody experiences life differently.

Picture two friends, a man and a woman, walking through a park. Suddenly a dog runs up to them. The man had a traumatic experience with dogs as a child; he becomes terrified as the dog approaches. The woman grew up around dogs, so she knows the chubby Labrador has just come to say hello.

Filters serve an important purpose, in that they enable us to make sense of a complex world.

Filters break down information to help us understand, allowing us to recognize patterns in everything from news stories to the behavior of friends and family. Our views and values, from politics to personal ethics, are affected by these filters.

Mental filters can also malfunction, however, and distort our perception of reality.

We tend to view the world through a lens colored by our beliefs, ignoring anything that contradicts those beliefs. If you see people as inherently untrustworthy, for example, then you're going to see more bad than good in people in general.

Psychologists Karen Reivich and Andrew Shatté call this the Velcro/Teflon effect. Anything that backs up our beliefs sticks like Velcro, while anything that contradicts them slides off like non-stick Teflon.

We form many of our filters as children by paying attention to those around us. Because of this, it's a good idea to inspect your beliefs and see whether you need to "update" them.

### 3. Flawed thinking leads to negative thought patterns that soon turn into debilitating demons. 

Picture a car driving in circles in a large field. After many loops, the circular grooves in the soil become deep, making the driving easier — but making any deviation from the track almost impossible.

Our _thought patterns_ are like this. We can get stuck in loops with thoughts and feelings, and once stuck, find it a struggle to move beyond them.

If you hold negative beliefs about yourself, often all it takes is a minor incident to start you on a downward spiral.

Let's say you show up at a party. You don't immediately see anyone you know and feel you're getting unfriendly looks. Then you see your old friend Rick telling a story to a small crowd. You go up and say hello, but you suddenly feel that you've annoyed everyone by interrupting the story — Rick included.

The evening is now a disaster, and you quietly leave, torturing yourself on the way home for your lack of social skills.

Of course, if Rick were asked about the incident, he would say that he was pleased to see you — but just happened to be building up to a punchline when you jumped in to say hello.

This story is a prime example of two of the most common types of _thought fallacies_.

The first is _over-dramatizing_. People tend to think negative experiences are more significant than they are. Inadvertently interrupting the flow of a story is not going to turn a room against you!

The second is _mind-reading_, or believing you know what others are thinking. By misinterpreting people's expressions and body language, you can easily make false judgments.

So if you feel yourself seeing only the worst in every situation, remember to question your thought patterns and think of alternative explanations.

### 4. Writing down your worries and listing what you are grateful for are good ways to combat depression. 

Psychologists used to focus only on mending what was broken, and mental disorders were the industry's bread and butter. The relatively new field of _positive psychology_ represents a radical departure from this tradition. Its techniques focus on increasing individual happiness.

The simple act of writing down a list of positive and negative activities in your life can give you a roadmap to recovery.

Take a sheet of paper and draw a horizontal line through the middle. In the top half of the sheet, list all the activities that bring you joy, such as playing music or spending time with friends.

Once you've written down everything you can think of, turn your attention to the bottom half of the sheet. There, list all the people and negative activities that sap your energy and worsen your mood.

This paper is a snapshot of the things that influence you daily. For the next week, keep the list nearby and make a conscious effort to seek out things to do or experience from the top rather than the bottom of the sheet.

You'll soon find that you automatically choose activities that match the kind of positive life you desire.

Another powerful tool for reducing depression is to express gratitude daily.

One way to do this is to keep a diary. In it, write down all the things you are grateful for, from the steadfast support a friend has shown to the warmth of spring sunshine. Write in your diary for two weeks, registering any differences in how you feel day to day.

Practicing gratitude works because doing so helps you to shift your focus from what you feel is lacking in your life to the valuable things that are already there.

Research has shown that keeping a gratitude journal can have a significant positive boost on the moods of people suffering from depression.

> "_If I were to point out just one simple habit that reduces depression it would be thankfulness."_

### 5. Use simple mental tricks to improve your mood and reduce tension daily. 

Most of us can only think about one thing at a time. This limitation can be made an advantage, however, as it can help banish _negative thought patterns._

In short, if you change your thought patterns, you can improve your mood.

When it feels like you're spiraling downward, you can use a _mantra_ to reverse the freefall.

A mantra — a repeated sound, word or phrase — works by interrupting the negative flow of thoughts.

You can come up with many words and phrases for your mantra. "Strength" or "This too shall pass" are two examples. A mantra is most effective, however, when you come up with your own.

First, decide in which situations you're going use your mantra. If you find you're stressed by incidents such as being cut off in heavy traffic, your mantra for such a moment might be, "calm."

To make this mantra have an even more calming effect, link saying the word to an image of a placid lake or the soothing sounds of waves lapping against the shore.

Another way you can reduce tension is by rating situations on a _catastrophe scale_. Doing so will help you put daily worries into a larger perspective. Your scale goes to 100, where one is the most trivial scenario and 100 is a true catastrophe.

Use this scale each time something upsets you. If you don't get that job, is it among the worst things that could happen to you, ever? Does it even deserve to be ranked above a ten?

By realizing that most setbacks are relatively minor, we can avoid the pitfall of over-dramatizing events.

### 6. Self-hypnosis can help you rewrite damaging mental habits with more positive ways of thinking. 

Have you ever watched anyone be hypnotized? Unlike what many people think, hypnotism doesn't involve a person taking control of another person.

For hypnotism to work, you have to _want_ to be hypnotized. A hypnotist is there to act as a guide, but people really hypnotize themselves.

Behind all the mystery, there is a rational, well-defined procedure for hypnotism.

When hypnotized, you enter a trance-like state but remain conscious and aware of your surroundings. In this state, your mind becomes more suggestible, so it is easier to make positive changes to your behavior and thought patterns.

We can think of self-hypnosis like technology. When you are hypnotized, you can fiddle with your brain's operating system and modify programs running in the background — your subconscious thoughts. Thus self-hypnosis is an effective way to change, delete and upgrade thought patterns.

So how do you go about hypnotizing yourself?

The best method is to create a soundtrack which you listen to regularly. A soundtrack is a script that will help you reprogram your brain — words or sentences that emphasize the beliefs and behaviors you wish to change.

For example, your soundtrack could include phrases like, "I am becoming healthier and healthier" or "Every day I'm worrying much less and sleeping far better." You can make your own soundtrack, for example, or purchase one pre-made.

Once you're happy with your script, listen to it in a relaxed environment. You need to listen to your soundtrack multiple times, as it can take time for change to happen. At this stage, your soundtrack has the power indeed to make you calmer, stronger, more confident and healthier.

You may already be hypnotizing yourself without even knowing it. Every time you tell yourself you can't achieve something, you're reinforcing thought patterns and affecting your behavior!

### 7. Visualization is an effective tool for achieving personal goals and battling general depression. 

Your mind can't always tell the difference between what's real and what's not. This is why just thinking of a tarantula crawling toward you might make you break out in a cold sweat!

Visualization can be a powerful thing. Some people have even increased muscle mass simply by lifting imaginary weights in their head!

You can practice visualization to help prepare for important moments in your life.

Shut your eyes and picture a movie screen. On the screen, you're watching an important upcoming event, in which everything goes as well as it possibly can.

If you're preparing for a job interview, picture yourself sitting in the interview. Make details vivid; hear the sounds of office chatter and smell the percolating coffee. Then see yourself bursting with confidence at the table, winning over interviewers with your wit and knowledge.

Then flash forward to when you accept the job, shaking hands with your interviewers. Notice how good this makes you feel! Finally, imagine yourself on your first day, pleased with your new position.

Visualization can also help you achieve more general goals, like becoming a certain type of person.

Picture your _ideal self_ as a lead character in a film. Add as much detail as possible, down to the color of the socks you'd wear! 

Follow this version of yourself around and notice the differences between the "real" you and the "ideal" you. Are you less stressed? Do you laugh more? Picture a full day where everything is the best it can possibly be.

The next stage is for you to step into the shoes of your ideal self and visualize what you would say and think. Do this for seven days and see if you notice any changes in your "real" life.

If this technique works, then build it into your daily routine for better health and happiness.

### 8. Final summary 

The key message in this book:

**While the depths of depression can vary greatly among individuals, there are practical steps anyone can take to set their lives on a more upbeat track. With some simple methods, such as hypnotism and visualization, you too can live a happier, more positive life.**

Actionable advice:

**Listen to music to put you in the right mood.**

Music can have an immediate effect on your mood. A tune doesn't have to be upbeat to cheer you up, yet there are times when a positive pop song is the right choice. Music that makes you sad or angry also can help you release emotions until you're ready to take steps toward overcoming the obstacles in your way.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _My Age of Anxiety_** **by Scott Stossel**

This book offers a candid, valuable glimpse into the world of the clinically anxious. The author takes us through his personal struggle with anxiety while presenting us with scientific, philosophical and literary work about the condition and the treatments available for it.
---

### Kristian Hall

Author Kristian Hall overcame a decade of depression by researching and practicing techniques from positive psychology and cognitive behavioral therapy. He lives in Norway, and this is his first book.

