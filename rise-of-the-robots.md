---
id: 56b8e7e8d119f900070000b7
slug: rise-of-the-robots-en
published_date: 2016-02-11T00:00:00.000+00:00
author: Martin Ford
title: Rise of the Robots
subtitle: Technology and the Threat of a Jobless Future
main_color: None
text_color: None
---

# Rise of the Robots

_Technology and the Threat of a Jobless Future_

**Martin Ford**

_Rise of the Robots_ (2015) explains how increased automation and progressively intelligent information technology will fundamentally alter the way we organize, conduct business and live our everyday lives.

---
### 1. What’s in it for me? Get ready for the machines. 

In the early nineteenth century, textile workers across the United Kingdom stormed factories and destroyed hundreds of weaving machines. These workers feared for their livelihood. Would they be replaced by low-skill, low-wage workers who only had to operate these new machines in some factory?

Since then, advancements in technology have eaten up one job sector after another. For the most part, society has stood around and watched. It wasn't until the advent of computers and information technology that this trend really spiraled out of control. 

Looking at the current speed of change, you would be forgiven for wondering if in another 100 years there will be _any_ jobs left for humans.

So what can we do about this? 

In these blinks, you'll learn

  * why information technology is not like other kinds of technology;

  * why Henry Ford doubled his workers' salaries; and

  * why you shouldn't have to work to receive an income.

### 2. Today machines are everywhere, and automation will impact every aspect of society. 

A lot of people think of machines as tools — things that help you accomplish a particular task. But this idea, as you'll soon discover, should be left in the past.

From the enormous robots on the factory assembly line to the smartphone in your pocket, machines are spreading faster than ever, and computer power is growing exponentially. These machines are no longer just tools for humans to wield. They increasingly work without human interference.

Just look at the development of robots.

Robots all run on the same software platform: _Robot Operating System_, or ROS. As recent history shows, once there is a standard operating system and some inexpensive, easy-to-use programming tools (think Android or iOS), an explosion of applications will follow. 

In turn, these applications will inevitably impact the workforce. Whereas companies in some industries, like American textile manufacturers, once moved their operations to developing countries to stay competitive, automation allows them to stay domestic. Robots today are extremely efficient, and they're more productive than low-wage workers. 

But even the most automated factory needs some manual work. For the American workforce, this means more jobs. For the low-wage countries where these jobs could end up, this means economic pain. For example, between 1995 and 2002, China lost 15 percent of its manufacturing jobs largely due to automation. 

What's even more noteworthy is that, historically, technological advancement was always correlated with more jobs and higher pay. That's no longer the case.

In 2013, a typical production worker earned 13 percent less (adjusting for inflation) than they did in 1973. Productivity, on the other hand, rose by 107 percent.

Think about this: in order to keep up with the growing number of people seeking employment, the market would need to create at least one million jobs per year. But in absolute terms, in the first decade of the twenty-first century, zero new jobs were created in the United States.

> _"It's a good bet that nearly all of us will be surprised by the progress that occurs in the coming years and decades."_

### 3. Information technology is unlike any other paradigm-shifting technology in history. 

The discovery of electricity, the invention of the automobile — these kinds of breakthroughs radically change society forever. Information technology is another such development, though the changes it brings are even bigger.

Computers and computer programs can perform a wide variety of tasks, develop quickly and are easy to replicate. But each step in advancement, like creating a program that calculates the economic risks of a given decision, leaves workers without jobs. 

Before information technology, employers looked for the cheapest, or "least bad," workers. Today, that doesn't make sense economically, because a piece of technology can probably do a better job at a smaller price.

In theory, information technologies like the internet lower barriers to entry, giving more people a chance to succeed. For example, merchants no longer need to invest in a physical shop to sell their wares — they can sell them online. But in reality, the outcome is nearly always winner takes all, and everyone else is left in the dust.

A few lucky people, like Mark Zuckerberg, profit immensely from taxpayer-funded technology. Indeed, most progress in IT was developed with taxpayers' money, through channels like the Defense Advanced Research Projects Agency, which developed the foundation of what eventually became the internet.

So what will a future driven by information technology look like? It's hard to know exactly, but we do know that these transformative changes will lead to new conflicts.

Take the two dominant perspectives regarding autonomous cars, for instance: the automotive industry envisions cars that drive autonomously but still require some driver intervention. Thus the concept of private car ownership would be maintained. Google, on the other hand, has an entirely different vision. They envision collectively owned, on-call cars that are shared between people and spend most of their time on the road. In Google's version of the future, cars would no longer be a sign of status, and parking spaces would be almost unnecessary. 

But even Google's vision has serious consequences. Just think of all the parking attendants and taxi drivers who would soon be out of work.

### 4. The advancement of information technology has had a huge impact on the economy. 

What will the future look like when robots are doing the jobs people normally do? Will there be any jobs left?

People have worried about the effects of automation for a long time. Even though many of these worries eventually prove to be unfounded, that doesn't mean there's nothing to worry about in the future.

Indeed, numerous trends clearly demonstrate the transformative role of advancing information technology and increased automation.

For example, before 1973, the percentage of the national income that went to wages was pretty much constant. Since 1973, however, that percentage has consistently decreased — and not just in the United States. For production workers, wages peaked in 1973 at $763 per week in today's dollars. From then on, automation increased productivity, but not wages.

As a result, inequality has steadily grown since the 1970s. Between 1993 and 2010, more than half of the increase in national income went to the top one percent of income earners in the United States. So it's no wonder why the other 99 percent worry about automation leaving them even further behind.

But surely information technology can't be held solely responsible for this unprecedented income inequality. Right?

Well, globalization has contributed to these trends. Manufacturers in developed countries moved their production overseas to take advantage of cheaper labor, which led to fewer jobs and depressed wages in those developed countries. But a 2011 study by Galina Hale and Bart Hobijn demonstrated that 82 percent of goods and services purchased in the US are produced domestically. In other words, globalization alone can't account for the new inequality. 

Politics also plays an important role. But while giving big tax breaks to businesses that invest in automation does exacerbate inequality, it didn't start it.

### 5. Automation threatens low-skilled, high-skilled and high-paying jobs alike. 

We've known for a while that increased automation cuts back on the need for low-skilled jobs. But today, even high-skilled jobs that require higher education are in danger. 

Though high-skilled work may be more complex than work on an assembly line, it's still largely comprised of routines. For example, a copywriter learns to write by reading good copywriting and assimilating that into her work. A computer can do the same thing. If you give it enough to learn, it can figure out how to do it just as well, or better, than the copywriter.

In fact, we already see initial versions of this today, such as the e-mail spam filters that have learned over time what's spam and what's worth reading. 

Computers have become so advanced that they can now improve themselves. Google Translate, for example, doesn't need any dictionary entry to be entered manually. Rather, it compares words and sentences with other translated documents in huge databases. That means that Google Translate becomes smarter and more accurate with each document it translates.

So self-learning systems can process historical data in order to do high-skill jobs. But an even larger effect will come via new data organization techniques. When information technology can analyze massive data warehouses, even jobs like quality management and accounting could potentially be automated.

For example, after the historic victory of IBM's supercomputer Watson, over human competitors on the game show _Jeopardy!_, Watson's analytical software was made public to the world. Now, among other things, it assists in diagnosis in some leading hospitals, thereby eliminating the need for a medical second opinion.

Moreover, _genetic programming_, a programming methodology based on Darwinian notions of natural selection, has even endowed computers with creativity. The computer cluster Iamus, for example, has created numerous original works of classical music, some of which have even been played by the London Symphony Orchestra!

### 6. There are just two areas not yet deeply affected by automation: health care and higher education. 

While computers seem ubiquitous in our modern world, there are still two job sectors that remain relatively unaffected by the advancement of information technology. 

The most important of these is health care. Costs associated with health care have steadily increased over the years, partly because hospitals and medical providers are hesitant to embrace automation.

That's not to say there aren't exceptions. For example, the MD Anderson Cancer Center in Texas uses Watson, and makes its data accessible online for other clinics to improve their diagnoses. The University of California Medical Center in San Francisco also makes use of robots to sort and dose medications.

These technologies, however, are exceptions. They play only minor roles in health care. While hospital robots can do things like read radiology data, they're not often utilized.

Similarly, higher education has been relatively unaffected by information technology. 

For example, some institutions use computers to score tests. But a lot of work that could be automated, like in admissions departments, is still being done by people. 

The biggest technological change in education came in the form of Massive Open Online Courses, also called MOOCs. The goal of MOOCs was to bring higher education to the masses by streaming lectures and classes over the internet. This way, you could take a Harvard University Philosophy class, for instance, from anywhere on the globe. The problem was that MOOC users tended to already be enrolled in universities and other education institutions. Another problem was the question of certification, with renowned colleges demonstrating an unwillingness to certify credits from free online courses. 

As a result, like health care, higher education's sluggishness in adopting information technology has resulted in rising costs.

### 7. Increasing automation will cut demand and endanger economic growth. 

By now, you've learned how automation will result in a loss of jobs for many people. But what kind of effect will widespread automation have on the economy in general?

For starters, there will be fewer workers. That means fewer consumers and lower demand for goods and services.

Only two groups demand products as end-users: individuals and governments. While businesses do purchase products and services, they are never at the end of the value chain. Rather, they buy products and services in order to create their own products and services demanded by individuals or governments.

But we live in a world where only top incomes are rising, and all other income brackets are stagnant or even falling. This will enlarge the market for luxuries, but negatively affect demand for a vast majority of goods.

As this trend of disparity in purchasing power continues, our mass-market economy will be in serious danger. 

Consider, for example, that in 1992 the top 5 percent of US households (in terms of income) accounted for 27 percent of consumer spending, and the bottom 80 for 47 percent. In 2012, the top 5 percent accounted for 38 percent of all consumer spending, the bottom 80 for only 39 percent. 

For a long time, this increase in income inequality was covered by credit, which then resulted in huge debts. But ever since the financial crisis of 2008, credit has become more expensive and more difficult for lower earners to receive.

Even the spending of the top five percent isn't secure, as most of them rely on income from work that could soon be automated. Only the richest of the rich seem to have any real security. Remember, during the recovery from the Great Recession, 95 percent of all income growth occurred in the top one percent of earners.

### 8. The best solution for the economic results of automation is a basic income. 

Money for nothing in exchange. You don't have to work or trade. Sounds too good to be true, doesn't it? It isn't. In fact, it could be the solution to the problems automation may cause.

In previous blinks, you saw how automation negatively affects demand. The masses need money to spend to increase demand. _Basic income_, that is, an income you receive independent of work, could make that happen.

Basic income isn't socialism. Actually, the idea was brought forward by a conservative economist, Friedrich Hayek. For him, basic income is the best means of keeping the welfare state small.

Basic income guarantees financial security and consumption power without large and inefficient bureaucracy. And since there is always a safety net, the idea is that people will be more likely to take economic risks and be economically active.

Despite its value, getting people behind basic income is tough. Many find the idea of paying someone who doesn't work downright offensive. Another point of contention is cost. Giving Americans between 21 and 65 years of age an annual basic income of $10,000 would cost $2 trillion. 

However, the cuts in government spending made possible by basic income, like food stamps, amount to $1 trillion. Moreover, this income would be taxable, and the increased economic activity would not only lead to more tax revenues but also fewer unexpected costs, like those which a recession can precipitate. 

In essence, basic income would largely pay for itself. 

The real sticking point is simply that other solutions won't work in a world driven by robots. For example, the classic response when managing job losses due to technological development is to encourage education. But education is costly, and won't guarantee you a job in our economy anyway.

Another approach might be to simply stop technological progress. But as long as automation is cheaper and more competitive than human labor, the proliferation of automation won't stop. Businesses are too incentivized to cut costs. 

Our best bet at saving the economy in the face of inevitable automation and information technology is basic income, or at least a step in that direction.

### 9. Final summary 

The key message in this book:

**Information technology will change the world like nothing has before, setting the stage for radically different social and economic organization. That is, if the benefits of automation don't kill us before we get there.**

**Suggested** **further** **reading:** ** _The Singularity Is Near_** **by Ray Kurzweil**

_The Singularity Is Near_ (2005) shows how evolution is drawing ever closer to a dramatic new phase, in that by 2029, computers will be smarter than humans, and not just in terms of logic and math. This event will not only profoundly change how we live but also pose serious questions about humanity's future.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Martin Ford

Martin Ford is the founder of a software development firm, public speaker and expert on the future impact of information technology. He is also the author of the bestseller _The Lights in the Tunnel: Automation, Accelerating Technology and the Economy of the Future_.

