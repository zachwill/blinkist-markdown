---
id: 59ec8effb238e1000645543f
slug: rise-of-the-rocket-girls-en
published_date: 2017-10-27T00:00:00.000+00:00
author: Nathalia Holt
title: Rise of the Rocket Girls
subtitle: The Women Who Propelled Us, from Missiles to the Moon to Mars
main_color: 84A8DA
text_color: 556C8C
---

# Rise of the Rocket Girls

_The Women Who Propelled Us, from Missiles to the Moon to Mars_

**Nathalia Holt**

_Rise of the Rocket Girls_ (2016) reveals the intriguing and enlightening stories of the women who worked at the Jet Propulsion Laboratory. It traces the laboratory from its earliest days through to modern times, from its quirky beginnings to its role as one of NASA's most important component parts. These women were responsible for crunching numbers and the important calculations that kept the United States in the space race and helped launch rockets, satellites and probes into the farthest corners of the solar system. Their influence cannot be denied. And, more than that, it must be acknowledged.

---
### 1. What’s in it for me? Learn about the women behind the space program. 

Despite the many forward steps in feminism and the women's rights movement, many people are still oblivious to the role that women played in the space program's early years during the 1950s and 1960s. What's worse, there are still people out there who think women can't "do" science.

Most history books and movies about space focus on the astronauts in space and the male engineers in the control room, but the truth is that intelligent and driven women were a central force in making all those male accomplishments possible. It doesn't matter that they went unseen and unrecognized at the time. Today, we recognize that their impact was astronomical.

In these blinks, you'll learn

  * that electronic computers once paled in comparison to human ones;

  * that not having an engineering degree didn't stop women from working on space missions; and

  * how much women's work in space exploration has gone unrecognized and uncelebrated.

### 2. Barbara Canright was the first of the female computers to work at the Jet Propulsion Laboratory. 

In 1939, after winning a grant from the National Academy of Sciences, a rambunctious trio of friends — Ed Forman, Frank Malina and Jack Parsons — founded the Jet Propulsion Laboratory, or JPL, at the California Technical Institute. Among the first people they hired were Barbara and Richard Canright.

Barbara and Richard had met at Caltech, where Richard was a graduate student and Barbara was a typist. This is also where Richard first met the founders of JPL.

As the name suggests, JPL was a laboratory that created jet engines. One of the early plans was to build a jet engine that could quickly propel an airplane off the ground, the hope being to speed up launching procedures and shorten runways.

JPL pursued this goal in a pretty straightforward manner, essentially just strapping powerful rockets onto small planes. Rockets were something that fascinated the team at JPL. Yet the word "rocket" was strongly associated with science fiction at the time, so, to sound professional and scientific, they claimed that their experiments dealt strictly with jet engines.

These experiments required a lot of calculations, which is where Barbara Canright came in. She was a math whiz and could expertly crunch numbers to measure force and propulsion. Since there were no automated computers in the 1940s, it all had to be done by hand. A single experiment might take a week of calculations and fill up to eight notebooks.

And so it wasn't long before JPL started recruiting a new computing team to help Mrs. Canright with calculations. Two more female computers came on board: Virginia Prettyman and Macie Roberts. Interestingly, in those days the term "computer" was still used for people rather than machines as they were the ones who did the computing!

Roberts was soon promoted to supervisor and charged with hiring new employees, and she set about establishing a female computer team. This was not a typical move in the early 1940s. Most places wouldn't put a woman in charge of hiring and managing her own all-female division within an engineering department. But JPL wasn't a typical place.

### 3. JPL expanded rapidly and soon began working on military rockets. 

It took a fair few crashes — thankfully none of them fatal — and explosions to get there, but, finally, on August 12, 1941, JPL successfully managed to reduce the length of an airplane's takeoff by 50 percent.

It was good timing, too. The United States entered World War II just a few months later, and JPL won a lucrative contract from the US Army.

Thanks to this influx of money, JPL expanded quickly and was soon able to engineer new and effective military rockets.

Their facilities, hidden away in the canyons on the outskirts of Los Angeles, increased in size and number. They were also able to hire new employees as engineers and computers, which they did by placing ads in newspapers and on college bulletin boards.

All that was required to become a computer were skills in advanced math. Degrees weren't a prerequisite, which was just as well considering it wasn't until the 1970s that a significant number of women were able to gain engineering degrees.

In the 1940s, JPL was determined to find the best rocket-propellant mixture. Indeed, Barbara Canright and Macie Roberts spent most of their time analyzing experiment results.

It was chemist Jack Parsons who had the breakthrough, however. He came up with the idea of using liquefied asphalt as the key ingredient in the mix.

But without the hard work of the computers at JPL, the optimal mix would have taken much longer to determine. That mixture ended up being 70 percent liquefied Texaco No. 18 asphalt, 30 percent lubricating oil and crushed potassium perchlorate as the oxidizer.

It was a hit with military bigwigs. The propellant provided a substantial 200 pounds of thrust — and they simply loved the fact that it was produced from a cheap material like asphalt.

JPL's stock was rising. The results were good. But they knew that the real future lay in unexplored territory: outer space.

### 4. While developing missiles for the army, JPL simultaneously laid the groundwork for space exploration. 

Soon after JPL created the propellant mix nicknamed "Jack's cake," it began working on the army's Corporal missile project. The idea was to create a guided rocket that could carry a large warhead.

But JPL didn't stop there. While developing the missile, the JPL team used the opportunity to research advanced rockets that could one day travel even farther. They dreamt of discovery, not weaponry.

Barbara Paulson was one of the computers who worked on the Corporal project. Her work was particularly focused on a missile variant that used liquid propellants and was based on the concept of a two-stage launch.

It was this multi-stage technique that proved to be the key to long-distance rockets. The initial launch gets the rocket off the ground, but subsequent "launches" fire the missile hundreds of miles farther once it's safely up in the air.

The multi-stage launch that Paulson helped compute ensured that the Corporal could carry its heavy warhead a staggering 200 miles. In fact, it's this self-same system that makes it possible for rockets to escape Earth's gravitational pull.

JPL wanted to be a pioneer in building rockets that left Earth's atmosphere, and so they began Project Orbiter, the aim of which was to send a satellite into space.

Sadly, though, the Department of Defense decided to cancel Project Orbiter and work with the navy instead. Needless to say, the engineers and computers at JPL were heartbroken. However, they refused to set aside their dreams of space exploration. When they started their next project, the Jupiter-C missile, they secretly continued their satellite research.

Due in no small part to the calculations of the computing team, Jupiter-C got better and better. And so it was that on September 19, 1956, they sent a rocket 3,335 miles up into the air, higher than any previous rocket..

### 5. The success of Jupiter-C was bittersweet, but it all counted in the space race. 

JPL's engineers were a diverse bunch of female computers. Among them were Helen Yee Ling Chow, whose computations led to Jupiter-C's success, and Janez Lawson, JPL's first African-American employee. Thanks to their efforts, JPL's Jupiter-C rocket was able to break free from Earth's gravitational pull.

But their efforts were overshadowed just a year later.

Although they'd successfully launched all four stages of the Jupiter-C rocket, it was still just a test. Therefore, instead of releasing a satellite in the fourth and final stage, the rocket only carried sandbags equivalent to a satellite's weight.

This meant that the honor of sending the first satellite into space went to the Russians; in 1957, they launched Sputnik.

It was only after the public failure of the navy's Project Vanguard, in December, 1957 — their rocket exploded on the launchpad — that JPL was given the go ahead.

In fact, JPL had been doing satellite research on the quiet during the Jupiter-C program. This meant they were ready to launch almost immediately: after government approval, it took just under 90 days for JPL to successfully launch the Explorer satellite on January 31st, 1958. It had been sitting in storage.

Feats of mathematics were involved, too. Computer Marie Crowley plotted the calculations for Microlock, a tracking system that accurately picked up the satellite's low-frequency radio signal from thousands of miles away.

Meanwhile, at mission control center, Paulson was analyzing incoming data and calculating the satellite's trajectory. This task required great skill. For eight minutes the satellite's signal was lost during the first crucial orbit. Panic broke out. But Paulson knew from her calculations that all should be well. And she was proved right when the signal was detected again.

And all the while the famous physicist Richard Feynman was peering over her shoulder!

### 6. In the 1960s, JPL set its sights on the moon, but electronic computers weren’t much help. 

The Explorer success cemented JPL's reputation. They became the go-to lab for space exploration.

When the US government decided the space program should be kept separate from the military, they signed the National Aeronautics and Space Act in 1958. The act created the National Aeronautics and Space Administration (NASA) and JPL became one of its primary research and development groups.

The women computers at JPL were particularly excited about plans to explore Venus and Mars. However, the rather less exotic Moon was viewed as the necessary first step. It was therefore set as the target for the Pioneer and Ranger programs.

Computers like Susan Finley were critical, plotting with pinpoint accuracy the trajectories and velocities needed to get lunar probes successfully into orbit.

At the same time, Paulson and Helen Chow spent hours of overtime calculating the trajectory of the Mariner probes that would be sent to Venus and Mars. They discovered there were only brief windows of time suitable for the ideal voyage. Only then would the alignment between the Sun, Earth, Mars and Venus be just right.

By the early 1960s, things had changed a little. Finally, the human computers had access to their electronic equivalents. But these machines were large. They were no longer room-size, as they had been in the 1950s — but, still, a machine like the IBM 1620 was as large as a desk.

Moreover, they were still slow when compared to talents such as Helen Chow. In fact, IBM's CADET, the Computer with Advanced Economic Technology, was given a new name to match its acronym: Can't Add, Doesn't Even Try.

Indeed, no machine was up to the challenge. They were too unpredictable and unreliable. They couldn't even compete in the games the team played to see who could calculate the fastest. In this, Chow, the undefeated champion, reigned supreme.

### 7. The Voyager and the Viking missions were highpoints for the computing team at JPL. 

Plotting a safe trajectory to Venus was hard. But it was nothing compared to the challenge of the Grand Tour. The plan was to send a probe through the entire solar system, using the planets' gravitational pulls — all while using as little fuel as possible.

The precise calculations required for that task were astronomical in scale.

Eventually, the Grand Tour was renamed the Voyager program. Sylvia Wallace and Sue Finley were tasked with calculating the trajectory and developing the computer programs for the task.

Around the same time, technology was becoming fast and reliable enough to be useful. It was therefore decided that the computer team take courses on coding and computer languages, such as FORTRAN and HAL, as they were in charge of these electronic machines.

Finley was also heavily involved in the creation of the Deep Space Network.

This was a large system of dish antennas set up at key spots on Earth. It allowed the mission-control centers at NASA to stay in contact with the probes when they were sent on increasingly longer journeys, like to Jupiter and Saturn.

Sue's work made Voyager possible. The women at JPL also helped write the computer code through which JPL could receive amazing images transmitted back by the probes.

And it wasn't just images. All sorts of data were streamed back, especially from the Viking mission to Mars.

Paulson played a crucial role in the Viking project, calculating the trajectory the Viking probe needed to take on its eleven-month journey. Her calculations were also needed for the critical final stages of the probe's entry into Mars's orbit. The probe was designed to separate into orbiter and lander sections at a precise distance from Mars; once detached, the lander would safely parachute down onto the planet's surface.

The mission was a success due in no small part to Paulson.

### 8. In the seventies, it wasn’t just computers that advanced at JPL. Feminism finally started to make an impact. 

Viking 1 reached Mars on June 19, 1976, helping engineers and scientists glean previously unimaginable knowledge about Mars. This initial mission paved the way for programs in later decades, like the Mars Pathfinder and the Mars Exploration Rover missions. These led to an even deeper understanding of the mysterious red planet.

But the 1970s saw advances beyond space missions. The women's liberation movement was also making significant contributions to gender equality.

Universities finally began accepting women to engineering programs. This would mean that later generations of women would be able to become engineers at JPL. Hitherto, talented women working at JPL had one option: become a computer. And though there was no shame in this, the position of engineer came with more prestige and authority. Women now had a choice, a privilege that had been denied pioneers like Helen Chow and Barbara Paulson

But that doesn't mean it was all progress and improvement. If a woman at JPL became pregnant, she was expected to quit her job; whether explicitly stated or not, all women, working whatever job, were expected to set everything aside in the case of a pregnancy. Women had to decide between their family and their career, and pregnant women were seen as a liability.

Women were forced to quit their jobs if they wanted children, even at JPL. Nonetheless, the culture there was still special: JPL was a diverse workplace, especially when compared to the general social climate in the United States in the 1960s. The company was particularly proud of its large and integrated female workforce. It was thanks to this culture that Helen Chow was able to rehire Barbara Lewis after she had had her child. The sisterhood was strong.

Eventually, things changed for the better. Maternity leave was introduced in the late 1960s (in the 1950s and early 1960s, there was essentially no such thing in the United States). Having a child no longer meant giving up a job.

Helen's role in these developments was critical. Throughout the 1970s, she hired female programmers in order to get them in the door at JPL. After that, she encouraged them to enroll in engineering classes and night school. She knew that once they had engineering degrees they'd be unstoppable.

### 9. The women at JPL are critical to NASA’s continued success. 

As we've seen, ever since the United States first turned its explorative gaze to the stars, the women at JPL have been there, making vital contributions. Though many have since retired, their contributions are still felt.

For instance, the Deep Space Network (DSN) that Sue Finley built and maintained remains the main line of communication for US space missions.

Meanwhile, many missions the world over have depended on Finley's expertise for their success.

Who could forget the French-Russian-US Vega mission, which sent delicate measuring equipment on a Russian rocket to the dark side of Venus in 1985?

It was a once-in-a-lifetime chance to get close to Halley's Comet and gather data on it. As Finley's male colleagues looked on, she manually entered commands to make the DSN's massive antennas and satellite dishes pivot and perfectly pick up signals from the sensitive equipment sent to Venus's atmosphere.

By the 1990s, most of JPL's early female team members and leaders, like Helen Chow and Barbara Paulson, had retired. But, as of 2015, Sue Finley was still working at JPL.

Finley wanted to put off retirement until she'd seen the successful conclusion of another project. This was the remarkable Juno mission, which involved sending out another probe, this time to orbit Jupiter. And, in July of 2016, the probe finally got there. It doesn't matter to Finley that it's the people in the control room that get most of the media recognition. She knows her worth.

Women like Sue never signed up for praise, but without her the US space program would never have been realized. Today, there are more women working at JPL than at any other NASA center. And it's all thanks to the trailblazing work of the women of previous generations.

### 10. Final summary 

The key message in this book:

**The women who worked at the Jet Propulsion Laboratory during its formative decades, from the 1950s to the 1980s, were a vital part of a team that revolutionized rockets and space exploration. Their calculations controlled the trajectories and plotted the courses of NASA's satellites, probes and exploratory rangers — a task similar to calculating how to shoot an arrow at a moving target from millions of miles away. In short, it was an almost superhuman feat. Now, in our more open-minded times, their accomplishments are finally getting the recognition they truly deserve.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Hidden Figures_** **by Margot Lee Shetterly**

_Hidden Figures_ (2016) reveals the untold story of the black female mathematicians who helped send John Glenn on his first orbit around the Earth and Neil Armstrong to the moon. These courageous, trailblazing women answered the call of duty by leaving their teaching jobs in segregated Southern schools behind and helping to shape the modern space program.
---

### Nathalia Holt

Nathalia Holt's work has been featured in the _New York Times_, the _Los Angeles Times_, _Popular Science_ and the _Atlantic_. She spent years speaking to the women of JPL and compiling their stories. She is also the author of _Cured: The People who Defeated HIV_.

