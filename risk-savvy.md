---
id: 54103bc536356200080f0100
slug: risk-savvy-en
published_date: 2014-09-09T00:00:00.000+00:00
author: Gerd Gigerenzer
title: Risk Savvy
subtitle: How To Make Good Decisions
main_color: 1D8C92
text_color: 187378
---

# Risk Savvy

_How To Make Good Decisions_

**Gerd Gigerenzer**

_Risk_ _Savvy_ is an exploration into the way we misunderstand risk and uncertainty, often at great expense to our health, finances and relationships. However, if we better understand risk, we can develop the tools necessary to navigate this highly complex world without having to become an "expert in everything."

---
### 1. What’s in it for me? Learn how to make the best possible decisions. 

All of us face countless decisions every single day: Do you jump out of bed at the sound of your alarm or hit the snooze button? Take the bus or drive to work? Get a loan to build a new house or stay in your old one?

No matter what the circumstances, you want to make the best choice possible. In order to do that, though, you need to have a good understanding of what's possible as well as the limits of what's knowable.

But figuring out the answers to these two simple questions opens up a whole new can of worms. How can you know what you don't know? And how can you make good decisions knowing that there's often great uncertainty regarding the consequences of your actions?

These blinks aim to add some clarity to the confusion by giving you the tools you need to make good decisions despite a lack of information. In addition, they'll show you how to get the most out of the limited information you have by helping you to ask the right questions.

In these blinks, you'll learn

  * how a simple rule of thumb saved the passengers of one airplane from imminent doom;

  * why testing positive for HIV isn't a death sentence; and

  * how to prevent your defiant teenage children from going off the deep end.

### 2. Risk and uncertainty are fundamentally different. 

What image comes to mind when you think about risk? Chances are good it's a high-stakes poker game in Vegas. Risk, however, is actually far more subtle.

In essence, a _risk_ is any situation in which you have many alternative outcomes, and are aware of them and the probability that they'll occur.

People often mistakenly think that situations are risky even when they're uncertain of the consequences. However, risk actually involves a great deal of certainty: you're not only aware of the possible outcomes but also of the probability of each outcome.

When you play a slot machine, there are only so many possible combinations the machine can generate. A quick look at the design of the machine can give you valuable information on the frequency with which each combination is generated, and therefore how probable each combination is.

Knowing this, you can play slots confident that you're at least aware of the dangers, since you can prepare for each possible outcome.

Sometimes, however, a situation has only one outcome. In this case, you don't face risk but _certainty_. For example, when you place your hand on a hot stove, you know for certain that you'll burn your hand.

This contrasts directly with _uncertainty_, i.e., not knowing the probabilities of all possible outcomes, or even not knowing what all the possible outcomes are _._

For many, this sounds much like their definition of "risk." For example, if you're at a poker table in Vegas, you probably don't know whether the player across the table has a pair of twos or a full house.

However, the variables in poker are knowable: since there are only so many cards in a deck, only so many possible combinations of cards are possible.

As a counterpoint, weather is often uncertain: tomorrow could be rainy, sunny, cloudy or even snowy — it depends on many factors, not all of which can be known.

### 3. People often mistake uncertainty for risk and risk for certainty. 

When faced with a difficult situation, people sometimes think they're taking a risk, but in actuality they're facing uncertainty.

This often happens when people mistakenly believe they've accounted for all possible outcomes of a given situation but have nevertheless missed something crucial.

Now, put on your make-believe hat for a moment: imagine you're a turkey. On your first day in the pen, there's a 50 percent chance you'll be fed. And after going to sleep every evening with a full stomach, it appears that the chances you'll be fed increase the next day.

On the hundredth day in the pen, it feels like it'll be a day like any other. But, surprise: it's Thanksgiving, and now _you're_ someone else's food!

If you'd imagined that the only outcomes each day were X (being fed) or Y (not being fed), then you might mistakenly have believed that risk was simply not knowing which will happen. However, there had actually been a hidden uncertainty all along because you'd never considered Z (Thanksgiving).

Moreover, people mistake risk for certainty. This happens when people falsely assume that there's one possible outcome for an event when there are in fact others. Therefore, they don't see the hidden (but knowable) risk in their decisions.

For example, if your HIV test comes back positive, you might think the outcome (being HIV positive) is _certain_, and thus become despondent and behave destructively.

However, there is another possibility: namely, that the test was wrong. This second alternative makes the HIV test a situation of risk, not of certainty.

Understanding the differences between risky, certain and uncertain situations is critical for good decision making, as you'll see in our following blinks.

> **Fact:  

** One out of every 26 positive HIV tests is wrong.

### 4. In order to make good decisions, we need to know how risks are calculated. 

If you truly understand a situation's risk, i.e., you know all the possible outcomes and their probabilities, you might think it's easy to choose your best option: all you have to do is pick the possibility with the best odds, right? Unfortunately, it's not that simple.

This has to do, in part, with the fact that risk can be calculated in more ways than one. In order to determine the probability of an outcome, you'll need to consider things like frequency, design and believability.

For example, if someone tells you that there's a 5 percent risk of a nuclear meltdown near your home, you'd want to know how they came up with that number. Maybe that 5 percent refers to the fact that there have been no previous meltdowns (frequency). But that 5 percent could easily change depending on the structural integrity of the plant (design) or whether other qualified experts make different assessments (believability).

It's important to consider how these statistics are derived. If you simply trust them blindly, you can easily be manipulated. You can avoid this by asking how these probabilities are calculated.

For instance, if journalists on your local news channel suddenly start reporting about the dangers of a nuclear meltdown near your home, ask where they obtained their information. If these journalists tell you that their calculations were based on the design of the nuclear power plant in your neighborhood, a 5 percent chance of a meltdown might not cause you to start packing.

However, if other experts chime in and say that it's only a matter of time until that power plant blows up, suddenly the perceived chance of a meltdown has jumped to 20 percent!

But before you sell your house to the first bidder, hoping to escape a nuclear explosion, consider how the risk probabilities got calculated and by whom. You might find that frightening probabilities reported on the news aren't all that well founded.

### 5. Simple rules of thumb can help you make decisions in the face of uncertainty. 

Sometimes, you have to make choices even when you can't be certain of their consequences. So how do you choose the best course of action? It's simpler than you think:

In situations when you're faced with high uncertainty, you can make fast, sound decisions by using a _rule_ _of_ _thumb_, or a simple rule that only considers one piece of information and can be used without conscious effort.

When both engines of US Airways Flight 1549 failed shortly after takeoff, for example, captain Chelsey Sullenberger and co-pilot Jeffrey Skiles didn't have time to page through the three-page dual-engine failure checklist. At only 3,000 feet off the ground and falling fast, they needed to make a quick decision.

They decided to use a rule of thumb known to all pilots: if you can't see a landing site through your windshield, you won't be able to reach it. Knowing they couldn't reach the airport, they made a successful emergency landing on the Hudson River, saving all passengers on board.

Sometimes, in situations of uncertainty, rules of thumb can be even more trustworthy than complicated solutions based on complex information. Indeed, if you mistakenly believe that you know all possible outcomes, your decision will be unable to accommodate the possibilities outside your knowledge.

For example, one rule of thumb in asset allocation is known as the 1/N rule, which says you should allocate money equally in each of N investments. So, if you want to invest $10,000 in real estate and have two properties, then you put $5,000 into each of them.

While it may seem overly simple, the head of investment at a large insurance company confided to the author that, after studying their investments from 1969 to 2014, they would have made more money if they'd used the 1/N rule instead of their more complex approach.

Due to the inherent unpredictability of the stock market, rules of thumb, while simple, can be quite reliable.

### 6. Sometimes you have to accept that there’s no best option – so just trust your gut. 

As we've seen, you can use rules of thumb to make sound decisions even in the case of uncertainty. But what if you're stuck between a rock and a hard place where there simply is _no_ best decision?

In order to choose the best of many options, you first have to be aware of all possibilities. But sometimes it's _impossible_ to know all the possibilities. Instead, try to pick one that's "good enough," knowing full well that it's not necessarily the best.

The author cites one friend who admitted that he chose to marry his wife over other women after estimating the probability that she would support his work, care for his children, etc. But he didn't consider that he couldn't know all the possibilities, i.e., that his wife would suddenly want to work full time instead of staying at home with the children. Eventually, he got divorced.

Rather than looking for the "best" option, he should have instead tried to find a partner that was "good enough" by focusing on what was most important to him (e.g., parenting), and then taken the first candidate that fit the bill. It might have saved him a divorce.

In addition to looking for what's "good enough," you should also place more trust in your _gut_ _feeling_ : that quick judgment we can't explain but is so strong that we act upon it anyway.

Decisions based on your gut feeling are good when you don't have enough information to make an informed decision. Your gut feelings are a form of unconscious intelligence, so while you might not be aware as to why you have this particular gut feeling, you can draw confidence from the fact that it comes from your personal experience, and is therefore probably right.

### 7. Mistakes are nothing to fear – sometimes they can even bring you great rewards. 

But no matter how varied and airtight our decision-making strategies are, people make mistakes. Still, there's no reason to fret: mistakes are just as much a part of the decision-making process as logic and probability.

In fact, mistakes are important because they always teach you something. Indeed, many scientific discoveries were stumbled upon by mistake. Remember Christopher Columbus? He wanted to find a new route to India, but his entire expedition was based on entirely false assumptions about the diameter of the earth.

However, these mistakes led to the discovery of what was then considered the New World.

What's more, mistakes help you better yourself by drawing your attention to your weaknesses.

Take professor of surgery Matthias Rothmund, who once mistakenly forgot a clamp in a patient's body during surgery. As soon as he found out, he informed the patient as well as his insurance. Not only did he correct his mistake, but he also invented measures to ensure that it wouldn't happen again by introducing a policy at his clinic of counting the instruments after surgery.

Indeed, mistakes, while sometimes painful, can bring with them great benefits. In contrast, being afraid of making mistakes can lead to poor decisions.

When you fear mistakes, you make decisions that are safe, but not necessarily right.

One study on doctors, for instance, revealed that 93 percent of them practice defensive medicine, meaning that they order tests, medications and invasive procedures that are not immediately clinically necessary. Considering the damage that a CT scan or even a simple x-ray does to our bodies, we can only ask: Why?

Because doctors fear they might be sued for malpractice if they make a mistake. To avoid that situation, they perform every possible test they can think of, even if it's bad for the patient in the long run.

So far, you've learned about how we misunderstand risk and what we can do to make better decisions. Our next blinks will show how you can use that knowledge to improve society.

> _"Learn by failing, or you fail to learn."_

### 8. Risks are often presented in a misleading way that causes unnecessary panic. 

As we've seen, the way that we calculate risks can change how we perceive them. Equally important to our perception of risk is _how_ that information is conveyed.

In medicine, risk can be presented in terms of relative risk, which is measured in relation to previously known facts, or absolute risk, which describes the probability of a medical event. Choosing one presentation over another can entirely change our perception of an event.

For example, after studying the side effects of third-generation contraceptive pills, the UK Committee on Safety of Medicines warned that these pills increase the risk of thrombosis by 100 percent.

In the study, two out of 7,000 women who took the third-generation pill developed a thrombosis, compared to only one out of 7,000 of those taking the second-generation pill.

So yes, when compared to the second-generation pill, the _relative_ _risk_ of thrombosis increased with the third-generation pill by 100 percent. However, more importantly, the _absolute_ _risk_ only increased by one in 7,000.

Unsurprisingly, the way this information was presented caused a panic: because women were now afraid to take the pill, there were around 13,000 more abortions in the year after the committee issued its warning.

However, this might not have been the case if newspapers reported the absolute risk (one in 7,000) instead of the relative risk (100 percent increase).

One way to get around this is to avoid probabilities altogether when presenting statistical information. For example, medical professionals could instead opt for _natural_ _frequencies_, i.e., the use the actual numbers from the raw data.

For example, mammography screenings for breast cancer seem absolutely reliable when presented in terms of probabilities: women without cancer test negative 91 percent of the time.

However, when we look at natural frequencies, we discover that only 9 out of 98 women diagnosed with cancer truly have the disease, while the other 89 are actually tested false positive. In other words, the majority of positive tests are actually wrong.

> _"Just how big is 100 percent?"_

### 9. With the right information and approach, you can be risk savvy. 

Becoming _risk_ _savvy_ isn't something that only the best and brightest of us can achieve. In fact, anyone can understand risk if the information is presented properly and if they ask the right questions.

As we've seen, understanding risk isn't about having all the information — it's about understanding that you don't have it all. In fact, having all the information you need to make a completely informed decision is generally impossible.

But just because you can't be an expert in every area of life doesn't mean you have to blindly trust the opinions of experts. As long as you understand when a situation is uncertain and not just risky, you can still make good decisions by using the right strategies.

The world of finance is a good example: you don't need a degree in economics to understand that it's a highly uncertain arena. That said, you can use simple rules of thumb to stay ahead, e.g., not taking out loans you can't pay back.

Furthermore, people are smart enough to understand risks without help, as long as the risks are presented to them in a clear and simple way.

One way the author suggests we can accomplish this is by using fact boxes that introduce relevant information in an easily understandable way.

For instance, a fact box about prostate cancer detection tests should answer questions like: How many study participants were there? How many died from cancer and other causes? And how many tests came back as false positives?

The bottom line is: we don't need paternalistic slogans that encourage us to take the test. We're completely capable of making those decisions on our own as long as we're presented with the information in the right way.

### 10. We have an obligation to teach children how to manage risk. 

If we want our society to be _risk_ _savvy_, we can never start too early. Teaching children about risk is the best way to ensure that they'll make good decisions.

Just like with adults, getting children to understand risk comes down to presenting them with the right information in the right way.

For example, if you want to explain the dangers of smoking to your children, how do you deliver that information in a way that will resonate with them? Just like with adults, it's better to present that data in terms of natural frequencies.

In fact, kids are quite talented at understanding natural frequencies. In one experiment, fourth graders who hadn't yet learned about proportions were asked to solve problems based on natural frequencies. Nearly all of them — 96 percent — were able to solve at least the easiest problem.

As we can see, children can understand risk. What's more, educating kids on how to evaluate risk and uncertainty will result in a better society.

In fact, many of our everyday problems can be solved with good understanding of risk.

In order to impart this understanding, the author suggests establishing risk literacy curriculums centered around health literacy, financial literacy and digital risk competence, i.e., understanding the risks brought by technology. In addition, children should be taught statistical thinking, rules of thumb and risk psychology to master these topics in order to help them make the best possible decisions.

Just imagine how much easier your life would be as a parent if you knew that your children were getting the tools they need to make healthy decisions. Rather than fret over whether your pubescent teen is out drinking or doing drugs, you could be confident that they're capable of making the right decisions because they've been presented with information in a way that is meaningful to them.

### 11. Final summary 

The key message in this book:

**In** **a** **world** **of** **uncertainty,** **logical** **thinking** **is** **not** **enough** **to** **ensure** **that** **your** **decisions** **actually** **represent** **your** **best** **interest.** **Instead,** **you** **should** **make** **an** **attempt** **to** **recognize** **your** **ignorance** **and** **find** **good,** **simple** **rules** **of** **thumb** **in** **order** **to** **make** **sound** **decisions** **in** **spite** **of** **complex** **circumstances.**

**Suggested** **further** **reading:** **_The_** **_Black_** **_Swan_** **by** **Nassim** **Nicholas** **Taleb**

_The_ _Black_ _Swan_ offers insights into perceived randomness and the limitations we face in making predictions. Our over-reliance on methods that appeal to our intuition at the expense of accuracy, our basic inability to understand and define randomness, and even our biology itself all contribute to poor decision making, and sometimes to "Black Swans" — events thought to be impossible that redefine our understanding of the world.
---

### Gerd Gigerenzer

Gerd Gigerenzer is a psychologist and director of the Center for Adaptive Behavior and Cognition at the Max Planck Institute and director of the Harding Center for Risk Literacy, both of which are in Germany. In addition, he has authored several books on decision making such as _Reckoning_ _with_ _Risk:_ _Learning_ _to_ _Live_ _with_ _Uncertainty_ and _Gut_ _Feelings:_ _The_ _Intelligence_ _of_ _the_ _Unconscious_.

