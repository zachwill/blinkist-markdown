---
id: 56f3b87e2bf5b9000700003b
slug: rocket-en
published_date: 2016-03-29T00:00:00.000+00:00
author: Michael J. Silverstein, Dylan Bolden, Rune Jacobsen, Rohan Sajdeh
title: Rocket
subtitle: Eight Lessons to Secure Infinite Growth
main_color: B3BA39
text_color: 6A6E22
---

# Rocket

_Eight Lessons to Secure Infinite Growth_

**Michael J. Silverstein, Dylan Bolden, Rune Jacobsen, Rohan Sajdeh**

_Rocket_ (2015) is an inside look at the success of brands like Starbucks and Victoria's Secret, whose rapid rise had nothing to do with luck. These blinks share the proven science of brand building that propelled companies like these to such impressive growth and immense success.

---
### 1. What’s in it for me? Fire your company to the stars. 

For most companies, the road to success is paved with hard work over the course of years and years, taking one small step at a time. But then there are some companies — rocket companies — that take off unexpectedly to achieve massive success in a way that seems to defy all logic and expectations.

So what's their secret?

Well, at the heart of many of these companies' success is their relationship with their customers; you should never underestimate the power of an engaged, loyal customer to spread the gospel of your brand. 

And while this is a core aspect of many companies' overall strategies, it is just one of the many lessons to be learned from some of the most successful companies in the world today.

In these blinks, you'll learn

  * how Les Wexner revolutionized women's underwear;

  * what 2/20/80 means; and

  * how to turn a customer into an apostle.

### 2. Sometimes you’ve got to tell consumers what they want. 

What do successful brands have in common?

They know that consumers aren't interested in abstract concepts like performance statistics and technical specifications; customers want to see and touch products that they can relate to in an instant. To accomplish this, brands need to create products that aren't just innovative, but also intuitive. 

In fact, you can open your customers' eyes to a new world by simply forming a new market. Just take the serial entrepreneur Les Wexner, who, in the 1980s, identified the huge gap between the luxury and budget markets for women's undergarments. 

Wexner noticed that although women wanted to be sexy, they couldn't do so without breaking the bank. It was this realization that led him to found Victoria's Secret, a brand that endeavored to forge a new market for glamorous women's underwear that was neither too cheap nor over-the-top luxurious. 

By forging a middle ground in the market, he enabled countless women to rediscover underwear as lingerie — and the business results were incredible. 

But even if you have a stellar brand and a lucrative market, you should never be satisfied; it's essential to continually reappraise your market and reinvent your brand. For instance, when Victoria's Secret's annual revenue stabilized at $2 billion, Wexner went looking for ways to rebrand the company. 

His search led him to hire the authors, who conducted 100 interviews with young women to gather data on their relationships to the brand. 

The results?

Most of the women were only wearing Victoria's Secret products on the weekend and would rather sacrifice glamour for comfort during the week. So, armed with this information, Wexner developed models that were both sexy and comfortable. Helped along by this change, the company's annual revenue is now over $8 billion!

### 3. The more loyal a customer, the more revenue they generate. 

Did you know that the most loyal customers to a brand can generate up to eight times more revenue than their personal purchases alone?

The more loyal a customer, the more they pitch your brand to others. These are your _apostle consumers_ and they're worth more than anything else. So, while these dedicated consumers will return time and again to buy your products, they'll also spread the word about your company. 

This is what makes apostle consumers essential, and the authors have explained their real worth through what they call the _2/20/80_ rule. Here's how it works:

Say your apostle consumers comprise two percent of all your consumers. Since they always return to your brand, they actually make up 20 percent of all revenue and, more importantly, their recommendations can produce up to 80 percent of your remaining sales. 

Apostle consumers are a fundamentally important aspect of your business, which is why it's essential to reward them every time they engage with your brand. For instance, the health food store giant Whole Foods works hard to make its apostle consumers feel special every time they enter the store. 

To do so, the company spends huge amounts of money and energy to make sure that every visit to a Whole Foods is a memorable experience that leaves customers hungry for more — sometimes literally. 

The store often hands out complimentary samples of expensive products like wine and shrimp. In fact, many of the stores maintain a space for their customers to meet each other and enjoy free classes on cooking and health. 

One of Whole Foods' apostle consumer said that each trip to the store taught him more about nutrition and health. He even gives the company credit for helping him improve his quality of life. So, if a friend told you Whole Foods changed his life, wouldn't you want to pay the store a visit?

### 4. Constantly re-evaluate the needs of your core consumers. 

When a company is faced with impending failure, management often looks back to past decisions that produced previous successes. But as a rocket company, to go backward you first have to learn what your customers want _now_. 

It's crucial to constantly take stock of what your core customers need. If your business starts to slow down, it might be time to rethink your brand's _demand spaces_, a category that helps you determine the unique needs of your customers and the different moments or _spaces_ in which they occur. 

When the food manufacturer Frito-Lay came up against a revenue wall, they used demand spaces to determine which snacks their customers wanted, when they craved them and why. For instance, the company analyzed ways in which different customer groups consumed snacks on different occasions, whether it was by themselves, with others or at certain times. 

Then, they considered how they could fill these demand spaces in the market while accounting for the strengths and weaknesses of their competitors. Finally, they evaluated each opportunity and the commercial rewards made available by every demand space. 

But identifying your demand spaces is just the beginning. In fact, doing so is useless without strong and effective implementation. So, once Frito-Lay finished their first round of demand space analysis, they began bringing products to market. 

For example, their analysis had identified one untapped demand space that was termed "Fun Time Together" — that is, the foods consumers eat while spending time with friends. Part of the implementation for this demand space was the release of a chips and dip combination that was marketed as a snack to be enjoyed with others. 

And guess what? It worked like a charm. The product's first year saw over $100 million in sales. 

So, running with this success, the company continues to delve deeper into the desires of consumers and the untapped demand spaces they can fill. To stay on top of this program, they conduct just this kind of research and implementation every year.

### 5. First impressions are crucial and every aspect of your brand should be aesthetically pleasing. 

Have you ever considered the sensory impression that consumers get when engaging with your brand?

Well, if the answer is anything remotely ugly or unappealing, it's time to reconsider your brand's aesthetics. After all, rocket companies know that brands need to appeal to every sense. 

Just take the Italian artisanal clothing manufacturer Cucinelli. They stick to the principle of great aesthetics and it has helped them build a $1-billion international business. Every item Cucinelli produces is made from high-quality, distinctive fabrics that are recognizable without any obvious branding. As a result, one apostle consumer says that converting ten of her friends to the brand simply took a single touch of her Cucinelli cashmere sweater. 

But the brand's backstory also touches on the emotions of potential customers. Cucinelli paints its founder, Brunello, as an artisanal craftsman who supports his tiny Italian village while treating his employees like family. All of these factors combine to produce a compelling and long-lasting positive first impression of the company's brand. 

So, a beautiful brand is essential and it's important to accept that having one is going to cost a pretty penny. As visually pioneering companies such as Disney consistently prove, there's no such thing as spending too much on aesthetics. 

For example, the attention to detail Disney invests in their different theme parks costs a fortune, with new rides often costing around $200 million. Not only that, but when designing new areas in their parks, they enlist the most talented artists and architects the industry has to offer. In the process, they may well spend upwards of $1 billion for projects that take five years or longer to complete. 

But all this investment in aesthetic detail is worth it, since it plays a huge role in Disney theme parks that produce yearly profits of over $1 billion!

### 6. Passionate employees make for satisfied customers. 

Your employees are the first people your customers interact with, so be sure to have only the best people on your team. Remember, every new hire is meaningful because new employees are the future of your brand. 

In the same way that good wine doesn't start with bad grapes, passionate employees need to come prepared with the right attitude on day one. 

Just take the online shoe store Zappos. The company's rigorous hiring process identifies a specific type of person who embodies the company's values in everything they do. To accomplish this, every job offer is the result of a democratic process involving every person who has interacted with the potential employee. 

For instance, if a potential hire makes it to the campus interview stage of the process, even the driver who gives them a lift from the airport has a say in the hiring process. And if that driver or anyone else in the process has anything bad to say, the hire is a no-go. 

But building a stellar team is just the first step. Once you've got one, it's important to care for your employees. In the end, the better you are to them, the better they'll be to your customers. In fact, a golden rule of customer service is to treat your employees like you want them to treat your customers. 

For instance, the Four Seasons hotel pays every employee above the industry average and offers them and their families free stays at the company's hotels worldwide. As a result, the employees treat their customers fantastically. 

So, during the horrific Indian Ocean earthquake of 2004, the Maldives Four Seasons became famous for the way it cared for its frightened guests. Every one of the hotel's 400 employees assisted the guests in reaching safety and reserved for the guests what little food and water remained. But what's most incredible is that not a single employee left their post throughout the entire ordeal.

### 7. Excellent virtual relationships with your customers are vital in the digital age. 

Did you know that 83 percent of Americans say they would rather give up fast food than go without internet access? It's true, and it's because the internet offers consumers instant gratification at the mere click of a mouse. 

As a result, your brand needs to pay serious attention to its internet presence. The digital world has paved new avenues for understanding and connecting with its customers, and it's imperative to take advantage of the rise of big data analysis. 

After all, the ability to gather huge quantities of information from every virtual interaction gives you a detailed look at what customers want. Just take the online retailer Amazon, whose success is founded on its effective use of big data.

The company analyzes every action their customers take and uses the information to build relationships with them. What's particularly effective is the company's ability to predict a customer's next purchase. What's more, this relationship gets stronger every time a customer returns to the site to find new recommendations based on their interests. 

Loads of Amazon users are converted to apostle consumers as a result of this extremely convenient shopping experience. These loyal consumers spend an average of $2,873 per year and bring in more than twice that amount by recommending the site to others. 

So, virtual relationships are important. In fact, such connections can be even more special than their "traditional" counterparts. Just look at Airbnb, a site that forges virtual relationships that often lead to users meeting and sharing real-life experiences. 

The site enables this possibility through its excellent review and identity verification protocols that serve to build trusting relationships between strangers. Not only that, but the company is also extremely financially successful, with a 2014 valuation of $10 billion. And it has all been made possible by the powerful virtual relationships at the core of Airbnb's business model.

### 8. Final summary 

The key message in this book:

**Every successful brand on earth has followed common strategies to get to where they are — and so can you. By identifying and maintaining your core customers, and by putting the finishing touches on your brand's killer aesthetic, you'll have all the tools in place to craft a stellar brand.**

Actionable advice:

**Understand your customers better with Toyota's** ** _complaint as a gift_** **strategy.**

Every interaction with a customer is important and careful data should be kept on all your customer engagements. This implies that customer complaints are actually a blessing in disguise, as they let you improve your brand. So, from the frontline sales team to your CEO, every employee should actively respond to customer suggestions, even if just through a simple thank-you e-mail. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Design To Grow_** **by Davis Butler & Linda Tischler**

_Design to Grow_ (2015) reveals how iconic brand Coca-Cola went from a small local soda company to one of the biggest and most innovative companies in the world. With these blinks, you'll be able to ensure your company is both agile and scalable, no matter how big your endeavor is.
---

### Michael J. Silverstein, Dylan Bolden, Rune Jacobsen, Rohan Sajdeh

Michael J. Silverstein, Dylan Bolden, Rune Jacobsen and Rohan Sajdeh are senior partners and managing directors at The Boston Consulting Group. Silverstein is the best-selling author of _Trading Up_, _The $10 Trillion Prize_, _Women Want More_ and _Treasure Hunt_.

