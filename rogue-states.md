---
id: 5101738ae4b0991857b92614
slug: rogue-states-en
published_date: 2013-10-10T10:00:28.540+00:00
author: Noam Chomsky
title: Rogue States
subtitle: The Rule of Force in World Affairs
main_color: 834394
text_color: 834394
---

# Rogue States

_The Rule of Force in World Affairs_

**Noam Chomsky**

In _Rogue States,_ Noam Chomsky holds a critical lens to the nature of state capitalism and to American Foreign Policy, providing an alternative view to the one proposed by government rhetoric and mainstream media.

---
### 1. The term ‘rogue state’ was invented by the United States to eliminate opposition and maintain control globally. 

By objective definition, a 'rogue state' is a country that unilaterally violates international laws and norms, and in doing so poses a serious risk to international peace and security.

In the post-Cold War, pre-9/11 era, US foreign policy and planning largely centered on targeting 'rogue states.' However, the list of states that qualified as 'rogue' suggests that this concept is not as clear-cut — and its purpose not as noble — as would first seem.

Consider the state of Cuba, which has consistently been defined and treated as a rogue by the United States for its alleged involvement in international terrorism. This is despite the fact that Cuba has not been directly linked to any such offense in well over a decade.

On the other hand, Indonesian dictator General Suharto, who oversaw the massacre of at least 100,000 inhabitants of East Timor, has not only been exempt from the list but also received implicit US support for many years.

The difference between the two? Cuba has vehemently rejected US economic and political orthodoxy, while Indonesia willingly supported their international objectives.

This selective adherence to the definition of 'rogue' demonstrates that, in a period exempt from clearly defined enemies such as 'communists' or 'terrorists,' 'rogue state' acts as a murky, catch-all term to target any state not willing to facilitate US political and economic interests. The extent of US power and influence results in almost complete international isolation for states that acquire the label, making it an extremely powerful tool at the hands of the global superpower. 

**The term 'rogue state' was invented by the United States to eliminate opposition and maintain control globally.**

### 2. The United States is itself a ‘rogue state’ – its position of power has been created and maintained through force. 

Throughout history, superpowers have created and maintained their position through force. However, following the destruction of World War II, the 1945 UN Charter outlawed the use of force except for self-defense. The charter was a historic commitment to the idea that sovereignty is a cornerstone of world peace and should be universally respected. The United States, however, has opted to consistently violate the charter and instead follow the example set by its superpower predecessors; it uses the rule of force in pursuit of power. 

In recent years, the US military has directly intervened in Vietnam, Iraq and Kosovo, while also sponsoring brutal state violence in countries such as Indonesia, Turkey, Columbia, Croatia, and, before they changed their minds, Iraq. Whether the United States is pointing the weapons or just paying for them, the drastic human effects remain the same. 

The goal is usually the same: to suppress and control threats to US political and economic supremacy. Forceful intervention has allowed the United States to install and support governments that respond favorably to their ambitions. For example, Guatemala's democratically elected government was overthrown by the United States in 1954 and replaced by a brutally repressive dictator. The Guatemalan government's crime? They supported labor organizations and agrarian reform that conflicted with the interests of large US corporations such as United Fruit. Since then, the United States has maintained strong support for the new regime despite the severe atrocities it has committed.

In fact, countless examples of such international aggression actually qualify the United States, by their own definition, as the most hostile rogue state in the world.

**The United States is itself a 'rogue state' — its position of power has been created and maintained through force.**

### 3. Economic objectives drive US foreign policy and foreign interventions. 

Contrary to media and official rhetoric, the United States' interventions abroad are not driven by moral superiority. The United States seems to feel obliged to intervene in certain conflicts, whereas others are completely disregarded. This discrepancy begs the question, what really drives intervention? The answer, inevitably, is economic interest.

Since the end of the Cold War, the United States' government has focused on opening global markets and implementing wider neoliberal reforms abroad. This _Washington Consensus_ is promoted in order to create optimum conditions for large US corporations to generate profit.

Adherence to the Washington Consensus requires developing countries to open their markets totally and implement harsh cuts in public and social spending. This means it is the world's poorest that shoulder the greatest burden while the richest reap the benefits.

This overwhelming concern with economic reform has lead the United States to support brutal and murderous regimes in countries such as Indonesia, Columbia, Guatemala and Zimbabwe, to name but a few. In each instance, 'moral obligations' only emerge when a brutal regime stops falling in line with US economic demands.

Conversely, states that pose a threat to neoliberal globalization are invariably punished. For example, despite an ostensible lack of any terrorist activity for many years, Cuba has been subject to some of the harshest sanctions in history for its refusal to fall in line with Western economic ideals.

**Economic objectives drive US foreign policy and foreign interventions.**

### 4. To maintain legitimacy, the United States touts itself as a humanitarian savior. 

Official Western rhetoric describes the post-Cold War era as a glorious US-led new age in which freedom, democracy and human rights prevail. This emotional rhetoric works as a powerful tool in legitimizing US foreign policy.

However, when US foreign policy is considered as a whole, it becomes clear that any commitment to morality is plain rhetoric.

Consider the 'drug wars' enacted in Latin America, for example. Official rhetoric paints intervention as a noble deed to protect defenseless citizens from drug traffickers. In reality, successive US governments have merely enacted fruitless programs of crop destruction targeting impoverished peasants to maintain the 'drug war' façade. At the same time, the United States has ignored or even supported the repressive and violent regimes truly in control of drug trafficking throughout Latin America in return for their compliance with neoliberal structural adjustments.

In reality, US policy and planning have changed very little since the Cold War. Rhetoric claiming protection from Communism has been replaced with rhetoric claiming 'humanitarian intervention', but the ultimate goal of US self-preservation remains supreme. This is demonstrated by the lack of any 'humanitarian' interest in violent conflicts such as Sierra Leone or Angola where no US interests were at risk. Moral principles are simply not what drive US foreign policy planning.

**To maintain legitimacy, the United States touts itself as a humanitarian savior.**

### 5. The mainstream media is instrumental to the United States’ power. 

The mainstream news media act as an important link between the government and public. The way the media report and frame foreign and domestic policy decisions determine how the public perceives them.

In order for the US government to maintain the public support for their wholly self-interested foreign policy, they must maintain the façade domestically and internationally that their actions stem from strong morality and an obligation to protect the world. While government rhetoric and propaganda play a key role in this, the media hold even greater weight as an 'objective' commentator.

Consider the illegal and unsuccessful bombing of Kosovo by NATO troops in 1999, led by the United States, which served only to aggravate the existing situation. The attack was covered widely in the media and described by the _New York Times_ very favorably as 'a democratic West and its humanitarian instincts repelled by barbarous inhumanity of orthodox Serbs.'

Conversely, the media can also influence public perception through omission: the US-funded bombing of East Timor by Indonesia in 1975 was barely reported at all.

Mass support for US power is maintained through such positive framing and selective reporting.

This positive spin is largely facilitated by the fact that the media is controlled by a small number of mega corporations that manipulate news reporting for their own ends. This is because the companies' interests converge with those of the US government, as both wish to maintain a stable political and economic climate domestically as well as promote free market capitalism worldwide.

**The mainstream media is instrumental to the United States' power.**

### 6. The United States disregards or invokes international law at will. 

In theory, international law is meant to play much the same role in the international arena as domestic law does in the domestic arena. Largely formulated after the Second World War, international law is a tool to punish states that act unilaterally to disrupt peace and security.

However, as the sole global superpower, the United States frequently chooses to ignore international law when its confines prove inconvenient for foreign policy goals. In the wake of the illegal US-backed Indonesian invasion of East Timor, the United States UN ambassador shed light on the United States' disdain of the United Nations: 'The Department of State desired that the United Nations prove utterly ineffective... I carried this forward with no inconsiderable success.'

The same disregard for legality can be observed in the bombing of Kosovo by US-led NATO forces; it was carried out despite being deemed illegal by the United Nations. These are but a few examples in a long and extensive history where various US administrations have ignored international legal obligations.

Although the United States frequently disregards international law, it does invoke it in its international dealings when convenient. Harsh sanctions against states such as Cuba and Iran are frequently justified through their supposed violations of international law. The United States' apparent commitment to and respect for international law is a useful way to justify and gain support for aggressive foreign policy.

The ability to selectively adhere to and apply international law starkly illustrates the magnitude of US power and its consequence for other countries.

**The United States disregards or invokes international law at will.**

### 7. The current global order benefits the few at the cost of the majority of the world. 

The ruthless pursuit of power and financial gain by successive US governments are not without human consequence: the losers are many and the benefactors few.

Through direct illegal military interventions and indirect support for brutal regimes, the United States has — in pursuing its own ends — violated the basic human rights of countless innocent people. Furthermore, many countries that are desperately in need of support but cannot contribute to US interests are ignored. In addition, the vast majority of developing countries, desperate for income, are burdened with 'odious' debt originating from Washington. This debt carries conditions that solely serve US interests as opposed to those of the debtor states, ultimately making their development intricately tied to the United States.

The systematic disregard for human rights by the very country that has the ability to make them universal is tragic. Countless lives have been lost or destroyed, while the hope that all human beings could one day hold unqualified claim to even their most basic needs has dwindled.

While the United States has proved that its disregard for human rights is universal, the nature of the global capitalist financial system imposed and promoted by Washington weighs disproportionately heavily on the workers and the poor. This is apparent both abroad and at home: Countries with strong labor laws and unions meet great financial penalties such as disinvestment or high trade tariffs from US-controlled financial institutions like the IMF; meanwhile in the United States, unions have lost much of their power, and workers' wages today are lower than in the 1970s.

**The current global order benefits the few at the cost of the majority of the world.**

### 8. It is market forces that truly drive and control the current world order. 

The post-Cold War era has been characterized by almost complete neoliberal globalization, under which markets are free to run with little intervention from governments. The unregulated nature of this economic system, along with the sheer power wielded by those with money, has created a world in which markets and large corporations reign supreme and dictate the political and economic landscape for the majority of the world.

The problem is that large corporations are by no means democratic institutions, and markets have little interest outside of financial growth and capital gain. As such, the current world order is one in which the small number of individuals leading those companies, largely based in one country and interested solely in their own ends, dictate the fate of the global majority. Freedom, democracy, sovereignty and human rights are of little concern in a world ruled by market forces.

As markets essentially play the part of governments, the role of the state is greatly reduced, although not rendered totally redundant. States are still essential in creating and protecting industry and intervening to overcome management failures, and in turn draw power from economic relationships.

Hence, a mutually beneficial relationship exists between autonomous market forces and the United States' government. US international policy is an inevitable byproduct of this relationship.

**It is market forces that truly drive and control the current world order.**

### 9. Final summary 

The key message in this book:

**In order to impose its political and economic will on the world, the United States labels countries that oppose it as 'rogue states,' which deserve to be punished by the international community. The United States' interests are solely self-serving, as they attempt to impose neoliberal capitalism on the rest of the world with little regard for morality or international law.**

The questions this book answered:

**What is the United States' real position and aim in international politics?**

  * The term 'rogue state' was invented by the United States to eliminate opposition and maintain control globally.

  * The United States is itself a 'rogue state' — its position of power has been created and maintained through force.

  * Economic objectives drive US foreign policy and foreign interventions.

**How and why is the true nature of international politics being misrepresented?**

  * To maintain legitimacy, the United States touts itself as a humanitarian savior.

  * The mainstream media is instrumental to the United States' power.

  * The United States disregards or invokes international law at will.

**Who are the winners and who are the losers in today's international politics?**

  * The current global order benefits the few at the cost of the majority of the world.

  * It is market forces that truly drive and control the current world order.
---

### Noam Chomsky

Noam Chomsky is a world-famous linguist, philosopher, historian and radical political critic. His popular writing focuses primarily on the subjects of war, politics and the mass media.

