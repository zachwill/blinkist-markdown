---
id: 54bd2d323465370009780000
slug: rookie-smarts-en
published_date: 2015-01-23T00:00:00.000+00:00
author: Liz Wiseman
title: Rookie Smarts
subtitle: Why Learning Beats Knowing in the New Game of Work
main_color: 2368AD
text_color: 2368AD
---

# Rookie Smarts

_Why Learning Beats Knowing in the New Game of Work_

**Liz Wiseman**

The world is changing faster than ever. Rookies and inexperienced employees have an advantage in the workplace when they are willing to learn and adapt. _Rookie Smarts_ shows how, when projects are approached with fresh eyes, organizations succeed. It also includes helpful tips on how even experienced workers can stay sharp by developing a rookie mindset.

---
### 1. What’s in it for me? Discover why, in the modern world, being experienced is not all it’s cracked up to be. 

When you write your resume, what aspect of yourself do you highlight? Maybe you make sure your educational background looks solid; maybe you make a strong personal statement. However, you probably place the most emphasis on showing how experienced you are.

We believe that, while youthful passion will take us so far, employees are looking for, above all else, the know how of time spent on the job. And, to be fair, for a long time this was true, but not now.

In the modern world, experience is no longer what we need. Instead, we need the drive, enthusiasm and flexibility found in rookies. These blinks show you how and why business of the future will be driven by the rookies.

In these blinks, you'll discover

  * how after one world-breaking runner's record led to several more;

  * how we can become a perpetual rookie; and

  * why one successful film director keeps reading the same book over and over again.

### 2. In today’s fast-paced workplace, hiring a rookie can be an advantage. 

Next time you're faced with a stack of resumes, look beyond experience. The strongest candidates might be _rookies_, or someone without experience. Why? Because technology has changed the modern workplace in three distinctive ways that work in their favor.

First, our knowledge about the world is increasing faster than ever. In fact, every year and a half or so, human knowledge doubles.

Second, because of constantly improving research, new findings are rapidly proving old ones false. Therefore, mastery of old knowledge is becoming less and less relevant.

Take for example, a 2005 study which showed that the annual _knowledge relevance decay rate_ —or how much knowledge becomes irrelevant each year—is 15 percent overall and up to 30 percent for high tech knowledge. So if workers aren't continuously learning, only about 15 percent of what they know may be up-to-date in five years.

The final change is that technological advancements have expanded the workplace beyond the 9-to-5 day. Now, it's standard practice to get an email from the boss at 9PM and be expected to respond.

Due to these developments, experienced workers can struggle in this new environment. Many find it difficult to keep asking questions and learning when they've already accumulated a base of knowledge earlier in their careers. In addition, it's harder for them to look for new solutions when they already have an old method that has worked in the past.

On the other hand, rookies aren't yet set in their ways, so they're predisposed to deal well with change. They are willing and eager to learn and try new solutions. In the modern workplace, these traits are what translates into success.

Because whether it's day one on the job or day 1,000, the enemy of success is cutting ourselves off from new information and approaches.

What makes rookies so valuable? Read on to find out how the rookie mindset leads to success and how everyone can have it.

> Fact: The world's knowledge doubles every 18 months.

### 3. Rookies don’t cling to the status quo, enabling them to innovate and outperform experienced workers. 

It's a common way of thinking: if it worked in the past, it'll work today. However, history has proven that the old ways aren't always the best.

So who can see new possibilities and improvements? Rookies can, because they aren't bound by the status quo.

Take, for example, runner Gunder Hägg. In 1945, Hägg beat the world record speed for running a mile, a record that had seemed unbreakable. A medical student named Roger Bannister disagreed. He developed his own unique training program to beat the record and did so in 1954.

But then, only 46 days after Bannister broke Hägg's record, someone else set an even faster one. Once people saw that breaking Hägg's record was possible, the illusion of a limit was shattered and others were inspired to innovate as well.

In contrast, rookies see and explore new solutions because they simply don't know what is considered possible yet and aren't fearful of tarnishing their reputation.

Consider the story of Hara Kefalidou, a Greek politician who first entered politics with no experience, amidst a financial crisis. Government spending was being slashed across the Mediterranean country, and thanks to her rookie status, Kefalidou was able to offer a valuable suggestion. Unaccustomed to the status quo of living life as a Greek politician, she quickly pointed out unnecessary perks politicians enjoyed which were burdens on Greek taxpayers.

For example, Greek legislators were paid extra when they attended committee meetings. To Kefalidou, attending these meetings was an important component of being a good legislator and not something deserving of a bonus, so she argued against them. She was a rookie, so she hadn't gotten used to the old system and was consequently able to spot this cost-saving opportunity easily. After some convincing of the entrenched politicians, the additional pay was cut in half.

> _"Sometimes not knowing is more valuable than knowing."_

### 4. Rookies often have more expertise than experienced workers because they ask questions and ask for help. 

It's easy to assume that veterans know more than rookies simply due to their experience. However, that's not always true.

Why? Experienced workers tend to fall into the rut of _opinion stasis,_ where they only seek out what confirms their own existing opinions and ignore everything else.

Because of their background and confidence, they might actively put aside ideas which could prove their old ideas wrong–including new, better ways of working.

Additionally, these blinders influence who they choose to associate with. Those with _opinion stasis_ gravitate toward others with the same views. This makes them _even less_ likely to get shaken out of complacency toward improved ideas and solutions because there are no differing notions around them.

On the other hand, rookies actively seek out multiple opinions and ask for help. Rookies _want_ to learn from a range of people and are more open to what they offer because they don't have their own experience to fall back on.

For example, a study showed that rookies reached out to experts 40 percent more often than experienced workers did. Additionally, rookies contacted six experts on average, while experienced workers only contacted one.

Since they're drawing information from so many different people and sources, the author estimates that rookies outshine experienced workers by five-to-25 times in terms of expertise.

To that end, think of Michelangelo's Sistine Chapel. Often considered his greatest achievement, it's also the work of a rookie.

Before painting the Sistine Chapel, Michelangelo had focused on sculpture and had never painted a fresco. To overcome his lack of expertise, he hired assistants who were fresco experts to work shoulder to shoulder with him for weeks. He also consulted the papal court for ideas on content.

Being a rookie pushed him to ask for help until he mastered the technique and could paint solo, bringing with him ideas from experts.

> _"We gravitate toward experts because they represent safety, comfort, and certainty."_

### 5. Rookies push themselves to deliver bold solutions. 

Think of an experienced worker as a veteran marathon runner who moves at a slow, steady pace. There's no reason to make a dash because she has run this before and already knows the pace needed to finish.

In the workplace, this attitude translates into experienced workers having a hard time pushing themselves beyond their comfort zone because they're so used to the current pace and attitude.

This can be seen in the example of data processing company DATAPROF. The organization wanted a new headquarters that would improve its public image and passed the task onto Jack Peterson, an experienced facilities manager.

After 12 months of work, Jack presented a potential project which worked from a financial and facilities point of view, but he failed completely at the task most important to the board: public image. His proposed construction looked identical to all the company's other buildings.

In contrast to experienced workers like Jack Peterson, rookies are eager to prove themselves, so they strive to overachieve. Because of their ambition, they reach and innovate. If rookies were running a marathon, they would sprint ahead and might well end up with a better time as a result. They're unaware of their limitations, so they keep pushing.

At DATAPROF, after Jack's flop, the company brought in a rookie colleague to take on the project. The rookie made a slam dunk.

How? Because he lacked facilities experience, he asked the employees who'd eventually work in the new building about their needs. And because he knew that public image was of utmost importance, he coordinated with the people who specialized in DATAPROF's image, utilizing their expertise.

Thanks to simply asking questions and considering new solutions, the rookie's eventual proposal turned out to be perfect.

### 6. Rookies excel at innovation because they always need to start from scratch. 

There's that saying: better the devil you know then the angel you don't. Well, experienced workers tend to get stuck in this mindset: they get bogged down in protocol and how things were done in the past because it seems safer. Unfortunately, working within the confines of that safe zone limits their innovative potential.

For example, a highly experienced New York-based sales manager received a promotion of greater responsibility, for a larger sales region. In the past, he had worked from an office in the city and relied on a narrow network of sales contacts. But when his region expanded, he failed to adapt accordingly. He needed more contacts and a new strategy to cover the larger area, but was too stuck in his ways to grow. Predictably, his performance plummeted.

Unlike that experienced sales manager, rookies are always in a position in which they have to improvise and build something from nothing. They lack the knowledge, routine and network to do things in any set way.

For example, at Stanford University, student Jane Cheng enrolled in a course with the goal of creating a baby incubator suited for the developing world. Jane and her team didn't have a massive research team or a million dollar budget; they just had curiosity and ambition.

So what did they do? They went out of their way to understand the countries to which they would provide incubators, learning about local issues like lack of electricity and how these countries couldn't afford the traditional $20,000 incubators.

The Stanford team developed an incubator that was, not only functional in a lab, but functional and realistic for the nations they were helping. They could provide incubators for just $25 which would serve the function of keeping an infant warm for four hours and could also be cleaned and recharged easily with boiling water.

Happily, it's not only recent graduates who can enjoy the rookie advantage. In the next blinks, you'll learn how you can build a rookie mindset and reap the benefits, even when you are no longer a rookie.

### 7. Being a rookie is a state of mind—which means anyone can be one. 

Despite what you might think, having the rookie advantage isn't a matter of youth—rather, it's all a matter of mindset. You can get the rookie advantage even if you're a senior employee.

There are _perpetual rookies_ : people who are able to combine experience with rookie smarts. They achieve that practice by having hungry, curious minds, and always being willing to take on a challenge. They're also humble enough to always allow others to teach them.

A great example of someone who pushed himself to be a perpetual rookie is the 1970s world champion surfer Wayne "Rabbit" Bartholomew. Despite being one of the highest ranked surfers, he avoided the big waves other professionals sought out and preferred to surf at Huntington Beach, California. There, he was inspired and energized by the teenagers and other rookies who frequented the inferior waves.

Another way to keep rookie smarts is to start fresh. Looking at your job and goals with fresh eyes is an easy way to start giving yourself the rookie advantage. Even if it's a project you've tackled 100 times, imagine that this is the very first time you've done anything like it.

The author learned this trick from business professor C.K. Prahalad. He regularly throws away notes he made from previous semesters, even if he's teaching the same course, because he wants to share fresh thoughts.

A final technique to adopt rookie smarts is to find inspiration among genuine rookies. Let their fresh ideas rattle your old ways of thinking.

With these tools, you can take advantage of important rookie traits even if being a perpetual rookie doesn't come naturally to you.

> _"Rookie smarts isn't defined by age or experience level; it is a state of mind."_

### 8. Put yourself in learning mode to regain rookie smarts. 

Do you feel like you've lost the rookie mindset? Not to worry, thankfully there are ways to regain it.

The first step to relearning rookie smarts is switching to _learning mode_, which is defined by a readiness to learn. Open yourself to new ideas; forget old notions of arrogance and routine; and use every source of inspiration available to you.

This is a technique San Francisco 49ers coach Jim Harbaugh used in preparing his players for a big game. While most of the team members were seasoned professionals, Jim saw the importance of their rookie enthusiasm and drive. He harnessed it by putting high school photos of each player above their locker to remind them of the learning attitude they had when they were young. It was that attitude that propelled them to the pros in the first place.

One way to ease yourself into _learning mode_ is to put yourself into a new, fresh situation. Try volunteering or swapping jobs for a day. By jumping into a new setting, it's easier for your brain to switch over to learning mode, asking questions and enjoying the rookie advantage when you're doing something new.

You can also develop rituals and patterns that will help bring back your rookie mindset: trick your mind into stepping back in time to when you were a true rookie.

Award-winning filmmaker Francois Truffaut had a rookie mindset ritual. Early in his career he went into a small bookstore and discovered a book about filmmaking which left a lasting impression on him. Even though he directed films for a quarter of a century afterwards, every year he would go back to the same shop and relive that defining moment by buying the same book and rereading it.

How can rookie smarts help entire teams perform? Keep reading to see how great leaders harness rookie power.

### 9. Rookies can energize organizations, but only if they’re given the chance. 

So how can you as a leader help harness the power of rookies in your organization?

One way to do that is by remembering that rookies don't just bring new ideas to the projects they're working on, they can inspire whole teams.

For instance, when Nike bought Converse in 2003, head of innovation Peter Hudson brought the brand back to the top of the industry by hiring new talent and entrusting older employees with bigger responsibilities, helping to instill a rookie mindset.

Beyond coming up with just fresh clothing designs, the reborn team also suggested structural changes like working directly with factories. This made Converse much more agile and adaptable by shortening the process from design to finished product by several weeks.

However, it's important to remember that in order to make the most of rookies' energy, leadership needs to point them in the right direction. Rookies need clear objectives to strive for.

In addition, rookies need to be given enough responsibility and challenges so they can shine. These challenges should not be so big they can damage the organization, but not too small that they settle into minor roles and get bogged down in the status quo, ultimately losing their edge.

When it comes to hospitals, experienced doctors often complain that resident doctors need to be reined in because they're not adequately experienced to handle a dying patient on an operating table. However, such dire scenarios constitute only 5 percent of all circumstances, so actually, 95 percent of situations could be handled by the rookie residents. That's _lots_ of responsibility rookies can safely assume.

So the next time a task needs dedication, creativity and enthusiasm, consider a rookie. He or she just might surprise your whole organization!

### 10. Final summary 

The key message:

**In today's fast-paced workplace, learning and innovating are the keys to success. This means rookies can often outperform experienced workers thanks to the rookie advantage. Thankfully, even senior executives can keep their rookie advantage by staying open to new ideas and inspiration.**

Actionable advice:

**In the office, assume a new role or swap jobs for a day. Or, volunteer somewhere outside the office.**

Is someone at work going on holiday for a few days? Offer to take on some of their tasks and responsibilities. New challenges trick your mind into stepping back in time, to when you were a true rookie. It's an easy way to get the brain used to learning, asking questions, innovating and enjoying the rookie advantage again.

**Suggested** **further** **reading:** ** _How We Learn_** **by Benedict Carey**

_How We Learn_ explains the fascinating mechanisms in our minds that form and hold memories, and shows how with this information, we can better absorb and retain information. You'll explore the many functions of the brain and gain practical advice on how to better study and learn.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Liz Wiseman

Liz Wiseman is an award-winning author, speaker and president of the Wiseman Group, a research and development firm in Silicon Valley.

