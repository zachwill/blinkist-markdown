---
id: 54b3a4763562330009160000
slug: rule-number-1-en
published_date: 2015-01-13T00:00:00.000+00:00
author: Phil Town
title: Rule #1
subtitle: The Simple Strategy For Successful Investing In Only 15 Minutes Every Week
main_color: C2B86E
text_color: 756F43
---

# Rule #1

_The Simple Strategy For Successful Investing In Only 15 Minutes Every Week_

**Phil Town**

Investing isn't just for experts. Really, anyone can become a savvy investor without even studying finance. _Rule #1_ teaches you all the specific qualities to look for in a company, along with some simple calculations you can make yourself in order to choose the most promising stocks.

---
### 1. What’s in it for me? Learn the only rule you need to beat the stock market and get rich. 

For most of us, the stock market is basically a no-fly zone: it's too complicated, and too dangerous. Let the experts handle it, we say.

But that's a shame, because there _is_ a way that anyone can make money in stocks. It comes from a Warren Buffet idea that Phil Town expounds in _Rule #1_ : Find a wonderful business, determine its value, buy its stock for half that value, and repeat until rich.

These blinks enable you to do just that. They teach you the key indicators for that right, wonderful business that could change your life.

After reading through these blinks, you'll know

  * which statistics prove companies have advantages over their competitors;

  * why the market isn't as efficient as people like to think; and

  * why a diversified portfolio isn't a magic bullet.

### 2. Contrary to popular myths, you don’t have to be an expert to be a great investor, and it is possible to beat the market. 

It's common knowledge that you'd be remiss to invest without consulting a financial expert, right? Well, the notion that managing money demands both time and expertise isn't exactly accurate.

You don't actually need to have depth of knowledge and all the tricks of a professional financial adviser. You just need a few good tactics.

Fortunately, today the internet has the tools and knowledge you need, at minimal costs. Information and tools such as stocks' histories and statistic calculators can make a lot of the work easier for you. For instance, websites such as MSN Money, Yahoo! Finance and CNN Money have data on thousands of stocks and the site www.ruleoneinvesting.com has several investment calculators.

Thanks to technology, these tools are actually far more accurate than anything experts had ten years ago.

Aside from these tools, you also need to know that you really _can_ beat the market. To do so, you must sell stocks for far more or buy for far less than their real market value.

Yet, according to the prominent _Efficient Market Theory_ (EMT) you can't do this, as everything that can be known about a company is already figured into the price. Thus, stock prices can't be too high or low.

But this is wrong.

Many of us recall the bubble in the nineties, where, contrary to EMT assumptions, prices were far above their real market worth for some years, only to plummet dramatically afterwards. Additionally, Warren Buffett showed that at least 20 investors were able to beat the market for over 20 years.

So cast aside the myths of expert help and an unbeatable market because, thanks to the internet and the strategies in these blinks, you can make great investments yourself.

> _"An expert is a person who avoids small error as he sweeps on to the grand fallacy." - Benjamin Stolberg_

### 3. The popular strategy "diversify and hold" is not nearly as safe as it's cracked up to be. 

You've heard the formula, "diversify and hold" a million times; it's supposedly the safest way to invest in the stock market. But it's also problematic.

First, it would've failed for approximately more than half of the last century.

Let's examine how a long-term, diversified portfolio would have performed between 1905 and 2005: it would've had a zero rate of return between 1905 and 1942, 1965 and 1983 and from 2000 to 2005. That's 60 out of 100 years!

What you should do instead of 'diversify and hold' is improve your security by keeping track of all your stocks — which you might not do if your portfolio is diversified, as it would take a lot of time and effort to monitor how all your stocks are doing. Also, it's unlikely that you would thoroughly comprehend the dynamics of all the businesses you're investing in. This would result in you not being able to react as quickly to save your portfolio from losses, as you would if you only invested in fields you understood.

Say you're a pharmacist, and you receive a Red-Hand letter warning you about a drug's serious, unexpected side-effects. You'll know this letter will result in considerable losses for a pharmaceutical company and therefore sell your shares immediately. Whereas if you were clueless about pharmaceutical companies, you wouldn't know about the pending losses until it's too late.

The truth is, if you diversify, whatever happens to the market will happen to you.

You should know that whenever a large amount of money is withdrawn from the market, mutual funds decline in value, investors withdraw even more money and values drop even faster. This could happen when all the baby boomers take out the money they invested for their retirement.

> _"It's ok to put all of your eggs in one basket. Just watch the basket."_

### 4. Only invest in a business you understand and you’d like to own. 

If you were to buy a business, rather than stocks, would your criteria for doing so be any different? Shrewd investors only invest in a company they'd like to own in the long term, so thinking about buying stocks as if you were to buy the company will guide you towards investing carefully.

When you consider buying stocks, you probably think it's easy to get out quickly if the company is doing poorly. You could therefore be tempted to opt for a risky business, pay too much attention to one promising growth-rate in an average business, or even lose money in a market bubble.

If you were to buy the whole company, however, you'd be far more cautious. You'd carefully weigh its performance, history and management against other companies and, in doing so, make far better decisions.

To make even better decisions on investments, it's also wise to invest in a sector within your field of interest, as you'll already be knowledgeable about it.

If you work in IT, for example, you're in a far better position to invest there than someone who doesn't. You'll know about the different providers, their strategies, the quality of their products and so forth.

Also remember that investing in a business means you're essentially voting for it to continue with its work.

Say you're researching some highly profitable stocks to fund your child's education. Would you purchase those stocks if the company is making a chunk of its profits by exploiting children in Bangladesh? As a _Rule #1-investor_, probably not.

Know that your investment will have an impact, and you're making a statement of where you choose to invest.

> _"Stand for whatever you want with your money and realize that it's a personal choice."_

### 5. There are good criteria for a company's ability to stand its ground. 

Like moats around a castle, certain assets protect a company against competition and inflation. When investing, it's a good bet to choose companies with one or more of these "moats."

So what exactly do moats do? A moat enables a company to dominate a market, increase its prices to keep up with inflation and produce enough profit to expand.

There are many different kinds of moats.

Some moats may be intangible competitive advantages, like a patent or a trade secret that makes direct competition really tough, or even illegal. For instance, Pfizer had a patent for its hugely successful _Viagra_, therefore other companies weren't permitted to copy the product. As a result, Pfizer faced no real competition for sexual enhancers.

Another kind of moat could be a strong brand that customers identify with or are extremely loyal to. Coca Cola, for example is an incredibly stable and powerful brand and its customers will often choose its soft drinks over others.

A moat can also mean having products that are cheaper or easier to buy. For instance, Walmart is so immense that it's in a powerful position to bargain with its suppliers. In doing so, it can sell goods at very competitive prices.

Another moat could be when the product itself isn't really affordable, but switching to another is a real hassle. Take Windows, for example. Many people use it even though they hate Microsoft. But it's easier to stick with it because so much software is exclusive to Windows and if they changed to another operating system, they'd have to change all their software, too.

Lastly, some companies' moats entail exclusive governance over a market due to a legal privilege. For example, the utility PG&E has a legal monopoly to provide power in its area.

> _"A wide moat is a critical part of a. . . business because a wide-moat business can recover from lots of disasters."_

### 6. Steady businesses have competitive advantages. 

We've now seen what moats are and how they can help businesses become extremely successful. But how can you know if a company has one? Well, you can be sure that a company has a moat if they score well on five specific indicators for a minimum of ten years.

The best indicator of a wide moat is return on investment capital (ROIC) over at least a ten year period. ROIC is the rate of return a company makes from the money it invests in itself in a year. For example, say you invested $20 in stamps and then later sold them for $30. Your profit would be $10. The ROIC is the profit divided by the total you invested, which in this case would be 50 percent.

Be aware that a company that generates high returns could draw so many competitors that a good position in the same market becomes difficult to secure and therefore none are able to make decent returns. Unless, however, the company has a wide moat.

Therefore, if a company can hold onto a ROIC of at least 10 percent for ten years, it's likely beaten out its competitors, which now means it has an advantage.

Another good way to gauge a wide moat is by the growth of a company's equity. _Equity_ is the amount of money that would remain if a company sold all of its assets and cleared all of its debts.

If a company's equity increases by 10 or more percent for at least ten years, it's an indicator that it has enough money to invest into its expansion, which also puts the company in a solid, competitive position.

In the next blink, we'll take a look at the final three indicators of a moat.

### 7. Some competitive advantages are easy to spot. 

Let's turn to three other ways to tell if a company has a moat, all of which are straightforward calculations.

The first calculation is figuring out if they've had at least a 10 percent increase in _earnings per share_ (EPS) for approximately ten years.

Say three friends start a restaurant. Their EPS would be the restaurant's net income divided by three. So if the store makes $60,000 net income, the EPS would be $20,000.

Then let's say in 10 years, the EPS rises from $20,000 to $160,000. 

To calculate the restaurant's EPS growth rate, first calculate how many times 20 needs to double in order to reach 160. So, 20 doubles to 40, then to 80 then to 160. So it doubles three times over the ten years, averaging around three years to double each time.

Now apply the _Rule of 72._ Divide 72 by the average number of years over which the figure doubles once. The restaurant's ten-year-EPS growth rate would be 72 divided by three, which equals 24. Twenty-four percent would be the restaurant's moat, and thus its competitive advantage.

You can also calculate when a _sales growth rate_ consistently increases over ten percent.

For example, Garmin's sales rose from $100 million to approximately $800 million from 1995 to 2004, meaning it doubled three times over nine years, or once every three years. If we apply the rule of 72, we see that 72 divided by three equals a 24 percent sales growth rate. Clearly, Garmin had a wide moat.

The last calculation is the _free cash flow growth rate._ If this is consistently over 10 percent, this means profits turn into cash.

Free cash flow is operations cash minus capital expenditure, or the money that can be invested into expanding the company.

Garmin's free cash flow grew from $40 million to $130 million in seven years, meaning it doubled just less than twice in seven years, or once every three and a half to four years. If you divide 72 by four, which equals an 18 percent sales rate, we see that Garmin was highly profitable.

### 8. For a Rule #1-investor, a company is only promising if the CEO is driven and owner-oriented. 

When you're considering which company to invest in, you can always refer to those Big Five criteria in the charts. But there's something else that's worth noting; how effective is the CEO you want to entrust your money to?

You need to opt for an owner-oriented CEO. They'll make smart long-term decisions and inform the shareholders on what they need to know.

As a shareholder, you'll need to know if there are any problems or you won't be able to respond appropriately.

A good owner-orientated CEO will announce if it's been a very bad year, and try to give a reasonable explanation. Likewise, an owner-oriented CEO will refrain from actions that temporarily increase the stock price but do harm to the company in the long term.

Picture an executive who owns a lot of shares trying to make the stock price rise so he can cash out. That executive doesn't care about the business down the line, or about your shares.

You should also look for a CEO who is driven, as he'll put in a lot of effort to ensure the company remains profitable. CEOs like this aim for big goals and they'll remind their employees of those goals. A company with such a manager is well set to generate more and more company value, which is ideal for your investment.

For instance, when Darwin Smith bought out Kimberly-Clark, it was a mediocre company. But he pictured it as the best paper-products business in the world. He worked for 20 years toward this lofty goal, transforming and expanding the company. Twenty-five years after Smith took over, Kimberly-Clark was the world's number-one paper-based consumer-products company and had brands like Kleenex under its wing.

Knowing if a CEO is driven and honest equips you to make a sound estimate of a company's value.

### 9. Demand a big margin of safety. 

Remember the first blink in which we said that the Effective Market Theory is wrong?

Here's why: contrary to the theory, _price_ and _value_ aren't the same thing, and the key is to buy when the difference gives you an advantage. A stock's price is what you pay, whereas its value is based on the money a business generates over time.

Stock value can be a little tricky to gauge, as it's based on a business's future EPS and price/earnings ratio. However, there are tools and places to assess value online, such as Sticker Price Calculators.

Markets are inaccurate in the short term. For instance, at the close of the nineties, prices for gold, silver and copper dropped dramatically. Yet, the real value of metals wasn't diminished; we'd need them eventually, so some savvy investors bought shares cheaply. Then, in the early 2000s, demand rose again, which increased share prices and those smart investors made a fortune.

What you should do is demand a large margin of safety. A _margin of safety_ (MOS) is the value, minus the price.

A good margin of safety should be half the value of an investment. For example, in 2000, Dell's value was estimated at $40/share, and so was its market price. That's a fair price with no margin of safety.

As a Rule #1-investor, its price would need to be no higher than 50 percent of its real value, or $20, before you decide to buy.

Only a year later, Dell's price fell to $20/share, while the stock's value increased, so its MOS reached 50 percent. That would have been the right time to buy.

The MOS rule will provide you with great returns, and if there's a bubble, it will prevent you from losing all your money. 

A good MOS is not to be overlooked; in 2000, every NASDAQ stock that crashed had a stock price which was higher than its value.

> _"The three most important words in investing are margin of safety." - Warren Buffett_

### 10. Final summary 

The key message in this book:

**Thanks to the internet, you don't need to be an expert to invest your money smartly; all the necessary tools and stock data are online. By using these tools, some specific calculations and knowing how to spot a successful, profitable company, you will be in a prime position to make the right investment choices and steadily build your wealth.**

Actionable advice:

**Be smart about checking the stock market.**

Don't work up a nervous sweat checking the market numerous times a day. The best time to check the market is when it's closed, and, even better, at the same time each night. Doing so will give you the most consistent data.

**Suggested further reading:** ** _The Intelligent Investor_** **by Benjamin Graham and comments by Jason Zweig**

_The Intelligent Investor_ offers sounds advice on investing from a trustworthy source — Benjamin Graham, an investor who flourished after the financial crash of 1929. Having learned from his own mistakes, the author lays out exactly what it takes to become a successful investor in any environment.
---

### Phil Town

A highly successful investor, Phil Town managed to turn $1000 into over one million dollars over the course of five years. He is an acclaimed motivational speaker and a best-selling author.

