---
id: 54e359e2303764000a240000
slug: running-lean-en
published_date: 2015-02-17T00:00:00.000+00:00
author: Ash Maurya
title: Running Lean
subtitle: Iterate from a Plan A to A Plan That Works
main_color: EF6C30
text_color: A34A21
---

# Running Lean

_Iterate from a Plan A to A Plan That Works_

**Ash Maurya**

_Running Lean_ presents you with a fail-safe strategy to bringing your new product successfully into the market. By promoting a methodology of clever testing and planning ahead, the book gives you a step-by-step guide in building a business model that works while saving time, money and effort.

---
### 1. What’s in it for me? Learn the fastest, cheapest and leanest way to bring a product to market. 

Sure, you might think you have a great idea. But the difference between a wannabe and a genius entrepreneur is the ability to make that idea work.

Yet you too can be that genius entrepreneur, turning your precious idea into a real product that has real customers. And importantly, you don't need a million dollars to make it happen.

What you do need to do is be ready to test your product again and again, tweaking and adjusting until you've hit the market's sweet spot. Then you're on your way!

As part of the Lean Series, _Running Lean_ takes the best of "lean" methodology and applies it to a particular mission: taking your great idea and turning it into a great product. These blinks walk you through each step of the way, from initial idea to marketplace sensation.

After reading these blinks, you'll know

  * how to adjust your product if it's not getting early traction;

  * why surveys and focus groups suck; and

  * what the right way is to get the most out of your prototype.

### 2. Create a “lean canvas,” or one-page diagram, to get down your business idea simply and clearly. 

Do you consider yourself an ideas person? If you do, you know what it's like. A good idea can pop into your head at any moment, while riding your bike or eating dinner with friends.

And once in a while, you'll come up with something great, perhaps the kernel of a successful business. But before your ideas become reality, they need to be written down first.

Good entrepreneurship starts with documentation, which allows you to clarify your initial plans and refine them. And since you'll refine them many, many times, you need a fast and flexible mode of documentation.

So what's the best way to do this?

It's all about keeping it short and simple. You might think that a business plan should take months to create. But there's a better option.

Creating a _lean canvas_, or a one-page diagram, takes very little time. A lean canvas can also be adjusted easily, which is vital.

Because really, no one knows whether an initial idea will actually fly. Your first light-bulb moment is built on assumptions and hypotheses, that are tweaked as you learn more and more.

For example, you might start your tech business with a focus on a beautifully designed website. But after some research, you might find that all your clients want is something functional, so you decide to shift your focus accordingly.

Altering a long, detailed business plan would be a painstaking process. But with a lean canvas, changing your vision is easy. So at first, make use of the lean canvas approach to experiment with different business models for your idea.

After a while, you'll have several one-page plans, and will be ready to start bringing your business to life. Review those plans, and choose the strongest models, the ones you think _really_ address a need in the market.

You might even choose a favorite, but wait: not so fast. There are a few more steps to complete before you're ready to jump in. Read on to find out exactly what they are.

### 3. To find a winning idea, test, test and test again. Follow the build-measure-learn loop! 

Why on earth do people camp out in front of Apple stores, just to purchase the next iteration of the iPhone? Such avid customers give the impression that they can't live without the product.

What does this behavior teach us?

For one, humans are weird. But second and more importantly, if you are able to build a product that your customers _really_ want, you'll have created a successful business.

So how do you figure out what your clients want?

You have to put your ideas through their paces. Running structured tests and experiments is a key part of the _lean start-up methodology_. In doing so, you'll make sure none of your resources are wasted.

Instead of building and shipping a product right away and having it sell poorly, build prototypes first. Use them to see what your customers _actually_ need before putting all your eggs into one basket.

But how should you go about testing your prototypes? Follow this three-step process.

The _build-measure-learn loop_ helps you structure your testing _._ Begin by _building_ a basic prototype, mock-up or even just a landing page for a website. This is all you need to _measure_ customer response to your product. By collecting the relevant data, you can then _learn_ what worked and what didn't. Then you tweak the mock-up accordingly, and start the process over again!

Still not clear? Let's see how it might apply to something as simple as a tagline.

You might think your tagline is so attractive that customers will sign up for your mailing list. You can build a test landing page with the tagline, and measure just how many people sign up. If the number is small, you can adjust your tagline appropriately to see if the numbers improve.

You can apply this process at every level of your idea development, and by doing so, you'll have created and perfected a product that you can be sure customers want.

This is what's called a _product-market_ fit — learn more about it in the next blink.

### 4. Potential customers will tell you what they want; talk to them before you build your product! 

So you know now that finding out exactly what your customers want is _vital._

But does this mean you should spend hours and hours trying to compile data and research on people? No way! There's a far better approach: get out there and talk to your potential customers. 

Despite what people say, surveys aren't always helpful in developing a product. If you're still building the product itself, it's difficult to write a survey that covers all the relevant questions, as you don't yet know what's relevant and what isn't!

You'll also need to outline possible answers for the survey, but it's even harder to know what these should be early on. In the end, a bunch of people answering "other" is not going to be helpful.

Focus groups don't really work either, as people get caught up in group thinking and won't offer you the individual responses you need.

So how do you discover your customers' problems so that you can build your product to solve those problems? Through open conversation, for example, in _interviews_.

Interviews are a great opportunity to explore information about your customers you'd have no idea about otherwise. For example, the author of this book wanted to develop software that made it easy to share pictures and videos online.

During a few interviews, he found out that it was especially important for busy parents to be able to share media with their families. This led him to his _Unique Value Proposition_, or a short formulation that declares _what_ your product is and _why_ people should have it.

His UVP was simple, yet effective: "Easy photo and video sharing for busy parents."

In the interviews, the author additionally was able to locate a market (busy parents) in which he knew his product was needed.

Much like the build-measure-learn loop, interviews reduce the risk of wasted resources, as they provide an opportunity for you to perfect your product before you enter the market.

And, there's one more way you can ensure your product is brilliant — learn about it in the next blink.

### 5. Before you build your final product, build a demo version to test both functionality and pricing levels. 

Now that you've learned about your customers, you're ready for the next step, which is building a _demo_.

What exactly is a demo, and why do we need one?

A demo can be anything that stands in for your real product. It could be video, sketches or early prototypes. A demo is vital because it helps customers evaluate the product. They get the chance not only to assess the idea and pricing, but to decide whether it's something that truly fulfills their needs.

For example, you could present your demo to customers in interviews and ask them what they like or dislike, what features they think are missing or what features they could live without.

To test your pricing, you can share the pricing model with customers and document their initial reaction. If they hesitate, you can discuss other possible pricing options with them.

After collecting this final round of valuable feedback, you're ready to build a product for first release!

But the learning process certainly doesn't end here — which is why it is crucial to release an early version as soon as possible.

If there are any further issues, you'll have time to rectify them and keep your business on course. Remember, the goal is to create a product that solves your customer's top problem and doesn't include unnecessary or "nice-to-have" features.

In addition, you should build a landing page online that makes a case for your product and yourself. A successful landing page needs to connect to visitors quickly, so it should include only the most basic of elements.

For example, the photo-sharing landing page might include the company's UVP, "Easy Photo and Video Sharing for Busy Parents," a picture that supports and describes the UVP, such as a parent pushing a stroller, a call to action like "Take a Tour!" and contact details for questions and feedback.

### 6. Analyze the customer experience by charting the path of early adopters and identifying weak points. 

Ever been on a website and fallen in love with a product, only to find that making an order was too complicated?

You won't want this to happen to your customers, so how can you prevent it?

A great way to target any obstacles before you launch more broadly is through tapping the expertise of your product's _early adopters_.

For instance, you could ask the people from your interviews to become early adopters of your product. Better yet, you could offer a free trial period to attract people to test your product. Their feedback will allow you to improve the customer experience beforehand.

And to find out which specific aspects of the customer experience need improving, you should attempt to trace your _customer lifecycle._ But what is this?

A customer lifecycle is simply the journey your customers take, from arriving at your landing page to becoming a paying customer. Through examining the lifecycle, you can find out exactly where you're losing those early adopters along the way.

Perhaps potential customers give up, put off by your website's design? Or do they sign up for the free trial but not bother to use it? Are they leaving products in their shopping cart without checking out?

By using web analytics to track people's behavior on your site, you may be able to pinpoint the problem.

If the problem isn't clear, you might work to line up another round of interviews to troubleshoot. In fact, interviews are always worthwhile if you want to find out about problems in detail.

While this may seem like a lot of work, using analytics will really only tell you that a problem exists. Real human contact is invaluable if you want to find a _solution._

Keep this in mind as we move on to our final blink.

### 7. Do you have traction, or are you still slipping around with slow sales? Troubleshoot to improve. 

So you've launched your product but activity is slow, reviews are indifferent and word of mouth seems mute.

No, your business isn't dying a slow death. These are just signs that that you haven't reached your _product-market fit_ yet. So how can you get there?

The first thing to observe is your early _traction_, that is, the evidence that people want your product, and the way that that evidence changes according to what product you're offering.

But how _exactly_ do you measure traction?

Products such as books are designed to be purchased and used only once, so simply showing good sales would indicate good traction. The story is different if you're making software. Instead, your traction will be indicated by how many customers are coming back each month to use your product.

If you don't have a way of measuring your traction, there are other ways to gauge how your business is doing. Often, it'll be something you skimmed over in your earlier planning.

For example, you can double check the conversion rates in the customer lifecycle and ask yourself again: _Where_ are people leaving your site? _When_ are you losing customers?

Then, outline a list of potential problems and prioritize what to fix first. For example, you should improve the payment system first if it's clear you're losing the highest percentage of customers during the check-out process.

You can also rework or remove any features that your site analytics show don't have a positive impact. If your problem is simply that customers aren't returning, reach out and talk to people to find out why.

This way, you'll be able to fix just about any problem that's hindering your business, reach your product-market fit and achieve that early traction that will allow you to sustain the growth of both your product and company.

### 8. Final summary 

The key message in this book:

**Building a product and launching it without testing it is a waste of resources and can only lead to failure. To successfully build a product that people will want to buy, you need feedback in every step you take, from your first idea to your online landing page to your final product. The best way to do so is not only by analyzing your progress but also by talking to your customers.**

Actionable Advice:

**Don't fear feedback; find any way possible to get honest answers.**

If you're a software developer unused to dealing with customers directly, you might find it difficult or even intimidating to reach out to people for feedback. Start off by contacting users you might already know to familiarize yourself with the process. If you can meet with people in person, try a neutral location like a cafe, which is more personable than a work space. And be sure to give yourself and your customer ample time to have a relaxed conversation, so you can get all the goods you need.

**Suggested further reading:** ** _The_** **_Lean Startup_** **by Eric Ries**

The _Lean Startup method_ helps start-ups and tech companies develop sustainable business models. It advocates continuous rapid prototyping and focusing on customer-feedback data.

The method is based on the concepts of _lean manufacturing_ and _agile development,_ and its efficacy is backed up by case studies from the last few decades.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Ash Maurya

Ash Maurya is the founder of several start-ups, most notably WiredReach. He works closely with other entrepreneurs to promote his vision of "lean start-ups" and holds mentor roles with start-up accelerators.

