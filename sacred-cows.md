---
id: 58c697638d900300040725c5
slug: sacred-cows-en
published_date: 2017-03-15T00:00:00.000+00:00
author: Danielle Teller and Astro Teller
title: Sacred Cows
subtitle: The Truth About Divorce & Marriage
main_color: 8CC6B7
text_color: 45615A
---

# Sacred Cows

_The Truth About Divorce & Marriage_

**Danielle Teller and Astro Teller**

_Sacred Cows_ (2014) tackles the sacrosanct subject of marriage, an institution often considered to be above criticism. Rather than offering practical guidance on how to fix a marriage or how to recover after a marriage dissolves, these blinks consider how society is arranged to make us feel bad about divorce. The advocates of certain societal viewpoints are given a firm dressing down.

---
### 1. What’s in it for me? Lead some sacred cows to the slaughter. 

What comes to mind when you think of the perfect family? Maybe it's the classic Norman Rockwell illustration of the nuclear family gathered around the table for Thanksgiving dinner. Whatever images pop into your head, they probably don't include a divorcee.

Indeed, we often look to married couples as "better" than those who have divorced. But why does such a hierarchy have to exist in the first place? What fuels the dogmatic belief that marriage is inherently good, and divorce inherently bad? The reasons behind such sacred cows are often, as we'll see in these blinks, simply a lot of hot air.

In these blinks, you'll learn

  * what exactly sacred cows are;

  * how absurd the marital promise of "forever" is; and 

  * why you shouldn't feel bad about divorce.

### 2. Cultural assumptions are there to be torn down, especially when it comes to marriage. 

We all know the world isn't flat, but it took us a long time to get there. For millennia, it was thought by most people that the world couldn't be anything _but_ flat. An unquestioned cultural assumption like this is known as a _sacred cow_. These sacred cows feel so much like facts that they are often thought to be integral to society. Challenging them can be difficult.

For example, thinkers like Galileo, a man who disputed multiple "facts," including the earth's flatness and the geocentric model of the universe, were harshly criticized by their contemporaries. But time — and science! — proved them right.

Of course, sacred cows can also exist on a smaller scale — for instance, in the world of marriage and divorce. For a number of reasons, society regards people who divorce as morally suspect, disloyal, irresponsible or even defective. Such a dogmatic view of marriage and divorce is more than questionable — it's a modern-day sacred cow, and it needs to be challenged.

We're taught that the institution of marriage is the bedrock of decent society. Married people are thought to be logical, responsible and caring. And divorce threatens these societal norms and personal attributes. Indeed, America is seething with people who propagate the sacred cow of marriage.

In the following blinks we'll look at some of these people, including the Holy Cow, the Expert Cow, the Defective Cow, and the Innocent Victim Cow. This myth-mongering herd can and must be challenged!

### 3. The Holy Cow believes marriage vows last forever, but, really, no one can promise that feelings won't change. 

If you've ever considered divorce, you'll be familiar with some difficult feelings — and you can thank the Holy Cow for not making things any easier.

The Holy Cow is a dogmatist. He or she firmly believes that marriage is inherently good and that divorcees are worse than people who abide by their marriage vows. The Holy Cow always argues for marriage and staying together, even if it compromises individual happiness.

For example, he may well tell you that divorce is an indication of individual weakness or selfishness. Or he might tout some recent study which claims that divorce rates are decreasing and, in so doing, make you feel unusual or different. The Holy Cow wants to depict you as non-conformist and out of sync with society.

But let's face it: this is a bogus view. In truth, no one can predict how feelings will change over time. Despite the popular formulation, no one can truly promise to love another "until death do they part."

Imagine you're shopping for clothes and an incredibly beautiful winter coat catches your eye. It's freezing outside, you have some money to blow and you were sort of on the market anyway. It seems like fate.

But then, when you go to buy it, the salesperson tells you there's a catch: you can only have the coat if you vow to wear it every day for the rest of your life. Well, you still want it. You're cold and you're utterly enchanted by the thing, and so you agree. Nine months later, however, it's mid-August and 90 degrees and you're still stuck wearing the damned thing. What then?

This is where the Holy Cow comes in. He'd tell you that any desire for change is selfish. After all, didn't you make a promise? Like it or not, when truly scrutinized, such "eternal" promises simply can't be expected to last forever.

### 4. When it comes to your true desires, listen to yourself, not the Expert Cow. 

In most relationships, there comes a time when some expert advice is needed. But be wary of the Expert Cow. She gives the same advice to everyone because she cares more about herself than your well-being.

The Expert Cow could come in the form of a clueless marriage counselor or a self-help book. Of course, not _all_ counselors or self-help books are disingenuous, but the Expert Cow, no matter what, is geared to self-gain, particularly if there's a financial incentive. She'll often peddle one-size-fits-all solutions, closely adhering to her own dogma.

For example, you and your partner might visit a marriage counselor. You tell her in the first session that you don't love your partner anymore and are more responsive to friendship. That's pretty clear, isn't it? But the Expert Cow has no interest in clarity. She believes only in her own principles. She'll encourage you to employ more empathetic communication or more validating behavior.

This of course means remaining married, despite the crystal-clear statement you made.

Where to go from here? Despite the Expert Cow, there is no such thing as one-size-fits-all advice. So, when it comes to counseling, stay attuned to your own desires.

You need to ask yourself what _you_ want. Self-examination is the first step to alleviating marital distress.

For instance, you might want your spouse to have more of an active life outside your relationship, but he might be perfectly happy being a homebody.

Whatever the issue, you need to work out your desires. Only then will you know which path to take and whether therapy is the best option for you.

For example, if you already know that what's troubling you is your partner's selfishness and lack of support, then there's little need to see a therapist specializing in reigniting physical passion.

### 5. If divorce is on the table, the Defective Cow will tell you that you're the problem. 

We're all prone to a little self-doubt, but when it comes to divorce, the Defective Cow will try to milk those feelings for all they're worth.

If you reveal to the Defective Cow that you're thinking of getting a divorce, he'll try to convince you that you're defective in some way. This is because he values marriage above all else and wants to put the blame squarely on you.

Let's say you're a married woman who's lost her libido. You've tried hard to rekindle the passion but nothing helps. Confide your misfortune to the Defective Cow and you'll have it spat back at you. You're the problem! Maybe it's because you don't have enough confidence in your body, or because your diet is all wrong.

Whatever the problem in your relationship, you're the source, and the Defective Cow will do his best to make you shoulder that responsibility.

But the Defective Cow has got it wrong. It's not unusual to lose interest in a spouse and it doesn't mean that you are defective in any way. In fact, a lost libido isn't just something you can easily fix. It could happen for all sorts of reasons; maybe the love has simply gone.

Regardless of this truth, however, the Defective Cow will insist on chimerical solutions. Maybe it will try to persuade you to go see a gynecologist or a psychologist or a sex therapist. Each fanciful solution will only shame you into feeling worse and worse, reinforcing the idea that you're to blame. That's the strategy of the Defective Cow.

That's not to say there are _no_ good reasons for staying together. But these reasons should be based on individual choice. Shame and guilt should play no part.

### 6. Beware the stats manipulator that is the Innocent Victim Cow. 

There's nothing quite so convincing in an argument as confidently pulling out a wad of statistics, right? The Innocent Victim Cow knows this tactic well. Unfortunately, as useful as statistics can be, they're extremely easy to abuse. The Innocent Victim Cow will try to convince you of the negative consequences of divorce, for example by telling you that you're denying your own children happiness by prioritizing yourself.

One factoid the Innocent Victim Cow will always wheel out is the notion that the children of divorced couples are themselves more likely to divorce. True enough, there is a correlation, but it's hardly backed by real evidence.

Remember that for the Innocent Victim Cow "fact" and guilt are two sides of the same coin. The deceit at the heart of the Innocent Victim Cow's use of statistics is to confuse correlation with causation.

For example, statistics tell us that those who own horses also tend to be wealthy. After learning this, would you rush on down to the nearest stables in the hope that buying a bay mare will bring you an immense fortune? Of course not. That's because the relationship between horse ownership and wealth is one of correlation, not causation. The data in itself tells us nothing about the causes of wealth acquisition.

The same basic confusion is commonplace in statistics related to the children of divorced parents. There may well be some correlation in the data between unhappiness and divorce, but we can't say more than that.

The Innocent Victim Cow uses statistics to exploit parental instincts and keep us believing that a hackneyed platitude — "we're doing it for the children" — is a sound reason for staying married.

### 7. Final summary 

The key message in this book:

**Society frowns upon divorce and fetishizes marriage as an institution. It prioritizes societal conformity over the needs of the individual and, in so doing, does everything in its power to make you feel bad for wanting a divorce. Being aware of these social mores empowers you to make the right choice for yourself, whether it means staying married or not.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Marriage, a History_** **by Stephanie Coontz**

_Marriage, a History_ (2005) covers the history of the institution of marriage, from its genesis in the Stone Age to its recent crisis.
---

### Danielle Teller and Astro Teller

Danielle Teller is a Canadian-born doctor of medicine, currently residing in Palo Alto, California. She trained at several universities, including Brown and Yale. In 2013, she decided to pursue her dream of becoming a writer.

Astro Teller, the husband of Danielle Teller, holds a PhD in artificial intelligence from Carnegie Mellon University, Pittsburgh. He studied computer science at Stanford University. He is also the author of _Exegesis_, a science-fiction novel.

