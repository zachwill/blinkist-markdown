---
id: 5a379ee7b238e1000633a346
slug: sales-management-simplified-en
published_date: 2017-12-22T00:00:00.000+00:00
author: Mike Weinberg
title: Sales Management. Simplified.
subtitle: The Straight Truth About Getting Exceptional Results From Your Sales Team
main_color: F3314C
text_color: BF263C
---

# Sales Management. Simplified.

_The Straight Truth About Getting Exceptional Results From Your Sales Team_

**Mike Weinberg**

_Sales Management. Simplified._ (2016) takes a critical look at today's troubled sales departments and offers a comprehensive plan on how to inject energy and vitality into your sales team. It runs through the most common and chronic problems and provides practical advice that any manager can follow to avoid pitfalls and turn a sleepy sales department into a strong and unified team.

---
### 1. What’s in it for me? Establish effective sales management and create a solid working culture. 

Sales gets a bad rap in our culture. Whether it's the less-than-useless Gil Gunderson in _The Simpsons_, or Arthur Miller's classic _Death of a Salesman_, the view on sales and sales reps is generally less than positive.

Still, every company needs people to sell their goods and services. So how do you manage sales in a way that leads to great revenues but avoids all the sleazy and stressed-out stereotypes?

In these blinks, we get insights from author Mike Weinberg's extensive work as a sales consultant and a sales manager — insights that show how the management of sales, rather than the salespeople themselves, is the most crucial aspect of creating a successful sales organization.

You'll also find out

  * how sales managers often are left prioritizing the wrong things;

  * why pushy salespeople are no better than infomercials; and

  * what the four R's are and why they matter.

### 2. Successful managers and sales teams need to avoid unproductive distractions and tasks. 

How do you become an excellent sales manager? Well, it all begins by asking yourself two basic questions:

  * Which task receives most of my time and effort?

  * And: Is this task a primary contributor to my results?

Managers are often overwhelmed by unnecessary meetings and tasks that aren't directly related to helping their team sell. More often than not, days are filled with tasks that do nothing to drive revenue, which should always be the primary concern.

The author spoke to one sales manager who explained how he once spent a day helping the maintenance crew set up a reception area for clients instead of getting his staff ready for the event.

This kind of situation is common, and if sales managers aren't strict about dedicating most of their time to sales-related tasks, all those non-sales-related tasks can easily eat up the workweek.

Another primary distraction is _customer relationship management_ (CRM) software, which captures and analyzes customer data and which can be helpful in maintaining customer retention.

Although it can help sales teams stay connected with their customers, CRM software won't fix an unproductive sales team. In fact, it can even harm performance as it can shift a sales manager's focus onto the software and away from the team. This results in managers spending most of their time reminding staff about data entry, which gives everyone the impression that updating the system is more important than the real job of sales.

An overreliance on CRM, as well as an overuse of email, can also lead to team managers avoiding the most effective means of communication: face-to-face meetings. Make no mistake, there is no replacement for the value of one-on-one meetings, regular team meetings and spending time in the field with salespeople.

Imagine how well a baseball team would do if the coach spent each day sitting in an office and only communicated with texts and emails. That team is all but guaranteed to lose.

### 3. Managers need to break the habits they developed as salespeople. 

Most sales managers started out in the business as sales people. This makes perfect sense, but since the role of a salesperson and the role of a sales manager require very different mindsets, a number of problems arise.

Salespeople are taught to be selfish with their time. If they spend too much time assisting a coworker, their own sales will suffer.

For managers, the opposite holds true. They're at their most effective when they're accessible to their staff and devote time each day to helping others, in addition to completing their other management tasks.

Now, this isn't the same as being a "player-coach" where you keep transitioning between selling and managing. Many sales managers have attempted this dual approach, but be warned: it always results in their underperforming as both a manager and a salesperson.

Sometimes a business will promote their top seller to manager and ask them to continue selling part-time. This happened at a heavy-equipment distributor the author worked with, and the result was typical: the new manager spent 95 percent of his time selling, leaving the team without any leadership.

Being a hero as a salesperson is straightforward — all you have to do is make a lot of sales. But managers become heroes by ditching personal glory and coaching their team on how to be more efficient and productive.

Unsuccessful managers, on the other hand, seek out chances to jump in, assume the heroic role and thereby diminish the work being put in by their team. Naturally, this has a negative impact on team morale and productivity.

Yes, it's intoxicating to make a fantastic sale. But sales managers who are still recovering from the hangover of their glorious salesperson days — managers who try to position themselves as the smartest person in the room and answer every question — rob their staff of any chance to learn.

The author knew one manager who obsessively micromanaged and wouldn't allow any salesperson to lead the preparations for a proposal. Without fail, he'd find an excuse to step in and take over. As a result, the performance of his team suffered.

> _"Are you a hero or a hero-maker?"_

### 4. Managers need to understand team roles and create the right environment for success. 

A manager's job isn't limited to coaching. They should also have an eye for talent and know what skills their sales team needs.

One major cause of poor performance is a failure to clearly define roles within a sales team. When this occurs, jobs end up being improperly assigned and, unsurprisingly, team members underperform and revenue is lost.

Many managers try to counteract poor sales by onboarding a veteran salesperson with a proven track record. This tactic is as bad as it is common. It's unwise to think that a salesperson will be as effective on a new team as he was on his old one.

That's because non-compete agreements usually bar salespeople from using their old contacts. And without all those contacts, gathered over the course of years, that veteran won't be able to maintain a pristine track record for long.

A far better strategy is to establish a consistent, high-performing workplace, and to do that two things need to be in place: strict management and quality mentorship.

There is currently a serious lack of skills in the sales departments across US organizations, and it's due to veteran salespeople not having the time to mentor new talent. It's up to managers to rectify this by making sure effective coaching and learning opportunities are available in the office and in the field.

It also falls upon the leadership to be strict in their management.

When this doesn't happen, and poor performance isn't addressed, the situation will only deteriorate further. When bad behavior goes unpunished, when a manager is more focused on being everyone's friend than on improving performance, salespeople get miserable. A team without justice will soon fall apart.

An especially bad habit is for management to give out commissions on sales that were never earned. This practice removes the very incentive that motivates a sales team to work hard.

Commissions must relate directly to performance and the amount should always be higher for new sales than for repeat sales. When new sales aren't given a special commission, a manager shouldn't be surprised when team members fail to go out, take risks and bring in new prospects. When there's no incentive, there's no motivation to stray from the safety of old sales.

### 5. It’s up to managers to train staff properly. 

A mere generation ago, the job of sales was utterly different. Thanks to the internet, salespeople today have unprecedented access to information. But so do customers. Indeed, one of the major challenges to modern sales teams is how to attract and retain the new, hyper-informed breed of buyer.

Yet the real problem most salespeople face is much more basic: they're not properly trained and, therefore, they're unprepared to snatch opportunity when it arises.

Without training or mentoring, new members of a sales team essentially wait for prospects to come to them. And when those chances to make a new sale arrive, they're often late to the meeting, disorganized and altogether unprofessional.

The lack of training extends to sales calls as well, which are being made without any sense of structure or understanding of the fundamentals. Many calls are being made aggressively and in such a way that makes the sales pitch no better than an infomercial.

So how should a call be conducted? First off, the salesperson should never lead with the product. When this happens, the client will only see the caller as someone trying to sell them something, and not as a valuable consultant or trusted advisor, which should be the goal.

A well-trained salesperson will know how to ask questions, open up a dialogue and figure out exactly what the customer needs.

Another mistake is when a sales department receives an unexpected _request for proposal_ (REP) from a potential client and, rather than wait to carefully weigh up the potential in the deal, an untrained salesperson will rush to send a reply. This shows an overeagerness that no client will respect. In fact, in the author's 25-year career, he has never won a sale by responding to a blind REP. Prospects value a salesperson with a measured, clinical approach, not one desperate to take any chance they get.

Delivering a disorganized and unprepared presentation is bad, but appearing desperate in your willingness to please the customer is perhaps even worse.

### 6. A healthy sales culture starts at the executive and managerial levels. 

Managing a sales team isn't rocket science. In fact, you can divide the job into three simple categories: _sales leadership and culture, talent management_ and _sales process_. Let's look at the first category in this blink.

Improving the culture of your workplace is a great way to improve lackluster performance, and doing so always starts at the top and then trickles down.

For a good example of what a winning sales culture looks like, the author points to his early career, when he worked for a company led by a CEO and CFO who both highly respected the sales department. Along with a proactive sales manager, they created an energized sales culture rooted in healthy competition and camaraderie. Performance-based compensation was offered as a highly motivating incentive and all sales reports were publicly available as a way to encourage and provide accountability.

Now let's look at a small software company with a terrible sales culture, which, unsurprisingly, led to poor revenue.

It started at the top, with executives who didn't understand how sales operated. They provided frantic, confusing direction, backed by an unclear strategy. In the sales office, there was so little excitement that the only sound being made was when someone clicked their mouse.

A good sales manager will facilitate a healthy culture by encouraging direct and open communication along with a sense of trust. Be brutally honest and make it understood that any criticism should never be taken personally.

A healthy sales culture should also offer intense coaching that focuses new hires on results and helps the team meet common goals. And everyone on the team should trust one another and feel confident that they are indeed a team and that no one is out to sink anyone else's fortunes. Jealous competition is not healthy competition, and salespeople will always be more effective when they trust that management is there to help, not get in the way.

With this in mind, managers and executives should not waste their staff's time with unproductive tasks, unnecessary meetings and constant calendar and email checking.

Managers should be focused on high-value tasks like mentoring salespeople in the field and providing staff with opportunities for one-on-one consultations.

### 7. Follow the Four R’s of talent management. 

To further facilitate a healthy sales culture, managers must pay strict attention to _Mike Weinberg's four R's_ of successful talent management.

The first is to put the _right people in the right roles_.

Great talent is scarce these days, as true sales hunters only make up 10 to 20 percent of sales teams. A hunter is someone who is always ready to close new deals and explore new avenues for sales. With so few available, a good sales manager will ensure that their hunters are placed in areas where they can garner the most sales.

The second R is to _retain top producers_.

If you don't reward your top sellers, you shouldn't be surprised if they leave. Many managers will give too much attention to their struggling sellers and fail to retain the ones bringing in the big sales. Don't make this mistake.

Instead, recognize good work and provide your super sellers with more support. Ask them about the barriers that stand in the way to even greater success and find ways to break through those barriers. You can also be creative with the rewards and offer things like a spot on the next exclusive company retreat.

The third R is to _remediate or replace underperformers_.

Don't let chronic poor performance slide, even if the person in question is a hard worker or a team player. Being lenient isn't going to improve results. Remember: ignoring bad work hurts everyone.

The first stage of remediation is an informal warning that clearly identifies the problem. Then, a plan is agreed upon to improve performance through cooperation and coaching.

Expectations must always be clearly communicated — for instance, tell the underperforming employee that you'll be setting up eight meetings with new prospects in 30 days. And if improvements aren't made and expectations fail to be met, the formal process of termination and replacement will begin.

Finally, there's the _recruitment_ process.

This can be improved by asking two simple yet powerful questions during an interview.

"Can you provide the details of your last two successful deals?" If you're dealing with a quality candidate, she'll see this as a perfect opportunity to provide examples of her skills. The wrong candidate will panic.

The second question is "Imagine you're left unsupervised for 90 days. What would you accomplish in this time?" Good candidates will provide an impressive plan, while bad ones will usually look stunned or give a thoughtless answer.

### 8. Managers must provide clear goals and make sure their team has the tools to reach them. 

It's every manager's duty to arm her team with the right tools so that it's ready for action. But those tools won't be very useful if the ultimate goal isn't clear.

That's why managers should always work with their team to come up with a list of clear strategic targets. Serious thought and consideration should be put into which customers and prospects will be the focus of the team's efforts.

Salespeople will often aim big, but a massive number of targets can lead to an unfocused sales team and limit the amount of time available for follow-up contact with a prospect. So the better tactic is to concentrate the team's effort on a manageable list of carefully selected targets.

The most important tool in a salesperson's toolbox is their _sales story_, otherwise known as their _elevator pitch_ or _value proposition_.

A sales story is a set of talking points that highlight the value of your offer. And it's absolutely necessary because every other tool incorporates parts of this story.

Generally speaking, there's always room to improve a sales story. The worst ones are boring, overly complicated or are more focused on the salesperson than the client. So help your team members prepare a succinct and captivating sales story that is client-focused.

All in all, the basic set of salespeople tools haven't changed much over time, and the old methods are still as relevant as ever.

Therefore, managers should routinely guide their team through a reassessment of the fundamentals, from sales-call technique to presentation organization and proposal writing. A salesperson's understanding of these core concepts will always provide insight into their capabilities.

A sales manager should keep these questions in mind when checking on their team's grasp of the fundamentals:

  * What is the minimum amount of research being done on a client before a sales call?

  * When making a call, what kind of insightful probing questions have been prepared?

  * After a sales call is finished, what key information is being recorded and shared?

### 9. It’s a manager’s job to make sure one-on-ones and team meetings are effective and productive. 

Once the sales team is in action, armed with the right tools and given clear goals, a good sales manager should focus on monitoring progress.

The most reliable monitoring happens in monthly one-on-one meetings that allow you to see where team members are at and to agree on where they're headed.

The author's former manager, Donnie, used a three-stage evaluation process.

Every month Donnie would take his notepad and sales report and sit down with his staff. In the first stage of the meeting, sales results would be reviewed. When the salesperson's numbers were in line with the monthly goal, Donnie would simply encourage them to keep up the good work.

If numbers were not high enough, Donnie would go to the second stage. This involved making sure the salesperson had sufficient opportunities in the month ahead to get back on track.

If nothing was set up for the month ahead, Donnie would go to the third stage and ask, "What the heck have you been doing?" At this point, a calendar would be used to figure out an improved business plan that would get sales back on track.

This is the kind of productive meeting that contributes to a winning culture, and it should extend to the team meetings as well.

Sales managers often struggle to come up with meaningful agendas. So here are some examples of how to engage and strengthen the bond with your team:

Going one by one and catching up on developments in your staff's personal lives is a great bonding tool. Then there's the reliable agenda of reviewing sales results and handing out praise to those who excelled and those who've made the biggest improvements.

The final piece of advice is for every manager to get out into the field. No manager can effectively lead a sales team from their desk. So get out there and see the real challenges your team faces first hand.

Being at your team's side in the field will strengthen relationships with your team as well as with your key customers.

### 10. Final summary 

The key message in this book:

**Developing a high-performance sales culture is absolutely critical to a sales team's success. Sales managers must prioritize high-value tasks, prepare and value their team and work strategically to achieve goals.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Sales Bible_** **by Jeffrey Gitomer**

Considered one of "Ten Books Every Salesperson Should Own and Read" by the Dale Carnegie Sales Advantage Program, _The_ _Sales Bible_ (1994, revised 2015) is a classic tome of sales strategy. The book takes an indepth look at the sales practices and techniques the author himself mastered to achieve lasting success in sales.
---

### Mike Weinberg

Mike Weinberg, a sales-management and business-development specialist, is a popular speaker and coach. He is also the founder of the consultancy firm The New Sales Coach, as well as the author of the Amazon bestseller, _New Sales. Simplified._

