---
id: 536a073a6631340007900000
slug: salt-sugar-fat-en
published_date: 2014-05-06T10:23:07.000+00:00
author: Michael Moss
title: Salt Sugar Fat
subtitle: How the Food Giants Hooked Us
main_color: 70AE51
text_color: 4F7A39
---

# Salt Sugar Fat

_How the Food Giants Hooked Us_

**Michael Moss**

_Salt_ _Sugar_ _Fat_ examines the rise of the processed-food industry in America and globally, and why it has been fueled by the liberal use of salt, sugar and fat. These three ingredients are near irresistible to us humans, but their overuse also comes with devastating health effects.

---
### 1. What’s in it for me: learn why you need to start eating home-cooked meals again. 

Ask yourself, how often do you eat meals prepared only from natural, unprocessed ingredients?

If you're like most people, probably rarely.

This is because ever since the 1940s, processed foods have become an increasingly prominent part of the diet of industrialized countries. While zapping a plastic tray in a microwave may be a convenient way of preparing dinner, these foods are unfortunately also chock-full of sugar, salt and fat — a very unhealthy trifecta.

In these blinks you'll discover, among other things:

  * How _Lassie_ played a part in the demise of the home-cooked meal.

  * Why, in the 1980s, the US government found itself stuck with a literal mountain of cheese.

  * Why any processed-food company trying to make healthier products is rapidly penalized by consumers.

### 2. After World War II, processed convenience foods replaced the ideal of the home-cooked meal. 

In post-World War II America, change was afoot: Women who had traditionally stayed at home to cook and clean while their husbands worked began to take jobs of their own. This meant that they suddenly had less time for the arduous process of preparing home-cooked meals.

At the same time, the increasing prevalence of televisions in American homes meant that there was another disincentive for spending more time in the kitchen, as you might miss great shows like _Lassie_.

Sensing the opportunity in this shift, food companies began to produce more heavily processed _convenience_ _food_ s designed to be quick and easy to prepare.

One of the first companies to do this was General Foods, which introduced Jell-O instant pudding in the 1950s to immediate success. A slew of other time-saving foods and products followed, as consumers were increasingly willing to trade a bit of their wealth for more free time.

Of course, the ideal of the home-cooked meal was hard to shake, not least because 25,000 home economics teachers in high schools in America were still advocating it and teaching students how to make it.

To overcome this resistance to processed foods, the food companies recruited their own home economics teachers to advocate processed foods by holding their own cooking contests and giving cooking lessons to mothers and teachers.

The most famous example of such a teacher is the entirely fictional "Betty Crocker" character invented by an advertising manager. Her catchy slogans, signature cookbooks and showrooms illustrating the ease of heat-and-serve meals contributed significantly to the shift of American food ideals toward the factory-processed foods that dominate supermarket aisles today.

The impact on American diets was particularly great due to the immense power of the three favorite ingredients of the processed-food industry: salt, sugar and fat.

### 3. Humans crave sugar, which is why processed food contains lots of it. 

We humans love the taste of _sugar_. This is because once upon a time, getting quick, concentrated calories like those in sugar increased our likelihood of survival, so our cravings provide an evolutionary advantage. We also love starchy foods like pizza, because starch can be converted into sugar. It's no surprise then that the processed-food industry uses sugar to make us hunger for their products.

Our desire for sugar isn't limitless though. At some point, things just get too sweet to be desirable. This means food companies need to identify the _bliss_ _point_ of sweetness for every food item they produce. This is the point at which enjoyability is maximized.

Our individual bliss points do vary somewhat — especially with age. Take, for example, vanilla pudding: children tend to hit their bliss point when the pudding comprises around 30 percent sugar, whereas for adults only half that is required.

Even with this built-in limit, processed-food companies cram a lot of sugar into their products: on average every American consumes some 22 teaspoons of sugar per person per day, with over two thirds of it coming from processed foods.

Consider soft drinks, for example: a 12-ounce can of Coke contains about nine teaspoons of sugar. Perhaps surprisingly, some fruit drinks like Tang and Capri Sun contain even more.

Another example can be found in many childrens' breakfast cereals, which can be 50–70 percent sugar.

Sugar is even added to foods where it doesn't belong, like pasta sauces: just half a cup of Prego Traditional pasta sauce contains over two teaspoons of sugar. In fact, after tomatoes, sugar is the main ingredient.

Just to put these figures into perspective, the American Heart Association recommends that adults get no more than 5–9 teaspoons of sugar per day on top of their nutritional needs.

### 4. The consumption of sugar has continued to skyrocket despite increasing health concerns. 

Today, almost everyone knows that eating too much sugar is unhealthy.

One of the first health issues connected to sugar was the radically increasing occurrence of tooth decay in 1970s America. One dentist, Ira Shannon, was so alarmed by the state of his young patients' teeth that he began testing the sugar content of breakfast cereals, as food companies were not yet required to divulge product ingredients on packaging. To his shock he found that the sweetest cereals were the ones being advertised during children's Saturday morning cartoons.

At the same time that Shannon revealed his results, a very prominent Harvard nutritionist named Jean Mayer began publicly implicating sugar as a factor in both obesity and diabetes.

These developments spelled trouble for the processed-food industry, and by 1977 the outrage had become so great that many health professionals asked the Federal Trade Commission (FTC) to ban all advertising of sugary foods aimed at children. In fact, the FTC went even further and proposed banning _all_ advertisements directed at children, whatever the product.

After much public debate, the proposal failed as many felt that it should be up to parents to temper their children's demands for something they saw on TV.

In any case, the public was now well aware of the dangers of sugar, and so the processed-food industry renamed their products to better suit the public mood: Kellogg's "Sugar Frosted Flakes," for example, became "Frosted Flakes."

Despite the health concerns, Americans' desire for high-sugar foods soared. Coca Cola's sales more than quadrupled between 1980 and 1997.

The health effects of this multi-decade sugar binge were obvious: by 1999, over half of American adults were considered overweight, with almost a quarter clinically obese. What's more, massive social costs as high as $100 billion a year were predicted for treating related illnesses like diabetes and heart disease.

It is noteworthy that these developments were also mirrored in other industrialized countries, where processed foods have also become popular.

### 5. We simply can’t get enough fat in our food. 

The second favorite ingredient of the processed-food industry is _fat_. As with sugar, evolution has sculpted us to crave fat because it is loaded with calories, and gram for gram, fat actually has double the calories of sugar.

Unlike sugar though, there doesn't seem to be an optimal bliss point for the amount of fat that we'd like our food to contain. It seems that more is always better.

For example, one study showed that milk simply _can't_ be too fatty: people always prefer fattier milk, even when it's fattier than heavy cream.

This may be partially explained by the fact that we're not great at gauging how much fat there is in food. In fact, we don't have taste buds for fat as we do for sugar — instead we sense its texture.

This is demonstrated by a study that found that although we can fairly accurately gauge how much sugar is in food, we're very bad at estimating its fat content. What's more, when sugar is added to food, people believe the fat content has been reduced. It seems that adding sugar "hides" the fat.

As you may have guessed, since there's no limit to our desire for fat, processed-food companies cram masses of it into their foods. In fact, many soups, pies, frozen meals and so forth deliver over half of their calories as fat, and yet for some reason they are not even considered fatty foods.

What's more, fat has other benefits for the industry: it gives food items longer shelf lives and from cookies to hot dogs, gives many products a more desirable texture and appearance. These factors also increase the amount of fat used in processed foods.

On average, Americans exceed their daily recommended intake of fat by 50 percent, which also helps explain why, as mentioned before, over half of Americans are overweight.

Fat, specifically _saturated_ _fat_, has also been linked to heart disease as well as type two diabetes, which over 100 million Americans have or are close to developing.

### 6. Over the past century, cheese has become a huge health concern. 

Perhaps the biggest fat-related threat to Americans' health comes from a seemingly innocuous source: _cheese_.

But why is this?

Cheese, and especially processed cheese, is very fatty, delivering more than two thirds of its calories as fat.

But what's more, Americans really love it: they consume 33 pounds of cheese and "pseudo-cheese products" each year.

The yellow stuff was not always this popular though.

In the 1930s, the US government decided that the dairy industry was so vital to the nation's health that it should be subsidized by the government guaranteeing to buy all the dairy products that producers could not sell on the market.

Then, in the 1950s, Americans began seeing milk as a fatty product, and so began demanding more low-fat milk. Dairy producers duly obliged by stripping the fat from their milk, but this left them with a new dilemma: enormous mounds of excess milk fat. Luckily for them, they could make cheese out of it and sell it to the government according to the subsidy program.

Soon, the government found itself buying more cheese than it knew what to do with. In fact, in 1981 its stack of cheese weighed 1.9 billion pounds!

When Ronald Reagan became president, he decided to end the program, but in order not to leave the dairy industry in the lurch, he also instituted a program that helped producers collectively market cheese to Americans.

The result? Americans today consume triple the amount of cheese they did in 1970.

The government, however, has begun to see the downside as well, as a 2010 report identified cheese as the biggest source of saturated fat in the average American diet, with red meat a close second.

Despite these findings and contrary to the calls of many nutritionists, the United States Department of Agriculture (USDA) did not recommend cutting the intake of cheese and red meat.

Some experts note that this looks suspiciously like the USDA protecting dairy and meat industries rather than the health of Americans.

### 7. Processed food delivers most of the average American’s salt intake. 

Finally, let's examine the third favorite ingredient of the processed-food industry: _salt._ Unlike sugar and fat, salt contains no calories, but it does contain a mineral that's essential for the human body to function: sodium. Unfortunately, when the body gets _too_ _much_ sodium, it increases blood pressure, inducing _hypertension_.

In the 1980s, this potentially fatal condition was becoming a severe problem in the United States, as one in four Americans suffered from it. High salt intake was one of the culprits identified, as it was found that Americans were ingesting ten to 20 times the amount of sodium their bodies required.

To remedy the situation, health officials initially instructed Americans to do away with their salt shakers. Soon they discovered that the real culprit was processed foods, which contributed more than three quarters of Americans' sodium intake.

It turned out that food companies were pouring buckets of salt into their sauces, pizzas, soups, canned spaghetti, and so forth: Consider that one single "Hungry Man" microwavable frozen turkey dinner contained more than double the daily recommended intake of sodium.

So why do food companies shove so much salt into their food?

The first obvious reason is taste. Salt brings out the flavor of food, and also hides unpleasant tastes that often remain in processed foods after manufacturing, so-called "off notes." The author tasted Campbell's Vegetable Beef Soup before salt was added, and found it had a bitter, metallic flavor.

So does salt have a bliss point like sugar? Studies suggest that it does, but it's flexible: by increasing our salt intake, we effectively raise the bar for "optimal saltiness" too.

On top of taste, there are other reasons for food companies to add sodium-based components to food: such compounds can be used to, for example, increase shelf life and bind ingredients together.

Unfortunately, all this adds up to an excessive sodium intake for the average American.

### 8. Food companies lose sales if they try to be healthier, but government interventions hold some promise. 

So what can be done about this unhealthy trifecta of salt, sugar and fat?

For a start, in some countries with similar problems to the United States, the government has stepped in to reduce the sodium content of processed foods.

In Great Britain this took the form of a voluntary program for manufacturers that set limits on the amount of sodium they could add to food. Impressively, the program is calculated to have saved 10,000 stroke- and heart disease-related deaths a year.

In Finland in the 1970s, government authorities mandated that sodium-heavy grocery items be clearly labeled with a "high salt content" warning, while launching a public health campaign to educate people about its dangers. As a result, by 2007 per capita deaths from strokes and heart disease had declined by 80 percent.

In the United States, government restrictions on the market are generally opposed, but some food companies have tried voluntarily imposing similar restrictions on their salt, sugar and fat use.

For example, in the late 2000s Campbell's decided to reduce the sodium content of many of its soups. Unfortunately, consumers reacted to the lack of flavor by buying less, and sales plummeted to the point that in 2011, the company announced it would up the sodium content once more.

Another example is Kraft, which in 2003 decided to launch a broad health campaign. The company stopped advertising nutritionally inadequate products to children and also tried to be more transparent in its nutritional labeling.

What's more, it set limits on the amounts of salt, sugar and fat that new products could contain. In the processed-food industry, this was practically heresy.

And what was the result of these bold changes?

Disappointing: it was found that consumers still preferred the salt-, sugar- and fat-laden products, so Kraft simply could not afford to forego them.

Clearly, as long as we consumers keep buying unhealthy foods, companies will produce them for us, so the change must come from us.

### 9. Final Summary 

The key message in this book:

**The processed-food industry crams its products full of salt, sugar and fat because we consumers are irresistibly drawn to this trifecta. Unfortunately, this has led to severe increases in obesity, diabetes and heart disease in industrialized countries. The only remedy is for consumers to resist the temptation of salt, sugar and fat.**

Actionable advice:

**Cut down on processed food.**

Rather than trying to separately curb your salt, sugar and fat consumption, why not cut off the problem at the source and stop buying processed foods altogether. Start preparing your own meals from fresh ingredients to rediscover the joy of cooking. If this seems too time consuming, prepare big batches of meals at a time and stick leftovers in your freezer for later consumption.

**Avoid frozen pizza.**

If you're in the habit of eating frozen pizza for dinner every now and then, it's high time to kick it. Pizza is essentially just a vehicle for cheese, which in turn is probably the single biggest contributor of saturated fat to your diet.
---

### Michael Moss

Michael Moss is a Pulitzer Prize-winning journalist who has written for the _New_ _York_ _Times_ and the _Wall_ _Street_ _Journal._

