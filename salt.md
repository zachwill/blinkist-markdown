---
id: 58582864d1a2f3000479688c
slug: salt-en
published_date: 2016-12-23T00:00:00.000+00:00
author: Mark Kurlansky
title: Salt
subtitle: A World History
main_color: BAA9D3
text_color: 61586E
---

# Salt

_A World History_

**Mark Kurlansky**

_Salt_ (2002) tells the fascinating story of this basic mineral, from its early uses in food preservation to its role as a precious commodity, driving trade and conquest. These blinks shed light on the political conflict sparked by society's demand for salt as well as the environmental damage wrought by the salt industry.

---
### 1. What’s in it for me? Discover how a basic mineral dominated nations and inspired revolutions. 

Each day, people like you consume approximately one teaspoonful of white, crystalline rock. Scientists call it sodium chloride; you call it table salt. Whatever its name, it keeps you alive.

You probably don't give a lot of thought to how salt ended up a staple in your kitchen pantry, but centuries ago, your ancestors fought tooth and nail to control its sale and spread.

Salt taxes triggered political uprisings well into the twentieth century. The rise of trading empires such as Venice was based on salt, and Indian independence only became a reality after Gandhi took to gathering salt crystals at the shoreline, against British orders.

In short, any historian worth his salt will be amazed at this mineral's role in shaping human history.

In these blinks, you'll discover

  * how two potentially deadly substances combine to make salt;

  * why the ancient Japanese tossed salt on theater stages; and

  * what an Egyptian mummy and a salted fish have in common.

### 2. Your personal survival depends on salt; human civilization has been shaped by this simple mineral. 

Sitting on nearly every table in the Western world is a shaker of table salt. An ingredient that heightens the flavor of food, salt is truly commonplace and simple. Or is it?

Table salt is actually made up of two volatile chemical compounds. The first is a silvery white metal called _sodium_, also known as natrium or "Na" on the periodic table. Sodium alone is so unstable that it bursts into flame when it comes in contact with water!

Salt's second component is chlorine, or "Cl." When you add chlorine to water, this poisonous gas turns into a dangerous acid!

But when you combine chlorine with sodium, you get NaCl or _sodium chloride_ — a stable, edible compound that we know as humble table salt.

Well, salt isn't so humble — sodium chloride is of vital importance to our health. If you don't eat enough salt, you may suffer from headaches or muscle weakness, and even die.

Thanks to sodium, your heart keeps beating, your nerves can send signals to each other, and your cells can feed themselves. You need salt to stay alive, yet your body can't produce it. Worse, you lose salt everyday — through sweating, for instance.

You might also be surprised to hear that salt has shaped civilizations.

This mineral was the first international trade good and was even used as money because it was considered so precious. It was salt that gave humans a way to preserve food from spoiling.

Salt has also held many different cultural roles. Ancient Egyptians believed salt created sexual desire, so priests refused to eat it. In Judaism and Islam, salt is thought to protect against evil spirits. A similar belief existed in ancient Japan, which is why theater stages would be sprinkled with salt before performances.

> _The body of an average adult contains 250 grams of salt._

### 3. Ancient China and Egypt were the first cultures to discover the wonders of salt. 

China was the birthplace of many innovations, from the invention of gunpowder and the compass to printing and — you guessed it, the extraction of salt.

The Chinese were the first culture to harvest salt by boiling ocean water. Ancient Romans eventually caught on and spread this method of salt extraction throughout Europe, but that wasn't until much later.

As early as 252 BC, the Chinese drilled the first brine well, proving that salt could also be found in the ground and successfully extracted.

The Chinese seasoned their meals with salt-based sauces such as soy sauce and understood that perishable foods such as eggs could be transported hygienically and safely by storing them in salt.

Ancient China also levied the first taxes on salt. By controlling salt sales, the state was able to raise additional revenues to fund wars, expeditions to the West and even the construction of the Great Wall. In fact, Confucius had a position as an intellectual advisor in the governance of salt policy as they developed policies on salt taxation.

Ancient Egypt wasn't far behind China in its salt explorations. Egyptians mined rock salt in the desert and harvested salt by evaporating seawater in the Nile Delta.

They were also responsible for many culinary innovations involving salt. When certain foods are salted, it kills bacteria and draws out moisture. This method allowed Egyptians to "cure" fresh fish, meat and vegetables, which could then be stored for months. They also discovered that olives, which are tough and inedible when fresh, become tasty and tender after a soak in salty water.

Seeing how well salt preserved fresh meat, ancient Egyptians connected the dots and began to preserve human corpses in salt, too. This is how the process of mummifying bodies began. The shared origin of salted fish and mummies wasn't forgotten either.

In the nineteenth century, customs officers in Cairo found themselves scratching their heads when faced with the problem of having to tax mummies before they could enter the city, while having no set policy on how to do so. So, they used the tax on salted fish as a precedent. Mummies were then officially granted entry into Cairo after being taxed as salted fish.

These ancient innovations marked just the start of the rise of salt's use in society.

### 4. Salt made civilizations like the Celts, the ancient Romans and their successors rich and powerful. 

You're probably familiar with the ancient Roman empire, but have you heard of the Celts? These ancient peoples left behind no written records. We only know about the Celts from the annals of their enemies, the Romans.

Ancient Roman writers called these peoples the _Gauls_, after the Greek word "hal," which means salt.

Celtic civilization thrived on salt. Let's quickly travel back to the fourth century BC, when Celtic territory stretched over huge swaths of Europe. The Celts became rich by trading salt and salted goods, including salt-cured ham.

Indeed, the Celts were responsible for creating an industry based on salt. They built saltworks across Europe and developed new techniques for salt mining, such as using bronze instead of iron tools, as bronze doesn't rust.

Despite their success as traders, salt miners and formidable warriors, the Celts were eventually dominated by the Romans in 50 BC with Julius Caesar's victory over Celtic forces.

In Ancient Rome, salt wasn't just an economic good but a political tool. When leaders needed the public's support, they subsidized the price of salt. The empire's armies were often paid in salt. The Roman senate even manipulated salt prices to fund the Punic Wars, which lasted from 264 to 146 BC, a drawn-out fight for supremacy between Rome and Carthage in the Mediterranean Sea.

And when the Roman Empire fell, salt remained influential. Along with Genoa, the city of Venice rose to become a world power, using its growing wealth to buy political influence. By offering merchants subsidies when they brought salt to Venice, Venetian merchants could then afford to import spices, selling them at lower prices than competitors. As a result, Venice dominated the world's spice trade.

Venice also had the fertile valley beyond the Po River to boost its rise to power. This agricultural region produced valuable food exports that required salt for preservation, such as prosciutto, salami and cheese. Venice sold salt to regional producers, then bought the preserved goods to sell later at a profit.

Yet Venice's domination wouldn't last. Once new trade routes to India and the New World were discovered, the Mediterranean lost its role as a trading hub, and Venice was eclipsed as a major power.

In the next blink, we'll move further north to follow the trail of salt.

### 5. Salt, cod and herring are three basic items that shaped the history of medieval Europe. 

Who was the first European to set foot in the New World? Not Columbus, that's for sure. It was the Vikings who first made the long journey across the Atlantic Ocean.

How did they manage it? By building great boats to weather the voyage, and amassing generous stores of salted fish to keep them fed.

They also inadvertently helped others make long voyages. After Vikings had invaded Basque settlements on the northern European coast in the ninth century, these people learned the advanced shipbuilding techniques of the intruders. Once they could build more efficient boats, they could venture further north to fish for Atlantic cod.

But why did these fishermen brave such distances for this basic fish?

Unlike fattier fish that can decompose quickly, cod, with its low fat content, can easily be air dried and preserved in salt. Salted cod was an excellent provision for sailors on long sea journeys.

Meanwhile, by the tenth century, the Vikings had crossed the Atlantic and reached Newfoundland, where there was cod in abundance.

The rest of Europe soon got a taste for salted cod, and the market for this preserved fish became hugely profitable across the continent.

Now the competition for salt boomed. The Vikings established saltworks across France, in places with enough sunlight to efficiently evaporate pools of salt water. They also adopted a method developed by the Venetians, creating salty water of higher and higher concentrations in a succession of artificial ponds.

Cod wasn't the only salted fish on the world market. Salted herring was also fast becoming an important commodity. Between 1250 and 1350, German merchants established the _Hanseatic League_, a commercial guild to facilitate and regulate the salt and herring trade. Members of this league greatly influenced commerce in Northern Europe.

Another trading nation with herring aplenty was Sweden. But this far north, there wasn't enough sunlight to evaporate seawater to harvest needed salt. Instead, Swedes tried to produce salt on the Caribbean island of St. Barthélemy.

The growth in demand for fish drove European nations to explore the New World in search of more. Fish, like salt, had become a crucial commodity.

But despite an early lead, it wasn't the Basques or the members of Northern trading guilds who would become the key influencers in the salt and fish trade. Instead, the great empires of Britain and France rose to prominence as the Atlantic became the sea in the "center" of the world.

> Between the sixteenth and eighteenth centuries, the average salt intake of a European rose from 40 to 70 grams per day.

### 6. The struggle over salt was a catalyst in both the French and American revolutions. 

Much has been made of the role of tea and taxation in the American colonies' fight for independence. But salt also played a role, and a large one at that.

Many early settlers were fishermen who were able to quickly establish a prosperous trading business — and being self-reliant, they harvested salt themselves.

Of course, the British didn't want self-reliant colonists. So they made British salt less expensive than the price of local salt, deliberately to damage local business. Later, when settlers were eager to expand trade, the British only provided enough salt for the colonies to produce goods for themselves and Britain, but not for export.

The British continuously raised taxes on salt; later, imposing punitive tariffs to keep control of the market. This angered colonists; arguments turned into conflicts, eventually sparking the American Revolutionary War in 1775.

During the conflict, Britain even cut off the colonies from salt imports to try to weaken the rebels' resolve. Yet the colonies prevailed, and America was acknowledged as an independent nation in 1783.

The French Revolution was also shaped by salt. Just like Ancient Egyptian and Roman leaders, when the despotic King Louis XVI needed more cash, he raised the salt tax.

Louis XVI led an extravagant lifestyle; so taxes kept rising. Higher taxes fueled citizens' anger, as did arbitrary tax exemptions for certain towns, religious institutions and important figures.

Soon salt smugglers were risking their lives to circumvent high salt prices, potentially facing jail time or decapitation if caught. Regardless, the French were required by law to purchase a certain amount of salt — the _sel du devoir_ — to secure the state's revenue stream.

The story of salt in pre-revolutionary France reflects the suffering and oppression of common people, which eventually led them to revolt. Once a revolutionary government came into power, one of the first decrees was the abandonment of the salt tax.

### 7. Demand for salt shaped the continuing conflict between America and Britain after the revolution. 

After the American Revolution, salt continued to play an influential role in the young United States. Despite having recognized the colonies' independence, Britain sought to restrict US trade.

In 1812, this inspired a new conflict, during which the British tried to destroy all the newly created saltworks in America. But troops couldn't bring the American salt industry down so easily.

Americans had built covered saltworks and even canals that facilitated salt transport. Many saltworks continued to flourish as British competition was stymied when the conflict cut off salt imports.

After the war ended in 1815, towns near saltworks became prosperous business hubs.

Salt also helped determine the course of the American Civil War, from 1861 to 1865. Northern troops made sure to destroy southern saltworks and blocked ports, preventing the south from harvesting or importing salt — one factor that led to the defeat of the Confederacy.

Traveling across the world to India, that country's struggle for independence in the early twentieth century started as a fight over salt.

With the India Salt Act of 1882, the British colonial government was able to establish a salt monopoly, effectively shutting down the local salt economy. As a result, salt was so highly taxed that most citizens could no longer afford it.

The British also responded violently to Indian protests and uprisings, making it illegal to pick grains of salt found naturally on the beach!

By 1930, civil rights leader Mahatma Gandhi decided to channel the burgeoning Indian independence movement into a campaign for salt independence, too.

He asked the Viceroy of India to abolish the salt laws and warned that he and his followers would disregard the law in protest if it remained unchanged. Gandhi then famously marched to the sea to harvest salt, sparking more protests that eventually would help India secure its independence from the British.

Salt has been the fuel of many revolutionary movements. But what is its influence today?

> _"The history of the Americas is one of constant warfare over salt. Whoever controlled salt was in power."_

### 8. Advances in chemistry and technology have helped society better understand salt and its extraction. 

In our first blink, we learned the chemistry of salt, or sodium chloride.

Just two centuries ago, salt's chemical makeup was a mystery. By 1800, however, chemists began to explore _electrolysis_, a process that allowed them to break down liquids into chemical components with an electric current.

With this new method, researchers discovered more about the composition and properties of salt.

In 1807, young Humphry Davy, a self-taught chemist, was the first to isolate sodium using electrolysis. Further experiments helped scientists understand salt's nature as a well-balanced compound created from the reaction of a base, sodium, and an acid, chlorine.

While technological advancements helped scientists better understand salt, the production of salt at the time made great leaps, too. Advanced drilling methods boosted salt production around the globe.

In the process, the new field of geology was born. We soon learned that salt deposits exist in pretty much every part of the planet!

One invention that boosted salt production was the vacuum pan salt process. Developed in the 1880s, this technology saves energy by using chambers within a vacuum, a space with a lower pressure than atmospheric pressure. Liquids generally have a lower boiling point in lower pressure environments; in short, the lower the pressure, the less energy is needed to boil a liquid.

To boil salt brine in a series of vacuum "salt pans" or evaporators, hot steam heats each pan in succession. Each pan maintains a lower pressure than the previous pan, allowing the brine in each to be boiled at a lower temperature in each consecutive pan.

This efficient method soon eclipsed older methods of salt extraction. Salt quickly went from a highly sought-after commodity to a cheap, accessible staple.

Since then, society's relationship with salt has changed a lot. But before we explore this statement, let's look at how industrialization has shaped salt-producing regions.

### 9. The salt industry caused much environmental damage, while salt taxes helped big firms dominate. 

Cheshire, a county in northwest England, harvested salt from brine springs before the Roman conquest.

Several centuries later, salt from Cheshire became a worldwide delicacy and was known as _Liverpool salt_. The regional industry boomed, which as a result had a considerable impact on the local environment.

Once rock salt was discovered in the 1700s, the British government built a network of canals to link Cheshire saltworks to the Mersey River, to transport salt to the port of Liverpool on the Atlantic.

As the eighteenth century drew to a close, many salt refineries were established along the Mersey, creating an early center of industrialization in Cheshire. The area flourished financially, and by 1880, as much as 90 percent of British salt came from the county.

But not all was well. Salt production began to create environmental problems. The air in Cheshire was black with coal smoke. Brine extraction from the ground created huge sinkholes, causing homes to sink and train tracks to buckle.

Things began to change by the late nineteenth century with a shift in the salt industry, not just in Cheshire but globally.

In 1891, Cheshire created a flat tax to pay for the damages the salt industry had wrought. This tax crippled small-scale producers, forcing them out of business. Big companies, however, could pay the tax easily and thus came to dominate Cheshire's salt industry.

The same process was mirrored globally, as international companies began to step up. Salt's historical champions such as France and Britain fell behind as the more industrially minded United States, China and Germany became the new market leaders.

American company Morton, for instance, started as a salt distributor. Business was good, so the company purchased a saltworks in 1910. Even with increased taxation, the company continued to grow. By 1996, Morton was the world's largest salt brand.

> "Wich" is the Anglo-Saxon word for a saltworks. Thus English towns or regions that used to produce salt often have names ending in -wich, such as Norwich.

### 10. Today salt is no longer the precious commodity it once was, but we still can’t live without it. 

It's astonishing how the value of salt has changed over the centuries. Thankfully, we don't fight wars over salt anymore; we also consume much less of it in our food.

Today, just eight percent of the salt production in the United States is linked to the food industry. This is partly because we don't use salt to preserve food anymore, preferring canning or flash freezing.

Freezing, in particular, has sparked a revolution in food preservation. When we freeze foods quickly such as strawberries, the water inside the fruit forms tiny crystals that are small enough to preserve the fruit's freshness and flavor without damaging its texture.

We also know now that too much salt is bad for the body and can potentially raise blood pressure. Most people still use salt daily, however, to enhance the taste of food.

Interestingly, many professional chefs have rediscovered the use of salt, but not the plain, white kind that is mass produced. People now pay a premium for rougher, larger grained or even colored salt from small manufacturers — salt that historically was, in fact, seen as less valuable and of poor quality.

Given this trend, it seems that the traditional saltmakers "won" the salt wars, able to secure a living not through the sheer quantity of salt produced but instead by guaranteeing its quality.

Global cuisine has also contributed new ideas about how to season with salt. Asian cuisines deftly combine sweet and salty flavors. And the technique of seasoning food with salty pickles and sauces rather than pure salt has also become popular in Western cooking.

### 11. Final summary 

The key message in this book:

**Salt in the modern day is a basic, mass-produced product. Yet for millennia, salt was a high-priced commodity that drove culinary and cultural change, from preserving mummies to inspiring exploration to inciting revolution. Many ancient cultures, as well as modern companies, owe their existence to salt.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _Cod_** **by Mark Kurlansky**

_Cod_ (1997) charts the rise and fall of the codfish. A major commodity in the European market during the discovery of the New World, cod went on to cause national conflicts and, due to overfishing, eventually became vulnerable to extinction. Find out how this fish changed the world only to end up on the verge of oblivion.
---

### Mark Kurlansky

Mark Kurlansky is a journalist who also writes fiction, children's fiction and nonfiction books, having authored some ten bestsellers including _Cod_, also available in blinks.

