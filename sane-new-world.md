---
id: 54e2055d306266000a080000
slug: sane-new-world-en
published_date: 2015-02-16T00:00:00.000+00:00
author: Ruby Wax
title: Sane New World
subtitle: Taming the Mind
main_color: 1E8994
text_color: 19717A
---

# Sane New World

_Taming the Mind_

**Ruby Wax**

In _Sane New World_, author Ruby Wax explains how we can better handle the stress of the modern world, pressures our brains just aren't naturally equipped to handle. She outlines concrete steps you can take to curb anxiety, depression and fear and lead a happier life. With dedication, you will learn how to "rewire" your brain, replacing destructive habits with healthier ones.

---
### 1. What’s in it for me? Learn what causes mental health issues and how you can overcome them. 

The stresses of the modern world never stop. We spend more and more time at work, and work itself is more demanding. So we shouldn't be surprised that stress-related illnesses are on the rise.

Yet even though so many people suffer from mental health problems, they often suffer in silence. Illnesses like depression are still largely ignored or misunderstood, even stigmatized, meaning that those who suffer find it increasingly harder to cope on their own.

These blinks help explain the science behind stress and how it affects not only our minds but also our bodies. While we all may suffer from the effects of stress at some point, there are ways to conquer stress and create a positive space where we can live happier lives.

In the following blinks, you'll discover

  * why a work deadline is potentially more dangerous than being stalked by a tiger;

  * how we try to stop working too much by working even more; and

  * how to "rewire" your brain and stop stress before it starts.

### 2. A lack of satisfaction, whether at work or at home, leads us to feel unhappy. 

When was the last time you felt insecure, or envious? It probably wasn't too long ago. We all suffer from negative thoughts, inspired by a variety of reasons.

That inner voice that tells you you've done something wrong developed when you were a child. When we're young, parents and teachers tell us what to do and what not to do. While important as we're growing up, this guidance often morphs into a voice that make us insecure and unhappy.

This critical voice doesn't like change, and doesn't want to deal with new, unpredictable situations.

Imagine you're at a party. You might only speak to people you already know, as your inner voice is telling you that if you talk to a stranger, they might not like you.

Here's the problem: if you're unhappy with your life, your inner voice may be preventing you from taking steps to change it.

When we're unhappy, it's often because we're constantly wanting something yet we're never satisfied when we get it. We may feel temporary happiness, but the moment passes quickly.

Some people deal with this permanent state of dissatisfaction by keeping themselves busy with unhealthy behaviors, so they don't have to think about what they really want in life.

You might want to be a team leader in your company, for example. If you get a promotion, however, you'll initially be happy about your new status and increased income. Eventually, however, you might feel burdened by the increased workload and responsibility.

So you may just try to keep busy with your new job, even if you don't enjoy it. Gradually you'll get burned out, and then feel even unhappier.

> _"No matter how powerful or successful we get, we still can't figure out how to deal with a mind that keeps us up at night, driving us to exhaustion."_

### 3. We're emotionally underdeveloped and thus prone to be unhappy in our modern world. 

Evolution has allowed us to adapt to all sorts of situations. But did you know that basic emotions, like jealousy, are the same now as they were millions of years ago?

Our emotions are underdeveloped, and anger is the clearest example of this.

In an earlier age, anger helped us defend ourselves or get things we needed for survival. It might empower a person to fight a rival for food or a partner, for instance.

Situations like these were relatively brief and could be resolved easily through a fight, so our rage would quickly dissipate afterward.

Today, however, is different. Things that make us angry are mostly related to stress, triggers that are often long-term and ongoing. We're also taught to fight with words, not with fists.

Imagine your secretary forgot to tell you that an important client called four days ago. You might feel angry, but you can only say that you're angry — which won't necessarily make your anger go away.

These days, we carry around lots of angry feelings, yet have no way of getting rid of them.

Feelings of envy are also a basic, primal emotion that can drive us mad.

Envy pushed our ancestors to strive for higher ranks in a clan or other social hierarchy. This privilege could get a person more food, for example, so we sought the highest rung on the social ladder.

Today envy doesn't spur us to be clan chieftain, yet it does push us to strive to be "better" than our peers. This also relates to our continuous desire for more and to be someone special.

But we can't all be a celebrity or a CEO. We long for things we can't possibly ever achieve.

Our primitive emotions thus often drive us to being unhappy. Accepting that your emotions are natural is the first step toward developing a better way to deal with them.

> _"The thing that helped us survive in the past (our alarm system) now gives us nervous breakdowns."_

### 4. Depression is an illness, but it is treatable. 

Some people are touched more by negative emotions than others. One in four people are said to suffer from depression, a condition that as a society we are still struggling to understand.

Depression is often stigmatized. When people suffer from depression, they're often afraid to open up to others about their problems.

A person who wants to curb their depression often feels helpless and struggles with self-loathing. The stigma surrounding their condition only exacerbates this, as depressed people often feel they'll be rejected by others because they are depressed.

This cycle often causes depressed people to feel as if they're alone in their suffering, which can deepen the depression even more.

Depression is actually a serious illness caused by chemical imbalances in the brain. The good news is that it can be treated.

Treatments for depression often include therapy and drugs. Such treatments can help lessen the symptoms of depression, but side effects and the overwhelming feelings of helplessness associated with depression can still linger and cause tremendous mental anguish.

If you're depressed, try reaching out to other people who have experienced depression. You'll see that you aren't alone. People who have suffered may also have meaningful advice on how to get through depressive periods.

Importantly, learn to read your body's signals. Develop practices for calming yourself and regaining your strength.

When the author suffered from depression herself, she developed obsessions. She'd be determined to find a certain pillow, for instance, and couldn't cope until she did.

To overcome this, she began practicing _mindfulness_, or a non-judgmental way of paying attention to one's emotions. This gradually helped her to see her pillow obsession for what it really was: just an obsession.

> _"By 2030...more people will be affected by depression than by any other health problem."_

### 5. Your brain is the main influencer of your behavior, and it is split into three different parts. 

It's critical to accept that _it's not your fault_ when you experience negative emotions or depression.

To gain a deeper understanding of your mental health issues, however, you need to understand how your own brain works.

Though your culture and your family interactions certainly help determine the way you act, the brain is the main influencer of your behavior.

Your brain essentially contains three different parts: the _reptilian brain_ (responsible for primal urges like eating and mating), the _limbic system_ (which helps us nurture children), and the _prefrontal cortex_ (which manages self-control and consciousness).

These parts work simultaneously, so sometimes our actions or thoughts contradict each other. Have you ever wondered how a completely rational person who studies philosophy could fly into a rage if he can't find his keys? That happens when the reptilian brain decides that the situation is a matter of survival, whereas the prefrontal cortex deals instead with intellectual tasks.

Our emotions and behavior are also affected by hormones like _serotonin, dopamine_ and _endorphins_. These hormones increase our energy level, reduce stress and make us seek rewards.

People who have too many or too few of these hormones can suffer. Antidepressants sometimes try to fill in for hormones that are lacking, but this is complicated, and there can be serious side effects.

People who suffer from Parkinson's disease, for instance, lack dopamine in the area of their brain that controls movement. We don't know yet how to send dopamine to that particular area, however, so the whole brain instead as treatment gets an additional dose.

Unfortunately, in some cases this can cause addictive disorders such as alcoholism.

### 6. Feelings of constant stress can make you physically and mentally ill. 

Today it's common for people to feel depressed or burnt out, and blame it on stress.

But why is stress such a big problem?

Humans have always had to deal with anxiety and stress. Our ancestors, however, only experienced stress in unusual or dangerous situations. We deal with it daily!

Our brains retain negative memories more effectively than positive ones, as to react better to a similar situation if it occurs again. So if you hear the growl of a hungry tiger, for example, you'll remember well that scary feeling and know that if you ever hear it again, you better run!

Today, this warning system is useless. We can't run away from the causes of stress, whether work, money or family.

As a result, we're perpetually bombarded with situations that we feel are threatening, but we can't let go of the stress that accompanies those feelings.

For example, if you're working toward a deadline, you might feel anxious about missing it or doing shoddy work. Your brain will store that information. The next time you have a deadline, you'll recall those bad feelings, even if you know you still have enough time to complete your work.

Living in this constant state of alarm has physical and mental consequences. While the psychological stress doesn't actually make us sick, how our bodies react to the stress is the real problem.

Mentally, stress absorbs our energy and willpower and leaves us exhausted and unmotivated. Physically, it inhibits our immune systems, leaving us more vulnerable to disease. And if you get sick, you're likely to get even more stressed!

> _"We are simply not equipped for the twenty-first century."_

### 7. Your brain develops and physically changes when you learn and have new experiences. 

Have you ever wondered why some people seem immune to stress? It's not because they ignore life's problems — it's just that their brains have good strategies for coping with them.

Even though a human brain is made up of the same parts, it develops differently in each individual.

Brains transmit information through _neurons_. Each time you perform an action, the information your brain collects about the action causes the relevant neurons to communicate with each other.

Because of this, your brain consists of an intricate network of many, many connections.

When you learn, the connections between neurons grow stronger and stronger, as the more neurons communicate, the stronger the bonds become. This allows for certain actions to become "automatic."

The more you play the guitar, for example, the better you'll become. The neurons that connect reading music and moving your fingers strengthen over time as you practice. They'll eventually grow so strong that they can transmit information automatically. You'll start playing without consciously moving each finger into position for every chord or note.

This doesn't just apply to learning the guitar. You can purposefully rewire your neural connections to make yourself happier. This is called _neuroplasticity_ — or reshaping your neural network.

Studies have shown that your experiences and the ways you think about them rewire your neurons. So the more you do sports and enjoy doing them, the more you'll naturally see sporting activities as something fun and rewarding.

Neuroplasticity isn't easy, however. It takes a lot of concentration and repetitive work. Yet if you master it, you can quite literally reshape your mind.

### 8. Practicing mindfulness can help you relax and reduce your stress. 

There are many ways you can reduce stress, and practicing _mindfulness_ is an effective solution.

Mindfulness stems from Buddhism, but it developed as a therapeutical practice in the 1980s and 1990s. The father of mindfulness therapy is Dr. Jon Kabat-Zinn, who worked with people suffering from chronic diseases. He had them focus on their pain rather than trying to distract themselves from it, which actually helped reduce painful sensations and stress.

The method was further developed by Mark Williams, John Teasdale and Zindel Segal, who applied it to emotional pain. Together, they founded m _indfulness-based cognitive therapy_ (MBCT).

MBCT has a patient come face to face with their anger, fear or other negative emotions, with the goal of gradually understanding that such feelings are constantly changing, coming and going.

Eventually, a patient will see these emotions as separate from themselves, so they'll be able to disassociate from them and feel better about themselves.

More specifically, mindfulness is about self-regulating thoughts and emotions. It's about accepting that your mind is always in flux, and learning to observe your thoughts without being too critical.

So strive to look at your inner world more objectively. Acknowledge your emotions, and don't hide from them. When you reach a state of total awareness, you'll be able to process your past and current experiences in a more straightforward manner.

A perpetual state of stress can make you nervous and anxious. When you don't deal with your emotions, you risk falling into a vicious cycle. It's true; many people, for example, work more to distract themselves from the stress of working!

Don't fall into that trap. Instead, take a step back. When you get a clearer perspective on your bad behavior, you'll be able to overcome it.

> _"Pain exists, but suffering is optional."_

### 9. Practicing mindfulness on a daily basis will make you more attentive and self-aware. 

Mindfulness can help you gain control over depression and other problems, but it requires dedication. It's a good idea to do some mindfulness exercises every day.

Here are two you can try yourself!

The first exercise helps you identify your emotions then disengage from them. It's called _Recognition, Acceptance, Investigation_ and _Non-Identification_, or _RAIN._

First, you _recognize_ what's troubling you. It could be the fear of not meeting a deadline, for instance. Clearly identifying the problem will help to calm you down.

Next, _accept_ the fear. Remember that it's just an emotion, and you can take charge of it.

Then try to _investigate_ what exactly this negative emotion is doing to your body. Does it make your chest tighten? Are you losing sleep? Focus on your physical sensations and you'll understand your feelings more.

Finally, step away from the emotion. _Don't identify_ yourself with it, but let it pass.

The second exercise — _stopping and noticing_ — is quite simple, but you can also expand on it. Stopping and noticing calms your mind and strengthens your focus.

Every now and then, _stop_ for five seconds and _notice_ what's happening in your mind. You can do it before or after eating, or while you're brushing your teeth.

Our minds all wander, and we slip into automatic pilot as we go through our day. This exercise helps bring back your focus.

Concentrate on one of your senses. Close your eyes and listen to the sounds around you, focus on your breath, or how the chair you're sitting in feels. This will help you detect any negative emotions, then you can start dealing with those emotions more directly.

The more these exercises become a part of your routine, the more you'll break your destructive habits and establish new patterns of healthy behavior.

> _"You, and the way you see the world, are the architects of how your brain is mapped."_

### 10. Cognitive behavioral therapy can help you evaluate how you think, to eliminate bad patterns. 

Practicing mindfulness may not be for everyone. We're all different! Other people may prefer yoga, reading or simply taking a long walk.

The most important thing is to find your personal _anchor_. Your anchor is what calms you down when your emotions are troubling you.

Whatever stress-reducing technique you prefer, it's important that it heightens your awareness and attention to yourself. You need to be aware of your emotions to fully live in the present.

It's also essential that you keep repeating your stress-reducing techniques, or else neuroplasticity — the "rewiring" of your brain — can't take place.

Apart from mindfulness, _cognitive behavioral therapy_ (CBT) is useful in analyzing yourself. It helps you re-evaluate how you think, so you can change the patterns that result in dysfunctional behavior.

CBT allows you to assess your present situation. To practice CBT, take a moment and ask yourself the following questions:

_What's happening now, and how does it make you feel?_ You might be late for work, for example, and feel anxious or ashamed.

_What are your automatic thoughts and physical sensations?_ Are you sweating? Is your heart pounding? Do you instantly feel like a failure because you're always late?

_What other information might balance the situation and how can you prevent it in the future?_ Did you oversleep? That can happen to anyone, and the office isn't going to burn down without you. Planning ahead, you could get two alarm clocks, or make sure your partner will wake you up if you sleep in.

Answering these questions will help you avoid being too tough on yourself, and you'll be better equipped to find a realistic solution to your problem.

### 11. Final summary 

The key message in this book:

**We all struggle with emotional or mental issues, whether big or small. Our brains are simply not built for the constant stress we're subjected to in modern society. However, if we practice stress-reducing techniques, we can literally restructure how we think and cope to lead happier and healthier lives.**

Actionable advice:

**Listen to yourself.**

Before you can overcome negative emotions, you have to understand them in the first place. Take time each day to pause, shutting out the rest of the world as best you can, and focus on yourself. The better you understand yourself and your feelings, the more able you will be to make the positive changes you need.

**Suggested further reading:** ** _Rewire_** **by Richard O'Connor**

_Rewire_ is about why we sometimes fall into self-destructive behavior, and how to move past it.

It delves into the brain activity behind addictions, and outlines strategies for rewiring yourself for improved self-control over your bad habits.
---

### Ruby Wax

Ruby Wax is an actress, screenwriter and comedian. A depression sufferer herself, Wax initially studied mindfulness-based cognitive therapy and other psychotherapeutic practices for her own benefit, and now brings the success of her studies to others.

