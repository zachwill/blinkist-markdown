---
id: 58446967a6ba9500046bed92
slug: saving-capitalism-en
published_date: 2016-12-07T00:00:00.000+00:00
author: Robert B. Reich
title: Saving Capitalism
subtitle: For the Many, Not the Few
main_color: 239E5F
text_color: 186B40
---

# Saving Capitalism

_For the Many, Not the Few_

**Robert B. Reich**

_Saving Capitalism_ (2015) is a biting critique of the world's economic order but also an optimistic look into how capitalism could support the common good. These blinks will teach you how and why capitalism is failing most people, and where it needs to go to do right by the majority.

---
### 1. What’s in it for me? Learn why capitalism needs to be saved for the good of all citizens. 

Capitalism is a social system adopted by many Western countries, based on the principle of individual rights. When applied to production, this system results in so-called "free" markets.

The problem is, free markets don't necessarily work for the good of all society. In fact, capitalism as it is practiced in today's economic world is a system that benefits the few at the expense of the many.

These blinks explain in detail why this is the case, and how people living under such systems can change the status quo. The bottom line is that the middle class has been squeezed for far too long, and it's time to push back — both economically and politically.

In these blinks, you'll also learn

  * how governments value property rights over health and welfare;

  * how Donald Trump hid behind bankruptcy protection while laying workers off; and

  * why the notion that people are "worth" what they're paid is nonsense.

### 2. So-called “free” markets simply can’t exist without government and the rules it sets to guide them. 

Unless you happen to live under one of the world's few communist regimes, you will be well-versed in the workings of capitalism — an economic system of private property and markets, ordered by supply and demand.

And if you live in a country with a conservative political majority, these markets are likely "free," that is, unencumbered by undue government regulation.

However, there's a problem with the conservative zeal for so-called "free" markets, namely that it doesn't account for the fact that governments are actually _creators_ of markets.

Nonetheless, the commonly held belief is that government intervention distorts markets, thereby reducing their efficiency. People who hold this belief assume that the market knows best, and thus that government oversight should be minimal.

But here's where we run into problems.

There simply can be _no_ free markets without government. Markets do not exist in a vacuum, and governments write the rules that create both markets and define societies. So, just to exist, every market needs a government that writes and enforces its rules — rules which shape the building blocks of capitalism.

These rule-based building blocks include _property_, _monopoly_, _contracts_, _bankruptcy_ and _enforcement_.

Property refers to rules regarding what can be owned. That's because making or buying something doesn't necessarily mean you can own it. For instance, we have laws that prohibit owning a nuclear bomb. But it's also necessary to have rules governing intellectual property, such as music.

Monopoly is a set of rules defining how much market control any one entity can have. For example, these rules govern how large and economically powerful society will let a company become.

Contracts define what can be bought and sold while agreeing to the terms of sale. That's because buying and selling commodities require more than merely setting a price. When selling regulated commodities such as drugs or food, for instance, it's essential to have a contract that dictates clear rules for the conditions of the sale.

Bankruptcy is the rule responsible for handling situations when purchasers can't pay up. And finally, enforcement is necessary to ensure that people abide by all the other rules.

Now that you know the five building blocks of capitalism, let's take a look at how they play out in America's economic system.

> _"Government doesn't 'intrude' on the 'free market.' It creates the market."_

### 3. The rules regarding property rights and monopoly status depend on political decisions. 

Some people are in favor of the total abolition of private property, but private ownership can be advantageous. Have you ever washed a rental car? Likely not, because we don't own rental cars.

Not just that, but owners often take care of and maintain _their_ property, whether a car, a house or a piece of land. So which do you think is better, private or common property?

Actually, it all depends on the context and on political decisions. The main issue around private property is how it's defined by government, and what exactly property ownership entails. In other words, what can be owned, under what conditions and for how long?

In the case of intellectual property, it's necessary to define how one can own something that's immaterial. Drug companies, for instance, receive intellectual property rights for the exclusive production of drugs that they patent.

But politicians have to decide how long a company should be able to hold such exclusive rights. If a drug is particularly beneficial to society, the government should make it accessible to all citizens who need it, ideally at a reasonable price.

Unfortunately, this isn't how it works in reality. Single companies produce countless lifesaving drugs and sell them for exorbitant prices. That's because such companies can renew their patents based on legal loopholes, such as small changes to a patent that technically make an existing drug "new." In essence, the government has deemed private property rights more important than the accessibility of lifesaving medicine.

And monopolies are similarly dependant on politics. Consider Amazon's de facto monopoly in the American book market, which it achieved by enabling consumers to save money and shop from the comfort of their home. Yet Amazon's market clout also gives it significant control over publishing houses.

In 2014, to gain better terms for its deal with book publisher Hachette, Amazon just stopped delivering the publisher's books. Nonetheless, whether Amazon's monopoly benefits or damages the common good is a political question, one that's decided through antitrust law designed to promote fair competition in the market.

> _"Liberty produces wealth, and wealth destroys liberty."_ — Henry Demarest Lloyd, nineteenth century American progressive political activist

### 4. Laws governing contracts, bankruptcy and enforcement disproportionately favor the wealthy. 

The three remaining building blocks of capitalism all have one thing in common — they all favor the hyper-wealthy. Here's why this is the case.

Major corporations and rich people can force others into unfair contracts. While it may be immoral to push people into deals they don't want, it still happens all the time, as firms or individuals leverage their financial power and insist on provisions that partners may find unfair.

For instance, a common condition of modern employment contracts is that all complaints are dealt with through an arbitrator, and that the arbitrator's judgment must be accepted without court involvement.

Arbitrators, however, tend to be hand-picked by employers, which creates an unfair bias. As a result, employees have no choice — because rejecting the provision would leave them jobless.

But that's not the only way our system benefits corporations and the wealthy. The rules also protect them from major losses through bankruptcy, a protection not afforded to employees.

For example, in 1984, Donald Trump opened Trump Plaza in Atlantic City. Some 30 years later, when the hotel shut down, 1,000 workers were laid off.

And Trump? He got away scot-free.

That's because corporations and the super-rich, like Donald Trump, can avoid the consequences of financial missteps by declaring bankruptcy, thereby limiting their liability as well as that of their companies.

On the other hand, people who move to places like Atlantic City for jobs have no such protections.

And the list goes on, as corporations and the rich can also use their financial power to undermine the enforcement of laws they don't like. One way to do this is to make laws essentially useless by depriving institutions of the money necessary for enforcement.

For example, American big business focuses a lot of lobbying power to defund the Occupational Safety and Health Administration, the government agency tasked with enforcing labor rights.

> _"When large corporations have disproportionate power … those who are relatively powerless have no choice."_

### 5. Capitalist society has tricked workers into thinking they’re not worth more than what they’re paid. 

Do you feel most people earn what their efforts and abilities are worth?

That's the idea behind a _meritocracy_, a commonly held assumption of capitalist societies, which says that people are paid according to their skills. For instance, the author once spoke to a group of power-plant employees who were debating whether to unionize.

One employee, intending to vote against unionization, said that he didn't think his labor was worth more than the $14 an hour he was already being paid. He went on to say that he was happy for those who make millions of dollars, and that if he had gone to school, or been smarter, he could be rich, too.

This story illustrates how plenty of low-wage workers assume that low pay is their own fault. They view their meager paycheck as a reflection of personal failures, or lack of intelligence. It's precisely this logic that enables people who earn a lot to think of themselves as exceptionally hard-working, more intelligent and even superior to other, poorer people.

But are the hyper-wealthy, people such as hedge fund manager Steven A. Cohen, who netted $2.3 billion in 2013, actually worth the money they make?

Absolutely not!

The idea that a human being is "worth" what he or she earns is entirely untrue. For starters, lots of factors other than a worker's personal ability determine what a person is paid. For instance, financial inheritance, personal connections, discrimination, luck and even marriage can all affect your salary.

Furthermore, if the paychecks of people doing social work, teaching, nursing or caring for the elderly truly reflected the social benefits of such professions, these workers would be earning a lot more than they are!

And finally, the idea of meritocracy can in no way explain the vast increases in CEO pay over the last few years. In 1965, for instance, a CEO earned 20 times more than the average worker. Today, a CEO earns over 300 times as much!

> _"The prevailing notion that individuals are paid what they're 'worth' is a tautology … . Most fundamentally, it ignores power."_

### 6. The bargaining power of the middle class has declined, while the number of full-time working poor has increased. 

The three decades following World War II witnessed a steady increase in the standard of living in the United States. During these years, a baker or mechanic could earn enough to buy a home, a car and raise a family.

However, beginning in the 1980s, the average household income in America stopped growing.

It's clear from this wage stagnation that the bargaining power of the middle class, its ability to negotiate for fair employment conditions and wages, has decreased.

Why?

One reason is the downfall of unions. For instance, the Walmart workers of today, as well as those of major fast-food chains, don't have unions to help negotiate higher wages. In fact, fewer than seven percent of all private-sector workers in America today are unionized. That means that most employers don't have to abide by union contracts, which work to ensure stronger labor rights.

Not just that, but high unemployment has forced workers to settle for lower wages. In fact, even full-time employees who have dedicated themselves to a company for decades can easily find themselves out of a job overnight, cut loose with no severance check, no job placement assistance and no health insurance. The economic insecurity that this causes puts workers in an even worse position to negotiate for higher wages.

But beyond low wages is another catastrophe: the number of full-time employees who still struggle to get by has risen. So while you may think that only the unemployed struggle with poverty, that's absolutely not the case.

Companies often boost profits by cutting labor costs, resulting in a continuous decline in wages. For instance, in 2013, one-fourth of all American workers held jobs that, even when worked full-time, paid below what was necessary to support a family of four and still exist above the federal poverty line.

> _"It is no great feat for an economy to create a large number of very low-wage jobs. Slavery, after all, was a full-employment system."_

### 7. The declining economic and political power of the middle class is unsustainable and a threat to the economy. 

So now you know how the demise of unions and the rise of major corporations has cost the American middle class its power to negotiate fair wages. But the economic and political power of the country's middle class has also been falling in general.

In fact, America is now a country of extreme wealth and income concentration.

For instance, the richest 400 Americans hold more wealth than the bottom 50 percent of the country put together. Not just that, but the wealthiest one percent own 42 percent of the nation's private assets.

Meanwhile, political power has grown increasingly concentrated. For example, a 2014 study by professors Martin Gilens of Princeton University and Benjamin Page of Northwestern University shows that the desires of the average American citizen have almost zero impact on policy decisions.

However, this decline in middle-class power, both economic and political, can't go on forever. That's because, from an economic perspective, as middle-class and lower-class incomes drop, these groups will lose the purchasing power necessary to keep the economy afloat.

But beyond that, the current inequities also undermine the economic system in several ways. For instance, if a majority of people view market rules as biased in favor of the rich, they will begin to believe that it's okay for them to ignore those rules, by accepting small bribes, overbilling or skimming profits.

This could mean big trouble, since economies are based on trust; violating this trust, even in the smallest ways, can have major consequences.

As trust breaks down, business deals will require ever more elaborate contracts, meaning a greater number of lawyers will need to be paid. But a bigger issue is that, politically speaking, high concentrations of wealth can provoke upheaval. Just look at the American populist revolts of the 1890s, which grew out of frustration with widespread government corruption.

In this way, the economic and political impotence of America's middle class will ultimately threaten the functioning of the country's capitalist system.

But while capitalism could be in trouble, it's still possible to save it. Read on to see how!

### 8. To save capitalism, we need to create a new political party and reinvent the role of corporations. 

The American economy can't remain afloat if the richest 10 percent keep growing richer while the bottom 90 percent keep getting poorer. And America's democracy doesn't stand a chance if the majority has no political voice.

But how can the economic and political power of the middle class be restored to save capitalism?

Capitalism can be saved through the formation of a new political party. For instance, did you know that the largest political party in the country is neither the Republican Party nor the Democratic Party, but the party of nonvoters?

Just consider the 2012 presidential election. Only 58.2 percent of eligible voters exercised their right to vote.

A third party could be founded to unite apathetic voters, returning a political voice to disenfranchised Americans. This party should endeavor to enable the economic success of the country's majority.

But to do this, the party would need to reform America's system of campaign financing, which currently allows wealthy individuals to leverage their money to influence politicians. Beyond that, the party would need to raise the minimum wage, give priority to labor agreements instead of creditor agreements and limit the size of Wall Street's gigantic banks.

When that's completed, the corporation too will need to be reinvented. As the system is set up today, the financial interest of corporations means lower pay for the average worker and extremely high pay for executives.

One strategy for changing this system would be to tie corporate tax rates to the ratio of what a CEO makes compared with the pay of an average worker. The greater the difference, the higher the tax. This would give corporations an economic incentive to increase the average wage of employees.

Capitalism is not lost. Yet if it is to survive, it will have to be reorganized to better distribute its profits.

> _"I believe that … we can make capitalism work for most of us rather than for only a relative handful."_

### 9. Final summary 

The key message in this book:

**Markets depend on government to exist, but current state-sanctioned economic systems that are defended by governments of capitalist countries grossly favor the wealthy. This has meant huge increases in wealth disparities and a loss of democratic control by the middle classes, a major threat to civil society overall. If capitalism has any future, it must be one of greater equality.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _PostCapitalism_** **by Paul Mason**

_Postcapitalism_ (2015) offers a close examination of the failures of current economic systems. The 2008 financial crisis showed us that neoliberal capitalism is falling apart, and these blinks outline the reasons why we're at the start of capitalism's downfall, while giving an idea of what our transition into _postcapitalism_ will be like.
---

### Robert B. Reich

Robert B. Reich is the Chancellor's Professor of Public Policy at the University of California at Berkeley. Prior to this, he served in three presidential administrations, most recently as labor secretary under former US President Bill Clinton.

