---
id: 516bc091e4b01b686deb9b1d
slug: savor-en
published_date: 2014-01-22T12:00:00.000+00:00
author: Thich Nhat Hanh and Dr. Lilian Cheung
title: Savor
subtitle: Mindful Eating, Mindful Life
main_color: F17130
text_color: A64E21
---

# Savor

_Mindful Eating, Mindful Life_

**Thich Nhat Hanh and Dr. Lilian Cheung**

_Savor_ (2010) provides advice and inspiration on how to find inner peace, joy and strength — especially for those trying to sustainably lose weight — with Buddhist teachings and techniques for appreciating the richness of life in the present moment. It also draws on the latest nutritional science research on the best ways to eat and exercise, presenting readers with a holistic method for improving their physical, psychological and spiritual well-being, and thereby transforming their lives.

---
### 1. Buddhist teachings backed up by nutritional science provide an effective, holistic method to tackling your weight problem. 

There is a growing, multi-billion-dollar weight-loss industry in the West, but, despite this, rates of obesity have increased to epidemic levels. Many people spend years struggling to lose weight with fad diets and pills, often with very little success. Clearly, the current market solutions are failing to solve their weight problems. 

The reason the weight-loss industry is not solving people's weight problems is that it pays little attention to the deeper issues of physical, psychological and spiritual suffering. This suffering is not just the individual's responsibility; it is also the result of wider societal influences.

Society today encourages more eating and less exercise — mass advertising promotes excessively salty, sugary and fatty foods, while meals have become oversized. At the same time, people's lifestyles have become much more sedentary. For example, the average North American home contains more TVs than people, and studies confirm that spending several hours a day in front of the TV is highly correlated with obesity.

Given these issues, it is evident that to tackle your weight problem you need to be aware of how forces in society are impacting your own life; namely, how they encourage an unhealthy lifestyle that causes you to suffer, and how this suffering in turn causes you to eat more. If you reflect deeply on this vicious cycle, then you can begin to resist it.

This is where a Buddhist approach gets to the heart of the problem. It teaches you how to gain deep _insight_ into the suffering that underlies your unhealthy lifestyle. This insight enables you to take control of your thoughts, feelings and behavior so that you can transform your life.

Combining the approaches of Buddhism and the advice of nutritional science, you have a sustainable, holistic method to losing weight and transforming your life.

### 2. Buddhism teaches four fundamental steps that will help you understand your weight problem and transform your life. 

In order to overcome your unhealthy eating habits, you need to transform your life. To do this, it is best to be part of a supportive community who'll help you make this transformation. It is also important to try for small successes rather than to try taking quantum leaps.

Buddhist teachings help you to make this transformation in four basic steps:

The first step is to acknowledge that you are _suffering_. Being overweight often causes high blood pressure, aching limbs, diabetes and various other health conditions, as well as social problems such as stigma and bullying.

The second step is to become aware of the _root causes_ of your suffering. Reflect deeply on your life: Do you realize how unhealthy your eating habits are? Are you aware of the fact that negative emotions may be compelling you to overeat? In this process of reflection, treat yourself compassionately by embracing your bad feelings instead of judging them harshly.

The third step is to realize that you _can_ lose weight. If you have failed in the past, that does not matter, because the past no longer exists; what matters is the journey you are on now, in the present.

The fourth step is to practice living _mindfully_, which means being fully aware of your body and mind in the present moment, rather than being distracted by thoughts of the past and the future. Living mindfully is the only way to regain control of your life; it makes you conscious of all your actions.

Remember that these four steps are not designed to tell you "the truth" — they will simply help you to awaken the truth that is already within you.

### 3. To overcome your unhealthy eating habits, learn to calmly focus on your mind and body in the present moment. 

Most of us go through our daily lives being very distracted by thoughts of the past and the future. Such thoughts tend to overwhelm us with worries, concerns and regrets. This means that we struggle to focus our attention on the present moment.

This lack of awareness of the present moment is called _mindlessness_. When we live mindlessly, it's a bit like operating on autopilot, and operating on autopilot prevents us from noticing feelings, desires and habits as they develop in us.

This lack of awareness is what causes us to fall into unhealthy habits such as overeating. Unhealthy habits cannot provide long-lasting satisfaction. In fact, they create both physical and psychological suffering: our bodies become unhealthy and our minds become unhappy.

But it is possible to overcome all unhealthy habits so that you can begin to experience happiness and peace. The way to do this is to stop living mindlessly: learn to focus your attention on the present moment. In this way, you will become more aware of everything in your life: your movements, your feelings and your eating habits. This way of calmly focusing your attention on the present is called _mindfulness_. 

If, for example, you practice mindfulness while you eat by focusing only on your food instead of other things, your meals will become a spiritual experience because you will start to deeply appreciate their taste and healthfulness. When you start to appreciate healthy food in this way, you will no longer have cravings for unhealthy food, and you will be able to stop eating when you are full.

This transformative power of mindfulness allows you to overcome the unhealthy habits that cause your suffering, and bring your attention back to the richness and joy of life in the present moment.

### 4. Since we are connected to everything and everyone on the planet, we must be fully aware of how we consume resources. 

We are used to thinking of ourselves individualistically, like separate islands disconnected from others and from our environment. But this belief is an illusion — we are, in fact, all connected.

Consider how eating a simple orange can connect you to the entire world: the orange grows on a tree that someone has planted; the tree grows out of the soil, with the help of water and sunlight; many people are involved in picking, packing and transporting the fruit, and all of them are influenced in turn by numerous other people. In this way, everything and everyone is ultimately connected to the orange.

Unfortunately, many people are not aware of these connections. Therefore, they consume without considering the well-being of people and the planet.

A good example of this is people who eat lots of meat, which is a huge drain on the planet's resources. It takes 100 times as much water to produce a kilogram of beef than a kilogram of protein from grain, and raising livestock also contributes massively to climate change. Additionally, meat production usually involves the inhumane treatment of animals. Therefore, eating meat is a selfish act, one which damages the planet.

As you come to realize that what you eat connects you to your community and, ultimately, to the whole planet, you will understand that your eating habits should not be selfish. This may inspire you to move away from excessive meat consumption and toward a diet richer in vegetables, beans and grains. In this way, you will be helping to sustain a healthier planet with enough food for everyone.

By looking beyond the rim of the plate as you eat, you acknowledge and cherish your connection with everything.

### 5. Losing weight and transforming your life isn’t just about how you feed your body; it’s also about how you feed your mind. 

Buddhism recognizes that "you are more than what you eat." Rather than just considering how we feed our bodies with food, we must also consider how we feed our minds.

One of the things that feed our minds is _sense impressions_ — for example, the sights, sounds and smells received through our sense organs. Our minds can be fed with certain senses intentionally, as demonstrated in the way the mass media and advertisements create many sense impressions to direct at us, some of which make us happier and more compassionate, and others of which make us anxious, angry or sad.

Another thing we feed off is _volition_. This is our deepest will or desire to obtain something. Craving food, money or fame will probably never allow us to feel satisfied, because cravings are unhealthy desires. On the other hand, the desire to be compassionate toward all living beings is likely to create joy and peace in our lives, because compassion is a healthy desire.

We also feed off various things in our _consciousness_, which has a deep, invisible level and a conscious "awake" level. All our experiences and perceptions have an impact at the deepest level, and eventually the effects of this impact rise up into our conscious mind — as either positive feelings of love and compassion, or negative feelings of anger and hate. Our conscious mind needs to encourage the positive feelings to grow instead of the negative ones.

To do this, we need to be _mindful_ of all our sense impressions, desires and conscious feelings. This will ensure that we do not feed our mind with negativity, because _mindfulness_ dissolves negative feelings while encouraging healthy desires, like compassion, to grow.

When we become more mindful, we become more healthy not just in body but also in mind.

****

### 6. To eat healthily, you need to think carefully about the quality rather than quantity of the nutrients you consume. 

Unfortunately, the market for weight-loss diets is full of contradictory advice. For example, some diets claim that carbohydrates are bad, while others claim that you can eat as many carbs as you like because it's actually fats you should worry about! 

In fact, it is best to ignore these extreme diets and instead learn from modern nutritional science, which teaches that, rather than eliminate an important macronutrient from your diet, you should focus on the quality — and not just the quantity — of these nutrients: choose foods with good carbs, good proteins and good fats, not bad ones. 

So where do we find food with good-quality nutrients?

The healthiest carbohydrates come from whole grains, legumes, vegetables and fruits, whereas the carbohydrates from white bread, white rice, pasta, sugary foods and potatoes are much less healthy.

Another essential nutrient is fat, but it's important to distinguish between good and bad fats. A simple rule is that monounsaturated and polyunsaturated fats (such as those found in olive oil, nuts, avocado, fish, tofu, and so on) are generally healthy. These sources often also contain healthy Omega-3 fats, which are extremely beneficial for the heart. Saturated fats, on the other hand (such as those found in red meat and dairy) must be limited, while trans fats (found mostly in deep-fried fast food) are best avoided altogether.

Proteins are best sourced from beans, nuts, seeds and whole grains, because these foods contain more healthy fibers, vitamins and fats. While red meat has lots of protein, it also contains lots of unhealthy fat, which is why you should limit how much red meat you eat.

### 7. Make regular physical exercise a central part of your mission to lose weight and lead a healthy life. 

No matter what "miracle" cures are advertised on the market, the fact remains that you have to balance energy-in and energy-out to maintain a healthy weight, which means you need to exercise to burn off calories; and the harder and longer you work out, the more weight you will lose.

In addition to helping you lose weight, regular exercise can bring many other health benefits. In adults, it lowers the risk of diabetes, heart disease and other conditions. It also boosts mood, reduces stress and extends your life. In fact, the weight-loss pills and programs on the market may claim to be "the perfect cure," but regular exercise is the closest to a magic potion that you can get!

So, as exercise is so essential to good health, how much do you actually need to do?

The US federal guidelines are that adults should get 2.5 hours of moderate-intensity aerobic exercise per week. This can be anything from brisk walking to ballroom dancing to gardening. It is also essential that after being active you get good rest: establish a regular sleep rhythm, and avoid heavy meals and caffeinated drinks before you sleep.

While this aerobic exercise helps you to lose weight, it isn't the only type of important exercise. You also need to focus on exercises that increase your strength and flexibility so you can build muscle, improve agility and avoid injury while doing aerobic exercise. These exercises can be as complex or as simple as you like. For example, you can do push-ups a few times a week for strength training and get in some stretches, every day if possible, for flexibility.

### 8. Think carefully about how you can break free from bad habits that keep you trapped in an unhealthy lifestyle. 

We all have habits that cause us to do things that we know we shouldn't do, such as unhealthy night-time snacking. If you want to transform your life, you need to try to conquer those bad habits.

The first thing you should do is change the habits that stop you from being _mindful_ of the present moment. For example, if you realize that you have a habit of watching TV mindlessly for hours, then try to stop yourself as you reach for the remote control, breathe calmly, and ask, "Why am I about to watch television?"

You may also find that your TV, cellphone or newspaper distracts you while you eat. Try to put these distractions away for at least one meal a day so that you can focus completely on your food. When you are focused on your food, you will be able to eat more slowly so that you know when you are full, and you will be more aware of emotions like boredom or anger that might be compelling you to eat.

This awareness makes it easier to keep your eating habits under control.

Another important habit you need to be aware of is the tendency to make up excuses so that you can avoid physical exercise. For example, you may tell yourself that you "don't have time" to exercise. If this is the case, then try to proactively "find the time" — wake up earlier, perhaps, or do some exercise during your lunch break.

If you feel that you're slipping back into old habits you thought you'd successfully conquered, don't give up all your efforts. Simply be _mindful_ of your actions again — this will allow you to become motivated anew so that you can get back on track.

### 9. Develop eating and exercise strategies to help you see your weight-loss plans through to the end. 

There is a wealth of good advice telling you how to sustainably lose weight; however, it's not always that easy to put it into practice. The best way to keep yourself on track and really make changes in your life is to keep a written record of your goals and progress.

These records can be separated into an _In_ Eating strategy and an _In_ Moving strategy.

Your _In_ Eating strategy should be a motivation for you to eat healthily and _mindfully_. Write down why you think a healthy diet is desirable, what obstacles are preventing you from reaching it and how you can remove them. For example, if you feel that night-time snacking in front of the TV is an obstacle, you can write a plan such as, "I will make an arrangement with a friend that we will call each other as a reminder to turn off the TV after an hour and get a good sleep."

Your _In_ Moving strategy will help you to get sufficient physical exercise while being mindful of your movements. Reflect on why exercise is important, consider what activities you actually like to do and write down your weekly exercise goals. Try to reach the same goals for four weeks, and then create a new set of harder goals for the next four weeks.

Your first goal might be to exercise for 20 minutes a day, say to walk for about 2500 steps. Your next four-week goal could then be to walk for 40 minutes a day, taking about 5000 steps, and so on.

By coming up with these eating and exercising strategies, you will be able to identify the problems you're having, think of solutions to them and then stick to your plans, making it much easier to transform your life.

### 10. Final Summary 

The main message of this book is:

**The obesity problem in modern society cannot be solved by fad diets. These neglect the important societal problems which cause us to overeat and refrain from exercise. Only by tackling these problems can we hope to live a healthier lifestyle. The Buddhist approach provides us with a solution: it allows us to truly reflect on all the problems that cause us to become unhealthy, and helps us deal with them by living only in the present moment. Combined with modern nutritional science, it provides the best way to living a healthy and sustainable lifestyle.**

Actionable ideas from this book in blinks:

**A healthy diet tip: Whole grains are the perfect food for having a healthy lifestyle.**

If you want to eat a healthy diet, whole grains are a great place to start. Whole grains are particularly good for your health, possibly because of their specific combination of nutrients, which protects you against heart disease, diabetes and cancer.

**Follow the seven practices of a mindful eater.**

If you want to eat mindfully, you must remember it takes dedicated practice. To help you stay with the goal, try these seven steps to eating mindfully:

1\. Honor the food — Keep mealtime conversations focused on the food you are eating; do not argue about other matters. In Vietnam, it is customary to avoid criticizing someone while they are eating so that they can digest peacefully.

2\. Engage all of your senses — Enjoy the stimulation of the sounds, smells, colors, textures and tastes of the meal.

3\. Serve food in modest portions — Modest portion sizes help you to avoid overeating, and preserves the planet's resources.

4\. Savor small bites and chew thoroughly — Small bites allow you to enjoy the food more, and chewing thoroughly helps you digest it better.

5\. Eat slowly — If you eat slowly, you will stop when you are satisfied, without accidentally overeating.

6\. Don't skip meals — Skipping meals can make us hungry so that we impulsively grab unhealthy food.

7\. Eat a plant-based diet — Look beyond the rim of the plate. Not only are red meats and processed meats unhealthy for the body, they also take a huge toll on the environment. According to some University of Chicago researchers, being vegetarian does more to prevent global warming than swapping your Camry for a Prius.

### 11. How to meditate 

**In order to practice mindfulness, it is best to use meditations:**

There are many meditations you can try to help you live mindfully. Meditating trains you to avoid distractions and focus your mind on the present moment.

Try the following when you're faced with the listed difficult situations or negative emotions:

**The Traffic-Jam Meditation**

No one likes being stuck in heavy traffic, and it can make us very angry. To stop yourself become distracted by this anger, practice the following meditation:

_Breathing in, I follow my in-breath._

_Breathing out, I follow my out-breath._

_Breathing in, I know everyone is trying to get somewhere._

_Breathing out, I wish everyone a peaceful, safe journey._

_Breathing in, I go back to the island of calm in myself._

_Breathing out, I feel refreshed._

**The Anxiety Meditation**

When you are feeling anxious or things are getting too much for you, try the following meditation:

_Breathing in, I know that this unpleasant feeling has arisen within me._

_Breathing out, I know that this unpleasant feeling is present in me._

_Breathing in, I am feeling anxious._

_Breathing out, I cuddle my anxiety._

**The Walking Meditation**

At least once a day, find a space where you can walk mindfully. As you walk, pay attention to your feet and their contact with the ground; walk only for the sake of walking. Be aware of the contact between your feet and the ground, and with each step, say, _"I have arrived."_ This will bring you back to the present moment.
---

### Thich Nhat Hanh and Dr. Lilian Cheung

Thich Nhat Hanh is an internationally renowned Buddhist monk from Vietnam. He is also a peace activist, and runs a meditation center in France called Plum Village.

Lilian Cheung lectures at the Harvard School of Public Health. She is also the creator of "The Nutrition Source," a website for journalists, health professionals and consumers.

