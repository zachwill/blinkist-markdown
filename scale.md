---
id: 5a1c0fdfb238e10006529825
slug: scale-en
published_date: 2017-12-01T00:00:00.000+00:00
author: Geoffrey West
title: Scale
subtitle: The Universal Laws of Growth, Innovation, Sustainability and the Pace of Life in Organisms, Cities, Economies and Companies
main_color: EB2F99
text_color: B82578
---

# Scale

_The Universal Laws of Growth, Innovation, Sustainability and the Pace of Life in Organisms, Cities, Economies and Companies_

**Geoffrey West**

_Scale_ (2017) is a glimpse into the hidden and fascinating world of the mathematical relationships that tie the world together. The blinks describe how such laws connect everything from microscopic organisms to international metropolises, and what they can tell us about the behavior of complex systems.

---
### 1. What’s in it for me? Get insight into the complex relationships of size and scale. 

Imagine that you have a cube with sides measuring 10 cm, and therefore a volume of one liter. Now imagine a second cube next to it, with sides measuring 20 cm, or twice that of the first cube. What's the volume of the second cube? Easy, it's twice that of the first cube, right? Wrong. It's actually eight times greater!

As you'll see in these blinks, when it comes to size and scaling, our intuition often leads us astray. Luckily, we can make up for this by studying what are known as _scaling laws_, the laws that govern relationships between various phenomena as they scale. Let's have a closer look at these laws and see what they can tell us about the world.

In these blinks, you'll find out

  * why Godzilla can't exist;

  * why steamships were considered unprofitable in the early 1800s; and

  * what scaling laws can tell us about our future on Earth.

### 2. A hidden pattern sustains all life and culture. 

Earth is home to over 8 million different species, from microscopic bacteria to enormous blue whales, as well as an incredible diversity of social life, cultures, cities and traditions.

It can be quite overwhelming to think about. But the truth is that there are some surprising systematic patterns behind the complexity of biological and socioeconomic life. For instance, if you plot a graph of the metabolic rate of animals — that is, the amount of energy they spend per unit of time — against the body mass of those animals, you'll see a perfectly straight line.

That's right; the metabolic rate of any animal from a mouse to an elephant is perfectly fixed relative to its body mass. Not only that, but if in the same equation you substitute the metabolic rate for the total number of heartbeats in an animal's life, you'll get another straight line!

There is, however, one trick at play. To get this result, the scales need to be _logarithmic_, meaning the units on each axis need to increase by factors of ten, so from one to ten to 100, and so on.

Take an example from the world of economics: if you plot out the number of patents registered in a city against that city's population, you'll find that the number of patents will increase 15 percent faster than the population, also producing a straight line.

Are these incredible correlations mere coincidences?

Hardly. What you're actually observing are called _scaling_ relationships. The examples above are just a peek into the ways organisms and cities _scale_ with size, and understanding this phenomenon can illuminate much about the world.

For instance, new drugs are often tested on mice to model the way they'll impact the human body. However, mice are of course much smaller than people — so how can scientists draw conclusions about humans from tests on rodents? Well, scaling can explain the answer. In the following blinks, you'll learn about other incredible ways scaling can explain organisms, cities and even companies.

### 3. Scaling laws are rarely linear, and this has important implications. 

Crowds go wild for mega monsters in movies like _Godzilla_. But could such beasts actually exist in real life?

Most people would tell you such an idea is pure science fiction and, in the seventeenth century, the Italian physicist and mathematician Galileo Galelei already knew they were right. After all, scaling laws rarely work in a linear fashion.

Just consider one square foot. If you scale up the length of each side to three feet, the enclosed area will become nine square feet. In other words, the sides will increase by three times, but the area will increase by nine.

In a similar way, a three-foot cube has a volume of 27 cubic feet, which means that if the sides of a cube are increased from one foot to three feet, they would increase by three times while the volume would increase 27 times!

In other words, area and volume do not scale _linearly_ with length, but with the square and cube of the lengths.

But what does this have to do with Godzilla?

Well, Godzilla is around 60 times bigger than a human. His volume and therefore mass would be 60³, or 216,000 times, the average weight of a human. However, the length and strength of his bones would only increase by the factor 60², or 3,600 times. As a result, he would be 60 times heavier than his bones are strong, and his bones would break in an instant.

So, nonlinearity can explain why Godzilla isn't real, but it also has important, practical implications. For example, at the outset of the 1800s, trans-Atlantic steamships generally weren't considered economically viable because of the tremendous amount of space needed to house fuel.

That is, until the English engineer Isambard Kingdom Brunel showed that, unlike the volume of cargo a ship can carry, which scales by the cube of its dimensions, the drag forces acting upon a ship only rise proportionally to the size of the vessel's hull, which scales by the square of the ship's dimensions. To put it simply, this means that the bigger the ship is, the less fuel it needs to carry each ton of cargo.

> Godzilla could not exist. Still, scaling laws allow a few calculations: Godzilla would pee some 20,000 liters of urine per day and poop three tons of feces.

### 4. Biological scaling laws can be explained by a network-based theory. 

By now, we know how the relationship between the metabolic rate and body mass of nearly all animals abides by a simple scaling law.

However, we can be even more precise. We can also say that an increase of body mass by the factor 10⁴, or by _four orders of magnitude_, will result in an increase of the metabolic rate by a factor of 10³, or _three orders of magnitude_.

What we haven't yet explained is how such relationships can be virtually identical for all life forms. The author has a theory about this.

He believes that a network-based theory can explain the origins of biological scaling laws. Here's what that means:

All biological systems function by way of networks, which transport energy, matter or information. Common examples are the circulatory, respiratory and neural systems.

Such networks share three generic properties, which might even explain the hidden regularities of biological diversity. First, networks are _space filling_, which means that the tentacles of the network need to reach every piece of the system they serve.

Second, _terminal units are invariant,_ which means that the endpoints, or nodes of delivery, such as the capillaries of the circulatory system, are about the same size and have similar characteristics, regardless of the size of the organism.

For instance, the capillaries in a blue whale are actually about the same size as those in a human. To understand why, just think about your house and the Empire State Building. If the electrical outlets in this skyscraper were scaled up relative to its height, they would be 50 times larger than those in your home.

And third, over the course of the long process that is evolution, the performance of biological networks becomes _optimized_. For example, because of the various design constraints it faces, the human heart has evolved to expend the least energy possible when pumping blood through the circulatory system.

By taking these three general systemic properties and translating them into mathematical language, the author found neat explanations for the surprising regularities that can be perceived in the world.

### 5. Biological networks extend toward a fourth dimension, explaining why humans stop growing. 

If you look closely at the mathematical details behind scaling laws, there's yet another surprise to be discovered: in most cases, biological scaling factors involve the numbers ¼ or ¾.

For instance, the number of leaves taken as a function of a plant's mass increases by a factor of 2 to the power of ¾. So, what explains the mysterious prevalence of the number four?

The answer is buried deep in the properties of biological networks.

These systems resemble _fractals_, which means that their geometry demonstrates a high degree of self-similarity. In other words, if you zoom in at random on one point of the network, the local picture will look like the zoomed out image. Just think of a cauliflower; if you cut out a single floret, it would look like a miniature version of the whole thing.

That being said, the more curled a shape is, the more it "extends" toward a higher dimension of space. This is the fourth dimension — and it's actually not all that difficult to understand.

Just consider a one-dimensional line on a two-dimensional piece of paper. The more curled the paper is, the more it fills the entire space of the page and the more two-dimensional it appears.

In a similar way, biological networks are, as you already learned, space filling, which means that they appear to extend toward a dimension beyond their own. It's this "fourth dimension" that explains the common role that the number four plays in scaling laws.

This important number even accounts for why humans stop growing in adulthood. Imagine you doubled the size of an organism. In so doing, you would be doubling the number of cells it contains and the amount of energy necessary for their maintenance.

However, scaling laws dictate that the metabolic rate of an organism only rises by the factor 2 to the power of ¾, which is less than 2. Because of this, the demand for energy increases faster than energy can be produced, halting the growth in its tracks.

The biological world is full of such interesting instances of the laws of scaling, but the social and economic spheres also offer compelling examples. You'll learn more about these concepts in the next blinks.

### 6. Scaling reveals surprising similarities between seemingly different cities, while biological networks are analogous to urban centers. 

Consider the cities of New York, Paris and São Paulo. They might not appear to have much in common; after all, they're in disparate corners of the world and are homes to highly divergent cultures.

Even so, there are a number of hidden similarities in these metropolises, and scaling relationships can make them plain to see.

For instance, the number of gas stations in a city and the total length of pipes, roads or electrical wires all consistently follow a simple scaling law that depends on population. Here's how it works:

If you double the size of a city's population, thus increasing it by 100 percent, the number of gas stations, as well as the length of pipes, roads and wires, will only increase by 85 percent.

Then, there's the _15-percent rule_, which holds that the larger a city is, the higher the wages, per capita GDP, crime rates, cases of AIDS or flu infections, number of restaurants and patent applications. That all seems intuitive enough — but what's shocking is that all these indices rise by a factor of 1.15.

That means a city with 10,000 residents and 100 restaurants would have 1,150 restaurants if its population were to increase to 100,000. That being said, since affluence varies by nation, such scaling laws only apply to cities within the same country.

However, the similarities between organisms and cities don't end there. Just like with biological organisms, cities can be thought of as networked systems.

After all, they metabolize energy and resources, produce waste as well as information, and grow, adapt and evolve. Even the three generic properties of biological networks find equivalent features in cities.

For example, biological networks are space filling, as are urban transport networks like roads, pipes and electricity lines. If they weren't, they would never serve all the residences in the city.

### 7. The speed of life, social interaction and economic activity all scale with the size of a city. 

Modern life has become so tied to the city that the author feels a new human era is upon us. In his opinion, it began with the Industrial Revolution of the late-eighteenth century and can be called the _Urbanocene_. So, what can scaling laws tell us about this new period in human history?

Well, in biology, the pace of life slows down as size increases. For instance, an elephant metabolizes energy much more slowly than a mouse and also lives for a much longer time. In cities, however, life appears to accelerate with size.

Just take walking speed, which often increases with city size. In fact, in a small town of a few thousand inhabitants, the average walking speed is just half that in a city of over a million people, where the average speed clocks in at around four miles per hour.

But it's not just the pace of life that scales with city size; social interaction and economic activity do too.

For instance, the author analyzed cell phone data to find that both the time people spend on the phone and the number of calls they make increase at a regular rate relative to the size of the city they live in. Consider Lisbon, Portugal, a city of 560,000. There, people spend about twice as much time on the phone and call twice as many people as their counterparts in Lixa, a rural Portuguese town of 4,200.

Or consider another example. While it goes without saying that the larger a city is, the more businesses it contains, the diversity of businesses — that is, the number of different types of establishments — increases only very slowly with size.

As a result, doubling the population of a city will result in an increase in business diversity of just five percent, while the number of total businesses will double on average.

### 8. Scaling reveals universal dynamics that correspond to a company’s size and age. 

You just learned how the number and diversity of businesses scales relative to city size. But what about the scaling that occurs within a business itself? How do sales, expenses and profits scale with size?

Actually, there are surprisingly common scaling patterns for all of these metrics. For instance, net income, gross profit, total assets and sales all scale up with incredible regularity relative to company size as measured by the number of employees.

That means, if you plotted a graph with these metrics on one axis and the number of employees on the other, you'd get a perfectly straight line. In other words, if a firm with 100 employees produces sales of $10 million, the same company would make $100 million worth of sales if it employed 1,000 people. The implication here is that, regardless of industry, location or age, there are clear patterns that depend solely on the size of a business.

Age is actually an interesting factor here and has its own corresponding scaling laws. Just like living organisms, companies are born, grow and, in most cases, eventually die. Not only that, but also like organisms, companies grow quickly in their youth before settling into a steady, but slow, rate of growth.

As a result, regardless of sector, location and size, a company's survival can be calculated with incredible accuracy based solely on its age. For instance, just around half of all publicly traded business make it past the ten-year mark. The probability of a company existing longer than 100 years is just 45 in 1 million, and a microscopic minority of one in 1 billion companies make it to 200 years.

To put this into context, this law means that the seemingly invincible firms of our time, like Google, Facebook and Apple, will all eventually meet their demise.

> The world's oldest company is the shoemaker Eduard Meier in Germany. The company was founded in 1596 in Munich.

### 9. Since the Industrial Revolution, the world has witnessed a population explosion, raising questions about sustainability. 

You've learned a number of rules of scale and how they correspond to all manner of circumstances. But what do these laws mean for human life in general?

Well, they can tell us a great deal about the human population. For instance, since the Industrial Revolution, the world's population has been growing at an incredible rate. This boom has marked an unforeseen shift since, for 2 million years, the human population had been growing at a steady, albeit much slower, rate.

Because of this growth, the human population first hit 1 billion around 1805. But the next billion came just 120 years later and the third a mere 35 years after that!

As a result, the world population now stands at over 7 billion and, in 2017 alone, it grew by 80 million, the equivalent of adding an entire new Germany to the world. In short, we're experiencing _exponential growth_. For every unit of time, the quantity measured will double.

Put differently, if on the first day there was one human, on the second there will be two, on the third there will be four and so on.

However, as we've already learned, there are natural barriers to the size and growth of any biological organism and scientists are in heated debate about whether this rapid population growth can be sustained. After all, the resources on our planet are limited and every new human needs to be fed, clothed and educated.

The first person to identify this issue was Thomas Robert Malthus, whose 1798 book _An Essay on the Principle of Population_ predicted that the food supply would grow more slowly than the population, eventually leading to the collapse of civilizations.

From there, an organization called the Club of Rome published a highly disputed study in 1972. Called _The Limits to Growth_, it demonstrated the potentially catastrophic results of sustained population growth.

While some of the predictions these researchers made have been proven incorrect, the fundamental issue remains; can human populations and economies continue to grow forever? You'll find out in the next blink.

### 10. It’s unlikely that the earth can sustain such rapidly accelerating population and economic growth. 

So, the human population exploded with the Industrial Revolution and has been increasing exponentially ever since. People have been warning about the impacts this sustained growth will have and it's becoming increasingly clear that such growth is straining the relationship between human society and nature.

This begs the question: Can we sustain a global population of over 10 billion and continue increasing levels of consumption without destroying the biosphere?

Well, given the finite nature of the earth's resources, the short answer is definitely not. This is a serious issue since today, both the world's population and its economies are growing at exponential rates.

Traditional economic theories maintain that such an ever-expanding economic sphere is possible; however, exponential growth would also require ever-increasing supplies of both energy and natural resources, things that the earth simply cannot provide.

As a result, such open-ended growth will have serious adverse impacts on the environment — but it's also bound to fail. To counter this argument, critics of such a notion like to say that innovation will play a critical role in solving this conundrum. But, at this point, even innovation would be a long shot to save the world from a major population crash.

After all, for innovation to keep up with the problems caused by growth, breakthrough discoveries would also need to occur at an exponential pace. To the credit of the people behind such innovation theories, this kind of exponential innovation has been the norm in recent history.

Indeed, thousands of years elapsed between the Stone, Bronze and Iron ages, while less than 30 years transpired between the recent Computer Age and the Information and Digital Age that followed.

But other problems loom. For instance, with the level of stress endemic to most modern lifestyles, it's hard to imagine how an ever-increasing pace of life could be sustained without a resulting collective heart attack.

The earth is in dire straits and serious action needs to be taken. What's called for is a Manhattan-style project that brings the top thinkers together to address these urgent questions of sustainability.

### 11. Final summary 

The key message in this book:

**The characteristics and dynamics of diverse biological organisms, cities and even companies are bound by surprisingly similar laws. These rules reveal astounding, and often hidden, regularities that describe the structure and systems of life, as well as its limits.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Magic of Maths_** **by Arthur Benjamin**

_The Magic of Maths_ (2015) reveals the magic hidden within the fascinating world of mathematics. These blinks will show you the beautiful and often surprising patterns of mathematical observations, expand your knowledge of geometry and algebra, teach you numerical party tricks and illuminate the mysterious properties of numbers like π (pi).
---

### Geoffrey West

Geoffrey West is a theoretical physicist whose interests range from the fundamental questions of physics to biology and global sustainability. He is a distinguished professor at the Santa Fe Institute and a visiting professor at Oxford University, Imperial College and Nanyang Technical University in Singapore. He was listed on _Time_ magazine's 2006 list of the "100 Most Influential People In the World."

