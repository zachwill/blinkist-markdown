---
id: 57b9e7b6ece3ed0003d5ac00
slug: scaling-down-en
published_date: 2016-08-22T00:00:00.000+00:00
author: Judi Culbertson, Marj Decker
title: Scaling Down
subtitle: Living Large in a Smaller Place
main_color: D13E2A
text_color: B83725
---

# Scaling Down

_Living Large in a Smaller Place_

**Judi Culbertson, Marj Decker**

_Scaling Down_ (2005) will help you understand the impulse to accumulate more things than you actually need, and why, once you've acquired those things, you don't want to let them go. With a helpful step-by-step guide, it provides effective strategies for scaling down and learning to live with less.

---
### 1. What’s in it for me? Learn how to be happy with fewer things. 

You might think that we humans have changed a lot since our early hunter-gatherer days. Indeed, hunting has gone a bit out of fashion and most of us forage in the supermarket instead of the forest. Nonetheless, we still do a great deal of gathering. We just gather different things.

Our ancestors used to gather precious berries and roots whereas we modern humans like to hoard all kinds of useless junk. Old clothes fill our wardrobes, tchotchkes crowd our shelves, and the boxes beneath our beds or in our attics are crammed with precious junk — be it stamps, baseball cards or stuffed animals.

So, perhaps it's time to scale down and toss some of this stuff out the window.

In these blinks, you'll learn how to part with those belongings you probably don't need, as well as how to decide which items to keep, which to toss and which items of sentimental value to let go of.

You'll also learn

  * how the Great Depression revitalized our drive to gather;

  * why people fear scaling down; and

  * why a collection of tin toys is no good.

### 2. If you want to scale down, start by creating a Scaling Down Mission Statement. 

Is your closet or garage overflowing with old stuff that you refuse to throw away? Or maybe you even pay for an external storage unit if you live in a city where closet space is a luxury reserved for the lucky few!

If that sounds like you, you're not alone. Keeping too much stuff has been an American trend ever since the Great Depression.

In the early 1930s, people ravaged by the economic recession simply couldn't afford to throw out anything unless it served absolutely no purpose.

But, of course, old habits die hard and mentalities are naturally inherited from generation to generation. So, even when conditions improved, the mindset of keeping anything that might be even slightly useful, whether now or in the future, was passed on to the next generation.

As a consequence, we've accumulated a lot of stuff, and what was once considered prudent thriftiness now looks more like out-of-touch greed.

So you own seven sets of china and an entire wall of sneakers. That's not unreasonable, right?

Yeah, right. If you've realized that this hoarding mentality is ridiculous and wasteful, you've jumped one hurdle. But the next thing is actually to get rid of the excess, a process that can be a bit overwhelming — after all, there's just so much stuff!

To help yourself out, try creating a _Scaling Down Mission Statement_ (SDMS).

An SDMS articulates the motivation behind why you want to scale down. A sample SDMS might be something like, "My great aunt is moving into assisted living with less space, so I want to help her sort out the important belongings she'll take with her."

If you're struggling to put your goal into a clear one-liner like that, try jotting down just a few keywords. Maybe it's "make room for home office" or "simple living." Putting your ideas and goals on paper will both give you a clear destination and help you get there, too.

> _"And so we keep on accumulating until, one day, we realize that we have been buried alive..."_

### 3. People have many apprehensions about scaling down – but they’re easily overcome. 

Hopefully, the SDMS has helped you figure out your goals. Nonetheless, the execution of the plan can still be daunting. If you're feeling apprehensive, rest assured that this is perfectly normal.

Maybe you're thinking, "But I might regret throwing this out later on!" Most people have done this — thrown something out only to regret it a few days later, so this is definitely an understandable mentality.

Here's the thing, though: if you hadn't been rummaging through your old stuff to organize and streamline your belongings, you probably would've forgotten you had, or would have been unable to locate, the thing you're now worrying about throwing out!

So, dare to be bold. If you haven't used something for five years, chances are you won't use it in the next five, either. So chuck it!

A second common apprehension is fear of the task's overwhelming nature — the worry that it's going to take forever to make a decision about every item.

The antidote to this fear is simply to get started. Grab a bag and walk around your home with the goal of throwing out a certain number of items **–** maybe it's five, maybe 25. Attack anywhere and everywhere, especially parts that look easy!

Of course, you're probably going to have difficulty parting with things that have some kind of sentimental value, especially since life circumstances are always in flux. Relationships, hobbies and jobs can change in the blink of an eye, and some things are just too difficult to shrug off and throw out. So don't be afraid to keep things for a set amount of time, and make a decision about it once that time is up.

For instance, maybe you just changed jobs and have a box filled with old meeting agendas, a coffee mug, sentimental goodbye cards and so on. Though some of that stuff might be ready for the garbage, give it a couple of months to see whether your life takes a new direction. In some cases, only time will tell.

### 4. Instead of massive, space-consuming collections, try smaller or even non-physical ones. 

Do you have hundreds of records or books that you dread schlepping around next time you move? If you're a collector, you probably invested countless hours and dollars into your collection, not to mention a great deal of emotional energy.

Some collectors love the thrill of the hunt. Desiring that rare item, searching for weeks, months, sometimes even years, and finally getting it and adding it to the collection is a rewarding and satisfying experience.

On top of that, many people view their collections as an investment. A collection might have a sum value that is greater than its parts, and it can be exciting to consider the profit you might make if you ever decided to sell.

Though these seem like good reasons, let's consider them more carefully and critically.

Let's take the careful collector who finally lands the latest set of first-edition books. Sure, she'll be thrilled at first, but she's likely to forget about it as soon as it's on her shelf. Why? Because she'll already be out hunting for the next acquisition!

Or let's take the investment idea. Maybe you're banking on your collection to make you wealthy, but the truth is that most items don't increase in value. And even if the value does go up, the profits will probably be marginal and not even worth the space the collection requires.

What should you do then? Well, if you still insist on being a collector, be a better one!

Instead of accumulating large objects, try collecting smaller, but still meaningful, things.

Consider objects like marbles, stamps, or baseball cards **–** even if your collection starts getting a bit out of hand, it'll be hard for these small items to take over the whole house.

Or consider the joy of bird watchers, who "collect" sightings of rare species, or of globetrotters, who've been to 30 countries. Remember, a collection doesn't even have to consist of physical objects!

> _"Snaring a prize collectible from an antique dealer, tag sale, or on eBay can be just as thrilling as bagging an elephant and a lot easier to bring home."_

### 5. Among the various strategies for scaling down are grouping and skimming. 

Okay, maybe you've resolved to collect memories instead of shoes. That's great! But there's still the business of getting rid of all the excess that you've already accrued, so let's explore some of the most common scaling-down strategies.

One strategy is called _grouping_. It consists of grouping together all the duplicate items to get a grasp of what should stay and what should go.

Do you have two, ten, or 20 umbrellas? In cases of such everyday objects, you might not even have been consciously collecting them. Rather, you may have added one here and another there as a precautionary extra, in case someone needed it in the future.

Of course, in the process of scaling down, some items will inevitably have to go. Grouping is a good method because seeing all the similar items together will make it easier to see which ones are worth keeping.

_Skimming_ is a different approach with a similar effect: when skimming, you keep the best and throw out the rest without a second thought.

For disposing of anything, from jackets to vases, skimming is a great strategy. Just get started by identifying anything that brings you only a moderate amount of use or satisfaction.

Using this strategy, many objects might leave you wondering "what if" **–** and that's actually a great sign that the item needs to go.

Maybe your cupboards are stuffed with dishes, so you open them intending to throw out all but the best two sets. But then another voice inside your head says, "But what if I want to have that huge dinner party one day and serve all 50 people with real china?"

Here, you need to counter that voice with another: "But am I ever going to have that party? And will they care if their food is on real china or not?" Answer: most likely not.

> _"...when you are scaling down and objections come up, don't accept them at face value. Go deeper, and find out what your feelings are based on."_

### 6. Performing triage and taking a photograph are more great strategies for scaling down. 

Everyone is different, and different people will prefer different scaling-down methods. So, in addition to grouping and skimming, try out these two additional strategies to find the best fit for you.

_Performing triage_ means collecting all the items in question into one spot, then picking out three from the pile randomly.

Out of those three, pick your least favorite. Once you do that, pick out another three random items, and again pick out your least favorite. In this way, you'll easily cut down your belongings by one-third.

Triaging is an especially great approach for collectors who have a lot of variations of one item, but it's also applicable for smaller, everyday clutter, too.

The final strategy is taking photographs of your beloved objects.

It might sound trite, but photos will truly help you hold on to the special memories or meanings attached to objects without taking up much space at all.

Of course, photos won't have the same physicality as the actual item, but you might be surprised at how much satisfaction you can get from a two-dimensional reproduction.

Much of the time, the value of an object lies in its ability to evoke the feelings or experiences attached to it. As we all know, photos can be invaluable when it comes to remembering that special dinner party or vacation with your loved ones — and the same is true for photos of your belongings.

Maybe you don't have any room left for your mother's wedding dress or the vintage car that's been sitting in your garage for years. It's not easy to say goodbye, but photographs will help you remember and share the special stories behind such items, even when they're gone.

### 7. You’ll feel better about scaling down if you give your stuff a good home. 

You've now successfully found a strategy that works for you and have culled the best from the rest. But the idea of trashing all your old belongings is still pretty distasteful, right? Well, the good news is that you don't have to!

Finding good homes for your stuff is actually an important part of scaling down in a happy, healthy way.

Knowing that your objects will go to appreciative new owners instead of disappearing into the landfill will make the process much more bearable and even enjoyable.

Finding the best match for one's belongings might mean finding a specific person or donating them to the right charitable organization.

Let's take Jeff as a case study. Jeff needed to scale down, and so decided, rather reluctantly, to part with his beloved old car, which held many great memories. Naturally, he didn't want to give it away randomly, so he found a reputable charity that would ensure someone else would benefit from it. Doing good while also decluttering his life was a double-whammy, in the best way possible.

Recycling is another way to part with your things with a positive result for both you and society.

Instead of throwing your things in the bin, try to salvage what parts can be recycled. Many communities have recycling programs for paper, metal, plastic and other materials.

As the saying goes, one man's trash is another man's treasure. Knowing that your stuff will be reused will ease the process of scaling down since you'll know someone will take advantage of it!

### 8. Final summary 

The key message in this book:

**We don't need a whole lot to be happy. Though it's not easy to break the habit of over-consumption, finding ways to sort and reduce your stuff without sacrificing meaning and memories can help you live with less, and be happier for it.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _How We Live Now_** **by Bella DePaulo**

In _How We Live Now_ (2016), you're taken on a virtual trip across the United States to explore the different ways in which Americans create homes for themselves, their families and friends. These blinks reveal the latest trends in communal living as well as the forces driving people to create new, fascinating ways to live.
---

### Judi Culbertson, Marj Decker

Judi Culbertson is an expert in organizing — so much so that she leads seminars on how to declutter one's life. She is also the author of _Traveling Light_.

Marj Decker is the CEO of Time is Priceless, a company based in Denver that helps people scale down to live a simpler life.

