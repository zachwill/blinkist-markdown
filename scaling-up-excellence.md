---
id: 53eb3a153634330007300000
slug: scaling-up-excellence-en
published_date: 2014-08-12T00:00:00.000+00:00
author: Robert I. Sutton and Huggy Rao
title: Scaling Up Excellence
subtitle: Getting to More Without Settling for Less
main_color: EA632F
text_color: B84D25
---

# Scaling Up Excellence

_Getting to More Without Settling for Less_

**Robert I. Sutton and Huggy Rao**

_Scaling_ _Up_ _Excellence_ is the first major business publication that deals with how leaders can effectively spread exemplary practices in their organization. Readers can expect to learn about the latest research in the organizational behavior field, lots of instructive industry case studies, and many helpful practices, strategies and principles for scaling up.

The authors help leaders and managers understand major scaling challenges and show how to identify excellent niches, spread them and cultivate the right mindset within their organizations. They also set out scaling principles that guide leaders in their daily decisions.

---
### 1. What’s in it for me? Find out how to spread best practices in your company or organization. 

Why can't all the stores in a city be as popular among customers as the ones in the city center? Why can't all the research units in a university top the league tables? Why can't your favorite football team play as well as the one from the neighboring city?

If those questions have ever crossed your mind, you're probably interested in finding out how you can replicate something excellent in another place and with other people. Because we all want more of the good stuff, right?

In _Scaling_ _Up_ _Excellence_, co-authors Sutton and Rao help readers understand the challenges involved in spreading outstanding practices and behavior. The book is not only packed with the latest research and a range of examples but also provides you with practical advice for your individual scaling challenge.

In this book, you'll find out:

  * why military knowledge is essential to spread best practices,

  * how knowing about Buddhism and Catholicism can help you to better scale up,

  * how disruptive employees can start to outperform all other employees, and

  * how watermelons saved the lives of university students.

### 2. You need it: scaling is essential to the success of organizations. 

_Scaling_ is one of the major challenges that every leader or executive in a successful organization faces. Scaling refers to the practice of spreading the excellent performance that always exists somewhere in the organization to more people and more places.

That's why scaling is also called the "problem of more."

But creating more commendable behavior looks very different in each different organization. For instance, scaling can mean growing a technology start-up from twenty to forty employees, opening a new burger store in another country, or reducing treatment errors in hospitals.

Moreover, scaling not only requires replicating best practices but improving performance along the way. In other words, the "problem of more" is also a "problem of better."

Scaling requires the drive to keep innovating, change organizational behaviors and structures, and find better strategies of delivering your product or service.

Take the Bridgewater International Academies, a chain of elementary schools in developing countries. It scaled up from one school in 2009 to over 210 today. During its scaling process, the leadership team not only focused on growing the organization but also on improving their practices.

For instance, they're currently working on a new initiative that equips teachers with customized content to work better with students who have different abilities.

Scaling is an important yet tricky organizational challenge. And while there's no easy recipe for effective scaling, if we look at successful examples, we can glean some fundamental principles and strategies for success.

> _"To spread excellence, you need to have some excellence to spread."_

### 3. In for the long haul: building persistence is more essential than quick wins. 

So, you want to spread excellent behavior in your firm? Before you start any kind of scaling effort, put your running shoes on — and you're not just going for a sprint! Scaling is like running a marathon, so be prepared for a long, strenuous journey before feeling the satisfaction of finishing the race.

When setting out to improve your organization's performance, you need endurance to overcome unforeseen setbacks and nasty roadblocks.

You can imagine scaling as waging a ground war instead of an air war. While an air war is a fairly quick and safe way to attack an enemy, it is rarely enough to defeat them. In terms of scaling, the analogy implies that you need to invest time, manpower and resources to "conquer" each opponent.

For instance, during Facebook's first years, Mark Zuckerberg worked with every employee to make sure they were fully committed to the company's vision and values. Even now, new hires go through an intensive, six-week program where they need to prove themselves in different teams, understand the company's culture and work with a mentor.

Facebook fights the ground war and knows that investing hard work and a considerable amount of time into training new hires will grow their commitment and improve their contributions to the company.

Having perseverance means starting from and with the people. So don't think about scaling as pushing one person a thousand feet forward but, rather, as pushing a thousand people one foot forward.

Here's one great example of the way persistence and people orientation make it possible to outperform competitors:

After a successful college football career at Stanford, Andy Papa worked in the pit crew for a motorsports company. Recognizing inefficiencies in their system, he used his know-how from the sports world to scale up. He recruited members from different crews and had the team follow a strict exercise, practice and learning regimen; in the end, the crew was able to significantly speed up its work during races.

### 4. The tricky continuum: navigating between standardization and local variation. 

Now that you've decided you're in it for the long term, it's time to learn about one of the most important trade-offs on your scaling endeavor.

When trying to spread outstanding performance, you'll most likely navigate between two poles.

The first one is _standardization_. Standardization describes a process in which much of what you're trying to spread is pre-determined — usually an ideal model. The authors call this approach "Catholicism" because it's reminiscent of the highly centralized approach of the Catholic Church.

One Catholic Church example is the successful US burger chain In-N-Out Burger. Every store is an exact copy of the other, and employees wear the same clothing, receive the same training and follow the same rules.

The second pole is _local_ _variation_. In this practice, a commonly shared mindset guides people's overall behavior, but specific actions can vary tremendously. Due to its similarities with the religion, the authors call this approach "Buddhism."

Swedish furniture giant IKEA takes a locally varied approach. Although it sells its items in pieces for customers to assemble themselves in most parts of the world, it offers home delivery and assembly services in China because the DIY approach is not very popular there.

But let's get one thing straight: your main question isn't to choose "Catholicism" or "Buddhism" but to find the balance that best suits your company.

The healthcare company Kaiser Permanente is a good example of a company with a mixed approach. When implementing an electronic health record system across the organization, it defined a couple of non-negotiable points and allowed for regional adjustments.

Although the name "KP Health Connect" was mandatory and the sensory experience — the look, feel and touch of the application — had to be the same in every region, each local chapter could choose their favorite configurations of the same software.

In the following blinks, we'll discuss some of the practical principles, methods and tools you can use to scale up successfully.

### 5. A twofold approach: changing beliefs versus changing behavior. 

Imagine you're at the beginning of your scaling endeavor. Where exactly will you start?

Your first step is communicating the scaling effort in a way that motivates your organization to want to scale. There are two ways to go about this message: either you target people's beliefs or their behavior.

Some say you need to change people's beliefs first because beliefs guide people's actions.

Imagine you want to increase the number of students who wear a helmet when they ride a bike to campus. You could work the belief angle by asking a student who has had an accident without wearing a helmet to share his story. This will most likely create a strong emotional connection to the issue and might cause students to change their beliefs about wearing a helmet.

Stanford University took this approach and, lo and behold, it worked.

However, some studies show that it's best to target people's behavior first because it can change their beliefs.

Applying the behavior approach to the same example, you could, for example, encourage students to wear personally designed helmets so wearing a helmet feels fashionable. Studies suggest that, over time, their beliefs would change and they'd prefer to wear a helmet.

Both approaches work in practice. So whichever approach you choose, just try to remember one thing: it doesn't matter where you start to get people on board your scaling effort. All that matters is that you start where it works best for you.

### 6. Concentrate on reduced complexity: it will help free up space for excellence. 

So you've taken the first few steps in your scaling journey. Now what?

As your scaling effort grows, you'll need to bring in more people, more organizational layers and more resources to keep it growing. However, one of the main pitfalls of scaling up is adding too much complexity before it's necessary.

If you add too many people, standards and rules too fast, you'll get what experts call the disease of a "big dumb company."

Let's say you're working as a sales manager in a shop and a customer requests a refund on a damaged product. Could you imagine if you had to obtain the approval of nine people at your company before deciding that small matter, rather than just relying on your own competence?

That's exactly what happened to the sales staff of West Coast gas stations, where red tape had gotten out of control, and employees often needed months to solve minor issues.

But however careful you are, you can't always prevent unnecessary burdens from building up because things change over time; what might have worked well for years could become unwieldy down the road.

The solution is for you to always be on the lookout for redundant rules and practices.

For instance, at the software company Adobe, leaders decided to abolish formal annual performance reviews. Instead they installed more frequent, personalized check-ins, i.e., regular conversations about performance and career progression, between line managers and their employees.

Switching to informal check-ins cut out unnecessary formal documentation duties of leaders, while simultaneously creating space for managers to have more effective talks with their employees in support of their personal development.

> _"The best organizations perform constant subtraction, not just addition and multiplication."_

### 7. People matter: foster accountability among excellent people. 

Involving the right people in your scaling effort is critical to its success.

But you don't only need excellent people with the right skills and training, you also need people who are accountable and act in the company's interests at all times. The difficulty is to find people with both excellent skills and accountability.

Tamago-Ya, a Tokyo-based food delivery company, solved that problem by employing mainly high-school dropouts as drivers.

It may sound paradoxical, but once the drivers are properly trained, Tamago-Ya has the best employees it could wish for: they know Tokyo's quirks better than many other locals, they're good at dealing with customers, and they pick up local news that might be interesting for the company.

Plus, the drivers are accountable to the organization, as they depend on being trained and paid, and are grateful for getting the chance to work for the company.

Wondering what specific practices could help you build a company like Tamago-Ya? There's a whole spectrum of things you can do to help build talent with accountability. One good method is to build a feeling of shared purpose and collective belonging.

At multinational gas and oil company BP, managers treat the company as a sports team and foster an excellent team spirit by mobilizing employees against the competition. The company wants to be better than Shell, its main competitor, by "slamming the clam" (the clam being part of Shell's logo).

By following BP's example of promoting pride, belonging and identity, and by building on internal values and strengths in your organization, you can clearly set yourself apart from your competition.

### 8. The power of connections: building relationships between people spreads excellence. 

What's even better than having excellent and accountable people who spread exemplary behavior throughout your organization?

Deep connections between as many of them as possible: the power of spreading good behavior lies, not only in people, but in numbers. In other words, diversity will help spread good behaviors more reliably than anything else.

Diversity helps to anchor your effort in many different departments, locations and functions in your organization. It also helps to show that scaling benefits different stakeholders across the organization, not just one group.

When airline JetBlue tries to solve a fundamental operations problem, a manager invites a wide range of employees with varying backgrounds to discuss the situation: baggage handlers, gate control agents, mechanics, pilots, flight supervisors and managers, of different ethnic origins and ages.

The rule of thumb for making the most of your company's diversity is to target your employees by using many different tools in many different ways.

Imagine you want to spread best practices for reducing energy use in your company — you may want to combine two strategies to publicize your initiative. On the one hand, the CEO of your company could openly embrace the campaign by issuing a message that calls on people to reduce their energy use.

On the other, you could also hold a bazaar-style event where employees share their stories about how they minimize energy use, thereby allowing them to connect on a more personal level.

By employing these two different strategies, you'll be more successful in catering to the diverse needs, preferences and motivations of the people within your organization.

### 9. Beware of the bad: clear out negative practices before spreading good ones. 

One of the biggest threats to your scaling effort is bad behavior: it is extremely contagious and can cancel out the benefits or even destroy excellent behavior.

Many of us have lived through it before: we've worked in a team where one member undermined the entire group's performance by lying or taking credit for others' performance.

Research on group effectiveness has also shown that group performance decreases by 30 or 40 percent if just one person with a disruptive mindset joins the group!

That's because negative emotions are likely to infect the whole group. Plus, team members need much more time and energy to figure out how to deal with the grumbler.

Experts in social research summarize this using the "broken windows theory." In a neighborhood where there are broken windows, criminals will start breaking more windows, and others might join in by breaking into houses.

The problem is that destructive behavior proliferates and escalates quickly. Even small acts can be extremely damaging to the performance of your organization.

Since destructive behavior is so infectious, leaders need to address it with vigilance and perseverance before trying to spread excellent practices. Having no tolerance for bad behavior is a good mindset to start with. From there, many tools and practices can help.

While firing might be an option, there are other ways to deal with people that undermine the performance in your organization.

Coaches at d.school, Stanford University's design school, put together a group made up of only disruptive characters to work together under the guidance of a coach. Some of the results were remarkable!

Although they were all people with big, extroverted personalities who tended to undermine others, when these disruptive people were among similarly disruptive people, they were on the same level and thus balanced each other out.

> _"Destructive behaviors of just about every stripe — selfishness, nastiness, anxiety, laziness, dishonesty, for example — pack a bigger wallop than constructive behaviors."_

### 10. Envision: time travel from the future 

There's just one last piece of the puzzle necessary for completing a successful scaling approach.

Namely, positioning yourself in the future and imagining that your scaling initiative has already been completed. Experts call this technique performing a project _premortem_.

To practice this technique, split your team into two groups. Tell one group to imagine that the scaling effort has been a spectacular success and the other that it has been a huge disaster. Then, both groups share their story as if it had already happened, identifying as many causes of the respective success or failure as possible.

For instance, someone in the first group might say, "The scaling effort was a big success because we devoted our attention to keeping our care patient-centered."

You'll quickly see the advantages of this technique. It helps address possible roadblocks more openly and keeps overly optimistic tendencies in check.

Management research shows that this technique increases people's ability to accurately identify the causes, barriers and enablers for future results by 30 percent.

When you reason from a place in the future, you can ask many more questions. Was the scaling effort actually feasible? Was spreading excellent behavior worth the time and money spent?

Imagine you're trying to grow the start-up you recently founded. Position yourself in the future and ask: Am I content with the company I've created?

Chances are you're similar to Mitch Kapor. He started Lotus, a technology company, in 1982. It soon became very successful and grew quickly. However, once Lotus was operating like a big corporation, Kapor realized it wasn't a place for him anymore: he didn't feel at home running a large company with all its hierarchies, routines and standards.

A premortem could've saved Kapor his disappointment!

### 11. Final summary 

The key message in this book:

**Spreading** **excellence** **is** **one** **of** **the** **trickiest** **challenges** **facing** **organizations.** **To** **successfully** **spread** **outstanding** **performance** **to** **more** **places** **and** **people,** **leaders** **need** **to** **engage** **as** **many** **different** **people** **with** **determination** **and** **persistence,** **cut** **unnecessary** **complexity** **and** **ruthlessly** **eradicate** **destructive** **behavior.**

Actionable advice:

**Take** **scaling** **efforts** **one** **step** **at** **a** **time.**

The next time you're thinking about taking the next big step forward — whether it's improving the performance of your team, accelerating the start of your new career or influencing a group to take on your policy proposal — remember to take a ground war mentality. Rather than focusing on making one big leap forward, you need to press all people steadily and persistently towards your goal.

**Try** **on** **a** **new** **pair** **of** **shoes** **every** **once** **in** **a** **while.**

If you're faced with conflict between people, teams or whole departments, use the "Freaky Friday" management tool. Just switch the roles of the opponents around, and you'll see how walking in each other's shoes for some time will help each of you recognize your part in the problem and get the group to cooperate.

**Don't** **underestimate** **the** **power** **of** **fun.**

When you're thinking about ways to create buy-in for your effort, make it fun. People are more likely to support you wholeheartedly if doing so gives them joy and pleasure. Throw people a "birthday" party on their first working day or create an online market where people can playfully vote, back, invest and improve each other's ideas.

**Suggested** **further** **reading:** _**Leaders** **Eat** **Last**_

_Leaders_ _Eat_ _Last_ by Simon Sinek explores the influence that neurochemicals have on the way people feel and consequently act, and examines the discrepancies between how our bodies were designed to function and how they function today. Ultimately, we need true leaders to direct us back on the right path.
---

### Robert I. Sutton and Huggy Rao

Huggy Rao is a professor of organizational behavior at Stanford University. He has published the book _Market_ _Rebels:_ _How_ _Activists_ _Make_ _or_ _Break_ _Radical_ _Innovation_. Robert I. Sutton is a professor of management science and engineering at Stanford University. Among his books are _Good_ _Boss,_ _Bad_ _Boss_ and _The_ _No_ _Asshole_ _Rule_ (also available in blinks).

