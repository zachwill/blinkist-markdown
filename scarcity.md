---
id: 546bcc3334353000084b0000
slug: scarcity-en
published_date: 2014-11-20T00:00:00.000+00:00
author: Sendhil Mullainathan and Eldar Shafir
title: Scarcity
subtitle: Why Having Too Little Means So Much
main_color: E04846
text_color: C7403E
---

# Scarcity

_Why Having Too Little Means So Much_

**Sendhil Mullainathan and Eldar Shafir**

_Scarcity_ makes the compelling case for the amazing impact that the perceived lack of vital resources — whether time, money or even friendship — has on our lives. It builds its case from fascinating scientific research which reveals how the feeling of scarcity can influence our decision making and even change the way we perceive the world.

---
### 1. What’s in it for me? Learn how to best navigate times of scarcity. 

Think about the last time your debt began piling up, or when deadlines started mounting for a long line of important projects. How did you feel? Were these moments where you got your best work done, made your best purchasing decisions or did your best budgeting?

Even though these stressful, pinched periods require us to be at our sharpest, the reality is that the perception of scarcity makes us even worse at dealing with these pressing problems.

In fact, the coping mechanisms we draw upon during times of scarcity actually worsen our situation, further entrenching us in our predicament. Therefore, it's vital to understand how and why we respond to scarcity in the way that we do, in order to develop proactive strategies to deal with it.

After reading these blinks, you'll discover

  * why becoming distracted at your daughter's softball game doesn't make you a bad parent;

  * why the wealthy are only sometimes interested in saving $50; and

  * why firemen should wear seat belts.

### 2. Many problems in society are linked by the concept of scarcity. 

What do the tragedy of global poverty and the difficulties of sticking to yet another fad diet have in common? At first there doesn't seem to be any link; in fact, they seem to be contradictory in every way.

Yet upon closer inspection we discover that they are both consequences of the same malaise: _scarcity_.

In fact, scarcity, defined as "having less than you feel you need," is the link that connects many of the seemingly unrelated problems we face every day. This isn't physical scarcity, as in the literal unavailability of objects, but rather the _feeling_ of not having enough, whether it's time, money, food or even something more abstract like education.

Imagine a world-class chef who, having spent her entire life perfecting her craft, must create her best dish in less than two hours for a TV show, under intense time pressure. Here, time is scarce, and she doesn't feel she has enough minutes for what she needs to do.

The chef's feeling of scarcity is essentially the same as that of the dieter who's struggling to eat less than he's accustomed to. To him, scarcity is the amount of calories he feels he's missing from his meals.

However, to a certain degree we can control our experience of scarcity by using something called a _critical safety valve_. For example, if you've overcommitted to work projects, your safety valve might mean missing a few deadlines. Or if you're on a diet, you can take a break and have a slice of pizza. In this way, you are choosing how much scarcity you have to face.

But not everything has a critical safety valve. If you're poor, you can't just decide to be rich for a day to alleviate the pressure. This lack of freedom to manipulate your situation means that poverty represents a particularly extreme form of scarcity. Regardless, the actual experience of scarcity is the same.

### 3. When faced with scarcity, our mind focuses closely on the thing we think we are missing. 

When you're hungry, food is all you can think about. This is because food — at that moment, even only for a few minutes — is scarce and the need to satiate your appetite is almost overwhelming. Even if we aren't aware of it, when we experience scarcity of _any_ kind, not just the kind regarding our biological needs, we become absorbed by it, obsessing over what we feel we lack.

Toward the end of World War II, as the Allies advanced further into German-occupied territories, they faced the problem of bringing back concentration camp inmates from the edge of starvation. To solve this problem, the University of Minnesota conducted an experiment with 36 male subjects in which the subjects were first starved and then underwent various feeding regimens. The findings helped the camp survivors, and also revealed some fundamental insights.

Not only did the subjects' bodies physically weaken and their appetites sharpen, but their minds changed as well — they became obsessed with food. They compared food prices, obsessed over cookbooks, fantasized about switching careers and starting restaurants and were unable to follow film narratives, focusing instead only on the scenes with food in them.

But scarcity does more than change the focus of our attention; it also changes how we interpret information.

Consider, for instance, that when people undergo traumatic yet lightning-fast events, like robberies or car accidents, they often feel that the events lasted longer than they actually did, due to the huge amount of information that the brain had to process. Researchers call this phenomenon the _subjective expansion of time_.

Scarcity is similar in that it can alter how we experience the world. Lonely people, for instance, are much better at judging others' moods and emotions. This is because their lives center around observing others, trying to see in them what they themselves miss.

Now that you know how scarcity works, the following blinks will examine how its effects play out in your daily life.

### 4. Scarcity forces us to myopically focus on immediate concerns and neglect others. 

As you look through your wallet, you stumble across a coupon with no expiration date for a free meal at a restaurant. This time, like every other time you see your coupon, you think: "Eh, I can use this any time. Not today, but maybe next week." Consequently, it never gets used.

This is actually quite common: coupons with no expiration date are less likely to be used than those that have one. But why?

Essentially, we become more attentive and efficient at managing pressing needs, a process known as _tunneling_. When time is limited, you tend to get the most out of it, be it work or pleasure. This is what the authors call the _focus dividend_ — the positive outcomes produced when scarcity captures the mind.

Deadlines, for instance, aren't just good for planning purposes, but also because they create scarcity (of time) and focus the mind. Take a study in which students were given essays to proofread with differing deadlines. Those who were assigned one essay per week over three weeks were more productive (in terms of meeting deadlines and finding typos) than those who were given three weeks to proofread the same three essays.

Yet this intense focus on one thing comes at the expense of others, a phenomenon described by the authors as the _tunnelling tax_. You've surely experienced this, for example, when you're so engrossed in a TV program that you can no longer hear your friend talking to you.

But the consequences of the tunnelling tax aren't always as trivial as hurting your friends' feelings. In fact, they can be deadly. Take firefighters, who are so focused on reaching a fire — due to scarcity of time — that they neglect other considerations, like wearing seat belts. This is exactly what happened in the case of Brian Hunton, who died after he was flung out of the door of a fire engine racing to a fire.

### 5. Scarcity reduces “bandwidth,” our ability to manage and compute information. 

The deadline for your presentation is tomorrow. But your daughter has her city championship softball match tonight. Like a good parent, you go to her match, but you just can't focus on the game, can't enjoy yourself — your mind keeps wandering back to that presentation. Why can't you just enjoy the moment?

This is because scarcity taxes our _bandwidth_ and, as a result, inhibits our most fundamental capacities, such as concentrating on that softball game.

Bandwidth measures our computational capacity — namely, our ability to pay attention, make good decisions, stick to our plans, resist temptation, and so on. Scarcity uses up our bandwidth, which has a negative impact on many aspects of our behavior, such as our patience, tolerance, attention and dedication.

A father who is preoccupied during his daughter's softball match may seem a bad parent, and a student who can't concentrate on his exam due to the stress of paying his tuition might seem incompetent or lazy, but these kinds of judgements are too shallow. Their lack of concentration isn't a personal flaw, but merely a sign that their bandwidths are taxed by scarcity (of time or money in these cases).

But why does scarcity tax our bandwidth in the first place? Basically, _top-down attention_, where the mind is directed by conscious effort to focus our attention, cannot prevent _bottom-up intrusions_, where our attention becomes captured by external stimuli. Scarcity is a bottom-up intrusion, and is therefore involuntary and powerful.

For example, in a study of attention, dieters were asked to push a button when they saw a red dot appear on a screen. Interestingly, they were less likely to see the red dot if they had just seen a picture of food.

Psychologists call this _attentional blinking_, because the picture of food (which created the feeling of scarcity) made them mentally "blink" as the dot appeared.

### 6. Scarcity is a vicious circle that is very difficult to escape unless we have some slack. 

How many times have you felt like you're playing catch-up with your debts, only to find yourself in exactly the same hole as you were before?

Scarcity is partly the result of an aspect of human behavior known as the _scarcity trap_. When we fall into the scarcity trap, two things happen: we constantly juggle, moving from one pressing task to the next, and yet are continually one step behind.

Surely you've experienced this at school or work, when you have many things to do, pressing deadlines and the feeling that there's too little time to get it all done. Instead of finishing your projects one at a time, you instead frantically jump from one to the other. As your deadline approaches, time becomes scarce for _all_ the projects, causing you to juggle more and more.

Being one step behind can also increase scarcity, as missed deadlines or delayed projects cause your new work to get a late start, and this can quickly spiral out of control.

Although the scarcity trap is easy to succumb to, there is one way out: giving ourselves _slack_.

Think of slack as the space in your vacation suitcase. If you pack your suitcase to the brim, then there will be no space to cram in the phone charger you forgot to include. The only way to get it in is to leave something else behind. However, if you intentionally leave a little space in the suitcase, then you'll have the resources to deal with the unexpected.

Slack might be something like free space in your schedule — a 15 minute window between meetings — which can save you the hassle of scarcity of time if your meeting overruns. Of course, not all situations allow for the creation of slack. Low-income earners, for example, simply don't have the resources available to save money and create financial slack.

### 7. Scarcity makes us experts at valuing the small benefits as well as the big. 

Have you ever poured from an unusually shaped bottle, only to find there was far more liquid inside than you thought was possible? This is because your perception was wrong — if you were to do it again, however, you'd probably measure it more accurately. Indeed, expertise — learning from our experiences — can alter our perception.

For instance, if a layperson were to pour you a shot of whiskey from each of four differently shaped bottles, you'd likely end up with four different-sized shots. However, an experienced bartender is far less likely to be influenced by the bottles' shapes, thus making all four whiskey shots uniform.

Likewise, the more we experience scarcity, the more we understand the value of each inch of space in our suitcases and each dollar in our bank accounts.

If you're an experienced traveller, then you know to stuff your socks and phone charger inside your shoes, uncoil your belt and slide it along the inside edge of the case, leave your umbrella at home, make trade-offs and leave frivolous things behind when you're packing for your next big trip.

In the same way, the pressing needs of the poor help them recognize the value of each dollar. We can see this in a study at a New Jersey train station in a wealthy town, where researchers surveyed commuters about how far they would go to get a deal. 54 percent would recommend traveling 45 minutes to save $50 on a $100 purchase, but interestingly, only 17 percent would do so for $50 savings on a $1,000 purchase. This is because the savings in the first case were proportionally much larger, and therefore more appealing to them.

Conversely, 87 percent of visitors at a soup kitchen in Trenton, New Jersey, would pursue the discount on a $1,000 purchase and 76 percent for a $100 purchase. For them, experience had made them value every dollar, so it didn't matter how _much_ they were saving as long as they were saving _something_.

### 8. Our inability to think about the future and our focus on present problems lead to bad decisions. 

How many times have you considered taking out a loan when your cash has run out? While you're certainly not the only one to toy with the idea, it's actually the most short-sighted thing you can do.

Unfortunately, scarcity causes us to make rash decisions with little regard for their long-term consequences. When we're intensely focused on putting food on the table, for instance, we're less effective at planning for the future. Of course, planning in general is difficult for many, but scarcity makes the problem worse.

For instance, when we're busy, we easily ignore the future health costs of eating takeout in favor of satiating our hunger and overcoming the scarcity of time. Similarly, we're less interested in the difficulties caused by debt in the future when we are tight on cash now.

This shortsightedness, which the authors call the _myopia of the poor_, leads to suboptimal decisions, such as borrowing.

We borrow for two reasons: an intense focus on the problem at hand as well as the fact that we cannot experience the future, only the present.

Consider the case of Sandra, a former student at the _Head Start_ child development program for low-income families. When times were tough she would use one loan to pay off another, causing her to pay $495 to $600 per month in fees, and be stuck in a position of perpetual debt. Her checks bounced, her car was repossessed and she ended up owing thousand of dollars in back taxes.

But borrowing isn't just about money. For instance, you can borrow time by putting off one project to make room for another. But this often results in "fees." Putting off work increases the time it takes to do it, makes it harder to juggle your projects and can even lead to missed deadlines.

Unfortunately, we often try to manage scarcity in ways that only exacerbate our problems.

As you've seen, scarcity can be a real nuisance and can lead to terrible decisions and awful consequences. In our final blink, you'll learn some ways to combat scarcity.

### 9. Curb the negative effects of scarcity by redesigning programs, adding slack and reducing bandwidth. 

Whether it's improving the lives of the poor, managing scarcity in organizations or dealing with scarcity in everyday life, the strategy is always the same: change the programs, add in slack and reduce bandwidth.

Whatever the area, you can always improve your program in a way that mitigates the effects of scarcity on everyone. Take university students, for example, who often have to manage the scarcity of time. They often miss classes because of deadlines they have for projects for _other_ classes, but this could be solved with a simple restructuring of the classes themselves.

Rather than having just one class, the university could schedule multiple modules starting at different times but running parallel to each other. Then, if someone misses one class, they can easily catch up in another.

The availability of slack is equally important. Take Saint John's Regional Health Center in Missouri, which used to struggle with meeting deadlines for planned operations and consultations. Unexpected medical emergencies would require all the available rooms at the hospital, meaning they would have to push back their planned procedures.

When an advisor examined this problem, he suggested a novel solution: always keep one room free. At first, the doctors thought this was crazy: "We _already_ don't have enough space, and now you want us to have _less_?!"

In the end, however, it created slack, thus providing space for the emergencies and unplanned events, meaning that planned surgeries could now go ahead without fear of disruption.

Finally, manage your bandwidth instead of the things that cause scarcity (time, money, etc.). When the authors first started working on this very book, they blocked out a period every morning to write, so that nothing would get in their way.

But their time ended up being unproductive. Before their writing period, the authors would do _other_ things, like look through and answer their emails. This small act stole their available bandwidth, meaning that their work period suffered. Time management is useless unless you protect your bandwidth!

### 10. Final summary 

The key message in this book:

**Scarcity causes us to behave in all sorts of strange and self-defeating ways. We become so focused on solving our immediate problems that we lose sight of the future, not out of some kind of personal failure, but because scarcity actually alters the way we perceive the world itself.**

Actionable advice:

**Develop a system that requires you to confirm decisions before you commit to them.**

If you want to buy a car because, for whatever reasons, you no longer have one, then you'll want some way to prevent yourself from making rash, short-sighted decisions that could have far-reaching consequences (in this case, for your wallet or mobility). This system could be something as simple as not making the decision to buy your car when you're on the lot.

**Suggested** **further** **reading:** ** _Nudge_** **by Richard H. Thaler and Cass R. Sunstein**

The message of _Nudge_ is to show us how we can be encouraged, with just a slight nudge or two, to make better decisions. The book starts by explaining the reasons for wrong decisions we make in everyday life.
---

### Sendhil Mullainathan and Eldar Shafir

Sendhil Mullainathn is a professor of economics at Harvard University, and a recipient of the MacArthur Foundation's Genius Grant.

Eldar Shafir is William Stewart Tod Professor of Psychology and Public Affairs at Princeton University, where he specializes in inference mechanisms, judgement, decision making and other economic behavior.

