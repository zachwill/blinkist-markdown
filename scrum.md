---
id: 5492883f62633000095d0000
slug: scrum-en
published_date: 2015-01-02T00:00:00.000+00:00
author: Jeff Sutherland
title: Scrum
subtitle: The Art of Doing Twice the Work in Half the Time
main_color: F13E2F
text_color: BF3125
---

# Scrum

_The Art of Doing Twice the Work in Half the Time_

**Jeff Sutherland**

Learn all about Scrum, the project management system that has revolutionized the technology industry. This is a uniquely adaptive, flexible system that allows teams to plan realistically and adjust their goals through constant feedback. By using Scrum, your team can improve their productivity (and the final result) without creating needless stress or working longer hours.

---
### 1. What’s in it for me? Learn how Scrum changed the technology industry forever. 

Project management systems never get the credit they deserve. It's always superstar engineers or beautiful products that steal the spotlight. But how do they come together, the engineer and the product? How do the two of them join together in a beautiful technological embrace _and_ move forward? _That's_ what project management systems are for.

In _Scrum_, Jeff Sutherland describes his Scrum system, a way of managing projects that is so effective at saving time and money that it has been used to clear up the bureaucratic mess at the FBI. And now, Scrum is changing the way most technological companies approach their work.

Although Sutherland is already known as one of the geniuses behind The Agile Manifesto, these blinks will explain the ideas that make him one of the most important behind-the-scenes thinkers in the world.

After reading these blinks, you'll learn

  * how Scrum learns from fighter jet pilots;

  * why leaders should report to their teams, not the other way around; and

  * how to start your first Scrum project today.

### 2. If you want to get things done, ditch the traditional waterfall method, and try Scrum. 

How many times have you planned to complete a project, only to find yourself painfully behind schedule as the deadline loomed closer?

Unfortunately, this happens often when we use traditional management processes, like the _waterfall_ _method_ epitomized by Gantt charts.

Gantt charts illustrate project timelines using color-coded parallel bars that indicate the timing and length of different parts of the process, which may occur simultaneously (the result can look like a stylized waterfall, hence the name). Although these are popular organizational tools, they often assume an outsized degree of importance, as when a project has fallen behind schedule, and managers pile more resources into it so that they can make their work fit that schedule, and not vice versa. It can lead to disastrous results.

For instance, the FBI planned to implement a modernized software system called Virtual Case File (VCF), in order to better share information and prevent another 9/11.

Using a Gantt Chart, the agency created a deadline for every important milestone. So the "technical design" stage was scheduled to end on a specific date, at which point the "coding and testing" phase of the project would commence.

Unfortunately, the project broke down before a single line of code was written. VCP ultimately wasted years of the agency's time and $170 million in taxpayer money.

Since these kinds of failures happen periodically with the waterfall method, many organizations have adopted the _scrum_ project management system instead.

Scrum is characterized by team building and constant feedback. This team-oriented approach is reflected by the name, which describes the moment in a rugby game when the team works together to move the ball down the field, all united by the same clear goal.

And this process works: When the FBI applied Scrum to Sentinel, their next attempt at large-scale modernization, the agency successfully implemented the system in less time, with fewer people and at a lower cost.

Curious to know more about the core ideas behind scrum? Well, keep reading to find out!

> _"Scrum asks why it takes so long and so much effort to do stuff, and why we're so bad at figuring out how long and how much effort things will take."_

### 3. Effective project management is all about promoting great teamwork. 

When you're working on a project, your best chance for success lies in how well your team works together. And as a manager, you can improve team performance by making four changes.

Firstly, let team members decide how they're going to reach objectives.

For instance, NPR's award-winning reporting on the Arab Spring in Egypt was only possible because the staff who were responsible for coverage had autonomy. The team made all the decisions about how to produce their stories, working together to navigate Egyptian bureaucracy, translation, issues of safety, and so on.

Here's another way to improve team performance: Teams accomplish exponentially more than individuals can on their own, so make sure your team has a broader purpose. In other words, teams are more powerful than the sum of their parts, so expand your staff's expectations and make sure everyone's working toward the same common goal.

This was probably another factor that contributed to NPR's success during the Arab Spring: This once-in-a-lifetime event was a huge reporting opportunity, which gave the journalists a higher sense of purpose.

And the third change you can make to improve teamwork is by asking different teams to periodically share results and create new, self-sufficient teams.

This is important because teams should be _cross-functional_, and have every skill needed to complete a project. Because it's about the final product and not just the individual team members, diversity in skills, thinking and experience will help achieve better results.

And finally, help your teams to work better by reducing their sizes. For most teams, seven members (plus or minus two people) are ideal.

Why do small teams make a difference? Well, increasing the number of people involved in a team creates more communication channels, which the brain can't really handle. When we have to work too hard to figure out what everyone else is doing, we slow down.

> _"Small teams get work done faster than big teams."_

### 4. Create a regular feedback system to keep your project on track. 

As you surely know, humans are bad at estimating how long things will take — and this leads to major project management problems. Luckily, Scrum addresses this issue through a system called _Sprints_, which will help your team with time management.

_Sprints_ are short periods of work (one to four weeks is optimal) focused on a specific task. After a Sprint, the team meets to review progress and refine goals before embarking upon the next Sprint.

The ultimate advantage of this process is that it allows you to respond to problems quickly; by checking in regularly, you can re-calibrate the objective for the next round of Sprints. That way, no one spends months working on something that ultimately gets discarded.

In order to use this process efficiently, focus on one task per Sprint. The Sprint periods are so short, you'll be able to concentrate effectively. It also helps to establish a consistent working rhythm: don't follow a one-week Sprint with a three-week Sprint.

And for another way to improve time management, try _Daily Stand-Ups,_ daily meetings which always occur at the same time and ask the same questions: What did you do yesterday to help the team finish the Sprint? What will you do today? What obstacles are you facing?

Make sure everyone stands up, and make sure that the entire meeting lasts no more than 15 minutes.

The author's friend, Eelco Rustenburg, actually put Daily Stand-Ups into practice when he was remodeling his house. The project was completed in just six weeks — on schedule! The short daily meetings brought every team member together to discuss their progress.

Eelco's successful remodeling process impressed his neighbors, who tried to replicate the results in their own home. They hired the same group of contractors to work the same job — but didn't use Scrum. And without Scrum, Eelco's six-week project took them three months!

> _"The greater the communication saturation — the more everyone knows everything — the faster the team."_

### 5. Avoid anything that distracts you from meeting your goals. 

We've arrived at another core idea in Scrum: avoid _waste_ — that is, anything that distracts you from completing the task at hand. Here are a few ways to eliminate waste.

Firstly, finish your projects by focusing on one thing at a time. Multitasking might seem appealing, but it just wastes time and energy. Do you really want to promote the project management equivalent of texting while driving?

You can also avoid waste by using what you've created. If you don't use what you create you expend effort without seeing the fruits of your effort, and you waste time and money. General Motors experienced a great loss when it started layoffs in 2012, because it had $7.5 billion in unsold trucks just sitting around.

Another way to avoid waste: If you make a mistake, fix it immediately, because it will _always_ take longer if you have to fix it later.

In fact, a study by Palm, an American smartphone manufacturer, found that it takes an average of 24 hours to fix a bug three weeks after it emerges. By comparison, it takes just one hour to fix the same bug the same day it's observed. The reason for this discrepancy? Well, remembering all the different factors that caused the bug wastes a ton of time.

Overworking your employees is another thing to avoid if you want to eliminate waste. Because when overworked employees get distracted, they mess up, and sometimes when they mess up they distract others and create still more mistakes. Along those lines, working fewer hours, taking vacations and eating lunch outside of the office will make everyone happier and improve the quality of their work — ultimately boosting productivity.

To that end, groups of researchers demonstrated that Israeli judges make better-reasoned decisions after a break. On the other hand, judges are more likely to make unsound decisions just before a break, when they're low on energy.

We've discussed a few different ways to avoid waste, but the most important guideline for achieving this is simple: Be reasonable, and don't waste your employee's motivation by setting impossible goals. After all, a series of crises and near-misses will burn your employees out.

> _"Waste is a crime against society more than a business loss." — Taiichi Ohno_

### 6. Increase your employees’ happiness in order to boost productivity. 

How is happiness connected to success? Well, people aren't happy because they're successful — _they're successful because they're happy._

Zappos is a great example of this principle. The über-successful retailer works to keep its employees happy by focusing on _connection_ via various programs — like a "boot camp" introductory training and internal "apprenticeships" — to foster learning and growth. And thanks to these policies, Zappos has been rewarded with 124 percent year-over-year growth.

In addition to connection, _visibility_ is another value companies can support to promote happiness. Visibility is the opposite of secrecy, and secrecy is toxic, because it creates suspicion and mistrust, which ultimately lower motivation and damage performance.

In addition to avoiding secrecy, you can also promote visibility by putting projects in a place where everyone can see them.

You can do this by creating a Scrum board that lists every project under one of the following columns: Backlog, To-Do, In Progress, In Review and Done! This way, everyone can see which projects are underway and which are stalled, ultimately giving employees the opportunity to step in and help where they're most needed.

Another way to improve visibility is to identify the _kaizen_ — this is the Japanese word for "improvement" — after each Sprint. You can do this by organizing a _Sprint Retrospective_ after each Sprint. You can ask your team questions such as:

  * On a scale from one to five, how do you feel about your role in the company?

  * On the same scale, how do you feel about the company as a whole?

  * Why do you feel that way?

  * What one thing would make you happier in the next Sprint?

Pay special attention to the answers to the last question and, if possible, apply the improvements immediately. Doing so will promote trust and happiness on the team, which will ultimately lead to better results.

### 7. Prioritization is a crucial component of project management. 

The last core value of Scrum is knowing how to prioritize. And this is exactly the job of the _Product Owner_ — figuring out what to do, when.

But first, taking a step back, there are three roles in the Scrum model:

  1. The team member.

  2. The Scrum Master, who helps the team figure out how to work together effectively.

  3. The Product Owner, who's responsible for the overall vision for the project, managing the backlog and setting the course for each sprint.

And to do the job effectively, the Product Owner should possess the following characteristics:

  1. Knowledge of their particular market.

  2. Authority to make decisions without interference from management.

  3. Availability to explain what needs to be done to team members.

  4. Accountability for the final product, or for how much revenue is produced.

It's worth noting that the Product Owner role was inspired by Toyota's Chief Engineers, who are responsible for their own product lines, like the Corolla.

Although the Corolla Chief Engineer creates the Corolla team, they don't actually report to her. Rather, the Chief Engineer's role is about guiding the project by creating a vision and persuading the team that they should share it.

Within the scrum system, Product Owners work with an _OODA loop_ to make decisions based on real-time feedback. OODA stands for _Observe_ (start the process by seeing where you are), _Orient_ (evaluate how to create more possibilities), _Decide_ and _Act_. Once these four steps have been completed, the Product Owner starts again from the beginning, with Observe (evaluating results of the prior action).

Fighter pilots are actually trained to use this decision-making process: First they assess current danger, then modify their position, figure out what to do next, and finally they follow through on that action — before starting the loop all over again.

When they're trying to decide which Sprint to embark upon, Product Owners should follow this very process.

### 8. Follow these simple steps to launch your first Scrum project. 

Now that you've seen how Scrum can help you complete projects, how can you implement the system in your own workplace? Well, there are just a few simple steps:

  1. Pick a Product Owner.   

  2. Choose a team. Remember, there should only be about five to nine people per team, and they should encompass all the skills needed for the project.   

  3. Pick a Scrum Master — someone who has already read these blinks. This person will be responsible for coaching everyone on how to maintain good Scrum techniques.   

  4. Create the _Project_ _Backlog_ — that is, a list of tasks you'll need to complete.   

Move the tasks with the highest value and the lowest amount of risk to the top of the backlog. Do this by asking questions like: Which tasks have the biggest business impact? Which are most important to the customer? Which will earn the most money? Which are easiest to complete? _  

_ _  

_Of course, you should also pay attention to what you can realistically achieve and what makes you passionate. But when you're looking at the backlog and making decisions about where to start, prioritize by what's easiest to accomplish and most valuable.  

  5. Make sure that everything in the backlog is actually doable in Sprints — meaning, it should take less than a month to achieve.   

  6. Host the first Scrum meeting and plan the first Sprint.  

  7. Create a Scrum Board to make sure all the work is visible.   

  8. Hold your Daily Stand-Up meetings to ensure that the initial Sprint runs smoothly.   

  9. When you've completed the Sprint, organize a Sprint Review so that the team can demonstrate that they've produced something usable. Keep this meeting open to anyone who wants to attend, including company executives and management.  

  10. Hold a Sprint Retrospective, and find actionable improvements you can implement to support your team in future Sprints.   

  11. And once this is all done, move on to your next Sprint!

### 9. Final summary 

The key message:

**Because it's based on employee-friendly principles like team building and transparency, the Scrum system will make your company happier and more productive. Ultimately, the core idea of Scrum is to create a process of constant feedback. So as long as you review and refine your goals on a consistent basis, you'll be heading in the right direction.**

**Suggested further reading:** ** _Less Doing, More Living_** **by Ari Meisel**

_Less Doing, More Living_ outlines some fundamental tools and methods for being efficient in every sphere of life, from sleeping to working. The end goal is for you to be able spend just 20 percent of your time on work, with the other 80 going toward recreation and self-improvement.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Jeff Sutherland

Jeff Sutherland is the co-founder and CEO of Scrum Inc. A West Point-educated former fighter pilot, Sutherland currently holds an advisory role at 11 different technology companies.

