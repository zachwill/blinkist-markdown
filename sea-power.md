---
id: 59db8810b238e10005594b05
slug: sea-power-en
published_date: 2017-10-13T00:00:00.000+00:00
author: Admiral James Stavridis, USN (Ret.)
title: Sea Power
subtitle: The History and Geopolitics of the World's Oceans
main_color: 44B8E1
text_color: 2D7994
---

# Sea Power

_The History and Geopolitics of the World's Oceans_

**Admiral James Stavridis, USN (Ret.)**

_Sea Power_ (2017) provides an enlightening look at the role Earth's oceans have played over the course of human history. From early voyagers who sailed into the vast unknown, to the tens of thousands of commercial ships now traversing the globe on a daily basis, our oceans have always been a powerful force that we've longed to tame and control. While we've come a long way, we still find ourselves faced with immense challenges that we'll only overcome by working together.

---
### 1. What’s in it for me? Journey through our planet’s violent waters to see how they have shaped history. 

When we look out over the ocean, it's easy to stand in awe of the incredible power of nature. But over the course of human history, the oceans have also played a central role in political power around the world, right up until the present day.

In these blinks, you'll learn the shapes of the seven seas and how they've determined the destinies of nations, from the first explorers that ventured out into the open waters to today's most alarming potential naval conflicts and ongoing geopolitical tensions.

You'll also discover

  * how much of the Pacific Ocean the famous Captain Cook charted in the late 1700s;

  * how melting Arctic permafrost results in new naval routes opening up; and

  * why overfishing and industrial dumping are urgent crises that we must tackle now.

### 2. Adventurous explorers and brilliant deals set the stage for exploring the Pacific Ocean. 

When it comes to sheer size, the Pacific Ocean reigns supreme. Believe it or not, you could combine every landmass on the planet, and it would still be smaller than the Pacific's 64 million square miles of surface area.

The Pacific is so vast that it wasn't until the 1500s that explorers began to fully discover the secrets that lay beyond the western shores of the Americas.

The intrepid Portuguese explorer Ferdinand Magellan would be the first European to reach the distant Pacific islands of Guam, the Philippines and Cebu. This last destination was where Magellan was killed by locals after making the mistake of getting involved in their affairs.

But perhaps the most celebrated sailor of the Pacific Ocean was James Cook. A captain in the British Royal Navy, Cook embarked on a series of voyages in the late 1700s that charted what was previously unknown, including key ports in places like Hawaii, Tahiti, Western Canada, Easter Island and parts of New Zealand.

The United States greatly increased its interest in the Pacific following the gold rush in the mid-1800s. During this time, people from all over the country flocked to gold-rich hills of the US west coast, just as coal-powered ships were being introduced.

These boats required coaling stations for long trips across the Pacific, which is exactly what Hawaii became when it was annexed by the United States in 1898. To this day, the Hawaiian islands continue to serve as a reliable gateway for the United States in the Pacific.

A far more questionable land deal was made by the United States in 1867 when Secretary of State William Seward orchestrated the purchase of Alaska from Russia. Unlike Hawaii, this land was in a much chillier part of the North Pacific Ocean, and many vocal critics believed Seward had wasted money on a useless chunk of frozen tundra.

They may have called it "Seward's Folly" at the time, but as we'll see, Alaska came to play a key role in the US economy further down the road.

### 3. The Pacific Ocean was a battlefield in World War II, and it’s currently home to a dangerous arms race. 

For many Americans, what happened at Pearl Harbor in the final days of 1941 still stands as a singular event. It was a devastating surprise attack on a US military base, unlike any that has happened before or since, and it made World War II something the United States could no longer ignore.

The United States would go on to spend years fighting Japanese forces in the Pacific, a massive oceanic battlefield that spanned a distance equivalent to that of the English Channel to the Persian Gulf.

But if we look closely at the history of Japanese military tactics, we can see that the use of a surprise attack was nothing new.

Fifty years before Pearl Harbor, Japan launched a surprise attack on a Chinese convoy that was delivering reinforcements to troops in Korea. This resulted in months of fighting, and to Japan eventually reasserting its claim to Korea and a number of other Pacific islands.

In 1904, Russia stepped up and contested Japan's expansion plans, and the Russo-Japanese War ensued. In the end, Russia lost both their Baltic and Pacific fleets as well as Port Arthur, a useful gateway to the Pacific in Northeast Asia.

But all of these conflicts were minor compared to the epic scale of the Pacific Theater of World War II.

It took years of grueling battles, but US forces went from one island to the next, pushing back the well-entrenched Japanese forces inch by inch.

Following World War II, the United States kept an active presence in the Pacific, intervening during both the Korean and Vietnam wars to protect its geopolitical interests in the region.

But these days, a new challenge is arising as many Pacific nations are increasing their stockpiles of arms and bringing the region ever closer to war. Between 2013 and 2015, China increased its military spending by 26 percent, while North Korea doubled its budget. Over the same period, US and European Union defense spending have actually dropped!

While it would be unwise to escalate tensions in the region, which are already dangerously high, neither should the United States be reducing its defense budget when the potential for war is so great.

### 4. The Vikings were the first Atlantic explorers, while the Portuguese greatly expanded the charted seas. 

Covering 40 million square miles, the Atlantic Ocean is second only to the Pacific in terms of size. The Atlantic accounts for 20 percent of the world's surface area, and that's without including its two significant tributary seas, the Caribbean and the Mediterranean.

The Greeks were likely the first to voyage into the unknown open waters of the Atlantic, but as far as recorded history goes, the Vikings were the first Europeans to venture out and catch sight of the new world.

The Vikings were especially active between 800 and 1000 AD, and at the end of the tenth century, a particularly bad storm pushed the ship of Bjarni Herjolfsson so far west that he spotted mysterious new land.

The Portuguese also began exploring the Atlantic in the fifteenth and sixteenth centuries, but these were voyages of a different kind. While the Vikings are generally considered _explorers_, the Portuguese are seen as _discoverers_ who, by contrast, were on a mission to expand and colonize.

And that's just what they did, with the help of new ships that had multiple masts and sails, and sailors who had improved navigational methods that included a keen understanding of the stars.

The Portuguese were also pioneers in using lightweight ships that took advantage of wind and water currents to open up quick routes, carrying them from Lisbon down the coast of northwest Africa.

One of the most important of the early Portuguese explorers was Infante Henrique, or Prince Henry the Navigator as he came to be known, who discovered the abundance of spices and gold that lay in northern Africa.

He was followed by Vasco de Gama, Bartolomeu Dias and Pedro Álvares Cabral. Cabral would eventually launch an epic voyage in the early sixteenth century, one that would forever connect Europe with South America, the Indian Ocean and Asia.

These explorers would mark the dawn of the Oceanic age, but the trade routes they created would also make way for the tragic exploitation of the people of Africa.

### 5. Great Britain has an unrivaled record of naval supremacy in the Atlantic. 

In the 1500s, King Henry VIII pushed for England to become a major player in the seafaring world, and the country never looked back, proving time and again that it was home to the world's best naval force.

Henry ordered heavily armed warships to be built just in time to challenge Spain for European supremacy.

In the years following Henry's reign, the British and Spanish clashed in the Atlantic, with Britain winning the day thanks to talented captains at the helm of lighter and more agile ships.

In the 1600s, the Dutch were Britain's next rival, followed by France in 1700s. The Royal Navy eventually bested both of them while making the key acquisitions of Canada and several Caribbean islands.

However, when the French came to the aid of the rebellious Americans, they did manage to deliver one of the only significant defeats in British history.

This occurred during the American Revolution of the 1770s. At the time, the Americans didn't have a naval force that could compete with Great Britain — but they did have a willing partner in France. In 1781, the French outgunned the British at sea near Yorktown, helping to secure the independence of the United States.

While it was a big loss for the British, there were more pressing concerns closer to home. In the early 1800s, the British Navy was able to reassert its dominance against the ambitions of Napoleon Bonaparte. In fact, Napoleon's inability to best the British Navy was a significant contributor to his downfall.

The British would eventually reclaim naval dominance over the United States in the War of 1812, which led to a peace treaty and a "special relationship" between the two powers that was tested during the wars of the twentieth century.

During World War II, British Prime Minister Winston Churchill saw the battle against German U-boats in the Atlantic as "the dominating factor" throughout the war. Both the land and air campaigns were dependent on the defeat of the U-boats, and getting supplies and troops delivered safely by sea.

Thanks to improved sonar, depth charges and brilliant code breakers, the campaign against the U-boats succeeded, and once again the Royal Navy claimed victory.

> Almost 3,000 Allied ships and more than 20 million tons of shipping were destroyed by German U-boats.

### 6. The Indian Ocean has always been a valuable trade route, and today it is also an area of great resources. 

Since the days of ancient Greece, the Indian Ocean has been a venue to trade spices, textiles, livestock and just about anything else that could fit on a boat. For centuries, Arabs, Persians and Chinese all used these waters to trade peacefully among themselves.

In the late 1400s, the Portuguese became the dominant traders in the Indian Ocean, but they were surpassed in the 1600s by the highly competitive British East India Company and Dutch East India Company.

These two companies eventually merged, and by the 1800s, the Indian Ocean became known as a "British lake." Around this time, the Suez Canal also became the "protectorate" of the British. Located in Egypt, it offered a valuable link between the North Atlantic and Indian Oceans, via the Mediterranean Sea and the Red Sea.

By the time the steam engine arrived on the scene in the nineteenth century, the British had acquired strategic territories such as Egypt, India, Iraq, Kuwait, Kenya, Sudan and Singapore.

When World War II broke out, the British believed Singapore would withstand any invasion, but it came under Japanese control from 1942 to 1945. During this time, the Axis powers of Germany and Japan had a gateway into the Indian Ocean, and the 200 years of it being a "British lake" came to an end.

By the late 1960s, the British began a strategic withdrawal from the Indian Ocean. Meanwhile, incredible natural resources had been discovered in the area, especially in the Arabian Gulf.

For most of human history, locations like Abu Dhabi, Bahrain and Dubai were predominantly known as humble fishing villages. But in the mid-twentieth century, these locations were revealed to be home to as much as two-thirds of the world's oil reserves, and a third of its natural gas, thereby completely changing their status.

### 7. Today the Indian Ocean is home to dangerous tensions, and the United States should respond with careful diplomacy. 

There are many reasons for the continued tensions in the areas surrounding the Indian Ocean, not the least of which is religion.

While more than 90 percent of the world's Muslim population resides near this ocean, the countries are divided by being either Sunni or Shi'a Muslims — or in the case of the Iraq, both.

This Sunni-Shi'a conflict is at the heart of a great deal of tension, which has the potential to erupt into armed conflict at any moment.

Since the late 1960s, following the British withdrawal from the area, there have been cultural, religious and geographic disputes between India and Pakistan that continue to be a great cause for concern.

Both of these nations are armed with nuclear weapons, and there has thus far been no sign of a peaceful resolution on the horizon. A particularly bitter point of contention is the region of Kashmir, which both nations claim as their own.

The relationship between China and India has also been highly antagonistic. While China continues to expand its influence around the Indian Ocean, it is likely that India will eventually overtake China as the most populous nation in the world.

Just how important is the Indian Ocean? Well, 50 percent of all the world's shipping passed through these waters, which some would say makes it more important than either the Atlantic or the Pacific. It also means that the United States needs to help keep this region stable and operating smoothly. As such, anything the United States can do to reduce tensions and encourage diplomacy is a positive.

The United States would also be wise to better recognize India's potential as a global leader. Since it is a democratic nation and holds many of the same values as the United States, more could and should be done to bring the two nations together.

### 8. The Mediterranean was the birthplace of strategic sea battles and a stronghold of the Ottomans. 

The Mediterranean might seem like a relatively small body of water on your world map, but if you measure it from east to west, it's as wide as the entire United States.

The Mediterranean starts as you leave the Atlantic and pass through the narrow Strait of Gibraltar, which separates Spain and Morocco. Two of its most prominent features are the boot of Italy, which juts down in the middle of the sea, and the Greek peninsula, which divides its eastern portion.

Because of its unique geography, the Mediterranean is where maritime strategy began.

These protruding land masses, as well as islands like Crete and Sicily, made the Mediterranean the perfect battleground for the earliest geopolitical maritime battles. It is where the Roman, Persian and Carthaginian empires devised strategic battle plans and engaged in some legendary warfare.

The Holy Roman Empire would eventually use the Mediterranean as a launching pad of sorts for crusaders to establish kingdoms throughout the Middle East.

Then came the arrival of the Ottoman Empire.

After arriving on the scene in the fourteenth century, the Ottoman Turks appeared unstoppable for centuries. But by the second half of the sixteenth century, Pope Pius V, who was determined to defeat the Turks, put everything he had into the Battle of Lepanto.

On October 7, 1571, off the western coast of Greece, 200 ships from the Pope's Holy League met 250 Ottoman ships, making it the biggest sea battle in 16 centuries. The Pope had sent massive, three-mast ships called galleasses, which were used to force the Turks into evasive maneuvers that exposed them to heavy cannon fire.

The loss at Lepanto also exposed Ottoman vulnerabilities, and their dominance would gradually decline over the next 200 years. Eventually, the Napoleonic Wars of the eighteenth century, along with the two World Wars of the twentieth century, would result in relative peace in the Mediterranean.

### 9. Hopes for continued peace in the Mediterranean Sea are under threat from Russia and ISIS. 

To the northeast of the Mediterranean is the Black Sea, a place that has proven to be a hotbed of aggressive activity.

After World War II, things were relatively peaceful. But in the 1990s, as Yugoslavia started to splinter and break apart, the Balkan Wars began.

During this time, Bosnian Muslims, Orthodox Serbs and Catholic Croatians engaged in horrific fighting, and while the brutality eventually came to an end, persistent, related conflicts have since arisen; in the meantime, the Black Sea has become something of a "wild west," as many smugglers and criminals continue to use the sea as a gateway to Europe.

Further complicating matters are Russia's aggressive attempts to reassert its presence in the Mediterranean. The author was once told by a Ukrainian naval officer that "Russia will never give up Crimea. Never." And this has certainly proven to be the case.

In 2014, Russia invaded the Ukrainian peninsula of Crimea and annexed it, an example of Russia's deep desire to be a dominant force in the Black Sea, which many have deemed to be a "Russian lake."

Russia has also been expanding its supportive relationship with the Bashar al-Assad regime in Syria, which is in direct opposition to NATO allies who are hoping to end Assad's violent rule.

Then there's the troubling presence of Islamic State, or ISIS, in the Mediterranean. These extremists have been attempting to start a religious war and spread violence across Europe, and it's another reason why this area deserves attention.

Ideally, a NATO task force could be established to help with surveillance and monitoring, especially around Italy, as ISIS had declared Rome to be their most desirable target.

It should be possible for NATO, the United States and anti-ISIS Arab nations to work together, gather intelligence and keep the Mediterranean safe.

### 10. The Caribbean has a colorful and troubled history, and the United States should take a more humanitarian approach to the region. 

The Caribbean will always be known as the home of sixteenth-century pirates, many of whom started out as Protestant raiders looking to steal from the Catholic Spanish royalty.

Sir Francis Drake is perhaps the most famous of these Caribbean pirates, having died a rich man after stealing his fair share of Spanish treasures.

Another famous figure was the Welsh-born Sir Henry Morgan. He raided so many Spanish commercial vessels that he was rewarded by England with the governorship of Jamaica, which he subsequently turned into a safe haven for other pirates.

More recently, though, crime in the Caribbean has been drug related, and it's a problem that the United States could do a better of solving by shifting toward a humanitarian approach in the region.

Many narcotics move through the Caribbean on their journey from South America to the United States. And for too long, any help the United States has provided has been part of their failed "war on drugs."

A better policy would be for the United States to recognize their moral obligation to help their Caribbean neighbors, especially the ones that struggle with corruption, violence and natural disasters.

It is particularly shameful that Cuba, a country so close to the United States, should still be ruled by a dictatorship. Currently, there's a "normalization" process underway, marked by a loosening of travel restrictions. The United States should embrace this process as a way of making Cuba a partner in democracy.

But for this to work, the United States will need to fix the situation surrounding its Guantanamo Bay Naval Base.

Cuba would prefer to see this land returned to them, but they may agree to let the United States keep it if it were to become a base of operations for genuine humanitarian operations in the area, rather than a prison for alleged terrorists.

The Caribbean is also routinely hit by devastating hurricanes and earthquakes, so it's fundamental to anticipate the next one and be ready to help these islands recover when it strikes.

When the Caribbean is healthier, with less poverty, violence and corruption, only then will we see a reduction in criminal and drug-related activity.

### 11. The Arctic Ocean holds great promise, as long as the United States invests in this area properly. 

Global warming is very much a reality, and if you need tangible evidence, the unprecedented melting of the Arctic should be exhibit number one.

For every degree that Earth's overall temperature increases, the temperature at the North Pole rises by five degrees. And while this situation has its dangers, it also comes with opportunity.

When the Arctic permafrost melts, it could release catastrophic levels of methane into the atmosphere. And while we should be doing all we can to meet the goals of the Paris Climate Agreement, we must consider the reality of the situation.

As the ice recedes, new maritime routes are opening up.

Russia is already moving to be the dominant force in a more open Arctic Ocean, knowing all too well the vast reserves of oil, natural gas and untold valuable metals that are to be found in the Arctic — not to mention the new fishing areas.

Russia is in constant competition with Canada, Norway and Denmark (which controls Greenland) over territory, but the United States has a major stake as well, thanks to the forward thinking of William Seward when he bought Alaska back in 1867.

Drawing territorial lines in the Arctic has the potential to be one of the most important geopolitical developments of our day, so it is important that the United States plays its cards right and not be left in the dust.

The Arctic has traditionally been an afterthought for the United States, and even though Obama became the first president to visit the area, the country still lacks a clear and viable strategy.

First of all, more icebreaker ships need to be acquired, as the United States currently has only three, while Russia has more than 30. There's also work to be done with NATO, as well as Russia, to form cooperative partnerships and centers for operations.

With a solid interdepartmental agency, the United States could soon have its own, robust Arctic policy — one that is environmentally friendly and focused on sustainability.

### 12. Piracy, pollution and environmental concerns can’t be ignored. 

Today, there are between 50,000 to 60,000 commercial ships moving through the oceans, along with around 5,000 military ships.

This is four to six times more ships than just 30 years ago. And while we've made many impressive advances, especially in terms of navigation and mapping, we've done remarkably little to reduce criminal activity and environmental damage caused by shipping.

Pirates remain a real threat these days, especially in the Gulf of Guinea, where the Islamic terrorist group Boko Haram has been regularly ambushing ships. A very effective deterrent is to hire security teams and have them be a visible presence on ships, but a more long-term solution would be to fix the problem where it begins — on land.

Piracy isn't an ideal career choice for anyone, and people usually decide upon it for one of two reasons: it's either the result of having no other options for survival, or it's sponsored by terrorists trying to fund their operations.

So, positive and long-term results can be achieved if we work on improving the conditions and political stability in the regions where piracy begins.

Another very serious problem that must be addressed is pollution and overfishing.

There's always public outrage when an accident occurs, for instance when millions of gallons of oil spilled into the sea in the Deepwater Horizon disaster of 2010 or the Exxon Valdez spill in 1989.

These were certainly tragic events, but they're not where our attention should be focused. Big spills like these are quite rare compared to the steady amount of toxins being dumped into the oceans by numerous companies every day.

Experts suggest that industrial and agricultural companies are pouring around 500 million gallons' worth of poison into the oceans every year!

Then there's the very immediate crisis of overfishing, which has resulted in 90 percent of every fish stock being exploited and overfished. Things have gotten so out of hand that the number of all fish stocks are at half of what they were in the 1970s. Clearly, this is not sustainable behavior.

Simply put, we need international maritime cooperation, as well as an improved international treaty on maritime law that includes fishing regulations and more protected waters.

### 13. Many factors determine a nation’s power on the seas, and there are steps the United States can take to remain a major player. 

One of the determining factors of a nation's naval power is its geography; you won't get far as a seafaring nation unless you have a good amount of coastline and easy access to open waters.

If that's the case, the nation must then make sure its policies and politics are aligned with the many ways the sea can bring prosperity. This means having beneficial trade policies and foreign relations, while also putting enough money aside to build great ships and have well-trained crews.

Great Britain is perhaps the best example of a mighty naval power that used all these factors to its advantage.

As for the United States, there are some pressing modern-day concerns that need to be addressed if it hopes to stay at the front of the pack.

First of all, despite what some people may think, NATO shouldn't be abandoned. Throughout history, every great naval power had powerful allies and friendly ports around the world.

Along with a strong network of allies, the United States needs a stronger fleet of ships if it intends to keep up with the likes of Russia and China.

There are currently two or three US ships in the Mediterranean, but that number should be closer to ten to meet the demands of combating ISIS and other dangers in the region.

In the South China Sea, there need to be more US submarines to balance out what other nations have in the area, as well as to establish a state-of-the-art _THAAD_, or Terminal High Altitude Area Defense system. This system could work alongside our Japanese and South Korean allies to destroy any dangerous missile strikes coming from North Korea.

In the Indian Ocean, friendly ties need to be strengthened with India, New Zealand and Australia by conducting more cooperative military training missions.

Finally, in the Arctic Ocean, besides adding at least four more icebreaker ships, more ports need to be opened, and an emergency search-and-rescue base needs to be established — after all, the Arctic is a dangerous place.

In the end, it takes a strong naval power to maintain peace and prosperity in the world.

### 14. Final summary 

The key message in this book:

**The oceans of the world have amazing stories to tell, showing us how remarkable humanity's achievements have been over the years, but also revealing how far we still have to go. By understanding our history through the lens of naval power, we can identify the mistakes of the past and steer clear of them in the future. From climate change and overfishing to terrorist threats and nuclear weapons, the world is home to many potentially catastrophic issues that require decisive action.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _A World Without Ice_** **by Henry Pollack**

_A World Without Ice_ (2009) is about our planet, its climate, its human residents — and ice. Ice has always been a major player in Earth's climate. These blinks explain why we may soon see a world without ice, why that would a have dramatic consequences for Earth and humans alike, and how we can cope with climate change.
---

### Admiral James Stavridis, USN (Ret.)

Admiral James Stavridis, USN (Ret.), spent close to four decades as an active member of the US Navy, where he served as commander of combat-ready destroyers and carriers. Before retiring, he was decorated as a four-star admiral with the rare honor of being the Supreme Allied Commander for Global Operations at NATO. He has since become dean of Tufts University's Fletcher School of Law and Diplomacy.

