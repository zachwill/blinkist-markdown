---
id: 55925e156561380007710000
slug: second-chance-en
published_date: 2015-07-02T00:00:00.000+00:00
author: Robert T. Kiyosaki
title: Second Chance
subtitle: For Your Money, Your Life and Our World
main_color: E5862E
text_color: 995A1F
---

# Second Chance

_For Your Money, Your Life and Our World_

**Robert T. Kiyosaki**

_Second Chance_ (2015) outlines the reasons for the growing wealth gap in the United States and offers advice on working your way up the economic ladder. It explains how you can leverage assets and debt to enjoy more financial success in your future, while lifting your nose from the daily grindstone.

---
### 1. What’s in it for me? Discover why the gap between the rich and the poor is so big. 

When the financial crisis hit in 2007–2008, a bewildered public struggled to come to grips with what had just happened. This was an extreme situation, no doubt, but when it comes to economics in general and how to become rich in particular, many of us still labor under a number of misconceptions.

For instance, most people still think the way to get rich is to get a degree, find a good job and work hard. As these blinks will show you, nothing could be further from the truth. Not even a well-paid job will make you rich. Our current financial system is simply not set up that way.

There is only one way to get truly wealthy: by owning and capitalizing on assets. But how do you do it? These blinks will show you.

In these blinks, you'll learn

  * how banks create money;

  * why getting a well-paid job won't make you rich; and

  * why you weren't taught finances in school.

### 2. The American economic system is based on inflation and robs people of their wealth. 

The phrase "The poor get poorer and the rich get richer" might sound like something from a cowboy film but in the United States it's never been more true.

Regular people are robbed of their wealth because the system is based on inflation. Inflation is certainly bad for the average person — it means the money they've worked hard to earn is suddenly less valuable. To make matters worse, inflation encourages people to put more of their money into their savings, but that's actually another cause of inflation. Here's why.

When you deposit your savings into a bank, the bank then lends your money out to debtors. However, banks lend out much more than they receive in deposits.

Prior to the crash of 2007, banks had a lending limit of $34, meaning they could only lend out $34 for every dollar they actually had. But that increase in the money supply causes inflation, making your one dollar worth much less.

This sort of wild lending periodically results in financial bubbles, which the monetary system then answers with _bailouts_. Bailout money is taken from taxpayers, meaning bailouts are actually _built into_ the system. That's another way the current monetary system creates poverty.

However, bailouts and a system of inflation are only harmful to the people who work for a paycheck: employees, freelancers, small businesses and specialists. These are the groups that pay the highest taxes and tend to keep their savings in the bank.

This is why the middle class is shrinking so quickly. In 1970, 50.3 percent of Americans earned a middle class income, but by 2010, that figure had decreased to 42.2 percent.

> _"Simply said, money is designed to keep people poor and slaves to those who control the monetary system."_

### 3. Most people aren’t well-educated on financial issues. 

There are a number of different views of wealth but one thing is certain: everyone wants to be wealthy. Unfortunately, schools teach people how to be poor instead.

Modern education is designed to turn people into employees, taxpayers and consumers. People often assume that education is the solution to poverty: if you study harder, you'll get a better job and earn more.

Unfortunately things aren't that simple. You can have three master's degrees and still be unemployed and poor. There's certainly nothing wrong with being a hard-working, taxpaying consumer, but that alone won't make you rich. Schools don't actually prepare students to make money in the world — schools don't offer any sort of financial education.

This leaves many defenseless against financial crises. For example, most people think the 2007 crisis was a stock market crash but it was actually a _derivatives_ market crash. Derivatives are the complex and risky insurance policies that made up a massive part of the market at the time.

If the population had better financial education, they might've known the increase in the derivatives market between 2000 and 2007 presented big risks and would eventually cause the crash. And they'd be better equipped to face similar problems in the future.

The lack of education also leads to misconceptions about money and wealth. Many people mistakenly assume that wealthy people are evil and purposefully make others poor. In some cases there's truth to that, but not always. Some people become wealthy precisely _because_ they're generous.

John D. Rockefeller was one example. He became rich by selling gas at cheaper prices than any of his competitors, which also made life much easier for millions of working-class people who didn't have access to gas before.

> _"If you want to become richer, you may want to find ways to become more generous. Ways to serve more people."_

### 4. You can’t become wealthy without adequate financial education. 

When people say you need to be educated to be wealthy, they usually mean you need to go to university and get a degree. What you actually need is financial education.

Schools don't teach financial education because it's too powerful. Financial education means looking beyond traditional explanations and gaining the knowledge necessary to create more assets — and it has the power to make you wealthy. In the past, slaves weren't allowed to read and write, because it would've given them too much power over their owners. These days, most people are deprived of financial education for the same reason.

Real wealth isn't about getting a big paycheck — that's another misconception. It's about having assets that put money into your pocket without you having to work for them. You need education to know how to create them.

If you buy an old house, then repair it and start renting it out, you'll have created an asset. You'll get rent from the tenants without having to work for it. Moreover, the government rewards assets with lower tax rates, so that'll increase the wealth you bring in.

People who haven't been educated on finance think in terms of income and expenses instead. That's why they often mistake liabilities for assets.

Many people see their own house as an asset, for instance. It's actually a liability because it takes away your money through mortgage payments and taxes.

> _"The moment a person begins to look for assets they can acquire or build, their world begins to change."_

### 5. Assess your financial situation and start building your assets. 

You might think it's easy to determine whether someone is rich or poor but it's actually not that simple. You can still be poor even if you live in a nice house and drive an expensive car.

If you want to change your financial situation, you first need to know where you stand. You need an _income statement,_ where you write down your income and expenses, and a _balance sheet_ where you keep track of your assets and liabilities.

For example, imagine you have a job that pays $10,000 per month and an apartment you rent out for $1,000 per month. You also pay $2,000 in mortgage payments every month and lease a Ferrari for $1,000 per month.

Most people would assume that all of those things are assets, but actually, the sole asset is the apartment. The other things cost you money or require you to work for them.

The best way to plan for the future is to choose between the four _asset classes_. After you've assessed your financial situation, it's time to focus on acquiring new assets — even if you don't know how yet!

The four asset classes are _business_, _real estate_, _paper_ and _commodities_. Choose the one you're most interested in — it makes a difference when you're working with something you care about.

The author, for instance, is most interested in commodities and real estate because he genuinely likes gold, silver, oil and historic buildings. His interest also motivates him to study his assets and hold onto them instead of selling when prices rise.

Acquiring assets is the first step toward your new, wealthier future. In the following blinks we'll look at how you set about it.

### 6. Choose the future you want – then aim for it. 

As we've seen, having a degree doesn't necessarily make you wealthier. Financial education is the key to success, but where do you actually start studying?

First, you need to figure out what kind of future you want for yourself. Remember: being wealthy isn't the most important thing in life. Some people are much happier working a nine-to-five job for their entire life than tackling a high-powered career.

If you _do_ want to be wealthy, however, a nine-to-five job won't get you there. So think about the future that suits you best, then aim for it. If you're looking to be wealthy and financially independent, you have to let go of your employee mentality and get into big business or professional investing.

And you can't succeed in big business or professional investing without the right education for it. You need the skills of an entrepreneur.

One of the key differences between entrepreneurs and those who are self-employed is that entrepreneurs are _generalists,_ meaning they know a little about a wide range of topics and hire specialists to deal with the rest. That's because when entrepreneurs start a business, they're responsible for every part of it: from developing the product to leading the team.

So don't specialize in a single field if you want to be an entrepreneur. Learn a bit about all the things that make a business work, like team building, leadership skills and company missions, for instance.

Strive to cooperate instead of compete. The employee mentality makes people think they have to compete with their colleagues to get ahead, but that's not the kind of thinking you want in your team. A skilled entrepreneur knows how to guide the group to success.

> _"Today we have high school kids and college dropouts who are billionaires — and it's because they're entrepreneurs, not employees."_

### 7. Learn practical thinking skills through experience and practice. 

One of the biggest differences between rich and poor people is their intelligence, but it's not the kind of intelligence measured in school. You're considered smart in school if you have a good memory and learn to read and write quickly, but in the real world, different skills help you get ahead.

For entrepreneurs, intelligence is all about handling risks and financial losses, learning from mistakes and staying level-headed. Entrepreneurs build their businesses from the ground up using practical skills that are much more important than remembering who the 36th president was.

So how do you acquire those skills? Well, you learn from a coach or a friend, or by taking a course from an expert. Then hone your skills by _practicing_ them.

Because above all, we learn when we _do_ — it's experience that stays in our memory, not information we've memorized for a written test. And _doing_ often means taking the wrong turn, miscalculating or making errors — errors that we'll never make again thanks to a bad experience. In school we're punished for our mistakes; we're made to believe that only stupid people make them. The truth is the exact opposite.

However, you can't learn by doing if you don't have enough cash to start investing right away, so you need to simulate the experience through practice. Before the author first bought property, he spent time studying ads, visiting houses and talking to brokers. That's the best way to learn the entrepreneurial skills you need to become financially successful.

### 8. Don’t be afraid of debt – go into it to create assets. 

Most people follow simple rules regarding debt: work hard, stay out of debt and you'll be OK. But as an entrepreneur, debt might be your new best friend.

Debt can actually be a powerful tool for gaining leverage. Most people assume that if they work harder, they'll earn more and become rich. In fact, the opposite often happens: they fall into a higher tax rate and earn even less.

If you want to become wealthy, find ways to do more with less. Think about a musician who moves from only playing live shows to selling their CDs: they're getting their music to more people by doing less work.

Debt can help you in a similar way. In the finance world, it can be powerful because it allows you to do things you don't otherwise have the savings to do.

The author, for example, first got into real estate in the 1980s by purchasing a small apartment for $50,000. He paid a deposit of $5,000, and took out a loan with ten percent interest for the remaining $45,000. His monthly payments (including interest) were about $450, while rent in the area was around $750!

So use debt to create assets for yourself. Traditional thinking says you should work hard, save money and buy assets without loans, but it doesn't work like that. Most people can't save enough to buy valuable assets — it makes much more sense to create them by taking out loans.

The author had a friend who had a lot of success with this strategy. He went into debt so he could buy a 150-year-old church in Scotland, which he then turned into an exclusive housing complex. The building had been in ruins for years and people walked by it all the time. But he saw the opportunity to turn it into an asset — he made money for himself and provided people with interesting new homes.

> _"Debt is the most powerful force in the world of money."_

### 9. Final summary 

The key message in this book:

**The current monetary system robs the working class of their hard-earned wealth, and the education system keeps them from learning how to stop this happening. If you want to become wealthy, figure out what asset class works best for you, then research it and acquire your assets by going into debt. Steer yourself toward the future you want.**

Actionable advice:

**Expand your financial vocabulary.**

Start reading financial publications like the _Wall Street Journal_ and look up the words you don't know. If you learn two words per day, you'll learn 60 in a month!

**Suggested** **further** **reading:** ** _Rich Dad, Poor Dad_** **by Robert T. Kiyosaki**

_Rich Dad, Poor Dad_ combines autobiography with personal advice to outline the steps to becoming financially independent and wealthy. The author argues that what he teaches in this _New York Times_ bestseller are things we're never taught in society, and that what the upper-class passes on to its children is the necessary knowledge for getting (and staying) rich. He cites his highly successful career as an investor and his retirement at the early age of 47 as evidence in support of his claims.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Robert T. Kiyosaki

Robert T. Kiyosaki is the bestselling author of _Rich Dad Poor Dad_. He's also an investor, businessman and radio personality.

