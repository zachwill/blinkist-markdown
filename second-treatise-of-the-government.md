---
id: 54733ea33635610009320000
slug: second-treatise-of-the-government-en
published_date: 2014-11-25T00:00:00.000+00:00
author: John Locke
title: Second Treatise of the Government
subtitle: An Essay Concerning the True Origin, Extent and End of Civil Government
main_color: E54A54
text_color: B23A41
---

# Second Treatise of the Government

_An Essay Concerning the True Origin, Extent and End of Civil Government_

**John Locke**

Locke's _Second Treatise_ offers an in-depth analysis on the origin of our right to liberty and the rights of governments. It shows how, by respecting the laws of nature, we can limit the power of government to best protect ourselves and our property from destruction or worse, tyranny.

---
### 1. What’s in it for me? Learn from a political philosopher about the limits and boundaries of power. 

What is the appropriate punishment for a parking violation? A $20 fine or a night in jail?

Most of us would fight tooth and nail against jail time for a simple parking infraction, but probably shrug at the imposition of a small monetary fine. But why is this?

What are the boundaries of power, and how do we draw them?

In these blinks, John Locke, one of history's most influential political philosophers, explains how we as a society legitimize the expression of power. You'll learn too what you can do when you feel that those who exercise power over you (such as the government) have gone too far.

In the following blinks, you'll also discover:

  * when it might be appropriate to make an individual your slave;

  * why parents are completely justified in interfering in their children's liberty; and

  * what exactly constitutes a "just war."

### 2. In the state of nature, we have absolute freedom and the right to protect ourselves and our property. 

Imagine waking up in a society where there are no government institutions and no laws.

Such a state is what political philosophers call the _state of nature_.

In the state of nature, all people are free and equal; there is no person or authority that holds power over you — no one to tell you to pay your taxes or parking tickets.

What's more, everyone in a state of nature follows the same laws, namely the _laws of nature_, a code of ethics granted by God which is — through human reason — inherent in each of us.

It is this law which tells us not to harm others, and to preserve ourselves and the rest of humankind. In this sense, therefore, all people are free and equal.

Given that we may preserve ourselves in a state of nature, we must also have the right to defend ourselves. In a state of nature, as in any other social constellation, there will necessarily be people who inflict harm on others, such as through theft or violence.

We have the right to defend ourselves and our property against these individuals.

However, in defending ourselves, we must still act reasonably. This means that we may only punish others to get reparation for our losses and to deter offenders from future crimes, without being heavy-handed.

For example, if someone stole your banana from the lunch room refrigerator, in a state of nature, you would be allowed to both take the banana back and demand a cookie as reparation.

Furthermore, you might punish the thief by taking _three more_ cookies from him, to deter him from committing further crimes. However, it would be unreasonable for you to beat or murder him — no matter how delicious or precious that banana was.

> _"Being all equal and independent, no one ought to harm another in his life, health, liberty or possessions."_

### 3. You can legitimately own property and land as long as you don’t violate someone else’s rights. 

Today it's extremely simple to get the things you want. If you want an apple, for example, you simply walk into a grocery store, find an apple and pay for it.

But how would this work in a system without government or laws that govern such transactions?

According to Locke, people earn the right to ownership through their labors.

God has given us the world and all of its resources — the land, flora and fauna — so that we may _all_ use them. However, this does _not_ mean that we have no right to ownership over these resources.

Instead, people may own resources which originally belong to no one by dedicating labor to those resources. Simply put, once you put work into a given resource, then you may own it.

For example, if you cultivate a field where apple trees grow and collect the apples, you've earned the right to own the apples, even though the apples formerly belonged to everyone.

But although we can appropriate land and other goods, there are nonetheless limitations on ownership.

First, the law of nature dictates that we must respect others and their property. Thus, you can't own something that already belongs to someone else.

Second, you can't own so much that there isn't enough left for everyone else. For instance, if only one apple tree existed on the planet, you wouldn't be allowed to own _all_ of the apples it produced, even if you collected them yourself.

And third, you may only own as much as you can actually use or consume. This means that you may not own 50 apples, if you can't actually eat them all.

The earth and its resources were created for everyone's use. Locke stressed that we must then respect this by putting limits on the appropriation of land and other goods.

### 4. According to Locke, slavery is justified only when people have forfeited their right to live. 

Slavery has been abolished in nearly every corner of the globe for some time. However, at the end of the seventeenth century — when Locke was writing — things were different.

In this regard, however, Locke's thoughts on slavery are quite enlightened for his time.

Locke held that human beings are free and can't be subjected to the will of another. We are born free by nature, and no power on earth has the right to intervene with this natural freedom. It doesn't matter if one person is stronger than another, or believes he has God's mandate to rule.

Yet some will argue that a person could willingly give up his liberty and become subject to the will of another, or become a slave. This objection, however, is baseless, as this form of slavery is not possible under the laws of nature.

The law of nature tells us to preserve ourselves. We therefore can't sell ourselves into slavery, as our masters, who now own our bodies, would then have the right to kill us. That would obviously go against self-preservation.

So, while we can sell our _work_ to others, we can't offer them our lives and become property.

There is, however, one condition under which a man may become a slave, when he forfeits his right to life.

Although we are all born free in the state of nature, there are cases in which an individual completely loses his right to life. For example, if someone commits a very serious crime, such as murder, he might forfeit his right to live and be sent to the gallows.

In such cases, the person to whom the criminal has forfeited his life may decide to take the criminal as her slave.

Locke places clear limits on slavery and ownership by appealing to the state of nature. The idea of natural liberty for all human beings is also central to his ideas about political power, as you'll see in the next blinks.

> _"In transgressing the law of nature, the offender declares himself to live by another rule than that of reason and common equity."_

### 5. Political power is a different relationship, and can be granted only through the consent of all people. 

Who should have more power over you: your father or the king? Importantly, how are these two types of power different?

Locke believed that parents hold legitimate power over their children.

As we've stated, all human beings are born free according to the laws of nature; as long as we don't harm others, no one may interfere with our individual liberty.

However, things become considerably more complicated with regards to children.

As children do not yet possess the full faculties of reason, parents may rule over them until they are old enough to reason on their own. This is true even in modern society. For example, as a parent, you may restrict the liberty of your child if you catch her doing drugs, as one can assume that she isn't fully aware of the consequences of her actions.

In this sense, employing power over children is justified, even though they are also free individuals.

In contrast to parental power, however, political power is legitimate only when all people agree to it.

No one has the right to exert political power over others until _all_ people have come together and agreed to create an authority with the power to govern them.

Adults, unlike children, _do_ have the reasoning faculties necessary to decide for themselves whether they should accept subjugation to a political power.

Thus, if people want to leave the state of nature — in which there is no government and no law but the law of nature — to create a system that ensures that people and their property are protected, all potential subjects must first consent.

So Locke has said that no political ruler has the right to exert power over a society without the clear consent of the people. But do these rules still apply to people from other nations?

### 6. In war, soldiers and generals are forfeit, but the populace still retains its right to be free. 

The subjugation of other nations and peoples by right of conquest is no longer a typical activity of nations. However, as in the case of slavery, the illegitimacy of conquest was far from decided in Locke's time. Nevertheless, his thoughts on the subject remain insightful.

Throughout human history, aggressive groups and nations have attacked foreign societies with the intent to plunder their villages and subjugate their people. Think, for example, of William the Conqueror, who violently took the throne of England from King Harold in 1066.

However, despite a strong historical precedent, strength and aggressiveness do not justify political power over people. No one has the authority to infringe the rights of others and to subject them to his will simply because he has the strength to do so.

For example, if someone threatens you with a knife, their power over you isn't _legitimate_ simply because they have the power to kill you.

But even after a _just war_, in which both parties have consented to battle, conquest does not justify political power.

All those that take part in the war, such as soldiers and generals, have forfeited their right to life to whichever side wins. The conqueror thus gains not only political power over them, but ultimate power. She may even decide to kill the defeated soldiers if she wishes.

The people, on the other hand, have made no such agreement. The winner of a war, therefore, doesn't gain any power over a foreign people, as they can't be held responsible for the deeds of their government and its forces.

Additionally, the conqueror has no right to the property of the defeated soldiers, as this property also belongs their families, whose rights must also be protected.

Now that we've examined the nature of political power and its justification, the final blinks will outline what a legitimate political system looks like

### 7. In a just government, there should be a clear distinction between legislative and executive powers. 

Why is it that so many governments spread power among parliaments, prime ministers, presidents and so on?

Such systems are the result of one of Locke's most important ideas, the separation of political powers.

Locke calls _legislative power_ the "first and supreme power."

When people decide to leave the state of nature to create a political community, they forfeit their right to protect themselves and to punish those who violate their rights. This doesn't mean, however, that they become defenseless.

Instead, they delegate their right to defense to a legislative authority that will introduce laws to institutions, such as the police and courts, that are charged with protecting people and their property.

The legislative power, or the creator of laws, therefore holds primacy over other branches of government.

However, legislative power is not enough. There must also be an _executive power_ to carry out the laws of the legislature.

Here, it is crucial that those who have legislative power do not _also_ have the ability to execute laws.

If both powers resided in the same hands, abuse of power would not only be possible, but — due to human fragility — quite likely.

As history has shown repeatedly, legislators could, for example, exclude themselves from the laws they write, or pass legislation that benefits only them, such as banning dissenting speech or arresting those who disagree with them.

> _"The end of law is not to abolish or restrain, but to preserve and enlarge freedom."_

### 8. Political authorities can act against the will of the people in a number of ways. 

People often complain about politicians overstepping their authority — but what, exactly, defines the parameters of their power?

Although legislative power is the highest political power, that doesn't mean that its power is limitless.

Once a legislative power has been created and its administrators elected by the people, it must then follow _majority rule_ : everyone doesn't have to agree to create a law, just a majority of members.

However, this doesn't mean that the legislative power has free reign to do _anything_ that the majority agrees to!

Legislative power was created by the people with the specific aim of protecting their lives and property. Thus, there are clear limitations on the power of legislators, namely: they must not act against the interests of the people.

For example, the legislature may not pass laws that threaten or infringe upon individual liberties, such as through excessive taxation or other types of oppression.

In a similar vein, an executive power can also become a threat to the people, due to its right to break or act beyond the law if a situation warrants. This is called the _prerogative of the government_.

For example, if a fire is threatening to burn down a village, the executive power might have to intervene and destroy a burning house to stop the fire from spreading.

The executive power has this right, even when there is no law governing how it should behave in this particular situation, and even though it must destroy an individual's property to act.

However, even in this case, there is a clear limitation: the executive power may only break the law or act beyond the law if it doing so is in the interest of the common good of all people.

Clearly, political authorities must adhere to well-defined limits on their power. But what happens when they don't respect these limits?

Our final blink will shed light on the fundamental issue of political resistance.

### 9. The people have the right to resist a government that infringes on its liberties. 

Throughout history, and recently with the uprisings of the Arab Spring, people have decided to rebel against political authority.

This was no less true in the seventeenth century, when Locke was forming his ideas.

Locke believed there are various situations in which the people have a right to forcefully resist a political authority.

If a political authority rules like a tyrant, by acting beyond the law with only its own self-interest in mind, people have the right to resist. In this case, the political authority has essentially declared war on the people, who then have grounds to fight back.

If political power is passed down through favoritism or nepotism, that is, without the consent of the people, then resistance is also justified.

And finally, the people are justified in resisting a legislative power if that power has lost the trust of the people.

Moreover, only the people can judge whether resistance is justified — _not_ the authorities against which they are rebelling.

Those who argue against the people's right to resist sometimes suggest that there will _always_ be people who are unhappy with government, and would thus try to incite others to overthrow it.

"Who," these sceptics might ask, "should be the judge?"

The answer to this question, however, is clear: there can be no other judge but the people. If the people feel that political powers aren't wielding their power properly, then they have the right to resist.

### 10. Final summary 

The key message in this book:

**Political power originates from the will of the people to protect themselves and their property, and is likewise limited by their will. However, governments that neglect their duties to defend their people and their property are illegitimate, and may be resisted with force.**

**Suggested further reading:** ** _The_** **_Prince_** **by** **Niccolò** **Machiavelli**

_The Prince_ is a 16th century guide on how to be an autocratic leader of a country. It explains why ends like glory and power always justify even brutal means for princes. Thanks to this book, the word "Machiavellian" came to mean using deceit and cunning to one's advantage.
---

### John Locke

John Locke (1632-1704) is one of the world's most important political philosophers. Considered the "father of classical liberalism," Locke was an influential thinker during the Enlightenment period, and his ideas on philosophy and civil government inspired other major personalities, such as Voltaire, as well as many American revolutionaries — ideas that are still alive and influential today.

