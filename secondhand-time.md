---
id: 592205fbb238e100077ef9f5
slug: secondhand-time-en
published_date: 2017-05-24T00:00:00.000+00:00
author: Svetlana Alexievich
title: Secondhand Time
subtitle: The Last of the Soviets
main_color: 345976
text_color: 345976
---

# Secondhand Time

_The Last of the Soviets_

**Svetlana Alexievich**

These blinks give some revolutionary insights into an epoch-making time in Russia's history. The author presents an oral history of Russia's transition from Stalinism to capitalism in which she lets people who were there tell their stories. In _Secondhand Time_ (2016), her witnesses tell us what it means to be Russian, then and now. All of them lived through this transitional period, but some did not survive.

---
### 1. What’s in it for me? Understand what it means to lose your country and beliefs. 

Imagine if everything you believed was true was thrown into question and the world as you knew it turned upside down overnight. How would you feel? How would you remember that old world? How would you adapt to the new one?

This is exactly what happened to the people of the Soviet Union. They were born into a society governed by a Communist regime that upheld state ideology as though it were religious doctrine and suppressed other views. In a short period in the early 1990s, that entire structure vanished.

This should have been a relief, right?

Here we'll get to know the feelings of the people who lived through the transition from the Soviet Union to Russia and the other states that formed when the Soviet era died. From interviews conducted between 1991 and 2012, we glean fascinating insights into ordinary people's views and lives. These are the voices usually left out of the history books.

In these blinks, you'll find out

  * why victims of the Soviet gulags continued to support Communism;

  * why people chose suicide at the fall of the Soviet Union; and

  * how one man continued to drink vodka with the colleague who had made his life hell.

### 2. Perestroika brought monumental changes to Soviet Russia. 

On August 19, 1991 something extraordinary occurred in Moscow. A group known as the General Committee on the State Emergency (GKChP) staged a coup while the president of the Soviet Union, Mikhail Gorbachev, was on vacation in Crimea. The coup, however, failed. In response, thousands of Muscovites poured onto the streets to protest, setting up barricades to halt the army tanks commanded by the GKChP.

Soon enough the soldiers sided with the protesters, and the GKChP backed down. Gorbachev had them arrested, including his own vice president. Just days later, on August 24, Gorbachev dissolved the Central Committee of the Communist Party of the Soviet Union. This, in effect, dismantled the Communist Party itself. The Soviet era was brought to a close.

But it didn't all happen in a flash. The dissolution effectively represented the culmination of what was known as _perestroika_, a period of unprecedented reform in Russia.

Let's step back a bit. Perestroika had begun around 1985. Gorbachev had triggered reforms, releasing political prisoners as well as loosening restrictions on freedom of the press and speech.

Just imagine: in a vibrant city like Moscow, it must have felt like the clouds were lifting. Banned books were finally published. It seemed like the dawn of a new humane socialism. In this spirit, the atrocities of the Soviet prison camps were also revealed.

But Russia is big, and it certainly wasn't just made up of Muscovite city slickers. Most people lived in villages and held their Communist values close to heart.

But neither side of the divide, no matter their opinions on perestroika, could have predicted what happened after the coup and the dissolution of the Communist Party.

Instead of evolving into a humane socialism, the country veered headfirst and unprepared into a quite different world: capitalism.

### 3. Even today the effects of perestroika divide Russians. 

Every Russian will give you his take on Communism if you ask. Someone born under Stalin will likely respond differently from someone born after perestroika. And there's a geographical divide too. Country folk will have different views to city dwellers.

The author set about gathering stories because she wanted personal perspectives that would help her understand how people felt about Russia.

She found former members of the Communist Party were deeply upset.

Consider Elena Yurievna, a forty-nine-year-old secretary. She sees both sides of Communism. Of course, there were negatives, like informants and labor camps, for example. But she also valued the compassion and camaraderie the system nourished.

She is deeply upset with Boris Yeltsin. He was elected president of the new Russian Soviet Federative Socialist Republic in 1990. She thinks he misled Russia after Gorbachev's resignation, giving the false impression that a "fixed" socialism was the likely path.

What actually happened? Well, Yeltsin divvied the country up among a privileged elite, ushering in a new era of capitalism. It's an alien system to Elena.

Like others, Elena is proud of some Soviet accomplishments. After all, the Russians defeated the Nazis at Stalingrad, built the Dnieper Hydroelectric Station and, of course, put the first man into space. But it's a different world for her now, one dominated by the cult of money.

She sees businessmen flashing their diamond-studded phones or boasting about golden toilets. She's upset people value them instead of writers, poets or actors.

Conversely, there are people like Elena's friend Anna. She happily admits that things didn't turn out exactly as hoped after perestroika. But she still respects what Gorbachev was trying to achieve. She remembers the time before the coup in 1991 fondly. People, she says, were positive about the future and what it could bring. It might have been naive, but there is no shame in that!

### 4. Suicide was not unheard of as Communism collapsed. 

Can you imagine what it was like to be raised under Communism? Unlike capitalism, the state ideology was almost religious in quality. Its citizens felt they had meaning and purpose. They were happy to tolerate awful conditions because they believed they were creating a utopia based on equality.

People were so deeply schooled in this mind-set that they died for its aims. In fact, when they felt their system collapsing it was too much for many to bear and many people died by suicide.

Take Alexander Porfirievich Sharpilo. He was sixty-three years old when he set himself on fire in his garden. For 30 years he worked at a furniture factory. He'd saved up five thousand rubles. In the old days, this would have been enough for a Volga, the snazziest car on the market. But with the advent of capitalism, you couldn't even get a pair of boots for that! What's more, his pension was devalued as the economy changed.

What was the point? Now all manual work seemed pointless. He survived for a year on the odd jobs he could get from his neighbors, but after a while, it was too much. Why even live in a world of false promises and lies?

Or consider the case of Sergey Fyodorovich Akhromeyev. He was one of the most decorated soldiers in the Soviet Union. He had fought for Communism and the motherland. He'd moved in the higher echelons of power with a role in government. He'd even been one of the government officials who'd supported the 1991 coup.

He was dismayed when Gorbachev dissolved the Communist Party, and he was also terrified that Communism would collapse in East Germany and Eastern Europe.

By December 1991 he'd had enough. He hanged himself in his office. He wrote in one of his three suicide notes, "I cannot go on living while the fatherland is dying."

### 5. People were even willing to endure imprisonment and torture for Communism. 

Among other things, perestroika revealed the prevalence of torture and the inhumane conditions in the prisons and infamous work camps known as the _gulags_.

Unbelievably, people returned from incarceration willing to defend Communism even more.

Let's consider Elena Vurievna's father. He was a veteran of the Russo-Finnish War which lasted between 1939 and 1940. However, he was considered a traitor because he "allowed" himself to be rescued from a frozen lake and become a prisoner of war rather than dying for Mother Russia.

Consequently, he was forced by the Russians to build the railway in Vorkuta. When he returned home, he was unrecognizably thin. Nevertheless, he bore no ill will. He even kept a portrait of Stalin in the living room. What's more, Elena became a Pioneer, a member of the youth moment of the Communist Party.

Or there's Vasily Petrovich, an eighty-seven-year-old. He's proud of the huge steps forward that occurred under Stalin. After all, Russia went from being a backward and antiquated country to a world-beating superpower in just a few decades.

Vasily's aware that Stalin used the gulags to achieve this. He also knows that it was the strengthening of the military that made Russia so powerful. After all, Stalin's militaristic version of Communism was what defeated the Nazi threat. The USSR was filled with people as devoted as Vasily. He'd been beaten, tortured and imprisoned after his 1940 arrest, but still wanted to fight for Mother Russia.

Vasily wasn't alone. People were devoted to the regime — even children who had been raised in orphanages when their parents were imprisoned.

Anna grew up in an orphanage like this. The staff beat children with impunity. Anna was happiest when her wounds became septic because this meant that the staff wouldn't touch her.

Nevertheless, when she left the orphanage, she immediately signed up for the _Komsomol_, the young Communists' organization.

### 6. Soviet Russia was a state based on informants and a code of violence. 

The prisons and gulags instituted by Stalin were central to the functioning of Soviet Russia. Your neighbor or co-worker could inform on you. Other Russians were employed to torture or murder their comrades. This climate of fear colored everything.

Let's return to Elena's friend, Anna. She had a son. To her pride and joy, he got a government job. Unbeknownst to her, he was an executioner. Now, under capitalism, he sells vodka, clothing and plumbing fixtures. And which of the jobs is Anna ashamed of? That's right. His tradesman hustling. She is still unaware that he had daily killing quotas and became deaf in his right ear from shooting so many people, or that he used to drown people by tying them up in stone-filled containers and dropping them in the ocean.

Then there's the executioner's uncle, Vanya. He was tortured at a camp. He was stripped and hung upside down. Officers poured alcohol into his orifices and demanded that he start informing on his comrades.

Vanya only did so when he saw another prisoner drowned in a bucket of feces. Eventually, he was released and returned to his old job. There, sitting across from him, was the man he knew had informed on him. But you know what? On holidays the pair would sit together and share vodka.

In the 1990s many of these atrocities were revealed. The evidence was damning. Some executioners killed themselves. Others, like Anna's son, were imprisoned even though the violence had been state sanctioned.

In total, he spent seven years in prison, even though the charges were never made clear. He believes another generation will put to use the same murderous tactics that were so abruptly abandoned in 1991.

Whether you perpetrated the violence or suffered from it, it's clear that everyone was in his own way a victim.

### 7. A series of bloody conflicts followed the end of the Soviet era. 

Olga thinks a great deal about death. She's like a lot of the former Soviet citizens the author interviewed. She witnessed brutal murders in her home region of Abkhazia when her neighbors started killing each other.

Before 1992, residents of Abkhazia just saw themselves as part of the Soviet Union. It made no difference whether you were Armenian or Georgian. But when Communism fell that all changed.

The Soviet Union was dissolved in December 1991. Within a month, Abkhazians set about killing Georgians and anyone they perceived to be a Georgian ally. Men, some of them just boys, broke into houses and fired off machine guns.

Olga, being Georgian, could bribe the police. She got a ticket to Moscow where she made her home in a train station along with thousands of others. After two weeks desperately spent trying to avoid rape at the hands of the police and the military, she felt confident enough to contact her aunt and implore her to flee as well.

Let's turn to Margarita. She's a forty-one-year-old Armenian. She had to escape the seaside town of Baku when civil war broke out between Armenians and Azerbaijanis.

Margarita was in a tricky situation. Just before the violence erupted, she had married Abulfaz, an Azerbaijani. Before 1992, no one would have cared, but when Armenian homes were being raided, and people were raped and killed, both families were against the match.

Margarita was forced to hide in an attic when she fell pregnant. After she had given birth in a crumbling hospital, she had to flee with her new baby to Moscow. It just wasn't safe.

Eventually, Abulfaz was able to join his wife and child. But it took seven years. Why? Because his Azerbaijani family stole his passport and tried everything to stop him reuniting with his Armenian wife.

### 8. If one thing hasn’t changed since Soviet times, it’s the strength of the militaristic authorities. 

In some parts of Russia, capitalism changed everything. However, elsewhere, some village squares still have their Lenin statues. In fact, many parts of Russia have not been touched by capitalism.

Another thing that hasn't changed is the role of the military. It is a constant presence, and Russia remains a military nation.

We shouldn't be fooled by the notion that Gorbachev was special. The president and dismantler of the Soviet Union may well have been a civilian, but Russia has a deeply militaristic culture, and its influence runs deep.

Let's look at Aleksander Laskovich. He came from a military family, but he still found the military environment dehumanizing.

He remembers one man killed himself on the way to military training. Aleksander considered suicide multiple times. For him, the training seemed designed to turn soldiers into violent savages. Recruits were beaten and held down. Then they were "shaved" with lighters or dry towels. Other times they were given stray animals to kill to acclimate them to blood.

And for what purpose? To fight former Soviet states filled with people just like themselves. The war in Chechnya is the prime example. The Chechens can't make head nor tail of the war. The Russian soldiers kill, mutilate and raze buildings. But it's the same soldiers who tend to the wounds of the civilians, rebuild their homes and tell them that Russia is there for them.

Even outside conflict zones, people are at the whim of the authorities. Chechens are pretty distinctive in Moscow. They have dark hair and eyes. They have to bribe the police so they are not beaten up, killed or robbed.

Even today people are arrested without due process. Protestors are told to inform on their friends. And the police are still using the same Stalinist interrogation techniques.

In some respects, nothing has changed.

### 9. Final summary 

The key message in this book:

**Russian history is complex. Many people in Russia in the 1990s suddenly found themselves thrust into a new capitalistic world. Those who had devoted their lives to Communism felt abandoned. Meanwhile, those who'd hoped for a new kind of humane socialism felt betrayed. A dark past came to light. Violence was no longer confined to prisons: there were civil wars and brutal mobsters. But for some, still living in tiny and remote Russian villages, nothing seems to have changed at all.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Red Notice_** **by Bill Browder**

_Red Notice_ (2015) is a gripping, true story of one man's experience with fraud, corruption and violence in post-Soviet Russia. After discovering rampant fraud in Russia's investment market, the author found himself in a nightmare: first, he was declared a national security threat, then he feared for his life.
---

### Svetlana Alexievich

Svetlana Alexievich is a Nobel Prize-winning author. She worked as a journalist before she started compiling her unique and exciting oral histories. She gives voice to people who lived through tumultuous times. Her other works include _Voices from Chernobyl_ and _The Last Witnesses_.

