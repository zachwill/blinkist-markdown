---
id: 5416bf593861300008410000
slug: secrets-of-dynamic-communication-en
published_date: 2014-09-14T00:00:00.000+00:00
author: Ken Davis
title: Secrets of Dynamic Communication
subtitle: Prepare with focus, deliver with clarity, speak with power
main_color: FDE633
text_color: 7D7119
---

# Secrets of Dynamic Communication

_Prepare with focus, deliver with clarity, speak with power_

**Ken Davis**

_Secrets of Dynamic Communication_ explains how to prepare and present a speech effectively. Through the author's own SCORRE-method, we are guided through the six key components of a successful speech: subject, central theme, objective, rationale, resources and evaluation.

---
### 1. What’s in it for me? Learn the recipe for moving, powerful speeches. 

Fear of public speaking is common; some even fear it more than death. What's more, even experienced speakers sometimes have trouble formulating their thoughts.

These blinks explore two problems relating to public speaking: how to prepare a speech effectively, and how to present it in a way that really moves the audience. At the core of successful speeches is a laser-like focus, and after reading these blinks you'll master the author's original and helpful SCORRE-method to define this focus.

In these blinks you'll discover

  * why after a presentation, 70 percent of people don't know what it was about;

  * how a touching story might ruin your performance if you're not ready to improvise; and

  * why it's always important to check that your fly is done up!

### 2. The secret to a powerful speech is focus. 

Have you ever heard a speech that gave you goosebumps? Did you wonder how the speaker managed to affect you so deeply? The secret to a powerful speech is simple: _focus_.

Though it can be tempting to cram a speech with facts and statistics in order to convince your audience, that tactic will surely reduce the audience's attention span.

This finding is reflected in a study conducted by the author. Of the 2,000 people surveyed, 70 percent were unable to recall the key messages of presentations they had just viewed. Even more surprisingly, 50 percent of the speakers were themselves unable to explain the purpose and motivation behind their own speeches.

In this way, it is clear that a speech that lacks focus will also lack impact. Instead, an effective presentation will first adhere to a specific goal, which is only then supplemented with information.

But how do we choose this goal? Making such a decision seems tough, but often we are able to do so without even thinking. Having objectives is part of human nature. It drives our instinctive ability to acquire food, shelter and ultimately survive.

Similarly, in writing a speech, our subconscious directs us to our objectives, even if we are not consciously aware of them. The trick is to become aware of this innate process. By taking a step back and reflecting on what it is we really want to communicate, we can harness the power of objectives and create a speech that leads directly to them.

In order to produce a presentation that has focus, our approach to writing it should also reflect focus. Davis's _SCORRE-method_ will allow us to create successful speeches by breaking it down into: subject, central theme, objective, rationale, resources and evaluation.

> _"If you aim at nothing, you will hit nothing every time."_

### 3. The SCORRE-method: To narrow the focus for your speech, choose a subject and a central theme. 

Sitting down to write a speech can be incredibly overwhelming. How, for example, can we possibly choose where to begin talking about the ever-changing discipline of science or the universally discussed theme of love?

This is where the first part of the SCORRE-method comes in, by helping us narrow our passion into a _subject_ that gives our speech focus.

It is astonishing how our grasp on a presentation improves once we've selected a subject that is narrow and specific. Otherwise, we run the risk of including everything we know about a topic, even getting lost on wild tangents unrelated to the topic itself.

In writing the first draft of this book, for example, the author initially included too many irrelevant anecdotes. However, he had a clear objective in mind — educating others about public speaking. By using this narrowed subject, the author could discern which information was valuable in relation to his specific subject.

Our subject should not only be specific, it should also be central to our speech. What does this mean? A central idea that runs throughout the presentation is one to which every point we make relates back. In this way, our presentation will take the shape of a complete story, rather than some scattered remarks across a handful of different aspects.

Say you wanted to create a presentation all about scuba diving. It doesn't seem too daunting a task. But a speech that covered the complete history of the underwater hobby would take several hours to present and drown your audience in information. Instead, simply selecting 'how to learn scuba diving' as your central theme would allow you to cover all vital aspects of the chosen subject within an appropriate duration.

With a subject and central theme in mind, you're well on the way to creating an effective presentation. But, that was just the first warm up for your next exercise: _crafting_ _an_ _objective_ _sentence_ _for_ _your_ _speech._

### 4. The SCORRE-method: To determine the purpose and structure of your speech, you need an objective sentence. 

You'd hardly think that a 10 to 15 minute speech could be boiled down to a one-liner. Think again! The _objective_ _sentence_ is a tool that does just that, and though it's just a single sentence, it should also reflect careful choices in its construction. Why? It's the essence of your speech, a vehicle for your overarching message and at the heart of the SCORRE-method.

The ingredients are threefold: a _proposition_, an _interrogative_ _question_ _and_ _answer_, and a _key_ _word_.

A proposition is simply what you aim to put out there with your speech. It is a bold statement for the audience to consider, such as "all couples can have a lasting marriage."

Next, add an interrogative question and answer that demonstrate how the proposition can be implemented. In this case, "how can couples have a lasting marriage?" forms the interrogative question, while "by avoiding temptations" is a possible answer.

Finally, a key word is required. In this example, we find it in the interrogative answer — _temptations_. This key word encompasses all the different forms of temptations that should and can be avoided, as you will demonstrate in your speech. The objective sentence of your speech emerges naturally as "all couples can have a lasting marriage by avoiding temptations."

Although this sentence does not reflect the snappiest turn of phrase, it is still incredibly valuable. With our objective sentence, the purpose of our speech is made clear. And in turn, the structure of our speech is also revealed.

For example, in writing your speech on marriage, you may be tempted to comment on the tension that arises between couples at the prospect of having a baby. But, in light of your objective sentence, you will find that this is a tangent not related to your key word, temptations.

So long as we actively return to this objective sentence while writing our speech, our focus will be retained, and so too will the power of our presentation.

> _"The [objective] sentence is rarely pretty, but neither is the foundation of your house."_

### 5. The SCORRE-method: Lay a solid foundation for your speech by using rationale, resources and evaluation 

After successfully working through the first three letters of the SCORRE-method, we come to the two R's and a final E - _rationale,_ _resources_ and _evaluation_.

If our speech were a house, then what we've achieved so far is perhaps its architectural plan. The design created through our subject, central theme and objective sentence requires a real and solid foundation to build on.

This is our _rationale_, a series of points that outline the argument of your speech so as to make it credible to the audience. These points should link not only to the key word but to each other, creating a logical flow.

Using our previous example of marriage, our rationale could be: avoiding the temptations of being (1) unfaithful, (2) selfish, and (3) jealous. These three points support the speech's argument and correspond with each other, so that understanding them is easy.

However, an argument that makes sense is not enough to create a successful presentation. We need something else to bring the speech to life. The something else are our _resources_, anecdotes and even jokes, that illustrate our argument in a way that is captivating and lively.

A humorous tale about the near break-up of a marriage through a trivial misunderstanding, or a contrasting tragedy of a couple that disintegrated in the face of temptation would not only demonstrate the truth to our speech's argument, but also reveal a human side of it that the audience cannot help but engage with. Creating that human interest is something that dry divorce statistics alone cannot do.

Finally, there's one more step in the SCORRE-method: _evaluation_. It is not enough to develop our rationale, supplement it with resources and leave it at that. Rather, we must evaluate and re-evaluate the choices we make when constructing the speech. In this way, we can correct a rationale that has gone awry or resources that don't quite fit, so that our focus remains strong as ever.

### 6. Finalize the speech by writing a captivating opening and an impressionable conclusion. 

The SCORRE-method allows us to achieve the bulk of the hard work involved in creating a speech. However, by performing some further steps, we will be rewarded with a goosebump-inducing speech that sticks with the audience for days after. The _opening_ and _conclusion_ are vital tools in this way.

The first moments of your speech are arguably the most important, but don't let this alarm you. All that is required of you is to take this chance to grab the audience's attention in any original way, before briefly introducing the topic.

After this, you'll have already won the battle, with the audience's attention starting strong and remaining strong throughout your speech. Think back to the 70 percent of forgetful audience members from the author's study — perhaps a more striking introduction would have made all the difference.

As your speech winds up, you're presented with another opportunity to leave a lasting impression. Contrary to what we've often been taught, a conclusion is not merely a summary. Yes, it should reiterate your points, but in a final persuasive act that leaves your audience with your message, and leaves them on your side.

How exactly to perform this bold move? A powerful conclusion is often best achieved by highlighting the real-world relevance of your argument, especially in the lives of your own audience. Encourage them to think back to your speech when faced with their own issues, perhaps in dealing with their own marriage and temptations, in line with our previous example.

In this way, your speech will become not just something your audience listened to once, but something they return to again and again, as they take on and apply the objective you set out to communicate.

### 7. Dedicate time to your presentation and reap the rewards. 

We all have limited time in our busy lifestyles, and you may be feeling a little reluctant to set aside substantial amounts to prepare your speech.

Certainly, the SCORRE processes of perfecting the objective sentence and interweaving an evaluated rationale with resources are time-consuming ones. But if you don't give up time to create your presentation, why should anyone else want to give up his or her time to listen to it?

The SCORRE-method is highly effective, but only when the time is taken to apply it properly. Use your time to prepare the speech, as well as presenting the speech.

The structured approach of a SCORRE speech will aid you in dividing each part into easily digestible points and designate appropriate speaking time for these. Naturally, practice runs of your presentation will prove invaluable in ensuring you have an accurate grasp of its duration.

What's more, simply leaving the speech aside for a week or so can work wonders. By returning to your work with a fresh outlook, you'll be surprised at how easily mistakes will jump out at you, and how quick it'll be to fix them. You may even develop some new ideas in your time away from your speech that provide great support to your central theme. Put it aside for a week if you can. Time is a luxury, and treating yourself to a week of revision is something you and your audience will surely appreciate.

Another thing your audience will appreciate is if you present your SCORRE speech with flair. We've all been members of a bored audience in the past, which makes the prospect of engaging one a little alarming.

### 8. Know your audience and use all your tools to engage them – personal anecdotes, body language and the presentation space. 

When giving a speech, think of your message as the product you'd like to sell. The audience is your customer — how are you going to engage them?

A speech isn't all about performing. What's more important is an awareness of what you're trying to communicate and whom you're trying to communicate to.

The concerns of a group of teenagers and a group of retirees are different, so why should you provide them with the same performance? Likewise, the mood of an audience can vary. The author had to make a last-minute change to a humorous introduction, rendered inappropriate as he found himself about to present after the previous speaker shared an emotional and personal story.

Both cases reflect that audiences are diverse and it helps to be flexible and sensitive to their interests. Speaking to _your_ specific audience is integral to effective audience engagement.

An awareness of our own physical presence is also central to engaging the audience. While we often don't pay attention to our voice, expressions and gestures on an every-day basis, on the stage these become powerful tools that can make a speech by bringing life to your words, or break a speech by providing visual distractions. For example, the author recalls several presentations in which he unwittingly distracted the audience by forgetting to close his fly.

The room in which you present can also strengthen or weaken your presentation. Attention to the lighting, sound and arrangement of the room is vital. Use each element to bring the focus towards you, and not to the background. For example, the author recounts the ineffectiveness of one speaker in capturing the audience's attention when competing with the stunning snowy landscape that stood behind him in Alaska.

By closely considering every element that will shape your presentation experience, you can ensure that none of the hard work put into your speech will go to waste.

### 9. Make use of every tool to touch the emotions of your audience, including humor. 

Now that we've considered the importance of the physical landscape that we present in, let's turn our attention to the emotional landscape of our speech.

We can't be sure that our audience will relate to every proposition in our speech. However, by engaging our audience on an emotional level, we can ensure that each audience member will connect with our presentation, regardless of his or her background or prior experiences.

This emotional engagement should stem from the fact that we ourselves are emotionally involved in the speech. This way our natural emotive language appeals to the audience's feelings ranging from sadness to joy. Allowing the audience to empathize with your ideas will ensure that they are effective and worth remembering.

What makes a speech perhaps even more memorable is humor. Its use is particularly effective at opening an audience up to new propositions. In order to demonstrate the effectiveness of a humorous approach, the author refers to a businessman who wrote to him after viewing one of his speeches. The audience member wrote that he was initially skeptical but that the speaker's light-hearted and funny presentation led the businessman to accept the messages of Christianity for the first time.

Humor can be used to turn audience expectations around, and it can be used even if you don't consider yourself a particularly witty person. Even the truth can be funny in the way that it startles us, and elements of surprise or exaggeration in its delivery are simple to implement and highly effective.

And with that, you've been equipped with all the vital tools for creating a speech, from coming up with a subject, to developing your focus and to communicating in an engaging manner. By making use of all of the author's steps, your focused speeches will be the kind that give goosebumps to the audience, and perhaps to you too.

> _"Humor is just plain good for you."_

### 10. Final summary 

The key message in this book:

**The** **secret** **to** **preparing** **an** **effective** **speech** **is** **to** **develop** **the** **focus.** **By** **implementing** **the** **SCORRE-method** **and** **engaging** **the** **audience** **through** **the** **physical** **surroundings** **and** **emotional** **landscape** **of** **your** **presentation,** **you** **can** **ensure** **your** **success** **as** **a** **moving** **and** **memorable** **public** **speaker.**

Actionable advice:

**Refresh** **and** **reflect!**

The evaluation process is vital for a successful speech, so make time to reflect on your work if you are encountering difficulties. Simply setting your speech aside for a week or so will provide you with a fresh perspective that may prove invaluable in creating a finished product that you are satisfied with.

**Suggested** **further** **reading:** **_Speaker,_** **_Leader,_** **_Champion_** **by** **Jeremy** **Donovan** **and** **Ryan** **Avery**

_Speaker,_ _Leader,_ _Champion_ examines top public speakers' most successful speeches to see what makes them great. It offers detailed tips for improving public speaking skills for everyone, whether you're a beginner or have years of experience.
---

### Ken Davis

Ken Davis is an American author and speaker. In addition, he is an educator of communication skills and radio show host.

