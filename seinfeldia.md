---
id: 584c4e8baf38d6000494d503
slug: seinfeldia-en
published_date: 2016-12-13T00:00:00.000+00:00
author: Jennifer Keishin Armstrong
title: Seinfeldia
subtitle: How a Show About Nothing Changed Everything
main_color: 3A7FBC
text_color: 326EA3
---

# Seinfeldia

_How a Show About Nothing Changed Everything_

**Jennifer Keishin Armstrong**

_Seinfeldia_ (2016) is an in-depth look behind the scenes of the hit sitcom that changed TV forever. These blinks explain _Seinfeld_ 's creation, history, fan base and the lasting impact it has had on TV history as well as millions of fans.

---
### 1. What’s in it for me? Dive into the world of Seinfeldia! 

In 1988, Jerry Seinfeld asked his friend Larry David if he wanted to join him in creating a sitcom. At the time, neither could have ever imagined the success that their collaboration would result in: _Seinfeld_ would go on to become known as one of the best — if not _the_ best - TV shows of all time.

But how did _Seinfeld_ become so successful? After all, the show simply told stories about mundane aspects of daily life, often based on David's and Seinfeld's real-life experiences. These blinks help explain how this simple formula propelled _Seinfeld_ to TV fame, changing the format of sitcoms forever.

In these blinks, you'll learn

  * how a "show about nothing" changed the face of American television;

  * why the internet contributed to _Seinfeld_ 's success; and

  * where you can go on a tour of real-life locations that were used in the show.

### 2. Seinfeld blurred the lines of reality and fiction from the very beginning. 

Have you ever wondered how the hit series _Seinfeld_ got its start?

Well, it began in 1988, when NBC approached the comedian Jerry Seinfeld with an offer to make a TV show. He immediately went to his friend Larry David, seeking a creative partnership; David signed on and they started working out a show based largely on their own real-life experiences.

During this initial process, the pair was joking around in a Korean supermarket when David suggested that what they were doing at that moment, making mundane but hilarious jokes and observations, should be the show's premise. This decision would prove revelatory.

After all, the fact that the show was so plain about reality gave it a unique feeling. Jerry would play a stand-up comedian named Jerry Seinfeld, but he wouldn't exactly be playing himself. Meanwhile, the character George would in many ways be similar to Larry David.

As a result, the show is full of real-world stories, and one such famous episode depicts Jerry and George pitching an idea for "a show about nothing" to TV executives. Their fictional show is basically the exact same idea as that of _Seinfeld_ — it's even called "Jerry" — and the audience is led to imagine that this enactment is similar to the real-life pitch made by David and Seinfeld.

Such comparisons to reality gave birth to a parallel universe known as _Seinfeldia_, a world only slightly different than our own that blended fiction and reality. The plotlines of the show were almost exclusively pulled from the real-world experiences of David, Seinfeld and, eventually, the rest of the writing staff.

Since the show was so focused on New York City, these writers were largely New Yorkers themselves, and could generate content that was true to the city. For instance, the Soup Nazi, a character in the show who rapidly rose to infamy for his catchphrase, "no soup for you!", was based on an actual New York soup chef that New Yorkers would immediately recognize, deepening the show's mix of fiction and reality.

This unusual meta aspect of the show was due in large part to its style and writing, but the ongoing existence of Seinfeldia owes a lot to the fans who so lovingly nurture it, which you'll learn all about in the following blinks.

### 3. Seinfeld’s major success created a demand for tourist-style attractions that, like the show, walk the line between reality and fiction. 

By the time it reached its sixth season, _Seinfeld_ had become the single most popular sitcom on American television — and it had the fans to prove it. Some of them were so obsessed with the show that they wanted to inhabit Seinfeldia, the show's parallel universe, themselves.

In fact, to this day, thousands of people gather at regular events across the country to hang out in Seinfeldia together. For instance, Larry Thomas, the actor who played the infamous Soup Nazi, makes a living by traveling the country, shouting "no soup for you!" at people who pay him for this privilege.

But for these die-hard fans, the idea isn't to pretend that they're characters in the show, nor do they think about the yelling Soup Nazi as an actor. Rather, for them, the Soup Nazi is real and they want to be yelled at in real life, so that they feel as much a part of Seinfeld's world as possible.

The high demand for _Seinfeld_ -related experiences has even led to guided tours of Seinfeldia. One such experience is run by Kenny Kramer, the real-life comedian that the _Seinfeld_ character Cosmo Kramer was based on.

But how did his name and likeness make their way into the show?

Well, Larry David used to live opposite the eccentric Kramer, just like Jerry does in the show. David drew heavily on his actual experiences with Kramer when writing the show, and even went as far as using his last name.

In fact, the tours that the real-world Kramer hosts actually end with a visit to his apartment. This builds on the surreal and hyperreal experience of the tourists who have spent the day going from one real location to the next, all of which appear on the show in a fictitious context, like Tom's Restaurant, the facade of which represents the main characters' main hangout.

### 4. Seinfeld and the internet were a match made in heaven. 

It was a happy coincidence that _Seinfeld_ 's rise to popularity coincided with an increase in home web-browsing; the online community welcomed _Seinfeld_ with open arms right from the start.

Even in the internet's early days, there were already wildly popular chat rooms devoted to _Seinfeld_. These chat rooms served as a space for fans to amass _Seinfeld_ trivia, an activity that remains popular among the show's followers to this day. In fact, in 1995, _Seinfeld_ had so many websites devoted to it that Yahoo! Internet Life magazine made the internet's obsession with the show its cover story.

At the same time, the growth of America Online, the first major home-use internet provider, was making TV executives nervous as it invaded American households like televisions had once before. However, much to the delight of NBC executives, the internet was so in love with _Seinfeld_ that the average number of AOL users online actually dropped while the show was on.

So, _Seinfeld_ grew up alongside the internet and Seinfeldia has continued to grow with the internet's explosion in popularity. While reruns have been an essential tool for keeping _Seinfeld_ fresh in the hearts and minds of its millions of fans and engaging a new generation of viewers, the internet has made _Seinfeld_ 's fame more widespread than ever.

Because of video streaming sites, more people have access to the show than ever before, and fans are continuing to gather online to test their trivia knowledge and expand Seinfeldia's boundaries into the digital world. But, not all of Seinfeldia's online presence has been met with positive reviews.

For instance, a Twitter account named SeinfeldToday inspired rage in many by blithely imagining plotlines the show's characters would live through if they existed today. People, including David and Seinfeld, found the gesture insulting as it suggested that anyone could write a _Seinfeld_ plot — which obviously isn't the case.

### 5. Seinfeld changed the face of television and inspired similar shows. 

Before _Seinfeld_, TV sitcoms had fallen into a tried and tired formula based on happy endings, dramatic romances and soul-searching characters. But that all changed when David imposed a strict "no hugging, no learning" rule on the _Seinfeld_ writing staff.

This decision was a major boon to the "show about nothing" and helped it alter sitcom history. But the impact _Seinfeld_ had on the sitcom medium was also a product of luck and compromise.

The NBC executives who picked up the show gave Seinfeld and David tremendous creative freedom for two reasons: first because they simply believed in their talent, and second because they intended to air the show late at night, when few people were likely to watch it.

However, while the executives were fairly relaxed in terms of rules, NBC did have _some_ conditions for airing the show, a fact that inevitably led to compromise. For example, David had originally not wanted a laugh track and had intended to shoot _Seinfeld_ in a more cinematic style than was normal for sitcoms. Ironically enough, the laugh track element of the show, which David eventually accepted, would become a mainstay of other shows inspired by _Seinfeld_.

And the show certainly did have a number of (often very successful) copycats. Shows like _Friends_ and _Will and Grace_ were especially reminiscent of _Seinfeld_, as both took place in New York, dealt with the mundane aspects of daily life and used the show's big compromise: the laugh track.

_Seinfeld_ even led to another show produced by Larry David called _Curb Your Enthusiasm_, which is a kind of sequel to _Seinfeld_ even though the two shows aren't officially related in any way. _Curb Your Enthusiasm_ is itself a major enabler of Seinfeldia's continued existence, as it adds another layer of reality-meets-fiction to the world. In the show, David plays a post- _Seinfeld_ version of himself and _Curb_ even includes members of the _Seinfeld_ cast as guest stars.

### 6. Seinfeld continues to be one of the most successful TV shows of all time in spite of various setbacks. 

_Seinfeld_ experienced tremendous success, but it also saw its fair share of problems, all of which it overcame in stride. For instance, in 2006, an incident at a Los Angeles comedy club involving Michael Richards, the actor who played Kramer on the show, resulted in some very bad press.

After being heckled during his routine, Richards exploded in a rage, hurling a vile racial slur at his hecklers. While _Seinfeld_ generally had a fantastic relationship with the internet, the internet also played a huge role in spreading word of what would become Richards's infamous outburst. In fact, it was one of the first viral videos filmed on a camera phone, and it effectively ended Richards's career.

Another potential issue was that many of those involved with the show early on thought it was "too Jewish." However, as the show rose in popularity, it became clear that viewers in parts of the country without large Jewish communities didn't feel that way, and the characters' cultural background didn't detract from their opinion of the show.

In fact, _Seinfeld's_ wide appeal led, in 2002, to _TV Guide_ naming it the best show of all time. Despite the fact that its first season wasn't a great success, a fact that can sometimes doom shows to a short run and eventual "cult" status, _Seinfeld_ quickly became wildly successful. Not only did it have an astonishing nine-season run, but it was also a huge commercial success, with advertisers dishing out $2 million for slots during the show's finale.

To put that into perspective, ad time during that year's Super Bowl, conventionally considered the most highly coveted and highly priced advertising event on TV, went for $1.3 million. Even in 2014, 16 years after the show's finale, an ad featuring Jerry and George aired during the Super Bowl, becoming the most popular ad of the year and racking up millions of YouTube views.

In this way, as well as many others, _Seinfeld_ — and with it, Seinfeldia — lives on, captivating and inspiring past and future generations.

### 7. Final summary 

The key message in this book:

**_Seinfeld_** **altered the course of TV history, won the hearts of millions of viewers and even gave birth to an alternate reality known as Seinfeldia. This world of half-fiction, half-truth is a product of the genius of Larry David, Jerry Seinfeld and a few million die-hard fans.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _A Curious Mind_** **by Brian Grazer and Charles Fishman**

_A Curious Mind_ (2015) investigates a vital attribute that many of us simply don't value highly enough: curiosity. These blinks explain the vital importance of curiosity, and outline the ways it can improve your relationships with your employees, customers or loved ones — and even help you conquer your fears.
---

### Jennifer Keishin Armstrong

Jennifer Keishin Armstrong is the TV Columnist for BBC Culture as well as a writer for the _New York Times Book Review_ and other publications. Her additional titles include a book about the _Mary Tyler Moore Show_ called _Mary and Lou and Rhoda and Ted_.

