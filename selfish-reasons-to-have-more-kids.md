---
id: 53cf890f3630650007470100
slug: selfish-reasons-to-have-more-kids-en
published_date: 2014-07-22T00:00:00.000+00:00
author: Bryan Caplan
title: Selfish Reasons to Have More Kids
subtitle: Why Being a Great Parent Is Less Work and More Fun Than You Think
main_color: A0ECF5
text_color: 4D7175
---

# Selfish Reasons to Have More Kids

_Why Being a Great Parent Is Less Work and More Fun Than You Think_

**Bryan Caplan**

_Selfish_ _Reasons_ _to_ _Have_ _More_ _Kids_ examines the demands of modern parenting and why people today are choosing to have fewer and fewer kids. The author argues that this trend is due to modern parents placing too high expectations on themselves, even when a far more relaxed style of parenting would get the job done just as well and make the whole experience more enjoyable.

---
### 1. What’s in it for me? Find out how many kids you should have, and why relaxed parenting is no worse than the high-strung kind. 

When the author told people that he had begun writing a book about selfish reasons to have more children, the most common reaction was, "Ah, because they'll provide for you when you're old, right?"

Actually, starting a family is not really a good investment if your goal is financial security in old age. Sure, your child may grow up to be the next Richard Branson but, on average, having kids does not pay off in this sense. In fact, a better analogy for having children is buying a high-definition television (HDTV).

No one buys a HDTV with the assumption that it will provide for them when they're old: they buy it because of all its cool features!

And kids have a ton of cool features that make spending time with them a very fun and rewarding experience.

Then the question becomes: Is it worth the cost of the investment? The sleepless nights and tuition bills?

In these blinks, you'll discover:

  * why it's ok to let your children play video games rather than force them to take violin lessons,

  * why it's actually pretty difficult to be a bad parent because ultimately what you do won't have much influence on your kids' academic success or adult sexual behavior, and

  * why kids today are much, much safer than they were in the supposedly idyllic 1950s.

### 2. If you forego children, you’ll likely regret it; but having children doesn’t automatically make you happier. 

Many people decide to start families and have children, but few pause to analyze the data and statistics regarding such a move. So is having children actually a good idea?

As is to be expected, the answer is not straightforward, so let's approach it from a few different angles.

First, we can consider it from a "customer satisfaction" perspective: if a business wants to know how satisfied customers were with a purchase, it can ask them whether they'd buy the same product again. Similarly, parents can say whether they'd have their children again.

It turns out that parenthood does well in this metric. A study regarding this matter showed there is very little _buyer's_ _remorse_ on parents' behalf: 91 percent did not regret the decision to have children, claiming they'd do it all over again with each child.

On the other hand, _non-buyer's_ _remorse_ seems pretty prevalent. Another survey showed that, among childless parents over the age of forty, more than two-thirds confessed they had regrets over not having had children.

In a nutshell, an overwhelming majority of parents seem to be happy with the kids they've got, while childless people often regret the decision later on.

A second way to analyze parenthood is by looking at overall happiness in life. The following data may seem surprising.

At first glance, parents tend to be happier than non-parents on average. But that's because parents also tend to be older, married and church-going, and these factors in themselves are positively correlated with happiness. If these factors are compensated for otherwise, the tables turn: there's a slightly negative correlation between having children and happiness.

However, the negative effect is very small and, after the first child, each subsequent child only adds slightly to that effect. This indicates that it's entirely feasible to reverse it by adjusting your parenting approach and improving your own life.

### 3. Modern parenting has turned kids into a heavy burden – go easier on yourself! 

These days, a common reason for parents not wanting more children is their belief that "the pain outweighs the gain," when in fact it's the modern approach to parenting that has turned kids into such a burden for parents.

One study showed that parents today spend significantly more time on child care, i.e., focusing just on their child, than parents in 1965: fathers have doubled their efforts from three to six hours per week, while mothers have gone from ten to thirteen hours per week.

Today's parents also tend to spend tons of time cooking, cleaning, babysitting and using their weekends to drive their kids to activities like Tae Kwon Do classes and Pokemon tournaments.

But, as a parent, you really don't need to make your whole life revolve around your children: there are plenty of ways to free up more time for you.

For example, you've probably pushed your kids into some activities they really don't even like — say, ballet or piano lessons? If so, cut them out of their (and your) schedule and let your kids decide how they want to spend their time. There are many stay-at-home alternatives such as playing video games, watching cartoons or playing in the backyard. Allowing kids to do what they love is just as good, if not better, parenting than chauffeuring them around town to activities they don't enjoy.

Another place to cut back could be family vacations: if the last trip was a nightmare, make the next one shorter to free up time for yourself.

Finally, sometimes you can easily get more time for yourself simply by paying for it. When you feel stressed out or tired, it's ok to pay for take-out meals, a babysitter or a cleaning service. It'll make you a happier and better parent, so it's money well spent.

> "_Children cost far less than most parents pay, because parents_ overcharge themselves."

### 4. Your parenting style won’t affect your child’s adult life as much as you think. 

So why do so many parents sacrifice everything for their children? The simple answer is: they worry that not doing so would ruin their children's future.

An unfounded fear: research indicates that, in the long run, your specific parenting style doesn't really matter.

Why not?

Because it's nature, not nurture, that mostly explains why children resemble their parents in the lives they lead.

This has been shown in studies of adopted children: when they're young, the children resemble both their adoptive parents as well as their biological parents. However, as the children grow up, only the resemblance to their biological parents remains.

Studies show that upbringing has no effect on things like life expectancy or income after the age of thirty. What's more, upbringing has little or no effect on factors like overall health, intelligence or happiness in adulthood. Nor does it bear much effect on academic success, the chances of teen pregnancy or adult sexual behavior.

So when parents think they're putting a child on "the right track" — say, towards becoming a musical prodigy — in reality, it's only temporary. In fact, children shouldn't be thought of as clay the parents can mold but as plastic: you can bend them with pressure, but they'll snap back to their original shape as soon as the pressure abates.

These disheartening findings beg the question: What's the point in parenting at all? Well, parenting does have a sizeable effect on how your children will remember you and how appreciative they'll be of your efforts. One of the things children most desire from their parents are "good memories."

So don't let these findings get you down. Look at them as great opportunities: you can raise your kids in _your_ _own_ way without feeling guilty. Treat them with kindness and respect without worrying you'll "ruin their future" by being too soft on them.

> _"False hope in the power of nurture leads to wasted effort and opportunities."_

### 5. Kids today are safer today than ever before. 

One of the worst things about being a parent is worrying that something horrible could happen to your child. But parents today tend to be much more worried than they should be. Despite what the media would have you believe, kids today are safer than ever.

For example, many parents long for the "idyllic" fifties when life was supposedly good and children safe. They compare this perception to what they see on TV, where child murderers and abusers abound as if they were a common occurrence.

In reality, kids are much safer today than they were in the 1950s: those under the age of five are about five times safer and those between the ages of five and fifteen are about four times safer.

And where has this dramatic increase in security come from?

Well, for children, disease was and is the number one cause of death, accounting for about 94 percent of their deaths. But, thanks to medical innovations, deaths from infectious diseases have effectively been eliminated, and the death rate from disease has declined by about 80 percent since 1950.

In addition, accidental deaths have also sharply declined for every age group thanks to technological developments like airbags and seat belts.

Another factor many parents will appreciate today is that, compared to 1950, when the Korean War was being fought, there is a smaller risk of their adult children dying at war. It turns out that even warfare has become safer these days since most fighting happens from the air. This development can be seen in the fact that the death rate has declined by a factor of 50 in the age group of 15 to 24 year olds!

In conclusion, parents today should actually be sleeping better than ever before.

> _"Our main challenge isn't keeping our children safe, but appreciating how safe they really are."_

### 6. Contrary to popular belief, the reason for fertility rates dropping isn’t that kids are hard to raise. 

One of the most common misconceptions about child rearing is that it's so difficult that it's driving people to have fewer and fewer babies. Let's examine this idea:

Indeed, fertility rates are declining in many places around the world. In the fifties, American women were having on average almost four children, whereas today they have just over two. Meanwhile, the fertility rate went from 2.3 to 1.3 in Germany and plummeted from 6.8 to 2.2 in Mexico.

Why such a drop? The popular explanation that it has to do with the difficulties of raising children just doesn't make sense.

For instance, some people believe that big families aren't economically viable anymore. But in fact today's families are richer than ever before: incomes have tripled since the fifties.

Another frequently cited explanation is that women are more reluctant to have children since they have their own careers and there's more at stake if they stay away from work for too long.

But in many ways women today have an easier time raising children than their counterparts in the 1950s: fathers participate more and useful technologies have emerged since then. Just imagine life without microwaves, Amazon, disposable diapers and dads who change them.

Finally, some also suggest that the drop in the fertility rate is due to the fact that kids don't provide for their parents in their old age anymore, so parents don't see the point in having more kids.

But this doesn't make sense, either: children have never been a good investment financially. Anthropologists have discovered that even in primitive hunter-gatherer societies, resources flowed primarily from the old to the young.

What's more, children today aren't really a worse retirement plan than they were 50 years ago. Before social security programs were introduced, elderly people died very young, at an average age of 68. But today the average life expectancy is around 80, so even if children give less annual financial support to their parents, at least they'll do so for a longer time.

### 7. The rewards of parenting grow over time: don’t focus on the immediate workload. 

It's not easy to decide to have more children or not. But when you do analyze the pros and cons, make sure you don't overemphasize the immediate negatives, like the sleepless nights, while forgetting the long-term rewards, like the pride in their accomplishments. Granted, this is easier said than done when you already have two toddlers running around raising hell and sapping your precious sleep. But remember: it won't last forever.

When you think about what would be the "right" number of children for you, the answer is likely to change over time. As children grow, parenting becomes less exhausting and more rewarding.

After all, kids you can interact with are much more fun than newborns who just cry, eat, poop and sleep. And by the time your children become teens, you'll be wishing they had more time for _you_. Later, when they've all moved out to college, you'll find yourself thinking that even three children wasn't enough.

To decide how many children you should have, you can use a simple rule of thumb: think about every major life stage for a child, and decide how many children you'd like to have in each. Then pick the average of the answer.

For example, if you think about infants, you may decide it's so hard you only want one. But then as they start walking and talking, they become more manageable, so maybe two would be all right. You feel like you could handle three teenagers sulking in their rooms, and definitely four adult children who only drop in from time to time. And then maybe you'd want five who'll start their own families and bring their grandchildren with them.

This would mean the right number of children for you is three.

Don't make the mistake of only looking at the first few years of a child's life and basing your decision to not have more children on them. This is short-sighted; in the big picture of life, having one child or none at all usually looks like a huge mistake.

> _"Basing your long-run decisions on your short-run crankiness doesn't make sense."_

### 8. More kids are good for society. 

One popular argument against having children is the danger of global overpopulation. But actually, having more people can also make the world a better place.

Why?

Well, first of all, more people means more ideas, and more ideas mean more innovations. Great ideas come from big communities.

For example, bigger language communities tend to create greater innovations: there are far more great books, films and television shows in English than in Ukrainian for the simple reason that more people speak the former and the talent pool and market are larger.

So, in a way, you could say you're contributing to a talent pool by raising a child.

Second of all, fertility is necessary for our current social system to survive because children are the future tax payers who will support the sick and elderly. And, as things stand, there won't be nearly enough of them.

Consider that in the 1940s, there were ten working people for every retiree. Today, the same figure is five, and in fifteen years it will be just three working people to support each retiree. Clearly, we either need to make more babies or policy-makers will likely have make some radical choices, like raising taxes or the retirement age.

So in that sense, having children is like making a charitable donation to future retirees!

Finally, population growth doesn't necessarily have to be bad for the environment.

At the moment, the main problem is that businesses have the wrong incentives: they can exploit natural resources and pollute the environment as much as they want for free, so naturally that's what they do. The government needs to step in and make doing that so costly that businesses will develop cleaner technologies and produce more environmentally friendly products. Carbon taxation is a good example of this kind of incentive.

Cleaning up the environment in this way isn't wishful thinking, either: stricter regulations have helped clean the air and water in many places. And it's surely a far more effective way of helping the environment than having fewer children.

> _"Parents who have another child make the world a better place, so you can walk the path of enlightened selfishness with clear conscience."_

### 9. If you want more grandchildren, support your children in their parenting and reward them for each grandchild they have. 

If you're starting to think that having children is a pretty good deal, wait until you get grandchildren! They're cute and playful just like your own kids, but with the crucial difference that you can send them home whenever you need a break. Sounds like a pretty good deal, right?

So how can you increase the probability of having grandchildren?

Of course, the simplest solution is to have more children of your own in the first place. But if this doesn't appeal to you, there are two other useful guidelines.

First of all, offer your support in raising their children, but don't criticize their parenting.

It too often seems that grandparents' child-rearing assistance only comes at the cost of unwanted interference and criticism: "You're giving the boy two toys for his birthday? You'll spoil him!"

Don't be one of those grandparents. You might feel your opinion is justified, but if the parents don't like what you're saying, they may decide against having more children. So don't bundle your babysitting assistance with unwanted parenting advice.

Of course, if you suspect child abuse, that's different, but as long as your children are raising your grandchild relatively normally, think twice before opening your mouth. Remember what was said in an earlier blink: the parenting style itself really doesn't matter that much, so don't try to impose your own ideal style on them.

A second method for getting more grandchildren is to subtly and tactfully reward your children for each grandchild they give you. If this sounds odd, consider that monetary rewards do work surprisingly well in boosting birth rates according to policy research.

One fairly diplomatic way of doing this without making anyone feel unfairly treated is setting up trusts for your grandchildren, with the parents as the trustees. This allows them to use the trust to pay for expenses like college, which they'd normally have to pay for themselves.

### 10. Final summary 

The key message in this book:

**Having** **kids** **is** **a** **much** **better** **"deal"** **than** **it** **might** **seem** **at** **first.** **Any** **perception** **that** **it's** **not** **is** **due** **to** **today's** **parents** **_artificially_** **increasing** **the** **"price"** **they** **pay** **for** **having** **kids,** **i.e.,** **by** **sacrificing** **and** **worrying** **too** **much.** **A** **more** **laid** **back** **parenting** **style** **would** **make** **parents** **happier** **and** **not** **hurt** **their** **children's** **future** **at** **all.**

Actionable advice:

**Go** **easier** **on** **yourself**

Parents spend far too much time worrying about how good a job they're doing raising their children. But as you've seen in the course of these blinks, it's an unnecessary worry. Children aren't some exotic species of tropical fish that need a highly controlled environment to flourish. If you provide a normal-ish childhood for them, they'll probably grow up just fine, so just relax!

**Stop** **making** **costly** **sacrifices** **for** **your** **children** **for** **no** **reason**

Many parents feel it's their duty to make costly sacrifices for their children, say, to put them in the best ballet school in town and ferry them to rehearsals every day. If you're doing that, pause for a moment to examine these activities and ask yourself three simple questions:

  * Do you enjoy it?

  * Does your child enjoy it?

  * Are there any long-term benefits?

This should help you gauge whether the sacrifice really makes sense — because leaving out unnecessary sacrifices _will_ make you a better parent.
---

### Bryan Caplan

Bryan Caplan is a professor of economics at George Mason University and the father of three children. _The_ _New_ _York_ _Times_ called his first book, _The_ _Myth_ _of_ _the_ _Rational_ _Voter_ the "best political book of the year."

