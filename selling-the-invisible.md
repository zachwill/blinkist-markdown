---
id: 50b3a29fe4b00d0268f2589f
slug: selling-the-invisible-en
published_date: 2014-01-22T14:30:00.000+00:00
author: Harry Beckwith
title: Selling the Invisible
subtitle: A Field Guide to Modern Marketing
main_color: AE2335
text_color: AE2335
---

# Selling the Invisible

_A Field Guide to Modern Marketing_

**Harry Beckwith**

Services represent a significant and growing portion of the modern economy, yet marketing them remains a mystery. _Selling the Invisible_ (1997) serves as a guide for promoting the intangible. It outlines how to set up a marketable service company, and how that business can then be advertised and promoted.

---
### 1. Services are playing an increasingly important part in the economy. 

These days, the American economy is going through some profound changes. Jobs that were seen as secure just a few decades ago have all but disappeared and new industries are springing up in the place of old ones.

One prominent shift is the staggering growth of the service industry, especially that of _service companies_ : companies that sell services, such as banking, legal representation and even haircuts, rather than tangible products.

Over half of the companies in the Fortune 500 ( _Fortune_ magazine's list of top American companies) are service companies, and more than 75 percent of the working American public is employed in this sector, a percentage which continues to grow.

Yet it is not just the service sector itself that is growing; services are playing an increasing role in other sectors of the economy too, especially when it comes to products:

Retail sales have always been strongly supported by customer service. A department store such as Dayton's, for example, sells physical products like clothes, but at the end of the day its revenues depend on the salespeople and how much they sell.

Likewise, McDonald's success relies on offering excellent customer service along with its popular fast food.

What's more, the services provided are ever expanding. Products themselves are now often accompanied by value-adding services. When you buy computer software, for example, you rarely get the program alone; it usually comes with a number of services such as technical support and upgrades.

Another good example can be seen in Levi's popular _Personal Pair_ jeans. After the customer has visited a Levi's store to have her measurements taken, Levi's uses these measurements to create a custom pair of jeans that fit her perfectly, and the finished product is couriered directly to her home. The extra level of service makes the product particularly desirable.

### 2. It’s difficult to market services because they’re intangible and their quality varies. 

In many ways, services are very different from physical products. For example, they can't be seen or touched, nor can they be mass-produced. This makes them very difficult to market effectively.

Traditionally, the product being sold plays a large role in marketing, but in the case of services the "product" is largely invisible. Marketing is easier when there's a physical product to sell, as customers like to see what they are able to buy. This is why companies with easily identifiable products — for example, Porsche — concentrate on image-heavy advertising where the product takes center stage.

Obviously, companies that sell services such as dry cleaning or insurance can't display their "products" in the same way. Therefore, they are forced to rely on more abstract advertising techniques.

For example, the US firm Travelers Insurance has no physical product to display, so they've created an image of their company in another way: they chose an umbrella as their logo because it symbolizes protection.

Another difficulty with marketing services is that the quality of the "product" isn't always consistent. Unlike physical products, where the standardized manufacturing procedure can be rigorously monitored, the quality of services can vary depending on elements such as which staff member serves you. This variation makes services difficult to market effectively.

For example, if a member of staff is rude to a customer, then it doesn't matter how good the marketing is, that customer will be wary of choosing any service from that company again.

### 3. Ensure your service exceeds customer expectations. 

Customers today expect better services than they did in the past. Companies like McDonald's raised the bar by being cleaner, faster and more consistent than their predecessors. Likewise, it is vital that your service is up to the standard in your industry. If you fall behind your competitors or have constant quality issues, you'll lose customers. To be competitive, you need to ensure customers see your service as high-quality.

But how do you evaluate the quality of your service?

Simply put, services are judged by how much they exceed customer expectations. For example, a customer in a restaurant expects a certain level of service: they want to be served quickly and to get what they ordered. If the restaurant delivers on this or provides something beyond this, the customer will judge it positively. However, if the table service is slow or if the order is wrong, they will feel dissatisfied.

You must, therefore, find out whether your service meets customer expectations.

The best way to find this out is by collecting feedback. Because most customers won't complain directly when they have problems with your service, you need to actively seek out their opinions. Feedback can take the form of an interview, questionnaire or telephone survey designed to gather honest customer opinions about your service.

So once you've gathered it, what do you do with this customer feedback?

Above all else, treat negative feedback as a learning experience. This is the perfect opportunity to correct your mistakes and ensure that you exceed customer expectations in the future. For example, if the feedback suggests that customers find your sales staff abrupt or unconvincing, retrain your sales employees so that they deliver better service in the future. By using feedback effectively, you can ensure that customers will come to value your service.

### 4. Make your service stand out by being different, bold and innovative. 

When a customer shops for a physical product like a car, it's easy for them to see the differences between models: each one looks completely different.

Services are a different story: Because they lack these visual elements, it's harder to tell them apart. Therefore, in order to distinguish your service from your competitors, it's important to set yourself apart in other ways.

The simplest solution is to do things differently from your competitors. Obviously, being better is advantageous, but if that's not possible or if customers don't recognize your superiority, then just being different helps set you apart.

For example, Fed-Ex redefined logistics by offering a service completely different from that of the US Postal Service. Because it was so different from its competitor, it really succeeded in grabbing the attention of customers.

Another way you can gain significant benefits is by breaking new ground. Those who do this can benefit from the adaptor's edge: the advantage gained by being one of the first in a new business territory, meaning that there are few, if any, competitors present.

A great example of this is the retail giant Wal-Mart, which opened its first stores in small towns that existing retailers had dismissed as having too few consumers. In fact, when these small communities were added together, they made a massive untapped market, which Wal-Mart was able to exploit to their advantage.

Even if you are well established in your business, long-term adaptation through innovation is also important. No market is stagnant. Conditions change, so companies must change with them, and companies that innovate to keep up with the changes stand the best chances of succeeding.

For example, in the past people used banks to help them manage all their finances, including their insurance policies. Yet the banks were too comfortable in their position and eventually became stagnant. This is when new businesses like credit unions and insurance companies sprouted, and these new organizations stole customers from the banks through their innovative services and strategies.

### 5. Don’t get stuck in the planning phase – instead, be proactive and be ready to take action. 

Many businesses, both large and small, practice _strategic planning._ This means that they carefully define both their future goals and the path they'll take to get there. However, despite its popularity, this planning can only take a business so far. 

In fact, obsessive planning has many limitations.

The first of these is that planning is largely concerned with determining future goals. However, you can't accurately predict the future. Planning for a future that may never happen can leave a company in the wrong place. For example, many people wrongly believed that television would erode the book publishing industry. Those who based their plans on this idea later found that their plans had become useless.

Another common planning drawback is that if you plan until you get everything perfect, you could get left behind. Many software companies, for example, have spent years and millions in resources developing the "perfect product," only to find themselves left behind when the market or technology changed, or when their competitors got there first. 

So if obsessive planning doesn't work, what does?

The key is getting a balance between planning and being proactive. You need to plan for what you expect to happen, but be ready to take advantage of unexpected events. In many cases, success comes not from having an exact plan but from simply being in the right place at the right time, and reacting intelligently to available opportunities as they present themselves.

While planning forces you to focus on the final version of your service offer, being proactive allows you to change and evolve. For example, don't be scared if your offers fail initially; you can always learn from mistakes. The Macintosh computer was released after Apple's first computer, the Lisa, failed. They learned from the experience and made a better product.

### 6. Make sure that every area of your business, from employees to branding, forms part of your marketing strategy. 

Companies typically spend huge amounts of their resources on researching, planning and implementing the perfect marketing strategy to get customers interested in their business.

Yet, this careful marketing strategy is often undone by problems in other areas. For example, a single rude shop assistant in a retail chain can have a huge effect on sales, regardless of the company's advertising, pricing strategy or the products themselves.

This is because customers don't distinguish between different areas of a company. From the advertising campaigns to the staff on the front line, everyone is perceived as a part of the business. Unlike companies, which generally treat marketing as a separate activity, customers see everything the company does in the same light, and thus everything is seen as "marketing."

Because of this, it's important to go through every aspect of the company the customer may encounter and make sure it reflects your marketing message, from business cards to the office lobby to the uniforms on your staff.

Your marketing message should also be reflected in your employees, especially in those who make contact with clients and prospects (potential customers). Analyze the people at each point of contact, from those on reception to your sales staff, and make sure they are all trained to reflect a positive image of your service.

If a prospect has nothing but good experiences from the employees they interact with, they will form a good impression of the company. 

Failure to treat all your staff as extensions of your marketing message can have negative consequences. Say, for example, a badly trained member of the finance department gives a customer a rude response. That customer's anger will be directed against the entire company, and it won't matter how well the staff in other departments are trained; the customer's experience of the whole company has already been tarnished.

### 7. Customer choices aren’t always rational – irrational factors like prestige and familiarity can also influence behavior. 

How do people choose what products and services to buy?

You might think that purchases are driven by reason and logical factors like price and quality, but in fact customers often choose their products based on irrational factors.

One of these irrational factors is prestige, which many people choose over practical factors such as price. Rather than look for the best value, people often seek out products that give the impression of being elite. For example, credit cards by American Express are popular, despite the fact that its fees are higher, it's less convenient to pay off and its cards are less widely accepted than other credit cards. This is because American Express markets itself as a club, and so its members feel that they are a part of something exclusive.

Another irrational factor in buying behavior is familiarity. The more people see or hear about something, the more likely they are to buy it, simply because it comes to mind much quicker than any alternatives. Therefore, it's important to make yourself visible and spread the word about your business. For example, the IRS tax office in the United States plants stories about tax evasion trials in newspapers nationwide each March, just before taxes are due in April. This helps reinforce the date in people's minds.

So, rather than appealing to customers' reason alone, also consider these other factors. Things like prestige and familiarity can influence buying decisions as much as price and quality can, and failure to consider this can yield disappointing results.

### 8. Position yourself for success by narrowing down the focus of your service. 

For your service to be successful, you need to build yourself a specific _focus_ and _position_. It is perhaps best to think of these in the following way: think of _position_ as how people perceive you, or, in other words, the external view of your business; think of _focus_ as the internal view, or how you perceive and understand yourself.

But how do you build both of these?

First of all, in order to determine your focus, you must narrow down your business to one distinct area. This allows you to concentrate your resources onto one particular area. Scandinavian Airlines provides a good example of how to do this. They narrowed down their focus to concentrate on attracting business-class passengers. In doing so, they increased their number of business-class passengers, and by filling more of these expensive seats they were then able to lower their economy-class prices.

Next, concentrate on your position. Usually, your position stems naturally from your focus, and customers will connect you with the area you concentrate on. However, this is not always the case, as you can't totally control what people think about your company.

In cases like these, you can help to guide your position by writing a _positioning statement,_ which helps you determine and clarify to customers what your position is. This statement outlines who you are, what you do, who your competitors are and what benefits you offer in comparison.

For example, the retail outlet Bloomingdale's could create a positioning statement outlining that it's a fashionable department store targeting upper-middle-class shoppers who prefer premium products. Once Bloomingdale's identifies itself as offering high-quality products and shopping services, they can ensure that they find the best way to communicate this to those prized customers and beat their rivals.

### 9. Low-cost and middle-market pricing are risky strategies to take; high-end pricing is often the safest strategy. 

Price is a strong communicator about the quality and value of a service. There are few things more important in marketing than pricing, but how should you price your service?

Logic would dictate that customers always go for the lowest price. However, this is rarely the case.

Although being a low-cost provider can be an attractive pricing position, it is one that should be taken with care. Being the cheapest may attract price-conscious customers, but there is almost always a cheaper option somewhere else, and you'll have to continually lower your prices to compete. For example, department stores such as Sears competed on the low end of the market for years, but were undercut when Wal-Mart expanded. Wal-Mart had an aggressive pricing model that Sears simply couldn't match, which caused its business to suffer.

While the low-cost option has its risks, it is even more risky to aim for the middle of the market. Having middle-market prices sends the message that you are good but not great. Therefore, quality-conscious prospects will choose competitors on the high end of the market, while price-conscious prospects will look elsewhere for a cheaper option. By being in the middle, you have to compete with both of these other price positions.

If low- and mid-range prices aren't the solution, then what is? Surprisingly, it's often best to aim for a higher price than your competitors. This is because people generally associate high prices with high quality. This is why a Picasso sketch is worth more than a simple doodle. Being priced at the top end of the market tells prospects that you believe your service has value and that it's worth paying for.

### 10. To be successful, turn your company and the service you provide into a brand. 

Imagine you are hungry after a long day of driving and you enter an unfamiliar town in search of somewhere to eat. Instead of choosing the first restaurant you see, you — like many of us — would probably continue driving until you spotted a branded service like Burger King or McDonald's. This is because certain brands promise a high standard of service; a brand is something like an insurance against poor quality. People feel more comfortable choosing recognizable brands as they know what to expect.

If you want to be successful, you should develop your service into a brand. But how do you do this?

The most important aspect in developing a brand is to make yourself easily recognizable. A strong, memorable name and an eye-catching logo will help to distinguish your brand from competitors. First, find a name that is both memorable and unique to help you stand apart. Then develop logos and other visuals to reinforce your brand. Make sure these fit your business. For example, FedEx uses blues and reds, similar to the US Postal Service logo in its own visuals, to both associate and contrast its delivery service with the more familiar service.

Not only must you build a great overall image, you must also develop the reputation of your brand. You can build and reinforce your reputation by _investing in your integrity._ This is where you spend time and effort ensuring that customers see your brand as trustworthy and high quality. For example, IBM spent years grooming an image of quality and reliability, even developing the phrase, "No one ever got fired for choosing IBM." This built up the reputation that the brand could be relied upon for quality.

If you manage to develop your service into a recognizable and trusted brand, then your chances of success will be greatly improved.

### 11. Stories are powerful and convincing things – turn your message into a compelling story about your service. 

A _story_ isn't always a work of fiction; in fact, a story can also be a message that creates a picture of your brand. Just as lawyers will use stories to vividly illustrate complex arguments in trials, marketers can use them to build up a positive picture of a brand for customers.

Yet, to create a good story, marketers must ensure that the message follows certain key principles. 

First, the message at the heart of your story should be _consistent_. Make sure all elements of the message match up in order to avoid confusion. For example, top restaurants create a consistent story through having stunning decor, an elegant atmosphere, and inviting exclusive customers. This consistent story helps them in presenting their food as top quality. As most customers aren't food experts, they look to the overall story to judge a restaurant's quality.

Next, you must make sure you are able to show _evidence_ to back up your story. This evidence can come in the form of, say, customer feedback, positive publicity or quality scores from surveys. If, say, a survey asking customers to rate your service out of a maximum of 4 comes out with a score of 3.96, then you should use this evidence to build up a story about the quality of your service. If someone asks why you think your company's great, you can show them the survey results.

Finally, the story you create needs to focus on the demands of the prospect. People don't buy things because someone wants to sell them something; they buy things because they need something. Therefore, your story must address these demands, otherwise people won't be attracted to your company.

### 12. Final Summary 

The main message in this book:

**Marketing services can be challenging, but it's very important in today's economy. The heart of any service-marketing effort is the service itself, as well as its focus and position, as these empower things like branding and reputation. As services are not tangible, they are best promoted through clear and consistent messages that tell a compelling story about the company.**

This book in blinks answered the following questions:

**Why is service marketing important?**

  * Corporate and customer services are playing an ever-expanding role in the economy.

  * As services are intangible and inconsistent, they pose significant challenges to marketers.

**How should you get started with marketing a service?**

  * Collect feedback to find out how customers judge your service, and identify how you can improve it.

  * Make your service stand out by being different, bold or innovative.

  * Don't get stuck in the planning phase — instead, be proactive and be ready to take action.

  * Make sure that every area of your business, from employees to branding, forms part of your marketing strategy.

**How can you attract customers to your service?**

  * Customer choices aren't always rational — irrational factors like prestige and familiarity can also influence behavior. 

  * Position yourself for success by narrowing down the focus of your service.

  * Low-cost and middle-market pricing are risky strategies to take; high-end pricing is often the safest strategy.

  * To be successful, turn your company and the service you provide into a brand.

  * Stories are powerful and convincing things — turn your message into a compelling story about your service.

### 13. Actionable ideas from this book in blinks 

**Work on making your service great before you start anything else.**

Service quality is very important to service marketing. It is hard to promote or advertise a service that's lacking in quality.

Ask yourself whether your service is something people would want to buy or would say good things about. If this is not the case, you will have trouble marketing it. Customers don't flock to businesses who offer poor-quality service, nor do they say good things about these businesses to others.

**Narrow down your focus to define a strong position.**

Focus is how you view and present your company. Position is how customers perceive your company.

You can't possibly do everything at once with your service, nor can you promote everything about your business at once. If you try to stress your quality, prices, speed and efficiency all at once, you will not find yourself in a strong position. Focus on one thing, and on doing and promoting that one thing very well. Your position will naturally stem from this focus, as customers will associate you with it.

**Turn your service into a brand.**

Brands are powerful things. Most people choose trusted brands over their unfamiliar competitors, because brands promise consistency and quality.

Craft your brand using the combination of a good, memorable name and visual elements that fit your company's image. These can be things like logos, letterheads or even your office design. Back up your brand with quality and integrity. This will take time, but it is well worth the effort.
---

### Harry Beckwith

Harry Beckwith had a successful marketing career before going on to become an influential speaker on sales and marketing topics. He gives lectures to employees at large national companies as well as to students at Ivy League universities, and is considered one of the best public speakers in his field. He has written five best-selling titles, including _Selling the Invisible_ and _Unthinking_.

