---
id: 595a326cb238e10007cd509b
slug: selling-with-noble-purpose-en
published_date: 2017-07-07T00:00:00.000+00:00
author: Lisa Earle McLeod
title: Selling with Noble Purpose
subtitle: How to Drive Revenue and Do Work That Makes You Proud
main_color: F67431
text_color: A84F22
---

# Selling with Noble Purpose

_How to Drive Revenue and Do Work That Makes You Proud_

**Lisa Earle McLeod**

_Selling with Noble Purpose_ (2013) is about finding the right balance between making money and doing something meaningful with your life. It allows you to reframe your work by focusing your intention on the customer and how they truly benefit from your product. It's a perspective that also keeps employees happier, more motivated and effective. Selling doesn't have to be focused on profits and greed; it can also be about making the world a better place.

---
### 1. What’s in it for me? Find out why purpose drives sales more than sales pitches and catchphrases. 

Have you ever worked in a sales team? If so, what was the atmosphere like?

So many sales teams operate in an environment of pressure and machismo. Each employee is encouraged to concentrate on maximizing their own sales, come what may. No matter what you're selling, or whether your prospect really needs it, you should push, push and push again.

But, as common as this is, it's not the best way to get great sales and revenues.

Instead, a sales culture should be about getting the people selling the product to understand the purpose of the product and how it will help the customers. Selling with a noble purpose will create the drive and motivation any sales organization needs. So let's find out how that is achieved.

In these blinks, you'll also discover

  * why our brains feel wired when we do something we believe in;

  * how customer and business needs go hand in hand; and

  * why you should invest in a CRM-system rather than manager salaries.

### 2. Finding the true purpose and meaning of your work will help you feel motivated and alive. 

Do you dread going to parties for fear of having to answer the question, "So what do you do for a living?"

If so, there's a good chance that you don't feel your work is making a significant contribution to the world. The common, vague response to the question could be, "Oh, I work for a software company," or, "I work in sales."

You can try this now — pretend you're talking to someone you've just met and take a moment to listen to yourself as you say what you do out loud. Do you sound less than enthused about your job?

If so, it's time to do things differently. After all, it's important to feel that you are making a difference in the world.

Start by asking yourself a new question, "When was the last time my work made a difference to someone else?" It doesn't have to be a client or customer — maybe it was a co-worker?

If you can, describe this scenario out loud, preferably to someone else, and consider how it makes you feel. Chances are, your speech will improve, you'll feel engaged, experience a boost in self-esteem, and you'll finish the conversation feeling motivated about your job.

This reaction shows how important it is to your own well-being to find meaning in the work you do.

There's a biological reason behind this reaction as well, as having a sense of purpose pushes your brain to operate at a higher level.

When you just rattle off your job title, it's like you're on autopilot and using the least amount of brain power.

But this isn't the case when you describe the difference you've made to someone else. It stimulates your frontal lobe, the part of the brain responsible for reasoning, planning, problem-solving, empathy and the ability to be altruistic.

In the next blink, we'll look at how to add purpose to the act of selling.

> _"Great minds have purpose, others have wishes." — Washington Irving_

### 3. Selling with noble purpose means focusing on customer needs, which can also generate profits. 

The concept of _noble selling_ starts by focusing on customers' needs, not on making them buy junk that's useless or even harmful.

Most companies start out with good intentions, so let's look at one of the biggest sellers: Procter & Gamble (P&G).

Around 2000, despite being determined to make more sales and defeat their competition, P&G were losing money. But then Jim Stengel took over as chief marketing officer and recognized that the company had lost sight of its purpose, so he refocused the staff on one concept: selling products that improved people's lives.

Under Stengel's leadership, every product that P&G developed had to show a significant and measurable way of making someone's life better. This principle would inform all the decisions that went into selling and marketing the product.

It was just the kind of motivation that P&G needed, and by 2001, the company was once again earning a profit.

Selling with a noble purpose has proven profitable for other companies as well — such as Southwest Airlines.

People do need to fly from time to time, and Southwest operates with the noble purpose of making the skies accessible and affordable to everyone.

Roy Spence is Southwest's marketing expert, and one of his favorite stories happened years ago when all the airline companies began to add extra fees for luggage.

When Southwest was told by their consultants that they should also add the fees, and likely earn $350 million a year, Roy Spence was not on board. He recognized these fees as being in complete opposition to the company's goals since it would make flying less accessible and more expensive.

Instead, Spence came up with an ad campaign that stuck to their purpose. It had the tagline "Bags fly free."

Once again, adhering to the company's noble purpose paid off: the ads brought Southwest new customers and increased their revenue by $1 billion.

### 4. Scary bosses are not good for business, as fear takes the focus away from customer needs. 

You probably encountered at least one scary and intimidating teacher during your school days. Chances are, whatever subject they taught became your least favorite and earned you your lowest grades.

It goes to show how fear is never a good motivator, a good lesson to which all businesses should adhere.

For a good example of why scary bosses are never good for business, let's return to P&G

In the late 1990s, the author was working for P&G when a new boss was made head of her department. One day, this intimidating boss accompanied the author on her trip to one of P&G's biggest clients so that he could assess her sales pitch.

Even though the presentation went well, the boss was furious with her once they got back to their car. He yelled at her for not pushing the client to buy more items.

So, what did this angry approach lead the author to do? The next time her boss accompanied her on a sales pitch, she called the clients ahead of time and got them to agree to play along, say they'd buy more products and then work out the real contract later to buy fewer items.

If the boss really wanted to improve the author's sales, he shouldn't have used fear as this only shifts the employee's focus away from the customer's needs to their boss's satisfaction.

When you're afraid, whether you're facing a growling bear or a screaming boss, your fight or flight instincts are activated, and the only thing you'll think about is how you can best get out of the situation.

So, if you're afraid of your boss, you won't be thinking about how you can best serve the client; you'll only be thinking about how to sell the most products possible.

Naturally, this can put your accounts in jeopardy as a client could easily assume you're not paying attention to their needs and start going to your competitor instead.

So remember: fear equals fewer sales.

### 5. An honest sales pitch can’t be faked, so it’s best to sell products that are truly helpful. 

A salesperson should be aware of other instincts besides fear. If you've ever been a customer with the sneaking suspicion you should say no to someone's sales pitch, however good it sounded, then you know how influential these instincts are.

Customers pick up on the unconscious signals that any salesperson can give off, such as body language and the tone of their voice.

UCLA psychology professor, Albert Mehrabian, studied the factors that go into a person's decision to trust — or not trust — someone. He found that 55 percent of the time that choice was based on a person's body language versus 38 percent on the tone of their voice. What they were actually saying only counted in 7 percent of the examples.

This is why your sales pitch is likely to get turned down if you show signs of being anxious or scared, which is bound to happen if you know you're selling a bad product.

Ultimately, you can't fake selling with conviction, which is why you must believe that what you are selling is truly beneficial to people.

Even though unconscious signals can't be consciously controlled, many companies will nevertheless try to defy logic by using training programs that attempt to teach salespeople ways of altering their voice, body language and expressions in order to seem more convincing.

But none of this is necessary if the salespeople believe in the product they are selling and can honestly say it will enhance the customer's business or life.

Then there's no need to fake body language or worry about unconscious signals. Salespeople will be genuinely excited to talk about their job and what they have to offer, and these are the signals the customer will pick up on.

### 6. Creating engaging stories about your company is crucial for motivation and sales. 

Every family has stories — the kind that might come out at holiday dinners when relatives reminisce about unforgettable events. But aside from being entertaining, they can also be informative, such as the stories the author's father would tell her about finding a way to get back at his cruel cousins. These stories taught the author how important it is to find the spirit to resist being a victim.

Such narratives are also crucial for a company to thrive.

By giving your company an engaging story, you'll have a great way to boost employee morale.

Graham-White is a Virginia-based manufacturer that produces equipment for trains, including brakes and filters, and employee morale wasn't always so great.

But then, one day, a company representative was out on assignment, trying to make repairs on a customer's locomotive. But doing his job wasn't so easy as it was next to impossible to find a time when the locomotive wasn't being used to drive a train.

Nevertheless, the representative waited and waited. When it turned midnight, he decided to wait in his car. As the snow fell all around him, he eventually fell asleep. But he awoke at 3 a.m., when the customer called to say the locomotive was ready.

Thirty seconds later, the sales rep was doing his job, and found a mechanic to make the necessary repairs to the engine and the locomotive was ready for the next day's work.

When the author heard this remarkable story, she made sure that Graham-White's managers shared it and told it to every new employee, as an example of how committed the company was to its clients.

Immediately, employees were in better spirits and felt more connected to their work.

What's more, a good story can also boost sales.

McLeod spoke to Graham-White employees to find stories that would testify to the quality of their products.

One employee recalled visiting a train yard on a cold day in Atlanta when temperatures had dropped well below freezing. Every locomotive had brakes that had frozen and needed repairs — except for the two that were fitted with Graham-White brakes.

These locomotives were then used to pull the other ones into the heated repair shop. It was the perfect illustration of how superior Graham-White products had saved the day and worked like a charm to bring the company new clients.

### 7. A good Customer Relationship Management system will improve sales more than a good supervisor. 

You might think the most important ingredient in good sales training is a good sales manager.

But in reality, a manager will only directly supervise a maximum of 10 percent of the calls a new salesperson makes; the actual total will be closer to 2 percent. Statistics also show that a salesperson only receives coaching before and after a call no more than 20 or 30 percent of the time.

There is one thing a salesperson always has reliable access to, and that's their computer. Which is why it's crucial to have a good Customer Relationship Management system (CRM) that's filled with useful information.

A good CRM system is a key ingredient to the success of your sales team, so it's important for it to be kept updated with useful information.

Let's say you have two sales associates, Caller A and Caller B, and on the screens in front of them, Caller A has a list of all the products their client has previously ordered and the amount of revenue this has generated.

Caller B has the same list, but their version of the CRM also contains details about their client's business environment, such as their professional goals and what kind of challenges they're facing from competitors.

Which salesperson is going to be more successful? That's right, Caller B, because they'll be able to ask more relevant questions that are tightly focused on the customer's interests and unique situation.

That's why, if you really want to give your sales team a helpful advantage and boost sales, you'll make sure you have a CRM system that contains detailed goal-oriented information.

In the next blink, we'll look at some helpful tips for collecting that information.

### 8. It’s important to collect detailed information on successful sales and share it with everyone. 

It's fine for a sales team to throw a party after an especially successful run or landing a big client, but don't spend more time planning the party than you do analyzing exactly what led to your success.

If you want to continue making great sales and increasing revenue, you need to practice your ABCs and Always Be Collecting information on what made those sales work.

At the software company, Citrix, the former chief marketing specialist, Traver Gruen-Kennedy, used detailed case studies of previous sales to boost revenue from zero to $500 million in just five years.

Citrix is now recognized as being ahead of the curve on cloud computing, but it wasn't easy landing those early customers. Not too long ago, many industries were still hesitant to adopt these new technologies.

So, to increase sales, Gruen-Kennedy wanted his employees to record all the details of one successful sale each month. He even made a template that the employees could use to ensure that they recorded the right details, such as why the client chose Citrix and how their software was beneficial to them.

This information is great to have, but it's only going to be useful once these secrets are shared with everyone else.

It's all too common for salespeople to think competitively rather than cooperatively. So they tend to be protective of the information they learn after closing a big deal.

Likewise, if your teammate lands a huge client, you might be tempted to just congratulate them while refraining from asking any questions so as not to come off as a snoop.

This is why it's so important to build an open library of case studies that everyone can consult at any time.

With this treasure trove of details, before an employee makes a call to someone in a specific industry, they can look up all the prior successful sales from that same industry and get a good idea of what kind of pitch to make.

For example, if they're calling a new mobile telephone company, the salesperson could bring up all the details on the Canadian mobile phone companies that were the early adopters of Citrix.

### 9. Simple rules can keep employees motivated and engaged, which always makes work more enjoyable. 

Just because your organization offers a beneficial service or product, doesn't mean you can't end up with unmotivated employees.

But there are steps you can take to ensure your staff stay engaged and on the lookout for some straightforward and quick actions to take.

Simple, one-minute acts are a great way to keep employees from drifting off into the land of the unmotivated if there happens to be a lull in activity. It might be as easy as checking in with clients to just remind them that you're always available.

A good example of this is a policy that Boston University uses to keep their staff engaged.

One day, the author was touring the campus with her daughter when they were approached by a kind man who asked them if he could be of assistance.

But this wasn't some part-time employee with nothing better to do; it was the dean of students. And even though he had 4,500 new students to worry about, he was still motivated to approach individuals personally and offer assistance.

The dean isn't just an over-achiever, either, he was following a school policy that says _any_ staff member who notices a student or parent looking lost should stop what they're doing and offer assistance.

This may sound excessive, but it leads to a better work environment by reminding the staff of their purpose: to educate, communicate and make students feel welcome on campus.

This proactive approach also makes work more enjoyable.

Everyone experiences a time when there aren't any emails to open or phone calls to answer. In these moments you might be tempted to surf the web and wait for something to come to you. But this can erode your sense of purpose.

Instead, try to think ahead, anticipate what's next and prepare yourself. Or, think in the other direction and analyze a past job to find ways of improving. Maybe you can create a template that will help you handle future requests more efficiently?

There are always improvements to be made; the important thing is to never stop looking for them.

### 10. Final summary 

The key message in this book:

**Selling isn't just about numbers, statistics and money. It's about making a contribution to the world by finding out what people need and connecting them to a product that can satisfy that need. With this in mind, selling can be a noble pursuit that makes the world a better place one sale at a time.**

Actionable Advice:

**Don't panic about sales calls.**

Gaining a customer's interest during a short phone call is hard. But if you're well prepared and remember to focus on your customer's interests, things will go much more smoothly. If you're a manager, remember to coach your salespeople both before _and_ after calls and keep reminding them to think and reflect on what the customer needs.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _To Sell Is Human_** **by Daniel Pink**

_To Sell Is Human_ explains how selling has become an important part of almost every job, and equips the reader with tools and techniques to be more effective at persuading others.
---

### Lisa Earle McLeod

Lisa Earle McLeod is a sales expert who has worked for leading companies such as Procter & Gamble, Apple and Kimberly-Clark. She is a commentator for the business website Forbes.com and has written for the _New York Times_ and the _Wall Street Journal_.

© Lisa Earle McLeod: Selling with Noble Purpose copyright 2008, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

