---
id: 599094e3b238e100058f7805
slug: sensemaking-en
published_date: 2017-08-14T00:00:00.000+00:00
author: Christian Madsbjerg
title: Sensemaking
subtitle: What Makes Human Intelligence Essential in the Age of the Algorithm
main_color: B19C4B
text_color: 7D6E35
---

# Sensemaking

_What Makes Human Intelligence Essential in the Age of the Algorithm_

**Christian Madsbjerg**

In a world where data, numbers and statistics are treated like holy relics, _Sensemaking_ (2017) powerfully advocates a return to humanities-based thinking. These blinks explain the process and principles of _sensemaking_, a way to make sense of the world through the interpretation of human culture. Being able to look beyond the immediate focus and understand the context surrounding the issues at hand is a critical tool for anyone looking to develop great, one-of-a-kind ideas.

---
### 1. What’s in it for me? Learn the power of a humanistic, culture-based way of thinking 

Many debates and discussions get clouded with abstract numbers and statistics. Perhaps you've come across some know-it-all who dismissed your arguments with a lofty "Well, what the data _actually_ shows is…"

There seems to be a growing tendency for algorithms and automated number crunching to be prioritized over philosophy- and humanities-based thinking. This skewed focus compromises education and business, and has detrimental effects on society.

In these blinks, you'll find out why we should instead turn to "sensemaking," a better way to engage with the culture around us.

You'll also learn

  * how big data failed to predict the H1N1 pandemic;

  * how investor George Soros used sensemaking to make $650 million in a single day; and

  * how the FBI freed a captive by using sensemaking.

### 2. Sensemaking is a form of cultural engagement made up of five principles. 

Compared to the mechanical efficiency of computers, it's clear our human thought processes are flawed. Yet we do have one distinct advantage over machines: cultural knowledge.

This cultural knowledge can be most powerfully harnessed in a method called _sensemaking._ Sensemaking is the process by which we gain wisdom through examining cultural knowledge _._ It's a kind of cultural engagement closely associated with the humanities.

For example, it might seem normal today to look to numbers for meaning, but sensemaking allows us to go further than sales figures, turnover rates and the like. It gives us a 360-degree perspective on the world.

There are five basic elements to sensemaking.

Firstly, sensemaking understands that humans shouldn't just be defined by their individual personalities. People are also characterized by their cultural context.

Secondly, look for thick data. Thick data establishes what is significant in a given culture. For instance, the fact that 86 percent of US households might drink more than six quarts of milk per week is _thin data_. Thick data would help us understand what milk means to them and _why_ they drink it.

Thirdly, consider that human behavior is best understood as existing in social contexts, rather than abstractly. Take love, for instance. It's so much more than a chemical reaction in the brain that can be studied in a lab. To understand it fully we'd have to look at how love has been experienced in, say, classical India or the West today.

Fourthly, you shouldn't look just to strict logical processes. Insights can be gained through immersion, intuition and hypothesizing.

The final precept reminds us that we shouldn't get our bearings from data alone. If we attune ourselves to the world — looking to the stars, not GPS — we can also reach significant insights.

### 3. Silicon Valley wants tech solutions that don’t fit with humanities-based thinking. 

Sensemaking is needed more than ever today. Just look at how the Silicon Valley mind-set is threatening humanities-based thinking. After all, the proponents of Silicon Valley think that technology can fix any problem, no matter what it is.

However, this Silicon Valley mind-set is completely at odds with the five principles of sensemaking. Just look at three of Silicon Valley's central precepts.

Firstly, the notion of _disruptive_ innovation is critical. Instead of setting out to simply sell new products, tech companies aim to overturn the market and create something radically new. Nothing could be further from the intellectual traditions of the humanities. The humanities are all about understanding the place of ideas and concepts _within_ contemporary and past cultures.

Secondly, Silicon Valley is obsessed with the statistical analysis of _big data_ or poring over extremely large data sets.

In 2008 Google developed Google Flu Trends. The company wanted to use search trends to predict influenza outbreaks. It was a huge failure. The 2009 H1N1 pandemic went unnoticed, while 2012 and 2013 were predicted to be huge flu years. They weren't.

Google's algorithm was thrown by searches related to flu _season_, which has little to do with actual flu outbreaks. It noticed correlations between different searches, but obviously, it couldn't decipher the _intention_ of each search. Correlated queries like "chicken soup" started churning up false flu warnings!

Thirdly, Silicon Valley loves _frictionless_ technology, which operates smoothly and intuitively, without needing any active human input. You'll recognize this from the way Google and Facebook filter the information that reaches you, as they try to guess your preferences or what you might want to buy.

But frictionless technology traps us in a _filter bubble_ where we get exposed to the same things again and again.

But real human interaction doesn't work like that, despite Silicon Valley's best efforts.

### 4. Knowing how cultural context defines humans leads to a business advantage. 

The first precept of sensemaking — that humans are defined by their cultural context — can be put to great use in business.

Ford did just this when the firm wanted to overhaul its luxury Lincoln automobile range. Times were bad for the Lincoln; it held a meager 5.5 percent share of the luxury car market. Of course, Ford had plenty of general data about the market situation and their customers. They knew how old Lincoln owners were and where they came from, but they lacked a deeper understanding of their customers' cultural context.

This is where sensemaking came in.

Ford devised a comprehensive study to establish the _vehicle ecologies_ of their customers. That is, they wanted to understand their typical customer's social structures.

Over half a year, they tapped the wives, husbands, sisters, brothers, neighbors and friends of 60 of their customers. They wanted to get a real sense of who they really were and what motivated them.

They discovered that Ford's emphasis on engineering and technical specs left many customers cold. For most of them, driving was only a small part of the overall vehicular experience. The data also revealed that luxury product ownership was a form of self-expression in private spheres for these drivers.

With all this data and understanding of cultural context, Ford set out to create a far more holistic product for their customers.

They realized that the concept of luxury itself should be used to structure the engineering and design process of their cars. It didn't matter if the antilock braking system or the window switches or steering wheel were the immediate focus of the design process. Each component needed to scream "luxury."

It was by using sensemaking that Ford was able to appreciate their customers' cultural context and reinvigorate the Lincoln brand.

### 5. Thick data will give you a competitive edge. 

Thick data expresses what is significant in a culture. It captures more than plain facts by supplying the context.

Investor George Soros is a prime example of how thick data can be used to make a fortune.

In 1992, Soros became known as "The Man Who Broke the Bank of England." He bet that the bank would have to devalue British currency. When it did, it made him a fortune of $650 million in a single day.

But how did he know? He was guided by the culture of thinking at Soros Fund Management. Soros himself had studied philosophy, so data meant more than just numbers to him. It was the thick data of experiences, newspaper articles and conversations that he put to use.

But what makes data thick? It's less about objective knowledge than about recognizing the context of this data.

To get a better sense of it, we'll consider the nature of knowledge. There are four types.

Firstly, there is _objective knowledge_. It's the basis for the natural sciences. "One plus one is two" and "water is made up of hydrogen and oxygen" are examples of objective knowledge statements. Objective knowledge is universally true.

Secondly, there is _subjective knowledge_. This is knowledge based on personal opinions and feelings, like "I'm cold" or "I want apple pie."

Thirdly, there is _shared knowledge_. It's public and cultural. It's the shared human experience. We can talk about the Jewish experience, for example.

Fourthly, there is _sensory knowledge_. This is what is often called the "sixth sense." This knowledge is intuitive. Soldiers might be able to "feel" the presence of booby traps nearby and thereby avoid them.

Sensemaking entails synthesizing all four types of knowledge without prioritizing any of them over the others.

### 6. Phenomenology provides a framework for sensemaking and business research. 

The third precept of sensemaking directs us to collect thick data in the real world, not some realm of abstraction. Choose the savannah, not the zoo!

For instance, philosophy provides us with the concept of _phenomenology._ This means we should describe phenomena as they actually are and not as we think they should be.

It's phenomenology that provides the framework for translating sensemaking into real-world practice. It's a call for us to go out and observe what people do. Instead of scrutinizing lions in a cage, we should observe them on the savannah.

As a general rule, most of us are confined to the metaphorical zoos of offices or corporate strategy sessions. Instead, we should go out into the real world and use our senses to experience what's happening. We should avoid being in hock to abstract theories and habitual assumptions.

The author used sensemaking-based phenomenology to help a large and struggling European grocery chain get back on track.

The chain's technical data was great but limited in its use. The firm knew all about how much a customer spent on each visit or what women between the ages of 25 and 38 liked to buy. But they didn't know about the actual experience of shopping and what happened to the food once it got home.

The answer lay in reframing the questions put by management to customers. The best way to increase revenue was to ask how people experienced _cooking_.

Once the company had compiled responses to this question, it was better able to understand the decisions that customers were making while in the store. Before too long, the company was firing on all cylinders again.

### 7. Creative ideas begin with immersion and sensitivity. 

How do you express the feeling when an idea arrives? You might use expressions like "it _came_ to me" or "it _dawned_ on me." That's because we often feel that ideas are revealed to us from an external source.

However, there's a school of thought called _design thinking_ which believes that ideas are generated by following a rigid mental process. In this way, creativity can essentially be manufactured. It's as though knowledge and expertise have nothing to do with it!

The most famous advocate of this approach is the design firm IDEO, headed by Stanford's Institute of Design founder, David Kelley.

The firm champions the idea that the best designers should work unencumbered by expert or specialist knowledge in a given field. It makes no difference if designers are creating a toothbrush, a tractor or a chair. They think the same _design process_ can be applied in every circumstance and should work wonders every time.

But this approach is clearly nonsense. Creativity doesn't work like this. The author sampled his friends and colleagues and asked them what they thought about the creative process.

He found that creative ideas were stirred by immersion into and sensitivity toward the field in question.

Immersion is, in essence, an empathic dive into another world. Receptivity and openness, often understood by taking on board relevant stories and anecdotes, were found to be essential tools in getting a proper hold of the context needed to create solutions.

You've got to be open to ideas, the author's friends said. Creative ideas rained down on them when they least expected it.

That's how Henry Ford created his famous car-building factory line. It was his observation of how pigs were processed in slaughterhouses, piece by piece, which went on to trigger his creative impulse in the automotive industry.

### 8. Sensemaking requires cultural interpretation. 

At the US Naval Academy, students aren't just trained to navigate using GPS satellite technology. They're also taught how to plot a course using the night sky. For good reason. The best navigators should be able to interpret information from a variety of available sources — not just technology.

Sensemaking works like this too. It encourages us to interpret the world through social intuition, active listening and cultural context.

FBI agent Chris Voss used just such a method to free the kidnapped American journalist Jill Carroll in 2006. He and his team navigated the crisis by using sensemaking, specifically by interpreting Arabic culture.

On January 7, 2006, Jill Carroll was kidnapped in Baghdad. A few days later a video of her and her captors was shown on Al Jazeera. As was normal practice, the FBI summoned up a team of skilled negotiators. Among them was Chris Voss. They set about developing an appreciation of the cultural matters that were of most importance to the Iraqi insurgents.

In the first instance, they recognized the importance of family and family honor in Middle Eastern cultures. So they filmed Carroll's father addressing the kidnappers directly.

They also noticed a detail in the kidnapper's video. They had left Carroll's head uncovered. This was unusual, as it's a sign of disrespect in Arabic culture. Consequently, Voss's team used this point to smear the captors' reputation in the Iraqi media.

In the next ransom video, the kidnappers put Carroll in full Islamic dress. The FBI's tactic worked because it exposed the kidnappers' weakness. Soon, the kidnappers were just trying to work out how to let her go without losing face as they were sensitive about being criticized for disrespecting Arabic culture.

On 30 March, Jill Carroll was released. All it took was a bit of effort navigating through Middle Eastern culture.

There is an art, you see, to navigation.

### 9. Final summary 

The key message in this book:

**_Sensemaking_** **is about understanding human culture and the context in which it operates. In contrast with the data-driven way the natural sciences encourage us to interpret the world, sensemaking is rooted in the humanities and acknowledges the richness of human culture, of people's stories, art, philosophy, and history.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Algorithms to Live By_** **by Brian Christian & Tom Griffiths**

_Algorithms to Live By_ (2016) is a practical and useful guide that shows how algorithms have much more to do with day-to-day life than you might think. And not just that; they can also lead to a better life by helping you solve problems, make decisions and get more things done.
---

### Christian Madsbjerg

Christian Madsbjerg is the founder of the strategy consultants ReD Associate, which embraces anthropology, sociology, art history and philosophy. Madsbjerg himself studied philosophy and political science in Copenhagen and London.

