---
id: 54a98e7f6233320009670000
slug: setting-the-table-en
published_date: 2015-01-08T00:00:00.000+00:00
author: Danny Meyer
title: Setting the Table
subtitle: The Transforming Power of Hospitality in Business
main_color: 3287BD
text_color: 2B75A3
---

# Setting the Table

_The Transforming Power of Hospitality in Business_

**Danny Meyer**

In _Setting the Table_, famous restaurateur Danny Meyer explains how to develop a great restaurant. Drawing on his own experiences of working his way to the top of the industry, he outlines the sheer power of great hospitality and the wondrous success it can bring.

---
### 1. What’s in it for me? Learn how one man turned a passion for food into a successful business. 

Fast-paced, star-studded, surrounded by great food and wine — who wouldn't want to be the next greatest restaurateur?

Seen often as a romantic pursuit, many people jump into the restaurant business, only to fail. It's just not enough to love food; you have to have business chops to make it in this competitive industry.

Danny Meyer, one of the most successful restaurateurs in the United States, not only had a great passion for great food but also had the skills to ensure success. He understood from the start that successful restaurants can't just dole out delicious dishes; market and service savvy are crucial.

Want to learn how you too can build a successful restaurant empire just like Danny Meyer? Read on!

In the following blinks, you'll discover

  * why you shouldn't give your best staff special treatment;

  * why you should Google your restaurant guests before they arrive; and

  * how a maitre d' sneaked into a customer's home and saved the Champagne.

### 2. Danny Meyer combined his passions for fine dining and hospitality by opening his own restaurant. 

Everyone loves great food. Many people love dining out. But not everyone ends up opening their own restaurant. That takes a lot more enthusiasm, as the author's story illustrates.

Danny Meyer grew up with a passion for international food. Meyer's father had a deal with the airline _Lineas Aereas de Nicaragua_ that allowed the family to fly around the world inexpensively. So Meyer from a young age travelled and learned to love all the different foods he tasted.

Many of the family's journeys were to Europe, where Meyer was amazed by not only the cuisine but also by the culture of hospitality that he noticed was largely missing back home.

As a child, sometimes his love of food went too far. He often snuck food into his bedroom (with the help of his housekeeper!) and ate secretly. Naturally, he suffered from health problems as a result.

Yet even though Meyer knew food was his passion and he wanted to somehow make a business out of it, he wasn't sure what his first step should be. He'd been to college, and both he and his parents were skeptical about the idea of him working as a restaurateur, thinking such a profession might be "beneath" him.

Meyer eventually decided to create a restaurant which would serve only the very best cuisine and provide the excellent service he'd seen as a child in Europe.

That's how he opened the _Union Square Cafe_.

The author's dream came true when the cafe was launched, but the business still needed time to grow, and Meyer himself needed more time to develop further restaurants and eventually his own business model.

### 3. Stay authentic when it comes to cuisine, but also adapt your menu to local tastes. 

When you open a restaurant, you might decide on Italian food because of your heritage, or French food simply because you like it. Offering authentic food, however, isn't enough on it's own.

Meyer was also interested in Italian and French food (and others, of course), but he didn't want to just recreate what he'd seen in Europe. He wanted to adapt his food to his customers.

This was his secret to success. Meyer didn't just offer foreign food in New York, he created something new. His restaurant _Tabla_, which serves Indian food (also located in New York), illustrates this well.

The upstairs of Tabla serves fine dining that's more American in style, but gently flavored with Indian spices. The downstairs serves bolder Indian dishes that are still tailored to an American palate. There aren't any rules about fusing foreign and local food, really, as long as you remain true to both.

Designing a new restaurant is all about finding the right balance between _content_ and _context_. Your content is the food you sell and the clients you attract; the context is the business's location and space.

Tabla, for instance, needed to be classic and minimalist, with wooden floors and high-quality furnishings, but also Indian, with vibrant colors and textiles.

Meyer thought carefully about this, paying attention to the small details, such as the high-quality, handmade Indian fabrics he put on the restaurant's walls.

Creating a new restaurant that properly balances content and context isn't cheap, either. Meyer spent over $3 million on antiques for his second restaurant, _Gramercy Tavern_, to create the atmosphere of an East Coast-styled tavern. He could've used replicas, but they wouldn't have looked as authentic.

Creating something that's simultaneously authentic and new is a central challenge for building a new restaurant.

> _"We have fun taking service seriously."_

### 4. Making your customers feel valued and listened to can make your restaurant stand out. 

Good restaurants should pay attention to service, but fine dining _requires_ top-notch service.

For Meyer, fine dining means not just having the best chefs or classically trained waiters who know how to pour wine and serve guests in the correct order. Fine dining centers around an entirely different concept of hospitality.

Meyer calls this idea _enlightened hospitality_, which means a guest feels that things are happening _for him, not to him._

When you go to a restaurant, for example, it can feel as if the staff is just going through the motions. If you're given a free glass of Champagne for your birthday, it's not really a heartfelt gesture — it's just company policy.

Enlightened hospitality aims to avoid this. It's supposed to make a customer feel as if he's an individual being catered to and looked after. You have to know what your guests want and work hard to give it to them.

The better you know your guests, the better you can serve them. So strive to remember important details about your regular customers, like their habits, allergies or preferred tables.

Remember however, that first-time customers are just as important. Collect any bits of information you can about them, so they feel listened to and taken care of.

And if they make a reservation at your restaurant, prepare as much as you can in advance.

Seating, for example, is key to great hospitality. Put tables for two near the walls for more privacy, and bigger groups in the center. But ultimately listen to what your guests want, and seat them where they feel most comfortable.

Enlightened hospitality has been the key to Meyer's success, but for it to work properly, one thing is crucial: you need the right people working at your restaurants.

### 5. You have to hire and keep the highest-quality staff to maintain an excellent restaurant. 

A restaurant can be perfectly designed and serve delicious food, but if the staff is unfriendly or the kitchen too slow, it won't ever be considered great.

You have to find the perfect staff to make a restaurant excellent. Meyer follows a hiring strategy called the _51 percent solution_ for finding employees.

With the 51 percent solution, you give 49 percent weight to technical skills that can be improved. The remaining skills are emotional, such as _optimistic warmth,_ the feeling that the glass is half full, or _self-awareness with integrity,_ the understanding that an individual is responsible for her own actions.

These emotional skills are vital, and no job candidate should pass muster without them. Overall, such skills are actually _more important_ than technical skills, which can be taught.

Once you've found the best staff, you also have to take care of them. Everything you give them — from training to promotion opportunities to income — has to be far above standard.

One of the worst dangers your restaurant can face is having an average team. This would mean losing your best staff to other employers and having to fire the worst, leaving you with the mediocre middle.

You can avoid this by making sure all your employees feel valued. Empower them — give them some control over parts of the business, like the menu, the restaurant's design or recruitment.

You still need to lead, however. Offer them clear instructions and constructive feedback, so they know what's expected of them and how close they are to achieving it.

Excellent employees are vital to an excellent restaurant, but even after you've found and kept the highest-quality staff, there's another important element: working well together.

> _"The only way a company can grow, stay true to its soul, and remain consistently successful is to attract, hire and keep great people."_

### 6. A service team built on mutual respect and accountability is vital for a successful restaurant. 

As a restaurant owner, you're only as good as your team. And importantly, your team has to be founded on and work with a sense of mutual respect.

No one should think they're the star performer. Meyer learned this the hard way when he encouraged employees to submit an essay describing the perfect waiter, as part of a national challenge. One of his employees won and was named "waitress of the year."

After this, however, she expected to be treated "better" than her colleagues. She also attracted a lot of media attention, and the team's group dynamic was damaged as a result.

In the end, Meyer's idea to encourage his team to work together essentially backfired.

Be careful when handing out promotions, too. People often get promoted just because they have great technical skills, even if they lack leadership skills.

Promote people instead based on how well they work with the team, not on how well they cook or serve.

Also, remember that even the perfect team still makes mistakes. Your job as their leader is to constantly correct them in a fashion that encourages learning and improving.

Mistakes happen sometimes when employees don't agree or are unclear on the business's values. As a leader, you have to ensure that the restaurant's goals are clearly communicated. And importantly, anyone who doesn't honor them is held responsible.

This happened at Tabla when a chef declared he wouldn't serve a party larger than eight people. One time Fern Mallis, the head of the Council of Fashion Designers of America at the time, showed up with nine guests; her party was turned away.

Afterward, the staff discussed this obvious mistake and how it had gone against the restaurant's goal of great hospitality. The chef was held accountable, and as a result became more flexible.

When correcting mistakes, also be careful to never treat your staff badly. Remember the golden rule in life (and business!): treat others as you'd like to be treated.

### 7. As a restaurateur, you’re not just serving food, you’re serving a community. 

No restaurant operates or is located in a vacuum. You should make sure to adapt your ideas and your business to your neighborhood; but to be really successful, you have to go even further.

A restaurant can only have lasting success if it becomes _part_ of the neighborhood. Make your business fit in with the local community, no matter what cuisine you serve.

Ultimately, your restaurant should become a _neighborhood restaurant_.

Giving it a name that connects it locally is one good idea. Meyer chose to do this with some of his establishments: Union Square Cafe, for example, has become _the_ cafe for Union Square, and the Gramercy Tavern is close to Gramercy Park in New York.

Your restaurant has to stand for the community, as do your suppliers, so hire local suppliers whenever possible. When you have to source overseas, make sure your partners still have connections to the area, too.

And since Meyer chose initially to serve Evian water from France, he ensured that this foreign company gave back to the local community too, supporting his efforts to feed the homeless. When he switched to Fiji water, however, he made sure that this company supported local programs as well.

Restaurants also need to give back to the neighborhood. Meyer located some of his restaurants in places that at the time were economically struggling or unfashionable, such as Union Square in the 1980s or Madison Square Park in the 1990s.

In both locations, he invested in the community. He helped support a farmers' market in Union Square, for instance, as well as supported efforts to make Madison Square Park safer for visitors.

Importantly, when you give to your community, the community gives back. Meyer's restaurants are now landmarks in their respective neighborhoods, because of the intimate connection they have with the local environment.

### 8. Dealing with the press is a necessary evil, but word-of-mouth marketing is the best you can have. 

It's important that your business stands for the right values and is seen in a positive light. But how exactly can you ensure this?

First, your restaurant needs to maintain a solid reputation, and one way to ensure this is to influence your media coverage.

Dealing with the press, granted, is like crossing the ocean on a hungry shark. There's a good chance you'll be eaten, but you'll drown if you don't try! You can't be successful without good press.

For the restaurateur, the individual that holds the most power is the restaurant critic.

Yet don't be too frightened of critics, however. They only see a snapshot of your business, and they often visit when your restaurant is still in its early stages of growth. Don't give up or change your whole concept because of one bad review.

In addition to press coverage, what your guests say about your restaurant helps to bolster or damage your reputation. It's vital to influence the stories that your guests tell.

Because of this, make sure you go the extra mile for every guest and give them service they'll want to tell others about.

For example, a couple once went to one of Meyer's restaurants to celebrate their anniversary. Before arriving at the restaurant, they had put a bottle of Champagne in their freezer to cool. They told the maitre d' that they had done this, and he told them, albeit politely, that this was a mistake!

The husband immediately wanted to go home to rescue the bottle before it exploded, but the maitre d' told them to sit down and enjoy their anniversary dinner. He then got their keys, went to their house and saved the bottle — and even left a free dessert with greetings in their refrigerator.

Naturally, the guests were thrilled, and never stopped telling their friends about the restaurant's above-and-beyond service. Stories like this are your most important advertisements, but they can also be your greatest threats — so be careful with what stories you create.

### 9. Long-term success comes by giving in abundance and taking care of your base. 

You might enjoy short-term success if you offer a dining experience that is new or creative, but long-term, sustainable success is much harder to achieve.

The first rule for lasting success is to give first, and give in abundance.

Even if giving costs you in the beginning, you'll get more back in the long run. During Restaurant Week in New York, for example, people can enjoy a three-course lunch for around $20 ($19.91 in 1991 and $20.14 in 2014).

Most participating restaurants limit their menu, offering only inexpensive items. Restaurants owned by Meyer do the opposite: they offer a wide selection of dishes worth much more than just $20.

Guests even receive a gift certificate that's worth the same amount they paid for lunch! And because they had such a great time, they typically share their experience with friends and bring them back to the restaurant. This kind of generosity is a great way to earn regular, loyal customers.

The second rule is to make sure you take care of the foundation of your business. Flip the traditional workplace hierarchy upside down: concentrate on employees and guests _before_ the managers and investors at the top.

Your first priority? It's your staff. Keep them happy, so they can keep your guests happy. After this, you can then move on to strengthening your ties to the community and your suppliers; and at the end, you make sure your investors are satisfied.

You can only achieve long-term success if your restaurant maintains this strong foundation. With a base of enlightened hospitality, you'll be able then to expand your restaurant empire!

### 10. Timing (and context) is everything when it comes to expanding your restaurant business. 

After you've established a successful restaurant business, your most difficult decision will be whether to expand.

For Meyer this was a dilemma, as he knew that his father's business failed when it expanded too rapidly. Eventually, however, he came to realize that expansion is good, as long as you do it correctly.

Don't concern yourself with expansion in the first two or so years of your restaurant's life. In that time, focus on perfecting your business, learning about the neighborhood and building your network.

When you're ready, make sure your staff is engaged and aware of your decision.

Remember that your view as the owner is subjective, and your staff's opinions are quite valuable. If you've trained them well, they should share your values, so you should trust their input.

When you finally feel ready to expand, be sure to proceed carefully. Don't rush, even if unexpected opportunities arise.

If opportunity knocks, however, ask yourself whether you'd pursue it even if the opportunity came with no price tag attached. This will help you decide if you really should go for it, or not.

To drive this idea home, think about someone giving you a free pair of shoes. Just because they're free, doesn't mean you're going to wear them. If you'd turn down a gift, then you should reconsider the opportunity.

Meyer was once asked to open a restaurant at the Museum of Modern Art in New York, but turned it down, even though he was interested.

Some time later, after the museum was renovated, he was approached again to open a restaurant there. This second time, however, the conditions were better and Meyer felt he had the experience and support to make the restaurant succeed — so he went for it.

Meyer began expanding after running just one restaurant for several years. It enabled him to become not just a restaurateur, but the CEO of a restaurant company. Expansion can lead to great things if done strategically!

### 11. Final summary 

The key message in this book:

**You can't achieve long-lasting success in the fine dining restaurant industry without great hospitality skills. So strive for enlightened hospitality, which isn't just about how you treat your guests, but also about how you treat your employee team, support network, suppliers, investors and local neighborhood. With this solid base, you'll be able to grow and expand a business that benefits not only you and your customers, but also the community.**

Actionable advice:

**Build your foundation from the bottom up.**

The first people you attend to in your restaurant are your staff. If your staff is happy, they'll take care of your customers. Then you can ensure you have the support and trust of your suppliers and community, and once that's assured, you take care of your investors. Yet when you work to please your investors first, you won't build a sustainable business.

**Suggested further reading:** ** _Never Eat Alone_** **by Keith Ferrazzi**

In _Never Eat Alone_, Keith Ferrazzi, a successful businessman and marketing expert, takes us through the secrets to successful networking. He focuses on building lasting relationships rather than merely exchanging business cards, which seems to be many people's idea of networking today. He summarizes his findings in a system of tried and tested methods.
---

### Danny Meyer

Danny Meyer is the founder and co-owner of several highly rated restaurants, cafes and other food-related businesses around New York City. He's coauthored several cookbooks. _Setting the Table_ is his first bestselling book.

