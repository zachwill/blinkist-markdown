---
id: 55c7c0ff6664360007010000
slug: seventeen-contradictions-and-the-end-of-capitalism-en
published_date: 2015-08-10T00:00:00.000+00:00
author: David Harvey
title: Seventeen Contradictions and the End of Capitalism
subtitle: None
main_color: FFB257
text_color: 80592C
---

# Seventeen Contradictions and the End of Capitalism

_None_

**David Harvey**

_Seventeen Contradictions and the End of Capitalism_ (2014) reveals the inherent and sometimes dangerous contradictions of capital. Through explaining the inner workings of capital, the book explores why the economic engine of capitalism might stutter, and why it sometimes appears on the verge of collapse.

---
### 1. What’s in it for me? Explore the role of capitalism in modern-day political and economic crises. 

Most of us don't give it much thought in our everyday lives, but living in the modern world necessarily means being part of a capitalist society. But what does capitalism actually entail?

These blinks aim to provide an answer by exposing the workings of capital and its 17 inherent contradictions. They will take you on a journey through the economic engine of capitalism, revealing its vast implications for the global economy.

You will learn how some of the contradictions of capital become apparent only in the face of a crisis, and how others emerge over time due to political and technological change. Finally, and most alarmingly, discover how some contradictions of capital are actually dangerous, and can even push societies and economies to the point of collapse.

You will also learn

  * how a contradiction of capital made 4 million people lose their homes;

  * why theft and swindling lie at the heart of capitalism; and

  * why 70 percent of full-time workers hate going to work.

### 2. The value of something is always a matter of perspective. 

When you go to the supermarket, you exchange some of your money for some food. This is because food is useful to you in ways that money isn't: you can't eat money, but you can eat food.

In fact, everything we buy, just like food, has both a _use_ and an _exchange value_. Take housing, for example. Its use value is the shelter it provides; it's a place where you can build a life, and it can also function as a symbol of status or architectural significance.

But if you were to exchange your house for something else, how would you measure its worth? One way would be to calculate the cost of the raw materials, the workers' wages and payments for any services required to build it. In other words, the cost would be fixed based on what was paid to produce the house.

In much of the capitalist world, however, exchange value also depends on _speculation_. All sorts of things can influence a house's value to a prospective buyer, like location, prestige, past owners and so on — each of these factors can affect its exchange value.

Buyers and producers, both trying to profit in some way from any exchange, are mostly interested in exchange values, that is, the actual costs of producing something plus the profit that can be made from selling it. 

The discrepancy between use value and exchange value can give rise to full-blown crises in the market. Take the US housing market crisis from 2007 to 2009. Before the crisis, housing speculation soared. More and more people hoped to profit from these rising exchange values, and thus borrowed more in order to buy into "safe" housing.

Eventually, the pursuit of exchange value destroyed access to housing as a use value. All of the speculation drove property values higher than large segments of the society could afford to pay.

This contradiction between properties' use and exchange value caused about 4 million people lose their homes due to foreclosure, as decreasing access to housing caused exchange values to crash.

### 3. Money itself is contradictory, as is the relationship between private property rights and state power. 

In order to exchange commodities such as food and houses, we need a measure of their worth relative to one another. This measure is money — it represents what we value.

But the gap between money and the value it represents constitutes a contradiction.

Money is a means by which we can capture the immateriality of socially or personally valuable things, like labor. Gold, silver and paper money were originally used to give immaterial labor a physical value. For instance, five hours of roofing might have been worth five pieces of silver. Today, however, these representations of value aren't even physical, they're merely numbers in digitized bank accounts.

We're left with a weird situation, whereby our account balances are representations of representations of social labor: the balance represents the paper money, which represents the labor.

Moreover, when money becomes a mere numerical abstraction, its potential quantity is limitless. Labor, in contrast, has limits: population, natural resources, production capability and so on.

The true contradiction, however, is that money is itself a commodity. Money, which is a measure of social value, can also be bought and sold. The same can't be said for other types of measurements, like kilos or inches.

Money is a means through which we affirm our right to own a commodity. But sometimes there are other actors at play than just the market — sometimes you have to deal with the state.

Thus, another contradiction lies between individual private property rights and state power. In capitalist societies, the state is supposed to protect people's private property rights.

However, in the name of public interest, the capitalist state can deploy its power to curb private property rights through regulations, taxation and sometimes even dispossession. Consider, for instance, a landowner being forced to sell her land so that a new road can be build for the public benefit.

> _"The abandonment of the metallic base of the world's money supply...created a whole new world of possible contradictions."_

### 4. Dispossession lies at the heart of capitalism. 

There are basically two ways of accumulating wealth: there's the legal way of exchange within markets, or there's the illegal way of robbery, theft, swindling extortion and graft. The latter doesn't involve exchange, but rather _dispossession,_ the nonconsensual seizure of a person's property.

Though we think of it as morally abhorrent, dispossession seems a foundational aspect of the creation of private capital.

Consider, for example, the "enclosure movement" in Britain during the 18th century. During this period, common lands that belonged to local communities were transformed into private properties in order to enable private investments for "greater productivity."

This privatization of previously common lands was nothing less than the dispossession of the local population from their property.

Or think of the present-day "land grabs" throughout Africa, Latin America and much of Asia, through which powerful political or economic agents, such as foreign governments and transnational corporations, illegally acquire huge swaths of land. This form of dispossession has been around for generations, and never really went away.

Another type of dispossession can be found in the contradictory relationship between labor and capital.

Capital, in terms of monetary profit, is created only when workers produce more in commodity values than they are paid in wages. In a very real sense, laborers are thus dispossessed from the real value of their labor.

This contradiction becomes readily apparent when examining the continuous conflict between these two forces:

Workers are constantly fighting for higher wages and better working conditions. The more they succeed, the higher their standard of living.

Capitalists, in contrast, fight to cut wages while increasing productivity and the intensity of work. The more they succeed in the struggle against labor, the greater their profits.

### 5. Capital is worth little without buyers, as are workers if they have no buying power. 

The capitalist starts each day with a certain amount of capital, which she uses to hire workers and purchase the means of production. The workers, in turn, collectively produce a new commodity that can be sold for a profit — capital thus returns to its monetary form at the end of the day.

Yet, there is an inherent contradiction within this circulation of capital. To generate profits, capital needs to flow. However, the transition from money to commodities runs into potential barriers.

Imagine you have money and you want to make steel. To start producing it, you'll need all the ingredients necessary for its production. But the iron ore is buried deep in the ground, and it takes a long time to dig it out. In the meantime, no value is actually produced.

Furthermore, the transition from commodity value into monetary value likewise has potential problems.

For example, if you own a container port terminal, you can realize its monetary value by charging fees for its use. But if no container ships arrive, the terminal's commodity value can't be transformed into money.

So the circulation of capital is contradictory, in that the creation of profits is hindered by natural barriers and delays.

Assuming capitalists can overcome these barriers, their goods still have to be purchased on the market by real people. This leads to our next contradiction.

In the production process, value is created through labor. But as with the flow of capital, the transformation of this value into money is complicated.

The value of work remains latent until it is realized through a sale in the market, and laborers themselves play a key role as buyers in the market. Capitalists are thus perpetually caught in a conundrum: they can either minimize labor costs and thereby threaten workers' buying power, or they can augment workers' buying power at the cost of potential profits through efficient production.

### 6. Technological change and the division of labor demotivate workers. 

Up to this point, all the contradictions of capital we've examined have been _foundational contradictions_. They are inherent features of capital, unaltered by changes in politics or technology. We will now investigate the _moving contradictions_ of capital, the ones that are unstable and change over time.

The relationship between technological change and the future of work constitutes one such contradiction. We've seen that social labor is a source of value, which allows capital to generate profits.

But today, labor is becoming increasingly displaced due to automation and robotization. Machines increase productivity and are able to create greater outputs, and thus faster profits, than humans could ever achieve. Thus, human labor becomes less necessary for production.

The effects of this displacement matter. As larger and larger segments of the world's population become "redundant" from the standpoint of capital, we will have a harder time thriving, both materially and psychologically. The increase in output due to machines and automation leads to a decrease in demand, as jobs, and thus incomes, disappear.

And while automation makes human labor redundant, division of labor itself makes the worker _feel_ redundant. The _division of labor_ refers to the separation of complex productive activities into specific but simple tasks. The different stations on a factory floor or even the division of departments at your job are examples of the division of labor in action.

Capital relies on both the division of labor and a motivated workforce to produce effectively. However, increasing complexity in the division of labor leaves little possibility for personal development on the part of the laborer. Any semblance of ownership a worker may have over their part in the production cycle evaporates, as does their motivation to work for capital.

Herein lies the contradiction: while division of labor helps make the organization of production processes more efficient, it makes the workers themselves less motivated, and thus less efficient.

### 7. In the pursuit of monopoly, capitalists end up devaluing the regions where they are located. 

Capitalism stresses the importance of competition, and committed capitalists claim that the monopoly power of companies like Google, Microsoft and Amazon wouldn't occur if a perfectly competitive market could be created.

But while monopoly undermines competition, it is also at the heart of capitalism. Consider, for example, that higher transportation costs for non-local competitors have long served as a form of protection for local enterprises to maintain local monopolies.

But with the advent of global, fast and cheap transportation, many local industries have lost their foothold in their respective local markets, as they have been forced into global competition.

You'd think that capitalists would welcome such competition; yet, if given the choice, most capitalists would prefer to be monopolists, and thus find other ways to construct and preserve monopolies. For instance, they can aggregate capital into mega-corporations or set up strategic alliances within the industry to dominate the markets.

Furthermore, to gain a competitive advantage, companies choose to disperse certain aspects of their business in a way that minimizes their costs of production. This, in turn, causes certain geographic regions to appreciate or depreciate accordingly.

In order to position their operations most effectively, companies often cluster together regionally. For example, car parts and tire manufacturers will locate themselves close to car plants in order to minimize the cost of transportation.

This clustering increases the region's value, thus attracting even more capital and labor. At the same time, this appreciation leads to increases in local costs due to the region's higher perceived value. Capitalists, always looking to save a buck, thus start looking for other, cheaper spaces to ply their trade.

The ensuing relocations generate crises of devaluation, in which regions become caught in a downward spiral of depression and decay. Thus, the value that the influx of capital creates in a given region is effectively destroyed once the cheap production opportunities for capitalists disappear.

> _"Capital is, we can conclude, in love with monopoly."_

### 8. Capital is looking for an impossible formula: cheap labor that is skilled, educated and reproduces itself. 

During the Industrial Age, Karl Marx recognized that limits had to be placed upon the crushing length of the working day and enormous exploitation of the working class — not only to protect the lives of the laborers, but also to protect the reproduction of capital.

It wouldn't be unreasonable to say that, during the Industrial Age, capital didn't care about the needs of workers. Capitalists provided workers with pitifully meager wages that were supposed to provide for them both materially and psychologically.

But it became clear that if workers couldn't cover their living expenses, were overworked to a premature death and, therefore, couldn't reproduce, capital's access to an efficient labor force would diminish. No workforce means no profits from labor, which means that capital can't reproduce itself.

Capital's unwillingness to ensure the social reproduction of labor, while at the same time being dependent upon it, is once again contradictory.

Today, laborers in many parts of the world are paid the wages needed to maintain a decent standard of living, meaning both they and their posterity can be exploited for their labor in order to generate profits for capitalists.

Still, capital is seldom willing to bear the costs of social reproduction. In social democracies, capital has to contribute to at least some of the costs of social reproduction through, for example, pension plans, insurance and healthcare in the form of wage provisions or taxation that supports the state's social welfare.

Nevertheless, capital has always wanted to cut costs, either in the form of production efficiency or by shirking its responsibility to the general population.

Yet, over time, capital has become more interested in an educated workforce, as profitability has depended on increasingly skilled labor. But the actual costs of acquiring skills, particularly through education, have been successfully passed on to households, kindergartens and public schools.

### 9. Capital creates its own crises by concentrating wealth. 

Karl Marx said that the true realm of freedom begins when the necessity of work is left behind. In a capitalist society, this freedom is thought of as financial freedom, something only achieved by a small minority.

In fact, capital itself creates the disparity between those who can achieve financial freedom and those who can't.

Let's look at New York City for a moment, a city with an extremely high cost of living. There, the average income of the top one percent of earners in 2012 was $3.47 million. Half of the population, in contrast, are scraping by on $30,000 per year or less.

Income disparities are nowhere as dramatic on the national scale, but they have been increasing markedly since the 1970s. In fact, by 2005, 84 percent of wealth in the United States was held by the top 20 percent.

Such disparities constitute a contradiction of capital, as historical evidence suggests that gross inequalities might be a harbinger of a looming economic crisis. This is because the contradiction between production and realization — that is, that low wages undermine workers' buying power — becomes much harder to keep in balance. The last time the United States experienced these levels of inequality was in the 1920s, triggering the Great Depression of the 1930s.

What's more, the resulting disparity of wealth leads to friction between freedom of capital and freedom of others.

In the face of great financial disparity, true freedom becomes available only for those with substantial incomes. The poor, lacking an income that frees them from want, are locked out of the realm of true freedom, and will be ruled by capital as long as they are struggling to earn a living.

The freedom of capital thus rests on the lack of freedom of others.

> _"Capital in effect has been deepening income inequalities and poverty in order to sustain itself."_

### 10. Continuous compound growth is impossible in a finite world with finite resources. 

We've seen the foundational contradictions along with the moving contradictions of capital, but there are worse contradictions still. These are the _dangerous contradictions_ of capital, the ones that threaten economic, environmental and social equilibrium.

We'll start with the concept of compound growth, which is when money grows faster with each passing year.

Compounding is simple. Imagine that you've used $100 to open a savings account that pays five percent annual interest. At the end of the year, you have $105. After two years you have $110.25.

The difference in growth between the first and second years isn't that great. In fact, the difference is only 0.25 percent, five percent of five percent of 100, or 25 cents.

But after a hundred years your $100 will have increased to $13,150! In total, your savings account will have grown by a whopping 12,550 percent!

This is what is meant by compound growth, and this is what capitalists, through their investments of capital, are trying to achieve. But is perpetual compounding growth really possible?

In short, no. In fact, it represents a dangerous contradiction.

You learned earlier that money represents social value. Increasing quantities of money must therefore represent increasing social value, such as in the form of new roads or bridges.

Capital also tries to find new values and markets by coming up with "human capital," such as human talent, skills and experience, or "nature capital" such as rivers or forests. 

However, as money accumulates faster with every passing year, finding corresponding social value will eventually become impossible. If no corresponding value can be found, monetary value becomes illusory, which can lead to economic disaster.

In this way, capital's desire for compounding growth, in combination with the actual social value that money represents, makes for a dangerous contradiction.

### 11. Despite needing both workers and natural resources, capital’s pursuit of profit destroys both. 

We've seen many contradictions of capital up to this point, all varying in their severity. The final two pose a threat to our very existence.

First, capital has a damaging impact on the environment. Goods require resources for production, such as oil or wood, which capital regards as nothing more than a vast store of potential use values that can be monetized and exchanged as commodities.

But capital's hunger for profits undermines nature's ability to regenerate the resources used for production.

Under the pressure of continued compound growth, the dangerous degradation of ecosystems will accelerate, likely resulting in environmental disaster. At the same time, capital destroys the basis for creating profits from natural resources by destroying those very resources. In its pursuit of profit, capital threatens the lifeblood of the natural world.

Second, capital evokes the revolt of human nature.

We've already seen how capital can alienate the worker from the social meaning of their work. Indeed, many people are not happy with what they do for a living. In fact, a recent Gallup survey in the United States showed that about 70 percent of full-time workers either hated going to work or had mentally checked out and become, in effect, saboteurs — spreading discontent and thereby costing their employer dearly in the form of lost efficiency.

This alienation from work will eventually evoke a revolt of human nature against its domination by capital. Human beings simply can't suffer through a world devoid of meaning. Consider as a recent example the Occupy movement in the United States, which protested against social and economic inequality. Add to that the countless anticapitalist uprisings that occurred throughout the 20th century.

This revolt of human nature contradicts capital's demand for a loyal and thus efficient workforce — a dangerous contradiction that threatens our social cohesion.

We're left wondering: can capital survive these contradictions? In any case, it's clear that something needs to be done to change the way we live and produce.

### 12. Final summary 

The key message in this book:

**The workings of capital are inherently contradictory. Some of these contradictions are latent, and become apparent only in the face of a crisis, while others change with the times. The most serious contradictions are outright dangerous and threaten the foundations of society and economy, as well as the natural world.**

**Suggested further reading:** ** _23_** **_Things_** **_They_** **_Don't_** **_Tell_** **_You_** **_About_** **_Capitalism_** **by Ha-Joon Chang**

In _23_ _Things_ _They_ _Don't_ _Tell_ _You_ _About_ _Capitalism_ Ha-Joon Chang destroys the biggest myths of our current economic approach. He explains how, despite what most economists believe, there are many things wrong with free market capitalism. As well as explaining the problems, Chang also offers possible solutions which could help us build a better, fairer world.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### David Harvey

David Harvey is Distinguished Professor of Anthropology at the City University of New York Graduate School, where he and his students developed a course on Karl Marx's _Capital_ that has been downloaded by more than 2 million people worldwide. He has also written a variety of books on capitalism, including _The Enigma of Capital_.

