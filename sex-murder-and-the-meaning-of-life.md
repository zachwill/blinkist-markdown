---
id: 55f6e94029161a00090000fb
slug: sex-murder-and-the-meaning-of-life-en
published_date: 2015-09-17T00:00:00.000+00:00
author: Douglas T. Kenrick
title: Sex, Murder and the Meaning of Life
subtitle: A Psychologist Investigates How Evolution, Cognition, and Complexity Are Revolutionizing Our View of Human Nature
main_color: F63D31
text_color: C23027
---

# Sex, Murder and the Meaning of Life

_A Psychologist Investigates How Evolution, Cognition, and Complexity Are Revolutionizing Our View of Human Nature_

**Douglas T. Kenrick**

_Sex, Murder and the Meaning of Life_ (2011) looks at the many ways in which our evolutionary survival and reproductive instincts influence our behavior in the modern world. From conspicuous consumption to cold-blooded murder, it often seems that humans will do just about anything to survive and reproduce, and these blinks takes a closer look at what drives these profound desires.

---
### 1. What’s in it for me? Discover how our reproductive drive makes us feel homicidal and prejudiced. 

All of us have probably displayed some seriously strange behavior when attracted to another person. Why is that? Well, although many of us know about evolution and humans' desire to find suitable mates, this powerful drive actually affects our lives much more than we could imagine.

A few decades ago, most social psychologists focused on how humans' thoughts and behavior were influenced by changes in their current situation. These same social psychologists rejected the idea that humans share innate traits as causes of certain behavior — but this view has changed over time. There are indeed consistent traits to be found that link the behavior of modern man to evolutionary instincts such as survival and reproduction.

In these blinks, you will delve deep into the human reproductive psyche and explore some not-too-flattering aspects of what this powerful force makes us humans think and do.

In these blinks, you'll discover

  * why 76 percent of students polled at Arizona State had homicidal fantasies;

  * how reproduction and survival determine what we remember; and

  * why an average-looking man with a Rolex is more attractive than a handsome man without one.

### 2. Preference for beauty and social dominance is linked to our reproductive instincts. 

Have you ever found yourself gazing at an attractive person across the bar — even though you had a partner waiting for you at home? While this kind of ogling is more or less inevitable, it still has clear consequences.

For starters, your level of commitment to your partner is likely to drop when exposed to attractive or socially dominant people. This was demonstrated in an experiment conducted by the author and his colleagues, in which men and women were exposed to attractive members of the opposite sex. Afterwards, participants were asked to rate their level of commitment to their partners.

The experiment revealed that men undervalued their commitment to their partner after seeing beautiful women. Women, however, tended to undervalue their commitment to their partners after being shown a series of socially dominant men.

If you want to nurture your loving relationship, then it's certainly not in your best interests to seek out beautiful or socially dominant people.

Interestingly, the same wisdom applies if you are actively seeking a partner.

To illustrate this point, the author shares an anecdote from his time at university. During his studies, he and one of his friends were at odds over the number of beautiful women on campus. The author believed that there were beautiful women everywhere; his friend disagreed since, despite being an average-looking guy, he couldn't seem to get any dates.

The author finally understood why they didn't see eye-to-eye after seeing his friend's bedroom: the walls were lined with Playboy centerfolds. Being surrounding by impossibly beautiful women made his friend unable to recognize other people's beauty.

So why do we show such deference to beautiful and socially dominant people? In essence, we perceive them as the best mating partners.

Our Stone Age ancestors always sought out the most beautiful women or most dominant men when it was time to find partners with whom they could mate and reproduce. Though we've come a long way in our modern society, this primal drive hasn't left us.

### 3. Men’s tendency to commit more homicides is tied to their reproductive instincts. 

Have you ever been so full of darkness and anger that you actually thought or fantasized about killing someone? If you have, don't panic — you're not alone.

Most of us have these murderous fantasies at some point or another. Indeed, a survey of 760 students at Arizona State University revealed that 76 percent of men and 62 percent of women had had homicidal fantasies.

And yet, despite the fact that both men and women fantasize about killing, men account for approximately 90 percent of homicides committed annually in the United States. 

How can this be? One possible explanation is that men engage in violent behavior in order to impress potential mating partners. As we learned before, social dominance, or status, is critical to attracting women. This means that if a man is publicly insulted, no matter how trivial the situation, his ability to attract women ostensibly declines, thus threatening his opportunities for reproduction. Violence is a way of recovering this status.

This can additionally be linked to two fundamental principles of evolutionary biology, namely _differential parental investment_ and _sexual selection._ Taken together, these principles constitute the evolutionary explanation for why men engage in violent and murderous behavior: they want to win over women. 

_Differential parental investment_ means that one sex, usually women, invest more in offspring than the other. Thus, they are much more picky about who they chose as a partner. As a result, their potential mating partners, usually men, have to compete more aggressively for a chance to reproduce.

This competition is called _sexual selection_. When competing for opportunities to reproduce, there are no rules. Sometimes the easiest way to succeed is to eliminate the competition by fighting, disabling or even killing the competitors.

### 4. Prejudice springs from our evolutionary survival instincts. 

Are you 100 percent unprejudiced? In other words, can you honestly say that thoughts like, "people in fancy sports cars are overcompensating for something" or "short people always feel like they have to assert themselves" have never occurred to you?

We all have prejudices that are tied to our evolutionary survival instincts. For example, we are often prejudiced against people from outside our social group.

Consider one study conducted by the author and his research team at Arizona State University: in the experiment, white and hispanic subjects were presented with pictures of white and black men with either neutral or angry facial expressions.

When presented with the neutral faces, participants remembered the white men better than black men. This is due to the concept of _outgroup homogeneity_, in which members of one group find it difficult to distinguish between members of another group different from their own. In this case, black men (the smaller demographic) were harder to distinguish for white and hispanic participants.

However, when presented with the angry faces, subjects remembered the _black_ faces better. In this case, the concept of outgroup homogeneity is reversed: when facing a threat, you'll remember an outgroup member better than a member from your own group.

From an evolutionary historical perspective, this tendency could be what drives prejudice against foreigners.

Our ancestors, for instance, had to be extremely careful about diseases. They couldn't just open their medicine cabinet or obtain antibiotics when they contracted a nasty illness. They had to be exceedingly cautious about who they let into their territory, since anyone could be carrying a disease!

While people are generally less vulnerable to diseases today, our evolutionary perspective still has a bearing on our behavior.

Consider this anecdote about a group of chronically ill Canadian students who were asked how they felt about letting certain groups of immigrants into the country. When the immigrants were from countries such as Peru or Sri Lanka, the students displayed more xenophobic attitudes than when the immigrants were from more familiar countries in Europe.

### 5. The memories that stick in our long-term memory are usually related to survival and reproduction. 

Here's a tough question: was the last person you interacted with wearing a blue shirt? Don't worry if you don't remember; it's really not that important.

Day in and day out, we're presented with enormous amounts of social information. As you go to work one morning, you might meet hundreds of people on your way, all of whom look, smell, sound and act differently.

Then, once you get to work, you interact with coworkers all day, who likewise bombard you with sensory information.

But do you remember it all? Of course not. In fact, most of this social information is forgotten immediately. Only a small fraction of this massive amount of information actually stays in our memory.

As a counterexample, consider the terrorist attacks on the World Trade Center in New York City. You probably remember the exact moment you heard about it or saw the dramatic images on TV. You might even remember the emotions and thoughts that went through your head, who you were with and so on — enough to paint a vivid picture of the experience.

Or, think about more pleasant memories, like a first kiss with someone you really liked, or a marriage proposal. Often, these memories are quite vivid.

So why do we remember some things so vividly and others hardly at all? Well, the brain is very selective about what it allows to be stored in our long-term memory. Usually, this information is related to our survival and reproduction.

The brain tries its best to allocate resources in a way that benefits our survival and reproduction. So, whenever social information relates to our survival (such as when we feel threatened by something) or to our reproduction (like when someone is attracted to us), that information sticks in our long-term memory. No wonder seemingly unrelated things such as 9/11 and your first kiss are permanently etched into your long-term memory!

### 6. Conspicuous consumption is tied to our reproductive desire. 

Every day, people choose to purchase extravagant, expensive stuff instead of the ordinary and affordable things that would do the job just as well. This trend has led many people to lament American consumer culture, seeing it as the source of modern society's social woes.

But they're wrong! Conspicuous consumption has always existed and would continue to exist even without capitalistic consumerism. Indeed, conspicuous consumption has been documented all over the world throughout various periods — it's certainly not a concept native to the United States.

For example, studies conducted by American economist and sociologist Thorstein Veblen, put forth in his book _Theory of the Leisure Class,_ revealed that conspicuous consumption was present in societies from Iceland to the Empire of Japan well before the rise of American consumerism. Even in the Trobriand Islands, part of Papua New Guinea, leaders gave away expensive jewelry just to show off their wealth.

So who, then, created this need for consumption? No one. Conspicuous consumption is simply a way for men to attract mating partners.

Consider this study conducted by researchers John Marshall Townsend and Gary Levy on sexual attraction and status symbols. They found that women were more sexually attracted to average-looking men wearing high-status clothing and accessories, such as a nice suit or an expensive Rolex watch, than to handsome men in low-status clothing, like a Burger King employee uniform.

Showing off is simply an evolutionary instinct. This is made even clearer by the fact that men engage in conspicuous consumption more than women.

We can again see this in a study conducted by the author, in which men were asked how they would spend $5,000 that someone suddenly gave them. Before answering, some of the men were encouraged to think about romantic situations, like a first date with someone they liked. These same men were willing to spend much more of the $5,000 on conspicuous purchases than the rest of the sample.

Money, much like violence, is just one of many tools people use to increase their chances of reproductive success.

### 7. Final summary 

The key message in this book:

**A great deal of our behavior is influenced by the lives of our ancient ancestors, who struggled tenaciously to survive and reproduce. While our lives are surely different in the modern world, the survival and reproductive instincts that guided our ancestors still play a key role in our lives today.**

**Suggested** **further** **reading:** ** _Our Inner Ape_** **by Frans de Waal**

Human beings are just as closely related to the gentle bonobos as they are to the aggressive chimpanzees. Frans de Waal compares the lifestyle of these two species of apes, in whose groups opposing characteristics such as sympathy and violence, fairness and greed, and dominance and community spirit clash with one another. Their sexual behavior tells us that we need to rethink the origins of our morality.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Douglas T. Kenrick

Douglas T. Kenrick is a professor of psychology at Arizona State University. His contributions to psychology and social research have been published in numerous academic journals, as well as in _The_ _New York Times_ and _Psychology Today._

