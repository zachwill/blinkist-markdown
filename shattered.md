---
id: 59fe4178b238e100067eaed3
slug: shattered-en
published_date: 2017-11-07T00:00:00.000+00:00
author: Jonathan Allen & Amie Parnes
title: Shattered
subtitle: Inside Hillary Clinton's Doomed Campaign
main_color: 3478AB
text_color: 2C6691
---

# Shattered

_Inside Hillary Clinton's Doomed Campaign_

**Jonathan Allen & Amie Parnes**

_Shattered_ (2017) takes you behind the scenes of the 2016 US presidential election campaign of Hillary Rodham Clinton. In these blinks, you'll learn what went wrong for Clinton and her team, leading a seasoned politician to come second to a reality TV star in a tumultuous and bitter race for the nation's highest office.

---
### 1. What’s in it for me? Uncover the inner workings of Hillary Clinton’s doomed 2016 presidential campaign. 

Unless you've been living off the grid since November 2016, you probably know that Hillary Clinton is not the current president of the United States. Before her election campaign got underway, many pundits and analysts believed she had both the experience and stature to win against most of her potential Republican challengers. But it was not to be.

From Bernie Sanders's unexpectedly serious challenge to her nomination to the email scandal that haunted her campaign before it even started, Clinton's failed bid to enter the White House was riddled with obstacles that she was, in the end, unable to overcome.

Although external factors, particularly latent misogyny, certainly played a role, these blinks seek to present a broader view of her failure, rather than, for example, simply blaming Russian hackers.

Indeed, some of the blame for this momentous loss inevitably lies with Clinton herself.

In these blinks, you'll learn

  * why Clinton's email scandal dogged her campaign up until the bitter end;

  * how infighting within Clinton's team diluted her campaign's message; and

  * that there is no single overriding reason that can explain Clinton's failure.

### 2. Hillary Clinton had good reasons to run for president, but could never put together the campaign she needed to win. 

In the lead-up to the 2016 presidential election, Hillary Clinton wasn't sure about throwing her hat into the ring. After all, she'd been there and done that in 2008, losing the Democratic Party nomination to Barack Obama. That being said, there were some factors leaning in her favor.

For starters, Obama still had a good approval rating, and Hillary's tenure as Secretary of State during his presidency had positioned her as one of the obvious choices to continue Obama's legacy. But beyond that, after the elections in 2008 and 2012, the Democrats thought they would have a lock on the 270 electoral college votes necessary to take the White House.

People were referring to it as the _Blue Wall_, a series of states that had voted Democrat since 1992 and would assure the Democratic candidate the votes needed to win. Not only that, but Democrats also had a broad network of congressional politicians across the country, many of whom supported the Clintons and could turn voters out to the polls.

So, with these factors in mind, on June 13, 2015, Clinton publicly announced her candidacy for president. However, right from the start, her campaign lacked the strong narrative and inspiring message voters tend to look for.

In the end, successful candidates simply must put forward a message that's both clear and compelling, one that tells voters precisely why they're running for office. By contrast, Hillary's first campaign speech was a mess; it was written by at least five different people, all of whom tried to shape it into something coherent. When it was finally delivered, the speech felt contrived and confused.

Some memorable lines that fell short were "America can't succeed unless you succeed" and the insistence that she wanted "to make our economy work for you and every American."

These lines fell flat because they were neither compelling nor memorable, but they also failed to shake the commonly held belief that Hillary felt entitled to be president, a problem her campaign would never overcome.

### 3. An ongoing FBI investigation caused constant problems for Clinton’s campaign. 

So, Clinton's campaign had an inauspicious launch, but her run was further hampered by a criminal investigation.

The whole affair, which turned into an ordeal of epic proportions, stemmed from her use of a personal email server between 2009 and 2013, while acting as Secretary of State. This scandal not only caused problems early on in her presidential run, but proved to be an obstacle for the duration of her campaign.

The whole saga began in 2014, when a congressional committee investigated an incident in Libya; four Americans had been killed during a terrorist attack and people wanted answers.

Eventually, the committee released its report in the summer of 2015, at the moment when the Clinton campaign was getting off the ground. The results showed that Hillary hadn't been using her government email account for official business, instead operating a personal account linked to its own private server.

The findings were serious enough that the FBI had to step in and, to cooperate, Clinton granted release of 55,000 pages of work-related emails, including some which held classified information.

Then, a year later, on July 5, 2016, James Comey, the director of the FBI, put out a statement revealing some unusual details. For instance, he called her aides "careless," and suggested that it was possible that "hostile actors gained access to her emails." Nonetheless, Comey concluded by saying that no charges would be pressed.

This could have been the end, but, unfortunately for Hillary, the email drama would persist. Both the Democratic National Committee and Hillary's campaign chairman, John Podesta, would, in turn, have their email accounts hacked and leaked in the lead-up to the election.

While these events clearly had nothing to do with Clinton's private server, that really didn't matter. For the public, the word "email" itself was all it took to connect the new events to the FBI investigation and Clinton's potential guilt.

### 4. Bernie Sanders posed a serious challenge that Clinton and her team failed to anticipate. 

Naturally, bad press about a confusing case of email security wasn't the only problem the Clinton campaign faced. In fact, a much greater challenge came in the form of a 72-year-old senator from Vermont: Bernie Sanders.

That's right; in 2016, all kinds of people were "feeling the Bern," but nobody was feeling it quite as hot as Hillary Clinton. In many ways, Sanders was the Democratic equivalent of Donald Trump; both candidates tapped into strong anti-establishment sentiments that had been rising in the United States since the 2008 election and financial crisis.

Clinton never fully understood the momentum this movement had behind it, and she greatly underestimated the threat that Bernie Sanders posed to her Democratic leadership bid. For instance, Hillary viewed many of Bernie's proposals, from free college tuition to universal healthcare to breaking up major American banks to be unrealistic and too "socialist" for the American public.

However, regardless of what the proposals were, Sanders had concrete policies, which was more than could be said of Clinton's aspirational yet ultimately empty narrative; voters were deeply moved by his campaign.

Perhaps just as importantly, Bernie was also an ardent speaker against corruption and injustice of all forms, and clearly saw Clinton as part of a corrupt establishment in need of reform. Since he was also running as a Democrat, such a tension was particularly damaging for Clinton and put her campaign in a difficult position.

For example, on the one hand, she wanted Obama voters to support her and couldn't criticize the policies she had, at least in part, been responsible for. But on the other, since Bernie was so well liked, she couldn't attack him either. Doing so would have backfired horribly, costing her votes just like it did against Obama in 2008.

### 5. Power struggles and infighting plagued the Clinton campaign. 

Following the loss of the 2008 Democratic Party nomination, Hillary was dead set on using analytics to secure the delegates she needed in 2016. However, more core to her 2008 loss was the fact that her team was overwhelmed by infighting and people who were more interested in their personal careers than getting Clinton elected.

This toxic environment reproduced itself in 2016, and in no small way contributed to the eventual failure of her campaign. As an example, take the natural rivalry between Robby Mook, Clinton's mid-30s, analytics guru campaign manager who led the so-called _Mook Mafia_, and Clinton's campaign chairman, John Podesta who, in his mid-60s, was a firm believer in the traditional politics of listening to voters.

Mook and Podesta clashed repeatedly throughout the campaign, but two other groups also contributed to the infighting. There was the _State Crew_, led by the campaign's vice chairwoman, Huma Abedin, who traveled far and wide with Clinton on the campaign trail, having the closest contact with the candidate. And there was also the _Communications Shop_, led by the communications director, Jennifer Palmieri.

With all these different directors, managers and chairpeople involved, there was constant confusion as to who was in charge of what. So, after experiencing early primary losses in New Hampshire and Michigan, Hillary put together the "Super Six," a team composed of Mook, Podesta, Abedin, Palmieri, policy chief Jake Sullivan and a longtime Clinton confidante, Minyon Moore.

This group was supposed to make decisions by committee, hopefully bypassing power struggles — but in the end, it accomplished nothing of the sort.

Instead, the team leaders tended to guard their intelligence, thereby maintaining their power within the campaign. Naturally, such a tendency led to problems.

For instance, toward the end of the campaign, Hillary was diagnosed with pneumonia and was seen fainting while making her way to a van. Abedin didn't tell anyone about the pneumonia diagnosis and the campaign couldn't get out in front of the story. As a result, Trump seized on the opportunity, accusing Clinton of being physically unfit for the job.

### 6. An overreliance on data analytics came back to haunt the Clinton campaign down the line. 

When Robby Mook was brought on as campaign manager for Clinton, it was because of the superb job he had done getting Terry McAuliffe elected as governor of Virginia in 2013, but was also the result of a recommendation from Obama's trusted strategist, David Plouffe.

Mook was a great choice because of his close attention to data management and efficient campaign budgeting but, ironically, this laser focus likely hurt the Clinton campaign in the long run.

For instance, by following the data analysis so closely, Mook distracted people from what was arguably the campaign's biggest issue: the lack of a strong message and narrative.

This can be better understood through a comparison to the Obama campaign. After all, Obama brilliantly explained to voters _why_ he was running and _how_ he would lead the country. Hillary, on the other hand, always expected her campaign to craft something equally brilliant — but the best they could muster were weak slogans like "stronger together" and "I'm with her," the latter of which reinforced a negative narrative that the campaign was about _her_ and not the voters.

That being said, it was clear to the campaign that they weren't connecting with voters to the same degree as Sanders and Trump, both of whom were drawing massive crowds. However, even with this knowledge, the Clinton team didn't do much to help themselves.

For instance, the likes of Bill Clinton and John Podesta were arguing for more volunteers to go door-to-door to clarify Hillary's message, but Mook shot such ideas down, saying that the data showed this approach was a waste of resources.

For a while, it was difficult to argue with Mook, since the campaign was bringing in delegates. Even in large states like Wisconsin that were sympathetic to Bernie Sanders, Clinton took 47 delegates to Sanders's 49. Beyond that, since Hillary swept the southern states, Bernie was always trailing in the delegate count.

But this sense of success wouldn't last. As the campaign progressed, Mook's approach would prove disastrous, as he ignored people who weren't already inclined to vote for Clinton.

### 7. The Clinton campaign got a solid boost from the Democratic Party convention and televised debates. 

So, Mook's refusal to pour resources into traditional door-to-door campaigning might have hurt the campaign in the end, but his data-focused strategy did earn Hillary the delegates she needed to secure the party nomination.

Beyond that, even though the Democratic National Convention, or DNC, was a bit messy, it gave Hillary a decent bump in the polls. Even so, the road leading up to the DNC was rough. Bernie's popularity continued to grow as the year progressed and, at the Philadelphia convention in late July, plenty of agitated Sanders supporters disrupted the proceedings with boos and heckles.

Nonetheless, speeches by Michelle Obama, as well as Bill, Chelsea and Hillary Clinton all helped boost the Democratic candidate's appeal. While pre-DNC polls pegged the candidates as neck and neck, following the convention, Clinton enjoyed an eight percent lead on Trump.

That being said, the most powerful speech of the entire convention was given by the Khan family. These two parents had lost their son, a young Muslim man, while he was on US military duty. In their speech, delivered directly to Trump, who had already proposed the policy of a ban on all Muslims entering the United States, they said, "you have sacrificed nothing for no one."

And, of course, the DNC wasn't the only time Trump met with stinging rebukes. Hillary had plenty of damning words to throw his way during their televised debates as well.

However, by the time of the September 26, 2016, debate between the two candidates, the momentum Hillary was riding out of the convention had dissipated and, early in that month, she made a catastrophic blunder, referring to some of Trump's followers as "deplorables."

Naturally, this line didn't match her all-inclusive, "stronger together" campaign branding. But she was able to regain some momentum during the debate. To do so, she simply quoted Trump back to himself saying "...this is a man who has called women pigs, slobs and dogs," directly citing comments he had made about a Miss America pageant contestant.

### 8. The FBI, Russian hackers and Wikileaks all contributed to an uncertain election day. 

Trump was so upset about the remarks Clinton made in the opening debate that he stayed up past three in the morning, unloading rage-filled tweets on the internet, blathering on about "crooked Hillary." However, that first debate would pale in comparison to the events of October 7, 2016.

On that day, three major events took place. First, at 2:30 in the afternoon, word came out that Russian hackers had broken into the emails of the Democratic National Committee and many people believed the act was intentionally intended to aid Trump. Substantiating this assumption, Trump immediately made a public appeal to the hackers to release Hillary's private emails.

Then, at 4:00, the Washington Post released a video of Trump saying he likes to grab women "by the pussy."

And finally, at 4:32, Wikileaks announced that they had obtained copies of emails from Clinton campaign chairman John Podesta and that they would be releasing them piece-by-piece in the lead up to the election.

To make matters even more complicated, just a couple weeks later, FBI director James Comey broke Justice Department protocol that prohibits interfering with an election by sending Congress an open letter. In his communication, he said that new emails had been unearthed that could be relevant to the investigation into Clinton's private server.

Despite this fact, none of the new information would change the outcome of the prior investigation; there still wouldn't be any charges filed against Hillary.

So, some serious problems came up for the Clinton campaign during this period. Surely, though, the Trump tape was the campaign killer — or was it?

Well, surprisingly, the contest continued to remain uncertain right up until its final days. According to a senior Clinton aide, since everyone "already knew [Trump] was a womanizing piece of shit, [the video] didn't change the narrative."

Instead, despite the lack of a connection to Clinton's qualifications to be president, the leaked emails aided the negative narrative that Trump had been driving home for a year; Hillary continued to be seen as untrustworthy and corrupt.

### 9. Hillary Clinton lost the election to Donald Trump on November 8, 2016. 

As election day dawned, the Clinton campaign was practically certain they would win; Hillary was leading in every poll and political analysts had all but begun writing the story of the failed Trump campaign. However, despite these assurances, November 8th, 2016, was not the day the glass ceiling was broken in the United States.

As Hillary and her team watched the results come in, some were flabbergasted while others remained in denial. That being said, looking back at their loss, Clinton's aides have some sense of what went wrong.

For instance, they point to low turnout among African-American voters, the dedication of white suburbanites to Trump and the anger among Trump supporters, fueled by the FBI investigation and Comey's October letter. They also cited misogyny as a factor. However, while some men were against Hillary, she also failed to attract a sufficient number of female supporters.

Bill Clinton's explanation of the loss on election night is also helpful in unpacking the failed campaign. As he watched the numbers roll in, he couldn't help but see a parallel to the British Brexit vote that had recently occurred.

After all, Brexit was also entirely unpredicted by analysts and Bill had been concerned about the "screw-it-vote" by Americans who simply wanted a political shake-up.

But despite all these explanations, there's no single reason for Hillary's loss. The failure to take Wisconsin was another major factor, and Mook's reliance on analytics, rather than grassroots organizing in the state, certainly didn't help.

Then again, neither did Bernie Sander's rising popularity, the infighting among Hillary's team and her campaign's lack of an inspiring message. In the end, it might actually be Hillary herself who should bear much of the blame.

Who knows how the election might have gone if she had been able to passionately describe her own perspective and plans for the country. We also can't know what the results would have been if she hadn't used her private email server for work purposes or taken all those paid speaking engagements at Wall Street banks. In all likelihood, these questions will just have to remain a mystery.

### 10. Final summary 

The key message in this book:

**Hillary Clinton's 2016 election loss shocked pundits, partisans on both sides of the aisle and ordinary people across the country. There's no one reason for her failure, but a few factors, including Bernie Sanders's remarkable primary campaign, a major scandal and a lack of a coherent message, can account for the decisive barriers to her eventually failed election campaign.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Stronger Together_** **by Hillary Clinton & Tim Kaine**

_Stronger Together_ (2016) introduces you to the backgrounds and policies of US presidential candidate Hillary Clinton and her VP, Tim Kaine. Find out how they propose to address domestic and foreign policy, including how to make college affordable; how they'll bring fair wages to women and more opportunities for immigrants; and how they'll tackle the ongoing threat of ISIS.
---

### Jonathan Allen & Amie Parnes

Jonathan Allen is a journalist who writes for Politico, Bloomberg, Vox and the weekly political column Roll Call. In addition to his writing, he helps run the political news platform Sidewire.

Amie Parnes is the senior White House correspondent for the Washington, DC, newspaper the _Hill_, where she currently covers the Trump administration.

