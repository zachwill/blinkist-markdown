---
id: 5e5542906cee0700068d3bd0
slug: she-has-her-mothers-laugh-en
published_date: 2020-02-27T00:00:00.000+00:00
author: Carl Zimmer
title: She Has Her Mother's Laugh
subtitle: The Powers, Perversions, and Potential of Heredity
main_color: None
text_color: None
---

# She Has Her Mother's Laugh

_The Powers, Perversions, and Potential of Heredity_

**Carl Zimmer**

_She Has Her Mother's Laugh_ (2018) probes the contemporary understanding of genetics and heredity, and provides an accessible history of the subject from the time of the Ancient Greeks onwards. Author Carl Zimmer also looks to the future, forecasting genetic developments on the horizon and unpacking what they might mean for humanity.

---
### 1. What’s in it for me? Discover the riches of your genetic inheritance. 

If you've ever been to a family reunion, you've probably been told that you have your grandmother's eyebrows, your uncle's smile, or something along these lines. It's accepted wisdom that genetic features, from height to male pattern baldness, are passed down from generation to generation.

But do we have the whole picture when it comes to how genetic inheritance works?

Our understanding of the mechanics of familial inheritance shifts dramatically over time and between cultures. In some early South American societies, _any_ man who had sex with a woman was considered the father of her children. Meanwhile, the Hawaiian language doesn't distinguish between sisters and female cousins.

As these diverse interpretations of genetic inheritance show, heredity is complex and our conceptions of it are constantly evolving. These blinks will give you a deeper insight into heredity and its intersections with science, medicine, culture, and history.

In these blinks, you'll discover

  * what links a nineteenth-century Moravian monk to contemporary genetic research;

  * how genetics had a hand in the downfall of the Habsburg dynasty; and

  * why some people are lactose intolerant and others aren't.

### 2. Inheritance is both a cultural construct and a biological process. 

What comes to mind when you think of inheritance? Genes? Wealth? Status? Actually, it encompasses all of these things. Inheritance is a complex concept, one with both biological _and_ cultural implications, as the story of the Habsburgs shows.

From the fifteenth to the eighteenth century, the Habsburgs were among Europe's most powerful dynasties, ruling over the Austro-Hungarian Empire. They secured their power through the dominant cultural model of inheritance: the throne passed from father to son upon the father's death.

Long before genetics became a field of scientific study, it was the basis for the transfer of wealth, status, and power in societies throughout the world, through inheritance.

In Western Europe, the Roman system of _hereditas_, in which a deceased person's estate was passed on to their _heir_, or inheritor, was prevalent up until the Middle Ages.

On top of hereditas, the Renaissance introduced the concept of blood inheritance. It was believed that traits such as intelligence and courage were contained within and transmitted through blood, from parent to offspring. Nobles, in particular, were loathe to taint their "superior" blood by intermingling it with that of people outside their class.

But it was basing their inheritance on blood and biology that proved to be the Habsburgs' undoing. They had the most rarefied blood in Europe and were preoccupied with keeping it untainted, so they only married those within a select gene pool. _Genes_ are units of hereditary information carried within our cells. What scientists now know — and what the Habsburgs didn't know — is that cultivating genetic variation, rather than purity, is key for avoiding genetic diseases.

After centuries of inbreeding, generations of Habsburgs were born with various genetic diseases, including hunched backs, pigeon chests, malformed jaws, and mental illnesses. Infertility is another side effect of inbreeding and so Habsburg kings started struggling to produce heirs — so much so that, eventually, the Habsburg dynasty died out.

Ironically, in 1592, during the dying days of the Habsburg empire, Luis Mercado was appointed court physician. In 1603, Mercado published a seminal early work, likely influenced by his connection to the Habsburgs; his book _On Hereditary Diseases_ suggested that physical traits and illnesses could — much like a crown — be passed down from one generation to the next.

### 3. The forefather of genetics was a nineteenth-century Austrian monk. 

What does a nineteenth-century Moravian monk have to do with the emergence of genetics? Quite a lot, actually.

Gregor Mendel was a young monk of the conservative Augustinian order, teaching math and science at a priory in rural Austria, when he was sent to the University of Vienna for further training. When he returned to the priory in 1853, he continued his scientific education independently.

Mendel was fascinated by _hybrid plants_, grown from the cross-pollinated seed of two different species. Biologists at the time knew that hybrid plants exhibited traits from both source plants, but couldn't predict which traits any individual hybrid plant would inherit, let alone explain why.

Mendel grew 22 varieties of pea, then painstakingly cross-pollinated a sample of 10,000 peas by hand. Crossing yellow peas with green peas, he found the first generation of hybrids was always yellow. In the second generation, however, some peas were green. Likewise, crossing wrinkled with smooth peas, or tall with short, produced similar results.

From these results, Mendel correctly theorized three principles of genetics. First, hybrid plants inherit traits from each parent plant. If one parent pea plant was yellow and the other was green, both colors would be contained in their offspring's genetic makeup. Only one of these traits is "expressed," though, which leads us to Mendel's second principle: the traits expressed in first-generation offspring plants are _dominant_ ones — in the case of the peas, the color yellow is the dominant trait. Third, traits that don't appear in the first generation but do appear in the subsequent generation are _recessive_ traits.

We now know these traits are encoded in genes. Each gene consists of two _alleles_, or variants of the gene. Just as Mendel posited, alleles can be dominant or recessive. Only one dominant allele needs to be present in a gene for a trait to find physical expression. For recessive alleles, however, it takes two. In the gene that determines eye color, for example, the allele for brown eyes is dominant, whereas the allele for blue eyes is recessive. When two parents' genetic material combines, if one carries the allele for brown eyes, their child's eyes will be brown.

Mendel's experiment articulated how genes and their alleles are shuffled and recombined from one generation to the next. He thus laid the groundwork for the discovery of DNA, the protein located in our cells that contains _all_ our genes.

### 4. DNA has revolutionized genealogical testing. 

In the annals of courtroom history, cases of disputed paternity go back a long way. But it wasn't until the early twentieth century that judges and juries were able to look to biological evidence to settle these cases.

One early, high-profile paternity dispute took place in Hollywood in 1942, when the actress Joan Barry claimed that Charlie Chaplin was the father of her daughter, Carol Ann. She sued him for child support and prenatal costs.

To prove Chaplin was the father, Barry consented for herself, Carol Ann, and Chaplin to undergo blood tests. In 1908, Polish scientist Ludwig Herzfeld had realized that the rule of dominant and recessive genes could also be applied to blood type. Of the four blood types — A, B, AB, and O — O is recessive. Only parents who both have type O blood can pass this down to their children.

As it turned out, Barry had type A blood, Chaplin type O. And Carol Ann? Type B, which she could only have inherited from her father. Chaplin was off the hook.

Of course, blood tests of this type can only rule out paternity, not confirm it. But the development of DNA testing meant genealogy could be determined with much greater accuracy — even if the subject in question was dead.

In 1917, the then-ruler of Russia, Czar Nicholas Romanov, was deposed. In July 1918, he and his family were executed at gunpoint. For years after their deaths, rumors swirled that one of the Romanov princesses had escaped.

In 1991, a grave was unearthed near the site where the Romanovs were executed. DNA samples confirmed that the remains in the grave belonged to members of the same family. But were they the Romanovs?

Forensic scientist Peter Gill was able to solve the mystery using _mitochondrial DNA_. For many years, scientists thought that our DNA resided only in the nuclei of our cells. In 1963, it was found that select DNA fibers also lived in our _mitochondria_, small units within our cells that take in nutrients and convert them to energy; our mitochondrial DNA, uniquely, is inherited from our mothers.

Gill realized that Czarina Alexandra Romanov was the granddaughter of the UK's Queen Victoria, and therefore shared her mitochondrial DNA. Comparing samples of the mitochondrial DNA found in the grave with a sample taken from one of Queen Victoria's surviving direct descendants — Prince Philip — Gill was able to confirm that the bodies in the grave belonged to the Romanov family, who had indeed all been executed that July 1918 at Yekaterinburg.

### 5. Genetics isn’t the only factor that determines our height. 

In Georgian London, two people with dwarfism named Judith and Robert Skinner made their living by exhibiting themselves as "curiosities." They soon earned enough money to retire and start a family. At the time, it was common for people with dwarfism to earn a living like this. What the Skinners did next, though, was less common. Having run out of money, they went on tour again, this time with their children. This second iteration of the Skinner's exhibition attracted even more public interest. Why? Because the Skinners' children were all of average height.

Geneticists have long been fascinated by height as an inherited trait. On the one hand, it's clear to see that tall parents often produce tall children and short parents often produce short offspring. On the other hand, this blueprint doesn't hold true 100 percent of the time — some children differ wildly in height from their parents. So, what's going on?

In 1823, Belgian scientist Adolphe Quetelet became intrigued by the question of whether, and to what degree, height is inherited. He measured a large sample of the population and found that their heights could be roughly mapped to a bell curve. A proportionately small percentage of people were either very short or very tall.

Sixty years later, in England, Francis Galton carried out a similar survey and mapped a bell curve remarkably analogous to Quetelet's. How could two surveys of height, decades apart, produce such comparable results? For Galton, the answer was simple: the curve remained constant because height is inherited.

In truth, it's slightly more complicated than that. Geneticists have determined that height is 86 percent heritable. In other words, you're extremely likely to inherit your height from your parents. But there are no guarantees. Why not? Well, unlike the distinct gene for eye color, it appears that our height is determined by the interplay of several different genes.

What's more, environmental factors like early-childhood nutrition can also impact the height you grow to reach. The French army, for example, saw a dip in the average height of recruits born during the famine-ridden Napoleonic wars. In fact, in the 1970s, the economist Robert Fogel found a correlation between national height averages and economic well-being. Essentially, when a nation grows wealthier, its citizens grow taller.

### 6. We don’t always inherit a discrete set of DNA. 

In 1953, a woman known as Mrs. McK went to donate blood. But once scientists analyzed her blood sample, they found something surprising. Mrs. McK had a mixture of type A and type O blood running in her veins. There are four blood types: A, B, AB, and O. Type OA blood was, seemingly, a genetic impossibility.

The scientists concluded that Mrs. McK must have been the recipient of a botched blood transfusion. But Mrs. McK had never had a transfusion.

In fact, she turned out to be the first recorded example of a _human chimera_, a person who possesses two distinct sets of DNA.

Before Mrs. McK, scientists had observed chimerism in some animals, and even created chimeras by hybridizing plants and other organisms. Now, they saw that humans could be chimeras, too.

There are a few explanations for how human chimerism occurs. Chimeras can form when two twin embryos fuse _in utero_, or when one twin is absorbed by the other. Sometimes, the DNA of a child that dies _in utero_ can be absorbed by its mother, rendering her a chimera.

Mrs. McK's blood was sent to medical researchers Robert Race and Ruth Sanger for further analysis. They too were mystified until Race connected Mrs. McK's case with that of a phenomenon observed in some fraternal twin cows, which carried their twin's blood type as well as their own. Race and Sanger correctly surmised, on the basis of her strange blood type, that Mrs. McK had a twin. When they asked her, she confirmed that she'd had a twin brother, who'd died when he was a toddler.

The existence of human chimeras complicates the prevailing understanding of DNA as unique and discrete to one individual. The case of Seattle woman Lydia Fairchild illustrates this. Fairchild separated from her husband, with whom she had three children and, in 2002, applied for paternal child support. The court-ordered DNA test found that, while the father's DNA was a match, Fairchild's DNA didn't match her children's. She was accused of stealing her children as part of a surrogacy scam. It wasn't until her attorney learned about chimerism that Fairchild was able to clear her name.

As genetic testing becomes more common, particularly in legal settings, we must realize that the results of a blood or DNA test might not always show the whole picture. Cases like that of Mrs. McK or Lydia Fairchild show that our understanding of DNA only scratches the surface.

### 7. There’s a reason that single cells can produce complex lifeforms. 

When does life begin?

Scientifically speaking, it begins with a single-celled _zygote,_ an egg that's been fertilized by sperm. Inside the zygote, two discrete _DNA chromosomes_, the molecules that contain all of an organism's DNA, are mingled and reshuffled. The zygote grows, dividing into two cells, then four, then eight, each imprinted with this new DNA. This collection of ever-multiplying cells becomes an embryo, and then a fetus: a new human life.

Is it really possible that our human bodies, in all their variety, are formed from a single homogenous cell?

Well, yes. And no.

Through the centuries, scientists have wondered how a single egg can go on to produce such a complex body. It wasn't until 1961 that Mary Lyon, a geneticist at the University of Edinburgh, proposed an answer to the puzzle.

Lyon was studying mice with a genetic mutation on their X chromosomes. Females that inherited this mutation sported mottled fur but were otherwise healthy. Males that inherited it died, because — Lyon theorized — they only had one X chromosome. The females survived because they had inherited two X chromosomes and could therefore shut one down — but how?

The answer lies in a process called _methylation_. It turns out that individual genes on our DNA sequences can be effectively switched off when they are methylated — that is, when cells coat them in a molecular shield. That's how Mary Lyon's female mice were able to switch off their mutated X chromosomes.

It also explains how something so complex as a human can be produced from a single zygote. A lone zygotic cell can potentially go on to become any one of the approximately 37 trillion cells that make up the human body; our entire DNA sequencing is located in each cell's nucleus. But each individual cell only expresses certain aspects of that sequence. Methylation activates the genes that determine a cell's function by turning off all the other genes.

These cells start off as _pluripotent,_ or able to differentiate into numerous different cell variations. But, after a pluripotent cell has divided several times, the function of its offspring cells becomes fixed. Tissue cells beget more tissue cells, stomach lining cells divide into more stomach lining cells. A zygote can produce a fantastically complex human being not just because of cells' pluripotent potential, but also because of their remarkable consistency once their function is fixed.

### 8. Acquired traits can be inherited by the next generation. 

Some traits, like eye color, are _innate_, meaning they are hardwired into our DNA and passed down from one generation to the next. Other traits, conditions, and behaviors — things like depression, or heart disease — can be _acquired_, meaning we develop them throughout our lifetimes.

But here's where it gets interesting. Scientists have found that acquired traits, like innate traits, can also be passed down at a genetic level, from one generation to the next.

In the 2000s, biologists at Washington State University were studying the effects of a fungus-killing chemical called vinclozolin on mice. They found that when pregnant mice were dosed with vinclozolin, their male offspring produced defective sperm as a result of their exposure to the drug _in utero_. But when those male offspring bred, the scientists observed something truly unexpected. The third generation of male mice _also_ produced defective sperm, despite having never been exposed to vinclozolin. So did the fourth generation.

What does this mean? That acquired traits have the potential to be inherited, in just the same way as innate traits.

A 2013 study at Emory University produced similar results. In this study, mice were exposed to acetophenone, a chemical that produces an almond smell. Shortly after their exposure, the mice were given an electric shock. Unsurprisingly, after three days of this, the mice stopped in their tracks when they were exposed to acetophenone, in anticipation of the shock to follow. What _was_ surprising was that the offspring of these mice also exhibited sensitivity to acetophenone, despite never having been given a shock after acetophenone exposure. The same held true for the following generation of mice.

It would seem that even learned behaviors can be passed down genetically between the generations. Countless more studies have shown that mice exposed to stress when they are young can not only experience symptoms of depression and hopelessness, but pass these symptoms down to their offspring.

What does this mean for humans? Potentially, that apart from our DNA, we can also pass the after-effects of trauma, poverty, stress, and violence down through the generations. It's a finding that might soon push us to look at intergenerational trauma in a new light.

### 9. Genetic mutations shape our taste for dairy. 

Did you know that about two-thirds of humans have some form of lactose intolerance? The explanation for this widespread intolerance — and for why the remaining two billion or so people on the planet _can_ enjoy a bowl of ice cream with no nasty side effects — lies in our genetic makeup.

Here's something weird about humans: we're the only mammals that consume milk products after we've been weaned from our mothers' breast milk. After they're weaned, other mammals stop producing _lactase_, the enzyme that allows them to break down the sugars contained in milk. Without lactase, milk and milk products are difficult to digest.

It seems likely that humans also used to stop producing lactase shortly after we were weaned. And, indeed, the two-thirds of us that experience lactose intolerance still stop producing lactase at a young age. Those of us that can consume milk without suffering side effects have inherited a genetic mutation that causes us to keep producing lactase into adulthood.

This mutation is found on the _genome_, which consists of the complete set of our DNA. Complete, in that it is formed of our _coding DNA_ and _noncoding DNA_. About 20,000 of our genes are coding DNA, meaning they are the genes whose traits are ultimately expressed in physical form. But coding DNA only accounts for about 1.2 percent of the genome. The rest is _noncoding DNA_. This DNA performs other important functions within cells, regulating protein production and switching off certain DNA strands by wrapping them in protein, for example.

Humans that can enjoy milk products are able to do so because an element of the noncoding DNA in their genome has evolved not to deactivate the _LCT gene_, which produces lactase.

Why did this mutation occur?

Well, this genomic development has been found in people who can trace their ancestry back to locations with a strong tradition of cattle herding, like East Africa and Northwestern Europe. Sometime after the domestication of cattle, our genome started to mutate, allowing us to consume dairy products.

Then, natural selection kicked in: when food was scarce, people who possessed the mutated gene were able to consume dairy products to avoid starvation.

So, remember: if you're one of those lucky people that can enjoy cheese or strawberry ice cream, you have your genome to thank!

### 10. Final summary 

The key message in these blinks:

**The process of genetic inheritance is intricate, complex, and hard to pin down. But as we learn about our genome, and the myriad ways in which it works, we access a deeper understanding of how our own bodies have been scripted by previous generations** ** _and_** **how our genetic imprint will write itself on generations to come.**

Actionable advice:

**Approach DNA testing kits with caution!**

At-home DNA testing kits are experiencing a surge in popularity and can be a fun way to delve into your family's history. But remember, your DNA is a complicated thing and a simple kit can't give you the whole story. Particularly if you have medical reasons for delving deeper into your DNA, it's best to consult a specialized geneticist.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Inheritance_** **,** **by Sharon Moalem**

If these blinks have fueled your fascination for heredity, why not take a closer look at your genetic building blocks? Sharon Moalem, an award-winning neurogeneticist, evolutionary biologist, and _New York Times_ best-selling author, takes a deep dive into DNA in _Inheritance_. Our blinks parse the cutting edge of genetic study in accessible terms, to help you understand how your genes shape your life — and how your life can shape your genes.
---

### Carl Zimmer

Carl Zimmer is a science journalist who reports on genetics, evolution, and parasites in "Matter," his _New York Times_ column. He is also the author of many popular science books, which have won him accolades such as the Stephen Jay Gould Prize, the Science in Society Journalism Award, and a Guggenheim Fellowship.

