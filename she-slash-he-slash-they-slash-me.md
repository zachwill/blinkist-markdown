---
id: 5d3eb2fa6cee0700080981fa
slug: she-slash-he-slash-they-slash-me-en
published_date: 2019-08-01T00:00:00.000+00:00
author: Robyn Ryle
title: She/He/They/Me
subtitle: For the Sisters, Misters, and Binary Resisters
main_color: 68C2AC
text_color: 3F7568
---

# She/He/They/Me

_For the Sisters, Misters, and Binary Resisters_

**Robyn Ryle**

_She/He/They/Me_ (2019) provides readers with a unique opportunity to explore the many concepts and phenomena of gender. Weaving anthropology, global history and gender studies into a fascinating blend of empirical information and theoretical speculation, author Robyn Ryle opens our eyes to the sheer vastness of the possible forms that gender can take.

---
### 1. What’s in it for me? Learn about the many possible forms that gender can take. 

What is gender? That's the question at the center of these blinks — but spoiler alert: they're not going to provide you with a definitive answer. Indeed, there is no such answer! Gender can be different things for different people in different societies at different times. It can even be different things for the _same_ people at different stages in their lives. 

Instead of a single, fixed phenomenon, we can imagine gender as being like a landscape, across which societies and individuals can chart various courses. Each course reveals another possibility — and the possibilities are nearly endless. 

As a result, we can't map out the terrain of gender in its entirety; there's simply too much ground to cover. But there are some crucial junctions at which the possibilities of gender branch off, like paths extending from a series of crossroads. In these blinks, we'll explore some of those main crossroads and paths. 

Along the way, you'll learn 

  * that there are more than two genders; 

  * what a female-dominated society might look like; and

  * what transcending gender altogether could mean for society.

### 2. The traditional view of gender is simplistic, reductive and problematic. 

Before we set off on our journey to explore the possibilities of gender, we first need to lose some of our baggage. Otherwise, we'll be carrying too much weight to travel freely. 

To lighten our load, we must set aside the simplistic, conventional notion of gender that can drag down our thinking about the subject. 

Let's start by laying it out on the table and examining its logic. Gender is typically thought of as a natural, objective distinction that divides people into one of two categories: male or female. If you're male, you normally have a penis, and you act in certain ways people call "masculine" — like being dominant and feeling sexually attracted to women. Conversely, if you're female, you normally have a vagina, and you act in certain ways people call "feminine" — like being passive and feeling sexually attracted to men. 

That's more or less the "common sense" understanding of gender in modern Western society. Notice what's baked into it: a set of unexamined assumptions about what's "objective," "natural" and "normal," coupled with a series of black-and-white, binary oppositions, like masculine/feminine, penis/vagina and dominant/passive. All of these assumptions and oppositions are highly questionable, for reasons we'll look at in the course of our journey. 

With this conventional notion of gender, we can also notice a lot of conflation going on between concepts and aspects of reality that should be distinguished from each other. In thinking about gender this way, we're mixing up _biological sex_, _gender assignment, gender identity_, _gender expression_ and _sexual orientation_ — blurring them together into a confusing mess of ideas. 

We'll unpack all of these concepts in due time. For now, the important thing to note is simply this: the conventional notion of gender is pretty reductive — and deeply problematic. 

As we'll see, the reality of gender is much more complicated. It involves many other factors, dimensions and possibilities, which are overlooked or even outright obscured by the "common sense" understanding of gender. 

And, as is often the case, this "common sense" may be common, but it doesn't really make much sense, at least in terms of capturing the complexities of reality. In the journey ahead, we'll be striving to do these complexities better justice.

### 3. There could be societies with no gender. 

Now that we've left behind some of our baggage, we can begin our journey. Right at the start, we find ourselves at a crossroads, with two paths open to us. The first one leads to all of the possible societies in which gender exists. The second one leads to those in which it _doesn't_ exist. 

Let's start with the second path. 

What would a genderless society look like? Well, people would still be distinguishable by their _biological sex_. In other words, they'd still have different genitalia, hormones and chromosomes related to their reproductive systems. But this society wouldn't divide them into _gender categories_ on the basis of such differences _._ That is to say, people wouldn't be classified as "men," "women" or any other gender term, nor would they be given any social status or behavioral expectations related to those classifications. 

To wrap your mind around this possibility, think about it this way. Human beings have all sorts of physical differences, from the color of their hair to the shape of their earlobes. The difference between their reproductive systems is just one of many physical variations that could be used to distinguish people from one another. But in modern Western society, we attach much more significance to the difference between people's reproductive systems than that between, say, the size of their feet.

In theory, though, we could ascribe just as little importance to the one difference as the other. In that case, calling people "men" and "women" would seem as weird as calling them "big foots" and "little foots." And it would seem even stranger to view and treat them as fundamentally different types of human beings on the basis of such distinctions! 

But has any such society ever actually existed? 

Well, it's possible that prehistoric societies lacked a concept of gender — or maybe they had one, but simply didn't attach much importance to it, compared to other traits. There are some examples of this second possibility in more recent human history. For instance, before European colonization, the Yoruba people of Africa's main measure of social status was seniority — and for them, seniority had nothing to do with whether someone was male or female. 

That being said, every culture that we know about has had some concept of gender — but many different concepts are possible. We'll take a look at them in the next blink.

### 4. There could be societies with one, three or even infinite genders. 

As we enter the realm of societies in which gender exists, we arrive at another crossroads — this one branching into four paths. Each path represents a different answer to a single question: How many genders are there? There's the familiar answer of "two," but that's just one possibility. Let's take a look at the other three. 

First, we have societies in which there's only one gender. 

Consider the ancient Greeks, for example. Although they thought about people as being men or women, they didn't consider them to be fundamentally different types of human beings. Instead, women were seen as inferior versions of men. Even their reproductive organs were thought of as different varieties of the same basic body parts; in their view, vaginas and ovaries were simply internalized versions of penises and testes. 

Next, we have societies in which there are more than two genders. In addition to the familiar masculine and feminine, these societies have additional genders, which are called _gender-variant categories._ Around the world, there have been societies with gender-variant categories throughout human history. 

Here's an example. Traditionally, the Mohave people of North America had a gender-variant category called _alyha._ These tribe members were classified as boys at birth. However, by the age of ten or eleven they were behaving in ways that the Mohave considered feminine, perhaps by playing with dolls or wearing bark skirts. 

Eventually, they were rechristened with feminine names, and they'd often marry men and even participate in menstruation ceremonies — but they weren't considered women. For example, they had their own separate set of rules regarding lineage names and courtship rituals. 

Finally, we have societies in which there are infinite genders. At present, these societies are only hypothetical — but as individuals, we can adopt this view of gender right now, and we can imagine a whole society that might share it someday. 

To get into the infinite-gender mindset, ask yourself the following question: If you identify as a man or a woman, do you behave in ways that your culture considers "masculine" or "feminine" in every single aspect of your life, at every waking moment? 

Surely you deviate from these _gender norms_ in one way or another. And the same is true of everyone else. Thus, each of us can be thought of as embodying a unique version of gender, representing just one of infinite possible variations.

### 5. Two-gender societies can vary a great deal, depending on their gender norms. 

After considering the complexities of a society with three or even infinite genders, you might think that having only two of them would be pretty straightforward in comparison. But even with just two genders, there are still plenty of possibilities. 

While the words might stay the same, the actual meaning of "masculine," "feminine" and their related terms can drastically change between different cultures, eras and groups of people within a particular society, such as races and socioeconomic classes. 

To get an idea of the sheer diversity of what's possible in a two-gender society, let's compare and contrast some of the gender norms of different eras and groups in the same country: the US. 

To begin with, imagine you're a wealthy white man who's the son of a plantation owner in the 18th-century American South. 

Given your race, class and regional culture, a large part of your idea of masculinity would center around notions of refinement and elegance. To follow your culture's gender norms, you might wear fancy clothing, appreciate fine wines and write poetry — activities that many contemporary American men wouldn't exactly consider "masculine." And although you'd be rich, you wouldn't be expected to strive to accumulate even more money, unlike your modern counterparts. 

Now imagine you're a white man living in a city in the North around the same time. 

You work as an artisan — a silversmith, let's say. In contrast to the Southern gentleman's life of leisure, your notion of masculinity would idealize physical labor and strength. But your culture only values strength because it allows you to do hard work — not because it enables you to beat up other men. Indeed, violence and aggressiveness in general weren't a part of either of these 18th-century conceptions of masculinity.

Now fast-forward to the 21st century, where American boys are encouraged to be "tough" and "take risks," which often leads to them acting aggressively or even violently with each other. 

This sort of behavior then gets excused as "boys just being boys" - as long as those boys are white. When black and Latino boys engage in such behavior, however, it tends to be seen as more dangerous than that of their white counterparts. Thus, it gets punished more harshly at school. 

In other words, gender doesn't exist in a vacuum; instead, it intersects with other dimensions of society, such as class and race. Therefore, people's experiences of gender norms can vary dramatically from one group to another within the same society. 

Next, we'll look at some different notions of femininity.

### 6. Gender norms are taught to children through a process called gender socialization. 

To continue our exploration of two-gender societies, and to see how different cultures can have radically divergent notions of femininity, let's first return to the contemporary US.

Here, girls are conventionally taught to be nurturing and passive. For example, they're "supposed" to take care of their younger siblings, and they're not "supposed" to get their clothes dirty or be "domineering" when they play with other children. 

But these feminine gender norms stand in sharp contrast to those that girls learned among the Mundugumor people of Papua New Guinea, whom the anthropologist Margaret Meed studied in the 1930s. In their tribe, girls and boys alike were taught to be physically aggressive and even violent. 

In the modern West, these characteristics would be considered "masculine," but the Mundugumor didn't make much of a distinction between boys and girls. Neither did the Arapesh — another tribe that Mead studied in Papua New Guinea. Within this community, boys and girls alike were taught to be gentle and non-violent. 

In other words, different cultures can teach their children very different — even diametrically opposed — gender norms. The crucial word to note here is "taught." Gender norms aren't like laws of nature, which govern our lives regardless of whether we're aware of them. They're human ideas and conventions that our culture must teach us to believe in and conform to, if they're to hold sway over our behavior. 

But how exactly are these norms taught to us? Well, there's a fancy term for it: _gender socialization._ This is a complex process, but one of the main ways in which it takes place is pretty straightforward. 

Basically, as children, we're rewarded for conforming to our culture's gender norms, and we're punished for violating them. 

The rewards and punishments are delivered by people like our parents, teachers and peers, and they often take the form of verbal feedback. For instance, a contemporary American girl might be praised as a "good girl" for cradling a doll, while she might be scolded for being "bossy" if she tells other children what to do. In contrast, a modern American boy could be mocked as a "sissy" for playing with a doll, but he might be celebrated as a "leader" for bossing other children around. 

Thus, the children get taught that leadership is for boys and men, while nurturing is for girls and women. These gender norms have some pretty big social implications, which we'll turn to next.

### 7. A two-gender society can be patriarchal or matriarchal. 

Now that we've seen some examples of gender norms and socialization, we can ask some more fundamental questions: What's the point of these norms? And why bother teaching them to children in the first place? 

To find the answers, let's consider a more straightforward question: If modern American boys are being encouraged to behave like leaders and discouraged from acting like nurturers, and if the opposite is true for girls, what kind of roles are the children being prepared for when they grow up into adults? 

The answer to this seems pretty obvious: boys are being prepared for roles of power and steered away from roles of caring, while the reverse is true for girls. 

To put it another way, both boys and girls are being prepared to live in a _patriarchal society_ — a society in which men have more power than women.

Patriarchy is marked by _gender inequality_ between men and women, and this inequality is usually accompanied by _androcentrism –_ a worldview in which men and masculinity are seen as superior to women and femininity. For example, allegedly masculine traits, like being rational, are seen as superior to allegedly feminine traits, like being emotional. 

As for measuring gender inequality, we can use a variety of statistics related to men and women's differing levels of health, education, economic status and political power. By looking at these statistics, international organizations like the United Nations and the World Economic Forum can quantify and rank countries' overall levels of inequality. The countries can then be divided into three categories: those with low, medium and high levels of gender inequality. 

In the low gender-inequality country of Rwanda, for example, 58 percent of legislative seats were occupied by women as of 2019 — earning it first place in this measure of women's political power. In contrast, those figures were a dismal 22 percent in the US and 12 percent in Syria, which have medium and high levels of gender inequality, respectively. 

As these examples demonstrate, patriarchal societies can be unequal to a greater or lesser degree. But those aren't the only possibilities for the balance of power in a two-gender society. The whole balance could be flipped, resulting in a _matriarchal society_ — that is, a society in which women possess more power than men overall. 

What would a matriarchal society look like? Stay tuned to find out.

### 8. Gender inequality exists in many different areas of society. 

Before we set off into the realm of matriarchal societies, let's remind ourselves of the definition we established in the previous blink: matriarchal societies are societies in which women possess more power than men _overall_. 

That word "overall" is crucial. If you just looked at the gender distribution of legislative seats in Rwanda, then it would seem like women had more power than men. 

But Rwandan women still face gender inequality in a number of other respects. At home, for example, even women who hold seats in Rwanda's parliament are expected to be subservient to their husbands. For instance, in a research study, one of those women was observed polishing her husband's shoes and ironing his shirts. The husband insisted she should perform these tasks, despite the fact that they had a housekeeper. 

That might seem backward from a Western perspective, but keep in mind that Western women who work full-time jobs and are married to men still do more household and childcare work than their husbands, on average. This work is often called their _second shift._

Some feminists argue that there's also a _third shift_ : the many hours per week that women might spend working on their bodies in order to conform to Western beauty standards — applying makeup, painting their nails and so forth. 

And then there's _emotional labor_ — another form of work that women are expected to do more than men. In an economic context, this is work that involves regulating your emotions in order to make other people feel a certain way. 

For example, if you're a waitress in the US, you're expected to smile to make your customers happy, regardless of how you're really feeling. If you're sad or angry, you need to hide those emotions and act as if you're cheerful. These kinds of jobs tend to be held more by women than men — and, not coincidentally, they also tend to be lower-paid occupations, compared to the ones that men dominate. 

So, there's a _lot_ of gender inequality that would have to be overcome on numerous fronts before a patriarchal society would turn into a matriarchal one. Just filling up a bunch of parliament seats doesn't turn the tide. 

But what if the tide were turned? We'll look at some of the possibilities in the next blink.

### 9. Matriarchy isn’t just the opposite of patriarchy, and gender equality might require transcending a two-gender society. 

You might suppose that a matriarchal society would look just like a patriarchal society in reverse — with women dominating men, rather than the other way around. But if the anthropological record is any indication, that's not necessarily the case. 

Now, there's controversy among researchers over whether any society has ever been truly matriarchal, and debate over what the word "matriarchy" even means in the first place. But there have been some societies in which power has tended to be held more by women than by men. For the sake of simplicity, let's call these matriarchal societies and look at some of their general features. 

To begin with, matriarchal societies tend to be _nonhierarchical_ ; that is to say, there aren't major differences in social or economic status between their various members. For example, material goods tend to be equally distributed, and decisions tend to be made through group consensus. The women have slightly more power than the men, but the imbalance isn't as large as it tends to be in patriarchal societies. 

Now, you might be thinking that this sounds promising — but why not just skip the last part and make everyone completely equal, regardless of gender? In other words, why not create a society that's neither a patriarchy nor a matriarchy, but rather one that's free of any form of gender inequality? 

Well, that's the ultimate objective of many feminists — but others would argue that it's a fantasy. As long as there are two genders, some would contend, one of them will always tend to dominate over the other. That's just the inevitable outcome of creating a dichotomy between two things; one of them ends up being seen as superior to other. Just think of other dichotomies in traditional Western thought, such as mind/body and rational/emotional.

But even if this argument is correct, that doesn't mean we're stuck with a choice between just having matriarchy or patriarchy. There would still be other possibilities, such as the ones we looked at earlier: creating a society with no genders, more than two genders or even infinite genders. 

As long as we're speculating, we can imagine all sorts of other possibilities. For example, in a distant future in which "people" might be free-floating consciousnesses uploaded to a digital cloud, what would gender become? 

Who knows? And that's the point: the world of gender is full of possibilities!

### 10. As an individual, your gender journey begins with your gender assignment. 

Up until now, we've been looking at possible answers to the following set of questions: For a given society, is there the concept of gender? If yes, how many genders are there? How are they defined? What are their norms? How are those norms instilled? And which gender has more power? 

The answers to these questions define the broad contours of what gender looks like in a particular society; they shape an overall gender landscape, so to speak. But individual human beings still need to navigate that landscape for themselves, and there are all sorts of possible paths they can take. 

From this point on, let's assume we're dealing with the type of society that most of us are familiar with: a two-gender, patriarchal society, which has some degree of overall gender inequality in favor of men. And for the purpose of establishing some of the technical details of the scenarios ahead, let's also assume that we're talking about the modern US. 

Within this society's gender landscape, what are some of the main crossroads and pathways that an individual's life might traverse? 

Well, imagine yourself beginning this journey. Let's start before birth, when you're a fetus of about eight weeks old. At this point, you're anatomically indistinguishable from a fetus of another biological sex. In the area of your body where your genitals will develop, you have a mass of tissue called a _genital tubercle._ Depending on the hormones to which this genital tissue is exposed, it will usually develop into a penis or a clitoris. 

If it ends up longer than 2.5 cm when you're born, the doctors will identify it as a penis and proclaim you a boy. If it's shorter than 1 cm, they'll classify it as a clitoris and declare you a girl. Either way, you'll have received your _gender assignment –_ the gender category into which your society places you. 

But what if your genital tissue ends up somewhere between 1 and 2.5 cm? In that case, you'd be said to have _ambiguous genitalia_, and you'd be characterized as _intersex_. This is an umbrella term that covers any condition in which a person's anatomy doesn't fit neatly into either of the traditional biological sex categories of male or female. 

In the next blink, we'll take a closer look at intersexuality.

### 11. Gender assignments can be rigidly imposed on people. 

Intersex conditions are much more frequent than you might think. Some estimates put them as occurring in as much as 1.7 percent of the global population — comparable to the percentage of people who have red hair! These conditions also illustrate some other important aspects of gender, so, for both reasons, they're well worth examining in more depth. 

Ambiguous genitalia is just one of many possible reasons a person might be intersex. Other intersex conditions include having a penis _and_ a vagina, an ovary _and_ a testis, or XXO, XXY or XO sexual chromosomes (unlike the biologically female XX and male XY chromosomes). 

If you have an intersex condition that involves your internal sexual organs or chromosomes, it probably won't be detected at birth. In that case, you'll be assigned a gender just like a non-intersex infant — your condition remaining hidden, at least for now. By contrast, if your condition involves your external sexual organs, it probably will be detected at birth. At this point, your doctor and family might pursue one of two approaches to your condition. 

The first is the _concealment-centered model_. Under this model, your doctor will view your condition as being pathological, even though intersex conditions are usually harmless. The doctor will then use hormones, surgery or other medical interventions to "treat" your condition. Some of these interventions are irreversible. For example, if you have genital tissue that could be considered a penis, but you've been declared a girl due to your having two X chromosomes, the tissue might be surgically removed. 

The point of these interventions is to conceal the "abnormality" of your ambiguous genitalia and "normalize" them — that is, make them look more like those of the typical boy or girl that someone else has decided you "should" be. 

The concealment may also include hiding your condition from you. For instance, certain medical records might be kept away from your eyes. The aim is to avoid "confusing" you about your gender — or rather, the gender you were assigned and are now being further shoehorned into, without any choice in the matter. 

In this particular respect, most of us found ourselves in a similar situation when we were children, regardless of whether we're intersex; our gender assignment was imposed on us by other people. But there's an alternative possibility, and it's illustrated by a different medical approach to intersex conditions, which we'll turn to next.

### 12. Gender assignments can be more loosely applied, opening up space for different gender identities. 

Imagine once more that you were born with a visible intersex condition — but this time, your doctor and family were ethically opposed to the concealment-centered model. As an alternative, they might follow the _patient-centered model_.

In this model, your doctor and family will choose a gender for you at birth — but it'll be a provisional one, and they'll assign it to you with the understanding that you might want to revise it when you're older. 

As you grow up, your family and doctor will avoid subjecting you to any irreversible procedures. Rather than viewing your condition as a pathology, they'll see and treat it as just a natural part of who you are. Sure, other people might see it as strange; indeed, this is a fear that motivates the concealment-centered model. But that's a societal problem, not a medical problem. 

With that in mind, your doctor and family will freely share information with you about your condition, and they'll respect your wishes if you later identify as a different gender than the one they initially settled upon. In that case, your gender assignment would be at odds with your _gender identity_ — the way you see yourself in terms of gender. 

Of course, other people can find themselves in this situation as well. You could be biologically female and receive a feminine gender assignment, but have a male gender identity. In that case, you'd be a _trans man –_ in contrast to a _cis man_, whose biological sex, gender assignment and gender identity are all in alignment from birth. If the case were the other way around, you'd be a _trans woman,_ in contrast to a _cis woman_. 

You could also be a _nonbinary_ or _genderqueer_ person who identifies as neither male nor female exclusively. Or you could be _agender_ and identify with no gender category at all! 

In all these cases, your gender identity will have become something different than your initial gender assignment. Because this can happen to anyone, regardless of the sexual organs they were born with, some intersex activists advocate the patient-centered model for everyone, not just intersex infants. 

That way, our gender assignments would be less firmly imposed on us, and we'd all be in a better position to revise them later in life, if we needed to realign them with our gender identities.

### 13. Gender expression and sexual orientation shape an individual’s experience of gender. 

Let's say that one way or another, you've established your gender identity: you're a trans woman, cis man, nonbinary person or whatever the case might be. 

But establishing your gender identity is hardly the end of your gender journey; in many ways, it's just the beginning. From here, all sorts of possibilities open up. We can divide them into two additional dimensions of gender. 

The first is your _gender expression_, which refers to all the ways in which your behavior expresses your gender identity. Your gender expression may or may not align with the gender norms that your society imposes on your gender identity. If it doesn't align, then you're a _gender-nonconforming_ person. 

For example, the American sociologist Betsy Lucal identifies as a woman, but she doesn't have long hair or wear clothing that her culture considers "feminine." As a result of not conforming to society's gender norms, she sometimes gets mistaken for a man. This can lead to strange looks when she enters public bathrooms for women. 

But the price of gender nonconformity can be much steeper. For example, if a boy likes to wear pink clothing, play with dolls or do other things that his culture deems "feminine," his peers might call him the derogatory term "fag." 

Of course, the boy may or may not be gay; there's no necessary connection between a person's gender expression and their _sexual orientation_ _–_ the gender(s) to which they're sexually attracted _._ But the boy's peers may assume that if he acts in ways they see as feminine, he must be gay _._

Now, by using a derogatory term, they would also be assuming that there's something "wrong" with being gay. Underlying this assumption is another one: that being straight is the "normal" sexual orientation — an assumption called _heteronormativity_. 

As such, sexual orientation comes into play as the second additional dimension of gender. You're doubtless familiar with the most well-known orientations: straight, gay and bisexual. But there are other possibilities. For example, you could be an _asexual_ person, who has no interest in having sex with anyone. Or you could be a _pansexual_ person, who can be attracted to people of any gender assignment, gender expression or gender identity. 

If you really want to get a sense of the sheer vastness of the possibilities of gender, imagine being a pansexual, gender-expansive person in a society with infinite genders. Or an asexual, agender person in a society with three genders. Or a straight man in a matriarchal society that follows a patient-centered model of gender assignment. 

The possibilities are endless!

### 14. Final summary 

The key message in these blinks:

**The possibilities of gender are virtually endless. Besides the two genders with which we're all familiar, societies can have one, three, infinite or even no genders. Within two-gender societies, there can be quite a lot of variety between different cultures' gender norms, gender inequality and gender socialization processes; for example, one culture can teach its girls to be passive, while another can teach its girls and boys to be equally assertive. People's individual experiences of gender are shaped by their gender assignment, gender identity, gender expression and sexual orientation. Some people's gender identities differ from their gender assignments, and their gender expressions can be at variance from their culture's gender norms.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Galileo's Middle Finger,_** **by Alice Dreger**

In these blinks, you learned about a wide range of gender-related concepts and phenomena, some of which center around the following question: What do you do when your gender assignment doesn't align with your gender identity? Identifying as a trans person is one possible answer. 

If you want to know more about this aspect of gender, Alice Dreger opens a fascinating window into the topic. Recounting the tale of a psychology professor whose research on transgenderism went against the grain of established ideas, Dreger's narrative also provides insight into what happens when academics, scientists and activists disagree with each other on a topic that greatly affects people's lives. To learn more, check out our blinks to _Galileo's Middle Finger_, by Alice Dreger.
---

### Robyn Ryle

Robyn Ryle is a professor of sociology and gender studies at Hanover College in Indiana. She's the author of the textbook _Questioning Gender: A Sociological Exploration,_ and her academic writing about gender inequality has appeared in SAGE Publications' _Investigating Social Problems_ textbook and the _Wiley-Blackwell Encyclopedia of Gender and Sexuality_. For general audiences, her essays have appeared in _Gawker_, _StorySouth_ and _Little Fiction/Big Truths._

