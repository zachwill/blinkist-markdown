---
id: 5c505a1c6cee070007c14b0d
slug: ship-of-fools-en
published_date: 2019-02-01T00:00:00.000+00:00
author: Tucker Carlson
title: Ship of Fools
subtitle: How a Selfish Ruling Class Is Bringing America to the Brink of Revolution
main_color: CB2932
text_color: CB2932
---

# Ship of Fools

_How a Selfish Ruling Class Is Bringing America to the Brink of Revolution_

**Tucker Carlson**

_Ship of Fools_ (2018) delivers a provocative indictment of the political, cultural and economic elites of American society. In it, Tucker Carlson presents his case for how the elites are responsible for a wide variety of issues, ranging from sexism and racism to poverty and immigration.

---
### 1. What’s in it for me? Understand a conservative’s view of Donald Trump’s ascendency to the White House. 

How and why did Donald Trump win the US presidential election of 2016? What motivated so many Americans to vote for him?

In the aftermath of Trump's unexpected victory, these were the questions raised by many commentators, political scientists and everyday citizens alike. Numerous answers have been offered and people have been arguing about them ever since.

These blinks will give an explanation from one of the most prominent voices on the conservative side of the American political spectrum, Tucker Carlson.

Basically, it boils down to the following contention: by voting for Trump, Americans were sticking a giant middle finger up to their country's elites, whom Carlson defines as the lawmakers, business leaders, journalists and other people who hold positions of power in America's dominant political, economic and cultural institutions.

Carlson sees many legitimate reasons why Americans would feel fed up with their country's elites. In these blinks, we'll go through those reasons one by one. Along the way, you'll learn Carlson's case for why

  * elites have shifted the blame for society's problems onto poor and middle-class Americans;

  * elites support immigration for ulterior motives; and

  * elites have turned the Democratic Party into the party of the rich.

### 2. Elites are out of touch with poor and middle-class Americans. 

To understand Donald Trump's ascendency to the White House, you first need to understand the historical context in which it took place. That context can be boiled down to a single phrase: the decline of the American middle class.

To see that decline, you just have to look at a few statistics. As these statistics demonstrate, there's been a significant reduction in both the relative size and economic power of the American middle class over the past four decades.

From 1970 to 2015, the percentage of Americans who were in the middle class fell from over 60 percent to under 50 percent of the adult population. Meanwhile, the middle class's share of all income earned in the United States plummeted from 62 percent to 34 percent, while wealthy Americans' share shot up from 29 percent to nearly 50 percent, significantly increasing their absolute wealth in the process.

In other words, the gap between the rich and the poor has gotten wider and wider, while more and more Americans have been pushed to opposite sides of it. And as the gap has widened, the rich have put more distance between themselves and everyone else, both figuratively and literally.

For instance, wealthy Americans tend to be cloistered in a small number of coastal enclaves, such as certain affluent neighborhoods in and around New York City, Washington DC and San Francisco. Geographically isolated from most of their fellow citizens, the rich then go on to live lives that are largely separated from them as well.

Their children go to elite schools, such as Sidwell Friends in Washington DC — a very selective private school that has been attended by the offspring of prominent journalists, political officials and politicians, including both the Obamas and the Clintons. Then, they attend elite colleges and universities, such as Harvard, Princeton and Yale. Finally, they enter elite professions, such as journalism, politics, finance and law.

Along the way, they tend to marry each other and settle down in the enclaves mentioned earlier. Even in their free time, they often live on a separate plane of existence. For example, when they attend sports games, they don't sit in the bleachers with the masses; they watch the spectacle from the comfort of their skyboxes.

Consequently, as Carlson sees it, American elites are out of touch with their fellow citizens. And this leads to all sorts of problems, as we'll presently discover.

### 3. Elites are lacking in empathy toward poor and middle-class Americans. 

Empathy and proximity often come hand in hand. The more we are separated from other human beings, the less likely we are to empathize with them. After all, empathy is built on understanding other people and their afflictions — and it's difficult to do that if you barely even see them in your day-to-day life, let alone work, live or socialize with them.

In the absence of proximity, other people tend to become anonymous and mysterious to us. That, in turn, breeds feelings of indifference or even contempt toward them and their problems.

Such has been the case with the elites of American society and their attitudes toward their fellow citizens. Because they don't experience the problems that are besetting poor and middle-class Americans, they're largely oblivious to them. As the old saying goes, out of sight, out of mind.

If elite Americans send their children to private schools, drive luxury cars and use private medical facilities, then they won't have any firsthand awareness of the deterioration of public schools, public transportation and public hospitals. Of course, through media reports, they might learn about the problems plaguing poor and middle-class communities — but they can then dismiss these problems in one of three ways.

First, they can downplay the problems. For example, they can tell themselves that poor Americans aren't really _that_ poor. After all, they own smartphones and televisions — luxuries that make them relatively rich compared to impoverished people in developing countries.

Second, they can deny responsibility for the problems by shifting it onto the victims themselves. For instance, they can blame poor and middle-class Americans' poverty and economic decline on their alleged laziness or other supposed moral failings, such as drinking too much alcohol, smoking cigarettes and making bad decisions in life.

Third, they can cast the victims in a deeply unsympathetic light, which makes it difficult, if not impossible, to feel much empathy for them. For example, poor and middle-class white Americans can be discredited as racist and xenophobic bigots. That makes them seem extremely odious to anyone who holds the opposite values of inclusivity and multiculturalism.

Most elites not only view themselves as holding these latter values but prize themselves for doing so. However, as we'll see in the following blinks, Carlson thinks they're hypocrites in this regard.

### 4. Elites are hypocritical about racism. 

In the United States, the term _racial segregation_ is most commonly associated with the Jim Crow-era South. That era was the period of time stretching from the late nineteenth century to the mid-1960s, when segregation between black and white Americans was widely practiced and legally sanctioned in Southern states.

However, this association can be misleading. Although it's no longer openly practiced or legally sanctioned, a form of racial segregation in the US continues to this day — but not where you might expect to find it. Today, the most racially segregated communities tend to be those of the Coastal and Northern elites.

For example, consider the demographics of the elite's enclaves in Chappaqua, New York; Cambridge, Massachusetts; and certain neighborhoods of New York City, Los Angeles and Washington DC. All of these enclaves are overwhelmingly white. The percentage of their residents who are black ranges from a mere two to six percent.

Hispanic Americans also tend to be underrepresented in these enclaves. For example, the affluent areas of New York City and Washington DC where former mayor Michael Bloomberg and former president Barack Obama live are less than five percent and eight percent hispanic, respectively.

The schools of the states and cities in which the elites live also tend to be segregated. Children of color tend to go to one set of schools, while white children go to another. For example, the state of New York has the most segregated public schools in the United States, and 90 percent of New York City's publicly funded, independently run charter schools have student bodies that are more than 90 percent children of color.

Meanwhile, the schools of the elite in areas such as the Upper West Side of Manhattan and the Park Slope and DUMBO neighborhoods of Brooklyn are predominantly white. Efforts to make these schools more racially diverse by relocating them to more diverse neighborhoods have been rebuffed by the mostly affluent white residents of all three of these areas.

These very same communities espouse the values of inclusivity and multiculturalism — or at least claim to espouse them. Because their inhabitants cluster together into racially homogenous neighborhoods and then fight to protect the homogeneity of their schools, Carlson considers them hypocrites. They don't practice the values they preach to others.

### 5. Elites are deepening racial divisions by practicing a form of reverse racism against white people. 

Imagine you're a member of the American elite. The poor and middle-class members of your society are suffering in a variety of ways: income stagnation, outsourcing of manufacturing jobs, increasing rates of opioid addiction, decreasing age of mortality — the list goes on and on. Meanwhile, life has never been better for you and your peers.

In such a situation, you might expect that the less well-off members of your society start feeling anger toward you. After all, you and your peers are the ones who control the dominant institutions of society — so if things are going badly, you must also be the ones to blame!

Short of actually fixing people's problems, what do you do? Well, you need to redirect their anger away from you. And one of the easiest ways to do that is to convince them to redirect it at one another. Split them apart and set them against each other before they can rise up and unite against you. Divide and conquer.

And how do you do that? Well, Carlson argues that racism is one of the main ways in which the American elite have divided their subjects against each other. But this is not racism against people of color. To the contrary, it is a form of reverse racism against white Americans that is causing the division.

White people — especially working-class white men — are being demonized and scapegoated for nearly all of American society's problems by the US media and academia.

This can be seen in media headlines that refer to white people in negative terms _._ For example, the progressive news and opinion website _Salon_ published headlines that declared that "White Men Are the Face of Terror" and "White Guys Are Killing Us."

This reverse racism is also seen in a series of tweets and Facebook messages written by left-wing college and university professors that expressed anti-white sentiments. For example, a professor at the University of Pittsburgh stated that "we're all screwed because white people are in charge," and a professor at the University of Hawaii wrote that she didn't "trust white people."

### 6. Elites are destabilizing American society by encouraging immigration. 

As Carlson sees it, a country's national unity depends on its citizens' shared language, ethnicity, religion, culture and history. The more or less unified a country's citizens are in these respects, the more or less unified their communities will be as a result.

In Carlson's words, the United States "was overwhelmingly European, Christian, and English-speaking fifty years ago," but now "has become a place with no ethnic majority, immense religious pluralism, and no universally shared culture or language."

As a result of these demographic changes, the United States no longer feels familiar to native-born Americans — to the point where they may not even recognize their own hometowns anymore.

By its very nature, the human mind is unable to tolerate demographic changes of such a magnitude, meaning they provoke feelings of disorientation, nervousness, suspicion and hostility between members of different ethnic and racial groups.

From 1965 to 2015, the percentage of the US population that was white dropped from 84 percent to 62 percent. Carlson attributes this decline almost entirely to immigration.

Finally, the Democratic Party, the Republican Party and the elite in general have all encouraged immigration and are therefore to blame for the destabilizing effects that result from it.

For instance, every four years, the Democratic Party releases a new party platform. Since 2004, each iteration of the platform has become increasingly supportive of immigration. In that year, it called for creating a path to citizenship for immigrants who came to the US illegally. In 2008, it then called for an increase in the number of immigration visas given to skilled workers and their family members.

Meanwhile, the Republican-controlled Congress's 2018 government spending bill placed restrictions on the hiring of immigration enforcement agents for the Immigration and Customs Enforcement agency, more commonly known as ICE. It also earmarked $1.6 billion to border security, but prohibited spending any of that money on the construction of a border wall between the US and Mexico.

The elites support increased immigration due to various ulterior motives: Democrats want immigrants' votes. Wealthy Republican donors want cheap labor for low-paying economic sectors such as the meatpacking industry. The rich want that same labor to fill their households with affordable nannies, cooks, gardeners and housekeepers.

### 7. Elites are ignoring the problems of male citizens and practicing a form of reverse sexism against them. 

In Carlson's view, gender inequality in the United States is a thing of the past. In areas like income, quality of life, education and career outcomes, female citizens have already caught up to or even surpassed male citizens.

On average, women make 77 cents for every dollar that men earn. However, the difference practically disappears if you compare men and women who possess similar professional experience, work similar numbers of hours and occupy similar positions.

Looking at quality of life, the statistics are sobering: Men's life expectancy is five years shorter than women's. Men account for 77 percent of suicides and 73 percent of deaths from opioid overdoses.

As for education, girls and young women outperform boys and young men in terms of attending and finishing high school, college and graduate school.

And when it comes to professional advancement, men have been and continue to be disproportionately harmed by recent and ongoing economic developments. The first development is the loss of about seven million manufacturing jobs in recent decades, which has affected more than ten percent of working-age males.

The second development is automation, which threatens a disproportionate number of male-dominated industries, such as logging, agriculture and manufacturing. In contrast, three of the five fastest-growing occupations are female-dominated.

Today, male citizens are more in need of a helping hand than female citizens. However, public and private institutions, which are controlled by the elites, continue to promote programs that aim to redress gender imbalances on behalf of girls and women, rather than boys and men. In doing so, they're ignoring the plight of male citizens and essentially discriminating against them.

### 8. Elites are undermining the rights to free speech and assembly. 

Once upon a time, the liberal American elite supported their fellow citizens' rights to free speech and assembly, even when they disagreed with the way those rights were exercised. Nowadays, according to Carlson, they've given up that support and are undermining those rights.

A case in point is the American Civil Liberties Union (ACLU), a nonprofit organization that advocates on behalf of Americans' personal freedoms and has historically defended even extreme right-wing citizens' rights to free speech and assembly. For instance, in 1964, it defended the rights of Clarence Brandenburg, a minor figure in the Ku Klux Klan, who was arrested for inciting violence through a speech that was racist and anti-Semitic.

Now, fast-forward to the summer of 2017, when protesters gathered for a rally in Charlottesville, Virginia.

Initially, the ACLU defended the group's right to assemble, but it backpedaled on this defense when more than 200 employees of the organization protested it following the rally.

And there are other examples of how the elite has abandoned the defense of free speech. For instance, there were a series of incidents in which prominent right-wing figures, such as Milo Yiannopoulos, were prevented from giving speeches at American colleges and universities due to student protests.

There have also been two cases in which people lost their jobs due to their espousal of right-wing views. In one case, Brendan Eich was pressured into resigning from his position as CEO of the Mozilla Corporation in the spring of 2014, after it became known that he had donated money to the promotion of California's Proposition 8 — a referendum that banned gay marriage in the state in 2008.

In the other case, James Damore was fired from his position as a software engineer at Google in August 2017, after he wrote a memo that suggested that gender disparities in the tech industry were due to biological differences between men and women.

### 9. Elites are indistinguishable on issues of economics and foreign policy. 

As Carlson sees it, the two major parties in the United States have become indistinguishable from each other in two main respects.

The first is economics. In the past, the Democrats were the party of the American working class. One of its primary political goals was the amelioration of economic inequality. It fought for higher wages and defended the trade unions.

Nowadays, however, the Democrats are the party of the rich. Consider these two sets of statistics:

The first involves the voting patterns of the rich. In the 2016 presidential election, eight out of the ten most affluent counties in the United States voted for Hillary Clinton over Donald Trump. For instance, Clinton won by nearly 20 points in Fairfield County, Connecticut, which is one of the main hedge fund capitals of the world.

The second set of statistics involves the political donation patterns of the rich. For instance, in the same election, owners and employees of private investment funds donated $47.6 million to Clinton's campaign and associated groups, compared to the $19,000 they donated to Trump.

What's more, Democratic and Republican elites have also become indistinguishable on foreign policy matters. The left once protested the Vietnam War, but since then, the liberal elites have supported a number of military actions.

These include the 2001 invasion of Afghanistan, the 2003 invasion of Iraq, the 2011 intervention in Libya, the 2014 intervention in Syria and the ongoing drone strikes in Yemen, Somalia and Pakistan, which began during the Bush Administration and ramped up during the Obama Administration. To this day, the US remains militarily involved in all of these countries, with continued Democratic and Republican support alike.

As a result of this convergence between the two parties, a bipartisan consensus has emerged between Democratic and Republican members of the elite. This leaves Americans feeling that it no longer matters which party they vote for, because they get the same outcome either way.

Consequently, they have concluded that American democracy is a sham. And to express their outrage and opposition toward this sham, they cast the ultimate protest vote in the presidential election of 2016, voting for Donald Trump.

### 10. Final summary 

The key message in these blinks:

**Carlson lays out a provocative indictment for how the elites of American society are objectionable in a number of respects. They're out of touch, lacking in empathy and hypocritical about racism. They're also deepening racial divisions by demonizing white people, destabilizing American society by supporting immigration, ignoring the problems of male citizens by engaging in reverse sexism and undermining free speech. Finally, liberal and conservative members of the elite have come to a consensus on issues of economics and foreign policy — a consensus that leaves many Americans with the sense that their country's democracy is a sham.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _The Managerial Revolution_** **,** **by James Burnham**

Now that you've heard one perspective on the American elite, perhaps you want to hear another. You also may have some lingering questions: Who exactly are these elite? What defines them? And where did they come from?

James Burnham provides a classic account of the answers to these questions. While his book dates back all the way to 1941, his answers still prove instructive today. Essentially, he defines the elite, or what he calls the ruling class, as the group of people who enjoy a high degree of control over society's means of production. If you want to learn more about them, check out the blinks to _The Managerial Revolution_.
---

### Tucker Carlson

Tucker Carlson is the host of _Tucker Carlson Tonight,_ a talk show on the conservative Fox News Channel. He previously hosted shows on CNN, MSNBC and PBS, and he has also been a staff writer for the _Arkansas Democrat-Gazette_ and _The Weekly Standard._ He was one of the cofounders of _The Daily Caller_, a conservative news and opinion website.

