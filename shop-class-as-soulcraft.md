---
id: 565c347d9906750007000012
slug: shop-class-as-soulcraft-en
published_date: 2015-11-30T00:00:00.000+00:00
author: Matthew B. Crawford
title: Shop Class as Soulcraft
subtitle: An Inquiry Into the Value of Work
main_color: 7E191F
text_color: 7E191F
---

# Shop Class as Soulcraft

_An Inquiry Into the Value of Work_

**Matthew B. Crawford**

_Shop Class as Soulcraft_ (2009) is an eye-opening view into how working with your hands can transform your life. These blinks take a look at the changing nature of work, the value of manual labor and how choosing a trade, as opposed to a profession, might be your ticket to happiness.

---
### 1. What’s in it for me? Discover the perks of a job in the trades. 

When asked what they want their children to be when they grow up, most parents today would probably answer something like: a manager, a lawyer, an IT specialist. Many parents probably wouldn't be thrilled to hear if their kids told them: "I want to become a car mechanic." But, as you'll learn from these blinks, parents needn't be afraid of such ambitions. The job market is changing and the career prospects of skilled manual laborers are looking better and better compared with those of knowledge workers, i.e., those whose jobs are all about handling information. 

There's also the (very important) question of happiness. Sure, it sounds comfortable to spend your life at a desk and never get your hands dirty. But many students who opt for white-collar jobs are heading for a miserable working life trapped in cubicles, overworked, underpaid and deeply unsatisfied. These blinks make a strong case for the alternative: a career in the trades. They address office workers young and old who are tired of dealing with menial tasks that can never satisfy their yearning for meaning.

In these blinks, you'll find out

  * why working in a cubicle can be much more exhausting than working on an assembly line all day;

  * what Sherlock Holmes has in common with your local car mechanic; and

  * why you may tell hundreds of dirty jokes if you're in a crew, but not a single one if you're member of a team.

### 2. Even though we don’t fix and build things ourselves anymore, an interest in manual labor is on the rise. 

Can you recall the last time you worked on your car, built a piece of furniture from scratch or made your own clothing? Chances are it's probably been a while — if you've ever done it at all.

That's because the modern world is full of devices that discourage users from repairing or even understanding them. Many modern products are so complex and delicate that the majority of people are too intimidated to attempt repairing them. Just imagine trying to fix a problem with your flat-screen TV.

In addition, many contemporary products are designed to conceal their messy inner workings with a seamless facade. For instance, if you open the hood of a new German car you won't see an engine but a plastic case. 

Products are even put together in such a way as to discourage their disassembly. For instance, they might come with extra hardware that can only be loosened with uncommon tools.

Part of the reason we've become more passive and dependent on other people to fix things is due to these changes. We used to make things; now we buy them. We used to fix things; now we throw them away. 

It's not just that we're discouraged from making repairs — we might also have the feeling that doing things ourselves is economically wasteful. After all, imagine all the time and effort that would go into knitting an imperfect sweater when you could buy a perfectly good one for a few bucks. 

However, the interest in manual work is slowly but surely on the rise. It's clear that people are growing increasingly discontent with not understanding the products they use and depending on businesses to provide them with everything from food to clothing. 

On top of that, the rapidly deteriorating state of the economy is demanding more self-reliance. As a result, we're seeing people grow their own food, fix things themselves and even raise chickens on the rooftops of New York City.

> _"Does the use of tools answer to some permanent requirement of our nature?"_

### 3. A good education doesn’t guarantee a decent career anymore. 

Most people were raised with the knowledge that earning a college degree was their ticket to a comfortable, well-paying job. Unfortunately, this expectation is being broken for people everyday. That's because a successful career is increasingly determined by criteria other than educational level. 

According to the economist Alan Blinder, the so-called gap between educated and less-educated people is shrinking. Blinder predicts that, in the future, a more obvious distinction will be between people doing jobs that can be executed via the internet versus those that can't. Only people doing work that requires face-to-face interaction will be able to count on a stable career, such as doctors or other people who provide an in-person service. Because, as Blinder says, "You can't hammer a nail over the internet."

While this assumption isn't anything new, it threatens a huge percentage of the Western workforce. However, one group might come out on top: skilled manual workers. In fact, as early as the year 1942, renowned economist Joseph Schumpeter accurately predicted that, as education levels exceeded the job market's demands, wages for white-collar workers would quickly drop below those of skilled manual laborers. 

And this phenomenon is compounded by globalization. That's because the United States currently has 30 to 40 million jobs that could be outsourced to places like India or the Phillipines. And it's not just telephone operators that should be concerned: this process could affect everyone from scientists and mathematicians to clerks. 

In summary, a college degree no longer equals a stable job and, in 2006, _The Wall Street Journal_ even ventured to think that "skilled [manual] labor is becoming one of the few sure paths to a good living."

### 4. The separation of thinking from doing hurts both blue- and white-collar workers. 

Even if you've never had a factory job, it's easy to imagine how mind-numbing work on an assembly line could be. After all, spending eight hours a day tightening bolts is a sure route to boredom. 

Over the last hundred years or so, a marked division of labor was drawn that took the intellectual component out of many jobs. The first result was the degradation of blue-collar work. 

Here's what happened:

In the late 19th century, management genius Frederick Taylor told factory managers to break complex tasks, like building a bike, a car or a machine, into a series of simple actions. The result was that low-wage, unskilled workers could be given an exceedingly simple task to do, like tightening a bolt, which they would repeat hundreds of times a day. These unskilled workers began replacing higher-paid, skilled workers who knew how to build an entire bike or car by themselves and this drastically reduced labor costs.

Since the knowledge component had been eliminated from the work floor, the only people who needed to plan and understand the whole operation were a few managers. But the troubling reality was that the work conditions got worse wherever manufacturing was reorganized to fit Taylor's model. 

It wasn't long before white-collar jobs were also reduced to routine work. But how could intellectually demanding jobs be routinized? Doesn't knowledge work require employees to actually understand what they're doing?

Well, not always. Take laboratory medical analysis. You'd probably expect that the people who do this work require a high level of knowledge and skills. However, today, knowledge workers deal primarily with ideas, or _abstractions_. These can be broken into routines, or translated into step-by-step processes — or even computer code. 

As a result, laboratory analyses can now be conducted by semi-skilled technicians utilizing information technology. In fact, since most of the data is stored by devices, technicians are required to know less and less, which means they can be paid considerably less than experts. 

As we've seen, the state of work is undergoing fundamental changes and education is following suit. The result is that schools are preparing students for a miserable life spent confined to a cubicle.

### 5. Schools today are less focused on teaching – resulting in a disengaged workforce. 

Did you know that manual trades are now looked down upon by most schools? Shop classes have been slashed over the last 20 years, as educators fear that students working with tools and their hands will distract them from their _real goal_ : becoming knowledge workers. 

In fact, many schools actively discourage students from choosing a career in the trades, instead urging them to focus on becoming knowledge workers. Although the latter is more likely to be the route to a lifetime of job insecurity, educators are still inclined to prepare their students for theoretically oriented jobs.

That's because they assume these jobs will provide better career options than craft training, which is still viewed as limiting a person's potential if they want to switch professions down the road. This narrow focus means that many students go into higher education regardless of their true passions and talents. These students end up earning a degree that fails to prepare them for any specific career and locks them into a potentially lifelong struggle to hold a job. 

But that's not the only change taking place in schools. As we know, knowledge work itself now depends less on knowledge, and the knowledge workers of tomorrow are well aware. As a result, high schools are no longer working to impart knowledge to their students. Schools now have more of a social function: they award degrees and grades that are more like merit badges, granting their bearers access to further courses of study and theoretical professions. 

That means schools have become focused on competing with each other for grades, instead of actually teaching. In a climate like this, it's no wonder that students are disengaged with their schoolwork when their schools are so disengaged with learning!

> _"The disappearance of tools from our common education is the first step towards a wider ignorance of the world of artifacts we inhabit."_

### 6. Knowledge work, cubicle jobs and obnoxious management techniques alienate workers. 

After the author earned his master's in philosophy, he became a knowledge worker, joining a company called Information Access Company where he read academic articles, indexed them and wrote short abstracts that the company sold to libraries. But the author wasn't happy. 

He, like many knowledge workers today, encountered working conditions worse than most manual laborers. That's because, just like assembly-line workers, many knowledge workers are put under extreme pressure to meet deadlines. 

The author was expected to read up to 28 articles a day, summarizing them in abstracts! However, contrary to assembly-line work, cubicle jobs can't be done mindlessly. Knowledge workers are actually expected to switch their focus from one task to another very quickly — a sure route to exhaustion. 

Not surprisingly, such demands made the author feel constantly tired, stretching him to the limit. Plus, feeling exploited by his employer, the author had a hard time being loyal to him. It wasn't long before he was barely completing his workload and feeling less and less proud of his job. 

Yet another isolating force is the ubiquitous modern cubicle. 

Even the way managers speak to lower-level knowledge workers, i.e., those who work in cubicles, can be alienating. That's because, to deflect responsibility, some managers adopt language that is extremely vague but also demonstrates expertise. For instance, they might tell you to improve the "cross-marketing synergies in telecommunications."

Their strategy is golden because if the plan fails they can blame you for misunderstanding it. But when managers communicate in this way they drive a wedge between themselves and their team. It's irritating when someone doesn't bother to communicate effectively, especially if he's a leader. And doubly so if he's doing so to avoid taking responsibility.

### 7. Teamwork is more confining than work in crews. 

Humans have been cooperating forever, but the idea of teamwork among knowledge workers dates back to the 1970s. It was during this decade that people began standing up to the idea that one person should dominate their coworkers. The result was _teamwork_, a tool that empowers all employees to participate in workplace decisions. 

Unfortunately, teamwork isn't as empowering as people claim it is. That's because the members of a team are judged based on their individual behavior, but in a team of knowledge workers, it's hard to see the influence of any one individual team member. 

Take the success of the iPhone: it's impossible to say how any single member of the marketing team affected its success. Compare that to the work of a _crew_, like one that builds a house or electrical grid — you'd be able to see the specific work each individual of the crew was responsible for.

As a result, the standards for objectively measuring the work of employees on a team are insufficient. 

Therefore, managers resort to subjective criteria to determine if individual employees act like team players or not. Such criteria often demand adherence to a strict code of conduct, which depends on the _corporate culture_ of a company, that is, the values and attitudes the company holds and abides by, for instance, concerning things like respect. 

Team players are also supposed to be passionate about the goals of the team, putting its objectives ahead of their personal interests. But the list goes on: a company may want employees to display the right _mentality_ as well. For example, team players might be required to uphold strict standards of political correctness and refrain from profanity at all costs. 

On the other hand, the value of a crew is measured by its achievements and the skills of each member: a set of criteria that allow everyone a lot more freedom. But obviously, in the trades, standards for a job well done are more objective. As a result, there's no need for made-up criteria like displaying the "right mentality," which means crews are allowed greater freedom in their work. 

So, you know how crews are different from teams. Next we'll look at how a different type of labor can help you transform your work life for the better.

> _"Teamwork … depends on group dynamics, which are inherently unstable and subject to manipulation."_

### 8. Finding a meaningful job pays off big time. 

For a lot of people, work is just a means to an end — a way to do all the leisurely activities they would be doing all the time if they weren't working. For instance, a well-respected financial analyst works grueling hours on Wall Street for months on end so he can finally climb Mount Everest during his two weeks of vacation. 

What's the problem with this lifestyle?

First of all, it's a waste to spend a huge portion of your life doing something that you'd rather not be doing, something you might even consider meaningless. Another problem is that this way of working lacks coherence.

For example, said Wall Street analyst is switching between most of the year working a job he hates, which actually makes him repress his fundamental personality, and a few weeks of joy where he can let loose and be himself. The change between the two environments is like a switch between two completely unrelated lives and bound to mess with his sense of self. 

The good news is that, if you search hard for it, you can secure a job that is meaningful to you every day. And when a job is meaningful to you, you are more engaged in it. 

In fact, jobs like this often have things in common. The work being done obviously needs to be important, like saving lives or putting out fires, but these jobs also tend to work with real, physical things or living creatures. For example, doctors work with human bodies, teachers with kids, and firemen work in an intensely physical way with fire and buildings. 

Plus, if you consider your job meaningful, your life will be easier and more rewarding. Seeing your work as making a difference will make you more devoted to it. As a result, you'll have more new experiences and gain more expertise, which will make your job easier and more fun.

> _"We want to feel that our world is intelligible, so we can feel responsible for it."_

### 9. Working with your hands is satisfying and can enhance your sense of self. 

Wouldn't it be exhilarating to work in a think tank where you tackled complex and exciting problems all day, every day? Well, in the author's experience, it can actually leave you oddly unsatisfied. In fact, he was so unfulfilled working at a think thank that he left to become a motorcycle mechanic, a job he loves. 

Why?

It's got everything to do with the joys of manual labor. That's because, if you work with your hands, whether you build a house, repair a car or plant a garden, the impact you're leaving on the world is clear. 

Manual labor can thus be an amazing source of pride and even build your sense of self. For instance, imagine putting together a motorcycle part by part and seeing it gleaming in front of you, fully functional and seriously cool. How could you not feel proud?

And there's another perk: other people can see what you've achieved, too. As a result, you'll be aware that people recognize your competence and won't feel compelled to boast. 

In fact, if you work with your hands you'll be less likely to develop an inflated ego. That's because the more you focus on something, the more important it appears. Since manual labor usually has you focusing on something outside yourself, you'll be less inclined to overestimate your own importance.

But working with your hands helps you stay humble in another way: by making it difficult to deny your failures. For instance, if you repair a motorcycle only to see it sputtering flames from the exhaust, it's hard to say you did a good job. That's because you have an objective measure of your success right before your eyes.

> _"[When a] man who works recognizes his own product in the world … he recognizes himself in it." — Alexandre Kojève_

### 10. Manual work is intellectually engaging and helps build autonomy. 

It's commonly assumed that manual labor is just for those who are better with their hands than their minds. But that's absolute nonsense. In fact, the trades tax a worker's brain just as much as your typical knowledge work. 

That's because to excel in a manual trade you have to pay careful attention to the physical world and understand how it functions. For example, by practicing his craft, a carpenter becomes an expert in countless types of wood by observing their resilience through different weather and myriad other changes.

But doing things with our hands also requires sound reasoning. For instance, Mike Rose once wrote about surgery that "the surgeon's judgment is simultaneously technical and deliberative, and that mix is the source of its power." 

So, any manual craft, whether it's surgery or carpentry, depends on the practitioner thinking like a top-notch detective. When a motorcycle mechanic hears an engine rattling, his mind jumps into action, sorting out the possible causes and methodically considering the likelihood of each before going to work on the machine. 

In some ways, this process is even more demanding than, say, solving a complex math problem. That's because an algebra equation can be done almost mindlessly by following instructions laid out in a book. But the problems that craftspeople deal with evade any simple application of directions. That means identifying, understanding and solving them can only be accomplished through experience and the sound judgment it breeds. 

Finally, the more you build or fix things yourself, the more autonomous you are — because you'll be a lot less dependent on the market and other businesses. In that sense, working with your hands is liberating.

Our consumerist culture promises us a different kind of freedom in buying things, i.e., the freedom to keep our hands clean and to choose from a long list of options. The only issue with this type of freedom is: it won't do squat to boost your autonomy.

### 11. Final summary 

The key message in this book:

**The educational system and job market of the modern world are geared towards producing students and workers that do intellectually taxing work. But the truth is that both job security and satisfaction are much harder to come by in a cubicle than in a shop.**

Actionable advice: 

**Learn a trade, no matter what.**

Even if you're certain that you want to go to college, it's a great idea to spend your summer vacations learning a physical trade. That's because such experience will both help you learn new skills and grow into a well-rounded person. And who knows, your trade might end up being a more satisfying and lucrative career option than what your degree prepared you for.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Matthew B. Crawford

Matthew B. Crawford is a philosopher and motorcycle mechanic. He earned a Ph.D. in political philosophy and took a job at a Washington D.C. think tank but wasn't satisfied. So he changed course to open an independent motorcycle repair shop and is still pursuing academia as a research fellow at the Institute for Advanced Studies in Culture at the University of Virginia.

