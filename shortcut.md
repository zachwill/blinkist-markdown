---
id: 548f024b6532350009df0000
slug: shortcut-en
published_date: 2014-12-29T00:00:00.000+00:00
author: John Pollack
title: Shortcut
subtitle: How Analogies Reveal Connections, Spark Innovation, and Sell Our Greatest Ideas
main_color: EB8749
text_color: 9E5B31
---

# Shortcut

_How Analogies Reveal Connections, Spark Innovation, and Sell Our Greatest Ideas_

**John Pollack**

Analogies are powerful and persuasive tools of communication. They can deceive us, or lead us to the right conclusions. _Shortcut_ is all about how analogies work, how to use them effectively and how to differentiate between the good and bad.

---
### 1. What’s in it for me? Learn about the awesome power of the analogy. 

Trying to explain complex things can be very hard to do, especially to someone who has little knowledge of what we are talking about. To help us we have a little linguistic shortcut; the analogy.

Analogies use one thing to explain another, helping us impart knowledge to others. We use them a lot. If you've ever decided to take rain check, or told a friend "there is plenty more fish in the sea" you've used an analogy.

But they can be far more than just helpful for us to explain things, good analogies can propel innovation, start wars or elections. Reading these blinks you'll find out how to unleash the power of the analogy for yourself.

In these blinks you'll discover

  * why you shouldn't "declare war" on something you have a problem with;

  * how the second industrial revolution started in a slaughterhouse in Chicago;

  * why you should let Pandora's box go into well-deserved retirement; and

  * how a domino piece caused over two million casualties.

### 2. Comparisons between unrelated things – analogies – can play a key role in creative innovation. 

You probably know a bit about the history of the modern assembly line, where an unfinished product moves along a line of employees who each perform the same task over and over again. It's well-known it was invented by the Ford automobile company.

You might not know, however, how the Ford company managed to come up with this. The idea for the assembly line was actually inspired by an _analogy_.

In broad terms, an analogy is a comparison between two unrelated things. It offers explanation of the terms, or creates deeper understanding of them.

We use analogies everyday. For example, we compare the horrible sound of nails on a chalkboard to things we find annoying or grating. If we want to express that a group of people is noisy or destructive, we can compare them to a pack of wolves or other animals.

What does this have to do with the invention of the assembly line? Well, a good analogy can help kickstart great innovation.

The inspiration for the assembly line came when a Ford engineer named Bill Klann visited a Chicago slaughterhouse in 1913. While he was there, he noticed how the animals were quickly transported on trolleys through the various stages of the butchering process.

He compared this to the way his company produced cars at the time. Before the assembly line, each worker was responsible for putting together all the parts they needed, which was not especially efficient.

Klann imagined the workers approaching the cars like the butchers approached their meat instead. The assembly line was a huge success. The time it took to produce a single car was reduced by _eleven hours_, and several other industries in the country quickly followed suit.

### 3. A great analogy can deeply impact societal and political developments. 

You may be currently reading this on a smartphone, tablet or laptop. Each one of those has an operating system that's based on a simple analogy, the desktop.

In the early days of personal computers, operating systems were incredibly difficult to use. If you wanted to use a computer, you had to write lines of commands, meaning only people with programming skills had access to them.

A worker at Apple got an idea for changing this limitation. He imagined an everyday desk, which contained everything you'd need to work, like a typewriter, paper and a calculator. Then he applied that idea to the computer: it could have a screen where all the things you needed were readily accessible.

This brilliant analogy changed modern society forever. It allowed a complete revolution in personal technology. A great analogy has the power to change the world.

Analogies can also determine the policy decisions that politicians make. Political topics are often very complex, even for leading politicians. Analogies can help them make decisions more easily.

For example, President Eisenhower was unsure of how to deal with the communist infiltration of South Vietnam in 1954. Admiral Arthur Radford offered an analogy that argued in favor of an intervention: he wanted to prevent Southeast Asian countries from falling to communism like a row of dominos.

This "Domino Theory" became an established policy in the White House, and it led to two American invasions that killed over two million people.

Unfortunately, the analogy turned out to be wrong. Vietnam _did_ fall to the communists, but no other dominos fell afterwards and in fact, Vietnam became an ally to the US.

### 4. Analogies are not neutral; they deeply influence how we think. 

Have you ever lost a debate, even though you clearly had better points?

This is a rather common occurrence, and it's often the result of an analogy twisting the argument.

No analogy is neutral. They're all packed with subjective ideas or underlying connotations.

Consider the common analogy that compares the economy to an ecosystem. It seems straightforward at first. An ecosystem is a complex web of different actors, like flora, fauna and weather. So is an economy — its actors are things like businesses and stock exchanges.

It's not so simple, however. An ecosystem is unregulated. It works perfectly on its own, without any outside interference. So when you compare the economy to an ecosystem, you're actually implying it should be free of any government control.

If you're arguing _against_ a laissez faire position, you're in trouble if you accept this analogy. If you agree with it, you'll suddenly seem like you think the economy will work best if left alone.

This means you need to be careful with the analogies you use. They'll influence your approach to the subject.

A war analogy, for example, can be very powerful, and not necessarily in a good way. In war you have an enemy and violence, and a society experiencing war is in peril. It's vital to pursue a victory, and that might mean using tactics that would be immoral in normal situations.

Think about what this means when the US declares its problems with illegal drugs the "War on Drugs." Calling it a war limits the ways you can go about solving the problem. You have to stand your ground, and you can't give in. Some experts have suggested the drug problem could be improved if certain "soft" drugs were legalized, for instance. But how can you do that in a war? That would be surrendering, wouldn't it?

> _"Sometimes even one-word analogies, which sound innocuous enough, camouflage entire arguments."_

### 5. An effective analogy has to be relatable, but not to the point of becoming a cliche. 

When a character in a film tries some exotic new food, there's a fair chance she'll say, "Mmm, tastes like chicken." They almost certainly won't say, "Mmm, tastes like zebra." This probably doesn't come as a surprise to you. The zebra comparison just wouldn't be effective.

An analogy has to be relevant and understandable in order to work. If the audience can relate to it, it can be very powerful.

This happened in 1963, when Bruce Reynolds and his gang spectacularly robbed a mail train north of London. They made a run for it with the equivalent of $60 million in cash, in _less than twenty minutes_.

In an interview years later, Reynolds called the robbery "my Sistine Chapel." He meant that it was his masterpiece. He used an analogy to depict himself as a great artist.

The analogy worked tremendously well, and cemented his fame even further. The Sistine Chapel is one of the best-known pieces of Renaissance art, and even people who aren't familiar with Michelangelo still know it's a great work of art.

Reynolds wouldn't have said, "it was my London sewer system." Even though that's a great work of engineering and planning, no one associates it with creative mastery or beauty.

Comparison can't be trivial however, or they'll lose their power. Analogy need to emotionally resonate with people.

Some analogies are made so often they've become cliches. That means they don't hold the same power, so avoid using them.

Calling something Pandora's box, for instance, is a perfectly acceptable analogy from Greek mythology, but it's been used so much it's lost it's emotional charge. It won't shock anyone, or challenge them to think in a new way. Creative analogies are much more effective.

### 6. A strong analogy emphasizes the similarities between things more than the differences. 

Imagine you've been single for a while and decide you want a relationship, so you register on an online dating site.

What do you look for in people's profiles? Probably someone who's similar to you, with as few differences as possible.

Well, what works for partners also works for analogies. For an analogy to be valid, the similarities between the two things compared need to outweigh the differences.

An analogy played an important role when DNA analysis was developed. There was a lot of controversy surrounding it, as people couldn't agree on when it should be legal to take someone's DNA samples. Those who believed the police should be able to take DNA samples from crime suspects came up with the analogy that DNA samples were the "fingerprints of the twenty-first century".

Indeed, fingerprints and DNA samples are both ways to verify people and link them to traces at a crime scene. And like DNA samples, fingerprinting was also considered a revolutionary technology when it was developed.

However, there were important differences between the two. It takes weeks to analyze DNA, and it can't identify someone as clearly as a fingerprint. DNA samples could also be misused more easily than fingerprints.

Naturally, proponents of using DNA samples in crime scenes didn't stress these differences when they pushed their analogy. Instead, they emphasized the similarities.

In the end, the analogy was successful. It helped make DNA testing as commonplace as fingerprinting in crime investigations.

The success of this analogy was facilitated by a process called _structure mapping_. In structure mapping, we automatically apply our understanding of one thing to another. So if we understand how fingerprinting works, then project that onto DNA testing, we feel like we have a grasp on that too. We take the similarities into account, and dismiss the differences as irrelevant.

### 7. A good analogy makes use of abstractions. 

Have you ever heard an analogy that really hit the nail on the head, but you weren't sure exactly why? We can't always identify where this feeling comes from, but sometimes it's definitely there.

Often, the component that makes an analogy strong is its use of _abstraction_. Abstraction refers to the connections we identify between seemingly unrelated topics. Our brains are very good at _abstract_ thought.

Let's look at an example. In 1995, O. J. Simpson went on trial for murdering his ex-wife and her partner. Several pieces of evidence pointed to his guilt. There was even a pair of gloves that contained DNA from both him and the victims.

When Simpson was asked to try on the gloves in court, they appeared to be too tight. John Cochran, Simpson's defense attorney, used an analogy with the gloves that ended up turning the tide.

"If it doesn't fit, you must acquit," Cochran declared in his closing argument. He wasn't just referring to the gloves, however, but rather the whole trial. He was hoping to plant the idea that the whole trial had had unsuitable evidence into the jury members' minds. He aimed to create an abstract connection, and it worked: on October 3, 1995, O. J. Simpson was declared not guilty.

Sometimes an analogy can _only_ work through abstraction. This was the case with Bill Klann and his assembly line analogy.

Klann was inspired by the overhead trolleys that transported the animals and their carcasses from one area of the slaughterhouse to another. He viewed the slaughter process abstractly: the working pieces (carcasses) moved automatically from one worker to the next. Animal carcasses are quite different from cars, but why not do the same?

### 8. An analogy needs a coherent story to be believable and memorable. 

As humans, we're programmed to dislike uncertainty. Unfortunately for us, however, we live in a complex world that's always at least partly driven by randomness.

We use stories to cope with the ubiquitous flow of new information that's always bombarding us. A coherent story processes random information into something we can understand. For example, if we suffer from a sudden misfortune like an accident or crime, we can use the narrative of fate or destiny to make it sound more comprehensible.

Stories are so important to us because they respond to how the brain works. The human brain is always looking for a shortcut, and stories can provide one.

A good story offers us a memorable flow of information, complete with connections and causations. It's much easier to process than a jumble of facts or events.

So if you want to ensure an analogy is effective and memorable, tell it as a coherent narrative.

Bizarrely, sometimes _leaving out_ information can make an analogy clearer. People will accept incomplete stories more readily than incoherent ones.

For example, consider any discussion on economic growth. A typical analogy people use here is "a rising tide lifts all boats." It suggests that economic growth in any area of the economy will benefit everyone else.

This is massively simplistic, and inaccurate. In fact, economic growth in one area often leads to decline in another. However, the comparison itself makes sense, even though the information is very sparse. If you add more details, the analogy would lose its coherence, and it would be harder to remember or understand.

> _"It is the consistency of the information that matters for a good story, not its completeness." — Daniel Kahneman_

### 9. Analogies that resonate emotionally with people are the most convincing. 

How logical do you think you are? You probably feel proud that you always look at things carefully and rationally. We all think this way about ourselves. We consider our ability to reason the main thing that separates us from animals.

Yet we aren't quite as good at logical reasoning as we imagine we are. Our rational brain doesn't always direct our behavior.

In fact, one psychologist named Daniel Kahneman argues that the logical part of our brain often simply justifies decisions that the emotional parts have _already made_. Emotions, it seems, overpower reason.

This means analogies with emotional appeal are more convincing than those focused on logic.

John Roberts, a conservative judge appointed by George W. Bush, offered an emotional analogy in 2005 that illustrated this well. Liberal senators were concerned that the nation's highest court was becoming overstaffed with conservative judges. Roberts's confirmation hearing was thus a rather controversial event.

Defending himself, Roberts stated, "Judges are like umpires. Umpires don't make the rules; they apply them." This comparison struck an emotional chord with the American public. Baseball is the nation's pastime, and many people associate it with warm feelings or memories. Baseball also reminds people of tradition, and umpires remind them of fairness.

The analogy worked, and Roberts was promoted to the Supreme Court as Chief Justice.

The emotional appeal of the analogy caused people to overlook the faulty nature of it. Actually, judges in the Supreme Court _do_ make the rules — they aren't like umpires at all.

And after he was appointed, Roberts did just that. He changed the way political campaigns could be financed by corporations, for instance, which helped out the conservative cause. Roberts's analogy wasn't accurate, but it certainly served the purpose he wanted.

> _"Emotions, once triggered, are like a genie released from a bottle: hard to recapture and cork."_

### 10. Final summary 

The key message in this book:

**Analogies play an important role in great inventions, political processes, societal developments and daily conversations alike. They provide shortcuts for our mind so we can understand things easier, and they can certainly sway us emotionally. We have to be aware of their merits and pitfalls, as they play such an important role in our lives.**

Actionable advice:

**Appeal to your listener's emotions, not their logic.**

Emotions are more powerful than logical reasoning. If you want to persuade someone of your ideas, use an analogy that they can connect with emotionally. They're much more likely to be convinced if they feel a personal connection to your comparison.

**Suggested further reading:** ** _I is an Other_** **by James Geary**

_I_ _Is_ _an_ _Other_ demonstrates that metaphor is not just a literary tool but a fundamental way we have of discovering and making sense of the world. The author shows various ways in which metaphors are used in diverse fields, such as politics, economics, psychology, science, business and more.
---

### John Pollack

John Pollack was a presidential speech-writer for Bill Clinton, and a communications consultant for Fortune 500 companies and various public sector leaders. He's written many books on communication, including _Cork Boat_ and _The Pun Also Rises_.

