---
id: 56377bc36234650007900000
slug: shrinks-en
published_date: 2015-11-05T00:00:00.000+00:00
author: Jeffrey A. Lieberman, Ogi Ogas
title: Shrinks
subtitle: The Untold Story of Psychiatry
main_color: A92A22
text_color: A92A22
---

# Shrinks

_The Untold Story of Psychiatry_

**Jeffrey A. Lieberman, Ogi Ogas**

_Shrinks_ (2015) tells the story of psychiatry's astonishing development throughout the centuries. These blinks take us on a tour of the discipline's crude past, its strange and shocking therapies and its great improvements.

---
### 1. What’s in it for me? Learn all about the history of psychiatry. 

Did you know roughly one in three people will suffer from some sort of mental illness at some point in their life? Mental health issues are incredibly common in society, and yet not many of us know much of anything about treating them.

When we think of psychiatry, we usually imagine something stereotypical — a person lying on a couch, for instance, telling a doctor about their deepest desires. Of course, there is much more than this to the study and treatment of mental illness. So what is the truth about psychiatry? These blinks take us through the history of the practice, showing just how far we've come in the last 300 years.

In these blinks, you'll discover

  * why some psychiatrists want to know your dreams;

  * what role animal magnetism played in mental health treatment; and

  * why a seventeenth-century family outing may have included a trip to the asylum.

> _"Anyone who goes to a psychiatrist ought to have his head examined."- Samuel Goldwyn_

### 2. In the eighteenth century, reformers tried to improve the horrible, desolate asylums housing those with mental illnesses. 

Before psychiatric hospitals, life was nothing less than horrifying for those with mental illnesses. While some sufferers were lucky enough to receive care at home, most were destined for a vagrant life on the street. Many others had it worse and were forced to spend their lives in asylums. 

In the eighteenth century, asylums were filthy, dark and overcrowded. Inmates were left locked up in tiny cells for weeks, chained, often beaten with sticks and doused with icy water. As if that wasn't bad enough, patients, like performers in a freak show, were put on public display on Sundays. 

Even in better institutions, treatment was still appalling. Patients were subjected to a whole host of primitive medical practices — bloodletting, purging and blistering, to name a few — that were, at the time, standard. Thankfully, a few reformers were determined to change these conditions. 

In Europe, physician Philippe Pinel proposed a new, humane treatment of the mentally ill. In 1792, he became head of the Paris Asylum for Insane Men. There, he ended the practice of bleeding and purging patients, and removed their chains. 

Emphasizing the importance of clean, pleasant housing, he treated his patients with fairness and created a structured schedule of activities and light manual tasks that they followed each day. The purpose of this schedule was to give back to patients a sense of self-mastery.

In the United States, the physician and humanist Benjamin Rush established a benevolent approach to psychiatry not unlike Pinel's. Born in 1745, Rush was among the founding fathers of the United States. Few today know that he was also America's very first modern psychiatrist. Rush also unshackled his patients and forbade the beating of asylum inmates, and lobbied for the improvement of living conditions for psychological inpatients in the state of Pennsylvania. 

In the nineteenth century, more and more psychiatrists came to follow the example set by Rush and Pinel. Psychiatry was on its way to becoming a humane practice. Or was it?

### 3. Famous physicians tried to treat mental illness as if it were the consequence of some kind of blockage. 

Yes, Benjamin Rush was a humane psychiatrist — but this doesn't mean that all his theories and therapies worked out for the best. Rush, for instance, believed that psychiatric illnesses were the result of disrupted blood circulation, and treated them accordingly. 

Seeking to improve disrupted blood flow in the brains of schizophrenic patients, Rush strapped them into a "rotational chair," a frightening contraption resembling a merry-go-round, and spun them around until they became quite dizzy. Unsurprisingly, it didn't help. 

Still, Rush wasn't the only psychiatrist who believed in the relationship between mental illness and circulation. In the 1770s, German physician Franz Mesmer attempted to treat the "energy blockages" that he believed were at the root of mental illness. 

Mesmer thought all illnesses could be attributed to an insufficient flow of what he called "animal magnetism." By hypnotizing patients and then probing certain parts of their body to restore proper energy flow, Mesmer actually drove his patients to states of crisis. Afterward, their symptoms would vanish temporarily. The patients were, seemingly, cured. 

Mesmer's miracle work made him a celebrity, and he toured Germany and France to provide his services to even more patients. But in Paris, a scientific committee examined and denounced his methods. 

In the twentieth century, yet another physician attributed mental illness to an insufficient flow of energy. Wilhelm Reich posited that individuals became neurotic because they didn't experience sexual climax on a regular enough basis. 

Then, in the 1930s, Reich developed his notion of "orgones," a hidden cosmic energy that was released during orgasm and other physiological processes. Convinced that mentally ill people could be cured if the flow of orgone energy was restored in their bodies, his patients had to spend time sitting inside a wooden box that was supposed to collect cosmic energy. Following an FDA investigation in 1947, however, orgone accumulators were banned.

### 4. Sigmund Freud revolutionized our understanding of the human mind. 

In the nineteenth century, medical research improved by leaps and bounds — think germ theory and anesthesia. Psychiatry, on the other hand, was far removed from the rest of medicine, and didn't progress as quickly. 

But at the dawn of the twentieth century, a brilliant Viennese came up with a theory that revolutionized the field forever. Sigmund Freud's approach to psychiatry placed the subconscious mind at its center. Freud, born in 1856, was a student of Jean Charcot, one of the best neurologists of his time, and, like his teacher, Freud was determined to understand the inner workings of the mind. 

Freud hypothesized that our subconscious mind had a life of its own, hidden from our waking consciousness. The fact that patients, when in a hypnotic trance, could access memories that they couldn't recall while fully awake was compelling evidence for Freud's ideas. 

For Freud, the mind was like an iceberg: the largest part, the unconscious, is hidden from sight. But he also emphasized that our mind wasn't just split into conscious and subconscious parts. Instead, he believed the mind had three components. 

The first of these was the _id_, a source of selfish desires and instincts that we're all born with. From the _id_ develops the _ego_, which ensures that the impulses of the id are expressed in an acceptable manner, or, if the impulse is very strong, allowing for creative, temporary release. 

For instance, if you're hungry in church, your id may urge you to gobble down all the host wafers. Luckily, the ego is there to prevent you from behaving inappropriately and humors your id with fantasies about pizza until Mass is over. 

The third component _,_ the _superego_, has developed by the time we've reached the age of five. The superego incorporates the moral standards we've learned from our parents, in school and from the world around us. We experience the superego as the nagging voice that says, "You can't do that!" if we, for instance, are tempted to eat all those wafers in church.

### 5. Freud’s “talking cure” was created to cure mentally ill patients of their inner conflicts. 

Now we know how Freud explained the workings of the human mind. So how did he account for mental illness? Well, Freud maintained that strong conflicts exist between the three components of our mind. 

The id and the superego naturally have opposing goals. The id is naturally greedy, and wants to do what feels good. Often in opposition to the id, the superego wants to do what's morally right. If we're religious, our superego damns the greed of our id. In this way, one part of our personality effectively condemns another. 

Freud saw these conflicts as the root of psychiatric illness. Usually we develop ways to cope with our inner conflicts, by either _sublimating_ or _denying_ the desires of our id. If a man wants to have sex with his married boss but knows he shouldn't, he may _sublimate_ that desire by secretly writing erotic stories about his boss. Or he may _deny_ being attracted to his boss altogether. 

But what if these mechanisms aren't enough? Then the individual is at risk of developing a disorder. For example, unable to control his id, the employee may become obsessed with the fear of blushing in his boss's presence. This in turn can escalate into a full-blown anxiety disorder. 

Freud's groundbreaking theory of mental illness was also accompanied by a radically new approach to treatment: talking. His patients were encouraged to talk about everything that came to mind, even things they dreamed about. This unearthed hidden conflicts and allowed Freud to _psychoanalyze_ them. Most conflicts typically linked back to experiences from the patient's childhood. 

Freud's approach to therapy was designed to help the patient in two ways. First, it improved patients' understanding of their own conflicts, thus making it easier to cope. Second, it allowed patients to experience _transference._

Transference refers to how patients experience their therapist as a parent over the course of their treatment. Unlike their real parents, the therapist will not damn the desires of the patient's id, ultimately relieving the patient of guilt.

> _"To name it is to tame it." - Jeremy Sherman_

### 6. Thanks to Freud, psychoanalysis gained traction in Europe and the United States. 

Freud's theories enticed many European intellectuals. In 1908, upon founding the Psychoanalytical Society, Freud started attracting droves of talented followers, including Alfred Adler and Carl Jung. By 1910, psychoanalysis was highly popular throughout continental Europe.

In North America, however, things were moving a little more slowly. From 1909 onward, psychoanalysis gradually began to take root in the United States. Also in that year, Freud traveled to America to give a series of public lectures and receive an honorary doctorate. He made quite the impression, and won the support of influential Harvard professor James Putnam. 

Putnam founded the American Psychological Association (APA) in 1911. By 1934, psychoanalysis had its own section in the annual meeting of the APA, the premier professional organization for US psychologists.

As the 1930s drew to a close, psychoanalysis developed into a mass phenomenon. Alfred Adler and other eminent analysts had fled Nazi Germany and Austria for the United States, and, upon arrival, began to obtain professorships at leading universities and establish psychoanalytic institutes across the country. 

While traditional psychiatry catered to the severely mentally ill, psychoanalytic theory holds that most people would benefit from therapy. In response, more and more people sought help in private practices. By the 1960s, as much as 66 percent of all American psychiatrists worked within private practices, compared to just eight percent in 1917.

It wasn't long before psychoanalysis became the mainstream field in US psychiatry. By 1960, almost every major psychiatric position in the United States was occupied by a psychoanalyst. For 48 years, most APA presidents were psychoanalysts.

Of course, this had its impact on the training of future psychiatrists. Psychoanalytic theory became the core of all study programs in psychiatry and anyone wanting to practice outside of state mental institutions had to personally undergo a successful analysis beforehand.

> _"They don't realize that we are bringing them the plague." - Sigmund Freud_

### 7. Psychiatry became popular, but far-fetched approaches multiplied quickly. 

Psychiatry had been in need of a good, coherent theory of psychiatric illness and Sigmund Freud provided it, accompanied by a new kind of therapy. Freud's teachings spread throughout the world. But not all was well. Indeed, psychoanalysis had problems of its own.

Psychoanalysis is dogmatic and more faith-based than scientific. Apparently, Freud didn't regard his assumptions as hypotheses that required testing; analogous to papal edicts, they were not to be questioned! Scientific rigor was accordingly discouraged in psychoanalysis, and Freud even denounced several of his favorite pupils, including Otto Rank and Alfred Adler, for advocating opposing views.

Just like Freud, later psychoanalysts came to propound theories unbacked by significant scientific evidence. Post-Freudian psychoanalysts soon began to blame parents for all kinds of mental illnesses.

Psychiatrist Frieda Fromm-Reichmann suggested that almost every case of schizophrenia could be traced back to a smothering and rejecting "schizophrenogenic" mother. And anthropologist Gregory Bateson came up with the interesting "double bind theory" of schizophrenia. 

He assumed that children retreated into a psychotic fantasy world because their parents kept barraging them with conflicting demands, like telling the child to be quiet and then scolding it for being too docile when it obeyed. At the same time, autism was blamed on the aloofness of "refrigerator mothers." 

Ultimately, these creative explanations were not enough to successfully treat severe mental illnesses. Freud himself had declared psychoanalysis unsuitable for treating psychosis, as analysis depended on a good contact with reality and a sufficiently strong ego. Nevertheless, the first psychoanalytic hospitals opened, trying and failing to remedy psychosis with talk therapy. Patients, meanwhile, only continued to suffer.

> _"… Freud stands in a class of his own, simultaneously psychiatry's greatest hero and its most calamitous rogue."_

### 8. In the early 1900s, new and crude therapies targeted the brains of psychiatric patients. 

In the first decades of the twentieth century, individuals with severe mental disorders had very little chance of recovery and many were permanently institutionalized. Hoping to find solutions, some physicians began to take desperate measures. 

One Austrian physician, Julius Wagner-Jauregg, experimented with a highly speculative treatment — curing psychosis with fever. To induce a fever, he infected psychotic patients with germs, such as tuberculosis bacteria. This risky treatment was, of course, unsuccessful.

Nevertheless, Wagner-Jauregg continued developing his theory, and in 1917 began to use malaria parasites to treat patients suffering from severe psychosis arising from neurosyphilis. 15 percent of the patients died; the remainder now suffered from malaria.

But the fever bouts did in fact diminish the syphilis bacteria. Patients improved. Yet when psychiatrists attempted to use parasites to treat other forms of psychosis, patients only became sick. Despite this, Wagner-Jauregg received a Nobel Prize for his efforts in 1927. 

In 1935, Portuguese neurologist António Moniz and his coworker, Pedro Lima, conducted another experimental procedure, in which they surgically damaged the frontal lobes of 20 patients' brains. Why? To calm them down. 

Indeed, the procedure, now known as lobotomy, successfully made patients compliant, and institutions around the world adopted the method. Tragically, this procedure was soon used not only to calm disruptive patients but as a means of dampening the emotions of psychiatric patients who were less severely afflicted. 

The lobotomized patients were easier to handle, but their personalities were destroyed in the procedure, turning them into zombie-like beings. Moniz, like Wagner-Jauregg, also received a Nobel Prize for his work.

Moniz's procedure was even optimized, in 1946, by Walter Freeman, a neurologist who developed his own form of lobotomy-to-go. Using an ice pick-like device, he could quickly access and damage a patient's frontal lobes via the eye sockets. Freeman personally performed procedures on 2,500 patients, inspiring many other physicians to adopt his method, too.

### 9. Shock therapy was developed in the 1930s and is still used today. 

If a person suffers from epilepsy, her neurologist will try everything to prevent her from having seizures. Sometimes, however, the same neurologist, with that person's best interests in mind, will intentionally induce seizures in a mentally ill patient. It sounds unbelievable. But it isn't done without good reason.

In 1927, Walter Sakel, another Austrian psychiatrist, used insulin shocks to relieve a patient's symptoms. This soon became a standard treatment. It's logical, to an extent: our brains depend heavily on glucose. If blood glucose levels are critically low, we'll fall into a coma or suffer from seizures. Massive doses of insulin can reduce our blood glucose to such a level. 

In this way, insulin shocks can alleviate psychotic symptoms, albeit temporarily. Such treatments also have negative side effects; they damage the patient's brain, can cause morbid obesity or, sometimes, death. Seizures induced by the stimulant metrazol also diminished symptoms — but the convulsions were so strong that 43 percent of patients experienced broken vertebrae!

Eventually, psychiatry began to use electricity to induce seizures, and still does so today. In 1938, Italian neurologists Ugo Cerletti and Lucino Bini were the first to experiment with delivering electric shocks to patients. 

The treatment was shown to induce convulsions much like those caused by metrazol. But when patients regained consciousness, their symptoms were markedly relieved, especially in depressive patients. 

It didn't take long before this method was adopted in institutions around the world under the title _electroconvulsive therapy_ (ECT). Today, ECT is still used to treat severe cases of schizophrenia, depression and mania. With anesthesia, muscle relaxants and a decreased energy dose targeting specific parts of the brain, it's an effective and safe technique.

### 10. New medications were used to treat major psychiatric disorders in the 1950s. 

Back in the nineteenth century, patients that acted up would sometimes receive a calming morphine injection. In the twentieth century, psychiatrists had far more medications at their disposal to make patients sleepy and placid. Yet none of them allowed patients to return to a normal life in society. 

At least, not until 1950, when the first modern tranquilizer entered the market. Have you ever heard of the drug meprobamate or "Miltown"? It alleviates anxiety without causing drowsiness, and while uncommon today, it was the first psychotropic blockbuster. In 1956, as many as one in three prescriptions in the United States was for meprobamate. 

The first drugs that helped treat schizophrenia arrived next. In 1952 in France, an anti-allergic medication called _chlorpromazine_ was administered to a psychotic patient for the first time. The results were astonishing; the patient, a highly irritable and violent young man, was immediately calmed. After a few weeks on chlorpromazine, his behavior had improved enough for him to be discharged from the hospital. 

In the same year that chlorpromazine was released in the United States, the asylum population decreased, as even long-institutionalized patients showed improvements sufficient for discharge. Other drugs soon emerged to alleviate affective disorders.

Also in the '50s, a Swiss pharmaceuticals company developed the G-22355 compound. While not catchily named, the substance was powerful enough to relieve severe depression and, in 1958, it was released as _imipramine._ The world's first antidepressant became an instant global success. 

For patients who cycled between deep depression and extreme elation, however, antidepressants were not a suitable solution, and in 1949, Australian physician John Cade found that lithium carbonate, a cheap white salt, stabilized the patients' moods. Although the FDA didn't approve its use until 1970, lithium is now the first-line medication in the treatment of bipolar disorder.

### 11. Psychiatry came under fire during a wave of skepticism in the ‘60s and ‘70s. 

Perhaps you're familiar with the '70s film _One Flew Over the Cuckoo's Nest_? The lively and unruly protagonist, McMurphy, is committed to a mental ward that's tyrannized by a sadistic head nurse. In the film, drugs and electroshock treatment are employed to coerce, rather than heal, patients.

This film is based on the 1962 novel by Ken Kesey and it encapsulates the public's fears and misgivings about psychiatry. Back then, even mental health professionals were critical of their own profession.

In 1961, psychiatrist Thomas Szasz published a book, _The Myth of Mental Illness_, which argued that psychiatric disorders are invented by psychiatrists to charge patients for scientifically unproven treatments. He stated that odd behavior isn't a symptom of mental illness but a reaction to problems of society at large, and went so far as to liken involuntary mental hospitalization to slavery. Szasz attracted lots of followers, especially among the young, anti-authoritarian crowd. 

A little over a decade later, in 1973, the journal _Science_ published an essay by the psychologist David Rosenhan. It was titled "On Being Sane In Insane Places" and featured a peculiar experiment:

Eight sane people pretended to hear voices and were admitted to one of twelve psychiatric institutions. Upon admittance, each of them said they no longer heard voices. Though they exhibited no signs of psychiatric illness, all but one were diagnosed as schizophrenic. No psychiatrist detected the pretense. Rosenhan interpreted his study as proof that psychiatric hospitals cannot distinguish the sane from the insane. The article provoked a public outcry.

> _"By the mid-1970s, American psychiatry was being battered on every front."_

### 12. By the 1980s, diagnosis had become more objective and less speculative. 

The anti-psychiatry movement condemned psychiatrists, declaring them unable to distinguish the insane from the sane, citing Rosenhan's study as evidence. Psychiatry's credibility was in shambles. So what went wrong?

First off, psychiatric diagnosis had failed to meet strict scientific criteria for a long time. Instead, it was based on psychoanalytic concepts that were often vague, speculative and subjective. 

Psychoanalysts believed that symptoms were merely the superficial, changeable manifestations of underlying conflicts. The APA's _Diagnostic and Statistical Manual of Mental Disorders_ (DSM), which psychoanalysts used to form their diagnoses, clearly reflected this belief. 

But, of course, any speculation about inner conflicts is rather hard to back up, as there is little visible evidence available. On the other hand, a diagnosis based on symptoms is open to observation and thus fairly objective. This was the approach psychiatry would have to take if it wanted to survive. 

In 1980, the APA purged the psychoanalytic influences from its diagnostic manual. Diagnostic criteria for any disorder were developed based on the available body of scientific research on that disorder. 

In the future, psychiatrists were to rely on the symptoms of an illness and its temporal course, and not ask about the illness's causes. This was to ensure that physicians, regardless of their theoretic background, would produce more consistent diagnoses.

> _"Physicians think they do a lot for the patient when they give his disease a name." - Immanuel Kant_

### 13. New scientific discoveries have given psychiatry a bright future. 

For more than a century, biological psychiatrists yearned to find the bodily substrate of mental illness. They tested the blood of schizophrenics for toxic substances and checked their skulls for conspicuous abnormalities — all to no avail. Eventually, however, they made the breakthrough they'd been searching for. How? With the help of genetics and neuroimaging. 

Today, tools like PET, MRI and fMRI are old news. But these powerful imaging technologies have taught us a lot about the differences between the brains of the healthy and the brains of the mentally ill. For instance, MRI studies revealed that in patients with severe depression, the hippocampus is smaller than in healthy subjects. This discovery was a game changer in the treatment of severe mental illness. 

Similarly, genetic research has been vital in helping us understand, prevent and treat mental illness, as we uncover the influence of family relations on conditions such as schizophrenia. The rate of this mental illness in the general population is just one percent. But the likelihood of a person having schizophrenia rises to ten percent if one family member is schizophrenic. If both your parents are, your risk skyrockets to 50 percent.

Research found that people with mental illness often have either too many or too few copies of an otherwise normal gene, causing imbalances in the brain. For example, some of actress Glenn Close's relatives exhibited certain symptoms and, upon investigation using genetic analysis, were found to have an additional copy of a certain gene. 

This leads to an increased need of the vital protein _glycine,_ which prevents the brain from becoming over-excited. This knowledge opened up a range of more suitable treatment options. Some family members only needed to take supplemental glycine to see a rapid improvement in their condition. 

This is one of the first examples of personalized medicine, and represents an approach that will take psychiatry far. Though the field had an erratic and controversial past, thanks to new developments, its future is undeniably bright.

> _"For the first time in its long and notorious history, psychiatry can offer scientific, humane and effective treatments."_

### 14. Final summary 

The key message in this book:

**For decades, psychiatry has struggled to understand and treat the mentally ill. The discipline has had its fair share of mistakes, from horrifying contraptions to outlandish theories. But, thanks to powerful insights into the biology of mental health and the adoption of rigorous standards, psychiatry is already greatly improved.**

**Suggested further reading:** ** _Cracked_** **by James Davies**

_Cracked_ gives a clear and detailed overview of the current crisis in psychiatric science: malfunctioning scientific standards and the powerful influence of pharmaceutical companies have caused the overdiagnosis and overmedication of people all over the world.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Jeffrey A. Lieberman, Ogi Ogas

Jeffrey Lieberman, MD, is a former president of the American Psychiatric Association. He is the Lawrence C. Kolb Professor and Chairman of Psychiatry at the Columbia University College of Physicians and Surgeons.

Ogi Ogas is a computational neuroscientist. A former Fellow of the Homeland Security department, he's contributed to two successful science books about sex, including _A Billion Wicked Thoughts_. Ogas, a passionate game show contestant, once won $500,000 on _Who Wants to Be a Millionaire_.

