---
id: 59d26133b238e10006d63700
slug: side-hustle-en
published_date: 2017-10-05T00:00:00.000+00:00
author: Chris Guillebeau
title: Side Hustle
subtitle: From Idea to Income in 27 days
main_color: FFE941
text_color: 665D1A
---

# Side Hustle

_From Idea to Income in 27 days_

**Chris Guillebeau**

_Side Hustle_ (2017) explains that anyone can design and launch a profitable side project. It details how to generate an income in the short-term, with the resources you already have at your disposal, and without taking on the risk of quitting your day job.

---
### 1. What’s in it for me? Bring in a little extra cash on the side. 

"Take the leap! Quit your day job and be your own boss!" Following this type of entrepreneurial advice comes with a fair share of risk. And let's face it: not everyone who tries self-employment thrives as their own boss. So what if you have an inner-entrepreneur who wants to venture forth, but you also want to keep the benefits of your current job? There's a lot to be said for health insurance, regular feedback and steady deadlines.

Well, maybe it's to consider becoming a side hustler!

In these blinks, you'll find out precisely what it means to have a business in addition to your day job, as well as learning the dos and don'ts of having a successful side hustle. From finding the perfect idea for your part-time business to optimizing your income from it, these blinks will make you into a side hustler in no time.

You'll also learn

  * how obscure hobbies, like writing fish tank reviews, can give you an extra income;

  * how some pretty simple math can prevent your making business mistakes; and

  * why Girl Scouts have a lot to teach the business community.

### 2. A side hustle provides a form of job freedom that absolutely anyone can attain. 

One day, a British construction manager decided to start writing reviews of fish tanks. The reviews included hyperlinks to Amazon product listings, and he knew he'd get a small commission if readers clicked through and purchased them. But the reviews were posted on an obscure website and, busy with his other day-job work, he half-forgot he'd ever written them. So he was pleasantly surprised when, several weeks later, he received a check for $350. Even now, years down the road, he's still getting $700 a month for the same reviews. In short, he has the perfect _side hustle_.

A side hustle can be defined as a profitable business venture that operates as an adjunct to other paid work or employment. No more than the bare minimum amount of time, money and effort should be invested. It shouldn't be a big deal.

You can even think of it as a kind of job security. The days of a "job for life" are over, and a side hustle ensures several incomes from different sources. You simply won't be held to the whims of a single employer whose loyalty to you can't be counted on.

Everybody needs a side hustle. It can make transitioning from your day job easier, too, if you decide to quit or are fired. Leaving may seem exhilarating in the abstract, but the reality can be difficult: you'll lose a reliable income source and health insurance. A side hustle gives you a taste of entrepreneurship, but without all the risks of going it alone.

There's also no reason to feel daunted when beginning a side hustle.

For starters, you don't need to spend a lot of time doing it. No more than an hour a day maximum. Any more is probably a waste of time. Secondly, there's no need to have a business degree to get going. Remember, it's _your_ business you're running, not someone else's — you're the one setting the entry requirements!

So those are the basics. But what kind of idea makes for the best side hustle?

> _"A side hustle is like a hobby, with one big difference: most hobbies_ cost _money and a side hustle_ makes _money."_

### 3. Strong ideas for strong hustles arise from careful questioning and a bit of math. 

The adage has it wrong; money _can_ grow on trees. But growing a money-bearing seedling requires planting the right seed under the right conditions.

All it takes is a little thought, however, and you'll find those fecund and productive ideas that are bound to blossom. To get going, it's important to recognize that hustle-worthy ideas share three qualities. They need to be feasible, profitable and persuasive.

If you can answer yes to the following three questions, then the idea is _feasible_. Does your idea motivate you? Will it earn you money? Can it be accomplished in a short period of time?

How do you know if your idea is _profitable_? Well, try explaining the merits of your proposal to potential customers in two sentences. No luck? Then your potential customers won't become paying customers any time soon.

An idea is _persuasive_ if your customers can't say no. Consider Julia. As a caricaturist, she was earning $100 an hour as a side hustle. But when she started sketching with digital drawing technology, she found she could charge $250 an hour. This was possible because few customers had seen the technology before. The novelty gave her a persuasive edge over the competition, and that wow factor was irresistible

So once you've got some feasible, profitable and persuasive ideas, you've got to do some basic math. You should calculate the _projected profit_ of each hustle with this equation: "anticipated income minus anticipated expenses."

Put this way, making a profit is easily understood. You should spend less money on your hustle than you bring in. You should also calculate the answer twice. One should be a conservative projection, the other an optimistic one, depending on the predicted strength of possible outcomes.

So now that you have an idea, how do you go about pitching it?

> _"A high-potential idea will bring in income not just once but on a recurring basis."_

### 4. Transform your side hustle idea into an offer with a price, a pitch and a promise. 

You might think it'd be difficult to make a lot of money from giving guitar lessons as a side hustle. After all, there are already plenty of guitar teachers doing the rounds. But Jake earns $6,000 a month doing exactly that. He can make this much because his _offer_ trumps the rest.

Once you've got your idea, you can turn it into an offer. Every offer includes three elements: a promise, a pitch and a price.

The _promise_ is a bold statement that tells customers how they'll immediately profit — that is, how you'll change their life. Jake promised "The most awesome guitar lessons in the universe."

A pitch tells customers all they need to know, with no irrelevant details. Jake's pitch was "The typical goal is to have fun (always first and foremost), as well as learn the instrument, all while maximizing efficiency so we meet your goals."

Your price communicates the cost, and should also include a "call to action." A tag like "phone this number" or "click this button" should do the trick. It needs to be easy and obvious.

The best offers also create a sense of urgency. Your potential customers need to think they want your hustle _immediately._

A good way to do this is to ensure you respond to queries from customers quickly and efficiently. A study by the Harvard Business Review found that companies who responded to a customer's request for information within an hour were seven times more likely to get business.

Another technique for communicating urgency is to use the color red. Highlighting words like "now" or "today" works wonders. Finally, if you're selling online, you'll find that a countdown on the checkout screen is great for hurrying customers along.

Okay, we've covered the things that constitute an offer, so now let's think about the kind of tools you'll need to get up and running.

### 5. Your side hustle needs resources. Make a shopping list and prioritize what’s required. 

One particular Valentine's Day, Sarah spotted a gap in the market. No one was selling custom-printed candy hearts. So she got down to it herself and within days had multiple orders. When her supplier wasn't able to keep up with demand, Sarah found her own printing machine to ensure the business kept chugging along.

If you, like Sarah, want to be resourceful and have the right attitude for rapidly growing your hustle, you need to have the basics covered. You need a resource shopping list which will include the following.

First, a _website._ This is your online home, and a content-management system such as Wordpress can make setting it up easy.

A _social media profile._ You needn't be operating on every single platform — just one or two should be fine. However, do register your side hustle's name with the most popular platforms, such as Facebook or Twitter, irrespective of your current reach.

A _scheduling tool._ A side hustle is a time-based commitment and time management is critical. A scheduling tool means you'll spend less time organizing meetings with colleagues and customers and more time working, as online scheduling applications are designed to display mutually available slots.

A _payment system_. Be sure to have an invoicing system, PayPal account or shopping cart on your website before you launch.

Once you have these four elements sorted out, you should prioritize providing more value and generating more money. Value is best improved by responding to customers' unspoken needs. For instance, say you've been walking a man's dog while he's on vacation. Maybe that same man needs regular dog care when he's back in town? Ask him.

Money can be generated through regularly scheduled price increases. Don't be afraid of doing this — customers will understand. After all, once your business has proven its reliability, you need to charge a fair price.

### 6. Sell your side hustle effectively by understanding its benefits and involving the right people. 

Each spring, Girl Scouts can be seen selling boxes of cookies at malls and outside supermarkets all over the United States. Their pitch is simple: "Would you like to buy some Girl Scout cookies?" They sell them by the truckload — 200 million boxes every year, to be precise. So what can a side hustler learn from the Girl Scouts? Well, they sell so many cookies not just because the cookies are scrumptious but because people _know_ that they are.

When you're selling a product or service, you've really got to emphasize its merits. Lead with the benefits. You might tell customers that your product will make them happier or their lives simpler and better. Ideally, you should connect with people's emotions.

Consider a dog sitter. She might not explicitly try to assuage an owner's guilt about leaving his dog at home alone, but she can imply it. A carefully crafted pitch might go "Leave your dog with me and he'll feel loved and cared for." Be subtle.

Once you're able to distill your side hustle's benefits, you should reach out to four types of people who can help you along the way. There's no need to do your solo side project alone.

First, find s _upporters_. Most likely this means your family and friends — people who can contribute in different ways and support your efforts. Second, seek out m _entors_. These are guides or experts who can give you feedback and advice. Third, identify _influencers_. These are trendsetters who'll spread the news about your product. Trusted authorities, like reviewers or bloggers, are generally best for this. Fourth, locate some _ideal customers_. These people are perfectly placed to evaluate products and respond to questions you might have with honest and detailed answers.

Once you've established this network, you're sure to go far.

### 7. Identify what’s working best in your side hustle. Then do more of it. 

Often, when business owners are asked how their businesses are going, they'll just respond with a perfunctory, "Oh, it's fine." That's not the right answer! A business is never in stasis. It's either on an upward trajectory — or it's sinking.

It's important, in the early stages of your hustle, to know exactly what sort of trajectory your business is on.

Once you're up and running, ask yourself a simple question: is your venture making money? There are three possible answers.

First, you might find you're far exceeding initial expectations. Fine, that's great. You've obviously got to keep going. Second, you might think your original idea was good, but people haven't latched onto it. It's difficult to admit it, but that's the time to cut your losses and move on. The most common response in the early stages is the third. You've found your idea hasn't completely gained traction, but as it's making a bit of money it doesn't make sense to pull the plug.

If this last option sounds familiar, but you're still not certain how to finesse the problem areas in your hustle, then have a look at your metrics. Metrics are measured in three areas. Profit — that's income minus expenses. Growth — ask yourself how many new customers or what new prospects you have. And time — how long do you spend on your hustle each week?

Once you've identified areas of concern, you can improve on them by applying two basic rules. In the first case, do more of what's working. In the second, abandon what's not. It's universally tempting to try to solve a problem. But don't. Really. In the author's experience, the most successful people just drop them and concentrate more on those aspects that function best.

Well, that's it! You have all the tools you need to start your side hustle. From here, the only way is up.

### 8. Final summary 

The key message in this book:

**Absolutely anyone can create and launch a successful part-time business. It doesn't require much time, money or effort to start one, and it doesn't mean quitting your day job. A side hustle is a great idea. It gives you an extra paycheck, but without the terrifying risks of being a self-employed entrepreneur.**

Actionable advice:

**Create a workflow for your side hustle.**

When working out how your customers will purchase your product or service and receive what they've paid for, it's useful to write a list of processes that need to take place along the way. This list of processes is called a _workflow_. When you make this master list of activities, actions and next steps, you should consider:

  * How will prospective customers learn about your idea?

  * What will happen immediately after the customer registers for or purchases what you're selling?

  * What else needs to occur in order for the customer to purchase and actually get your product or service?

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The $100 Startup_** **by Chris Guillebeau**

_The $100 Startup_ is a guide for people who want to leave their nine-to-five jobs to start their own business. Drawing from case studies of 50 entrepreneurs who have started microbusinesses with $100 or less, Guillebeau gives advice and tools on how to successfully define and sell a product, as well as how to grow your business from there.
---

### Chris Guillebeau

Chris Guillebeau is an author, blogger and speaker. He wrote _The $100 Startup_, which was a _New York Times_ best seller _._ He also runs _The Art of Non-Conformity_, a blog which is among the 15,000 most visited websites in the United States. In 2013, he completed his plan to visit every country in the world.

