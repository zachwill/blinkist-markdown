---
id: 580ba383d4d2ce0003dd24a1
slug: silent-spring-en
published_date: 2016-10-27T00:00:00.000+00:00
author: Rachel Carson
title: Silent Spring
subtitle: None
main_color: 37B646
text_color: 206928
---

# Silent Spring

_None_

**Rachel Carson**

_Silent Spring_ (1962) is about humanity's misguided attempts to control nature through the use of chemical pesticides. Find out how our using manufactured poisons to kill unwanted pests disrupts the delicate balance of nature and threatens our very existence. Ultimately, these poisons have the power to destroy the environment, infect the food we eat and contaminate our very lives.

---
### 1. What’s in it for me? Take a look at the spring of the environmental movement. 

The second verse of Joni Mitchell's "Big Yellow Taxi" starts with the lines "Hey farmer, farmer put away the DDT. Give me spots on my Apples and leave me the birds and the bees." The inspiration for Mitchell's lyrics was the burgeoning environmental movement, which owed many of its ideas and its strength to the shocking revelations contained in these blinks.

Though it took place more than 50 years ago, the story told in these blinks still has great relevance today. Indeed, recent years have been the warmest on record, and biological diversity and ecological resilience are being put to the test on a daily basis. These blinks are a stark reminder of Mitchell's iconic lines: "Don't it always seem to go, that we don't know what we got 'til it's gone."

In these blinks, you'll find out

  * what effect DDT had on robins at the University of Michigan;

  * how hen eggs that are not treated with pesticides can still be poisonous; and

  * why killing a harmful insect can be completely counterproductive.

### 2. The development and use of man-made poisons to kill pests increased dramatically after WWII. 

Unless you enjoy studying insects, you probably look at bugs as an unwanted nuisance. This can be true for farmers as well, many of whom are eager to rid their land of crop-eating pests.

So, in the years following WWII, many synthetic poisons were created to combat these pesky bugs.

The poisons that emerged during this period were a byproduct of the work WWII scientists were doing in the field of chemical warfare. As certain chemicals from war were discovered to also be lethal to insects, chemical _pesticides_ were developed to target what we deem to be "pests," including insects, weeds and rodents.

Over 200 chemicals were developed in the period between the mid-1940s and the 1960s. And during this time, pesticide production skyrocketed to five times its previous rate; in 1947, we produced between 124,000 to 259,000 pounds of pesticides, but by 1960 we were up to 637,000 to 666,000 pounds.

These chemically manufactured poisons were also far deadlier than the ones used in the past.

Earlier forms of pesticides used organic chemicals like arsenic, a highly toxic mineral that remained a basic ingredient in a number of weed and insect killers in the early 1960s.

But studies showed that even arsenic has toxic, carcinogenic side-effects. Areas that have been contaminated by arsenic have caused sickness or death in a variety of animals, including horses, cows, goats, pigs, deer, fish and bees.

The chemical compounds that followed, however, would prove to be even more dangerous.

This is especially true for _dichloro-diphenyl-trichloro-ethane_, a popular ingredient in pesticides that is better known as DDT. Though it was first synthesized by a German chemist in 1874, its life as an insecticide didn't begin until 1939.

Modern pesticides like DDT are used in the form of sprays, dusts and gases — and once it enters its target's body, in whatever form, it causes insidious and often deadly damage.

These chemicals destroy enzymes that protect the body, prevent oxidation, cause various organs to malfunction and infect cells, slowly causing irreversible and malignant damage.

### 3. There is undeniable evidence that chemical pesticides have an unintended and deadly impact on nature. 

Scientists did come up with some generally effective ways of killing pests, but just as arsenic had some pretty bad side effects, so did these new chemically manufactured pesticides.

While there are a number of ways chemicals like DDT might cause unintentional harm, a primary concern is how it can damage the environment by entering our water systems.

Harmful chemicals can pollute our waters in a variety of ways. Hospitals and laboratories produce radioactive waste; nuclear explosions spread poisonous fallout; cities and factories dispose of domestic chemicals; and then there are the pesticides we spray on gardens, crops, forests and fields.

Water purification plants remove some harmful substances, but synthetic chemicals like DDT can escape detection.

For instance, a sample of drinking water taken from a Pennsylvania orchard, and tested on fish in a laboratory setting, was found to contain enough insecticide to kill every fish in just four hours.

And a stream containing water that had drained from a sprayed cotton field was also lethal to fish, even after it had gone through a purification plant.

There are numerous other case studies that highlight the amount of damage these chemicals can cause in animals.

Birds are especially vulnerable. All across the United States, birds have lost the ability to fly and been left paralyzed and sterile by chemical spraying.

In 1954, DDT was used on the Michigan State University campus to protect trees from Dutch Elm Disease, a fungal infection spread by elm bark beetles.

The following spring, robins across the campus were found dead, dying or incapable of reproducing. And when healthy new migrant birds appeared, they would also be dead or dying within a couple of weeks.

It turned out that the birds were eating poisonous earthworms that had fed on leaves that were sprayed with DDT. Experiments revealed that the epidemic of dead birds was a direct result of DDT ingestion, with the tissue of the dead birds containing high levels of the chemical pesticide.

> _"They should not be called 'insecticides,' but 'biocides.'"_

### 4. By working their way into our food chain, pesticides are a poisonous threat to humans as well. 

As you might imagine, what is lethal to fish and birds is probably not good for humans. And, of course, that's true: chemicals like DDT are an insidious danger.

Nonetheless, in the early 1960s, it was a common misconception to think of these chemicals as being harmless.

After all, many people only knew of DDT as a powder that was used in wartime to kill the lice that would plague soldiers, refugees and prisoners. Since it was applied directly to the hair and skin, many people assumed that such chemicals must be safe.

When used in its powder form, however, the chemical is not easily absorbed through the skin and therefore it is far less dangerous. But when it's dissolved in oil to be used as a spray or gas, DDT is dangerously toxic.

Even the smallest amounts can cause irrevocable harm. Experiments on animals have shown that just three parts per million can inhibit an essential enzyme in the heart muscle, while five parts per million cause liver cells to disintegrate.

While there was still much testing to be done in the early 1960s to determine how harmful chemicals like DDT were to humans, the author knew that the potential was high.

In fact, at that time, the average amount of exposure to DDT was already far exceeding the levels known to cause harm to the liver and other organs and tissues.

Those working in environments that offered direct exposure to DDT, such as agricultural workers, showed levels of 17.1 parts per million; and workers at insecticide plants had 648 parts per million!

But even people with no known exposure to DDT were still showing an average of between 5.3 and 7.4 parts per million.

This was due to the poison entering our food chain: DDT was used to protect fields of alfalfa; that alfalfa was used to feed hens; the hens then lay eggs contaminated with DDT, and we ate those eggs.

### 5. The use of pesticides also destroys the delicate balance of ecosystems, doing more harm than good. 

At this point, given how harmful pesticides are to the environment, you might be wondering how effective they are at doing their intended job. Amazingly, even here they tend to cause dangerous problems.

Part of the problem stems from the fact that these chemicals kill indiscriminately.

This means that they don't just kill their target; they also exterminate the predators that naturally go after these pests.

As a result, poisons like DDT end up disrupting nature's own system of checks and balances, which only functions properly when there's a balance of predators and prey.

A classic example of this system breaking down involves the Kaibab deer in Arizona.

There was once a time when the deer population in this area was living in harmony with its environment, thanks to a variety of natural predators like coyotes, wolves and pumas.

But then a misguided campaign was launched to help protect the deer by killing off these predators. As you might guess, once their enemies were removed, the deer population grew out of hand and there wasn't enough food for them to survive. Now the deer were starving and dying at a higher rate than before, and the entire environment ended up being damaged due to their desperate search for food.

This is exactly the kind of problem that can arise with chemical pesticides. Even when they are reasonably effective in killing their target, they can unleash a whole new pest-related problem that wasn't there before.

This is what happened in 1956, when the US Forest Service sprayed 885,000 acres of woodlands with DDT to combat the spruce budworm pest.

The following summer it became apparent that the spraying had caused an even greater problem: the DDT had also killed the natural predators of the spider mite.

As a result, the spider mite turned into a worldwide pest, spreading at such a rate that they damaged the majestic trees of the Helena National Forest and the slopes of the Big Belt Mountains.

> _"'Man can hardly even recognize the devils of his own creation.'"_ \- Albert Schweitzer

### 6. To combat the harmful effects of these chemicals, we must learn to be responsible and find holistic alternatives. 

You might be asking yourself: How did these dangerous chemicals ever make it into production and use?

Unfortunately, there was a lack of proper tools and government regulation when they were being introduced.

In the decades following WWII, there were no procedures for chemists to test for these kinds of pollutants and no way to remove them from the water we use. Therefore, chemically polluted water went undetected and the FDA and Department of Agriculture did little to follow-up and determine if our food was being contaminated.

And without protective regulations, the government itself often launched chemical spraying projects without conducting adequate research into potential dangers.

Meanwhile, pesticide companies simply claimed that their products were harmless and helpful.

To correct this kind of harmful behavior, we need to take more responsibility for what we're using and how much of it is being used.

While the author's concerns date back to the 1960s, the same problems exist today. Not enough research goes into healthier alternatives such as biological pest control, and such alternatives certainly aren't used enough. These include mass sterilization of pests, and employing parasites, predators, pathogens and pheromones to control the population and lure pests away.

Approaches like these not only avoid the pitfalls of chemicals, they are also less dangerous to us and the environment.

We can no longer afford to ignore nature's warnings and the damage being done by mankind's chemical campaign against pests.

Chemical spraying is just one of humanity's many misguided attempts to control nature, and it has resulted in devastating damage.

But it's not just the fact that people are being contaminated; it's that most people are unaware of the dangers around them and that so little attention is being given to the harmful side effects of pesticides.

At the very least, we must stay informed about the chemicals being used on our environment and our food and the risks that come with them. Further, it doesn't hurt to question the human impulse to control nature that fuels us to use them in the first place.

### 7. Final summary 

The key message in this book:

**Humanity has a long history of trying to control and dominate nature, rather than living harmoniously with it, and this has resulted in the creation of many destructive forces. One such force is the dangerous use of pesticides. While this was intended to rid us of weeds and pests, it has actually had a much more detrimental effect on all living things. We must introduce stricter regulations on the use of pesticides and, at the very least, educate ourselves on their harmful impact.**

Actionable advice:

**Consider alternatives to poisons!**

Next time you're faced with the decision of whether to deal with household pests with chemicals or through natural means, don't forget to consider the potential harmful side effects of using poisons. They can do lasting harm to your health, so be careful and do your research before spraying them in your home.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Sixth Extinction_** **by Elizabeth Kolbert**

_The Sixth Extinction_ (2014) chronicles the history of species extinction and shows how humans have had more than a hand in the rapidly decreasing numbers of animal species on earth. Through industrialization and deforestation, not to mention climate change, humans have damaged the environment and disrupted habitats, leading to a massive reduction in biodiversity.
---

### Rachel Carson

Rachel Carson (1907 - 1964) was a writer and marine biologist who spent much of her life working with the US Fish and Wildlife Service. As the author of many acclaimed books on nature, she was an early and vital voice for environmental causes. Her other books include _Under the Sea-Wind, The Sea Around Us_ and _The Edge of the Sea._

