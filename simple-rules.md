---
id: 55ed536211ad1a00090000ad
slug: simple-rules-en
published_date: 2015-09-11T00:00:00.000+00:00
author: Donald Sull and Kathleen Eisenhardt
title: Simple Rules
subtitle: How to Thrive in a Complex World
main_color: 41B14B
text_color: 2E7D35
---

# Simple Rules

_How to Thrive in a Complex World_

**Donald Sull and Kathleen Eisenhardt**

_Simple Rules_ (2015) contains the ultimate rules of thumb that'll guide you to your goals in your professional life, and your personal life too. The tips provided in these blinks will help you rediscover simplicity in an increasingly complex modern world, without committing you to a crazy checklist that takes over your life.

---
### 1. What’s in it for me? Achieve more with just a few Simple Rules. 

If you want to change your life or get tighter buns, there's a world of guidebooks and expert websites out there to help you. Just plop yourself down with a nice cup of coffee and some cookies and start reading. A few hundred pages in, it's dark outside, you've forgotten half the tips you've read and you're no closer to a fun new life or rock-hard glutes. 

Why? Well, here's the thing about rules: If there are more rules than you can comfortably memorize, you might as well forget all of them! All they'll exercise is your memory — and there are more rewarding ways to do that. 

Also, good rules are simple. They allow you to focus on what really matters and react flexibly to new situations. Research shows that the most successful companies in the New Economy prefer a small set of simple in-house rules to more elaborate systems. 

So how can you work out the right Simple Rules for your specific situation?

Read on to find out

  * how to draft your own Simple Rules and stick to them;

  * why it can be a good idea to always paint the same subject; and

  * why you're not getting any thinner.

### 2. The world is overwhelmingly complex – but Simple Rules will help us cope. 

Take a moment to imagine you're a medical doctor at an army hospital. Several badly wounded patients come in, and you're faced with the task of deciding whom to treat first. What do you do? Luckily for you, there are a handful of reliable rules to follow that'll make things simpler. 

_Simple Rules_ are clear guidelines for well-defined activities or situations. Their purpose? To help you make good choices when things start to get convoluted. 

Simple Rules reduce the factors you have to consider in a situation by highlighting the _crucial_, thus preventing you from ruminating over trivial details. This enables you to make good choices, and fast — something very useful in the case of an army hospital. How do you sort out what's crucial in this hypothetical scenario? 

Check your patients' vital signs, such as their pulse, to estimate the severity of their condition. This takes less than a minute per patient. Those with the most alarming vital signs have to be treated first — unless they're beyond hope, in which case you'd better focus on saving someone else's life. Already this basic triage has lifted you clear of the pressing dilemma of what to do.

As one of the only tools that help us deal with complexity, we need Simple Rules more than ever. Our world is far from straightforward. Firstly, more and more things are interconnected — from our technological devices at home to markets around the globe. Moreover, our own rules and regulations have gotten so intricate that it's hard to stay on top of things.

Just take the US tax code: it's 3.8 million words long. To put that into context, that's seven times longer than _War and Peace_! The code is so complicated that the government employs 1.2 million tax preparers just to cope. Unsurprisingly, the US tax code overwhelms even the experts: On average, every third piece of advice by an IRS expert is wrong. 

In such a labyrinthine world, mistakes seem unavoidable. But with Simple Rules, you can better your chances of making your way through treacherously tangled situations. Find out more in the coming blinks.

> _"For those who wield the power of Simple Rules, complexity is not destiny."_

### 3. Simple Rules are easy to follow, and leave you room to choose. 

Say you want to lose some weight, and decide to do a little internet research. There's a huge variety of popular diet programs to choose from. However, they've all got one thing in common: a whole stack of rules that seem just about impossible to remember. Just getting acquainted with one takes days. How likely are you to follow through?

The first big advantage is that Simple Rules come in small numbers, so they're easy to remember and adhere to. 

Just take the simple diet rules of best-selling author Michael Pollan. He distilled decades of research on nutrition into three Simple Rules: First, "Eat food," by which he means real, natural food rather than processed, food-like substances. Second, "Not too much." Third, "Mostly plants."

That seems far too easy to be true. But it actually works, according to scientific research. You'll find that living by these rules is bound to decrease your risk of diabetes, obesity and heart attacks.

There's another advantage to the rules' simplicity. Although they provide concrete guidance, they're not too prescriptive. This means more room for choice and creativity. For example, Pollan's diet rules don't tell you whether you should have blueberries or bread for breakfast.

Simplicity can foster creativity. Sometimes it makes sense to limit yourself in order to thrive. Consider the great impressionist painter Claude Monet. You can tell from his pictures that he imposed Simple Rules upon himself like "Limit your subjects, mostly paint hay stacks and water lilies" and "Focus on the light." This enabled him to explore in depth the artistic possibilities of these subjects — and create truly original art that heralded the impressionist era.

> _"Complicated solutions can overwhelm people, thereby increasing the odds that they will stop following the rules."_

### 4. Simple Rules help us coordinate collective behavior. 

There are certain tasks that individuals can't perform alone — it's hard to imagine one person building the Pyramids! In times like these, individuals come together as groups to achieve outcomes they wouldn't have achieved as individuals.

And it's not just humans that need groupwork: the animal kingdom thrives on it too. Consider the Japanese honeybee. A single bee stands no chance against a fierce and gigantic hornet that approaches the hive. But together, the bees can defeat the intruder. They cluster together around the hornet and vibrate their wings so fast that the attacker dies from overheating. This is called "thermoballing."

Just like honeybees, humans need to be coordinated to be effective — and this is where Simple Rules come in. Rules are necessary to direct all individuals toward a shared goal. Since overly complex rules are unlikely to be followed, Simple Rules are the best way to manage people's (and bees') interactions.

Simple Rules work best when communities set norms and make its members stick to them. Norms are rules that seem so basic and obvious that people won't think twice about committing to them. 

For instance, Zipcar, the world's biggest car-sharing network, has no drop-off centers. So a person who rents a car is highly dependent on how the vehicle was left by the driver who used it before him. Therefore, they need rules. But instead of chaining the users to a long, fine-printed contract, Zipcar has only six very short and Simple Rules, like "Refill gas." Since most users stick to the rules, high costs and problems can be avoided.

Simple Rules are extremely helpful, and we definitely use them everyday. As we'll see in the next blink, there are different kinds of rules for different situations.

### 5. Three kinds of Simple Rules help us make better decisions. 

Now that we know what Simple Rules are and why they work, let's consider the different _types_ of Simple Rules. There are six of them in total. The first three are rules of thumb that help you make better decisions: _boundary rules_, _prioritizing rules_ and _stopping rules_.

_Boundary rules_ guide you if you have to make a typical yes-or-no decision. Imagine you're a burglar: How do you decide whether to break into a house? Of course, you'll want to go for an unoccupied house — but how can you tell? 

While movie-goers might think that criminals use complex formulas to plan their crimes, a study found that they mostly rely on one Simple Rule, and it's the single most reliable predictor of whether a house is occupied or not: "Don't break into houses with a vehicle parked outside."

Second, there are _prioritizing rules_. These come in handy if you need to rank different options. As an investor, for instance, you have endless options, but not endless money. What should you opt for? 

Intricate formulas attempt to answer this question, but none of them outperforms one Simple Rule dating back to the Babylonian Talmud: "A man should always place his money, one third in land, one third into merchandise, and keep a third in hand." Translated into a modern finance context, this rule becomes: invest in every asset class equally.

Thirdly, there are _stopping rules_. Knowing when to stop could save you from obesity. Here's a cross-cultural example to explain: classic French cuisine is drenched in butter, while many Americans follow low-fat diets. And yet, significantly fewer Frenchmen suffer from obesity. Why?

A study compared the eating behaviors of Chicagoans and Parisians and found that the Frenchmen owed their leaner physique to one Simple Rule, "Stop eating when you're feeling full."

### 6. Three more Simple Rules improve our performance. 

After exploring the Simple Rules that help us make the right decisions, we'll now consider the three rules that can help you do things _better._

It starts with _how-to rules_. Their most important feature is that they're not too prescriptive, leaving ample space for creativity. Take, for example, sports commentators. In the early days, they'd just say what came to their mind, until BBC announcer Seymour Joly de Lotbiniere revolutionized the role. 

His legendary announcements were based on six Simple Rules, such as "give the score or results, regularly and succinctly" or "share 'homework'", like interesting historical facts or personal information about players. Those rules seem commonsensical today, but they weren't back then.

We've also got _coordination rules._ These rules are necessary in social contexts when individuals interact and need Simple Rules to know what to do. For instance, in improvisational comedy, actors need to come up with good answers in an instant — without any script or rehearsal. How? They use Simple Rules that provide clear guidance. 

To make time, they respond to everything that was just said beforehand with "Yes, and…" You don't have to be on an improv stage to find that trick useful. Another rule is to never tell jokes prepared beforehand. In a performance based on spontaneity, they're bound to appear out of place. 

Finally, we're assisted by _timing rules_ — rules that help you determine _when_ to do things. For example, when you should to go to sleep. That's a question that's quite tricky if you're an insomnia sufferer. Fortunately, the findings of medical research into sleep can be condensed into four Simple Rules. They are: "Get up at the same time every morning;" "Avoid going to bed until you feel sleepy;" "Do not stay in bed if you're not sleeping" and "Reduce the time spent in bed." These rules are the simplest and most effective way to improve your sleeping habits.

By now, we've got a good grasp of Simple Rules in theory. How can you create your own personal Simple Rules, and put them in action?

### 7. Experience and negotiation allow us to create our own Simple Rules. 

We can't just pull our personal Simple Rules out of nowhere. To make sure that our rules are the right ones, we need to base them on a vast and reliable source of knowledge. That source? None other than our own personal experiences. 

Every day we learn something new about how to get by in the world. In fact, we've been learning all our lives! Why not take what you've learnt from the past and turn it into some rules that'll help you in the future?

That's just what leading comedian Tina Fey did. Drawing on her experience with legendary _Saturday Night Live_ producer Lorne Michaels, she wrote down nine Simple Rules for managing a comedy show — like "When hiring, mix Harvard nerds with Chicago improvisers and stir" or "Never tell a crazy person he's crazy." 

If you feel you lack experience or knowledge, start by drawing from the experiences of others. This works well for young companies, who profit from observing the practices of established competitors. Movie rental revolutionizer Netflix started out by adapting the rules of the DVD rental service Blockbuster, like charging $4 per unit and late fees. 

You can also base your rules on someone else's scientific research. Just think of those insomnia rules shaped by sleep medicine researchers.

And what if you need Simple Rules not just for your own life, but rules to share with your friends, coworkers or loved ones? Remember that Simple Rules are the results of successful negotiations. 

This is what the National Oceanic and Atmospheric Agency had to do when searching for reasonable whale-watching rules that would protect endangered killer whales in the Salish Sea. 

There were huge conflicts of interest between scientists, private-boat owners and whale-watching businesses. Fortunately, they came up with one Simple Rule everyone could agree on: All boats have to stay at least 200 yards away from the whales and 400 yards out of their path.

### 8. Create your personal Simple Rules in three steps. 

If you're eager to develop your own Simple Rules, there are three crucial steps to take. First, figure out the _critical action_ that will help you achieve your goal. 

For a company that wants to increase profits, the critical action could be to increase the one factor that leads to most profits. For example, the trading platform eToro has a special feature, eToro OpenBook, where expert investors share their trading strategies. Many investment novices go there for valuable advice. The expert investors whose advice has the greatest following achieve the rank of "Popular Investor." 

There are two ways Popular Investors can increase revenues: They trade a lot themselves (and pay transaction fees) _and_ they attract followers and encourage them to invest, too (and they pay transaction fees in turn). Obviously eToro wanted to attract more Popular Investors to the site because they generated more revenues than all the others.

Next, eToro needed to identify the _bottleneck_, or the problem that kept them from thriving. That's where Simple Rules will yield the most impact. eToro OpenBook's bottleneck was clearly the shortage of Popular Investors. 

Finally, you're ready to formulate your own Simple Rules based on your discoveries during the first two steps. For eToro, the Simple Rule for winning more Popular Investors was offering incentives — like financial rewards to the investors that attained higher rankings. 

For a simpler example: if you want to lose weight, your critical action is to eat less. A common bottleneck might be late-night snacking. To stop midnight fridge raids, these are two personal Simple Rules of one the authors, Donald Sull: "Eat snacks from a small bowl, not the bag" and "Don't stockpile snacks in the cupboard."

> _"You have to work hard to get your thinking clear to make it simple." — Steve Jobs_

### 9. Simple Rules are not forever. 

After a while, we can get so used to our Simple Rules that following them requires no effort at all. Of course, this is a very comfortable level to achieve. Sadly, it doesn't last. 

Even if your initial rules still work out somehow, you shouldn't stop striving to improve on them. Rules that worked well once may become less than adequate if the situation changes. For instance, usually it's a good rule to eat an apple a day but if your teeth enamel gets thinner, sugary apples suddenly aren't so good to eat any more. 

Also, your initial rules may be based on slightly mistaken assumptions. The internet company Airbnb is a space for people to rent out private lodgings. The founders expected their typical customers to be young folks on a small budget, and they came up with rules to accommodate them — e.g., they focused on cities that would run festivals.

But a survey found out that actually, many of their customers were much older and wealthier than expected, and naturally had different hospitality expectations to enthusiastic festival-goers. So Airbnb founders adapted their rules to what they'd learned. For example, one rule now states: "Always have fresh soap." 

Sometimes, however, the rules are hopelessly outdated. In this case, you have to _break_ them. Take TV shows as an example: Before the widespread use of digital video recording, if you missed an episode, that was it, you had little chance to catch up on it. 

As a result, TV shows needed to be written in a way that enabled you to tune in at any time, no matter what you missed. One indispensable rule was "Have a stable cast of characters that appear in every episode." But today, in a TV show like _House of Cards_, a character disappears in the second episode and only returns in episode eight. That's because Netflix, in allowing you to rewatch episodes whenever you want, broke that outdated Simple Rule, affording more creative flexibility for writers everywhere!

### 10. Final summary 

The key message in this book:

**In a world of overwhelming complexity, Simple Rules are here to help. They help us make better choices and improve our performance, while remaining easy to follow and highly customizable. With experience, negotiation and considered observations of your own problems, you too can create your own personal Simple Rules and find easy fixes for even the most complicated of problems.**

Actionable advice:

**Look out for role models as a source of your own Simple Rules.**

The next time you struggle to reach a goal, look out for people in your personal environment who already managed to tackle a problem similar to yours. Chances are that they came up with a set of viable Simple Rules that could help you to deal with your own problem. Don't hesitate to ask — people are mostly flattered when asked for advice and genuinely glad to be of some help! 

**Suggested** **further** **reading:** ** _The Power of Less_** **by Leo Babauta**

_The Power of Less_ introduces Leo Babauta's ideal of _productive minimalism_. His approach focuses mainly on the development of good habits as the key to long-term changes.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Donald Sull and Kathleen Eisenhardt

Donald Sull is management guru and expert on strategy and execution in turbulent markets. He is also a former professor at Harvard and the London Business School.

Kathleen M. Eisenhardt is a professor of strategy at Stanford's School of Engineering. She is also the coauthor of _Competing on the Edge: Strategy is Structured Chaos_.

