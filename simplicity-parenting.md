---
id: 5a00d4f3b238e100073f3a27
slug: simplicity-parenting-en
published_date: 2017-11-08T00:00:00.000+00:00
author: Kim John Payne and Lisa M. Ross
title: Simplicity Parenting
subtitle: Using the Extraordinary Power of Less to Raise Calmer, Happier, and More Secure Kids
main_color: 4EA5CA
text_color: 30667D
---

# Simplicity Parenting

_Using the Extraordinary Power of Less to Raise Calmer, Happier, and More Secure Kids_

**Kim John Payne and Lisa M. Ross**

_Simplicity Parenting_ (2009) teaches parents how to reduce the levels of stress experienced by their children. Parents can accomplish this by controlling their children's environment, limiting their access to the adult world and providing them with a steady, rhythmic schedule. Taking these simple steps will improve family life for all involved.

---
### 1. What’s in it for me? Simplify your way to great parenting. 

Raising children in today's world is one heck of a challenge. Even buying toys is a fraught undertaking. What kind should you buy? Educational ones? Fun ones? And how many? Is it better to encourage children to play sports, or study music — or both? And how much "adult" stuff, such as news and politics, should they be exposed to, and at what age?

While there is no single right answer to these questions, there is one thing that every child definitely needs, and that is _simplicity_. Simplicity in the face of all the clutter, noise and uncertainty of the modern world. Simplicity so they have room to grow and learn at their own pace.

The good news is that you can help them achieve this. How? By detoxifying their environment, by giving them a clear daily rhythm, by balancing their schedules and by filtering out the adult world. Let's delve into the details.

In these blinks, you'll learn

  * how predictability is key to learning;

  * why doing nothing is as important as doing something; and

  * why Barbies are too complete a toy.

### 2. The hectic modern world can have a detrimental effect on the well-being of children. 

Most people live ridiculously overloaded lives. We're constantly staring at our phones or computers and, if we do look up or step outside, it's only to be bombarded by advertisements on billboards and buses and buildings. Life in today's modern age is extremely hectic.

As adults, we can cope with this constant inundation, but for children, it's a whole different story. They struggle to handle the stress caused by this cascade of stimuli — so much so that it can alter the way they behave. This condition is what the author refers to as _cumulative stress reaction_, or CSR.

In his work as a teacher and private consultant, the author often saw children in states of acute stress, anxiety and nervousness — all of which are symptomatic of CSR. They become hypervigilant and overly controlling, and they exhibit little resilience or empathy.

Of course, a child's life shouldn't be completely stress-free, which is just as well since you can't prevent them from experiencing stress and trauma; they'll probably fall out of a tree or get into a fight with one of their friends at some point. But these experiences build their resilience, improve their understanding of the world and also teach them how to behave.

However, when children are _constantly_ stressed, their resilience doesn't improve; rather, it declines, resulting in the above-mentioned symptoms of CSR.

Take the example of James, whose parents reached out to the author for help. They were worried about James's anxiety, which was getting worse and worse.

James's parents were educated people; they enjoyed engaging in intellectual debate and staying abreast of current events — CNN was on in their household 24/7. But being constantly involved in this adult world was having a detrimental effect on their son. Eventually, he started to show symptoms of CSR — he became controlling and highly nervous. In fact, he didn't learn how to ride a bike until he was eight years old because he was too scared about the possibility of falling off.

To maintain your child's well-being, it's important to simplify his life as much as possible. You'll learn how to go about doing this in the following blinks.

> _"...society is poking quite a few holes in the protective filter that should surround childhood to buffer it from adult life and concerns."_

### 3. Children don’t need a lot of toys. 

According to the sociologist Juliet Schor, an American child will, on average, receive 70 new toys per year.

But children don't really need to be showered with new playthings. What they need is time and space to explore and grow.

By filling your children's play area or room with loads of toys, you're giving them too much choice. This can actually prevent them from growing up at a normal, natural pace because they'll become demanding and insist on having more and more. Playtime may begin to center around material objects rather than free, imaginative play.

To prevent this from happening, cut the number of toys your child has by halving the current amount twice — or even three times. There's no definitive number of toys that should or shouldn't be allowed; the aim is just to considerably reduce the quantity.

To get started, get rid of all the toys that are defective in some way.

Also, ditch those that no longer appeal to your child's imagination. You may have noticed that sometimes children disassemble toys. That's because they may be too "fixed" to begin with. A Barbie doll could end up with its legs and arms removed because your child found it too unchangeable and boring to engage with — and, therefore, was less careful with it. Most fixed toys you can either dispose of or donate in this first halving already.

The second time you halve the pile, you can keep discarding and donating toys, but when it comes to those you can't quite bear to part with, you can also store them in your loft or basement for now.

And the final time you halve the pile, be really picky — only save the toys you know your child would really want to keep.

### 4. Create more rhythm in your day-to-day family life. 

Even people living alone often struggle to maintain a daily rhythm, so you can imagine the difficulty of keeping a consistent schedule when there's a whole family that's supposed to stick to it.

When the author asks families what their typical day is like, most respond that they have no such thing.

But it's vitally important that children have structure in their days. They need predictability and regularity in their everyday lives; without it, they can't focus on learning about themselves and the world around them.

To build structure, you need to create repetition: mealtime, playtime, bath time and bedtime need to follow a strict routine every day. Eventually, days will become more predictable for your child.

They'll begin to think to themselves: "Aha, after I wake up, it's time to eat breakfast," or, "Before I go to sleep, it's time to read a book." They'll have a solid foundation free of distractions and, therefore, of insecurity.

Another good idea is to limit the number of surprises your child gets by always giving him or her a "preview" of the day to come. Of course, if your family is like most others, you'll lead a busy, unpredictable life and can't possibly be expected to know what's going to happen on every single day. But it's possible to let your child know certain details that can be counted on.

Take the example of six-year-old Justin. His parents lived extremely busy, unstructured lives, which meant that Justin had no predictability or sense of transparency in his life. In response, Justin started to refuse to leave his bed in the morning — that way, nothing bad or unpredictable could happen.

To combat this behavior, his parents started giving him previews. Before he went to sleep every night, they'd give him details about what the following day would bring. They'd tell him how he'd travel to school and who'd be picking him up. Now Justin knew what to expect and, as a result, his life was less hectic and confused. He started getting out of bed again.

### 5. Don’t overload your child’s schedule with activities. 

If a child has activities planned for most days of the week, it may be too much.

In wanting the best for their children, some parents sign their kids up for everything: soccer, school plays, club meetings, auditions, karate, vaulting.

Although the child may look forward to each activity, by doing so many of them, her life will become focused on competitiveness and what she can achieve, rather than sheer enjoyment and satisfaction.

To describe such children, the author uses the term _overscheduled_. They have little to no time to just do nothing, and they're unable to fully engage deeply in creative play because they don't have the free time. Depriving them of this creative playtime also deprives them of the opportunity to learn and grow.

Of course, getting your child involved in sports and other positive activities can be highly beneficial for them. But such activities shouldn't come at the expense of personal playtime.

The best way to organize your child's schedule is to strike a balance between their various activities and some downtime.

Take Sarah, a mother who decided to structure her daughter's life this way. One weekend, Sarah's entire family was over at her house for Passover. This environment was extremely chaotic, and Sarah's daughter, Emily, got overwhelmed and started acting out aggressively.

To calm her down, Sarah took Emily out for a bike ride. This gave Emily the break that she needed from all the activities that were taking place — activities that, to her, felt like a strict schedule she had to stick to, without time to relax and play.

Similarly, Frankie's mom realized that he needed time to just do "nothing." Frankie's dad loved monster trucks and regularly took him along to derbies. Although Frankie thoroughly enjoyed them, his mom noticed that he'd always imitate the proceedings and become more aggressive with his toys afterward. To balance out such an intense activity, Frankie's mom scheduled some time for him to play freely, and peacefully, on his own terms.

### 6. Protect your child from the adult world by reducing the number of screens in your home. 

In our fast-paced society, the majority of adults are under a lot of strain, which is causing widespread anxiety. And this anxiety is spilling over to children.

To protect them, it's important to separate children from the adult world. An easy way to do this is to curtail the constant stream of information that's broadcast through all the screens surrounding us.

Nowadays, every household has multiple screens: TVs, computers and mobile phones are all readily available, each providing an incessant flow of information and stimulation. But television is by far the worst medium; it delivers a constant stream of empty entertainment rich in sensationalism and violence. So jettison your TVs, and give your child the opportunity to learn and grow naturally without being corrupted by a constant influx of adult information.

In 2005, neurologists discovered that to optimally develop their brains, children between the ages of zero and two must interact with other humans and their environment, and play problem-solving games like peekaboo. Clearly, watching TV doesn't involve any of these activities, so young children who watch TV receive zero developmental benefits.

In 1999, the American Academy of Pediatrics delivered a report that recommended that no child under the age of two should watch TV, and that children above the age of two should only watch a limited amount.

Therefore, it's sensible to significantly reduce the amount of screens in your home. It'd be ridiculous to think that you can protect your child from screens all the time, but at least you can control the situation within your own private environment. You don't have to cleanse your home completely, but definitely introduce some limitations.

Getting rid of the screens in your home may seem like a monumental move, so it may not be the first step on your way to simplifying and improving your child's life. But when you eventually do get around to it, you'll find that it'll do wonders for your family's happiness and well-being.

### 7. Final summary 

The key message in this book:

**Modern life is stressful and anxiety-inducing for many people, but adults are not the only ones affected by the strain. It also has a detrimental effect on children, and many of them are developing anxiety and hypervigilance as a result. By simplifying your child's environment, introducing a stable schedule and protecting them against the adult world, you can help your child lead a happy, healthy life where they're able to play and flourish.**

Actionable advice:

**Declutter without your child.**

When it comes to cleaning up his play area, don't invite your child to help you complete the task. At first, it may seem like a good idea, but involving your child will make it even more difficult than it already is. It'll be a constant battle to determine what to discard, what to keep and what to store where. If your child is under the age of nine, it's simply better to declutter on your own.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _How to Raise an Adult_** **by Julie Lythcott-Haims**

_How to Raise an Adul_ t (2015) reveals the ways in which the most common parenting method today, helicopter parenting, is doing more harm than good, both for parents and kids. These blinks outline a better way to parent — one that actually raises children to become truly independent adults.
---

### Kim John Payne and Lisa M. Ross

Kim John Payne is an expert in child-rearing with 27 years of experience in school, adult and family counseling. His other books include _The Games Children Play_ (1996) and _Being At Your Best When Your Kids Are At Their Worst_ (2017).

Lisa M. Ross is a writer and former literary agent. She has worked in the publishing business for 20 years but currently works as a writer and ghostwriter.

