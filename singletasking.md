---
id: 56a6aef0fb7e870007000025
slug: singletasking-en
published_date: 2016-01-28T00:00:00.000+00:00
author: Devora Zack
title: Singletasking
subtitle: Get More Done One Thing at a Time
main_color: 882276
text_color: 882276
---

# Singletasking

_Get More Done One Thing at a Time_

**Devora Zack**

_Singletasking_ (2015) tackles some of the common myths surrounding multitasking and productivity. Full of practical advice and tricks to help you get more from your day, _Singletasking_ clearly demonstrates how immersive focus on a single task leads to a more efficient, and ultimately happier, life.

---
### 1. What’s in it for me? Become less stressed and more focused – and get more things done. 

Do you often multitask to save time? Whether it means brushing your teeth while driving to work, answering texts while you're eating or constantly checking Facebook while working, multitasking might seem like a life saver. It isn't. It makes you more stressed, less focused, and ultimately you don't really do anything well. It can also harm you physically.

So what should you do instead? You should learn to do ONE thing at a time, and ONLY one thing. No texting. No Facebooking. No twittering. Just the task at hand. This is easier said than done, but these blinks will give you some handy tips and get you started on your new, less stressful and more productive life.

In these blinks, you'll learn

  * why multitasking costs the American taxpayer billions every year;

  * why our brains are not hardwired for multitasking; and

  * how to deal with yapping coworkers. ****

### 2. Multitasking isn’t the answer to a hectic life; quite the contrary, it can even be fatal! 

**"** Multitasking kills!" Now there's a campaign sticker you thought you'd never see. Laugh all you want, but it's actually true!

Indeed, multitasking causes the deaths of thousands of people as well as economic damages amounting to billions every year.

For example, did you know that distracted driving, such as driving while texting, talking on the phone, etc., is the second leading cause for car accidents after driving under the influence?

In fact, the American Automobile Association (AAA) recently discovered that about 67 percent of all American drivers use their phones while behind the wheel. As a result, tens of thousands of people are dying as an immediate consequence of people trying to pay attention to too many things at once. 

The economic cost — the loss of property and the impact of injuries and loss of life — due to distracted driving is similarly astonishing, totalling $871 _billion_ per year. 

Not only is multitasking dangerous, it also won't make you more productive.

In fact, there's really no such thing as multitasking, per se. As Stanford University neuroscientist Dr. Eyal Ophir has shown in his studies, our brain is simply incapable of focusing on multiple things simultaneously.

Rather, it simply switches the focus of its selective attention between the various tasks it has to handle at any given moment. When your brain has to make the switch, your performance suffers, causing you to work less efficiently on the task you're currently focused on. 

Moreover, divided attention makes it more difficult for you to handle incoming data, as it hinders the information processing in your short- and long-term memory. This was demonstrated in a Harvard study conducted in 2011, which examined the relationship between multitasking and academic performance. The study revealed that our cognitive functions, especially in our memory, become increasingly limited while multitasking.

### 3. Our brains did not evolve to multitask. 

From an evolutionary perspective, multitasking is not part of our programming. Rather, our hunter-gatherer ancestors had to be alert and aware, not lost in thought, when searching for food. After all, it's hard to see the saber-toothed tiger lurking in the bushes when your head's in the clouds.

Not only is it contrary to our evolutionary programming, multitasking can also damage the brain. When you multitask too much, constant overstimulation and the stress associated with it cause the gray matter of your prefrontal cortex — the part of the brain responsible for complex thinking — to shrink.

Furthermore, when you try to do a bunch of things at once, like finishing your homework, talking to your friend on the phone, and watching the news while eating ice cream, your brain releases a stress hormone called cortisol. This hormone reduces your ability to effectively complete any of those tasks, and further degenerates the hard-working neurons in your brain.

In contrast, _singletasking_ empowers you to achieve world-class performance. Only when you are fully absorbed in the task at hand can you enter what the world-renowned psychologist Mihály Csíkszentmihályi calls the _flow state_ : a zone of complete immersion and focus.

It was this flow state that American soccer goalkeeper Tim Howard experienced during a 2014 game between the United States and Belgium. Although his team lost, Howard played an astonishing game with a record-breaking 16 saves. 

When asked about his incredible focus, Howard said that "once that whistle blows, everything else disappears." Nothing else mattered, and he focused all his attention on the single important task in front of him.

You've seen how bad multitasking is for your health and productivity, but is there anything you can do to fight the urge to multitask? The following blinks will address this very question.

### 4. Take control by developing an awareness of when your mind wanders and by eliminating distractions. 

We've all had the experience of meeting someone for the first time and then immediately forgetting their name. Though it might not seem like it at the time, these kinds of faux pas can be caused by multitasking.

Multitasking doesn't necessarily mean a conscious effort to engage in numerous activities at the same time. It could be as simple as thinking about things other than the immediate task at hand. 

Thoughts will creep into your mind; that's natural. The trick is to prioritize what is most important in any particular situation and commit fully to that activity, guiding yourself back into focus when your mind wanders. 

While you're trying to focus on something important, you may find it difficult to stop thinking about other things. For example, you might worry about what to buy your wife for her birthday, or experience a flash of anger about something your co-worker said the day before. 

That's OK. Just come back to the task at hand and focus on it. When you feel like your head's about to explode from all that focusing, take a short walk outside, or take a break to meditate. 

Just as important as maintaining focus is fending off potential distractions.

There are so many things in our lives that just beg for our attention. Be it a Facebook message, gripping news headlines, emails or a yapping co-worker, distractions are very much part of our everyday lives.

You may have to get creative to prevent these distractions from grabbing your attention. Try things like putting your phone on flight mode, closing your office door and putting up an "I'm busy" sign for all to see, or maybe even add an auto-blocker to your web browser to prevent you from visiting Facebook, Instagram and other distracting sites during the workday.

### 5. Get through your stressful day by implementing simple hacks and techniques which help you singletask. 

In our last blink, we talked about some ways to fight the tendency to multitask. In this blink, we'll get into some of the tricks that will help you actively singletask.

One such trick is the _cluster tasking technique_, which combines similar activities and tasks into a single group in order to help you work more effectively.

To do this effectively, you must first identify which tasks require similar methods to complete, and then group them together. Then it's simply a matter of setting a timeframe for each cluster, deciding how you want to spread them throughout your schedule and completing the tasks.

For example, you could put all tasks which involve making calls or Skyping into one cluster, or block. You could then follow this up with a block full of writing activities like emails, meeting summaries and so on, and then finish your day with tasks that demand physical activity, like chopping wood for your fireplace or tending to your garden.

Another technique that will help you be more effective is one commonly used by doctors around the world: built-in flexibility in your schedule.

Have you ever wondered why it seems to take ages to get an appointment at the doctor's surgery when you have a sore throat, but if it's an emergency a physician is always available to handle it? This is because physicians usually have two "free" 30-minute periods during the day dedicated to medical emergencies that would otherwise put them behind schedule.

If you plan a bunch of meetings one after the other, sooner or later you'll fall behind schedule. But if you build a buffer into your schedule, an unanticipated call from your boss or a sudden, important incident won't disrupt your routine, making it possible for you to singletask more efficiently.

These techniques are great for improving productivity in the workplace. In our closing blinks, we'll show you how you can use singletasking in every part of your life.

### 6. Singletasking is the only way to give people your full attention. 

Sometimes, when you're having a conversation with someone, it seems as if they're not really paying attention and their thoughts are elsewhere. It never feels good to be in that position — it's uncomfortable, and it makes you feel less valued. You don't want to be that person, and singletasking can help.

Not only does singletasking improve your productivity at work, it also helps you further develop your social skills, empowering you to develop better, deeper connections with the people in your life.

We can see evidence for this in a study conducted at the University of Southern California, in which high-level executives were interviewed about their perception of the use of smartphones in formal settings. The interviewees said that someone who was constantly fiddling with their phone signals to them that they lack attention, respect and self-control, thus causing their view of them as a person to depreciate overall.

So, whether you're answering calls during a meeting or replying to text messages during a conversation, this kind of distracted behavior will be viewed as inappropriate and disrespectful.

Active listening, on the other hand, is a sign of respect, as you demonstrate to your conversation partner that you value both her and the conversation you're having.

Look at an interaction between one of the author's clients and former Secretary of State Henry Kissinger. The author's client can still vividly remember meeting Kissinger even after 30 years.

During their conversation, Kissinger gave him his complete and undivided attention, despite the fact that thousands of others were waiting to hear him speak. Kissinger held steady eye-contact throughout and took a real interest in what he had to say, even though he didn't have to.

This short encounter left a lasting impression — one that we should all try to emulate.

### 7. Free time will rejuvenate you and improve your quality of life. 

"Less is more" is a maxim that we've all heard countless times. Yet, many workaholics set aside this valuable advice and rack up overtime at the office. Bad idea.

Singletasking is most effective when combined with regular breaks. The type and length of these breaks will differ depending on the type of work you do. For instance, if you have a desk job, then a nice stroll outside could do the trick. If you're a construction worker, sitting down for a moment's rest would help.

In either case, you want to give your brain some time to calm down and replenish its energy.

But you shouldn't just take breaks from actual work. It's also a good idea to take a break from the stress that comes from being available all the time. 

Start slow and unplug for just an hour a week, and use that time as a period of personal reflection. It's important to take some time off, as we tend to avoid thinking about what's happening in our lives when we multitask.

As Dr. Ethan Kross from the University of Michigan has pointed out, while we're very good at giving our friends advice and helping them through their problems, we usually fail when it comes to our own issues. This is due to the numerous egocentric biases that skew our self-image and make it difficult to change our behavior. 

A scattered mind prevents you from digging deep and gaining a nuanced and balanced view of the issues you face. It's therefore essential to take the time for self-reflection without the distraction of outside influences.

Remember, it's not unproductive to set aside some time solely for the purpose of reflection. Who knows, maybe you'll even have an "aha!" moment and find the solution to something that has bothered you for a while.

### 8. Singletasking helps you become immersed in the moment, leading to a happier and more balanced life. 

As you've seen, singletasking improves your productivity, your relationships and even your self-awareness. But, as research has shown, perhaps the greatest benefit of singletasking is the effect that it has on overall happiness and fulfillment. 

In a recent Harvard study involving over 2,000 participants, researchers found people are much happier when they're highly engaged in a single task.

The study also showed that people who tend to have a small attention span and are easily distracted are inclined to be less happy. When it comes to overall happiness, the ability to immerse oneself solely in the moment is a good indicator of a fulfilled life. 

If you ever feel like you're having difficulty singletasking, you can find inspiration in looking at the way young children go through life with a lightness and fascination for the things around them.

Joshua Bell, who numbers among the most respected violinists in the world, once took part in an experiment in which he dressed as a busker and played six Bach pieces in a Washington, D.C. subway station. During the 45 minutes he was there, only a few people decided to stop and listen — most were too busy looking at their phones.

During the experiment, he made $32 in donations. To put this in perspective, he usually earns tens of thousands of dollars for his performances. But there was one group of people who frequently stopped to listen to his beautiful music: kids!

Young children never multitask; they still have the joy and wonder which most of us have unfortunately lost. So follow their lead, and go through your life with both eyes open.

### 9. Final summary 

The key message in these blinks:

**Contrary to popular belief, the key to being more productive is not filling your schedule and tackling everything at once. Rather, you should singletask, or prioritize your tasks and work through them one at a time with focus and tranquility.**

Actionable advice:

**Cut the fat from your everyday tasks.**

The next time you're at the office, scan your tasks before doing anything else in order to figure out what's really essential. Make a list that outlines these crucial few tasks, along with the many other trivial things you have to take care of. By cutting the fat and focusing on what's really important first, you can increase the quality of your work.

**Suggested further reading:** ** _Mini Habits_** **by Stephen Guise**

_Mini Habits_ (2013) explains the logic behind an innovative approach to achieving your goals. Motivation and ambition aren't necessarily what will drive you to success; rather, it's your small day-to-day habits that will really get you on the right track. Learn how to harness their power with these blinks.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Devora Zack

Devora Zack is a leadership consultant and founder of Only Connect Consulting. Her success in leadership consulting has served as the basis for her many books, including _Networking for People Who_ _Hate Networking_ and _Managing for People Who Hate Managing_.

