---
id: 55a36ad23738610007000000
slug: six-capitals-or-can-accountants-save-the-planet-en
published_date: 2015-07-13T00:00:00.000+00:00
author: Jane Gleeson-White
title: Six Capitals, Or Can Accountants Save The Planet?
subtitle: Rethinking Capitalism for the Twenty-First Century
main_color: None
text_color: None
---

# Six Capitals, Or Can Accountants Save The Planet?

_Rethinking Capitalism for the Twenty-First Century_

**Jane Gleeson-White**

_Six Capitals, Or Can Accountants Save the Planet_ (2015) traces the rise of a new accounting paradigm that holds corporations accountable for their impact on the environment and society. These blinks consider what role nature and society play in wealth creation and describe how accounting can push businesses to adopt long-term sustainability strategies for the 21st century.

---
### 1. What’s in it for me? Discover the planet-saving potential of accounting. 

Who will save our planet from the threats of environmental issues and dwindling resources? Most people would probably point to scientists, engineers or activists of some sort. Probably no one would think of _accountants_ as the potential heroes of the 21st century.

But these blinks will show you that, in fact, accountants can shape the way corporations handle wealth, and thereby revolutionize the impact they have on the environment and the rest of society. Accountants can change the daily operations of business corporations so that they don't destroy the natural resources of the planet, but help give it further value instead.

You'll also learn

  * how _nature value_ stopped the fast-food industry from depleting forests in Costa Rica;

  * that modern corporations and psychopaths have much in common; and

  * why nature should be granted some of the same rights as human beings.

### 2. History has seen three great waves of wealth creation, each with its own corresponding form of accounting. 

Impeccable posture, business attire, nose in a ledger book. Such is the figure of the accountant in the public imagination. What goes unrecognized, however, is the crucial role these professionals play in our economy by categorizing and measuring wealth.

But, throughout history, the practice of accounting has changed with each new wave of wealth creation.

The earliest accounting records date back to 7000 BC, when settled farming communities appeared in Mesopotamia (now Iraq). And that's how wealth was created during the first wave — agriculture. In order to keep track of their new agricultural products and exchanges, farmers recorded their transactions with fired clay.

The Industrial Age — a period that started in the late-18th century — ushered in the second great wave of wealth creation, which saw the rise of new, mechanized methods of production. The corresponding accounting technique, however, had been developed centuries earlier: the now ubiquitous double-entry bookkeeping system (which records debit and credit entries in two columns) first appeared, in Northern Italy, around 1300.

Double-entry bookkeeping proved a highly effective way to measure production in the Industrial Age, transforming simple business ledgers into detailed records calculating profits and losses, and thus accurately reflecting a company's financial health.

But we've moved past the Industrial Age and into the third great wave of wealth creation: the Information Age, precipitated by the 1951 invention of the first commercial computer. It's an economy driven by information and global networks, not agriculture or industrial production.

Like the waves that came before, this new wave forces people to grapple with novel forms of wealth. For instance, in order to be sustainable, businesses need to measure more than just financial information; they also have to measure innovation and long-term strategies.

In the following blinks, we'll discuss an accounting system suited to the third wave — one that measures factors related to nature and society.

### 3. Standard measures of economic output, like the GDP, don’t account for intangible forms of wealth. 

Our primary measure of economic output, the gross domestic product (GDP), is a byproduct of the production-oriented Industrial Age. And although the GDP makes it easy to measure the value of consumer goods (cars, jeans, burgers), it's not so effective in a digital epoch dominated by things of intangible value.

In fact, new forms of intangible wealth pose a major challenge to the GDP, which can only measure legal monetary transactions of goods and services. So, unsurprisingly, the GDP can't capture the value and impact of speed and innovation, two essential aspects of today's economy.

Consider the phenomenal technological progress we've made since the 1990s — like the advances in computing and the advent of smartphones. Since the retail prices of digital devices have fallen dramatically over a short period of time, the GDP registers the sales and use of such products in terms of negligible or declining wealth. But clearly the reverse must be true, since these technologies have become so ubiquitous.

Also, the growing availability of purely digital and free products and services (think online music, search engines, apps and social networking platforms) scrambles GDP figures. Remember: the GDP only measures monetary transactions, not data or information itself.

Another crucial drawback of the GDP is that it doesn't account for one of our most important forms of wealth: nature itself.

After all, we're using the earth's natural resources — fresh water, soil, clean air — without measuring their depletion. Degrading an ecosystem comes with enormous costs; unfortunately, these costs are ignored, because they don't appear in our economic accounts.

In other words, the GDP treats natural resources as if they were free and unlimited. But they're not: the demand for wood, for example, has corresponding costs in the form of shortages in clean air. Meaning, we're making a major omission by not accounting for the wealth of nature in our financial ledgers.

We'll discuss ways to rectify this in the following blink.

### 4. Adopting the concept of “nature capital” might temper the depletion of natural resources. 

How can we reflect the wealth of nature in our accounting practices? Well, here's one promising idea: extend the economic principle of capital into the realm of "environmental goods and services." That is, start accounting for "nature capital" in order to temper the depletion of natural resources.

There's evidence that this would work: During the fast-food industry boom of the 1970s, vast tracks of Costa Rican forests were cleared to make way for cattle ranches. Then, in the mid-1990s, the government decided to translate natural resources into the language of economics, as part of a forest-conservation effort.

They did so by estimating Costa Rica's ecotourism earnings and also by calculating the value of the water that forests preserved for the country's hydroelectricity industry (which generates electricity using water power).

And since the introduction of the concept of "nature capital," Costa Rica's forests have been restored, paving the way for others to adopt similar measures.

Nonetheless, not everyone supports the concept of nature capital: critics say that it extends the logic of capitalism into the realm of nature. They're concerned that nature-capital accounting just commodifies parts of nature that lie beyond the market, simply so they can be exchanged for cash.

Meaning that, according to market rules, an existing forest could be destroyed if another similar forest were created elsewhere. In other words, placing a monetary value on nature is challenging, because ecosystems like forests are unique, place-specific and extremely difficult to recreate — very much unlike a pair of jeans! In fact, some ecosystems could be said to have infinite value.

However, by _not_ assigning a value to nature, we've allowed its default value to be zero. So the problem isn't the principle of putting a value on nature, but rather interpreting this value as a tradable market price.

### 5. Today’s chief commercial agents, multinational corporations, have developed psychopathic traits. 

It's time to talk about the chief commercial agents of our time: _corporations_, companies authorized to act as a single entity.

The rise of multinational corporations in the 1980s was unprecedented; some firms now wield the sort of power once solely reserved for states. Consider this: In 2000, a study estimated that 51 of the 100 largest economies in the world were multinational corporations, not countries!

Another relevant statistic: in 2013, the 2000 largest publicly traded companies in the world employed 87 million people and generated around half of the world's GDP.

This isn't necessarily be a bad thing. However, the modern corporation has developed some psychopathic traits.

The modern corporation has few responsibilities beyond maximizing profits. And yet, it still enjoys the rights to enter into contracts, loan and borrow money, sue and hire employees. In short, corporations enjoy rights similar to those of human beings.

However, a person that felt entitled to so many rights but acted without compunction would be diagnosed as psychopathic.

Well, that's exactly what Joel Bakan, professor of law and author of _The Corporation_ (2004), discovered when he decided to extend a corporation's legal personhood to its logical conclusion by testing its psychological profile; using the American Psychiatric Association's _Diagnostic and Statistical Manual of Mental Disorders_, Bakan concluded that corporations possess many of the characteristics which define psychopaths.

That is, corporations break the law whenever they can get away with it. They hide their true behavior; they sacrifice long-term welfare for short-term profit; they're aggressively litigious; they ignore health and safety codes. And the kicker: they remorselessly cheat their suppliers and workers.

As an example, one only needs to remember the 2001 Enron scandal: The financial firm collapsed amid charges of greed, bribery, corruption, deceit, insider trading, tax avoidance, environmental destruction, human rights abuses, theft of worker entitlements and other infractions too numerous to list.

It's a bleak picture. Is a psychological intervention even possible in the case of the modern corporation? We'll find out in the upcoming blink.

### 6. Accountants have the power to make sustainability a priority for corporations. 

Considering the fundamental self-interestedness of corporations, are multinationals capable of operating for the greater good? Well, perhaps! Imagine if financial accounts also measured the company's impact on nature and society.

Such integrated reporting would encourage corporations to adopt sustainable business strategies, organized around minimizing the negative effects of the company's activities on the local community and environment.

Corporations are already starting to realize that sustainability issues — such as climate change, urbanization, population growth and water and food shortages — have an increasingly significant bearing on the viability of their operations.

This shift is partly due to increased awareness on the part of investors, who are beginning to pay closer attention to the social and environmental practices of companies.

So failing to address and report on these topics could have significant financial implications for companies, potentially discouraging investment.

That's why so many companies are already starting to practice integrated reporting. For instance, in 2002, Swiss pharmaceutical giant Novartis devoted half of its 160-page report to nonfinancial issues.

Similarly, the Canadian electric utility company, BC Hydro, presented social and environmental performance information along with financial data in its annual report from 2003.

Which leads to a crucial point: accountants have the power to reframe environmental and social issues, making them standard components of economics and business.

Because, as we've discussed, modern corporations are massively powerful. But who do executives turn to when they need advice?

To accountants!

Meaning, accountants play an advisory role that is critical to the effective implementation of integrated reporting.

And if the accountant's mindset were oriented toward integrated reporting, then he or she could advise business leaders to think beyond profit — and to consider the impact of the company's business practices on society and the environment.

In sum, accountants can lead the way for corporations to become more responsible in terms of the broader good.

### 7. Integrated reporting incorporates non-financial forms of wealth – “capitals” – into a company’s balance sheet. 

Let's dig into the details of integrated reporting, which we introduced in the previous blink. Unlike traditional financial reporting, this approach incorporates nonfinancial information in a company's balance sheet.

It's worth noting that traditional reporting is highly technical and relies almost exclusively on numbers and statistics to measure a company's economic activities. Accordingly, these reports comprise information about two kinds of quantifiable wealth: financial and manufactured capital.

_Financial capital_ refers to the money that is available to an organization to produce goods and provide services. And _manufactured capital_ captures the physical objects (such as buildings and equipment) available to a company for producing its goods and services.

Well, integrated reporting is much less technical and has more to do with a company's broader story. It comprises four "new" nonfinancial forms of capital: intellectual, human, social and natural.

Breaking those down: _intellectual capital_ includes intellectual property like patents, copyrights or software.

_Human capital_ refers to the skills, experience, motivation and health of people associated with the company.

_Social and relationship capital_ describes institutions, relationships, communities and other networks. It also includes forms of trust (things like brand and reputation) that are attached to the organization.

And finally, natural capital refers to environmental resources like air, water, forests and the health of the ecosystem — each of which provides goods and services that support the organization's prosperity.

Importantly, these "new" capitals aren't directly owned or controlled by the business (think of carbon emissions or supplier labor practices). That's why these capitals lie outside the boundary of traditional reporting.

Additionally, since we haven't yet developed rigorous ways of quantifying these forms of wealth, integrated reporting is by nature nontechnical and narrative, describing how a company approaches these capitals.

And ultimately, this six-capitals framework encourages organizations to think beyond financial capitals and assess the company's health in a more holistic way.

### 8. Redesigning the corporation and extending legal rights to the environment would change the world. 

As we've discussed, profit maximization is the sole goal of most multinational corporations. And as a result, their daily business operations are destroying our planet.

And that's why designing a new kind of corporation is the foremost challenge of our time. The future of our planet hinges on redefining corporations so that they have a social purpose, pushing them away from pure profit-orientedness. Modern firms should be legally obligated to make a material, positive contribution to society and the environment, and be held accountable for failure to do so.

Attempts to redesign corporations have already been made. Consider the "B Corporation." The "B" stands for "benefit" — to workers, the community and the planet itself — and the idea is that, in order to become a certified B Corporation, a company must undergo rigorous testing to measure their social and environmental performance standards. Ice cream giant Ben & Jerry's is one example of a company that has undergone such a certification process.

It's worth underlining the fact that these firms are doing something truly radical by grafting conscience onto the old-style way of doing business. But making that happen on a broader level might require extending human rights to natural resources.

What does that mean? Well, society agrees that human beings ought to enjoy basic rights, like the right to life and physical integrity.

In contrast, common law treats natural objects as mere things — resources for humanity to master and use. But what if we endowed nature with the same legal rights as human beings? Or what if any person could act as legal guardian to a threatened ecosystem?

If we introduced legal rights for the environment, then water pollution and forest destruction wouldn't be permissible. A new body of law addressing the rights of the environment would uphold the dignity of nature within our society.

### 9. Final summary 

The key message in this book:

**Modern corporations privilege financial capital over every other form of wealth, including nature itself. Seeking to maximize profits at the expense of everything else, daily business operations are destroying both the environment and human society. Changing that and saving the planet requires firms to re-adjust their balance sheets to include non-financial forms of wealth.**

Actionable advice:

**Even if you're not an accountant, you can make a difference.**

Introduce accountants and executives at your firm to the concept of the six capitals — _financial, manufactured, intellectual, human, social_ and _natural_ — to transform your brand image, attract long-term shareholders and, ultimately, save the planet.

**Suggested further reading:** ** _Accounting Made Simple_** **by Mike Piper**

_Accounting Made Simple_ (2013) provides a brief introduction to the fundamentals of accounting, illustrating how to read the most important financial statements and draw a conclusion about the numbers. It also outlines the double-entry ledger system, a hallmark of accounting best practices.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Jane Gleeson-White

Jane Gleeson-White is the author of _Double Entry: How the Merchants of Venice Created Modern Finance_ , which won the 2012 Waverly Library Award for Literature. Gleeson-White studied at the University of Sydney and holds degrees in economics and literature.

