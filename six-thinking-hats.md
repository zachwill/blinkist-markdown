---
id: 53fba75237316200080e0000
slug: six-thinking-hats-en
published_date: 2014-08-26T00:00:00.000+00:00
author: Edward de Bono
title: Six Thinking Hats
subtitle: None
main_color: 618EF5
text_color: 4362A8
---

# Six Thinking Hats

_None_

**Edward de Bono**

_Six_ _Thinking_ _Hats_ offers you valuable tools for group discussions and individual decision making. The book shows ways to compartmentalize different ways of thinking to help you and your group use your brains in a more detailed, cohesive and effective way.

---
### 1. What’s in it for me? Learn how to brainstorm with the utmost efficiency. 

Many of us feel like our thoughts are always muddled; that they come to us without any rhyme or reason — emotions mix with rational thoughts, memories and hopes for the future — to form a tangled web that makes it difficult to think clearly, especially when it comes to making decisions.

And yet, since thinking is a skill like any other, we actually _can_ take active steps to improve it.

This book presents a complete system for making better decisions by untangling the complex web of thoughts that hamper our thinking.

The Six Hats method aims to compartmentalize these different kinds of thoughts in a way that allows each of them the time they deserve without letting them disrupt your thinking process.

After reading these blinks, you'll learn

  * when it's best to spill your emotions;

  * why it's so important to avoid eating the wrong berries; and

  * how one company solved a problem that was costing it $100,000 per day in only 12 minutes.

### 2. The six hats are tools that help us to conceive new ideas and improve thinking. 

What's the main obstacle in the way of thinking clearly? The answer: confusion.

When we process information, we confront a number of varying thoughts simultaneously: ones that are emotional, informational, logical, creative and future-oriented. Trying to process them all at once is like trying to juggle too many balls.

The solution? The Six Hats method.

In this method, each "hat" has its own, easy-to-remember color — white, red, black, yellow, green, or blue — that represents a certain type of thinking.

Here's how it can come in handy.

Let's say you're a manager and you want to know your team's emotional reaction to something. But you know they won't show their true emotions directly: people are afraid to be judged because of their emotions and would rather avoid the subject.

However, since everyone on the team knows that the red hat represents emotional thoughts, you could instead say "Let's try the red hat on" and the neutrality of the color allows the team to think emotionally without feeling embarrassed.

That's why, in practice, the hats are only referred to by color, never by function.

Furthermore, the Six Hats method ensures that the entire group is thinking the same way.

Imagine you're at a gathering at a large house: one person is standing in the front yard, another in the back yard, another on the roof, another in the dining room, and one on either side. Each person has a different view of the house: from the side it appears very long, while from the front it seems quite short.

Now, think about each perspective as a different hat. If you want to view a topic from the same vantage point, you'll all need to be wearing the same hat.

The six hats, therefore, enable your team in order to engage in _parallel_ _thinking_, in which everyone views the problem from the same perspective.

### 3. When using the six hats, keep some rules in mind. 

There are two ways to wear these hats: in single use or in sequence.

When the hats are used singly, they function as symbols indicating that you want to concentrate on a particular type of thinking.

Over the course of a conversation or discussion, you may reach a point where you need to generate some fresh opinions. Saying something like "I think we need some green-hat thinking here" allows you to easily convey this.

Once people have been trained in the vocabulary and usage of hats, they'll know exactly how to respond to your request: ask for green-hat thinking, and they'll know to think of alternative options.

Then there's the sequential use. In this case, use any hat you like as often as you like and in any order, depending on what your group wants to accomplish. For instance, the leader may ask the group to switch from the white to the yellow hat and leave out the red hat entirely!

Regardless of how you use the hats, you'll need to consider both discipline and timing.

Members of the group must have the discipline to use and maintain only the hat that is required at that present moment. Only the group leader, chairperson or facilitator has the power to change it.

Luckily, developing that discipline only gets easier with practice — so keep at it.

Whenever your group puts on a hat, it's important to keep your time focused in order to foster concentration and reduce idle talk.

For this reason, it's recommended that your group spends one minute per person under a particular hat. For example, if there are four people in the meeting, you'd spend four minutes wearing each hat.

Now that you understand the purpose and importance of the hats, the following blinks will examine each one individually.

### 4. The white hat involves thinking like a computer: look for data and information. 

Imagine role-playing as a computer: you'd compute calculations and regurgitate facts in a neutral, objective manner — all in your best robot voice, of course. And you could forget about interpretation — that wasn't programmed into your software! You only care about the facts.

That's what thinking with the _white_ _hat_ is like.

The white hat is used to discuss whatever information is currently available and to identify any information that might also be necessary. When wearing the white hat, you're not concerned with opinions but facts and figures.

This means that there are _no_ _arguments_. If facts contradict one another, you treat them equally.

So when's the right time to put on the white hat?

Usually, in order to lay down the foundation of your discussion by providing background information at the beginning of a thinking session. You can also use it at the end of a thinking session as a sort of debriefing, or to confirm that proposals agree with existing information.

Imagine, for example, that the head of marketing of your company wants to overhaul her current strategies. She can begin a team meeting by requesting white-hat thinking to lay out the current strategy's hard numbers, like its scope and budget.

You can also use the white hat to find alternatives when proposals contradict existing information.

For example, after considering a new strategy that would exceed the budget, the same head of marketing could request additional information, like how many employee hours are available.

With white-hat thinking, you contemplate what information you have and what data is missing. Then ask yourself how to best acquire that information.

Above all else, remember that, when using the white hat, it is critical to stay neutral and objective. White, absent of all color, indicates neutrality. Thus, there's no room for maybes — only facts.

Don't start white-hat thinking by stating what you believe to be true. Stick to hard facts, like statistical data.

> _"Just the facts please — without the argument."_

### 5. Be honest with your emotions when you wear the red hat. 

So what's the opposite of neutrality, objectivity and raw data? That would be the _red_ _hat_, representing fire and warmth. Prepare to get emotional!

Wearing the red hat gives your team members an opportunity to express their feelings, emotions and intuition without any need to explain or justify them.

Your intuition is likely based on an accumulated experience, so even saying things like "I feel this is a risky tactic" can be useful. However, it's important to note that intuition isn't always correct, and should therefore be treated with caution.

A range of feelings can be expressed under the red hat: you can express your enthusiasm ("I love it!), ambivalence ("I'm not so sure about this.") or even dissatisfaction ("I hate it!").

Of course, not all emotions fit neatly into these categories. If people say they have "mixed feelings" about something, ask them what feelings are in the mix.

This can be tricky, as what's considered appropriate expression of one's feelings varies across cultures. Whereas in the United States someone might express their dissatisfaction in a very direct way ("What a lousy idea!"), in Japan, these same feelings would sound much more contained ("I must consider it.").

It's also important that the leader never ask anyone to explain or justify their feelings. When people feel like they have to validate their feelings, they'll only convey those that can be validated.

Rather, just express the feelings as they arise in that moment.

Red-hat exercises should always have full participation, i.e., every person in the group should voice their feelings on the subject. No one should be allowed to pass.

The group must also give everybody the sense that their feelings are important by listening carefully. This ensures that voicing an unpopular emotion or being alone in expressing a particular emotion will not be met with condescension or punishment.

Whenever you put on the red hat, you're allowed to express _exactly_ how you feel about the topic.

> _"Any good decision must be emotional in the end."_

### 6. The black hat cautions you against looming dangers and negative outcomes. 

Imagine you're wearing your black judge's robes as you preside over a murder trial. You're the one responsible for weighing all evidence with the utmost scrutiny and prudence — after all, someone's life is hanging in the balance.

That's what it's like to wear the _black_ _hat_ : you stay within the law, point out difficulties and problems, and hold tight to values and ethics.

The black hat is all about cautiousness. It stops us from doing things that will ultimately work against our interest, e.g., illegal, dangerous or unprofitable things.

We base our critical judgments on past experiences by using an inherent mechanism called the _mismatch_ _mechanism_. Just as an animal learns which berries are poisonous (based on inherited experience), your company's survival can depend on which fruits it chooses to pluck.

You can avoid these sorts of mistakes when you wear the black hat: it will help you find the elements of your policy, strategy, ethics, etc., that contradict one another.

Just make sure you don't let the black hat make you overly cautious. Some people spend too much time under it, obsessing over finding faults. Here's a comparison: we know that food is essential for life. We also know that too much food can make us overweight, cause health problems and even lead to death. An obsession with the black hat functions in much the same way: while the black hat is necessary for decision making, you don't want to overdo it.

One of the great values of the Six Hats method is that we balance things out by making time for _each_ hat. So don't spend all your time under the black hat!

> _"It is much easier to be critical than to be constructive."_

### 7. The yellow hat lets you play the role of the eternal optimist. 

Have you ever had one of those moments when everything seemed right in the world? When everything had a silver lining? Those moments occur when you think with the _yellow_ _hat_, characterized by optimism and a focus on potential benefits.

The yellow hat is harder to master than the black hat: our mind is naturally inclined to warn us against danger, not seek out sunshine and rainbows.

To be able to find the positive, we need to develop _value_ _sensitivity_, i.e., the awareness that there's value in even the most unattractive ideas.

The yellow hat is crucial because an idea's positive impacts aren't always immediately apparent, and trying to achieve something where the benefits are unclear is a waste of time.

When wearing the yellow hat, you should consider things like how your new marketing plan could potentially change your brand's image and reach new customers. Otherwise, what's the point of making a new plan?

Crucially, the positivity of the yellow hat should be logically based. Although the yellow hat strives to find value, that doesn't mean we should deceive ourselves. Because while the yellow hat permits us to express our visions and dreams, it also values realism over fantasy, so these dreams should be framed by realistic expectations.

In other words, when you don the yellow hat, you don't fantasize about how your new marketing plan will save your company, make you a trillionaire and put your face on magazine covers. Instead, you think about what good things your new marketing plan might _realistically_ achieve for your company.

Yellow-hat thinking poses questions like: What are the potential values of this idea? Whom do they benefit? Under what circumstances? How are the values delivered? What other values are there?

If an idea is promising and logical, the yellow hat can help motivate and encourage your team to take the steps to make it happen.

### 8. The green hat makes space for the zaniest ideas. 

Now it's time to get creative! The _green_ _hat_ is all about new ideas, concepts, alternatives and alternatives to alternatives.

These include both the obvious alternatives to the status quo as well as fresh, zany ideas. Wearing the green hat means being open to _any_ possible improvements to your situation and to consider crazy alternatives that might seem impossible!

In addition, the green hat permits us to consider possible outcomes. Because without a creative vision for the future, progress can come to a standstill.

Just think: two thousand years ago Chinese technology was far more advanced than Western technology. But between then and now Chinese technological progress seemed to have run into a wall. But why?

The most common explanation is that the Chinese were simply too satisfied with their situation. They didn't fantasize about future possibilities — which made it impossible for them to find the incentive to progress.

What's more, the green hat can also be used to overcome the difficulties encountered during black-hat thinking. Whereas the black hat might have helped you pinpoint a major danger in your new marketing strategy, the green hat might help you figure out a creative way to avoid these dangers or brainstorm completely new ideas.

However, in wearing the green hat, it's important to remember that creativity is no longer just the business of the one "creative person" in your group while everyone else eagerly awaits their sagely wisdom.

_Everyone_ is encouraged to try on the green hat and make a creative effort.

In order to do this, you'll need to appropriately manage your group's _expectations_. People are very good at doing exactly what's expected of them — not more, not less. So, when you expect your group to be creative under the green hat, even those who've never considered themselves the "creative type" will start making a creative effort.

As their confidence increases, so will the number of creative ideas.

### 9. The blue hat is about process control at any time. 

The _blue_ _hat_ is the hat for "thinking about thinking." Like a hawk flying high into the blue sky to gain a better perspective of the fields below it, the person wearing the blue hat aims to get an overview of the other hats.

The blue hat is used at the beginning of a thinking session to define the purpose and aim of your session. It might define a particular problem that needs to be addressed or the parameters of your brainstorming session.

Under the blue hat, you consider which other hats will be needed during a thinking session in order to tackle a given problem. Maybe this particular session will require a lot of red and green thinking or black and white thinking.

Whatever the case may be, all of this is determined under the blue hat.

Typically, the blue hat is worn by the facilitator, chairperson or leader of the session. This is a permanent role and, as such, the group leader will not remove the blue hat during the session. However, during a specific blue-hat session, in which everyone dons the blue hat, anyone can make procedural suggestions.

During the session, the blue hat helps maintain the discipline of the group, announce when it's time to change hats and ensures that everyone is wearing the right hat.

At the end of the session, it's the blue hat that asks for the _outcome_. This may come in the form of a summary, conclusion, decision or solution, etc.

The blue hat also lays out any next steps that will need to be taken after a session, such as the next actionable steps in a project or the topics for the next meeting.

So now you know all about the different kinds of hats and what they're used for. But why should you use the Six Hats method? The final blink will answer that question.

### 10. The Six Hats method will save you time, money and headaches. 

Now that you've learned all about the Six Hats method, you might be wondering _why_ you should adopt this strategy over others. There are three main reasons why the Six Hats method is so successful.

First of all, it saves time.

Here's one real-life example: a week after the _Financial_ _Times_ published a short write-up of the Six Hats method _,_ the author received a letter from a man who told him about how he and his wife had been arguing for a long time about whether or not to buy a large house in the country.

But once they read the write-up, they decided to use the Six Hats method. By compartmentalizing the different elements of their conversation — factual, creative, emotional and so on — they were able to come up with a decision that satisfied them both in only ten minutes.

Then there's the major corporation ABB, which used to spend 20 days on their multinational project team discussions before reaching a conclusion. Using the Six Hats method, these discussions now take as little as two days!

Second, it saves money.

Norwegian multinational oil and gas company Statoil is a shining example of that. The company once had a problem with an oil rig that was costing it about $100,000 _per_ _day_. A certified trainer came in and introduced them to the Six Hats method and solved their problem in just _12_ _minutes_. Just imagine how much money they saved!

Finally, the Six Hats method reduces irritation and provides an end in sight.

Imagine you're in a car with three other people and you have to drive somewhere but everyone only vaguely know the roads. There's bound to be many heated arguments over which road to take, which will only get worse the longer you're on the road.

However, if you have a road map, then it's much easier to choose the best route, making the best possible choice become obvious to all. The Six Hats method gives you that map.

### 11. Final summary 

The key message in this book:

**Thinking** **is** **a** **skill** **just** **like** **any** **other** **and** **can** **be** **improved!** **Using** **the** **Six** **Hats** **method,** **you** **will** **be** **able** **to** **think** **more** **effectively,** **solve** **problems,** **develop** **creative** **solutions,** **stop** **wasting** **valuable** **energy** **and** **falling** **prey** **to** **disorganized** **and** **chaotic** **thinking.**

**Suggested** **further** **reading:** **_The_** **_Wisdom_** **_of_** **_Crowds_** **by** **James** **Surowiecki**

_The_ _Wisdom_ _of_ _Crowds_ explores why, and under which circumstances, groups of people can come up with better solutions to problems than any one person — even if that person is an expert. By analyzing the way individuals and groups make decisions, the blinks get to the bottom of the wisdom of crowds, and show how this wisdom can be used to make reliable decisions.
---

### Edward de Bono

Edward de Bono was a Rhodes Scholar at Oxford and has held faculty appointments at the universities of Oxford, Cambridge, London and Harvard. In addition to his academic career, he has offered consultation to many multinationals, including IBM, Procter & Gamble, Shell and Ford. His bestselling books include _Lateral_ _Thinking_, _De_ _Bono's_ _Thinking_ _Course_ and _Teach_ _Your_ _Child_ _How_ _to_ _Think_.

