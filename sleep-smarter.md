---
id: 5d00b63c6cee070007f2858d
slug: sleep-smarter-en
published_date: 2019-06-13T00:00:00.000+00:00
author: Shawn Stevenson
title: Sleep Smarter
subtitle: 21 Essential Strategies to Sleep Your Way to A Better Body, Better Health, and Bigger Success
main_color: 51B7DC
text_color: 35778F
---

# Sleep Smarter

_21 Essential Strategies to Sleep Your Way to A Better Body, Better Health, and Bigger Success_

**Shawn Stevenson**

_Sleep Smarter_ (2014) is your quickstart manual for improving the quality of your sleep. Drawing from a host of scientific studies and punctuated with practical tips, this accessible guide promises to change the way you think about sleep and give you a better night's rest, starting tonight.

---
### 1. What’s in it for me? Learn the keys to some quality Zs. 

We've all been there: waking up from a bad night's sleep, we drift through the day like half-conscious zombies, yawning uncontrollably and unable to focus on our work. We dream about the moment we can get back to bed, but as soon as our head hits the pillow, we're suddenly wide awake. It's a nightmare.

But it doesn't have to be this way. Sleep can be a wonderful, rejuvenating experience, a time for repairing our bodies and refreshing our minds. And although the modern world seems intent on destroying the sanctity of sleep with screens, artificial lights and strenuous demands on our time, you can use modern science to push back against this.

That's where these blinks come in _._ They'll equip you with the fundamentals of sleep science and their relationship to our physiology. And with them in hand, you'll be armed with a host of practical, biology-backed tips, which will help you drift off faster and sleep deeper. This knowledge will have an instant, noticeable impact on your life.

In these blinks, you'll learn

  * Which specific hormones play a vital role in rest and recovery;

  * Why sleep is more rejuvenating during a certain period of the night; and

  * How houseplants can improve our rest.

### 2. The importance of sleep is overlooked and underrated. 

In our modern world, which prizes speed and productivity, sleep can seem like such a waste of time. If we're swamped with work or overwhelmed by a laundry list of tasks, the first thing that we sacrifice is our sleep. This war on rest has become so acute that the average person's sleeping hours have decreased from roughly nine hours to seven.

But there are big problems with painting sleep as the enemy of productivity. When you're sleep deprived, not only are you slower and less creative — you also accomplish less.

One study, published in the medical journal _The Lancet,_ found that surgeons awake for over 24 hours took up to 14 percent longer to complete tasks and made 20 percent more mistakes once they did. And if there's one person you don't want to slip up, it's your surgeon.

One primary reason for this is that sleep deprivation reduces the brain's glucose content — an essential carbohydrate that your grey matter uses as fuel. And this glucose starvation doesn't affect all parts of your brain equally; the ones that are hardest hit are your parietal lobe and prefrontal cortex, which are involved with problem-solving and high-level thinking. After 24 hours without sleep, glucose in these regions decreases between 12 and 14 percent.

Furthermore, several studies have shown that skipping one night of rest makes us as insulin resistant as a type-2 diabetic. Having an insufficient amount of this essential hormone leads directly to weight gain, signs of aging and decreased sexual drive.

Thus, sleep is not the enemy of productivity or an obstacle to overcome. It is a necessary restorative state, vital for our physical and psychological well-being. We simply cannot be healthy or function at peak performance without good quality sleep.

That's partly because being awake is _catabolic,_ while sleep is _anabolic._ Catabolism occurs when molecules oxidize — they combine with oxygen, causing them to break down. Anabolism is the opposite of this: it constructs molecules from smaller units, literally building us up. So, while we sleep our bodies are repairing themselves — fighting signs of aging and strengthening our immune, muscular and skeletal systems.

Hopefully now you understand the value of a good night's rest and that scaling back on sleep in the name of productivity is _counter_ productive. So, what can you do to increase the quality of your Zs? Find out in the next blink!

### 3. When it comes to sleep, light is our best friend and worst enemy. 

Sleep is a basic life need we cannot compromise on. And now that we've seen the light, it's time to talk about just that: light. It's something we usually take for granted, but it has a huge impact on the quality of our rest.

To understand this, we need to understand melatonin. This is a hormone produced by our brain's pineal gland, and it has powerful rejuvenation and antioxidant properties. But, most importantly, it regulates the body's circadian rhythm — our internal body clock, which tells us when to sleep.

Our production of melatonin is heavily affected by exposure to light; when the sun goes down in the evening, our bodies naturally release the hormone, making us sleepy. And people exposed to light early in the mornings will produce more melatonin in the evenings, allowing them to fall asleep faster and in a deeper state once they doze off.

This is why we should try to maximize light exposure during the day. And it's particularly important to expose ourselves to sunlight early in the morning, because bright light also prompts our brains, organs and glands to wake up and be alert. With this in mind, try getting a short walk in somewhere between the hours of 6:00 a.m. and 8:30 a.m.

At the other end of the day, in the evenings, limit your exposure to screens, starting 60 minutes before bed. Electronic devices pump out a heavy blue spectrum of artificial light. Your body is especially sensitive to this spectrum, and exposure to it in the evening inhibits the release of melatonin, making it harder to sleep. There is software available for phones and computers that filters this blue spectrum out, but it's better to steer clear of the screens completely and read a book before bed instead.

Also, ensure you're sleeping in a pitch-black room. Light isn't just absorbed through our eyes, but our skin as well. Nightlights and daylight rays peeking through curtains inhibit melatonin production, which leads to lighter and shorter sleep. Studies have shown that light in bedrooms can suppress melatonin levels by over 50 percent, so blackout blinds are a must for those sleeping after sunrise!

But an easier — and cheaper — way of blocking out light is to ensure we get all of our rest when the sun is down. And this leads us onto our next important factor for sleep: timing.

### 4. For good-quality sleep, timing is everything. 

It's all well and good to do the right thing, but doing it at the wrong time is completely counterproductive, like applying sunscreen in the evening or watering flowers in the rain. This is especially true for sleep. We can do everything right, but if we don't pay attention to our timing, we won't get the best rest we possibly can. So let's look at some timing tips that will improve our nightly rest.

First, we should respect our internal body clock by going to bed within 30 minutes of the same time every night.

The modern world of work encourages us to cut down on sleep during the week and catch up on weekends — but this wreaks havoc with our circadian rhythms. Our bodies don't know when it's Sunday; they just can't adapt quickly enough to incorporate our late-night Netflix binges on weekends! If we hit the hay at a consistent time, our circadian rhythm will run smoothly, and we'll find it much easier to fall asleep and wake up.

Second, we should tuck in and wake up early.

One 2008 study by researchers at the University of North Texas makes the case for early mornings. It found that students identifying as morning people achieved higher academic grades, averaging a 3.5 GPA, compared to just 2.5 for night owls.

The reason for this is encoded into our DNA. Humans are a _diurnal_ species, meaning that we're active during the day. And for most of human history, we've been hunter-gatherers. Our ancestors were programmed to sleep at sundown because snoozing during the day was a very effective way of being eaten by a predator.

So, for thousands of years evolution has conditioned humans to react to the Earth's patterns of light and darkness. Only in the last 150 years have humans started to override this instinct with the invention of the light bulb. But 150 years is a fleeting moment on the grand scale of evolution, and our bodies haven't caught up to our new nocturnal habits.

Third, we should take advantage of the magic window of sleep between 10:00 p.m. and 2:00 a.m. This is when our bodies reach their peak production of hormones such as melatonin and human growth hormone. This means that sleep during this time is deeper and more rejuvenating than sleep after 2:00 a.m. That's bad news for night-shift workers: they can sleep a full 8 or 9 hours after work, but they'll have a less restorative sleep than someone who sleeps during money time.

### 5. To snooze soundly, make the correct lifestyle choices. 

Understanding the biological aspects of sleep is an enormous help, but biology is only one side of the coin. To push our sleep game to the next level, we need to combine this knowledge with the right lifestyle.

First up, we need to set an unbreakable caffeine curfew.

Caffeine is a nervous system stimulant, one that on a molecular level is very similar to _adenosine._ Adenosine is a chemical that our brain produces while we're awake, and once our adenosine levels hit a certain point, we start to get sleepy.

Because of its similar nature, caffeine fits snugly into the very same receptors in our brain that adenosine is trying to enter, blocking its path. That's why we don't feel tired after drinking a coffee.

But caffeine has a half-life of eight hours, meaning that 200 milligrams at 7:00 a.m. becomes 100 milligrams at 3:00 p.m., and 50mg at 11:00 p.m. In short, it sticks around a long time. That's why we should refrain from caffeine after 4:00 p.m. It allows our bodies to flush enough of it out that we can rest.

Evening alcohol consumption is another thing we should curb.

Although a few drinks might help us to fall asleep, it's highly disruptive once we're away, limiting the amount of time we spend in the deeper stages of sleep. In particular, alcohol interferes with REM sleep — the sleep stage responsible for memory processing. This means that alcohol-laced snoozes are terrible for memorizing facts and fortifying intelligence. Sorry, students!

So if you're out drinking, try laying off the booze a few hours before you hit the sack. Also, drinking a glass of water for every alcoholic drink you have will help combat the nasty effects alcohol has on your body.

Thankfully, though, there is some good news in this blink: you should have an orgasm before bed.

That's because having a big O is a natural sedative, releasing a cascade of chemicals into your system, like serotonin and oxytocin. These two compounds are natural stress reducers, and they trigger the release of feel-good hormones called endorphins. With this cocktail of relaxing chemicals and hormones coursing through your system, you'll find it much easier to drift away.

### 6. Maintaining a healthy mind and body is crucial for a great sleep. 

As we've seen, good quality sleep is essential for maintaining a healthy body and a sharp mind. But the opposite is also true: a healthy body and mind are vital if we want a good night's sleep.

Let's start with the body. Did you know that one of the benefits of exercising is better quality sleep?

Because exercise creates micro-tears in our muscle tissue, which our body needs to repair, our brain lets loose a host of rejuvenating anabolic hormones like testosterone and human growth hormone (HGH). All this repair work means the body induces a deeper, more restorative sleep.

Supplementing with magnesium is another great way to stay healthy and supercharge your snooze.

This mineral is responsible for over 300 biochemical reactions in the body, but it's particularly good for reducing stress and calming our nervous system. Magnesium balances blood sugar levels, optimizes blood pressure and relaxes muscles, which all translate to a more relaxed state and a higher-quality sleep.

But when supplementing magnesium, apply it to your skin in cream form. This is the most effective way, because much of this mineral's power is lost during digestion, making oral supplements weaker.

Some will know it's possible to supplement melatonin — the hormone closely associated with sleepiness. This can be effective, particularly for insomniacs, but there are risks involved with it. The most important takeaway is that supplementing melatonin can inhibit your body's ability to produce this naturally. For that reason alone, it should be considered a last resort.

Maintaining a healthy mind is also key to high-quality sleep — and for this, we need look no further than meditation.

It's estimated that more than 50,000 thoughts run through our minds each day. And although most of them are short-lived, our inner monologue often intensifies when we're trying to drift off to sleep. But meditation can change this. The simple act of closing our eyes and focusing on our breathing, even just for 10 minutes, is proven to lower stress and release feel-good endorphins into our system, priming us to drift off. In short, meditation is like a tonic for our manic, hyperactive minds.

And what better place to start meditating than in your new sleep sanctuary? Find out more about that in the next blink.

### 7. Creating a sleep sanctuary gives us the best possible environment to rest in. 

What's the use of all these sleep suggestions if we're going to ignore the _place_ we sleep in? Our surroundings have a huge impact on the quality of our rest, so taking sleep seriously means taking our bedrooms seriously by creating a _sleep sanctuary._

The key point here is that our sleep sanctuary should have fresh, clean air and provoke feelings of relaxation.

Introducing house plants into the bedroom is a great way to achieve this. The organic sights and earthy smells of greenery have been shown to induce calm and increase happiness, and plants are excellent natural air filters. And maintaining good air quality is crucial when creating sleep sanctuaries.

That's because air contains _ions,_ which are simply atoms with electric charges. Ions with a negative electrical charge are extremely energizing, and they improve our health by oxidizing mold, parasites and toxic chemical gases. But over time, the air we breathe becomes stale as the oxygen content drops and ions lose their negative charge.

Plants fix this, converting carbon dioxide into oxygen and recharging the air's ions. One great houseplant for your sleep sanctuary is English Ivy. Not only is this plant extremely hardy; it was also found by NASA to be the best air-filtering houseplant, pumping out oxygen and absorbing formaldehyde, a harmful neurotoxin all too common in industrialized countries.

Another great choice is mother-in-law's tongue. This is a tough piece of flora, needing minimal water and light to flourish — perfect for a dark bedroom! Most remarkable, though, is its ability to convert carbon dioxide into oxygen at night; most plants only do this during the day.

But if there's one golden rule for sleep sanctuaries, it's this: keep work out of the bedroom.

Bringing phone calls, texts and emails into our sleeping space is one of the worst things we can do. That's because it creates a spike in our cortisol levels — a hormone closely associated with stress and wakefulness. It also leads our brains to create a negative association with our bedroom, which subconsciously makes it more difficult to sleep there. We need to be strict with ourselves and not allow work to enter our sleep sanctuary; this is a place for us to switch off and zone out.

All that's left now is to enter your new sleep sanctuary armed with all of these practical sleeping tips, and to get ready for the best night's rest you've ever had!

### 8. Final summary 

The key message in these blinks:

**Don't listen to the screams and shouts of modern life: sleep is something to embrace, not overcome. With a basic understanding of the biological processes involved in sleep, some sensible lifestyle changes and the construction of your own sleep sanctuary, you'll find yourself quickly falling into a deep, rejuvenating sleep — one that keeps you younger, more healthy and performing at the peak of your mental capabilities.**

Actionable advice:

**Ease yourself into early mornings.**

If you usually wake up at 8:00 a.m. but want to change your ways, don't try waking up at 6:00 a.m. straight away. This will be a shock to your system, and it will create a negative association in your brain, which will make it even harder the next day. Instead, gradually push your alarm clock time back 15 minutes every morning until you've reached your ideal wake-up time. You'll find this much easier, because it consumes far less of your willpower.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Better than Before_** **, by Gretchen Rubin**

With the bundle of useful sleeping tips you've now got under your belt, you should be excited to turn in tonight and begin your wholesome new sleeping habit. There's nothing more fulfilling than developing healthy habits and conscious self-improvement. And the winds of change don't have to stop blowing here. Why not take this opportunity to give your life a little spring cleaning and revamp other areas, too?

If that sounds like a great idea, check out the blinks to _Better Than Before._ Written by Gretchen Rubin, the talented author of a string of bestsellers that include _The Happiness Project_ and _Happier at Home, Better Than Before_ runs you through the theory and science of habit making and breaking. Teeming with useful advice and insightful anecdotes, these blinks will help you break toxic habits and create new, beneficial ones.
---

### Shawn Stevenson

Shawn Stevenson studied business, biology and kinesiology at the University of Missouri–St. Louis, before founding Advanced Integrative Health Alliance — a company providing wellness services for individuals and organizations worldwide. He is also a keynote speaker and podcaster, having spoken at TEDx and created _The Model Health Show_ podcast.

