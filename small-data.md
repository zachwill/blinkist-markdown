---
id: 573f039c679fb60003483ffb
slug: small-data-en
published_date: 2016-05-23T00:00:00.000+00:00
author: Martin Lindstrom
title: Small Data
subtitle: The Tiny Clues That Uncover Huge Trends
main_color: FDCD33
text_color: 7D6519
---

# Small Data

_The Tiny Clues That Uncover Huge Trends_

**Martin Lindstrom**

_Small Data_ (2016) is a guide to utilizing minor details about people's lives to connect with them and sell them on your brand image. These blinks incorporate observations of cultures all over the world to point to the emotions and desires that help brands become household names.

---
### 1. What’s in it for me? Uncover the small bits and pieces that make for a great marketing concept. 

Today, everyone talks about big data. It's the buzzword of modern-day internet giants like Google, Facebook and Amazon, who collect masses of personalized data — surfing habits, interests or buying behavior — to try to infer what people like, how they tick, what desires they might have and what they are likely to buy. Big data truly is the Holy Grail of marketers today.

But is big data really the one and only savior when it comes to marketing and making a brand successful? The simple answer is no! And this is where _small data_ comes in.

Small data are the minor but telling details in our lives that can be found everywhere in or near our homes. These blinks will take you on a detective's journey through people's living rooms, garages and drawers to show you what the small things, like vacation snaps, souvenirs or collections, can reveal about our desires and needs. These blinks will tell you how this small data can help you come up with great marketing concepts that are tailored to what your customers really want.

You'll also learn

  * what your fridge can tell you about your personality;

  * what you can infer from the knowledge that teenage girls take 17 selfies each morning; and

  * why it was vital for the Roomba cleaning robot to say "oops" and "dood-dood."

### 2. Big Data has a hard time predicting the emotions and desires of users. 

Chances are you've heard of _Big Data_ — the term has been a buzzword for years. As you might know, it refers to observable patterns in huge amounts of data that offer insights into consumer behavior on online shopping platforms like Amazon.

But despite the massive quantities of data users produce, the information this data offers is actually quite limited.

People search shopping sites, click on ads, stream videos or post on Facebook every day, in the process creating data about the products they like, their tastes and even the way they feel. However, this online behavior is a poor representation of who users really are, especially when it comes to emotions. When people are online, they tend to be less empathetic.

Just as being in a car can make you more rude toward other drivers, being online makes it easier to post a mean reply because you can't see the other person's reaction.

So, the information people leave online is insufficient to make judgments about them. While big data does allow for some emotional analysis, it doesn't offer the specific data necessary to measure or build desire for a brand.

For instance, Google's algorithms have a 70 percent chance of inferring a user's emotional state just from the way they write and the typos they make. But it's very difficult to determine whether a brand can appeal to a user's emotions based solely on data gathered from web browsing.

This is important because such information is essential for great brands to stand out by making consumers desire their products. The makers of the BMW Mini, for example, know that the desire for their car is fuelled by customers' urge to indulge in the joy of driving.

So, online behavior is insufficient to paint a complete picture of consumers. But where _can_ you find the information you need to build a highly desirable brand?

> _"Big data is pretty incompetent at suggesting how to increase love."_

### 3. The tiny details of people’s homes say a lot about them and their desires. 

Home is one of the most personal spaces we have. Everything in our houses is there for a reason, and whether consciously or not, we've made decisions about how to arrange everything from our shoes to our kitchenware.

It's these tiny details that make up _small data_, which can be analyzed to determine what makes people tick and what they value. In fact, the way we decorate our houses alone reveals a great deal about how we want to see ourselves — and be seen by others.

Much like the way we showcase our lives on social media, we put our lives on display to every person who visits our home by showing off photos, souvenirs and our taste in design; every little detail can be telling.

For instance, in Brazil, the author saw lots of people display beer bottle collections in their homes. He knew that Brazilian people are highly appreciative of their beer culture and inferred that these collections were indicators of an easy-living and freedom-loving lifestyle.

But our homes also reveal our wishes, needs and desires, whether conscious or not. These tend to be found in places that aren't intended for public exhibition like our garages, drawers and fridges.

In Siberia, the author saw that lots of people use fridge magnets with travel motifs. In his opinion, these travel motifs pointed to a desire for travel and an escape from the daily grind.

And that's not the only way a fridge can be telling. If you put healthy foods like vegetables and low-fat yogurt at eye level and unhealthy, fatty foods lower down, you might be at odds with yourself about your eating habits; in other words, you might wish to lead a more healthy lifestyle than you do.

So, now that you know what small data _is_, it's time to learn how it will lead you to a breakthrough branding concept.

> _"If you want to understand how animals live, you don't go to the zoo, you go to the jungle."_

### 4. Marketing ideas begin by gaining an authentic perspective on your market, noticing peculiarities and looking for imbalances. 

Now you know that small data is a tool to help companies learn more about customers — but _how_ exactly?

Well, it's essential to start by uncovering authentic perspectives of your market and its culture. A great way to do this is through local foreigners who see lots of people — perhaps foreign cab drivers or hairdressers. These people came from outside the local culture and are thus better equipped to spot its peculiarities.

But another source might be your target demographic itself. For instance, while working for the clothing line Tally Weijl, the author asked teenage girls to compose a month-long video diary about what was on their minds.

Once you've gained an authentic perspective, it's time to pick out the peculiarities of the culture that your marketing target group belongs to. For instance, in the case of Tally Weijl, the author realized that the girls spent as long as two hours every morning coordinating outfits with their friends.

Or, when charged with redesigning the American supermarket chain Lowe's, the author picked out peculiarities of suburban American culture. He noticed that children in the suburbs tend not to play outside, that there are no downtown areas where people congregate and that church attendance has experienced a rapid decline.

Finally, it's essential to search for imbalances. In the case of Lowes, the author's observations of low church attendance, empty public spaces and few children playing outside pointed to a lack of community. The author then addressed these imbalances by encouraging the supermarket to feature local products prominently.

Or, in the case of Tally Weijl, the author noticed that the girls would send about 17 selfies every morning, implying that they had an intense desire for advice and social approval before committing to an outfit.

So, whether it's a lack of community or social approval, the next step is to learn how these little imbalances can be channeled into a successful marketing strategy.

> _"In the search for small data, almost nothing is off-limits."_

### 5. Imbalances cause desire and addressing this desire is the key to a brand’s success. 

Can you think of something every single human would agree upon? Well, just about every one of us feels like we are lacking something.

Many people feel imbalanced in one way or another, and these imbalances eventually turn into desires. For instance, when the sales of the Roomba, an autonomous robot vacuum cleaner, began to drop, the author was brought on board to find out why.

What he discovered was that many Roomba owners didn't store the cleaning robot away, but used it as a conversation piece and even gave their robot cleaners names. He combined this finding with a previous observation — the lack of community in American suburbs — and realized that many Roomba owners were out of balance because they were feeling lonely.

This imbalance caused them to desire the product. People were especially keen on the idea that the Roomba would say "oops" or "dood-dood" when moving around their homes. The problem was that a new version of the robot was produced without these sounds, thus causing a decline in sales.

Desire is a foundational aspect of marketing and addressing the desires caused by imbalances can make for a successful brand. In the case of the Roomba, the author proposed giving the robot its voice back to address customers' feelings of loneliness; the product was soon selling well again.

And what about the teenage girls from Tally Weijl?

To address their desire for advice and social approval, the author suggested installing mirrors in the changing rooms that were also giant computer screens that let customers log into their Facebook accounts.

This allowed them to connect to their friends, live stream their outfits and call for a vote to receive instant feedback on which shoes or shirts to buy. Once the company addressed its customers' desire for social approval, Tally Weijl's sales rose and the company's Facebook fans quadrupled.

> _"Desire is full of endless distances."_ — Robert Hass, poet

### 6. Customers buy from brands that convey emotion successfully. 

Have you ever heard of Paris syndrome?

Paris syndrome refers to how many tourists, especially those from Asia, experience intense culture shock that may even land them in the hospital upon visiting a city, simply because it falls short of their inflated expectations. The name refers to the fact that the city of Paris is itself a brand associated with romance, love and fine food, all of which entice visitors to book a trip.

In fact, every amazing brand has an emotional aspect to it that it promises to impart to its customers. For instance, Apple's name not only stands for high-quality technological products, but also for desirable design and creative innovation. As a result, lots of the people who buy Apple products hope that these qualities will permeate their lives.

So, when a brand successfully conveys an emotional aspect, its products will become successful. Consider once more the American grocery store chain Lowe's. In the case of this brand, the author figured out that lots of the store's customers felt lonely because they lacked community.

Using this information, he reinvented Lowe's brand image by creating a more local and welcoming store atmosphere that appealed to customers' desire for community. The result was a huge success.

But how did the author do this? By suggesting small changes. Here are a few of them:

He proposed that the store incorporate a local round table where shoppers could connect with local farmers or attend fruit-cutting classes for kids. In addition, the butchers started handing meat to customers with two hands, making them appear more caring. Finally, the staff would sing and perform a short chicken dance every time a freshly grilled chicken came off the barbecue.

After these simple changes, shoppers said that they felt "at home" in the store — and it's all because humans use the promises of brand images to identify and replace what's missing in their lives.

> _"Innovation often is the juxtaposition of two things that have not been combined before."_

### 7. Final summary 

The key message in this book:

**Every culture, country and human being has unsatisfied desires. These wishes can be uncovered by examining the small details of how people arrange their worlds, and can be utilized by brands to create images and products that draw customers in by promising to satisfy their desires.**

Actionable advice:

**Create a "Permission Zone" to help customers let loose.**

Permission Zones are spaces that let people switch into an alternate emotional state, where we allow ourselves to do things we usually wouldn't. A great example is when we visit the zoo: we stroll around aimlessly and eat food we usually wouldn't, justifying it simply through the fact that, well, we're at the zoo!

To get customers to make such a switch, you need to wake them up and cause them to operate according to a new set of rules. For instance, the fast food chain Five Guys established just such a zone by showcasing bags of potato chips all the way from the entrance to the counter, thereby making it more acceptable for its customers to eat high-calorie foods. Establishing such zones in your business can entice customers to bend their self-imposed rules.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Buyology_** **by Martin Lindstrom**

Day in and day out we're bombarded by thousands of brand images, logos and commercials enticing us to buy their products. However, only _some_ ads actually motivate us to whip out our wallets. Why? Using cutting-edge neuromarketing methods, _Buyology_ answers that question and explores the hidden motivations behind our purchasing decisions.
---

### Martin Lindstrom

Martin Lindstrom is a world-renowned marketing and brand-building expert, and he has been an advisor to the likes of Walt Disney, Pepsi and Red Bull. A study of 30,000 marketers named him the number one brand-building expert of 2015, while _Thinkers 50_ named him the world's 18th-best business thinker.

