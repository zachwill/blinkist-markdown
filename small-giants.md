---
id: 543e78833065650008660000
slug: small-giants-en
published_date: 2014-10-17T00:00:00.000+00:00
author: Bo Burlingham
title: Small Giants
subtitle: Companies That Choose to Be Great Instead of Big
main_color: E78B2E
text_color: 9C5D1F
---

# Small Giants

_Companies That Choose to Be Great Instead of Big_

**Bo Burlingham**

There's a new phenomenon emerging in the American business world: _Small Giants_. These are privately owned companies that don't follow the usual corporate dogma of growing revenues at any cost. Instead, they're driven by their heart-felt enthusiasm for their product, and focus on factors like quality and caring for their workforce. The author has examined 14 small giants to explain how this strategy has made them successful.

---
### 1. What’s in it for me? Discover how to run a successful business while remaining small. 

For as long as there have been businesses, there's been a basic premise that the only way to succeed is to expand, constantly looking for ways to sell more products to more customers.

But in these blinks, you'll discover that there's another way to make your business a success, and it depends on you chasing quality, not growth. This is what companies like _Anchor Brewing_ and _Zingerman's Deli_ are doing when they lovingly prepare their mouthwatering ales and sandwiches. The result? A resounding triumph for their bank balance and their quality control.

In these blinks, you'll discover:

  * why being a business owner means you should memorize the names of 1,700 people;

  * how the shoe company _ECCO_ helped one of its employees buy a house, and made her a shareholder; and

  * the secret to the _Mona Lisa_ 's fame.

### 2. Small giants: why some companies decide not to grow. 

If you've ever seen the CEOs of huge, publicly traded companies being interviewed, you'll know that they always emphasize the importance of growth. No doubt this is how the companies became so big in the first place.

But in fact, there's another kind of company: the _small giant_. These are privately owned companies that have faced a moment of truth where they had to decide whether to grow or not, and decided against expanding if it meant compromising their mission.

Think of _W. L. Butler Construction Inc._, which expanded rapidly in the 1970s and 1980s. At one stage, the owner, Bill Butler, found himself with 129 employees and $20 million in annual sales.

But he was unhappy. He realized that he didn't want his company to keep growing as he felt he should know every employee personally. So Butler narrowed his business focus and reduced his clients from 25 to ten. This even meant letting go of his largest client, an abusive financial services company that accounted for 50 percent of the value of the company's projects.

But staying small has some real business benefits.

Consider the Californian brewery _Anchor Brewing,_ which has remained at around 50 employees for the past 20 years.

The small workforce fosters better relationships as everyone knows everyone else, and it also helps the employees take pride and responsibility in their jobs as they can actually see the impact of their efforts immediately.

Not only do small companies have a nicer atmosphere at work, they also tend to be better at tasks that benefit from personal interaction, like customer service. This is because the owner can literally know the face and name of every customer, which would be impossible in a big corporation.

> _"Nothing in business forces anyone to pursue a particular growth strategy."_

### 3. Passion and control: owners of small giants keep control of their companies so they can do what they love. 

As we saw in the previous blink, one of the key concerns for the owner of a small giant is retaining control of the company.

This is why owners want to keep their company stock in the hands of just a few individuals. If they really wanted to grow, they would probably need to find outside investors to fund the expansion, and this would mean giving away shares and thereby some oversight of the company.

That's why only four of Burlingham's 14 small giants had "outside" shareholders who didn't work directly for the company.

Another trait of small giants is that typically the owners are incredibly passionate about what they do and devote their attention wholly to _quality_ instead of profit.

Look at _Anchor Brewing's_ philosophy: the company feels so strongly about beer that it chooses many traditional brewing methods over more cost-efficient modern ones. Copper kettles are used instead of stainless steel ones, and when it comes to fermentation, the beer is cooled using only the night air instead of ice. These methods are used because they result in better beer, which is more important than profits.

The American gourmet food business _Zingerman's Deli_ also found creative ways to keep their unique character. The founders declared that their vision was to sell "sandwiches so big you needed two hands to hold them and the dressing would roll down your forearms."

As the company grew, the founders decided that franchising would jeopardize the quality of the food sold under the brand, so instead they developed a network of businesses with their own distinct identities under the name _Zingerman's Community of Businesses_. This helped ensure the high quality of their beloved sandwiches.

> _"Many people don't lose their companies, but you almost always lose a signification portion of your independence when you sell stock to outsiders."_

### 4. Local roots: why small giants are deeply embedded in their communities. 

The representatives of almost all large companies, even huge multinationals, often claim to care deeply about the communities around them. But in the case of small giants, this claim is undoubtedly true.

This is because the local community plays a crucial role in shaping the company.

Consider a restaurant and all the ties it has to the surrounding community, from the customers it serves to the suppliers it buys its food from, and through those producers the restaurant's dishes are even influenced by local geographical factors like mineral levels in the soil, hours of sunshine and levels of rainfall. This is what the French would call the "terroir" of a place.

Unlike their larger competitors, small giants can consider all these aspects, and this helps them produce high quality products.

What's more, small giants actively contribute to their communities.

Consider the Buffalo-based music label _Righteous Babe Records_. When an old church in downtown Buffalo was about to be demolished, label-owner Ani DiFranco rescued it by financing emergency repairs. This act of kindness persuaded the city to let her set up her business _inside_ the church.

New York-based record storing company _CitiStorage_ is another great local philanthropist. The business had a program where employees could choose to "donate" part of their annual holiday as cash to a city charity, and the company's close ties to the community inspired many employees to do just that.

### 5. Caring instills loyalty: how small giants benefit from taking good care of their employees. 

In the previous blinks we described the owners of small giants, so let's turn our attention to the employee's experience.

Unlike most large corporations, small giants care for their staff and want to meet their needs.

Consider Michele Howard, a single mother of three with no education. She started working for the shoe brand _ECCO_ in a part-time role demanding manual labor, but as she gained experience, she was able to take on a fulltime role in customer service.

Eventually she was able to buy a house by borrowing money from _ECCO_ 's 401(k) pension account. What's more, she has since become an _ECCO_ shareholder by participating in the employee stock ownership plan that controls 58 percent of the company's shares.

Another example of how well small giants treat their employees can be seen at _O. C. Tanner Co_., a 1,700-employee human resources consultancy focusing on employee recognition. The late owner, Obert Tanner, was famous for wanting to engage in personal talk with as many employees as possible, and it is said he knew the names of every employee there!

Tanner also arranged for his 65 percent stake in the company's stock to be transferred to a one-hundred-year trust so that the company could not be sold, merged or taken public. This helped guarantee the staff their jobs in the long term.

Caring for employees like this would be unheard of in most large companies, but in small giants it instills a great sense of loyalty.

The benefits to small giants can be seen at _Artists Frame Service_ ( _AFS_ ), a Chicago-based picture framing company, where one manager was offered a $10,000 raise in annual salary to jump ship. Despite the tempting offer, he felt the competitor did not treat its employees as fairly as AFS and declined.

Stories like these are the reason small giants continue to care for their employees.

### 6. Real soul: why employees at small giants are passionate about what they’re doing. 

You often hear employees of huge corporations lamenting the fact that they feel like mere faceless cogs in a machine. This is not the case for people who work for small giants.

Why?

Simply put, small giants have _soul._ Their working atmosphere has been cultivated from early on and it has an elusive quality that is meaningful, motivating and exciting for employees.

They've achieved this by having employees regularly reflect on what they value about the atmosphere, and in particular the emotions it evokes in them.

For example, at American organic food company _Clif Bar_ the owner found that the employees could describe "the soul" of the company far better than him by explaining how it made them feel. One of them described it as, "You got that engine running, baby, and the sky's the limit!"

Another reason working for small giants is so rewarding is that the entire company is intrinsically motivated to do well at what it does, and not just to make as much money as possible. Since the owners originally set up the business to do something they cared about, they hired people who also cared about their product, whether it was beer, music or shoes.

The most important manifestation of this is the drive for outstanding quality. At _Zingerman's Deli_ everyone has a passion for great cooking that helps them understand and consider factors that many other companies might see as unimportant, like the smell of food. They are the company's best fans, putting themselves in the place of potential customers.

### 7. The benefits of staying put: why small giants are usually happy right where they are. 

Do you think the _Mona Lisa_ would have become the world's most famous painting if it had been hanging in some dingy truck stop in Nebraska instead of the _Louvre_ this whole time?

Probably not.

This is because the location and setting of something, whether it's a painting or a business, greatly impacts the experience of patrons who visit it.

This is why Danny Meyer, the owner of the famous New York-based restaurant chain _Union Square Hospitality Group_, turned down the opportunity to open a branch in Las Vegas. He knew the value of the setting to the dining experience, and felt that opening a café in Vegas would be like hanging the _Mona Lisa_ at a truck stop.

Such devotion to the setting and location of their business is typical of small giants, which is why they're often happy right where they are, with no particular desire to expand or move to new location.

But there are also pragmatic reasons for staying put: it's easier to deliver top-notch quality if you can develop a concentrated network of partners you trust, and this takes time.

When _Righteous Babe Records_ was founded, the cost of living in Buffalo was so low that the company didn't have to focus solely on generating profits, but could instead develop its own signature style over time. This helped it find a dedicated network of amateur musicians who wanted to record with them. In other cities with a more competitive music scene, this slow approach would not have been possible.

> _"You don't have to spend much time around [Righteous Babe] to notice how similar its spirit is to that of the city in which it resides."_

### 8. Final summary 

The key message in this book:

**Small giants are businesses that have deliberately chosen not to chase growth and profits, but instead have stayed true to their ideals. They're passionate about what they do and focused on doing it extremely well, and following this little-trodden path has made them very successful.**

**Suggested** **further** **reading:** ** _Built to Sell_** **by John Warrillow**

_Built to Sell_ details key strategies for growing a small service company and preparing the business for a future sale. These blinks illustrate these insights by telling the story of Alex Stapleton, owner of a marketing agency, and his advice-giving friend Ted Gordon, who is a successful entrepreneur.
---

### Bo Burlingham

Bo Burlingham is editor at large at _Inc._ magazine and has written for other publications such as _Mother Jones_, _Boston Magazine_ and _Esquire._ He's also been a board member for _Body Shop Inc._, and founded the global business network _PAC World_.

