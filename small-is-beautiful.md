---
id: 55f6d9f529161a0009000041
slug: small-is-beautiful-en
published_date: 2015-09-15T00:00:00.000+00:00
author: E. F. Schumacher
title: Small is Beautiful
subtitle: A Study of Economics as if People Mattered
main_color: 995638
text_color: 995638
---

# Small is Beautiful

_A Study of Economics as if People Mattered_

**E. F. Schumacher**

_Small is Beautiful_ (1973) is a collection of essays by renowned British economist E. F. Schumacher outlining his critique of the Western economic system. First published in 1973, this classic collection, which is now considered to be one of the most influential books published since World War II, is as relevant today as it was in the '70s.

---
### 1. What’s in it for me? Discover how economics would look if people were taken into account. 

In economic terms, is bigger really always better? Not according to the world-renowned economic thinker, E.F. Schumacher. On the contrary, he posited that "small is beautiful" and that economists should value life on earth over income and profit.

In these blinks, you'll be presented with his critique of our modern Western economic system, which, according to him, depletes our resources and sucks the meaning out of our lives. Arguing that our economic system legitimizes greed and values money above all else, Schumacher's essays are as relevant today as they ever were.

In these blinks, you'll also learn

  * why universal prosperity wouldn't lead to peace;

  * how growth can be destructive; and

  * that nuclear energy can spell ruination instead of salvation.

### 2. The modern economic system relies on depleting the Earth's natural resources. 

Modern economic systems have brought great prosperity to the Western world; however, that prosperity came with a big price tag. Our lives are now so removed from nature that we constantly destroy it without compunction.

For instance, thanks to the global industrial system, we squander a great deal of our precious natural resources — such as fossil fuels. The modern economy treats fossil fuels as _income_, a constant stream of goods, rather than _capital_, a finite supply of goods. To thus regard them is a way of justifying waste.

If we viewed fossil fuels as capital rather than income we'd be much more concerned with their conservation. However, we use them as if they'll never run out — which, of course, they will.

It's not possible for humans to manufacture or recycle fossil fuels, so once they're gone, they're gone. And running out of them too quickly would threaten the very foundation of our modern economy, which requires a steady energy supply.

The modern economic system itself also threatens two types of _natural_ capital: the _tolerance margins of nature_ and the _human substance_.

Since World War II, the world has seen a dramatic increase in industrial production, which threatens the tolerance margins of nature. Our actions are harming the environment at a rate much greater than its natural rate of regeneration.

The economic system also devalues humans, who are reduced to little more than cogs in the economic machine. For instance, most people in the world don't find their work fulfilling and many spend their lives doing back-breaking labor.

Such problems threaten the foundation of the modern economy, too, because they threaten society as a whole. The economic machine can't sustain itself if the people who run it can't live.

### 3. Universal prosperity alone can't ensure lasting peace. 

An examination of the history of economics and political stability reveals an interesting pattern: the rich tend to be more peaceful than the poor. So it would follow that economic equality and prosperity could provide the foundations for a peaceful world, right?

Let's consider this idea by examining three points:

  1. Universal prosperity is possible.

  2. We can attain universal prosperity through the materialist philosopher's dictum "enrich yourself."

  3. Doing so will lead us to peace. 

When we break down the idea of peace through universal prosperity into these three points, it becomes clear that the theory doesn't hold up.

At the moment, we can only achieve universal prosperity (the first point) by consuming more fossil fuels, which would lead to greater environmental damage. We have to question the idea of unlimited economic growth for two reasons: fossil fuels are in short supply and the environment can only withstand so much damage. If we keep consuming at our current rates, we'll face even greater pollution problems.

The second point is problematic when we consider that self-gain is driven primarily by greed and envy. If these human vices get built into our economic system, GDPs may rise for a time but people will feel burdened by increasingly acute feelings of frustration, alienation and insecurity. The global GDP will eventually stop rising because populations will be paralyzed by meaninglessness.

Finally, the third point: universal prosperity simply can't sustain peace. Universal prosperity, in the modern sense of endless economic growth, can only be attained through greed and envy. Those emotions destroy happiness and meaning, which is itself a threat to peace. Furthermore, we can't attain universal prosperity without continuing to damage the environment.

### 4. The prevailing economic mentality values most highly those actions that are economic, at the expense of people and the environment. 

There are a lot of common misconceptions about economic growth. Most people assume that it's always good if the GDP rises — but there _is_ such a thing as unhealthy growth.

Those who think that economic growth is always good would have it that anything _uneconomic_ is negative. Something is uneconomic if it doesn't generate a profit — and unprofitability is generally regarded as bad.

On the other hand, anything profitable is held to be good. This kind of thinking is flawed, however, because when we only value economic activities in terms of the profits they yield, we don't factor in the human cost or their damage to the environment. And, in turn, uneconomic actions that _do_ help people in need or protect the environment might be disregarded.

It would be uneconomic for a seller to reduce their prices for poorer customers, for example. It would also be uneconomic for a person to buy goods produced locally if imported goods are cheaper.

In other words, the prevailing economic thinking prizes money above all else. An economically thinking buyer is always on the hunt for a bargain. He doesn't care about the conditions under which his goods were produced — or their harm to the environment. His sole concern is getting the biggest bang for his buck.

If a buyer turns down a bargain because the practices behind it are exploitative or destructive to the planet, she risks being criticized for being uneconomical. So we're collectively suppressing vital actions that we need to encourage in order to help both ourselves and our earth. Economic thinking puts a price tag on things priceless, like our humanity and our home.

### 5. Education is the greatest of all resources – but only if it instills the right values. 

History tells us that education is a key factor in economic development, that it's much more important than having access to natural resources. Furthermore, the belief in the power of education is deeply embedded in modern societies.

Today, a lot of people assume that societal progress relies on education. That's partly true, depending on what we teach. Education shouldn't just be about preparing young people to enter the job market — we have to instill in them the right values, too.

Modern societies are complex environments to survive in, and we often assume that superior higher education is always better for any given person. We also expect education to lead us toward scientific and technological progress. Most people feel like they should understand basic scientific concepts like electrons, and know how to operate the latest technology.

Scientific knowledge isn't inherently useful, however. What's important is learning how to harness our scientific knowledge to build a better society for ourselves. And that's where education comes in.

Education ought to teach children values they can use to lead a meaningful life. The natural sciences don't provide them with any moral guidance; even the greatest scientific breakthroughs usually only have specific applications — they certainly don't tell us how to live our lives.

We know _how_ to do a lot of things, thanks to scientific progress, but we don't know _what_ to do. Sometimes we think studying the humanities helps, but humanistic education can't save us if it doesn't teach metaphysics or ethics, for it is these disciplines that address the most powerful ideas and transcend the realm of mere facts.

### 6. Nuclear energy could be mankind's most dangerous invention, but it's only debated in economic terms. 

Fossil fuels are a limited resource, so we're soon going to need an alternative energy source. Many people felt that nuclear energy was the answer when it was first developed, especially because it seemed to have arrived just in time. Few understood, however, what nuclear energy could really do.

Nuclear radiation has become the greatest threat to life on earth, and the effects of nuclear radiation are well known. Radiation particles are like tiny bullets that tear into an organism; the damage they do depends on their nuclear dosage and the types of cells they hit.

The atom bomb raised general awareness of dangers of nuclear radiation. However, "peaceful" uses of atomic energy may prove to be an even greater danger to humanity.

We know how to produce radioactive elements but there's nothing we can do to reduce radioactivity once it has been created. Only the passage of time can reduce its intensity — chemical reactions and physical interferences are useless.

So far, we haven't found any place on earth where radioactive waste products can be stored safely, without having an impact on the surrounding life. Even small accidents can cause major catastrophes.

Despite all the downsides, we continue to use nuclear energy, simply because it's economic for us to do so. This is one of the clearest examples of how money determines everything we do. The decision of whether to build nuclear power stations or traditional power stations run on fossil fuels is purely an economic one — made without regard to the social consequences.

If nuclear energy doesn't end up saving us, it could easily destroy us.

> _"Radioactive pollution is an evil of an incomparably greater 'dimension' than anything mankind has known before."_

### 7. Modern technology has deprived humans of the work we find most satisfying. 

We tend to assume that further technological advances will always make our lives better. But modern technology has caused a number of societal crises, forcing us to call the value of technological progress into question.

In many ways, technology has deprived humans of the joys of work. After all, the purpose of technology is to make our lives easier. We've developed tools to help us survive, ostensibly so we can focus on other, more enjoyable tasks, but a lot of our technology has taken away the work we enjoy the most, such as crafting.

Modern technology has nearly eliminated the need for creative handiwork, like weaving, pottery and metalworking. These art forms have become increasingly rare in technologically advanced societies and it's nearly impossible to make a living off them now.

Instead, modern technology has created the demand for new kinds of jobs that almost no one enjoys, such as assembly-line work in large factories.

Another important feature of technology to be kept in mind is that it doesn't solve our most pressing problems.

There are a number of serious crises facing the modern world. We've looked at how our consumption of fossil fuels creates pollution, and the supposed answer to that problem — nuclear energy — may turn out to be even _more_ harmful.

Moreover, there's no reason to believe that technology will eliminate poverty or unemployment. As we've seen, it often robs us of the work we enjoy the most. That's the problem Karl Marx was referring to when he said, "The production of too many useful things results in too many useless people."

### 8. Developmental aid shouldn't consist of money alone, and it needs to be given primarily to rural areas. 

Throughout the world, the poor are getting poorer and the rich are getting richer. That's part of the reason that wealthier countries send developmental aid to poor countries. True economic development, however, is a major challenge.

Why?

Because you can't help a society grow just by infusing it with more money or material goods. In fact, material problems, such as a lack of infrastructure or natural resources, are the _secondary_ causes of poverty.

The primary causes of poverty are immaterial. An insufficient education system, governmental structure or legal system are much more detrimental to a society. These factors are crucial for economic prosperity, as evidenced by the fact that many wealthy countries, such as Israel, don't have any natural resources.

History also shows us that countries with a high level of education and societal organization tend to recover very quickly after war, as the foundations for economic growth remain in place.

But there's another central problem of development: it can't be done quickly. Change has to evolve over time — it's not possible to rush it.

Aid and development should also be focused on rural and small-town areas. Most developing countries have a _dual economy_, meaning there's a big divide between people in rural and urban areas. A typical developing country might have 15 percent of its population in its one or two big cities, while the other 85 percent live in small towns.

Most development aid goes to the cities, however. So 85 percent of the population might not benefit at all. Developmental aid would also be more effective if it was sent primarily to people outside the modern sector of the country. This would help counter the problem of mass migration to the cities, too.

> _"Development does not start with goods; it starts with people and their education, organisation, and discipline."_

### 9. Large-scale organizations have to foster loyalty and find a healthy balance between order and creative freedom. 

In 1999, Exxon and Mobil merged to create the world's largest company, Exxon Mobil _._ Since then, the business world has seen a trend toward more huge companies of this scale. The deeper these mega-corporations penetrate into our society, the more individual people are devalued and reduced to pawns.

All organizations need some degree of structure, whether they're big or small, but they also need to be unstructured enough to allow room for creative freedom. If an organization falls into disorder, it won't accomplish any of its goals; if the rules are too strict, however, it will also suffer.

Freedom in the workplace is vital because without it the team members lose their motivation for creative thinking. They'll become so frustrated by their work that they won't have the energy to develop new ideas.

When talented people are given room for creativity, though, the possibilities are endless. Creativity leads to innovation, something our society depends on.

That's why large organizations should be made up of several smaller, semi-autonomous groups. Each needs the freedom to grow and innovate on its own.

Large-scale organizations also need to foster loyalty and motivation among their employees.

Loyalty is a key element in any effective organization, and the best way to earn it is to trust the people working at lower levels. Those in higher positions shouldn't take over low-level responsibilities; being higher up in an organization doesn't automatically make you wiser or more effective at the job.

Motivation is also a central part of healthy, large-scale organizations. The upper management rarely struggles with motivation, but motivation tends to decrease as you move down the organization's hierarchy.

Upper management teams tend to assume that people work only for the money. Up to a point, this is true, but if employees aren't challenged, they won't find any meaning or satisfaction in their work.

> _"The fundamental task is to achieve smallness within large organisation."_

### 10. Final summary 

The key message in this book:

**The modern Western economic system is harmful to humans and our planet. It's unsustainable because it relies on natural resources that are rapidly running out, and it values money over people. We can start solving this problem by reforming our education system, helping poorer areas develop and restructuring our organizations. Our current economic thinking simply has to go.**

Actionable advice:

**Think outside the economic box.**

Stop valuing every action in terms of how "economic" it is. After all, life is about much more than money.

**Suggested** **further** **reading:** ** _How Much is Enough?_** **by Robert Skidelsky and Edward Skidelsky**

_How_ _Much_ _is_ _Enough_ sets out to critically inform the reader about the moral, historical and economical backgrounds of modern day capitalism, its obsession with accumulating more and more, and its consequences. It also vigorously outlines an ethical alternative to this lifestyle, in which our obsession with "more" is replaced with "the good life."

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

Cover Image: flickr.com/photos/eflon
---

### E. F. Schumacher

Ernst Friedrich Schumacher, the protégé of John Maynard Keynes, was a famed economist. From 1950 to 1970, he served as the Chief Economic Advisor to the UK National Coal Board.

