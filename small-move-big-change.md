---
id: 5406e9ec3933310008a40000
slug: small-move-big-change-en
published_date: 2014-09-02T00:00:00.000+00:00
author: Caroline L. Arnold
title: Small Move, Big Change
subtitle: Using Microresolutions to Transform Your Life Permanently
main_color: EED731
text_color: 6E6317
---

# Small Move, Big Change

_Using Microresolutions to Transform Your Life Permanently_

**Caroline L. Arnold**

In _Small_ _Move,_ _Big_ _Change,_ you'll learn why it is we so often fail to follow through with the changes in our lives that we so desperately want to make. Author Caroline Arnold explains that we fall short because we're not crafting our resolutions in a way that is actually achievable. Instead, we should focus on "microresolutions" — small, easy-to-keep commitments that add up to big change.

---
### 1. What’s in it for me? Learn how to stick to your resolutions and change your life for good. 

We've all been there. You wake up with a splitting headache after a big night on the town and say, "I'm never drinking again." Or maybe you've stepped on a scale for the first time in months, only to see a number so horrifying that you resolve to finally "get in shape."

But how many times have you actually followed through? Like so many of us, you'd probably be embarrassed to even say. In fact, most of us don't stick to our resolutions. As a result, we end up keeping bad habits that we know we want and need to change.

These blinks will show you exactly _why_ we have such a hard time keeping promises to ourselves and _what_ we can do to change it. And not only that, you'll learn how making a change, if you can formulate your goals in a way that actually makes sense, is in fact quite easy.

In these blinks, you'll learn:

  * why most of our resolutions are really just "wannabe" resolutions;

  * how to use your love for food as a way to lose weight; and

  * how to keep yourself from "falling off the wagon" and sticking to your promises.

### 2. It’s hard to stick to resolutions you’ve made, as often they are too broad or idealistic. 

Alongside sipping Champagne and counting down the seconds until midnight, one of the highlights of New Year's Eve is sharing your resolutions with trusted friends, swearing to lose 10 pounds or quit smoking. You always say that this year — unlike last year — you'll do it.

Yet inevitably you fail, year after year. In fact, some estimates say that people fail in keeping some 88 percent of the resolutions they make. But why is this the case?

In part, you fail because your resolutions are simply too overreaching.

On New Year's Eve, for example, it's easy to feel guilty about having spent the past holiday week lazing around and overeating. To make up for the gluttony, you vow to make huge, sweeping changes.

However, these changes are usually too broad, and therefore too easy to let slip.

Say one of your promises was to "get in shape and go to the gym." This resolution is too broad, and leaves you with too many unanswered questions. _When_ do you plan to go? _How_ will you determine when you're sufficiently "in shape?"

Broad, imprecise resolutions such as these will most likely lead you to slacking altogether, with excuses like: "I'll skip the gym today and go tomorrow instead."

Sometimes resolutions fail because you want things to happen without having a concrete idea of how to achieve them. These are _wannabe_ _resolutions_, and are little more than commandments to make you a better person, rather than an actual change.

For example, if you've ever told yourself that you "want to be more organized," then you've uttered a wannabe resolution. Sure, you may want this, but _how_ do you achieve it?

There's nothing built into your resolution that offers you a place to start — and if starting is difficult, then it's unlikely that you'll continue and reach your goal.

> _"We begin with enthusiasm and determination, yet our will falters and resolutions fizzle."_

### 3. When your brain goes on autopilot, you fall prey to bad habits. 

When you fail to keep a resolution, it's easy to berate yourself for having poor self-control. However, this is far too simplistic. The real reason why we fail is that our habits and routines — the things we do without thinking — govern much of our behavior.

In short: we spend a lot of our time on _autopilot_.

When you drive to work, it is your autopilot mode that makes you to stop at red lights without thinking, "I see a red light, therefore, I need to brake."

Running on autopilot helps us to save mental energy for the more important work that requires your undivided attention.

Yet it is also responsible for our bad habits. Autopilot also makes us visit the vending machine at work to buy a pack of salty pork rinds without thinking whether we're even hungry.

Our autopilot habits are very powerful and very resistant to change. Consequently, this makes the bad habits hard to break.

In fact, the _only_ way to end a bad habit is to cultivate the opposite of autopilot — being fully aware of your actions, or _mindfulness_. In contrast to autopilot, mindfulness requires a great deal of mental energy and willpower.

But willpower is a scarce resource. In trying to break bad habits, you can easily exhaust your willpower and then relapse into the old routines you so wanted to change.

Only in very rare situations, such as after receiving bad health news or during a messy breakup, can you develop enough willpower and energy to make a fundamental change in your behavior. Absent serious environmental factors, though, you may have to resort to other instruments.

To break the cycle and take steps toward self-improvement, declare war on your autopilot. The following blinks will give you the best weapons to do just that.

### 4. You can make big life changes by creating small behavioral changes called “microresolutions.” 

So what should you do to change your bad habits? Make _microresolutions._

Microresolutions take your broad and unmanageable resolutions, chop them up and turn them into precise and practical plans that you can quickly achieve.

Rather than forcing you to tackle a large problem all at once, microresolutions instead aim to make one or two small, tangible changes in your life or daily habits.

Microresolutions are always explicit; they are concrete orders that you can follow. Consequently, your microresolution must always focus on a specific _change_ in behavior, and not a _result_ that can be achieved in a number of different ways.

For example, if you want to live healthier and lose weight, your resolution might be to "just eat less." Yet this goal is unspecific and hard to put into a concrete plan of action.

Let's transform it into a microresolution!

Say you have a bad habit of snacking on too many cookies at your daily planning meeting at work. This is a great place to make an easy change! By forcing yourself to not grab a cookie, you'll cut some 350 calories a day from your diet.

Plus, unlike the broad goal of "eating less," this step is easy to measure _and_ to achieve.

And how easy is it to say "no" to one cookie per day? The idea behind microresolutions is that they are _so_ easy and achievable that it would be absurd not to keep them.

With your cookie resolution, there are no loopholes or possible excuses. You just don't eat the cookie.

What's more, your microresolution gives you instant gratification — you can see the results immediately, not in the distant future.

> _"Microresolutions focus on doing, not being."_

### 5. Although small, microresolutions do lead to fundamental and long-lasting changes. 

Although you can easily keep a microresolution, it may take some time for your brain to permanently adjust to following new habits.

Early on, it's not unusual to feel confused or annoyed as you struggle to keep your new habits.

After a few weeks, however, your resistance to these changes will start to dwindle. You'll see new, healthier habits forming as your changes becomes incorporated into your autopilot. And once it's there, it will stick.

Say you plan to walk to work instead of taking the bus to become more fit. During the first days and weeks, you'll probably struggle — the walk is long and boring, and it takes too long!

However, with time you'll eventually just do it without a second thought. The annoyance will fade and the benefits will be evident, as your health will gradually improve.

As you start to make positive behavioral changes, you might be tempted to overload with more and more microresolutions. Don't do it! Ease into the process and give yourself a chance to adapt.

Consider your microresolution to walk to work. Perhaps it's not best to immediately start walking to work _every_ _day_, since that's a huge commitment. You could instead start by walking every Monday. That's a far more achievable first step, and offers you room to improve.

Resist the impulse to implement hundreds of new microresolutions, and instead limit yourself to one or two at a time. By doing so, you ensure that you have the concentration and endurance to undergo a complete behavioral shift.

Only once you've made your new habits part of your autopilot can you start to build on top of them with more microresolutions.

> _"The key to lasting transformation is not speed or force, but nurture."_

### 6. Microresolutions should be framed in a way that ensures you will keep them. 

You are unique, with your own desires and needs. Consequently, there is no universal master list of microresolutions from which you can choose to change your behavior for the better.

Microresolutions are also unique. They correspond to your individual needs and to your mind-set, and should be framed in a way that makes it easy for _you_ to follow them.

However, there are some rules that you should follow.

When you are crafting a microresolution, use _positive_ _framing._ That is, create a resolution that is positive, and not merely a command. This way, you'll be motivated to actually follow through.

Perhaps you have a bad habit of eating far too quickly. Having finished your meal before everyone else, you're often tempted to go back for seconds, causing you to put on extra pounds.

To break this habit, you might consider a microresolution like: "Chew your food slowly." This command isn't very exciting or motivating, and therefore it will be harder to stick to.

But you could easily come up with a more positive microresolution. How about: "I will savor my food and drink." Here, you are giving yourself permission to enjoy your meal, a pleasurable activity. This way, it's much easier to follow through on your microresolution.

However, while positive framing is ideal, you might have to take an opposite approach and use _zero-tolerance_ _framing_. This is best for resisting specific traps that lead to bad habits.

For example, if you "quickly" check your email at 11 p.m. on the way to bed, but once hypnotized by the screen, stay glued to your computer until 1 a.m., you'll suffer from a lack of sleep and wasted time.

Here, a zero-tolerance resolution is needed to eliminate the bad habit that _causes_ sleeplessness. For example: "Zero tolerance for using the computer after 10 p.m."

### 7. Microresolutions work best when they are linked to recognizable cues. 

Have you heard of Pavlov's dogs? Russian scientist Ivan Pavlov conducted an experiment in which he rang a bell whenever it was time for his dogs to eat. Gradually, the dogs connected the bell with meal times, so when the bell rang, they began to salivate — even if there was no food in front of them.

We are programmed in much the same way — hearing, smelling or seeing certain cues can cause our bodies to react in certain ways. Often, this can lead to unhealthy habits.

After a hard workout, for example, you pass by a vending machine at the gym. You're hungry, so you buy a snack. The next day, you do the same. And again the next.

Soon, the mere _sight_ of the vending machine will prompt you to buy a snack, even if you aren't actually hungry.

While these impulses can clearly create bad habits, you can also use them to make following your microresolutions easier. To do this, you'll need to find a cue that you can use as an anchor for your microresolution.

One useful cue is using a specific time. For example, if you want to walk to work in the mornings, you can anchor it to specific day, such as: "Every Friday morning I will walk to work." Your microresolution will be triggered after you've had your breakfast, look at your calendar and notice that it's Friday!

It can be challenging, however, to find the right cue. In this case, you can _piggyback_ your microresolution onto another, more established habit.

If you've resolved to floss, but keep forgetting to do it, you can piggyback flossing with your evening habit of brushing your teeth. Then, you can't help but remember to floss whenever you brush.

So now you know the ground rules for microresolutions. In the following blinks, we'll see how microresolutions work in action.

> _"We are what we repeatedly do. Excellence, then, is not an act but a habit."_

### 8. Microresolutions can help you change some habits important for health, such as your sleep. 

We all need to sleep, yet we sleep far too little. Even though science has shown that sleep deprivation wears us down and fogs our thinking, some 75 percent of American adults still sleep less than six hours per night.

Most of us desperately need to find ways to get more sleep. Luckily, microresolutions can help!

You can start by examining the habits that cause you to stay up late. Often, we consider the time after work and chores as our "time off." Because of this, you might not feel like going to bed immediately, even when your body feels like cement and you know full well that tomorrow will be exhausting.

This practice can result in bad habits. For example, after a stressful day, all you might want to do is relax and zone out by watching TV. Instead, you fall asleep on the couch; but then get up again later to brush your teeth, floss, charge your phone and so on.

After finishing your nighttime routine, you finally make it to bed only to lie awake, no longer sleepy.

After examining your routines that keep you from getting a good night's sleep, you can then design a suitable and easy-to-keep microresolution.

If you know that watching TV will make you sleepy, you can use this information to piggyback a microresolution on to another cue or habit.

You could, for example, make sure you get ready for bed as soon as you've put your child to bed. In this way, if you get sleepy on the couch, you can go to bed immediately, having already completed your nighttime routine.

With enough repetition, this resolution will become part of your autopilot and you'll do it without a second thought — and improve your sleeping habits for good.

### 9. Shake off your sedentary lifestyle by using microresolutions to improve your fitness. 

Who hasn't made a resolution to "get fit" and then almost immediately broken it? Getting in shape is one of the most difficult resolutions to keep.

Our hunting-and-gathering ancestors never had to worry about slimming down. But our modern lives are far less active: we sit at a desk all day, slouch on the couch all evening, then lay in bed all night.

Scientific studies have shown just how unhealthy this sedentary lifestyle is. Sitting all day doesn't burn calories so we gain weight; and it's been found that sitting all day actually shortens our life span.

The good news, however, is that it doesn't take much to lower the risks of inactivity. All we have to do is make tiny changes to become more active and lower the risks to our health.

Indeed, even small amounts of activity can make a difference in terms of overall health and body weight, as moving around just a bit can help you maintain healthy blood sugar and insulin levels, critical to preventing diabetes and heart disease.

So, you don't have to rush to the gym or run a 5K every day to extend your life span. That's a good thing too, since we know that these all-or-nothing resolutions rarely work out.

Knowing that even small changes can do wonders for your body and mind, microresolutions seem like the perfect way to improve your health, no?

For example, you could resolve to "climb six flights of stairs every workday after lunch." By anchoring your microresolution to the moment you get up from the lunch table, you have an easy way to start building a positive habit. Once established, you will always remember to take the stairs after lunch, thus sticking to your resolution and leading a healthier life.

As you can see, small changes can make a huge difference. As you make your microresolutions, you will become inspired to build upon them, leading to a steady and satisfying path to self-improvement.

### 10. Final summary 

The key message in this book:

**The** **big** **changes** **we** **want** **to** **make** **in** **our** **lives** **often** **fail** **precisely** **because** **they** **are** **so** **big.** **Instead,** **we** **should** **aim** **to** **make** **small,** **measurable** **changes** **–** **microresolutions** **–** **to** **put** **ourselves** **on** **the** **path** **to** **improvement.**

**Suggested** **further** **reading:** **_Switch_** **by** **Chip** **and** **Dan** **Heath**

_Switch_ examines why it is difficult for people to switch their behaviors, and how, by understanding our minds, it is possible to find shortcuts that make change easier. Through examples from scientific studies and anecdotes, _Switch_ provides simple yet effective tools for implementing change.
---

### Caroline L. Arnold

Caroline L. Arnold is an executive director at a leading Wall Street investment bank and is considered one of the technology leaders on Wall Street. She is also a recipient of the Wall Street & Technology Award for Innovation.

