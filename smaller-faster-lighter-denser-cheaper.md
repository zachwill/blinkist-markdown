---
id: 57e8e6fa4af49600036fe3f2
slug: smaller-faster-lighter-denser-cheaper-en
published_date: 2016-09-30T00:00:00.000+00:00
author: Robert Bryce
title: Smaller Faster Lighter Denser Cheaper
subtitle: How Innovation Keeps Proving the Catastrophists Wrong
main_color: FEAA33
text_color: 805519
---

# Smaller Faster Lighter Denser Cheaper

_How Innovation Keeps Proving the Catastrophists Wrong_

**Robert Bryce**

_Smaller Faster Lighter Denser Cheaper_ (2014) explains the psychology behind our anxiety and pessimism regarding climate change. Rather than giving up and reverting to a medieval way of life, these blinks explain how innovation and new technologies will help humankind survive and continue moving forward.

---
### 1. What’s in it for me? Discover why it’s crucial to push innovation forward. 

Since the start of the twentieth century, the world has witnessed a dizzying array of technological and industrial innovations that have made just about everyone's life much easier.

However, these innovations have always come at a serious cost for our planet. We've polluted the air, contaminated our lakes and rivers, hunted animals to extinction and destroyed habitats and ecosystems. Upon realizing this, many people want to turn back — they want to reverse the innovations and revert to a bygone way of life.

But if we hope to deal with the catastrophic consequences that we, and our planet, struggle with today, this is not the answer. These blinks will explain why, and will tell you exactly how we can emerge even stronger from this ongoing crisis that threatens our very existence.

In these blinks, you'll discover

  * if you, too, suffer from collapse anxiety;

  * why it's both unrealistic and harmful to revert to a simpler way of life; and

  * how the need for speed relates to your heart rate.

### 2. Global problems give us collapse anxiety, but innovation can help us find the right solutions. 

At present, humanity is facing many complex problems, such as climate change, terrorism, bloody civil conflicts and the threat of nuclear war. But is it too late to combat these problems and move forward? This is one of the predominant questions of our age. While many people seem ready to give up and roll over, this is not the way forward if we want to combat these problems effectively.

These days, we are being constantly bombarded with bad news from around the world. Often, this news is either about environmental disasters, such as earthquakes and tsunamis, or political crises, such as war and terrorism. Either way, our worldview is continuously shaped by unpleasant information.

The bombardment of bad news we all face causes many people to suffer from _collapse anxiety_. Basically, collapse anxiety is a fear that the world as we know is on the brink of destruction, and that there's nothing we can do to stop it.

It can be tempting to give up and resign ourselves to the idea that the world is going to, well, collapse. But this stops us from fighting to improve our lives and those of the people around us.

Technological innovations have been a constant source of dramatic improvements in the everyday lives of people around the globe. Thanks to these innovations, people live longer and healthier lives than ever before. Advances in medicinal science have enabled us to overcome formerly life-threatening diseases such as smallpox and polio.

Innovation has also given rise to the internet, a tool that gives us access to countless websites, many of which are packed with useful information. The internet allows millions of people to pursue life-enriching education that would otherwise be unavailable to them.

Clearly, innovation has been the source of many great achievements that have improved the lives of human beings everywhere. Next, we'll explore some of the innovations that humans have come up with in the last millennia, and how such innovations could save us and our future.

### 3. Humans have always sought improvement, even if innovation has its drawbacks. 

History is filled with examples of the genius of the human mind. Anywhere you look, people have found ways to improve their lives, along with the lives of others. These improvements have taken the shape of innovations that have been used to make things better, faster and more accessible to everybody.

Consider the invention of the printing press, which allowed books to be produced much more quickly and cheaply than ever before. Thanks to this innovation, common people suddenly gained access to a huge range of information. Before this, of course, the multitude of people who couldn't afford books simply had to rely on word-of-mouth information from those that could.

Just think of the airplane. The first functioning example was invented by the Wright brothers. In 1903, Wilbur and Orville managed to fly their plane for 12 seconds, which was an astonishing feat for the time. Obviously, a century of innovation has improved this invention in ways that would have been completely unimaginable over a century ago. We can now travel anywhere in the entire world in a matter of hours!

There is, of course, a downside to all this positive innovation. The innovative technology that we produce also has the potential to be misused, and can often end up posing a danger to people's lives. The most obvious example of this is the invention of nuclear weapons. Before we invented them, there was no way one nation could inflict so much damage on another so immediately.

Does this mean that we should stop innovating? Well, it's true that some innovations have had negative consequences, with perhaps the most current and relevant instance being climate change. But reverting to more primitive forms of life would have far worse consequences than continuing to innovate.

In the next blink, we'll look at some of those possible consequences.

> Global grain yields experienced an average increase of 1.9 percent per year in the years between 1950 and 2010.

### 4. Global leaders are pushing for a new age of de-growth, but this isn’t the solution. 

So, when it comes to climate change, most would agree that the rate at which we're burning fossil fuels is far too high. But what can be done about this? Some people mistakenly suggest a strategy of _de-growth_. These people think that we should stop innovating and instead revert to our pretechnological, environmentally-friendly roots.

This idea of _de-growth_ is glamorized by a great number of political and opinion leaders around the world — from the founder of Greenpeace, Rex Weyler, to Bill McKibben and Naomi Klein. These public figures believe that the best thing for humans to do is go back to small-scale, community living. This would entail giving up access to imported products from around the world, and replacing them with local goods while reducing energy consumption.

This _de-growth_ model is attractive, but it's not the right way to go. Sure, _de-growthing_ would, in some way, prevent damage to areas of the globe that have been polluted by human activity. But what are the other potential consequences?

For starters, millions of people would lose their jobs, causing them to fall into poverty. Furthermore, without innovation, we would not be able to reverse the damage that climate change has already done.

So, what other option do we have? Instead of degrowthing, we can continue to innovate, creating technologies that save on energy and reduce waste. This way, we will be able to keep — and perhaps even _raise_ — our standard of living, thanks to our ability to improve our lives through technology.

### 5. Making tools faster can drive the creation of new, improved and cost-effective technology. 

Humans are always looking for ways to improve the speed at which things function. Just like Olympic sprinters, we constantly seek to push our limits and go faster. It's not clear why this is, but it's likely down to the fact that we all know that we have a limited amount of time on earth.

We can see our innate need for speed at work in our desires to build faster cars and planes — not to mention spacecraft! This desire can even explain the invention of the wheel, which enabled us to move objects, and ourselves, much more quickly.

Furthermore, the need for speed is so innate that we go crazy if something is going too slowly. Think about times you might have been in a long line at the supermarket, or waiting for a website or TV show to load.

Sometimes, our desire for speed can seem unhealthy, but it's been responsible for many great improvements to our day-to-day lives. Thanks to improvements in medical technology, we can measure someone's heart rate and blood pressure in a matter of seconds.

And it's not only our health that has benefited; just look at the smartphones we carry around with us. These devices would have been a miracle to people living just 50 years ago, who had to wait for letters or other information to arrive by post.

The push for fast technology has also made this very technology increasingly cheaper. Consider how just decades ago, very few people owned a computer with internet access. Fast-forward to the present day, where over two billion people are using the internet regularly. Our need for speed led us to build the most efficient, and therefore cheapest, methods of production. Now, even people in the world's poorest countries can benefit from technology.

### 6. Green energy isn’t the solution we wish it were. 

Chances are, you've heard people spouting the idea that green energy is good, while all fossil fuels are bad. This stance is usually expressed by people with an agenda, specifically those who are seeking financial support for their new ways to produce clean, green energy from sources like wind, solar or water.

The idea of "green energy" has become very popular in recent times. Renowned public figures, such as Al Gore, and large organizations, such as Greenpeace, all claim that wind, water and solar energy are the only way forward in terms of providing energy for the future.

Even US President Barack Obama has called for an end to the "tyranny of oil," which plays a big part in both the global economy and the world's energy needs.

However, there is scientific evidence that the energy density of green energy is simply too low to replace the energy sources that we are used to. To take just one example, wind power produces only 1-1.2 watts per square meter. Meanwhile, your average nuclear power plant produces 50 watts per square meter! An average oil plant lands somewhere in between, at 27 watts per square meter.

So, reverting to green energy would clearly be a good idea if we needed _less_ energy. But our energy needs are rising every day. On top of this, the world's population is growing, and a vast number of people around the globe are suffering from hunger and living in poverty. If we switched to green energy tomorrow, these people would be the hardest hit by the resulting energy shortages.

Unfortunately, Western leaders still continue to propose large-scale investment in green energy. It would instead be wiser to promote innovative methods of making it cleaner and safer to use fossil fuels. This way, we could curb the effects of climate change without doing damage to those most in need.

### 7. Like it or not, the future of energy is gas, oil and coal. 

So what about fossil fuels, then? These days, lots of people are of the opinion that fossil fuels like oil, gas and coal are the energy sources of the past. But this is simply not true! If anything, fossil fuels are the energy sources of the future, as they are the only sources that can sustain our current way of life.

For technological innovation to occur as it should, we are going to need more and more energy. This is also true for the world's increasing population: the more people there are, the more energy we need to provide to them.

The fact of the matter is that only fossil fuels have a high enough energy density to provide sufficient energy for our rapidly advancing world. To get enough energy from green resources, we would have to set aside massive swaths of land to gather, for example, solar or wind energy; meanwhile, the technologies needed to extract and refine fossil fuels have been continuously improved and modernized.

If we really want to curb our reliance on fossil fuels, we should consider a return to nuclear energy. After numerous nuclear-related tragedies, countries have gradually moved away from nuclear energy.

Germany, in particular, has greatly decreased its reliance on nuclear power. However, Germany is a country with massive energy needs. So did they go green once they closed their nuclear power plants? Not quite; rather, they increased their reliance on coal instead.

While there are definitely downsides, it's clear that fossil fuels offer the only hope of meeting our ever-growing energy needs. For instance, in a country like India, there are still millions of people living without access to electricity. We could not possibly hope to meet the basic needs of these people with green energy, as it is too expensive and inefficient.

### 8. Final summary 

The key message in this book:

**Even though catastrophists are convinced that the world as we know it is ending, we must still take steps to prove to ourselves and others that change is possible. By rejecting notions of de-growth and continually pushing for innovation in technology, we're much more likely to find the solutions we need to current global challenges, such as climate change.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Moral Case for Fossil Fuels_** **by Alex Epstein**

In _The Moral Case for Fossil Fuels_, author Alex Epstein explores the benefits of fossil fuels such as coal, natural gas and oil. The book outlines the dramatic and positive effects using such fuels has had on society and examines the many myths associated with fossil fuels. Importantly, using fossil fuels is a moral decision in that their benefits to humanity outweigh any of today's environmental concerns.
---

### Robert Bryce

Robert Bryce is an American author and journalist. He has written for the _New York Times,_ the _Wall Street Journal_ and the _Washington Post_ on the energy industry and climate change. Bryce is also the author of _Power Hungry_ and _Gusher of Lies_.

