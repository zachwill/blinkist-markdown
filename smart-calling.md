---
id: 5799d754355cc30003d1fed1
slug: smart-calling-en
published_date: 2016-08-01T00:00:00.000+00:00
author: Art Sobczak
title: Smart Calling
subtitle: Eliminate the Fear, Failure, and Rejection from Cold Calling
main_color: FBBF43
text_color: 7A5D21
---

# Smart Calling

_Eliminate the Fear, Failure, and Rejection from Cold Calling_

**Art Sobczak**

_Smart Calling_ (2010) is all about the art of cold calling, an important business practice that even seasoned salespeople dread. Many of us tend to associate cold calling with call center cubicles, boredom or manipulative strategies — but it doesn't have to be this way. These blinks explain how you can overcome the challenges of cold calling to create a more pleasant and fulfilling experience for both you and your prospective customer, funder or employer.

---
### 1. What’s in it for me? Turn your cold calls into smart calls. 

You've most likely heard the term _cold calling_ before; it's when you call someone out of the blue to, for instance, sell something or get hired for a job. Many of us let out a sigh whenever we think of cold calling; we've had too many bad experiences, hang-ups and rejections to ever want to try it again.

However, there are ways to turn cold calls into warm ones, and that's exactly what these blinks are about. They offer a practical guide to how to approach your cold calls and prospects, and how to execute a cold call to perfection.

In these blinks, you'll also discover

  * how to grab your prospects' attention in some unconventional ways;

  * why you don't always have to speak with the CEO; and

  * which digital research tool you might want to deploy.

### 2. Master cold calling and make your calls smart by planning well and avoiding common mistakes. 

_Cold calling_ tends to have a negative reputation: it calls to mind useless offers, robotic conversations or manipulative sales reps. But there's really no reason for it to be so painful — cold calling is easier than it seems. In fact, you might be making some of the classic cold calling mistakes without even knowing it!

After all, cold calling is about more than just talking on the phone. Let's go over some common errors people make when conducting cold calls.

First off, be sure to address people by their proper names, avoiding any nicknames. And don't launch straight into asking the person on the line for something; instead, make them curious about what you have to say by bringing up an interesting idea.

Don't focus on _you_ at the beginning of the conversation. Rather, tell the other person how you might be able to help them out with something. And be specific! They'll lose interest if you're too vague.

A bad call, for example, sounds like this: "Hello Sander, this is George Barkley with Gold Insurance, a provider of health insurance products. I'd like to take 20 minutes to offer you a deal." This is not a good start. It's dry, impersonal and boring.

In general, it's best to avoid cliches like "I'd like to introduce myself and my company." This sentence is uncreative and it sounds like you're apologizing for getting your prospect's attention.

Also, be sure to avoid the word "just;" it makes you sound weaker and less confident about your offer. Don't state obvious facts either, like "You sure were hard to reach!" That might be true, but it isn't valuable information for your contact — and the last thing you want to do is waste their time.

### 3. Plan your calls in advance, strive to be empathetic and aim to help your prospect fulfill a need. 

Planning a call in advance might sound strange. After all, it's just a phone call, right? Well, as we've seen by now, it's actually much more than that; you're pursuing important goals with these calls, so you need to know what you're aiming for.

When planning your calls, try to be empathetic and put yourself in the other person's shoes. Remember, everyone has their own subjective worldview. They view things from their unique perspective and they'll only be interested in what you have to say if it resonates with them.

So, ask yourself: What is this person's daily routine? What does she need? How might your offer make life easier for her?

People have different needs, so take care to consider what your contact's might be. Maybe they need help reducing their workload or maybe they need to increase their sales revenue.

Another important part of staying empathetic is keeping your contact's happiness in mind. And don't bore them! Tell them only what they might want to know.

The first thing customers typically want to hear about is how your product or service _works_. They want to use enjoyable products without having to stress out over complicated installation processes. Save their time by getting straight to the point.

If you're selling a vacuum cleaner, for instance, be clear about how it's assembled. Be up-front about any important technical details a customer will need to know right from the start.

If you're speaking to a top-level executive, however, take the opposite approach. Executives don't care about details; they want to know what long-term benefits you can offer them. They want to know your price, how reliable you are as a seller and your backup plan if the vacuums have any unexpected problems.

### 4. Research your contact before you speak with them, and be sensitive to their unique needs and situation. 

A lot of sales reps expect cold calling to be routine and repetitive. But remember, the voices on the other end of the line are humans. They have fears and responsibilities, and they're influenced by the world around them. You, as the cold caller, need to know what these influences might be.

So, in what kind of socio-cultural context is your target? Economic recessions, major political changes, technological innovations or other shifts in society can have a significant impact on people, so keep them in mind.

Imagine there's a big news story about the negative effects of vegan food, for instance. That would, of course, have an impact on vegan food producers. If you're trying to work with a vegan food reseller, the story might change their feelings about your idea. They might not even be in the mood to talk to you!

If that's the case, it's probably not the best time to ask to increase your distribution or engage them in serious negotiations. They might appreciate an offer of expertise in crisis communication, however.

So before you make a call, learn as much as you can about the company. Company websites are usually designed to deliver easy-to-read information about the enterprise's staff, vision and core values — so use them!

If the website itself doesn't tell you enough, look elsewhere. Trying Googling your search inquiry in quotes, followed by "site:company.com," like this: "journalist" site:nytimes.com.

Sometimes it's a good idea to use Google Street View to learn a bit about the contact's office, especially if you're communicating with someone high up in the company hierarchy, like a CEO. This can help you set the right tone for the call. If she works somewhere luxurious, like in a skyscraper in a financial district, be as professional as possible. If their work area is more modest or informal, they may prefer a more laid-back approach.

### 5. Use social media and digital research tools to learn about your prospect. 

So, we've looked at the importance of research — but what's the best way to go about conducting it? Instead of opening three dozen tabs, why not use a digital search tool?

Digital search tools give you everything you need on just one screen, and they're especially useful when you have the time to do some thorough research before making your call. Online information can be overwhelming, but research tools like _InsideView_ can help you sort through all of it to get what you're looking for.

InsideView is one of the best-known research tools. It gathers basic contact information, like email addresses, phone numbers and Twitter accounts. Additionally, it maps your Facebook connection to your prospect, so you can see if you're already linked to him in any way. It even updates you on important social media news concerning that person's company.

Social networking is also important, of course. Facebook and Twitter are particularly useful for learning more about a person or organization because they're usually less formal than a company website. Check them out to learn more about your contacts, so you can relate to them in a more personal way.

Look through forums and blog posts, too. Amateur writers and filmmakers are very active online, and chances are someone has already written about the CEO you want to talk to. A blog post will probably disclose some details that more professional sites would gloss over.

And don't forget to look up your prospect on YouTube! You never know when you'll come across exclusive video material like an interview or short documentary.

Remember, we live in the digital age. Use online tools to build up your cold calling skills. Digital tools make all the difference, so use them even if you have to pay for them — they're worth it in the end.

### 6. Smart calling is about aiming for clear goals – but have a backup plan if they don’t work out! 

Why do we call up our prospects in the first place? That question might seem silly, but a lot of people phone their prospects without clear goals in mind.

Goals are critical to success, and successful cold calling isn't easy. If it were, the rejection rate wouldn't be so high!

Even "professionals" make the mistake of having only vague objectives, like "getting the word out." This can lead them to babble on over the phone, boring their prospect and getting nothing in return.

So, write your goals down before you pick up the phone. Your goals should be big — perhaps even visionary — but they also need to be achievable. A good example of a goal would be, "No matter what happens, I'll aim to build trust so that Mercedes-Benz will one day become my sponsor, even if that can't happen right now."

Don't be afraid to ask your prospect for specific information. If you hold back, you might fall victim to _limiting beliefs_ or _self-fulfilling prophecies_ — self-imposed boundaries that prevent you from achieving your goals.

However, even if you plan things perfectly and go into the call with good questions prepared, you never know when an unexpected problem might arise. This is where your secondary goals come in.

Even the greatest preparation won't always get you where you want to go. So, if things go astray, shoot for your backup goal. A backup goal might be, for instance, leaving a good impression on your prospect. If you still have a good conversation or get permission to contact them again in the future, it could definitely come in handy down the line.

### 7. Get your prospect’s attention by surprising them with unconventional tactics. 

Just about everyone loves an entertaining surprise, so don't forget this when you're smart calling. Surprising your prospect with unusual call tactics isn't just more fun, it can also work wonders!

So why not try calling your contact at an unusual time? It might sound odd or counterintuitive, but it makes you stand out as braver than the rest.

At some point in your career, you've probably been given advice like, "Don't call them over the holidays!" or "She'll hate you if you call over a long weekend." These rules might seem like common sense — but the smartest callers know how to break them.

When you call someone at a strange time, you come off as bold. Your prospect will probably feel flattered in turn, because you couldn't resist calling when you shouldn't have.

It's good to call between Christmas and New Year's Eve, for example. A lot of people are looking to spend the money they've received from their families or clients during that time.

And don't let bad weather hold you back either: a Xerox sales representative once set a three-day sales record during a snowstorm. How?

Well, a lot of office assistants stayed home during the storm, but their bosses were still going into work. The Xerox sales rep was usually in touch with the office assistants, who functioned as gatekeepers, making it hard for him to contact his sales prospects. But during the storm, he got a chance to speak with the bosses directly.

Sending your contact unusual gifts or notes also makes you stand out. It's hard to make an impression if you're the one thousandth person they've spoken to that month, but if you send an interesting gift with an invitation to a nice dinner or a night out, you're sure to get their attention.

And, of course, humor always helps. Why not send them a new pair of shoes with a clever note: "Just trying to get my foot in the door!"?

### 8. Treat assistants with respect – they might be more than just gatekeepers. 

It's a common frustration every salesperson knows: you do great research, gain useful insights on your prospect, plan your call, develop a backup plan — then dial the number and get an assistant!

Assistants or other gatekeepers can be frustrating to talk to when you're looking to make a sale with their boss. But don't think of them as mere obstacles. Instead, put yourself in their shoes: you're at work and doing a great job, when suddenly a salesperson calls out of nowhere and starts being rude to you. Would you want to help that person out? Of course not. You might even want to prevent them from showing the same rudeness to your boss.

So don't lose your patience. Instead, strive to develop positive relationships with any assistants you encounter. Introduce yourself, don't interrupt them, listen to them and answer their questions thoughtfully.

Assistants, or any other kinds of gatekeepers, are more than just barriers to your goal; in fact, they're often great business contacts. And don't forget: they're human too!

Don't assume a gatekeeper is low on the company hierarchy, either; you might be very wrong. Some gatekeepers do a lot more than just guard the gates!

The author points to an example of one sales rep who has a very special gatekeeper: his wife. She makes 90 percent of their office's purchases, but many sales reps ignore her simply because she isn't the company CEO. In fact, the CEO is the _only_ person higher than her in the company hierarchy!

Callers would do much better to befriend her; she may be a gatekeeper, but she's also a very powerful contact within the company.

### 9. Final summary 

The key message in this book:

**Cold calling doesn't have to be a negative experience for you or your prospect. Start by planning it out: use digital tools to do your research and put yourself in your prospect's shoes. Have a backup plan ready in case things go wrong, and look for ways to stand out among your competitors while treating any gatekeepers with the respect they deserve. Cold calling shouldn't be unpleasant — it should be** ** _smart_** **.**

Actionable advice:

**Devote two weeks to training yourself.**

Set some goals you'd like to achieve within a two-week period. Practice by calling a set number of prospects each day; you can even ask them for feedback! If you're open about the fact that you're new to this, they might be willing to help you. Even if you don't meet all your goals before the two-week period is up, you'll still gain valuable business insights.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Conversations That Sell_** **by Nancy Bleeke**

_Conversations That Sell_ (2013) reveals the changing nature of sales as its focus shifts away from waxing lyrical about a product to demonstrating a commitment to the needs of the buyer. From preparation to problem solving, these blinks guide you to a winning sales conversation and a strong sales career.
---

### Art Sobczak

Art Sobczak has over 30 years of experience in the sales industry. He's a professional B2B cold calling trainer at his company, Business By Phone Inc., and _Smart Calling_ is his first best-selling book.

© Art Sobczak: Smart Calling copyright 2013, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

