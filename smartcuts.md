---
id: 54347b4766636200083e0000
slug: smartcuts-en
published_date: 2014-10-10T00:15:00.000+00:00
author: Shane Snow
title: Smartcuts
subtitle: How Hackers, Innovators, and Icons Accelerate Success
main_color: 3A9AAA
text_color: 296D78
---

# Smartcuts

_How Hackers, Innovators, and Icons Accelerate Success_

**Shane Snow**

_Smartcuts_ (2014) is about the secrets used by innovative companies and bright minds to achieve big successes in the shortest possible time. It outlines the reasons why unconventional methods are much more powerful than traditional ones in today's business world, and what you can do to take advantage of them.

---
### 1. What’s in it for me? Take a smartcut to success. 

Steve Jobs and Mark Zuckerberg have become synonymous with entrepreneurial success, and the stories of how their innovations changed the world are the stuff of legend. But they're not alone. All around us individuals and companies are propelling themselves to incredible successes. So what's their secret?

One thing they all have in common is that they didn't climb the ordinary ladders to success; they invented their own. But that's not the only thing they share, as these blinks will show.

In these blinks, you'll learn

  * why you need a mentor;

  * how to ace your next basketball shooting contest; and

  * why Steve Jobs always wore black turtlenecks.

### 2. Don't climb the career ladder from the bottom up – hack into it instead. 

A lot of people assume that climbing the career ladder step by step is the best route to success. That's rarely the case, however, because success isn't just about working hard — it's about working _smart_.

That's where _lateral thinking_ comes in. Lateral thinking is about making unseen connections between ideas and searching for smart solutions outside the box. It allows you to uncover possibilities you might not otherwise notice.

Lateral thinking helps you find _smartcuts_, the most efficient and sustainable means to success. Don't be turned off by the term's similarity to "shortcut," which has a negative connotation and implies that quality might be skimped. Smartcuts get you to where you want to be in the smartest way possible. Remember: there's nothing wrong with taking the quickest path to success. In fact, that's the best way to use your time and resources.

A lot of successful people have made good use of smartcuts, even several US presidents.

About one-third of US presidents didn't follow the traditional route of serving in Congress for a considerable period before running for the presidency, instead spending only about seven years in an elected office. They gained relevant experience in other fields, then managed to "hack" into the presidential ladder at just the right time.

Consider that Woodrow Wilson was the head of a university and Dwight Eisenhower, who went on to defeat Hitler, had _never_ held an elected office before he became president. These men didn't climb their way straight up the political hierarchy — they moved across it diagonally. This is what you too need to aim for as an entrepreneur.

> _"Traditional paths are not just slow; they're no longer viable if we want to compete and innovate."_

### 3. Expert mentors help you realize your full potential. 

Have you ever heard of a famous athlete who trained without the guidance of a coach? No, and you probably never will. Coaches, or _mentors_, are critical to achieving success.

You gain an edge over others in your field when you work with a world-class expert. Business research has shown that entrepreneurs who work with mentors raise an average of 70 percent more capital than those who don't.

Jimmy Fallon, for example, wouldn't have become the host of the _Tonight Show_ without the help of Randi Siegel, his talent manager. Siegel had faith in him and kept encouraging Fallon after his first round of unsuccessful auditions for _Saturday Night Live_.

This kind of successful, long-term mentoring is founded on an organic relationship between the mentor and the mentee. In fact, psychologist Christina Underhill conducted a study on mentorship and found that informal mentoring brought much more success in terms of salary, promotions and self-esteem than formal mentoring did. Structured mentorship programs still helped, but they weren't as powerful as mentoring relationships that arose naturally.

That's because personal relationships are more effective when it comes to guiding the mentees through challenges. Mentors are more invested in their mentee's journey when they're close to them.

This happened when a team of heart surgeons from Great Ormond Street Hospital in London asked the Ferrari Formula One pit team for help. The pit team oversees track changeovers during a race. The surgeons wanted their guidance in improving the transfer of patients from the operating team to the team in the ward.

The surgeons made this unusual request because of the Ferrari team's remarkably high skill. The two groups worked together for so long that they developed close relationships and became deeply committed to producing an effective, long-term solution.

As a result of the mentorship, errors in the hospital decreased by 66 percent and Ferrari became "the hospital's official sponsor."

### 4. Don't take failure or negative feedback personally – use it to improve your company instead. 

Nobody likes to fail, but it's not possible to succeed 100 percent of the time. Failure is inevitable. So when you fail, don't just accept it and move on. Learn from your failure too — it's an important form of feedback.

Not all feedback is equally useful though. Avraham Kluger and Angelo DeNisi did a 1996 study on feedback in which they found that some kinds of feedback actually made people's performance worse. Feedback that focuses on the individual, such as "Throw it harder!" or "Bend your knees!" just makes people self-conscious and hinders them.

Interestingly, the most useful feedback is negative. Research has shown that negative feedback is more effective than positive feedback, as long as the person receiving it doesn't take it personally. That's why experts prefer negative feedback. Unlike positive feedback, it gives them something to do because it directs them toward weaknesses in their work. That's usually not the case with compliments.

The more you're exposed to constructive, negative feedback, the better you'll get at using it as a learning tool. The Second City improv school in Chicago, which has trained a number of famous comedians, even incorporates this thinking into their curriculum.

At Second City, students perform in front of live audiences every week, so they get used to hearing negative feedback. They're taught to switch off the part of their brain that associates negative feedback with failure, and see it instead as important information they can use to improve their craft.

When students are repeatedly exposed to negative feedback in a safe context, they learn not to take it personally. This allows them to benefit from it and builds their confidence too.

### 5. Make use of existing platforms relevant to your particular field. 

You still need help from other people even when you make use of smartcuts. Most of the time that means utilizing good _platforms_, meaning building on things other people have already created.

Look for platforms that already exist so you can focus on your own developments instead. It's important to know how to access the platforms in your particular field. Seek out the infrastructure, technology, strategies and environments that will help you realize your goals.

Good platforms don't just allow you to accomplish more in less time. You also learn new skills when you use them.

Computers, for example, are the product of decades' worth of work by top scientists. Every time you use one, you take advantage of that work. So use any advanced resources that you can.

Sometimes it's also a good idea to tailor existing platforms to your particular needs. The Finnish public education system did this in the 1990s when they reworked the existing system into a super-education platform.

In the new education system, salaries were increased and only teachers holding at least a master's degree were hired. Teachers then shifted their focus toward fostering long-term mentorships with individual students, centered around topics the students were passionate about.

The results of the smartcut spoke for themselves. The Finnish education system went from a mediocre standing to being one of the best in the world. Today, Finnish students advance much more quickly than the global average and 82 percent earn at least a high-school equivalent qualification.

When you use tools that already exist, you can spot flaws in those tools and make them even better. Using and improving existent platforms saves you from struggling to reinvent the wheel.

> _"You cannot dig a hole in a different place by digging the same hole deeper." — Albert Einstein_

### 6. Know your field and don't be afraid to change strategies when new patterns emerge. 

Every industry has its own particular codes and patterns. If you want to excel in your field, you need to understand those patterns.

That's why it's critical to gain an in-depth understanding of your industry or sector. It's the only way to stay alert enough to spot potential changes.

You're much more likely to identify new opportunities when you know how your industry operates, even if you don't have a lot of experience working in it. Experts and novices alike can spot changes in patterns if they understand how they work.

Riding a new _wave_, or profitable trend, might take you somewhere very quickly. Even expert swimmers can't go as fast as waves.

Sonny Moore caught a powerful wave when he was just a teenager. He identified both the onset of social media and the onset of the "screamo" music trend in the early 2000s, using them to make his band FFTL a smash hit.

He didn't stop there, either! Moore also anticipated the trend of EDM, or electronic dance music, and ended up becoming a successful producer under the name Skrillex.

When you aim to break new ground, like Moore did, allow yourself time to test things out. After all, you can't catch a big wave unless you're in the water. You can only identify an incoming wave if you know what the wave patterns are.

Google's Gmail is a good example of this. Before becoming the most popular email service in the world in 2012, it was just a side project for Google.

Google was already in the water when more and more people started wanting a better email service than what was available. In fact, the company allowed employees to dedicate 20 percent of their time to trendy side projects like Gmail.

### 7. Use superconnectors to help spread the word about your product or service. 

Even if you have a great product or service, it's not always easy to spread the news about it. That's where _superconnectors_ come in. Superconnectors are influential people or social platforms that can quickly spread the word about your venture.

Superconnect with people by figuring out what they truly value and want. Your networking endeavors will be much more successful when you give people what they're looking for. Aim for a relationship that benefits both sides.

If you don't want to spend much on advertising, for example, try to create something that people will want to share. Aaron Patzer did this with Mint Software, Inc., an online service that helped people manage their personal finances.

Patzer started out by writing a blog offering tips on how to be smarter with money. After he had gained a following, he reached out through _social bookmarking,_ in which people share lists of articles or videos that they've "liked" online. Social bookmarking means your entries can potentially be seen by millions of people once they've appeared as top stories on sites like reddit or Digg.

A lot of people shared Mint's blog posts because they thought they were useful or interesting, which allowed Mint to build the loyal customer base that became the key to their success.

People will spread your message if you're honest and sincere. Sharing rests on credibility.

That's how _Radio Rebel_, Che Guevara's broadcast during the Cuban Revolution, became so successful. It didn't attract a lot of followers immediately, but locals came to trust it over time as the revolutionaries taught them to read and defend themselves. The broadcast became part of their empowerment: they were educated and learned about the revolution at the same time.

Ultimately, it doesn't matter what medium you use to spread your message. If it's meaningful, authentic and useful, people will pass it along.

### 8. Use the momentum of your early successes as a springboard for further success. 

If you achieve some success — even just a bit of it — it means you have a good understanding of the way your field works, and you're on the right track. The next step is to pinpoint what it is that made you successful and expand on it.

Keep your momentum going. Michelle Phan did this in 2009 when she became a prominent online cosmetics expert.

Phan had been creating makeup video tutorials since 2007 and had attracted a small and loyal following. She got her big break after Lady Gaga released her "Bad Romance" music video, and Phan posted her own video explaining how to recreate Gaga's look.

This generated a lot of buzz for Phan's content. She rapidly amassed a much larger number of followers, then harnessed the momentum by uploading a series of new videos. She now runs her own YouTube channel and has founded a successful company based around her cosmetic work.

Phan's Lady Gaga makeup tutorial wasn't necessarily better than her previous videos. It was the timing that made all the difference. Thirty million people from all over the world watched the tutorial and found it useful, which created a powerful springboard that Phan then took advantage of.

Phan herself said of the events, "Success is like a lightning bolt. It'll strike you when you least expect it, and you just have to keep the momentum going."

But even after you gain a lot of success like Phan did, you still have monitor yourself and your work. Remember: it's always possible to improve.

> _"Success is like a lightning bolt. [...] It'll strike you when you least expect it, and you just have to keep the momentum going."_

### 9. Constantly reassess your approach and strive to make things as simple as possible. 

Have you ever been forced to do something the traditional way, even though you knew it wasn't the most efficient method? This can be frustrating. Just because a certain approach is widespread or "classic," doesn't mean it's the most effective.

Maintaining your success is about constantly reevaluating yourself and looking for areas to improve upon. Remember: simplicity is always the best.

You should be constantly monitoring your products and processes, looking for ways to simplify them. Simplifying your processes makes the company more efficient, cuts your costs and gives you an edge in your industry.

In the 1990s, Clayton M. Christensen coined the term _disruptive innovation_ to describe this strategy. Disruptive innovation aims to take over existing markets or create new markets by introducing cheaper products.

Disruptive innovation is ultimately about saving money, but you can't save money without developing more efficient ways to create your product or solve problems you encounter. Just think of how much simpler USB sticks are than floppy disks or burned CDs. USB sticks made the process of storing and moving data much easier, faster and more powerful.

Simplify your daily routines too! Dr. Kathleen Vohs of the University of Minnesota found that people lose their sense of self-control when they're forced to make a lot of decisions. We're not good at thinking in a balanced way throughout the day. When we're confronted with too many choices, it hampers our willpower and creativity.

Successful people simplify their daily lives so they can focus their creativity on the things they consider most important. Routine can make a big difference. In fact, Steve Jobs owned a dozen black turtlenecks so he didn't distract himself by thinking about what to wear.

> _"Geniuses and presidents strip meaningless choices from their day, so they can simplify their lives and think."_

### 10. Applying 10x Thinking can be a shortcut to success. 

You should have an idea of why self-evaluation is so important by now. So it's time for the next step: going big.

Somewhat paradoxically, an ambitious goal like making something ten times better might be easier than improving it by only 10 percent. This is due to the magic of _10x Thinking_, developed by Astro Teller at the Google[x] lab in California.

Linear improvement isn't as powerful as you might think. When you aim to improve linearly, you only make small adjustments each time. With each improvement, you only produce a slightly better version of your previous product or method.

Teller argues that the only way to make huge improvements is to throw out your old assumptions and start from scratch. 10X Thinking is counterintuitive and comes with big risks, but it also has much higher potential for reward.

10x Thinking works partly because people tend to support idealistic efforts. Behavioral psychology and business studies alike have proven that people are generally open to visionary projects that rouse their imagination. These are the projects that attract investment.

Elon Musk used 10x Thinking to create a private space technology company with its own rockets. He could have focused his efforts on improving existing space technology but he set his eyes on a bigger goal. Since working with existing technology would also be time-consuming and costly, he concentrated his efforts on his own project.

Musk inspired those around him by proving that he could learn from his failures, which got the attention of investors too. Falcon 1, his first rocket, was launched into space on February 28, 2008.

Don't be afraid to dream big like Musk did. Bigger dreams have a bigger payoff.

> _"Big causes attract big believers, big investors, big capital, big-name advisers, and big talent."_

### 11. Final summary 

The key message in this book:

**If you want to make it big, you have to break the rules. Linear thinking and traditional business methods aren't rapid or powerful enough for the modern world. So hack into the career ladder by streamlining your processes, utilizing superconnectors and 10x Thinking. Use negative feedback to your advantage instead of letting it get you down and don't be afraid to think big. Smartcuts take you where you want to go, and they get you there fast.**

Actionable advice:

**If your career ladder is blocked, switch to another one.**

If you've reached a point in your career where you can't foresee opportunities for development and advancement, don't just wait around for someone to retire. Look for tangential careers or positions in another industry or company where you can advance. Think about the ways the skills you use in your current job might be applicable in another. If you work in sales for an advertising agency, you could look for sales positions at other companies.

**Suggested further** **reading:** ** _Zero to One_** **by Peter Thiel with Blake Masters**

_Zero to One_ explores how companies can better predict the future and take action to ensure that their startup is a success. The author, one of the world's foremost venture capitalists, enlivens the book's key takeaways with his own personal experiences.
---

### Shane Snow

Shane Snow is an award-winning journalist whose work has been featured in _Wired_, _Fast Company_ and the _New Yorker_. He co-founded the company Contently and serves on the Contently Foundation Board for Investigative Journalism.

