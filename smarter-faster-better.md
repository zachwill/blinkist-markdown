---
id: 576ba933e0dd250003d615ba
slug: smarter-faster-better-en
published_date: 2016-06-30T00:00:00.000+00:00
author: Charles Duhigg
title: Smarter Faster Better
subtitle: The Secrets of Being Productive in Life and Business
main_color: C52733
text_color: C52733
---

# Smarter Faster Better

_The Secrets of Being Productive in Life and Business_

**Charles Duhigg**

_Smarter Faster Better_ (2016) combines personal stories and business research to show that being productive isn't just about managing your to-do list, but about making the right choices and maintaining the right mindset. These blinks offer advice on how to stay motivated, keep yourself on track and work in teams effectively to maximize your creativity, productivity and success.

---
### 1. What’s in it for me? Stick to your goals and become a more productive person. 

We all know people who stick to a schedule, no matter what. Despite illness or injury, they make sure to meet their goals or deadlines.

Sure, most of us aren't like this. We may have wonderful ideas but never find the time to realize them. Or if we do pursue a particular goal, we easily get sidetracked, time and again.

The good news is that you can learn how to stick with a plan, even if the world keeps throwing distractions your way. These blinks will give you helpful and easy methods to stay on track and realize your most lofty goals — smarter, faster and better.

In these blinks, you'll find out

  * how to become a marathon runner even if you've never run a mile;

  * why two researchers read 17.9 million papers in a quest to understand creativity; and

  * why storytelling can help you reach your goals.

### 2. Stay motivated by letting yourself make choices and reminding yourself of long-term goals. 

Have you ever been excited to start a new project, only to have that excitement fade over time? This is a common problem. Let's look at some tips for keeping your spirits high as you stay on track.

You can maintain motivation by making choices to benefit yourself, your team or your project. Researchers at Columbia University found that people feel more motivated when they have greater control over a situation. Basically, people get excited when they get to make choices! A sense of control and responsibility can also help you recover from any setbacks during the process.

In fact, neuropsychologist Mauricio Delgado at Columbia found that one of the brain's "motivation centers" lights up whenever a person has an opportunity to make a choice — even something as simple as selecting a colored key in a video game.

So make this phenomenon work for you! If you're stuck with a task, let yourself make a decision. Even mundane choices can help to pick you back up. If you have 40 emails to read, for instance, just pick four and look at the rest later.

Making choices isn't always enough to give you a boost, however. You also need to remind yourself how your choices contribute to the project and your overarching goals.

Remembering the big picture is another important element in staying motivated. Even if a task doesn't feel rewarding on its own, you'll feel good when you know it plays a part in something more important.

Let's say you want to develop a drone. You start reading an essay about drone building, but it's terribly dry, and your attention starts to wane. You'd rather stop and do anything else to keep from falling asleep!

What do you do in this situation? How do you get yourself back on track to focus on your goal?

Consider this simple trick. At the top of the essay, write in bold letters: "Reading this will help me make the drone." In short, remind yourself of what you want. When you do this, the essay that will bring you one step closer to your goal won't seem so boring anymore.

> _"Animals and humans demonstrate a preference for choice over non-choice, even when that choice confers no additional reward."_ — Mauricio Delgado, neuroscientist

### 3. Set an ambitious goal then break it down into smaller, easily achievable parts. 

Now that we've reviewed the power of choice, let's go over some tips for bringing your ideas to life.

Start with _stretch goals_, or your biggest ambitions. Don't be afraid to dream! Studies have shown that people are more creative when they reach for higher, bigger goals.

A 1997 study revealed that after Motorola had incorporated stretch goals into its management training, engineers were able to develop new products in a tenth of the time it took previously.

This same philosophy can be applied to personal goals, such as an obese person wanting to lose a lot of weight. Even if the ultimate goal of shedding 100 pounds isn't realized, aiming high can push a person to still achieve impressive results.

Stretch goals by definition are goals that are out of reach, so sometimes they can feel overwhelming. If your goal seems too much, that's where a SMART goal can come in handy.

SMART goals break up your stretch goal into more manageable parts. They are _Specific_, _Measurable_, _Achievable_, _Realistic_ and _Time-bound_.

Let's say your stretch goal is to run a marathon. First, you'll want to break this goal into smaller, more specific steps, such as "be able run six miles without stopping."

Then figure out how to measure yourself: "I'll run six laps on my local track."

Next, ask yourself if your goal is achievable. If you combine running with biweekly visits to the gym, it just might be. Remember to be realistic, too. You might tell yourself, "This will be tough and take time, but I can do it!"

Finally, figure out the most effective way to schedule how long you'll need to reach your goal. Maybe you'll start running two miles the first week and aim to add a mile each week after that.

Another good thing about SMART goals is you get an extra motivation boost each time you complete a stage. The more progress you make, the more excited you'll be to keep going!

> _"You can change how people act by asking them to think about goals differently."_ — Steve Kerr, former chief learning officer of GE

### 4. Stay focused on your goals by anticipating any potential distractions. 

Life is unpredictable. Even if you outline your stretch and SMART goals, unexpected events always pop up and draw your attention away.

So how do you stay focused? One good way is to create _mental models_ : positive stories that prevent distraction and keep you excited about the future.

Mental models prepare you for upcoming projects or conversations. Let's say you have a stressful week ahead. Get yourself through it by imagining how you'll conquer, step by step, each challenge.

Imagine you're a journalist and have to write an article for a travel magazine on three top SLR cameras. First off, you might make a list of ten cameras, to later narrow down.

Be sure to consider potential distractions, like a full email inbox. Perhaps you'll turn on your email blocking software before lunch, to give yourself some uninterrupted thinking time.

Once you've picked your top five cameras from your list, imagine yourself experimenting with them. Then imagine yourself choosing the best three, and writing out the results of your survey.

Now you've envisioned the whole article, from start to finish, so you can write it in a snap. In sum, once you figure out how you want your week to go, you start working toward it!

Granted, even the best plans can be interrupted, so consider how you'll handle these ahead of time. Think about possible distractions and how likely each is during a work day.

Maybe your partner will invite you out to lunch, for example. How could you spend that time in a way that doesn't disrupt your entire day? You might want to go to a self-service restaurant near work, or pack a lunch together in advance. This way your meal will take 45 minutes instead of two hours.

> _"Models help us choose where to direct our attention, so we can make decisions, rather than just react."_

### 5. Enhance your team’s performance by making sure each person feels safe and valued. 

What's the recipe for a great team? A group of overachievers or superstars? Not exactly.

Google's Project Aristotle spent two years researching what makes a team great. The project found that even a team of average performers can accomplish great things if the team has the right _dynamic._

But what's the "right" dynamic? Well, the most important factor is whether team members feel psychologically safe. Team members feel safe when they know they won't be ridiculed for making mistakes or suggesting ideas. Project Aristotle found that safe teams were more likely to perform better, envisioning more innovative products or meeting sales targets.

Psychological safety enhances performance because it allows team members to admit mistakes, which means any issues can be quickly addressed. Team members also feel more comfortable sharing unconventional ideas, which makes the team as a whole more creative.

"Safe" teams also thrive in an environment that is, in general, caring and respectful. Project Aristotle found that the safest teams are made up of empathetic individuals, which shouldn't come as a surprise: it's easier to trust people when they care about you.

Team members also feel safe when they're encouraged to contribute to a project's success. Such encouragement shows each member that he or she is an important part of the group with opinions that are highly valued.

Naturally, team leaders are tasked with fostering this psychological safety. So if you're a leader, make sure all your team members have their voices heard at least once in every meeting.

If you notice a team member is upset, encourage the person to share what's wrong and make sure other members respond in a caring way. Resolve any conflicts in the open and never interrupt team members when they are speaking. Make sure everyone feels valued and respected!

> _"We had to manage the how of teams, not the who."_ — Abeer Dubey, director of Google's People Analytics division

### 6. Fostering a commitment culture increases your company’s overall success. 

During the boom times in Silicon Valley in the 1990s, many CEOs felt that HR departments and other "company culture" ideas were irrelevant to the startup world. Developing ground-breaking ideas and products were all that mattered.

Were they correct? Not at all. An exhaustive study has shown that company culture is deeply important to the success of any firm. And a "commitment culture" is the most successful kind of culture you can nurture at a company.

In a commitment culture, management focuses on building trust and attachment to the company. Commitment culture companies rely on trust, care and the emotional connections between the organization and its employees. Such companies don't necessarily aim to hire the smartest, highest achieving individuals. Instead, firms look for people who fit their larger team and company vision.

In 1994, Stanford Business School professors James Baron and Michael Hannan started a study of nearly 200 Silicon Valley technology start-ups, in an attempt to understand more about the relationship between company culture and profit.

The team found that of the five different company cultural styles they identified, commitment culture companies consistently proved to be the most successful. Not one commitment culture firm went bankrupt; they were the fastest to go public; and they maintained the highest profitability ratios.

There's another benefit to supporting a commitment culture, in that it allows a company to maintain fewer middle managers. Commitment companies hire driven, high-quality specialists who are usually good at managing themselves. That means a company's middle management ranks can be leaner.

Your company is more efficient when specialists can address questions or problems directly. A mid-level operations manager might not know which server to choose, for example, but an IT specialist can make the right choice immediately.

### 7. Find new applications for old ideas and let your emotions guide your creative work. 

Innovation isn't always about starting from scratch. After all, you don't have to reinvent the wheel to develop a groundbreaking new car!

So instead of trying to create something completely new, strive to use old ideas in new ways.

Behavioral economics is a good example of this phenomenon. The pioneers of behavioral economics combined psychological and economic models to try to understand why humans so often make decisions counter to their interests. People decline deals if they perceive them as unfair, for instance. This runs against the idea of rational behavior, which holds that people make decisions purely for gain, put forth by traditional economists.

Behavioral economists thus managed to gain groundbreaking insight by finding new applications for old concepts.

Brian Uzzi and Ben Jones, business professors at Northwestern University, in 2011 analyzed a range of creative academic papers. Using an algorithm, Uzzi and Jones evaluated 17.9 million papers and found that in the most creative papers, 90 percent of the content had already been published elsewhere.

The innovative papers were considered groundbreaking because they approached existing concepts from new angles, not because they developed new concepts alone.

Here's another good way to boost creativity: tune into your feelings. Let emotions and intuition guide you. How you feel about a situation or idea will tell you whether you're dealing with something great or just run-of-the-mill.

Disney Animation President Edwin Catmull uses this strategy with his writers. When his team was working on the movie _Frozen_, for instance, he had them explore their emotional connections with siblings. Doing so allowed the writers to portray the relationship between the characters Anna and Elsa in a raw, authentic and relatable way, one big reason for the film's sweeping success.

### 8. Final summary 

The key message in this book:

**Staying productive, motivated and competitive is ultimately about making the right choices, both in your daily life and with your most ambitious goals. Set stretch goals for yourself, then narrow them down into achievable parts. Overcome distractions by staying prepared. Making the right choices isn't just good for you — it's better for your team and the company overall.**

Actionable advice

**Learn a new concept by talking or writing about it.**

Next time you read about something interesting, tell a friend or write a short essay about it. Your brain processes ideas more deeply when you engage with a topic, allowing your brain to build new neural connections. You'll better understand an idea and will be more able to recall it, the more neural connections you establish.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Power of Habit_** **by Charles Duhigg**

_The Power of Habit_ explains how important a role habits play in our lives, from brushing our teeth to smoking to exercising, and how exactly those habits are formed. The research and anecdotes in _The Power of Habit_ provide easy tips for changing habits both individually as well as in organizations. The book spent over 60 weeks on the _New York Times_ bestseller list.
---

### Charles Duhigg

Charles Duhigg is a Pulitzer Prize-winning reporter for the _New York Times_. His first book, _The Power of Habit_, stayed on the _Times'_ bestseller list for over 60 weeks.

