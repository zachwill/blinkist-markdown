---
id: 54fdc0ad626661000a450000
slug: smarter-en
published_date: 2015-03-11T00:00:00.000+00:00
author: Dan Hurley
title: Smarter
subtitle: The New Science of Building Brain Power
main_color: EFEE30
text_color: 707016
---

# Smarter

_The New Science of Building Brain Power_

**Dan Hurley**

_Smarter_ questions our understanding of intelligence in this new age of brain-training games. From the traditional adage of "healthy body, healthy mind," to the latest advances in computerised brain training games, these blinks explore scientifically established methods of improving cognitive abilities.

---
### 1. What’s in it for me? Find out why scientists think you can make yourself smarter. 

Do you think you can make yourself smarter? How exactly: by reading more; picking up a new instrument?

Well, there's good news and bad news. The good news is that many scientists believe your efforts to improve your intelligence will pay off. The bad news is that maybe they won't.

As these blinks explain, intelligence is unlike a muscle in many ways; you can't lift weights to grow your intelligence. And what improves our intelligence is confounding, tricky to measure precisely.

But certain techniques have proven to increase intelligence, as these blinks will show you. These blinks will also take a closer look at the most contemporary understandings of intelligence.

After reading this pack, you'll know

  * why some scientists think intelligence can't be increased;

  * why resistance training makes you smarter; and

  * why the military has invested in an intelligence project!

### 2. Intelligence is notoriously difficult to define, but we’re getting there! 

It's a little like love: We all know about it, but where is it? What is it? Among the seemingly endless questions, we're starting to make some headway in understanding intelligence.

Psychology research has proposed two general categories of intelligence. In 1971, psychologist Raymond Cattell coined the terms _fluid_ and _crystallized_ intelligence, differentiating between the two ways we think.

Fluid intelligence is our ability to think _logically_, and solve novel problems. This type of thinking underpins the act of reasoning. It allows us to see patterns, and solve things that we haven't been taught explicitly.

Crystallized intelligence, on the other hand, is the _storehouse_ of information or how-to knowledge that we accumulate throughout our lives. Crystallized intelligence helps us with many things, from answering those general knowledge questions at pub quizzes, to ensuring we remember how to ride a bicycle.

While our crystallized intelligence is constantly growing, scientists agreed that fluid intelligence was unchangeable. Up until now.

It was understood that fluid intelligence reached its peak in early adulthood, around the time you might go to university. A peak at that age explains why most of the influential work done by mathematicians, musicians and physicists occurs in their twenties, and rapidly slows after that.

Moreover, fluid intelligence is closely linked to how our brains are physically structured. So, just as we'd never be able to go to the gym to train our eyes to change from brown to blue, we can't memorize numbers and then solve equations we've never seen before! Or can we?

New evidence suggests we can. To find out, we need to overcome one sizeable hurdle: how can we measure fluid intelligence in the first place? The next blink covers the methods that work.

### 3. Psychologists are continually devising new ways to measure fluid intelligence. 

What's the true measure of intelligence? IQ tests? Brain scans? Both are contenders.

Measuring fluid intelligence is challenging because there's no way to directly observe it. That's why psychologists call it a _construct:_ a conceptual idea that we can't directly observe but nevertheless can attempt to measure. Dark matter is another example of a construct: physicists use complex equations to measure it but they've never actually seen it.

Traditional intelligence or IQ tests run by psychologists use _latent variable analysis,_ whereby multiple indirect measurements are analyzed in conjunction with each other; the degree to which the measurements sync up indicates the IQ test results. In this manner, you'd ask multiple questions about mathematics, for example, and then analyze the correlations among how the test-taker answered.

While IQ tests provide some indication of intelligence, it's an uncertain measurement. Brain scans, on the other hand, might provide more direct measurements.

A technique called functional magnetic resonance imaging (fMRI) may help us observe fluid intelligence in action. fMRI measures brain activity by showing blood flow to regions of the brain. More blood flow equates to more activity. More activity means more neuronal firing, which would mean a greater amount of brain matter working on a problem. This, in a very simple manner, is related to intelligence.

But how?

Studies show that 6.7 percent of fluid intelligence is determined by the amount of neurons, or _gray matter,_ in the brain. A further 5 percent is reflected in the size of the left lateral prefrontal cortex. fMRI shows this area to be highly active during tests of _working memory._

Working memory is the way in which we manipulate what we're asked to remember, as distinct from _short term memory_. While short term memory has little to do with intelligence, working memory is the process that new brain games aim to improve. The next blink explains how and why brain games target the working memory.

### 4. Computer-based brain games can improve your cognitive performance. 

Imagine if all you had to do to become smarter was play a quirky little game, like Angry Birds — an appealing prospect! It's not entirely out of reach either. In fact that's exactly what Lumosity offers.

Lumosity was co-founded by Michael Scanlon in 2007 during his neuroscience doctorate at Stanford. By April, 2013, it had already grown to a massive 40 million members. By turning psychologist-designed cognitive tasks into fun and engrossing computer games, Lumosity became a huge success.

One of these games is called _N-Back_, and it's designed to target your working memory. The _N_ refers to the number of places back you have to remember when items in a sequence match. So the sequence "A B C C A" is a 1-back task, as you only have to remember one place back to the last time you saw the C. In a 2-back task, you'd have to remember the letters at least two back from what you're up to in the sequence to see if there is a match.

So in a sequence of "H J K _J_ S H _S_ ", for example, you would have to hit the button on the letters that have been italicized, as these are the letters that match another letter two places back in the series. The game gets more difficult when the task becomes 3-back, 4-back and so on.

Can a simple task like the N-Back game really improve our working memory? Many said no, until psychologist Susanne Jaeggi shattered expectations in 2008 by showing that the N-Back tasks, after a mere four weeks, increased participants' scores on fluid intelligence tests by 40 percent. Similar training games have also improved the attention spans of children suffering from ADHD. 

But, these tasks aren't the only proven approaches for improving cognitive ability.

The next blink shows how you can make yourself smarter, not just in front of the computer screen, but by getting your body moving!

### 5. Non-computerized ways of increasing intelligence have also stood up to scientific scrutiny. 

Don't feel like spending much more time at your computer? Never fear: There are ways to improve your intelligence that don't involve staring at a computer screen.

As far back as the 1960s, research has suggested that physical fitness influences cognitive performance. In one study, elderly people who played tennis showed significantly better results on a variety of cognitive tests than those who didn't exercise.

Current research looks at what type of exercise yields the biggest gains in intelligence. Typically, these contemporary studies investigate aerobic exercise, such as running or swimming, and resistance exercise, such as weight-lifting. Interestingly, a study conducted by Teresa Liu-Ambrose in 2012 found marked differences in the cognitive impacts of different kinds of exercise. 

In Liu-Ambrose's study, 86 women were randomly assigned to groups of either toning, aerobic or resistance training for six months. At the end of the study, only the resistance group showed improvements on traditional cognitive tests of attention and memory, as well as increased activity in fMRI tests. 

Another active area in cognitive research is the link between learning music and fluid intelligence. In 2004, psychologist Glenn Schellenberg, a leader in the field, published a paper called "Music lessons enhance IQ," which has been cited in over 363 papers to date.

The study compared voice lessons, keyboard lessons, acting lessons and no lessons at all among young children who were randomly assigned to each group for a year. After 36 weeks all the groups showed improved IQ scores, which is to be expected after beginning elementary school. However, the students trained in voice showed the greatest improvements, followed by the children who had keyboard lessons, then drama and then no lessons.

Despite the positive evidence, some scientists are still skeptical of our capacity to improve our own fluid intelligence.

But one group in particular continues to invest in cognitive training. Read our final blink to find out which.

### 6. Skepticism about increasing fluid intelligence persists, but not in the military. 

Why can't scientists agree about whether or not you can improve your intelligence? And if evidence is so inconclusive, why's the military so interested? A meta-analysis into research on brain training and intelligence has cast doubt on whether intelligence can really improve.

A _meta-analysis_ uses various statistical techniques to make sense of differing results across studies. Patterns across studies allow for greater clarity among conflicting research.

Charles Hulme and Monica Melby-Lervåg published a meta-analysis of 23 studies on improving working memory through brain training. This study wanted to see if _far transfer_ could be found from brain training games such as the N-Back task. Far transfer is when gains on one task are also seen on a task that is not directly related. For example, verbal working memory training improves non-verbal reasoning, such as problem solving.

The results showed that there was transfer of improvement, with 22 of the studies showing small but reliable transfer from working memory to non-verbal reasoning.

However, the researchers concluded that just because improvements on working memory tasks were also seen on dissimilar non-verbal reasoning tasks, that doesn't mean it will improve real-world performance in reading or math.

Nevertheless, the US military is one group that definitely sees the value in brain-training. Federal agencies in the United states have been so impressed by brain training studies that, in January, 2014, a $12-million program was initiated by the Intelligence Advanced Research Projects Activity (IARPA). IARPA is run by the US Director of National Intelligence. The program plans to use a variety of brain-training techniques to improve the cognitive abilities of their intelligence analysts.

### 7. Final summary 

Key message in this book:

**Research shows we can improve our intelligence using brain games, but it's unclear to what extent and how exactly. Research also shows that traditional methods like learning an instrument continue to have positive results, as well.**

Actionable advice:

**Keep it natural!**

Magic smart pills such as ritalin do not increase intelligence and impair one's natural ability to improve the brain's capacity. Avoid them as much as possible.

Claims that natural supplements such as Omega-3 oil and B vitamins improve cognitive ability are also unproven. If they work, you may be experiencing the placebo effect.

**Suggested** **further** **reading:** ** _On Intelligence_** **by Jeff Hawkins and Sandra Blakeslee**

These blinks provide an overview of the human brain's capacity for thinking and for comparing new experiences to old memories. They also explain why today's machines still aren't able to emulate this capability, but why we may soon be able to build ones that can.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Dan Hurley

Dan Hurley is an award-winning science journalist. He has written nearly twenty-four articles for the the _New York Times Magazine_ since 2005, including "Can You Make Yourself Smarter?" one of the most read articles in 2012.

