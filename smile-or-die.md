---
id: 58592da797bebb00046271b3
slug: smile-or-die-en
published_date: 2017-01-06T00:00:00.000+00:00
author: Barbara Ehrenreich
title: Smile or Die
subtitle: How Positive Thinking Fooled America and the World
main_color: FBDC6A
text_color: 7A6B34
---

# Smile or Die

_How Positive Thinking Fooled America and the World_

**Barbara Ehrenreich**

_Smile or Die_ (2009) explores the impact of positive thinking on mainstream American culture. These blinks show how Americans have convinced themselves that they alone are in control of their happiness, buying into a mass delusion which in the end only does them harm.

---
### 1. What’s in it for me? Discover the drawbacks of positive thinking. 

It's natural to worry. Yet our society is overly concerned that persistent worrying will damage our health or keep us from fulfilling our true potential.

As a result, we feel obliged to be cheery and optimistic. "Lighten up!" your friends might say.

But why did we as a society become so obsessed with positivity? How did we come to believe that everyone can achieve (almost) anything? Are we really better off in the pursuit of happiness?

These blinks help shed light on America's obsession with positive thinking and the negative aspects of always trying to "be happy."

In these blinks, you'll also learn

  * how positive thinking played into the hands of the 9/11 terrorists;

  * why a bestselling author says that negative thinking causes tsunamis; and

  * why human evolution favors pessimists.

### 2. The United States today is a land of “positive thinking,” but early settlers were far more pessimistic. 

Let's say God has already determined whether you're going to heaven or hell, and there's nothing you can do to about it. If you look deep within yourself, or catch yourself thinking sinful thoughts or acting lazily, you'll just confirm what you already knew: that you're doomed.

That's what early European settlers to America, adherents to Calvinism, believed. Calvinism is a strict, frugal form of Protestantism that stresses the importance of labor and frowns upon leisure, frivolity and excess.

Reacting to their religion's extremist beliefs, many children raised in Calvinist households eventually rebel, preferring a less forbidding God and developing new, more accepting spiritual attitudes.

Take Mary Baker Eddy, for example. The daughter of strict Calvinists, her spiritual writings helped shaped the New Thought school in nineteenth-century America.

New Thought was a philosophical movement that taught that God's loving spirit lives within all people. Believers felt that anyone could overcome suffering, even physical illness, by thinking "divine" or positive thoughts.

This marked the beginning of what we now call _positive thinking_, or the idea that every person is in charge of his or her fate. This mind-set went on to transform America into a place of boundless optimism and opportunity.

Just like Calvinism, New Thought stressed self-analysis, but through a different lens. Positive thinking encourages a person to believe that things can always get better, an assumption that makes one think she can influence fate.

In other words, such an ideology promotes the idea that anyone can do anything as long as they try hard enough.

Since then, a cult of positive thinking has spread and grown to become a national ideology: the idea that every American has the opportunity to succeed.

But there's a major problem with such logic. If people believe that it's up to them to change fate, then they'll hold themselves accountable for everything that happens to them.

And that's precisely what has happened in America.

> _"If pride goeth before a fall, the United States has one heck of a comeuppance in store."_ — Paul Krugman

### 3. Plenty of people subscribe to the “prosperity gospel,” believing that Jesus wants you to be rich, too. 

Positive thinking today has become a prominent ideology in American culture at large, touching everything from people's personal lives to the workplace. It has even taken hold in places of worship.

Many churches in America today have stopped preaching about the weight of sin, guilt and suffering, telling congregations instead that God wants Christians to maintain a positive attitude. What's more, these churches hold that God will directly help believers succeed and get rich, too.

Naturally, churches that spread the "prosperity gospel" attract plenty of followers.

For instance, in a 2006 _Time_ magazine survey, some 17 percent of American Christians were found to adhere to a "prosperity gospel," and a whopping 61 percent agreed with the statement, "God wants people to prosper."

The number of "mega" churches — places of worship with a weekly attendance of at least 2,000 people — has grown to 1,210 in America alone, and many promote a prosperity gospel.

One effect of this positive ideology is rising expectations. In its extreme form, positive thinking claims that personal beliefs determine everything that happens to a person; if one is accountable in one's life, one can assume that good things will follow.

After all, if a person can shape the future by simply thinking about what he wants, human potential suddenly is not limited by the confines of a person's actions. Along these lines, becoming a world-class musician just requires the appropriate mind-set — not unique talent or years of practice. 

Yet the assumption that a person can control his life through the power of thought has produced expectations that are exceedingly difficult to meet.

For example, it set the stage for "get rich quick" books and schemes because, once people decided that their success was up to them, spiritual enlightenment wasn't enough. They expected to make a fortune, too, through a commitment to positive thinking.

> _"I believe God wants to give us nice things."_ –Joyce Meyer, influential evangelical author and speaker

### 4. Positive thinking has become a profitable business, and corporations benefit the most. 

Let's say your friend, an intelligent, hardworking and driven black man, has been passed over for promotion for the fifth year running.

What advice do you offer him? Would you tell him to file a discrimination complaint with human resources, or seek motivational coaching?

For businesses, the latter option is the preferred route. In fact, motivational coaching based on positive thinking has become popular in the business world because of the benefits it affords employers.

Motivational coaching holds people personally accountable for their successes and work satisfaction. As a result, employees learn to blame themselves — instead of employers — for any discontent they may feel at work.

This is true even of employees made to work under poor conditions — whether they're forced to accept low pay, face discrimination daily or suffer under an insurmountable workload.

Motivational coaching also aims to boost employee morale, encouraging workers to do better and more, boosting the bottom line for employers and, occasionally, rewarding employees too.

To keep employees performing and generating profits for the company, motivational coaches have workers design _dream boards_. These collages use pictures and words to represent the things that an employee wants. The board is then put on a worker's desk to serve as a reminder that working hard will help the employee reach his goals.

Another reason businesses promote positive thinking is that it helps to assuage the concerns of employees over job security. By telling themselves everything will be "fine," employees can remain focused on work and not looming layoffs.

As a result, positive thinking itself has turned into big business. Clearly, a motivated workforce is key to a corporation's growth and prosperity, no matter if the workers are deluded.

Positive thinking books, conferences, coaching programs and motivational speakers have coalesced into a thriving industry. In 2007, coaches preaching the positivity doctrine earned $1.5 billion globally.

### 5. Some people claim positive thinking has health benefits, but science does little to support this theory. 

Before advances in science, many people believed in magic, witch doctors and other spirits to keep a body healthy. Today, of course, modern society is far more rational. Or is it?

Many health experts even today claim that positive thinking is good for your health. Yet such arguments are seriously flawed.

If a person maintains a positive outlook on life, supporters claim, he'll avoid even potentially overcome illness. Stress hormones reduce the body's natural defenses; thus, the thinking goes, if you remain positive, your body's immune response is strengthened.

In his book _Love, Medicine and Miracles_, surgeon Bernie Siegel detailed how greater self-acceptance can boost a person's immune system, helping it fight cancer. He also argues that cancer might be a "blessing," as it can help patients adopt a more loving attitude toward the world and themselves.

There are plenty of reasons to mistrust such claims.

Cancer researcher Penelope Schonfield in 2004 found that lung cancer patients who remained optimistic during their treatment had the same survival rates as those who maintained a more gloomy attitude.

Another study published in 2007 by psychologist James Coyne and his coauthors examined the relationship between psychotherapy — which can improve a person's mood, reduce stress and help develop a positive outlook — and survival rates in cancer patients.

The study found that, of all the experiments claiming that psychotherapy increases survival rates, not one held up under scrutiny. This was in large part due to the studies' flawed methods. Many were done with too few patients to be reliable and, in one study, patients in the psychotherapy group received medical care of a much higher quality.

### 6. Positive thinking can turn quickly into delusional and potentially dangerous blind optimism. 

While the "cult" of positive thinking has its issues, is it so bad to remain optimistic?

Mainstream culture shuns skeptics and pessimists; nobody wants to be a "downer." But by insisting that everything will be fine because we believe it will, we're denying reality.

Bad things happen to everyone — disregarding that fact won't offer you any real protection.

Nevertheless, proponents of positive thinking remain persistent. Rhonda Byrne, author of the bestselling self-help book _The Secret_, said that people killed and displaced by the 2006 tsunami in Indonesia must have "manifested it" — that is, brought it upon themselves — through their negative thinking.

Blaming victims isn't the only problem with this ideology. Blindly thinking positive also causes people to deny signs of danger by insisting that things are always good or improving.

Any rational person knows it's dangerous to deny that real threats exist. As a species, we've survived as long as we have because instinct tells us to keep an eye out for danger, and not bask in moments of bliss.

As a result, we're always ready to fight or flee. This helps us survive because, counter to the logic of positive thinking, it's beneficial to anticipate less-than-ideal outcomes. Doing so helps us prepare for, deal with and sometimes prevent bad things.

To see such logic in action, consider the security of health or life insurance or the inclination to get an abnormal mole checked out by a doctor.

Let's turn to the tremendous dangers of positive thinking, for a moment. Consider the blind optimism of the Bush administration leading up to the attacks of September 11, 2001.

The government was well aware of potential clues before the attacks, such as warnings about an impending terrorist strike involving a plane and suspicious student pilots. But the Federal Bureau of Investigation and the president refused to believe these details, and therefore failed to take precautions that could have saved thousands of American lives.

### 7. Final summary 

The key message in this book:

**A culture of positive thinking has pervaded American society, influencing everything from health to religion to work. While optimism and willpower are celebrated as helpful and even fundamental to improving our lives, such a belief promotes behavior that blinds us to reality.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Antidote_** **by Oliver Burkeman**

_The Antidote_ is the intelligent person's guide to understanding the much-misunderstood idea of happiness. The author emphasizes that positive thinking isn't the solution, but part of the problem. He outlines an alternative, "negative" path to happiness and success that involves embracing failure, pessimism, insecurity and uncertainty — what we usually spend our lives trying to avoid.
---

### Barbara Ehrenreich

Barbara Ehrenreich is an award-winning American author and journalist who has written over 20 books. She wrote _Smile or Die_ following her diagnosis of breast cancer, when she discovered that the positive thinking rooted in cancer care can stifle the expression of a person's true emotions.

