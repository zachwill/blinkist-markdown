---
id: 53319c203862370007cd0000
slug: snakes-in-suits-en
published_date: 2014-03-25T15:30:00.000+00:00
author: Paul Babiak and Robert D. Hare
title: Snakes in Suits
subtitle: When Psychopaths Go to Work
main_color: 4E6054
text_color: 4E6054
---

# Snakes in Suits

_When Psychopaths Go to Work_

**Paul Babiak and Robert D. Hare**

_Snakes in Suits_ (2006) examines what happens when a psychopath doesn't wind up in jail, but instead puts on a suit and gets a job. The book outlines the tactics these predators use, how they damage companies and how you can protect yourself.

---
### 1. What’s in it for me? Protect yourself from the predators in our midst. 

Do you know people at work who lie constantly for apparently no good reason? Do they also seem slick and charming, but really just work people like tools to get what they want? Do they avoid responsibility and act impulsively? Do they take credit for the work of others?

If so, that person might be a psychopath. Not all psychopaths wind up in jail; some wheedle their way into big corporations and become so-called corporate, or white-collar, psychopaths. Once they're in, they manipulate their bosses, colleagues and subordinates to get what they want while destroying everyone in their way.

In the following blinks, you'll find out what psychopaths are like and why they're able to easily identify and take advantage of your weaknesses, even if you're a trained psychologist.

You'll also discover the simple divide-and-conquer tactics psychopaths use to manipulate their way into power in a company while keeping coworkers and bosses in the dark about their true nature.

Finally, you'll discover how you can prepare yourself for the unfortunate event of having a psychopath in your life. Because although they're a rare occurrence, it's likely you'll meet one sooner or later, and you don't want to become their lunch.

### 2. Not all psychopaths are violent serial killers. 

For most people, the mention of the word "psychopath" evokes images of serial killers and evil villains from movies, like the disturbing character of Dr. Hannibal Lecter. But how accurate of a depiction is that, really?

Some one percent of the general population fit the criteria of the personality disorder known as _psychopathy_. We will dive into the traits of such individuals soon, but first we must acknowledge that there is some truth to the Hollywood image: psychopaths do tend to be more violent and prone to criminality than the general population.

Though they represent only one percent of the general population, they are responsible for over half the serious and violent crimes that occur in society. Their violence is especially chilling since it tends to lack any emotional component; rather it's usually a cold means to an end.

However, not all psychopaths are violent criminals and, in fact, the definition and diagnosis of psychopathy is a complicated and difficult matter. Perhaps the best description can be extracted from a diagnostic tool called the _Psychopathy Checklist Revised (PCL-R)_.

The checklist states that psychopathic behavior is exhibited as abnormalities in four domains of personality. In each domain, the specific psychopathic traits can be identified as follows:

In the _interpersonal domain_, psychopaths are superficial, deceitful and grandiose.

In the _emotional domain_, psychopaths lack empathy, remorse and the ability to take any responsibility for their actions.

In the _lifestyle domain_, psychopaths tend to lack life goals and act in a very irresponsible and impulsive way in general.

Finally, in the _antisocial domain_, psychopaths typically have a history of lacking behavioral control, evidenced by delinquency in adolescence and adulthood.

These traits paint a compelling picture of remorseless, impulsive predators who take what they want and care little for the rules of society.

However, a word of caution: readers should not jump to conclusions about themselves or someone else being a psychopath based on these criteria, as only qualified professionals can make such diagnoses, and even perfectly normal people may exhibit several of these traits.

### 3. There’s a predator hunting you. 

Unfortunately, sooner or later, you're likely to meet a psychopath. When this happens, you'll probably have no idea who you're dealing with.

Most psychopaths wind up in prison because they lack the skills to manipulate people to get what they want, forcing them to resort to a more direct approach: violence. But psychopaths who are not incarcerated are much more subtle and capable manipulators. They often use the following three-stage process:

First, in the _assessment phase_, psychopaths will evaluate their victims' utility to them, e.g., the victims' ability to provide them with money, power, sex, celebrity, recognition and so on. At the same time, psychopaths will also assess their victims' weak spots. Not distracted by emotions or social inhibitions, psychopaths are experts at reading people, and can easily identify their needs, likes, vulnerabilities and emotional hot buttons.

Second, in the _manipulation phase_, psychopaths extract what they wants from the victim. They do this by changing their personality into a completely fictitious character: whoever is needed to manipulate the victim. Psychopaths are pathological liars and can quickly make up whatever stories they think will strengthen the bond with their victims. They are incredibly adept at managing the impression they make on others and can change their apparent personality to suit any situation: anxious victims find them soothing and bored victims find them exhilarating. A chameleon changing its color to snare a fly seems an apt analogy.

Finally, in the _abandonment phase,_ when victims are no longer useful to them, psychopaths discard them abruptly, typically cutting off all contact. The victim is often left in emotional turmoil, shocked at the betrayal of a supposedly close friend or lover. Psychopaths do not feel regret or remorse over this action like normal people. In fact, they don't feel any emotion at all: brain imaging experiments have shown that psychopaths' brains do not react to emotional material like the brains of normal people. They know that other people have things called emotions, but they neither feel nor appreciate them themselves.

### 4. Psychopaths can wear suits and ties. 

Just like any other predator, a psychopath also knows where to find his victims.

Favorite targets include affinity groups like charities or religions, because such groups are built on mutual trust, which is easy to take advantage of.

Consider the example of conman Bryan Richards, who became a prominent member of a small-town religious community by claiming he was a Christian. He even had his own radio show as "the rock jock who spins for Jesus." Secretly, though, he was running numerous scams like selling fake timeshares to fellow Christians and a Christian dating service so he could chase numerous women.

Big companies are also attractive targets for psychopaths because their employees need to trust each other, and the corporate world often offers handsome rewards like money, power and prestige.

However, the organizational controls in big businesses make it difficult for psychopaths to thrive. Psychopaths are not team players and have no interest in the company's goals, nor do they care about the rules, processes or regulations of a company, so, normally, they would be quickly spotted and ousted.

This is where _corporate psychopaths_ come in. They're psychopaths who have the necessary skills and self-control to not only avoid prison, but to manipulate coworkers, management systems, and so forth, so that they can thrive in companies.

As you can imagine, employing corporate psychopaths can ruin companies. Their abusive behavior increases employee churn, as motivated, talented employees leave. At the same time, psychopaths' grandiose risk-taking nature and their disregard for rules leads to erratic strategic choices and even law-breaking, putting the entire company in jeopardy.

So how do corporate psychopaths weasel their way into companies?

The answer is simple: through lies and charm. As pathological liars, psychopaths can easily make up impressive resumes and concoct stories of their prior experiences to suit the role at hand. Since interviewing and selecting candidates is always a subjective process, psychopaths can also greatly benefit from their ability to charm interviewers.

### 5. Are you valuable to a psychopath? 

Once psychopaths have entered an organization, they start assessing the utility of everyone there. This is made easier by the fact that new employees are actually expected to actively seek out and meet everyone in the company in their first months. Psychopaths can thus easily assess the utility of coworkers, bosses and support staff, and then charm the relevant ones.

Of course, all new employees try to be charming when, for example, they meet their boss for the first time — that's normal. The difference is that psychopaths have no intent of ever delivering any actual work.

When meeting their new colleagues, a psychopaths is primarily interested in identifying and bonding with _pawns_ that can provide him with useful resources: money, information, expertise, influence, and so forth. A corporate psychopath can, for example, deliberately build a bond with technically proficient coworkers with the goal of manipulating them to do his own work.

Psychopaths often target people who have _informal power_ in an organization, meaning those who are well-liked and whose opinions are valued. In one example, a psychopath bonded with a secretary just to hear gossip circulating around the company and also to spread glowing rumors about himself.

Through their charm, charisma and ability to change their personality, psychopaths convince pawns that they are their trusted companions. Often, the pawns become so enamored and loyal that they refuse to believe anyone who speaks ill of the psychopath.

In addition to pawns, psychopaths also look for _patrons._ Typically, a patron is a high-level executive who has had only limited interaction with the psychopath, but their few meetings have been so impressive that the patron is willing to take the psychopath under his wing, fast-tracking the psychopath's career and even protecting him from detractors.

Eventually, the psychopath is likely to depose the patron, who has expended his or her organizational influence to promote and protect the psychopath. Thus the patron becomes a patsy.

### 6. The organizational environment also hinders the psychopath. 

In addition to pawns and patrons, psychopaths also put people in companies into other categories.

If a psychopath sees that an employee has no utility to offer, that person is deemed a _low-utility observer_, and ignored. This means that, in fact, such employees are in a uniquely objective position to observe the psychopath and realize that he's manipulating others. Unfortunately, few voice these concerns to management because they fear the psychopath's or his patron's influence.

Companies typically also have some form of _organizational police_ in departments like Human Resources or Security. Their job is to protect the organizations from harm, and they pose a clear threat to psychopaths because they're so adept at spotting fraudsters. Unfortunately, they often lack the necessary management support to take action, especially as psychopaths often have supporters in upper-management positions.

One additional challenge for corporate psychopaths is that they can't discard pawns easily once their utility is depleted. The shared work environment makes the sudden shunning and abandonment all the more obvious. Initially, the pawns often wonder if they've done something wrong. Eventually though, some pawns may actually confront their former "friend" or even bring the matter to management.

Unfortunately, by this stage, the psychopath has already spread false rumors about the pawn to undermine the latter's credibility. For example, the psychopath may have given upper management the impression that the pawn is not a team player and complains a lot, so the pawn's complaint about the psychopath only serves to verify this. In the mean time, the psychopath has probably taken all the credit for the pawn's work, so he seems like a glowing employee by comparison. It's not hard to guess who of the two will get fired when a conflict arises.

### 7. Psychopaths should be stopped at the gates. 

How can organizations protect themselves when psychopaths come charging at their gates?

Naturally, the best defense is to never let them in at all, so the interviewers who make hiring decisions have a great responsibility.

Unfortunately, discerning impostors from promising candidates is not easy: psychopaths are pathological liars, have no social inhibitions or fears, and are experts at reading people, making them very impressive or even downright dazzling in interviews.

Luckily, there are ways that interviewers can avoid being taken in by a psychopath.

First of all, make a plan of the topics you want to cover in the interview and stick to it. Psychopaths are experts at avoiding inconvenient questions and changing the topic to something more beneficial to them. Whatever you do, don't lose control of the interview; ensure that you're the one deciding which questions are answered.

Second, never settle for vague answers because psychopaths like to speak in grandiose terms and gloss over any inconsistencies with deliberate vagueness. In fact, it always makes sense to dig deeper for details until you get tangible examples of the skills the candidate claims to have.

Third, ask for work samples. Psychopaths rarely have any actual technical expertise, so they can't demonstrate things like reports they've written. This may help in deterring them from applying.

Fourth, try to get multiple data points. Take copious notes in the interview so you can objectively see if the candidates answers change in follow-up interviews, and also compare notes with other interviewers to spot any discrepancies.

Finally, call references and do background checks on the internet. These are simple yet crucial steps in uncovering fraudulent applicants.

Of course, when organizations promote people internally, they must also be careful. One potential red flag is if the candidate has been consistently unable to form a team. This could indicate a psychopath's unwillingness to share credit or accept blame, which often derails teams quickly.

### 8. You can defend yourself. 

If you're unlucky enough to become the target of a psychopath, you are in for a wild, emotionally draining and eventually devastating ride. Most likely, you won't realize what you're dealing with until it's too late, but there are a few ways to prepare yourself.

First, know your enemy: learn about psychopathy. Understand the traits of a psychopath to see past their "mask" of normality. However, be careful you don't start diagnosing people around you, especially not based on a mere few traits. It takes a qualified professional to make such judgments, and the "psychopath" label is too serious to be wielded carelessly.

You should also familiarize yourself with the three-stage manipulation process described earlier.

Second, learn about yourself. What are your weaknesses, likes and emotional hot buttons? What riles you up? What makes you like someone? Psychopaths are experts at reading people, and will easily spot and push these buttons to manipulate you.

For example, you may be insecure about your looks and always respond positively when complimented on them. A psychopath will take advantage of this to build rapport with you and extract favors.

In addition, it's important to understand how you might be useful to psychopaths. Do you have a resource that they want? Or do you wield power, formal or informal, in a group with resources? Psychopaths have been known to target people like hurricane victims who have lost everything, so don't think not being wealthy will protect you.

Finally, try not to get sucked into the psychopath's game.

It's very easy to be swayed by a psychopath's charm, so when you feel you're bonding with someone unnaturally quickly, try to think critically about your first impression, and how plausible their stories really are. Of course, this is easier said than done. In fact, even one trained prison psychologist was so taken in by a psychopath that she lied at his parole hearings in the hope that they would get married once he was released. She lost her job and her reputation as a result.

### 9. What if you run into one at the water cooler? 

Now that you know that corporate psychopaths exist, you may well want to know what you can do when you meet one at work? Besides the tips presented in the previous blink, the work environment presents a few additional considerations.

First, as always, relationships and reputation are important, but especially so when dealing with a psychopathic boss or coworker. You should also try to build a reputation as a high performer. By always doing your best, you build a solid reputation against any rumors the psychopath may spread about you, and also stop him from using any poor performance against you.

Second, you should get to know your company's formal processes and procedures, such as your personal obligations and the best channels to voice concerns. Know your options for filing a formal complaint against an abusive boss or coworker, for example.

Third, whenever possible, try to get work assignments, performance feedback, and so forth, in writing. Even if your boss isn't a psychopath, this approach ensures that you'll have a mutual understanding of what is meant. For example, when you see your performance review on paper, make sure it's detailed and clear. Always ask for justifications if there is any negative feedback.

Fourth, whatever you do, avoid confrontations and always keep calm. The psychopath will deliberately push your buttons to try to make you explode at a moment that will make you look bad. If attacked, defend your decisions with reason and calmly state the facts, that's it.

Finally, if necessary, leave on your own terms. If your boss is a psychopath, truth be told, you are likely in a no-win position. It may be that your only recourse is to find another job. Try to get references from all over the company, not just from your boss, and try not to burn any bridges on your way out.

### 10. Final Summary 

The key message in this book:

**Not all psychopaths are in prison: many of them put on suits and roam corporate offices. They use their incredible impression management skills to charm victims and extract whatever favors they want, often rapidly advancing up the corporate ladder at the expense of their colleagues and the company. The only hope for those involved is to quickly spot these predators before they destroy the organization.**

Actionable advice:

**Know thyself.**

Ask your family and close friends — the people who know you best — to describe how they see you: strengths, weaknesses and all. You might well be surprised at what you hear, and this will help you better understand what emotional hot buttons you have that a psychopath may take advantage of.

**Know your company.**

If you don't already, spend some time getting to know the rules and formal processes of the company you work for. It is good to know what options are at your disposal, and also it will help you spot any situations where someone — psychopath or not — asks you to do something which may be in violation of those regulations.
---

### Paul Babiak and Robert D. Hare

Paul Babiak, Ph.D, is an organizational psychologist who specializes in management development. His work has been featured in the _New York Times_ and _Harvard Business Review_.

Robert D. Hare, Ph.D, is an emeritus psychology professor at the University of British Columbia and considered one of the world's foremost experts on the phenomenon of psychopathy.

