---
id: 519de67fe4b0a17389a8ac09
slug: so-good-they-cant-ignore-you-en
published_date: 2014-01-15T15:47:38.000+00:00
author: Cal Newport
title: So Good They Can't Ignore You
subtitle: Why Skills Trump Passion in the Quest for Work You Love
main_color: F26432
text_color: A64422
---

# So Good They Can't Ignore You

_Why Skills Trump Passion in the Quest for Work You Love_

**Cal Newport**

How can you find a job that you are good at and enjoy? This book advocates the "craftsman mind-set" of patiently developing skills instead of the typical "follow-your-passion" advice, and offers practical solutions to acquiring and maintaining job satisfaction.

---
### 1. What's in it for me? 

Probably, at one point or another, someone, somewhere has told you that the secret to success and happiness is to "follow your passion." This is the slogan of countless self-help gurus sporting fake tans around the world. But is the advice good? If so, why did Steve Jobs build one of the most successful companies in the world when his passion was to become a Zen master?

_So Good They Can't Ignore You_ (2012) does away with the "passion trap" and instead looks at more realistic and practical ways to succeed in and be satisfied with your career. You will learn how to develop the rare and valuable skills that great jobs require. You will also learn how to parlay those skills into control and autonomy in your work — a "must" for job satisfaction. You will also discover how you can find your own mission: an inspiring goal to strive for professionally.

### 2. Passion is rare, and striving for a job you’re passionate about often leads to unhappiness and dissatisfaction. 

"The passion hypothesis" taught by life coaches and authors urges individuals to "do what they love." The gist is this: find your passion first, and then meaningful work will appear at your fingertips. 

But is passion necessarily the right path?

First of all, real passion that coincides with professional possibilities is extremely rare. When questioned in a 2002 study, 84 out of 100 Canadian university students responded that they did have passions. However, most of the passions they identified had no viable relationship to available careers, but were instead hobbies such as dancing, reading and skiing. In fact, only four in 84 of the students identified passions with direct connections to work or education, such as computer programming.

Secondly, passion can be dangerous.

Since the birth of "the passion hypothesis" in 1970, more people have begun to follow their passions. Convinced they should do only work they love, they switch jobs more frequently. But the job market can't meet these demands. Since we cannot all be professional beer drinkers or poets, more job seekers wind up in jobs they are unhappy with. In fact, job satisfaction has actually _declined_ in recent decades; in 2010, only 45 percent of Americans surveyed were happy with their jobs, down from 61 percent in 1987.

This means that looking for the work you were "meant to do" is likely to be a route to constant job-hopping and self-doubt.

Take Thomas, for instance. Thomas was unhappy with his work. Wanting to become a Zen Buddhist, he finally followed his passion to the monastery. He described his arrival as the feeling of "being really hungry" and expecting "an amazing meal" — only to find that the meal didn't satisfy him. Although Thomas succeeded in his Zen practice, nothing had changed; he still had worries and anxieties. He learned that the path of passion does not guarantee happiness.

**Passion is rare, and striving for a job you're passionate about often leads to unhappiness and dissatisfaction.**

### 3. Don’t do what you love. Learn to love what you do – by acquiring mastery, autonomy and relatedness. 

It would seem that finding a passion first and then creating a career around it isn't the route to success. But if it isn't, how else can you be happy with what you do?

The answer might lie in experience. A survey of college administrative assistants found that this was the determining factor in the workers' satisfaction. Although the assistants' job seems boring, when they were asked about their work, their answers varied. A third considered it a _job_ — merely a way to pay the bills. Another third termed it a _career_ or a path toward something better. The final third considered it a _calling_, or an integral part of their life and identity. The more experience the assistant had, the more likely they were to enjoy their work and consider it a vocation. So passion comes with time, since you are more likely to be satisfied with what you do when you've become good at it and have developed a sense of efficacy and strong relationships with your co-workers.

But experience isn't the only factor contributing to job satisfaction. Another is expertise.

When you have mastered something, it is more likely that you will become passionate about it. A scientific theory called the _self determination theory_ demonstrates this. The theory has identified three basic factors required to generate intrinsic motivation, which is in turn linked to higher levels of job satisfaction. These three factors are: _autonomy_, the feeling that you have control over your day; _competence_, which is the feeling that you are good at what you do; and _relatedness,_ the feeling of connection you have to other people.

To be autonomous and competent means to achieve mastery in your given field. To do that, you don't need passion, only the willingness to work hard to acquire that mastery **.**

**Don't do what you love. Learn to love what you do — by acquiring mastery, autonomy and relatedness.**

### 4. Adopt the craftsman mind-set, practice hard and get out of your comfort zone. 

The passion mind-set revolves around the question: "What do I really want?" This means people who have it tend to wonder if their job is right for them. They focus on the value their jobs provide them, and are acutely aware of everything they dislike about their work. The result? Their job satisfaction and overall happiness decreases.

By contrast, the "craftsman mind-set" asks: What value can I bring to my job?

The craftsman mind-set acknowledges that no matter what field you're in, success is always about _quality_. As comedian Steve Martin puts it: "Be so good they can't ignore you." Focus on the quality of the work you are doing now, instead of always wondering if it is your true calling.

When you adopt the craftsman mind-set, you will not hesitate to do what is necessary to improve the quality of your work.

And how can you improve quality?

Through _deliberate practice_, which is practice that stretches one's abilities and from which constructive feedback is sought.

A chess player, for example, must devote roughly 10,000 hours to practice and study to become a Master.

That's not all, however. Once Master-level has been achieved, the best chess players aren't the ones that practice more, but those who practice _smarter_. What does that mean? It means they practice strategically and study seriously — that is, they engage in _deliberate practice_.

In the case of the chess player, deliberate practice might involve studying difficult theoretical chess problems instead of just playing more. Why? Playing does not necessarily push the player outside of his comfort zone, because opponents are chosen randomly, and therefore may not have the skills to really challenge the player. Theoretical chess problems on the other hand can always be tailored to the player's current level.

Although deliberate practice is often strenuous and uncomfortable, you should not avoid it because only by adopting it can you attain true mastery.

**Adopt the craftsman** **mind-set** **, practice hard and get out of your comfort zone.**

### 5. To get a great job, collect career capital by acquiring rare and valuable skills. 

The craftsman mind-set is helpful in any career because, by encouraging deliberate practice, it helps you acquire specialized skills and mastery. That's a good thing, because people with rare skills are more likely to get great jobs. These are the rare jobs where workers can be creative and have control over what they do.

So how do you get one?

In a supply and demand employment market, if you want a rare and valuable job, you need equally rare and valuable skills. These skills are called _career capital_, and they are what helps set you apart from other individuals.

To see the importance of career capital, consider the example of Laura, a former accountant who took a risk and opened her own yoga studio. The studio was successful for a while, but then a financial crisis hit. While Laura had lots of career capital in accounting, she had very little in yoga studio management, and therefore mismanaged the studio. It was one of the first to go under.

So how can you acquire sufficient career capital to avoid Laura's fate?

Through the craftsman mind-set: it forces you to get better at what you're doing.

A good example of this can be seen in the story of Alex, a television writer.

In his chosen profession, Alex faced a particularly brutal "winner-takes-all" market. Instead of forging ahead with passion alone, he coolly surveyed the scene and determined that the career capital he needed was the ability to write scripts really well. So he practiced deliberately: he wrote scripts and asked colleagues to give critical feedback, over and over again. Practicing in this way, he acquired the career capital needed to out-write the competition.

**To get a great job, collect career capital by acquiring rare and valuable skills.**

### 6. Acquire career capital to maintain control and autonomy in your work. 

Research has shown that being _in control_ is a key ingredient to a happy, meaningful life. This is also true of work, where there are a few control-related traps to avoid.

For a start, we often think that control acquired without career capital is sustainable. It isn't.

Let's take two examples.

First, consider Jane, a highly accomplished student who wanted to gain total control over where and how she worked. To accomplish this, she started a travel blog to finance her world travels. But Jane's blog failed because she had no career capital in blogging. In fact, she had not even considered how to attract readers or monetize the blog.

Secondly, consider Ryan, who also wanted to gain control over his work by starting his own farm. Although he had no formal background in agriculture, he had already acquired the necessary career capital by growing and selling crops in his backyard. This career capital made his Red Fire Farm an enduring success.

Another trap is the temptation to relinquish control gained at work in return for a promotion.

For example, say you've achieved such mastery of your job that your boss gives you a fair bit of autonomy in managing your work. If you try to make a change like cutting back your hours, you will probably encounter resistance, because your boss will see it as a threat. They might lose a valuable employee.

To avoid losing you, your company might try to promote you. Beware though, the salary may be better, but you'll probably lose your hard-earned control as you have to take on new, unfamiliar tasks.

This almost happened to Lulu, a software developer, who built up career capital and used her strong position to barter for a shorter working week. When the company offered Lulu a promotion to keep her on full-time, she was brave and turned it down, opting for shorter hours and better control.

**Acquire career capital to maintain control and autonomy in your work.**

### 7. Use the craftsman mind-set to find a motivating mission that's a unifying goal for your work life. 

Every job is more motivating if you have a _mission_. People who have a useful, meaningful goal in their work are more satisfied with their work and are better at handling even stressful work.

Harvard biologist Pardis Sabeti, for instance, has a mission to use modern computing technology to fight old diseases. Although she works in a demanding field, this mission allows her to enjoy her work and even leaves her with energy for other creative activities too.

But where do you look for a useful career mission?

In the _adjacent possible_.

The adjacent possible is the space that contains all the discoveries waiting to be made next: the possible combinations of existing ideas that hover just beyond the current cutting edge of science and technology. Since they are within reach of everyone working on them, scientific breakthroughs often occur repeatedly and simultaneously as multiple researchers make the same discoveries. For example, four researchers discovered sunspots independently in 1611, and oxygen was successfully discovered twice by different people in the space of just a few years.

Good career missions, like scientific breakthroughs, lie in the adjacent possible of any given field. Dr Sabeti's motivation comes from using technology to push the boundaries in biology, not from doing the same old thing.

In any field you have to already be at the cutting edge to find a mission.

So how do you get there?

By choosing only a few critical areas to develop skills in, and then using the craftsman mind-set to practice those skills. If your interests are scattered, you'll only develop superficial skills and will never reach the cutting edge from where you reach the adjacent possible.

But remember that missions aren't always good starting points. Don't worry too much about finding one — they usually come along by themselves as you acquire rare and valuable skills.

**Use the craftsman** **mind-set** **to find a motivating mission that's a unifying goal for your work life.**

### 8. Success in a mission requires making bold bets and standing out from the crowd. 

The first step towards arriving at the cutting edge of your field is acquiring career capital, and you'll find a mission when you get there. But when you do, how do you put your mission into practice?

You know the old adage "Rome wasn't built in a day"? Well, neither are careers.

Rather than trying to fulfill your mission in one grand fell swoop, pursue small and achievable projects to make headway step by step. Make _small bets_ that take only a few months to realize and give immediate results, so you know whether you have succeeded or failed. Leverage the small but significant wins to advance in your mission, and when you inevitably fail on occasion, learn from your failures and improve.

For example, consider Kirk, an archeologist with a mission to popularize archaeology. To pursue his mission he made a few baby-sized bets: First, he digitized old footage from a 1960s archaeology documentary and published it as a DVD. Then, he filmed some of his own documentary footage, and applied for additional funding. Both bets are manageably small, and can easily be deemed successes or failures, thus providing feedback.

Ultimately though, for a mission to succeed, the end goal must be _remarkable_. Remarkable means two things: it must compel people to talk about it and it should be published in a venue where they can do so.

A good example of having a remarkable mission is Giles, a computer programmer who wanted to create an open-source artificial intelligence program that could write music. The project was unique and intriguing enough to stand out; it was as attention-grabbing as a purple cow among brown cows. When he published his program, he did so in the open-source scene, where people are eager to learn about new programs and can chat about them via forums.

So, now you know how to be so good they can't ignore you: Use the craftsman mind-set, acquire career capital and, step by step, find and pursue your mission.

**Success in a mission requires making bold bets and standing out from the crowd.**

### 9. Final summary 

The key message in this book is:

**Instead of searching for a job that corresponds to your passions, learn to love what you do. The first step in doing so is acquiring career capital through deliberate practice. This demands the adoption of the craftsman mind-set. Also, it never hurts to have a mission to pursue and it can greatly increase your job satisfaction.**

Actionable advice from the book:

**Decide what kind of market you're in**.

If it's a winner-takes-all market, there is one sort of career capital that is successful: quality. You have to be the best at what you do. The auction market, on the other hand, allows for many different approaches to success. In the competitive blogosphere — an example of a take-all market — only really well-written advice blogs can succeed and attract readers.

**Identify the skills you need to succeed in your given field**.

This is called "identifying your career capital type." If you are a blogger, how can you write exceptionally compelling blogs? How can you use the skills you already have and build on them? You must remember that it is very difficult to start from scratch.

**Keep control traps in mind when making important decisions about your work.**

Without career capital, you will never have control in your job. That's because until you've proven your competence and mastery, others will not give you control over your work.

**To find your mission, do background research in your field regularly.**

You never want to lose sight of the adjacent possible.

**Suggested further reading: _How To Be a Straight-A Student_ by Cal Newport**

_How_ _to_ _Be_ _a_ _Straight-A_ _Student_ offers you successful strategies used by actual straight-A students to help you score better grades while studying less. From time management to concrete advice on developing a thesis, this book gives you all the tools you need to earn the perfect 4.0 without burning out.
---

### Cal Newport

Cal Newport earned a PhD in computer science in 2009. He was a post-doctoral associate at MIT and was en route to becoming a professor when he learned how frustrating looking for an academic job could be. He wanted to stay in the United States for family reasons, but learned that it might mean starting over from scratch. This book is about the answers he found to the question, how do you end up loving what you do?

