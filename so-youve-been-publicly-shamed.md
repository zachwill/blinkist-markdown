---
id: 553e266c38393500071c0000
slug: so-youve-been-publicly-shamed-en
published_date: 2015-04-27T00:00:00.000+00:00
author: Jon Ronson
title: So You've Been Publicly Shamed
subtitle: None
main_color: 23AAB1
text_color: 19787D
---

# So You've Been Publicly Shamed

_None_

**Jon Ronson**

_So You've Been Publicly Shamed_ (2015) looks into the terrifying nature of online public shaming. Tracing it back to its historical roots, the book details the motivations behind modern public shaming and offers tips on what to do if you find yourself at the center of a public shaming scandal.

---
### 1. What’s in it for me? Discover how the ancient phenomenon of public shaming has returned and what it does to its victims. 

What do you think about when you hear the word "punishment"? A fine? A prison sentence? For those convicted of a crime, the prospect of spending time in jail is terrifying enough. But for many, there's something far worse: the shame.

Public shaming started long ago, and most societies have had ways of not just inflicting physical pain on those convicted of a wrongdoing but of degrading and shaming them in public. Many of these foul practices have long since been buried, but with the rise of online communities and social media, public shaming is once again rearing its ugly head. All it takes is one ill-advised picture or comment, and you might face a storm of verbal abuse or even get fired from your job.

Luckily, there are ways to minimize the damage and turn the public eye elsewhere — that is, if you're rich.

In these blinks, you'll learn

  * why companies fear the power of social media;

  * why you should never fabricate a Bob Dylan quote; and

  * why you should stay away from crowds.

### 2. Public shaming has been a common punishment for centuries – and it’s seeing a renaissance in the online community. 

If you've ever seen one of Europe's many medieval torture chambers, you were surely surprised to discover that, while many torture devices were indeed designed to inflict severe physical pain, others were designed with the express purpose of embarrassing and publicly humiliating their victims.

Though they left much of their old lives behind, Europeans settling in the New World didn't abandon the practice of public shaming.

Indeed, public shaming was a common but dreaded punishment in North America during the 18th and 19th centuries.

Puritan settlers, for example, had a penchant for punishing transgressors with public whippings. Newspapers of the time featured accounts of the whippings, relishing in the gruesome details, such as how violently the victim squirmed.

For instance, in 1742, a married woman and her gallant were accused of adultery, and subsequently sentenced to lashes at the public whipping post. The woman, instead of asking not to be whipped at all, pleaded for a _private_ whipping in order to spare her children the mortification. 

Public shaming faced opposition in the North American colonies, and was eventually abolished. 

Benjamin Rush, one of the United States' Founding Fathers, called for the abolition of public shaming in 1787, on the tail of an already growing opposition to the practice. By 1839, public punishments were abolished in all states with the exception of Delaware.

According to the author, opposition to public punishment wasn't due to an observed ineffectiveness, but because it was perceived as especially brutal.

Today, public shaming is experiencing a renaissance in global online communities. Seemingly small infractions, such as a silly yet tasteless Facebook photo posted by charity worker Lindsey Stone in 2012, elicit extreme public reactions.

Her photo showed her pretending to shout and swear next to the "Silence and Respect" sign at the Tomb of the Unknown Soldier. This trivial act brought forth an army of 30,000 people requesting her termination. Another 3,000 signed a petition demanding the same.

> _"It would seem strange that ignominy should ever have been adopted as a milder punishment than death." — Benjamin Rush_

### 3. Sometimes, people use public shaming to right a perceived wrong. 

We sometimes feel powerless in the face of injustice. We discover that mega-corporations exploit their employees or that a politician goes back on their election promises, but there's not much we can do, despite our feelings of outrage.

Social networks, however, have begun to change this.

Instead of feeling ineffectual and incensed, we can now organize and take action. All you have to do is Tweet what you think of this or that person, tell all your virtual friends to do the same, and suddenly you've created a torrent of public shame.

And public shaming is sometimes an effective way to oppose injustice.

This was the case for one unfortunate couple that was unable to cancel their LA Fitness gym membership, even though the husband was newly unemployed and unable to afford the fees. Adding insult to injury, the wife was seven months pregnant! When _The Guardian_ exposed the company for their callousness, readers were appalled.

Thus began a massive public shaming. At first, readers sent letters of protest. Then, the news began circulating on Twitter, allowing thousands of users to join the protest. Some even canceled their _own_ membership with LA Fitness out of disgust!

Eventually, LA Fitness backed down and waived all charges against the couple.

Social media makes public shaming particularly easy to engage in. All you have to do is post derogatory comments on someone's Facebook page or Twitter timeline, and the next thing you know you're part of a huge public shaming campaign.

This is precisely what happened after journalist Michael Moynihan, in a 2012 article in the _Jewish Tablet_, revealed that author Jonah Lehrer had fabricated Bob Dylan quotes and plagiarized passages from other texts. Soon, internet message boards were abuzz with deprecatory and spiteful comments.

It didn't take long for things to get personal. Soon, users were getting vitriolic: "The twerp is such a huge overachiever that there's something delightful about seeing him humbled," said one user. And another: "Jonah Lehrer is a frigging sociopath."

### 4. Shaming others can be a means of reclaiming part of the public space or to regaining a sense of control. 

In the past, public shaming was dictated by some authority figure, be they a judge or a religious leader. Today's digital shaming, in contrast, emerges out of the midst of the online community. That's no coincidence.

Today, many people feel that (offline) public space has been taken away from them. They feel that the process of gentrification is pushing them out of the areas where they used to live and spend their time.

This is certainly the case in New York, where police officers randomly stop and frisk as many as 1,800 people each day. According to city officials, this "Stop and Frisk" policy is intended to reduce the incidence of petty crimes, such as illegal graffiti.

But according to the NY Civil Rights Union, around 90 percent of those subjected to this policing policy are completely innocent. And many people report feeling degraded by the procedure. In conversations with the author, they said authorities used this policy to claim public space, or to protect the property of the privileged few at the expense of the majority.

Public shaming online can be seen as an act of reclaiming control. It's a powerful weapon.

When the author spoke with members of 4chan, an online message board and renowned meeting place for trolls, they reported that feelings of complete disempowerment motivated them to join 4chan and participate in public online shamings.

One such public shaming occurred following an incident at the tech event PyCon, where participant Adria Richards overheard two men joking about a device's "large dongle." Upset by their sexualized language, she took a photo of them and posted it to Twitter, requesting help in dealing with the situation. As a result, one of the men, "Hank," was fired from his job.

Enraged, 4chan users started a massive public shaming campaign against Richards. In addition to personal attacks and threats, they attacked her employer's website, resulting in her termination.

### 5. Public shamings get out of hand sometimes – and we don’t really know why. 

It's a familiar scenario: a Twitter user tweets something stupid, and then suddenly they're accosted by an angry, anonymous mob intent on their destruction. But what are the psychological mechanisms that set the gears of this kind of online public shaming in motion?

Individuals behave differently in crowds than they do on their own. In his seminal work _The Crowd_, Gustave Le Bon, a nineteenth century French physician, described the phenomenon thus:

In a crowd, even the most civilized individuals can become irrational and impulsive. For example, at a large gathering, a few agitated people can infect the whole crowd with their excitement. Once this excitement reaches a fever pitch, it may result in a riot. Le Bon called this sudden public mayhem "crowd madness."

Le Bon uses the metaphor of "contagion" to describe the mechanisms of collective public outrage. His choice of words implies that rioting occurs unintentionally; people simply obeyed the dynamics of the crowd.

Social psychologist Steve Reicher, however, opposes this view of crowd behavior. According to him, people don't unintentionally join forces with others, even in a crowd.

Indeed, there _are_ patterns that guide crowd behavior. Without them, we'd be incapable of spontaneously acting together or without a leader.

To illustrate this, imagine that you arrive at a college campus on a warm summer day and catch sight of people dancing to a captivating rhythm. As more and more people join in, you feel compelled to join them, and eventually find yourself participating in this spontaneous dance.

But not all group activity is like a dance. Usually, people engage in group behavior because of their own moral convictions.

For instance, if someone joins in the public shaming of a homophobe, it's probably because they personally consider homophobia wrong, not because of the hypnotic force of group dynamics.

> _"For Gustave Le Bon, a crowd was just a great ideology-free explosion of madness… But that wasn't Twitter."_

### 6. Public shaming hurts both the targeted people and the people around them. 

In the days after posting that harmless (though quite tasteless) photo of herself posing by the Tomb of the Unknown Soldier, Lindsey Stone saw many posts on social media wishing on her either death or sexual violence or both. She was so traumatized by this massive public shaming that she rarely left her home for a year.

Indeed, public shaming is very harmful to its victims. The abuse makes them feel attacked and humiliated, which can be so traumatic that it leads to depression or even PTSD.

Victims of public shaming can also find their livelihoods ripped out from under them; often, as a result of the scandal the shaming causes, victims get fired.

Both "Hank," the guy at PyCon who made a dirty joke, and Adria Richards, the woman who reported his behavior on Twitter, were fired because their bosses thought that the shaming might harm their companies.

Moreover, victims of public shaming often suffer long periods of isolation. Fraught with humiliation, they retreat into their private spaces. Even dating becomes difficult, as their dates might Google them and find out about the shaming.

Jonah Lehrer reported feeling "radioactive" after his public shaming and feared that it might also hurt his personal relationships.

Public shaming doesn't just harm the individual, though. It also creates an atmosphere of terror.

The mere possibility of becoming the target of public shaming is terrifying to many. So many of us have worked hard to establish a good reputation, and the careers of others depend on this hard-earned reputation. This is especially the case for politicians, PR people and journalists.

It's even more terrifying that something as small as a stupid tweet, or as private as an affair, could be enough to elicit the wrath of the public. Even the author felt the effects of this terror when discussing Jonah Lehrer's case at a party. He realized that any of his past indiscretions could be unearthed, and his reputation upended.

> _"What I mostly feel is intensely radioactive." — Jonah Lehrer_

### 7. The relationship between shame and crime is complicated. 

For more than 20 years the notorious Texan Judge Ted Poe has tried to restore order in Houston by publicly shaming defendants convicted in his court. But did his strategy work?

As it turns out, public shaming is indeed an effective deterrent to crime. Consider the case of a notorious young shoplifter who was sentenced to parading around a store while carrying a sign that read, "I stole from this store. Don't be a thief or this could be you."

Transformed by this experience, the young man earned a B.A., founded his own company and never committed another crime.

According to Judge Poe, about 66 percent of the people he sentences to prison eventually commit another offense, compared to only 15 percent of those who were publicly shamed.

However, shame can also elicit crime and violence.

According to the psychiatrist James Gilligan, violence is almost always directly linked to shame. If we experience enough shame, we can eventually become violent, either to ourselves or to others.

Acting violently toward others is sometimes an attempt to restore self-esteem that's been damaged by shame. People sometimes feel that when they victimize others, they are no longer powerless victims themselves.

People also inflict violence on themselves (e.g., self-mutilation) as a way to overcome the physical and emotional numbness they feel as a result of intense shame.

A prime example of this occurred in Massachusetts prisons and psychiatric wards in the 1970s, where there was a surprising epidemic of suicides, homicides and fire-setting. In one particular prison, there was a murder every month and a suicide every six weeks!

Gilligan looked into these incidents and spoke with prisoners in an attempt to get to the bottom of all the violence. He discovered that nearly all the offenders had experienced utter shame and humiliation prior to their crimes, often beginning in childhood.

So far, we've seen the motivations and aftermath wrought by public shaming. In our final blink, we'll examine ways that people can mend their damaged reputation.

> _"If shaming worked, if prison worked, then it would work. But it doesn't…" — James Gilligan_

### 8. You can restore a damaged online persona – if you have money. 

How is it that some celebrities can commit heinous acts and yet, after three years, the scandal has magically vanished from Google search results? Well, it's not magic — but it _is_ expensive.

There are agencies out there, such as Metal Rabbit Media and reputation.com, that offer restitution services for a damaged internet reputation. In essence, their job is to make sure that the first pages of a Google search on their clients will yield only harmless information or — even better — flattering trivia about their charitable work, their passion for marathons, and so forth.

Damaging information or details on the scandal are pushed down to the following pages where searchers are less likely to look.

While some of these companies will take on any paying client — even convicted child abusers! — others reserve their services for people who were the victims of unwarranted public shaming.

But this service is extremely laborious, and thus very expensive. They have to spend their time doing things like editing clients' Wikipedia pages or creating fake news content about their clients in order to push unwanted results off the first page of Google.

Indeed, those fascinating gossip articles you read about celebrities on the internet might be fabricated by a reputation-management service to hide something even juicier!

As we've mentioned, these services are expensive **–** so expensive, in fact, that only the privileged few can afford them.

Consider that, while Michael Fertik, the owner of reputation.com, helped Lindsey Stone for free, he estimates that a case like hers would normally cost up to $700,000.00.

So, should you ever become the victim of unwarranted public shaming, there is a way out — but only if you're a millionaire.

### 9. Final summary 

The key message in this book:

**Public shaming has long been part of human history. But now that we're all connected through social networks, it has taken on a whole new dimension. Even the slightest careless faux pas can make you the center of public vitriol. But should that ever happen to you, there is hope — but it will cost you.**

**Suggested** **further** **reading:** ** _Trust Me, I'm Lying_** **by Ryan Holiday**

_Trust Me, I'm Lying_ is an in-depth exposé of today's news culture, which is primarily channeled through online media sites called blogs. By detailing his experiences with multimillion-dollar public relations campaigns, the author takes us behind the scenes of today's most popular and influential blogs to paint an unsettling picture of why we shouldn't believe everything that is labeled as news.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Jon Ronson

Jon Ronson is an award-winning author, journalist and documentary maker who has contributed to _The Guardian_, _Time Out Magazine_, BBC Television and Channel 4. He has written nine books, including the best-selling novel _The Men Who Stare at Goats_.

