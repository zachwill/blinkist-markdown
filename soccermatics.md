---
id: 57e4f62ed05fcf0003713d8d
slug: soccermatics-en
published_date: 2016-09-29T00:00:00.000+00:00
author: David Sumpter
title: Soccermatics
subtitle: Mathematical Adventures in the Beautiful Game
main_color: 5B8F3A
text_color: 4B7530
---

# Soccermatics

_Mathematical Adventures in the Beautiful Game_

**David Sumpter**

_Soccermatics_ (2016) highlights the link between the world's most popular sport and something slightly less popular — math. These blinks will show you how statistical models can help explain the beautiful game, from strategy on the field to tips for beating the spread.

---
### 1. What’s in it for me? Study the science behind soccer. 

Wherever you are on this planet, the next soccer team probably isn't far away. Like few other sports, soccer inspires the masses and every four years, when the World Cup is played, millions of fans will sit in stadiums and behind television screens to cheer on their favorite team.

It is a sport packed with emotion. However, there is also another, more rational approach to soccer. Over the last several years, scientists have discovered soccer for themselves and have begun to meticulously analyze the game, the players and the viewers — with some remarkable findings.

In these blinks, you will learn about the science behind one of the most exciting sports in the world. You will learn about the mathematical beauty of the game, what makes for a successful team and how to best place your next bet on a soccer match.

You'll also learn

  * about Zlatan Ibrahimovic's knowledge of physics;

  * how players like Cristiano Ronaldo and Lionel Messi baffle statisticians; and

  * why a crowd of soccer fans is a wise entity.

### 2. Successful teams often utilize geometric patterns of movement and establish decentralized passing networks. 

Most of us know that soccer is all about team strategy and that a team's ability to work together determines whether it will soar or plummet. But did you know that math is an integral aspect of these tactics?

It's true. The movement patterns of successful soccer teams often fall into geometric forms. Just take Inter Milan, which during the 1960s used a formation known as the "net." In this strategy, the midfield and defense form a web of connections, making it nearly impossible for an attacker to break through.

Or consider Liverpool FC in the 1970s and 80s, which filled the pitch with right-angled triangles, helping the team pass and move ahead.

And there's FC Barcelona's 2008 team, which invented a strategy known as _tiki-taka_. In this technique, players quickly pass the ball, hoping to force an imbalance in the other team's defense. Tiki-taka also relies on the beauty of geometry because the team uses well-spaced zones for passing, building a network of wide-angled triangles.

But using shapes in their formations isn't all successful teams do; they also form decentralized passing networks. For instance, while analyzing matches in the English Premier League, scientist Thomas Grund paid special attention to the relationship between passing networks and performance. He found that teams that passed the ball between fewer players — in other words, who employed more centralized networks — were on average less successful.

A good example is when, in the 2012 European Championship, Italy went up against England in the quarter-finals. Italy employed a solid passing network centered around the midfielder Andrea Pirlo, but while the Italians were in possession of the ball for two-thirds of the match, they couldn't turn this advantage into goals. As a result, the match ended in a 0–0 draw.

In the end, Italy's passing network was too centralized, allowing the English players to easily block their central path forward.

On the other hand, Spain's national team in Euro 2012 relied on four central midfielders. This decentralized passing network produced greater variability, more striking options and resulted in them winning the championship.

> The first international football match ever played was held between England and Scotland in 1870 and ended in a 1–1 draw.

### 3. Great players intuitively apply and defy scientific laws. 

When most people think of soccer, they think of the stars of the sport; everyone knows about the likes of Messi, Ronaldo and Ibrahimovic. Week after week, these exceptional players give breathtaking performances. But what makes them stand apart from the rest?

In the case of the Swede Zlatan Ibrahimovic, it's a surprisingly accurate sense for Newton's laws of physics. For instance, in a friendly match between England and Sweden in 2012, Ibrahimovic scored an incredible goal. He was 30 metres out with his back to the goal and executed an astonishing bicycle-kick volley that soared over the goalkeeper and ended with the ball in the back of the net.

But what does that have to do with physics?

Well, by using Newton's equations, it's possible to calculate combinations of angles and speeds that will result in a goal. And the number of such combinations that could result in such a goal are slim. So, while Ibrahimovic obviously wasn't doing math on the field, he has an outstanding intuition for physics — which he applies to his sport.

In other words, his jaw-dropping goal was a combination of luck and incredible instincts.

And while Ibrahimovic was utilizing physics, Messi and Ronaldo were offering performances that defy the models of probability. For example, in the 2010-11 and 2011-12 seasons, Cristiano Ronaldo and Lionel Messi successively broke the record for most goals scored in the Spanish Primera Liga.

Up until 2010, the record holder was Hugo Sanchez, who scored 38 goals in the 1989-1990 season. In 2010-11, Ronaldo scored 46 goals, while in 2011-12, Messi netted a clean 50!

Obviously, these numbers are incredible, but they also completely defy the odds. The _extreme value distribution_, a spread of extreme deviations from a value set's average, can be used to determine the probabilities of records being broken, just as it can be used to calculate the chances of the hottest or rainiest days.

But the performances by Messi and Ronaldo since 2010 practically put this system on its head as they each netted a hugely improbable number of goals in single seasons.

### 4. As the old adage goes, the best offense is a stellar defense – and math can make it even stronger. 

Nowadays, scientists can collect and analyze the motion data from sensors in a player's cleats or a goalkeeper's gloves. This is a major boon for tactics and strategies, because this data has a lot to tell us.

In-game data suggests that counterattacks offer the best opportunities to score, and here's how we know:

In 2014, Alina Bialkowski of the University College London studied how often different attack situations ended with a goal. Every tenth of a second, her team recorded different positioning features like how far defenders moved from their standard positions and how many defenders stood between the attack and a goal.

Based on these measurements, the team determined that the likelihood of scoring a goal was highest during a counterattack. In other words, an attacking drive by the opposing team offers the best opportunity for your team to go on the offensive as soon as their attack breaks down.

But beyond that, defending by _pressing_, or constantly putting pressure on the team with the ball, is essential to a team's success. Effective pressing relies on narrowing your opponent's options and coaches employ this technique in different ways.

For example, the Prozone data scientist Paul Power recently studied all the games from an entire Premier League season, looking at two forms of pressing in particular: _counter-press_, which is utilized immediately after losing possession of the ball, and _deep press_, which is done while defending in the third of the field closest to your team's goal.

He found that for an effective counter-press, the first player should move toward the ball 2.3 seconds after the opposing team gains possession, and that a second player should arrive within 5.5 seconds. Once this two-man press is engaged, the other team will usually give up the ball.

For the deep press, the most important aspect is reducing the speed at which the ball is moving toward your goal. To do so, a single defender should approach the ball, while others close the passing channels. Then, after intercepting the ball, you can launch a counterattack.

So, pressing is key, but it's all part of a greater category: collective action. In the next blink, you'll learn why the collective spirit, in general, is such an important factor for soccer teams.

### 5. Good coaches build collective spirit, but the interests of individuals and their teams don’t always align. 

Soccer is a team sport and having lots of superstars isn't enough if they can't work as a team, investing in the collective effort. Successful coaches believe in and instill the collective spirit in their teams, and to do so, they must build an atmosphere in which individuals will readily sacrifice themselves for the greater good.

Just take Louis van Gaal, the renowned former manager of Bayern Munich and Manchester United, who believes coaching is all about building team spirit and self-discipline. And van Gaal wasn't alone in his support for this theory — in fact, it grew to become the predominant soccer philosophy in the Netherlands, where Van Gaal grew up, during the 1970s and 80s.

Or consider the Ukrainian coach Valeriy Lobanovskyi, who understood that a team can be much more than the sum of its parts. With this knowledge in mind, he worked to build trust between his players and synchronize their movements. As a result, he led Dinamo Kiev to success at two Cup-Winners' Cups in 1975 and 1986.

Teamwork is crucial, and nature has something to show us about the value of sacrificing individualism for the team.

Many soccer players tend to be interested in enhancing their own image, because they want to increase their chances of a lucrative future contract. As a result, they might hold off on passing to a teammate who is in a better position so that they can demonstrate their own abilities — even though it might cost the team a goal.

Here's where nature comes into play, because this very conflict occurs everywhere in the natural world. However, animal communities have evolved great ways of dealing with such issues.

For instance, in beehives and ant colonies, all the individuals are genetically programmed so that they act in the collective interest, which is why these insects are often referred to as super-organisms. In a similar way, successful soccer teams, centered upon an ethic of dedicated teamwork, can also act as super-organisms.

> When Celtic won the European cup in 1967, all but one of the team members had been born less than ten miles from Celtic Park.

### 6. Groups are better at making predictions than individuals – but only when each member acts independently. 

Have you ever heard of the _wisdom of the crowd_? It refers to the mathematically supported fact that the average of a group's predictions tends to be closer to the truth than individual guesses. This knowledge has been used to outperform experts in a number of fields, and it is likewise invaluable for making predictions about soccer.

But first, a little bit about how solid the predictions of a group can be. The first experiment testing such performance was conducted by Sir Francis Galton in the early twentieth century. His study consisted of visiting a commercial fair and asking both expert and amateur attendants to estimate the weight of an ox. And guess what? The average of all amateur predictions outperformed those of individual experts.

But why does this happen?

Because errors in the various predictions tend to cancel each other out, bringing their average close to the truth. For instance, in soccer, overly optimistic or pessimistic bets like 7–1 and 1–7 will, when averaged out among a large number of predictions, even out to a more realistic 1–1 draw.

However, the accuracy of the crowd's wisdom drops when individual predictions aren't made independently. In 2011, behavioral biology professor Andrew King and his colleagues invited the visitors at an exhibition at the Royal Veterinary College to judge the number of candies in a glass jar — but they also told the participants the average of all the previous guesses. So, while the new participants had more information, they were for the most part led astray by the previous predictions.

In the end, they relied too heavily on what others had guessed before them and didn't add any new information to the overall group estimate. As a result, the average predictions were farther from the truth than in the initial experiment, in which people independently formed their own ideas.

So, with this information in mind, is there a good strategy for consistently betting better than the crowd?

### 7. Three techniques can help boost your bets. 

It's always a challenge to do better than the crowd, but it's still possible to make a bit of extra cash on bets — and here are a few pieces of advice that can help:

First, only bet when your subjective probability of winning beats the odds. The odds offered by bookies tell you how much money you'll win if a particular outcome occurs; to ensure a fair chance of consistently winning, you should only bet if your perceived likelihood of winning is higher than the bookies'.

For instance, say you think your team has a 60 percent chance of winning. You should only bet if you can win more than 60 percent of what you bet. And since betting services offer different odds, it's important to stay informed about the odds that everyone is offering before placing your bet.

Second, don't rely on any individual expert. After all, experts don't always do better than your average Joe.

As an example, a simple yet effective way to predict the next season's final league table is simply to stick with last year's rankings. The average change in positions across major leagues over the last five seasons was just 2.92. And, of the 17 pundits who published predictions for the English Premier League, only five performed better than this simple strategy in 2014-15 and only one did better in 2013-14.

Finally, be sure to employ several strategies that simulate the wisdom-of-the-crowd effect. For instance, you can average the opinions of several experts, use the standings from the previous year or average the number of goals scored by the teams in this season.

A good idea here is to weight your different strategies according to how well they do. In other words, give preference to strategies that have performed consistently well and use those that performed poorly less often.

With a little luck and perseverance, maybe you too can outperform the crowd and the betting market.

### 8. Final summary 

The key message in this book:

**While soccer might appear to be all about chaos, creativity and passion, there are plenty of ways that mathematical and statistical regularities can shed new light on this beloved sport. Whether it's applied to game strategy or your bookie, mathematics has something to offer every soccer fan.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Beautiful Game Theory_** **by Ignacio Palacios-Huerta**

_Beautiful_ _Game_ _Theory_ (2014) shows us how applicable economics is to our daily lives by looking at the fascinating world of professional soccer. By examining compelling statistics and studies on shoot-outs, referee calls, and ticket sales, _Beautiful_ _Game_ _Theory_ offers some interesting insights into the psychology of behavioral economics.
---

### David Sumpter

David Sumpter is an applied mathematician and a professor at the University of Uppsala in Sweden, where he leads the collective behavior research group. In his spare time, he coaches his ten-year-old son's soccer team.

