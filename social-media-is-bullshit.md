---
id: 53736c0764343600070b0100
slug: social-media-is-bullshit-en
published_date: 2014-05-13T13:21:01.000+00:00
author: B.J. Mendelson
title: Social Media is Bullshit
subtitle: None
main_color: 2585BB
text_color: 2072A1
---

# Social Media is Bullshit

_None_

**B.J. Mendelson**

_Social Media Is Bullshit_ puts a damper on the hype around social media by unveiling the economy behind it and addressing the commonly held misconceptions about the value of social networks for business. While social media can be a valuable asset for certain companies, it is _not_ the cure-all that marketers and social media gurus would have you believe!

---
### 1. What’s in it for me? Learn not to get fooled by the hype around social media. 

Social media has become ubiquitous in recent years. Be it a "find us on Facebook and Twitter" button on every product or commercial, or its role in major social upheaval like the Arab Spring, social media often finds its way into the news.

Not only that, but there is a growing number of products and people who aim to offer you or your business the secrets to unlocking the power of social media. Furthermore, they'll tell you that social media is _indispensable_, and that without it you're simply missing out on potential profits.

So you probably have been intrigued enough to try your luck and get in on social media platforms to promote yourself, your product or your business. All those books by so-called experts told you to do it, so it is important to do, right? WRONG!

In these blinks, you'll learn:

  * why you do not _need_ to have an active social media account for your business

  * why total accidents can lead to huge record deals

  * how marketers breathed life back into their businesses just by inventing new words

  * and finally, how marketers invent their expertise out of thin air just to sell you some bullshit.

### 2. The social media revolution in society and business is a myth. 

In recent years, social media has been hailed as revolutionary and as a marketing tool that no entrepreneur or business can neglect if they want to be successful. But are these assumptions about social media really true?

Let's start with the fact that most social media outlets are actually owned by corporations. Sure, it's true that there was a time where we more or less had an equal voice on the web, and you could use your blog, for example, to independently gain access to an audience and be heard. But today the majority of the most popular blogs are owned by media conglomerates like Google, Time Warner or AOL.

In fact, most sites with user-created content are actually _exploiting_ their users: people create content _for_ _free_ for sites like the Huffington Post, which the big corporations then turn to _profit_ through advertising.

Furthermore, there's nothing too new or revolutionary about social media. In fact, most of these "new" sites are simply reiterations of older ideas: before Twitter, there was AOL Instant Messenger; before Facebook, there was Classmates.com; and even Google has been around since 1999.

And they're no more "revolutionary" than they are new. There is no real difference in the way we interacted with media before the advent of social media and what we do now: we tweet about what's happening on TV, not the other way around.

Moreover, all the hype around social media sounds a lot like a pitch for a get-rich-quick scheme: just use social media and soon you'll be a millionaire, too!

Social media is not a revolutionary marketing tool, but a handful of success stories where someone like Justin Bieber was lucky enough to get rich after becoming a YouTube sensation make it easier for _Marketers_ to sell us the "secrets" of making money on social media, and for _Analysts_ to sell their advice to businesses.

### 3. Social media platforms do not create overnight miracles. 

Surely you've heard of so-called "social media success stories," where Justin Biebers are discovered and then land a record deal. But how realistic is this?

It's still really difficult to reach a big audience even if you use social media. In fact, without a budget, insiders, or networks, it's almost impossible.

Consider YouTube, for example: in theory, anyone can post videos that reach millions of people, right? Wrong.

Generally, there are three basic groups of posters on YouTube:

  * celebrities, such as musicians or comedians, and well-established companies,

  * original creators, such as the BarelyPolitical channel, who are often prolific users that have been on the platform for a long time and have a well-developed network with other users,

  * and "normal people" like you and I. This group creates about 70 percent of the content, yet gets almost _none_ of the traffic.

Knowing this, it should come as no surprise that cases of "social media success" involve factors other than mere exposure on a platform.

While every so often some people _do_ become famous through their YouTube channels, their success is really due to luck and external factors, and not to the platform itself.

For example, Justin Bieber _did_ get famous after posting videos on YouTube, but only because a music producer stumbled upon his videos and used his own resources and networks to promote him further. It's unlikely that organic views and likes by regular users alone would have led to the success that Bieber enjoys today.

In other cases, people have become famous on social media because the platforms themselves promoted them.

For example, YouTube used to feature hand-selected videos on its front page, and these videos obviously gained huge amounts of traffic. Twitter, too, used to provide users with suggestions about which celebrities to follow, thus giving those accounts hundreds of thousands of followers.

Nevertheless, such examples will be sold as "social media success stories" by marketers, as if those successes were somehow replicable.

### 4. Social media platforms do not necessarily benefit businesses. 

If you're an artist, entrepreneur or want to promote yourself for other reasons, then you have probably noticed all the buzz around marketing on social media. So should you get in on it too? In short: probably not.

First, when you engage with people on the web, then you aren't engaging with _everybody_.

A good rule of thumb is that only one percent of web users are active — i.e., retweeting, commenting, liking or otherwise participating in social media. Thus, when you market a product on social media, you will only interact with same small cross-section of users and lose out on the rest of the market.

It's impossible to measure how many of the remaining 99 percent actually "listen" to what you have to say. In other words, social media offers no guaranteed audience.

In addition, most platforms' services are not useful for promoting you or your business.

For example, while LinkedIn might be useful for the corporate and professional workforce, and Facebook is great for friends and family connections, neither is great for promoting businesses.

There is simply no proof that more attention on the web translates into more business. In addition, you can't tell how many "likes" came from people who would have bought your product anyway, regardless of whether you had a social media presence.

What's more, there is no proof that a connection exists between online presence and sales. In fact, even hugely popular companies like The Gap or Banana Republic weren't successful in selling products to their fans via Facebook.

Moreover, marketing on social media is _expensive_.

Even if joining those platforms is free, it still costs money to create informative or amusing content, and to pay the workforce that maintains those channels.

And on most social media websites, that money is simply wasted. This might not hurt big companies with enormous marketing budgets, but it's critical for smaller business and private persons who must be prudent with their limited resources.

It would seem that the assumption that social media is _indispensable_ to the success of your marketing campaign is little more than a myth — in a nutshell, it's bullshit. The following blinks will investigate where this myth comes from.

> __

### 5. Books by marketing gurus are not applicable to your own projects. 

Have you ever flipped through a book on marketing and noticed that all the examples were corporate success stories, or techniques from well-established brands? That's no coincidence.

The primary clients for marketers who get paid for advice on how to promote a company or market a product are big corporations.

This is because corporations are where the money is: it's more likely that a publisher will find a potential audience for their marketing books among corporations and corporate employees. Once published, the author will make more money selling to corporations and their staff than from other audiences.

Yet these books are still marketed to the masses — people like you who want to know how to promote their own work.

But this isn't because their advice will actually prove helpful for you, but rather because a marketer who gets on the _New_ _York_ _Times_ bestseller list earns more credibility than other authors, and thus can use their prestige to make _more_ money — for example, as a public speaker at things like corporate seminars.

If an author said, "I wrote this book as advice for General Motors," they probably won't be making too many sales, as only a narrow niche would be interested. So instead they broaden their focus and market to everyone.

However, the tools and advice in those books — which might work for huge corporations — won't work for you.

Large companies have a big marketing budget, workforce and experts, as well as an established brand and online presence. As a small business owner, or an artist seeking to promote yourself, you don't have any of that! You simply can't use the same advice that a megacorporation can, because your circumstances are fundamentally different.

For example, social media expenditures are often calculated as a budget loss; do _you_ have room in your budget for a planned loss? Clearly, this is one area where you shouldn't even try to emulate their tactics.

### 6. Marketers benefit from the myth of social media – a myth they create and actively perpetuate. 

Surely you've seen or read about so-called "experts" on social media. They are bloggers, authors and consultants who promote various social media platforms and strategies, AKA marketers. But why exactly do they promote social media so vigorously?

By claiming that social media is an indispensable marketing tool for all businesses, marketers can then sell their "expertise" to a broader range of clients.

Usually, you can't give people generalized market advice, as every good marketing decision should take into account the exact circumstances and opportunities of a particular business.

For example, TV commercials might work for large clothing retailers, yet be a waste of money for a one-man tailor shop.

But by creating a myth that social media platforms benefit all businesses in the same way, these "experts" also increase the size of their _own_ markets!

In addition, marketers trick their customers into thinking something new is happening by taking existing internet phenomena and describing them with new buzzwords.

Take "Web 2.0," for example. This buzzword was promoted by marketers after the dotcom bubble burst, when investors were wary of investing in the web because they had lost so much money. But the term "Web 2.0" sounded like something new and different, and thus opened up their wallets.

In reality, "Web 2.0" is just the same ol' web where content is created and shared, and _not_ something completely novel.

Furthermore, marketers create new marketing opportunities for themselves by building upon the myth of social media.

For example, when no one knows what buzzwords like "Web 2.0" or "social media" mean, then marketers help fill the information gap that they themselves created by speaking at conferences and seminars, or by writing books or offering consultations.

"Social media" or "Web 2.0" are only two examples of many; marketers will invent new buzzwords to increase their sales when things get quiet. What will they come up with this year?

### 7. Marketers spread false information and establish their expertise for their own profit. 

Wonder how self-declared social media "experts" end up being interviewed, get invited to give speeches, asked to do consultations, and so on? In fact, there is a whole system that experts exploit to make money from myths like social media.

For starters, marketers establish their "expertise" in areas that they themselves insist are important.

If you want to do the same, then just follow this simple formula.

First, create a professional homepage where you can present your myth. For example, your myth could be: "Twitter will decide the next election."

Next, start talking to people in your field or a related one, like political bloggers, and do some superficial research on your bullshit. Try reading a few articles about media influence on voting, and make sure to answer any blog posts and write comments to make yourself and your myth seen.

Then contact some media organizations. The media is usually uncritical of things like marketing buzzwords, so they will quickly get on-board if your topic attracts a large enough audience.

If you start seeing your myth in the media, then you are the de facto "expert." You're now on the path to working as a speaker and consultant, where you could talk about something like "campaigning in social media," and then publish a book about peer pressure on Twitter and watch the cash roll in.

Once that well runs dry, you simply fabricate new information about your myth, or create a new one altogether so that your audience doesn't lose interest.

And it's _so_ _easy_ for marketers to get away with selling bullshit.

Marketers have zero accountability. They don't have to answer to their clients for the lack of results; they can just move on to other companies and cash in again.

The inevitable bad reviews online won't even tarnish their reputation: they can just hire people to write nice reviews for them. Problem solved!

### 8. The platforms themselves also benefit from and contribute to the social media myth. 

Have you ever "liked" a company on Facebook? Probably, but did you ever _buy_ their products because they were well represented on Facebook? Probably not. So why on earth do companies think social media is a good way to sell their products?

To answer this question, we should learn a bit about these platforms.

Social media platforms profit from traffic. For example, the more traffic Facebook receives, the more money they can ultimately charge for ads on their site. In fact, advertising comprises 85 percent of Facebook's revenue.

Consequently, your presence on Facebook benefits Facebook more than you — _you_ put in the work for a professional page while _they_ benefit from the traffic. Rather than going to your homepage off Facebook, your fans and customers land on your Facebook page, thus generating traffic — and therefore ad revenue — for Zuckerberg.

And it costs money to maintain your ad campaign; that content doesn't generate itself, and no one works for free. Wouldn't you have gotten more out of that traffic if users had simply gone to your own homepage?

Because they directly benefit from your presence on their sites, it is only logical that social media platforms _themselves_ would help spread the myth that they are a useful channel of promotion.

Facebook, for instance, actively promotes the idea that their site is a useful channel for promotion. They once released a study claiming that the Kia motor company's Facebook presence had increased their success, measuring such things as "awareness" and "perception."

People believe this growing awareness and perception translates into more sales — however, it's difficult to measure what effects actually resulted from Kia's Facebook presence: Did anyone actually buy a Kia _because_ they saw them on Facebook?

Social media platforms want us to believe that "likes" mean "sales" and that followers are equivalent to faithful customers; however, there is simply no way of knowing how many sales actually result from social media presence — if any.

### 9. There is an economy behind spreading web-related myths. 

Have you ever had the feeling that, whenever there is a new fad, it's all you hear about? That businesses, books, and media all jump onto the bandwagon? Well, you may be right: there's an entire system used to spread web-related trends.

The _myth_ _circle_ often begins with some new online platforms that are being hyped by the media and self-proclaimed cyber fans and experts. Let's look at Twitter as an example.

Once it has been created and successfully hyped, marketers will quickly come to understand the technology and tell their clients that they _must_ establish a Twitter presence and invest in marketing campaigns on that platform.

Next, these marketers will create a buzzword out of the trend, thus further hyping it and exploiting the hype for profit. In this case, that buzz word is "social media."

Next, industry analysts and consultants will repackage what marketers have said in order to resell it.

For example, they might sell a report on how using Twitter can benefit their clients' businesses after having read blog posts written by marketers, essentially doing little more than repackaging information spread by marketers and then reselling it as their own work.

Finally, corporations will spend their money according to the advice given to them by analysts and consultants, thus completing the circle.

Big corporations have the marketing budget to allow them to invest big in a high quality, active Twitter account, if a consultant tells them to do so.

This in turn leads to more media reporting on Twitter, which then recreates a demand for the books and services of the marketers, who helped create the myth in the first place!

The problem with all this, of course, is that not all social media technologies turn into success stories; for each Twitter, there are _hundreds_ of unsuccessful new tech companies. For example, do you remember FriendFeeds, that once upon a time was said to have the same potential as Facebook? No one else does either.

> __

### 10. The old marketing tools still apply today. 

Despite what social media experts say, marketing functions the same way it always has, and the same tried-and-true marketing tools still apply.

If you look behind all the social media buzz, good business still boils down to these basic principles.

First, no matter what you are promoting, you rely on real offline connections, luck, support and other external factors in order to succeed.

If you organize a charity event, for example, and invite both people you know _personally_ as well as Facebook fans that you hardly ever see, who is more likely to show up to your event? Obviously your personal contacts.

Second, you need to be in the media spotlight.

You just can't get anywhere in marketing without the media. A good place to start is always local media, so contact them directly to get their attention and maybe even some coverage.

For example, your charity event should feature something like a fund-raising dancing contest as a way to get people interested in your cause.

Third, if you're working with a small budget, then never hire anyone for something that you could do yourself.

For example, if you aren't a PR expert, you can still learn about it and start promoting your charity event yourself without spending the money on a PR manager right away. Save that for when you have a bigger budget.

And finally, as a small business owner, focus solely on the customer, _not_ on social media.

For smaller operations, the only thing that matters are sales. This means that it's important to satisfy the people who actually buy your product, and _not_ to collect likes or online followers.

One way to optimize your service, product or performance is to conduct surveys on your customers to find out what to improve or change. Looking back to the charity ball, one thing to do would be to ask people to rate the event or tell you how they heard about it.

For a small business, keeping the customer happy is far more important than social media!

> __

### 11. Social media will not make or break your business. 

Even though marketers and media want to convince you otherwise, your success does _not_ depend upon whether you are active on social media platforms. While social media might not hurt you, you should also remember the following.

Most businesses don't actually _need_ a web presence other than their homepage. For many businesses, it's better to show the customer that you care by being available in person — or at least over email — rather than having them interact with your company through a social media account.

Only use those platforms if doing so actually serves your business and if your audience is there. Ultimately, only _you_ can know what suits your business, so don't follow the generic advice of self-proclaimed marketing gurus.

Remember too that marketers will say that you _must_ market on social media platforms or else your business will suffer — but this simply isn't true.

In places like New York, Los Angeles or other metropolitan areas, where many people actually do use social media, then it might be worth it to join.

But for a regular business in a small town, it's just a waste of time and resources to try and create a presence on these platforms, since your customers probably won't read your material.

Good marketing always takes into account your specific circumstances and customers. For example, if your customers are over 50, then you simply won't reach them using social media.

And in the event that you _do_ decide to go on social media, don't let the critics get you down — they are simply unavoidable.

You will definitely attract criticism on social media, but remember that they only represent a small part of your market. Don't waste time getting into debates with critics online. What matters is the quality of customer service for your _real_ customers, not satisfying anonymous critics.

> _"Twitter, Facebook, and YouTube are just platforms. They're not good or bad, useful or useless."_

### 12. Final Summary 

The key message in this book:

**In spite of what the gurus say, while marketing on social media platforms might not hurt your business, avoiding them won't hurt you either. The traditional marketing methods, like developing a network of _real_ _people_ and being frugal, work fine for most businesses.**

Actionable advice:

**You don't _have_ to use social media.**

Only use social media platforms that suit your cause. If your primary audience isn't comprised of active social media users, then engaging in social media might just be a money sink for your operation.

**Build connections offline.**

If you are hosting an event, and you invite both people you know _personally_ as well as some Facebook followers, who do you think is more likely to come? When it comes to getting something done, personal connections will always prove far more useful than any followers or likes on social media platforms.
---

### B.J. Mendelson

B.J. Mendelson is an author, comedian, and advocate for breast cancer awareness. He has also contributed to CNN, Forbes and the Huffington Post.

