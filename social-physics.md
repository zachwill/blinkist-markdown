---
id: 548f033a6532350009ef0000
slug: social-physics-en
published_date: 2014-12-30T00:00:00.000+00:00
author: Alex Pentland
title: Social Physics
subtitle: How Good Ideas Spread: The Lessons from a New Science
main_color: 27823B
text_color: 1F692F
---

# Social Physics

_How Good Ideas Spread: The Lessons from a New Science_

**Alex Pentland**

_Social Physics_ offers a crash course in the new self-declared field of science. It asks questions about how we can best use the treasure troves of data available to companies and researchers today to better understand human interaction and social organization, and hopefully create a better society.

---
### 1. What’s in it for me? Learn how the data we generate everyday can be used to unlock the secrets of human interaction. 

Every time you call someone on your mobile phone, surf the internet, download an app or use your travel card, you generate data. And if you think about how many of us across the world do each of these things everyday, that's an awful lot of data!

For social scientists, this information on where you've been, who you've talked to and what you've bought is incredibly useful. If it is carefully collected and analyzed, the data can teach us everything we've ever wanted to know about how we behave.

These blinks provide some insight into what the scientists have found. Read on and you'll discover the secrets of human interaction and how we can design a society which perfectly suits us.

In these blinks you'll learn

  * what the perfect society looks like;

  * the power of peer pressure; and

  * what makes Zurich the best city to live in.

### 2. Social physics uses the huge amount of data available in our modern world to analyze human behavior. 

We're all familiar with the concept of "physics" and "social science." But have you ever heard of _social physics_?

Social physics is an approach to understanding human behavior — one that aims to find out what we're _really_ doing, and not what we _think_ we are doing.

Social physics takes one huge idea from traditional physics, namely the idea that the flow of energy translates into changes in motion. Think of how, if the wind blows hard enough, it can pluck an apple from a tree, for example.

We can apply this concept to human behavior as well, by analyzing how the flow of ideas and information — much like energy — translates into changes in human behavior.

Unlike traditional approaches to understanding human society, however, social physics doesn't use abstract models of human behavior. Instead, it uses _living labs_.

Instead of using one-time surveys and lab experiments that analyze a particular element of behavior, social physics uses data from every facet of our behavior over a much longer period of time, often spanning years.

Social physics makes good use of _big data_, i.e., a huge collection of information, which can then be analyzed for patterns. Some of this data takes the form of "digital bread crumbs." Our credit cards, for example, show where we buy goods, cell phone data shows where we are and even who we're talking to.

Another tool in social physics' arsenal is the _sociometric badge_, devices worn around the neck that capture data about our body language during interactions.

In analyzing this huge collection of data, patterns emerge about an individual's or a community's life — a process known as "_reality mining_." And behind these patterns lies the truth about human behavior.

### 3. Social physics has revealed that our peer groups heavily determine our behavior. 

Would you consider yourself to be pretty independent? You probably make your own decisions based on your own personal, rational thoughts, right?

Most of us believe this to be true, likewise most believe that society is comprised of individuals who make rational choices based on self-interest.

Social physics, however, reveals that things are much more complicated.

In fact, our behavior and decisions aren't so individual after all, and are heavily determined by the _peer groups_ — i.e., groups of people based on shared interest, vocation or other characteristics — to which we belong.

We often belong to many peer groups at once. For example, you could be a football-loving (one peer group) police officer (another) who dropped out (another) of a particular high school (yet another).

Peer groups aren't the same as your friends. Although many of our friends may come from our peer groups, the majority within those groups will be little more than passing acquaintances.

Nevertheless, we spend a great deal of our time at the same bars, cafeterias, sports stadiums and other social environments as those in a particular peer group, and thus become heavily influenced by the numerous interactions we have with those around us.

Take this experiment conducted by the author, for example, which showed that we tend to share certain beliefs with those in our peer group.

In the experiment, a group of students were given modified smartphones which tracked their communication and their social environments leading up to the 2008 US presidential election.

They were asked a range of questions about their political views before the election and then finally were asked for whom they voted, after the election. 

The study found that the students' political leanings didn't come from those that they directly talked politics with, but rather from their wider peer group.

In short, their beliefs came from observing others and overhearing conversations within their peer groups.

Clearly, we have much greater influence on one another than we might otherwise think. Our following blinks explain why.

### 4. Adopting the behaviors that we observe to be beneficial helps us to thrive in social groups. 

Having just read about how heavily influenced you are by other people's behavior, you might be thinking: "Yeah right! I saw two people fighting at my favorite bar the other night, but I didn't jump in just because of peer pressure!"

Well, good for you! But that's really nothing to brag about. In order for behavioral change to spread across a peer group, it must match these two criteria:

  * First, it needs to be repeated often over a short period of time; and

  * Second, it needs to demonstrate a positive, beneficial outcome.

If those criteria are met, then the chances are greater that the behavior will be influential.

Imagine, for example, that during coffee breaks at work, everybody in your team stops drinking coffee in favor of tea. Likely, you'll pick up the new behavior as well, since it both happens every day and will benefit you by winning the approval of your coworkers.

By the same token, fights won't break out every night at your favorite bar (though they might, depending on the bars you frequent), and getting into bar-fights yourself will likely not demonstrate a positive outcome.

So why are we prone to adopt behavior in this way? It's likely that we evolved this way in order to succeed in social groups.

When, for example, capuchin monkeys are deciding where their troop should move, the monkeys at the head of the group will call out when they find a path. The monkeys behind will copy their call in turn to communicate the message to the rest.

We still rely on this phenomenon today to determine our behavior. Indeed, aping the positive behaviors of others helps us to get by.

Observing decorum at a party, for example, influences us in a way that keeps us from behaving like rude buffoons, and to instead act politely. These influences thus help society accept the norms that help it run smoothly.

### 5. Social physics has shown that social incentives work much better than individual ones. 

In 2009 a department at the US Department of Defense celebrated the 40th anniversary of the creation of the internet by creating what became known as the _red balloon challenge_ :

Ten red weather balloons were hidden around the United States, and teams had to utilize the internet and social media to discover their locations. The winning team would win $40,000.

Of the 4,000 teams who signed up, most tried to recruit help in their search by utilizing traditional methods of incentivizing people, doing things like offering rewards whenever people could provide the locations of one or more balloons.

The assumption was that people work best when you appeal to their self-interest.

The author and his team, however, took a completely different approach. Through his work on social physics, he had come to realize _social incentives_ — the ones based on our social networks (our friends, peer groups, etc.) — work much better than individual ones.

Rather than giving people an individual incentive (for example, by offering $3,000 for finding a balloon), they offered incentives for people to recruit help.

So, for example, if you find a balloon, we'll give you $2,000. If you told the balloon finder about the challenge, you'll get $1,000. And if you told the person who told the balloon finder about the challenge, you'll get $500.

This incentivized people to spread the message about the balloon, thus recruiting 2 million people across the world to join in the search — far more than the other teams did.

And, of course, his team won; motivated by social incentives, they found all the balloons in under nine hours.

### 6. Freely flowing ideas and community engagement are crucial for innovative, productive societies. 

The question of how to best organize our society is one with which humanity has been engaged since the dawn of civilization. Today, we generate a huge amount of data from a wealth of resources, which gives us some deeper insights into what types of society work best.

Social physics has shown two factors can make for a more productive, harmonious society:

The first of these factors is _engagement_, the interactions between people from all areas of society where ideas and social norms are developed and strengthened.

Engagement happens when people meet at the shop, the theater, the workplace or anywhere else. All that matters is that everyone can interact; engagement can't work without some sort of social group.

The second of these factors is _exploration_, i.e., when people are able to seek out ideas from a diverse range of people. The benefit of exploration is that it constantly introduces new ideas and norms to the social group, thus allowing them to better mix and develop.

Societies need to be able to balance these two factors in order to thrive. A society where there is too much exploration and not enough engagement is one in which people don't interact enough within their communities.

Although they may have access to a greater range of ideas, they lack the social cohesion granted by shared values, and thus lack trust in one another. Indeed, these societies suffer higher crime and alienation as a result.

On the other hand, too little exploration leads to a lack of innovation and creativity, as people don't have access to those who think differently.

But how can we ensure that our society maximizes the benefits of engagement and exploration? In our following blinks, we'll look at how to find the perfect balance between the two.

### 7. Social physics can show us what type of city will produce the best environment for innovation. 

In the previous blink, you learned how societies that balance exploration (the flow of ideas between different groups) and engagement (strong interactions between people) offer the greatest potential for innovation and productivity.

But how can we ensure that our societies offer both?

In truth, it's extremely difficult to get the balance just right. In fact, an imbalance between exploration and engagement can be found in many modern cities.

Some cities, for example, are huge metropolises, in which people travel far and wide to go to work, go to school and engage in cultural activities. Yet, although a larger population means greater access to different ideas, people spend less time interacting with those in their own communities. As a result, crime and antisocial behavior are much higher in the cities.

In smaller towns, however, people will spend much more time in the company of their neighbors. Here, engagement and trust are much higher, but they also have a harder time finding the alternative ideas that are needed for innovation.

Social physics shows us that a utopian city would be a combination of the two.

The ideal city would combine a central hub, packed full of diverse cultural institutions and places of work, with a highly developed transport infrastructure connecting satellite towns where people would know their neighbors well and have plenty of opportunity to engage with them.

The city of Zurich in Switzerland offers a perfect model:

The majority of people in Zurich live in the cheaper towns and villages surrounding the city. From there, they use a fast, reliable and inexpensive public transportation system to get to and from the busy and cosmopolitan center. The result is a metropolitan area where people have a high level of engagement while also having equal opportunities for exploration. In other words, it's the perfect mix.

### 8. The most productive teams are those in which members can closely interact with their counterparts. 

As we've seen, engagement and exploration are critical in developing societies that function at their very best. Interestingly, these same principles also apply to smaller groups, such as teams at work.

Indeed, the most productive teams are those in which people are encouraged to interact and engage.

This interaction is critical for a fairly straightforward reason: while an individual might be great at coming up with revolutionary ideas, these ideas are meaningless if they aren't shared and discussed with the colleagues who can help put them into action.

For this reason, it's absolutely important that team members have the opportunity to regularly come together and exchange ideas and information.

When you share your opinions with others, you give them the chance to comment on or develop them. It is through this process that your good ideas become great ideas.

The author verified this in a study he conducted with call center workers at Bank of America.

He and his team analyzed four teams of 20 workers for a period of six weeks using sociometric badges. In their analysis, they found that the productivity of each team (measured by the average call handle time) directly correlated with the amount of time the team members spent interacting with one another.

Creating an engaging environment doesn't have to be a product of chance. In fact, the author advised Bank of America to make specific changes in order to encourage greater engagement. 

For example, rather than staggering coffee breaks for the teams, such that each member took their break at different times, he advised them to organize a _single_ break period for everyone.

The results were hard to believe: average call handle time decreased so much that the company forecasted $15 million in annual savings if they would expand that policy nationwide.

All of these discoveries have been possible because of big data. In our final blink, we'll look at the future of data collection.

### 9. We need to fundamentally rethink who owns the data we create. 

You may or may not be aware that _everything_ you do online leaves a digital footprint. Whether it's the books you buy on Amazon, the transactions you make with your debit card at the gas station or logging into online banking, your digital trail can be found everywhere.

Despite this being "your" data, private companies are able to profit from it by selling it to advertisers. You have no say in this whatsoever, and this has to change.

We need a _new deal on data_ — an acknowledgement that the data that _we_ produce we likewise _own_.

There are three aspects to this deal:

  * First, all the data you generate belongs to you;

  * Second, you have complete control over what happens to this data. This includes whether it is shared and with whom; and

  * Third, you have the right to have your data removed or destroyed altogether.

In this new deal on data, it's equally important that we, along with protecting our data, also allow for it to be used productively.

As you've seen throughout these blinks, there is much to be discovered by using the data that we create every day. It's therefore vital that people are encouraged to continue to allow their data to be used for productive projects in some way.

One solution might be to anonymize data, i.e., making sure that the privacy of the person from whom the data originates is preserved. Hopefully, anonymity would encourage people to share their data even if they don't have to.

Another way could be to create strict regulations regarding the use of data that are just as stringently enforced.

Thankfully, we've already witnessed developments in this area. For instance, after pressure for regulatory agencies, Google produced _Google Dashboard,_ whereby users can easily see what types of data the company collects.

While this is a nice start, there's still a long way to go.

### 10. Final summary 

The key message in this book:

**Social physics brings together social science and big data to help us better understand human interaction and society. While there is potential for the data to be used to create a world ideally suited to its users, we need to actively set standards of data usage and privacy.**

**Suggested further reading:** ** _The_** **_Second_** **_Machine_** **_Age_** **by Erik** **Brynjolfsson** **and** **Andrew** **McAfee**

_The_ _Second_ _Machine_ _Age_ examines how technological progress is drastically changing our society, and why this development is not necessarily positive. It compares the rapid development of computer technology to the advent of the steam engine, which once catapulted the world into an Industrial Revolution.
---

### Alex Pentland

Alex Pentland is director of the MIT's Human Dynamics Laboratory, and is a founding member of numerous start-ups, institutes and labs, including MIT's Media laboratory. He has published many articles in various scientific magazines and has published one other book, _Honest Signals_.

