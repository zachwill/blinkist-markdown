---
id: 5c1124576cee07000793582c
slug: socialism-en
published_date: 2018-12-14T00:00:00.000+00:00
author: Michael Newman
title: Socialism
subtitle: A Very Short Introduction
main_color: DFC780
text_color: 786B45
---

# Socialism

_A Very Short Introduction_

**Michael Newman**

_Socialism_ (2005) is a dash through the history of the term after which the book is named. Socialism has played an important role over the past 200 years of human history, but its original goal of achieving an egalitarian society has, in recent decades, been somewhat forgotten. This book is a thorough tour of socialism's history. It's also an exploration of the various ways the word has been implemented and a guide to ways we might use it in the future.

---
### 1. What’s in it for me? Discover what socialism is, and what it isn’t. 

The last few years have witnessed a revival of the term "socialism," which, mere decades ago, inevitably conjured images of dictatorship and oppression. But recent events — the financial crisis of 2007–2008, the Great Recession and Bernie Sanders's 2016 presidential campaign, to name a few — have ushered the word back into public discourse. This resurgence raises an important question: What, exactly, do we mean when we say socialism?

The truth is that socialism was developed, not as a means of oppression but of emancipation. Back in the nineteenth century, the world was changed forever. The industrial revolution forced millions of people into dirty, overcrowded cities. This impoverished urban population had little power or control over their work, and, especially when compared to those at the top, they received a pittance for their toil. In this new, alienating world, philosophers and thinkers began dreaming of a better way to organize society. They imagined a system based on equality and fairness, not oppressive moneygrubbing and productivity at any cost.

Their socialist ideas didn't always lead to where they thought they would. The twentieth-century socialism of Stalin and the USSR is a clear example of this. But socialism exists in many forms — just consider the liberal social democracies of Sweden and Western Europe. Socialism remains a difficult concept to pin down, but these blinks give you an idea of what it means, and why it remains as relevant as ever in the twenty-first century.

In the following blinks, you'll learn

  * what surplus value is;

  * what Sweden and Cuba have in common; and

  * why socialism remains more important than ever.

### 2. Although there’s no single definition of “socialism,” the forms it’s taken over the years share a number of characteristics. 

What do you think when you hear the word socialism?

For many, the term "socialism" is often reduced to its most infamous incarnation — the long-lived USSR and its brutal dictator, Joseph Stalin.

However, the USSR's Stalinism is far from the only form that socialism can take. Indeed, socialism has taken many forms since it first arose in the nineteenth century. Consider the huge difference between Cuba and Sweden. The former is a communist single-party state; the latter, a parliamentary social democracy. Although the two nations are a world apart politically, each has a system of government that stems from socialist ideals and is rooted in a number of common socialist characteristics.

The most overarching principle guiding all forms of socialism is the goal to create an egalitarian society. And although various forms disagree on how to achieve this, they all agree that society's inequalities in income and power stem from the economic system that dominates the world today: capitalism. To varying degrees, socialists of all stripes have sought to remove the structural barriers put in place by capitalism — specifically, the ownership of capital by a small elite — so that all members of society can prosper equally.

Beyond this overarching characteristic, socialists of all shapes and sizes believe in the _possibility_ of creating an egalitarian society founded upon human solidarity. This optimistic belief is rooted in a fundamental assumption: that human beings are by nature cooperative, not competitive.

But how would such an egalitarian system be brought about? Well, all socialists concur that such a system must come about via _conscious human agency_. It won't happen naturally; human action is required. In other words, if people want change, they must be the agents of that change.

Now that we have an idea of what binds socialists together, let's delve into how the ideology developed. It's time to head back to the nineteenth century, a time of industry and progress.

### 3. The growth of capitalism in nineteenth-century Europe gave birth to socialism, and Karl Marx became its most famous proponent. 

In the early 1800s, huge leaps in technology led to the industrial revolution. Across the West, new factories and industries sprang into being. As a result, masses of people who'd previously lived in cooperative, rural communities were crowded into poorly planned cities. The conditions were dreadful, cramped and filthy. To survive, residents were forced to compete with each other for factory jobs that paid measly wages.

Many welcomed these changes, hailing them as signs of progress. Others, angry at the extreme poverty and inequity inherent to this new industrial system, were less pleased. These disillusioned thinkers and activists gradually banded together under the banner of "socialism."

But it wasn't until two socialists, Karl Marx and Friedrich Engels, came together in 1843 that socialism began to grow exponentially.

Marx's philosophy of _class struggle_ played an especially large role in the development of socialist ideology. He believed that capitalism had divided people into two classes: the _bourgeoisie_ and _proletariat_. The bourgeoisie were the owners of capital, such as factories and industrial machines, and to run these factories and machines, they relied on labor. That labor was provided by the proletariat.

Marx argued that it was only by exploiting proletarian labor that the bourgeoisie were able to make profit. For example, say a worker is paid a wage of 100 dollars a day — enough to pay his rent and feed his family. In the first four hours of work, the worker will probably produce goods equaling 100 dollars in value. And during the second half of the day, as he continues to work, he will produce another 100 dollars.

This additional amount is what Marx called _surplus value_. The foundation of the "class struggle" between the proletariat and bourgeoisie was over the question of the _rate of surplus value_. The bourgeoisie wanted to increase surplus value — the majority of which ended up in their pockets — whereas the proletariat sought to decrease it by petitioning for higher wages.

As a consequence, the bourgeoisie and proletariat are locked in a constant, ever-worsening struggle. Marx believed this class struggle would eventually bring down capitalism entirely and usher in a more equal society.

When Marx died, in 1883, socialist parties espousing Marx's theory of class struggle were quickly spreading throughout Europe. However, not everyone approached his theory in the same way. By the end of the century, many of these parties had splintered and taken on new forms.

### 4. Near the turn of the twentieth century, socialism was mainly divided into two diverging schools of thought. 

By the end of the nineteenth century, all mass socialist organizations essentially agreed on the importance of reducing — or abolishing — economic inequality and of promoting human cooperation and solidarity.

But a gap in Marx's philosophy led to growing disagreement among socialists about exactly _how_ to implement socialism.

Marx himself hardly touched upon the topic. He knew that societies at different levels of development would require different approaches to implementing socialism. So he left the question open.

By the dawn of the twentieth century, two main answers had emerged.

The first of those answers was provided by the reformists. They espoused the belief that socialism could be brought about by reforming capitalism via democratic means within the existing capitalist system.

Reformists claimed that their ideology had a crucial advantage: once socialist parties had won control in elections, they could immediately use the power of the state to implement reforms advantageous to the proletariat. After the Second World War, proponents of this form of socialism were known as _social democrats._

The second answer came from those who thought that socialism could only be achieved by means other than the ballot box — in other words, by revolution alone.

This was most famously championed by the Russian revolutionary Vladimir Lenin. He critiqued reformism as being unrealistic; he thought that the bourgeoisie system would prevent the proletariat from developing the right consciousness to rule. A _vanguard party_ led by well-trained intellectual elites was therefore necessary; it would inspire a revolutionary consciousness in the proletariat, who, eventually, would rise up and violently overthrow the bourgeoisie. This form of socialism became known as _communism._

While the two sides initially coexisted in an uneasy truce, the 1917 communist Revolution in Russia opened a massive schism between social democrats and communists.

On the one hand, social democrats viewed Lenin's increasingly authoritarian rule as antithetical to the socialistic goals of an egalitarian, democratic society. Conversely, Lenin began demanding that other European socialist parties obediently adopt his communist philosophy. Unsurprisingly, many social democrats refused to follow his lead.

The scene was set for two distinct socialist ideologies that would compete with each other for decades to come. To get an idea of how each form of socialism fared over the last century or so, let's look at two case studies: communist Cuba and social-democratic Sweden.

### 5. While Sweden’s social democracy is one of the most successful in history, it is by no means without problems. 

By the 1920s, socialism was struggling. Social democratic parties in democracies such as Germany were being frozen out of government, despite winning elections, and totalitarianism — in the form of Stalinism and fascism — was growing increasingly powerful. It seemed that socialism was in retreat.

But one nation bucked the trend: Sweden.

In 1932, the Swedish Social Democratic Party (SAP) entered government. Aside from a few years out of government in the 1970s and 1991s, the SAP maintained power for the rest of the century. Throughout this long period, the SAP pursued the doctrine of _folkhemmet,_ or "people's home," which attempted to protect all citizens against economic exploitation and ensure social equality.

The SAP was broadly successful in achieving its aims. By the 1990s, Sweden had spent the most per capita on health care, education and social welfare in the Western world. They also convinced Swedish voters that an egalitarian society was beneficial for all, allowing them to raise tax levels; in 1983, top earners were paying income tax rates of up to 80 percent!

In order to reach these high levels of redistribution and equality, the SAP followed what became known as a _socially controlled market economy._ Rather than focusing on nationalizing business — a policy that most socialist parties would have implemented — the SAP created strong labor laws, giving workers the power to bargain with private business owners.

Such laws allowed the SAP to pass equal pay for equal work legislation, increase wages for the bottom tier of earners and restrict the wages of the highest earners.

Of course, the SAP didn't have it all its own way.

Since the mid-1980s, external pressures, such as economic globalization based on principles of deregulated free trade, have negatively affected the Swedish economy. As a result, governments have had to cut public expenditure and accept higher levels of unemployment.

Then, in 1995, Sweden joined the EU and had to adopt EU economic regulations, the majority of which were much less progressive than its own.

Such conditions meant the slow undoing of decades of SAP progress, with public expenditure dropping from 70 percent to 56.3 percent of gross national product between 1993 and 2002.

Despite this regression, Sweden still boasts some of the most positive social indicators in the world. Just compare Sweden's child poverty rate from 2005 — 2.6 percent — with that of the United States in the same year — 22.4 percent. With statistics like this, it's clear that Scandinavian social democracy must still be doing something right.

### 6. Cuba’s communist state has, against all odds, succeeded in implementing socialist policies. 

On the other side of the Atlantic, the Caribbean island nation of Cuba experienced a very different form of socialism than Sweden. Unlike Sweden's democratic route to a socialist society, Cuba's journey began in 1959 with a revolution overthrowing US-backed dictator Fulgencio Batista.

One of the revolution's leaders, Fidel Castro, quickly began implementing a distinct Cuban form of socialism that he branded _humanism._ This ideology centered on policies such as agrarian reform, anti-imperialism and rent reduction. Directly after the revolution, about 15 percent of the nation's wealth was transferred from landowners to laborers and peasants.

Castro's policies angered the US government, which eventually placed an embargo on the island. Faced with this pressure from the United States, Castro aligned himself with the USSR. In 1961, a trade deal was signed, and then, in 1965, Castro founded the Cuban Communist Party, cementing single-party socialist rule over the nation.

Even with the US embargo in place, Cuba advanced the cause of socialism in many areas. Take women's equality, for example. In 1974, female participation in the economy increased from the prerevolution 18 percent to 37 percent — the highest in Latin America.

Cuban communism could also boast the only universally free health-care system in Latin America. And, by 1992, life expectancy had risen from 59 years (the prerevolutionary number) to 76. What's more, in 1990, infant mortality rates in Washington, DC, were twice as high as those in Havana. Education at all levels was also made universally free, producing a literacy rate of 96.4 percent, one of the highest in the developing world.

This progress toward an egalitarian society wasn't without its setbacks, however.

The dissolution of the USSR and the decline of the global communist trade network caused Cuba to lose 70 percent of its purchasing power. At the same time, the United States tightened sanctions, squeezing the Cuban economy even further. All of this led to an economic crisis in Cuba incomparable to any other experienced by a twentieth-century nation in peacetime. Castro's government eventually increased political repression, resulting in the imprisonment of anyone seen as cooperating with the United States.

Despite this, Cuba's communism survives to the present day, although how long it will soldier on isn't clear.

Looking at the parallel development of Cuban communism and Swedish social democracy, it becomes clear that they share many similarities. Both promoted the cause of equality and solidarity, and pursued a more egalitarian society. Socialism wasn't — and isn't — universally embraced in either country, and, in both cases, social progress tends to be closely tied to sustainable economic progress.

### 7. Other forms of socialism in the post-war era have revolved around feminism and green politics. 

In the post-war era, social movements like feminism and environmentalism presented socialism with fresh problems.

Today, it seems obvious that an egalitarian society must promote equality for women. However, this wasn't always self-evident.

For example, even though communist states in Eastern Europe were far ahead of the West in terms of equality for women, this was mostly restricted to levels of education and professional advancement. In communist East Germany, for instance, 95.1 percent of three- to six-year-olds attended kindergarten in 1989, compared to 67.5 percent in capitalist West Germany. However, the communist government still maintained that women should remain in charge of domestic affairs.

This wasn't that surprising. The majority of socialist organizations at the time — both communist and social democratic — were dominated by men, with little space for women or their concerns.

It was only after the first wave of feminism and women's liberation in the 1960s that socialism began to face these uncomfortable facts. Because women had been frozen out of official socialist organizations, change largely came from grassroots activists.

Socialist orthodoxy was similarly challenged by the green movement.

Decades of economic growth via industrialism and resource extraction had taken a huge toll on the environment. For social democrats, the green movement proved a problem. After all, economic growth — and the tax revenue it generated — was vital in maintaining the welfare state. Should economic growth and socialist progress _really_ be slowed down to protect the environment?

Because of this, green socialists remained on the fringes of socialist thinking for decades. Their actions were mostly restricted to campaigns against specific issues, such as anti-nuclear politics. Indeed, it wasn't until the news of the US's refusal to sign the Kyoto Protocol hit headlines in 2001, that public discourse changed and mainstream socialist thinking began to accept _sustainability_ of the economy for future generations as important as growing the economy today.

Generally, socialism has benefited greatly from the inclusion of green and feminist politics. But there have also been disruptions.

For one, the anti-hierarchical, activist models used by the green and feminist movements have caused the socialist movement to fragment. Where socialists once had a few clear aims and objectives, they now have many, some of them contradictory. For example, it's tricky for socialist parties to simultaneously be against the industrial damage caused to the environment _and_ pro-trade union.

### 8. Although the current outlook for socialism seems bleak, it remains more important than ever. 

Capitalism is the most powerful system today. In the last 30 or so years, _neoliberalism_ — a form of capitalism that advocates the deregulation of markets, privatization and austerity — has been on the march.

To understand why neoliberalism is so influential today, we need to look back to its first few years of domination.

In the first decades after the Second World War, social democracy, fueled and funded by strong economic growth across the West, boomed. By the 1970s, however, this growth had begun to slow. Once-flourishing economies stagnated, and the tide began to turn against social democracy.

Neoliberalism flooded into the ideological space vacated by social democracy. Promoted by politicians such as Margaret Thatcher in the United Kingdom and Ronald Reagan in the United States, neoliberalist policies became the mainstream.

With the ideological change came an attitudinal switch. People no longer believed that welfare systems and public spending should be at the heart of a functioning society. Rather, individuals should be freed of excessive taxation and regulation. They should be allowed to take care of themselves.

The repercussions of this have been severe for socialist ideals. From the 1970s to the 1990s, inequality in Western nations has grown rapidly. Internationally, UN reports show that, in the 1990s, the average income of 54 countries declined. And by 2005, it was estimated that 1 percent of human beings possess as much wealth as the world's 57 percent poorest.

So how can socialists fight back?

Perhaps the most important thing socialists can do is reflect on the mistakes made during socialist experiments over the last century.

One clear lesson is that any future socialist society must be democratic at all levels of its existence. This must involve elements of both participatory democracy in society, and a representative multiparty system. The single-party, authoritarian rule of former communist states cannot be repeated.

Secondly, socialists must seek to develop feasible, sustainable economic strategies. Both centrally planned and growth-based models have proven unsustainable; alternative economic forms such as cooperativism or decentralized public ownership should, therefore, be explored.

Finally, the scope of how socialism should function, whether centralized or local, still needs to be addressed. The increasingly globalized world suggests that socialism should be an internationalist movement. The difficulty of achieving this, however, is hard to surmount.

While these lessons leave many questions unanswered, we can be fairly certain that capitalism will not itself solve the multitude of issues it causes. And as long as this is the case, socialism will retain its importance.

### 9. Final summary 

The key message in these blinks:

**While attempts at socialism over the last century have had varying degrees of success, each has sought to establish an egalitarian society to replace the structural inequalities inherent to capitalism. Future attempts at socialism must address the mistakes of previous iterations if they are to replace capitalism with an egalitarian society free of inequality and economic hardship.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Capitalism_** **by James Fulcher**

_Capitalism_ (2015) chronicles the history of the dominant socioeconomic system that society runs on today. From its humble beginnings in medieval Europe to its present global dominance, capitalism's history is marked by its dynamic — and sometimes unstable — nature. Nevertheless, its influence on how society has developed over the last 200 years is paramount to understanding the modern human condition.
---

### Michael Newman

Michael Newman is a professor of politics at London Metropolitan University. His other books include _Ralph Miliband and the Politics of the New Left_ and _Democracy, Sovereignty and the European Union._

