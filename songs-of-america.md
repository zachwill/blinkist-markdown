---
id: 5d2ef8136cee07000855aaf1
slug: songs-of-america-en
published_date: 2019-07-19T00:00:00.000+00:00
author: Jon Meacham, Tim McGraw
title: Songs of America
subtitle: Patriotism, Protest, and the Music That Made a Nation
main_color: B23A27
text_color: B23A27
---

# Songs of America

_Patriotism, Protest, and the Music That Made a Nation_

**Jon Meacham, Tim McGraw**

_Songs of America_ (2019) explores the music that has shaped American History. From African American spirituals to Elvis Presley's rock and roll, these blinks shine a light on the music that has defined the progress, defeats and protests of Americans from all walks of life, over the last three centuries.

---
### 1. What’s in it for me? Discover America’s musical soul. 

What shapes the soul of a nation? Your history teacher may have emphasized the role of politics, wars and social activism in nation-building. However, music's role in shaping a country's dream, beliefs and direction was probably overlooked. And yet, more than a mere soundtrack to important events, music has often been the driver of those events. It helps spur on their protagonists and embodies their hopes and fears. 

In these blinks, you'll join Pulitzer Prize-winning author John Meacham and Grammy Award-winning musician Tim McGraw on a fascinating tour of America's rich musical heritage. From the American Revolution to the Great Depression and the Vietnam War, you'll discover the songs that formed the backdrop to America's triumphs, conflicts and protests. Shining a light on the cultural impact of icons such as Bing Crosby, Johnny Cash and Elvis Presley, these blinks take a look at America's past by listening to its most famous voices. 

In the following blinks, you'll discover

  * the battle-scarred origins of the American national anthem;

  * why American troops in Vietnam loved Aretha Franklin;

  * how Native Americans expressed their pain through song.

### 2. Early American freedom fighters were inspired to write revolutionary songs. 

On June 10, 1768, Boston was on the verge of war. John Harrison, a British customs official, was attempting to confiscate a local ship, claiming that its American owner had not paid sufficient taxes on its contents. In response, Boston locals threw bricks, stones and insults at their British overlords. Though the British were not defeated that day, something else would come out of the Boston uprising — a song. 

Following the riot, Pennsylvania colonist John Dickinson was inspired to write verses about the colonists' right to self-determination. Entitled "The Liberty Song," Dickinson implored his fellow Americans to "join hand in hand" to answer "fair Liberty's call." This rebellious song had an immediate impact. After it was published in the _Boston Gazette_, people all over the city left their homes with whatever musical instruments they had on hand and sung it in the streets. 

Why this immediate popularity? Well, this song was one of the first to connect the idea of American independence with music. It affected people emotionally in a way that dry political writing simply couldn't. Furthermore, "The Liberty Song _"_ showed Americans the potential for unity when they took to the streets together to sing it.

Thanks in part to individuals like Dickinson and their rousing messages of freedom, America finally signed the _Declaration of Independence_ in 1776. Unfortunately, though, this momentous event did not enshrine everyone's liberty. For women and African American people in general, life carried on as it had before — in subjugation and slavery, respectively. 

Nevertheless, in revolutionary America, these marginalized groups also expressed their yearning for freedom through song. In 1795, for instance, a song appeared in the _Philadelphia Minerva_ newspaper. Entitled "Rights of Women," and set to the familiar melody of "God Save the King, _"_ it proclaimed that "Woman is free" and should not "yield to slavish fear." 

20 years earlier, another American woman also called for liberation through verse. Her name was Phillis Wheatley, and she was an educated enslaved person. In a set of verses called "On Being Brought from Africa to America," Wheatley addressed the taboo subject of equality. She wrote, "Negroes, black as Cain, may be refined, and join th' angelic train." Incredibly, Wheatley was such a gifted writer that some of her verses reached the desk of George Washington and were published after receiving his approval.

### 3. Brutal nineteenth-century conflict inspired both Native Americans and white settlers to write music. 

Some songs are forged in the fires of battle. In 1814, American colonists were still fighting with the British Crown to have their _Declaration of Independence_ validated. And at Fort McHenry, Baltimore, on August 13, British Naval Forces were treating the colonists to a relentless cannon and mortar attack. Francis Scott Key, a lawyer from Washington, watched as the events unfolded. As night fell, he dreaded the destruction he might see in the morning. Had all been lost overnight? 

When dawn came, however, Key saw through the smoke and fog, something profoundly moving. 

That something was the Star-Spangled Banner. The American flag was waving defiantly in the wind. His countrymen had taken a battering but had not been defeated. Inspired by their defiance in the face of adversity, Key rushed back to his Baltimore hotel and put his patriotic feeling into words, resulting in the creation of "The Star-Spangled Banner." In this moving song, rich with longing for victory and freedom, Key remembers how "the bombs bursting in air, gave proof that our flag was still there."

Unlike John Dickinson's "Liberty Song," whose lyrics point to a more abstract notion of freedom, "The Star-Spangled Banner" focuses on a particular symbol of independence — the American flag itself. By 1814, the flag, created during the Revolution, was already a symbol of equality and the perseverance of the United Colonies. Though Key's song was an instant hit, it was not until 1931 that "The Star-Spangled Banner" became the national anthem. 

Sadly, other American songs written in times of conflict have not seen such a triumphant outcome. Though the colonists finally gained their independence from Britain, the Native Americans met with a very different fate. Driven from their lands throughout the nineteenth century, Native Americans experienced hunger, destitution and genocide. Many songs from this period capture their wish for freedom. 

In one Choctaw Nation song written during the 1830s, a period when the Choctaw people were forcibly removed from their ancestral lands in Mississippi, the unknown writer sings "When I die, I am going to be in a good land." Lyrics like these express longing for salvation, as well as a mournful understanding that it will probably not arrive in this life. Unfortunately, in the case of the Native Americans, these sentiments were devastatingly prophetic.

### 4. Activists expressed their dream of a slavery-free world through song. 

The American dream hasn't always lived up to reality. In 1852, Frederick Douglass, a former enslaved person, gave an Independence Day speech in New York and spoke some hard truths to his fellow Americans. What did July 4 mean to black people in America? Douglass asked. What could African Americans divine from the celebrations, other than the vanity and hypocrisy of white Americans, who refused to acknowledge black peoples' humanity? With stirring orations like this, Douglass and others like him were pushing hard for the abolition of slavery. Here too, the fight for freedom was accompanied by powerful music.

British abolitionists Julia and T. Powis Griffith composed this music for Douglass. "Farewell Song" was written to coincide with Douglass's return to the United States in 1847, after he had toured England with his anti-slavery message. In response to descriptions of America as the land of the free, the song laments, "Alas! That my country should be America! Land of the slave." 

Music addressing this disconnect between slavery and the American ideal of freedom also inspired those fighting in the Civil War. As northern Unionist soldiers clashed with the slave-owning southern Confederates, it was the song "Battle Cry of Freedom" that rallied their spirits with its proud proclamation that "not a man shall be a slave, shouting the battle cry of freedom." This song was so powerful that President Lincoln remarked that its writer, George Frederick Root, had done more with "Battle Cry of Freedom" than any military leader could have to stir the Unionist soldiers' souls as they headed to war.

The end of slavery, when it eventually came, was also celebrated with music.

On December 31, 1862, the night before slavery was abolished in most southern states, African Americans gathered across the country to wait for the clock to chime midnight and usher in an unprecedented era of freedom. Harriet Tubman was a leader of the Underground Railroad, a clandestine network that helped southern enslaved people reach safety in the north. That night, a crowd of black Americans gathered in Washington and sang Tubman's favorite song, "Go Down Moses," which was an African American spiritual — a genre of song that emphasized Christian values, as well as the pain of slavery. As the first hour of freedom drew near, the crowd chanted "Go down Moses...tell old Pharaoh, to let my people go."

> _"Shall I, like a coward, not join the fight...Scared by the enemy's tyrannous might?"_ \- Farewell Song

### 5. Music accompanied the struggles and triumphs of women and black Americans. 

In October 1915, tens of thousands of women took to the streets of New York. Walking arm in arm, they headed down Fifth Avenue to Central Park. This was a march of celebration. Finally, after decades of struggle, women would vote in the next presidential election. Once again, music had propelled the fight for women's suffrage forward. 

One of the most famous songs of the suffrage movement was entitled "Daughters of Freedom, the Ballot Be Yours." First appearing in 1871, this stirring protest anthem included lyrics imploring women to "sunder the fetters custom hath made" and to "yield not the battle, till ye have won!" A year after it's release, one American woman fulfilled the song's message. 

In 1872, after reading a newspaper article urging people to register to vote in the upcoming presidential election, a New York woman named Susan B. Anthony decided to do just that. As the article did not specify that only men should register, Anthony bravely set out for her local electoral office. After an argument with voting officials, she got her registration card and illegally voted.

Predictably, the male authorities were outraged. After her arrest, prosecution and conviction, Anthony used the courtroom as a platform to make a barnstorming speech about the natural right of all American women to vote. Her powerful words built the momentum of the modern women's suffrage movement, and in October 1915, women marched to celebrate the fulfillment of Anthony's vision. 

Unfortunately, the early twentieth century was not a cause for celebration in all quarters of America. The song "Lift Every Voice and Sing" was written in 1916 by brothers James and John Johnson. It spoke of black Americans' journey "out of the gloomy past," but also emphasized that they must "march on till victory is won." Lyrics like these pointed to the fact that, although African Americans were free from slavery, they still battled a legacy of white supremacy. 

Around this time, the hate group the Ku Klux Klan had reformed to lynch and terrify black Americans, and cruel segregation laws still ensured that African Americans lived as second-class citizens. Thus, "Lift Every Voice and Sing" offered a cautious message of hope about the progress made, while acknowledging the pressing challenges that lay ahead. This is a message that continues to resonate today. Known as the Black National Anthem, "Lift Every Voice and Sing" was performed at Barack Obama's 2009 inauguration and at the Coachella music festival in 2018, when Beyoncé became the first black American performer to headline the event.

### 6. The Great Depression gave birth to songs of both optimism and deep frustration. 

In the early 1930s, America was down on its luck. Caught in the middle of the Great Depression, much of the country was without work and hopeless. Into this mood of despair stepped Franklin D. Roosevelt (FDR), who was inaugurated as President in 1933. Determined to keep his country together, FDR seemed to many to be a walking promise of better things to come. Notably, his march toward hope was accompanied by song. 

In the cash-strapped 1930s, that song was "Happy Days Are Here Again." It came to national attention when it was played during an FDR rally in 1932. This upbeat tune initially seemed out of step with depressed America. Nonetheless, it perfectly captured the infectious optimism of FDR himself, who always expressed confidence that the nation would lift itself out of its present hardship. 

In 1933, this hope was a much-needed antidote to a frightening world. Adolf Hitler was poised to become Chancellor of Germany and Russia was in the grip of Joseph Stalin. Although recent evidence suggests FDR privately worried that America would also fall prey to political extremism, in public he constantly preached that "happy days" were just around the corner. 

In these troubled times, not every song was so upbeat. As the Depression wore on, a tune that perfectly captured the mood of hopelessness was, "Brother Can You Spare a Dime." Written in 1930, and most famously performed by Bing Crosby, the song tells the story of a veteran of the Great War who, despite working hard and serving his country, ends up unemployed and queueing for free bread, unable to support himself. 

As America struggled under the weight of unemployment and broken dreams, another songwriter sought to keep the nation's spirits up. Written by Irving Berlin, "God Bless America" is a stirring patriotic anthem that celebrates America as "the land that I love." 

However, not everyone was convinced by Berlin's sentimental message. Woody Guthrie was a musician from a blue-collar family who had witnessed, first-hand, the terrible human cost of the Great Depression. In Guthrie's opinion, "God Bless America" struck an inappropriately triumphant note. He thought the nation should be searching its soul, rather than patting itself on the back. Thus, Guthrie sat down to write a response. The result was, "This Land is Your Land," a folk song that questioned Berlin's rosy vision of the United States.

> _"By the relief office, I seen my people...I stood there asking, is this land made for you and me?"_ \- This Land is Your Land

### 7. The Vietnam War sparked songs of protest, as well as conformity. 

In the late 1960s, the Vietnam War was raging, and America was bitterly divided. On one side stood millions of young people who demanded that the troops come home. These were the children of the counterculture, a population championing pacifism and progressive values. On the other side was Middle America. These were social conservatives who thought the dissenting youths were disrespectful and shamefully unpatriotic. On both sides, music fed their strong opinions. 

During this era, anti-war songs like Edwin Starr's 1970 hit, "War" and The Animals' "We Gotta Get Out of This Place," boomed from loudspeakers at protest marches. And Middle America, the group that President Nixon would call the 'silent majority' who quietly agreed with the Vietnam War, had their own anthem. While not exactly a pro-war song, Merle Haggard's country song "Okie from Muskogee" explicitly denounced those young hippies who wore their hair long, destroyed their draft cards and questioned their country's foreign policy. 

And what about the American troops in Vietnam? What were they listening to as they fought in this controversial war? 

The musical tastes of soldiers were often divided along racial lines. As one veteran recalls, white soldiers were typically found on one side of the room, listening to country music, while African American recruits were on the other, listening to James Brown and Aretha Franklin.

In the late 1960s, Brown and Franklin were important cultural icons. Brown innovatively fused gospel influences with rhythm and blues, culminating in a potent vision of black empowerment and self-reliance. Equally, with her song "Chain of Fools," Aretha Franklin seemed to offer a rebuke to the horrors of Vietnam. This classic track was widely interpreted as a criticism of bad leadership. In the midst of a war that many believed to be unjust, it was a fitting choice for many black American soldiers. 

But even in this highly polarized debate, not everyone was aligned with one side or the other.

Johnny Cash, a country music star, was somewhat ambivalent. As a patriotic American, Cash's instinct was to support the war effort. And yet, during hospital visits to injured Vietnam veterans, Cash couldn't help but recognize the dreadful toll of the conflict. This inner turmoil led him to write "Ragged Old Flag" in 1974. In this complex song, Cash sings of how the American government has been "scandalized across the land." But he also sings, "she's been through the fire before...she can take a whole lot more." His conclusion, it seems, is that America had lost its way, but would soon be renewed.

### 8. Elvis Presley and Bruce Springsteen represented two differing visions of America. 

In 1954, at an open-air music performance in a Memphis park, something incredible was happening on stage. A white kid was singing the blues. Furthermore, with his incredibly versatile voice, electrifying good looks and outrageous dance moves, he was making them his own. That kid was Elvis Presley, and he would go on to become one of the most powerful cultural forces in twentieth-century America.

In many ways, Presley's enormous success — and his terrible demise just two decades later — mirrors the fortune of America itself.

With his incredible rise to fame, Elvis neatly embodied the American Dream. Born to a poor family in Mississippi, Presley's father had done jail time, and his mother struggled to make ends meet. As such, in the eyes of an adoring public, he was proof that anyone with enough talent could make it in the land of opportunity. 

Unfortunately, Elvis embodied more sinister American values, too. Indeed, as one black musician put it, one of the reasons Presley was so successful was because he was a white performer singing black music. In mid-twentieth century America, when segregation among the races was still commonplace, Presley was a Southern white man who culturally appropriated black music for a white audience. 

Presley came to represent another classic American shortcoming in the years before his premature death at 42 years old. Famous for his love of fried peanut butter sandwiches, he was increasingly more renowned for his obesity and excessive lifestyle. In this way, Presley was a precursor to the gaudy celebrity culture and over-consumption which have proliferated in the decades since his death. 

Several years later, another phenomenally successful musician would overturn the decadent vision of America that Elvis represented. His name was Bruce Springsteen. 

Springsteen, who came from a blue-collar background, was a charismatic pop star. He was also more than just a symbol of everything sexy and cool in 1980s America — he had a political message. His hit song "Born in the U.S.A." became an anthem for the American working class, who Springsteen felt were being exploited by the country's elite. Addressing a crowd in Pittsburgh in September 1984, Springsteen warned that inequality was rising and that America was becoming a land of excessive wealth for one half of the country, and extreme poverty for the other. 

Arguably, this musical message is still relevant for many Americans today.

### 9. Final summary 

The key message in these blinks:

**Even before America gained its independence, its people declared their patriotism and unity through the medium of song. As well as togetherness, Americans used music to express dissent in the form of protest songs. Over three centuries, American songwriters have created a rich cultural tapestry and communicated their hopes, fears and lament for their nation, through music.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _The Soul of America_** **, by Jon Meacham**

Now that you've explored America's history through its music, why not take a deeper look at its troubled past by reading the blinks to _The Soul of America_. Penned by the same author, Pulitzer Prize winner Jon Meacham, _The Soul of America_ is a whistle-stop tour of the clashes and confrontations that have made this nation what it is. 

From the final moments of the Civil War to the twentieth-century civil rights movement, these blinks reveal the deep divisions and opposing beliefs that have created American conflict since the country's birth. So, to understand why today's politics are so polarised, and gain insights into where America might be headed next, check out the blinks to _The Soul of America._
---

### Jon Meacham, Tim McGraw

Jon Meacham is an American author. In 2009, he won the Pulitzer Prize for his biographical book _American Lion: Andrew Jackson in the White House_. He is currently a contributing editor to _Time Magazine_. Tim McGraw is a country music singer and actor. He has received three Grammys and ten American Music Awards.

