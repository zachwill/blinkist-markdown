---
id: 553501f76638630007d50000
slug: sonic-boom-en
published_date: 2015-04-23T00:00:00.000+00:00
author: Joel Beckerman with Tyler Gray
title: Sonic Boom
subtitle: How Sound Transforms the Way We Think, Feel and Buy
main_color: F4F375
text_color: 5C5B2C
---

# Sonic Boom

_How Sound Transforms the Way We Think, Feel and Buy_

**Joel Beckerman with Tyler Gray**

_Sonic Boom_ (2014) highlights the many ways sounds permeate not only our environment but also our personal lives. From your company brand to your personal space, sound has the power to make us remember and even can encourage us to buy. These blinks show you exactly how to harness the power of sound in your business and life.

---
### 1. What’s in it for me? Discover how to harness the power of sound in your business and life. 

From a sticky company jingle to a pervasive pop song, sound is a powerful thing. Even after you've turned the radio off, the notes continue to play in your head, connecting feelings and memories.

Yet why is sound such a powerful force? How can we hear the sizzle of meat in a pan one moment and the next be overwhelmed by a serious hunger, and want to purchase a plate of fajitas?

The power of sound is not an indecipherable mystery. These blinks will show you how to use sounds effectively in telling your brand's story or setting the mood for a great customer experience. What's more, you'll learn how to use sound to improve your own life, to find a little peace after a loud day.

In these blinks, you'll find out

  * how a restaurant sells more dinners with the sound of sizzling steaks;

  * why one man's symphony is another's sonic garbage; and

  * how a revised jingle brought a struggling TV station back from the brink.

### 2. While you might try to block out all the world’s noise, your brain never stops listening. 

Close your eyes right now and take in the sounds around you.

What do you hear close to you? Perhaps you can eavesdrop on a whispered conversation on the bus. Now, what do you hear further away from you? Maybe you can identify the rumble of highway traffic, or children shouting in a nearby playground.

Every day we are surrounded by sound, and what we hear shapes the way we feel, influences our energy level and touches our consciousness. Movie music does this particularly well: you can't help but be put on edge when the "shark theme" from _Jaws_ — just two resonant bass notes — is played!

The right sound heard at the right time can even trigger a memory or evoke a powerful emotion. When this happens, it's called a _boom moment_. But why exactly do boom moments occur?

It's simple: your brain _never stops listening._ Even when you might not consciously be paying attention to the sounds around you, your brain is processing it all and connecting the sonic input with experiences you've had.

Chili's _,_ a Mexican restaurant chain in the United States, has recognized the power of sound to influence its customers. When you enter a Chili's restaurant, you immediately hear the sizzle of meat on a hot iron plate.

This sizzle grabs your attention and inspires a chain of sensory reactions: you hear and smell the meat, the laughter of other happy and sated guests surrounds you, and you realize suddenly just how hungry you are for fajitas!

This is a boom moment, and one that obviously contributes to the restaurant's bottom line. No surprise that Chili's sells as much as 60.4 _million pounds_ of fajitas per year!

The influence of sound on how we feel isn't random; there's definitely a science to it. Researchers Clare Caldwell and Sally A. Hibbert found that diners spent almost 15 minutes longer in a restaurant in which slower-tempo music was played, in contrast to a restaurant that played faster-tempo music.

The reason? Slower music made diners feel relaxed and safe, so they lingered over dinner.

Would you want to incorporate the power of sound in your business? There's more to it than just turning on the radio. Read the next blink to learn more.

> _**"** The quickest way to understand the power of sound is by seeing what happens when it's gone."_

### 3. Use sound to create an authentic experience for your customers, inspiring positive emotions. 

How would you use sound to draw people to your company's brand?

Harnessing the power of sound is much more than just picking a popular song to suit your brand's story. What your brand really needs is a _sonic identity_.

A sonic identity is a sound made especially for your product. When people hear it, your brand should immediately spring to mind. The first seven notes of Disney's "When You Wish Upon A Star" has the power to remind you of childhood trips to Disneyland, or watching the animated film, _Pinocchio._

Sonic branding at its best can tell a deep, meaningful story. At its worst, it will kill every and any part of your brand story. You won't have a sonic identity; you'll have _sonic trash_.

Sonic trash is a sound that reminds you of fake or unreal things, something that turns a customer off, or connects to a memory they don't want to recall.

Think about the sounds that your product creates. Are those sounds potentially unpleasant, like the obnoxious rustle of a potato chip bag? If so, creating a positive sonic identity should be a priority in your creative branding process. You need to counter that sonic trash with a positive sound.

So how do you do this? It's actually not that complicated. Sometimes the best sonic experiences are the simplest ones, as such sounds give your product a sense of _authenticity._

Let's say you own an antique shop. What does your shop's front door sound like? How about your shop's floorboards: do they squeak or sound solid? How do people's voices echo around the room?

The aim is to build an emotional connection with your customers, through sounds that support the authentic experience you're creating, and importantly, sounds that they'll remember.

How about the tinkling of an antique cash register? Never underestimate the impact a seemingly insignificant sound can have on a customer's experience.

### 4. Music can act as an anchor to pull people together, inspiring them to act as one. 

We've already seen how sound can make people feel comfortable, but it can also do far more.

Did you know that sound can even inspire a person to follow her convictions?

Music is a profound tool in the expression of the beliefs, ideas and values of a community. Sound can even form an anchor that pulls people together and inspires them to act as one.

In 2010, the American Spanish-language broadcasting company Univision was losing viewers to other networks. The company decided it was time to overhaul its anthem and so contacted Man Made Music, the author's sonic branding company.

By 2013, Univision saw a complete turnaround. How did they do it?

Man Made Music found sounds inspired by Mexican and Caribbean music, and created an anthem that was upbeat, sounding like a celebration of family and life.

In December 2012, the company premiered its new anthem across the network. And in the next year Univision had recaptured its viewers, claiming the largest Hispanic and Latino audience worldwide.

Another way to ensure that your company's sonic identity resonates with your audience is to keep it fresh and up to date.

American broadcaster NBC was looking to exceed its viewer numbers for the 2011 Super Bowl, when it chalked up some 111 million viewers. But how could the broadcaster do this?

The company decided to do some sonic rebranding by modernizing the station's theme music for the upcoming 2012 Super Bowl. Famed composer John Williams and the author collaborated to produce a vibrant fusion of modern music, including rock guitars, hip-hop drums, electro beats and dubstep.

The emotions fans feel while watching American football's pinnacle moment — tension, triumph, optimism and energy — were evoked in the new theme music.

And the new music had the desired effect, as the 2012 game broke the previous year's viewership record, becoming the most-watched television event in U.S. history, with 111.3 million viewers.

### 5. How you sound when you speak or walk can affect how people think about you. 

You've now seen the power sound can have in company marketing. Let's look now at how you can use sound to make your life more productive and enjoyable.

Life in an urban environment is often lacking peace and quiet. The noises of the busy street, the neighbor's pounding feet upstairs: sleeping pills and earplugs are the norm for a good night's rest.

Cheer up! It needn't be all so glum, as you can reshape your sonic experience.

Isolate yourself from unwanted sounds by installing low-cost noise machines to create a soothing sonic environment. Even turning on a fan can dampen the sound of car horns outside.

If you're feeling stressed, you can also reshape your sonic experience by listening to birds singing or even just by centering your focus on the sound of your own breathing.

Think too about how the sounds you make might affect others. Personal sounds can influence the impressions we hold of other people.

The experience of Sarah is an enlightening one. A business school whiz, Sarah was looking for a summer internship; yet despite chatting with many Fortune 500 companies, not one took her on.

Why? Sarah is an "up-talker." Everything Sarah says sounds like a question, in that the pitch of her voice is raised at the end of every sentence. This style of speaking led her potential employers to assume she was too young and thus inexperienced.

How you talk, the sounds you make when you walk or even the rustle of your clothes can affect how you relate to those around you, and how they relate to you!

But how do you avoid creating a negative sonic experience? Be a good listener and be aware as you speak, move and interact with others. Then ask yourself, "What do I want to hear from my world?"

If you know the type of sonic environment you want, there's no reason why you can't create it!

### 6. Final summary 

The key message in this book:

**Though you may not be aware of it, you are constantly influenced by the sounds around you. Once you start to listen closely, however, you can use sound to improve not just your company's brand and your customer experience, but also your everyday life.**

Actionable advice:

**Hack your workouts with sound!**

If you want to get the most out of your workout, delete those podcasts or television shows and instead listen to fast-tempo music. Music with a fast beat is motivating, and you'll be even more pumped to burn those calories off!

**Suggested** **further** **reading:** ** _Musicophilia_** **by Oliver Sacks**

_Musicophilia_ explores the enriching, healing and disturbing effects of music. It delves into fascinating case studies about disorders that are expressed, provoked and alleviated by music.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Joel Beckerman with Tyler Gray

Joel Beckerman is an award-winning composer, television producer and founder of Man Made Music, a company specializing in sonic branding.

Tyler Gray is an editorial director for Edelman in New York.

