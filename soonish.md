---
id: 5a9684bab238e100088283e3
slug: soonish-en
published_date: 2018-03-01T00:00:00.000+00:00
author: Kelly Weinersmith and Zach Weinersmith
title: Soonish
subtitle: Ten Emerging Technologies That'll Improve and/or Ruin Everything
main_color: 2EBFE4
text_color: 19697D
---

# Soonish

_Ten Emerging Technologies That'll Improve and/or Ruin Everything_

**Kelly Weinersmith and Zach Weinersmith**

_Soonish_ (2017) explores transformative technologies that will emerge in the future, from space exploration to brain-to-computer interfaces, and the ongoing real-world efforts undertaken to make them a reality today. For each technology explored, Kelly and Zach Weinersmith consider its current status, the primary concerns and the effect each technology is likely to have on the world as we know it.

---
### 1. What’s in it for me? Find out what the future will look like by checking in with today’s cutting-edge technologies. 

We may not yet have those flying cars that so many sci-fi movies over the years have predicted, but we've still made some pretty astounding technological breakthroughs in recent years. Since our attention is often grabbed by other headlines, it's likely that you don't even know where we're at when it comes to things like bioprinting, spaceplanes and microscopic robots.

So, let Kelly and Zach Weinersmith bring you up to date on the latest developments and fill you in on when you can expect to 3D print your own custom-made home on the cheap. The answer is, probably, "soonish" — as in, not today, not tomorrow, but soonish.

In these blinks, you'll discover

  * the difference between fission and fusion power;

  * why a bucket of stuff is both incredible and terrifying; and

  * why Pokémon Go is only a small step toward the incredible virtual world to come.

### 2. Developments in space exploration are hindered by the cost of getting to space. 

Given that the first moon landing happened way back in 1969, you may feel disappointed that so little progress has since been made when it comes to getting humans further into space.

But the reality is, building rockets and launching them into space is an expensive business, which is why scientists are looking into cheaper methods of space exploration.

One such method is sort of like using a really big elevator. Imagine a giant cable stretching from a mobile sea platform on Earth all the way to a giant asteroid in orbit. It could be used to send cargo, passengers and spacecraft back and forth, eliminating the need for all that expensive and explosive rocket fuel.

Sounds good, right? However, there is currently no material that is both strong enough and light enough to make a space elevator a reality — though scientists are still eagerly looking into this idea.

Another potentially less expensive method is the spaceplane.

Spaceplanes would use two different kinds of engines. The first would use a combination of air and fuel to create a high-pressure force powerful enough to propel the plane out of the Earth's atmosphere. Then, because there isn't any air in space, the spaceplane would need to switch to a more traditional rocket engine that uses normal propellant. This would be cheaper than the current method where the rocket has to carry enough expensive oxidizer to power the rocket engine all the way into space.

Another important part of making space travel affordable could be asteroid mining. This has the potential to provide cheap materials that could be sent back to Earth, or used for building the settlements on other planets.

The US-based company Tethers Unlimited has already proposed a system for capturing asteroids. It essentially works like a space net, and they've called it "the Wrangler."

It would allow us to trap an asteroid in a net, and then to use that asteroid as a base of operations for a settlement. We could also theoretically drag it elsewhere to create colonies in space or to mine its resources; from what we already know about the main types of asteroids, water, metal and oxygen can be extracted from them.

### 3. Fusion power has been a tough nut to crack, but this might change in the near future. 

There's an important difference between nuclear _fission_ and _fusion_. Fission refers to the energy produced when atoms split apart, while _fusion_ captures the energy produced by atoms fusing together.

So far, our attempts at harnessing fusion power have been inefficient and largely unsuccessful, though scientists are still hard at work trying to find a solution. There's still much hope that fusion power will be a reliable and clean source of energy in the future.

The best solution we've come up with so far is known as the blasting approach, which involves taking a lot of fusion fuel and blasting it with a laser to create massive reactions. This is what scientists are currently doing at Sandia Labs in the United States, in an experiment known as the _MagLIF project_.

Scientists are using an enormous generator called the _Z machine_, which collapses a cylinder filled with fusion fuel, causing it to release an immense amount of fusion energy at the same instant. While the energy required to operate the Z machine is still far greater than the energy released from the fuel, scientists are continuing to improve upon the design and hope to at least break even on its energy production by 2020.

If the right solutions are found, the energy yields may eventually make fusion power a reliable source of clean energy.

So far, the most promising improvement is a configuration called the International Thermonuclear Experimental Reactor or ITER, which results in a better way to confine the fuel and then heat it.

In a collaboration among scientists from 35 nations, the ITER configuration plans to house the fuel, in this case plasma, in a donut-shaped chamber and confine it there through the use of magnetic fields. The plasma would then be heated to extreme temperatures, causing the atoms to fuse with each other.

One advantage of ITER is that the heating process will cause multiple fusions to occur within the plasma, creating a continuous chain reaction. The problem so far has been an unfortunate series of delays and budget overruns, but there's still hope that a functional fusion reactor will be in operation by 2027.

### 4. Programmable matter has applications in areas like medicine and construction. 

Imagine living in a house that could change its structure depending on the weather. It might sound like pure fantasy, but such _programmable matter_ that can change its physical properties based on its programming is actually already being worked on.

As you can likely imagine, programmable matter has the potential to be a huge asset for humankind, and not just in building materials.

At the Massachusetts Institute of Technology, Dr. Daniela Rus is already experimenting with such matter in the form of origami robots. Just like paper origami, these robots can fold into different shapes thanks to actuators, or flexible machine parts, placed along specific folding lines. So far, Dr. Rus's material of choice for these robots has been pig intestine.

The hope is that one day, programmable matter robots will fold into different shapes, allowing them to travel through the body and take medicine to any specific part that a doctor might want to target. And after the robot performs a task, depending on the material, it could simply dissolve inside the body.

As for the programmable house, one such project is called the _HygroScope_, and the programmable material in question is wood that responds to humidity by bending into different shapes, opening and closing hundreds of little pores, thereby making the material more water-resistant and durable.

However, given the directions programmable material could take us, there are also some ethical questions being raised too.

If we take this technology to its logical ultimate goal, we'll eventually get to the _Bucket of Stuff_ : a container of a goo-like substance that could be programmed to turn into whatever you need, such as a hammer, a wrench or a bowl. Now, what would prevent someone from buying a Bucket of Stuff to make a gun or some other weapon?

Indeed, 3D printing has already raised similar difficult questions for lawmakers. And with the idea of programmable matter giving agency to physical objects, even more such questions could emerge — like what if the programmable parts of your car malfunctioned and caused a fatal accident? Who would be at fault?

As the technology advances, we must continue to grapple with such ethical concerns.

### 5. New technologies have paved the way for buildings constructed by robots. 

Robots have been producing things for us for some time now, but there's one area that they've yet to infiltrate: houses. This is because houses are big, complicated structures with lots of different elements and possible locations to consider, and robots are better suited for precisely-controlled factory conditions.

However, these obstacles may soon be a thing of the past.

In China, the company _WinSun_ has cleared the impressive hurdle of 3D printing houses. The company first prints the walls and other components layer by layer in its factory and then assembles them on site.

But a group in the United States, led by Dr. Steven Keating, is using a different approach to 3D-printing in construction.

Dr. Keating has built a truck that has a giant 3D-printing arm capable of rapidly creating a lightweight foam mold into which concrete can be poured. This allows for quick, customizable and inexpensive construction, along with all the sturdiness that comes with using concrete.

But that's not all; Dr. Keating went one step further and made a second, self-driving version of the truck. This means that autonomous construction machines could build structures in places considered too dangerous for human labor, whether at unstable disaster sites, underwater or even in space.

While this raises the threat of putting humans out of work, the benefit of more affordable, sturdier and quicker construction methods may be too good to ignore. There's no denying that some jobs will be lost to automated construction robots, but some specialists are already predicting that the real problem will be an increase in the wage gap, with computer-savvy engineers being paid even more, and the ground crew being paid even less.

Nevertheless, this sort of 3D printing could solve the issue of housing solutions for refugees and millions of other impoverished people in slums or similar life-threatening situations.

And for architects, there's the aesthetic benefit of 3D printing and being able to design and build structures that are far too difficult or even impossible to build with today's methods. So, not only will beautiful houses be affordable, they'll also be like nothing we've ever seen before.

> _"You're probably better off having robots build your Martian home's radiation shield than doing it yourself."_

### 6. In the future, augmented reality will increase our efficiency, but it also poses a number of concerns. 

Imagine walking through an actual forest with a virtual guide a few steps ahead of you, giving you the details of each type of tree you walk past. This is the kind of future that _augmented reality_ or AR, will provide. Unlike virtual reality, which places you in a virtual world, AR adds layers of virtual elements onto the real world around you.

One of the benefits AR technology could provide is efficiency.

Take the _Smart Helmet_, for example, made by the company _DAQRI_. This AR device displays virtual layers on the helmet's eye shield, which can make it a great tool for learning complex jobs in a safe, virtual environment.

In a study involving the assembly of aircraft parts, practicing on the Smart Helmet made trainees 30 percent faster than before and reduced their normal error rate by a whopping 94 percent.

AR is also being developed to assist in surgeries, construction and combat. Just like the internet, AR will put more information at our fingertips, so to speak, and make us more efficient because we won't need to stop what we're doing to search for that information.

However, also like the internet, AR comes with issues regarding control and privacy.

Pokémon GO is a good example of a game that leveraged the power of AR by taking your real surroundings and superimposing images of virtual characters for people to interact with. It was hugely popular, but was not without its problems:

One issue was players finding Pokémon at the Holocaust Museum in Washington, DC, prompting museum personnel to ask players to show some respect and turn off the game, which raised the question of who exactly is responsible for monitoring the AR world.

But this technology poses bigger threats, some of which have already been raised by the 3D facial recognition software Recognizr.

In theory, you could take Recognizr with you wherever you went and use it to instantly and secretly get information about the people around you without their consent. Obviously, this kind of technology, in the wrong hands, could cause all sorts of problems.

### 7. Synthetic Biology could change life as we know it. 

In 2015 alone, malaria killed over half a million people, and efforts to eradicate the disease have remained unsuccessful. But there is hope that the growing field of _synthetic biology,_ or advanced DNA manipulation, could finally give us the tools we need.

Scientists can manipulate the DNA of mosquitoes with a gene that is resistant to malaria. Introduced into the larger population, it is believed this gene could quickly spread and wipe out the disease in any particular region. While this might sound like a miracle that should happen as soon as possible, scientists want to conduct more tests before releasing a synthetically engineered organism into the wild.

Plus, mosquitoes are just the tip of the synthetic biology iceberg. Scientists are also looking into "humanizing" pig organs so that humans could have a large supply of any number of organs for medical transplants.

And then there is the growing field of made-to-order organisms.

Recently, scientists have discovered how to manipulate the bacterial immune system in such a way that allows them to cut DNA at a specific location, remove a piece from one organism's DNA and add it to another's.

This technology is called _CRISPR-Cas9_, and it could enable us to do things like cut potential diseases out of human embryos or change the eye color or hair color of your future child.

This may have you wondering, how close are we to purely synthetic life? So far, Dr. J. Craig Venter has created an organism named Syn 3.0 by replacing the natural genome of a bacteria with a laboratory-made, synthetic genome.

While Syn 3.0 could be called a synthetic organism, it only has the most basic functions of life, like reproduction. But it comes with the possibility that we could program this simplified organism to do whatever would be most useful, such as cleaning up toxic spills or recycling waste.

Of course, tampering with nature at the molecular level poses a variety of ethical concerns, many of which still need to be addressed.

> _"Ultimately, synthetic biology should give humans the power to have organisms made to order."_

### 8. In the future, precision medicine can provide an immediate, accurate medical diagnosis and prescribe treatment. 

Despite the great progress made in modern medicine, thousands of patients still get misdiagnosed every day. In the future, however, we might be able to employ something known as precision medicine.

In a world of precision medicine, you could walk into a clinic and get an instant diagnosis and prescription of the best possible treatment. The key to this system would be _biomarkers_ or biological indicators that would immediately detect any unwanted molecules entering your bloodstream, cancerous growths or even symptoms relating to depression.

While precision medicine, along with a community of doctors and scientists who are capable of analyzing this kind of data, is likely a long way from becoming a reality, there have already been some major advances.

Since being able to recognize the appearance of new molecules is crucial to the development of biomarkers, the discovery of microRNA has marked a significant step in the right direction. While scientists are still coming to grips with the true nature and full implications of microRNA, it is believed that these tiny molecules, which are found in the human bloodstream, can indicate the presence of cancer and even tell us what stage the cancer is at.

Now, aside from curing diseases, precision medicine could also introduce every patient to their own individual _metabolome_. This is the name of your unique system of molecules, including sugars and vitamins, which help your body function. It would allow patients to know which foods and activities are best for them, and which ones they should avoid in order to stay healthy.

On top of all this, there's also the possibility of _behavioral_ _biomarkers_ that could indicate the development of mental health problems.

Dr. Christopher Danforth of the University of Vermont already believes there are signs we can pick up on that may signal a troubled mind. For example, in his research, Dr. Danforth has found that depressed people tend to post darker photos on Instagram than people with more positive outlooks.

### 9. Scientists are developing the technology to create human organs with a 3D printer. 

While the previously-discussed 3D printing of houses is undoubtedly impressive, you might be wondering how far this technology can really go. Well, one of its potential applications may offer some good news for the 8,000 people who wait in vain for an organ transplant every year: _bioprinting_.

Bioprinting aims to let us print the organs that these people need. But, as you may expect, it's an extremely complex procedure.

Organs tend to be made up of a dozen or more types of cells. So, to print an organ, there needs to be a dozen or more kinds of bio-ink for the printer to replicate these various cells. Plus, depending on how the cells react with one another, the bio-ink will need to undergo any number of treatments, such as being heated up or exposed to UV radiation, all while being printed. Then there's the challenge of printing quickly enough such that the cells don't die before the transplant takes place!

The biggest challenge of bioprinting so far has been figuring out how to recreate the tiny blood vessels that are vital to making organs work.

Nevertheless, despite the many complexities it entails, substantial strides have been made in bioprinting.

At Rice University, a team led by Dr. Jordan Miller has been trying to solve the blood vessel problem by using a gel to encase a dissolvable sugar that would then dissipate and help cells stick to the walls of the veins. While Dr. Miller's work is still ongoing, he's already been able to print some of the body's thicker blood vessels.

Meanwhile, companies like _Organovo_ have used bioprinting to recreate human cells to test new pharmaceuticals on. This is a significant advance in removing human beings from the potentially deadly business of drug trials.

In addition, the thin parts of the human body, such as the cartilage and heart valves, have already been successfully printed, and a group at Princeton University has even 3D printed a human ear.

### 10. While still far from upgrading the human brain, scientists have made remarkable strides with brain-computer interfaces. 

In sci-fi movies, we've seen characters plug things into their brain in order to quickly download life-saving information. However, the more we learn about the brain, the less likely it seems that we'll ever be able to hook ourselves up to a thumb drive for a cranial download of the Encyclopedia Britannica.

So instead, what scientists are focusing on these days is how to use brain-computer interfaces to fix problems, such as blindness and paralysis, rather than enhance abilities.

One recent development has been _electrocorticography_, or ECoG, which has allowed paralyzed patients to control robot arms and move computer cursors solely through brain signals.

Scientists have also figured out ways to repair and redirect neurological information and correct issues of blindness, deafness and even dementia.

For example, a deaf patient can now receive a cochlear implant, which uses a small microphone to deliver sounds to a receiver in the patient's skin. The receiver then translates the sound into electrical signals that are sent to the inner ear. The result has been described as sounding like a low-quality cassette tape recording, which is still far better than silence.

Other enhancements are likely to arrive as we come to learn more about the brain.

However, current methods for understanding and treating the brain are so invasive that they are usually reserved for extreme cases where all else has failed, such as suicidal patients or ones with constant seizures.

One such procedure is called _deep brain stimulation_, and it uses a surgically implanted electrode that connects the brain to a battery under the skin. It sends high-frequency electricity to targeted areas in an effort to reduce neurologically induced symptoms, like seizures or the tics that accompany Tourette's syndrome.

Curiously, the results suggest that the high-frequency signals can also improve a patient's memory of spatial information.

Other studies suggest that less invasive techniques, like external magnetic stimulation, which places magnetic or electric fields over a patient's head, might also improve memory and cognitive abilities.

So far, the data has been inconclusive, but as with other scientific and medical breakthroughs, only time will tell.

### 11. Final summary 

The key message in this book:

**Incredible research into future technologies is currently being conducted all over the world, yet it tends to fly under the radar of our usual news feed. By exploring the technologies of the future, we can think about what the future might look like and, more importantly, consider the ethical dilemmas that may be in store for humankind.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _What If?_** **by Randall Munroe**

In _What If?_ (2014), Randall Munroe presents earnest, thoroughly researched answers to absurd, hypothetical questions in a highly entertaining and digestible format. Munroe serves up the most popular answers from queries he received through his _What If?_ blog, along with a host of new, delightful, mind-bending questions and answers.
---

### Kelly Weinersmith and Zach Weinersmith

Dr. Kelly Weinersmith works in the BioSciences department at Rice University, and is the cohost of _Science . . . Sort Of,_ a top-rated science podcast. Her research has been featured in the _Atlantic_, _National Geographic_ and _BBC World_.

Zach Weinersmith is the creator of the popular webcomic, _Saturday Morning Breakfast Cereal_. His work has also been featured in a variety of publications, including the _Economist_, the _Wall Street Journal_, _Slate_ and _Forbes_.

