---
id: 56f3afbb2bf5b9000700000f
slug: southern-theory-en
published_date: 2016-03-28T00:00:00.000+00:00
author: Raewyn Connell
title: Southern Theory
subtitle: The Global Dynamics of Knowledge in Social Science
main_color: EDB43F
text_color: 876724
---

# Southern Theory

_The Global Dynamics of Knowledge in Social Science_

**Raewyn Connell**

In _Southern Theory_ (2007), sociologist Raewyn Connell investigates the emergence of the social sciences in the context of Western imperialism. She explains how sociological knowledge and theory was and is primarily produced from the perspective of the colonizers, and not the colonized.

---
### 1. What’s in it for me? Understand why sociology should be grounded in the society it studies. 

Sociology 101 is a familiar rite of passage for college students, but how many wonder where the discipline came from, or what its original purpose was?

Originating in the high colonial period of the nineteenth century, sociology has always been based on the perspective of European and American academics and observers. Usually, they're training their lens on those in poorer countries, with results that aren't as straightforward as they seem at first glance. 

So, let's explore the history of sociology and how it became a "Northern" theory of the rich and privileged, and see what the alternative — a "Southern theory" — can tell us about the world today.

In these blinks, you'll find out

  * the part imperialism played in the creation of sociology;

  * how a Nigerian poem sparked a theory about the pillars of society; and

  * what reading Donald Duck can tell us about the North's view of the South.

### 2. Sociology was created in the late nineteenth century at the height of Western imperialism. 

Any sociology major will tell you that sociology as a discipline began with three thinkers: Max Weber, Karl Marx and Émile Durkheim. But this isn't the whole story. What often goes unmentioned is that the social sciences emerged during a very specific period in history.

In the decades leading up to World War One, the empires of European countries and the US grew dramatically. The German, British and French empires expanded into Africa and Asia while the United States conquered parts of the American west and overseas territories such as Hawaii, Puerto Rico and the Philippines.

At this time, colonizers justified their actions by claiming they were modernizing and civilizing so-called "primitive" peoples. Asserting their position of white superiority, colonizers took it upon themselves to spread progress, as they believed other societies were inferior and unable to develop independently.

It was in this period that sociology was created in order for the empires to study the people they had colonized. Not only were colonizers claiming "new" land by violently obliterating resistance, but they also began observing the culture of the people there. For example, between 1898 and 1913 in the sociological journal _L'Année Sociologique_, over two-thirds of the reviews were about colonized territories. Durkheim, who founded the journal, was therefore only able to write about Algeria's Kabyle people because the French had conquered the country not long before.

Later in the early twentieth century, sociology was reinvented in American universities and became established as an academic discipline. It was only then that the works of Weber, Marx and Durkheim were prescribed for all sociologists. The new discipline required a curriculum, so the first sociology professors narrowed it down to these three academics. Consequently, any wider sociological perspectives were ignored and the violent and exploitative origin of sociology was covered up.

> _"Sociology was formed within the culture of imperialism, and embodied an intellectual response to the colonised world."_

### 3. Mainstream modern social theorists ignore the impact of colonialism. 

Although the old empires have since dissolved and colonialism has mostly come to an end, the world remains split into those who have power and those who don't. And this imbalance is still evident in sociology today.

Actually, sociology is conducted in largely the same way as it was in colonial times. That is, theories are formulated and published in the _metropole_ — the central cities of the empires — and these theories are then applied to formerly colonized territories. For example, some scholars still study "primitive" cultures from the perspective of a "civilized" observer. In other cases, academics sidestep indigenous and colonized cultures while still claiming _universality_ — the idea that concepts can be applied to every part of the world.

US sociologist James S. Coleman is one example of this approach to modern sociology. Known for his _Coleman Report_ on education, in 1990 Coleman published _Foundations of Social Theory_, in which he distinguishes the constructed or _modern_ societies from the peripheral or "natural" and "primitive" societies. Coleman also uses universality to compare, for example, nomadic sub-Saharan tribes sharing a camel to "Eskimos" handing out the parts of a slain bear, even though these cultures are completely distinct from one another.

In addition, Coleman fails to broach the subject of colonization and the effects of slavery on society. Although he claims his theories are universal, he ignores the colonization that affected the majority of the world and the implications it has for a universal theory.

Unfortunately, this type of modern sociology dictates that only theories from metropoles are considered legitimate. This means that a scholar in London or Chicago has no problem publishing work based on concepts created by European scholars in Europe and applying the data to Manila or Brasilia. However, a scholar doing the reverse has a slim chance of being published.

So how exactly did empire and colony become the global North and South?

### 4. Acknowledgment of global inequalities leads to a more truthful way of understanding the world. 

The world is becoming increasingly connected through modern technology, international corporations and trade agreements. All these things make geographical distances almost irrelevant, but unfortunately, our interconnectedness doesn't mean that we're all equal.

Around 1990, sociologists in the United Kingdom and the United States began using the term _globalization_ after it became accepted in business and economics publications. These sociologists claimed to explore the world as a new, global society. However, globalization theories still view the world from the standpoint of those in power.

Most of these theories aren't so different to the logic followed by colonizers. For instance, modernization is seen as sprawling out from the European and North American post-industrial countries into the developing nations, who embrace the progress. 

But this isn't an accurate representation of what is going on. By assuming that all societies want the same thing and advance in the same way, theorists disregard those who experience "progress" as something negative. Take the deforestation of the Amazon and the impact it has on the indigenous societies living there — is that progress? By overlooking the damage the developed North is inflicting upon the developing South, some sociologists fail to recognize how colonial history is repeating itself under the guise of a "universalized global culture."

Furthermore, splitting the world into North and South exposes global inequality. Much like the terms _First_, _Second_ and _Third_ _Worlds_, _The West_, or _developing_ and _underdeveloped_ countries, it doesn't point to a geographical place, but to an unequal relationship that began with European imperialism.

Seeing the world as being unequally divided into North and South doesn't ignore the perspective of the powerless. This is _Southern theory_ : knowledge created in places regarded as less legitimate by the powerful Northern countries.

So has anyone taken this approach to sociology? Yes! The South has developed some important sociological theories in the last few decades, and, in the following blinks, we'll look at three of them.

> _"With few exceptions mainstream social theory sees and speaks from the global North."_

### 5. During the African Renaissance, African intellectuals produced social theories based on indigenous cultures. 

The political tensions within and between the many nations of Africa are well known today. But before these conflicts, cultural struggles were widespread as African nations fought against European conquest. These movements are known as the _African Renaissance_, a term which can refer to two historical instances.

Some use the term African Renaissance to describe the time in the 1990s when African politicians focused on African identity, democracy and development. For example, South African president Thabo Mbeki, Nelson Mandela's successor, used the term to refer to the economic and cultural development he envisioned, which included the emancipation of African women.

Others use the term to refer to earlier Africanist movements like _négritude_ and _pan-Africanism_.

This interpretation considers the African Renaissance as a twentieth-century movement to reaffirm indigenous African culture and knowledge. 

So how was the African Renaissance treated in sociological theory?

Scholars like Akinsola Akiwowo fought for theories based on indigenous African societies. Instead of using European and North American concepts to analyze African societies, Akiwowo proposed applying indigenous African culture concepts to the rest of the world. He translated a ritual poem about a creation story written in a Yoruba language and commented on the work's terms and meaning.

Central to Akiwowo's interpretation was the concept of _asuwada_, which he defined as "the purposive clumping of diverse _iwa_ " — which loosely translates to "beings" — as a basis for how parts of Nigerian society are created. Basing social theories on local culture was a reversal of the North/South relationship which sociology had used since its inception.

Akiwowo's work was one effort in the African Renaissance that sought to describe the experience and culture of colonized peoples from their perspective rather than from the standpoint of the colonizers. ****

### 6. Latin American sociologists explored the continent’s dependency on the United States. 

Few can deny the economic and political power the United States has maintained for over a century. But less well-known is the way the Northern superpower's culture has asserted its dominance over the global South.

One sociological study that explored this relationship was _How to Read Donald Duck_ by Chile-based sociologists Ariel Dorfman and Armand Mattelart.

Originally published in Spanish and later translated into English, the study analyzed 100 issues of Disney comic books. Dorfman and Mattelart argued that Disney comics revealed the way the United States saw Latin America and the Third World. Take, for instance, the class hierarchy depicted in the cartoons: it never changes. Scrooge McDuck is always rich and the lower class Beagle Boys are always thieves.

In the cartoons, uncolonized societies are portrayed as exotic, primitive and unable to fend off villains, with half of the stories centering around the main characters visiting places like "Aztecland" and "Unsteadystan." The rich ducks always swoop in to save the "noble savages" from the bad guys, by, for example, building a resort that the locals can run in order to save their village.

According to Dorfman and Mattelart, the comics culturally perpetuated the social structure and global dominance of the North as, at the end of each adventure, the ducks always restored their world's status quo, keeping the rich wealthy and the poor unlawful.

In a way, these comics blinded people in Latin America to exploitation, and undermined the subcontinent's chances of freeing itself from its dependence on the North. _How to Read Donald Duck_ was considered so radical that Disney tried to have translated copies of it seized in the United States.

> _"Reading Disney is like having one's own exploited condition rammed with honey down one's throat."_ — Ariel Dorfman and Armand Mattelart

### 7. In India, the Subaltern Studies Group investigated society from the perspective of the disadvantaged. 

Once dubbed "the jewel in the crown," India was an invaluable colony of the British Empire. So when it gained independence in 1947, the subcontinent became an inspiration to the colonized world. 

But its colonial past continued to leave a cultural imprint long after independence, especially on Indian historiography.

In the 1980s, the radical Indian historians of the _Subaltern Studies Group_ wanted to initiate a discussion of Indian history from the perspective of the poor and oppressed. The group therefore focused on the political experience of Indian subalterns, like the working class and peasant movements.

Highlighting this alternative perspective, Gautam Bhadra's work _Four Rebels of Eighteen-Fifty-Seven_ is based on the records of the peasant uprisings in that year. Bhadra uncovered the stories of villagers like Devi Singh, who, along with the leaders of the peasant uprising in Raya, launched an attack on moneylenders and the British authorities.

The uprising was quashed, the leaders hanged and their story kept quiet until Bhadra's study. The Subaltern Studies Group managed to gather momentum and eventually became well-known in North American universities, where it was acclaimed by postcolonialism scholars such as Edward Said.

The biggest challenge, however, was that most subaltern groups didn't leave written records of their movements. So, the Indian scholars researched subaltern history by examining the records of the colonial elite.

For example, in _Conditions for Knowledge of Working-Class Conditions_, historian Dipesh Chakrabarty argued that the lack of officially required documentation for the jute workforce in early twentieth century Calcutta exposed the shady methods of the elite. They didn't want to leave a paper trail. Therefore no records remained.

In this way, the Subaltern Studies Group could reveal the histories of the powerless despite the fact that they had been silenced or disregarded by the elite.

These are just some recent examples of social theory from the global South. And although there have been more, there's still a long way for Southern Theory to go.

### 8. Sociology must recognize power inequalities, and respect all forms of knowledge. 

We've seen that what we think is legitimate or true is largely determined by the place and people involved in the production of knowledge. So how can sociologists keep a balanced perspective on this?

First, the social sciences must recognize the relationship between the periphery and the metropole.

The pattern of global inequality needs to be explicitly named in order to undo the supposition that theories written by the privileged are universal.

Sociologists must acknowledge the inequalities in power, wealth and culture that originated in colonialism and that the social sciences don't reflect the experience of the great majority who suffered during colonization.

Second, sociologists must acknowledge the existence and diversity of the periphery. Not only must claims of universal validity be withdrawn, but particular situations in the periphery must also be addressed. Take the Australian aboriginal people, who are the most marginalized group in Australia. According to a 1997 study by Michael Dodson, the life expectancy of Australian aborigines is 18 to 20 years shorter than that of other Australians. They're also three times more likely to be unemployed. What's more, 38 percent of aboriginal communities have a water supply that fails to meet WHO standards. This isn't a matter of having the right resources; it's the inability of the privileged elite to recognize an urgent problem.

Lastly, social sciences must document the histories of the oppressed. It's vital that these narratives are more than just description, as in European scholars' accounts. The goal should be to educate people about one another from an equal footing.

The oppressed must have their experiences and politics voiced instead of having them erased from the history books. Eugene Genovese's 1976 book _Roll, Jordan, Roll: The World that Slaves Made_ is one example in which the situation of the oppressed — in this case, slaves in the American South — is told.

Before an equal exchange of knowledge can take place, scholars must recognize the history of colonialism and its flawed universal claims.

> _"The development of social science involves an educational process, which we now need to think about on a world scale."_

### 9. Final summary 

The key message in this book:

**Sociology emerged during, and because of, European colonialism. Since this time, sociological knowledge and theory have been valid only when produced by the metropoles of the North, particularly Europe and North America. In order for an equal exchange to occur between the global North and South, sociology must both acknowledge and rectify this inequality of power.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Half the Sky_** **by Nicholas D. Kristof and Sheryl WuDunn**

_Half the Sky_ is about unlocking the greatest untapped resource on Earth: women. It outlines some of the most serious problems facing women throughout the world, such as human trafficking and gender-based violence, and why it's so difficult for the world to overcome them, and what we stand to gain if we do.
---

### Raewyn Connell

Raewyn Connell has served as an advisor on UN initiatives and was a founding professor of sociology at Macquarie University, Australia. Currently Professor Emeritus at the University of Sydney, she is a renowned scholar in Southern theory, as well as in gender and masculinity studies. The Australian Sociological Association named the biennial Raewyn Connell Prize after her in 2010.

