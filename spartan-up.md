---
id: 562e109f3864310007010000
slug: spartan-up-en
published_date: 2015-10-26T00:00:00.000+00:00
author: Joe de Sena
title: Spartan Up!
subtitle: A Take-No-Prisoners Guide to Overcoming Obstacles and Achieving Peak Performance in Life
main_color: E42E48
text_color: C92840
---

# Spartan Up!

_A Take-No-Prisoners Guide to Overcoming Obstacles and Achieving Peak Performance in Life_

**Joe de Sena**

_Spartan Up!_ (2014) is an in-depth guide to living life to its fullest by challenging yourself and being the best person you can be. The "Spartan" way of life draws inspiration from the Spartans of ancient Greece; implement their philosophies in a modern context to live a healthier and more fulfilling life.

---
### 1. What’s in it for me? Discover how the mentality and practices of extreme sports can make you a modern Spartan. 

When people think of ancient Greece, many think of Greek gods, philosophers or maybe famous plays, but some definitely think: _This is_ _Sparta!_

This quote from the movie _300_ came to internet fame in 2006 and since then, the movie's fierce Spartan warriors have been a household image. Sparta was the strongest military force among all the ancient Greek city-states, and it was indeed known for the strength, energy and mentality of its soldiers. But how can we apply their example to our modern lives?

We can take the image of hardened Spartan soldiers as an inspiration for how to lift our modern, soft and comfortable lives out of everyday mediocrity. Through the lens of extreme sports, _Spartan Up!_ gives us ideas on how we can lead more exciting and fulfilling lives.

In these blinks, you'll discover

  * how the author still remembers the name of the woman who was a janitor at his university;

  * why the roots of honesty are essential for a modern Spartan; and

  * why we should live by the ideals of the Greek philosopher Epictetus.

### 2. Avoid comfort, challenge yourself and stay disciplined. 

The people of Sparta, the ancient Greek city-state, defended their homeland through intense physical and mental preparation. It might be thousands of years later, but you can still learn from their legendary abilities and become a Spartan yourself.

Life's comforts make our existence predictable and boring. They disconnect us from our true passions and goals, but you can overcome that lethargy by taking a real jump into life. 

That jump could be extreme sports, for instance. Joe de Sena, the author, organizes various extreme sports events, such as the Death Race. Death Race isn't just an over-the-top name — participants actually sign a waiver certifying that they know they could die during the challenge. 

The Death Race contains several events, such as diving for pennies in cold water, walking 50 kilometers and lifting rocks for hours. Afterwards, de Sena often gets grateful messages from participants who say it changed their lives for the better. 

However, you'll only be able to handle physical challenges like this if you have strong endurance skills. So build your endurance through self-discipline and by delaying your own gratification. 

It's easy to get quick satisfaction in the modern world, but resisting instant gratification is an important part of personal development; Walter Mischel's famous marshmallow experiment does a good job of illustrating this. 

In the experiment, Mischel presented children with a marshmallow. The children were told that if they waited to eat it, they'd get another. Mischel then left the room to see what the children would do when left alone with the treat. Years later, Mischel follows up on the lives of these children.

The study showed that the children who had the self-discipline to resist the marshmallow grew up to be wealthier and healthier than those who didn't. Challenges in adulthood are similar: you have to resist immediate gratification and hold out for something better. 

At any given time, life can be easy or challenging — but it's up to you to make life _fulfilling_. You never know when hard times will hit, so keep building your endurance skills. You'll be more prepared for challenges and more adventurous, too.

### 3. Stay focused on your goals by planning in advance. 

The first part of meeting a goal is planning for it. Plans can tell you where you're going. 

Sign up for activities in advance whenever you can, like boxing lessons or dance workshops. When you've committed to something in advance, you're forced to be honest with yourself if you don't go. 

The word _honesty_ comes from honor, a core value in any Spartan's life. Part of leading an honest life is following through with what you've said you'd do. 

When following your plan, keep a _success-and-failure journal_ where you can record your progress and give yourself credit for any achievements. 

Anyone can be a part of your plan. One of de Sena's college professors taught his students the value of this when, on a written test, he asked the name of the woman who worked as a janitor at the university. 

The students assumed it was a joke — but it wasn't. The professor wanted his students to appreciate everyone who works to make their lives better and is therefore a part of their plans. That's how you build strong relationships, which are a key component of realizing your goals. 

The lesson had a profound impact on de Sana, and he remembers that woman's name to this day: Sarah. 

In terms of health, planning your diet is just as important to your fitness as planning your workout. Don't get sucked into overcomplicated fads; diet plans should be simple. 

De Sana disagrees with most mainstream diet tips. He believes that diets should be restricted to fundamental ingredients like water, raw fruits and vegetables. 

Planning your meals is especially important because you don't always have access to good food. If you only want the best, you have to plan ahead and spend time cooking or finding healthy restaurants. 

Spartans plan so they can get the best out of their efforts. This doesn't just apply to sports and health — it's true in all aspects of life.

### 4. Society encourages us to be passive and mediocre, through bad diets and a lack of exercise. 

Which feels better: a day spent lounging on the couch or a day spent working out and eating healthy food? The first might be more comfortable in some ways, but it's certainly less healthy. 

Most people eat processed, unhealthy foods that might taste good in the moment but cause serious long-term problems. Almost every standard food product has unnatural amounts of sugar: juice, sauces, sodas or cakes are just a few examples. 

Sugar causes your brain to produce more _dopamine,_ so it's easy to get addicted to it. And a sugar addiction increases your risk of developing diabetes. 

This trend has become a very serious problem. Since 2000, the number of Americans who have diabetes or prediabetes has risen from nine percent to 23 percent. 

Eating right isn't just about your physical health — it improves your mental health, too. A true Spartan has strong willpower, and studies have shown that willpower is strengthened by healthy foods like greens, fruits and nuts. 

After all, our bodies are designed to extract nutrients from those kinds of foods, not from unnatural foods with colorants or additives. 

Unhealthy food is one of modern society's major problems, but most of us have another serious issue on our hands: we're _passive_. Passiveness makes our lives dry, dull and insignificant. 

Children naturally avoid being passive. They exercise without realizing it; they explore, take risks and discover things for themselves. 

But we lose these abilities as adults. Instead, we adopt society's standards that tell us to avoid anything that's new or that requires effort. This leads us into a life of mediocrity. 

Modern-day ailments like stress, depression, lethargy and anxiety can all be relieved through exercise. When you exercise, your brain produces a substance called _brain-derived neurotrophic factor_, or _BDNF_. 

BDNF makes your stress levels drop, so exercising on a regular basis will help you stay mentally fit, too.

> Low-income households had more free time in 1999 than in 1965. Unfortunately, most of this new free time was spent watching TV.

### 5. We can achieve big things with small steps and repetition. 

Have you ever heard the phrase "less is more"? This saying isn't always true (such as during repetitive training) but in most aspects of life, the simpler path is the better one. 

That's because you limit yourself when overanalyzing things. It's simpler and more effective to just act. 

De Sena is often asked how exactly he achieved certain goals, and he responds by saying that he did it without thinking. This is especially the case when it comes to extreme activities like running marathons on two consecutive days with no sleep! Overanalyzing the feat would just slow him down.

One great strategy for achieving your goals is to just keep trying until you succeed. Repetition allows tasks to become hardwired into your brain's neural pathways, so the task at hand becomes easier each time you do it. That's a key part of reaching your goals!

De Sana was rejected by colleges three times in a row because of his severe Attention Deficit Disorder, but he kept trying. On his fourth attempt, he got in. He never begged anyone or tried to bargain; he just applied again and again.

Leading a simpler life would also make you happier. 

Society teaches us that happiness results from external factors, and it also teaches us that we never have enough. The outlook that results from the combination of these two ideas can make even the richest people feel miserable. 

In fact, de Sena came from a rich family, but his wealth spoiled him and made him unable to appreciate the little things in life. Happiness is really about feeling satisfied with what you have, which is why Epictetus, the Greek Stoic philosopher, once defined happiness as having few wants. 

So keep things simple. Complex problems don't always require complex solutions. In the realm of exercise, squats are a great example. They consist of one simple movement, but they train nearly every important muscle in your body.

### 6. Don’t disregard your safety or the safety of others. 

When you push yourself to the limits of your physical abilities, you might accidentally go too far. So how should a modern Spartan think about safety?

There's always a risk when it comes to extreme activities. Sometimes people get too excited or panic and go over the edge, causing serious harm to themselves or others.

It's important to be motivated, but you also have to evaluate situations rationally so you can prevent injuries to the greatest extent possible.

Some Death Race attendees, for example, are incredibly irresponsible. They continue racing when they have life-threatening injuries or after the organizers tell them to stop. The organizers have to try to make such participants leave the event as quickly as possible.

In life, there are perceived limits and real limits. You need to know and respect the difference.

So accept a real limit you have now, but keep trying to succeed in the future. You or your circumstances can always change, and you might be able to overcome your previous limits. In fact, de Sena wasn't athletic in his youth, but he never gave up and eventually surprised even himself with his skills in extreme sports.

You can push yourself but you can't force yourself to work endlessly. Make sure you take in the right nutrients so you can push yourself in an effective way. Low energy levels can limit you.

Energy drinks, for instance, are useless. They'll give you a quick, short-term high but the glucose in them won't help you in the long run.

Finally, don't complain about things you can't control; a real Spartan doesn't waste their time on such things. In fact, the expression _stoic calm_ comes from the ancient Greek stoic philosophers' acceptance of the more difficult sides of life.

### 7. Final summary 

They key message in this book:

**Never give up! Life can be tough sometimes but you have to be even tougher. Outline a plan for yourself, stick to it and stay dedicated. Keep yourself mentally and physically healthy and don't succumb to the modern life of boredom and mediocrity. There's still a place for Spartans in today's world.**

Actionable advice:

**Go outside your comfort zone.**

Push yourself to do something extreme! It could be related to sports, education, travel or anything else, as long as you're interested and you're not used to doing it. Travel to Niagara Falls or read a psychology dissertation. Push your limits and remember: comfort is the enemy!

**Suggested** **further** **reading:** ** _The Obstacle is the Way_** **by Ryan Holiday**

In _The Obstacle is the Way_, Ryan Holiday brings the age-old wisdom of Stoic philosophy up to date. By examining the struggles of historical figures of inspiring resilience, Holiday shows not only how obstacles couldn't stop them, but more importantly, how these people thrived precisely _because of_ the obstacles. Holiday shows how we can turn obstacles to our advantage, and how we can transform apparent roadblocks into success, both in our businesses and our personal lives.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Joe de Sena

Joe de Sena is the CEO and co-founder of _Spartan Race_, a series of obstacle courses held throughout the world. He was born into a wealthy family but lost everything in the stock market crash of the 1980s, before working his way back up to the top by co-founding and growing his own multi-million-dollar business.

