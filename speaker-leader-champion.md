---
id: 53cf783b3630650007850000
slug: speaker-leader-champion-en
published_date: 2014-07-22T00:00:00.000+00:00
author: Jeremy Donovan and Ryan Avery
title: Speaker, Leader, Champion
subtitle: Succeed at Work Through the Power of Public Speaking
main_color: B4242A
text_color: B4242A
---

# Speaker, Leader, Champion

_Succeed at Work Through the Power of Public Speaking_

**Jeremy Donovan and Ryan Avery**

This book examines top public speakers' most successful speeches to see what makes them great. It offers detailed tips for improving public speaking skills for everyone, whether you're a beginner or have years of experience.

---
### 1. What’s in it for me? Learn how to give great speeches and presentations. 

These blinks will show you how to become an effective public speaker, and the benefits to be gained from learning to speak well. Using the speeches of past world champions, these blinks explore the basics of creating great content, structuring that content well, and using strategic verbal and nonverbal delivery techniques to fully engage your audience in what you're saying. You'll also learn how your manner of speaking should fit with the setting, atmosphere, and occasion of your speech.

In these blinks, you'll find out:

  * how being a good public speaker can get you a job promotion, and

  * how the author went from stammering through his first speech to becoming a world champion.

### 2. Public speaking is a vital skill for any professional. 

Have you ever had to give a speech in front of a large audience? Or have you ever had to convince others with an enticing presentation? Many jobs require you to speak in front of your fellow employees, and improving and refining these skills is critical to advancing in your career.

From the very start of your working life, in your first job interview, public speaking has been crucial. Research has shown that one of the most important skills that employers look for during interviews is the candidates' ability to give clear speeches and presentations in front of bigger audiences.

After you've been hired, public speaking remains key to standing out at work. This is clear from the results of the Job Outlook 2013, a survey of over 240 employers. The ability to communicate verbally, inside as well as outside the company, was ranked as the most valued skill by far, rated 4.63 out of five potential points.

Good public speaking skills will also help you advance in your career. If your employers are looking to promote someone, they're much more likely to choose someone who stands out, and good public speaking skills will get you noticed. This will give you an edge over any colleagues who might also be in consideration.

The authors of this book are living examples of this, though in quite different ways. Jeremy Donovan advanced in his career in a corporate environment, and Ryan Avery trained at Toastmasters, a club for people looking to improve their speaking abilities. Through his training with Toastmasters, Avery went on to become one of the most successful professional public speakers of our time.

Learning how to speak confidently, clearly and convincingly in front of an audience is an enormously useful skill that will be critical at all points of your career, and it can make you a highly regarded professional.

### 3. Your confidence and skills will improve as you practice public speaking and get feedback. 

We all have to speak in front of audiences sometimes, whether it's at a school presentation or a business meeting. It's normal to feel nervous in these situations, and we can all work on improving our public speaking skills.

You'll improve your capabilities in producing and delivering a speech as you practice public speaking. A strong speech isn't just about a good topic and accurate facts. You'll also learn how to maintain eye contact, keep an authoritative but approachable kind of posture, project your voice well, and use humour appropriately. You'll learn how to correctly structure a speech, which will make it easier and more interesting for the audience to follow.

Honing your public speaking skills will also increase your self-confidence. One way to build your confidence is to make sure you always take feedback seriously, and use it to improve your weaker areas. Over time you'll feel more at ease with public speaking, as you'll understand better how others perceive you.

Ryan Avery, for example, became successful largely because he was able to use feedback to know what areas to improve. In the first speech he ever delivered, he stammered often and kept using unnecessary filler words like "umm . . ." that distracted the audience. As you might imagine, this was torturous for both the audience and himself.

After this, he used the evaluations he received from the other Toastmasters to improve his speaking skills. Only a few years later, the formerly introverted Avery, who'd barely been able to speak to strangers without getting anxious, had become a world champion in public speaking. Public speaking is now a great passion of his and his profession!

As you keep improving with feedback, like Avery, remember that feedback won't always come in the form of criticism. As you go on, you'll probably find people telling you that they enjoyed your presentations. This too is valuable feedback, so take it seriously: give yourself a pat on the back.

### 4. Choose a topic that’s interesting to both you and your audience. 

The first question you'll probably ask yourself before you have to craft a speech is: What in the world am I going to talk about? Well, the secret to creating good speech content is to find a topic that will interest your audience, but that's also interesting to you personally.

First, ask yourself who your audience will be. They're the ones you're hoping your speech will affect. For example, when you give a speech to your family at Christmas, you'll probably talk about love or the value of a close family, not your company's new business strategy. This way, your audience is more likely to feel appreciated and inspired.

Second, be an _expert_ who _speaks_ rather than an expert speaker. This means not only being knowledgeable about your topic, but also being genuinely interested in it. If you speak about microbiology, but actually hate studying science, you'll lack the motivation and passion necessary to deliver a good speech. Even if your structure is solid, your words are well chosen and your facts are correct, a speech without passion will never really inspire others.

For example, Nelson Mandela's speeches about his dream of a free and democratic South Africa were deeply moving because he dedicated his life to fighting passionately for that dream. He wouldn't have been able to speak well about the newest iPad, just as Steve Jobs would've sounded awkward speaking about racial segregation. Both were excellent speakers because they gave speeches on topics they had real passion for.

The topic is the backbone of every speech, and if you choose one that's important to you, your speech is much more likely to be great.

> _"Tell me a fact, and I will learn. Tell me a truth, and I will believe. But tell me a story, and it will live in my heart forever."_

### 5. Keep your audience engaged by making sure your speech is focused. 

Sometimes a speech can have generally good content, but still seem to waver and lose the audience's attention. Why? Because speakers often put in too much unnecessary information, instead of focusing on the core of what they really want to say.

Focus is the key to a great speech. A seven minute speech, for example, really shouldn't have more than one main message. If it does, you won't have time to completely flesh out your points, and convince your audience of what you're saying. To stay focused, make sure you don't put in unnecessary information, such as irrelevant remarks or side-stories — this will make the audience's attention waver. The ultimate goal of your speech is to affect your audience, so distracting them from the message you want to send with superfluous information is counterproductive.

Instead, try to streamline your speech by focusing on _one_ _message_. Here's a general rule of thumb based on the experience of many successful speakers: You should be able to express your main message in ten words or fewer, so make sure to really focus on what's essential.

For example, public speaking champion Craig Valentine once gave a speech with the core message "Your dreams are not for sale." This powerful and succinct message stuck so well that years later he was still being approached by people who'd been in his audience, and who told him about how deeply his speech had affected them. Some had declined job offers because they'd realized that having a high-paying job wasn't worth sacrificing their dreams.

So, the best way to engage your audience is to focus on having one strong and catchy message. Be the one they still remember years later, because your speech changed the way they think about life.

### 6. Build a connection with your audience using both verbal and nonverbal techniques. 

Have you ever been in an audience for a speech that really stuck with you? Did you feel a powerful bond with the speaker? That strong connection between the speaker and audience is what you'll want to strive for in your speeches. You can build and maintain this connection by using both verbal and nonverbal skills.

The first thing you'll want to consider on your way to building that connection is how you'd like to come off to the audience. Do you want to be a well-informed educator? Do you want to move the audience emotionally?

However you'd like to come off, there are some basic tactics to use, many of which are non-verbal.

Be sure to maintain good eye contact and have good posture. If you can, use the room itself to your advantage. The 1995 public speaking world champion, Mark Brown, delivered a speech that really incorporated the large auditorium he performed in. He used very dramatic body language so that he could be seen by those further away, and took advantage of the acoustics in the large room by varying the volume of his voice depending on the subject matter.

Of course, there are also verbal techniques you can use to better connect with your audience. Change your tone and pace to suit what you're saying. In general, you should lower your voice when you want to build suspense, and raise it when you get to the climax or key message.

Be sure to also play with your word choice, and your volume level and intonation. Sometimes having a catchphrase, or repeating certain words periodically, can make the main point clearer to your audience, and make your speech easier to follow.

Also, memorize your speech, because you can focus more on engaging the audience if you aren't struggling to remember what to say.

So, while a speech definitely needs good content, the way you express it can be just as important.

> "_Your voice is only the first part of_ how you say it _._ "

### 7. Work with a speaking coach – they can make a huge difference to your progress and development as a speaker. 

Training your public speaking skills is just like training for a sport: you need to practice continually and work with a coach who can guide you.

Working with a coach who speaks at the level you're trying to reach (or even at a higher level) will definitely help you progress. Speaking coaches are usually experienced and successful public speakers themselves, so you can work on trying to get your speaking skills up to their standard. On top of that, they've also learned the best techniques for passing their own skills onto others.

Many successful speakers, such as Ryan Avery, often attribute their success to the training they got from their coaches. Coaches can be a great source of motivation to improve.

In addition to teaching speaking techniques, a coach will know how to give you very effective feedback. Being evaluated is essential if you want to improve, and a coach will give you very useful feedback. They'll know how to pinpoint the skills you should work on, and can help you improve in even the most challenging areas, such as failing to engage your audience.

The Toastmasters method has been successful largely because of its emphasis on working with evaluations. At Toastmasters, all members provide evaluations for each other, so they can reflect on what they need to improve on, but also on what's going well.

Criticism might make you feel nervous or self-conscious sometimes, but don't be afraid of it! Feedback — especially if it comes from an experienced speaking coach — will make you more confident in the long run. So don't fear it, embrace it!

### 8. Final summary 

The key message in this book:

**With** **practice** **and** **dedication,** **anybody** **can** **gain** **the** **skills** **necessary** **to** **become** **a** **great** **public** **speaker.** **These** **skills** **will** **improve** **your** **confidence,** **and** **help** **you** **in** **a** **range** **of** **situations,** **from** **making** **your** **family** **feel** **loved** **and** **appreciated** **over** **the** **holidays,** **to** **getting** **you** **that** **new** **promotion** **at** **work.**

Actionable advice:

**Nervous** **about** **public** **speaking?** **Do** **it** **more!**

Give yourself more opportunities to speak, so you can gain more feedback about your speaking abilities. Receiving more feedback will help you identify your strong and weak points and let you know how you come off to the audience. You'll know which areas to work on, and you'll feel more confident over time.
---

### Jeremy Donovan and Ryan Avery

Ryan Avery is professional speaker and a former champion of the Toastmasters' Public Speaking World Championships. Jeremy Donovan is also a professional speaker, and the author of the bestselling book _How_ _to_ _Deliver_ _a_ _TED_ _Talk_.

