---
id: 53eb51043634330007bd0000
slug: spillover-en
published_date: 2014-08-12T00:00:00.000+00:00
author: David Quammen
title: Spillover
subtitle: Animal Infections and the Next Human Pandemic
main_color: None
text_color: None
---

# Spillover

_Animal Infections and the Next Human Pandemic_

**David Quammen**

_Spillover_ takes a look at where the world's most deadly diseases come from, explaining how humanity is at risk from viruses and bacteria hiding in animal populations. It also shows that the closer we get to the natural habitats of wild animals, the greater our risk of coming face to face with deadly foes: pathogens.

---
### 1. What’s in it for me? Learn where deadly diseases come from. 

It seems like clockwork: every few years, there's some extreme viral or bacterial outbreak that threatens to wreak havoc on civilization, and often _does_ cause a humanitarian disaster in the areas most affected. But what is it that causes diseases to make their sudden appearance and then slip back into hiding just as quickly?

These are the questions the author aims to answer in _Spillover_. In his near ten-year search to gather knowledge about the major diseases that have most recently affected modern civilization — SARS, HIV, ebola, hendra, nipah and malaria — he found a common thread: these diseases are, or are suspected to be, _zoonotic_ diseases, i.e., ones that jump between animal species and into human societies.

These blinks will lay down the foundations for what pathogens are and how they work. You'll also discover how our interactions with the natural environment make us susceptible to new exotic diseases, and how the ever-increasing global population and continual urbanization may foster the next global pandemic.

In these blinks, you'll learn:

  * why a diet based on chimpanzee meat could seriously damage your health,

  * why the Chinese government wrongly killed thousands of civet cats, and

  * why we run a greater risk of creating a global pandemic now than ever before.

### 2. Diseases can sometimes jump from animals to humans: a phenomenon known as zoonosis. 

Think about the last time you got sick: How did it happen? Did you kiss someone with a cold? Did a kid sneeze on you? Or maybe you didn't catch it from a fellow human at all: maybe you got a _zoonosis_ from a non-human animal!

Zoonosis occurs when _pathogens_, or tiny, disease-causing entities such as viruses or bacteria, switch their targets. Here's how it works:

Generally, pathogens tend to infect particular species, similar to how predators, such as lions, prefer to hunt a particular type of prey.

However, sometimes a predator will deviate from its preferred meal. For example, although we often don't appear on a lion's menu, they certainly wouldn't scoff at the opportunity to dine on tasty human flesh should such an opportunity arise. Pathogens act in a similar way, changing targets with changes in their environment.

The outbreak of mad cow disease is one such example of how a pathogen usually associated with cows successfully infected humans. The toxocariasis infection, too, can be passed from dogs to humans via the pathogen called roundworm.

Zoonotic pathogens are also especially dangerous, as they can "hide" in animals without causing them harm, thus making it impossible at times to know when and where they appear or reappear in human populations.

The ebola virus, among the most deadly viruses known to us, often appears and disappears suddenly, emerging from the animal kingdom only to quickly disappear again. Consider the latest outbreak in West Africa in 2014, which occurred after many years without any ebola incidents.

Also distressing is the fact that zoonotic pathogens are quite common. In fact, a study published in the prestigious journal _Nature_ found that, from the years 1904 to 2004, approximately 60 percent of new emerging infectious diseases were zoonotic.

But how exactly do these pathogens work? The next few blinks will show you just that.

### 3. Although tiny, viruses are the most dangerous pathogens. 

Of all the existing pathogens, _viruses_ are the most dangerous.

Part of this has to do with their size: they're extremely small. In fact, a virus's diameter ranges from approximately 15 to 300 nanometers. To give you an idea of how big that is, consider the fact that the width of a single hair on your head dwarfs that, measuring between 40,000 to 60,000 nanometers.

In fact, viruses are so tiny that, for a long time, they were impossible to see with our scientific and medical instruments. Although the Spanish influenza of 1918–1919 caused up to 50 million deaths around the world, nobody at that time could pinpoint the origins of the illness. Only after the invention of the electron microscope in 1931 could viruses finally be detected.

And it's precisely the virus's size that makes it so dangerous in the first place: they are simply so small that they _need_ other living cells to survive.

With their tiny size comes a very limited genome. Compared to a mouse's genome, which has 3 billion _nucleotides,_ the basic building blocks of DNA and RNA molecules, a virus only has somewhere between 2,000 and 1.2 million.

Consequently, viruses don't have enough _amino_ _acids_, the proteins necessary for many critical biological processes, to survive on their own. In order to stay alive and thrive, they need to gain access to these proteins through other sources.

And so, viruses invade other living cells to find these proteins, often damaging and destroying the cells, which can lead to dramatic consequences for the host, such as illness and death.

The SARS virus, which has only 30,000 nucleotides, has to look for other cells to survive. When it found its way into the human population between 2003 and 2004, it brought terrible consequences, killing 774 people.

### 4. The genetic material of a virus determines how dangerous it is. 

Viruses, like all other organisms on the planet, are diverse and varied in their composition and survival strategies.

Generally, they can be divided into two categories: those with genetic material encoded in DNA and those encoded in RNA.

A virus encoded in DNA has a more complex genetic structure — namely, the famous _double_ _helix_, in which genetic information is stored in pairs. It is the same structure that forms the genetic material of our cells.

In addition, these viruses are outfitted with an enzyme called _DNA_ _polymerase_, which corrects genetic mutations, thus making them quite stable. One result of this stability is that viruses with a DNA code don't change very often.

In contrast, viruses encoded in RNA have a much simpler structure: rather than having the two parts of the double helix, they only have a single strand of genetic information.

What's more, the _RNA_ _polymerase_ is not as effective at correcting mutations as its DNA counterpart, making RNA viruses more prone to genetic mutations and thus causing them to constantly change.

Regardless of whether they contain DNA or RNA, both types of virus are incredibly dangerous.

Herpesviruses, for example, make up a large family of DNA viruses that can be quite dangerous. Herpes B, for example, goes straight to the brain and destroys cells there. If left untreated, Herpes B kills 80 percent of those it infects.

Contrast this with a famous RNA virus: HIV. In 2012 alone, HIV was responsible for an astonishing 1.6 million deaths.

While both kinds of viruses can result in death, RNA viruses have the potential to cause much more damage. The flimsier structure of their RNA make them more prone to genetic mutations, allowing them to evolve more quickly.

The speed at which these viruses evolve can sometimes even cause adaptations that allow them to jump between human and non-human populations.

As if that weren't enough, their ability to mutate quickly makes it very difficult for us to develop drugs to combat them.

### 5. The danger of a virus depends on its transmissibility and virulence. 

Why is it that some viruses cause huge pandemics and others don't? That all depends on the virus's transmissibility and virulence.

_Transmissibility_ refers to a virus's ability to jump from one host to another. There are various modes of transmission: vector-borne (malaria), sexual (HIV), airborne (SARS) and blood-borne (ebola), to name a few.

Discerning which mode of transmission is most advantageous for a virus depends on various factors.

For example, sexual transmission is great for viruses with a weaker structural make-up. It allows them to "live out their days" without ever having to leave a host, protected from potentially damaging environmental factors.

Airborne transmission, however, is arguably the easiest mode of transmission.

Similar to living organisms, a virus's goal is to survive and reproduce. Airborne transmission allows a virus to infect many hosts in a given area in a very short amount of time and thus replicate quickly.

However, if a virus kills all its possible hosts, it will also die.

Therefore, we must consider another important factor for the success of a virus: _virulence._

Virulence refers to the danger a virus poses: the more virulent the virus, the more likely it is to kill its host. High virulence isn't necessarily a good thing for the virus because it needs a sufficient number of hosts in order to replicate.

Let's consider, for example, a family of five rabbit viruses from 1950, each with differing degrees of virulence.

The most virulent killed nearly 100 percent of its rabbit hosts, while the least virulent killed only 50 percent.

But since the most virulent virus killed the host before it could be spread and the least virulent couldn't withstand the rabbit's built-up resistance, the virus in the middle of the virulence spectrum was the most successful of all.

In short, in order for a virus to survive and reproduce, it must find the optimal combination of transmissibility and virulence.

### 6. In spite of our growing arsenal against bacteria, they remain a real threat to humanity. 

So far we've learned about one of the pathogens which can cause us harm: viruses. In this blink, we'll learn about another: _bacteria_.

Although both are quite dangerous, bacteria and viruses are fundamentally different from one another.

First, unlike a virus, a bacterium is an organism that doesn't require other living cells to replicate. Instead, it reproduces, as most living cells do, through a process called _fission_, i.e., splitting in two.

Moreover, bacteria are considerably larger than viruses, outsizing them on average by a factor of ten.

And unlike with viruses, humankind has made great leaps in the battle against the bacterial threat: a great many bacterial infections can be treated with simple antibiotics, such as penicillin.

In spite of recent strides, bacterial outbreaks have caused immense harm to humanity throughout our history.

One such problematic bacteria is Lyme disease, which can affect humans' joints, skin, nervous system and heart. In the past, Lyme disease was a scourge to society, leading people to become so terrified that they took drastic steps to eradicate it. One of the more rash "solutions" had been to shoot dead as many deer as possible, because at the time people incorrectly believed that deer carried Lyme disease.

We now know that Lyme disease is transmitted to humans via bloodthirsty ticks, and it can easily be treated with antibiotics. In fact, 95 percent of cases now result in no long-term harm to the infected person.

Antibiotics haven't solved all our bacterial problems. Despite our best efforts to control their impact, bacteria could still prove dangerous to humanity.

In fact, a recent study conducted by the World Health Organization found a rise in antibiotics-resistant bacteria, which can't be killed easily. It is therefore likely that the bacterial threats of the future will once again be quite serious.

Now that you have a good understanding of how pathogens work, you're ready to investigate the most horrifying diseases ever to strike humanity.

### 7. The retrovirus HIV, which causes AIDS, spreads to humans from chimpanzees. 

Ever since its sudden mass outbreak in the twentieth century, AIDS has terrified societies across the globe. But where did it come from?

AIDS is caused by a _retrovirus_, a type of virus that hijacks other cells for the purpose of reproduction.

Upon infiltrating another cell, they convert their RNA into DNA. They then penetrate deep into the core of the cell, the _nucleus_, where they then implant their DNA, essentially "reprogramming" that cell.

Because the infected cells retain the rogue viral DNA when they replicate, this method of reproduction allows retroviruses to spread quickly through a body, causing widespread damage along the way.

In fact, both of the retroviruses that cause AIDS, HIV-1 and HIV-2, use this strategy to attack and undermine the hosts' immune system.

Although our first interactions with HIV are quite recent, HIV itself is very old, stemming from our primate relatives, the chimpanzees. In fact, many monkeys carry viruses similar to HIV: _Simian_ _immunodeficiency_ viruses, or SIV.

One such strain particular to chimpanzees is very similar to HIV, and is believed to be the source of the the human-affecting AIDS.

The _spillover_ of SIV from chimpanzees to humans is estimated to have occurred around 1908 in Cameroon, and was most likely the result of the butchering of a chimpanzee, either for food or a ritual. And from there, it has spread throughout most of the planet.

The spread of HIV was aided by the mechanization of syringe manufacturing during the 1920s, which enabled 2 million syringes to be produced globally in 1930.

At the time, syringes were both a wonderful blessing and terrible curse: the wide consensus that needle-delivered cures could benefit patients was counterbalanced by the lack of understanding regarding sterilization and infection.

In fact, in an attempt to inoculate the population against sleeping sickness, French physician Eugène Jamot and his team performed more than 1.2 million injections in Cameroon _without_ _changing_ _the_ _syringe_ _between_ _injections_ between 1927 and 1928.

As a result, the virus began its campaign of terror throughout the global population.

### 8. The deadly ebola virus may have been passed to humans from apes and bats. 

If you've been watching the news lately, you've likely seen coverage of the alarming 2014 ebola outbreak in West Africa and the numerous other outbreaks in the past few decades. People are _terrified_ of ebola — with good reason.

Ebola is a highly lethal virus and spreads quickly. It is easily transmitted, both through the air — by coughing or sneezing — and through the exchange of blood.

And once the virus has found its next victim, it will most likely kill them. In fact, the death rate for ebola is as high as _90_ _percent_.

Ebola also arrived on the scene relatively recently, with the first two outbreaks dating back to 1976. In both outbreaks, scores were killed before the virus simply disappeared, only to periodically re-emerge as time passed.

But where could this disease have come from so suddenly? Scientists believe the virus may have been transferred to humans via bats and apes.

In fact, scientists have found strong evidence that apes act as _amplifier_ _hosts_ for ebola, meaning that they host the virus before it spills over into the human population. In 1996, eighteen people in northeastern Gabon contracted ebola after butchering and eating the chimpanzees they had found dead in the forest. This suggests that they had contracted the virus from the dead chimp.

However, apes are not the actual source of the disease. And today it is still unclear as to where ebola originated from.

One hypothesis is that ebola came from bats, which would make them _reservoir_ _hosts_, or hosts that carry the viruses to new hosts without actually suffering any symptoms themselves.

It's possible that, as they feed on fruit, they contaminate their feeding grounds with the ebola virus, which is then picked up by apes. And so, the apes work as amplifier hosts, enabling the virus to spill over into the human population.

### 9. Malaria, one the most dangerous diseases on the planet, likely came to us from gorillas. 

The only time people living in the West give the disease malaria a passing thought is when they're planning a trip and need to get vaccinated. Yet, for many people across the world, the threat of malaria is an everyday reality. In fact, the disease kills more than half a million people every year.

Scientists used to be positive that malaria was a _vector-borne_ illness. A _vector_ is a carrier for pathogens, transmitting viruses from one host to another. In the case of malaria, mosquitos functioned as vectors, sucking an infected person's blood and then passing that blood on to someone else.

Certain parasites, called malarial parasites, are also known to transmit the disease. The most dangerous among them, _plasmodium_ _falciparum_, accounts for roughly 85 percent of Malaria cases.

As vectors merely carry the virus, and as we know of other non-human animals that were affected by the disease, scientists presumed that malaria was not a zoonosis, i.e., that it didn't spill over from animals to humans.

But recent evidence suggests that malaria might, in fact, be zoonotic. For example, a recent study found that a significant number, 37 percent, of western gorillas were affected by the malarial parasite.

What's more, the genetic similarities between the gorilla malarial parasite and the human malarial parasite suggests that malaria may have actually originated in gorillas, thus making it a zoonotic pathogen.

Of course, to someone dying of malaria, whether the disease is zoonotic or not may seem irrelevant. Yet, with this vital piece of information, scientists may be able to find new ways of stopping the disease.

> _"Pause to wonder why God devoted so much of His intelligence to designing malarial parasites."_

### 10. The SARS virus, which induced global panic, came from animals. 

Are you old enough to remember the worldwide panic surrounding the outbreak of the SARS virus? In 2003, the media was full of stories about how this dangerous disease would soon become a global pandemic that would kill millions. But where had it come from?

It all started in February 2003 on Air China flight 112 from Hong Kong to Beijing, where a man with SARS was on board who managed to infect 24 other passengers with his coughing fits. Soon, SARS had spread into 70 hospitals in Beijing alone, infecting almost 400 people.

Yet, in spite of the number of infections, SARS remained undetected. That's because it's a _coronavirus_, a type of RNA virus shaped like a crown.

This made it difficult for scientists uncover their patients' ailments: SARS was the first coronavirus that has ever been found to cause serious illness in humans, which is why they overlooked it in their analyses.

They simply didn't know what they were looking for. It was only after intense scrutinizing of their data that the virus was finally found.

They soon learned that, like many other horrible diseases, SARS had originated in animals, finding the first connection to the animal kingdom in the civet cats found in Asia.

Armed with this information, the Chinese government decided to prohibit the market sale of civet cats and 53 other kinds of _wild_ _flavor_, i.e., foods comprised of exotic meats such as civet cats.

Thankfully, this seemed to finally put a stop to the epidemic in the same year after 8,089 infections and 774 deaths.

It came right back, however, in January of 2004, which lead to the mass eradication of civet cats in order to completely exterminate the virus. Did it work?

No! Even after destroying all these cats, scientists discovered that horseshoe bats are a — or perhaps _the_ — reservoir host for SARS, and that civet cats were simply the amplifiers.

Now that we've seen how pathogens infiltrate the human population through the animal kingdom, these final blinks will look at our future interactions with nature.

> _"People in south China will eat everything that flies in the sky, except an airplane."_

### 11. Changes in ecology and the nature of pathogens means the battle against disease will never end. 

No matter how much we learn about viruses and bacteria, and no matter how many new methods we develop to combat them, there will never be an end to the fight. We will always face the risk of new viral and bacterial diseases or totally new types of diseases.

This is due, in part, to the fact that changes in _ecology_, i.e., the relationship between humans and the animal kingdom, will bring forth new threats.

One of these changes is caused by a changing habitat. For example, the deforestation of rainforests puts pressure on animals living there to adapt to their changing circumstances or face death.

Monkeys and apes, for instance, might be forced to adapt their primary habitat to one closer to human civilization. The resulting increase in encounters between apes and humans may also put humans in contact with unknown diseases carried by apes.

The same applies to mosquitos: if they're forced out of the rainforests, they'll have to rely on human blood as a tasty alternative to their customary diet. This could result in a dramatic increase of malaria incidences or other vector-borne diseases.

And as the shared living space of humans and other animals becomes more intimate, there is an increased chance that we'll incorporate new animals into our diets. Just as eating "bushmeat" has led to outbreaks of ebola, we have to ask: What other horrors are hidden away in non-domesticated animals?

Furthermore, pathogens themselves are constantly changing.

As described in previous blinks, viruses — especially RNA viruses — mutate quite rapidly. Some of them might even mutate in such a way that makes them perfectly transmissible and virulent. A disease that is both easily spread _and_ kills at the optimal speed could have devastating consequences for human society.

### 12. The growing human population density might make us more susceptible to viruses in the future. 

So, knowing what we now know about viruses, do we have cause for concern? Will we be facing a global pandemic anytime soon?

That all depends on how humanity develops. At present, the future doesn't seem too bright.

Why? Because the human population is becoming more and more densely concentrated, fueled by the global population increasing at an unprecedented rate. This growth is projected to continue for many years, at least until the population hits 9 billion people, before it levels off.

And with this increase in population comes an increase in _urbanization_ as people flock to the cities in search of jobs and higher standards of living.

Both of these trends will drive us to explore foreign territory. For example, in Africa, large areas of land will likely be cleared to build cities. Yet, as we clear these natural habitats, humanity will be brought closer to the wild animals that host deadly diseases. We might even see the spillover of a virus more deadly than ebola.

In addition, our behavior helps viruses and bacteria spread.

For instance, by operating huge farms with thousands of animals, such as pigs, chickens, cattle and goats, we up our risk of acquiring pathogens from external sources — for example, from bats roosting above the pig pen.

If an animal becomes infected, our global trade in foodstuffs could very well send it on a world tour at breakneck speeds. Nicely packaged bits of diseased cow meat could find themselves anywhere in the world in no time at all.

Just imagine what would happen if a major disease found its way into a densely populated area. It would spread so quickly that we could do little about it.

So what's to be done? It's hard to say. Viruses, bacteria and the environments they inhabit change too quickly to make predictions. The only thing we can say for sure about the next pandemic is that there's nothing we can be sure of.

> _"The most serious outbreak on the planet earth is that of the species Homo sapiens."_

### 13. Final summary 

The key message in this book:

**It's** **most** **likely** **still** **out** **there:** **the** **virus** **that** **will** **be** **able** **to** **adapt** **enough** **to** **cause** **the** **next** **global** **pandemic.** **Nobody** **knows** **where,** **how** **and** **whether** **it** **will** **emerge.** **However,** **man-made** **changes** **to** **the** **natural** **environment** **are** **likely** **to** **coax** **it** **out** **of** **hiding** **and** **into** **human** **populations.**
---

### David Quammen

David Quammen has written numerous books on science and wildlife and is a regular contributor to _National_ _Geographic_. In addition, he is the recipient of the John Burroughs Medal for nature writing, and has also received an Academy Award in Literature from the American Academy of Arts and Letters.

