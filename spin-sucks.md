---
id: 560928178a14e60009000059
slug: spin-sucks-en
published_date: 2015-09-30T00:00:00.000+00:00
author: Gini Dietrich
title: Spin Sucks
subtitle: Communication and Reputation Management in the Digital Age
main_color: F9A28E
text_color: 946054
---

# Spin Sucks

_Communication and Reputation Management in the Digital Age_

**Gini Dietrich**

_Spin Sucks_ (2014) cuts through the out-dated marketing clichés and updates business communication for the social media age, where customer service blurs into brand messaging and every misstep is preserved for posterity. It teaches you how to maximize your SEO power and content marketing to reach — and retain — an audience for your product.

---
### 1. What’s in it for me? Learn why sex and spin sucks for marketing in a digital age. 

You already know it: Sex sells! All you have to do if you want to promote your content and market your company is to plaster a city's billboards with an image of a semi-naked girl interacting with your product somehow. Then sales will increase. Or will they?

Today, it takes a bit more than that. The digital age is upon us, and this also means that the web is packed with ridiculously large amounts of content and information. For people to notice _your_ content and what _you_ have to offer, you have to stand out. In _Spin Sucks,_ the author introduces various practices to help you win over the hearts and clicks of your customers.

In these blinks, you'll discover

  * why good content is more important than ever;

  * what a black hat and a white hat mean to the promotion of your content; and

  * what you should do if someone steals your content.

### 2. In an overpopulated media landscape, only stand-out content with a clear vision will attract an audience. 

We exchange trillions of emails each year, watch billions of online videos weekly and create millions of blog posts on a daily basis. In short, there's a massive amount of information online. And it keeps growing day by day, week by week. Which means that creating stand-out content is more important than ever before. 

Look at it this way: if you want your content to rise above all the other media competing for people's attention, you can't deliver something your audience can get elsewhere on the web. Instead, you have to create something that delivers real value.

And that starts with the headline. Since it's the first piece of content audiences encounter, it has to be special to persuade them to stick around. 

As you can see, the details are important. But so is the big picture: if you want to market your content effectively, you need to have a broad strategy — a clear content vision.

One fantastic example of clear content vision comes from "Chicago Cabbie," a Windy City taxi driver who created a Twitter account to facilitate easy cab reservations. As he became increasingly popular, he started adding additional services, sending customers calendar reminders of their reservations and posting interesting city tips when he wasn't driving. 

Part of his success can be attributed to his crystal-clear content vision: "Be a trusted resource about all things Chicago." Focusing on that singular purpose gained him a lot of credibility: whether he was delivering traffic updates to locals or recommending restaurants to visitors, everyone benefited from his bite-sized Twitter content. And as a result, his business increased by 20 percent in the first year alone.

> _"A good rule of thumb is: If you don't want to bookmark it and share it, no one else will, either."_

### 3. Connecting with people and building trust are crucial to good PR in the internet age. 

Old-school ad-men thought they could sell everything with spin or sex. But in the current media environment, building a connection based on trust is the key to good PR. 

Of course, people still love drama, sex and the absurd. But these elements don't help your company. They might attract some initial interest, but if your product isn't actually useful to people, it will swiftly be rejected. 

So instead of focusing on sex appeal or spin, you should encourage people to tell their own stories. That's what online recipe database Foodily tried to do when it asked people to share their best dinner table memories via social media. The prompt quickly sparked a natural conversation, pulling in more and more people. 

The campaign was effective because it was emotional. And posting photos of hot, shirtless male chefs would have only killed the vibes.

As we've seen, getting people to tell their own stories is a big draw. And it also helps build trust. Newsletter email platform Mailchimp offers a good example of this principle in action. Their website features stories about their customers, giving them a space to talk about the work they love doing. Mailchimp's customers can mention the service, but they don't have to. However, after watching all the videos and reading all the stories, most people naturally want to learn more about Mailchimp, simply because they see successful, interesting people using it. And that builds trust in the company.

Of course, building trust with human beings is one thing. Building trust with a search engine? That's a whole different story. Read on to find out how to make that happen.

> _"Sex sells, but only if you're selling sex."_

### 4. A “white hat” search engine strategy built around high-quality content beats the “black hat” techniques. 

Search engines offer a powerful way to deliver your content to an audience, so it would be wise to play by the rules of search platforms. 

But people don't always agree about what that means. "White hat" search engine experts think that creating stand-out content is the key to achieving a good ranking in search results. 

These professionals see it as a marathon, not a sprint: Even though creating consistently high quality content takes time and effort, it's ultimately more valuable to human beings. Consequently, resource-intensive content is more valuable to search engines, who want to deliver good results to their users. Search engines will reward high-quality content with high search rankings.

So, according to top-notch white hats — those who keep up with the ever-changing search engine algorithms — the only sure-fire way to have good site rankings is to invest a lot of hard work and a lot of time. 

Unfortunately however, there are still some "experts" who try to trick the system. They're called "black hats" and they see search engines as an enemy, using every method possible to try to trick the system, like creating bad content stuffed with keywords or even "scraping" (stealing) other people's content to boost their own search engine results. 

Black hats don't worry about being banned for using these methods, because they typically have many sites in their portfolio. So if Google removes one of those sites, the black hat can move on to the next.

As you can see, having a long-run search engine strategy developed around high-quality content is the best approach for achieving good rankings. Because ultimately, search engines are predictable — they just want you to play by the rules. 

Critics and trolls on the other hand? That's a whole different story. But if you read on, you can learn four steps for converting critics into loyal fans.

### 5. Start the four-step process of addressing criticism by evaluating the validity of the complaint and its source. 

Everyone's a critic. And what's more, no one can escape criticism. But if you can deal with criticism gracefully and learn to distinguish between trolls and valid customer complaints, you'll be able to establish a larger and more loyal fan base.

First you have to figure out whether the criticism is fair. Does this person have a valid bone to pick or are they just complaining for the sake of complaining? To answer that question, you have to figure out whether there are problems with your product and service. And if you find some, fix them. 

Imagine a company that provides assisted living and retirement homes. One day, someone on Facebook complained about a beautician in one of their homes: a resident's hair turned blue after she had it dyed. Although this phenomenon isn't uncommon among older women, it's very upsetting for the customer. The company should address this complaint instead of ignoring it — otherwise, its customers will feel neglected. 

So now that we've established that this is a reasonable complaint, let's move on to step two: verify the source of the criticism. If the person is only complaining to cause trouble — in other words, they're a _troll_ — responding will add fuel to the fire. However, if the person is a long-term, loyal customer, you should move on to the next steps. 

In the case of the blue-haired senior, the source of the complaint was the daughter of the resident. She was very angry, and ended up leaving a mean and unprofessional comment. But nonetheless, she wasn't a troll. This was the first time something like this had happened and she was sorely disappointed in the company.

### 6. The four-step process of addressing criticism ends with strategizing your responses and finding solutions for problems. 

Whenever you encounter multiple critical comments, you need to be strategic about how you answer them. You must evaluate the relative influence of each critic and prioritize your responses accordingly. 

That doesn't mean you can neglect people you deem unimportant. It just makes business sense to respond more quickly to highly influential people.

Once you've decided, start by replying publicly to the complainers and ask them to send you their contact info in a private message. By ensuring this interaction happens publicly, you will show other customers that your company takes complaints seriously. 

That's what the CEO of the retirement and assisted living company did: He posted a comment asking the angry customer to send him her phone number, so he could contact her. Then he called her privately and solved the problem. He listened to her feedback for ten minutes and offered to give her mother a coupon for three free salon visits. He also assured her that he would call the salon manager, to make sure the hairy blue situation would never happen again. 

In the end, the woman was so pleased with the interaction that she wrote about her good experience on the company's Facebook page. And today, she's one of the company's biggest fans. 

Replicating this kind of result doesn't require a CEO. You just have to apologize to the customer and make an effort to fix the situation. 

That's the secret to turning a critic into a loyal fan: Don't be sneaky or try to avoid the criticism. Instead, be honest and forthright.

> _"Sometimes we just want to be heard."_

### 7. It’s easy to manipulate the media, but it’s a waste of time: great companies don’t lie – they do good. 

People used to trust the media, but times have changed: today, media manipulation is easier than ever. 

In fact, Ryan Holiday demonstrated this sad fact in his book, _Trust Me, I'm Lying_. Holiday indiscriminately responded to every query posted on the internet service "Help a Reporter Out" (HARO), which connects journalists with sources. He even hired an assistant to help him handle as many requests as possible. After a few weeks, he had far more requests than he could manage. 

One day he was featured in a Reuters story, where he discussed Yikes — young adults who save money but are reluctant to invest in the stock market. The thing is, he wasn't especially well-informed on the topic. 

Another day, Holiday appeared on MSNBC, where he described his experience working for Burger King. The kicker? He was lying! He'd never worked for Burger King in his life.

He pretended to be an expert on several other national news outlets, including ABC, CBS and the _New York Times_, where he spoke about boats, vinyl records and more. Although his entire media experiment was built around lies, the reporters never noticed. No one ever fact-checked him.

But now, after showing you how easy it is to manipulate the media, we're going to tell you to reject that approach. Instead, simply do good and market your efforts well. 

That's what Walmart did after Hurricane Katrina hit New Orleans. The retailer supplied meals, necessities and cash to the victims. Two months later, the company set new goals for sustainability, women's empowerment and waste reduction. 

Their efforts were widely publicized and they didn't have to lie or create fake stories. Instead, they did something good and announced their commitment to further improvement. And that's the mark of a great company.

### 8. Either fight content thieves or find a way to use the stolen content to your advantage. 

Good content is valuable, but it's also time-consuming and resource-intensive. That's why dishonest people steal content instead of creating their own. 

If someone steals from you, don't passively accept the theft — take action. Simply letting the thieves know you're on to them might do the trick. 

For instance, whenever you find your content on another site, comment with something like, "This looks familiar." Or, "I'm glad my content was good enough to steal." That will show people that you're the original author. 

And whenever someone uses your content without linking back to your site, ask them to make a fix. If they refuse, file a _Digital Millennium Copyright Act_ takedown request to their hosting company.

Here are two more methods for dealing with content theft that are more time-consuming, but also more fun.

Method one: Use internal linking — links to already-published content on other parts of your site — in every piece you publish. That way, if someone steals your content, you'll get a notification. And as a bonus, you'll get an additional link to your site. 

Method two: Use Yoast or a similar plug-in. Whenever someone steals your content, the plug-in will automatically produce a customized message below the stolen post on the other site, notifying any reader of the theft. A standard message is: "This originally appeared on XX site."

These two methods can't stop content thieves, but they will at least allow you to benefit from the theft by notifying readers that you're the original source.

Because ultimately, content thieves are annoying, but you can use them to your advantage. Plus, if you have an established brand, accruing new links back to your site is beneficial. 

And in the next blink, we'll show you how to tap your customers to help build that brand.

### 9. Although social media allows you to tap your customers to build a brand, this has some downsides. 

For decades, companies relied on traditional marketing to simply reach an audience. The web makes this easier, but it's a double-edged sword.

The benefits are clear: customers can help build your brand by talking about it on social media platforms like Facebook and Twitter. 

Just imagine a retail customer checking in at your business using Facebook Places or Foursquare. Since all his hundreds of friends see the check-in, your store now has hundreds of potential new customers. 

What's more, Twitter and Facebook allow customers to immediately share their new purchases, which can further promote your business.

However, social media presents challenges. You can't only display good feedback while filtering out the bad stuff. To have a credible brand, it's crucial to be honest and consistent. 

Here's an example of how social media can backfire: a few years ago, an Applebee's employee posted a customer receipt online. The guest, who was also a pastor, had crossed off the automatic 18 percent tip, explaining his actions with the note: "I give God ten percent, why should you get 18?" Applebee's corporate headquarters responded by firing the employee for violating the guest's privacy. 

But some online sleuths found another, older online Applebee's post which also showed a guest receipt, but this time with a positive message. The public was confused: Why hadn't this employee been fired as well? Pretty soon, the controversy had attracted hundreds of thousands of negative comments. The fact that Applebee's responded to every single one of those complaints with the same pre-approved message only fueled the company's negative image.

### 10. Customer experience and real-time interaction are today’s two biggest marketing themes. 

Though the internet has given rise to scores of new marketing technologies, they all tend to fit within two major themes: good customer experience and real-time marketing.

Starting with the former, no business can survive without its customers. That's why you should focus on improving their experience, rather than making money. 

To that end, consider the company Men's Warehouse, founded by George Zimmer in 1973. The retailer was successful until 2013, when Zimmer was dismissed as executive chairman because he disagreed with new policies regarding customer experience. As a consequence of his dismissal, the company reported a 28 percent decrease in profits the following quarter. The reason? New management was focused on stockholder value and market trends, not customers. 

Since employees constantly heard the word "earnings" — not "customers" — from higher-ups, they got the message that the company was all about money, not customer experience. Eventually, Men's Warehouse shifted their attention back to their customers, having realized that this was the only way to make a profit. 

Moving on to the second emerging marketing sector: Real-time marketing offers endless opportunities for reaching customers, as long as the timing is right. 

For instance, remember the blackout during the 2013 Super Bowl? While everyone else was waiting for the lights to come back on, Oreo saw the blackout as an opportunity. Only four minutes after the blackout hit, they tweeted out the message, "Power out? No problem." Attached to the text was a black-and-white picture of one of their signature cookies, with the caption, "You can still dunk in the dark." It was a priceless and wildly effective bit of marketing.

### 11. Final summary 

The key message:

**Modern marketing is a fast-changing field. But ultimately, focusing on delivering high quality content and an enhanced customer service experience are key to reaching an audience and building a loyal following.**

Actionable advice:

**Create good content.**

This is easier said than done! But it's crucial. 

The next time you write something for your personal blog or business, aim to create the best possible content, in the sense that it delivers value to your readers. That way, your audience will want to share it with their friends, organically expanding the reach of your marketing efforts. 

**Suggested** **further** **reading:** ** _The Art of Social Media_** **by Guy Kawasaki and Peg Fitzpatrick**

_The Art of Social Media_ reveals the most effective ways to promote yourself or your product professionally on social media platforms. The authors explain how to get the most of the many dominant social media platforms today, including Google+, Facebook, Twitter and others.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Gini Dietrich

Gini Dietrich founded the marketing and communications firm, Arment Dietrich. She's also the founder of Spin Sucks Pro, which provides advice to PR and marketing pros. Her clients include BASF, Bayer, Abbott and Sprint.

