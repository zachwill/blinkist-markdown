---
id: 57dbe70984aa58000368e78a
slug: spqr-en
published_date: 2016-09-20T00:00:00.000+00:00
author: Mary Beard
title: S.P.Q.R.
subtitle: A History of Ancient Rome
main_color: None
text_color: None
---

# S.P.Q.R.

_A History of Ancient Rome_

**Mary Beard**

_S.P.Q.R._ (2015) lifts the curtain of time, allowing us to examine the lives and legends of ancient Rome, from Romulus all the way to Constantine. They work to separate fact from fiction by challenging the prevailing myths that have kept generations of historians and scholars enthralled. Learn what really happened during the rise and fall of the Roman Empire.

By the way, S.P.Q.R stands for Senatus Populusque Romanus (The Roman Senate and People) and still appears on Roman currency and monuments.

---
### 1. What’s in it for me? Refresh your knowledge on the Roman Empire, and debunk some of its myths. 

We all know a thing or two about the foundation of the Roman Empire. But how much more do we know than that? After all, it's hard to know exactly what happened in the eighth century BC — which is when the city of Rome is said to have been founded.

The responsibility for telling this story fell to historian Titus Livius, or Livy. Written in the first century AD, the tale he told — of Romulus and Remus and the founding of Rome — still remains the popular myth to this day. But there is certainly more to the tale than that. These blinks take a look at Livy's tale and add some historical explanation to the story of how the ancient and small town of Rome grew into classical Europe's largest and most powerful metropolis.

In these blinks, you'll discover

  * how celebrating the founding of Rome resulted in a war;

  * why Rome's regal period might not have been so regal after all; and

  * why all the stories about imperial villainy might be pure fiction.

### 2. The story of Rome’s foundation contains many themes that would echo throughout Roman history. 

To go all the way back to when Rome was first created, we turn to Livy, the most famous Roman historian to write about the many myths surrounding Rome's earliest days.

Livy tells the story of Romulus, the man regarded as Rome's founder.

Romulus and his brother Remus were born to a virgin mother named Rhea Silvia, who was miraculously impregnated — though against her will — by the god Mars.

But Silvia's uncle, King Amulius of Alba Longa, saw the children as potential threats to his power, so he ordered them to be thrown into the Tiber river. However, the men tasked with the job put the babies in a basket and sent them off downstream instead.

The infants were first rescued by a wolf that protected and nursed them; later, a farmer found the boys and raised them until they were old enough to set off and establish their own city.

But when they worked together on designing their city, they had a bitter dispute over the construction of a protective wall and, in the fight that followed, Romulus killed his brother Remus. This left Romulus as the sole founder of Rome, which he named after himself.

Now Romulus had a town but no people. To fix this, he first declared Rome a welcome place for asylum-seekers, exiles and refugees. He then threw a party and invited the Sabines, his neighbors from the town of Latium. But once the Sabines arrived, Romulus had his men abduct and marry their women.

Due to this kidnapping, war quickly broke out. This lasted until the leader of the Sabines, Titus Tatius, agreed to rule alongside Romulus. But when Titus was later murdered during a riot, Romulus was again the sole ruler of Rome, which he remained for the next 30 years.

Many themes from this story resurfaced as Rome grew into a city. For example, as Rome expanded, it faced many civil wars — a large-scale analog of the fratricidal conflict between Romulus and Remus.

And, as we'll see, violence, murder and unrest often accompany transitions of power in Rome.

### 3. Rome’s early years were marked by a supposed regal period of six kings, responsible for many important advancements. 

As Livy's story continues, it explains how certain Roman traditions began. The genesis of these traditions coincides with a time when Rome was ruled by kings. This period is called the _regal period_, and followed Romulus's rule. During this time, six different kings helped shape the city.

However, many historians consider this a dubious claim. Since the regal period lasted from the mid-eighth century to the late sixth century BC, this would require each of the six kings to have spent over 30 years in power, which is highly unlikely for the time.

Also, archeological evidence suggests that the population of Rome grew from only a few thousand to around 20,000 people, an extremely small society to have had a regal system in place.

It's far more likely that these "kings" were chiefs or community leaders who helped organize and structure their society.

Indeed, societal advancements are what Livy attributes to these six kings, with the last one kicking off the Roman Republic.

The first king was Numa Pompilius. He's regarded as the man who brought religion to Rome.

Then came Tullus Hostilius, and, as his last name suggests, he was famous for his warmongering ways.

Ancus Marcius then founded Rome's seaport _, Ostia_, or Rivermouth.

The fourth king, Tarquinius Priscus, founded the Roman Forum and began the chariot races and gladiator fights of the Circus Games.

He was followed by Servius Tullius, who is credited with developing Rome's first political institutions and laying the foundation for Roman politics. This is when the census and the army, as well as voting and elections, began. Alongside this came the idea to structure all of these institutions by wealth, with more money equalling more power.

The last king was Tarquinius Superbus, the tyrannical son of Priscus. As the story goes, his son, Sextus Tarquinius, raped the Roman matron, Lucretia. As a result, Lucius Junius Brutus, the son of Priscus's daughter, Tarquinia, called for an end to the tyranny of kings and the start of a new Roman Republic.

### 4. The Roman Republic began gradually, with the development of the Twelve Tables and a clash with the town of Veii. 

Lucius Junius Brutus is credited with calling for a Roman Republic. But no government is born overnight. Indeed, it was a gradual process that took decades, if not centuries.

One of the best indicators of when the republic actually began to take shape is contained in the Twelve Tables, a set of rules and standards that laid the groundwork for Roman law.

Unlike for some other tales, there is physical evidence of the Twelve Tables in the form of partial inscriptions that historians continue to study today.

These date back to around the mid-fifth century BC, when Rome was still not very prosperous but the people were looking for laws to protect them.

The Twelve Tables provided these Romans with some structure, and they now provide us with insight into their lives at that time. They tell us that agriculture was a growing concern, and that people were preoccupied with issues like protecting private property. They also tell us that early Romans still feared spells and magic. (One clause says the appropriate punishment for a patron who harmed a client is a curse!)

These laws were a big step forward for Rome, but the transition from a small town to a large, powerful city really took off in 396 BC, when Rome clashed with the neighboring town of Veii.

What caused the conflict isn't really known, but, according to legend, it launched a ten-year battle that is mythologized in a way that makes it comparable to the Trojan War of Greece. The battle against Veii was likely a far less grand affair. Nevertheless, when the Romans won, they doubled the amount of land in their possession.

However, Rome hit a setback in 390 BC when it was attacked by rampaging Gauls who looted and wreaked havoc on their land, which led the Romans to build a protective wall around their city.

From this point on, the Romans would remain the aggressors, not the victims.

### 5. In the fourth century BC, Rome started to expand and redefine itself during a string of legendary wars. 

After taking over Veii and being sacked by the Gauls, Rome focused on increasing its manpower and transforming its army into a powerful, centralized organization.

What followed was a series of wars that put Rome on its path of extraordinary expansion.

At first, Romans continued to expand locally, but even these battles made legends out of wartime leaders.

Some of these local battles occurred simultaneously, such as the Latin War (341 - 338 BC) and the Samnite Wars (343 - 290 BC). (By the time these two wars ended, Romans controlled half of the Italian peninsula!)

The Samnite Wars are also impressive for having concluded 200 miles away from Rome, on the opposite side of the Apennine mountain range. The final battle was led in part by Lucius Cornelius Scipio Barbatus, a legendary consul of Rome who began the Scipio dynasty, which helped define much of the next 150 years.

A crucial part of Rome's successful expansion was the alliances they formed as a result of these conflicts.

By reaching favorable treaties that granted Roman citizenship to people like the Samnites and Latins, Rome gave them a stake in the ongoing victories and maintained the momentum of their military campaigns.

Not only did this swell the Roman army with troops; it also brought more citizens under the Roman banner. The treaties began to redefine what it meant to be Roman. Indeed, by the start of the third century BC, a Roman citizen could be just about anyone in Italy. This was quite the development, for these people used to consider themselves very different from one another.

It was already becoming difficult to define exactly what it meant to be Roman, and, as expansion continued, attempts at such a definition only became harder to make.

> _"...Romans saw their expansion more in terms of changing relationships with other peoples than in terms of control of territory."_

### 6. As Roman power grew and flourished, politics evolved too – but not without a fair share of violence. 

In the fourth century BC, the ongoing battles led to more Roman citizens and more power, and the politics of Rome began taking shape.

While no one is certain exactly when the term "consul" was first used, or when it came to define the highest office in the Roman Republic, there is evidence that it was used in the third century BC. This is when it was inscribed on the tomb of Lucius Cornelius Scipio Barbatus, the man who played a vital role in the military expansion of Rome and who served as consul in 298 BC.

Consuls were elected each year by the people of Rome. To keep them from having too much power, two were elected. Generally speaking, they were the ones who controlled the military and oversaw the senate to pass legislation and keep the Republic running.

This might sound fairly democratic but, just as it is today, it was costly business to run for public office, so one of the first qualifications was to be wealthy. Therefore, much of the senate was made up of rich and aristocratic men.

But, every so often, someone would try to change Rome's political bias toward the wealthy, which inevitably had fatal consequences.

This is what happened to Tiberius Sempronius Gracchus, who came up with a plan to distribute land to the poor in 133 BC. His reformist ideas got him and hundreds of his supporters killed by angry senators.

Sadly, the Gracchus family seemed to be a stubborn bunch. Tiberius Sempronius's brother Gaius Sempronius Gracchus had similar ideas of helping the poor and he too was killed, in 121 BC, along with 3,000 of his supporters. The river, it is said, was clogged with dead bodies.

### 7. Expansion led to tension within the empire, resulting in social conflict and a shift toward dictatorship. 

Things were pretty tense in Rome when it came to politics. And, throughout the Roman empire, social tensions increased as more people and different cultures came together.

Between 146 and 88 BC, Rome continued to expand; it dominated North Africa, Greece and the rest of the Mediterranean. But things didn't always go smoothly when Roman soldiers and governors occupied these territories.

By the start of the second century BC, it wasn't uncommon for Romans to abuse their power while abroad.

For example, simple mistakes were often met with brutal punishment. If a local in an allied territory failed to perform a task to a Roman's satisfaction, he might get flogged or killed, even if he had been granted Roman citizenship.

This behavior reached its boiling point in 91 BC. When a Roman soldier insulted a local in the central Italian town of Asculum, the townspeople revolted and killed every Roman in sight. This led to the Social War, which lasted until 88 BC. During the conflict, as Italian allies fought the Roman Republic over issues of inequality, some 300,000 Roman citizens lost their lives.

This opened the door for the ambitious military commander Lucius Cornelius Sulla Felix, who took advantage of a Roman law that allowed the Republic to install a dictator in times of crisis.

Sulla essentially invaded Rome and granted himself power to make laws, requisition lands and purge the city of his political enemies. He even had a hit list, which included thousands of men. It also contained one-third of all senators, who were systematically killed.

As we'll see in the next blink, Sulla's actions would have long-lasting repercussions.

### 8. With the ascension of Pompey the Great and the Gang of Three, the leadership dynamic in Rome began to shift. 

Sulla's move to control Rome was certainly an audacious one, and it set a dangerous precedent for other ambitious military leaders to come.

One such Roman general was Gnaeus Pompeius Magnus, or Pompey the Great, who made a bold gambit to become consul.

In 66 BC, Pompey was put in charge of defeating King Mithridates in Asia Minor, a job that he performed exceptionally well. In fact, he ended up claiming more provinces than any Roman before. In 61 BC, he returned to Rome, a celebrated hero. In the East, coins were printed with his likeness, new cities were named after him and influential people, like senator Cicero, lavished him with praise. Pompey also had a huge theater dedicated to him in 55 BC, which was substantially bigger than anything a general had received before.

All this, on top of his own extravagant and reportedly pompous behavior, set the standard for the kind of treatment future Roman leaders would come to expect.

But Pompey had long-term ambitions and he retained his power throughout much of the 50s BC by becoming part of a triumvirate, along with Julius Caesar and military general Marcus Licinius Crassus. They were known as the Gang of Three, and they brought a new leadership dynamic to Rome.

Through their combined efforts, Caesar was elected consul in 59 BC; later, in 55 BC, Crassus was made consul alongside Pompey. They would continue to manipulate much of Rome's politics through these years.

Alas, the triumvirate came to an end in 53 BC, when Crassus was decapitated by the Parthians in the Battle of Carrhae. As legend has it, Crassus's head was used as a prop in Parthian productions of Euripides's play _The Bacchae_.

After Crassus's death, in 52 BC, Pompey was elected sole consul of Rome. Soon, he and Caesar were at extreme odds.

### 9. Another civil war led to Caesar’s relatively short reign. 

Like Sulla and Pompey, Gaius Julius Caesar had made quite a name for himself in battle.

For Caesar, it was in the west, fighting the Gauls and taking a symbolic step into Britain. When he returned to Rome in January of 49 BC, he was ready to take power.

But, with Pompey desperate to retain power, what followed was a four-year civil war, with Caesar's supporters facing off against Pompey's. Historians have considered this a particularly ironic civil war since no one was fighting for liberty; for each side, victory merely entailed a perpetuation of autocracy.

However, Pompey was eventually beheaded in 48 BC, after trying to flee to Egypt, and Caesar took firm control of the Roman Republic. Four years later, in 44 BC, Caesar made an unprecedented move and had himself made dictator for life.

This lifetime dictatorship was indeed a dramatic deviation from the traditions upon which the Roman Republic were founded — nor did it go unnoticed.

During Caesar's relatively short reign, he made a lot of reforms. He micromanaged and controlled everything from the calendar and construction of roads to the selection of officials in small communities.

At a ceremony one month before Caesar's assassination, his right-hand-man, Mark Antony, presented him with a royal crown. Even though Caesar refused the offer, this gesture, along with his appointment as dictator for life, may have been seen as the last straw for a certain group of people.

And so, on March 15, 44 BC, 20 to 30 angry senators stabbed Caesar to death in front of his own statue, in the senate house that Pompey had built.

Though the senators may have believed they were killing a tyrant, they soon learned that they couldn't kill tyranny in Rome.

### 10. Emperor Augustus overhauled the Roman government and set the standard for the next 150 years. 

Both Pompey and Caesar possessed many of the qualities that came to define a Roman emperor; the first person to actually hold the title of emperor, however, was Caesar's heir — his 18-year-old great-nephew Gaius Octavius. Or, as he would later be known, Emperor Augustus. He would go on to hold his position longer than anyone else had before — he ruled for more than 50 years, until his death in 14 AD.

As with many transitions of power, there was a period of unrest following the assassination of Caesar.

The resistance against Octavius was led by Mark Antony, but he was unable to stop the forces at Octavius's disposal. Utterly defeated, Mark Antony killed himself in Egypt alongside his Egyptian ally and lover, Cleopatra.

When Octavius held full control in 31 BC, he changed his name to Augustus and made numerous changes to the government and city of Rome.

For instance, he granted himself the power to appoint all generals as well as full control of Rome's military. This meant that he could take credit for all victories, and it prevented other generals from rising to fame and threatening the emperor's power, as Sulla, Pompey and Caesar once had.

He also gave all soldiers a stable pension, granting them land or a cash settlement equalling 12 times their annual pay. He also wisely moved the active soldiers out of Rome, which simultaneously reduced both violence in the city and potential threats to himself.

Additionally, to make more palatable the fact that he'd changed the senate into an administrative wing of his own government, Augustus increased benefits for senators and their families.

While doing all this, he busied himself with setting the standard for having grand, marble buildings built in his honor. This further transformed the architecture of the city of Rome into a mighty display of its imperial power.

### 11. With many emperors in the first and second centuries AD, it’s difficult to know what separates fact from fiction. 

After the rule of Augustus, a string of 14 emperors followed. They became more or less known by their varying degrees of villainy or sadistic behavior. But how bad, or good, were these emperors, really?

These days, historians believe it's likely that many of the unpleasant stories are nothing more than propaganda invented by their successors.

Take Gaius Julius Caesar Augustus — better known by his childhood nickname, Caligula. He was assassinated by his own personal guard on January 24, 41 AD. In order to justify this assassination, anti-Caligula propaganda was probably spread to make it something more than just a dispute with his personal guard. Tales of Caligula's bad behavior — he supposedly threatened to make his horse consul and had an incestuous relationship with his sisters — were created and spread to make the people of Rome more eager to accept Caligula's nephew, Claudius, as his successor.

But there's little evidence to support the outlandish stories about Caligula, or the ones that have followed other emperors, such as Claudius's successor, Nero.

It's claimed that Nero, who reigned from 54 - 64 AD, started a fire in Rome in order to make room for his own building. It's said that while the city burned, he calmly played his lyre. Modern scholars look at much of Nero's bad reputation as coming directly from anti-Nero propaganda spread by the Flavian dynasty that followed his suicide in 68 AD.

Conversely, later emperors who died peacefully, such as Hadrian (117 - 138 AD) and Marcus Aurelius (161 - 180 AD), were remembered fondly.

But the reason they've gone on to be admired is likely due to their successors having no need to spread negative propaganda, not because they were flawless rulers. For example, Hadrian supposedly had an architect killed over a design dispute and Aurelius was known for the violent suppression and murder of German prisoners.

In the end, there probably wasn't much difference between the 14 emperors that followed Augustus, and, if there was, it didn't have much of an effect on the general public.

### 12. Life outside the emperor’s palace had its own set of concerns and practices. 

While there is little evidence about what emperors did behind closed doors, there are surviving letters and documents that paint a picture of what life was like outside the palaces.

For instance, there are diaries indicating that, relatively speaking, women had it better in Rome than in other cultures.

Though the life of a Roman woman often meant taking care of the household, there were free-spirited women who led an active social life as well. The former slave Volumnia Cytheris was one such free-spirited woman who was rumored to have slept with both Brutus and Mark Antony.

Unlike in Greek culture at the time, women weren't forbidden to be out socially in Rome and mix freely with men.

Marriage was considered a private affair: if you said you were married, it was taken as an accepted fact, just as divorce was considered an understood agreement between two people.

However, there were traditions that could force parents to make a troubling choice when it came to childbirth.

The father of a bride was expected to provide the newlyweds with a dowry; for a poor family, this dowry was an expensive proposition. In fact, some families were so fearful of having to pay a dowry for their daughter's future wedding that they would throw a female baby in the garbage rather than raise it. Many of these discarded babies ended up being raised as slaves.

Slavery was indeed a big part of Roman culture, and slaves' standard of living varied tremendously.

In Italy in the first century AD there were roughly 1.5 to 2 million slaves, but there was no such thing as a typical slave. Life could be cramped, abusive and miserable, or luxurious enough to make a poor Roman citizen envious. Some ran away to freedom. But many eventually became free citizens through the decree of their masters and even ended up becoming prosperous and memorialized.

> _50 percent of children died by age 10._

### 13. A citizen’s place in Roman society was largely defined by wealth, though rich and poor shared some concerns. 

There is some evidence of homeless people in ancient Rome. Old signs have been found that warn people against squatting in public buildings.

But while there was a fair amount of urban poverty, there are more details surrounding the lives of people who could find work.

Rome did have squatters and early versions of what people now call shanty towns, with the poorest people living under improvised and flimsy shelters. And though we do have evidence of beggars in old paintings, homeless people did not live long, and they left behind little that tells us of their lives.

Higher on the social ladder, there's more evidence of what life was like. As Rome became a main shipping point for the world, we know there were thousands of people who got by on the unsteady, seasonal work of unloading ships.

Steady workers, on the other hand, gained employment through skilled jobs such as carpenter, baker or butcher, and many joined local trade organizations that ensured them a good funeral. These weren't exactly the equivalent of guilds or modern workers unions, though, since they had no rules or influence on politics or wages.

But there were some aspects of Rome that affected both the poor and the rich.

Rome was densely crowded, and had a poor infrastructure. This meant that rich and poor often lived cheek by jowl and were faced with the problems that came from inefficient waste management: smells, germs and diseases.

To fit everyone into the city, Rome built multi-story apartments called _insulae_, or islands. The poor lived at the top in the smallest and barest rooms; the wealthier folks lived on the bottom.

This might sound backward, but it actually meant that, if a fire broke out, the wealthy could easily escape and the poor were less likely to survive.

### 14. Eventually, expansion reached its limit and actions by Caracalla and Constantine marked the beginning of the Empire’s decline. 

One could say that the downward trajectory of the Roman Republic started in 192 AD, when Emperor Commodus was killed and 100 years of constant civil war followed. During this time, one emperor after another came along, each unable to maintain power.

Another event that many point to as the beginning of the end occurred in 212 AD, when Emperor Caracalla suddenly declared all free people in the vast Roman Republic, from Britain to Syria, official Roman citizens.

No one is completely sure why he did this; if it was for more money, he could have simply raised taxes.

And if it was a move of generosity, it backfired since it only deepened the social divide between the rich and the poor. After Caracalla's gesture, laws were rewritten and poor citizens were deprived of rights they had had previously in order for the wealthy to hold on to their superiority.

But many see this as the beginning of the end because it completely evacuated Roman citizenship of all cultural and political meaning. There was a time when people fought and died for citizenship, but now, at the whim of an Emperor, most people in the known world could call themselves a Roman. It signified nothing.

Then came Emperor Constantine in the fourth century AD, at a time when Rome was no longer the military power that it once had been. By now, more organized barbarian enemies were popping up, finally presenting a real threat to the Roman army.

Meanwhile, other cities in the empire were vying for dominance. Constantine spent most of his time in the city named after him, Constantinople, later renamed Istanbul, and soon the empire would split in half, with Constantinople as the capital of the East.

But the big surprise came on Constantine's deathbed in 337 AD, when he converted to Christianity. This move further blurred the meaning of Roman identity and, in the end, led to a whole new set of inner conflicts that inevitably tore the empire apart.

### 15. Final summary 

The key message in this book:

**You may think that there's nothing more to learn about ancient Rome and that it's all been told before. But modern historians have more evidence than ever to sift through as well as the ability to pinpoint the skewed opinions of previous historians. The truth is that Rome's history has always been a healthy mix of fact and fiction, of true tales and mythology, that will continue to fascinate people for ages to come.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Alexander the Great_** **by Philip Freeman**

The eponymous hero of _Alexander the Great_ (2011) is remembered as one of the greatest military commanders who ever lived. Setting out from Greece at the age of 21, Alexander waged a ten-year campaign, during which he defeated the Persian Achaemenids and, in so doing, created the largest empire the world had ever seen. By spreading Greek culture and language throughout Eurasia, his legacy remained influential for centuries after.
---

### Mary Beard

Mary Beard is an acclaimed author and professor of classics at Newnham College. She is a member of the American Academy of Arts and Sciences as well as the British Academy. Her other books include _The Roman Triumph_ and _Pompeii_.

