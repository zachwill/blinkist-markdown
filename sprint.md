---
id: 5761875f0beabd0003161b53
slug: sprint-en
published_date: 2016-06-24T00:00:00.000+00:00
author: Jake Knapp, John Zeratsky, Braden Kowitz
title: Sprint
subtitle: How to Solve Big Problems and Test New Ideas in Just Five Days
main_color: 27AAC4
text_color: 186878
---

# Sprint

_How to Solve Big Problems and Test New Ideas in Just Five Days_

**Jake Knapp, John Zeratsky, Braden Kowitz**

_Sprint_ (2016) is a guide to start-up success, broken down into a five-day plan that lets you test new ideas and solve complex business problems. These blinks give you everything you need to move quickly from an idea to a prototype and, ultimately, make a decision on whether or not to launch.

---
### 1. What’s in it for me? Get a quick and clear appraisal of your ideas’ potential for success. 

Do you have a business idea that you want to get off the ground as soon as possible? You're not alone — but not all ideas are a good fit for their respective markets. Many people spend huge amounts of time and money in vain, trying to push their ideas in markets where they simply won't stay afloat.

So don't make the same mistake. Instead of entering a market with an impractical or incomplete product, one that you might not even have tested, consider another, more efficient approach. These blinks will reveal exactly what this approach entails and show you how to get started.

In these blinks, you'll discover

  * why you need to equip yourself with more than a mere idea;

  * why your approach to playing with Lego might come in handy; and

  * why you don't need to gather loads of people to test your prototype.

### 2. A sprint is an effective project management tool, especially for start-ups. 

Do you ever find yourself facing multiple deadlines and feeling certain that you won't be able to deliver the results you need while sticking to your schedule? Well, don't sweat it. People do their best work when they take on challenges with little time to complete them.

The author came to this conclusion while working at Google. He had just one month to develop an important new feature for Gmail that would automatically organize messages. But despite the tight deadline, his team generated a design, a prototype and ultimately earned loads of positive user feedback.

So how did they do it?

By using a new approach to project management. The author calls it a _sprint_ and it has three key components:

First, a short deadline is essential for preventing procrastination. For example, when a project has to be completed within a few days, the ability of a team to focus is much stronger than the urge to hang out and drink coffee.

Second, it's important to have people with different skills all working in one room. Engineers, designers and product managers tend to get more done when they're all together and are able to answer each other's questions.

The third and final ingredient of a sprint is for the project to produce a concrete prototype, not just an abstract idea like those shouted out during brainstorming sessions.

The author identified these three points as the foundation of his project management style — but it wasn't until later that he realized sprints are perfect for start-ups. After all, start-ups usually have just one shot to launch a product; if their first attempt goes bust, they generally run out of money before they can launch another.

But with sprints, start-ups have an effective tool for testing out their projects before they pour money into their production and launch.

### 3. Sprints help ensure great ideas and outstanding execution. 

If you look at the most successful start-ups, you'll see that their rise to fame was based on powerful and innovative ideas. But ideas are just one aspect of start-up success; they're not much good if poorly executed.

Take Webvan, an online store that launched in 1999 to allow users the luxury of buying groceries from home. It's an idea that remains popular today, with Amazon currently expanding its operations to offer something similar, AmazonFresh.

But Webvan failed shortly after its launch because its managers had no prior retail experience; they burned through all their capital buying delivery trucks and warehouses, without knowing what direction they were headed in. In other words, they lacked strong execution.

And that's where sprints come in. By using sprints, you can quickly and effectively solve the problems standing between you and successful execution.

Instead of launching a _minimum viable product_ — that is, one with just the bare features necessary to collect feedback from potential customers and gauge the market — sprints help you build a realistic prototype that garners real customer responses.

Regardless of whether you're developing a new smartphone app or working on improving your company's marketing strategies, sprints are great for all types of projects. Google even used the technique with the team that built its self-driving car!

So, now that you know the benefits of a sprint, it's time to learn the five-day step-by-step plan that forms the core of this strategy.

> _"My best work happened when I had a big challenge and not quite enough time."_

### 4. On Monday morning, identify your challenge and gather a diverse team. 

It's impossible to move fast if you don't know where you're going, which is why the first day of your sprint should begin with pinpointing the problem you need to solve. The best way to start this process is by identifying the challenge you're facing.

So, simply pose the question, "what's our challenge?"

For instance, some companies are challenged by high-stakes projects in need of direction, while others face tight deadlines and need to speed up development. Just take Blue Bottle Coffee; this successful coffee chain from San Francisco raised $20 million and set up an online store that sells fresh coffee beans.

In the beginning, they had no clue where to start because neither their designers nor their web developers knew anything about how to tackle the problem. Even so, in the first few hours of their sprint they began to understand their challenge: translating the cozy appeal of a coffee shop onto a computer screen and coming up with the best online design to replicate the in-store experience.

After identifying your challenge, the next step in a successful sprint is to build a diverse team of seven that includes two key roles: a _decider_ and a _facilitator_. The former is someone who takes control and settles disputes, often the CEO, while a facilitator is in charge of managing time, summarizing discussions and smoothing out the process.

But why the five other people?

Well, experience has shown that seven is the magic number. If you add more people, the sprint will be sluggish because the team generates too many discussions that can take time to settle.

What's more, a diverse group is also important because you need people who bring the right input and ideas, regardless of their position in the company — or even whether they are an employee of your company at all.

For instance, when it came to Blue Bottle's sprint, the CFO offered up some powerful insights even though he didn't know much at all about tech.

> _"Some important projects are hard to start. Others lose momentum along the way. In these situations, a sprint can be a booster rocket: a fresh approach to problem solving that helps you escape gravity's clutches."_

### 5. On Monday afternoon, draft a roadmap from finish to start and choose a target. 

After identifying your challenge and building the right team, you'll be off to a great start. So, keep your momentum rolling into the afternoon of your first day by making a roadmap for your sprint. But don't make it chronological; it's best to plan your sprint from your ultimate goal and work your way backward to identify any potential pitfalls.

A successful sprint depends on your long-term goal. So, ask yourself what aspect of the business you need to improve within the next six months. For instance, Savioke, a company that designed a robot to help hotel staff deliver small items like toothbrushes and towels, set the goal of "a better guest experience."

But when sketching your roadmap, it's also crucial to consider any failures you might run into. Be sure to review these potential mishaps so you can tease out their causes.

For instance, Blue Bottle Coffee knew that communicating their credibility through their online store was going to be a foundational step; their main concern was whether customers would trust their expertise.

Another key to guiding your sprint is to ask the right people for advice. After all, nobody knows everything, not even Bill Gates or Mark Zuckerberg. You should devote some of your afternoon to finding people, either within your sprint team, within your company or from the outside, and collecting their input.

And finally, before calling it a day, you should set the _target_ of your sprint, meaning the thing you wish to impact with it. Essentially, your target is both your customers and the decisive moment of their experience with what you are offering. Setting a target is all about knowing how and when your customer will use your product or service.

For instance, Savioke's target was the moment when a hotel guest opens their door and faces a robot delivering a brand new toothbrush. By identifying this moment, they knew where to concentrate their efforts.

### 6. Tuesday is the day you collect ideas, present them to each other and sketch them out. 

If you're into electronic music you'll know how DJs sample an old track to create a brand new song. Well, Tuesday is your day to play DJ; this is when you draw inspiration from existing products.

The day starts by bringing together existing ideas from near and far, from which you can build your own solutions. Just think of it like playing with Lego: you amass as many pieces as possible and put them together into an original design.

So, when putting a project together, it's best to gather lots of ideas and see how they combine. And remember, these ideas might come from anywhere, not necessarily just your competitors.

For instance, Savioke didn't get their robot's aesthetic from the latest robotic innovations, but rather from the Japanese animated film _My Neighbor Totoro_. In this movie, the main character is a giant friendly monster that proved a huge inspiration for finalizing the appearance of the robot's eyes.

After researching existing ideas, your work should be presented to the group through what's called _lightning demos_. In this process, each team member takes three minutes to present on their favorite existing solutions, preferably from fields that are different than yours.

After that, you're ready to sketch out how the solutions presented by your team fit together. This is a great way to level the playing field for your team members. After all, not everyone is a tech nerd — but everyone _can_ draw simple visualizations of potential solutions.

Sketching ideas is also in line with the teachings of the productivity guru, David Allen. For instance, as Allen explains in his book _Getting Things Done_, you shouldn't take on an assignment as one big daunting task — such as finding a job — but rather start with small initial steps, like updating your resume.

Similarly, your sketch will help you break down your solution into distinct parts that you can play around with and take on separately.

> _"On Monday, you and your team defined the challenge and chose a target. On Tuesday, you'll come up with solutions."_

### 7. Wednesday’s task is to select the best ideas and create a storyboard of their development. 

At this point, your walls should be plastered with sketches of potential solutions. But Wednesday is the day to separate the wheat from the chaff. After all, you can't see _all_ your ideas through.

You should begin the day by sorting through solutions. To avoid wasting time and energy on never-ending discussions, simply present solutions, answer questions and then vote on an idea.

When it comes to making these kinds of decisions, discussion isn't helpful. For instance, people might be easily influenced by someone just because he's more charismatic or higher up on a hierarchy, even if his ideas are less developed than others.

To avoid this, sprints offer an alternative strategy.

For every sketch pinned to the wall, the facilitator has three minutes to present the concept. Then, when his time is up, the creator of the idea answers questions. Once all the sketches are reviewed, a simple vote is taken to choose the best ones.

But you shouldn't jump right into prototyping after choosing your idea. Instead, start storyboarding the winning solution's development. So, while your winning solution is still pinned to the wall, you should add additional information and expand the story into ten or 15 panels surrounding the original sketch, detailing how the solution will be developed.

Just take the team behind _Slack_, a hugely successful app for team communication, that built its ingenious product by literally drawing every step of its implementation. For instance, the company even sketched out the final steps where customers would read about their product in the news, go to their website and, ultimately, register for the app.

### 8. Thursday is the day to create a realistic prototype. 

So you've got great solutions, but only two days left on your schedule. At this point, instead of actually building your idea, you should fake it; the intention of your sprint is to collect reactions and you only need a prototype that seems real _enough_.

After all, there's no way you'll have the time to finish a market-ready prototype — but a low-quality prototype is no good either, since people won't believe that it's real. So, put yourself into a _prototype mindset_ where you accept that anything can be prototyped as long as it has a sufficiently believable facade.

For instance, to attract new customers, Slack needed their website to offer a demo of their messaging app. This required a program with some degree of artificial intelligence that would automatically begin a dialogue with potential users, giving them a first taste of the app.

So, when it came to testing day, the team pretended to be the artificial intelligence. In other words, when people browsed Slack's website, the team members would answer via instant message as if they were the computer program.

It just goes to show that making this illusion believable is possible with simple tools. In fact, if you're working on an app, a piece of software or a new website, a simple set of slides on Powerpoint or Keynote are probably sufficient, because in full-screen mode they can look like the real thing and even be responsive to a degree. This presentation should be enough to showcase the prototype of the interface and gather people's impressions.

For instance, in the case of Fitstar, a company that designed an innovative fitness app, the sprint was too short to reprogram their application. So, they just faked it through Keynote, using a template that displayed buttons as if it were the actual app. In full-screen mode this simple presentation was convincing enough to get the precious feedback they needed from the sports enthusiasts who tested the prototype.

> _"Thursday is about illusion. You've got an idea for a great solution. Instead of taking weeks, months, or, heck, even years building that solution, you're going to fake it."_

### 9. The sprint ends on Friday with testing your idea and collecting feedback. 

Friday is the final day of your sprint, when all your hard work pays off: the prototype you created on Thursday will be tested, preferably by a small sample of guest users.

For this phase, five is the magic number. Five guest users are all you need to get a good sense of your solution's pros and cons. In fact, this optimal figure was first defined by Jakob Nielsen, a Danish pioneer in website usability.

Back in the 90s, Nielsen did a lot of individual testing on website designs and found that, in most cases, 85 percent of the design flaws were uncovered in the fifth interview.

So, five is the magic number and one-on-one interviews with guest users are the best way to observe their reactions. To lead one, the interviewer should start by welcoming the user in a friendly manner to put her at ease. Then, before presenting the prototype, the interviewer should ask the guest user some background questions to put her reactions into perspective.

For Fitstar, for example, it was relevant to know whether or not the person had prior experience with workout apps. After all, if the user was new to the world of fitness apps, she might struggle with Fitstar regardless of its good design.

But it's also essential that these one-on-one meetings end with the guest user giving her opinion. So, simply ask her what she liked or didn't, and what needs work.

And the rest of your team?

Well, while the interviews are taking place, they should be in another room watching the scene live. This way, they can write down their observations on sticky notes and post the positive and negative feedback on a whiteboard. This strategy will quickly let you know if your idea and prototype are up to snuff.

### 10. Final summary 

The key message in this book:

**A sprint is a five-day brainstorming-prototyping-testing session and the definitive strategy to determine if your idea is worth launching. This rapid-fire development period applies the pressure necessary to get a clear picture of your idea's potential in no time at all.**

Actionable advice:

**Use "How might we" to define your challenges.**

You can zero in on the core problem you need to solve with a method developed by Procter & Gamble in the 70s, called "How might we."

Starting with "How might we…", your whole team should write down on sticky notes the main challenges that the project is likely to face. At the end of the day, you can choose the most interesting ones and use them as a roadmap to achieve your goal.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _The Lean Startup_** **by Eric Ries**

The _Lean Startup method_ helps start-ups and tech companies develop sustainable business models. It advocates continuous rapid prototyping and focusing on customer-feedback data.

The method is based on the concepts of _lean manufacturing_ and _agile development,_ and its efficacy is backed up by case studies from the last few decades.
---

### Jake Knapp, John Zeratsky, Braden Kowitz

Jake Knapp is a partner at Google Ventures and the creator of the Google Ventures sprint process, through which he has advised more than a hundred start-ups.

John Zeratsky is a partner at Google Ventures specializing in design. He has written on the topic for major publications including _The Wall Street Journal_, _Wired_ and _Time_.

Braden Kowitz is a partner at Google Ventures and founded the Google Ventures design team in 2009.

