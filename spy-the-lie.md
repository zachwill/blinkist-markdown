---
id: 53722ba462643900070b0000
slug: spy-the-lie-en
published_date: 2014-05-13T10:37:28.000+00:00
author: Philip Houston, Michael Floyd, Susan Carnicero and Don Tennant
title: Spy the Lie
subtitle: Former CIA Officers Teach You How to Detect Deception
main_color: CF3429
text_color: CF3429
---

# Spy the Lie

_Former CIA Officers Teach You How to Detect Deception_

**Philip Houston, Michael Floyd, Susan Carnicero and Don Tennant**

_Spy the Lie_ reveals the typical strategies that liars use to try to deceive you, as well as the tools to help you detect them. This book draws on field-tested methods for lie detection developed by former CIA officers, which helps to spot the signs of a lie and ask the right questions to uncover the truth.

---
### 1. What’s in it for me? Learn how to distill truth from a web of lies. 

If someone asked you how many times you lie on a given day, how would you answer (without lying)? Some studies indicate that we lie up to an amazing 200 times per day. Others suggest that it is near _ten_ _times_ that amount.

We lie for numerous reasons, both good and bad: sometimes to protect others; sometimes just to save our own skins.

But no matter why people lie, many of us are involved in activities that require us to have access to truthful information. If you're a lawyer, CEO or accountant, it doesn't matter _why_ people lie. What matters is the truth.

These blinks provide you with the field experience of highly trained professional lie detectors so that you can better detect whether other people are telling the truth, as well as how to ask the right questions to get people to reveal their lies.

You'll also learn about

  * why it's so hard to sit still when you're caught in a lie,

  * why sports fanatics always seem to hate the referee, and

  * the different types of lies and various strategies to turn them against the liars themselves.

> _"Truth only reveals itself, if one gives up all preconceived ideas."_ — Japanese proverb

### 2. Communication is hard to interpret because it is complex and non-universal. 

When was the last time you told a lie? It probably wasn't too long ago. In fact, we all lie about ten to twenty times a day! Despite that, we're incredibly bad at recognizing other people's lies. How can that be?

One reason we often fail to detect lies has to do with the fact that it's difficult for us to precisely interpret what others are communicating to us — for a number of reasons.

First, we find it hard to concentrate on verbal and nonverbal communication simultaneously.

When we communicate with one another, we employ two kinds of communication: verbal, i.e., with our speech, as well as non-verbal, i.e., using body language.

However, we find it hard to discern the communicative styles of others, since our perception of the world constantly teeters between being dominated by the visual or by the auditory.

Thus, when we are communicating with someone, we focus our energy either on listening closely to what they say _or_ watching for clues in their body language that might give us insight into their intentions or motivations.

Unfortunately, however, we aren't very good at doing both simultaneously, and we therefore often miss crucial elements of people's communication.

Secondly, communication is difficult to interpret because single behaviors can actually have many causes and be interpreted in more than one way.

What does it mean, for example, when your conversation partner breaks eye contact with you? You might suspect that they're _unable_ to look you in the eye because they're deceiving you. Yet again, it could also simply be that they have bad manners, or lack the self-esteem, confidence or social sophistication required to hold eye contact.

So how do you know which it is? Well, you can't.

### 3. Humans are bad at lie detection because we have biases preventing us from evaluating information objectively. 

Have you ever watched a football game with a die-hard fan of one of the teams? Chances are there was a lot of cheering, some yelling and maybe even a little cursing. How did they perceive the events on the field? They probably judged the referee's calls based on whether they helped or hurt their favorite team.

This slanted, "non-neutral" perception of events is called a _bias_. We can't help that we're biased and, in fact, biases are very much a part of who we are: since they originate from our experiences and personal beliefs, they are quite difficult to avoid.

Your football-fanatic friend isn't aware that his perceptions are biased; he simply feels that he's expressing himself honestly. An outside observer, however, who sees the referee's call and your friend's biased reaction, can see that these biases are indeed there.

But what does bias have to do with lying?

Our biases dictate how trustworthy we believe people to be. In other words: we naturally believe that certain people will not lie to us.

For example, the leader of a satanic cult in California was accused of molesting about sixty children based solely on the accusation of one thirteen-year-old girl. The cult leader, of course, denied it all.

In spite of the satanist's steadfast denial, everybody was convinced that this girl was telling the truth until she was finally interviewed by one of the authors, where she admitted that everything was completely fabricated.

But _why_ did people believe her?

In part, it's because the people she told her story to felt that little girls were more trustworthy than satanists. This bias caused them to approach the accusation with their minds already slanted in one direction, thus causing them to miss her lie.

The moral of the story? Don't underestimate the influence of biases on your decision-making.

> "_People believe lies not because they have to, but because they want to."_ — Malcolm Muggeridge

### 4. Don't be fooled by truthful behavior; lies can be hidden within. 

As you've learned so far, we are naturally not very good at detecting lies. As if that weren't enough, liars also use tricks to mask their dishonesty! If you know how they accomplish this, then you stand a better chance of resisting their manipulation.

Often, liars will use our own biases against us and imitate _truthful_ _behavior_, i.e., behavior that we normally associate with honest people, in order to sneak their lies past our defenses.

One way they accomplish this is by peppering their lies with other information we feel is reasonable.

Imagine, for example, that you're a teacher and you accuse a student of cheating on an important exam. They might respond by telling you, "I'm an honest person! I wouldn't want to jeopardize my studies by cheating!"

Sounds pretty reasonable, right? Inevitably, part of you will think that it even sounds like something _you_ might say in the same situation.

That's because we're naturally biased to believe people rather than doubt them. We look for reasons to believe people, and sometimes they provide us with them.

Luckily, we can circumvent this pitfall by simply ignoring truthful behavior altogether when trying to determine whether to believe someone.

For instance, when someone offers direct and spontaneous answers to your questions, it's not necessarily to their credit. They might just be well-prepared liars trying to emulate truthful behavior.

So even though that student's reply seems convincing, it's actually irrelevant. The issue isn't whether they would "jeopardize" their studies, but whether or not they cheated.

A good way to respond to these kinds of statements is to acknowledge what they said, perhaps by saying, "I've heard this about you," before you address the actual topic once more by asking something like, "What happened during that exam?"

Only the answer to this question matters. While it might seem counterintuitive, excluding truthful behavior from your decision-making is crucial when detecting lies.

### 5. Liars try to avoid lying bluntly by either not answering questions directly or not answering at all. 

If you've ever watched a US court drama, you've probably heard the question, "Do you solemnly swear or affirm that you will tell the truth, the whole truth and nothing but the truth, so help you God?"

This oath actually highlights three different types of lies, all of which we'll explore in the next blinks.

The first, called _lies_ _of_ _omission,_ deal with "the whole truth." They're the easiest lies for liars to tell since they're less direct than other types of mistruths.

Lies of omission are lies where people simply leave out crucial details of their story rather than lying directly.

One way liars do this is by simply skipping over compromising details.

For example, if someone goes on a business trip and does something that might get them in trouble, they'll simply gloss over those details when recounting their trip to family and friends.

They also might generalize their story in order to avoid lying directly.

Luckily, you can identify lies of omission if you hear lots of qualifiers, such as words like "basically, probably, usually" or "mostly."

So if you ask someone how their business trip was, they might respond that they "mostly" just stayed in the hotel and worked on their presentation. But what _else_ did they do that they haven't told you?

Another way people lie by omission is by electing not to answer the question at hand.

When a liar feels cornered, they may try to buy time by repeating or reinterpreting the question, perhaps by claiming they don't understand, or by referring to earlier statements they've made.

These tactics all indicate that they want to avoid having to lie directly. Ultimately, if you ask for information and people don't supply it, they probably have a reason.

### 6. A liar might try to hide their mistruths in a web of details or by appealing to their upstanding reputation. 

The next type of lie, called _lies of commission,_ deals with the "_tell the truth_ " part of the oath.

Lies of commission occur when liars feel the need to increase their credibility in order to avoid revealing the truth. When a simple lie wouldn't be believable enough, they embellish their lies with details in order to _make_ them believable

For example, liars might give overly specific details about an event to cover up the truth. If you accuse them of stealing, they might spill a deluge of details about why they couldn't be suspects.

They might say, "But I couldn't have stolen that money! I was at work _until_ _3:45_, and it took _12_ _whole_ _minutes_ on the _35_ _bus_ before I got home!"

Another way of making a lie more credible is to "dress up the lie," i.e., to make a sudden shift in polite language. For example, liars might suddenly start calling you "sir" or "ma'am," or even invoke religion by swearing "to God" that they are telling the truth.

The last type of lie deals with telling the truth and "_nothing_ _but_ _the_ _truth,_ _so_ _help_ _me_ _God_." These are _lies_ _of_ _influence_ — the most powerful kind of lie.

If a liar has no facts in his arsenal to combat your questions, he may choose instead to convince you of his uprightness rather than give truthful information.

For example, if you suspect someone of stealing from your company, they might respond: "I'm an honest person with a reputation to keep! I've worked here for more than half of my life and am about to retire. Why should I risk my pension by stealing?"

Indeed, these lies of influence are powerful and difficult to detect because they seem so reasonable; we _want_ to believe them.

Although all three types of lies can be difficult to catch, you can still spot them as long as you're aware of them.

### 7. Lying is a stressful activity that can show up in physical responses. 

Think back on a situation in your childhood when you had to lie to somebody about something important. How did it feel? Did the blood rush to your head? Did you get cold, sweaty palms and dry mouth? You may have even started nervously shuffling your feet in an effort to stay calm. Why is it that we respond this way?

Having to lie in the face of incriminating questions raises our stress level, which then triggers the _fight-or-flight_ _instinct_. This powerful instinct instantly prepares your body to face threatening situations. Its roots are buried deep within the brain and we cannot turn it off, which explains why children often try to run away from difficult situations.

In addition, lying causes activity in the hands and face. When we are under stress, our blood flow is drawn away from our extremities, such as our hands, nose and ears. This, in turn, causes these parts of our body to itch and grow colder, and our hands involuntarily move toward these areas to scratch or rub them.

That's why, for example, your palms were sweaty as a child while you were struggling to lie.

Furthermore, liars will also try to dissipate their anxiety in an effort to remain calm by adjusting their _anchor_ _points_, or the parts of the body, such as the hands, feet and buttocks, that lock us into a fixed position.

CIA questioners make it more obvious when someone adjusts their anchor points by placing the people they question in a swivel chair to amplify their motion.

You probably feel lucky _they_ weren't around to catch you in your lie!

### 8. Liars often display signs of deception; judging their timing and frequency can indicate whether you’re being deceived. 

Now you're familiar with some of the ways to spot clues that people are lying. Could you pick out a liar from the crowd? Not quite. Just because you recognize one of these clues in someone's body language doesn't necessary mean they're lying.

Unfortunately, the reality is that _deception_ _detection_ is not an exact science.

Indeed, human behavior and communication are simply too complex to be reduced to generalizations like, "his eye twitched; he must be lying!"

In fact, behaviors which we might at first consider deceptive may actually have been perfectly innocent.

For example, some people are just naturally anxious, so their profuse sweating may not be a sign of deception, but simple nervousness.

But that doesn't mean that the behavior of liars doesn't betray their dishonesty!

One way to detect lying is to look for _clusters_, i.e., the presence of two or more clues that indicate deception. The presence of clusters can mean that you're being deceived.

For example, if you ask a child whether they did their homework and they snap back asking, "Why don't you trust me?" with their eyes shut and feet shifting, then you would have reason to think that they're lying. Although these clues might be harmless in isolation, they're quite meaningful as a deceptive cluster.

It's also crucial that you catch these clues within five seconds of the question. In that time, we sift through about 625 to 750 different words, after which the child's brain will likely have moved from the accusation to something else, thus no longer offering you these subtle clues.

So, while it's impossible to become a "human lie detector," we can at least catch a lie or two by looking for the right clues.

Now that you know how to detect deception, the next blinks will offer you insight into the kinds of questions you can ask to coax people into revealing their dishonesty.

### 9. Deliver your questions in a neutral way and pay attention to the suspects’ answers. 

Think of the last time you made an involuntary slip of the tongue and revealed your unconscious motives, i.e., a _Freudian_ _slip_. The same thing can happen to liars when they're making an effort to cover up the truth, thus accidentally revealing their secrets.

In order to catch a liar in this trap, you must pay careful attention to the exact, literal meaning of the answers they provide.

Often, while calculating their response, a liar can unintentionally convey truthful information.

Imagine, for example, that you're interrogating someone who finally says:

"Truthfully, I cannot answer that question again, and I don't understand why you keep asking. I've already given you a perfectly plausible answer!"

Unbeknownst to our subject, the words "truthfully" and "plausible" reveal that he's aware that he's lying!

But in order to be able to use clues, you first need to know how to draw them out of your subject.

To do so, you must ask precise questions. If you ask an ambiguous question, like "When did you leave work yesterday and how did you get home?" it's hard to know which part of the question causes the subject to react, thus giving him more "wriggle room" to formulate his answer.

Moreover, people think ten times faster than they talk. Asking lengthy questions gives your subject extra time to prepare his lies, so keep your questions straightforward.

In addition, you should be non-confrontational and ask your questions in a calm, neutral, matter-of-fact manner.

You want to ensure that a deceptive response is triggered by the content of the question; _not_ by your delivery.

For example, if you provoke someone's anger, they might display many of the clues associated with lying, such as sweating or shuffling their feet. In that case, you won't know whether they're lying or just ready to smack you!

### 10. Be non-confrontational when you suspect a lie and avoid making the liar repeat it, as it makes them more comfortable. 

Nobody enjoys being lied to. But how should you react when you suspect someone of lying to you? Is it best to confront the liar? If so, then how should you proceed?

First and foremost, you should resist the urge to be confrontational: in order to be entirely sure someone is lying, you ultimately need them to cooperate and tell you the truth.

If you identify a logical inconsistency in their statement and immediately bring it up, the person will be extra cautious and less likely to cooperate. If this happens, you might never find the truth.

Instead, you should simply continue with your probing in a friendly manner.

And when you do, it's also best to avoid asking the same question repeatedly. Because the more often people repeat a lie, the more easily it comes to them and the harder it is to detect.

Although one popular belief is that repeating questions offers liars a chance to come clean, in actuality, liars tend to simply entrench themselves in their lies, thus making them even easier to repeat.

Imagine you ask someone, for example, whether they've ever used illegal drugs, and they reply that they tried marijuana once. If you immediately ask question about "when, where, how much," or "with whom," then they'll likely think of you as an adversary and put up their defenses.

Worse, if they've already lied to you about their marijuana usage, this line of questioning just allows them to repeat the details of their story, thus making it easier for them to lie about other things.

The smarter approach is to allow them to change their story by asking the right follow-up question, such as, "Okay, well then what other things have you tried?"

### 11. Disrupt the liars’ game by coaxing more information from them than they’d planned to reveal. 

Most of us are bad liars. And we're also aware of that, so when we lie, we like to be prepared to make the best of it.

Indeed, a liar will have a "game plan" of which facts he'll admit and which he'll lie about, and will expect questions regarding those topics.

If you follow a liar's game plan, he'll only give you the information that supports his story, and you won't get what you need to detect his lies.

For example, someone who's robbed a bank will expect questions regarding his whereabouts, whether he had access to the vault, etc. In most cases, he'll have a plan to address these questions.

You should therefore try to disrupt his game plan by probing for more information than he initially provides.

When the liar starts adapting or changing his game plan to respond to your questions, he is more likely to slip up and give you clues. But how do you make him do that?

One way is to use follow-up questions to convey that continuing to withhold information will be difficult, such as "What else?" or "How do you know that's true?"

You can also use _bait questions_ that might make a liar consider the possible consequences of his game plan. For example, you could ask, "Might your former employers see that differently?" This question implies that you might follow up with their employer, thus disrupting their game plan.

In addition, when your subject gives you information, broaden your focus.

If a liar finally provides a number of details about his story, explore each detail in reverse order since the last detail is the one given most reluctantly and therefore most likely to disrupt his plan.

### 12. Final summary 

The key message in this book:

**Communication is too complex for us to spot every lie. Nevertheless, lie detection is possible. There are tiny clues which sometimes indicate dishonesty and if you spot a cluster of these in a short space of time then that person may be lying. By asking precise questions and paying close attention to the reactions of your subject, you stand a chance at distilling truth from lies.**

Actionable advice:

**Agree with liars to neutralize their lies.**

Liars often make appeals to their credibility rather than answering your questions directly in an attempt to make you question whether they're legitimate suspects. The best way to neutralize these _lies_ _of_ _influence_ is to simply acknowledge them by saying things like, "I know you work very hard. And I think everybody knows this round here," and then steering the conversation back to the subject at hand.

**Suggested further reading: _What Every BODY is Saying_ by Joe Navarro**

This book is all about the hidden meanings we can find in our body language. Our brains control our body movements without us being conscious of it, and sometimes those movements can be very revealing. This book goes into detail about the ways you can train yourself to become an expert in observing other people's nonverbal cues and uncovering their meaning.
---

### Philip Houston, Michael Floyd, Susan Carnicero and Don Tennant

All the book's authors have worked for the US government in organizations such the NSA, the CIA and the US Army Military Police. All are recognized for their competence and experience in the field of conducting of interviews, interrogations and polygraph examinations and have worked all over the globe. They are also the founding partners of QVerity, a company offering deception detection training and consulting.

