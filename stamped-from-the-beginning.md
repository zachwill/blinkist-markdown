---
id: 5a5b9bd0b238e100078c4feb
slug: stamped-from-the-beginning-en
published_date: 2018-01-17T00:00:00.000+00:00
author: Ibram X. Kendi
title: Stamped from the Beginning
subtitle: The Definitive History of Racist Ideas in America
main_color: BF2628
text_color: BF2628
---

# Stamped from the Beginning

_The Definitive History of Racist Ideas in America_

**Ibram X. Kendi**

_Stamped from the Beginning_ (2016) offers a powerful examination of the modern history of racism in the United States, including where racist ideas originate and how they spread. In particular, the author looks closely at how the presidential campaigns and administrations of Richard Nixon, Ronald Reagan and Bill Clinton have helped propagate racist thought and had a detrimental impact on America's black communities.

---
### 1. What’s in it for me? Get woke to the realities of racism in America. 

When Barack Obama was elected US president, some commentators discussed the emergence of a post-racial America, suggesting that racial discrimination was now officially a thing of the past.

If only it were that easy. Thanks to scholars like Ibram X. Kendi and movies like _The 13th_ and _I Am Not Your Negro_, the general public is getting closer to the truth: racism is so deeply ingrained in the fabric of American society that one black president can't reverse hundreds of years of oppression.

As Kendi makes clear, racism and discrimination were fundamental in developing federal policies that are still in place, thanks in part to politicians who actively worked to court the racist vote while keeping black voters oppressed.

In these blinks, you'll learn

  * how Richard Nixon and Ronald Reagan worked to attract racist voters;

  * how Bill Clinton helped imprison millions of nonviolent citizens; and

  * how statistics help promote discrimination.

### 2. The presidential campaigns of Richard Nixon and Ronald Reagan featured a fresh spin on old, racist ideas. 

How can presidential candidates win over racist voters without sounding explicitly racist themselves? This was the question Richard Nixon was mulling over during his election campaign in 1968, and his eventual strategy was to appeal to racists without actually deploying the terms "black people" or "white people."

More specifically, Nixon's campaign was hoping to appeal to "nonracist racists."

These were people who weren't obvious or aggressive in their racial discrimination, but who did see black neighborhoods as dangerous places to avoid and considered black schools to be inferior to white schools. Most of the time, people with these ideas wouldn't believe or admit that they had any racists tendencies.

Nixon understood how these people thought and pandered to them in TV ads that featured foreboding music over a montage of violent images captured at civil rights protests. Then came Nixon's voice, assuring "I pledge to you, we shall have order in the United States."

As a private recording revealed, Nixon praised the ad by saying it "hit it right on the nose. It's all about those damn Negro-Puerto Rican groups out there."

There's little doubt that Nixon's tactics were effective. In a 1968 Gallup poll, 81 percent of respondents believed the part of Nixon's campaign platform that stated, "Law and order has been broken down in the country." Nixon went on to win the election.

As the United States was about to enter the 1980s, Republicans began using a new approach to appeal to white America's underlying racism.

In his campaign against incumbent president Gerald Ford to become the Republican Party's presidential candidate in 1976, the governor of California and former actor Ronald Reagan used the story of Linda Taylor to link black people receiving welfare to criminal behavior.

Taylor was a black woman from Chicago who earned $8,000 through welfare fraud. It wasn't a common crime, but Reagan held her up as a typical example and often exaggerated her story by saying, "Her tax-free cash income is over $150,000."

### 3. Reagan’s racist policies would have devastating effects for black people. 

After his electoral victory in 1980, within the first year of Ronald Reagan's presidency, the _New York Times_ reported that most of the government programs that had been established to combat poverty had been "wiped out."

Reagan quickly made his intentions clear by approving tax cuts for the rich and increasing the military budget. His way of balancing the budget for these policies was to cut funding for social programs that benefitted middle- to low-income households.

The effects were devastating: within Reagan's first year, black families' average income fell 5.2 percent, while 2.2 million more Americans were facing poverty.

But Reagan had more plans up his sleeve that would cause even further damage to minority families: in 1982, the federal government's war on drugs officially began.

The timing for this so-called war was questionable, as drug crimes were on a downward trajectory and only 2 percent of the nation saw combating drugs as a top priority. Nonetheless, Reagan's directive served to make law enforcement more aggressive, particularly in making drug-related arrests.

By 1985, the unemployment rate of black youths was four times greater than it had been in 1954, and it only got worse as the US Drug Enforcement Administration (DEA) made a concerted effort to bring crack cocaine to the media's attention.

This task fell to DEA agent Robert Stutman, who flooded the press with sensational stories about "predatory" crack dealers, crackheads and crackwhores. The picture that was painted by the government made it seem like all the characters in this story were black.

Then, in October 1986, Reagan signed the Anti-Drug Abuse Act, which gave anyone caught with at least 5 grams of crack a minimum five-year sentence in jail. The majority of the people prosecuted under this act were black people from poor families. Meanwhile, for regular cocaine, a drug popular with rich white people, a five-year jail sentence required possession of at least 500 grams.

### 4. The Anti-Drug Abuse Act led to the mass incarceration of Black Americans and contributed to the false notion of “dangerous black neighborhoods.” 

The United States Constitution sets out a limit of two consecutive four-year terms per president, so that one individual can't have too much influence — but Ronald Reagan's damaging policies continue to be felt.

Between 1980 and 2000, there wasn't an increase in crime rates, yet the US prison population quadrupled thanks to the stricter sentencing policies introduced by the Reagan administration.

Starting in 1985, two-thirds of that increase came from drug offenses; by 2000, 62.7 percent of all drug offenders held in state prisons were black, while 36.7 percent were white.

This might lead you to think more black people were involved in drug use; however, the National Household Survey on Drug Abuse in 2000 found that an equal percentage among both white and black people used illegal drugs: 6.4 percent. This statistic is backed by other studies as well.

In 2012, the National Survey on Drug Use and Health found that white youths were 32 percent more likely to sell drugs than black youths. It also found that despite this data, black youths were far more likely to be arrested for it.

This is the scenario that played out during the height of the crack craze in the late 1980s and early 90s. Both white and black people were using and selling illegal drugs at similar rates, yet black users and sellers were getting arrested and convicted at a much higher rate.

In 1996, two-thirds of crack users were white or Latino, yet 84.5 percent of the defendants in crack-related court cases were black.

Another racist myth that has persisted in America is the assumption that white neighborhoods are safer than black neighborhoods. This notion has affected the housing market, policing and countless other factors.

But in reality, crime isn't caused by race; it's caused by economics. When the National Longitudinal Youth Survey studied data collected between 1976 and 1989, the initial results showed that young black males were far likelier than white males to be involved in a serious violent crime. But when the researchers took employment rates into consideration, the racial disparities disappeared.

### 5. Racist notions and statistics were biased by racism, but beneficial to certain politicians. 

If you only look at certain statistics, you might think black neighborhoods are indeed more dangerous places than other areas. A black neighborhood might have a higher arrest rate, but that's because the area has a greater police presence — and the neighborhood is subject to more active policing because there's more perceived crime going on.

This logic loop brings us to the ongoing problem of how the methods used to get certain statistics can also be prone to discrimination.

Let's look at statistics on violent crimes. Even though drunk drivers kill far more people than "urban black violence," drunk drivers are not taken into account in violent crime studies. One might wonder if this is because 78 percent of the drunk drivers arrested in 1990 were white.

Drunk drivers also kill more people than cocaine does. In 1986, there were 21,702 cocaine-related deaths, while drunk driving claimed 23,990 lives. So it's safe to say that suburban roads pose a greater threat to Americans than inner-city violence.

Perhaps more damaging to public opinion than statistics is the skewed media attention given to black neighborhoods and families.

A typical story from 1986 was a special report on CBS News called "The Vanishing Family: Crisis in Black America." It featured a series of stories, all about young moms struggling to raise a family due to absent fathers. The general conclusion of this report was that black men neglect their responsibilities, which happened to support the opinion spouted by the Reagan administration, which blamed "predatory ghetto subculture" on a lack of "hard work, education" and "respect."

Like Nixon before him, Reagan benefited from pandering to the racist vote. But Reagan went a step further and put millions of poor black people behind bars due to nonviolent, drug-related crimes. In jail, these people were stripped of their voting rights and, thanks to Republican voting laws, they'd still be unable to vote after they got out.

It's not hard to imagine that at least a few elections between 1980 and 2000 might have turned out differently if not for the war on drugs. The 2000 presidential election was especially close, as were seven senate elections that went to Republicans.

### 6. Reagan’s tough-on-drugs policies were devastating to black Americans, a legacy that Bill Clinton would continue. 

As the United States transitioned from the 1980s to the 90s, the George H. W. Bush administration continued to feed the media frenzy on the so-called crack epidemic. This included wildly exaggerated stories about babies being born to crack-addicted black moms.

These stories even had a UCLA pediatrician claim that "crack babies" were born without the brain function that "makes us human beings." Research would later disprove such claims, but the dehumanizing of the black population would continue under the next president, Bill Clinton.

On the campaign trail, Bill Clinton sounded like an exciting new voice in American politics. But underneath his liberal fiscal agenda was yet another message aimed at the typical "nonracist racist" voter.

In not wanting to appear weak on crime, a standard criticism against Democrats, Clinton came across as proud that he approved the execution of Ricky Ray Rector, a mentally impaired black man.

When talking about the 1992 Los Angeles riots, Clinton referred to the protestors as "lawless vandals" with "savage behavior." There was little sympathy for the real cause of the riots, which was the acquittal of four LAPD officers who were truly savage in their beating of Rodney King, an unarmed black man.

In another appeal to racist voters, Clinton made a campaign promise to "end welfare as we know it." After Clinton won the election, he made another promise during his first State of the Union Address: to pass a "tough crime bill."

He followed through on that promise in August of 1994, by enacting the Violent Crime Control and Law Enforcement Act, the biggest crime enforcement bill in US history. It created dozens of new federal laws, including a "three-strikes" rule that made a life sentence mandatory for certain repeat offenders.

This act gave billions in funding to the prison system and paved the way for the US prison population's biggest increase ever — most of which was due to nonviolent drug offenses.

As a presidential candidate, Clinton claimed that no Republican would be tougher on crime than him, but what he might as well have said is: no one would have found a way to lock up more of the black population than him.

> The Violent Crime Act also contained a rule making it virtually impossible for poor criminals to receive higher education in prison.

### 7. Racist thinking held that personal responsibility was the solution to black communities’ problems. 

As you probably know, blaming the victim for their situation is a common occurrence, and this was certainly the case in the 90s. All too often, citizens were told that poor people shouldn't receive help from the government because they're lazy and undisciplined.

The author identifies two kinds of racist thought that were still common in the 90s.

The first group are _assimilationists_. They acknowledge that racism is a factor, but believe black people are also to blame for their own troubles.

The second group are _segregationists_. They believe black people are solely to blame for their troubles.

So, both of these groups would agree that the reason there was a disproportionate amount of single moms in the black community was due to them being more promiscuous than white women. However, assimilationists would add that a black woman could learn to be more disciplined and behave more like a white woman.

Segregationists, on the other hand, would suggest that the best solution in this case would be for black women to be sterilized or to receive an injection of a long-term contraceptive such as Norplant.

Despite tests that revealed a number of severe side effects, the US Food and Drug Administration approved the use of Norplant in 1990. The following year, former Grand Wizard of the Klu Klux Klan, David Duke, suggested that Norplant be used to reduce the number of black women receiving government welfare, as part of his campaign for governor of Louisiana.

Sadly, David Duke wasn't the only politician airing out his racist thinking. The difference was, others were couching their ideas under the label of "personal responsibility."

In 1994, Republicans were elected to be the majority in Congress after running campaigns that appealed to the prized racist vote. The popular terminology this time was that black people needed to take personal responsibility for their socioeconomic situation rather than counting on government assistance.

In 1996, President Clinton showed his support for this idea by signing the Personal Responsibility and Work Opportunity Reconciliation Act (PRWORA) into law. This introduced new limits to federal welfare, which were supposedly intended to give people more of an incentive to gain employment.

### 8. The notions that black people are to blame for discrimination, and that racism is over, are persistent but false. 

Have you ever heard someone claim to be "racially color-blind," or say that "race doesn't exist" for them? They may think they're being progressive, but this is still racist thinking.

This idea is actually quite similar to the way the government in the 90s was chipping away at social programs that benefited poor black families. By doing so, the government was pretending that something very real didn't exist, while also giving a generation of people the impression that black people were to blame for their own problems.

In 1996, the state of California banned the practice of affirmative action, which gives an incentive to public employers and universities to hire or enroll minorities. Those who supported the ban claimed the practice was "reverse discrimination."

Unsurprisingly, once this practice stopped, the percentage of black students at University of California schools declined.

Around the same time, the Republican Speaker of the House, Newt Gingrich, declared that "racism will not disappear by focusing on race." This might sound rather illogical, yet it seemed to start a whole new trend of people believing that ignoring the issue of race, or being "color-blind," is the answer.

In his 1997 book _Liberal Racism_, journalist Jim Sleeper suggested that anyone who wasn't color-blind was racist. That same year, in their book _America in Black and White_, political scientists Abigail and Stephan Thernstrom claimed that "race-consciousness policies make for more race-consciousness; they carry American society backward." They also wrote that "few whites are now racists."

However, a more accurate way to look at this talk of color-blindness is as a way of avoiding the very real subject of ongoing discrimination and racial inequality.

Anyone who believed racism was extinguished with the election of Barack Obama in 2008, need only see the white nationalist rallies accompanying the election of Donald Trump as a sign that this isn't the case. Ignoring something won't make it go away, and for anyone who hopes to fix a problem like racism, the first, fundamental step is to fully understand how deep the problem goes.

### 9. Final summary 

The key message in this book:

**Racism is a constant theme throughout the history of the United States and can still be seen quite clearly among the country's modern-day citizens and current government. For generations, politicians have stoked underlying racism among American voters to gain their votes, win elections and push forward damaging policies. Racism isn't limited to discriminatory actions; it extends to the way people ignore the issue as well, which only helps perpetuate the harm done to black communities.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Between the World and Me_** **by Ta-Nehisi Coates**

_Between the World and Me_ (2015) is an open letter to the author's 15-year-old son about the realities that face black men in America. Filled with personal anecdotes about the author's personal development and experiences with racism, his letter tries to prepare young black people for the world that awaits them.
---

### Ibram X. Kendi

Ibram X. Kendi is assistant professor of African-American history at the University of Florida. He is also the author of the award-winning book, _The Black Campus Movement: Black Students and the Racial Reconstitution of Higher Education, 1965-72._

