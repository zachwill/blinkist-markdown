---
id: 543b85333662390008030000
slug: start-something-that-matters-en
published_date: 2014-10-14T00:15:00.000+00:00
author: Blake Mycoskie
title: Start Something That Matters
subtitle: None
main_color: 30ABD6
text_color: 1F6E8A
---

# Start Something That Matters

_None_

**Blake Mycoskie**

_Start Something That Matters_ gives concrete, colorful and often humorous advice on overcoming your fears and starting a company — even if you don't have business experience or many resources at your disposal.

---
### 1. What’s in it for me? Learn how to overcome your fears and start your own business. 

Many of us dream of starting our own business, whether that means selling handicrafts from a market stall or developing a global behemoth that influences the lives of millions of people. And yet, how many of us actually take steps to realize our dreams?

Too few. Most of us are just scared to start. We're scared that our business won't be successful, that we don't have sufficient resources, that we're not good enough to lead a company.

This kind of fear holds us back and prevents us from taking a risk. Often, we give up altogether.

These blinks will show you how to overcome your fear and deal with the challenges that arise. The author, using the in-depth knowledge he gained by starting his own company, shows you how to follow in his path and become successful while still making the world a better place.

In these blinks you'll learn

  * how to recycle other people's business cards;

  * why your employees deserve a reward for making mistakes; and

  * why you should let bad things happen.

### 2. Fear is totally natural – but it can have a damaging effect on our lives when it goes unchecked. 

Even though we don't always like to admit it, all of us get scared sometimes. Fear can seem shameful or embarrassing, but the truth is, it's perfectly natural.

That's because fear typically arises when we encounter a situation we don't know how to deal with or simply haven't experienced before.

Going to a job interview, talking to strangers in a foreign language or starting a business are all typical anxiety-producing situations, because we don't encounter them frequently and we can't easily predict their outcome.

This fear of failure is so uncomfortable that sometimes it causes us to miss out on things we actually want to be involved in.

For example, someone who fears airplane travel will look for alternatives or might try to avoid long-distance travel altogether.

Unfortunately, in our society, we don't like to talk openly about our fears. It can make us feel embarrassed or ashamed to be scared of something, especially if we think others will view our fears as trivial. But when we don't talk about what we're scared of, we often can't overcome it. And these persistent fears can severely damage our lives.

To understand how this works, imagine a person who's afraid of job interviews. If she doesn't actively confront and combat her fear, she won't be able to look for new, better jobs.

Or think of the man who's frightened of air travel. His fear might cause him to miss out on family holidays, interesting conferences, promising job opportunities and inspiring overseas travel.

Holding on to your fear simply isn't worth everything you might lose as a result. Or, as President Franklin D. Roosevelt famously put it, "The only thing we have to fear is fear itself."

### 3. If you want to overcome a fear, start by identifying it. 

How can you overcome your fears? The best way is simply to _face them_ : Do what you're scared of and let whatever happens as a result, happen.

Yes, it's scary. Fear is one of the most powerful emotions we have, so in many cases, it feels like a deadly threat.

But most of the time, when we finally confront what we're afraid of, we discover that our fears were relatively minor and easy to overcome after all.

For example, some people are afraid of dogs. Every time they see a dog, they try to avoid it. But if they were to muster their courage and pet a friend's dog, they would quickly realize that most dogs aren't a threat at all — in fact dogs are typically incredibly nice!

You can prepare yourself for doing what you're afraid of: When you decide to confront a fear, start by making a list of all the reasons you're scared.

That's what the author did when he was about to start his shoe business, _TOMS_, in Argentina. He was terrified: He didn't have any business experience or a whole lot of resources at his disposal. In fact, he was tempted to give in to his fear and scrap the plan altogether.

But instead, he made a list. On one side, he wrote down all the things he was scared of; on the other side, he tried to come up with the worst thing that could happen if his fears came true.

One of the big things he was scared of was that nobody would buy his company's shoes, which would cause him to lose his entire investment — and all the time he'd put into it.

It was an awful thought, but when he'd written it down, he realized that after all, it wasn't so bad. Because he still would have come out of the situation with all the new skills, experiences and friends he had acquired during the process. So what could be left to fear?

### 4. You can never predict what’ll happen in the future – so don’t let uncertainty hold you back. 

New business ventures are a natural breeding ground for fear, especially when it's our first attempt at starting a business. It's terrifying to know that you might take the wrong approach, fail and lose all your money. These fears prevent many people from taking the leap at all.

We needn't be so scared. Some of this fear comes from the misguided assumption that we need to have tons of expertise and experience to embark on something new. That way, we'll be able to predict exactly what'll happen.

And yet, this is faulty reasoning. _No one_ can possibly know exactly what will happen in the future. Thus, planning the "perfect" risk-free strategy is impossible.

For example, sometimes we hesitate to act because we're waiting for the timing to be perfect. But if you sit around waiting for the best time to make a move, you might never make a move at all.

So if you have a crush on someone but never ask her out, eventually someone more decisive will swoop in — and your chance will be gone.

Luckily, there are a few easy strategies that will help you overcome your fear and get moving, at least with regards to your business pursuits.

You can start by reading biographies and blogs by successful entrepreneurs, which will allow you to identify their personal fears and coping strategies. Chances are, they had their own fears when they were starting out. How did they overcome them and reach success?

Another way to master your fears is to write down your goals and hang them over your desk. You can also put up motivational quotes like this one: "Success is the ability to go from one failure to another with no loss of enthusiasm."

It also helps to surround yourself with inspiring people, like enthusiastic interns or supportive friends. Finding other people who want to share your goals will keep you going when times are tough.

### 5. If you want to create something successful, keep it as simple as possible. 

When we encounter something very complicated, we tend to think that it must be very sophisticated. So when we look, for example, at a big city map with multiple lines and train stations, we are awed by this impressive feat of urban planning.

However, this way of thinking is misguided. When you live in a big city, the number of delays and cancellations you encounter on that "sophisticated" subway should prove that complexity doesn't always equal brilliance.

In fact, for the most part, the opposite is true. If you want to create something successful, keep it as simple as possible.

This is especially true in the case of design: The most popular designs tend to be incredibly simple.

_Apple_ products are a clear example of this principle. The _iPod_ became wildly successful because it had clean, unfussy lines and it was easy for anyone to use, regardless of their tech savvy.

Simplicity is also useful on the broader level of strategy and business model. For example, _In-N-Out Burge_ r in the United States is extremely popular because it focuses purely on making the best possible burgers for its customers. The chain doesn't waste its resources on fancy décor or long, complicated menus.

If you want simplicity to be a guiding principle in your life, one rule of thumb is to concentrate on the few things that _really_ matter and pretty much ignore everything else.

Learn to let irrelevant "small bad things"_just happen,_ and you'll find it much easier to stay motivated to achieve the big important goals.

Think about this way: Paying a small late fee on a DVD rental is better than missing an important appointment just so you can swing by the rental store. It's all about priorities.

Keeping it simple on a daily basis will improve your work routine and, in turn, your final product.

### 6. It’s possible to grow a successful business with limited resources. 

So we've learned that simplicity can benefit us in our life and work. Well, another great thing about simple products and solutions is that they don't demand many resources.

For example, the author's wildly successful shoe business (and philanthropic organization) _TOMS_ proves that it's possible to make the very best of limited resources:

When the author was first starting out, he had very, very little money. You might think this would have limited him, but in fact, the author approached his constraints with a creative mind-set.

In the very beginning, he didn't even have enough money to buy business cards. So he handwrote his name and contact info onto other people's business cards, after scratching off the original info.

The tinkered cards were well-received, because they showed potential investors and business partners that he was truly economical and committed to not wasting money when there were creative solutions available.

Also, today there's so much technology and information available online for free that no one should feel limited by a perceived lack of resources.

So, for example, instead of hiring a publicist you can easily and cheaply promote your products by blogging and using social media. Nowadays, these free tools are just as good as paid solutions.

You can also save money and boost productivity by using free services like _Doodle_ to arrange business meetings, and _Google Drive_ for organizing paperwork and hosting documents online.

In the end, limited resources don't have to hold you back. Instead, they can be an opportunity to build a healthy business model that's based on simplicity.

### 7. Stretch your personal network and surround yourself with like-minded people to build a motivated, enthusiastic team. 

Few successful businesses have been built by hermits. In order for your new venture to succeed, you'll need a lot of help from other people — be they acquaintances or staff.

To draw people in, start by mapping out your _personal network_. Make a list of friends, family members and colleagues you know.

Each person on your list will have their own unique abilities and knowledge that could add value to your business model.

Consider _Wikipedia_ as a model: The extensive online encyclopedia is built from countless little entries and corrections on a huge range of subjects, and all of the work has been done by unpaid non-professional contributors from around the world. Each contributor donated just a small portion of their time and knowledge, but together all these contributions added up to a huge trove of information.

In addition to tapping into the unique skills and knowledge of those around you, it's also important to look for passionate, like-minded people who share your goals.

When you start a business, the first people who get involved should be enthusiastic. The right people will believe in your project and be excited to be part of something new. Together, you can create a powerful working atmosphere that will enhance your own abilities and knowledge.

You should also try to cultivate a _garage mentality_ within your business. Many successful companies, like _Apple_, were first started in garages on a shoestring budget. Of course, you don't have to literally go work next to a parked car — you can create a "metaphorical garage" within your company by promoting an atmosphere of togetherness and shared adversity.

In most cases, you don't need a lot to engender a sense of joy and motivation within your company, both for your employees and for yourself.

### 8. A “good” company has giving at its core and a sense of humor about its own mistakes. 

What kind of business do you want to run? One that's respected by its employees and the community around it, or one that is universally reviled but at least makes a lot of money?

Most people would probably go with the first option, but why? What are the benefits of being a "good" company?

There _must_ be benefits. After all, most successful companies have some element of philanthropy and _giving_ at their core.

The famous Manhattan department store _Saks Fifth Avenu_ e works with the organization, charity: water, to help provide clean drinking water to people in the developing world. The company donated its windowspace to raise awareness and solicit donations for the charity's cause.

This partnership helped bring clean water to the developing world, promoted good feelings within _Saks Fifth Avenue_ and improved the company's public image.

In addition to caring for your public image, it's also important for your employees to think highly of you — so create a working environment that is centered on _trust_.

How can you do this?

By being understanding when your employees make mistakes. After all, mistakes will happen, but they're rarely truly disastrous or irreversible.

In fact, mistakes can even be very useful learning experiences, as long as the working environment is built on mutual trust and support.

The advertising company _Brogan & Partners_ excels at cultivating this kind of supportive environment. Each month, the company hands out an award called _Mistake of the Month_. New employees might find it intimidating, but the ritual has fostered a more open and lighthearted company culture. Since everyone knows that mistakes can happen all the time, there isn't as much individual and collective pressure regarding failure. This allows people to take risks and be innovative, without feeling embarrassed or inadequate when something doesn't work out.

> _"Giving is good business — in both senses of the word 'good.' It's good because its helps people, it's good because it makes money."_

### 9. When it comes to marketing a product, storytelling is far better than plain facts. 

Let's say your business is now up and running, and you've got a great product to sell. But what's the best way to market it?

It can be tempting to use lots of facts and statistics to communicate with your customer, but in fact, the best way to make your product appealing is to tell _a good story._ Vivid stories are more memorable and convincing than straight facts.

In fact, a study found that people were willing to pay more for a product after they heard an emotional story about it. Customers who heard plain facts and figures about the same product were only willing to put up half the price.

Why is this?

Good stories are effective because they do much more than sell a product. They also define the values of an organization and give it an identity. To really succeed at this, stories have to evoke emotion and establish a connection between the customer and the company.

In fact, the author's shoe enterprise is a great example of the power of storytelling. The author started out as an economist with no professional retail background and not much start-up capital — all he had was a dream. He wanted to donate one pair of shoes to an Argentine child for every pair sold through retail.

At one point, his simple idea attracted the attention of a reporter at a big Los Angeles newspaper, and the resulting press gave his business a huge boost. People rushed to buy a pair of _TOMS_ shoes, because they wanted to be part of the inspiring story.

And just a few years later, _TOMS_ is a massively successful enterprise that benefits both poor children and the company's employees.

Of course, finding a personal story that suits your business might be tricky. But it'll be worth it.

### 10. Final summary 

The key message in this book:

**Don't be scared to start a business, even if you don't have a lot of business experience or many financial resources. Just keep it simple, surround yourself with like-minded people and find creative ways to economize, and you** ** _can_** **create a successful company.**

Actionable advice:

**Name your fears.**

If you're really scared of something, write it down. Then write down the worst thing that could possibly happen if your fear were realized. Just by identifying the fear, you'll lessen its hold over you. And by figuring out what could happen if the fear came true, you'll probably realize that even the worst possible outcome wouldn't be the end of the world.

**Suggested** **further** **reading:** ** _Delivering Happiness_** **by Tony Hsieh**

_Delivering Happiness_ tells the story of Tony Hsieh and his company _Zappos_, demonstrating how thinking long-term and following your passions can not only lead to profits but also a happy life for your employees, your customers, and yourself. The central theme of the book is the business of literally delivering happiness while living a life of passion and purpose. The book describes an alternative approach to corporate culture that focuses on the simple concept of making people around you happy, and by doing so increasing your own happiness.
---

### Blake Mycoskie

Blake Mycoskie is the US entrepreneur who founded the wildly successful _TOMS_ Shoes, which donates one pair of shoes to a child in need for every pair it sells.

