---
id: 54f59ff6623735000a850000
slug: start-up-nation-en
published_date: 2015-03-05T00:00:00.000+00:00
author: Dan Senor and Saul Singer
title: Start-Up Nation
subtitle: The Story of Israel's Economic Miracle
main_color: 237FAD
text_color: 1E6D94
---

# Start-Up Nation

_The Story of Israel's Economic Miracle_

**Dan Senor and Saul Singer**

For a small country, Israel punches far above its weight as a global hub of innovation and tech entrepreneurship. _Start-Up Nation_ explores the country's history, geopolitics and culture to try and understand where this creative energy comes from, and offers stories of companies that exemplify the distinctive ways in which this drive is channelled.

---
### 1. What’s in it for me? Learn what drives a nation’s start-up spirit. 

Can you be born into entrepreneurship with the gift of an "innovative" nationality? According to Senor and Singer, being from Israel can give you a considerable head start.

These blinks show how Israel has become a global powerhouse in innovation and entrepreneurialism. Its culture and geopolitics make the country swirl with both creative and sustainable start-ups developing everything from electric cars to money transfer systems. So if you're curious to learn about the social and economic building blocks that provide the foundations for successful start-ups, Israel is the country to investigate.

In these blinks you'll discover

  * how an Israeli idea overcame the obstacles facing electric vehicles;

  * that an anti-hierarchical ethos drives the Israeli people; and

  * that the military creates highly successful business folks.

### 2. Israel has a high concentration of innovation and entrepreneurship – fertile grounds for creative start-ups. 

The media invariably offers a skewed portrayal of Israel, only reporting on its ongoing political conflicts with neighboring countries, conflict with the Palestinians, or the Iranian nuclear issue. But a major facet of Israel is often disregarded, and that is its economic and innovative achievements.

Many people are unaware that Israel represents one of the most densely concentrated centers of innovation and entrepreneurship in the world.

In fact, Israel is the word leader when it comes to the percentage of the economy spent on research and development. As a result, Israel also has the highest density of start-ups in the world, with a total of 3,850 — one for every 1,844 Israelis.

Today, it's difficult for technology companies to overlook Israel — and most haven't. Cisco, for example, has acquired nine Israeli start-ups and is on the hunt for more.

Israeli start-ups also reel in a large amount of venture capital, which is the most crucial measure of technological potential. In 2008, venture capital invested per capita was two and a half times larger in Israel than in the United States, more than 30 times greater than in Europe, and 80 times greater than in China.

Israeli companies also have a powerful presence on the American stock exchange NASDAQ, accounting for more than all European NASDAQ companies combined.

Israel's start-up scene is vibrantly creative. One example is BetterPlace, which was founded to promote electric cars. There are well-known disadvantages to electric vehicles, such as expensive batteries, a short driving range, and a lengthy recharge time. However, if car buyers don't need to purchase the batteries outright with the car, then electric cars can be as cheap as gasoline cars. So BetterPlace used this to their advantage by leasing car batteries and creating stations where the batteries could be swapped.

But why is the start-up scene flourishing so well in Israel and not elsewhere? Let's find out!

> _"The West needs innovation; Israel's got it."_

### 3. A culture of doubt and argument, assertiveness and informality shape the national ethos of Israel. 

One reason for Israel's entrepreneurial achievements can be found in its people's mentality. Israeli people live in, and are framed by, a culture of rebelliousness, informality and assertiveness.

In 2008, PayPal bought the Israeli start-up Fraud Sciences — a company offering solutions to online payment scams. Scott Thompson, president of PayPal at the time, stated that the demeanor of the Fraud Sciences employees during his first meeting made quite an impression on him. The employees were unintimidated and openly challenged Paypal's methods for detecting fraud.

Scott Thompson was experiencing what is known to Israelis as _chutzpah_ — a Hebrew word meaning "cheek" or "audacity." To Israelis, _chutzpah_ is the norm. Whether at home, in school, or in the army, they learn that assertiveness is welcome and expected.

Israeli people are also traditionally open to questions and foster an anti-hierarchical ideology.

Questioning is built into the Jewish faith thanks to centuries of Rabbinic debates concerning the interpretation of the Bible. These open-ended interpretations and counter-interpretations produced a culture of doubt and argument that spilled over into civilian life. As a result, anti-hierarchical mind-sets are present throughout Israeli society.

Take the Israeli military: the Israeli army is deliberately understaffed at senior ranks, because fewer senior officers lead to more individual initiative at lower levels.

In addition, once a year, thousands of men are enlisted into the military reserve forces. This curtails social hierarchy because in the reserves, taxi drivers can give orders to millionaires, and young adults can train their older relatives.

> Fact: According to the Organisation for Economic Co-operation and Development (OECD), 45 percent of Israelis are university educated, which is among the highest percentages in the world.

### 4. The Israeli military serves as an incubator for high-tech start-ups and prepares its cadets for business environments. 

Because every Israeli has to serve in the army for a minimum of two years, their military is an integral part of society. It's also where much of the country's creative energy comes from, so it acts as an incubator for high-tech start-ups.

In Israel, all 17-year-olds must report to military recruiting centers for screenings that include psychological testing and medical evaluation. Those who do particularly well in the screening are offered training in the military elite units — the highest of which is the Talpiot unit.

Talpiot cadets complete an accelerated university degree in math or physics while they are introduced to the technological requirements of all military branches. The aim of this is to mold them into leaders who can seek out cross-disciplinary solutions to military problems.

Although Talpiot training is designed to develop the military's technological elite, the acquisition and combination of leadership experience and technical knowledge is also applied when founding new companies, making cadets the ideal people to do exactly that.

But it's not just Talpiot graduates who enjoy this kind of training: every military unit provides training in innovative and adaptive problem solving, which is helpful in business environments.

Furthermore, the military is a space where young men and women work closely with people from different cultural, socioeconomic and religious backgrounds. Learning how to deal with a wide variety of people is something that the cadets can benefit from later when dealing with international business partners.

As an added bonus, many business connections can be formed during the long hours of operations and training.

So we can see how the Israeli military fosters an entrepreneurial culture, and it's no surprise that the graduates have become founders of some of Israel's most successful companies.

### 5. The collective kibbutzim communities and a new venture capital industry characterize two great leaps in the Israeli economy. 

Israel's economic history is marked by two significant leaps. The first came during 1948 to 1970, and the second is still happening today, having started in 1990. During these leaps, Israel was transformed from a less-developed country into a trailblazing hub of innovation.

The collective communities known as _kibbutzim_ were at the core of Israel's first great economic leap. Kibbutzim were created as agricultural settlements devoted to abolishing private property and forming an egalitarian community. They were both hyper-collective and hyper-democratic, and through them technological breakthroughs were made.

For example, in the Hatzerim kibbutz in the Negev Desert, the soil was found to be too salty and troublesome to cultivate adequately. To solve this dilemma, the members of the kibbutz came up with a way to flush the soil so that they could grow crops there. They were successful, and in 1965, the kibbutz started a business to manufacture irrigation systems. This was the beginning of what became Netafim, a global drip irrigation company.

During Israel's second great leap, a new venture capital industry emerged.

In 1993, the Israeli government began an initiative named _Yozma_ that offered tax incentives for foreign venture capital investments in Israel, and promised to double any investment with government funds. This created an eagerness in the US venture community to invest in Israeli start-ups, allowing the Israeli tech scene to participate in the tech boom of the 1990s.

During this time, Israel's information-technology revenues rocketed from $1.6 billion to $12.5 billion.

Eventually the Yozma program created a new venture capital industry which revamped the Israeli start-up scene. This was pivotal to Israel's second great economic leap.

> Fact: Today, Israeli companies are firmly integrated in the economies of China, India and Latin America.

### 6. Immigration has been a boon to the Israeli economy – as has the movement of Israelis abroad. 

Imagine being able to have anything you want for dinner, from Yemeni cuisine, to Russian specialities, to Mediterranean dishes. This is a reality in Israel, which is home to over 70 nationalities and cultures due to large influxes of immigrants.

Immigration has also boosted the Israeli economy in other ways.

Take, for example, the Russian Jewish immigrants. By the 1990s, large waves of Russian Jewish immigrants began to arrive after the fall of the Soviet Union. Russians with doctorates and engineering degrees flowed into Israel in huge numbers. Though finding jobs and building houses for the new arrivals was quite an undertaking for the government, the Russians had arrived at an ideal time.

By the mid-1990s, the international tech boom was gaining momentum, and Israel's technology sector was eager for engineers. The Russian engineers were the right people for Israeli tech start-ups and contributed a great deal to their success.

An influx of new talent is a considerable benefit to the economy, but what about those leaving the country? Well, the movement of Israelis to and from the country also helps foster its economy.

For example, after earning an engineering degree at Ben-Gurion university in Beersheba, Israel, Michael Laor attained the position of director of engineering and architecture at Cisco in California. In 1997, 11 years later, he decided to return to Israel.

In order to keep hold of such a talented employee, Cisco agreed that Laor should start a research and development center for the company in Israel. By 2008, Laor's center had 700 employees, and Cisco has spent at least $1.2 billion purchasing and investing in Israeli companies.

People like Michael Laor contradict the notion of the widely discussed Israeli "brain drain."

> Fact: Israel became the only nation in history to explicitly address the need for a liberal immigration policy in its founding documents: the Law of return guarantees that "every Jew has the right to come to this country."

### 7. Israel turned geopolitical disadvantages into economic advantages. 

Israel is surrounded by political adversaries on every border and is in a constant state of conflict. This may seem harmful to the Israeli economy, but these geopolitical circumstances have actually been advantageous in the long run.

Israel has managed to turn its political isolation into an economy built on knowledge and innovation.

Because of the political problems, Israelis weren't able to travel to any neighboring countries until recently. Living in such isolation, it was therefore natural for Israelis to look internationally and embrace fields in technology and telecommunications, which would make borders and distances essentially irrelevant.

In addition, Arab trade boycotts forced Israel to export to markets further abroad. As a result, Israeli companies spurned the production of large, readily manufactured goods with high shipping costs in favor of small technical components and software.

Israel's geopolitical confinement positioned the country perfectly for the worldwide turn toward knowledge- and innovation-based economies — a trend that is still vibrant today.

Moreover, Israel turned its enduring military threat into an advantage in the high-tech economy.

Initially, Israel opted to buy large weapon systems from other countries, as opposed to investing the massive resources required to produce them.

After 1948, Israel formed an alliance with France, which included the French supply of military equipment and fighter aircraft. But in 1967, France withdrew its military support, leaving Israel exposed and vulnerable considering its geopolitical situation and the continual risk of war with neighboring countries. However, this sparked Israel's drive for technological independence and, as a consequence, Israel started to develop its own military technology.

The impressive technological talent that was poured into military projects was later unleashed into the economy, where military engineering graduates entered the private technology sector and helped instigate the high-tech boom.

### 8. There are several threats to Israel’s economic success. 

The Israeli start-up scene may seem fairly established, but it began just over a decade ago. So what if Israel's economic success was simply a fluke resulting from random events? In order to ensure its ongoing strength, Israel's economy will likely have to overcome several challenges.

One of these challenges could well be that Israel's economy gets too dependent on venture capital.

A dwindling venture capital supply means less money for Israeli start-ups. This was already evident during the global financial crisis in 2008, which reduced the dollar amount available for venture capital investments. With insufficient financing, many Israeli start-ups were left with no choice but to shut down.

Israeli companies also lean too heavily on export markets.

Over half of Israel's GDP (gross domestic product) comes from exports from Europe, North America, and Asia. When those economies fall on hard times, this means fewer customers for Israeli start-ups. Due to the Arab trade boycott, Israel has no access to the majority of regional markets and the domestic market is simply not big enough to act as a substitute.

Yet probably the most worrying threat to Israel's economic growth is its minimal participation in the economy.

Israel's low workforce participation rate (just over 50 percent) is mainly attributed to two minority communities: _Haredim_, or strictly orthodox Jews, and Israeli Arabs.

84 percent of men in mainstream Israeli Jewish society between ages 25 and 64 are employed, and 75 percent of women in that group. Among Arab women and Haredi men, these percentages are almost reversed: 79 percent and 73 percent, respectively, are unemployed.

The primary reason behind this low participation is that these groups generally do not serve in the army. This means that they are less likely to learn and develop the entrepreneurial skills that the army offers, and they cannot cultivate the business networks that Jewish Israeli youth form while in the military.

### 9. Final summary 

The key message in this book:

**Israel is home to a high density of innovation and entrepreneurship and an impressive number of creative start-ups. The reasons behind Israel's remarkable economic success can be found in the country's history, culture and geopolitics.**

**Suggested** **further** **reading:** ** _Startup Rising_** **by Christopher M. Schroeder**

The Arab Spring uprisings left much of the Middle East politically unchanged, but they made a whole lot of noise. And yet there's a second, quieter revolution taking place in the same region — and it's all about the rise of tech and entrepreneurialism. Through stories of Middle Eastern entrepreneurs living in Beirut, Amman, Dubai, Istanbul and elsewhere, _Startup Rising_ shows an entire region reinventing itself as a center of economic opportunity.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Dan Senor and Saul Singer

Dan Senor is a writer, political advisor and a senior fellow for Middle East studies at the Council on Foreign Relations. He was chief spokesperson for the Coalition Provisional Authority in Iraq, has travelled extensively in the Middle East, and studied in Israel and at Harvard Business School. He frequently contributes to the _Wall Street Journal_ and has written for the _New York Times_ and _Time_.

Saul Singer is a columnist and former editorial page editor at the _Jerusalem Post_. He is author of _Confronting Jihad: Israel's Struggle and the World After 9/11._ Singer has also written for the _Wall Street Journal_ and the _Washington Post._

