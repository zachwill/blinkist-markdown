---
id: 570250f48fb8ff000700000e
slug: start-up-wealth-en
published_date: 2016-04-06T00:00:00.000+00:00
author: Josh Maher
title: Start-up Wealth
subtitle: How The Best Angel Investors Make Money in Start-ups
main_color: BB4425
text_color: BB4425
---

# Start-up Wealth

_How The Best Angel Investors Make Money in Start-ups_

**Josh Maher**

_Start-up Wealth_ (2015) is your guide to better investing. From momentum investors to value investors and alternative investors, the range of angel investment strategies is broad. These blinks will help you better determine which of today's start-ups are worth your time and cash.

---
### 1. What’s in it for me? Make smarter investments, like a top angel investor. 

Angel investing in recent years has become a common way for start-ups to get funding and jump-start a business. Although angel investors usually don't have the money that larger investors do, angel investments, however small, can make an entrepreneur's dreams come true. 

But how do you get an angel investor on the hook? A good place to start is knowing what sort of angel investor you need. And if you want to invest in start-up ideas yourself, you should consider which strategies will serve you best as you seek out the next big thing. 

There is no better place to start than by looking at the skills and experience of some of the most successful angel investors out there.

In these blinks, you'll learn

  * why passion and drive will take you far with a momentum investor;

  * how a start-up's first test market is often an investor's family and friends; and

  * why having a quick exit strategy is good for both investor and entrepreneur.

### 2. There are three types of angel investors; each has its reasons for investing. 

Before we examine what makes an angel investor successful, let's first define the category. Angel investors — the wealthy individuals who financially support start-up companies — come in three types: _momentum investors, value investors_ and _alternative investors_. 

_Momentum investors_ are characterized by a reliance on instinct and vision. In other words, their selection technique doesn't involve much in the way of specifics or hard data. Rather, if a momentum investor feels a connection with or is somehow engaged by a company, he will offer his support. 

Brad Feld, a managing director at Foundry Group, is a leading momentum investor and has a particular approach when evaluating his gut feeling about an entrepreneurial project, that we'll explore in depth in the next blink. 

_Value investors_ invest in companies that come to the table with solid financials. David Verrill, founder and managing director at Hub Angels Investment Group, is a renowned value investor who takes the time to investigate a company's revenue sheet before taking the plunge. He prefers to review company data from the last 12 to 24 months, which helps him determine whether a business idea will eventually take off. 

Finally, _alternative investors_ want to do more than make an investment; they want to send a message. Rather than investing in stocks or bonds, alternative investors focus efforts on a specific business area, such as health care, antiques or even wine, to create a significant impact. 

Catherine Mott, the founder of the BlueTree Capital Group and the BlueTree Allied Angels, is a good example of an alternative investor. Her organizations assist entrepreneurs in building solid businesses, with a focus on private equity investments, to ensure local start-ups receive the necessary financial support.

Let's now take a closer look at each angel investor category, starting with the momentum investor.

### 3. Momentum investors invest in ideas that excite them and entrepreneurs who engage them. 

Have you ever wondered why there aren't more people like Bill Gates? The answer is simple: not all business ideas have potential. This is a problem with which every budding investor has to wrestle.

So how do you decide which businesses are worth supporting? Angel investor Brad Feld, an established momentum investor, has a few pretty good ideas. 

Feld believes that any potential investment target must offer three things: a passionate entrepreneur, great initial product feedback and a long-term relationship. 

Any person who is passionate about his or her idea is an entrepreneur with potential in Feld's book. After all, how can a business leader get customers excited about a product if he isn't thrilled with it himself? As an investor, however, Feld believes it's crucial that the product excites him, too. 

But of course even the most exciting products can fail. So, before making an investment, Feld ensures that the product has been properly road-tested with the company's target audience. 

Let's say Feld is interested in a start-up selling all-natural sweets. He'd first share the product with his family and friends, to gauge its market potential. If feedback is positive, and Feld's family and friends feel that such a product is missing in the market, it's likely that Feld will decide to invest. 

But there's one more test that a start-up needs to pass before Feld is on board. Feld asks himself whether he'd consider investing in the company for the long term, even if he were only to play a small role within the company itself. 

Here, Feld doesn't place too much weight on the experience or past achievements of the start-up's entrepreneurs. What matters most is a feeling of connection between the business and the investor. Only when this connection is solid, and the business has successfully met his other two criteria, will Feld decide to invest.

### 4. Value investors crunch the numbers to determine whether a start-up is worthwhile. 

David Verrill is a _value investor_. He first worked in corporate fundraising at the Massachusetts Institute of Technology, and later joined other investors to found the Hub Angels Investment Group. 

This group focuses on local, capital-efficient start-ups; that is, companies that use relatively low capital expenditures to produce output that is, nonetheless, great. Hub Angels doesn't look for grand visions but instead considers whether a start-up's finances genuinely show growth potential. 

Because most Hub Angels founders have a background in the life sciences, the group invests largely in areas such as diagnostic and health-information technology, yet avoids pharmaceutical companies. 

Why? Well, according to Verrill's experience, such companies — often seeking funding to the tune of $20 million to $40 million — simply weren't profitable investments. 

Hub Angels is a relatively small investor group, and might struggle if faced with competing investors in the pharma field. So Hub Angels adapted its strategy to its scale and only invests in start-ups with lower capital requirements. 

Another feature that makes the Hub Angels strategy stand out among other angel investors is that the group doesn't just invest in a company once. 

Investors usually offer a single investment to a start-up and then are done. But Hub Angels discovered that by following investments attentively, the group could invest multiple times at certain moments that could boost returns. 

A case in point is the company Localytics, which enables apps to analyze a user's mobile-phone activity and generate targeted in-app advertisements. Hub Angels participated in two investment rounds for the company. Based on its initial success, Localytics has since raised $16 million from other venture capital companies. 

Like other successful start-ups, Localytics will soon reach a point where it no longer requires help from Hub Angels. At this point, the angel investor's job is done!

### 5. Both investor and founder can also benefit from short-term investment relationships. 

David Bangs became an angel investor in 2007, when he joined the Northwest Energy Angels investment group. A typical alternative investor, Bangs invests in clean technology and has clear ideas about what makes a good investment as well.

Bangs believes that, as a good investor, you must have a strategy that provides you and your money with a quick exit. But what if a company simply doesn't seem to be set up for such a contingency? There are alternatives. Bangs often suggests purchasing some of the company's shares, but only with the condition that company owners will repurchase your shares at an agreed price if you decide you want out. 

So if Bangs wanted to leave an ongoing arrangement to invest in another start-up, for example, his pre-agreed condition would allow him quick access to his money — which is much faster than having to find another buyer to take on his shares. 

This simple strategy is highly advantageous not only for investors but also for founders, although you might be wondering why any entrepreneur would agree to such a deal. 

Many companies appreciate the opportunity to repurchase a given percentage of shares, so long as this percentage is smaller than the company's margin. This arrangement offers the benefit of a greater ownership stake and the potential for a bigger exit, should company owners decide to sell. 

To illustrate this, let's imagine a start-up with a 40-percent margin. If the company allocated five percent of its returns to repurchase shares, it would have an effective margin of 35 percent. This, in turn, is an attractive result for shareholders!

### 6. Consider joining forces with other angel investors to help keep your feet on the ground. 

We now know how angel investors evaluate potential investment targets and decide to direct funds to an inspiring start-up company. An angel investor's strategies and principles allow him to make decisions effectively. 

But sometimes, it's not so easy to make the call alone. This is when you need other investors around you. 

Catherine Mott, an _alternative investor_, is part of an angel group. Here's why:

Let's imagine that you're excited by an entrepreneur's idea. You can't wait to help him enter the market, and are thrilled to have found such a great opportunity. But how great is the idea, really? It's likely that in your enthusiasm, you've overlooked a few key problems. 

This is where fellow angel investors come into play. They can offer you critical opinions that might bring you back down to earth, allowing you to make a more rational, informed decision. 

Catherine Mott pays particular attention to the management abilities of start-ups. She makes sure that start-up founders display due diligence; she wants to be sure they're genuinely ready for the market. 

As part of this process, Mott assesses the team leader's skills. How well does the person suit the role? Can the person take criticism? Does the person help the team work smoothly together? If these questions can be answered in the affirmative, then the team is ready for the market. 

But we're not quite done yet. Next, Mott examines the target market, in light of the business model or product. This process takes between four and six weeks, depending on the quality of the team. 

Although time-consuming, this phase is important, as it reduces the risk of failure. And the start-up gains too, as Mott is always there to ensure that the company's financial strategy is sound and sustainable.

### 7. Final summary 

The key message in this book:

**As an angel investor, you need to perform due diligence, testing your investment target thoroughly and getting a second opinion from fellow investors to make successful, lucrative decisions. While momentum investors rely on connection and intuition, value investors stick to the numbers when deciding which start-up will win the funding round. Alternative investors invest in a cause they believe in, creating a positive impact.**

Actionable advice:

**Even angels need a mentor.**

If you want to start investing, it's a good idea to find yourself an investment mentor. This experienced person can provide you with insider information and advice. Especially if you're not familiar with a particular investment area, your mentor can set you straight and potentially prevent you from investing capital into an idea or project that might not pan out. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _What Every Angel Investor Wants You to Know_** **by Brian S. Cohen and John Kador**

_What Every Angel Investor Wants You to Know_ (2013) offers start-up entrepreneurs tips on what to look for in an investor — and how to actually get an "angel" to invest. With solid preparation and a good understanding of what motivates an investor, any entrepreneur can secure financing for her next big idea.
---

### Josh Maher

Josh Maher is an investor and technology consultant. He is also the president of _Seattle Angel_, a non-profit organization focused on the capital market for start-ups in the Pacific Northwest.

