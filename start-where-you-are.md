---
id: 5a1e9e88b238e10007f56655
slug: start-where-you-are-en
published_date: 2017-11-30T00:00:00.000+00:00
author: Pema Chödrön
title: Start Where You Are
subtitle: A Guide to Compassionate Living
main_color: 3D4B88
text_color: 3D4B88
---

# Start Where You Are

_A Guide to Compassionate Living_

**Pema Chödrön**

_Start Where You Are_ (1994) is an enlightening guide to opening up your heart and mind and learning to feel happier in your skin. Discover the practices that bring calm and serenity to Buddhist monks and nuns, as well as the philosophy that puts people on the path to nirvana. This isn't advice about what incense and candles to buy; it shows you how to look deep within yourself to confront your demons and find strength in your weaknesses.

---
### 1. What’s in it for me? Discover the wonders of meditation. 

Has anyone ever told you that meditation changed their life? You might feel skeptical about this, but you really shouldn't be — meditation has been shown to increase daily happiness and calm the mind in stressful situations.

The shamatha-vipashyana school of Buddhist meditation is particularly useful to first-time meditators. It is among the simplest and most accessible schools and can bring increased tranquility into your life, helping you rein in your mind's darker side.

In these blinks, you'll learn:

  * how to practice shamatha-vipashyana meditation;

  * what _tonglen_ breathing is and how it can help both you and those you care about; and

  * why the Buddhist concept of emptiness (Nirvana) is compatible with quantum physics.

### 2. Meditation can help bring peace of mind by keeping you focused on the present. 

What is it about meditation that people find so useful?

Meditation is all about celebrating and living in the present moment — the now.

Even though all we ever have is the present moment, we do a pretty good job of avoiding it. We get bogged down in regrets about the past or worries about the future. But the more you focus your mind on the now and keep it off those hypothetical worries and regrets, the more content you will be.

This is what the simple yet powerful _shamatha-vipashyana_ meditation technique can help you achieve. The name comes from the Buddha, who said meditation should bring tranquility, or _shamatha_, and insight, or _vipashyana_.

There are two steps to practicing shamatha-vipashyana meditation, and the first is to focus on your breathing.

Start by sitting upright with your eyes open and your legs crossed. Take a few deep but gentle breaths. As you settle in, be aware of your breathing and your environment. Notice the sights and sounds around you. Is it bright or dark? Is it quiet or noisy outside? Notice these things but remember to keep your focus on your breath.

This simple practice will help you live in the now. And since this is your only reality, it should be the center of your awareness.

While doing this, keep your mind from wandering to your worries and regrets.

Everyone's mind wanders to happy or sad thoughts. You may drift off to contemplating your to-do list or an annoying event from yesterday, but all you need to do is recognize this as normal thinking and bring your attention gently back to your breath.

Don't punish yourself or try too hard to prevent these thoughts. You might find it helpful to calmly say "thinking" to yourself as a way to stop a wandering train of thought and return your attention to the gentle rise and fall of your breathing.

### 3. Maintain a joyful mind by not taking life so seriously and by looking for ways to shake things up. 

Throughout our lives, we struggle with our sense of obligation not only to others but also to our sense of self-worth and what we should or shouldn't be doing.

When you wake up in the morning, do you feel hope or fear? Are you excited about the possibilities, or tired of living up to expectations? You might be eager to set out and get things done, or already disappointed with yourself for having had too many pints the night before.

It's fair to say that most of us make a big deal out of our lives and often feel it'll be the end of the world if we don't do this or that. Just imagine how less stressful life could be without these added pressures.

Happily, meditation can help you get there.

Once you become familiar and comfortable with meditation, you can add it as part of your regular daily life. This way, you'll be helping yourself to see each day in a positive light and every moment as an opportunity to live in the now.

It also enables you to proactively choose to be joyful. Life is just a collection of little moments, and you can choose how you meet these moments: with a heavy burden on your shoulders or with joy in your heart.

One of the best ways to create joy is to change up your routines and always be on the lookout for something new.

Next time you're in the shower, why not belt out your favorite Broadway tune? Or, when you're making yourself dinner, why not make it breakfast and serve up a stack of pancakes? Or how about simply taking a new route to work and riding a bike instead of the subway?

It's all about seeing things with fresh eyes and using your time to find new experiences. Instead of greeting every moment with the same baggage, find opportunities for joy around every corner.

> _"You can splash cold water on your face, you can sing in the shower, you can go jogging — anything that's against your usual pattern. That's how things start to lighten up."_

### 4. Free yourself from judgments by embracing emptiness. 

If you're familiar with Buddhism, you probably know that the ultimate spiritual goal is called _nirvana_, which is also often translated as "emptiness."

To Western ears, "emptiness" has a very negative connotation. For example, we say the optimist sees the glass as half-full while the pessimist thinks it's half-empty. But from a psychological perspective, emptiness can be a great relief.

If we reach a state of emptiness, it means we're free of judgments and labels like good, bad, happy or sad. These disappear once you realize that the ultimate goal is emptiness.

According to the laws of quantum physics, reality is like a hologram: it looks very solid and real, but when you look closely, you realize there's nothing there. And like a hologram, the same image will look different from other perspectives.

Recognizing the freedom in emptiness can also help you control anger.

This is best expressed in a classic Zen story. A man is enjoying a day on his boat until he notices another boat getting close. As it approaches, he gets angrier and angrier at the disturbance until he's shaking his fist and yelling. But then, as the boat floats right up next to him, the man realizes it's empty and he's been shouting at nothing the whole time.

We make up our own reasons to be angry, and we constantly put obstacles in our own path. Most of what happens isn't intrinsically good or bad; it is us who ascribe these unnecessary labels.

Embracing emptiness can also be helpful in times of death.

The author had two friends she refers to as Jack and Jill who both died slowly but in very different ways.

Jill had a deep fear of the emptiness, so she fought against it desperately, spending each day crying and frightened. Her ego refused to accept emptiness.

Jack, on the other hand, embraced the emptiness and became increasingly cheerful as his body and mind slowly faded away. Since he knew the fundamental nature of reality is emptiness, he was not resistant to it and passed away happily.

### 5. Build strength and compassion by accepting pain and sorrow. 

Many who come to Buddhism for the first time think they should embrace emptiness by closing off their minds or by ignoring pain.

But such techniques are little more than tricks that can be achieved with a bit of meditation practice; it's a big mistake to think that they are the whole point. Buddhism is about celebrating both the highs _and_ lows of life. This happens by acknowledging and feeling both positive and negative emotions but also learning not to grow attached to them or to try to avoid them.

So don't hide from life's ups and downs. If life gives you manure, use it to fertilize your beautiful garden.

Think of the strong and enlightened Buddhist monks who've spent many years on the road to enlightenment. If one of these monks got some bad news, would he hide away and bottle up his emotions? Or would he give himself over to the experience?

The answer is to let yourself feel the pain. If you don't allow yourself to experience the bad with the good, you'll never grow and learn how to deal with these inevitable emotions. So if you hope to be resilient and wise, the only way is to experience it all.

Think of all the great compassionate people of history, like Mother Teresa and Jesus. These are people who never turned away from feeling the pain, poverty, sickness and death of the people with whom they worked.

They washed the feet of those with leprosy and stayed close to those suffering, and by doing so were able to recognize and appreciate the full spectrum of life, which was key to their wisdom and greatness.

Those who care the most are those who have felt suffering up-close. And those who run away from suffering will inevitably end up becoming less compassionate.

In the next blink, we'll see how actively seeking out the worst in yourself can help you to become a better person.

### 6. Strength comes from embracing your weaknesses and demons. 

Some self-help books tell you to start each morning by looking in the mirror and saying things like "Today I'm going to be a better person." Don't do this.

Instead, learn to be comfortable with yourself and become familiar with your strengths _and_ weaknesses.

Having a deep understanding of your weaknesses will make you stronger.

According to Buddhism, there are three weaknesses called _the three poisons_ : they are craving, aversion and ignorance. They reveal themselves when we cling to things we like, run away from things we hate and ignore everything else. They're at the root of our worst behaviors, such as jealousy, ignorance, hatred, apathy, violence and addiction.

But you shouldn't repress these feelings. Instead, allow them to arise and use them to better understand yourself. Be familiar with what makes you angry or sad and where these emotions come from so that you can accept yourself for who you are. The only way to do this is by getting close to your demons.

The tale of Milarepa is a classic Tibetan story about just this.

Milarepa meditated for years in a cave, until one day he went out to collect food and came back to find his cave full of demons. At first, Milarepa tried to lecture them on the teachings of Buddhism and compassion, but this didn't get them to leave. So instead, he sat down and just let them be, saying, "We may as well live together." Faced with this accepting attitude, all the demons scattered and disappeared except for one.

But Milarepa knew what to do with the stubborn demon. He walked over and jumped right into its jaws. With nothing left to do, the demon vanished in a puff of smoke and Milarepa was once more at peace.

The moral of the story is to accept your demons. Once you stop avoiding them, they will soon go away.

### 7. Strength also comes from breaking free of your fixed narrative and not being afraid of looking foolish. 

Everyone in the world has their own story. It's the story that explains everything from where we grew up to who we want to be. These storylines help us explain who we are.

But can you imagine how liberating it would be if we all let go of these stories? It would leave us free to be whatever we wanted to in the present moment, and we'd no longer have to worry about looking foolish or being embarrassed because of the expectations created by our defining stories.

Consider the story of Juan, a tough kid from a rough part of Los Angeles. If you had tried to talk to him a few years ago, the likely response would have been laden with profanities.

But then Juan went on a meditation retreat where he met Buddhist master Trungpa Rinpoche, and he learned an important lesson about real strength. Trungpa led a group of people in song, and Trungpa's voice was so out of key that Juan couldn't help but cry. It wasn't that the master's singing was sad, but rather Juan was overcome with emotion at how brave Trungpa was to sing with his squeaky voice.

It was the bravest thing Juan had seen, and from that moment on, Juan saw how many limits he was putting on himself by sticking to his storyline of being a tough kid from LA. Now he could be honest about his emotions and live in the moment. This allowed him to turn his life around. He got an education and is now helping kids in Los Angeles.

Storylines are also a way for us to lie to ourselves, even when it comes to meditation.

When you start meditating, you might feel the need to tell others how much you're benefiting from it, even if you're not.

Chödrön remembers a student who said meditation made him feel "blankness" and that he'd achieved clear-headedness. But then, the student later admitted that the accurate feeling was "numbness." He had merely wanted to exaggerate the benefit he was getting from meditation.

But Chödrön reminded the student that actually, there's nothing wrong with feeling numb. The goal is to be comfortable in your present state, and feeling numb is just as valid a feeling as anything else.

### 8. Buddhism can make you strong enough to help others, even your enemies. 

Hopefully, these blinks will bring you closer to accepting the darkness within yourself and others so that you can live truthfully.

By making meditation part of your daily life, you'll be moving toward a clear-mindedness and a greater awareness of those around you. Remember, being present means coming to terms with suffering, especially that of those who need help.

But it's best to stop thinking in the strict terms of helper and helpee.

If you see yourself as a saintly helper trying to care for others, and the people in need as being somehow below you, you're effectively erecting barriers between yourself and others. This added separation will only cause more pain.

So drop the helper storyline. There's a breathing exercise called _tonglen_ that can help awaken a different, more compassionate mind-set .

With tonglen breathing, you breathe in the undesirable — pain and suffering — and breathe out the desirable — strength and happiness.

If it sounds strange, it's probably due to the Western idea of only letting in good things and rejecting anything unpleasant. But this is an important part of Buddhist meditation, and by practicing it, you are sharing your goodness and happiness with the world while letting go of your selfish desires.

If you feel like a more advanced practice, you can also breathe in the pain of those you hate and send them your positive thoughts as you breathe out. With this practice, you're also opening your heart so you see things from their perspective, understanding their demons and how they may have suffered in the past.

As you'll surely discover when you dive deeper into your meditation practice, even when it comes to our enemies, we're more alike than you might think.

### 9. Final summary 

The key message in this book:

**You won't live a meaningful and joyful life by avoiding your weaknesses, ignoring your demons and creating barriers to pain and sorrow. A full life is one that accepts all emotions and takes the bad with the good. It's all part of being alive. Meditation can help you live with life's pain and suffering, and by doing so, you'll become a more powerful and compassionate person.**

Actionable advice:

**Don't cling to happy memories, pass them on.**

The next time a happy memory comes to mind, instead of lingering in it and living in the past, take a deep breath and pass that happiness on to someone who needs it more than you do. Perhaps even your worst enemy.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Twelve Steps to a Compassionate Life_** **by Karen Armstrong**

_Twelve Steps to a Compassionate Life_ is a step-by-step guide to bringing more compassion into the world. It shows you in concrete terms how you can cultivate compassion in your everyday life, and helps you to do your part in making the world a better place.
---

### Pema Chödrön

Pema Chödrön is a resident teacher of Buddhism at Gampo Abbey, America's oldest Tibetan monastery. In 1981, she became the first American to be made a _bhikkhunī_ or a fully ordained nun in the Vajrayana tradition. She has also authored several best-selling books, including _The Wisdom of No Escape_ and _When Things Fall Apart_.

