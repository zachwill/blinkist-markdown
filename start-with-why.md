---
id: 52bec76a3933330008000000
slug: start-with-why-en
published_date: 2014-02-04T09:00:00.000+00:00
author: Simon Sinek
title: Start with Why
subtitle: How Great Leaders Inspire Everyone to Take Action
main_color: E02D4E
text_color: C72845
---

# Start with Why

_How Great Leaders Inspire Everyone to Take Action_

**Simon Sinek**

_Start With Why_ (2011) tackles a fundamental question: What makes some organizations and people more innovative, influential, and profitable than others? Based on best-selling author Simon Sinek's hugely influential lecture of the same name, the third most-watched TED talk of all time, these blinks unpack the answer to that conundrum. As Sinek's examples from the business world, politics, and technology show, it's all about asking "Why?" rather than "What?"

---
### 1. What’s in it for me? Learn the art of leadership. 

What do Martin Luther King, Jr., Steve Jobs, and the Wright brothers have in common? On the face of it, not a lot. They worked on different projects for different reasons at different times. 

But there is one thing that unites the four of them: they all understood that no one buys into an idea, project, or product unless they understand _why_ it exists. 

This, as author Simon Sinek argues, is something you'll find in every great leader and successful organization. Think of it as a sense of purpose. People do their best work and inspire others when they know why they're doing what they're doing. This doesn't mean striving to make money. It means engaging in purposeful activity, an approach that often results in financial gain. 

But how do you lead in this way? In these blinks, we'll be examining a framework that shows how organizations can be designed for success and how people can be inspired.

Along the way, you'll learn

  * why manipulating customers undermines businesses' long-term viability; 

  * how Apple's message taps into the decision-making center of the brain; and 

  * why one of the world's most successful retailers lost its way.

### 2. Success is the fruit of design, not of short-term patches. 

There's a famous story about a group of American automobile manufacturers who visited Japan to inspect the country's car plants. The assembly lines they saw were pretty much the same as those in their own factories, but there was one notable difference. In the United States, a line worker used a rubber mallet to tap the edges of car doors to ensure they fit. In Japan, this job didn't seem to exist. 

When the American executives asked how these plants managed without this position, their Japanese guide smiled sheepishly and explained that "we make sure it fits when we design it." 

**The key message here is: Success is the fruit of design, not of short-term patches.**

Unlike their American counterparts, Japanese car makers weren't looking at a problem and attempting to figure out a makeshift solution — they were engineering the outcome they wanted from the get-go. 

This has a couple of obvious benefits. First off, a well-designed door is likely to both last longer and be more structurally sound in an accident. Secondly, if you've designed the door correctly, you don't need to buy mallets or hire workers to wield them. That eliminates a lot of waste and saves a lot of money, time, and hassle. 

But that's not how things work in many organizations. What the American carmakers were doing with their mallets is a metaphor for the way in which many companies around the world are led. Faced with a result that doesn't match up with their original plan, leaders often turn to perfectly effective short-term patches to achieve their goals. 

This might keep things chugging along, but it's not the best approach. The most successful organizations don't need mallets — they build products and companies according to a blueprint. Put differently, they make things fit by design, not by default. 

Every instruction leaders give, every course of action they put in motion, and every target they set begins with the same thing — a decision. Some decide to hammer doors into place; others start somewhere very different. 

In these blinks, we'll be exploring the second path. As we'll see, this is what guarantees long-term success. It all begins with a simple but powerful question: _Why_?

### 3. Manipulating consumers brings short-term benefits but undermines firms’ long-term viability. 

Between 1990 and 2007, the American automobile manufacturer GM saw its market share in the United States drop from 35 to 24 percent. Faced with competition from Asian companies like Toyota, GM tried to boost its sales by offering customers cash-back deals. It began selling more cars, but there was a catch: by 2008, it was losing $729 for every vehicle sold. This wasn't sustainable, and that's the key message here. 

**The key message here is: Manipulating consumers brings short-term benefits but undermines firms' long-term viability.**

Manipulation comes in different forms. In general, it's anything that pushes consumers to buy — think clearance sales, two-for-one deals, advertising hype, or appeals to authority in claims like "Four out of five dentists prefer Trident." It's a popular strategy — and in the short term, at least, it's pretty effective. 

Unfortunately for firms like GM, it rarely pays in the long run. Take one of the most common forms of manipulation — the "price game." Drop your prices low enough and folks will buy your products. This is catnip to sellers. There's a fantastic short-term gain, but it quickly becomes a habit that's hard to kick. The lower your prices, the more reluctant your customers will be to pay more. The result: slimmer margins that can only be offset by more sales, which in turn require even lower prices. 

Even companies that maintain profitability while sinking their prices tend to suffer. Walmart, for example, has a pretty healthy bottom line. Its reputation, however, is in tatters. The only way of making money while offering consumers constant bargains is to cut costs elsewhere. Today, Walmart is notorious for its shoddy treatment of underpaid and overworked employees in its in US stores. 

Worse, manipulation might be able to drive transactions — individual sales — but it doesn't breed _loyalty_. You can see why when you think of how rewards work. When you lose your kitten and offer a reward to whoever returns it to you, for example, you're not trying to build a relationship with the finder — you just want your cat back. 

That's not a sustainable basis for a business. When GM dropped its financially ruinous policy of offering cash-back deals, customers abandoned the company and started buying cheaper Asian vehicles. The only factor that had tipped the scales in GM's favor was a transactional reward. Once that was removed, there was nothing to fall back on. 

So what's the alternative? Let's find out!

### 4. Companies like Apple don’t just sell products – they affirm their customers’ beliefs and values. 

When you get down to it, Apple is just another computer company. Like Dell, HP, or Toshiba, Apple has some systems that work well and some that don't. All four firms have equal access to resources, talent, and media channels to publicize their wares. Rationally speaking, it doesn't matter which company's product you choose — they're all pretty decent. 

But that's not how it works. In the real world, folks pay more for Apple devices and stand in line for hours for the latest iPhone. Why is that? 

**The key message here is: Companies like Apple don't just sell products — they affirm their customers' beliefs and values.**

Here's one way Apple _is_ different. Unlike its rivals, it isn't associated with a single product category. This is unusual. Typically, you would expect customers to buy their computers from one company, their cell phones from another, and their mp3 players from a third. Apple has been hugely successful in all three market categories. 

There's a good reason for this: the _why_ matters more than the _what_. Let's break that down. 

Most organizations have a straightforward pitch. They describe what they do, state why they're better than their competitors, and wrap things up with a call to action. If Apple were like most other companies, its pitch would be simple: "We make great computers, they're beautifully designed and user-friendly — wanna buy one?"

But that's not what Apple says. Here's its actual message — the pitch that has made it one of the most successful companies of all time: 

"Everything we do is about challenging the status quo. We believe in thinking differently, and we prove this by making beautiful, user-friendly products. Oh, and we also happen to make great computers. Wanna buy one?" 

Notice the difference? There's no manipulation or free stuff. In fact, the emphasis isn't really on products at all. Apple isn't telling us what it makes — it's telling us _why_ it does what it does. 

This is a great example of _starting with why_. Apple isn't arguing that we should buy a Mac or an iPhone because they're better and cheaper than rival devices, or because they've been endorsed by celebrities. It's telling us that if we also believe in creativity and thinking outside the box, Apple is the right company for us. Put differently, it "gets us" — it sees us as people with beliefs and values rather than as mere consumers. 

So why is this kind of message so persuasive? To answer that, we need to take a look at human biology.

> _"When the_ why _is absent, imbalance is produced and manipulations thrive."_

### 5. Our rational brain doesn’t control our decisions. 

Here's a tricky question: Why do you love your partner? Often, we'll say something like, "Well, I don't know — she's funny and smart." But there are millions, even billions of funny and smart people out there with whom we don't want to share our lives. Or we might say, "He completes me." But how on Earth do you look for someone who does _that_? 

These aren't the real reasons we fall in love — they're attempts to describe something that's virtually indescribable. There's a reason for that. 

**The key message here is: Our rational brain doesn't control our decisions.**

If you look at a cross-section of the human brain, you'll see three areas. On the outside is the most recently evolved part, the neocortex. This is responsible for rational thought and language. The two middle sections make up the limbic brain. These are responsible for feelings like loyalty and trust, as well as decision-making. Crucially, these older areas lack the capacity for language. That's why it's so hard to describe why you love someone. 

It also explains why companies fail to connect with customers. Say you're trying to sell a TV. You explain its price, specs, and features. All this data is a great way of engaging the neocortex, but here's the rub: People can process vast amounts of complex information, but information isn't what drives behavior. 

If you want to change behavior, you need to access the limbic brain. Take an example from the laundry-detergent industry. 

For years, American companies made commercials claiming that their detergents made people's whites whiter and their brights brighter. On the face of it, this was a solid value proposition — that, after all, was what market research had shown consumers wanted from their detergent. Because researchers had asked consumers _what_ they wanted, they spent all their time talking about _how_ their product worked. Some claimed protein was the miracle ingredient; others pointed to their patented color enhancers. 

But that's not _why_ we clean our clothes. We assume that all detergents get our clothes clean — that's just what they do. As an anthropological study later found, when we take our clothes out of the machine, we don't hold them up to the light to inspect how white or bright they are. We smell them. _Feeling_ clean matters more than objective cleanliness. 

The whole detergent industry was acting on a false assumption. That just goes to show why it's worth starting with _why_!

### 6. Innovations spread far and wide when they’re championed by a minority of true believers. 

Can you imagine spending $40,000 on a new type of TV that could be the next big thing but might just be a flop? No? You're not alone. Only a small minority embraces new products and ideas, but this sliver of the consumer population is vital to the success of companies and organizations looking to reach a mass audience. 

**The key message here is: Innovations spread far and wide when they're championed by a minority of true believers.**

This is called the _law of diffusion_, a concept that goes back to a 1962 book by communications theorist Everett M. Rogers. His theory states that every population can be divided into five segments, each of which responds differently to innovation. 

_Innovators_, who make up 2.5 percent of the population, are the first to embrace novelty. The next 13.5 percent are _early adopters_. Then come the more risk-averse _early majority_ and the _late majority_. Together, they account for 68 percent of the population. The final 16 percent are _laggards_ — folks who only buy touch-tone phones because rotary phones don't exist anymore. 

Innovators and early adopters are happy to pay a premium or suffer inconveniences to own a product or espouse an idea because it feels right. This has little to do with the true merits of that product or idea and a lot to do with their own sense of themselves. Most people don't make these kinds of intuitive decisions, however. They don't want a phone or TV that aligns with their values; they want something that works and doesn't cost too much. 

As Rogers pointed out, the irony of mass-market success is that it's impossible to achieve if you're trying to convince the practical-minded majority. Why is this? Simple: the majority won't try something until someone else has recommended it to them. If you want to reach the middle of society, you need a minority of loyalists to get the word out — something they'll only do if they believe in your _why_. 

This isn't true only in business. In August 1963, 250,000 people stood in the scorching sun in Washington, DC, to hear Martin Luther King, Jr. talk. No invitations had been sent out and there wasn't a website to check, but word had spread. Dr. King wasn't the only man who had suffered in pre-civil rights America or the only great orator in the nation, but he never stopped talking about what he believed. His message had been taken up by a small following who had made it their own. By 1963, it had finally reached middle America.

### 7. Companies run into trouble when they lose sense of “why.” 

Imagine a small mid-century retail establishment in the United States. The company's founder came of age during the Great Depression and believes in hard work and fairness. Look after folks, he likes to say, and they'll take care of you. The business gradually grows, but it never loses sight of its founding ideals. Sure, prices are low, but that's not what makes the company so popular. What people really love is its holistic philosophy of giving back to employees, customers, and the community. 

You'll have heard of this company, but you might not have heard it described in this way. It's Walmart. So what went wrong? 

**The key message here is: Companies run into trouble when they lose sense of "why."**

Walmart's original values didn't outlive its founder, Sam Walton. After his death in 1992, the retail giant lost its way. Abandoning its earlier commitment to the communities in which it operated, it redefined its mission as selling more and more at the lowest possible price. 

This new orientation wasn't down to external competition — in fact, Walmart was in better shape than ever. Kmart, one of its biggest rivals, filed for bankruptcy protection in 2002, and Walton's stores were selling around six times as much as Target. All in all, Walmart was racking up about $400 billion in annual sales. 

This should have been a golden age for Walmart, but that's not how it felt inside the corporation. This is a common problem. Like the leaders of other large corporations, Walmart's leadership had lost its sense of purpose. 

By 2008, it was facing 73 new class-action lawsuits for violating its employees' rights and had already paid out millions of dollars to settle previous cases. Towns and cities that would have once welcomed the opening of new Walmart stores now fought tooth and nail to keep the company out of their communities. Neither labor unions nor political representatives had a good word to say about the retailer.

Walmart's fixation on increasing its turnover stemmed from an issue that plagues many American businesses: Even when they're doing well financially, they don't _feel_ successful. 

When the author attended the Gathering of the Titans, an annual meeting of America's 50 top business leaders in Boston, he witnessed something remarkable. Asked whether their companies had met their financial targets that year, some 80 percent of attendees raised their hands. But when asked if they felt successful, 80 percent of the hands went down. 

This is one of the risks of achieving so much. As success follows success, companies become more and more confident about _what_ they're doing and forget _why_ they're doing it.

### 8. Teams focused on “what” often fail, while those that start with “why” are capable of extraordinary achievements. 

Samuel Pierpont Langley had it all figured out: he was going to be the first man to fly. A well-connected senior officer at the Smithsonian Institute, he had friends in high places — people like Andrew Carnegie and Alexander Graham Bell. His contacts had helped him land a $50,000 research grant from the US War Department and assemble the brightest minds in the United States. Reporters from the _New York Times_ followed him around, and the public was rooting for him. Success was virtually guaranteed. 

But despite his hard work and numerous attempts, he just couldn't get his contraption off the ground. Here's why.

**The key message here is: Teams focused on "what" often fail, while those that start with "why" are capable of extraordinary achievements.**

On December 17, 1903, a small group of onlookers witnessed a man — _not_ Langley — take flight for the first time in human history. The "flying machine" he used had been built by two brothers, Wilbur and Orville Wright, and their team. No one involved held a college degree, and the project had been funded by proceeds from the Wrights' bicycle shop. How did they achieve what Langley couldn't? 

They started with _why_. As James Tobin notes in his biography of the Wrights, "Wilbur and Orville were true scientists" who cared deeply and genuinely about the problem they were trying to solve — the problem of flight and balance. More importantly, they knew that if they succeeded, it would change the world forever. 

Langley, by contrast, was obsessed with the _what_. He wanted the fame and prestige that came with a major scientific breakthrough and was indifferent to its application. When the Wrights beat him to it, he didn't attempt to improve on their flying machine — he simply gave up. 

Orville and Wilbur had another string to their bow: a dedicated team. By practicing what they preached, they inspired others to join them. Every day, this motley crew of dreamers and tinkerers returned to the field behind the Wrights' shop with five sets of parts. After five failures, they trudged in and ate dinner. The next day, they started again. 

Both Langley and the Wright brothers were highly motivated, and had a strong work ethic and keen scientific minds. Langley, however, had paid for talent to help him make his name. The Wrights' commitment to altering the course of history excited those around them and filled them with belief. 

And that's the lesson here. Hire people for _what_ they can do for you and they'll work for your money. Hire people who believe in your _why_, on the other hand, and they'll give you their blood, sweat, and tears.

> _"Average companies give their people something to work on . . . innovative organizations give their people something to work toward."_

### 9. Final summary 

The key message in these blinks is:

**Businesses, individuals, and movements of all kinds should always start with "why" — their reason for doing something. This "why" should be the basis for every decision its leaders make and every message they transmit. By doing so, they will attract loyal supporters and garner long-term success.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Find Your WHY_** **by Simon Sinek, David Mead and Peter Docker**

_Find Your WHY_ (2017) offers something that every person and business is looking for: a true purpose. The authors provide strategies and exercises that individuals and teams alike can use to discover their most powerful motivations, and their reasons for getting up in the morning and starting the workday. This is a useful guide if you're searching for the right job, trying to hire the right employees or hoping to gain a better understanding of yourself and the people you live and work with.
---

### Simon Sinek

Simon Sinek is a self-professed optimist determined to create a better and brighter future for humanity. An influential speaker and coach, Sinek has helped organizations around the world, like Microsoft, American Express, the United Nations, and the Pentagon, inspire their employees. He is also the author of _Leaders Eat Last_ and _Together is Better_.

