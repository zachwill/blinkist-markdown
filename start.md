---
id: 548625c86331620009d60000
slug: start-en
published_date: 2014-12-11T00:00:00.000+00:00
author: Jon Acuff
title: Start
subtitle: Punch Fear In the Face, Escape Average, Do Work That Matters
main_color: DA312C
text_color: BF2B26
---

# Start

_Punch Fear In the Face, Escape Average, Do Work That Matters_

**Jon Acuff**

Are you ready to escape the average and become awesome? Do you want to overcome your fears and follow your dreams? What does it mean to live a purposeful life and do work that matters? Ultimately, the most important step in achieving any great goal to _START_.

---
### 1. What’s in it for me? Learn how to stop letting fear control you and start living a meaningful life. 

We all want to escape the average and the humdrum.

If you're tired of being like everyone else and are ready to start being awesome, these blinks are the path that will get you there. Importantly, you'll learn how to get past the most difficult block to your amazing new life's journey: the very start.

Find out how to overcome the fear that holds you back from living your dreams and start your own personal journey to being awesome!

In the following blinks, you'll also learn:

  * why sometimes you have to be illogical to be awesome;

  * how to edit your life of elements that don't bring you joy; and

  * how to deal with haters who try to get in the way of your ultimate success.

### 2. Escape the average and become awesome by punching your fear in the face. 

Do you dream of escaping the average? Do you want to become awesome and exceptional, but don't know where to start?

Here's a tip: If you want to become awesome, you have to _punch fear in the face_.

Fear can alienate you from your dreams. It goes without saying that you shouldn't let it, but to beat fear, you have to understand how it works. Fear prevents you from chasing your dreams by convincing you that they're impractical or unattainable.

But don't buy into that kind of defeatist thinking. Instead, write down your fears and then exaggerate them ridiculously, to show yourself how silly those fears actually are.

For example, if you're afraid that starting a new project will cause you to lose your job and stay unemployed, write that down. And then let yourself go farther. Imagine you'll end up on the street with a scraggly beard, sleeping on top of garbage bags and talking to stray cats.

Does that seem likely? Probably not, which is why you need to write it down. That way you can see how illogical your fears really are, and thus prevent them from controlling you.

Luckily, there's one more effective way to deal with fear's mind games: _Just start doing whatever you're scared of._

You have no control over the eventual result, but that won't ever change. You'll never know how things will end up, but at least you can control the outset. If you want to do something, do it!

For example, consider the Segway. When it was first introduced, people thought it might spark a transportation revolution. And yet, since its launch the Segway hasn't been such a great success.

Still, the Segway people deserve respect. They risked failure and pursued their awesome dream of developing weird, two-wheeled electric transporters.

> _"If taking the first step on the road to awesome were easy, then everybody would already be on it."_

### 3. Be realistic about where you are now, but dream big to figure out where you want to go. 

If you want to be awesome, you have to get comfortable with tension. You have to be a realist and a dreamer, practical and impractical, logical and illogical — all at the same time.

To start your journey, you need to find out which parts of your life _feel_ _average_. Do this by examining key areas of your life — social, intellectual, professional — and writing down which ones you want to improve.

Here's where the tension comes in. Although you should dream big, it's also important to be realistic. List current debts, responsibilities (are you a husband, a father, a coach?) and assets (university degrees or social connections) that would affect your ability to pursue your dream.

For example, the author wanted to start an ad agency with a friend. They agreed to build a website for their first client, a church. After a few months of effort, the author faced some tough realities: He had no idea how to run a business. Also, he'd never built a website before, and neither he nor his partner had any programming skills. In the end, they decided to quit the project and refund the church.

What went wrong here? Well, the author had been wildly unrealistic.

Learn from his mistakes. Your current reality doesn't have to be a cage, but it _is_ a jumping-off point. If the author had been realistic about his lack of experience, maybe he would have started a smaller project and managed to complete it successfully.

Still, acknowledging your reality shouldn't prevent you from dreaming big! Write down every crazy dream you have — even if you've never played an instrument, but want to rock a major festival.

Next to your dreams, write down a list of first steps that could get you there. Then pick one and get going!

> _"Be realistic about your present circumstances. Be unrealistic about your future circumstances."_

### 4. Stop waiting around to find your purpose in life and start living with purpose instead. 

What is it with people needing to find their purpose? Most people use finding their purpose as a smokescreen to avoid doing anything.

Don't be like them: Forget about finding a purpose. Think about all the pressure you put on yourself when you say that you want to find your one and only, true reason for being.

That much pressure is going to prevent you from doing anything, much less what you were meant to do.

This is especially important advice if you're young. Many teenagers and twenty-somethings freak out because they haven't figured out a life path. They shouldn't worry.

Scientists believe that our brains continue developing well into our twenties, so how is a not-yet-fully-formed teenager or a fresh college graduate supposed to figure out a purpose to guide the rest of his or her life?

Besides, waiting around until you find your purpose later will only prevent you from living with purpose today.

So stop waiting for some big epiphany. Wherever you happen to be now, start living with purpose.

When you go to work, for instance, do it with purpose: Write those thank-you notes for employees and colleagues by hand _today_.

Love your partner with purpose: Buy tickets for a show you'll both enjoy, and make dinner reservations for your favorite restaurant beforehand.

You can even vacation with purpose by only checking your email once a day. Do you really need to be available for emergencies around the clock? Instead of living for someone else, take your vacation into your own hands!

So, now that you're ready to start being awesome, it's time to get familiar with the five stages of success.

> _"The opportunity and speed with which you can reach awesome has never been greater."_

### 5. The first stage of success is all about learning! Try many new things to gain experience. 

How do you get to the magical land of awesomeness? There are five stages, and the first one is all about _learning_ — that is, trying many different things and gaining experience.

After all, how can you know what you want to do if you haven't tried a bunch of different things?

Think about it like this: When a scientific experiment fails, the scientist isn't deemed a failure. We understand that even if the experiment didn't work, the scientist still learned something valuable.

This gradual learning process is what allows people to become awesome. After all, no one is born an expert.

Consider Tiger Woods: He won the Masters when he was only 21 years old. That might seem young, but remember, he had been practicing for 18 years to get to that point!

So, to start the learning process, ask yourself this question: If you died tomorrow, what would you most regret having never done? Are you doing that thing now? Why not?

And then just start doing it! But start small. For example, if you want to learn Chinese, find 30 minutes a week to sit down and get a feel for the basics.

Studies have shown that taking these kinds of small steps will prevent you from getting exhausted and giving up.

Take Roy Baumeister's famous 1998 experiment. Participants were divided into two groups — one group had permission to eat delicious cookies and the other was only allowed radishes.

Then both groups were given a tricky geometry exercise. The group that had eaten cookies made a concerted effort to solve the puzzle; meanwhile, the group that was only allowed radishes quickly gave up, because they had used up all their willpower denying themselves cookies.

The point is, your willpower is finite. So plan to tackle the most difficult tasks in the morning and not late at night, when your willpower is mostly depleted by a day's worth of self-control.

### 6. Edit your life of things that don’t give you joy on your path to the magical land of awesome. 

Now that you've mastered the art of learning, you have to focus on something. This second stage of success is all about _editing_ your life.

In the previous blink, we saw that learning is all about trying many different things. In fact, learning is a way of gaining possibilities and options.

Unfortunately, you can't be an expert in everything. So start editing: Look at all the different things you've learned, and then choose one you want to develop.

For example, if you have a passion both for writing and for giving speeches, you have to decide which skill you want to spend more time developing. The other one you can pursue as a hobby, or come back to it later in your life. In fact, make your own "later list" with possible future paths.

But in the meantime, ask yourself: What gives you the most joy?

It's important to phrase the question exactly that way. Normally, we ask different kinds of questions — we ask _result questions_ : What will earn us more money? Which industries are growing? Where does my experience lie?

And although these are important questions, asking them isn't the right way to start. Just because certain jobs will allow you to earn money doesn't mean they'll deliver you to the magical land of awesomeness.

That's because being awesome means doing something that inspires you. And once you've figured out what does, you'll have plenty of jobs from which to choose.

For example, let's say that you've edited your life and realized that you absolutely love sharing ideas. You can apply this in so many ways: You could become a blogger, an author, a podcast host, a counselor or a copywriter.

Whatever it is that you want to learn, just start working on it. And if halfway through, you realize that it's not your way to awesome, simply edit it and move on to the next thing!

> _"Learning is about addition. Editing is about subtraction."_

### 7. To master your passion, build your expertise and ignore the haters in the way. 

Now that you've figured out what you want to focus on, it's time to take the next step, to mastery.

So what is the best approach for becoming an expert?

The trick is to start simple. _Volunteer_ somewhere; that is, trade your time for expertise. You'll need to do this in order to get better at whatever you're passionate about.

For example, if you want to become an awesome blogger, start by checking out and commenting on a lot of different blogs. Eventually, you can offer to write guests posts for free. This will allow you to improve your blogging skills while simultaneously connecting with the blogging community.

The first step? Create a list of people who are already doing what you'd like to do. How did they get where they are? What schools did they go to? What books have they read? What can you do to follow in their footsteps?

Next, prepare yourself for the inevitable feedback you'll get as you pursue your passion.

This is crucial, because no matter who you are or what you're working on, you'll encounter _haters_. Thus, you'll need to understand the difference between sheer hatred and constructive criticism.

Constructive criticism is there to help you improve; hatred, on the other hand, is just meant to hurt you.

Once you've identified hatred, you can deal with it by asking yourself two questions:

_Where did the hurtful comment come from?_ If it's from a stranger, especially online, let it go. You probably wouldn't tolerate it if a random person yelled at you in real life; why would you listen to a stranger online?

_Why did they say it?_ Just thinking about the other person's perspective might allow you to feel better. Sometimes haters simply want attention; other times, they're lashing out because they're struggling with something in their own life.

### 8. When the finish line is in sight, it’s crucial to stay focused and not lose momentum. 

The fourth stage is all about _harvesting_ all the great things you've been working on.

This is actually a tricky time, because the closer you get to something, the more you realize how much is still left to do.

Deal with any loss of motivation by creating small finish lines for yourself. That way you'll reach a goal each week and keep the momentum going.

For example, writing a book can take months. The best way to tackle such a big project is to divide the work into chapters, each with its own deadline. Alternately, you can set a goal with the number of words you want to write each day or each week. When you meet these smaller goals, you'll stay motivated to keep going.

It's also important to keep your focus. Once you start reaching big goals, people around you might tell you to start aiming for more.

This can be an enticing message. For example, if you've started a business that's not earning a profit, "more" could mean making your venture profitable.

But be skeptical. Whenever someone encourages you to pursue "more," ask them why you should.

Think of the entrepreneur who sacrifices time with his family to chase ever-bigger profits. Is he really so awesome? Figure out what matters to you, and stick to it.

To stay on track, think about what you did to get to this stage and figure out what's no longer necessary for your path.

For example, if you've started a blog, you probably spent a lot of energy attracting followers at first. Maybe you spent lots of time commenting on other people's blogs to draw people to your site.

Well, now that your blog is popular in its own right, you don't have to spend so much time commenting on other people's sites. Instead, dedicate yourself to delivering what your readers want.

### 9. The fifth and final stage is guidance. Share the love and help people on their own way to awesome. 

Let's say you made it to the end of your awesomeness journey. What's next? Sharing the love! 

Now that you've found something you love, you can guide others to follow their own version of the path. As it turns out, helping other people is incredibly satisfying.

How does this process work? Well, guiding others starts with a conversation. When you're helping someone, it's not about you, so ask questions! The only way to give good guidance is to be a good listener, so practice asking questions and listening to what people say.

To ensure you actually stick to the guiding path, connect it to something that's already important in your life.

For example, the author and his friend sought mutual guidance from each other. They tried to meet for breakfast regularly, but they were both too busy, and only managed to see each other sporadically.

Then they realized they both like to work out, so they decided to start going to the gym together. Now they meet, chat and guide each other three times a week, while they're working out.

Guiding is great, but don't get stuck doing only that. Always look for aspects of your own life that you want to develop, and then go all the way back to stage one — to the learning stage — and start the journey to awesomeness all over again.

For example, let's say you improved your career prospects and training, and finally landed a fantastic new job. Congrats! But don't let your success make you lazy.

Remember the "later list" you made during the editing stage? Pick something you haven't awesomified yet and get to it. Now's the time to start!

### 10. Final summary 

The key message in this book:

**You can stop being average and start being awesome, but first you have to know where to start. It's all about overcoming your fear, living with purpose and following the five stages of success: learning new skills, editing to find your passion, mastering an expertise, harvesting the results and then guiding others.**

Actionable advice:

**Write down all of your biggest, wildest, craziest dreams!**

Don't hold back: If you've always wanted to be a pop star, a race car driver, an astronaut, write it down! Then, once you've made your list, go back and write down the first step you could take that would allow you to accomplish each goal. Then start!

**Suggested** **further** **reading:** ** _The 4-Hour Workweek_** **by Tim Ferriss**

_The 4-Hour Workweek_ advocates the idea of the New Rich. These are the people who abandon their jobs as modern desk slaves and instead live a life that is all about enjoying the moment while still achieving big goals.
---

### Jon Acuff

Jon Acuff is a _New York Times_ bestselling author who writes about how people can overcome their fears and follow their dreams.

