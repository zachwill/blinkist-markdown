---
id: 56d41614bb82ae000700001d
slug: startup-growth-engines-en
published_date: 2016-02-29T00:00:00.000+00:00
author: Sean Ellis, Morgan Brown
title: Startup Growth Engines
subtitle: Case Studies of How Today's Most Successful Startups Unlock Extraordinary Growth
main_color: EB2F45
text_color: D12A3E
---

# Startup Growth Engines

_Case Studies of How Today's Most Successful Startups Unlock Extraordinary Growth_

**Sean Ellis, Morgan Brown**

We've all heard success stories of start-ups attracting millions of users and earning billions of dollars virtually overnight. _Startup Growth Engines_ (2014) shows us what all these companies have in common: a new approach called "growth hacking". These blinks reveal how your business can use viral marketing techniques, freemium business models and other growth engines to rapidly achieve business success.

---
### 1. What’s in it for me? Learn the secrets behind Silicon Valley's greatest success stories. 

It is the dream of every entrepreneur to attain the rapid growth experienced by the likes of Uber and Facebook — to take his fledgling business into the stratosphere in next to no time at all. 

Happily, it doesn't have to remain a mere dream. The hugely successful start-ups of Silicon Valley got where they are today through clever marketing and positioning techniques that any entrepreneur can employ.

But what are these techniques? Well, in these blinks, you'll discover a few of the growth hacks that have greatly benefited companies like Github, Yelp and many more.

In these blinks, you'll also discover

  * how you can make money out of giving stuff away for free;

  * how Uber took advantage of Chicago's awful weather; and

  * why no one needs to pay for ads ever again.

### 2. Growth hacking is a new type of marketing inspired by the dazzling successes of Silicon Valley start-ups. 

Chances are you've heard of Silicon Valley as being home to many innovative and successful start-up companies. But you might be impressed to learn just how successful these companies are. 

The figures are truly astonishing: in a very short period of time these start-ups have generated billions of dollars in revenue and attracted hundreds of millions of customers. 

Take Uber, for example. The transportation service officially launched in 2011 and in less than five years has achieved worldwide fame; as of 2015, the company is valued at $62 billion. What's remarkable is how fast the growth was in comparison to the traditional economy.

Start-ups achieve this kind of success by largely avoiding traditional growth techniques.

For instance, Uber didn't rely on typical marketing tools like costly paid advertisements. Instead, they targeted their clients using data analytics and relied on viral and word-of-mouth marketing to expand their customer base. 

This new way of doing business became known as _growth hacking_.

This approach to marketing is focused on quickly maximizing growth and isn't limited to one specific sector. Growth hacking can work for mobile applications, business-to-business services or marketplaces and social networks alike.

And there isn't one simple formula for successful growth hacking, either. It involves a wide range of factors that are combined to achieve excellent performance through attracting, retaining and generating revenue through new customers.

To achieve this, growth hackers lead teams of experts in multiple fields who are just as familiar with scientific thinking as they are with data, statistics and creativity.

In the following blinks, we'll look at some of the best growth hacks and how they were put to use by some of today's most successful start-ups.

### 3. Solving a problem is the best way to guarantee success. 

You may have heard this before, but it is worth repeating: the best ideas for a successful new business come from identifying a problem and offering a solution. But how do you find the right problem to solve?

The best kind of problem is one that affects a broad group of people.

Square is an example of a company that found success by providing a solution to a problem many people were finding difficult to overcome. Jack Dorsey, co-founder of Twitter, started Square in 2009 after observing his friend's inability to conduct a payment transaction when a customer wanted to use a credit card to purchase a glass faucet.

Dorsey knew many small businesses were experiencing the same problem. Even though many potential customers use credit cards, not all businesses can afford the equipment necessary to allow these kinds of payments.

Square solved this problem by reducing these costs and offering affordable hardware and software to make credit card payments more accessible to small business. 

This shows that you don't have to look far to find a problem needing a solution. Observe your surroundings, focus on what interests you and pay attention to the problems people run into. An unsolved problem can be just around the corner.

The crowdsourced review site Yelp is an example of a solution to a local problem that became a worldwide success.

Before Yelp, local small businesses had the problem of being unable to compete with the kind of marketing budgets of big chain stores and restaurants. They had to rely primarily on word-of-mouth marketing from satisfied customers.

Yelp recognized this issue and created a platform based on customer recommendations. It disrupted the old model and offered equal exposure, offering any business a chance to enjoy the benefits of a good reputation.

### 4. Providing a valuable service can turn your idea into a must-have that changes people’s behaviors and lives. 

Have you ever come across a product or service that ended up becoming an essential part of your life? Some start-ups like WhatsApp and Facebook have managed to create these kinds of must-have products that find their way into customers' daily routines. So what's their secret?

These innovations come from keen observation and an understanding of customer needs. This kind of analysis is exactly how Uber targeted their customers in the transportation market. The company found opportunities by looking at situations when people find driving and getting around most problematic: during holidays, sporting events, bad weather or when you're out on the town.

After starting out in San Francisco, Uber had this valuable knowledge in mind when they moved to Chicago. Since the city is known for its troublesome weather, great nightlife and many loyal sports fans, it was the perfect match for the company. Nowadays, Uber is such a _must-have_ application that some users actually question the need to own a car at all.

Try starting the search for your must-have idea by looking at your own life and asking what solution you could use to improve your situation. If you find a problem this way, you have the added benefit of being able to test out your service yourself and knowing whether it will meet customer expectations.

This is how the web platform GitHub was born in 2008.

It started when computer programmers realized they needed a better way to manage their code. Sharing code on open-source projects was problematic and inefficient, requiring downloading, revising and uploading code back and forth.

GitHub offered a solution that disrupted the entire process and allowed programmers to share code online, removing the daily struggle and creating what would become a $2 billion company in only a few years' time.

### 5. Starting local and expanding later is a sure-fire way to get enough initial traction. 

Once you find your must-have product or service, it might be tempting to launch it on a global scale. But many start-ups have seen this approach backfire. By going too big too soon, you run the risk of spreading yourself too thin and ending up a small player stretched across several markets.

To increase your visibility and gain momentum, it's better to start big in a local niche market.

Many successful Silicon Valley start-ups followed this advice by taking advantage of the local San Francisco Bay Area market to generate momentum.

This is where Uber first launched its services, focusing intently on one city before rolling out to other locations at a rate of one per month. Uber knew the tech community around San Francisco would be receptive to their services; the company took advantage of this by sponsoring tech and venture events and even offering free rides to attendees!

Uber used this initial customer base to collect vital information on the industry and market. With this knowledge in hand, the company could then successfully adapt its approach when it came time to expand.

By focusing your efforts to satisfy the expectations of one specific audience, you can generate the kind of positive word-of-mouth exposure that will enable you to grow.

This is also what Yelp did in its first year in San Francisco. By focusing on one city, they proved they could gather an abundance of reviews and create an entire entertainment guide for the area. With their reputation in San Francisco cemented, they could then move on to the next city.

### 6. A freemium business model is a good way to get customers to try your service, but it’s not without risks. 

Have you ever been scrolling through mobile applications and decided to try out a free version before committing to a purchase? You're certainly not alone. Many companies know the benefits of offering a non-cost service, or a _freemium model_, to eventually generate revenue.

A freemium model gives customers a chance to use a basic free service with the option to unlock an upgraded, premium version for a certain price.

The note-taking app Evernote used this business model to create millions in revenue. Evernote users can access the application for free but have to pay a subscription fee for additional features like storage space, offline access and syncing across multiple devices. 

Evernote could confidently use this strategy because they observed that the more time users spent with the application, the more engaged they became with it. 

The key to the freemium model is offering an upgrade with a clear increase in value to motivate customers to buy it.

Otherwise, you run the biggest risk that comes with the freemium model, which is giving customers no clear reason to upgrade.

The creators of GitHub faced a similar challenge when they launched their platform as a free service with unlimited public storage. With this model they ran into two problems: first, all the user content was accessible to anyone who joined, which some major companies didn't appreciate; and second, their server bills were quickly adding up.

Luckily, a solution presented itself. GitHub decided to offer private storage that more protective companies could pay to use, while keeping the public repository free of charge.

### 7. Offering free content or tools is also a good way to reach wider audiences. 

While some companies have found success with the freemium model, not all free offers have to involve a basic version of a paid product or service. 

Offering free content or tools to the public is another proven way to generate business. Not to be confused with a free demo version of a product, this refers to a free stand-alone offer that aims to help a potential client. 

But, like the freemium model, the ultimate goal is for the free content to draw in paying customers.

This is the strategy that the content marketing service HubSpot successfully employed when it started in 2006. The company offers ways to improve a customer's commercial visibility through tools and services like social platform marketing, content management and search engine optimization.

HubSpot came up with a clever way to market itself and let clients know that its services were needed. It created a stand-alone tool called Marketing Grader where people could add a URL and see the website's performance grade. If the site performed poorly, what better way to fix it than by using Hubspot's services!

Free tools like this usually end up being good for both your business and your customers.

This method also lets you provide free assistance to potential customers while you gather valuable information, which you can then put to use when they show interest in paying for your services later on. 

This is exactly what HubSpot did, and it allowed them to dramatically grow their customer base while keeping their sales force to a minimum. By putting a free tool on the market, they were able to gather huge amounts of customer contacts while simultaneously enticing customers to pay for their services.

### 8. Attain improved exposure by achieving virality and going social. 

Now that we've learned how start-ups quickly maximize their growth, let's look at two ways to quickly maximize a company's exposure in the marketplace.

First, _viral marketing_ can be a key to success — even though it is difficult to control.

Managing virality is about creating the conditions that make it possible for your idea, product or content to quickly jump from one person to the next and go global overnight.

The good part about virality: it costs next to nothing. The bad part about virality: it usually happens unintentionally and without a clear reason.

One company that might have cracked the code of virality is Upworthy, a media website that specializes in viral content and the emotional, surprising videos that users like to share. A mere 20 months after Upworthy launched, it was attracting 88 million unique monthly visitors.

The articles that Upworthy uploads are all tested to find the virality "special sauce." An article might be given up to 25 different headlines to find out which one will generate the most clicks!

Another way for websites to improve exposure is through social platforms.

In the age of Facebook, it is clear that you can attract and grow your customer base by creating a sense of community.

This is one of the secrets to how Yelp succeeded: while other review sites featured anonymous users, Yelp created a healthy network of subscribed members, each with photos and full profiles. The active users had an identity and were part of a community, which made their opinions more trustworthy than those of an anonymous stranger.

GitHub also formed a similar network by creating the largest programmer community on the web. This has transformed GitHub into a social platform that even headhunters might turn to when looking for new hires!

### 9. Final summary 

The key message in this book:

**Traditional marketing strategies are no longer relevant in the new economy. A successful start-up needs to find new ways to compete by using** ** _growth hacks_** **. Top performing companies are using new and efficient methods to maximize growth, expertly target customers and put themselves on the fast track to success.**

Actionable advice:

**Make sure the freemium model is right for you.**

Before implementing the freemium model, ask yourself if your product or service requires a significant time to learn how to use it. If the answer is yes, customers are actually more likely to use your service if they pay for it. Research shows that a customer who pays for something will usually take the time to learn how to use it, because they see it as an investment. On the other hand, as a free product it might get discarded without being given a proper chance.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Startup-Playbook_** **by David S. Kidder**

_The Startup-Playbook_ (2012) gives you business-building tips straight from the founders of some of the world's biggest start-ups. By conducting interviews with the founders of companies like LinkedIn and Spanx, the author uncovers what you need to do to make it big.
---

### Sean Ellis, Morgan Brown

Sean Ellis is an entrepreneur, investor and business advisor with extensive expertise in start-ups. He is also a frequent contributor to _Entrepreneur Magazine_ and _The Wall Street Journal_.

