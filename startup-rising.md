---
id: 5485cce76331620009780000
slug: startup-rising-en
published_date: 2014-12-09T00:00:00.000+00:00
author: Christopher M. Schroeder
title: Startup Rising
subtitle: The Entrepreneurial Revolution Remaking the Middle East
main_color: E5D067
text_color: 665D2E
---

# Startup Rising

_The Entrepreneurial Revolution Remaking the Middle East_

**Christopher M. Schroeder**

The Arab Spring uprisings left much of the Middle East politically unchanged, but they made a whole lot of noise. And yet there's a second, quieter revolution taking place in the same region — and it's all about the rise of tech and entrepreneurialism. Through stories of Middle Eastern entrepreneurs living in Beirut, Amman, Dubai, Istanbul and elsewhere, _Startup Rising_ shows an entire region reinventing itself as a center of economic opportunity.

---
### 1. What’s in it for me? Understand the bubbling entrepreneurial revolution underway in the Middle East. 

Whether it's about a military offensive, a religious conflict, or American interests at risk, we hear about the Middle East almost every day in the news. But the media focuses on the Arab Spring's fallout, and rarely touches on the other revolution spreading across the area.

This revolution is not like the Arab Spring. It's bloodless, present all over the world, and having real impacts on every Arab country, every day. It's a technological, entrepreneurial revolution, and it's only just beginning.

These blinks will show you why Google, Intel, Paypal, and many other companies are flocking to the Middle East, trying to make as many partnerships and investments as they can. They'll explain why it's only a matter of time before the Middle East overtakes Silicon Valley as the center of the tech world.

After reading these blinks you'll have learned

  * why the violence we see on the news doesn't affect the economic revolution in the Middle East;

  * about a "marriage site" inspired by internet dating sites; and

  * why women are playing such an important role in this tech revolution.

### 2. Innovative entrepreneurship is on the rise in the Middle East. 

Today, innovation is happening all over the world, and economic success is appearing in even the most unlikely places.

Consider the fact that Nokia was created in Finland — a country mostly known for its wood products. Similarly, M-Pesa, a company that represents almost half of all mobile payments globally, was created in Kenya.

So why is so much innovation happening outside the bounds of Silicon Valley? Well, it's partly because of the rapid rise of cheap, easy access to software and information.

And today, that innovation is now also happening in the Middle East, with the emergence of a new generation of entrepreneurs. And major global private equity firms, investors and tech companies are taking notice. Google, Intel, Cisco, Yahoo!, LinkedIn and PayPal have all made significant investments in the Middle East, despite political instability. Google, for example, has recently opened an office in Cairo.

So what's behind the Middle East's entrepreneurial rise? Well, there are three main factors.

First, technology offers an irreversible level of transparency, connectivity and inexpensive access to capital and markets. The rising generation of entrepreneurs has never known a world before tech, and it's still a young market overall, with over 100 million people under the age of 15.

The second factor behind the Middle East's rise in innovation is that today's investors are comfortable with political risk. The impressive economic growth of the BRIC countries (Brazil, Russia, India, China) has shown the world that political instability needn't deter investment.

And there's one more factor supporting the Middle East's entrepreneurial rise: Although changing market dynamics and economic growth were set in motion before the uprisings, it was the uprisings themselves that persuaded younger generations that the world was ripe for change. What once seemed impossible now seems attainable, and they feel that they can make a difference in their countries and in their economies.

> _"The West may be missing an 'unobvious' opportunity of historic proportion." - Peter Thiel._

### 3. In the past, entrepreneurial success in the Middle East came down to cronyism, but today, that’s changing. 

Although business and entrepreneurship have always existed in the Middle East, in the past, it required something called _wasta_.

Wasta refers to cronyism; it's all about acting favorably toward someone without taking their qualifications into account. And in a wasta economy, who you know determines your business success.

For example, Mohammed al-Ajlouni, a Jordanian "fixer" for major US news organizations, once said that you needed the approval of the Egyptian prime minister's son to do any business in Egypt.

Well, today, that's no longer really the case, as Arabs are learning to embrace the promise of entrepreneurial success without wasta.

In the past, your friends wouldn't have hesitated to criticize your new business idea and your parents would have forbidden you to quit your "safe" job to launch your own company — but that's all changing.

And according to Samih Toukan, the creator of one of the first all-Arabic internet portals, Maktoob, the Middle East has always had ideas and creativity. But up until now, it's been missing a widespread cultural acceptance of tech entrepreneurship.

And yet, Maktoob itself has played a big role in changing that. In 2008, it was sold to Yahoo! for $175 million — more than any other technological company in the Arab world.

And remarkably, Toukan, the founder, didn't rely on wasta at all while he was building his business — and yet he still became successful.

His story gave other young start-ups a sense of hope: People started to believe that an entrepreneurial ecosystem could thrive in spite of wasta.

Toukan also provided crucial support to other young entrepreneurs, by offering office space and providing financial, strategic and legal support. And in fact, some of Maktoob's own employees went on to start their own wasta-free companies. As more and more of these businesses thrive, they encourage others to join them.

In addition to the death of wasta, there are other key factors changing the entrepreneurial game in the Middle East. What are they? Well, read on to find out!

### 4. The internet is changing the Middle East by creating new opportunities for education. 

In the Middle East, more than 83 percent of the population goes online every single day. So how does the Arab world's appetite for the internet impact this generation?

Well, the current generation of Middle Eastern entrepreneurs can be divided into three categories:

  1. _Improvisers_ adopt models that worked in the English-speaking world and rework them to reflect linguistic and cultural differences. For example, ArabMatrimony.com is the leading "dating site" in the Middle East, although in fact, it's actually a marriage site!  

  2. _Problem solvers_ launch companies that address local challenges which were once exclusively the government's domain. Take the case of RecycloBekia, which was founded to encourage Egyptians to recycle their used computers.   

  3. _Global players_ aim to found unique, innovative companies poised to reach global markets outside of the Arab world. WeatherHD, for example, became the largest paid weather app in the world, with nearly seven million downloads at the time of writing — half of which came from the United States.

Of these three groups, the problem solvers are the ones tackling the Middle East's educational infrastructure, which badly needs help.

Today, the whole region lags behind the rest of the world in secondary and higher education enrollment. And despite gains in certain areas — like gender parity — teachers are underpaid and rote memorization is the dominant pedagogical method.

However, two recent developments suggest that the situation may change:

  1. Businesses are taking corporate social responsibility seriously, and not just for the PR benefits. Companies are making a priority of strengthening society in order to improve their own working climate.   

  2. There's significant potential for innovation coming from the bottom, because today's youth have broad access to technology. Young professionals can easily connect with others and also enhance their own skills.

Essentially, technology is allowing Arabs to learn through the internet and to advance their newfound knowledge across society.

> _"The most recurrent theme I hear across the builders of the Middle East: 'Success breeds success.'"_

### 5. Women have a huge impact on tech entrepreneurship in the Middle East. 

There's no question that men dominate the tech scene, and yet, female entrepreneurs are becoming increasingly prevalent. Nowhere is this more apparent than in the Middle East.

If you have any doubts, just ask Hala Fadel, who runs the Middle East MIT Business Plan Competition: She sees the number of female applicants increase each year. And in 2012, 48 percent of the competing teams included women. Silicon Valley has yet to achieve similar gender parity.

Why have Arab women been able to thrive in the tech space? Well, Alyse Nelson, the CEO of Vital Voices, says that since women can now create change from their Twitter account, they can reach spaces which were once closed off to them.

This turned out to be the case for online activist Manal al-Sharif, who took on the restriction against female drivers in Saudi Arabia. Not only did she get behind the steering wheel, she also uploaded a video of herself driving on YouTube, where it instantly went viral. These kinds of social network campaigns give women a platform they never had before.

So what's it like for female entrepreneurs in the Middle East? Well, women thrive when they focus on gender-specific concepts.

So says Rana Ayoubi, creator of the multiplatform content production house, Rubicon Group Holding. Aroubi acknowledges that since men and women are different, female entrepreneurs should choose to cater to female concerns.

For example, Yasmine el Mehairy and Zeinab Samir created _Supermama_ for young, internet-savvy mothers struggling to find Arabic-language information about how to negotiate a work/life balance.

In fact, this is the key reason the internet has created so many opportunities for women: It allows women to find better ways to juggle the many facets of their lives — such as wife, mother and daughter — while starting their own business.

For example, today's technology allows female workers and entrepreneurs to adopt more flexible working hours, possibly even working from home. At the same time, the entry costs associated with launching an internet property are low.

### 6. Religion plays a key role in the Middle East’s entrepreneurial ecosystem. 

With many different cultural, technological and economic factors at play, the Arab world's tech scene is an ecosystem like any other.

And great entrepreneurial ecosystems are made up of three components:

  1. _Investors_ : People who finance companies and also provide mentorship, contacts and access to services like accounting, legal advice and HR.

  2. _Conveners_ : The people that bring entrepreneurs together to network and connect.

  3. _Recognizers_ : Organizers who set up competitions and highlight the best projects and ideas.

These three groups face distinct challenges in building start-up economies in the Middle East (dealing with, for example, inconsistent application of the law), but they play a crucial role in propagating fresh ideas across the region.

But in the Middle East, that's not all there is to it. There's one more key factor which shapes the entrepreneurial landscape: Religion.

And although Islam and technology have points of convergence — many Islamic financial principles, for example, coincide with libertarian ideas about private property — start-ups _are_ changing the nature of religion in the Islamic world.

To this end, some founders have implemented a "no religion, no politics" rule for their start-ups; others have changed their perspective on the role their religion can and should play in their business dealings.

For example, one Salafist entrepreneur, who might otherwise have never spoken with the author, now does so — despite the fact that he believes the author is going to hell. That's because entrepreneurship is all about networking, and someone who is unwilling to speak to others has no chance of success.

Other founders take a different approach to their start-ups, seeing them as an extension of their personal values. This is the case for Kuwait-born Naif al-Mutawa, who created a TV cartoon series, _The 99_, featuring 99 superheroes representing the 99 core values of Islam. Since he doesn't include overt religiosity in his stories, children from all backgrounds can watch and enjoy the series.

### 7. It’s only a matter of time before the technological revolution in the Middle East explodes in a big way. 

There's so much energy in the Middle East that nothing can prevent the region from exploding with entrepreneurial potential.

That's because the same tug-of-war that exists in most societies is more pronounced in the Middle East. This tug-of-war comes down to two narratives:

  1. The top-down power of governments and politicians to control society's agenda.

  2. The bottom-up, tech-enabled approach to problem-solving and opportunity building.

Although these two narratives coexist in most societies, according to investor Arif Naqvi, the West sees only one side of this dynamic in the Arab world. That's because the West is ignoring three key issues:

  1. Middle Eastern instability and violence blinds the West to clear economic trends and opportunities available in the region.

  2. What's happening in the Middle East is part of a global economic, and, to some extent, political shift from the West to the East.

  3. Middle Eastern young people are fighting for the same values as their Western counterparts: Social mobility, equality, democratic policies — and also for access to technology.

And ultimately, if technology can take hold in unstable regions, it can take hold anywhere. Of course, violence and instability are realities in the Middle East, but here's another reality: There's a growing group of curious, change-minded, tech-savvy youths. And the fact is, even politically active people still have to live normally, see their families and hold a job.

To that end, Palestinian Saed Nashef is the perfect example of how even people in war-torn places can get on the tech bandwagon. Together with Israeli-New Yorker venture capitalist Yadin Kaufmann, Nashef co-founded _Sadara Ventures_, a $30 million venture capital fund based in the West Bank. He says that technology is the right place to focus because it's less susceptible to geopolitical changes, and is clearly where the next wave of economic growth will be centered.

> _"The youth have dropped their fear, and that is reflected in their demands for freedom, career choices, and the wave of entrepreneurship. There is no going back." - Youssri Helmy_

### 8. Final summary 

The key message in this book:

**The internet is changing the world, and perhaps nowhere more so than in the Middle East. Through technological advancements and the opportunities that come with it, the region is developing culturally, socially and politically. And as entrepreneurs continue to pop up in the region, the Middle East is transforming into a center of economic opportunity.**

**Suggested further reading: _Breakout Nations_ by Ruchir Sharma**

Although many emerging markets across the globe grew at historically high rates over the past decade, for many nations these growth rates are unsustainable and are the result of a unique set of conditions in the global economy.

_Breakout Nations_ examines the factors likely to determine which emerging markets can take advantage of changing economic conditions and become breakout nations.
---

### Christopher M. Schroeder

Christopher M. Schroeder is an entrepreneur and venture capitalist. He has written extensively on start-ups and the Middle East for the _Wall Street Journal_, _Harvard Business Review_, _TechCrunch_ and others.

