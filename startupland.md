---
id: 5653a09463626400071d0000
slug: startupland-en
published_date: 2015-11-24T00:00:00.000+00:00
author: Mikkel Svane
title: Startupland
subtitle: How Three Guys Risked Everything to Turn an Idea into a Global Business
main_color: 51BBE7
text_color: 2E6982
---

# Startupland

_How Three Guys Risked Everything to Turn an Idea into a Global Business_

**Mikkel Svane**

In _Startupland_ (2015), Mikkel Svane tells the origin story of his own company, _Zendesk_. He explains how he went from working on a small website in Denmark to becoming the CEO of a million-dollar trading company in the United States, and shares the insights he gained along the way.

---
### 1. What’s in it for me? Improve your chances of standing your ground in the land of startups. 

Say you've just struck upon a brilliant idea for a business. Or maybe you already had one, but failed to follow through with it. Either way, you're hoping to build a successful business around your new idea. 

But in the seemingly open universe of creativity, competition is fierce. Here, everyone fights tooth and nail to turn their innovative ideas into a reality. So, how can you avoid getting swallowed up by the hordes of start-up predators?

You can start by taking advice from one of the heavyweights in the field. In these blinks, the author shares his personal story of building his own successful company, Zendesk. From tentative ideas to a thriving US-based business, the author invites you along his journey of ups and downs, offering plenty of tips on how to get your own ideas off the ground.

In these blinks, you'll discover

  * which investors are good investors;

  * why working on your team-building skills is so important; and

  * why the author had to relocate from Copenhagen to the United States.

### 2. It’s okay if your first start-up isn’t your last. 

When Columbus returned from his journey to the Americas, some of his peers insisted that any Spaniard could have "discovered" the new continent — all they had to do was sail westward. To prove them wrong, Columbus placed a hard-boiled egg on the table in front of them and challenged them to make it stand on its own. When they failed, he took the egg and gently tapped it against the table, creating a small, flat base that it could stand on. 

Columbus's point was that it's easy to do something once you know how — when you don't, you have to take a risk. Start-ups work the same way.

Founding a start-up is all about using your entrepreneurial spirit to try new things. The author worked on a number of projects before developing Zendesk, a software tool that enables companies to provide better customer support to their clients. 

His first venture created 3-D illusions based on 2-D patterns, using a piece of software. He ran his business alone: customers sent him orders and he sent the disks out himself.

The author's next product was a tool for producing websites, and it gave him his first taste of failure when it collapsed during the dot-com crash of 2001.

When founding a start-up you also have to realize that sometimes the best ideas are not always the most interesting. 

At first, few people had faith in the idea for Zendesk, as they weren't inspired by the thought of a company focused on providing better help desks and customer support. Alex Aghassipour, who ended up becoming one of the company's core members, initially thought Zendesk sounded incredibly boring.

Like Zendesk, a lot of important start-up ideas might seem unexciting at first, but they can become very attractive if they're executed well. Take the company Dropbox, for instance. File-sharing isn't the most exciting service to provide, but Dropbox has made this tedious task easy, interesting and even socially engaging.

### 3. Choose your investors wisely. 

If you're a kid on the playground looking for money, the school bully probably wouldn't be the best person to go to. He might give you the money, but if so, he'll ask for it back with interest. The start-up scene isn't all that different. 

Not all investors are good investors. Unfortunately, start-ups don't usually have a lot of options when it comes to getting their initial investments; when you first start out, you'll be exposed to a lot of investors who are only concerned with themselves. The best thing for your start-up is to turn them down and hold out for a bit longer. 

The author dealt with this same problem when he founded Zendesk. The first angel investor who was interested in his project kept asking for unreasonable amounts of information and materials from the company. The author eventually realized this was a tactic to put the young team under pressure. The investor knew they needed money urgently and was trying to gain more negotiating power by delaying the investment decision and asking for more paperwork.

So where should you look for investment? Sometimes your friends and family members make the best investors. When Zendesk started to run out of money, the founders asked their friends and families to invest, and word of the project spread from the founders' colleagues to their colleague's bosses. One person invested as much as $30,000!

Asking your friends and family for money isn't easy, however. The author's advice is to have low expectations, refrain from giving investors any influence and be prepared to disappoint people — you might even lose some friends. 

And even if you've collected enough funding already, don't turn down a good investor.

After the Zendesk founders had raised enough funds for their business, they were contacted by another angel investor. Instead of turning him down, they accepted his investment too, which allowed them to grow even more ambitiously. What's more, they gained a new, experienced financial partner.

### 4. Pick a great team – and fight to keep them together. 

Did you play team sports in gym class as a kid? Which team member was the most fun: the person who wanted everyone to participate and have a good time, or the star player who scored all the goals but did it alone? Of course, it's the former.

Start-ups work in a similar way. People with good team-building skills often make the best entrepreneurs; however, it's not always easy to keep the team on track. 

In Zendesk's early days, the three founding members had a hard time focusing on the project, as they were all working without pay. They had little money with which to support their families and it took a lot of commitment to stay with Zendesk instead of giving up and moving to something safer. 

The author kept the team together by offering to pay one of the founders a small salary, even though the company didn't have any money. He realized that keeping the team together was more important than breaking apart because of a lack of funds. 

But it can also be hard to get a team to agree, especially when they are in fragile situations. Zendesk ran into trouble when the founders had to acquire some major funding and started selling the company shares. This meant the founding members would become a minority among stakeholders — a hard reality to face. The new board members were even able to fire the founders if they wanted to.

The author lost his temper once during this stressful time, and at one point when he wasn't able to persuade his two partners of the switch, he yelled at them during a meeting and stormed out. The next few days were awkward, but things eventually settled down. The team members came to accept that getting major funding was the only way forward.

### 5. The United States is the best place to be in the start-up world. 

Outsiders are often annoyed by American aphorisms like "fake it till you make it" or "winners never quit." American optimism might seem unrealistic at first glance, but it's actually helped foster a great start-up environment. You can't succeed with a start-up if you don't have the confidence to take risks!

In fact, all major internet-related businesses (and the internet itself!) originated in the United States. Denmark, for instance, only had one internet provider and very expensive dial-up in the early '90s, but American cities like San Francisco embraced the internet early on and were enthusiastic about it right away. 

San Franciscans were using the internet to communicate, advertise and order food long before most Europeans even had e-mail. 

Why? Because a lot of great innovators and big investors congregate in the United States. It was thus clearly the best place for the Zendesk team to find their first investors because the American start-up scene was thriving. 

The author attended a _TechCrunch_ summer party in San Francisco after he launched Zendesk, and nearly all the guests there had heard about or were using his product! They were all working on their own start-up projects too. The author felt much more at home in that community than he did in the much more conservative start-up scene in Copenhagen. 

So the Zendesk founders weren't surprised at all when their first major deal required the company to relocate to the United States. When the team accepted the venture capital funding they had been so reluctant about, the offer came with a condition: Zendesk had to move to Boston, where the venture capital firm was based. Though Boston wasn't their ideal destination, the author convinced the team to make the move.

### 6. Your start-up will have an impact on your family. 

Entrepreneurs' lives often revolve around their work. They might work over 12 hours a day and when they're not working, they're thinking about work. Naturally, this can be challenging for their families for a number of reasons. 

For one, it's hard to take financial risks when you have a family. The author struggled with this when he accumulated a lot of credit card debt and used up his retirement money while building Zendesk. He also took out a $50,000 loan that he was personally liable for. At one point, he was two weeks away from going bankrupt if he didn't get more funding. 

The author didn't want his partner, Mie, to be stressed about these problems, so he didn't fully explain them to her. 

The author's work also created stress for his family when they had to move from Denmark to Boston. This was made even worse when they arrived in Boston that summer only to discover that their air conditioner had broken down. The young family had to walk around the house in their underwear for two months. 

The stress for the family didn't end with that move. In fact, not that long after, the family had to move from Boston to San Francisco!

The family moved west when Zendesk got a new, $6 million investment deal with a company called Benchmark, which is based in San Francisco. It was a hectic time for the family, as they had to move on pretty short notice. 

On one of their first nights in the new place, the children somehow managed to lock everyone out of the bathroom. Mie was sick at the time and the author had to spend two hours struggling to get the door open before he rushed off to work. When setting up a start-up of your own, hectic episodes like these are likely to become a fact of life.

### 7. Being adaptable and flexible with your recruitment can help you find the right employees. 

If you've ever had to sort through a pile of 80 CVs, all written in the same cryptic language, you know that picking the right candidate can be difficult. And sometimes interviews just make the situation worse: the interviewees get nervous and speak too little or too much. 

Now imagine if you had to go through all this in a foreign country! That's exactly what the Zendesk founders had to do after they moved to Boston, and they learned some pretty important lessons in the process.

The Zendesk team found that Americans and Danes present themselves very differently in job interviews. The author and his team were used to the Nordic approach: humility is highly valued in Scandinavia and you aren't supposed to suggest you're better than anyone else. 

So the Zendesk team was initially thrown off by their applicants' displays of American overconfidence and bravado. They believed all the applicants were amazing because they thought their bragging was actually modesty and humility.

The author and his colleagues quickly realized that they needed to adapt their recruitment process, and they came up with some great new strategies. 

For example, the head of human resources, who was a former army man, would take potential employees to a nearby café. On the way over, he'd walk too quickly to see if they could keep up, and he paid careful attention to how they handled the bill. He would also use foul language throughout the interview to see if they deal with it. 

Zendesk recruiters also didn't ask the applicants about their educational backgrounds. Instead, they asked them about their travel experiences and how they confronted difficult life situations.

### 8. Be prepared to make mistakes. 

Launching a start-up is like jumping from a ten-meter diving board: you might land a perfect dive, or do a painful belly flop. And you'll definitely have to take several dives before you really figure out what you're doing. 

For example, when Zendesk was starting out, the author had a few false starts while looking for funding. At one point, when the company was still based in Denmark, he flew to the United States to meet a partner in a venture capitalist firm. But his timing could hardly have been worse: the 2008 financial crisis was underway, mortgages were no longer available and big banks like Lehman Brothers and Merrill Lynch had collapsed. 

The venture capitalist firm and their partners had a big argument over whether or not to invest in the author's project; in the end, they decided not to. 

Another common rookie mistake is to overlook important details in your early years. The Zendesk team learned this when they hired Amanda Kleha, a former marketing manager at Google. 

Kleha arrived on her first day without a computer, assuming the company would provide one. When she left to go home and get her own, she made a dry remark that she guessed she had joined a start-up. This made the author realize that they needed to develop a more comprehensive onboarding strategy. 

The worst mistakes you can make, however, are those that affect your customers. The author learned this when Zendesk planned a price increase and their customers turned against them. Many of the customers' comments went viral, jeopardizing the company's reputation.

  

He realized the team had taken Zendesk's customer relationship for granted. They informed the customers about the price change but failed to explain the reason for it, so the customers felt they were paying for something they hadn't agreed to. Zendesk made an official apology and withdrew the decision to raise their prices.

### 9. Final summary 

The key message in this book:

**Building a start-up is all about developing new ideas and taking risks, so there's no one formula for success. Get a strong team together, watch out for disingenuous investors, be prepared to make mistakes and adapt your strategy when things don't go as planned. You'll hit roadblocks you can't prepare for, just like the author did when he built up Zendesk. But like the author, you can also grow your start-up if you stay flexible and committed to your vision.**

**Suggested** **further** **reading:** ** _Behind the Cloud_** **by Marc R. Benioff and Carlye Adler**

_Behind the Cloud_ (2009) details the rise of Salesforce.com from a one-room apartment start-up to a prosperous global company. Using often unconventional methods, Marc Benioff — the founder and CEO of Salesforce.com — steered his company through financial problems and the dot-com bubble burst, and came out on top.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Mikkel Svane

Mikkel Svane is the founder and CEO of Zendesk, a software company for customer support based in San Francisco.

© Mikkel Svane: Startupland copyright 2015, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

