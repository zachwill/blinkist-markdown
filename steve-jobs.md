---
id: 542bfc2d6238310008180000
slug: steve-jobs-en
published_date: 2014-10-05T00:00:00.000+00:00
author: Walter Isaacson
title: Steve Jobs
subtitle: None
main_color: EEA630
text_color: 875E1B
---

# Steve Jobs

_None_

**Walter Isaacson**

This book chronicles the audacious, adventurous life of Steve Jobs, the innovative entrepreneur and eccentric founder of Apple. Drawing from Jobs's earliest experiences with spirituality and LSD to his pinnacle as worldwide tech icon, _Steve Jobs_ describes the man's successful ventures as well as the battles he fought along the way.

---
### 1. What’s in it for me? Find out how Apple’s Steve Jobs became a worldwide technology icon. 

It's almost impossible to overstate the role that Steve Jobs played in the creation of our contemporary, computer-mediated world.

A single-minded perfectionist, Jobs was a visionary who wanted to change the world through technology.

In this best-selling biography, you'll discover that while Jobs's perfectionism and intensity pushed him to achieve great things, those same traits were the cause of friction and conflict. In his relationship with employees and collaborators, Jobs's behavior was often seen as brattish — even though Jobs might argue that he was simply trying to push people to deliver their best.

The following blinks detail the fascinating life of one of the most influential tech icons of our time, and tell the amazing story of a teenage prank that cemented a partnership that would eventually create one of the most valuable technology companies in the world.

In these blinks, you'll also discover:

  * how LSD and meditation led to the creation of today's tech gadgets;

  * why Woody or Buzz Lightyear wouldn't exist without Steve Jobs; and

  * why Jobs tragically believed he could cure his cancer with acupuncture and eating fruit.

### 2. A handyman father and a prankster best friend instilled in Jobs a love of engineering and design. 

On February 24, 1955, a boy was born to Abdulfattah Jandali and Joanne Schieble.

However, Jandali and Schieble wouldn't raise their child. Since Schieble came from a strict Catholic family who would disown her for having a child with a Muslim man, the pair were forced to give the baby up for adoption.

And so the child was adopted by Paul and Clara Jobs, a couple living in Silicon Valley, who named him Steven.

Paul Jobs was an engine technician turned car mechanic, and he introduced Steve to the world of engineering and design.

From an early age, Paul tried to pass along his love of mechanics to Steve, who recalls being impressed by his father's focus on craftsmanship. If the family needed a cabinet, for example, Paul would simply build one, letting Steve help him in the process.

Additionally, the family's smart yet inexpensive Eichler house — an "everyman" modern home, with floor-to-ceiling glass walls and an open floor plan — sparked Steve's obsessive interest in clean, elegant design.

Later, in high school, Steve Jobs met Steve Wozniak; the two immediately clicked.

Wozniak was five years older and already a talented computer technician, from whom Jobs learned a lot about computers.

In many ways, Jobs and Wozniak were typical young boys and liked to play pranks. But they also loved to explore the world of electronics and see what they could create.

Combining both interests, in 1971 they launched their first product: the "Blue Box," a device that allowed users to make long-distance phone calls for free.

Wozniak supplied the design and Jobs turned the innovation into a business, taking $40 worth of parts and selling the device for $150.

The pair sold almost 100 boxes, giving them a taste of what they could do with Wozniak's engineering skills and Jobs's vision; and paving the way for what would become their joint venture — Apple.

### 3. Jobs’s interest in spirituality, LSD and the arts shaped his aesthetic sense and extreme focus. 

In the late 1960s, the interests and curious cultures of geeks and hippies began to overlap.

So it was perhaps inevitable that, in addition to his passion for math, science and electronics, Jobs would immerse himself in the counterculture and begin experimenting with LSD.

Jobs later attributed his refined aesthetic sense and extreme focus to his experiences with psychedelic drugs and spirituality.

In 1972, Jobs enrolled at Reed College, a private liberal arts school in Oregon, and there became very serious about both meditating and experimenting with LSD with friends.

He felt that his drug experiences helped reinforce his sense of what was important in life, by showing him that there is "another side to the coin." In Jobs's case, this meant realizing that creating great things was more important than anything else.

Eager to explore Eastern spirituality, Jobs even travelled to India, where he ended up staying for seven months. Zen Buddhism in particular became a deeply entrenched aspect of his personality, influencing his minimalist aesthetic approach and introducing him to the power of intuition.

Both interests — LSD and spirituality — helped him to develop a certain kind of focus, which became known as Jobs's _reality distortion field_ : if he'd decided that something should happen, then he would simply make it happen by bending reality to his will.

Another factor that shaped Jobs's minimalist aesthetic was his enthusiasm for the arts. Throughout his career, Jobs would emphasize over and over that the design of Apple products should be clean and simple.

This ideal was formed during his college years. Even though he dropped out of college, Jobs was allowed to continue taking classes, which he did solely for the purposes of self-enrichment. One of these was a calligraphy class, his skill in which later became a key element in the Apple Mac's graphical user interface.

> _"Jobs's craziness was of the cultivated sort."_

### 4. A visit to an apple farm gave them a name; a counterculture vision and hard work made a company. 

It seems an odd match: a spiritually-minded, LSD enthusiast and the staid computer industry. Yet in the early 1970s, many people were starting to see computers as a symbol of individual expression.

So while Jobs was immersed in drugs and Zen, he was also dreaming of starting his own business. And around the same time, his friend Steve Wozniak came up with an idea for the modern personal computer.

In the early days of the Silicon Valley technology revolution, Steve Wozniak joined the Homebrew Computer Club — a place where computer "nerds" met to exchange ideas and where the overarching philosophy was that the counterculture and technology were a perfect marriage.

It was here that Wozniak had his idea. Computers at the time required several, separate hardware components to work, making them complicated to manage and difficult to use. Wozniak imagined a device as a self-contained package, with a keyboard, screen and computer "all in one."

Initially, Wozniak considered giving away his design for free, as this was the ethic of Homebrew. However, Jobs insisted that they should profit from Wozniak's invention.

So in 1976, with just $1,300 of start-up capital, Wozniak and Jobs founded Apple Computer.

On the day they had to come up with a company name, Jobs had visited an apple farm, and — because it was simple, fun and familiar — the name "Apple" stuck.

Wozniak and Jobs toiled away for a month building 100 computers by hand. Half of the total was sold to a local computer dealer and the other half to friends and other customers.

After just 30 days, Apple's first computer, the Apple I, was on the verge of being profitable.

Together, Jobs and Wozniak made a powerful team — Wozniak was the engineering wizard while Jobs was the visionary, who saw that the personal computer could change the world.

### 5. Jobs was a controlling, temperamental boss driven by an uncompromising passion for perfection. 

Those who knew Jobs personally would agree that he was an erratic, even quirky individual. If work didn't meet his high standards, he would throw temper tantrums and verbally assault people.

But why was Jobs so controlling and temperamental?

In short, he was an unforgiving perfectionist. Jobs wanted the Apple II to be a perfectly designed, fully featured computer that was integrated end to end. But while his drive helped make the Apple II a success when it was released in 1977, it also drained the energy and will of those in the company.

If Jobs felt an employee's work was poor, he'd tell them it was "shit," and would be furious if he noticed even the tiniest imperfection.

As Apple grew as a company, Jobs only became more erratic. Mike Scott was eventually appointed as Apple's president, with the main task of keeping a tighter rein on Jobs.

Scott basically had to confront Jobs with the thornier issues that other employees just didn't have the energy to do. This often resulted in disagreement, sometimes with Jobs breaking down in tears, as he found relinquishing any control over Apple extremely difficult.

Jobs found it especially frustrating that Scott tried to put limits on his perfectionism. But, for his part, Scott didn't want Jobs's perfectionism to take precedence over pragmatism.

For instance, Scott stepped in when Jobs thought that none of the 2,000 shades of beige were good enough for the case of the Apple II, and also when Jobs spent days deciding just how rounded the corners of the computer case should be. Scott's focus was to get the case manufactured and sold.

However, because the company was doing so well, these personality clashes were somewhat manageable. But, as you will see in the next blink, this was not to last.

> _"Jobs did not wear his growing responsibilities gracefully."_

### 6. The Macintosh put Jobs on a pedestal as a tech icon, but his vitriol got him knocked off. 

The Apple II, with some six million computers sold, was considered the spark that led to the creation of a personal computer industry.

But for Jobs, it wasn't a complete success, as the Apple II was Wozniak's masterpiece, not his own.

Jobs wanted to create a machine that would, in his words, "make a dent in the universe." Driven by this ambition, Jobs began work on the Macintosh — a successor to the Apple II that would further transform personal computing and establish him as a technology icon.

Yet the Macintosh was not solely Jobs's invention, as he actually stole the Macintosh project away from its founder, Jef Raskin, an expert on human-computer interfaces. And so Jobs took the idea and created a machine that ran on a microprocessor powerful enough to accommodate sophisticated graphics, and could be largely controlled by a mouse.

The Macintosh became an unparalleled success, partly due to a lavish marketing campaign that included a sensational TV commercial — now known as the "1984" ad — directed by Hollywood filmmaker Ridley Scott. Tied in with the commercial's popularity, the Macintosh launch caused a kind of publicity chain reaction, for Jobs as much as for the product.

Crafty as ever, Jobs managed to land a number of high-profile interviews with several prominent magazines, by manipulating journalists into thinking the interview he was giving them was an "exclusive."

His strategy worked, and the Macintosh made Jobs wealthy and famous. He had achieved the kind of celebrity that could get singer Ella Fitzgerald to entertain at his extravagant 30th birthday party.

However, those same personality traits that had enabled Jobs to make the Macintosh a success also soon got him fired.

His perfectionism and oppressive behavior toward Apple employees continued unabated. He would constantly call people out for being "assholes" if he thought they didn't care about perfection.

Jobs's brattish behavior led to a showdown with the company. In 1985, the Apple board of directors decided to let Jobs go.

### 7. Jobs flopped with NeXT yet struck gold with Pixar, a company at the cutting edge of animated films. 

After Jobs recovered from the blow of being fired from Apple, he realized he could now do things exactly the way he wanted to — indulging both his good and bad sides.

He first created a new business venture aimed at the educational market, a computer called NeXT.

With the NeXT project, Jobs got to indulge his passion for design. He paid a $100,000 flat fee to get the logo designed, and insisted that the NeXT computer case should be a perfect cube.

But Jobs's perfectionism made the computer hard to engineer and manufacture. For one thing, the sides of the cube casing had to be produced separately, using molds that cost $650,000.

Jobs's uncompromising vision was essentially NeXT's death knell. The project almost ran out of money, the release was delayed by several years and in the end, the machine was far too expensive for the end consumer. And because of its high price and small software library, NeXT barely made a wave in the larger computing industry.

During the same period, however, Jobs also bought a majority share in a company called Pixar. In his role as chairman, Jobs revelled in being part of a venture that was the perfect synthesis of technology and art.

By 1988, Jobs had sunk almost $50 million into Pixar while also losing money in NeXT.

But after years of financial struggle, the studio released _Tin Toy_, a film that showcased Pixar's unique vision for computer animation. _Tin Toy_ went on to win the 1988 Academy Award for Best Animated Short Film.

Jobs thus saw that he should shift his focus from software and hardware products, which were losing money, to Pixar, a company that was making cutting-edge, potentially lucrative animated films.

Eventually, Pixar partnered with Disney to produce its first feature film, _Toy Story_. Released in 1996, _Toy Story_ became the top-grossing movie of the year.

When Pixar went public, the shares that Jobs held (80 percent of the company) were worth more than 20 times his initial investment: a whopping $1.2 billion.

### 8. Away from Apple, Jobs made amends in his private life, reconnecting with his biological family. 

In addition to learning a lot professionally during his 12 years away from Apple, Jobs also developed in his private life.

In 1986, after the death of his adoptive mother, Jobs became curious about his roots and decided to search for his biological mother.

When he finally found Joanne Schieble, she was emotional and apologetic for giving Jobs up for adoption.

Jobs was also surprised to learn that he had a sister, Mona Simpson. Both he and Simpson were artistic and strong-willed, and eventually became close.

In 1996, Simpson would publish a novel called _A Regular Guy_. Its protagonist was based on Jobs and shared many of Jobs's unflattering character traits. However, since he didn't want to feel any anger toward his newfound sister, Jobs never read the novel.

Around the same time, Jobs met Laurene Powell. The pair married in 1991, with the blessing of Jobs's old Zen guru. Powell was already pregnant with their first child, Reed Paul Jobs. The couple would go on to have two more children, Erin and Eve.

With Powell's encouragement, Jobs also attempted to spend more time with Lisa Brennan, a daughter from his first relationship with whom he'd initially been estranged.

Jobs tried to be a more proactive father to Lisa; and eventually, she moved in and lived with Jobs and Powell until she went to college at Harvard.

Lisa would grow up to be as temperamental as Jobs, and since neither was good at reaching out and making amends, the pair could go months without saying a word to each other.

In a broad sense, Jobs's way of engaging with people in his private life was similar to his behavior at work. His approach was binary: either he was extremely passionate or extremely cold.

### 9. As Apple’s fortunes dimmed, Jobs returned like a prodigal son to lead the company as CEO. 

In the years following Jobs's dismissal, Apple started to fail as a company.

To halt this decline, Gil Amelio was appointed CEO in 1996. Amelio knew that to get Apple back on the right path, it needed to team up with a company with fresh ideas.

And so in 1997, Amelio chose to acquire NeXT's software, effectively making Jobs an advisor to Apple.

Once he was back at Apple, Jobs grabbed as much control as he could manage. To this end, he quietly began building his power base by installing his favorite NeXT employees into Apple's top ranks.

During this period, Apple's board realized that Amelio was not going to be Apple's savior. But they thought that the company might have a chance again with Jobs.

So the board offered Jobs the CEO position at Apple. Surprisingly, Jobs declined. Instead, he insisted on remaining in his position as advisor, and helped lead the search for a new CEO.

Jobs used his status as advisor to increase his influence within Apple. He even forced the board to resign — the very same board that had recommended he take the CEO role — as he felt they were slowing his progress in transforming the company.

As advisor, Jobs also managed to establish a partnership with rival Microsoft, getting the company to create a new version of Microsoft Office for the Mac, thus ending a decade of legal battles and making Apple's stock price skyrocket.

Eventually, and after much hesitation, Jobs became CEO and demanded that the company refocus on making fewer products.

Jobs ended the licensing deals that Apple had made with several other computer manufacturers and decided to focus the company on making just four great computers: a desktop and a laptop for both the professional and consumer market.

In 1997, Apple recorded a loss of $1.04 billion. But in 1998, after Jobs's first full year as CEO, the company recorded a profit of $309 million. Jobs had effectively saved the company.

> _"In returning to Apple, Jobs would show that even people over 40 could be great innovators."_

### 10. Bold ideas and visionary design made the iMac and the first Apple Store astronomical successes. 

When Jobs discovered the visionary talent of designer Jony Ive, he made Ive the second-most powerful person at Apple, after himself. Thus began a partnership that would come to be the most important industrial design collaboration of its era.

The first product that Jobs and Ive designed together was the iMac, a desktop computer priced at around $1,200 and designed for the everyday consumer.

With the iMac, Jobs and Ive challenged the conventional idea of what a computer should look like. In choosing a blue, translucent case, the pair reflected their obsession with making the computer perfect, both inside and out. This design also gave the computer a playful appearance.

Launched in May 1998, the iMac became the fastest-selling computer in Apple's history.

However, Jobs began to worry that Apple's unique products could be lost among the generic products of a technology megastore. His solution was to create the Apple Store as a way to allow the company to control the entire retail process.

As Gateway Computers suffered financially after opening its own retail stores, the Apple board was understandably opposed to Jobs's idea. However, convinced that he was right, Jobs stuck to his guns and the board approved a trial run for four Apple Stores.

Jobs began by building a prototype store, furnishing it completely and obsessing over every detail of the service experience and general aesthetic. He insisted on a sense of minimalism throughout the process, from the moment a customer enters the store to the moment they go through checkout.

In May 2001, the first Apple Store opened. It was a massive success, as Jobs's careful design had pushed retailing and brand image to a whole new level.

In fact, the Manhattan store would eventually become the highest-grossing store of any store in New York, including such historic outlets such as Saks Fifth Avenue and Bloomingdale's.

### 11. Wanting to control the entire digital experience, Jobs created the iPod, the iPhone and the iPad. 

Following his success with the Apple Store and the iMac, Jobs came up with a new, grand strategy. His vision was a personal computer at the center of a new digital lifestyle.

He called this his _digital hub_ strategy.

The strategy envisioned the personal computer as a kind of control center that coordinates a variety of devices, from music players to video cameras.

As the first step in realizing his vision, Jobs decided that a portable music player would be the next Apple product.

In 2001, Apple released the iPod, a streamlined device which combined the now-famous click wheel with a small screen and new hard disk technology.

Critics were skeptical that people would spend $399 on a music player, but consumers made the iPod so successful that, by 2007, iPod sales accounted for half of Apple's revenues.

The next step was to design an Apple cell phone, since Jobs was worried that soon, mobile phones with built-in music players would make the iPod redundant.

In 2007, Apple released the first version of the iPhone. Two important technologies made the iPhone possible: the touch screen, which could process multiple inputs simultaneously, and the incredibly robust cover glass, called Gorilla Glass.

Again, critics were skeptical of Apple's strategy, arguing that no one would pay $500 for a cell phone — and again Jobs proved them wrong. By the end of 2010, profits from iPhone sales accounted for more than half of the total profits generated in the global cell phone market.

The final step in Jobs's strategy was the release of a tablet computer, the iPad.

Apple officially launched the iPad in January 2010. However, because Jobs had unveiled the product before it was publicly available, the press trashed it before they even tested it.

Still, when the iPad was officially released, it was a great success. Indeed, Apple sold over one million iPads in the first month, and 15 million in the first nine months.

With the release of the iPod, iPhone and iPad, it was clear that Jobs's bold _digital hub_ strategy had succeeded in transforming the consumer technology industry.

### 12. Jobs’s insistence on perfect, closed systems mirrored his fanatical obsession with control. 

Throughout his entire career, Jobs maintained that a closed, tightly integrated system produced the best consumer experience. This ideal reflected Jobs's desire for control, since by sealing his systems shut, he prevented consumers from modifying them.

This obsession with control generated some major conflicts — especially with Microsoft and Google.

Microsoft's Bill Gates had a vastly different approach to business and technology, in that he was open to licensing his company's operating system and software to a variety of third-party manufacturers. In fact, Gates had even written software for the Macintosh.

However, what was once an amicable business relationship between Jobs and Gates turned into a life-long rivalry.

When Gates produced the Windows operating system, Jobs accused him of copying the Macintosh's graphical interface. The truth, though, was that both systems had "borrowed" the idea from another technology company, called Xerox.

At the end of his career, Jobs also led a crusade against Google. In the company's design of its Android operating system, Jobs argued, Google had copied many of the iPhone's signature features.

While both Microsoft and Google believed that open computer systems and natural competition should determine which technology should prevail, Jobs maintained until the very end that both companies had stolen ideas and concepts from Apple.

But the targets of Jobs's vitriol weren't only competing companies. Jobs also fought relentlessly for perfection within Apple, leading to employees either burning out or getting fired. Under Jobs, there was zero tolerance for underperforming at Apple.

Whenever he thought that someone wasn't an "A-player," and didn't work 90 hours a week, he'd often not bother to ask them to improve. He'd just fire them on the spot.

And when a company was having trouble delivering enough computer chips on time, Jobs stormed into a meeting, screaming that they were "fucking dickless assholes." This sort of behavior was quite symptomatic of Jobs's aggressively intense perfectionism.

### 13. Jobs ignored all conventional wisdom regarding his cancer diagnosis and died prematurely in 2011. 

Jobs first learned that he had cancer during a routine urological exam in October 2003.

Unfortunately, Jobs approached the problem of his cancer the same way he approached a design problem: he ignored all conventional wisdom and decided on his own method for fighting it.

He refused surgery for a period of nine months, and instead tried to cure himself with acupuncture and vegan diets. As time passed, the cancerous tumor grew and eventually, Jobs had to have invasive surgery to remove it.

Yet even when the cancer returned in 2008, he again insisted on a strict diet of certain fruits and vegetables, which led him to lose over 40 pounds.

Eventually, Jobs was convinced to have a liver transplant; but following this, his health took a serious dive from which he'd never recover.

Jobs died in 2011. Behind him, he left a legacy in one of the most valuable tech companies in the world.

Everything that Jobs did in his life was a product of his incredible intensity, and before he died he said, "I've had a very lucky career, a very lucky life. I've done all that I can do."

Like practically no other individual, Jobs's personality was fully reflected in his creations, as every Apple product was a tightly closed, integrated system of hardware and software.

And while the open strategy employed by Microsoft — allowing its Windows operating system to be licensed — led them to dominate the operating system industry for many years, Jobs's model proved advantageous in the longer run, as it ensured a seamless, elegant end-to-end user experience.

Shortly before his death, Jobs was able to witness Apple finally surpassing Microsoft as the most valuable technology company in the world.

> _"The unified field theory that ties together Jobs's personality and products begins with his most salient trait: his intensity."_

### 14. Final summary 

The key message in this book:

**Steve Jobs grew up in Silicon Valley at the intersection of arts and technology, drugs and geekiness. There he would cement a friendship that would lead to the birth of Apple as well as a profound shift in the world of technology. During his life, Jobs managed to transform our relationship with technology, creating a range of digital products with a clean design and user-friendly interface.**

**Suggested further reading:** ** _The Everything Store_** **by Brad Stone**

While today the company is valued at more than a billion dollars, Amazon's humble beginnings started in founder Jeff Bezos's garage. From the get-go, Bezos was driven by the grand vision of creating an _Everything Store_ — which has since virtually come true. Focusing equally on the company and its founder, this book shows how Bezos turned his dream into a reality.
---

### Walter Isaacson

Walter Isaacson is an American writer and biographer. He was formerly the editor of TIME magazine as well as CEO and chairman of the CNN news network. Isaacson has written best-selling biographies of Albert Einstein and Benjamin Franklin, and is also the author of _American Sketches_ (2003).

