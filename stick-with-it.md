---
id: 5ab3bf58b238e10005bf5806
slug: stick-with-it-en
published_date: 2018-03-22T00:00:00.000+00:00
author: Sean D. Young
title: Stick with It
subtitle: A Scientifically Proven Process for Changing Your Life – for Good
main_color: F7EB3D
text_color: 78721E
---

# Stick with It

_A Scientifically Proven Process for Changing Your Life – for Good_

**Sean D. Young**

_Stick with It_ (2017) offers sensible advice that you can start using today to make positive changes in your life. Drawing from real-world case studies and the latest behavioral research, author Sean Young provides a thorough and rational plan for how you can combat your bad habits and follow through on your dreams and goals.

---
### 1. What’s in it for me? Find out how to make your New Year’s resolutions stick. 

It's the same thing every year — you want to get in shape, start eating better, stop smoking or finally finish that project you've been talking about for ages. And, most of the time, you're lucky if you make it to the end of February before it all falls apart.

It's time to resolve not to let this happen again.

We all have certain habits and tendencies that are at odds with our best intentions. The good news is, we've made some major progress lately in terms of understanding the way our mind works, and how we can trick ourselves into doing what we'd really like to do.

These blinks represent a compilation of some of the best research on how to start good habits, as well as techniques to help you follow through on your goals. So dive in to start checking off those goals and making resolutions that will last!

In these blinks, you'll discover

  * what Weight Watchers can teach you about achieving goals;

  * the difference between a neurohack and a life hack; and

  * how PTSD therapy can help you break a bad habit.

### 2. If you’re looking for more motivation, break down your dreams into manageable steps. 

No matter how big your goal is, try to remember that Rome wasn't built in a day.

Indeed, the first tip for achieving your goals is to know that your mind will respond better to small steps than to giant leaps.

Just consider a 2012 study that followed the progress of 126 overweight women who were trying to lose weight. The researchers found that those who focused on short-term goals, like sticking to low-calorie food every day, were more likely to lose weight. Not only that, but those who focused on hitting long-term goals, like losing a certain amount of weight by the end of the month, actually _gained_ weight.

Short-term goals are effective because they take advantage of the way your mind works. For instance, the pleasure we feel when earning a reward is associated with the dopamine our brain releases when we anticipate that reward. Just the thought of being able to check off a meaningful goal is treated like a reward in your brain. Therefore, it's more pleasurable and motivating to set small and frequent daily goals, and thus earn these consistent rewards, than it is to aim for some distant, future goal.

So, when you're setting up a new target for yourself, think about how you can break this broader task into three smaller categories: _steps_, _goals_ and _dreams_.

_Steps_ contain small tasks that take two days or less to complete. If your goal is to learn a new language, this might entail signing up for a course or buying a workbook.

_Goals_ should contain both the short-term and long-term objectives. The short-term goals, which should take around a week to complete, should add up to complete the long-term ones that take around a month to complete.

Lastly, _dreams_ are ultimate goals that will take three months or longer to complete. So, If your dream is to write a book, the steps might be to write one page a day, the short-term goal might be to write 2000 words a week, and the long-term goals might be finishing a chapter every month.

### 3. If you want to stick with it, make sure you're part of a supportive community of like-minded people. 

So if the first stride toward reaching your goal is breaking down your dream into the right steps, the second can be to find external support.

That's why the next tip is all about giving yourself a solid support system. In fact, a great way to increase your chances of success is to surround yourself with people who have like-minded goals.

Organizations such as Alcoholics Anonymous, Weight Watchers and CrossFit are excellent examples of the great results that can come about when people join forces to achieve common goals. And it's all due to the amazing potential for change that is created when people support one another.

As a matter of fact, there's a specific kind of power that comes from such organizations, and it's called the _social magnet_. This refers to the magnetic pull that a community can have on individuals, making them less likely to splinter off and abandon the shared goal. Plus, communities usually offer a safe space for people to confront problems they may be otherwise hesitant to deal with.

This community doesn't have to be a physical one, either. For example, the author helped a team of researchers create _HOPE_, an online forum for men at risk of contracting HIV. HOPE allowed people to join anonymously, which made them feel more comfortable sharing experiences and not worry about the unfortunate social stigma that can still accompany homosexuality. There were also online mentors on HOPE who would encourage members to express their concerns and whatever challenges they were facing.

Statistics showed that those who joined HOPE were twice as likely as non members to get tested for HIV. The member retention numbers looked good as well: after 15 months, 82 percent were still active in the online community.

HOPE, along with similar communities and support groups, addresses a basic human need to feel encouraged, empowered and trusted. So, if you're feeling helpless, or if you just need a push in the right direction, chances are there's a community of like-minded individuals who would be happy to offer their support.

### 4. Take advantage of human nature in order to reach your goals. 

There are three basic needs: food, shelter and sleep. While people in modern societies don't usually give much thought to these basic elements of human survival, being mindful of them can help you reach your goals.

Another important human need, and one that can greatly influence our behavior, is social interaction.

Research shows that the human brain reacts the same way to friendly social interaction as it does to the physical sensation of being warm. Likewise, if the social interaction is unfriendly and someone feels rejected, the brain reacts the same that it would to physical pain.

This implies that our need for social connections is essentially a physical need, like staying warm and unharmed, which brings with it a very strong instinctual motivation. This is another reason why linking your goals to social programs like HOPE can be so beneficial.

A similar benefit can also be found in the basic need to stay healthy.

A common new year's resolution is to quit smoking. Statistics show that when smokers are given specific details about how damaged their lungs can get through smoking, they're far more likely to quit than if they're given a more general warning about their health.

In other words, we're more motivated when things are put in a relevant personal context. This motivating factor is something psychologist Hal Hershfield has studied in his attempts to better understand how people's long-term behaviors can be changed for the better.

In particular, Dr. Hershfield's research focused on how people dealt with their future retirement savings. The study had two groups of participants: one group was given current photos of themselves, while the second group was given photos of themselves that had been digitally altered to make them look older.

The results showed that the second group, faced with the fact that they'll one day be older and more fragile, consistently put aside more money for retirement.

### 5. Making changes to your environment and having a clear direction can help you stay on track. 

There's an old saying from the author Henry David Thoreau that says, "The path of least resistance leads to crooked rivers and crooked men." Thoreau's piece of wisdom is one that we tend to hear repeated throughout our lives: don't take the easy way out.

But not taking the easy way doesn't necessarily mean you should take the hard way.

When it comes to achieving your goals, there's no reason why you shouldn't remove as many unnecessary obstacles as you can. And there are some relatively small changes that you can make to your life that can greatly increase your chances of sticking with a goal.

For example, if you're on a diet, why wrestle with your willpower by surrounding yourself with junk food, when you can make things easier for yourself by turning your home into a junk food-free zone?

The same goes for trying to quit smoking. In a six-month study of over a thousand smokers, the ones who cleared their home of all tobacco products were shown to have the greatest chance of quitting.

Another way to make things easier for yourself, and to prevent yourself from veering off-track, is to make a road map to your goals, complete with concrete steps along the way.

In a study conducted at Yale University, researchers wanted to find out what kind of information led to the best results in terms of getting people to follow public health directives, such as getting a tetanus shot. Half the participants were given leaflets with detailed information about tetanus, and reasons for getting the shot, such as what kind of bacterial infection it is and how it causes severe muscle spasms. The other half were given this same information, along with a map of how to get to the clinic, the opening hours and other practical information on how to get the shot.

Unsurprisingly, those who were given the road map and the action plan were more likely to go get a tetanus shot. And the same principles apply to any desired results: the more concrete steps you have in front of you, the likelier you are to reach the goal.

Makes sense, right? Not everything in life is complicated, and there are certain steps you can take to help make sure you finish what you start.

### 6. By taking certain actions, you can change your thoughts and stay motivated. 

Have you ever heard of a _neurohack_? It's like a "life hack," but one designed to improve the way you think.

Researchers have been finding all sorts of ways our behaviors can be hacked. Some of them are rather surprising, like the research that suggests holding a pencil between your teeth tricks your brain into laughing more!

There are different types, but the behavioral neurohack is considered the most important kind. This is when an action is used to counteract unwanted thoughts or feelings.

For example, a group of researchers was observing a group of heterosexual men who all admitted to feeling insecure and socially awkward around women. As part of their study, which was conducted in 1982, the researchers paired the men with women for 12-minute conversations. After speaking with the women, the men reported feeling less anxious and insecure; after six months, most men still felt better and some even had success with dating.

Now, this simple 12-minute conversation is considered a neurohack — a quick and easy way to make someone feel more confident and socially capable than they previously thought possible. One thing this teaches us is that one of the best ways to overcome fears and anxiety is to confront them head-on, which can be very helpful for finding motivation.

Another way to change your behavior, and to motivate yourself to act a certain way, is to choose your words carefully and to use speech that both reflects and shapes your identity.

In a 2011 survey that aimed to find out how likely people were to vote, participants were asked two versions of the same question. The first was, "How important is it for you _to vote_?" and the second was, "How important is it for you _to be a voter_?"

The researchers learned that voting rates were higher for people who responded strongly to the second question, since it spoke to their identity as a voter, and not just the act of voting. So, if you're setting out to accomplish new goals, use identifiers like this to your advantage. If you're trying to start running, then start referring to yourself as a runner, and not just someone who runs.

These tips might seem overly simple, but when they are all used together, they can add up to make a big difference.

### 7. Avoid the pitfalls of fear-based motivation and working solely for rewards. 

If you're trying to teach a dog a new trick, you're probably going to reward it with a treat when it accomplishes a task, right? Well, as it turns out, humans also respond well to rewards. But since we can be more complex than animals, there are a few things to keep in mind.

To begin with, remember that positive reinforcement is a more effective motivator than fear.

It's true that fear can motivate someone, like using the threat of a heart attack to get someone to stick to a healthy diet, but it often only works for a little while. People tend to see fear as an immediate threat, so when time passes and nothing bad happens, or the threat is no longer pertinent, the person can return to their bad habits.

Fear can also backfire, as we can see in the persistent problem of educating Americans about safe sex.

Public health officials have tried to use fear-based tactics to change behaviors and educate people about the dangers of HIV/AIDS, yet the rate of condom use has actually declined in the most at-risk areas. What researchers found was that the fear campaign caused people to panic, and, as a coping mechanism, many people ignored the efforts of the health officials.

But positive results have been observed when using rewards to encourage good behavior.

For example, the Colorado state prison system has benefitted from using "good behavior" incentives, which involves rehabilitating inmates through psychiatric therapy and cognitive behavior classes. For inmates who show improved behavior, rewards include padded chairs, earlier meals and access to television. Over a period of 14 months, prisoners in the good behavior program at the Limon, Colorado penitentiary were responsible for only two percent of rule violations.

However, it's important that you avoid attaining a goal solely for the reward. The act of completing the goal should always be seen as a reward in and of itself.

In the results of a famous study by independent scholar Alfie Kohn, employees were found to be more motivated by simple goal-setting and workplace training than when incentive programs or rewards were used. The results showed that employee rewards worked in a similar way to fear-based incentives: there were some short-term improvements, but these were negligible in the long run.

So remember, make the glory and benefit of self-improvement your motivation, and stay away from fear or the fleeting pleasures of a reward.

### 8. The secret to eliminating bad habits is to replace them with positive ones. 

Bad habits are like an uncomfortable pair of shoes or an old broken toaster. The best way to fix them is to replace them with something that works.

However, swapping out a bad habit for a good one is trickier than going to the shop and getting a new toaster. In this case, it helps to know how a habit forms in the first place.

Your brain responds well to actions that are both simple and efficient, which is why it can quickly memorize the behaviors you repeat and turn them into habits. So, good habits like healthy eating and exercise can be formed through repetition, which, in turn, will make these activities easier.

Once a behavior becomes a habit, it becomes ingrained in your mind and can be done more or less automatically. This frees up mental space, making it an efficient behavior that can be performed with relatively little thought.

Now that you know how habits form, you can keep this in mind when seeking new, more positive habits to replace your unwanted ones.

It may also help to know the story of one of the first American infantrymen to enter Iraq in 2003. David George, like many soldiers, witnessed repeated acts of horrible violence that eventually led to post-traumatic stress disorder, or PTSD. For people suffering from PTSD, common triggers like flashing lights and loud noises can immediately bring repressed traumatic memories to the surface.

Interestingly enough, the way in which these triggers consistently summon negative memories is similar to an unwanted habit. So, after finding little success with medication and other treatments, David eventually found relief from his anxiety and stress through the daily repetitions of meditation practice.

In fact, meditation is a perfect example of a beneficial habit that can be used to replace a bad habit. In the final blink, we'll look at some of our most common unwanted behaviors that might be remedied through some mindfulness.

### 9. There are three types of unwanted behavior that we typically engage in. 

If you make a mistake, someone might ask, "Why on earth did you do that?" This can be a tricky question to answer — but generally speaking, it was probably caused by one of these three types of behaviors:

The first is known as _automatic behavior_, which is the kind we engage in without conscious awareness. This would include acts like nail-biting or mindless snacking on junk food. The best solution here is to remove temptations and to replace these habits with positive ones.

So, to break a nail-biting habit, you could chew gum or wear gloves until a new habit forms.

The second category is _burning behavior_, which are compulsions that get triggered by certain impulses or irresistible urges. A common burning behavior these days is to constantly be checking your cell phone. The best way to change a burning behavior is similar to the previous category: you should remove certain elements from your environment and try to replace these compulsions with positive ones.

For example, if you spend too much time checking your phone, you could make sure to turn it off during certain times of the day and replace it with a more constructive activity, like reading a book.

The third category is known as _common behavior_, which covers more conscious actions, and is actually the type of behavior people try to change the most.

A typical bad common behavior is not getting enough exercise, and the most useful technique to remedy this is to surround yourself with a supportive community of people that share your goals. This way, with a group of like-minded friends, you'll have people who can both motivate you and hold you accountable for meeting your goals.

Well, there you have it, some proven and reliable techniques for sticking with the objectives you set for yourself. According to the author, those who adopt these strategies generally experience three times the success rate of those who don't!

### 10. Final summary 

The key message in this book:

**Everyone wants to achieve their goals and break their bad habits. The good news is, there is a growing body of research that is finding ways to help people achieve their goal. So remember, you'll be far more likely to reach your goals by breaking them down into smaller, achievable steps. You'll also be more likely to stay motivated if you're surrounded by a supportive community that will hold you accountable to what you have committed to achieve. By using neurohacks and keeping the predictable tendencies of human nature in mind, you'll improve your chances for success.**

Actionable advice:

**Take an improv or jazz class.**

Acting out scenes spontaneously requires you to adapt to a number of different situations, which works as a neurohack to train your mind to handle changes more effectively. And if you don't feel comfortable acting, you may prefer to take jazz lessons. Jazz music is often structured around improvisation, and helps the mind stay in the present moment.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading: _Barking Up the Wrong Tree_ by ****Eric Barker**

_Barking Up the Wrong Tree_ (2017) explores the divide between the extremely successful and the rest of the pack. These blinks draw on science, statistics and surprising anecdotes to explain the factors that determine success — and how almost anyone can attain it.
---

### Sean D. Young

Sean D. Young, PhD, is a professor at the Department of Family Medicine at the University of California, Los Angeles, and the director of the UCLA Center for Digital Behavior.

