---
id: 543e2d546363320008430000
slug: stiff-en
published_date: 2014-10-16T00:15:00.000+00:00
author: Mary Roach
title: Stiff
subtitle: The Curious Lives of Human Cadavers
main_color: None
text_color: None
---

# Stiff

_The Curious Lives of Human Cadavers_

**Mary Roach**

This book will take you on a fascinating journey, traversing life, death and their significance in human history from ancient times to today. Through extensive and hands-on research, Roach has created an exploration of our bodies that is remarkable for its insight and clarity. Through a discussion of highly relevant issues including organ donation, Roach may change your perspective on life, and death too.

---
### 1. What’s in it for me? Find out what happens to you when you die. 

Like it or not, death is something almost all of us fear. Perhaps it is because we know nothing of what happens after death, and this ignorance terrifies us.

Luckily, these blinks can help cure you of this fear by providing a perspective on living and dying that you've probably never considered.   

You'll learn about the history of the hunt for the soul, the changing definition of death, how people were buried alive in the past, the use of human body parts in medical practices and research by the Church, the dirty history of body-snatching, and why some students at the University of California now call their cadavers by name.

You'll discover that your body can have many different adventures after you've died, from a career in landmine equipment testing to a practice patient for facelifts.

In the following blinks, you'll also learn:

  * why some doctors would pull on the tongues of dead bodies for hours after their death;

  * how one scientist made gruesome attempts to prove that the blood on the _Shroud of Turin_ was real and

  * how your dead body can save eight people.

### 2. When we die, our soul departs, while our body remains as an empty shell. 

What happens to us when we die? This question is both fascinating and unanswerable, as the deceased have no way of sharing their experiences with us.

In simple terms, when we die, our _soul_ leaves us. Let's use this word not in a religious sense, but simply to represent the characteristics that make us human and individual. These are the characteristics — thoughts, emotions, expressions — that leave us at death. Our body, however, remains behind. Can a body give us any clues about where the human who once occupied it has gone?

Unfortunately not! When we think about it, the cadaver of the deceased is no longer human. A human being is a living soul inside a living body. When we die, the soul leaves and the body remains; without the soul, the body is just the shell of a _former_ human.

This makes sense when we think about the way we mourn for deceased loved ones. We grieve for the people they once were, and not the mere bodies they previously inhabited. The author provides us with a poignant personal example.

After her mother died, the author and her brother sat by the body for an hour before the funeral commenced. Both agreed that they felt uncomfortable around the cadaver of their mother, because it was _not_ their mother. Although their mother had once inhabited it, after her soul departed at death, the cadaver felt more like a foreign object.

This example reflects that the soul and the body after death must be thought of separately. The soul departs to an afterlife, if you believe in one, or nothingness. The body remains, but it is no longer human. A human is comprised of a soul _and_ a body, and it is only with this understanding in mind that we can explore these concepts further in the following blinks.

> _"Death. It doesn't have to be boring."_

### 3. For millennia we’ve been asking the same question: where does the soul reside? 

Where is our soul located? Take a guess. Some might say the brain, the heart, or maybe even the liver, as ancient religions speculated. Or maybe the soul doesn't inhabit a single organ, and instead uses the whole body as a vessel.

This discussion about the soul's location and existence has been ongoing for millennia. Ancient Egyptians felt sure that the soul lived in the heart, whereas Babylonians were convinced it resided in the liver. Meanwhile, Mesopotamians argued that the soul was split between the two.

Modern scientists attempted to answer the question by weighing dying people in their very last moments. They actually found that the body became slightly lighter once the heart stopped beating! It must be said that this could well be due to factors like the evaporation of sweat, inaccurate measuring equipment or a combination of other factors.

This experiment is just one example of attempts to uncover the soul by scientists whose curiosity led them to be terribly cruel. In the nineteenth century, scientists decapitated criminals and attempted to revive their dismembered heads by administering dog blood transfusions to the brain or asking them questions.

These inhumane experiments appeared successful, as the heads reportedly showed some kind of awareness of what was happening, and in some cases, reacting to their name called. This was in fact because brain cells, when cut off from blood circulation, remain alive for six to ten minutes, though scientists at the time were inclined to believe that it was because the soul resided in the brain.

While the question of souls has fascinated scientists and cultures around the world throughout history, we are still no closer to proving the soul's existence.

> Fact: The human brain can survive for six to ten minutes even after the heart stops beating.

### 4. Death has often been worryingly misdiagnosed – a stopped heart doesn’t necessarily mean a dead body! 

When are we truly dead? Most would answer when our heart stops beating. But what about those who have undergone resuscitation and survived? They're certainly not zombies! However, before knowledge of heart resuscitation, many people were mistakenly pronounced dead.

Previous centuries provide us with harrowing tales of people being buried alive or waking up hours after being declared dead. Doctors didn't realize these bodies were still alive because their pulse was too low to be monitored.

To solve this problem, doctors sought alternative methods to ensure that a person was definitely dead, before taking them to the morgue. French physician Jean Baptiste developed the bizarre technique of pulling on their tongues for hours, while a more horrifying approach promoted the insertion of scorching hot metal rods inside the anus.

Even with these procedures, mistakes were still prevalent, so much so that the 1800s saw the development of waiting mortuaries, where bodies were left for 48 hours to ensure they were really dead.

Eventually doctors learned how to restart the heart, and the definition of death shifted to the ceasing of brain activity. It was agreed that a person was dead when his or her brain was dead. This definition of death as established in 1974 was preceded by a controversial legal case:

A man who was shot in the head was declared brain dead, while his heart remained functional and was donated to a medical patient. Andrew Lyons, who had been charged with the shooting, attempted to claim it wasn't he who was the murderer, but the surgeon who switched off the man's life support.

While this case did not hold up in court, it reflects that the way we define death has many implications for society. It is vital that this definition is continuously re-evaluated as medical technology progresses.

Though the soul escapes in death, the body may live on in varying ways. In the next blink, we'll learn what happened to these bodies in the past.

### 5. Human bodies have been used for various practices and experiments for thousands of years. 

Did you know that in the past, human body parts were used for medical treatments? Believe it or not, the penis was particularly sought after for its healing qualities! The ways in which the human body was used don't end here, either.

Doctors, anatomists and scientists all share the goal of understanding the human form. How does our blood circulate? Why do we get sick? How can we treat our illnesses?

These are all questions that you couldn't resolve by simply cutting open and examining a canine. Animal systems share some similarities to ours, but can only give us limited knowledge. What anatomists of the past really needed were human cadavers.

And human cadavers were not only used for the justifiable purpose of medical advancement. The research conducted by Dr. Pierre Barbet gives us one of the strangest examples of human cadaver use, all in the name of religion.

In 1931, he was asked to investigate whether the blood of Christ that was splashed on the _Shroud of Turin_ was genuine. Barbet went to shocking lengths in his investigation, nailing cadavers and recently amputated arms onto wooden crosses and observing the way the blood spattered. Naturally, it was not advisable for young men with arm aches to visit the doctor's office!

This example reflects a widespread misuse of human cadavers in history. Nevertheless, we have gained great knowledge from studying them.

### 6. Anatomists of the past did just about anything to get their hands on a cadaver. 

Our current understanding of human anatomy is sophisticated, but unfortunately, scientists didn't achieve it by simply observing humans. The path of anatomical study has been difficult and those who wanted to learn more often resorted to treachery.

Culture and religion have often hindered scientific development in the past. Anatomists desperately needed the opportunity to examine real human bodies in order to solve medical problems. However, in societies where religion was central, the dissecting of human bodies was forbidden.

For example, Christian doctrine dictated that upon the last day of judgement, you would stand before God appearing in the way that your body was when buried. With brains or stomach sliced open or limbs missing, your chances of entering paradise were reduced.

Nevertheless, scientists needed cadavers, and were willing to go to astounding lengths to acquire them without the knowledge of the Church. This demand was the beginning of the career of _body-snatchers_.

Anatomists told gangs of criminals what types of bodies they needed for their studies — a young pregnant mother or an elderly man. These body-snatchers would then go and dig up these bodies from their graves, flagrantly disregarding the beliefs and wishes of the deceased and their families. Body-snatching became so prevalent that the wealthy often had their coffins protected with metal cages.

> Fact: One gang of body-snatchers actually murdered 14 or more people in order to provide anatomists with fresh cadavers.

### 7. Today, cadavers are fortunately treated with more care and respect. 

All these tales of the mistreatment of cadavers in the past might make you wonder why human cadavers should be donated to medicine today. Times have changed, however, and scientists now are educated to treat cadavers with respect.

Most students of medicine or biology will at some point take an anatomy class in order to practice their surgical skills on cadavers. Though it is tempting to objectify the bodies in order to cope with our anxieties of facing death, these students understand that the bodies previously belonged to another human and as such, to not treat them as mere objects.

The students at the University of California, for example, often learn about the history of the cadaver's donor, thank the body afterwards or even refer to the cadaver by name.

This respect continues in the careers of scientists, who also have to use cadavers in their work. Even the most skilled surgeons must physically practice their procedures.

For example, your aunt's face-lift next week won't go so well if it's the first time for a cosmetic surgeon. It's better that her surgeon practice beforehand, preferably on something that won't feel pain or suffer if the procedure goes wrong.

In fact, highly qualified surgeons frequently take part in training events, in order to learn and attempt new methods in medicine safely, with the use of cadavers. In these events, the respect for the cadaver is also clear — never would a surgeon take the head home in a bucket or cut off random parts for fun!

The respectful use of cadavers today is in stark contrast to the approach taken in previous times. This respect will be carried forward in future medicine, and allow the further advancement of anatomical knowledge.

### 8. Dead bodies can help us save lives and solve crimes. 

You might think that death is the end of the road. Well, we don't know about the soul, but your body can have many adventures after you leave it! Let's examine a few possibilities.

Being a crash-test-dummy might not be the ideal career for you while you're alive, but your cadaver can in fact do great things for the car safety industry. Safety mechanisms in our cars that we take for granted, such as three-point seat belts and airbags, in fact were developed through extensive testing on cadavers.

While some research can be conducted with dummies built from synthetic material, accurate studies of impact damage on bones and muscles requires a human cadaver. Previously, car developers and fanatics offered their own bodies as living test subjects, albeit only until their pain threshold was reached. Human cadavers provided a more effective and less dangerous solution for testing.

Here are some statistics to demonstrate their success: for every cadaver used in the testing of the three-point seatbelt, 61 lives are saved in the US per year; the cadavers used in airbag testing save a whopping 147 lives on average annually. It's astonishing how your body can be a lifesaver even after your last breath.

The auto industry isn't the only place where your body can achieve great things after you die. The parts that you no longer require can be donated to those in need. Your body could assist testing boots for landmine removers, who you can bet will be incredibly grateful for footwear that prevents their feet from being torn off.

Or, your body could bring justice to murder cases, by helping medical examiners improve their knowledge of how bodies decompose or what the impact of a bomb detonated close by is.

Each of these possibilities demonstrates the diverse adventures your cadaver can experience after death, some experiences too adventurous to partake in during life.

### 9. Donating your body and organs can make a dramatic difference in others lives. 

Everyone knows that we should sign organ donor cards. And yet, despite the thousands of lives that could be saved, there are still far too few organ donors willing to assist those in dire need.

Giving up our organs may be disconcerting. People from all walks of life unfortunately suffer head injuries causing brain death, which is a tragic event in itself. To then consider the difficult decision of organ donation is incredibly tough, as their bodies will only function for a few more days before organ failure sets in.

While the thought of removing the organs from a human whose heart still beats causes us to flinch, it is vital that we overcome this fear and accept that the body is now an empty shell. With this in mind, we should allow the body's use to end the suffering of those with medical conditions.

For example, the author describes her viewing of an organ transplant procedure during her research. Watching organs being removed from a brain-dead woman she calls H, while monitors beep and blood continues to pump in her body, was a surreal experience for the author. Yet, she recognized that this woman would never wake up again.

H was able to save three suffering patients and extend their lives after she had left hers. Some organ donors can even save up to eight lives. And we need far more people to learn from H's example, as currently there are more than 80,000 people in desperate need of a donation, 16 of which die daily as they wait.

Despite this need, over half of the families of brain-dead patients decide against organ donation. Even if you may not personally wish to donate your organs, discuss the topic with those close to you. Maybe you'll help someone you know come to the decision to donate.

> _"Cadavers like H are the dead's heroes."_

### 10. Final summary 

The key message in this book:

**Death is not something to fear, rather something that we should spend time thinking about. While the question of the existence of souls has been discussed for millennia, there is still no conclusive answer. What we do know, however, is that our bodies can be incredibly valuable, even after we've departed the world of the living.**

Actionable advice:

**Take the time to consider volunteering as an organ donor.**

While organ donation is a fantastic way to save lives, there are far too few registered organ donors, while thousands of people waiting for transplants suffer. Don't be afraid to explore this issue with your family and loved ones. Perhaps you or they will decide to become an organ donor. It can be truly astonishing, the way your body can give to others long after your soul has departed.

**Suggested** **further** **reading:** ** _The Poisoner's Handbook_** **by Deborah Blum**

_The Poisoner's Handbook_ details the work of New York City's first scientifically trained medical examiner, Charles Norris, and his partner, Alexander Gettler, the city's first toxicologist. It offers an insider's view of how forensic science really works by walking readers through their investigations into notorious and mysterious poisonings.
---

### Mary Roach

Mary Roach has written for a diverse range of publications including _Salon, Wired, GQ, Discovery, Vogue_ and _the New York Times Magazine_. Her work as an author in the field of popular science reflects a diverse range of interests and a talent for making the wonder of science accessible to readers.

