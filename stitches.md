---
id: 5891e44c78a78e0004051aab
slug: stitches-en
published_date: 2017-02-03T00:00:00.000+00:00
author: Anne Lamott
title: Stitches
subtitle: A Handbook on Meaning, Hope and Repair
main_color: 3E7638
text_color: 3E7638
---

# Stitches

_A Handbook on Meaning, Hope and Repair_

**Anne Lamott**

_Stitches_ (2014) is about embracing the negative aspects of life that you're powerless to change, and building a community to help you work through them. These blinks explain why so many people run from suffering and pain, and why acknowledging such difficult experiences is the only way to overcome them.

---
### 1. What’s in it for me? Find a greater sense of meaning in life, even during the toughest of times. 

It's inevitable that human beings will have painful and traumatic experiences as they go through life. When these difficult episodes arise, what is it that allows people to keep their heads up and continue?

The stitches that hold all of our experiences together are the communities we find solace in. The answer, if there is one, to the eternal question of life's true meaning surely lies in togetherness and love.

And that's what you'll learn in these blinks. They're full of great insights about the feeling of meaning in life, and why it can be so hard to find.

In these blinks, you'll also discover

  * why you shouldn't push yourself to "get over" something;

  * why you shouldn't feel bad if someone says you're too sensitive; and

  * why you feel so bad when hearing about a tragic loss — even if you have no connection to it.

### 2. It’s easy to find meaning in life when everything is going well, but it can be tricky when the going gets tough. 

Isn't it amazing when life just seems to work out? You and your family are healthy, you have a job you love and everything feels like it's in the right place.

It's common to feel that life is in order after experiencing good fortune, and this tendency has to do with the human ability to find sense, meaning or purpose in a string of positive events. Simply put, it's easier to see meaning in life when things work out for us.

This tends to be the result of expecting things to turn out a particular way, while failing to see chaos as an inherent aspect of life's overall meaning. Instead, we find meaning in the things we purposefully intend to do, like finding a job we love. If we achieve these things, that sense of meaning is naturally reinforced.

Because of this positive reinforcement, rather than seeing the mess and chaos of our lives as meaningful, we quarantine them, associating them with negativity. The same can be said for stigma and failure, which people tend to associate with painful, difficult or unplanned experiences.

So, given this tendency, it should come as no surprise that, when we experience suffering or difficult moments, we find it hard to feel positive about life or think of it as meaningful. Just consider catastrophic events like terrorist attacks, natural disasters or the loss of a loved one. In painful moments it's hard to believe that life has any positive meaning at all.

In such instances, it doesn't even matter if the event happens to us or to another person. The painful experiences of one person are still part of the human community and, in that sense, all people are affected by the negative things that happen to all others.

In other words, we're all joined together by bad experiences. For instance, if someone murders another human, the rest of society is partially responsible, since they live in the same human community as that person.

But this realization doesn't have to be a total downer — there's actually a lot to gain from these negative experiences and, in the blinks that follow, you'll learn about how to cope with life's struggles.

> _"How can there be more meaning than helping one another stay warm and stand up in the wind."_

### 3. Life is a hodgepodge of good and bad experiences, and the latter have a lot to teach us. 

Nobody only learns from quick, easy and painless events. But neither do they look forward to the dark times. In reality, life is a jumble of good and bad experiences, all of which have something to teach us.

In this way, life is a lot like an eclectic patchwork quilt, composed of innumerable different moments. It has holes that have been patched up, repeated sections and, for the most part, it's improvised. The only thing we can do is take it one stitch at a time.

But people tend to hold onto the idea that everything in their lives should fit perfectly together in a neat, presentable package. They expect that they'll finish high school, maybe go to university, find a career and build a family.

In other words, they're stuck on a linear, step-by-step ladder. As a result, people don't plan and aren't encouraged to think about how, at some point, things will likely go off course. This is astonishing, since people around the world get laid off, lose loved ones or experience natural disasters every day.

Such negative events are simply a fact of life — but if we accept them as inevitable, we can actually learn a lot from them. These experiences shape us and are fundamental contributing elements to who we are.

So, in a way, we can never get over our greatest losses, as the pain they spur will forever shape us. But that's not necessarily a negative thing.

For instance, instead of trying to "get over" losing her best friend Pammy, the author held onto a shirt Pammy had given her shortly before she died. Wearing the shirt kept her connected to Pammy, while acknowledging the pain she felt from losing her. By honoring rather than suppressing the experience, Lamott was able to see beyond the pain of losing someone dear to her.

We've seen how we can't trust or cling to our expectations of having linear, straightforward lives. But where do we get these expectations from in the first place?

> _"Sometimes we feel that we are barely pulling ourselves forward [. . .] but we do come out the other side, exhausted and changed."_

### 4. Societal pressures often discourage sensitivity, causing people to downplay difficult and painful experiences. 

From early on in our lives, the pressure is on for things to go right. Whether it's our parents, teachers or even our bosses, we're pushed to follow certain paths.

But the truth is, things never work out as planned. So, why do we cling to this illusion of an unobstructed, painless ride?

Because we're inundated with external pressures that tend to discourage sensitivity. As a result, we're generally not taught how to engage with the fact that life can be difficult.

Just take a term like "overly sensitive child." It implies that, even as children, people are criticized for engaging with the pain of existence — and that a tendency to do so is seen as disruptive or unhelpful.

Especially as people grow up and make their way through the education system, a tremendous amount of pressure is placed on achievement, which harms sensitivity of every kind. At the same time, the stereotypically American obsession with money and status inhibits our awareness of this fact and interrupts our ability to see what truly matters.

It's this same attitude that has created an American culture that frowns upon asking for help. As a result, the process of working through difficult experiences becomes stigmatized and people are further isolated.

So, what are the effects of this emotional dulling?

Well, if sensitivity is discouraged, people ultimately end up denying or minimizing the painful experiences in their lives. This is a huge problem because, as we now know, everyone experiences suffering at some point and, by denying it, we're simply suppressing the emotions such events produce.

Just take children who grow up with alcoholic parents. Their parents tend to insist that they have vivid imaginations and that the horrible things they describe haven't actually occurred. By denying the truth in this way, the problems are left unresolved, ultimately compromising the children's emotional well-being.

### 5. Being together with others is a profound experience that gives us a sense of meaning. 

Do you have one really amazing friend who you can tell anything? Or do you have a person in your life who you couldn't imagine living without?

Well, if you don't, you might want to start building such a relationship. These types of close connections are powerful and, in general, something profound happens when people come together.

For instance, when we're on our own, a single, simple task can feel like an immense responsibility. But when that same task is done with others, the load is not only shared, we're also capable of doing much more.

Just consider a person who is going through a painful experience. Simply by sitting with them, passing the time together and being present, another person can ease their pain. This gesture takes very little from us, but it fundamentally alters the other person's experience of suffering.

Or take the community that can be built in the aftermath of a natural disaster. When people embrace such cataclysmic events together, the sense of community and compassion that forms during these hardest of times can bring people closer together.

We also find meaning in communities, as our links to those around us help support us. Just imagine being a part of a community that's working toward a shared goal. Wouldn't it feel better than sitting alone at home, working on the same project by yourself?

And finally, when people feel that they're part of a community, they become more likely to open up about their pain and suffering. As a result, they build more open and honest relationships that only add to the feeling of togetherness and, therefore, meaning.

For example, the community built around women's movements has inspired countless women to speak up and free themselves of everything they had been taught by a sexist, misogynistic society. Because of this togetherness, the movement grew ever larger, as people were drawn in by an increasing sense of community.

> _"When we agree to (or get tricked into) being part of something bigger than our own wired, fixated minds, we are saved."_

### 6. Final summary 

**The key message in this book:**

There's not a person alive who hasn't questioned what life means or what their purpose is on the planet. When our lives are off-balance, it's difficult to see any meaning at all in this crazy world. But by coming together and being there for each other, we can give life meaning while supporting one another.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Bird by Bird_** **by Anne Lamott**

Told from the personal perspective of author Anne Lamott, _Bird_ _by_ _Bird_ is a guide toward becoming a better writer and improving your life along the way. Lamott's distinctive approach, honesty and personal anecdotes make this book a must for writers or anyone who wants to become one.
---

### Anne Lamott

Anne Lamott is the author of the _New York Times_ bestseller _Help, Thanks, Wow_. Her writing offers lessons and ideas, learned through personal experiences, that focus on being happier, more loving and kinder to those around us.

