---
id: 584ab67166f3e20004cb68dc
slug: stoned-en
published_date: 2016-12-29T00:00:00.000+00:00
author: Aja Raden
title: Stoned
subtitle: Jewelry, Obsession, and How Desire Shapes the World
main_color: 58A58E
text_color: 3D7363
---

# Stoned

_Jewelry, Obsession, and How Desire Shapes the World_

**Aja Raden**

_Stoned_ (2015) is a collection of historical stories told through the lens of human desire — and the lengths to which we'll go in pursuit of that desire. These blinks take you around the world and through time, showing how our desire for beautiful objects can move mountains and why our valuation of those objects can change so easily.

---
### 1. What’s in it for me? Understand the cravings of humankind. 

Precious stones and gems are among the most valuable things today. Indeed, it's been that way for most of human history. In the past, gold and silver, along with trinkets like glass beads, were used in huge transactions. But why is it that some things that were once of extreme value are essentially worthless today, while the value of others has increased immensely?

Let's delve into the history of human luxury and try to understand what makes value wax and wane. We'll explore the sale of flower bulbs and land, of pearls and diamonds and, in the process, try to understand why some things make us go dizzy with desire. 

In these blinks, you'll find out

  * how one particular stone changed world history;

  * what pearls can tell us about value; and

  * why today's most expensive real estate was once dirt cheap.

### 2. People’s pursuit of desire has helped shape world history. 

What quality do you think motivates people the most? The Greek philosopher Plato once said, "Human behavior flows from three main sources: desire, emotion, and knowledge."

Plato was probably right to mention desire first. Indeed, many figures throughout history, desiring some precious object, have taken drastic action to obtain it.

One of the most famous objects is La Peregrina, a perfectly pear-shaped white pearl that's so big it fits snugly in the palm of an adult hand. The name translates to "the wanderer" or "the pilgrim." The name is apt, for it has a long history of finding its way into the hands of many influential people around the world. In recent history, it was owned by Elizabeth Taylor. In 1969, the actress received the pearl as a Valentine's Day gift from her husband, Richard Burton.

This gift is even more impressive when you consider that, 400 years earlier, La Peregrina was changing world history. It all started in the sixteenth century, when Philip II of Spain gave the pearl to his betrothed, England's Queen Mary I. She adored La Peregrina so much that it can be seen in almost all portraits of her. But someone else had also developed a desire for the pearl: Mary's sister, Elizabeth I. 

When Elizabeth eventually became Queen of England following Mary's death in 1558, Philip II offered her the pearl along with his own hand in marriage. Elizabeth, determined to rule England on her own, refused the proposal.

Philip returned to Spain, taking the prized pearl with him. But Elizabeth's desire burned on, so she passed a new law that allowed English privateers — basically sanctioned pirates — to plunder Spanish ships in search of La Peregrina.

Philip was not happy about this, and, in 1588, he raised a Spanish armada to invade England and overthrow Queen Elizabeth, who Pope Pius V had called "the patroness of heretics and pirates." England devastated the Spanish Armada, however, marking the end of Spain's naval dominance and paving the way for England's global commercial empire.

### 3. Our pursuit of the things we desire is based on our perception, and it can affect us both physically and economically. 

How much money would you spend on one tulip? Even if you were fantastically passionate about flowers, you probably wouldn't bankrupt yourself buying them.

Yet this is what happened in the Netherlands when tulip mania struck and destroyed the Dutch economy during the 1630s.

After being introduced to Europe from Turkey in 1559, tulips became hugely desirable. By 1630, any respectable Dutch person had to have at least a modest tulip garden. As the demand increased, tulip auctions began being held. At one point, a single bulb could cost more than some people's homes. The most expensive tulip ever sold went for the price of twelve acres of prime real estate!

But then, in 1636, the bubble burst. Suddenly, during one high-profile tulip auction, no one wanted to buy. The public panicked. Many people had written agreements to buy or sell the flowers, but now that the public perception had changed, the value of tulips was in freefall, and no one wanted to honor their contracts.

As a result, the economy collapsed and, within two months, half of Holland was reduced to poverty.

This shows us just how quickly our perception of value can shift. Often, the abundance or rarity of something is what determines its desirability.

According to a study on the neurological effects of supply and demand, the less there is of something, the more appealing it becomes. During the study, subjects were repeatedly presented with two kinds of cookies, and the participants always perceived the ones of which there were fewer as being more valuable. Even more interesting is that the subjects had a _super_ valuable perception of the cookies that started out as abundant, but then gradually became scarce. This led the participants to believe that these were the cookies that everyone else desired.

Other studies reveal how desire can affect us physically. For example, when something we want becomes harder to get, we become agitated. Our blood pressure rises, we're unable to focus on anything else and, with a brain clouded by desire and jealousy, we lose the ability to properly assess the situation.

### 4. The history of the pearl market shows how scarcity can change our perception of value. 

We've seen how even the number of cookies on a plate can affect our perception. But what happens in the real world of precious goods on a global scale? 

When we look at the development of cultured pearls in Japan, we can see how value can be affected by a combination of both real and perceived scarcity.

The technique of cultured pearls was invented in 1893 by Kokichi Mikimoto, the son of a poor noodle maker. The process involves planting a particle of nacre, or mother of pearl, inside the shell and patiently waiting at least four years for the pearl to form.

Mikimoto's achievement didn't go unnoticed. In 1927, he met his idol, the inventor Thomas Edison, who told him that his technique was "one of the wonders of the world" since culturing pearls had been widely regarded as biologically impossible.

But not everyone was as enthusiastic as Edison. When that meeting took place, Japan was the world's biggest pearl exporter, shipping out millions of perfect pearls and making the traditional pearl sellers very unhappy.

After all, the number of natural pearls found every year ranged between a dozen and a few hundred; Japan, during its peak year of 1938, produced ten million cultured pearls. Plus, with so many pearls flooding the market, their value was quickly depreciating.

Western companies started trying their best to de-value Mikimoto's pearls (and increase the value of their own) by claiming that they weren't "real" since they were formed using an unnatural process. But Mikimoto won in the end. Today, cultured pearls are widely available and affordable, and only collectors pursue the "natural" pearls.

Mikimoto persevered by being a master salesman and knowing how to use marketing to help convince the world that his pearls were superior. He even publicly burned any imperfect pearls that he produced to ensure customers that his company sold nothing but the most perfect pearls!

Today, Mikimoto's company remains one of the world's leading pearl companies.

> _"Advertising is legalized lying." –_ H. G. Wells

### 5. Marketing has a huge influence on how we perceive value. 

Chances are you've heard the famous slogan, "A diamond is forever." But less familiar is the fascinating story of how this effective marketing catchphrase came to be.

It's easy to think that the idea of a diamond engagement ring has been around as long as the idea of marriage itself; but, in reality, diamonds have only been considered a "necessary luxury" for about 80 years. To put this in perspective, that's about as old as the microwave oven.

Much of the value we place on diamonds today can be credited to the ingenious marketing techniques of the famous diamond company, De Beers.

Back in 1882, the diamond market was in trouble. There were simply too many diamonds on the market, and the industry was on the verge of collapse.

As a response, De Beers eventually ceased one-third of its production in order to slow things down and give people the perception that diamonds were still rare objects. But since this wasn't a viable long-term strategy, they had to come up with another way to make diamonds seem desirable.

It was De Beers chairman, Nicky Oppenheimer, who came up with the idea to create the illusion that diamonds were _necessary_.

Oppenheimer worked with the influential advertising agency N. W. Ayer to come up with the manipulative marketing strategy of selling the _idea_ of diamonds.

This is how we arrive at N. W. Ayer's iconic 1947 campaign that featured the slogan, "A diamond is forever," and other concepts like, "A proposal is not a real proposal without a diamond," and, "What's two months' salary for something that'll last forever?"

These ads performed well with their intended target of young ladies with romantic dreams of marriage and true love.

Plus, De Beers made sure movie stars and celebrities were wearing diamonds during public appearances to help create the illusion that diamonds went hand in hand with fame and glamor. Diamonds are still considered a rare object and a status symbol, even though, thanks to expert marketing, these perceived values exist only in the mind of the consumer.

> _"Diamonds are intrinsically worthless, except for the deep psychological need they fill."_ — Nicky Oppenheimer

### 6. The value of something and our desire for it changes over time. 

As we've seen with pearls and tulips, the value of something can depreciate dramatically for a variety of reasons. But it's also possible for the value of something to reach unimaginable heights. Just consider the story of Manhattan.

The island of Manhattan was first sold in 1626. A Dutchman named Peter Minuit purchased it from the Lenape Indians for $24-worth of glass beads, buttons and trinkets.

It might seem ridiculous now, since even a one-bedroom apartment in Manhattan can cost a small fortune, but, at the time, both parties were happy with the sale.

Back then, the 23-square-mile island had few resources and hardly anything to make it seem desirable.

Even the Lenape Indians didn't think much of it. Their name for the island was _manahachtanienk_, which translates into "place where we all got drunk," and they only stopped by to fish and pick oysters.

And since no one from the tribe lived there, selling it to some Dutch people for what the Indians saw as rare, exotic jewels was seen as a fair trade.

In the New World, glass beads had never been seen before since glass-making technology had yet to arrive. So the beads became a currency in the sixteenth and seventeenth centuries; they were easy to transport, and since they were rare everywhere except Western Europe, they had perceived value.

The inflation of Manhattan's value combines much of what we've covered so far.

Since there is so little space on the small island, people were forced to build upward. This ultimately resulted in a beautiful skyline — and since available real estate remains so rare, it continues to be perceived as an extremely valuable place to live.

Of course, Minuit had no idea just how vital the island would become. And the fact that we think of it as such an unfair deal now should remind us of the many ways our perspective can — and, as time passes, certainly will — change.

> _"One man's trash is another man's treasure." –_ Old proverb

### 7. Final summary 

The key message in this book:

**Beauty is a powerful motivator, and it has channeled the course of history from figures like Queen Elizabeth to the sale of Manhattan. The value of beautiful objects is far from constant, however, and it is influenced by many different factors, such as marketing and supply and demand.**

Actionable advice

**Compare value and cost.**

Next time you're coveting a diamond ring, whether for yourself or for a loved one, consider how much of that desire has been manipulated by advertising and marketing. Is that $10,000 ring really more valuable to you than a $100 ring from a vintage store? If so, what makes the difference? Consider such questions carefully before you take the plunge!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading: _Queen of Fashion_** ** __****by Caroline Weber**

_Queen of Fashion_ (2006) reveals the untold ways in which Marie Antoinette, with her iconoclastic sense of fashion and her rebellious behavior, challenged the status quo of the eighteenth-century French court. Her daring originality was a way for her to share her voice and personality, and her story tells us a great deal about the revolutionary politics that can be found in the history of both fashion and France.
---

### Aja Raden

Aja Raden is uniquely trained as a jeweler, a scientist and a historian. She studied at the University of Chicago and has worked as the head of the auction division at House of Kahn Estate Jewelers and as a senior designer for Tacori, a fine-jewelry company in Los Angeles.

