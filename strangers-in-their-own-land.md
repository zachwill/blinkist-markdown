---
id: 5a1b3fd8b238e10006ebb481
slug: strangers-in-their-own-land-en
published_date: 2017-11-28T00:00:00.000+00:00
author: Arlie Russell Hochschild
title: Strangers in Their Own Land
subtitle: Anger and Mourning on the American Right
main_color: 736A4D
text_color: 736A4D
---

# Strangers in Their Own Land

_Anger and Mourning on the American Right_

**Arlie Russell Hochschild**

_Strangers in Their Own Land_ (2016) discusses the issues that divide American politics, with specific focus on the Tea Party of Louisiana. In the course of explaining how Louisiana ended up where it is today, the author encourages readers to empathize with disparate political stances.

---
### 1. What’s in it for me? Learn how politics is failing the forgotten people of Louisiana. 

Louisiana is one of the poorest states in the Union. Yet the people of Louisiana have recently voted in a succession of Republican state representatives who have arguably made them even poorer. This paradox inspired sociologist Arlie Hochschild to go and see for herself how the state got into its current predicament.

During her time there, Hochschild met with the people of Louisiana and listened to both their grievances and their hopes. She learned that the state faces some serious problems, from an abnormal spike in cancer cases due to increased pollution created by big oil companies to the economic stagnation of the white working class. She also learned that the best way forward is to empathize with those whose politics don't jibe with yours.

In these blinks, you'll learn

  * why Republican governor Bobby Jindal gave away 1.6 billion dollars to oil and gas corporations;

  * what the Tea Party is; and

  * how Fox News drives the political debate in Louisiana.

### 2. The supporters of the Tea Party in Louisiana seem to be behaving paradoxically. 

You'd think that the poorer American states would advocate for government aid. After all, if you need help, and help is available, why wouldn't you try to get it? But, paradoxically enough, Louisiana, one of the poorest states in the nation, seems to be _against_ such aid.

This may well be because a large number of Louisiana's citizens support the Tea Party, a politically conservative movement within the Republican Party that favors the free market, supports big businesses and desires a small government which exercises little regulation.

A majority of the citizens of Louisiana evidently support these ideologies. They elected Bobby Jindal to the position of state governor in both 2007 and 2011, and Jindal reduced the number of regulations in place for the large chemical and oil companies operating within the state. He purposely lowered the region's pollution standards so that the companies would meet the requirements, even though these lowered standards were far below the national average. In addition, he offered cash incentives worth billions of dollars to oil companies, encouraging them to relocate to Louisiana.

As for Louisianans' desire for a smaller government, they believe that the state already has too much power. They believe that the state "steals" their tax dollars, passing the money on to people who, rather than struggling with unemployment, choose to be lazy and accept government handouts. A majority of Louisianans think that if the government were reduced in size and influence this wouldn't be the case.

Ironically, this overt lack of regulatory policies and reduced governmental involvement has led to Louisiana being one of the worst-off states in America. A year after Jindal was first elected, Louisiana was ranked number 49 out of 50 in The Measure of America. The ranking assessed life expectancy, the number of people with educational degrees, median personal income and school-enrollment levels.

The people of Louisiana believe that the few regulations still in place are the root of their ills. So why do they think this and continue to vote along conservative lines? In the following blinks, you'll find out.

### 3. Oil is highly overestimated as a benefit to Louisiana. 

Louisianan politicians often talk about the positive impact the billion-dollar oil industry has on the state. But such praise is mere rhetoric. In fact, big oil has been exceedingly detrimental.

When an oil company plans to open a plant near the homes of Tea Party supporters, the prospects are often shared with residents. They're told that the construction will provide them with steady jobs, but, in reality, most of these positions go to professionals who relocate there from outside the region. Workers from poorer countries — willing to accept much lower pay rates than American citizens — are also often recruited to work on these plants.

The pollution produced by these plants is also a major issue. Waterways across Louisiana have been poisoned by effluent, the result being that many fishermen are now out of work.

Meanwhile, the state offers oil plants huge tax incentives, while the benefits realized by the people of Louisiana don't outweigh the amount of tax revenue lost.

For companies that relocate to Louisiana, Governor Jindal proposed ten years of tax-free business. After those ten years are up, these businesses are permitted to simply change their names, giving them an additional ten years of tax-free operation. This policy has cost the state $1.6 billion in lost tax revenue — money the state desperately needs.

Oil companies are not only drawn to Louisiana's rich supply of natural resources; its populace also makes the state a prime location for such business.

An investigation made on behalf of one oil company found that certain towns possess qualities that make them ideal — for oil companies, that is. Long-term residents who haven't received education beyond high school, devout Christians working in the farming or mining industries — towns full of people who fit these descriptions are perfect. And Louisiana is full of such towns, which is probably why oil companies have settled in the state.

### 4. The Tea Party supporters feel neglected by the federal government. 

Underpinning the attitude of Tea Party members is a shared belief that they're hardworking individuals who aren't justly rewarded for their efforts. Collectively, they're looking for someone to blame — but their ire is often misplaced.

Much of the discontent stems from issues relating to race, class and gender. The majority of Tea Party supporters are white men who're also blue-collar workers. Essentially, they feel that they're the backbone of America — that, thanks to their hard work, the country is what it is today. However, they feel that they're barred from the "American Dream," whereas minorities are constantly given assistance, such as affirmative action.

They regard Obama's ascendence to the presidency as a prime example of preferential treatment. This take on Obama was championed by Fox News, which both encouraged its viewers to distrust him and routinely linked his political success to a system poisoned by affirmative action and political correctness.

This shared attitude has given rise to older, white, lower-middle-class men believing that they are a minority requiring special attention, too. They feel that they're being forgotten, while every other faction of society is receiving handouts.

The mere existence of the Tea Party validates these feelings. It proves to these men that they aren't alone, a sentiment that Donald Trump continues to capitalize on. Trump often spouts whatever he thinks, with little regard for the feelings of others, and his opinions are often shared by these men who, until now, have been kept in check by the liberal media. In the eyes of Tea Party members, Trump blames the people who deserve to be blamed. And his promise to return the country to its previous state — when, unlike now, the white working class was autonomous and revered — is music to their ears.

### 5. Tea Party members feel disrespected by the rest of the nation. 

When it comes to mocking Republicans, the liberal media often doesn't hold back. Naturally, this mainstream behavior makes Tea Party members feel condescended to.

In fact, Southerners claim that they've been forcibly made to change their ways since the 1860s. According to them, this all began with the Civil War in 1861, when residents from the North came down to the South and told the inhabitants that their way of life was inherently wrong and had to change. Southerners believe that this occurred again later during the Civil Rights Movement, and that it's essentially happening again now between the political right and left.

And indeed, many Northerners believe that Louisiana has an issue with racism. However, the white inhabitants don't consider themselves to be racist in the sense of actively hating black people — but many do tend to have an issue with people of color being closer than themselves to realizing some form of the "American Dream."

Another thing Tea Party members feel they're judged for is their relationship with religion. Louisianans frequently refer to the concept of "being churched" — this is, the idea that the church, and religion as a whole, constitutes an important part of you. The church provides a sense of community for them, and regularly encourages people to aid those in need.

Although this sounds positive, the Tea Party followers tend to have an issue with being told how to express their magnanimity — especially when directives are coming from the liberal members of society. Take the plight of Syrian refugee children, for instance. Tea Party members believe that they're made to feel guilty for not helping refugees, while their efforts to volunteer at, say, a support group for American soldiers is deemed insufficient.

Furthermore, they feel that liberals mock their Bible-based education. For instance, marriage, as defined by the Bible, can only exist between a man and a woman. But liberals are quick to call people who adhere to this definition homophobic.

It's no wonder, then, that Tea Party members tend to feel attacked — or that, as a result, they act defensively in order to protect their way of life.

### 6. Louisiana’s small-government policies are destroying its environment and executing its residents. 

Oil companies have done terrible damage to Louisiana — too much to list in full. But mentioning a few atrocities may suffice to drive home the general point.

The Bayou Corne Sinkhole was one of the worst disasters the state has ever seen. In 2012, an oil drill struck a salt dome under the canal's floor, which caused the oil trapped within it to spill into the water. The crack then widened, and trees, boats and homes were sucked into the gaping hole left behind. The incident left an entire habitat poisoned and destroyed.

The general pollution created by oil companies has also been linked to a rise in cancer cases. The number has shifted dramatically in recent years — in fact, a study conducted in 2015 by the American Cancer Society listed Louisiana as the state with the second highest amount of male cancer patients per capita.

One couple reported that nine members of the husband's immediate family had fallen victim to the disease. There were no big smokers or drinkers in his family and, interestingly, there were no instances of cancer before the arrival of the oil companies.

To distract the populace from the devastation happening around them, Fox News seems to have a plan of action. The network doesn't report on the disasters that the people of Louisiana face, but rather chooses to focus on sensationalist stories that inspire fear in viewers. According to Fox, the federal government is a lot worse than any toxic sinkhole.

The network also pillories the Environmental Protection Agency, portraying it as a negative organization controlled by the federal government. Charles Krauthammer, a Fox commentator, even compared the EPA's bid to raise air-quality standards to an attack on America by enemy forces.

In effect, the people of Louisiana are trapped. Influenced by Fox and habitually circulating damaging ideologies among themselves, they neglect to address the issues that are slowly killing them.

> _"To some, Fox is family."_

### 7. It’s important to try to empathize with the other end of the political spectrum. 

In order to understand someone else's political stance, you have to understand that person's history — the personal experiences and practical exigencies that have informed her system of belief. If one fails to empathize in this way, it becomes difficult to work toward mutually tolerable political compromises.

Indeed, the left and the right are unable to understand each other because of such a failure of empathy. Though each side agrees there are major societal problems, they can't agree on the cause of those problems. 

The left blames big businesses, which make up 1 percent of the population and yet hold the other 99 percent hostage.

People on the right pin the blame on the government, the poor and minorities; they feel that these groups take from honest citizens what rightfully belongs to them.

It's all a matter of perspective. People from the South, for instance, see things much differently from people who grew up in the North.

In the antebellum South, there was a definitive hierarchy — plantation owners at the top, slaves at the bottom and poor white farmers in the middle. These white farmers were terrified that they, too, might one day live like slaves, so they did their utmost to differentiate themselves.

Today, that structure is much the same. Impoverished white people are closer to the desolation that exists at the bottom of the pile, so they're naturally eager to avoid it at all costs. It's this attitude that makes them favor big business over the government; to accept government aid would be to admit that they are no better than the poor people who need it.

Both sides would struggle less if they chose to empathize more. It's this current lack of understanding that's causing both supporters of the left and the right to feel displaced.

On the right, people feel mocked for upholding values that they perceive as quintessentially American. They have lost the right to say "under God" in the pledge of allegiance in schools — and it's important to remember that while religion may no longer play a pivotal role for some, it's of the highest importance to them.

On the left, people feel that their public services are being stolen, and that the Tea Party members are encouraging this theft.

Ultimately, the fundamental issue is the same on both sides — Americans no longer recognize the country they once knew and loved. Empathizing with people on the other side of the political divide may be the only way to make America look like home again.

### 8. Final summary 

**The divisive political climate within Louisiana is clearly having negative repercussions on the people of the state. These Louisianan trends in opinion are representative of national ones, which is why, when someone holds political views that differ from your own, empathy is the best policy. In order to progress as a society, it's important to collectively work toward a better future and dismantle the organizations and ideologies that really pose a threat.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Hillbilly Elegy_** **by J.D. Vance**

_Hillbilly Elegy_ (2016) is an autobiographical walk through the life of a man who grew up in an impoverished neighborhood of Middletown, Ohio. These blinks tell the story of a boy who, despite a turbulent childhood, beat the odds and pulled himself out of poverty.
---

### Arlie Russell Hochschild

Arlie Russell Hochschild is professor emerita in Sociology at the University of Berkeley, California. In 2015, she won the American Sociological Association's Lifetime Achievement Award. She is also the author of _The Outsourced Self: Intimate Life in Market Times_.

