---
id: 5563a8c064616400071c0000
slug: strategic-connections-en
published_date: 2015-05-26T00:00:00.000+00:00
author: Anne Baber, Lynne Waymon, André Alphonso, Jim Wylde
title: Strategic Connections
subtitle: The New Face of Networking in a Collaborative World
main_color: 86D9CB
text_color: 47736B
---

# Strategic Connections

_The New Face of Networking in a Collaborative World_

**Anne Baber, Lynne Waymon, André Alphonso, Jim Wylde**

_Strategic Connections_ (2015) offers practical tips on developing the skills to become an effective networker. In an increasingly connected world, networking has never been more important. Find out which skills and knowledge you need to succeed in the new collaborative workplace.

---
### 1. What’s in it for me? Learn how to become an expert networker. 

We all know that in the modern business environment, networking is important. Yet, how many of us actually know how to network properly? The truth is, hardly any of us do. Most people associate networking with awkward attempts to chat with colleagues at a dinner party or with competitors at conferences. Perhaps as a result of bad experiences, many of us deliberately avoid the whole process of networking and assume that we are no good at it.

But this view of networking is false. There is much more to the process than we think, and these blinks show you exactly why. Reading them will give you the skills and information you need to become a _strategic networker._

In these blinks, you'll discover

  * why everyone you know belongs in a particular net; and

  * why you should introduce yourself like James Bond.

### 2. The first step toward becoming a strategic networker is committing to a new networker identity. 

Unfortunately, many people feel that their jobs or their personalities make networking either unnecessary or impossible. "I'm a doctor," they say, "I don't need to network!" Or, "I'm an introvert. Networking just isn't part of my skill set."

This kind of passive thinking is seriously outdated. The concept of networking has gradually shifted over the years within our changing environment.

Today's workplace is becoming increasingly collaborative. Business today is much less about protecting your ideas or preserving your hard-earned position within the company hierarchy, and much more about spreading novel strategies and working with your colleagues, no matter how far up the totem pole they might be.

In this kind of collaborative environment, _networking_, the process of creating relationships that are beneficial for individual or organizational success, is vital.

Today, we have access to people all across the globe whose skills and knowledge complement our own. Whatever you do for work, there is a fair chance that there's someone out there whose skills would be of great benefit to you. So if you are a doctor, for example, you could look for someone with experience in growing a business, and could help you expand your practice.

In this network-oriented world, everyone has a role to play; anyone can develop a _networker identity,_ enabling himself to reap the multiple benefits of an interconnected world.

Introverts, for example, might feel that their personalities limit their abilities to network. However, they might also be adept planners and listeners — two qualities that are crucial for networking.

Clearly, networking has significant benefits in the modern workplace. The following blinks will show you how to develop the skills that will make you a _strategic networker._

### 3. To make the most of networking, you’ll need a strategic approach. 

The first step to becoming a networker is easy, and has everything to do with developing the right mindset. All you have to do, no matter what your job title is, is add "plus networker" to the end of it. So, you're no longer merely an "accountant" or a "doctor," but instead an "accountant plus networker" or a "doctor plus networker".

When you accept that networking is part of your professional role, you can then approach it in a strategic way, just as you would your actual professional role.

These tips will help you become a better _strategic networker_ :

First, be _proactive_. If you want to be the best networker, don't sit around and wait for things to happen to you. You have to make moves! Go to conferences, join groups of like-minded people and seek out relationships that could help you and the company you work for.

For example, a partner at an architectural firm that specializes in hospital design could join the American Institute of Architects or a hospital administrators' association. At either organization, she is likely to find plenty of potential contacts that could help her expand and improve her business.

Another way to be proactive is to always think about what your personal network can do for your company. Ask your boss if there is anything the company needs help with; maybe the right person for the job is already part of your personal network! You'll never know unless you ask.

Finally, strategic networkers are always ready for chance encounters with potential contacts. Indeed, some of the best contacts you will ever make will be the result of chance encounters. It could be the CEO you meet in the hallway, or the friend of a friend you bump into in the elevator. Make sure you are ready for them by preparing what you might say, thinking about how you might describe your job and deciding what kinds of connections you want to make.

### 4. A strategic networker has contacts in many areas and works to develop trusting relationships. 

So who exactly belongs in your network? Your co-workers? People in the same industry? Your friends and family? It could be any and all of these! In fact, everyone's network consists of four _nets_ :

The _WorkNet_ includes everyone you work with on a daily basis, such as your co-workers.

Your _OrgNet_ includes people in other divisions and departments from your organization; these could be your friends in finance or IT.

The _ProNet_ includes professional contacts outside your workplace. These could be your colleagues in, say, the national engineers' association, or your former clients and co-workers.

And finally, your _LifeNet_ encompasses your family, friends and even the people you play squash with.

Ask yourself how solid your connections are in each of these nets — a good networker has them all covered.

However, it's not enough to simply know people that fall into each of these categories. To develop a strong network you need to develop _trusting relationships_ with the people in your nets.

So how do you cultivate trust between you and your contacts? Show them your _competencies_ and _character_.

Demonstrating character is obvious: you simply need to show people that you embody the qualities of a trustworthy person by being honest, open and loyal.

Yet even the most honest person won't be trusted if he isn't capable of getting the job done — you need to show people that you're actually good at what you do. Show them your successes from the past and make sure you have the right skills to do your job well.

For example, if you work as an HR manager, prove that you can be trusted by showing that you are well organized and a good listener.

But no matter what, always remember that building trust takes time. On average, it takes six to eight conversations to build trust with a new contact, so keep at it.

### 5. As a strategic networker, you must increase your social acumen and deepen interactions. 

You might find that to develop your network, you'll have to attend a range of events relevant to your field. But it's not enough for you to just show up. In order to build trusting relationships, you'll have to actually meet and talk to people.

By improving your _social acumen_ you'll be better equipped to go through the rituals inherent in effective networking. This can be daunting to some people, so here are a few tips to help you manage:

Be sure to learn peoples' names and make sure they remember yours as well. When meeting someone new, repeat your name to her. For example: "I'm _Thomas_, _Thomas_ Smith." Likewise, you should repeat her first name in order to better remember it: "It's nice to meet you, Louise."

Be confident about joining groups. When you arrive at an event it can often seem as if everyone has already formed their cliques — but that's all in your mind. Don't think: "I have to break into this group." Instead think: "This group isn't complete without me."

Finally, when it's time to end the conversation with a new contact, find a way to turn it into a greater networking opportunity, both for you and for her.

When you're ready to end your conversation, ask her to introduce you to someone in your field. For example, if you're an architect, just ask if she knows anyone at the event who is also involved in architecture. You could also introduce your new contact to others: "John, I'd like to introduce you to Laura. She's also a doctor."

While you're involved in conversations with new contacts, it's important to keep focused on building an actual relationship. Pay close attention to what your contact reveals about her character and competencies. Maybe she's a great accountant, just when your firm is looking for one.

### 6. Strategic networkers communicate their expertise. 

It's natural to feel uncomfortable when talking about yourself. Even so, it's something you'll have to get over to become a successful networker. Here are some tips to help you out:

When someone asks what you do, answer with more than just your job title. Instead, answer with something that demonstrates your competencies.

A good way to do this is to use the _Best/Test_. First, reveal one skill you want people to remember about you, then come up with an example that backs up that skill.

So, you might say, "I make sure software development projects stay on schedule. Due to our precise and timely delivery, IBM is interested in our work."

Another way to help your new contacts remember you and your competencies is to tell them stories. The best stories follow the _5-S story formula_.

It starts with a _segue_. Signal that you have something to say. For example, "This great thing happened to me!"

Then, describe the _situation_, "Last week I went to a conference in Paris with my CEO."

Next, introduce the _SNAFU_, that is, the problem you had to solve: "The night before the conference my boss called to tell me that his wife had just gone into labor. He had to fly back to Berlin to be with her, but there were still nearly 200 people expecting us to present our research the next day."

Then, present your _solution_ : "I suggested that I do the presentation myself, even though the thought terrified me. My boss agreed, and I spent all night practicing."

Finally, explain the experience's _significance_ by highlighting the positive impact it had on you, on others or on your organization: "The presentation went really well and my boss was very impressed."

If you stick with the 5-S formula, you can be sure to create a memorable story that demonstrates your key qualities and competencies to your new contacts.

### 7. Networking will allow you to stay on top in the modern technological world. 

Today, it seems like new technologies are coming out faster than we can keep up. Just think about how technology has changed our lives even in the past five to ten years. Fortunately, your new networking identity and networking skills give you the tools to enjoy success in a changing environment.

In order to be an adaptable networker, you'll need to follow these three principles: _reframe networking_, _risk reaching out_ and _reinforce collaborative culture_.

By _reframing networking_ in your mind, you encourage yourself to acknowledge the importance of networking. By committing to your networker identity, you know that you're far more than your mere job description. Remind yourself that you're not just your job title; you have plenty of untapped talents.

_Risking reaching out_ urges you to leave your comfort zone in order to develop new relationships. Recall your newfound networking skills to help you feel more confident when reaching out to new contacts.

_Reinforcing the collaborative culture_ encourages you to act as if you own the organization you work for. If you treat it as your own, you'll be better equipped to develop strategies to help the company achieve greater success.

Finally, remember to apply your face-to-face trust-building strategies online, too. In our modern world, lots of networking is done online, and it's just as important to keep your word online as in person.

Say you've received an important query from your boss and you fail to reply as the day's workload steadily grows. Your boss finally has to remind you to reply a few hours later, but you rush your response and your answer is inaccurate, on top of being late. Do you think she'll trust you with a similar task in the future? Being as reliable online as you are in person is key to building trust among your contacts.

By making these practices part of your everyday life, you'll be well on your way to becoming a strategic networker.

### 8. Final summary 

The key message in this book:

**Networking is more important today than it has ever been, which means that developing networking skills is paramount for today's workers. You can become a strategic networker by leveraging your contacts in new and creative ways, and fully committing to your networker identity.**

**Suggested** **further** **reading:** ** _Coffee Lunch Coffee_** **by Alana Muller**

_Coffee_ _Lunch_ _Coffee_ is a practical guide to networking. Using her personal and professional experience, along with tips and exercises, author Alana Muller demonstrates how to develop networking skills and build lasting relationships that can help us in our personal and professional lives. A must-have for anyone who wants to succeed professionally.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Anne Baber, Lynne Waymon, André Alphonso, Jim Wylde

Anne Baber, Lynne Waymon, André Alphonso and Jim Wylde are collaborators at Contacts Count LLC, an international training and consulting firm that has specialized in business networking for 24 years. Baber and Waymon co-founded the firm 24 years ago, and have, among other networking books co-authored bestselling book: _Make Your Contacts Count: Networking Know-How for Business and Career Success_. Their high-profile clients have included eBay, Lockheed Martin and KPMG.

