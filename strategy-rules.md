---
id: 557594313935610007300000
slug: strategy-rules-en
published_date: 2015-06-09T00:00:00.000+00:00
author: David B. Yoffie and Michael A. Cusumano
title: Strategy Rules
subtitle: Five Timeless Lessons From Bill Gates, Andy Grove and Steve Jobs
main_color: 1E7C98
text_color: 196880
---

# Strategy Rules

_Five Timeless Lessons From Bill Gates, Andy Grove and Steve Jobs_

**David B. Yoffie and Michael A. Cusumano**

_Strategy Rules_ (2015) explores the business strategies and leadership styles of three hyper-successful tech CEOs: Bill Gates of Microsoft, Andy Grove of Intel and Steve Jobs of Apple. These blinks break down the strategic expertise necessary to build a competitive business and ensure long-term success.

---
### 1. What’s in it for me? Discover the strategies that made Steve Jobs, Andy Grove and Bill Gates the most successful CEOs of our time. 

Are you reading this on a PC? Or a Mac? Or maybe an iPhone? If it's one of the three, you're benefitting from the skills and strategies of three of the most influential and successful businessmen of modern times, Apple's Steve Jobs, Microsoft's Bill Gates and Intel's Andy Grove.

It wouldn't be overstating the case to suggest that these three men changed the world by providing affordable powerful computers and digital gadgets that everyone could use. These gadgets lay behind the digital revolution and modern society.

So how did they do it? And can you learn from them? These blinks show you the strategies and skills that allowed them to create some of the biggest companies in the world.

In these blinks you'll learn

  * why Steve Jobs would have made a great judo master;

  * why Microsoft marketed a service it didn't have; and

  * why Intel's first conference calling system was simply too far ahead of its time.

### 2. Strategize by creating a vision, setting priorities and anticipating customer needs. 

Wouldn't it be great to know how top companies like Apple, Microsoft and Intel came to dominate their industries?

The answer is simple.

They all share a common key to success, _vision_. In short, they're the best because they started with a clear picture of where they wanted to go.

But knowing where you want to go isn't helpful if you can't get there, as these companies knew. So after setting their visions, they developed the strategies to make them realities.

How?

Start realizing your vision by setting your priorities.

Take Gordon Moore, co-founder of Intel. In 1964, Moore predicted that computing power would double every 18–24 months. Andy Grove, Intel's CEO, used this information to foresee an industry shift from a horizontal structure where companies built and sold complete systems to a vertical structure where they focused on smaller product groups to boost efficiency.

So Grove shifted the company's priority from producing complete computers, including all hardware and software, to microprocessors — specific components — thereby leading to Intel's dominance in the field.

But Moore's discovery went beyond Intel. Bill Gates used his own interpretation of it to define Microsoft's priorities.

Here's how.

Gates realized that if computing power increased exponentially, it would eventually become virtually free. So, instead of selling hardware like processors that were sure to drop in value, Gates focused on software to fully take advantage of this computing power.

Priorities are essential to realizing your vision, but anticipating the needs of your customers is also crucial because it will help keep you on track.

For instance, in 1979, Steve Jobs visited Xerox's research center and saw the first graphical user interface (GUI). Until this point, operating systems only responded to typed command lines, making them difficult to use. Jobs knew that in order to appeal to customers, personal computers needed to be more user-friendly, and seeing the future of computers in GUI technology, he used it to create an accessible user interface that made computers mainstream.

It's clear that having a vision, setting priorities to realize it and anticipating your customers' needs are all essential to guiding your company. But what are your goals as you move forward?

### 3. Ensure your product’s success by honestly evaluating market conditions and deterring competitors. 

Say you have a vision for your company, you've set priorities to realize it and know what your customers need. Sounds like the makings of a great business. But before setting up shop it's essential to determine if the market is right and whether you'll be able to beat out competitors.

Look at the limits of the market and surrounding technology in order to evaluate your product.

For instance, in the 1990s, long before Skype, Intel put hundreds of millions of dollars into a conference calling service called ProShare. But they made one mistake by ignoring the limits of their era: the hardware required was expensive and the technology needed to transfer data was slow and unreliable. This meant their groundbreaking idea was doomed to fail.

On the other hand, Apple had a similar experience in the early 2000s but was honest about the circumstances. The company was experimenting with iPad prototypes as early as 2002 or 2003. The products worked brilliantly but there was one problem: Wifi had only just become more widely available and customers couldn't yet realize the device's full potential. Realizing this, Apple waited until this vital infrastructure was more developed.

Clearly then, knowing when to launch your big idea is key. But once your product is released, your job's still not done: you'll need to control competitors by building _barriers to entry._

How?

One way is by making your product the industry standard. Here's how Microsoft did it.

When IBM asked Gates to develop an operating system for their computers, he agreed but only if he could also sell the operating system to other manufacturers. IBM agreed and Gates went on to develop the now famous _Disk Operating System (DOS)_. Gates could have made a fortune in licensing fees by making his software exclusive to IBM, but he had bigger plans.

His vision was to sell DOS at low prices and high volume to many computer manufacturers so that it became the industry standard.

It worked. His rock bottom prices meant competitors could barely get a foot in the door!

### 4. Staying competitive requires taking risks, just don’t bet the whole company. 

As a CEO you'll need to take big risks to stay on top. But all great CEOs know that while big changes are necessary to remain competitive, it's essential not to bet your shirt on them.

Take Apple's famous shift from IBM's PowerPC hardware to Intel's microchips.

In the early 2000s Mac's PowerPC architecture had been falling short for a while and Jobs knew it was time for a change. Intel's undeniably better microprocessors were an obvious choice but switching hardware would mean completely rewriting Mac's operating system and applications. Not to mention the added liability of sales falling in advance of the new release, or worse, loyal customers abandoning the company altogether instead of replacing old software.

In short, the situation looked pretty risky; one analyst even predicted that the shift could lead to Apple's demise as they lost customers and developers. With such high stakes it might seem like Jobs was betting the whole company. But he wasn't.

Why not?

Jobs knew the extra income from Apple's extraordinarily high-selling iPod, which sold over 10 million units in the two quarters preceding the announcement, would give the company a financial cushion. So he could take this risk because the pressure to ensure the company's survival was lifted from Mac sales.

Looking back, the decision couldn't have been made at a better time. Macintosh computers' market share doubled over the next five years!

Sometimes taking risks to stay competitive will even mean eating into sales of your other products. As a CEO, it's essential to know when it's necessary to cannibalize your cash cow business to develop new products.

Remember when Apple released the iPod Nano? iPod Minis were still flying off the shelves. The company did it again when they released the iPad, thereby cannibalizing their laptop sales. But the lost laptop revenue was nothing compared to all the Windows users the iPad converted!

### 5. Instead of developing a great product, develop a strong platform – the key to exponential growth. 

A stellar product can feel like the ticket to success, but what if there's no demand for it? The best CEOs in the business know that success means building platforms that hook people on your product, making it an industry standard.

When Steve Jobs first started out he didn't understand why platforms were key. He was obsessed with controlling user experience and refused to license Apple's operating system to other manufacturers. Taking the opposite route of Gates, who sold both DOS and Windows to everyone he could, Jobs focused on his own niche with limited success. Remember, in the 1990s Mac's market share was in the low single digits!

What changed?

Jobs made a comeback after growing as a strategist and understanding platforms: He built a Windows version of iTunes, the iPod's supporting software. Its wider usability rapidly made iTunes a platform for Apple to control the music and digital media market, and the iPod, now compatible with any computer, dominated its field.

Andy Grove understood platforms too. He knew that Intel could only sell more microprocessors if more computers were being sold, and that the consumer value of computers was hindered by conflicting hardware standards. Armed with this information, he opened a research lab, which, although outside his personal area of expertise, discovered a number of things modern consumers look for in computers. One discovery was the need for a universal connector like the USB. Up until that point every manufacturer had different specifications, and simple tasks like connecting a printer could be a nightmare.

Grove patented the lab's findings but made them accessible to all. Computer manufacturers seized on the information, making easier-to-use computers that sold better. And in each computer? An Intel microprocessor. The company's sales skyrocketed!

Grove's trick was recognizing that the computer industry as a whole was Intel's platform. By improving the platform he increased his product's sales.

### 6. Strategize with cunning and strength but know when each tactic is appropriate. 

In the world of business, competition can take many forms. Some CEOs use their cunning to sneak around like judo players while others are bold and bullying like sumo wrestlers.

But both methods are essential and the best CEOs know when to use each one.

Using _judo_ tactics means being agile, stealthy and making sneaky maneuvers. The key to judo success is to appear as harmless as possible, leading your competitors to underestimate you.

For instance, when Jobs launched iTunes he used a _puppy dog scheme_ of feigning harmlessness.

At first he intended to buy Universal so he could sell their music through his platform. But, instead of using this strategy, which would have put him in direct competition with the major record labels, he played the part of a niche outsider. How threatening could a computer company with a mere two percent market share be to major record labels?

It worked: the labels underestimated Apple's weight and gave them better terms in contract negotiations.

Judo tactics are great for pulling the blinders over your competitors' eyes, but sometimes you'll need to haul out the big guns and this means _sumo_.

Sumo tactics are all about power and size: think buying out competitors and ruthless undercutting. For instance, in the classic Fear, Uncertainty, Doubt (FUD) strategy, a market-leading company announces a product long before it's ready. The potential of this unavailable product keeps consumers waiting, preventing them from buying competing products.

A great example of FUD in action happened in 1982 when VisiCorp gave a preview of a GUI operating system. Gates, who was in the early stages of developing his own GUI OS was spurred into action and started promoting Microsoft's product, even saying it would launch before VisiCorp's. Microsoft's reputation kept customers holding out for another _two years_ until the product was launched!

### 7. Imbue your company with your unique skills but consult experts for the rest. 

By now we know the strategies and techniques that helped Jobs, Grove and Gates build powerful companies that dominated their markets. But what about the individual personalities and leadership styles that made these men and their companies the best?

For starters, they knew how to inject their gifts into the DNA of their companies.

All three of them made the drive and desire that propelled them to success part and parcel of their companies. Job's love for design led Apple to push the standards of product design and user experience. For Grove, it meant using his discipline to develop a clean, systematic process for Intel to tame the cacophony of engineering and manufacturing. And Gates, the passionate hacker, was so involved he personally rewrote his developers' code because he "just didn't like the way they coded."

But sharing your gifts with your company can only get you so far. These businessmen also knew that the success of their companies depended on recruiting experts to complement their strengths.

That's because if they had counted on their skills alone they wouldn't have had the diverse knowledge necessary to be strong leaders. Therefore they all continued their educations and often consulted experts. Jobs and Gates even found partners that complemented both their skill sets and characters.

For instance, Jobs didn't care about finances or operations at all. In Tim Cook, his partner and successor, he found a collaborator who both shared his vision for the company and could handle the business that Jobs couldn't. Jobs could therefore concentrate on his area of expertise.

For Gates, an often introverted and sarcastic nerd, the perfect match was Steve Ballmer, a high energy salesman who loved competition.

Now that we know the strategies that made Gates, Jobs and Grove leaders in their fields, it's time to see how others have drawn inspiration from their legacies.

### 8. How the new generation of tech all-stars adopted the strategies of the past. 

The tech geniuses of our time, Larry Page of Google, Pony Ma of Tencent, Jeff Bezos of Amazon and Mark Zuckerberg of Facebook built companies that transformed the way we live, communicate and consume. But they're standing on the shoulders of giants like Gates, Grove and Jobs.

For instance, Zuckerberg opened up Facebook to third parties in a classic example of using the synergy of platforms to create exponential growth.

When Zuckerberg launched the website it was limited to Harvard students. He then expanded to other colleges and eventually the general public. But the real shift came in 2007, at a time when MySpace still had four times as many users as Facebook. Zuckerberg opened his doors to outside developers, giving them the tools to build Facebook applications.

The site's appeal to users, advertisers and developers boomed. By 2014, Facebook had 1.3 billion users and 20 million running applications, leaving MySpace, with its 50 million users, in the dust.

But Zuckerberg wasn't the only one to learn from the previous generation.

It's easy to take the fact that Google is everywhere in our lives for granted. The company started out dreaming big — the letter accompanying Google's IPO stated they would actively pursue high-risk activities even if they meant short-term losses.

Their gambles paid off.

For example, the company's 2006 decision to buy YouTube for $1.6 billion lost them money for years. But in the long run they prevailed, turning YouTube into the premiere video hosting site on the web.

Or consider 2005, when Google acquired Android for $50 million. Larry Page made Android's operating system free, a seemingly colossal mistake. But the risk resulted in huge returns when Android's market share hit 80 percent in 2014 and Google was raking in mobile advertising profits from the system while boasting a market value of nearly $400 billion!

### 9. Final summary 

The key message in this book:

**The founders of Microsoft, Intel and Apple all built their tech empires using the same strategies: they knew to create a vision, define a plan to realize it and take risks without endangering their entire company. Once their businesses were going strong, they launched them into the stratosphere beyond their competitors by sharing their talents with their companies and building platforms that established their products as industry standards.**

Actionable advice:

**Realize your vision by retracing your steps:**

Don't know how to make your vision a reality? Try imagining your path to success. Start by visualizing yourself realizing your vision, and then imagine moving backward, one step at a time, to see how you got there.

**Suggested** **further** **reading:** ** _Coffee Lunch Coffee_** **by Alana Muller**

_Coffee_ _Lunch_ _Coffee_ is a practical guide to networking. Using her personal and professional experience, along with tips and exercises, author Alana Muller demonstrates how to develop networking skills and build lasting relationships that can help us in our personal and professional lives. A must-have for anyone who wants to succeed professionally.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### David B. Yoffie and Michael A. Cusumano

David B. Yoffie, in addition to being the longest serving member of the Intel board, teaches business administration at Harvard Business School. He is a prolific writer and the author or editor of nine books, including the bestselling _Competing on Internet Time_ (co-written with Cusumano) _._

Michael A. Cusumano has been writing about the software industry since 1985. He is professor of management at MIT and the author or editor of 12 books, including the classic _Microsoft Secrets._

