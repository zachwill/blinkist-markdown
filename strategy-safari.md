---
id: 54e3d909303764000ad80000
slug: strategy-safari-en
published_date: 2015-02-20T00:00:00.000+00:00
author: Henry Mintzberg, Bruce Ahlstrand and Joseph Lampel
title: Strategy Safari
subtitle: A Guided Tour Through the Wilds of Strategic Management
main_color: 92C85C
text_color: 47612D
---

# Strategy Safari

_A Guided Tour Through the Wilds of Strategic Management_

**Henry Mintzberg, Bruce Ahlstrand and Joseph Lampel**

_Strategy Safari_ is a comprehensive guidebook to strategy formation, complete with a thorough run-down of all its major schools of thought. By explaining the primary strengths and limitations of each school, _Strategy Safari_ gives you the tools to form the strategy that is right for your business.

---
### 1. What’s in it for me? Take a journey through all the approaches to managing strategy. 

Everybody has an opinion about strategy. Some think that strategy is unnecessary and that knowing how to react is more important. Some think it's better to devise a strategy and stick to it, no matter what you face.

Well, there's good news and bad news. The good news is that whatever your view on strategy is, you're probably right — at least partly. The bad news is that strategy isn't as simple as having an opinion!

Strategy is pretty complex when you get into it. It has many different aspects, because it's not just about making plans and following through. Strategy is more abstract than that, it's about handling stability and change, and can even be about the patterns, perspectives and positions that influence people when they make decisions.

This pack presents a crash course in management strategy, covering all ten of the most influential schools of thought to give you a comprehensive overview of the field.

After reading these blinks, you'll know

  * which strategy Sun-Tzu uses in _The Art of War_ ;

  * which strategy is best for start-ups; and

  * which school of thought combines all the strategies.

### 2. The first and oldest management school is the design school. 

Some schools of strategic management are well known even beyond management circles. And although many entrepreneurs aren't familiar with the term "_design school_," its concepts have nonetheless been widely incorporated into many fields.

Design school distinguishes itself from other management strategies because it understands strategy as the process of coming up with ideas. It accomplishes this by employing the famous _SWOT_ model: Strengths, Weaknesses, Opportunities and Threats.

When you use the SWOT model, you consider your organization's strengths and weaknesses to be contextualized by the opportunities and threats within the business environment.

SWOT also takes into consideration things like social responsibility and the values of management, meaning that strategies are tailored to each individual case.

By laying out the essential elements of strategy, the SWOT model offers you a bird's eye view of your market and your organization. You can easily see where your strengths match up with an opportunity, and make strategies without even leaving the meeting room.

What's more, by evaluating your strengths, you can develop strategies based on what you already have, without necessarily having to acquire new assets.

Despite all its benefits, the design school also has some major drawbacks. The school can sometimes be too dogmatic; people who have devised SWOT-based strategies often think they're grand and universal, which can lead them to overlook problems with feasibility when it comes to actually implementing their theoretical strategy.

Moreover, designing a strategy too far in advance or with too much distance from the actual implementation can lead to huge failures. We have only to look at US military strategies during the Vietnam War — formulated on the other side of the world with no appreciation for the conditions on the ground — to see that this is true.

### 3. The planning school is great for assessing what you have and for setting goals. 

Notice that the design school considers the whole strategy _before_ putting it into practice. It is this property that makes it a _prescriptive_ strategy, i.e., one where the plan defines the actions.

The _planning school_ is another prescriptive school. It aims to divide the SWOT-model into neatly defined steps, from analysis of the situation (and the SWOT) to the actual implementation of the strategy. It is most often seen in urban planning, system theory and socialistic systems.

The planning school operates from the assumption that innovation can be embedded into institutions. Once goals and objectives are clearly defined, you can start working toward a precise plan that can fulfill your aims. In essence, it works like this:

  * first, conduct a thorough analysis of your situation;

  * second, develop a plan to address the situation in order to reach your goals; and

  * finally, work out the exact steps that people in the organization will take to enact the plan.

Although the planning school is very clear in establishing its plan of action, it sometimes struggles to pick the correct direction.

As in the design school, planning school practitioners are sometimes guilty of planning in isolation, away from the realities of the market or the rank-and-file within their organization.

What's more, planning school strategies rely on forecasts and predictions about future circumstances. But this approach is inherently flawed.

First, a single incorrect prediction can send the entire strategy off course. Second, these kinds of plans require lots of information gathering, synthesis and analysis, and this can take forever! Planning, therefore, can be slow moving and less active or dynamic than other strategies.

> _"It is the performance of the organization that matters, not the performance of its planning."_

### 4. The last prescriptive school is the positioning school, which views strategy formation as an analytical process. 

Most people don't want to have to choose from a myriad of strategy opportunities, but would instead rather choose between clear decisions. This can be fulfilled by following the _positioning school_.

For the positioning school, strategy formation is an analytical process. The concepts behind the positioning school are in fact quite old. Historical military thinkers, for example, went to war with the belief that there was a set of defined strategies and analysis processes to follow. Sun-Tzu's _The Art of War_ is only one of many examples.

In the positioning school, like the other prescriptive schools, all the processes are developed far away from where they will actually be implemented, like a general surveying the battlefield. The battle, so to speak, is reviewed after the fact, and managers then make adjustments for future battles.

While the positioning school puts its focus on analysis, these analyses don't necessarily produce strategies. Like the other prescriptive schools, positioning lacks a strategy for coming up with a strategy!

Placing your focus on analysis isn't wrong per se, but it is too narrow. Calculation is important, but a good strategy is only as good as the people behind it. Inevitably, good strategies can fail without the right people to implement them.

Prescriptive schools are great at theoretical analysis, but _descriptive_ schools bring things closer to reality. But are they any better at explaining and defining the formation of management strategies?

### 5. The entrepreneurial school relies on the vision of its leaders. 

In the previous blinks, we saw how the deliberately planned strategies of the prescriptive schools don't necessarily guarantee success. But what explains the impressive track records of those companies whose long-term success was guaranteed by a visionary leader, such as Bill Gates of Microsoft?

One type of visionary leadership is found in the _entrepreneurial school_, which focuses on the personal role of a single leader, rather than on collective work.

Strategy formation through visionary leadership leads to flexible and dynamic strategies based on inspiration. Rather than focusing on the physical resources an organization has access to, it focuses instead on the emotional resources of its leader: inspiration, intuition, judgment, insight, experience, etc.

In this way, company leadership is almost spiritual, in that it guides the company and "shows the way to the light," so to speak.

While reliance on a single leader for strategy formation has obvious limitations, the entrepreneurial school nevertheless brings important insights to the table.

Companies ask themselves, for example, how they can rely on a vision for the future when the future is neither fixed nor well-defined. However, visionary entrepreneurs often use the past as a precedent when formulating strategies.

This makes the entrepreneurial school inherently _descriptive_ : it analyzes the process of strategy formation itself, describes it and problematizes it, and this process leads to a new strategic direction.

Remember, however, that too much analysis can blind organizations to unexpected new developments.

Despite this tendency to overanalyze, the entrepreneurial school is nonetheless useful. Start-ups that need an innovative shot in the arm, for example, may find success following this school.

> _"It is better to build a visionary organization than to rely on a leader with mere vision."_

### 6. The cognitive school focuses on how we actually formulate strategies. 

As we saw in the previous blink, the entrepreneurial school describes _who_ formulates the strategy. But it fails to address the way in which a strategy is carried out. The _cognitive school_ is concerned with the _how_.

In essence, the cognitive school is about thinking. It imagines managers as information workers who have to process data, organize it, store it and so on. So the cognitive school looks at how managers create their mental maps and how they interpret ideas and situations.

These maps and interpretations are important, because strategies emerge from them.

The cognitive school is not a monolith. Indeed, the school divides itself between those who define mental processes as more objective or more subjective. Yet both agree that subjective mental processes are at least influential.

The cognitive school thus serves as a bridge between the prescriptive schools, which claim to objectively analyze an organization and formulate a strategy, and subjective schools like the entrepreneurial school, which openly embrace the subjective perspective of one person.

However, the cognitive school can focus too much on certain strategies while missing others.

We've created all sorts of mechanisms to order the world. Take the constellations, for example. These are more a creation than a description. The Big Dipper is not _really_ there, but if you draw lines between certain stars you can imagine it.

We're able to make sense of the Big Dipper in part because we filter out other information, like the surrounding stars. In the same way, cognitive maps filter out information as a means to create order. While they can tell you _how_ a manager thinks, they can't tell you _why_ she thinks that way.

The cognitive school has a lot of potential, but it isn't fully developed. It's not yet clear how to take a manager's mental map and turn it into a concrete strategy.

### 7. The learning school stresses strategy as a process where ideas come from everywhere. 

One thing we haven't touched on yet is changing and molding strategies over time to suit new conditions. Sometimes you can't just blow up one strategy and start over! The _learning school_ offers a good alternative.

The _learning school_ sees strategy as an emergent process that occurs as people work collectively with one another. In this way, strategy formulation and implementation are intimately linked.

For most business processes, management observes what works and what doesn't, and continues implementing what works while scrapping the rest. The learning schools takes this a step further, allowing individuals anywhere in the organization to contribute to strategy. Great initiatives can come from anywhere within the organization and then be authorized by management.

Learning occurs when our plans are interrupted. Mistakes both in planning and in execution can help to optimize your company's strategy.

The tobacco industry, for example, learned to diversify their product offering after the first governmental restrictions against smoking. To cope, they began developing different products, like e-cigarettes, and even branched out into markets unrelated to smoking.

In addition, the learning school is closely linked to _chaos theory_, which holds that seemingly obvious relationships can produce totally unpredictable outcomes. Sometimes these unexpected outcomes lead to strategies that change the world.

The learning school freely admits that strategies are never final, and that constant adaption is necessary. But this comes with some inherent dangers.

For starters, without a fixed strategy, you run the risk of placing too much focus on optimization and learning instead of implementing the actual strategy.

Plus, learning is time consuming, and can be expensive when experiments don't pay off. So successful learning organizations are careful to prevent the kinds of _unnecessary learning_ that don't lead to the development of new or improved strategies.

Finally, the learning school is not the best in times of crisis, when there simply isn't time to test and learn, and a quick solution is necessary.

### 8. The power school sees strategy formation as a process of negotiation. 

So who gets to pick which strategy will be implemented when there are a number of viable options to choose from? Should it be consultants, or maybe executives? It's not always clear who should decide.

This question is at the heart of the _power school_, which looks at strategy as the butting of heads between the people who hold power in the organization as well as the organization itself.

There are two kinds of power: There's _micro power_, which represents individuals and groups within the company and their ability to influence decisions and strategy formation. Then there's _macro power_, which concerns an organization's dependence on its environment and other larger forces.

But the various holders of power may prefer different strategies. The power school is about the way an organization negotiates the wants and needs of micro and macro power.

The power school is thus highly realistic, because it doesn't prioritize a leader's vision over the reality of the competitive landscape.

There are many ways in which power can be negotiated, from the Darwinian process of "survival of the fittest" to a cooperative one that embraces more viewpoints. These power negotiations also take into account the power dynamic and cooperation between organizations within a network.

A good example of the power school in action is the way banks cooperate in managing shared ATMs. The power school helps banks within an ATM network deal with the politics and power dynamics that exist both within their network and outside it.

For all its plus points, the power school can also be too radical. Although it's important to consider the role of power and politics, the power school puts too much emphasis on them, which can serve to divide an organization and damage morale.

What's more, a focus on power can lead to ineffective tactical adjustments in response to low perceived power, instead of the development of broader strategy goals.

### 9. The cultural school focuses on the way collective processes lead to strategy. 

Like power, culture is always present. However, strategy schools are hesitant to address it. Not the _cultural school_, however. The cultural school focuses on all those social connections which create culture, in the hope of fostering efficiency and innovation.

The cultural school involves all the different departments within a company, and recognizes their contributions to the overall strategy. It formulates strategy using everyone's input while adhering to their culture, social behavior, values and preferences.

The larger a company gets, the greater its culture's influence becomes. This can make changes to company culture difficult to implement. For example, when companies merge during an acquisition, the employees that are used to the informal working processes from their old company may have difficulty adjusting to the new, highly formalized processes of the acquiring company.

We can see culture's role in strategy formation throughout history. The automobile, for example, was invented in Europe, where it was part of a culture of craftsmanship for special products, and was designed for the upper class. The United States had no such tradition, so car manufacturers reinvented the automobile as a mass product.

In other words, the American automobile strategy had to diverge from the historical precedent in order to succeed in a different culture.

One major problem with the cultural school is that it focuses so much energy on something nebulous and abstract. Focusing on something as vague as culture can lead to vague strategies.

It can also be difficult to implement cultural strategies. People don't like changes, especially when it comes to their culture, and are likely to resist them.

> _"A corporation doesn't have a culture. A corporation is a culture. That is why they're so horribly difficult to change."_

### 10. The environmental school looks at how external issues affect strategy formation. 

So far, the schools that we've examined have focused on a particular area as a way to formulate strategy, such as culture, planning and managerial vision. The environmental school is different, in that it takes a huge step back to look at the entire situation in which a company is situated.

For the _environmental school_, external forces are not a factor in strategy formation, they are _the_ factor. The environmental school makes a conscious effort to remove all choice from their strategy. Instead they simply let their environment shape the strategy.

Management, then, is constantly on the lookout for changes that occur, making small course adjustments or adapting in order to manage the specifics of new environments.

So what do they consider to be "the environment"? In essence, the environment is all things external to the company, such as stability, complexity, market diversity and hostility, and so on.

No one can or should deny that environment is an important factor to consider when formulating management strategy. But it can't be the _only_ thing you consider! For example, the environment alone can't explain how two organizations can be successful in the _same_ environment with totally different strategies.

What's more, the environmental school has no agency. By seeing companies as being mere puppets of their respective environments, the environmental school removes all choice from the organization

So far we've seen a good selection of both prescriptive strategies, whereby the plan informs the action, and descriptive strategies, in which circumstances inform action. The following blinks will look at strategies that successfully combine these two seemingly contradictory ideas.

### 11. The configuration school combines the best elements of the prescriptive and descriptive schools. 

By this point, you've probably noticed that these schools seem to position themselves in opposition to other schools. There is one school, however, that aims to combine their strengths: the _configuration school_.

For the configuration school, strategy formation is a process of transforming the organization from one strategic approach to another, and then transforming the organization itself. The school has two sides: configuration and transformation.

_Configurations_ are the stages of a company's development. While a company might be configured in a certain way for some time, that configuration will change later on in its development, thus changing the organization itself. Naturally, different configurations lead to different operations and approaches.

_Transformation_, in contrast, sees development in terms of time: time for cohesion and time for change. Most of a company's time will be spent in relative stability, with dramatic changes being the exceptions.

The configuration school tries to balance change and continuity. Since change is difficult to manage, this strategy is about maintaining continuity and knowing how to deal with changes as they occur.

While this school delivers a clear model of strategy formation and environment, it is precisely this clarity that is its weakness.

Basically, it's impossible to divide time into neatly defined periods of change and continuity. Rarely are there periods where _nothing_ changes or _nothing_ stays the same. The shades of gray in between are extremely important in strategy formation.

Like all the other schools, the configuration school is too simplified, and misses the nuances that define a company's circumstances.

Nevertheless, simplification often helps us understand what's going on. For example, thinking in terms of continents is a helpful way to organize the world. While grouping Australasia as its own continent might be an oversimplification, it nonetheless helps us order the globe in a meaningful way.

You've now reached the end of the safari, having seen all ten strategic management schools. Our final blink will help you to decide how to unravel all the pros and cons to find the school that's right for you.

### 12. While the schools each have their strengths, none is perfect – you’ll have to mix and match them. 

It's a difficult task to choose from ten different schools, and even more difficult to stick to only one — so maybe it's better to stay flexible!

Interestingly enough, there are some threads that run through all of these strategic management schools. For example, all the schools deal with control and change.

The schools differentiate themselves in their overall approach to formulating strategies, but there is really no need to dedicate yourself to a single school. Instead, it would be best to use each school when it is most relevant. In times of stability, for example, it might be best for you to employ the learning school, while the design school could be more appropriate in times of uncertainty.

Indeed, the most rational approach to selecting a strategy is to think in terms of your past, present and future circumstances, rather than stick to generalities and theories. You're free to start with one school of strategy and then switch over to other ones as circumstances require, or use other schools as a framework to help brainstorm new strategies.

In other words, all the schools are actually part of a single process: _strategy formation_.

Strategy formation encompasses all ten schools as well as the ones that have yet to be developed. The key is to get beyond the confines of each school and appreciate the wealth of ideas about strategy formation.

In fact, the entirety of strategy formation is an enormous field, far too large to comprehend. But understanding the pros and cons of all ten schools will help you make a start!

### 13. Final summary 

The key message in this book:

**No school of thought about strategy formation is a one-size-fits-all solution. Each has its strengths and its limitations. By understanding these pros and cons, however, you can formulate a winning strategy for your organization.**

**Suggested** **further** **reading:** ** _Competitive Strategy_** **by Michael E. Porter**

_Competitive Strategy_ presents a thorough examination of the nature of industry competition as well as the common strategies that successful businesses employ to get ahead. It not only offers valuable insights into how to compete in the market, but also reveals how companies can use their competitors' information to best them at their own game.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Henry Mintzberg, Bruce Ahlstrand and Joseph Lampel

Henry Mintzberg is an author and scholar of business and management. In addition, he is a professor at McGill University in Montreal and has written several books on management and strategy.

Bruce Ahlstrand and Joseph Lampel are academics in the field of strategy formation.

