---
id: 59f65941b238e10006f60786
slug: streaming-sharing-stealing-en
published_date: 2017-10-30T00:00:00.000+00:00
author: Michael D. Smith and Rahul Telang
title: Streaming, Sharing, Stealing
subtitle: Big Data and the Future of Entertainment
main_color: 25ACB7
text_color: 16656B
---

# Streaming, Sharing, Stealing

_Big Data and the Future of Entertainment_

**Michael D. Smith and Rahul Telang**

_Streaming, Sharing, Stealing_ (2016) is about the ever-changing entertainment industry. Recent years have seen the emergence of new players who continue to utilize technology to transform the landscape. This book assesses how companies like Apple, Netflix and Amazon use data to understand their consumers' needs.

---
### 1. What’s in it for me? Learn how technology has disrupted the entertainment industry. 

In 2012, Kodak filed for bankruptcy after dominating the film and photographic industry for most of the twentieth century. The former corporate behemoth had failed to adapt to the new digital photography trend. Although Kodak had been aware of the seismic shift and even developed their own digital photography technology, they were afraid of cannibalizing their successful analog film business. As a result, they never gained a footing in the new market and quickly dwindled.

There were no more Kodak moments.

The moral of this story is that failing to adapt to new technology can be dangerous. During the last decade, many major players in the entertainment industry have failed to heed this warning. And many have seen themselves outrun by tech-savvy startups. So, how have these new companies managed to wrestle power away from the big players? These blinks explain.

In these blinks, you'll learn

  * what six major music executives called "shit" in 1997;

  * why over half a million people binge-watched the first season of _House of Cards_ ; and

  * what piracy did to the Indian movie industry.

### 2. Technology has changed the power dynamics of the entertainment industry. 

A while ago, CDs were widely used to play music. Nowadays, you rarely see them, save for a small minority who collect them for nostalgic purposes.

What led to their demise? Big players in the music industry ignored the potential of digital music, which allowed companies such as Apple, Pandora and Spotify to emerge and kill off the CD.

In 2003, AT&T was the first company to attempt to introduce digital downloads with its division A2B Music. Its cofounders pitched the concept to top music executives but they were met with ridicule, as growing CD sales were creating so much revenue. A2B Music quickly dissolved, but in the early 2000s, companies like Apple began to flourish using the same technology.

Soon, digital music became popular, and CDs became irrelevant.

Encyclopedia Britannica suffered a similar fate. In 1985, Microsoft requested non-exclusive rights to the company so they could digitize the contents of its encyclopedias. Personal computers were growing in popularity at this time, but the print-based business wasn't convinced. The repercussions of this decision would be felt in 1993; that year, Microsoft created a multimedia encyclopedia called Encarta, and sales of print encyclopedias plummeted by $110 million as a result.

A modern-day company which harnesses the power of technology, is Netflix. It uses data analytics to truly customize the user experience. Formerly, production companies created pilot episodes of new shows and aired them to network executives. If the pilot received a positive response, the network requested more episodes — if not, the show was rejected.

When the creators of _House of Cards_ pitched their show to Netflix, the company ordered two seasons for $100 million without seeing the pilot. The Netflix team had thoroughly analyzed their user data and knew _House of Cards_ would be a hit with their audience. The show's creators were further incentivized to exclusively release both seasons on Netflix because, as a digital platform, it didn't have the same airtime limitations as TV networks.

### 3. New technology gives artists creative freedom, reduces content creation costs, and offers consumers flexibility. 

In the past, only a few companies had the money to create content and facilitate new artists. If you were a creative, your vision was determined by the entity in control of the budget.

However, newbies like Netflix have given artists and directors free rein.

The first episode of _House of Cards_ illustrates this. The show begins with an injured dog being strangled to death. Before, the scene wouldn't have made it past network executives fearful that risky content could result in the loss of advertising money. Netflix doesn't feature ads at all, so the show's creators had greater liberty.

Showrunner Beau Willimon didn't have to split the series into 30- or 60-minute slots with cliffhangers in each episode either. Instead, he was able to write the show as if it were a 13-hour film.

Viewers also benefit from the way Netflix operates as they can binge-watch at any time of the day. The company doesn't restrict its consumers to watching episodes at specific times — 670,000 people binge-watched the entire second season of _House of Cards_! This is similar to how YouTube works — you can watch videos about any topic, and you can access them whenever you like.

Artists also benefit because expensive equipment is no longer necessary. The Academy Award-winning documentary _The Lady in Number 6_ was shot using a Canon EOS 5D Mark III, which cost only a few thousand dollars — a lot less than a traditional movie camera.

Companies like YouTube directly try to help artists save money on production. The platform offers YouTube Spaces to those of its users with over 5,000 subscribers. These facilities provide access to editing, makeup, lights, design, videography and equipment.

### 4. Companies capture new markets by sensing opportunities early and moving in quickly. 

The entertainment business is constantly changing, so the real challenge for industry leaders is to perceive these shifts at the right time, understand their scale and respond effectively.

The story of Thomas Edison and his musical invention is a prime example of a businessman failing to comprehend an evolving market.

In 1877, he created the _phonograph_ — a machine that could record sound onto a tinfoil-covered cylinder. He patented his invention and moved on to developing the electric light bulb.

However, others continued to modify his idea until the _graphophone_ came into existence; this was a far superior device which used wax cylinders to record sound.

A rich businessman bought the rights to both the graphophone and the phonograph, but his company eventually went bankrupt. Edison bought back the rights to his machine and planned to develop it for jukeboxes in amusement parks.

Later, in 1887, Emile Berliner patented sound recording on discs called _records_. They were easy to mass produce and store, so his business quickly proved to be extremely lucrative. Unfortunately for Edison, he chose to stick with his cylinders, while Berliner's records dominated the music industry.

In contrast to Edison, small indie labels of the 1950s judged their market correctly and benefitted from the emergence of rock 'n' roll. The big music companies saw rock and roll as a fad, or a niche interest for teenagers who didn't have much purchasing power. Influencers of the period like _Time magazine_ and Frank Sinatra also openly ridiculed the genre. 

However, Alan Freed, a prominent disc jockey of the era, championed rock and roll and predicted its widespread popularity. As independent labels had nothing to lose, they collectively embraced the genre. A decade later, 42 labels had rock 'n' roll records in the charts.

### 5. For a while, big companies tightly controlled the distribution of entertainment. 

Today, the entertainment industry is more diverse — small companies exist alongside bigger businesses and still have a broad reach. However, this wasn't always the case.

Throughout the twentieth century, a handful of companies used their economic power and size to dominate the industry. During the 1950s, radio was essential to the music scene. In exchange for airtime for their artists, big labels would promise radio stations perks like backstage access, free concert tickets and various freebies. Smaller labels didn't have the ability to do this.

Similarly, the movie industry was presided over by six businesses: Disney, Fox, NBC, Paramount, Sony and Warner Brothers. Together, they controlled 80 percent of the market because they operated on such a large scale. In the publishing field, the book trade was controlled in much the same way by Penguin, Random House, Macmillan, Harpercollins, Hachette and Simon and Schuster.

Money facilitated this domination. Major players like Universal Music had the capital to take a risk on a range of artists, as one lucrative musician could make up for multiple failures.

Financial strength also attracted top talent. A gifted artist was easily tempted away from working with an independent label when offered a sizable contract. In turn, labels associated with successful artists naturally enticed fresh talent.

Before the internet, leading labels were also able to control the distribution of music because they could ensure maximum visibility for their artists by paying physical stores. In the 1990s, most outlets only carried around 3,000 to 5,000 albums — even the largest superstores held a maximum of 15,000. To promote their talent, these labels offered in-store interviews and advance album copies.

Altogether, these advantages led big labels to perceive themselves as invincible. To their detriment, they failed to consider that technological advances could diminish their hold over the industry.

### 6. Publishing platforms draw in consumers by providing online access to niche products. 

There's a limited edition vinyl that was pressed in the 1980s that you're desperate to add to your collection. In the age prior to the internet, you'd have to put in considerable hours to source such rare or less popular titles. Now, of course, it's much easier to both access and download records, books and other works online.

In fact, it seems that the more obscure, the better. Unconventional artists tend to attract large, loyal audiences, and people are willing to pay small fortunes for rare titles. One of the authors of _Streaming, Sharing, Stealing_ was prepared to do this while on the hunt for a 30-year-old pharmaceutical book. He was struggling to locate the book in a physical store, but by doing a quick search on Alibris — an online store for new, old and out-of-print titles — he was able to track down a copy. The book was his for $20, though he'd have been willing to pay more. When it arrived, $0.75 was written inside the front cover; the book had been sitting in a bookshop for years without attracting a single customer.

So online stores provide an easier method of searching and more variety than offline establishments.

To test this theory, the brains behind _Streaming, Sharing, Stealing_ teamed up with researchers Alejandro Zentner and Cuneyd Kaya to study data from both the physical and online stores of a large video rental chain.

They discovered that rentals of the 100 most popular DVDs made up 85 percent of transactions in-store but only 35 percent of online transactions. But did the online customers tend to go for obscure titles because they were shopping online rather than in a physical store?

To ascertain this, the group looked at how an individual's consumption pattern shifted when one of the chain's local stores closed. The consumer was forced to switch from the limited in-store selection to the extensive online array. They found that this person was then more likely to rent unconventional titles over blockbusters.

> Facebook averages a thousand A/B tests every day.

### 7. Data-driven processes allow publishers to create more effective marketing campaigns. 

For a while, companies relied on good old intuition to determine which content would appeal to certain audiences. However, it's since been discovered that companies which utilize their customer data make better creative decisions.

By making use of _big data_, a company can bolster its marketing efforts. The app-based company Shazam is a prime example of this. The business works by allowing users to identify any track they hear. As it performs millions of searches each day, it's built a competitive advantage by documenting all of this information.

Due to the predictive power of Shazam's data, it's proven highly popular with many music agents. In February 2014, the company announced that it would be using its data to produce music for a new Warner Music Group imprint.

User data also proved to be exceptionally useful when it came to promoting _House of Cards._ In order to target different customers, separate trailers were produced which utilized information collected about the show's viewers. Visuals were created which focused on Kevin Spacey, aiming to appeal to those who had watched the actor's other work. Similarly, trailers featuring David Fincher's directing style were made to cater to those familiar with his aesthetic.

The decision to use data-driven processes to create content has resulted in more recognition for online platforms, as well as numerous award wins. As a company, Amazon relies on data-driven processes to inform its creative decisions. At the 2015 Golden Globe Awards, it was up against networks such as HBO and The CW but still won the Best Comedy award for its show _Transparent._

Netflix is also winning awards for its creative, data-driven content. In 2016, the company received eight Golden Globe nominations — more than any other television network that year. Netflix had succeeded in ending HBO's 14-year winning streak, proving that the importance of user data shouldn't be underestimated.

### 8. Piracy harms producers and consumers, but the issue can be prevented. 

It's no secret that the advent of the internet brought several benefits. However, one downside of this technological feat is the proliferation of piracy — but there are ways to curtail it.

Piracy is so detrimental because it allows consumers to receive content without paying. In 2015, the authors of _Streaming, Sharing, Stealing_ presented a paper about the issue to the World Intellectual Property Organization. To compile this document, they surveyed all the peer-reviewed journal articles they could find that discussed the effect of piracy on sales. Of the 25 articles they unearthed, 22 reported instances in which piracy had significantly harmed sales.

The Indian film industry in particular has suffered huge financial loss because of piracy. Back in 2014, the authors worked with the economist Joel Waldfogel to compare data on the industry gathered before 1985 and between the years 1985-2000. They discovered that the revenue generated declined rapidly as VCR-based piracy emerged. They also found that there was a significant decrease in the amount of movies produced in India post-1985 and that their quality also depreciated.

In order to prevent piracy, consumers have to understand the risk of committing the crime. In 2012, the authors studied the effect of an anti-piracy law passed in France in 2009, which meant that notices about copyright infringement were sent to pirates.

The fact that the law solely affected French consumers permitted the authors to examine its effect on iTunes sales patterns in the country. They ascertained that when it was legally dicier to commit piracy, many consumers switched to legitimate consumption. The data also showed that the law caused a 20 to 25 percent increase in sales. 

Therefore, as damaging as piracy can be, entertaining content can still be protected and shared fairly. It's vitally important to do so, as piracy can cause artists and producers to lose their incentive to create new, high-quality work, causing consumers to ultimately lose out in the long run.

### 9. Final summary 

The key message in this book:

**Since the twentieth century, the entertainment industry has changed significantly. The advent of the internet has meant that it's easier to source and share content, and big companies no longer have a monopoly across the board. To succeed nowadays, businesses need to make use of user data and ensure the prevention of piracy.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Everybody Lies_** **by Seth Stephens-Davidowitz**

_Everybody Lies_ (2017) is about the data collected in vast quantities by computers and over the internet. This data can help reveal fascinating information about the human psyche, behavior and quirks, because, as it turns out, people aren't always so willing to communicate their true hopes and desires to others.
---

### Michael D. Smith and Rahul Telang

Michael D. Smith and Rahul Telang are professors at the Tepper School of Business and Carnegie Mellon University, where they co-direct the Initiative for Digital Entertainment Analytics. Over the course of their careers, they've been interviewed by _Forbes_, _NPR_ and _Fortune_, and have featured on _Talks at Google_.

