---
id: 5714b456344e1b0007a4c852
slug: strengthsfinder-2-dot-0-en
published_date: 2016-04-21T00:00:00.000+00:00
author: Tom Rath
title: StrengthsFinder 2.0
subtitle: None
main_color: F55243
text_color: C24135
---

# StrengthsFinder 2.0

_None_

**Tom Rath**

In _StrengthsFinder 2.0_ (2007) you'll learn how to identify your skills and develop them to your advantage. Tom Rath presents a powerful framework to both cultivate your potential and match your strengths to your profession.

---
### 1. What’s in it for me? Discover your innate strengths and learn to leverage them at work. 

When you think about self-improvement, do you mostly think about building on your strengths or working on your weaknesses?

Most people are obsessed with "fixing" habits and character traits they're not happy with. This is important, but all too often leads to our completely ignoring what we're already good at: our strengths. Identifying our strong points helps us better leverage them professionally, leading to better performance.

And guess what? Focusing on what you're good at is a lot more fun than trying to mend every chink in your armor.

Thinking along these lines, the author has identified 34 key strengths that people can exhibit. In these blinks, we'll take a look at a few of the most important ones.

You'll also learn:

  * what strengths best describe you and how you can best put them to use professionally;

  * why startups need both idealists and dreamers; and

  * why Achievers are at a high risk of burning out.

### 2. Achievers are driven by a constant and insatiable need for achievement. 

When the alarm clock rings in the morning, most people just want to hit the snooze button and roll over. But some wake up with such drive that from the moment they open their eyes, they already feel they're behind on their day.

Meet the _Achievers_.

Achievers are people who are so driven they can't take a day's rest. Though these people often overwork themselves, they do it for good reason: tackling new tasks is what makes them happy. This powerful drive allows them to remain highly motivated and productive despite the grueling hours they often work, and pushes the pace of the whole work environment.

Take, as an example, a top lawyer: she puts everything she's got into each case, staying in the office later than everyone else, every day.

However, there is a danger to being an Achiever: they can get so caught up in the next goal, that, despite being the best in their field, they always go home feeling they haven't done enough. So if you're an Achiever, how can you do your best without burning out? 

First, remind yourself regularly of how much you've achieved in the past. Think back to times when you accomplished a challenging task, or overcame a challenging obstacle. This way, you can continue to compete with yourself, but remain grounded, thanks to your past achievements.

Next, place yourself in a work environment where you can set up a s _coring syste_ _m_ to keep track of your success. This will give you an objective way of measuring productivity, instead of comparing yourself to your peers, and will also allow you to reinforce your sense of achievement at the end of the day.

Finally, always remember to channel your drive toward the tasks _you_ can do best, rather than tackling anything and everything just to tick things off your list. This will both save you energy and create the most value for your company.

### 3. Believers possess enduring core values; Futurists can imagine the future. 

We all know someone who never goes against his beliefs. And while this stubbornness can block progress in some situations, a _Believer_, when in the right environment, can bring a lot to an organization.

Indeed, for Believers to flourish, they need to find work that aligns with the values they believe in; for them, there's no difference between belief and motivation. 

Say someone believes in making the world a better place. They wouldn't fit in a typical profit-seeking company, but would be a powerful force at an NGO or firm with a compassionate mission.

Believers often can't find an organization that expresses their particular beliefs, which pushes them to start their own enterprise. And they do this best if they find a partner who is a _Futurist._

Futurists are people who can harness their powerful imagination to create inspiring visions of the future. By questioning the limits of what is possible, they paint vivid pictures of an inspiring future that can drive others to create superior products, work more effectively in teams and even change the world for the better. 

This makes them the perfect partners for Believers, or anyone starting a new enterprise. Futurists give form to idealistic visions in a way that energizes entire organizations.

### 4. Commanders like to take charge, while Developers are happy to encourage others. 

Is there someone in your workplace who is never afraid to share his opinions? Someone who always wants to take the lead?

Then you've met a _Commander_. These people are often invaluable when a decision needs to be made; however, if they're not careful, they can also create resentment and fear among the workforce.

Commanders tend to be brutally honest about how they see things, an intimidating habit. If _you_ are a Commander, try softening your approach. This may turn a potential weakness into an undeniable strength.

Rather than always imposing your view on others, build a reputation as someone who calls it like he sees it. Manage that, and in times of crisis, you won't have to impose your will; people will simply look to you for a frank assessment of the situation.

So Commanders get things moving by directing others. _Developers_, on the other hand, concentrate on encouraging others and transforming their potential into reality. A Developer will get excited when someone else makes progress, and support those who've lost faith in their abilities. This desire to help others means Developers are particularly well suited for nurturing roles, like coaching, teaching and HR.

Here's a good piece of advice if you're a Developer: to boost people's skills, praise something specific about their behavior, so they know exactly what to do more of.

### 5. Harmonizers excel at finding common ground, and Responsibles strive to deliver what they promise. 

While Commanders relish the opportunity to show that their opinion is best, _Harmonizers_ strive to make sure everyone agrees.

This desire to establish consensus means Harmonizers do well in work environments where everyone shares the same fundamental values and vision — so, jobs where teamwork is paramount. They usually don't thrive in highly competitive sectors like sales or politics, where individual achievement is valued above all else.

But even in a solid team, the Harmonizer tendency to equally consider all opinions can backfire, and lead to disharmony.

This happens when they waste too much time listening to everyone's wishes, which slows down proceedings, and often frustrates the team. So if you're a Harmonizer, here's a tip: learn when to pause and make sure everyone is on the same page, but always keep an eye on the clock.

Harmonizers have quite a lot in common with _Responsibles_ — those who hate to let people down.

Both seek strong feelings of social bonding, but instead of creating a group consensus, Responsibles do it by striving to never let anyone down. This dedication to fulfilling their duty makes them best suited for roles that need someone highly dependable.

However, this drive to fulfill the expectations of others also puts them in danger of taking on too much work. So if you're a Responsible, you need to balance your schedule by saying "no" every now and then. Otherwise, you might get overworked, start making mistakes — and become irresponsible.

### 6. Ideators love creating ideas, and Learners love the process of accumulating knowledge and skills. 

Are you the kind of person who is always drawing connections between dissimilar things?

If so, you're definitely an _Ideator_.

Ideators are always looking for new ways of seeing things. They intuitively bridge the gap between disparate fields; this gives them a broad understanding of the world that they can share with others.

If you're an Ideator, try to find work that will pay you for your strength: idea-dependent fields like marketing, writing or academia. Also, remember to take on jobs that you're really interested in, because Ideators tend to get bored fast.

Finally, boost your ideation powers by learning what exactly helps you make connections. Do you get ideas while reading, listening or talking? Reproduce these situations as much as possible, and watch the ideas bloom.

While Ideators love new ideas, _Learners_ love the possibility of acquiring new knowledge and skills.

For Learners, the pleasure is in the process, not the facts they acquire. They relish the journey from ignorance to mastery, no matter what they're learning — be it Spanish, Judo or how to chop wood.

If you're a Learner, make the most of your hunger for learning and reach for well-paid consulting positions. There you'll be paid to enter unfamiliar situations and quickly learn new languages, competencies and skills. You should also ask your organization if they will subsidize your learning — by, say, covering certification costs or granting scholarships for skills training.

### 7. Strategists look for analytical solutions to problems, while WOOers use the power of persuasion. 

When you hear the word _Strategist_, you may think of something warlike, such as a general. Well, there are Strategists in times of peace, too. Indeed, some people are born to strategize.

They can imagine many different paths of action, and foresee the potential obstacles that will cause confusion and resistance. So they eliminate all the bad routes until they find the one that will work best: the optimal strategy.

But Strategists sometimes don't foresee all the things that may get in the way of their own success.

For example, some people interpret confident strategic thinking as an attack on their own ideas. So if you're a Strategist, practice delivering your ideas gently, so you can get your message across without hurting anyone's feelings.

On the other hand, some Strategists don't voice their opinions forcefully enough, which can result in a lot of wasted time. Especially at the beginning of a project, it's crucial that you say what you have to say to prevent the project from going down the wrong path.

Strategists tend to be dry and analytical. On the other end of the spectrum are _WOOers_, who use their warmth and friendliness to _Win Others Over._

They imagine the world as a giant group of friends they've yet to meet, and their charm enables them to get everyone on their side. WOOers don't leave when the day is done: they jump on the chance to build their network and stay around to build friendships with their co-workers. Their extraordinary social skills make them invaluable for socially focused roles like public relations and media management. 

But sometimes their desire to know everyone gets in the way of creating deep connections. So if you're a WOOer, be sure to partner with co-workers who have a lot of empathy. Then you can introduce new acquaintances to your empathic co-workers, who will forge a deeper relationship with the newcomer that, in the long run, will enrich your company.

### 8. Final summary 

The key message in this book:

**Our work culture, and society in general, puts too much emphasis on the idea of overcoming our weaknesses and fighting our flaws. These blinks paint an alternative picture, where everyone has his or her own innate strengths that can be harnessed to truly fulfill each person's full potential.**

Actionable advice:

**Look for the strength in others.**

Next time you get frustrated with a co-worker's seemingly inexplicable approach to projects, work or life, try to identify which strength they might posses. This will lead to deeper understanding and help you both move toward a more productive working relationship. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Are You Fully Charged?_** **by Tom Rath**

_Are You Fully Charged_ (2015) is your guide to eliminating your off days, one positive interaction at a time. From socializing more to sitting down less, these blinks reveal easy-to-implement tips and tricks for generating the mental and physical energy you need, all by finding greater meaning in your life.
---

### Tom Rath

Tom Rath is a bestselling author who specializes in innovative business thinking. His other books include the number-one _New York Times_ best sellers _How Full Is Your Bucket?_ and _Strengths-Based Leadership_.

