---
id: 5950a635b238e10005bb4932
slug: stretch-en
published_date: 2017-06-29T00:00:00.000+00:00
author: Karie Willyerd, Barbara Mistick
title: Stretch
subtitle: How to Future-Proof Yourself for Tomorrow's Workplace
main_color: FBC96C
text_color: 7A6235
---

# Stretch

_How to Future-Proof Yourself for Tomorrow's Workplace_

**Karie Willyerd, Barbara Mistick**

_Stretch_ (2015) is about the art of growing your career in ways that will make you an in-demand force in an increasingly unpredictable economy. There's very little you can take for granted in these chaotic times, but there are reliable methods you can use today to help ensure work for you tomorrow. No one wants to be left behind. Be proactive and make yourself indispensable no matter what the future has in store.

---
### 1. What’s in it for me? Stretch your career in the ways you want. 

There's nothing as enjoyable as having a good long stretch and yawn after a hard day's work. And the pleasures of stretching needn't be limited to the physical realm. At work, you can stretch your mind by looking for creative solutions, being proactive about developing tasks that really interest you and interacting as much as possible with your coworkers and, during breaks, with other friends and acquaintances.

Though fun, stretching is not always easy. Many office workers end up a crumpled, dispirited heap, gazing listlessly at their computer screens without any idea of what to do. It's almost as if they're just waiting to get fired. So how do you avoid this and become an alert and excited person instead?

In these blinks, we'll explore

  * how to remain open and motivated in the workplace;

  * how to multiply career options and opportunities; and

  * how to bounce back strong after a career setback.

### 2. To keep stretching your work possibilities, maintain control, keep your options open and set goals. 

Before entering the professional world, you are surrounded by parents and teachers who can point you in the right direction and straighten you out if you get off track. But after college, you're more or less on your own, even though there's still a great deal of growing, or _stretching_, that needs to be done.

That's why there are three _stretch imperatives_ that every professional hoping to stay at the top of the game should keep in mind.

The first stretch imperative is to remember that _you_ are in control.

If you're unhappy with a job, it can be easy to blame your boss or be too lazy to do anything about it. But this isn't good for you or your employer.

Let's look at a typical career: You graduate from college and decide to pick up a job in construction because you thought you might enjoy working with your hands. But after a while you realize that you'd rather work with people. What do you do?

You take control and find another job, this time as a camp counselor. But here you realize you'd rather work with adults instead of kids, so you decide to work in sales instead.

And this leads us to the second stretch imperative: Give yourself options and broaden your horizons so that you have a healthy amount of possible opportunities.

Let's say you hate your boss in the sales department but you don't have the qualifications for anything else. Instead of feeling sorry for yourself, you take action. You sign up for adult education classes and earn a second or third degree, such as an MBA.

And this brings us to the third stretch imperative: Setting goals.

These don't have to be related to your career; they just need to be realistic goals that will help you stay on track, especially during tough times.

Everyone hits a rough patch at one point or another in their career. When you hit one, remember that gaining experiences is always productive and will help you in the search for what's right for you.

> _"We believe you have the power to change from disengaged to engaged."_

### 3. Be open to learning on the job and listening to feedback. 

If you've worked at enough office jobs, then you're surely familiar with the unhappy employee who dozes off at his desk, despite a veritable drip feed of caffeine.

No one wants to be this employee. In fact, the best kind of employee is the polar opposite of dissatisfied and slumberous. She's alert and engaged, learning on the job and continuing to develop her skill set.

If you have a full-time job, then you know how hard it can be to continue your education in the small amount of spare time you do have. That's why you must learn on the job.

Let's look at Jonah, who got a job as a teacher, but began to worry after realizing how difficult his work really was. He knew he needed to improve, but he had no time to get more training. So he took matters into his own hands.

Jonah performed a self-assessment and realized that he needed to be more organized, so he began interviewing other teachers on how he could better structure his class.

He also recognized that too much class time was spent with him talking, and that students learned more when they had time to practice. So he shortened his lectures and created more practical exercises that allowed his students to develop their skills.

The results were great: Jonah is now training other teachers and on track to becoming the next school principal.

The key is that Jonah was open to change and feedback, a quality that is essential to keeping yourself from getting stuck in a career rut.

Chris, a frustrated executive trainer who was having trouble connecting with his trainees, is another good example of the benefits of openness.

Despite the trouble he was having, Chris remained open with his boss and receptive to the feedback he received.

This allowed Chris and his boss to work together and pinpoint the problem. Eventually it became clear that Chris wasn't listening to his trainees. He'd developed a habit of criticizing their ideas before they could fully explain themselves.

By being open to change and feedback, Chris learned that being a leader isn't about him — it's about being part of a team. And once he began listening to his team members, his leadership skills greatly improved.

### 4. Job opportunities come with networking and gaining new experiences. 

It's generally a good idea to give yourself options. Most people don't apply to only one college or send out applications to only one job. They put their eggs in multiple baskets.

Use the same approach for your career and increase your options by utilizing a broad network of contacts.

Everyone has at least two different kinds of contacts: professional colleagues and personal friends. Both can be used to provide more options as your career progresses.

For example, Zach started out in a marketing company, working day and night and never taking the time to develop a social life or expand his contact list. And when Zach decided to strike out on his own, he was working even longer hours, but still not giving any attention to his contacts — even though he was growing quite concerned about his lack of clients.

Luckily he had one good personal contact — his dad, who was an experienced entrepreneur and recognized his problem right away. He explained to Zack the importance of having good contacts, especially in the world of sales.

Sure enough, after focusing on his network and sharpening his social skills, Zach's company began to thrive.

Another way to increase your career options is to collect a variety of experiences.

You may not have a clear idea of what you want to do with your life, so why not try a number of different jobs and discover what's out there?

Alexandra did exactly this after earning a graduate degree in international relations and feeling disillusioned after working for a financial service company during the 2008 economic crisis.

As Alexandra began to reflect on what she wanted to do with her life, she realized she had a real passion for nature and conservation. She was drawn to the Lewa Wildlife Conservancy in Kenya, and began reaching out to people from the organization and adding them to her network.

Those within Lewa recognized that Alexandra's experience in finance and international relations could be a great benefit, so they hired her as an international-development manager.

And with Lewa on her résumé, Alexandra was able to add nature conservation to her growing list of experiences.

### 5. Use career setbacks to bounce forward, and don't lose sight of your goals. 

When your career hits rock bottom, one of two things can happen to it: it can either shatter like a glass or bounce back to new heights like a rubber ball.

With the right frame of mind, you can turn a career setback into the start of a new chapter.

If you lose your job, there's no reason to give up — especially if you have your network in place and remember that you're still in control. So keep in mind that any setback can be an opportunity to gain experiences and propel forward movement.

Even when things look bleak, you can come soaring back. All it takes is the right amount of positive determination.

When journalist Jill Abramson was run over by a truck in New York City, she had to face an enormous uphill battle to learn how to walk again. But that's exactly what she did, because she never gave up her belief that she could accomplish the goals she set for herself.

And not only did she get back on her feet; she went on to become the first female executive director of the _New York Times_.

To help you hold on to your goals when times are tough, here are three "ounces" of advice:

The first is to _pounce_. You've got to keep learning and takings risks. Don't worry about looking dumb or falling on your face. It's far more important to stay curious and always be asking questions.

The second is to _trounce_. Keep practicing and developing your skills no matter how many times you fail. It's not about reaching perfection, it's about continuing to grow and knowing that sooner or later your continual practice will pay off.

The third is to _announce_. Share both your goals and difficulties with your peers. This will keep you focused on reaching those goals as well as on improving yourself. After all, it's easy to let a goal slip away if you're the only one who knows about it, so improve your chances of following through by letting the world know what you're aiming for.

### 6. Your career tasks will change, but emotional intelligence will prepare you for any future. 

It's common for people to have anxieties about an unpredictable future, but when you take the right steps toward becoming aware and prepared, you can rest easy.

One thing's for sure: Whatever career path you are currently on will change within your lifetime.

Everything about the traditional workday is changing. The hours are becoming more flexible just as companies are becoming less tied down to physical locations. Even universities are responding to the constant changes by regularly updating their programs to match emerging fields.

Indeed, both universities and corporations know how important it is for adults to keep learning new skills.

Georgia Tech, Udacity and AT&T, in an effort to keep the US workforce up to date and competitive, have pooled their resources to create a relatively accessible master's degree program in computer science that only costs $7,000.

Workplace hierarchies are flattening out these days, so it'll also be to your advantage if you can show employers that you come with your own leadership skills and the ability to turn visions into reality. Ideally, this should also include a strong capacity to collaborate and work with social media.

But being prepared for the future requires non-technical skills as well.

You shouldn't underestimate emotional intelligence as a key part of your continued development, which means learning how to accurately understand and respond to the emotions of others.

After all, it's not just the economy that's changing; people are changing, too. Lady Gaga recognized this a few years ago. In 2012, she launched the Born This Way Foundation to promote emotional intelligence, well-being and kindness in schools.

So if you really want to secure your place in tomorrow's workforce, develop a sharp emotional intelligence. It will allow you to explain how your personality complements your skills, making you an indispensable asset for any company.

> _"Only 50 percent of the employees from our survey believe the skills they have today will be the skills they need just three years from now."_

### 7. Final summary 

The key message in this book:

**In order to stay competitive in the future, you have to keep learning today. So stay current and observe the changes going on around you. With the right perspective, you can stay ahead of the curve while simultaneously collecting contacts, experiences and furthering your education. With this foresight, you'll be in a good position no matter what lays ahead.**

Actionable advice:

**Become helpful instead of doleful.**

If you don't feel like you're being useful at your workplace, don't just sit there being depressed. Find at least one thing that you can do to make someone's life a little easier at work. If you're not sure how to do that, observe your coworkers with genuine interest and curiosity. This will help you develop empathy, grow your network and gain new inspiration for ways in which you can be more engaged and happy at work.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Thrive_** **by Arianna Huffington**

_Thrive_ argues that it's time for society to stop thinking of success only in terms of money and power, and redefine it altogether. If we want to truly thrive in our professional and personal lives, we have to create room for well-being, wisdom, wonder and giving as well.

### 1. What’s in it for me? Discover your hidden potential. 

People tend to equate success with getting more. It's taken for granted that bigger is better. As a result, we spend our lives chasing rewards, trying to accumulate resources and desiring what others have.

There are obvious drawbacks to this outlook when it comes to happiness and peace, but the truth is it's not even a recipe for success. In reality, you're much better off focusing on what you can accomplish with what you've already got.

That's why you should adopt a new perspective. These blinks explain why you should forget about chasing, and start thinking about stretching.

In these blinks, you'll learn

  * how to stretch your resources to the limit;

  * why planning isn't necessarily good; and

  * how your self-expectations influence your success.

### 2. It’s easy to get lost chasing what others have. 

If you saw your neighbor driving home in a shiny new car, how would it make you feel? For most people, the sight might cause a pang of jealousy. After all, the entire human conception of success is based on having better stuff than others. This habit is called _chasing_ and it pushes us to pursue things we don't actually need.

For instance, researchers at Vanderbilt University found that people literally tend to perceive their neighbor's lawn as greener than their own.

It may be a cliché, but, in the popular imagination, a healthy green lawn means success and prosperity. The desire for such status symbols is in fact so powerful that people work tirelessly to have the best lawn in the neighborhood. However, the results are often for naught: Meticulously manicuring your lawn necessarily means diverting resources from what makes you truly happy!

So what's the solution?

Well, on the other end of the spectrum from chasing is what's called the _stretcher mindset_. In a nutshell, it refers to focusing on what you want to accomplish with the resources at your disposal.

In order to have a stretcher mindset, the first step is to feel assured that you're in control. For example, say you work in a clothing store. Retail can be soul crushing, but by simply imagining yourself as the owner of the store you can boost your confidence in an instant.

Just thinking that you're in charge of the circumstances will help you seize the reins and tap into your creativity.

Beyond that, staying mindful of the situation at hand and the resources at your disposal will compel you to be creative with what you've got. In other words, you'll have to recognize your limits and, once you know what you can and can't do, you'll be able to creatively pursue the options before you. That's why studies have found that teams with a deadline or budget produce better results than those with more open-ended parameters.

> "Stretchers find beauty and richness in places where others struggle to see anything of value."

### 3. Outsiders are often more innovative than experts. 

When making an important decision, whether it's choosing a new IT system at the office or a medical treatment for an illness in the family, people tend to place their trust in experts.

However, experts aren't always the most reliable people. In fact, traditional experts often don't have the best perspective. For instance, in 20 years of analyzing political issues, the psychologist Phil Tetlock found that experts aren't any better at predicting future events than your average person, regardless of their professional experience. Whether people were liberal or conservative, optimistic or pessimistic, the results were the same. In most cases, the experts and non-experts offered comparable prognoses.

In fact, professional experience can actually be a hindrance. As people gain experience, they also become stuck in their knowledge, unable to move past conventional approaches.

As a result, _outsiders_ tend to overshadow experts. Outsiders are newcomers to a field, people who lack deep experience. Naturally, you can't be a newcomer at all times, but you _can_ think like an outsider. It's as easy as following four steps:

First, explore the world and experience new things that help you see them from a different angle. For example, if you're a political expert, you might go to a musical, watch a cartoon or do something that's entirely unrelated to the political world.

But you also need to stay in touch with what you already know, which is the second step. Of course, you can't know everything about your field, so work to share your ideas with others, seeking feedback and learning as you go.

From there you can move on to the third step: looking for solutions outside of your own domain. The design company IDEO offers a good example. They have an open office in which employees hear about the problems and innovations of other departments, thereby inspiring them to find solutions to their own problems.

And finally, whatever ideas and innovations you _do_ develop, it's key to constantly test your assumptions, expecting that most of them will fail.

### 4. Planning may make us feel safe but it can also block out valuable information. 

Whether it's for a summer vacation or a work project, most people love having a plan. Planning offers a comfortable roadmap to follow, ensuring that we'll achieve the best results. But there's a downside, too. Planning can also cause us to miss out on all kinds of beneficial things.

Often, people learn more by acting than by planning.

For instance, the Stanford professor Kathy Eisenhardt found that executive teams who make more rapid-fire decisions tend to collect more information and generate more alternatives than those with long and detailed plans. That's because quick-acting teams focus on the present, working with real-time information about their work and competition. Planners, on the other hand, tend to spend most of their time and energy trying to figure out what the future might look like.

Beyond that, planning can cause us to miss valuable information that's right in front of our eyes. After all, burying your head in project planning can make it difficult to notice your surroundings.

Just take an experiment by the psychologist Malcolm Brenner. Brenner asked participants to speak and listen simultaneously. He found that, as it came time for a subject to speak, it was common for her to block out outside information. She directed her energy to what she was planning to say. The same thing happened as participants finished speaking. They would reflect on what they had said, missing how others were reacting to their words. Simply put, whether it's for a project or for what to say next, planning causes us to miss out on the world unfolding around us.

That's why, instead of planning, stretchers embrace improvisation as the key to progress. After all, when we improvise, we free ourselves up to move and use resources in the most efficient way. We can bend, stretch and position ourselves relative to challenges in the world, making the actions we take all the more effective.

### 5. Low expectations can hamper your relationship to others and yourself. 

People often act as we expect they will — a fact that can have a major effect on our interpersonal interactions. Expectations influence experience, and they can define relationships, too.

Our expectations are essentially images of people or things that we haven't yet encountered. Just imagine a new employee joins your company. You haven't so much as spoken to him but your coworker assures you that he's a jerk. When you _do_ meet this new colleague, you'll likely treat him differently than if you hadn't heard anything about him.

However, expectations shape more than just first impressions. They also direct feelings in ongoing relationships. Consider the example above. Now that you have a bad impression of your new officemate, you might send him signals that express these negative feelings. As you ask him rude questions or adopt a cold tone when speaking with him, you'll eventually lead him to view you as a jerk as well.

So expectations can damage your relationships with others, but the expectations you hold about yourself can also affect the way you handle challenging situations. That's because having lower expectations about your abilities will lead you to see challenges as threats, making it harder for you to rise to the occasion.

Such a mindset can have a pretty bad effect. Being scared or mistrusting your ability will lead to hesitation and passivity. As a result, when faced with a challenge — say, the chance to give a speech at a conference — you'll shrink away from the opportunity. Instead of welcoming a new experience, you'll block yourself and choose to remain passive instead.

But it doesn't have to be that way. Stretchers know that you can overcome such obstacles by developing positive self-expectations, leading to a transformation that allows you to see challenges as opportunities. If you regard that speech as an opportunity to demonstrate your skills at the podium, you'll not only take on the task, you'll also gain new skills and open up greater resources for future growth.

### 6. A dose of creativity and a willingness to collaborate can boost your success. 

Whether it's a morning coffee ritual or an evening stroll, everyday routines give you a structure that helps you accomplish more things in a shorter time. That being said, people also tend to see routines as boring and inflexible.

So how can you get the most out of your routine without letting it bog you down?

It's simple, actually: add a touch of creativity. This will help you transform the way you understand your routine and the way it functions. Stretchers do this on a regular basis by mixing up combinations in their routine work. In other words, you can do the same exact things every day and, as long as your individuality shines through, always feel that your work is new and exciting.

For instance, you could probably get your kid's lunch ready for school on autopilot. But if you adopt a stretcher mindset, you might think to add a little note to your son's lunch, wishing him a great day. This little touch of creativity will make him happier and get you out of your routine without disrupting your day.

Injecting a dose of creativity can do wonders for a stretcher and a similar approach can be used to challenge your assumptions about competitors. For most people, competitors are there to be beaten. But this outlook isn't necessarily conducive to coexisting with competitors. A better strategy is to treat them like friends.

Working to beat the competition may be a great motivation, but it's not as good as working together. Just take the professors Paul Ingram and Peter Roberts, both of whom studied hotel managers. They found that managers who were friends with their competitors brought in 15 percent more revenue than those without other manager friends.

These friendships positively impacted the work of the managers by giving them greater access to knowledge about their field and prospective clients. It's just one more reason to break down mental barriers, get creative and focus on building relationships.

### 7. Becoming a stretcher also means holding back. 

Now that you've got a sense of what it takes to be a successful stretcher, it's time for another important lesson: how to avoid _overstretching_. Without this knowledge about how to keep yourself in check, you'll risk injuring yourself or others. To stay safe, just avoid these common mistakes:

The first way that people tend to overstretch is by becoming a _cheapskate_. For instance, while carefully managing your resources is key to being a stretcher, there's a limit to how far you can go. Remember, the difference between being economical and being a cheapskate is that the former saves to invest in something meaningful while the latter is scared of spending any money at all.

The second risk is that of spreading yourself too thin or looking in too many directions at once. To avoid going down paths that have nothing to offer, you should make your career your number-one priority. Become a specialist in your field and learn everything you can about it. Once you've established that base, you can begin exploring new ideas.

Third, keep in mind that changes can often be helpful, but only after you've properly analyzed the past. Without this reflection, you risk _leaping without learning_. In other words, failure is a great way to learn, but only if you properly consider what went wrong. For instance, if your business goes bankrupt, you should certainly prepare to found a new one, but only after you understand why your first attempt went belly up.

Fourth, while you know now that having high expectations can foster confidence and courage, having _unrealistically high expectations_ is a recipe for disaster. Nobody expects to run a marathon after their first jog, so be realistic about your goals. Otherwise, you'll risk disappointment at every turn, no matter how much you succeed.

And, finally, when mixing up your routine, make sure that you strike a balance between novelty and usefulness. Without a good mix of the two, you'll be in danger of producing a _toxic mixture_ that will kill innovation.

### 8. A mental workout is part of maintaining a strong and focused mind. 

As people age, they start to learn that working out is necessary to avoid injuries, and the same goes for stretchers. But instead of hitting the gym, the ticket here is to work on balance, specifically between your existing resources and your exploration of new things.

Look around and identify the resources you have at your disposal but haven't used. This can turn up all kinds of gems. For instance, a recent Indiana University study found that the most important scientific papers were "sleeping beauties," papers that were published but forgotten.

Identifying your own sleeping beauties is crucial. You've got to ask the right question, namely, what personal resources, skills, connections or ideas have you left unused? Make a list of all these resources along with their potential to help you advance an objective and you'll have a good sense of where to focus.

Once you've done that, you're ready to explore new things. Just set aside a few hours each week to read something new, go to a workshop or spend time working with new colleagues. You never know what kind of ideas you'll stumble upon.

But flexing those balancing muscles won't be effective if you don't also take breaks to be grateful for all the hard work you've done. After all, too much concentration can be a disaster for creativity.

If you feel overwhelmed by your work, you should take a breather. Get out of the office. Spend some casual time with a client. Take an afternoon stroll. This last one is great because it's simple and effective. A group of Stanford University psychologists even found that people who take a walk during their breaks are 81 percent more efficient compared to those who sit.

Another great way to decompress is by spending time reflecting on what you're grateful for. This can be as simple as setting aside time once a week to write down five things in your life that make you feel gratitude. This will help you see the overall picture, and remind you what it is you're stretching toward.

### 9. Final summary 

The key message in this book:

**You have all the resources you need to be successful right in front of you, it's just a matter of identifying and prioritizing them. So, rather than chasing after what other people have, focus on what you've got and expand from there. Look at things like a newcomer, beware the pitfalls of planning, get creative with your routine, be sure not to overstretch and engage in some mental workouts. And when you need a breather, remember what you're grateful for.**

Actionable advice:

**Take a moment to reflect.**

The next time you find yourself rushing into a new project, take a few days off for a trip. Get out of your environment and break free from your routine. By taking this space, you'll be able to reflect on what you learned from your last project, how your plan went and whether having a plan for this project even makes sense.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Radical Candor_** **by Kim Scott**

_Radical Candor_ (2017) offers valuable tools that any team leader or manager can use to establish the best possible relationship with their employees. You'll find an insightful approach to management that creates a working environment where great ideas emerge, and individuals can reach their full potential. It's time to stop doubting yourself and become the kickass boss your employees will be proud to follow.
---

### Karie Willyerd, Barbara Mistick

Karie Willyerd, the workplace futurist for SuccessFactors, has been the chief learning officer for five different Fortune 500 companies. She is the coauthor of the bestseller, _The 2020 Workplace: How Innovative Companies Attract, Develop & Keep Tomorrow's Employees Today_.

Barbara Mistick is an experienced entrepreneur with an MBA from the University of Pittsburgh. She is currently the president of Wilson College and was previously the first female president of Pittsburgh's Carnegie Library.

© Karie Willyerd, Barbara Mistick: Stretch copyright 2016, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

