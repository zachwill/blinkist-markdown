---
id: 5918c2edb238e100066a4e4a
slug: string-theory-en
published_date: 2017-05-16T00:00:00.000+00:00
author: David Foster Wallace
title: String Theory
subtitle: On Tennis
main_color: 288743
text_color: 206E36
---

# String Theory

_On Tennis_

**David Foster Wallace**

_String Theory_ (2016) is a collection of essays about tennis by David Foster Wallace. The best players in the world sacrifice their lives so that they can entertain us, but their sacrifice elevates them to a level of greatness that the rest of us lowly mortals will never achieve.

---
### 1. What’s in it for me? Get into the mind of a professional tennis player. 

What is going on in the minds of those physical savants during a match at Wimbledon? And what does it take to get there? If you've ever wondered such things, join the crowd — there's a whole world of competitive tennis that is intensely cutthroat, and only the best of the best make it onto our TV screens.

It might surprise you, but the esteemed writer David Foster Wallace was once an active participant in that world. In these blinks, we'll get an insider's look at this rarefied domain from one of the most beloved literary minds of our time.

In these blinks, you'll learn

  * how sweating might benefit your tennis game;

  * how Federer perceives a ball on the court; and

  * why sports memoirs are usually so dull.

### 2. David Foster Wallace served up more than fine prose. 

David Foster Wallace is known as one of the most talented authors in modern American fiction. But his career could have easily gone another way. Wallace actually started out as a competitively ranked junior tennis player. The other kids even gave him a nickname, Slug, as a sort of backhanded compliment. He was lazy and slow, but, somehow, he still smashed his opponents.

Wallace explains that he used the microclimate to his advantage. He grew up in Philo, Illinois, deep in the midwest, where it's even windier than Chicago, famed throughout the world as "The Windy City." Wallace learned not to fight the winds when he played. Instead, he harnessed their power.

And it wasn't just in tennis that he used the winds. Wallace liked to ride his pushbike around town, tacking across the wind by using an outstretched, book-laden arm as a sail. Needless to say, the rest of the town thought he was crazy.

He applied this atmospheric expertise to his game on the court. The other kids were, without a doubt, stronger and more technically proficient than he was. They could fire shots off into the corners. But Wallace adopted a different strategy. He lobbed high, slow and straight, letting the wind wreak havoc.

Wallace had another slippery trick up his sleeve. He sweated. Profusely. Although inordinate perspiration isn't usually regarded as a talent in day-to-day life, it worked wonders for Wallace on the tennis court. By the end of play in the clammy Illinois summer, Wallace wouldn't be the prettiest sight — but, with enough water and salty snacks, he could play on and on.

His slicked and preppy opponents, on the other hand, would soon begin to wilt in the heat, sometimes even passing out. Wallace called himself "a physical savant, a medicine boy of wind and heat, [who] could play just forever."

That's quite some racket.

### 3. There's no other way to spin it – Tennis aces don't emerge from nowhere. 

The world of professional tennis is tough. The top 100 players in the world qualify automatically for all the tournaments on the tennis circuit, including the highly prestigious grand slams. But those ranked outside this echelon must fight it out among themselves for the remaining spots. These qualifying events — known as "the quallies" — occur before the tournament proper, and they're barbaric in their savagery.

The quallies are packed with players on the brink of the top 100 — fading lights of previous generations, now too old or too decrepit to climb back up the rankings, as well as top 100 players who somehow missed entry-form deadlines. Most depressingly, there are hordes of lithe athletes who somehow will never get beyond the most "mediocre" of rankings.

Consequently, there's a huge disparity in ability on display. It can be pretty demoralizing to watch as the 75th in the world crushes the 180th!

And what's the reward for making it through the quallies? You'll get to meet the best players in the world, nice and rested and ready to give you a good spanking!

Unfortunately, the glitz of the big tournaments isn't the only misleading idea when it comes to the best tennis players. People only get there after years of pain and sacrifice, really, the making of a top tennis player isn't a pleasant sight.

The lives of these players may seem glamorous — they jet from tournament to tournament, and appear in ads for watches and sportswear — but what seems like the high life actually hides a harsh reality. Never mind the effort required to get into the top five; even to get into the top 500 is incomprehensible to most of us. What does it take to get there? Stifled childhoods, unforgiving training schedules, discipline, controlled diets, even abstinence from most of the joys of life.

Let's face it — they suffer. And, like modern-day saints, they endure this suffering for our betterment. By watching them, we experience a kind of glory; by witnessing their passion, we partake of their splendor.

### 4. No trick shots here. Life as a pro demands hard work and ability. 

Think you've got tennis skills? Think that, if only you'd really dedicated yourself, you could have played pro? Think you might be able to duel with a top 100 player? Think again. You're deluded.

On the small screen, it looks easy, but TV simply can't transmit the abilities of these players.

With stunning speed, professionals can move across the width of the court to reach the ball. And after that they still have complete mastery over the pace and spin of the return. Plus, it's exhausting. Playing a three-set match requires as much energy as playing a quadruple-length basketball game on a full-size court.

Those towels and sweatbands are not just for show. What may seem like gentle dabbing disguises the plain truth: towels and sweatbands are essential. They ensure that rackets don't fly out of slippery hands and that players aren't blinded by perspiration.

On top of all that vigorous movement, tennis pros need to have expert vision. In fact, they use two kinds at once.

There are people who could probably smash a ball like a professional. But that's not enough. You have to hit hard _and_ accurately. For this you need the kind of vision associated with hand-eye coordination.

After years of constant training, the best players in the world have this skill down to a tee. It becomes second nature.

Imagine that you had to catch a baseball as it skitters erratically toward you on uneven ground. Now imagine that you have to hit the ball back where it came from, to a location far away. Now imagine doing that for two hours.

The second kind of vision is peripheral vision. It means you have to know where your opponent is at all times, in what direction he or she is going, and just what kind of shot you need to play to take advantage of his or her position.

### 5. Athletes’ memoirs are boring because great competitors need to be boring to be great. 

One of the few certainties in publishing is that the memoirs of top athletes sell by the bucket load. But these books are, almost without exception, mind-numbingly lifeless.

Which leads one to wonder, how can someone of such physical prowess, the acme of the human condition, produce such drivel? More to the point: What do we lesser beings think we have to learn from the sonorous catacombs of their minds?

Wallace was familiar with this dilemma. He had once been addicted to sports memoirs, a guilty fascination that ended abruptly when he read the ghost-written autobiography of tennis prodigy Tracy Allen. Sand would have had more personality. The book was nothing more than a tally of matches and points in prose. It was deeply dissatisfying.

Foster concluded that we read these memoirs despite ourselves. We read them even in the face of inevitable disappointment because we think they will lead to deeper truths and greatness.

The books are riddled with clichés and empty mantras. But, Wallace thinks, maybe that's the point. Herein lies the genius.

Perhaps the minds of sports stars simply _are_ empty. Maybe that's the secret to their success. These great players have been gifted the physical abilities of gods on earth. But, if they falter even for a moment, if their focus drifts, they will fail.

Players become great because they can blank out their minds. They silence their doubting internal voices. They have to. Can you even imagine what it's like playing a tiebreak before a silent crowd while an audience of millions watches from home?

This is what we can chalk their success up to. When players repeat the same hackneyed phrases in post-match interviews — like taking each match one point at a time — they're not vacuous platitudes to them. These are the plain truths and adages that ultimately lead to success.

### 6. Federer has returned tennis to grace as he ascends to brilliance. 

Until recently, many thought tennis had reached a dead-end. The old style of tennis was finished. In the old days, it was all about serve-and volley. After the service, both players ran to the net to face off.

But the advent of improved racket technology spelled the end for this style of play. With the improved rackets, players could linger on the baseline, blasting balls across court with immense power. Rallies went on forever. It was interminable.

This impasse was broken by one man: Roger Federer.

He found a middle way. In the face of ferocious baseline smashers like Andre Agassi and Rafael Nadal, traditional serve-and-volley players disappeared, but Federer found the sweet spot. He is nimble and intelligent. He wins points by forcing his opponent around the court. He opens up space for himself while simultaneously squeezing the opposition into tight corners. Pair that understanding of space with lightning fast reflexes and you can begin to understand how he produces unthinkable shots from nowhere. Even when you see the slow-motion replay on the TV, you still ask, "How is that possible?"

Federer proves that, no matter what some commentators say, there's certainly no truth to the view that subtlety in tennis is dead.

When you watch Federer, you see a master technician at work. No. We can go further still. There is poetry in the rhythms and gracefulness of Federer. Technical explanations can only get us so far. The basketball star Michael Jordan was made of similar stuff. He sometimes hung in midair above the hoop, seemingly defying gravity.

Federer likewise seems to defy the natural laws we mortals have to follow. On court, the strangest things happen. For him, when he is at the height of his powers, the ball seems to balloon in size, or slow to a snail's pace.

This experience could not be further from what we spectators see from the stands or on TV.

For us, it is a flash of brilliance; for Federer, an eon of glory.

### 7. Final summary 

The key message in this book:

**Tennis is a beautiful game. It demands all the strength and intelligence of its greatest players. in return, it gives them moments of unbelievable grace. We spectators are but the congregation to its miracles.**

Actionable advice:

**Go watch a professional tennis match.**

If you can go to a tournament, head to one of the smaller matches where you may be able to stand meters away from the action. A qualifying match would be ideal. This is the only way you'll get to experience the vast gulf between good tennis players and the best in the world.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Bounce_** **by Matthew Syed**

In _Bounce_, Matthew Syed explores the origins of outstanding achievements in fields like sports, mathematics and music. He argues that it is intensive training, not natural ability that determines our success, and people who attribute great performances to natural gifts will probably miss their own chance to succeed due to lack of practice.
---

### David Foster Wallace

David Foster Wallace was an American novelist, essayist and short story writer _._ In his youth, he played high-level competitive tennis.

