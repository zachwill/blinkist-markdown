---
id: 5846b1a9714e5f00047cdc72
slug: stronger-together-en
published_date: 2016-12-08T00:00:00.000+00:00
author: Hillary Clinton, Tim Kaine
title: Stronger Together
subtitle: A Blueprint for America's Future
main_color: 40C2F5
text_color: 25718F
---

# Stronger Together

_A Blueprint for America's Future_

**Hillary Clinton, Tim Kaine**

_Stronger Together_ (2016) introduces you to the backgrounds and policies of US presidential candidate Hillary Clinton and her VP, Tim Kaine. Find out how they propose to address domestic and foreign policy, including how to make college affordable; how they'll bring fair wages to women and more opportunities for immigrants; and how they'll tackle the ongoing threat of ISIS.

---
### 1. What’s in it for me? Get familiar with the policies of Hillary Clinton and her VP candidate, Tim Kaine. 

Election time is here. The social-media brouhahas, the vitriolic debates, the prolix speeches: from this sea of rhetoric and retribution, it can be hard to fish out the pearls of a candidate's actual policy. And, perhaps more than all presidential elections of the past, this has been an election defined by sound bites and half-informative snippets, each relentlessly commented on by pundits and laymen alike. In short, it's been a year of horse-race journalism; there's been plenty of focus on the race — the polls and public perception — but barely any on what really matters: the policies of the candidates and their concrete plans for tackling the future.

These blinks handle things differently. They focus on the actual policy plans of one side of the presidential campaign, the Democratic nominee Hillary Clinton and her running mate for vice president, Senator Tim Kaine. So let's put aside polling data and sound bites, and get down to the real nitty-gritty of good governing.

In these blinks, you'll find out

  * how 80 percent of American families could send their children to college for free;

  * why improving immigrants' lives will improve the economy; and

  * that tackling global security risks through cooperation is the best way forward.

### 2. Hillary Clinton’s humble upbringing gave her a desire to help others, and this led to her taking political action. 

As a former first lady of the White House, and the current Democratic presidential nominee, Hillary Rodham Clinton is one of the most prominent people in the world. So it would be reasonable to assume that she comes from a wealthy and elite family. But this is far from the truth.

Hillary's humble origins taught her the importance of helping others.

Her family has roots in Scranton, Pennsylvania, where Hillary's grandfather worked at a lace mill for 50 years. A selfless man, he worked hard to make sure his kids had more opportunities than he did.

His efforts paid off when Hillary's father, Hugh Rodham, was able to attend college at Pennsylvania State University before enlisting in the Navy after the events of Pearl Harbor.

Things were tougher for Hillary's mother, Dorothy Howell. Abandoned by her parents at a young age, Dorothy scraped by on her own, working as a housemaid at the age of 14.

Had it not been for the help and kindness of others, chances are that Dorothy wouldn't have made it. For instance, her first-grade teacher, noticing that Dorothy had nothing to eat at lunch, brought extra food to share with her.

This kindness never left Dorothy's heart, and she taught her daughter that no one gets through life alone, and that we have to look out for one another and lift each other up when times are tough.

And just as Hugh's father had done for his children, Hugh and Dorothy succeeded in giving Hillary better opportunities than they ever had. She eventually graduated college and attended Yale Law School.

During this time, she worked for the Children's Defense Fund, an advocacy group for kids facing obstacles like those her own mother had to overcome. Hillary went door to door to gain support for disabled children who were being denied even the basic opportunity of attending school.

And this is when it became clear to Hillary that compassion and caring alone aren't enough. She realized that true progress is made when both hearts _and_ laws are changed — and that it was time for her to take real action.

### 3. Tim Kaine’s passion is fighting for equality and basic human rights. 

Even though Tim Kaine has had a long and influential career in politics, he isn't as well known as Hillary Clinton. But, like Clinton, Kaine's desire to help others is what brought him to public service.

As Tim grew up, he was also influenced by the hard work of his parents. Faith played a big role as well.

Some of Tim's earliest memories are of his father's iron-working shop in Kansas City, and it was there that Tim learned the basics of carpentry and welding. And Tim's time at a Jesuit school taught him the religious values that continue to guide much of his life.

He even took a year off after law school to volunteer as a Jesuit missionary in Honduras. He worked with teenagers and taught them the skills he'd learned at his dad's shop, and they, in turn, taught him Spanish.

But that's not all Tim learned in Honduras. It was there that he got his first look at a rigged social system, where those at the top reaped all the rewards and the rest were left behind. The experience convinced him that society needs to offer equal opportunities for everyone, no matter their race, wealth, background or sexual orientation.

Another influential figure in Kaine's life was his wife's father, A. Linwood Holton, Jr., the former Republican governor of Virginia who played an instrumental part in integrating the state's public schools.

By sending his own kids to an integrated school in 1970, he showed his personal support for the landmark 1954 Supreme Court ruling in Brown v. Board of Education, which declared segregated schools unconstitutional.

With these guiding principles, Tim Kaine began his career as a civil-rights lawyer and represented those who'd been denied housing for discriminatory reasons.

Over the course of nearly two decades, Kaine fought dozens of cases against banks, landlords, real estate firms, insurance companies and even local governments that had treated people unfairly.

His crusade eventually led to his becoming the mayor of Richmond, Virginia — and then to assuming the position of governor and senator.

### 4. Clinton’s top priority will be to strengthen the US economy through improvements in infrastructure and education. 

Following the 2008 financial crisis, nearly nine million Americans lost their jobs, and even though President Obama's leadership has helped the economy bounce back, a lot of work remains.

Clinton is prepared to continue the repairs and, if elected, she plans to make bold investments in national infrastructure.

Many of America's most fundamental programs, such as public transportation, roads and clean water and energy, have been deteriorating for decades. And this kind of neglect will continue to cause great harm to our public health.

Despite having support from both Democrats and Republicans, federal-infrastructure investment remains around half of what it was 35 years ago.

As a result, roads are backed up with traffic and people sometimes get stranded at airports for days. Even worse, since 2014, people in Flint, Michigan, have had to deal with poisonous water due to neglected and deteriorating water infrastructure.

Fixing problems like these will be a high priority on Clinton's list, as will making college a debt-free possibility for all Americans.

Her plan to achieve this is called the New College Compact, and it proposes to waive tuition fees at public universities for all students whose families earn $125,000 or less per year.

Under this plan, over 80 percent of US families would be able to send their children to college free of charge. Plus, the New College Compact would also remove the tuition fees from the nation's community colleges.

To help cover the costs of this new system, students will be asked to work ten hours per week at jobs that will provide them with opportunities to develop important skills. This will also introduce students of all backgrounds to the possibility of careers in public service.

This plan will also be made possible by increasing federal funding for financial assistance through the Pell Grant and similar services that make a college diploma an affordable and realistic goal.

### 5. Clinton and Kaine are ready to fight income inequality and help families struggling with childcare. 

Long ago, it was considered normal for men to be the sole moneymakers in a family. But nowadays it's not unusual for women to be the primary breadwinner in any given household.

Yet despite the progress that has been made to secure equal rights for women, many obstacles, including childcare costs and unfair working conditions, are still preventing women from earning a comfortable living.

Hillary Clinton is determined to fight these problems and ensure equal pay for women.

Remarkably, women still earn, on average, around $11,000 less per year than men who work in equivalent positions, and they hold the majority of the lowest-paying jobs.

America is overdue for an unbiased economy. With this in mind, Clinton will call for salary transparency across the nation's workforce to ensure that women have the necessary information to negotiate for fair pay.

This is the goal of the Paycheck Fairness Act, which aims to eliminate the costly legal fees that currently go hand in hand with fighting wage discrimination and prohibiting retaliation against employees who seek fair pay. Plus, the act would increase the penalties against employers who practice gender discrimination.

Clinton also proposes raising the country's minimum wage to $15 per hour, and supports the right for women to form and join a nationwide union.

Another problem is cost of childcare. Sending two kids to a good preschool can cost a family more money than they spend on rent, and, in some states, it can cost more than four years of college tuition.

So Clinton will also fight for more affordable family care plans and improved medical-leave benefits.

For this to happen, she proposes increasing the federal subsidies that help families pay for childcare and offering tax relief to working families. This way, no family should have to pay more than ten percent of their income to care for their children.

And to help ensure working parents can be there for their kids, Clinton intends to provide them with 12 weeks of paid family and medical leave.

### 6. Clinton and Kaine’s immigration-reform proposal will improve the lives of immigrants and the economy. 

America is a cultural melting pot. It's home to 42 million immigrants — a whopping 13 percent of the population.

Immigrants both founded and built this country. And yet they still face barriers and limited opportunities.

That's why Clinton hopes to create the very first Office of Immigrant Affairs, which will support immigrant integration and improve the coordination between local and federal agencies.

Clinton's policy is one of comprehensive immigration reform that would keep immigrant families together and put an end to the fear-based policies that are currently separating — or threatening to separate — countless families.

Karla is an eleven-year-old girl from one such threatened family. She loves math, science experiments and Disney movies, but whenever she leaves the house for school, Karla is terrified that her parents won't be there when she returns — that they'll have been deported.

The fear was so bad and constant that a heart specialist told Karla's parents it was giving her a dangerously arrhythmic heartbeat.

To fight these miserable conditions, Clinton will defend the Deferred Action for Childhood Arrivals (DACA) and Deferred Action for Parental Accountability (DAPA) programs.

The DACA program makes undocumented immigrants who entered the US before their sixteenth birthday eligible for a renewable two-year work permit that would exempt them from deportation.

The DAPA program offers the same kind of protection for the parents of immigrants who have attained citizenship or permanent residence.

Both programs help protect and keep together the families of 16 million people, where at least one member is not properly documented.

Policies like these, and Clinton's hope for comprehensive immigration reform, are also good for the economy. By allowing foreign students and entrepreneurs to study and to join the economy, the United States adds $800 billion to its GDP.

### 7. Clinton’s hard-line plan to defeat ISIS includes strengthening America’s alliances through NATO. 

Over the past couple of decades, terrorist organizations have brought an increased threat to security around the world.

Most recently, the terrorist group known as ISIS has gained control of a sizable territory in Iraq and Syria, where they behead civilians, kill entire religious and ethnic groups and rape, torture and enslave girls and women.

As part of her foreign policy, Clinton is meeting the challenge of ISIS with a hard-line stance against global terrorism.

Her counterterrorism strategy would include intensifying air campaigns against ISIS fighters and key structures, as well as supporting more Arab and Kurdish forces in the area to protect civilians.

Clinton would also offer more support to Syria's neighbors, Jordan and Lebanon, countries that take in many Syrian refugees and play an important role in the efforts to maintain stability.

Clinton is aware that ISIS is also active online and would work with Silicon Valley to fight their recruiting efforts and discredit their message.

But we also need to promote national and global security by strengthening our alliances. While the United States is a crucial part of any solution to a global crisis, it can't solve such a crisis on its own. Nations need to work together, and NATO, one of the world's best alliances, is absolutely vital to affecting peace in the Middle East.

We need to show the same solidarity that we did after 9/11, when, for the first and only time, NATO invoked Article 5, which states that an attack against one NATO member is an attack on everyone.

Through the cooperation of NATO, and strong allies like Japan, South Korea and Israel, we can also present a united defense against other aggressive forces.

Clinton points to China and Russia as two rival competitors, and suggests that the United States should remain firm with them in order to maintain a leadership role in the global political arena.

### 8. Final summary 

The key message in this book:

**The United States will be stronger if we work together and help one another fight for a better life. This doesn't just apply to those that are already at the top; it applies to marginalized populations, too: the poor who are fighting to get by, the women who are fighting for equality and the immigrants who are fighting to keep their families together. Hillary Clinton and Tim Kaine promise improved infrastructure, education and immigration policy, as well as an intensified offensive against terrorism through allied collaboration.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Hard Choices_** **by Hillary Clinton**

_Hard Choices_ offers a first-hand account of the trials and impressive diplomatic successes of the early years of the Obama administration. In this telling memoir, former Secretary of State Hillary Clinton places you at the administration's negotiating table where key policy decisions were made.
---

### Hillary Clinton, Tim Kaine

Hillary Clinton has nearly four decades of experience in public service. She was the 67th US secretary of state from 2009 to 2013.

Tim Kaine has been in public office since 1994 and is currently the US Senator of Virginia. He has worked as a civil-rights attorney, and as a missionary in Central America. He is one of only 30 people to have worked as mayor, governor and senator.

