---
id: 569c9901e37b120007000004
slug: structures-en
published_date: 2016-01-20T00:00:00.000+00:00
author: James Edward Gordon
title: Structures
subtitle: Or Why Things Don't Fall Down
main_color: A4443C
text_color: A4443C
---

# Structures

_Or Why Things Don't Fall Down_

**James Edward Gordon**

_Structures_ (1978) examines the fundamental, physical laws that keep the physical structures of our world intact, from man-made structures like airplanes, to biological structures like the body of a horse. These blinks outline the ways in which our structures are prone to collapse, and the critical value of scientists who perform complex calculations to keep our structures sturdy — and keep us safe.

---
### 1. What’s in it for me? Build structures that withstand the test of time. 

From the magpie's nest in the tree outside your window to a termite colony on the African savannah, nature is full of wonderfully resilient and efficient structures. And perhaps this shouldn't surprise us; after all, nature has had millions of years to perfect its creations through the process of evolution.

We humans have not had quite as long. Even so, with the help of a little science, we've been able to build some pretty impressive structures. In fact, if you understand the underlying forces that affect a structure and the materials it's made of, you have a good chance of building something that will stay standing for millennia. 

In these blinks, you'll learn

  * that materials can also suffer from stress;

  * why every roof needs a beam; and

  * how to deal with a fatigued structure.

### 2. The study of biological and artificial structures began in the seventeenth century. 

What holds an airplane together as it moves through the air or keeps a bridge from collapsing under the strain of cars? It's all in the design of their _structure_.

A structure can be defined as a collection of materials intended to sustain loads. Our world is full of them: structures occur both in the biological and man-made world.

Biological structures, which are of course much older, transport matter and provide living things with protection. Present-day biological structures are mostly soft, like muscle tissue or flower petals. However, there are also rigid biological structural parts, like horns, bones, teeth or tree bark.

Artificial structures, on the other hand, are man-made. But in relative terms, humans haven't been formally studying structures for very long at all.

The study of structures began in the seventeenth century, thanks largely to Galileo. Galileo had to switch disciplines after the Catholic Church threatened to persecute him for his work in the field of astronomy in 1633. He left astronomy behind and began studying the strength and character of different physical materials.

Galileo's prestige brought greater academic attention to the subject. In the mid-1650s, scholars began researching the ways in which different materials and structures behave under heavy loads. That same century, Robert Hooke also discovered how matter behaves at an atomic level.

Hooke wrote that a structure can only resist a load by pushing back on it with an equal force. So, if a cathedral pushes down on a foundation with its weight, the foundation will either break or push back up with an equal force. This is one of the fundamental concepts of structures and their strength.

> _"In one sense a structure is a device which exists in order to delay some event which is energetically favored."_

### 3. Stress and strain are forces that act within the material of a solid structure. 

Stress isn't just a psychological problem affecting humans — structures can suffer from stress as well.

In physics, _stress_ is a force that can exist at any point inside a solid. It measures the degree to which atoms and molecules within the material are being pushed apart by external forces.

Stress is measured by dividing a force by the area it's acting upon (in other words, newtons/area). Therefore, if two people pull on either end of a rope, this generates stress within the rope. If they pull with the same force on another rope made of the same material but with a smaller diameter, there would be _more_ stress in that rope because its area is smaller. That's why a thin rope would break before a thicker one.

So, stress tells us the degree to which atoms are being pushed apart. _Strain,_ on the other hand, measures how _far_ they're being pushed apart.

We measure strain by comparing the increase in an object's length to its original length. So, if a machine pulls on a rod that is 10 cm long and increases the rod's length by 0.2 cm, the resulting strain is 0.02 — and unlike stress, strain doesn't have a unit of measurement.

Stress and strain also describe the _stiffness_ of a material when taken together. Stiffness, also known as _Young's modulus of elasticity_ (named after the scientist Thomas Young), measures the elasticity of a material under a given stress.

Some materials are more elastic than others. Rubber, for instance, is of course far more elastic than diamond, so Young's modulus of elasticity for diamond is much higher than the modulus of elasticity for rubber.

### 4. Tensile forces pull atoms apart and may cause creep. 

When you pull on either side of a piece of rubber, it stretches out. But what allows a solid to change its shape under pressure?

Rubber changes shape when it's pulled because the atoms inside it move around. These atoms are being acted upon by _tensile forces_.

Tensile forces pull atoms apart rather than pushing them together. They play an important role in pressurized vessels, like bladders, arteries, diving cylinders and balloons. The molecules in a balloon stretch apart when you inflate it, for instance. Sails work the same way: they stretch out when hit by a gust of wind.

When tensile forces are applied, _creep_ occurs. Creep happens when a solid material gets deformed under the influence of mechanical stress. And if that force is applied regularly, the material will keep changing and morphing over time. 

Think of the way new shoes become more comfortable the more you wear them: the areas of the shoes that are most physically stressed change such that the stress gets redistributed. Eventually, your body weight gets distributed around the shoes more equally. Creep is essentially an adaptation mechanism for structures.

Materials don't necessarily stretch out because of tensile forces, however. If that were the case, blood vessels would keep growing over time, but this isn't the case. When tensile forces stretch a solid, the solid extends in the direction it is being pulled — but it simultaneously contracts laterally, at a 90° angle to the direction of the force being applied.

So, when blood flows through the aorta, it causes _longitudinal_ stress, which counteracts the _circumferential_ stress that affects the walls. These two kinds of stress cancel each other out and the aorta retains its shape.

### 5. Structures built on compression are usually quite strong. 

Some structures, like ancient churches, castles or monuments, have managed to stay intact for centuries or even millennia. What makes it possible for them to stay standing for so long?

Structures stay in place largely thanks to the _compressive forces_ acting upon them. Unlike tensile forces, compressive forces _push_ on a structure rather than pulling it.

Even before they began studying physics, our ancestors intuitively knew that they should avoid constructing buildings that could be affected by tensile forces. Instead, they built structures where everything was compressed, like masonry buildings held together by lots of small pieces pushing on each other.

A structure is only stable when compressive pressure acts evenly on its supporting elements. If two bricks are attached with mortar but the compressive forces are stronger on one of them, the other might be affected by tensile forces, which can cause it to crack.

Ancient people implicitly understood the principles of structures and their forces well before they could perform calculations about them. This is what enabled them to build massive castles and churches before the scientific era, when Galileo and other scholars uncovered the basic principles of structures. 

When a compressed structure _does_ fall, it's due to a lack of stability, and not a lack of strength. Tension structures may collapse when their stress becomes too high, but it's rare for a compression structure to collapse for the same reason.

Children also understand this intuitively. When they build a tower with blocks, they know it will eventually collapse if it is too high and isn't vertically straight enough.

### 6. The beam was an important invention for making compressive structures safer. 

The roof of your house is obviously an important part of its design, since it protects the house from the elements. A roof might seem a simple enough component of a structure, but for engineers, a roof is much more complicated than it seems.

Roofs present a lot of challenges for engineers. A permanent and stable roof can be dangerous because it puts a lot of weight on the surrounding walls of the building. And because so many roofs are triangular, the resulting force usually doesn't push down vertically; it pushes outward against the walls. The resulting tensile forces on the walls could cause the building to collapse on itself.

Windows complicate things even further. People want to be able to look outside and let natural light into their home, but windows make the walls even weaker and lessen their ability to sustain the weight of the roof. As a result, engineers had to find a way to construct triangular roofs that wouldn't put too much stress on the walls and thus put the structure at risk. 

This is how they came up with the _beam_.

Beams channel the force of the roof downward and away from the walls. They support a load at a right angle to the length of the beam, but don't put any horizontal force on the supporting elements.

And beams occur in nature, too. Any heavy structure may well be held together by a beam. Horses, for example, have heavy bodies but very thin legs — yet they can still carry a human on their back! This is likely because of their horizontal backbone, which functions in a way similar to the beam of a roof.

So beams make heavy, compressive structures more stable. Even with a beam, however, other problems can occur.

> _"Nature seems to be a pragmatic rather than a mathematical designer."_

### 7. Cracks and the misuse of certain materials can cause a structure to collapse. 

Even seemingly well-designed or newly built bridges can collapse sometimes. There are a few possible reasons for this, but cracks and material problems are often the main culprits.

Structures can fall apart when they develop cracks or when new material is added onto them. C. E. Inglis wrote about this in 1913, when he published a book about the significance of irregularities within a material.

Inglis found that localized regions of stress in materials are exacerbated by holes, cracks and sharp corners. A structure might appear to be stable but can still break if it develops a lot of local stress.

Holes and cracks aren't the only agents of local stress, either. The addition of new material can also cause stress by increasing the structure's stiffness. So, if you put a patch on a torn piece of clothing, for example, it might just cause it to tear even more.

Cracks aren't all the same, however; they're not always dangerous and don't always put a structure at risk. The key factor determining whether a crack is dangerous is its length.

The critical point at which a crack's length makes it a danger to the structure is called the _critical Griffith crack length_. Shorter cracks are, of course, safer and more stable.

The Griffith crack length depends on the levels of stress in a structure's material. The more stress a structure has, the shorter its Griffith crack length.

When a crack reaches the critical Griffith crack length, it starts growing very rapidly as the structure puts more and more pressure on it. This process can cause the structure to collapse quite suddenly.

### 8. Tensile and compressive forces can cause a structural collapse, but for reasons other than cracks. 

If you keep stretching out a rubber band, it eventually reaches a point where it will break; the tensile forces acting upon it cause its material to develop cracks or holes.

When tensile forces are applied to a material, the interatomic bonds within it stretch out — and they can only stretch to a certain extent. After about a 20 percent increase of tensile strain, the chemical bonds become weaker and eventually unstick from each other, producing a crack or hole.

Compressive forces are different, however. When a structure collapses from compression, it occurs because of _shearing,_ the process by which one part of a material is forced to slide past another part.

When a solid is compressed, its interatomic bonds don't get stretched out, because the atoms are being pushed together, rather than pulled apart. But compressive stress can still cause a structure to collapse from what's called _compression failure_.

Compression failure is caused by shearing, usually when shears occur at around a 45° angle. Diagonal shear cracks are similar to tensile cracks: they also have a critical Griffith length at which they start growing rapidly and may cause the structure to collapse suddenly.

If a crack in glass or stone reaches the critical Griffith length, the resulting release of energy may even cause splinters to shoot out.

So far, we've seen how dangerous tensile and compressive forces can be. Even if a structure appears to be sound, a seemingly small crack can lead to its demise. Thankfully, we have scientists who conduct complex research into how to design and build structures that will have a very high probability of being structurally safe. But how do they address the problems posed by various forces and cracks?

> _"If tension is about pulling and compression is about pushing, then shear is about sliding."_

### 9. Calculations and experimental testing increase the safety and efficiency of man-made structures. 

When a person gets frustrated or stressed out, they will often dwell on their problems, which only serves to increase their stress levels. A similar phenomenon occurs in metal — and, appropriately enough, it's called _fatigue_.

Fatigue occurs when the heavy load on a metal fluctuates and causes it to lose strength. People first noticed this phenomenon during the industrial revolution, when machinery would sometimes break when it was moved.

When the pressure on a metal changes, it can alter the crystalline structure within it, causing its cracks to extend even if they haven't reached the critical Griffith crack length.

Metal fatigue is very difficult to spot, so metallurgists have done a lot of experimental testing to determine how to calculate it. These experiments and calculations are very important for keeping us safe.

Calculations for structural strength are based on statistics, meaning that they're based on probability. So, there's technically always still a chance that a structure can collapse, even when building calculations seem sound.

These calculations are so complex that, in the 1970s, different groups of experts independently evaluated the structural design of airplanes to make sure they would be safe. Experimental testing was also used when new airplane structures were in development. 

In fact, between 1935 and 1955, about 100 different kinds of airplanes were built and tested to destruction. 

Experimental testing also makes structures more efficient. Structures always break at their weakest point, so it's important to make these weak points as strong as possible. We can also reduce the weight and material in the areas of the structure that are less prone to breaking.

### 10. Final summary 

The key message in this book:

**Structures are a fundamental part of both the man-made and natural worlds. And while a rooftop and a horse would seem to have little in common, they're affected by the same scientific concepts that keep structures intact. Stress, strain, force and strength are the keys to designing and maintaining the structures so important to our everyday lives, and researchers play a critical role in keeping us safe from their collapse.**

**Suggested** **further** **reading:** ** _Stuff Matters_** **by Mark Miodownik**

_Stuff Matters_ (2013) is an adventure into the seemingly humdrum stuff we encounter daily. Materials scientist Mark Miodownik delves into the true makeup of modern materials, and invites you to look at your surrounding world through new eyes.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### James Edward Gordon

James Edward Gordon was one of the founders of the field of material science. He wrote several books in a highly respected academic career and was awarded the British Silver Medal of the Royal Aeronautical Society, as well as the Griffith Medal of the Materials Science Club.

