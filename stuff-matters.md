---
id: 55116924376430000a360000
slug: stuff-matters-en
published_date: 2015-03-24T00:00:00.000+00:00
author: Mark Miodownik
title: Stuff Matters
subtitle: Exploring the Marvelous Materials That Shape Our Man-Made World
main_color: 509171
text_color: 42785D
---

# Stuff Matters

_Exploring the Marvelous Materials That Shape Our Man-Made World_

**Mark Miodownik**

_Stuff Matters_ (2013) is an adventure into the seemingly humdrum stuff we encounter daily. Materials scientist Mark Miodownik delves into the true makeup of modern materials, and invites you to look at your surrounding world through new eyes.

---
### 1. What’s in it for me? Take a look into the wondrous world of materials. 

Did you know that early celluloid billiard balls were so chemically unstable that they'd sometimes explode on impact?

Or what about the fact that just as much engineering went into getting chocolate to melt in your mouth as into constructing some of our most complex buildings?

Welcome to the crazy fascinating world of materials, where there is always a story and some deeper meaning behind what our naked eye can see. These blinks contain a peek into the magically complex realm of molecules, cultures and the things that make up our world.

After reading these blinks, you'll know

  * why paper clips bend;

  * why we don't have stainless steel toilets; and

  * what differentiates you from a pencil.

### 2. What are everyday objects made of? Depends on the scale at which you study them. 

From the pencils we write with to the steel utensils we use to eat, every day we encounter countless man-made items. But what are these objects actually made of? In fact, the answer depends on the scale at which you're examining the object in.

At first glance, they may seem like monolithic, single-piece units, but look a little closer and you'll find that the material is usually made up of an intricate arrangement of smaller parts that interlock perfectly. A sweater is made of tightly woven threads, for example. And if you examine those smaller parts more closely, you'll find that they too are made up of even smaller parts, much like a Russian nesting doll.

Unfortunately, the naked eye cannot reveal more about your sweater, but a powerful microscope would show the individual fibers that make up these threads. Looking even deeper, you'd see that the fibers consist of hundreds of thousands of molecules, and, finally, atoms.

These molecular and atomic levels of detail are important, because changes there can have a profound effect.

For example, consider that though they look completely different, both diamonds and the graphite found in pencils are made up of carbon. The difference in appearance arises from the fact that on a molecular level, the carbon atoms are arranged differently in each material: graphite contains atoms arranged in sheets, whereas in diamonds they are in a cubic structure.

So, as far as materials are concerned, remember that there's much more to them than meets the eye.

> Fact: By heating diamonds in a vacuum, you can turn them into pure graphite!

### 3. Even the minute substructures in seemingly solid materials do, in fact, move around. 

As you now know, all materials, including the most seemingly solid structures, contain immeasurable smaller parts.

Metals, for example, are actually made up of billions of tiny crystals. This might seem counterintuitive, because we usually think of crystals as translucent gemstones on rings, but a powerful microscope reveals it's true.

The density with which these smaller structures are packed into the material determines how much they can move around, which in turn determines the behavior of the material when pressure is applied to it.

In the case of a metal object like a paperclip, the metal crystals are packed loosely enough that they can move, and thus paper clips can be bent. The crystals merely change their position.

If the metal of the paper clip was comprised of more tightly packed crystals, as is the case with steel, for example, the paperclip would not bend. Rather, it would snap in half, as the crystals would have no room to maneuver.

So, put simply, soft materials are soft because their substructures are packed more loosely.

> Fact: As you bend a paperclip, you are causing approximately 100,000,000,000,000 metal crystals to move at the speed of sound.

### 4. Living and non-living matter differ mostly in how they react to stimuli. 

We humans may consider ourselves the pinnacle of evolution, but actually we're made of exactly the same atoms and molecules as everything around us. Atomically, our bodies are composed of some of the most common elements on earth: carbon, hydrogen, oxygen and nitrogen.

It's only small variations in how these elements have arranged themselves in our bodies, as well as a few more unusual elements like calcium, that account for our human physical form: our body, hair, skin, eyes, and so forth.

So clearly if you're just looking at the atoms and molecules, living things are no different from non-living ones. The distinction arises when we look at how the atoms and molecules react to what's around them:

Living materials exhibit much more reactivity than non-living ones.

Consider what happens when a material encounters an external stimulus like fire, for example. Sure, non-living materials can react to it by bending, snapping or resonating, but this is a mere passive reaction: the fire acts on them rather than the other way around.

The layers of molecules in living material, on the other hand, can communicate: cells in your skin can sense they're close to a source of high heat, which may trigger a reaction like sending a nerve signal that produces a reflex to withdraw the skin from the heat source.

The discrepancy in reactivity between living and non-living matter doesn't mean that they're made of different "stuff" per se; rather, the various components of living matter are interconnected in a much more complex manner, and it's this interconnectedness that allows it to react.

### 5. Our choice of materials often has an emotional as well as a practical component. 

Have you ever wondered why we have such high regard for material possessions in modern society?

We're smitten with objects because it's not just their utility we value, but also their social purpose.

In fact, archeological evidence shows us that as soon as humanity developed tools, we also began to produce items of pure social value, like decorative jewelry, art and clothing.

And sometimes the social or emotional value of an object can even be more important than its utility, as the materials we use for certain items tells us.

Consider the fact that stainless steel is cheap, highly durable and easy to clean. So why don't we use it for plates or toilet bowls? Why do we instead rely on delicate, breakable ceramics in the dining room and bathroom?

Clearly, there's an irrational, emotional component to our choice of materials: We don't like the idea of combining the cold, hard nature of steel with our warm, cozy ritual of eating. Nor do we like steel to be part of the passing our most intimate wastes.

So where do these emotional bonds with materials come from?

The connotations we give materials are influenced by where we encounter them in our everyday environment. For instance, if our boss at work has a rosewood desk, we'll likely view that material as more prestigious.

What's more, we often surround ourselves with materials that reflect our values: someone who values luxury and appearances may wear silk clothes and build their kitchen out of marble, whereas someone who values practicality may instead choose polyester and granite, respectively.

In conclusion, don't underestimate how much someone's choice in materials says about them!

> _"...materials are a reflection of who we are, a multi-scale expression of our human needs and desires."_

### 6. The Final summary 

The key message in this book:

**Take a closer look at the objects around you, and you'll find that far from being solid and monolithic, they're comprised of tiny subcomponents, and subcomponents beyond those. By looking at these tiny particles on a smaller scale, we can better understand their properties, and begin to create even more incredible materials in the future.**

**Suggested further reading:** ** _A_** **_Short_** **_History_** **_of_** **_Nearly_** **_Everything_** **by Bill Bryson**

_A_ _Short_ _History_ _of_ _Nearly_ _Everything_ offers an enlightening summary of contemporary scientific thinking relating to all aspects of life, from the creation of the universe to our relationship with the tiniest of bacteria.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Mark Miodownik

Mark Miodownik is a professor of materials and society at University College London, where he also works as the director of the Institute of Making — home to a library containing some of the rarest materials on Earth. He won the 2014 Royal Society Winton Prize for Science Books for _Stuff Matters_, and he's been invited to give the Royal Institution Christmas Lectures.

