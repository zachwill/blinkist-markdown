---
id: 55d0f995688f670009000054
slug: stuffocation-en
published_date: 2015-08-19T00:00:00.000+00:00
author: James Wallman
title: Stuffocation
subtitle: Living More with Less
main_color: F9A232
text_color: 8F5D1D
---

# Stuffocation

_Living More with Less_

**James Wallman**

_Stuffocation_ (2013) explains how having too much stuff not only places an unnecessary burden on us, but is even leading to health issues. Our lives have become oversaturated with things, and a new value is emerging: the importance of experience over material possessions.

---
### 1. What’s in it for me? Discover what a future beyond materialism will look like. 

Be honest: how much stuff do you have that you never use? If you're like most other people, probably a lot. Whether it's dragging home useless things from a Sunday flea market, or buying yet another electronic gadget that you don't need, most of us suffer from the same thing: _stuffocation_.

This trend is not as harmless as it might seem. Buying more and more things, and hoarding them all in your home, doesn't just make it harder to stay organized; as these blinks will show you, having so much stuff actually has the potential to kill you.

For a long time, materialism and consumption have been the reigning doctrines, not just as a path to personal happiness, but as the engines that drive the economy. However, this is starting to change as more and more people around the world find alternative ways to live beyond a state of constant consumption.

So what are these alternative lives, and what would a world beyond stuff look like? These blinks will show you.

In these blinks, you'll learn

  * why materialism has seen its heyday come and go;

  * how you get people to pay 50 pounds to watch a movie; and

  * how the economy can thrive without today's levels of consumption.

### 2. Stuffocation is one of the biggest problems we suffer from today. 

Is your cellar or spare room cluttered with things you never use? Do you have junk-filled drawers that you can't even open because they're so jammed? If so, you suffer from stuffocation — and chances are all the things you have no longer make you happy.

For a long time, happiness was widely considered to be determined by people's belongings, and how many of them they had. But those days are over; owning more and more stuff no longer holds value for us. The reality is, "more" means having more things to organize and worry about. Materialism has become overwhelming. 

In 1979, four out of five people in the United Kingdom, France and West Germany agreed with the notion that material things make us happy. These days, that same statistic is only about one in two, meaning that about half of the population today has had enough of so much stuff. 

But how did we come to love stuff in the first place? Well, our minds evolved to be conscious of the threat of scarce resources, so gathering things was important. But in today's abundant world, we don't need to worry about this at all. We now have to change the way we think.

There are different reasons for our turning away from materialism. Ask a political scientist or a philosopher, for example, and you'll get different responses. 

Environmentalists are bucking materialistic values in favor of the environment; political scientists stress that most people care less about basic necessities such as food and shelter than they do about post-materialistic needs such as freedom of speech; and economists emphasize that rising costs and stagnating incomes are making us less materialistic, since we have less money for shopping.

But no matter who you speak to, there is a growing trend toward a new idea of happiness — that which prioritizes the enjoyment of experiences over possessions.

> _"Happiness is more likely to come from the enjoyment of experiences rather than the accumulation of stuff."_

### 3. Stuffocation leads to unhappiness and even death. 

Do the piles of stuff you have ever make you anxious? What about not having the stuff that other people have? Even the thought of organizing all your stuff probably has you pulling your hair out from time to time. But you're not alone.

One day, British philosopher Jeremy Bentham noticed something as he was enjoying his morning coffee: the first cup he relished, but the second didn't really satisfy him. You can probably relate to this feeling — a small amount of something is good, but you can also have too much of a good thing. 

Material goods have come to substitute our craving for meaning and status, and our consumer culture has become a kind of pseudo-religion. But now, since so many products are mass-produced, their meaning has been stripped away and they no longer excite or inspire us.

Even more problematic is these products' direct connection to status anxiety. Since 1979, rates of mental illness in developed countries have doubled. Our mass consumerism causes mass depression, and our mounting piles of stuff offer a direct route to unhappiness. 

As if sparking mental illness wasn't bad enough, having too much stuff can also be deadly!

We can all think of a person who never throws things away, and stores stuff they never use in basements, attics and containers. This hoarding behavior is a surprisingly widespread problem; new studies have revealed that between two and six percent of people in developed countries participate in it.

But hoarding can be life-threatening. The biggest threat during a fire, for instance, is flashovers. Flashovers occur when so much heat builds up in a confined space that everything within it spontaneously combusts. The more stuff we have, the higher the risk of this happening and the quicker it can happen.

Studies in Melbourne have found that 30 years ago, flashovers would occur between 28 and 29 minutes after a fire had started. Nowadays, this number is closer to three to four minutes because of all the stuff we keep in our homes.

### 4. Overconsumption and overproduction are historically linked. 

When demand escalates, supply has to match it. Overconsumption needs overproduction to feed it, and this is very clearly demonstrated throughout US history.

In the 60 years following the Civil War, the American population increased by a factor of three, from 35 million to 114 million. At the same time, the production of goods increased between 12 and 14 times. This disparity had to be dealt with, or radical production cuts would need to come into effect, leaving many unemployed or even causing an economic collapse. In the 1920s, addressing this gap between production and consumption was one of the major challenges faced by the US.

Those associated with economist John Maynard Keynes and industrialist W. K. Kellogg opted for cutting people's work hours, thereby producing fewer goods and leaving more time for leisure. But others, such as President Herbert Hoover or General Motors CEO Alfred Sloan, had another solution: consume more.

Previously, prosperity had been considered a direct result of saving money and spending less. Now, the push to spend more fed a virtuous cycle of more jobs, and higher wages and benefits for everyone. After the fall of the Berlin Wall and the Iron Curtain, this materialistic model of prosperity proliferated across the globe.

Materialism and the notion of consumption as a solution to overproduction were dominant principles throughout the 20th century. But these same ideas have become the root causes of the 21st century's most pressing problems. 

A significant proportion of the climate change occurring today, for instance, comes from burning waste. This waste most often consists of cheaply made, oil-based products made for brief consumption rather than lasting joy.

Rest assured, however, we can all change this. In the 1930s, the elite decided that consuming more was the way to go. Today we face another crossroads — but now, more of us can speak and be heard.

> Fact: Henry Ford proclaimed in 1922: "We want the man who buys one of our cars never to have to buy another", in 1932, in times of crises, it changed and every year saw a new style of Ford cars.

### 5. Minimalism, a simpler life or opting for medium chill are possible, but not perfect, cures for Stuffocation. 

So what can you do to overcome stuffocation? There are three approaches you can try: minimalism, a simpler life or setting your life up for _medium chill_.

Minimalism basically involves sifting through your possessions and eliminating the majority of your accumulated stuff.

A handy way to do this is to put all your stuff in boxes for 21 days, and only open them when necessary. You'll find that many of the boxes will remain untouched, and that you could easily get rid of the stuff you hardly use. With this minimalistic approach, not only are you helping limit environmental harm, but you also don't have to work as much, since you consume less. 

Another approach is to opt for a simpler life.

People pursuing a simpler life might move to the countryside and completely refuse modern consumer goods that even a minimalist might buy, like an iPad or a new car. While a minimalist is still likely to live in a modern home while owning fewer things, someone choosing a simpler life opts to live with even less. 

This route isn't easy, and a lot of time is dedicated to just surviving with the most basic necessities. As a result, a simpler life is seen by many as too boring or too difficult. Even so, it remains an option for the few who can handle it.

A more popular approach is to go for what environmentalist David Roberts calls _medium chill_.

Imagine being offered a promotion, but turning it down. Perhaps you're fine with letting other people speed ahead of you in the rat race; you're happy to settle with what you already have. This is medium chill. It's not about abstaining from material goods, but it implies opting for more free time and less work. 

However, medium chill runs counter to the "big chill" that some go for, which entails working more than 80 hours per week until you hit your 40s, and then selling your company in order to retire and do nothing.

> _"Stuff, in short, is good because it is human, and useful, and social, and fun. It feels good, it makes us feel good."_

### 6. Experiences last longer than material goods – wisdom you can live by without becoming a hippie. 

Should you spend your bonus on your next vacation or splurge on a new car? A concert or a new outfit? These are questions of whether to _do_ or to _have_. Let's make the decision a little easier: having is not all it's cracked up to be.

In a 2003 study by psychologists Gilovich and van Boven, participants were asked to think of experiential and material purchases they had made, and were then asked questions about how happy it made them and how much it contributed to happiness in their life. The results showed that experiences bring people more happiness than material possessions.

The cure for stuffocation, then, is _experientialism_. Experiences hold a higher value than material possessions for a number of reasons.

Experiences, even negative ones like that camping trip where you endured a week of torrential rain, can be reinterpreted in a positive way. Perhaps, for example, that same trip was also a bonding experience for you and your family. Experiences contribute more to our identities than possessions because they allow us to grow and develop. 

Experiences are also more difficult to compare with one another than belongings. While it might be easy to see which new car is more expensive or whose laptop is superior, it's tough to argue about whether a holiday in Thailand is better than a trip to Provence. With experiences, you get far more benefits and personal growth — regardless of whether it was a great time or not!

While experientialism can be considered post-materialistic, it is not to be confused with hippie culture. Experientialism doesn't reject materialistic values as the hippies did, but instead aims to transcend them. Experientialists are happy to enjoy what they need, but they don't chase after status, meaning or happiness through material possessions. 

Since they don't view material consumption and experiences as black and white extremes, they are still quite happy to exist within mainstream society.

> _"The experimentalists' minds may be focused on a new set of values, but their feet are firmly planted in mainstream culture."_

### 7. Experientialism is the best and most widely accepted way to overcome stuffocation. 

Experientialism is a widely embraced philosophy. People from all walks of life love taking long trips, going skydiving, eating great food and swimming in the sea. 

But experientialism does have its downfalls. As experientialism can itself lead to elevated social status, it can unfortunately also lead to status anxiety.

Philosopher Alain de Botton stresses that this status anxiety is a real issue. As we tire of all our stuff, the new fear is one of missing out on an experience. Four out of ten people aged between 18 and 34 in the US and the UK now feel this new social pressure.

The new emphasis we place on experience can be seen everywhere, from the immense popularity of live music and festivals, to increasing spending on holidays and tourism. Even the growing prevalence of e-books instead of physical copies reflects an overall trend: people care more about the reading experience itself than the ability to display books on their shelves.

We can also see this trend toward experience and away from materialism in policy decisions worldwide. How well a country performs is no longer measured by GDP alone; more and more indices are instead expressing progress in terms of quality of life or the well-being of inhabitants, illustrated by measures like the Human Development Index and the Index of Economic Well-Being. 

The trend toward experientialism is global in its scope. While many newly industrializing countries, such as China, Vietnam and Brazil, are now in, or are at least approaching, a phase of mass consumption, the new and rising middle classes aren't just reaping the benefits of materialism — they're also being faced with its disadvantages, such as resource scarcity, environmental damage and status anxiety. 

The rapidly expanding overconsumption in these countries is paving the way toward stuffocation, and citizens of these countries will soon come to a crossroads, with one way pointing toward experientialism. These countries, too, will come to understand experiences as the new luxury.

### 8. Experientialism does not run contrary to the modern economy. 

While they can be alright for some, minimalism or leading a simpler life don't really fit with today's economic system. Experientialism, on the other hand, does.

Companies trying to sell products today can no longer stand above the rest simply by making the most noise or through flashy advertising, as used to be the case. Consumers now crave status and experience, so brands who want to stay ahead of the curve will need to keep this in mind. 

Our economy needs people to spend in order to function. In fact, consumer spending constitutes around 65 percent of the British economy and up to 70 percent to the US economy. Consuming less would put jobs at risk, and prosperity in traditional terms would suffer. 

But experientialism is not anti-consumption — it's a different kind of consumption. Our economy needs to transform into an experience economy.

So what would an experience economy look like? It would offer memorable experiences. 

One example is the Secret Cinema in London. Here, a movie is shown at a special location with an added touch: the audience dresses up, and the staff take on roles that match with the movie. Because it offers such an immersive experience, moviegoers are willing to spend around 50 pounds to take part in it. 

The global economy has already started following the experiential trend; if we want to keep selling services and products, we need this trend to continue.

Apple are particularly adept at making shopping an experience. Buying an Apple product is not just about the product itself — you are buying an experience. 

Apple stores were some of the first in the world in which customers could try out all the products themselves. Apple understood the importance of user experience in order to get more customers, and they even plan the experience of opening up the packaging around a new iPhone or MacBook. 

Companies are also doing away with clutter. Puma have produced shoe bags that simply disintegrate after three minutes when placed under water, making for less clutter and simultaneously providing an interesting experience.

### 9. Final summary 

The key message in this book:

**We have far too much stuff, and our possessions have become overwhelming. But this is changing as more and more people opt for experiences over things as the new way to live a meaningful life. This trend is not only a cure for the problems — and sometimes crises — that stem from materialism, but it can also help boost the economy.**

Actionable advice:

**Do you really need to own it?**

The next time you're contemplating a new product, find out if there is some alternative way to access it, borrow it or use it in a more resource-friendly way.

**Change the way you look at the price tag.**

The next time you look at the price of that new phone you think you should upgrade to, ask yourself what you could do with the same amount of money in terms of a holiday, or signing up for a course you've always wanted to take.

**Suggested** **further** **reading:** ** _The Life-Changing Magic of Tidying Up_** **by Marie Kondo**

_The Life-Changing Magic of Tidying Up_ isn't just a guide to decluttering, it's a best seller that's changed lives in Japan, Europe and the United States. The _Wall Street Journal_ even called Marie Kondo's Shinto-inspired "KonMari" technique "the cult of tidying up." Kondo explains in detail the many ways in which your living space affects all aspects of your life, and how you can ensure that each item in it has powerful personal significance. By following her simple yet resonant advice, you can move closer to achieving your dreams.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### James Wallman

Futurist and best-selling author James Wallman has appeared on MSNBC and the BBC, and his opinions have been cited in publications such as _Time_, _The Economist_ and _The New York Times_. He also wrote the futurology column in _T3_ magazine.

