---
id: 529ee41b3632620014000000
slug: stumbling-on-happiness-en
published_date: 2013-12-04T10:50:21.000+00:00
author: Daniel Gilbert
title: Stumbling on Happiness
subtitle: None
main_color: C0264D
text_color: C0264D
---

# Stumbling on Happiness

_None_

**Daniel Gilbert**

_Stumbling on Happiness_ (2007) explains how our brains make us think about the future. It employs accessible language and everyday examples to help us understand complex theories from psychology, neuroscience and philosophy.

_Stumbling on Happiness_ helps answer the question: why do we make decisions that leave us unhappy? By showing how our brains work, it aims to help us imagine our futures in new ways, ways that could leave us happier.

---
### 1. Our minds are capable of filling in missing details without us knowing about it. 

Though we're not aware of it, everybody's field of vision contains a blind spot, a place where the eye cannot see images. Yet, when you look at a photograph or someone's face, you don't see a big black mark where your blind spot is; you seem to see the whole image. 

This is because your brain fills in the missing details automatically. It instantly scans the area around the blind spot and fills in what it thinks should be there. The mind effectively invents a portion of your vision without you knowing about it.

This example shows the immense capability of the mind to fill in missing details and transform our perception of reality. We assume that what we see is a true reflection of the world, and yet it isn't: it's partly the construction of our mind.

But the brain's ability to fill in details goes far beyond adjusting our vision: it also influences how we remember past events. When we remember the past, we can't possibly accurately recall everything that's happened; there is just too much information to store. Therefore, what our minds do is store only key details and emotions. 

For example, if you've had a bad dining experience at a restaurant, when you think back to the evening in question, you might remember being angered by a rude waiter or drinking corked wine. But along with these key details, your brain will fill in the surrounding picture as best it can with what it assumes should be there. For instance, it may insert the detail that the rude waiter had a mischievous grin as he served the spoiled wine. This, of course, was not really the case. As with the blind spot, our brains fill in these details so quickly that we don't even know it's happening. 

So, although we consider our memories and our vision as accurate representations of facts, they are in fact a mixture of reality and imagination. 

**Our minds are capable of filling in missing details without us knowing about it.**

### 2. We trust that our predictions of the future are accurate – yet they are merely single scenarios in a sea of possibilities. 

Let's say that tonight you're planning to go out to a pizzeria you've never been to before. You allow yourself to daydream about this little indulgence, and your imagination willingly conjures a very detailed scene of how you think the evening will unfold, right down to the waiter's twirled moustache and the sizzling mozzarella cheese on your pizza. You can hardly wait!

The above is an example of how your mind can create a very vivid and credible prediction of the future based on just one simple piece of information: that you intend to eat pizza.

The problem is that after you've imagined this particular scene in your future, you will tend to imagine this as the one and only way the evening could unfold. You expect things to turn out as you imagined them.

But remember, the only thing you know for sure is that you intend to have pizza tonight. Your brain filled in all those other little details in your prediction. In fact, there is an infinite number of alternative courses that the evening could take. Maybe the restaurant has no pizzas with mozzarella? Maybe the waiter has a beard instead of a moustache? Or maybe the whole place burned down the night before?

And yet, despite these alternative futures, you will probably still consider the fantasy you constructed as a good guess of what is to come, and put an unwarranted degree of trust in this "prediction."

Unfortunately, this applies to all our predictions about the future: they're largely based on our imagination helpfully filling in details, but we still put our faith in them.

**We trust that our predictions of the future are accurate — yet they are merely single scenarios in a sea of possibilities.**

### 3. Our current emotional state heavily influences how we think about the future – which leads to mistakes. 

Have you ever done a week's grocery shopping only to discover mid-week that you've completely underestimated the amount of food you needed? Interestingly, this could be because you made the mistake of doing your groceries on a full stomach. 

People often mistakenly believe that their predictions about the future (like the amount of groceries they need to survive a week) are totally rational, but in fact our current emotional state strongly influences how we imagine future events.

For example, if you feel full when you're in the grocery store, it's hard for you to imagine feeling hungry in the future — which means you'll tend to buy insufficient food. 

Why?

Because your brain is much more concerned with the present moment than the future. This trait evolved because it is a necessity for survival: our ancestors had to focus on the sabre-toothed tiger stalking them in the present, rather than daydreaming about the future. 

This tendency is so strong that you can't even will yourself to feel hungry in order to make a better prediction about the future. If you are feeling one way in the present (not hungry), you cannot imagine feeling another way in the future (hungry).

This tendency extends beyond just food: our emotional state can color our perception of the future in other ways, too. For example, if you are feeling angry and think about some future event, say, an important presentation at work, you will likely see it in a rather gloomy light, perhaps predicting it as a disastrous experience where no-one will listen to you. This pessimism might then cause you to worry unnecessarily about that presentation, and you might even cancel it altogether, when in all likelihood it will go well.

**Our current emotional state heavily influences how we think about the future — which leads to mistakes.**

### 4. We should value products based on how much satisfaction we get for our money – not how much their price has increased. 

When we see a product with a price tag, we usually have a gut feeling as to whether the price is fair or not. But how do we decide this?

Most of the time we use a simple measuring stick: does this item cost more or less than it did in the past? If the price has risen, we tend to feel it is now overpriced, whereas if the price has fallen, we feel we're getting a bargain. Let's say, for example, there are two identical but differently priced holiday packages: Package A, which costs $500 but was reduced from $600; and Package B, which is $400 but has increased from $300. Unbelievably, most people would choose Package A because they think it's a bargain, despite the fact that it's less value for their money.

Obviously, this approach is far too narrow. A more sensible method would be to compare the item in question to other items that the same amount of money could buy.

Think of a cup of coffee. If its price goes up from $1.50 to $2, we may feel it is now an overpriced rip-off. But if instead we compare it to other things that $2 could buy — say, a single sock or ten minutes of parking — then the coffee becomes a steal because it will give us more satisfaction for $2 than the alternatives would.

However, valuing products based on their past price comes to us so naturally and effortlessly that we tend to forget about other possible uses for that $2, and therefore we think the coffee is overpriced.

**We should value products based on how much satisfaction we get for our money — not how much their price has increased.**

### 5. We can’t trust our memories because we remember the strange and unique over the mundane and normal. 

Imagine you went camping and, as with most camping trips, you spent most of the time battling mosquitoes and sleeping on rough ground, or even jagged stones. Afterward, your memory of the event would probably be dominated by these mundane concerns and you'd think twice about going camping again.

Now imagine that, after spending a week enduring these conditions, you stumbled over a rock and found $100 buried underneath. It's a good bet your memory of the trip would be transformed: This unexpected surprise would dominate your memories over the insects and sleeping discomfort. You might even think camping isn't so bad after all.

Situations such as these show how we can't trust our memories when making decisions. Bizarre moments, like finding $100, take precedence in our memory because we pay them special attention. The other normal, boring details, like the sleeping conditions, can be taken for granted and so are forgotten or overlooked in favor of these unique ones.

Another fault with our memories is that the mind naturally assumes that the things we recall easily must happen more frequently. Because we are unaware of why we remember unusual events more clearly, we incorrectly assume that these unusual moments are more common than they are. Since we focus on unique events rather than the whole experience, a few fantastic moments can make us remember the entire experience as being better than it was.

Together, these two factors lead us to remember past experiences wrongly. Unfortunately this can lead us to make mistakes when we try to replicate the unpleasant experiences, like camping, that we falsely remember as being great.

**We can't trust our memories because we remember the strange and unique over the mundane and normal.**

### 6. The false belief that money makes us happier spreads because it is beneficial for society as a whole. 

Why do you think great ideas, like a recipe for delicious cheesecake, get passed around so widely and quickly? Quite simply, because people like to pass on ideas and advice that are accurate (i.e., _true_ ).

However, as anyone who's ever heard an untrue rumour knows, inaccurate information can also be spread. But why would anyone wish to share misinformation? In fact, this only happens when it is in our own interests, and in the interests of the community, that the false belief is spread. 

A good example of this is the well-known belief that the more money you have, the happier you are. To a certain extent this is true: wealth increases happiness when it gets people out of total poverty and into the middle class. Americans who earn $50,000 a year are much happier than those who earn $10,000. But, beyond this level, having more money does not increase one's happiness to any real extent. So the belief that more money makes you happy is false.

Yet the idea still spreads. Why?

Although having more money does not necessarily make individuals happier, it is vital for the growth of the overall economy. If everyone was content with what they had, no-one would buy anything and the economy would collapse.

A stable society depends on a strong economy to survive; it therefore needs people to strive to earn more money. The false myth that money leads to happiness helps achieve this. This means we happily nurture and spread this myth even though it is false — it helps the very societal system we live in to survive.

**The false belief that money makes us happier spreads because it is beneficial for society as a whole.**

### 7. We think we are unique so we don’t ask others for advice – even though we could benefit from doing so. 

We like to think that, as individuals, we're pretty unique. We're convinced that we are in some way special and that everybody else is completely different from us. We feel this way not out of petty selfishness but because our brains are wired this way. Yet, this attitude causes us to make a grievous mistake: we think we're so unique that we cannot possibly benefit from the help and advice of others.

Imagine you were thinking about quitting your job to go travelling around the world. You would probably sit there for hours on end, thinking about the pros and cons, mulling over the same thoughts again and again. Someone might suggest you talk to a friend who went on a world trip last year, but you shrug them off: that person was in a completely different situation, you think. You believe your thoughts and experiences are so unique that no-one else can offer relevant advice.

However, our experiences are not as unique as we assume. In fact, the solutions to a great many of our concerns lie in the experiences of others. People react to things in pretty similar ways. Studies have shown that you can make accurate predictions about your feelings based on a report of someone who has been through the same experience.

So, instead of thinking on your own about that trip around the world, it is better to share your concerns with someone else who has done exactly what you're planning. You might be surprised at how similar their situation is after all.

**We think we are unique so we don't ask others for advice — even though we could benefit from doing so.**

### 8. Always take action to seize the day; we can learn from mistakes but will always regret inaction! 

Which scenario do you think you would regret more: marrying a person who later becomes an axe murderer, or not marrying a person who later becomes a Nobel Prize-winning billionaire or movie star?

Surprisingly, the answer is the latter. This is because our brains are fundamentally wired to make even bad decisions look better in hindsight. When experiences are unpleasant, we quickly try to explain them in ways that make us feel better. So, while marrying that axe murderer may have been a mistake, we can always tell ourselves that we are stronger because of it, or that we've learned something valuable, like how to recognize signs of psychosis in other people.

We regret inaction more because our minds have a harder time coming up with positive views of events we don't have first-hand knowledge about. Our brains can't extract the positives from our having done nothing. This means that when we pass on marrying the movie star, we simply regret it, without being able to say we learned something from the experience.

We rarely, however, realize our mind works this way. Studies show that people believe they will regret foolish actions more than foolish inaction, which may make us more hesitant to try something we're unsure about. Yet, in reality, the most commonly voiced regrets are always of things people didn't do, like not going to college, not starting their own business or not taking that trip to the Andes.

So if you are hesitant about doing something, the best option is just to go for it. You can always learn from your mistakes, but you won't learn anything from inaction.

**Always take action to seize the day; we can learn from mistakes but will always regret inaction!**

### 9. A chipped nail can feel worse than a house fire: your mind defends you against the more traumatic experiences. 

From time to time, we all face unpleasant events in life. These range from minor things, like chipping a nail, to major disasters, like getting fired or your house burning down.

Strange as it might seem, we often feel worse after the trivial misfortunes than the more traumatic ones.

This is because when an experience is traumatic enough, it triggers a built-in psychological defense mechanism in our minds, which is there to ensure we are not mentally crushed by devastatingly unpleasant events. Thanks to this mechanism, someone whose house has just burned down may actually start to feel better much sooner than they expected.

However, these defenses do not engage in the face of experiences that are only slightly unpleasant. This means that, effectively, a slight misfortune like a chipped nail can make us feel worse for longer than having our house burn down. Alternatively, we may be quick to forgive our spouse for cheating on us but remain miffed for months at their bad habits, like them leaving dirty underwear on the floor.

Of course, we don't realize that we're reacting in this bizarre way. This means that we're inherently poor at trying to imagine how we would react to traumatic events and how long we're likely to grieve over them.

**A chipped nail can feel worse than a house fire: your mind defends you against the more traumatic experiences.**

### 10. Although we value freedom and choice, we are often happier when we can’t change things. 

Imagine it's your birthday and you've just received a fancy watch as a gift. Now ask yourself: If you could choose, would you like the gift to be exchangeable in case the store had something more your style?

Most people think that, in any given situation, it is always better to have more options. Yet, this is often false reasoning, because when we have options, we start wondering about whether one of the alternatives would be a better choice. If you know you have the choice to exchange the watch, you'll probably examine it more critically, looking for reasons why you should exchange it.

This means that, paradoxically, we will often feel happier in a situation where we have no choice whatsoever than in one where we have many options. As a result, if you know you can't exchange the watch, you'll probably see it in a much more positive light, focusing on its good qualities and feeling more satisfied with it.

Unfortunately, we don't know that we react in this way, which is why we constantly seek out situations that give us more choices and more freedom. We don't understand that sometimes it is actually a lack of freedom that can make us happy.

**Although we value freedom and choice, we are often happier when we can't change things.**

### 11. An explanation robs an unexplained event of its mystery and emotional impact – with both positive and negative results. 

Having a secret admirer who drops off gifts at your doorstep can be flattering and exciting. But if you find out who it is, chances are that you will feel a little deflated. This is because an explanation of those mystery gifts changes the way you feel about getting them.

Unexplained events and phenomena hold a special allure for us, for two reasons.

First, we feel intense emotions about unexplained events because we think of them as rare and unusual, and rare events naturally trigger stronger reactions in us than mundane ones; for example, we get more excited by our birthday than we do about a normal Tuesday.

Second, we think about the unexplained for longer because we search for ways to explain it. The longer we think about something, the longer our feelings toward it will last, thereby exaggerating those feelings further.

Providing an explanation for an unexplained event helps mitigate these factors. This can be beneficial when the unexplained experience is negative; for example, explaining and discussing a traumatic event, such as a car crash, is usually beneficial for the victims.

Yet in the same way, if the unexplained event is making us happy, then the explanation can rob us of this happiness. Not knowing who your secret admirer is will likely make you feel curious but happy as you ponder who it might be. However, as soon as you find out it's your roommate's second cousin, the mystery and its emotional impact disappears.

**An explanation robs an unexplained event of its mystery and emotional impact — with both positive and negative results.**

### 12. Your friends may not be as unbiased as you think: we unknowingly surround ourselves with those who support our views. 

You probably know people who are wildly optimistic in their views — people who see every glass as being half-full and who think that every difficult situation has a silver lining.

You might wonder: How do these people manage to see everything in such a positive light? The reason is that they only see what they want to see; they surround themselves with information that backs up their positive worldview.

To a certain extent, we all act in the same way. We carefully control the information we are exposed to, paying more attention to information we regard as positive and ignoring everything else.

A clear example of this can be seen in the care with which we select our friends. We tend to surround ourselves with friends who think like us and approve of the person we are. This means that when we ask them for their advice or an opinion, more often than not they will echo our own sentiments. They do this either because they share our opinions or just don't want to hurt our feelings. Either way, the information we receive is heavily biased.

We go even further to ensure we get the right information. When asking friends for their opinions, we often rig the question to elicit the answer we most want to hear; for example, we might ask, "What do you like best about me?" instead of "Do you think I'm a good person?" We don't know that we are instinctively doing this, and so we trust the answers given as being honest and accurate.

**Your friends may not be as unbiased as you think: we unknowingly surround ourselves with those who support our views.**

### 13. Final Summary 

The key message of this book is:

**We use our memories and imagination to make choices about our future. Yet, we are not aware of how our brains make these processes work. This lack of awareness leaves us prone to making mistakes when deciding about the future. Therefore, the choices we make will often leave us unhappy.**

Actionable ideas from this book in blinks: 

**Be bold.**

If you're faced with a decision of whether to do something or not, just do it. Your mind is an expert in taking the positives out of your experiences, but it cannot do the same when you don't act. So, even if the decision does not pan out the way you expected, you will still be able to take something from the whole scenario.

**Ask people about their experience.**

When you are unsure about doing something, it is often better to ask someone who has been through a similar thing. You might think that, because you are unique, your experience of something will be different to everybody else's. You might therefore think that asking for advice from others would be misguided — but you'd be wrong. We all feel a similar way when confronted by similar things. So, by asking someone how they felt, you will in fact get a glimpse of how you will feel.
---

### Daniel Gilbert

Daniel Gilbert is a Professor of Psychology at Harvard University who has won numerous awards for his teaching and research. In addition to the international bestseller _Stumbling on Happiness,_ his essays and writing have appeared in many publications including the _New York Times_ and _TIME._

