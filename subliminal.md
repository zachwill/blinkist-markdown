---
id: 531706643764650008460000
slug: subliminal-en
published_date: 2014-03-05T11:24:48.361+00:00
author: Leonard Mlodinow
title: Subliminal
subtitle: How Your Unconscious Mind Rules Your Behavior
main_color: 5EBC44
text_color: 387029
---

# Subliminal

_How Your Unconscious Mind Rules Your Behavior_

**Leonard Mlodinow**

_Subliminal_ (2012) shows us as we are, under the bonnet. It's about how the unconscious mind is in charge, working away like an efficient yet imperfect machine, while we go on with our lives unaware. The reader finds studies, examples and anecdotes about the peculiarities of the unconscious mind, such as the pitfalls of memory recall, choosing a mate, buying a stock or scheduling a project. These are mined from historical events, science experiments and the authors' own experiences, as well as from his friends in the scientific community.

---
### 1. What’s in it for me? Learn how our unconscious affects how we make decisions. 

How do we make decisions? Do we weigh up all the facts equally and make an informed choice?

_Subliminal_ suggests otherwise. Instead of being conscious actors we are driven by our unconscious mind.

The dominance of our unconscious mind can lead us to do strange, irrational things and _Subliminal_ will show you what these are. For instance, it will show you that we are more likely to be attracted to someone if we are standing on a high bridge.

By reading this book in blinks you will gradually understand the answers to important questions from "why do I think I am special?" to "why can't I seem to deal with my emotions effectively?"

### 2. Many theories tried to explain the unconscious mind but modern technology finally revealed its complexity. 

For millennia, philosophers have speculated about the nature of the _unconscious_ _mind_.

For example, in the 1700s Immanuel Kant proposed that our mind does not experience objective reality, but rather that it creates its own version.

Then, in 1900, Sigmund Freud postulated an interpretation of the unconscious mind that became very popular. Freud said that the unconscious is often unnatural and unhealthy due to our repression of incestual attraction and painful memories.

Yet this view didn't stand up to scientific rigor as it was mythical, dream-like, irrational and, as a theory, highly unreliable.

As it was so hard to unlock the secrets of the unconscious mind, later research moved away to other areas. Many scientists began to argue that humans were like animals: complex yet predictable machines with brains like computers.

However, by the 1980s new research had re-ignited our interest in the unconscious. And at the same time new technology finally enabled us to map the brain's organization and activity.

Technology called _functional_ _magnetic_ _resonance_ _imaging_ has allowed neuroscientists to scan the brain's blood flow changes and structure with 3D precision. From the activity of nerve cells and the oxygen consumption of research subjects performing tasks, mental processes can be linked to specific neural pathways.

With this information we can make great strides in explaining the unconscious.

We know of the brain's three interdependent layers. The deepest and oldest is "reptilian" — responsible for tasks like breathing, eating and the fight-or-flight instinct. We share it with reptiles, amphibians, birds and fish.

Over this part sits the more sophisticated "old mammalian" or limbic system, responsible for unconscious social perception and behaviors.

Above this is the neocortex, which promotes goal-related actions and conscious thought. It also deals with the unconscious processes of rendering vision and precise motor movements, such as fingers and facial expressions. We humans have this part in abundance.

### 3. The unconscious mind collects raw data from our senses and summarizes it for our conscious mind 

If you were to hear a loud, unexpected bang, the chances are that you would react instantly and jump out of your skin. That's because when your unconscious mind perceives a threat it acts instinctively to keep you out of danger.

This subliminal awareness of the environment evolved in our ancestors long before conscious awareness. Its primary goal was to keep us alive, to avoid potential threats, and find food and mates.

Our unconscious mind gathers this data from all of our senses and uses it to keep us safe.

A great example of this is vision. Vision is processed in a small part of the brain called the visual cortex, using the light received by the eyes. But even when people's visual cortex is damaged, the unconscious mind can help us. This is called _blindsight_.

For example, a man who had a stroke and went blind had both visual hemispheres of his brain wiped out. Yet his eyes still gathered light as normal and his unconscious mind was still able to use this. The man showed researchers that he could guess whether a face shown to him was happy or angry. He could also navigate an obstacle course without walking into any obstructions.

But the detail that our unconscious mind receives from the senses is imperfect. To turn this into information that our conscious brain can use, the unconscious mind takes all the raw information and filters it.

For example, our vision is flawed. Each of our eyes has a blind spot where the nerve connects the eye to the brain. Our eyes also jerk around unconsciously many times every second. What's more, our vision outside a small central area of focus — about a thumbnail's width at arm's length — is akin to having severe short sight.

You might think we would notice these irregularities. But the unconscious mind processes the information supplied to it from both eyes to form a steady, smooth image that we can consciously use.

### 4. Body language allows us to unconsciously interpret the behavior and opinions of others. 

Our bodies are just as expressive as our words. Without body language, we would find it hard to sense the intentions of other humans. For example, each of us has relied on body language to signal such things as potential threats, friendships and relationships.

We inherited body language perception from our distant ancestors, and it is as natural and essential to humans as it is for other animals.

For example, we share peace signals with our primate cousins. Monkeys bare their teeth to avoid attacks from more powerful monkeys, while dominant chimps smile to indicate they're not a threat to others. Smiles are also vital signals for humans and they can't be faked. They need to move both the muscles around the mouth — which we can move on purpose — and those around the eyes too, which we can't deliberately move.

Not only are they hard to fake, facial expressions are also universal and innate rather than learned. Expressions such as fear or disgust are identified in the same way by tribes around the world.

That's why we don't need to put effort into making our own gestures or reading those of others. Our unconscious mind does it.

However, body language is not limited to conveying social signs. We communicate our expectations of people and affect their behavior without intending to.

In one study, a group of students were given photos of faces that the researcher said had been judged as exuding success or failure, but which had in fact been judged as neutral.

The students, following a script, asked another set of students how much they thought each face conveyed success or failure. This second group confirmed the expectations of the students asking the question. They automatically picked up on and responded to the first students' unconscious body language.

### 5. Subtle changes in our voice advertise our attractiveness to others and reveal the nature of our character. 

Many people interpret evolution as telling us that men seek dominance and sex, while women want a loyal mate and babies. The evidence for this comes from analyzing our body language, especially the tone, pitch and volume of our voice.

Indeed, men and women unconsciously adjust their voice for sexual success.

In one experiment a series of males competed for a date with a female. They could see the female, but could only hear the other males. Results showed that the men lowered or raised the pitch of their voice depending on how powerful they felt compared to their rivals.

Other results showed that women find lower-pitched voices especially arousing when they are ovulating, and at that time women's voices also become smoother, which attracted men in the process.

You might think women's unconscious preference for lower-pitched voices might come from these deeper-voiced men being taller and having more muscles. In fact, there's no link to physique, but there is one to testosterone. This was shown when an anthropologist trekked to meet Tanzanian hunter gatherers who have no contraceptive methods. He found that men with higher testosterone fathered more babies, showing why women favor men with lower-pitched voices.

People also judge the characters of others based on their voice.

To research this, scientists took vocal recordings and mixed them up so subjects could hear the qualities of the voice without the meaning behind it. They also adjusted the speed and pitch. Subjects believed high-pitched voices were more dishonest and anxious, and less convincing. Slow voices seemed less truthful and persuasive, but also more apathetic. Voices that were faster, louder, smoother and with variation were deemed to be lively, knowledgeable and bright.

For example, as a young politician Margaret Thatcher wanted to rise through the ranks of her party. But fellow members remarked that her high-pitched voice was "dangerous to passing sparrows." She worked hard to lower her pitch, and in the end her new powerful voice helped her to become prime minister.

### 6. We can’t remember everything, so our unconscious mind fills in the gaps in our memories – but it isn't always accurate. 

However much we'd like it, our brains are not video cameras, recording everything for our later recall. The amount of information would be overwhelming to our brains.

Instead, our memory evolved to discard the majority of our experiences and save memories in general form — especially the important ones.

The important memories include friends and foes, prey and predators, where they're found and how to get home. Storing the gist of these in easy-to-retrieve places helped our early ancestors.

You'd think that this way of remembering would constrain us and that we would do better if we remembered every last detail of our life. But this would be a massive hindrance.

For example, a man called Solomon Shereshevsky was famed for his amazing memory. He could remember exact series of words spoken, but he didn't understand the meaning of the sentences he recalled. He stored the many versions of each face instead — for example, from a different angle or with a different expression, and therefore he had no difficulty recognizing familiar faces.

However, as we remember only small aspects of our experiences, we need something to piece everything together into coherent stories. Our unconscious mind does this. It creates stories from fragmented memories and the situations we find ourselves in. Unfortunately this can lead us to make mistakes.

This is highlighted by research which shows that 25 percent of witnesses pick an incorrect suspect from a lineup, and 75 percent of people that are exonerated from crimes by DNA evidence were originally convicted due to incorrect eyewitness statements.

One such case saw a rape victim pick out from a police lineup the man she thought was her attacker, and he ended up being jailed for the crime. But the real attacker wasn't in the lineup. A retrial saw the victim again pick the false attacker, even after seeing both men. She misremembered the attacker's face and the first trial reinforced the false memory, which only DNA evidence resolved.

### 7. Our emotions arise from unconscious biases and sensory data, so our conscious mind can’t uncover their roots. 

The human brain is not particularly adept at helping us understand our feelings and emotions. This is a legacy from evolution — the main drive of our ancestors was to stay alive and breed, not to comprehend themselves. This makes it hard for us to recognize and deal with our emotions.

This difficulty partly arises from the fact that our feelings are a product of our unconscious mind. Emotions work in the following way: the environment supplies data to our senses, to which our unconscious mind produces a physiological response. It is this response that we experience as emotion.

But because they arise from our unconscious, we find it difficult to understand and recognize our emotions correctly.

For example, in one study, male subjects who were questioned about a school project by an attractive female researcher on a high bridge were more likely to call her later on. This was because the increased heart rate and alertness due to the surroundings combined to create an emotional response which their conscious minds took as flirtation.

Yet this inability to comprehend our emotions doesn't stop us from confidently thinking that we can explain them.

When we ask ourselves why we find someone likable or dislikable we provide detailed answers, but they often turn out not to be true indications of our unconscious feelings.

A study showed this: male participants were given photos of two women and asked which one they found more attractive. Then the participants were passed the same photos again, but this time face down. They were asked to look again at the cards and explain why they had preferred one over the other. The test continued with more photos of many different women.

What they didn't know was that for some of the comparisons, the researcher had switched the photos. And most of the participants were unaware of the change. Nonetheless, their answers still reflected their initial answer — they were explaining their feelings to the wrong photos.

### 8. We are biased in our judgments and decision making when we defend previously held beliefs. 

Our minds are made up of two opposing characters. We have our conscious mind, which thinks like a scientist — weighing up evidence and seeking the objective truth. Then there's the unconscious mind, which acts like a lawyer — first making a decision, then defending its position.

Unfortunately for us, our lawyer brain is the stronger of the two characters. This leads us to support conclusions that affirm our beliefs rather than look for any alternatives. Also, this makes us adjust facts to fit viewpoint and ignore unfavorable evidence; this is an example of _motivated_ _reasoning_.

All of us do this; even scientists are not averse to these kinds of biases. They too think like lawyers sometimes.

For example, in the 1950s and early 1960s one camp of scientists believed that the universe existed in a steady state with no start or end, while another believed it started with a big bang. When the afterglow of the big bang was discovered by radio astronomers in 1964, some of the steady state proponents continued to hold onto their beliefs, even 30 years later, despite the unambiguous evidence.

This defense of a pre-decided belief or position can dramatically alter how we interpret information. Even when different people are given identical information, their previous beliefs can ensure that different conclusions are drawn.

For example, in one court case a motorcyclist sued a driver for injuries sustained in a crash and was awarded a cash settlement. In response to this, researchers set up a mock repeat of the case, giving volunteers the chance to represent the plaintiff and defendant. As an extra incentive, the volunteers were offered a bonus payment if they guessed the correct settlement from the real case.

What's interesting is that despite the chance of losing the bonus, the volunteer plaintiff's estimate of the real settlement was double the volunteer defendant's estimate. They took on the positions of the roles that they played and gave widely different conclusions to the evidence.

### 9. We all think that we are special and this makes us prone to overestimating our abilities. 

In 1959, three mental patients who claimed to be Jesus were put in the same room together, to test how this would affect their self-perception.

When they were introduced, one gave up his belief and resumed normality, one accused the others of being deluded, while the third completely avoided addressing the situation. Unbelievably, in spite of the evidence, two of the three maintained their fantasy.

This may be an extreme case, but we're all guilty of the _above_ _average_ _effect_. This manifests itself in a generous self-image which is out of sync with other people's perception of us.

For example, a study found that every single individual out of one million high school seniors rated themselves as at least average in getting along with people, while 25 percent put themselves in top one percent. This kind of delusion carries on in later life: 94 percent of college professors deem their work to be above average.

Unfortunately our inflated view of ourselves is at odds with reality. We think we can do things better and faster than we are actually able to.

One area where this is apparent is task scheduling, where we are usually optimistic in our estimations.

Often projects such as new buildings and infrastructure go way over budget because the providers set ideal completion dates. For example, the US General Accounting Office estimated that only one percent of new military technology that was purchased ended up being delivered before its deadline or within budget.

It's not really our fault. Evolution favored individuals with self-belief and that came with an ego that demanded to be honored. This has been crucial to our adaptation and progress as a species. Without this phenomenon we would have remained stagnant as a society, perhaps even still fashioning flints into tools.

### 10. Our unconscious mind compels us to be sociable; the consequences of exclusion include physical illness. 

Before we are able to talk or even walk, we are attracted to friendliness and repelled by hostility. Babies like to touch the faces of people they see help others, and are repelled by faces whose owners hindered others. Research has shown this to be true even if the faces are fake — like bits of wood with eyes stuck on them.

This is evidence that it's in our nature to socialize. And our brains are equipped with the hardware to help us do so.

The human neocortex is unusually large. The neocortex is the newest part of the brain to have evolved and it helps us to understand complex relationships between people. We are able to use these skills in society because we have what scientists call _Theory_ _of_ _Mind._ Only humans have enough of this to work out who everyone is, how they all relate to each other and what each person wants.

Studies have found that there is a direct correlation between neocortex proportion in the brain and the size of a species' social group. Therefore gorillas band together in groups of ten, macaques in groups of 40 and humans have an average social group of 150. That's not just close friends but family and acquaintances too — the people with whom we keep in contact.

But what happens if we don't socialize or we are left out? The consequences of isolation are not just pangs of social pain, but physical pain too.

Physical pain hits us with a two-pronged attack. It puts both our senses and our emotions in turmoil. But what's interesting is that the part of the brain that this emotional element is linked to — the _anterior_ _cingulate_ _cortex_ — is the same region where social pain arises. What this means is that we can take painkillers to reduce our hurt feelings. It has also been proved that social isolation leads to greater chances of high blood pressure, obesity and shorter life expectancy.

### 11. Our social behavior is determined by chemicals in the brain and unconscious habits. 

Scientists and philosophers have long wondered how unique we are as humans. Certainly no other species shares our complexity or capability. Yet we aren't as special a species as we like to think. Areas of our brain are governed by the same principles found throughout nature.

The behavior of both humans and animals is driven by brain chemicals. For example, the release of a few chemicals in the brain has the power to make us trusting. Yet we wouldn't want too much of this, as we'd end up trusting everyone. So it's a finite resource, switched on and off by nature.

For example, a female sheep will usually be hostile to her offspring when they want to suckle. Only when she starts giving birth does she become loving to both her own offspring and others. But after the birth, she's back to her old horrible self. This is all due to the turning on and off of a protein in her brain called _oxytocin_, caused by the stretching of the birth canal, which women also experience during childbirth. Humans also produce oxytocin during sexual intimacy and even in hugs.

Yet it's not just chemicals that determine how we interact; we also follow unconscious patterns of behavior, just like animals.

Researchers wanted to test how compliant people are in ordinary situations. In a library, a researcher waited until someone went to use the photocopier before approaching and asking them if they could use the machine before them. Without a reason to back it up, 40 percent refused. But when a reason was given, even if it wasn't a proper reason, like "I have to make some copies," refusals dropped to just six percent.

Scientists speculate that we follow a set of unconscious instructions like a computer program. When we are quizzed we will give an answer based on this script — for example, we say no unless some reason is given.

Rather than being masters of our destiny, much of our behavior is governed by factors over which we have little control.

### 12. People rely on implicit stereotypes and labels which affect how we see others in society. 

Do you think you are fair and open-minded?

Many of us pride ourselves on having these qualities. Unfortunately, lurking beneath our surface are some murky prejudices.

One of these prejudices is the way we form opinions of someone's character based on their outward appearance.

For example, a study found that people were less likely to tell a member of staff if they saw a shoplifter — really a researcher — wearing a tie and pressed trousers, than one who was unshaven and in workmen's clothes. Those who reported the scruffy man did so with gusto, as if they thought he had made many errors, while those who reported the well-dressed man expressed some doubts that they had seen what happened correctly.

In this situation the unconscious feelings of people were voiced, but we have many biases that are harder to spot.

The Implicit Association Test asks people to respond to a series of words shown on a screen in quick succession. One shocking result of this test was that 70 percent of people associate black people with terms like failure and white people with terms like success, and that proportion includes many people who consider themselves non-racist.

This is not the only example of these hidden prejudices. And these prejudices really affect our society. They might lead us to label all old people as careless or slow-witted, or all women as more proficient in the arts than in the sciences. Media and culture help spread these descriptive stereotypes.

This phenomenon is so prevalent in society that it will take a great effort to avoid them. Identifying them is the first important step. We can also try to avoid making these judgments by spending more time with people that we are prone to label. This will help us become empathetic and override any casual labelling.

We can't control our unconscious mind but we can try to manage it.

### 13. The instinct to identify with groups fuels prejudice in favor of members and against non-members. 

We all have groups to whom we feel we belong. These are groups such as gender, nationality, politics and religious belief.

We switch our identities depending on the situation in which we find ourselves.

For example, residents of New York were divided by factors such as wealth and race, until the attack on the World Trade Center saw these identities melt away, and then everyone saw themselves as New Yorkers.

Although there is flexibility in our affiliations, we have a higher opinion of members of the group we identify with than we have of members of other groups.

For example, a study of professionals such as doctors and hairdressers found that they all rated the members of their own profession higher than members of other professions for their likeability and diversity.

Our attachment to the group can even lead us to feel superior over members of other groups, even if they are assigned at random.

Subjects in one study were assigned at random to be a fan of either Wassily Kandinsky or Paul Klee — two artists for whom they had no preference or specialist knowledge. They were given money to distribute among the rest of the volunteers. Unbelievably they gave more to the people assigned to their own artist than the other one, despite not knowing anything about the other subjects beyond this arbitrary grouping.

This example shows that although we gain psychological comfort from being part of a group, we show prejudice against those who are not members. It doesn't even matter if the groups are totally artificial — we will still unconsciously favor our group at the expense of others.

### 14. We base the choices we make in life – from what we buy to whom we vote for – on the most trivial of details. 

No one would think the weather or the pronunciation of a company's name would affect the performance of stocks and shares. But they do. Companies whose name is harder to pronounce are more likely to fail than those whose name is easy to say.

Many of our purchases are made based on such trivial factors as this — factors of which we are unaware.

For example, the background music in supermarkets influenced which nation's wine customers bought. More people bought French wine when French music was played in the store, and more German wine was bought when German music was played. Of course, when asked, the customers refused to believe their choice was due to the music.

It's not just in our shopping habits that this is apparent. We even elect people to positions of power based on how they physically appear, rather than their policies.

One example of this is a _face_ _effect_ shown to determine in advance the outcome of elections by gauging how competent voters deem the candidates to be based on seeing their photo. A face has the power to swing elections.

In 1960 Richard Nixon and John F Kennedy took part in the first ever US presidential debate. It was broadcast on TV and radio. Nixon was recovering from an infection and looked awful, but still went ahead with the broadcast. Those who watched on TV saw Kennedy trounce Nixon, while many senior Republicans were angry that Nixon had thrown the election away.

But those who listened by radio actually thought Nixon performed better, since he had a deep, resonant voice, in comparison to Kennedy's high-pitched, middle class one. The election was won by Kennedy, but only but a whisker. It was no coincidence the TV audience was larger than the radio audience.

### 15. Final Summary 

The main message in this book is:

**We are unaware of the work that our unconscious mind performs all the time — for example, to avoid danger, find food or be powerful. It has evolved to work well, but it also means that we are unable to understand it, we fail to recall events that happened to us with accuracy, and we like people and objects because of superficial factors. We are prone to adopt an us-vs-them mindset, labelling anyone who doesn't fit into our current group as worse than our own, or just all the same. Our self-perception is generous, while we gloss over our errors.**

Actionable ideas from this book:

**Use body language to sway decisions in your favor.** If you take the amount of time you look at someone's eyes when you are talking and divide it by the time you spend looking at their eyes when listening to them, you arrive at your _visual_ _dominance_ _ratio_. This determines the perception of hierarchy and dominance that our unconscious mind possesses. If it's at least one, you're going to be giving orders, whereas if it's 0.6, you're going to be carrying out the orders. Just think about that when you are trying to get a job or are bartering at the market.

**Calm your body and mind before making important judgments.** If we rush to work because we are late, our bodies produce adrenaline which can lead our conscious selves to say yes to some work-related decision we'd normally be uncertain about. Physiological responses from recent events can taint our decisions in the present. If we are giddy because we are scared, our conscious mind might interpret that as arousal in response to someone we don't really find attractive. And although faking a smile won't win you friends, it has been shown to at least make you happier.

**Buy products based on substance, not environmental factors**. People prefer wine if they know it costs a lot, while a menu that describes items with flowery adjectives makes people rate the food better. We are swayed by attractive packaging and think it means better contents, but in reality the contents are usually no better than the rest. And beware of sunny weather. Although it can make us more optimistic, it seems to make us less careful with our investments.
---

### Leonard Mlodinow

Leonard Mlodinow is a physicist and author of bestselling science books such as _The_ _Drunkard's_ _Walk:_ _How_ _Randomness_ _Rules_ _Our_ _Lives_. He has a PhD in theoretical physics from the University of California and teaches at the California Institute of Technology. His parents were concentration camp prisoners during World War II and their stories feature in _Subliminal_. In the past he has collaborated with luminaries like Richard Dawkins and Stephen Hawking. He has also written scripts for TV science fiction series including _Star_ _Trek_.

