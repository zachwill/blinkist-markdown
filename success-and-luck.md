---
id: 58c688518d90030004072587
slug: success-and-luck-en
published_date: 2017-03-13T00:00:00.000+00:00
author: Robert H. Frank
title: Success and Luck
subtitle: Good Fortune and the Myth of Meritocracy
main_color: BE4B52
text_color: A64148
---

# Success and Luck

_Good Fortune and the Myth of Meritocracy_

**Robert H. Frank**

_Success and Luck_ (2016) reveals the role of luck in the lives of the most famous achievers in sports, business and the arts. These blinks explain how most of us are blind to the impact of luck on a person's success, and also reveal how this ignorance may result in public services being devalued.

---
### 1. What’s in it for me? Find out just how lucky you really are. 

Steve Jobs was an incredibly talented person. He excelled in the business world and achieved things beyond most people's wildest dreams. But does he really deserve sole credit for his success? Did luck not also play a part? It's important to remind ourselves that Jobs had the good fortune of, for example, being born in a rich country with access to computers and other gadgets.

These blinks examine the role that luck plays in all of our lives. You'll discover that luck doesn't just appear out of nowhere and that it also seems to accrue. It favors the same people again and again until they become financial magnates or top sportspeople — but whom it favors is no coincidence. In the end, no person is entirely self-made.

In these blinks, you'll learn 

  * how hindsight bias means we don't appreciate the role of luck;

  * how luck can make us successful; and

  * how public services create the preconditions for lucky countries and individuals.

### 2. In hindsight, everything seems obvious –  but nothing is quite so predictable, especially when it comes to success. 

If you've ever had the misfortune of being doored while pedalling along on your bike, you'll know that no amount of foresight or anticipation can compensate for the fact that the world is unpredictable.

Even so, hindsight can play tricks on you. If you replay the bike accident in your mind, it will seem all so preventable. This phenomenon is called _hindsight bias_, the predisposition to assume after the fact that an event was predictable, even when it wasn't.

This way of thinking also means that we may unwittingly accept inferences or hypotheses as foreseeable facts.

The American sociologist Paul Lazarsfeld investigated this tendency toward hindsight bias in an experiment in the 1940s. He told participants that, according to a previous study, people from rural areas were much more adept at handling the challenges and strains of military service than city dwellers were.

All the people Lazarsfeld spoke to found this to be an obvious conclusion. However, the findings of the previous study had actually led to the exact opposite conclusion — Lazarsfeld had deliberately falsely reported the results to demonstrate hindsight bias. Participants considered his findings easy to accept because of their predisposition to see any given outcome as logical or predictable.

What's more, hindsight bias is widely applicable, especially when we consider fame and success.

You might not believe it, but the _Mona Lisa_ was not always as celebrated as it is today. That's hindsight bias playing tricks on you.

In fact, it wasn't until 1911 that the _Mona Lisa_ began achieving the fame it enjoys today. In that year, an Italian, Vincenzo Peruggia, stole it from the Louvre in Paris. When he, and the painting, emerged two years later in Florence, its reputation had soared. Peruggia was lionized for bringing the painting "back" to Italy, and the fame of the painting was assured. Meanwhile, Peruggia had only been trying to get the thing off his hands!

The point is, as humans we tend to explain away history and trends as predictable, especially when hindsight is applied. This is as true for our understanding of fame as it is for our interpretation of any reports and information we come across.

### 3. We abide by the marketplace cliché that “winner takes all;” but what kind of thinking does this represent? 

You've no doubt seen what happens when big retail chains come to town: smaller shops simply can't compete and they soon go out of business.

This is precisely how the globalized market works today, and this trend is summed up by the cliché, "winner takes all."

The economics of distribution and sales means that smaller enterprises like your local shoe retailer or bookstore are less viable nowadays. In the past, it was cheaper to buy locally than to ship in from elsewhere. But now that transport networks have made our markets global, local businesses must compete globally.

Of course, the internet has only exacerbated this. Customers can find anything online and have it shipped to them. As a result, those businesses that are already big prosper, while smaller local businesses find the odds stacked against them.

It's the same story when it comes to the arts. There seems to be no limit to the amount of money that famous artists can rake in — just think of Adele or Taylor Swift.

But when you think about it, is there really such a big difference between the very successful and their nearest rivals who earn much less? It hardly seems like fair competition.

Soloists in classical music or principal dancers at the ballet have always been better paid than the rest of the orchestra or the _corps de ballet_. But this wage gap is getting wider and wider.

Indoctrinated by modern economic thinking, we might assume that the markets are justly rewarding the best entertainers and the most efficient businesses. But, as we'll explore in the next blink, plain good luck likely contributes far more to their success than we might like to think.

### 4. Like it or not, Lady Luck has loaded the dice. 

The chances of stumbling across a $100 bill are tiny. The odds are, in fact, so small, that we tend to attribute such events to luck. But "luck" is really just another way for us to conceptualize how the improbable occurs.

For example, the odds of tossing a coin 20 times and having it land on heads every time are miniscule. But do it enough times with enough people and it will happen eventually.

Although we're aware that such a thing _could_ theoretically happen, we're so unaccustomed to improbable phenomena that we often prefer to think that some external or supernatural force has played a part. In fact, we should be much more willing to accept that luck is simply part of our daily lives.

Nor should we stop there. In truth, luck can be the deciding factor in close contests.

This effect can be seen especially clearly at the Olympic Games, where the very small number of competitors are all extremely talented and exceptionally well trained. When the differences are so slight, luck becomes a key factor.

To push this example further, consider the impact of wind direction in eight world records in track and field. Seven of the eight world record holders, taking into account men and women, in the 100m, the triple jump, the long jump and the 110m hurdles were lucky enough to have had a tail wind behind them when they set these records.

We can't escape it — luck has its part to play when it comes to success and unlikely events. Even when it seems insignificant, it can mean the difference between winning and losing.

Despite this, myths still swirl around the concept of luck. We often have trouble grasping what it's all about and what effect it has, particularly when it comes to getting rich. Why is that? We'll find out in the next blink.

### 5. Taxes may be taxing, but in the long term they make us luckier and richer. 

"There is nothing so certain as death and taxes," goes the old saying. It certainly makes paying taxes sound like a real burden; after all, when you've worked hard for your success, writing a check for the tax collector is the last thing you want to do. The good news? It's not worth worrying about.

The mistake many people, especially the rich, make when thinking about taxes, is overestimating the impact higher taxes will have on their buying power. But economics doesn't work like that. If you are taxed more, so are other people like you, which means your relative buying power stays the same.

What's more, your social standing stays the same as well, at least in terms of what you can afford.

Say you and your neighbors are all considering buying boats. If before a tax hike you can afford to buy the second-nicest boat in the neighborhood, this won't change with the tax. This is because everyone in the neighborhood will need to buy a cheaper boat to compensate for the added expense, so you're relative position stays the same. Everyone is in the same, well, boat.

What's more, taxes aren't just money that disappears into the ether; tax revenue creates favorable conditions in a country. Rich people should be especially grateful for this and should be prepared to share their luck with others.

Why? Because well-spent tax revenues create the preconditions to cultivate luck. If you're born in a wealthy country, you're going to benefit from infrastructure built with tax money. Roads, good schools and robust legal systems are made and maintained with that cash.

Therefore, it's nonsensical for the rich to oppose the higher taxes that helped them succeed in the first place.

This is where luck comes in. If we acknowledge its importance in success, we also need to face the fact that we need to pay enough taxes. This, in turn, will nurture the preconditions to keep us lucky.

### 6. Burning through cash like there's no tomorrow? We all do it and it's not good. 

Humans are pretty suggestible beings, easily influenced by the actions of others. But when it comes to spending, this social awareness can backfire.

Enthusiastic or extravagant spending isn't always constructive; let's illustrate this by considering what we spend on weddings. In 1980, the average American wedding cost $11,000 (inflation accounted for). By 2014, that sum had risen to $30,000. We're happy to spend such crazy amounts because we see other people doing it.

Remarkably, the Social Science Research Network showed in 2014 that spending more on your wedding day actually _decreases_ the chances of you and your partner staying together.

On top of that, you'll be affected by the consequences of reckless spending even if you try to avoid it.

Imagine you decide to live in a less expensive neighborhood. This could have many more repercussions than just putting up with a smaller front lawn. It will impact your children because, like it or not, better schools are concentrated in more expensive neighborhoods, and schools tend to recruit students from their immediate vicinity.

So, if you want your children to get better schooling, you might want to consider moving to that fancy neighborhood. In the end, your hand has been forced by the reckless spending of others.

And there's another side to this coin. If people aren't thrifty but instead decide to blow their cash on luxury items, there will be less money in their pockets at the end of the day. As a consequence, they'll do what they can to pay less taxes.

When there's less tax money in public coffers, public services are adversely affected, and they can deteriorate very quickly. People will then start turning to privatized amenities such as private schools or security services, which further increases inequality.

But that's no reason to despair. You might think that so many push factors will only make you spend more and more — but what if there were special incentives to save? We'll explore this possibility in the next blink.

### 7. The route to success? A progressive consumption tax. 

Whether you're spending or earning, you're faced with paying tax every day. Your paycheck? Income tax. That sandwich? Sales tax.

You might not have thought about it but the tax system can be geared to make your life better. If properly implemented, it can reshape spending patterns. It can also ensure that taxes will be spent on infrastructure and education. In short, all the things that keep society lucky and successful.

The answer lies in something called the _progressive consumption tax_. In tax speak, that means making savings deductible from taxation.

Under this system, a family earning $100,000 a year that saves $20,000 and has deductions of $30,000 would only pay tax on the remaining $50,000. This remainder is called consumption.

We should give up the current progressive income tax model in favor of a much more steeply progressive consumption tax. This means rates would start low but increase quickly if you earn more and spend more. While traditional tax systems tend to discourage savings and investment, this new system stimulates them. You will be taxed less for saving more.

There is a second benefit to this new tax system. Because it's a progressive tax plan, rich people will pay more, and this extra money can be used for public services. This is particularly important now, at a time when acquired spending patterns as well as the financial crisis have made it hard for the government to tax middle-income families.

The progressive consumption tax system would have a series of knock-on effects. It would alter people's spending patterns and money would quickly be invested in infrastructure and education.

In short, by reorganizing the tax system, we would keep ourselves lucky. The richer and more successful would also be helping middle-income families who are under increasing pressure to make ends meet. They, in turn, will prosper. If we acknowledge the role of luck in success, we can nurture it and incorporate into our social system, making everyone lucky.

### 8. Final summary 

The key message in this book:

**Without luck, success as we know it would not exist. Few of the great success stories in the arts, sports or business world would have progressed to such great heights entirely on their own. Luck plays a huge part, especially when the stakes are high. If we cut funding for public services, we will only undermine ourselves and our societies; it's these institutions that keep us lucky and ensure that we succeed as a society.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Our Kids_** **by Robert D. Putnam**

_Our Kids_ (2015) takes a look at the crisis of opportunity in America, where success depends more on wealth than on hard work and ability. The American dream has been betrayed, the U.S. opportunity gap is widening; this book will tell you why, and explain what every U.S. citizen can do to fix the problem, ensure equality and save democracy.
---

### Robert H. Frank

Robert H. Frank is a Professor of Economics and the H. J. Louis Professor of Management at the Johnson Graduate School of Management at Cornell University. He writes a column for the _New York Times_ and is the author of _The Winner-Take-All Society_ and _The Economic Naturalist_.

