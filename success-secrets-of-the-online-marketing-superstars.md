---
id: 5919b19db238e1000736bc0d
slug: success-secrets-of-the-online-marketing-superstars-en
published_date: 2017-05-19T00:00:00.000+00:00
author: Mitch Meyerson
title: Success Secrets of the Online Marketing Superstars
subtitle: None
main_color: 208AA0
text_color: 1B7587
---

# Success Secrets of the Online Marketing Superstars

_None_

**Mitch Meyerson**

_Success Secrets of the Online Marketing Superstars_ (2015) helps you plan a campaign to bring your product to a potentially vast new audience. These blinks illustrate the principles and strategies for building a winning blog, creating great online content or making your videos go viral.

---
### 1. What’s in it for me? Boost your online presence. 

Imagine you want to become a filmmaker and the world's top directors, screenwriters and camerapersons gather to teach you their personal secrets for success. Does this sound like a dream come true?

These blinks are the online marketing equivalent of that fantasy symposium. They'll enable you to launch an effective campaign to promote your product, your writing or even that film. The very best of their field will share their strategies with you step by step, whether you're talking about video marketing, working with social media or creating a thriving blog.

In these blinks, you'll learn

  * how to make it rain;

  * how to outshine Kim Kardashian; and

  * why size really matters when it comes to generating clicks (but not the way you might expect).

### 2. Personal media brands are the future of marketing. 

Do you feel you've got a special connection to the weather? Know how to make it rain when you feel like it? If that sounds a bit ridiculous to you, well, that's because it is! But substitute rain for _media_ and it's a whole different story.

It's time to stop playing the plain old marketer and start making waves as a _media rainmaker._ A media rainmaker brings in the business by moving beyond traditional marketing methods toward innovative, breakthrough strategies. Marketing and media are two different things, after all.

Marketing is what we all run away from, wary that someone out there is trying to sell us their product. Media, on the other hand, draws us in. People get to know your business through media because they are _genuinely interested_. But how do you get customers to flock to your business of their own accord?

By building a _personal media brand._ This approach will give you the edge over your competitors. Take Kim Kardashian, for example. Why exactly is she famous again? Most of us barely know anymore. But what we do know is that she was able to build a personal empire through her brilliant use of media.

But what if you're not famous? Well, you can still build a winning personal media brand. You must show your audience that you're the expert on a given topic. People will turn to you as their most valuable source of information and content.

For a personal media brand that's a little different than Kim Kardashian's, let's look at the Digital Photography School _._ It's a well-known online educational platform for any aspiring photographer looking for loads of top quality information for free.

But wait — shouldn't it be pretty unprofitable for founder Darren Rowse to give all his advice away? Not so. It's precisely because of the great content provided by Rowse that his readers are willing to buy his e-books, which is how he generates his profits.

Between Digital Photography School and his other popular site, ProBlogger Blog Tips, Darren Rowse has established himself as a go-to source for online advice _and_ his very own personal media brand.

> _"Wouldn't demonstrating that you know what you're doing work better than claiming to be the number one-whatever?"_

### 3. Social media is your greatest tool. 

Everyone's saying it. And guess what? It's true. Social media is king! Nearly everyone has an account somewhere, whether it's on Facebook, Twitter, Instagram or Pinterest. Statistics show that 73 percent of _all_ internet users also use social media. That's a lot of prospective customers! So how can you reach out to them?

By arming yourself with a _social media strategy._ This is an outline of why and how you'll use social media to achieve your business objectives. This might sound simple, but it's a crucial step. Putting content onto the internet without a social media strategy is like getting into a car without deciding on a destination. You won't know if you've arrived if you had no idea where you wanted to go in the first place!

So, social media strategies are important. But how do you build one? It's not too complicated. It comes down to selecting your sites, turning them into a platform and creating a plan for content _generation_ and content _promotion._

Consider the demographic of your target audience when selecting your social media site. If your customer is likely to be a female between the ages of 30 and 45, where would she be online and looking for inspiration? Probably on Pinterest and Facebook. But if it's a younger millennial you're after, then Instagram and Tumblr would be your best bets.

While each of the sites you select should serve their own purpose, it's important to ensure that they are all cohesive and aligned toward the common goals set out in your social media strategy. This is something to keep in mind as you develop your _content generation plan_, where you decide what kind of content you'll create, and how often.

Alongside this, you'll need to create a _content promotion plan_ too, considering how you can get your content to more viewers. For example, would you like to pay for ads that link to your content, or try and get your content shared in other ways? Either way, you'll need to ensure that your customers and readers find you. More on this in the next blink!

### 4. Microcontent makes your readers hungry for more. 

These days, the internet is huge _._ With so many companies out there competing for the customer's attention, how can you, a mere blogger, make your mark? _Short-form microcontent_ is the answer.

Short-form microcontent may take the form of photos, memes, GIFS or text images, which are pictures with short pieces of writing like titles or quotations superimposed on top. These can be deployed on social media networks to catch the attention of your readers and encourage them to seek out your long-form content too. You could keep it as simple as posting an image on Facebook with a link to your blog.

Images play a key role in driving traffic to your site. It's proven that articles with images get as many as 94 percent more views than those without. Why? Well, it's often because people view these articles as attractive enough to share them with their friends and followers.

Of course, you can't just start sharing images left, right and center. As we learned in the last blink, you'll need to know your audience in order to reach them in the right places. Google+, Facebook Business Page, Pinterest, Instagram and Twitter are typical platforms used for sharing short-form microcontent. But it may be that your audience is found elsewhere, on certain forums or smaller social networks. Check out who is using what, work out what they want and adapt your microcontent to suit.

### 5. Go viral with strategic video marketing. 

We go nuts for video! YouTube is the second-most popular search engine; using video will increase your chance of featuring on page one of a Google search by 53 times _and_ it's predicted that video will account for 90 percent of web traffic in 2017. It's clear that video is a brilliant tool for your company's future.

But beware — not just any video will do. Before you start planning your viral videos, build a _video marketing strategy_. This outlines your goals, your viewers, and how you'll get your videos to them. With a clear and calculated strategy, you won't need expensive cameras or stunning lighting.

Take wellness coach Tania, for example. Her business is selling workshops online. Knowing that her audience cares more about quality content than professional studio production, Tania makes videos on her computer to post to YouTube and her blog. She has successfully boosted her visibility and following this way.

Once you've got your _video marketing strategy_ in place, how do you go about making the videos themselves? By applying the four Ps of video marketing: _Purpose_, _Premise_, _Platform_ and _Promotion._

_Purpose_, of course, refers to your goals. Whether you want to boost visibility of your product or direct traffic toward your long-form content, ensure your purpose is specifically defined!

Next, work out your _premise_. How are you planning to reach your viewers? How do you want them to respond to your videos, and what devices can you use to trigger that sort of response? Equally important to define is your _platform_. This entails the style of your video, be it natural, professional, edgy, with an on-camera or off-camera feel. Finally, you'll need to consider where and how you'll _promote_ your videos.

A thoughtful video marketing strategy will allow you to produce videos that attract the right viewers, stay in their minds and help you get your business out there.

### 6. Your blog benefits from a great strategy too. 

So, you want a brilliant blog. One where new posts are eagerly awaited, and not just by members of your immediate family. If you want a strong following and an ever-growing blog, there are a few tips that'll help you get there.

The first step is _research._ The internet, as you well know, is already saturated with blogs. If you want yours to stand out and be worth reading, find ways to set yourself apart from competitors. As they say, keep your friends close, and your enemies closer! Get to know your competitors. What do they write about? Do they do it well? What is their content missing? Give your customers what your competitors don't provide and you'll have the edge.

Next, ensure you have solid foundations for your blog. This means a strong platform and content strategy. A reliable and safe platform such as Wordpress, which already has a large community built around it, provides a great foundation for your blog. Your content strategy is the backbone of your blog, and answers those key questions of what, when, and for whom you're producing content.

Finally, remember to invest time in your network. Strong bonds with influential people can help your blog win more valuable followers. If you haven't really got a professional network to begin with, don't fret! You can start building one anytime, by finding people who might want your help. If they appreciate your expertise, they might offer to help you in return.

One great way to create ties with influential figures in your industry is by offering them an interview for your blog. Most influencers love to give interviews, which gives you a great opportunity to get to know them.

### 7. Final Summary 

The key message in this book:

**With a clever, calculated strategy, anyone can create a successful online marketing campaign. Microcontent and video marketing are essential tools for building a follower base that will turn you and your business into a valued resource that customers trust.**

Actionable advice:

**Try PicMonkey or Canva when creating your microcontent.**

Want your microcontent to look professional without having to spend hours on Photoshop? Programs such as PicMonkey and Canva are here to help. They'll help you produce brilliant graphics with highly customizable templates for free.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The New Rules of Marketing & PR _****by David Meerman Scott**

_The New Rules of Marketing & PR _(2007) is your guide to the world of online marketing strategies. These blinks explain how and why communicating with authenticity and agility on social media will help you reach more customers and take your brand to the next level.
---

### Mitch Meyerson

Mitch Meyerson is a consultant, public speaker, songwriter and the author of 11 books on business and personal development. He's asked highly proficient practitioners to share their best tips in _Success Secrets of the Online Marketing Superstars_.

