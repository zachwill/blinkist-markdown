---
id: 56cda6e774766a000700002e
slug: success-through-a-positive-mental-attitude-en
published_date: 2016-02-24T00:00:00.000+00:00
author: Napoleon Hill
title: Success Through a Positive Mental Attitude
subtitle: Discover the Secret of Making Your Dreams Come True
main_color: 9C1F29
text_color: 9C1F29
---

# Success Through a Positive Mental Attitude

_Discover the Secret of Making Your Dreams Come True_

**Napoleon Hill**

_Success Through a Positive Mental Attitude_ (1960) shows how to achieve the life of your dreams by developing a positive mental attitude. Near the turn of the twentieth century, at the behest of Andrew Carnegie — one of the wealthiest men of his time — Hill interviewed hundreds of famous and successful people in an attempt to uncover the secret to success. This book is one of the outcomes of his findings.

---
### 1. What’s in it for me? Classic knowledge about achieving all you want. 

Go to your nearest bookstore and check out the self-help aisle. Chances are, you'll find row upon row of titles. The self-help and self-improvement market is truly massive — because people really want to know what will make them better, more successful people.

And yet, of the thousands of titles, only a few have become classics. _Success Through A Positive Mental Attitude_ is one of them. 

Though hatched way back in the 1950s, the book's ideas are still relevant today. They show you how anyone can become a success, just by adopting a positive way of thinking. These blinks highlight some of those ideas. Read on and start your journey toward a new, successful life.

Also find out

  * how a positive mental attitude allowed Henry Ford to change the world;

  * how a swamp became one of America's greatest entertainment spots; and

  * why the Wright brothers succeeded where all others failed.

### 2. Reach your goals by developing a positive mental attitude. 

To many of us, success is a mysterious thing. Is it the result of favorable conditions over which we have no real control? Does it come down to knowing the right people, or having the right timing? No, no and no. In fact, success is about having a _positive mental attitude_ (PMA). 

The level of your success directly corresponds to whether you nurture your PMA — or its rival, your _negative mental attitude_ (NMA).

People with PMA are success magnets. They approach life with positivity, and they're unfazed by failures or mistakes; in fact, they see missteps as opportunities. To someone with a PMA, each failure is a step that brings you closer to a solution.

Because they're unafraid of failure, such people are unafraid of trying, even if success seems unlikely. They're eager to try, and hungry for self-improvement. Those with a PMA achieve greatness because they burn to accomplish new goals and ascend to new heights.

Take Henry Ford, a prime example a man with a PMA. He wanted to develop a cheap V8 engine suitable for mass production. Hitherto, eight cylinder engines were only used in luxury cars, and Ford's engineers criticized his dream, claiming it was impossible. Nevertheless, he continued to fight for his idea, and after a year, he achieved the unachievable: the mass production of the V8 prototype. 

The V8 was a massive hit — the innovation that established Ford's brand, making him one of the most successful businessmen in history. And all thanks to his PMA. 

The great news is, it's never too late to improve your mental outlook. Anyone who starts working at the age of 25 will work roughly 100,000 hours before retiring at around the age 67. This means you have plenty of time to start cultivating your PMA.

> _"The greatest secret of success is: there's no secret."_

### 3. A PMA allows you to see problems as opportunities for success. 

Success is all about confronting and solving problems. Just think of history's greatest minds and business moguls: behind every great invention or innovation was a problem that someone faced and tried to solve.

It's precisely our mental attitude that dictates how we handle problems. When you encounter a challenge, do you resign yourself to failure and throw your hands up, or do you find the positive in the problem and take it upon yourself to fix it?

Obviously, those who succeed adopt a PMA and therefore take the more productive approach.

Conversely, those with a NMA will fall short of their goals, as they admit defeat at the first sign of trouble — when their prototype fails, for example, or when a potential investor turns them down.

People who care about developing their PMA see setbacks as a challenge to be taken. So, instead of giving up, they try harder. This reaction is known as _inspirational dissatisfaction,_ and those who have it use it to rise above even the most desperate and hopeless situations.

Even serving time can be a positive opportunity, as a man named Charlie Ward taught us. During his time in prison, Ward was able to change his NMA to a PMA. This shift won him the respect of both inmates and prison guards, and he moved up in the prison hierarchy. During this time, he met the industrialist Herbert Bigelow, who was serving time for tax fraud. Bigelow was so impressed by Ward that he promised him a decent job after he was released. Sure enough, when Ward was released, he got that job. Furthermore, after Bigelow died, Ward became CEO of the company and died a millionaire.

So literally any problem, whether professional or personal, can be conquered with a PMA.

### 4. You can only be successful if you avoid mental near- and farsightedness. 

Maybe you've learned to live with some sort of visual impairment. But what if you suffer from _mental_ near- or farsightedness? Such dysfunctional mentality will bar your way to success.

So what _is_ mental nearsightedness? It means you're unable to see the possibilities lying in your future. Perhaps you fail to understand the importance of planning and having goals to strive for, or you waste all your energy tackling problems that are right in front of you, and are blind to the bigger picture.

But being able to plan for the future is one of our biggest evolutionary advantages.

Take Dick Pope, a man who wasn't blinded by mental nearsightedness. Pope saw an opportunity to build a tourist attraction on the farmland surrounding the town of Winter Haven, Florida, where others saw nothing but desolate swampland. So he bought an old cypress swamp and turned it into the now famous Cypress Gardens. He also offered photography equipment and free advice on taking pictures, giving tourists the joy of going home with wonderful pictures to show their friends, thereby spreading word of the gardens.

And it's not just nearsightedness that hurts your success: mentally farsighted people have problems, too.

They want to leap immediately to the top. They dream of the future but forget to work on the things they need to get there. Consequently, they fail to reach their goals and don't take the opportunities that are right in front of them every day.

And such everyday opportunities are often the most lucrative. Consider the paper clip or the Post-It note. Both are simple designs to solve a tiny, yet widespread problem. People who recognize such everyday problems can generate millions. Mentally farsighted people never grasp this.

So, if you want to be successful, you must notice the opportunities in front of you, but also plan out how you want to make your mark on the world.

### 5. Along with a PMA, you also need determination. 

A PMA gives you a solid grounding for success but it must be accompanied by determination and motivation. Rather than blaming all your difficulties on the world, you should set about changing it into a place you want to live in. When you feel determined to make a difference, your motivation will soar, which will allow you to reach new heights in your life.

Someone who embodied passionate determination and motivation is farmer Milo Jones. Due to a severe illness, Jones became paralyzed. Although his body no longer served him, his head was working overtime and he began figuring out how to accomplish certain tasks in the most effective way. With this motivation, determination and PMA, he managed to convert his little corn farm into an empire that sold Jones' Little Pork Sausages, which made him a millionaire.

But keep in mind that along with your determination, you'll also need goals.

It's best to write your goals down and attach a deadline to them. For example, noting what you want to achieve by the end of the year. The higher you set your goals, the more you'll fight for them and the more you'll achieve. 

If you have a particularly challenging goal, you should break it down into smaller goals that you can achieve in the next week or month to build up to the end goal.

So, for example, if your dream is to learn Spanish, a lofty goal would be speaking it perfectly, which may take years. But when divided into smaller goals, like ordering your first beer successfully or reading a novel in Spanish, these smaller triumphs will help you stay focused and motivated.

### 6. Anchor your goals to your subconscious by using autosuggestion. 

Our actions are the result of our conscious decisions, right? Well, not quite. Our _sub_ conscious mind wields a powerful influence over us, too, affecting our dreams, desires and needs, as well as driving our behavior.

Thankfully, we can harness this subconscious power and use it to our advantage using _autosuggestion_. With autosuggestion, we can anchor goals and wishes to our subconscious. This, in turn, will influence our thoughts and, thus, our life.

Whether it's about health, finances or relationships, autosuggestion can help us reach any of our goals. So how do you do this? You can start by reading a sentence aloud twice a day, for instance about how much money you wish to earn this year. Your subconscious will remember this and influence your conscious mind to seek out things that will get you there, such as new work opportunities.

The same method applies to adapting your mental attitude. If you tell yourself that you'll feel happier every day, this feeling will gain momentum and, after a while, your mental attitude will show a vast improvement, and you'll feel happier.

One man named Bill McCall was a master of autosuggestion. McCall lived in Sydney, he was 21, his business had gone under and he had failed at running for Federal Congress. But instead of admitting defeat, he resolved to use the power of autosuggestion: he wrote down that, by 1960, he wanted to be a millionaire. After some years of repeating this goal to himself, McCall became not only the youngest man in history to become a member of Parliament, but also the chairman of 22 family-owned corporations — oh, and he also became a millionaire.

Once you're able to internalize your positive mental attitude in this way and can use autosuggestion effectively, you'll be better equipped to overcome any obstacle with determination and optimism.

### 7. Change your behavior by changing your habits. 

We've seen how we can have more control over our mind, but what about our behavior? Good actions are what make us good people — and improving the way we act is merely a matter of the right training.

We know that our feelings influence our actions. But the converse is also true: our actions influence our feelings.

So by modifying our behavior, we can influence and change our feelings. Just consider Founding Father Benjamin Franklin. Franklin worked hard at bettering himself and his behavior. He would will himself to alter his behavior for a period of time — practicing abstinence or frugality or moderation until he'd mastered that particular virtue. Once he felt competent in one virtue, he'd set about mastering the next.

We should all take a leaf out of Franklin's book. For example, if you wish to have a more moderate life, practice moderation for a few weeks. Don't overindulge in fatty foods, and don't get blind drunk if you already feel good after one glass of wine.

By changing our behavior in this way, we change ourselves, too. And the more we work on these virtues, the more they become part of us.

You can even train your preferences. Let's say you want to stop eating chocolate because you know it's unhealthy. You could start by not binge-eating two bars every time you sit on the couch to watch the news after work. This simple change will have you feeling healthier and help you pay more attention to your bodily needs. Eventually, you'll actually prefer _not_ eating that chocolate.

You can also train yourself out of procrastination by heading straight to work instead of fussing around for hours over needless activities. Just a few weeks of doing this will start to make this new behavior part of your personality and prevent your bad habits from sabotaging your life.

### 8. Never give up and always look out for any missing pieces that can help you on your path. 

Oftentimes, our plans don't quite come to fruition because one last piece of the puzzle was missing — the crowning glory that would've made your project a huge success. People with a PMA know that they always need to be on the lookout for this last piece.

Everyone who can see their final goal on the horizon should stop and ask themselves what this last piece might be. If you let a negative attitude creep into your thinking, failure is a sure thing. But if you failed, think about it and be frank with yourself — what more could you have done? And what is that special something that could take your project even further?

The Wright brothers were great at doing precisely this. They succeeded where others had failed because they possessed strong PMAs. Instead of being disheartened when their airplane designs failed, they spotted that missing piece: movable flaps mounted on the plane's wings! These flaps enabled the pilot to place the wings in various positions and keep the plane steady. This piece would have been forever lost had the brothers had an NMA, and their dreams would have never seen the light of day.

When looking for the missing piece, an unconventional approach is sometimes the best approach. When trying to brainstorm new ideas, many of us sit down with a pen and paper and expect ideas to magically descend upon us. Then, when we become distracted, our focus slips and we lose time and potentially great ideas. So why not try an unconventional route and focus your mind completely?

For example, American inventor Dr. Gates used to lock himself in a small, dark, soundproof room so that he could concentrate on a solution to a problem. Only when he had an idea did he switch on the light and jot down a possible solution.

> _"When you seek success with PMA, you keep trying."_

### 9. Motivate others by motivating yourself. 

When it comes to motivation, we all play two roles — we're motivated by others and, in turn, give others motivation. First, though, you need to be able to motivate yourself.

Motivation is what governs your choices; without it you won't make any progress toward success. Therefore, it's paramount to your journey.

To ignite your motivation, list all the things you want to achieve as well as by when you want to achieve them, and then go out and change your habits. There are many ways to get motivated, but the key is simply to get moving. Don't sit back down after reading this and tell yourself you'll get around to it later. Get up! And while you're at it, why not try helping someone else, too?

For a wonderful example of the interplay and exchange of motivation, let's look to parent-child relationships. As babies, children motivate their parents to take care of them and parents nurture their children by caring for and supporting them, which fosters trust.

Thomas Edison, for example, was always rallied by the trust his mother instilled in him, even when he was a misbehaving, insubordinate student! She stood by her son, even when he was allegedly failing.

In order to effectively motivate someone with trust, whether child or adult, you must give her _active faith._ That is, actually voice to the other person that you believe in them and that they have your trust and support. Simply maintaining passive trust isn't motivating, because the person won't be able to detect it! So, if you want an employee to know that you're assigning them an important task, speak to them. Say that the fate of the project is in their hands because they can be trusted with it.

Finally, as a bonus to motivating others, the more of your PMA you can offer to other people, the more others will reciprocate.

### 10. Final summary 

The key message in this book:

**A positive mental attitude, coupled with the right habits and the right approach to your goals, will help you achieve anything you set out to do. And that's really all there is to success!**

Actionable advice:

**Trade in your NMA for a PMA.**

What are you unhappy about in your life? If you think a negative mental attitude is the reason behind your dissatisfaction, why not make some small, positive changes to your thinking? You could start small, adjusting your thinking in one area of life, such as work, and build up from there!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Think and Grow Rich_** **by Napoleon Hill**

As a young man, Napoleon Hill was asked by industry magnate Andrew Carnegie to investigate the methods of the 500 most successful people of his time, including the world's richest men, top politicians, famous inventors, writers and captains of industry. To find these blinks, press "I'm done" at the bottom of the screen.
---

### Napoleon Hill

Napoleon Hill (1883-1970) was an American author. In 1937, he published the landmark self-help book _Think and Grow Rich_ (also available in blinks) _,_ which has sold over 60 million copies.

