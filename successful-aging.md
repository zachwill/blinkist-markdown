---
id: 5e5547af6cee0700075dcb5a
slug: successful-aging-en
published_date: 2020-02-27T00:00:00.000+00:00
author: Daniel J. Levitin
title: Successful Aging
subtitle: A Neuroscientist Explores the Power and Potential of Our Lives.
main_color: None
text_color: None
---

# Successful Aging

_A Neuroscientist Explores the Power and Potential of Our Lives._

**Daniel J. Levitin**

_Successful Aging_ (2020) turns the idea that old age is a time of inevitable decline and discomfort on its head. Daniel J. Levitin gives us insight into the neuroscience of aging and, along the way, a bunch of tips about how we can not only cope with aging, but actually appreciate it as a unique life phase.

---
### 1. What’s in it for me? Learn how to live longer, happier and healthier. 

Try as we might, none of us can escape growing old. Cognitive and physical abilities shift, and old age brings new obstacles. We might not be able to do our favorite hike anymore, or completing the crossword might become too taxing.

Although there are undeniably challenges in aging, it's also a time in which neurological shifts usher in some positive changes. We are more likely to feel at peace, to be able to regulate our emotions, and focus on the important things. Even more importantly, there are a number of factors within our control that can have a huge impact on whether we experience aging as a time of decline, or a time to flourish in new and different ways.

These blinks explain how your brain changing as you age offers new opportunities, how many ideas about aging are actually myths, and how you can apply a simple, five-part principle to make your life longer, happier, and healthier.

In these blinks, you'll find out

  * which personality traits are linked to longevity;

  * how cuddling your children extends their predicted life spans; and

  * why the Dalai Lama sleeps nine hours every night.

### 2. Although we often think of old age as a time of mental decline, it also brings improvements in brain function. 

When you hear the words "growing old," what do you think of? Maybe you picture a nursing home, or endless doctor's appointments. Most likely, what you are picturing has negative associations. But while there are challenges that accompany aging, there are also a lot of positive aspects to growing older.

There's no denying that, as we age, aspects of our mental capabilities decline. Due to plaque build up in the brain and the reduction of neurochemicals and dopamine, we experience slowing of cognitive function. That's why it can take us longer to recall a name, or why we might leave our keys in the refrigerator.

But it turns out that other neurological shifts in the brain open the door for new, positive things as well. For instance, as we age, a chemical change takes place that literally makes it easier for us to accept death. In fact, due to deactivation of our _amygdala_ — an area of the brain responsible for memory, decision-making, and emotional responses — we experience less fear in general, and are more emotionally balanced. Research also shows increased tendencies toward understanding, forgiveness, tolerance, and compassion.

Another strength that comes with aging has to do with two important categories of intelligence: _practical intelligence_ and _perceptual completion_. Studies show that people over the age of 50 score the highest in both of these categories.

When asked questions like, "Imagine you are stranded on the side of an interstate in a blizzard. What would you do?," studies show that the older you are, the better you will be at coming up with solutions. This is an example of practical intelligence, and it is informed by a lifetime of experience.

Similarly, perceptual completion is a skill that is critical for survival, and that only strengthens as we age. Because of the way perception works, we often have blind spots that our brain needs to fill in, or "complete." Imagine that you're driving into a gated community and you pass a sign. Because of your position and speed, you see _lcck the gate_. Your brain automatically turns _lcck_ into _lock_, based on the context. Our brains are constantly making these leaps, and the brains of older people statistically score higher at filling in these details. 

Every stage of life has pros and cons. Aging successfully means accepting the limitations and challenges that come, but also enjoying the positive parts.

### 3. Aging successfully depends on debunking the many myths about growing old. 

In the previous blink, we began to explore how the aging brain doesn't simply decline, but actually enjoys some neurochemical benefits. Now we'll look at two other myths about aging that need to be debunked. 

The first myth is about memory loss. How often do you hear old age characterized as a time of forgetfulness? In reality, memory function is way more complex than people tend to think. For one thing, research shows that people of all ages experience short-term memory lapses. It's just that when we're young, we tend to shrug it off, saying that we're tired or overworked, or maybe even that we're just having an off day. Conversely, when older people experience short-term memory loss, they have been primed to believe it's the result of cognitive decline.

And there is even an aspect of memory in which older brains perform better. This has to do with our ability to make judgments and decisions based on pattern recognition. When faced with complex situations, older brains are better able to recall prior experiences and the neural connections their brains laid down in those experiences.

This improved memory function makes older brains better at zooming out and taking a bird's-eye view of things. Consequently, older people have an advantage when it comes to grappling with decision-making, especially about tricky human questions like whether to stay in a relationship or not, or whether confronting your boss is the ethical thing to do. If you have a question that requires objectivity and wisdom, ask older adults for advice — their brains are literally wired better for it!

Another myth is that older people are past their prime — that, if they start a new hobby or skill, they won't be able to achieve as much as when they were younger. This is simply not true. Take Anna Mary Robertson, whose paintings are on display at the Smithsonian and New York's Metropolita n Museum of Art. Robertson didn't start painting until she was 75! 

Another great example is the founder of KFC, Harland Sanders. Drifting between jobs his whole life, he ended up founding KFC at the age of 62. Fourteen years later, he sold it for the equivalent of $32 million dollars. 

Now that we've done away with some of the negative misconceptions about aging, let's explore the factors that do impact how we age.

### 4. When you give your child an extra cuddle, you’re extending their longevity. 

Everyone ages differently. How we age is impacted by our genetics, our opportunities, and our environment. However, one of the biggest factors that impacts how we experience our later years is our childhood. 

This brings us to our next question: What do cuddles have to do with life span?

A lot, as it turns out. In one experiment, rat pups who were licked more by their mothers in the first six days of life grew into adult rats that were far more secure. The study showed that these rats produced far fewer stress hormones, and that this trend held well into adulthood.

Being nurtured impacts us on a chemical level, affecting the _glucocorticoid receptors_ in the _hippocampus_ — primary components of the stress response and immune system. What this means is that when you aren't given enough nurturing, your immune system is compromised long-term. Conversely, a lot of love and attention, especially in the early years of childhood, will reduce your stress hormones and strengthen your immune system throughout your life.

A famous experiment carried out in the 1960s illustrates just how deeply ingrained our desire for contact and comfort is. Harry Harlow, an American psychologist, conducted a series of controversial experiments on baby rhesus monkeys. He isolated each infant in a cage with two wire monkeys representing the baby's absent mother. One of the wire mothers was wrapped in terry cloth while the other had a bottle of milk.

Harlow wanted to test whether the infants would choose the nurturing comfort of the cloth, or be overpowered by their drive for survival, and choose the wire mother with the milk. The experiment showed that they overwhelmingly chose the terry-cloth mother.

It's clear that we crave and benefit from being adequately nurtured. And while we can't change how we were raised, we can intentionally address the impact of our childhood experiences on our brains, for example through therapy. And the reason this is possible is something called _neuroplasticity_, which we'll explore in the next blink.

### 5. It’s possible to change our brains in ways that increase healthy longevity. 

In the early twentieth century, thinkers like Sigmund Freud and William James argued that our personality was set in stone at a young age. Whether that's true or not is crucial, as research shows that the single most important determinant of lifetime happiness is your personality. So, on a neurochemical level, are we stuck with who we are, or is change possible?

First off, it's important to note that our personality isn't just constructed out of our neurochemistry, but also by our environment and our opportunities. Nevertheless, the way our brains operate on a chemical level does have a huge impact on how we interpret, react, and engage with the world around us, all of which plays a big role in determining our personalities.

Let's take an extreme example. Sarah is quiet, calm, and conscientious by nature. In contrast, Derrick is extroverted and prone to taking big risks, regardless of the potential danger to himself or others. These personality traits have a big impact on both of their predicted life spans. More importantly, it impacts their predicted _health span_, which is the time of their lives in which they'll enjoy relatively good physical and mental function.

Based on Derrick's personality traits, he's statistically more likely to suffer from disease, lung, or liver damage. In fact, studies show that lower childhood conscientiousness is correlated with obesity, physiological dysregulation, and worse lipid profiles in adulthood. Conversely, Sarah, who scores high in conscientiousness, is likely to enjoy an extended health span.

Maybe you started off with personality traits that correlate to an increased health span. But if you didn't, the good news is that it's possible to improve these traits. In fact, the work of Nancy Bayley and Paul Baltes in the 1970s proved that no single period of life defines our personalities. Further studies in North America, Europe, and Asia have gone so far as to show that personality change is possible up through our eighties.

This is because of one of the brain's most remarkable features: _neuroplasticity_. This allows us to lay down new neural connections, produce new cells, and redirect the use of cells toward different ends. For instance, someone who is blinded in an accident is able to redirect the function of the cells in the part of the brain that used to process vision to promote other ways of navigating the world, like an increased sensitivity to sound and touch.

So, if we know that it's possible to change our brains, what should we do in order to extend our health span? This is where the COACH principle comes in.

### 6. To age successfully, use the five-part COACH principle: curiosity, openness, associations, conscientiousness, and healthy practices. 

Calm and quiet Sarah will likely live longer than risk-loving Derrick because she is _conscientious_. But this is just one of what the author has coined as the five crucial aspects to healthy aging. These five aspects form the COACH principle: curiosity, openness, associations, conscientiousness, and healthy practices. Let's dive into three more of them.

Imagine two people get admitted to a nursing home on the same day. Luca recently fell and now has to walk with a cane, which he hates. After a lifetime of achievements in the business world and prowess on the tennis court, Luca is ashamed of his diminished abilities. As a result, he spends most of his time alone, and doesn't take the risk of trying anything new.

In contrast, Fatima enters the nursing home eager to connect with the other residents. She attends the optional craft and exercise classes, and joins a book club, even though she's never read much literature before.

Studies show that because Fatima scores high in openness, curiosity, and associations — another word for social interaction — her risk of heart disease, Alzheimer's, and diabetes drops. In fact, people like Fatima, who are curious and open to new experiences, statistically perform better in essentially all life outcomes.

In contrast, people like Luca, who are more concerned about getting recognition for their achievements or hiding their failures, are far less likely to seek out new learning experiences. As we age, seeking out new learning experiences becomes more and more important. By refusing to stretch and expand his mind, Luca increases his risk of cognitive and physical decline.

One way to think about the difference between Luca and Fatima is through a classification system created by Carol Dweck. She claims that all people can be categorized as having a _fixed_ mindset or a _growth_ mindset. Her research shows that people with a growth mindset consistently outperform those with a fixed mindset. This finding is backed up by other research showing that CQ — or curiosity quotient — is just as good, if not a better predictor of life success, as IQ.

Fatima's chances of living a longer, healthier life have a lot to do with her growth mindset. By trying new things, remaining curious, and connecting with others, Fatima is aging successfully!

> _"The one constant of happy people seems to be that they don't think about happiness — they're too busy doing things and being happy to stop and think about it."_

### 7. If you want to eat well as you age, stop listening to all the diet fads. 

Now we come to the last of the five COACH aspects: healthy practices. And healthy practices start with healthy eating.

Depending on when and where you grew up, you have probably been exposed to a number of different diets that celebrities, friends, or coworkers take on and swear by. The problem, as Stanford nutrition scientist Christopher Gardner puts it, is that no matter how crazy or unfounded a diet is, if you get enough people to try it, it will work for someone. Diets that have absolutely no scientific backing, from a tapeworm diet to a placenta diet, get promoted by a small number of influential voices who experienced success.

Unfortunately, that success rarely carries over to many of the people who try it, and can sometimes even damage their health. For instance, while boosting your antioxidant and vitamin C intake is currently in fashion, some studies show that it can actually block some of the health-promoting benefits from exercise.

That said, going on a diet most of the time produces some positive results. So why is that? In simple terms, dieting works because it increases your awareness of what you are eating, and prevents you from eating too much.

Overeating is linked to all sorts of health problems, especially as we age. In fact, one method that has been scientifically proven to extend longevity is caloric restriction. We don't yet know whether it is best to limit calories daily, practice intermittent fasting in half-day cycles, for a full day once a week, or for two weeks out of the year, but we do know that some sort of caloric restriction does impact longevity.

There are a few other healthy practices around eating that science backs up again and again, especially as we age. Eat fatty fish and B12 to promote your neural health. Three tablespoons of virgin olive oil a day has been shown to relieve oxidative stress on cells and regulate cholesterol. Increased consumption of cruciferous vegetables, such as kale and bok choy, helps protect you against cancer. Make sure you're getting enough protein for bone health and stay hydrated, as your thirst detectors break down with age. Eat when you're hungry, stop when you're full, and enjoy an occasional treat.

It doesn't sound exciting, but the best guide for your diet is your common sense.

### 8. Physical movement has the biggest impact on healthy longevity. 

Imagine you're walking on an unfamiliar dirt path in nature. There are branches and rocks, and diverse scenery all around you. Every step you take, your brain and body need to work together to make hundreds of micro adjustments for foot pressure, angle, and pace. To do this, your hippocampus is activated, and your brain starts firing in the exact way it has developed to be used. By taking a walk, you're actually doing the single best thing for your mental and bodily health!

As we age, it's common to let go of physical exercise. We're no longer able to run that 10K charity race or play pick-up soccer. As our dexterity declines, so too does our motivation. After all, there's nothing glamorous about taking a walk around the block or spending ten minutes on a stationary bike. Why bother?

Well, research shows that of all the healthy practices we should prioritize as we age, physical exercise, even in very small doses, has the biggest impact. When it comes to choosing what kind of exercise to engage in, it turns out that simply walking, especially outdoors and in new environments, is the most beneficial.

For instance, one study took two groups of elderly people and had them walk in the same area for a set amount of time. One group followed a rectangular path, while the other group was encouraged to wander off the path and explore the space.

Afterward, the groups were tested on a creative cognitive activity. They were asked to list as many alternative uses for chopsticks as they could think of — for instance, drumsticks, a hairpin, or a flagpole. The group that had gone off the path far outperformed the group that stayed on it. That's because walking in new environments promotes creativity and stimulates the brain.

In fact, so many studies show evidence for the physical and mental benefits of walking that some doctors are beginning to prescribe it! Scottish doctors have begun prescribing "rambling and bird-watching" to their elderly patients, and doctors in Quebec are prescribing free visits to the Montreal Museum of Fine Arts as a way to improve the physical and mental health of their patients.

### 9. If you want to make the most of your life, sleep more! 

In a time when productivity is valued aboveover all else, it's common for people to brag about how little sleep they're getting. But the reality is that sleep is one of the most important factors in maintaining a long health span. You may feel like you're less productive when you need to give up that event or that extra hour at work. But, in the long term, you're adding time onto your life!

Unfortunately, there's a myth that as we age, we need less sleep. In reality, it isn't that older people need less sleep, but that they have a harder time getting it. That's because as we age, the signaling from a part of the brain called the _suprachiasmatic nucleus_ — or "SCN" — degrades. Think of the SCN as your master clock. It regulates your circadian rhythm, which is essentially how your brain operates in a twenty-four-hour cycle, signaling things like when to get sleepy or when to wake up.

What this means is that, as we age, it gets harder to maintain a regular sleep cycle, and that leads to negative physical and mental consequences. Studies show that 99 percent of people need at least seven hours of sleep every night. Sleep-deprived people show 60 percent greater activation in their amygdala, which leads to fear, anxiety, and stress. Sleep deprivation is also correlated with hypertension, Alzheimer's, and diabetes.

So how can you make sure you get enough sleep? One way is to reframe the way you think about sleep. Instead of thinking of it as wasted time, think of it as time spent doing really important work. Sleep is restorative. While you're asleep, cellular repair and cleaning mechanisms kick in. It's when wounds are healed, and bacterial and viral infections are fought off. It's also when memories are consolidated, and emotions and problems are processed. If you're learning a new motor skill, that learning is encoded in your memory at night.

Other habits you can adopt to promote the right amount of sleep include avoiding screens for two hours before bed, sleeping in a completely dark space, and, most importantly, going to bed and waking up at the same time every day.

Sleep is important. After all, when the Dalai Lama was asked what the most important contributor to his healthy and happy existence was, he answered simply, "Nine hours of sleep, every night."

### 10. Final summary 

The key message in these blinks:

**When it comes to aging, neuroscience shows us that a handful of manageable habits can radically improve and extend our lifetimes. If you sit around feeling isolated and useless, your physical and mental health will plummet. But if, in contrast, you push yourself to remain active, to maintain and build new relationships, and try new things, all while eating well and getting enough sleep, you may actually enjoy aging as a time of increased stability, peacefulness, and enjoyment. Aging comes with challenges, but it can be done successfully!**

Actionable advice: 

**Pass your skills and knowledge to the world.**

According to many of the oldest and most successful people alive, the key to aging is to resist slowing down and stay engaged. So find a way to pass on your skills and knowledge to the world. Volunteer, join a club, or mentor a younger person, and you will see your health and happiness increase!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _The Organized Mind_** **, by Daniel J. Levitin**

So now you've learned what goes on in an aging brain, and how you can lead a longer, happier life. When you recognize aging as a time like any other, with pros and cons, you can start making the right life choices that will turn it into a time to enjoy.

But aging isn't the only realm where knowing how your brain works helps you to live better. In _The Organized Mind_, Daniel J. Levitin reveals how the brain organizes and synthesizes information and how this helps you come up with strategies for living a stress-free, productive life. To learn how to organize your brain, head on over to our blinks to _The Organized Mind_.
---

### Daniel J. Levitin

Daniel J. Levitin is an emeritus professor of psychology and behavioral neuroscience at McGill University. He is the author of four best-selling books, including _The Organized Mind._ Levitin is also a well known public speaker, and his TED Talk has over 16 million views.

