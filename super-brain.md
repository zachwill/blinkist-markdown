---
id: 5885f2045806db0004d7fedb
slug: super-brain-en
published_date: 2017-01-27T00:00:00.000+00:00
author: Deepak Chopra and Rudolph E. Tanzi
title: Super Brain
subtitle: Unleash the Explosive Power of Your Mind
main_color: FBDE3A
text_color: 7A6C1C
---

# Super Brain

_Unleash the Explosive Power of Your Mind_

**Deepak Chopra and Rudolph E. Tanzi**

_Super Brain_ (2012) offers a fascinating look at the amazing powers your brain has to change your life and even alter your perception of the world. "Mind over matter" is a rule; with the right set of tools, you can rise above the daily grind to live a healthier, longer life free from fear and disappointment.

_"Loved this book-in-blinks! I'm very interested in learning about the brain, endocrinology and the biochemical processes of the body. As a personal trainer, massage therapist and yoga teacher, I'm somewhat of a body nerd."_ — Lisa, Blinkist user, USA

---
### 1. What’s in it for me? Unleash the “super brain” hidden within your mind. 

A jumble of some 100 billion neurons, the human brain is one of nature's most complex biological creations. People tap the power of their brains daily to perform menial tasks to math equations, to access memory of past experiences and process feelings about current events.

Yet the complexity of daily life barely makes the human brain break a sweat. Imagine if we could truly unlock the full capacity of those billions of neurons and move beyond the brain's baseline activity?

These blinks show you that there is a way. By increasing self-awareness and focusing the brain on tasks of self-improvement, you can push your brain beyond what you think is possible. In these blinks, you'll learn through scientific insight and spiritual wisdom how to make your brain into a "super brain."

In these blinks, you'll find out

  * why babies offer a role model in developing a super brain;

  * how exercising your brain is the best way to achieve a beach body; and

  * why performing altruistic acts prolongs your life.

### 2. Once you understand how the brain works, you can use this knowledge to heal your body. 

The mind and the body are not separate. In fact, the human brain is deeply connected to each and every cell in the human body.

When the brain communicates with the body, _feedback loops_ can occur. Feedback loops happen when outside stimuli set off a chain reaction of simultaneous and sometimes conflicting messages.

Let's say you're walking down a street when a dog jumps out from behind a door and startles you. This event causes your brain to send a fight-or-flight message to your body; your heart in response will start to beat faster.

A scare like this possibly could lead to a heart attack, but this is usually prevented by another message from the brain that sets off a feedback loop. While kicking your heart into gear, the brain simultaneously tells the adrenal glands to produce hormones to keep your heart from beating too fast; these hormones eventually will ease the heart back to a normal rhythm.

What's exciting about this process is that we have the power to control feedback loops as well as determine how this communication affects the body.

For example, Tibetan monks practice a harmonious balance between mind and body. Through intense focus, monks can control which messages the brain sends to the body. When the temperature dips below freezing, a monk can meditate and encourage his brain to send his body messages that will raise his body temperature to a comfortable level.

This kind of mind control has also been tapped to fight diseases once considered chronic and incurable.

When a person has a stroke, the trauma can sever neural pathways used to send messages from the brain to parts of the body, which can result in partial paralysis.

While medical professionals once thought such paralysis was incurable, now therapists can help stroke victims train the brain by sending repetitive messages to affected areas of the body. As part of the therapy, a patient focuses on simple tasks, such as lifting a cup or writing down a word, and over time, the neural pathways will repair themselves and regular movement can be restored.

This example shows us how resilient and powerful the brain can be when we give it focused attention.

> _"Everything hinges on how you relate to your brain. By setting higher expectations, you enter a phase of higher functioning."_

### 3. People can generate brain neurons through intellectual challenges and physical exercise. 

Sometimes people talk about activities in terms of how many brain cells a challenging task will "cost" — as if humans have a limited supply of brain cells!

It's true that the human brain contains a lot of neurons. What's more, the brain can even generate more neurons if needed.

There's a popular myth that suggests the brain is like any other machine. Over time, the brain suffers so much wear and tear that it will inevitably deteriorate or become permanently damaged.

The human brain daily loses the use of some 85,000 neurons on average, but really, this total is a drop in the ocean. There are 40 billion available neurons in the cerebral cortex alone!

If we factor in the neurons we lose daily, at this rate, a human could live for hundreds of years and still have plenty of neurons left to live a life as a brilliant, talented mathematician.

Amazingly, the human brain is creating new brain cells all the time.

At the University of Rochester, neurologist Paul Coleman made a fascinating discovery. He found that the brain of a 20-year-old person and the brain of a 70-year-old person basically have the same amount of neurons. This is because of our body's ability to regenerate cells.

The human brain is always growing and evolving. It does this when it is given a mental challenge, such as learning a new language or working out a math problem.

Interestingly, the same effect has been observed in birds. When researchers studied the zebra finch, they found that a bird's brain grew remarkably during mating season, as birds were learning new songs to attract mates.

Physical exercise also helps produce neurons in the brain. At the University of Chicago, neurologist Sam Sisodia showed that neurons created through exercise can also help keep mice from developing diseases such as Alzheimer's.

In the next blink, we'll take a closer look at the things you can do to increase your brain's power.

### 4. To grow and improve your brain’s power, be open to new experiences and the world around you. 

Who doesn't want a super brain, or to become the next Steve Jobs or Warren Buffet? If you're interested in growing your brain, however, you should look at different role models.

Babies are experts at learning and developing their brains. They can teach us a lot about increasing our brainpower.

The first step to growing your brain is to be open to the world and the possibilities it offers for learning and growing.

Most people go about their day closed off from the outside world. They keep their heads down, uninterested in deviating from the safety of a daily routine.

Shutting down your powers of observation and curiosity keeps your brain from growing. To stimulate your super brain, stay open to new sights and observe nature or the environment where you live.

Talk to new people, and pay attention to new sounds and smells; don't be afraid to deviate from what's "normal." Try new dishes, taste new things!

Reading is also important. Yet many people are put off by newspapers since they are often filled with stories that are sad or overwhelming. This attitude shuts out new information; and with no new information, your brain starves.

So don't be afraid to absorb all the news you can — such information is a valuable resource in finding out more about your world, what is happening "out there" and how people in other countries live.

While you're exploring the world through news, it's also important to remain open-minded when experiencing the opinions of other people.

A developing brain shouldn't be prejudiced or hold preconceived notions about what is right or wrong. Don't dismiss someone's opinion if that opinion doesn't align with your thoughts, on politics, religion or otherwise.

Instead, use discussions as an opportunity to become more informed about all sides of an issue.

### 5. Tap your brain’s feedback loop and your emotions to find ways to eat less and live healthier. 

There's no end to the benefits of a super brain. When you focus on developing your brain, you'll not only improve your intellect but also curb any damaging behaviors, such as overeating.

We all know how hard it is to stick to a diet. And there's even scientific research that supports dieters' experiences of the difficulties in shedding unwanted pounds.

Scientists in Australia have discovered a "hunger hormone" called _ghrelin_. This hormone kicks into overdrive when you lose weight, making you feel desperately hungry.

So if you're lucky enough to get rid of your "love handles," ghrelin puts your body on red alert and before you know it, you've put the weight back on!

In this particular instance, your body's mechanisms are working against you. Once again, you can use the feedback you give your brain to find a solution.

More often than not, we overeat for emotional reasons, such as when we're overtired, stressed, lonely or anxious. Consuming sweet foods or other high-calorie snacks can help us soothe unwanted feelings.

The first step to dieting successfully is to recognize the difference between emotional eating and real hunger. Once you are conscious of why you want to eat, you can provide your brain with the appropriate feedback.

By giving your brain conscious feedback that acknowledges you are eating for emotional reasons, your brain will eventually understand the real issue and stop pushing you to seek more food.

Instead of sending you to the kitchen, your brain will treat your craving as an emotional issue and trigger a desire to see a friend or counselor, to talk through the issue and find a resolution.

### 6. Intuition is a powerful trait that may be able to help you predict events before they happen. 

Chances are you've experienced a strong sense of intuition at some point. Perhaps you felt you needed to stay away from someone at a large gathering because that person seemed somehow dangerous.

Human intuition can be more accurate than you might think. Recent studies suggest that a person's snap judgment can sometimes be more beneficial than taking the time to rationally think things over.

Studies also have shown that people are better at recognizing faces when they're relying on intuition rather than being analytical.

In one study, participants were asked to watch images of faces, and identify if a particular face came up. In the first experiment, they were just shown a split second of the images, and then in a second, they were given more time to look at them. Unbelievably, people were better at recognising the particular face when they were only given a brief glimpse of it.

While human intuition provides a great service when recognizing a friendly face, it's also a trait that guides us in many decisions, from choosing a job to selecting a mate or place to live.

Intuition can be so strong, in fact, that it can even predict events before they happen.

One experiment involved participants being shown random photographs, many of which depicted violent images.

Researchers monitored the reactions of participants as they viewed the photographs by measuring heart rate, blood pressure and sweat produced.

Each time a participant saw a violent image, as was expected, that person registered a stressful response, with elevated heart rate and blood pressure.

After some time, however, things in the study took a curious turn. Even though the photographs were displayed at random, participants' stress levels began to spike microseconds before a violent image was shown.

Somehow, the participants' intuition began to predict the exact moment when a violent image was about to appear.

> _"If you ignore your intuition, you lose the ability to feel out situations, which leads to blind decisions."_

### 7. People think money and fame are what create happiness, but those typical goals are misguided. 

Global businesses and billions of dollars are devoted to making people feel comfortable and happy. Yet if you take just a moment to examine the faces of people riding a bus or subway train, you'll quickly see that happiness for everyone is still an elusive goal.

There's a simple reason for this. People have the wrong idea about what will make them happy.

Most people equate happiness with money, fame, finding a mate or having children. These people then come to find that achieving these goals doesn't automatically come with a feeling of happiness.

Having children, for example, comes with a lot of stress; it doesn't automatically lead to a happier life.

The same is true for professional success. When the top tennis players in the world were asked what motivated them, they said it wasn't the joy of playing or happiness when winning a match. They trained hard because they were afraid of losing — not exactly a positive feeling!

So if you want to live a happy life, it's important to set the right goals.

This means replacing unfulfilling goals with ones that align with personal values and that continue to provide long-term results. So instead of working for financial success, find a job that you love and that provides you with the opportunity to improve the lives of others.

This is what Brendon Grimshaw did. In 1962, he purchased an abandoned tropical island in the Seychelles for £8,000. He quit his job as a journalist in England in the 1970s and became the island's permanent caretaker.

Grimshaw lived a happy life on the island for decades, planting hundreds of mahogany trees and turning his island into a wildlife refuge for animals, such as the giant tortoise. Grimshaw passed away in 2012.

### 8. Meditation and altruistic behavior can positively stimulate the brain and body, prolonging life. 

If you discovered that cells in your body were damaged, your first instinct might be to seek treatment with pills or surgery. Science has shown, however, that there are more effective and less invasive alternatives.

Meditation, for example, isn't just a great way to bring calm and peace into your life, it also can rejuvenate your body.

A 2010 study performed by researchers at the University of California at Davis showed that practicing intensive meditation can increase the body's supply of the enzyme _telomerase_.

As we age, the "caps" at the ends of DNA strands called _telomeres_ can deteriorate, and this can lead to cell damage.

The enzyme telomerase strengthens telomeres, making sure that both DNA and cells remain healthy.

But this isn't the only job of telomerase. The same study revealed that people with higher levels of telomerase also experienced more happiness and resistance to disease. So these people not only live longer, healthier lives but also experience more contentment.

Additionally, another way to live a longer, healthier life is to practice altruism. Altruism is so powerful that just witnessing it in action can have health benefits!

Harvard University researchers monitored the reactions of people who watched a documentary about Mother Teresa, a Roman Catholic nun and missionary who worked with the poor.

The documentary showed Mother Teresa caring for sick children in Calcutta. As the participants watched the film, their blood pressure and heart rates decreased to healthy levels — levels that if maintained, could indeed lead to living longer.

Another case for altruism's positive effects was made by psychologist Sara Konrath of the University of Michigan in 2008.

She found a study from 1957 that observed the health of some 10,000 high school graduates. She decided to track these people down 50 years later and see how healthy they were.

She found that people who practiced volunteer work during their lifetimes indeed lived longer, but only if they were motivated to help others and not just escape personal troubles.

### 9. There is no “fixed” reality. Our brains perceive the world, yet such perception is just illusion. 

What defines "reality?" Just because you perceive something, does that make it real?

One could say that the human brain is so powerful, it turns illusion into reality.

If you're admiring the majesty of the Grand Canyon, for example, your brain works to process information coming to it through your senses. You see that the stone cliffs are red; you feel on your skin a slight breeze and the warmth of the sun; you can smell the valley's wildflowers.

You believe that this is reality. Yet what you are experiencing is just your brain's translation of information, sent to it by electrons in a chemical process that is devoid of any color, smell or tactile sensation.

Reality, thus, is what we perceive — there is no proof of any physical world outside our brain, no "fixed reality" as all we know depends on what we perceive.

After all, we say that grass is green because our brains tell us that it's green. But for someone who is colorblind, reality is something altogether different.

The study of quantum physics has also raised fundamental questions about our world.

In 1923, physicists Bruce Rosenblum and Fred Kuttner published the book _Quantum Enigma_, explaining how the world can't be described as being "fixed." In fact, only some particles of matter remain still, while others move in wave-like patterns in a given spectrum.

In short, matter isn't solid, but fluctuates. Or, in other words, _reality is indeed an illusion_.

A statement like this of course leads to many, many other questions. But if your super brain is open to all possibilities, the idea that there is a deeper consciousness, something that is part of everything we perceive, isn't so frightening.

Indeed, coming to an understanding like this explains why our brains are capable of so much!

### 10. Final summary 

The key message in this book:

**The brain is not a calculator with a limited battery. It is a living, pulsating organ that grows as you learn, expands as you meditate and communicates with every cell in your body. It is also capable of far more than we give it credit for — so don't underestimate its powers. Learn more about what your brain can do to improve your life.**

Actionable advice:

**Practice mindfulness.**

Mindfulness is the practice of being profoundly aware of what you are thinking and doing. When you play the violin, for example, you are not just drawing a bow across some strings. To play mindfully, you see yourself playing, breathing deeply, and are in touch with your thoughts and feelings as the music comes from not just the instrument but also deep inside you. In everything you do, regularly ask yourself, "How do I feel?" to stay connected with your inner consciousness.

**What to read next:** ** _The Seven Spiritual Laws of Success_** **, by Deepak Chopra**

You now know some of Deepak Chopra's techniques for increasing success — but there are many more. As one of the leading advocates of alternative medicine and spiritual values, Dr. Chopra understands many strategies for growing your abilities — strategies that most people don't know.

In _The Seven Spiritual Laws of Success_, Deepak Chopra explains the hidden forces in the universe that, if used correctly, can lead you toward a better job, stronger relationships and increased wealth. To discover how to use these forces yourself, and to learn why the universe gives back to those who give, read our blinks to _The Seven Spiritual Laws of Success_.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Deepak Chopra and Rudolph E. Tanzi

Rudolph E. Tanzi is a professor of neurology at Harvard University, specializing in the genetics of aging and associated neurological disorders, such as Alzheimer's. He has received many awards for his work, including the Metropolitan Life Award. He is also the coauthor of the book _Super Genes_.

Deepak Chopra is a doctor of internal medicine and endocrinology and teaches at the Kellogg School of Management. He is the founder of The Chopra Foundation and author of many bestselling books, including _Ageless Body, Timeless Mind_ and _Synchrodestiny._

