---
id: 56fa9d80df8142000700003d
slug: super-immunity-en
published_date: 2016-03-31T00:00:00.000+00:00
author: Joel Fuhrmann, MD
title: Super Immunity
subtitle: The Essential Nutrition Guide for Boosting Your Body's Defenses to Live Longer, Stronger and Disease Free
main_color: 768750
text_color: 606E41
---

# Super Immunity

_The Essential Nutrition Guide for Boosting Your Body's Defenses to Live Longer, Stronger and Disease Free_

**Joel Fuhrmann, MD**

_Super Immunity_ (2011) reveals the secret to a better, stronger immune system and healthier body: superfoods. These blinks shed light on the shortcomings of modern medicine and teach you how to take advantage of the healing powers of plant foods rich in nutrients and phytochemicals.

---
### 1. What’s in it for me? How superfoods can boost your health. 

There are so many diet books out there telling you what to eat and what to avoid. Some say that carbs are the devil while protein is all important. Others say that you should cut your protein intake, and that the key to healthy living is carbs. 

So, what should you believe?

These blinks show you that instead of fussing about with carbs and fats you should look for foods rich in essential nutrients, or "superfoods." A diet based on these few simple ingredients will leave you healthier, fitter and stronger.

In these blinks, you will find out

  * why your flu jab may leave you ill;

  * what vitamin supplements you should not be taking; and

  * why superfoods can help prevent cancer.

### 2. Healthy eating that heals your body is an ancient idea, but more relevant than ever today. 

"Let food be thy medicine" was the advice of forefather of modern medicine and all-round ancient wise guy Hippocrates. From year dot to today, humans have appreciated the healing power of certain foods. 

Historical documents show that the Ancient Greeks and Egyptians made use of soothing herbs and restorative recipes to cure colds, improve health and ward off disease. Advancements in biology have allowed us to pinpoint why certain foods can help our health. 

It's all a matter of chemicals known as _phytochemicals_, which are found in particular plants _._ These compounds are essential for the survival and growth of the plant. Phytochemicals are vital for human bodies too — we need to consume them to keep our immune system in working order. 

Phytochemicals make incredible healers. Some studies even suggest that phytochemicals can reduce the risk of contracting AIDS in individuals with good health and nutrition. Today, diseases and viruses are as real a threat as ever. You'd think that phytochemicals would make up a major part of our diet. Unfortunately, this isn't the case. 

Animal products, processed foods, cold cereals and pretty much anything made with white flour are far more likely to make up major parts of our diets today. As little as 10 percent of the average American diet is comprised of vegetables, fruits, beans, nuts and seeds. But even this figure can't reflect sufficient phytochemical consumption — half of this 10 percent is made up of white potato, which is far from rich in phytochemicals. Our immune systems are suffering as a result. 

So how do we survive if we no longer rely on food for good health? With an unprecedented dependence on medical care. Yes, medicine today is more advanced than ever. But does this mean we should rely on it? Find out in the next blink.

> _"For superior health, we must eat more nutrient-rich foods and fewer calorie-rich foods."_

### 3. Modern medicine treats the symptoms rather than addressing the causes of illnesses. 

It's comforting to know that if you were in a severe car crash tomorrow, you'd have a great chance of surviving due to medical advances made in recent decades. But aside from these emergency cases, modern medical care is rather regressive. Why? Because it treats the symptoms, not the causes of a disease. 

Take type 2 diabetes, for instance. This is an illness that doctors combat with prescription medicine. A study of over 90,000 participants diagnosed with type 2 diabetes revealed that the two most popular prescription drugs for this illness actually increase the risk of congestive heart failure. This statistic is shocking, but perhaps not surprising when we consider the one key drawback of drugs. 

Type 2 diabetes, along with many other health issues, is the result of insufficient exercise and high calorie, low-nutrient diets. Drugs don't treat this cause, only the symptoms. And what's more, diabetes medication often makes it harder for a patient to make positive changes, as it increases appetite. 

Like medication, vaccination is also seen as a huge leap forward in medical care. But are vaccinations as effective as we're told? According to the author, this is rather doubtful. 

The US Center for Disease Control (CDC) recommends that everyone older than six months should receive a universal flu vaccination. Many doctors encourage patients to get these flu shots each year to prevent the disease from spreading and infecting individuals. But the vaccination has a few crucial problems. 

Consider that the flu can actually be caused by 200 different viruses. The flu vaccine can only act upon 10 percent of these viruses, and is thus rather unlikely to provide sufficient protection. On top of this, every flu vaccine has 25 micrograms of _thimerosal,_ which contains toxic mercury. Getting a flu shot every year thus increases the risk of mercury-induced damage to the brain and nervous system. 

The reality is that staying healthy isn't as easy as taking prescriptions and getting vaccinated. We need to take a more active approach. But how? With superfoods!

### 4. Superfoods may not only prevent cancer – they can also make you more resistant to the flu. 

So what exactly are superfoods? Well, there's a whole range of them. From mushrooms to greens to cruciferous vegetables, superfoods all have one thing in common: high amounts of great nutrients and phytochemicals that help our bodies heal. 

New research has shown adding superfoods to your diet can lead to lower rates of cancer. But how does this work? Well, cancer is tied to the process of _methylation_, where a methyl group containing one carbon and three hydrogen atoms is added to a gene. This methylated gene malfunctions, and can affect the process of cell division so that cells grow at an explosive rate. This is what leads to cancer. 

Superfoods are actually able to disable these methylated cells, which gives them their cancer-crushing ability. Kale, broccoli, cauliflower and collard greens are particularly good at this. Superfoods also often contain _isothiocyanates_ or _ITCs._ These are compounds that give our immune systems an additional boost and further protect us from cancer. 

As we can see, superfoods aren't just like any other vegetable. Studies conducted at the Harvard School of Public Health underlined this by demonstrating how a 20 percent increase in plant foods leads to a 20 percent decrease in cancer rates, while a 20 percent increase in cruciferous vegetables leads to a 40 percent decrease in cancer rates. 

It's not just cancer that superfoods can arm us against. Viruses such as the flu also have a harder time bringing us down if we've got superfoods in our system. Research demonstrates that ITCs contained in cruciferous vegetables stimulate our immune system to fight off viruses and bacteria by lifting cell-killing and resistance capabilities. ITCs can even protect us when antibiotics can't. With their antimicrobial effects, ITCs boost our natural defenses and up the ante when our body is facing drug-resistant bacteria.

> _"Cruciferous vegetables are not only the most powerful anti-cancer foods in existence; they are also the most micronutrient-dense of all vegetables."_

### 5. Antibiotics and cold medicine are quick fixes that can do more harm than good. 

Antibiotics often seem like a great response to any annoying sickness — at least, that's how many doctors and pharmacists portray them. Antibiotics are effective against bacteria, but not viruses. And yet, viruses cause 95 percent of acute illnesses, including the common cold. So, antibiotics are useless in most cases. But it gets worse. 

When taken unnecessarily, antibiotics destroy much of the vital bacteria in our gastrointestinal tract, which is also home to 70 percent of our immune cells. Painful digestive disturbances and immune-system dysfunction are both the results of damaged gut bacteria. 

Like antibiotics, cold medicine is another medical "quick fix" that actually does more harm than good. Over-the-counter cold remedies such as NyQuil, Dimetapp and Robitussin won't make your cold go away. By suppressing the symptoms of the cold, they can actually keep you ill for longer. 

Coughing is one of our body's most important natural healing mechanisms. A cough allows our body to clear out dead cells, virus particles and mucous from our airways. If cough suppressants were effective and truly interrupted this function, viral illnesses would turn into more prolonged and serious diseases such as pneumonia. On top of this, many cough and cold remedies have negative impacts on our sleep cycle and digestive system. 

Rather than relying on antibiotics and cold medicine, we should maintain our health by eating the right foods. But what are the right foods for our body?

### 6. Lots of nutrients and wisely chosen fats, carbs and proteins comprise a healthy diet. 

How should we choose what to eat? With countless diet books out there, it's tempting just to focus on fats, carbs or proteins when assessing the foods available to us. But there's far more to healthy eating than just those. 

In fact, a healthy diet should feature lots of nutrients and few calories. Colorful vegetables, especially greens, count as nutrient-rich foods that help us meet our body's needs for fiber, vitamins, minerals and phytochemicals. 

On the other end of the spectrum, you've got food like bread and pasta, with lots of calories but very few nutrients. Unsurprisingly, the consumption of such foods causes waste products to accumulate in our cells. This leads to premature aging as well as increased susceptibility to disease and heart attacks. 

But what about carbs? They're a sure sign of an unhealthy diet, right? Not quite! From beans, peas, tomatoes and berries to squash, quinoa, wild rice and potatoes, there are many healthy, nutrient-rich and delicious carbs out there!

Similarly, we needn't fear the role of fat in our diets. The truth is that having less than 10 percent fat in your diet is actually bad for your health. A diet with 15 to 30 percent fat can be considered healthy, so long as you're getting the nutrients you need!

Finally, it's time to reconsider what we thought we knew about proteins. We can gain protein from animal food sources, as well as plants. But, while proteins from plants boost our health, too much animal protein is linked to cancer, decreased immune function and accelerated aging. 

A nutrient-rich diet is vital for great health. Although we can gain the nutrients we need from food sources, supplements are also becoming quite popular. But, when it comes to supplements, you've got to read the fine print! Find out why in the next blink.

> _"Ask yourself, is the food I am about to eat a whole, natural plant source of calories?"_

### 7. Vitamin supplements are good in theory, but only if you choose the right ones. 

Let's face it: we can't eat a perfectly healthy diet every day. Unfortunately, this means we're likely to be deficient in the vitamins and minerals we need. Supplements can be a great way to ensure your body is always nourished. But be discerning!

Vitamin D, vitamin B12, zinc and iodine are essential vitamins and minerals that are tricky to keep at a sufficient level. Iodine intake decreases naturally if we eat less salt, while zinc and vitamin B12 are hard to source in food unless you eat meat. As we spend less time outside in the sun, many people have a vitamin D deficiency too. 

In light of this, supplements seem like a pretty good solution. People are often drawn to multivitamins as a one-stop shop for nourishment. Unfortunately, multivitamins may contain other vitamins that do your body more harm than good. One of these is vitamin A. 

Beta-carotene, which is converted to vitamin A in our bodies, was previously considered a safe supplement. But recent studies have revealed that ingesting it in supplement form can raise the risk of cancer. On top of this, vitamin A is linked to calcium loss responsible for osteoporosis. 

It's a similar story with folic acid. Folic acid is often confused with folate, which belongs to the vitamin-B family, is found in natural plant foods and is essential for pregnant women. 

Unlike folate, folic acid is synthetic, cannot be found in natural foods and is tied to breast cancer in women, colorectal cancer in men and cardiac birth defects in children. Folate is abundant in green vegetables, so a folic acid supplement is entirely unnecessary.

### 8. Limit your salt intake and consume more omega-3 fatty acids to keep your body balanced. 

Who doesn't love a salty snack? Human bodies are actually drawn to salt consumption. Whether it's potato chips or sardines, we've got our salty craving. And no kitchen is complete without table salt for seasoning. But for millions of years, human diets didn't feature any added salt. 

Although we need the sodium that table salt provides, too much salt can be incredibly harmful. Today, humans consume around 3,500 milligrams of salt each day. Yet, our ancestors tended to consume far less — around 600 to 800 milligrams per day. This increased intake is putting us at risk of stomach cancer, osteoporosis and heart attacks. 

Studies show that there is a strong correlation between higher salt intake and high blood pressure. High blood pressure is the leading cause of 62 percent of strokes and 49 percent of coronary heart disease. Elderly people living in suburbs and cities often exhibit high blood pressure, while those living in rural or remote areas do not. Why? Because the latter group does not use extra salt in their diet. 

While salt is one food that has a powerful negative impact on our bodies, there are foods with equally significant benefits. Take omega-3 fatty acids, for example. Though our bodies don't produce these fatty acids themselves, we need them to lower inflammation, protect our brains and prevent cancer. So, we have to find it in the foods we eat. 

From hemp and chia seeds to walnuts, fish and green vegetables, there are plenty of foods rich in these acids. Processed foods also contain fatty acids, but a kind that is less useful to our body. 

It's also a good idea to increase our omega-3 intake with the aid of supplements. While fish oil capsules are popular, it's worth noting that they can contain toxic levels of mercury if improperly cleaned or stored. A safer option is high-quality supplements derived from algae grown indoors in a clean and controlled environment. 

Reducing your salt intake and supplementing your diet with omega-3 fatty acids are just two of many ways to improve your health in the long run. So, if you aren't feeling as fit as you'd like, take a look at your diet with these blinks in mind. It might be time to try out some new recipes!

### 9. Final summary 

The key message in this book:

**Boost your health and immune system not with quick-fix medicine, but with foods rich in nutrients and phytochemicals. Since ancient times, food has proven itself to be a great healer. Add superfoods to your diet and enjoy their flu-fighting and cancer-crushing benefits.**

Actionable advice:

**Get blending!**

Need an easy and delicious recipe to kickstart your healthy diet? Well, how about turning a salad into a refreshing smoothie? You'll need half a cup of pomegranate juice, one peeled and cored apple, a quarter cup of walnuts, three cups of collard greens, a cup of lettuce and a quarter cup of water or ice cubes for extra refreshment. Blend it all together till smooth and you're ready to go!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Missing Microbes_** **by Martin Blaser**

_Missing Microbes_ (2014) explores the strange and microscopic world inside your guts. It sheds light on the crucial role played by microbes — tiny creatures that keep your body happy and healthy — and explains the dangers of overusing antibiotics. To find these blinks, press "I'm done" at the bottom of the screen.
---

### Joel Fuhrmann, MD

Joel Fuhrmann, MD, is a family physician who focuses on using nutritional and natural methods to prevent and reverse diseases. He created the Health Starts Here initiative for whole foods and is the research director of the Nutritional Research Project of the National Health Association.

