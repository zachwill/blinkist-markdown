---
id: 56cdb01e74766a0007000060
slug: superbetter-en
published_date: 2016-02-25T00:00:00.000+00:00
author: Jane McGonigal
title: SuperBetter
subtitle: A Revolutionary Approach to Getting Stronger, Happier, Braver, and More Resilient
main_color: 2DA9A5
text_color: 1F7573
---

# SuperBetter

_A Revolutionary Approach to Getting Stronger, Happier, Braver, and More Resilient_

**Jane McGonigal**

_SuperBetter_ (2015) teaches you how to overcome major challenges by harnessing the power of games. Whether you're getting over an illness, writing a novel or trying to become a vegetarian, the game of SuperBetter can revolutionize your life and get you where you want to go.

---
### 1. What’s in it for me? Game your way to a better life. 

The average American spends well over ten hours every week playing computer games. Whether it's teaming up with friends for an evening of _Call of Duty_, a sweaty session of aerobics on _Wii Sports_ or a few minutes of _Angry Birds_ on the bus to work, computer games have become a big part of our lives. And, actually, people have always played games. it's part of what makes us human.

You might think of games simply as entertainment or a way to relax. But, as these blinks will show you, there's much more to games than that. Introducing a spirit of gaming into your life can strengthen your cognitive skills, help you overcome prejudices and propel toward your goal. In short, they can help you become superbetter.

In these blinks, you'll learn

  * how throwing snowballs can relieve pain;

  * why games improve your social skills; and

  * what you can learn from _Zelda_.

### 2. SuperBetter is a game that helps you overcome challenges and grow stronger as a person. 

Have you ever heard of _SuperBetter_?

It's a game created by the author, who came up with the idea while recovering from a brain injury. During her recovery, she realized that she didn't just want to get _better_, she wanted to be _even better_ than she was before the injury — _superbetter_! So she created a game to help her get there. 

Games make it fun and exciting to overcome challenges. And they can even alleviate stress and interpersonal problems! SuperBetter is literally designed to empower you and give you more control over your life whenever you're feeling overwhelmed by illness or any other serious problem.

SuperBetter works because of two human phenomena: _post-traumatic_ _growth_ and _post-ecstatic growth_. 

Trauma doesn't necessarily lead to long-term emotional or mental problems. In fact, trauma can usher in a life that's happier and more fulfilling than the one you had before! That's how post-traumatic growth works. 

You don't need trauma to experience personal growth, however. If you overcome challenges you set for yourself, such as running a marathon or quitting smoking, you can undergo post-ecstatic growth _._

Post-traumatic growth and post-ecstatic growth are forms of positive development that you experience during difficult times. And, if you follow the seven steps outlined in the SuperBetter method, you'll stand a much better chance of growing. These steps, the effectiveness of which has been thoroughly tested and proved, will help you build _physical resilience_, _mental resilience_, _emotional resilience_ and _social resilience_. Read on to learn more about these terms — and why games are so good at helping you build them!

> _"With post-traumatic growth, we find the strengths and courage to do the things that make us happy, and to understand and express our true selves."_

### 3. Games can lessen pain and help you overcome trauma. 

On average, over one billion people spend at least one hour per day playing video games. 

And those games lend themselves to more than just fun. They can actually alleviate physical pain and help people overcome trauma!

This was demonstrated by a recent study on burn victims. Severe burns cause pain that can't be lessened with medication; however, a group of researchers found that burn victims benefited from playing _Snow World_, a 3D game about a virtual world of ice caves and snowballs. If they played _Snow World_ while undergoing painful treatments, they felt less pain. 

Games like _Snow World_ have this effect because they control a person's _attention spotlight_. Generally, your brain is constantly being bombarded with signals. Pain, being a particularly strong signal, usually gets the most attention. But if a patient directs their brain's attention elsewhere — toward a game like _Snow World_, for instance — the impact of all other signals is lessened. 

Another study found that _Tetris_ helped reduce flashbacks. If, within six hours of some traumatic event — a serious car accident, for example — a patient played ten minutes of _Tetris_, the patient, though still able to remember the accident, experienced fewer involuntary flashbacks.

When we play games, we don't just give them our attention, either. We pay them a special kind of attention called _flow_, the state of being completely absorbed in an activity. 

Flow isn't about distraction; it's about engagement. So leisure activities like watching TV don't count as flow. An activity that gets you into flow has to be challenging and interactive; you have to have more control over what you're thinking and feeling.

That's why children who play _Super Mario Bros_ before going into surgery feel less anxiety and need less medication: they're simply less focused on their fear of the surgery. Comic books or TV shows don't provide the same release because they don't require the same kind of cognitive absorption.

### 4. Games can help you build both empathy and positive relationships with others. 

When confronted with a big challenge, it's easy to feel alone. But remember: you're never _really_ alone! Everyone is surrounded by potential allies — you just have to find them. 

Humans naturally mimic those around them. They imitate each other's speech patterns. They fall into step when walking down the street. And the more time people spend together, the better they get at predicting each other's thoughts and actions.

When two people play the same game in the same room, they develop a strong neurological and physiological bond. They display the same facial expressions; their heartbeats even settle into a similar rhythm — and this happens regardless of whether they're playing with or against each other.

And the bond goes even further than that: we're also more likely to _like_ someone if we're playing a game with them. That's because we're drawn to people we perceive as similar to ourselves. 

Playing video games with someone can connect you in other ways, too. Another study found that when a younger and elder person bowled together on _Wii Sports_, they both developed more positive opinions about their game partner's age group. If they only watched TV together, they developed a more positive opinion of that person, but _not_ of their age group as a whole. Games can open up whole new worlds of friends!

The Middle East Gaming Challenge is founded on just such findings. It brings together Israelis and Palestinians in hopes of bridging differences with gaming. 

It's important for gaming partners to be in the same room, however. Excessive competition against strangers _not_ in the same room generates negative feelings, like anger and aggression, especially after a loss. If the players are in the same room, on the other hand, they develop empathy for each other.

### 5. Games strengthen your motivation and perseverance. 

Do you struggle with motivation at times? If you feel unmotivated or discouraged, don't fret — because games are here to help!

Games boost your motivation, confidence and strength. They can also build character by improving your work ethic and sense of compassion. After all, they make you the hero of your own story!

In the game _Re-Mission_, for example, the player fights cancer cells in the human body with chemotherapy blasters and antibiotic grenades. It was designed to help young cancer patients stay strong-willed during difficult treatments. 

The game fosters optimism and reinforces a sense of control. _Re-Mission_ also helps patients internalize the message that every treatment dose counts, because the blaster gets weaker with every dose that's missed. People who played for just two hours adhered better to their medication schedule for three months. 

Young cancer patients who played _Re-Mission_ also reported feeling more powerful and optimistic. That state of mind is called _self-efficacy_ : the belief that you have the power to effect positive change in your life. Once you've developed a strong sense of self-efficacy, it tends to stick around.

And the confidence you build playing games shows up in your everyday life, too. Video games increase your level of _dopamine_, the brain's pleasure neurotransmitter, which also increases your motivation and determination. 

Every time you're faced with a decision, your brain does a quick cost-benefit analysis — and the result usually depends on your dopamine level! If your dopamine level is high, you worry less about the decision's pitfalls and focus more on its potentially positive results. That makes you more determined to succeed and less discouraged by setbacks. 

Again, this affects more than just your gaming. When you play games, you train your brain. Dedicated gamers spend more time solving problems — and so they're more persistent and have a stronger work ethic, as well as a more positive outlook on what they can achieve.

### 6. Games can have a positive impact on your life, as long as you play for the right reasons. 

Video games cause some people to become antisocial shut-ins; others become more creative and hardworking. What accounts for this difference?

The effect games have on your life doesn't depend on which games you play or how long you play them. It depends on _why_ you play. 

Not all people play games for educational or social reasons. Some play games as a form of escape from the problems of life — and that's when gaming becomes harmful. If you play games with a positive goal, however, you're more likely to reap the benefits and feel happier in your non-gaming life, too. 

And consciously developing a positive gaming mindset is easy, even if you started out as an escapist player. Just remember that games can be a source of strength, skill and willpower.

Everyone experiences a form of _immersion_ when they play games, but that immersion can be _self-suppressive_ or _self-expansive_, depending on the person's motivation for playing. 

If you're playing in order to avoid something, that's _self-suppressive_. The burn victims who play _Snow World_ are, in part, trying to avoid pain. But it's also a proactive activity, something that empowers them to face their treatment — and that's _self-expansive_ immersion. 

Video games also do more than help you overcome challenges! They hone your visual attention and spatial intelligence, and studies show that they improve your problem-solving skills and creativity with storytelling. 

If you play them right, video games put you in a better mood, too. You'll get better at managing your own frustration, especially if you play multiplayer games that depend on strong team coordination and communication.

### 7. Live gamefully by replacing a threat mindset with a challenge mindset. 

Now let's get back to SuperBetter. SuperBetter gives your life structure. It consists of seven simple steps — no matter what kind of challenge you're trying to overcome or what kind of positive change you're trying to achieve, there are always seven steps.

The philosopher Bernard Suits defined a "game" as a "voluntary attempt to overcome unnecessary obstacles." There's no logical reason for attempting to hit a small white ball into a hole with a thin metal club — but we do it because golf is fun.

You also volunteer to be challenged (by your opponent or the game itself) when you play a game, which puts you in a more competitive mindset: a _challenge mindset_. The game won't always be positive — even miniature golf has bunkers! — but you'll strive to achieve the most positive outcome in the situation. 

So challenge yourself to a game of overcoming an obstacle to reach a certain aim! That's the first step in SuperBetter. Your aim can be concrete, like losing 25 pounds, or more general, like learning about opera. 

Substituting a challenge mindset for a _threat mindset_ is the foundation of living _gamefully_. So first, decide what challenge you want to overcome — whether it's an illness, a new job or a new language. Remember: it's a _challenge_, not a _threat_. Once you've identified your target, you can start building gaming into your life. 

You'll turn your anxiety into excitement when you do. Anxiety and excitement are similar. They induce similar bodily reactions, like excess energy, so the key is to change the way your mind interprets those physical sensations. 

That's as easy as saying "I'm excited!" aloud a few times! By doing that, you'll change more than just your mindset. Your arteries constrict when you feel threatened and expand when you feel challenged, making you able to accomplish greater physical feats.

### 8. Identify the power-ups and bad guys in your life. 

Super Mario fights walking turtles and seeks out mushrooms as _power-ups_ to get stronger. As a player in the game of SuperBetter, your next two steps are to identify your power-ups and _bad guys_, too! 

You need your own little boosts of power, just like Super Mario — and it's up to you to collect those boosts and activate them!

Power-ups can be anything, as long as they consistently make you feel stronger, happier and healthier. They don't have to be complicated; listening to your favorite song, petting your cat or doing a few push-ups can be power-ups.

Power-ups are about frequency, not intensity. The more positive little boosts you give yourself, the better equipped you'll be to face your problems. That's why physical power-ups, like doing a few push-ups or going for a short walk, are so beneficial.

After you've identified some power-ups, the next step is to identify your bad guys.

A bad guy is anything that blocks your progress or makes your journey or gameplay more difficult. In _Scrabble_, for example, the letter "J" is a bad guy because it's difficult to use. 

It's tough to face bad guys, but doing so forces you to develop strategies for becoming a better person. Once you know who (or what) your bad guys are, you'll develop new ways to deal with them. _Scrabble_ players learn more words with the letter "J" so they're prepared when they pull that dreaded tile. 

You need to be flexible when facing your bad guys. Stay alert to potential obstacles and experiment with different responses to them, including simply retreating from them. Battles with bad guys are important learning experiences, even if you don't win them all.

### 9. Finding allies and designing quests makes you stronger and helps you reach your goals. 

It's much harder to achieve great things when you're all alone. That's why _allies_ are so important!

Allies are friends or family members who help you move toward your goal. And games are a good way to get social support. It's much easier to ask someone to play a game with you than to ask them for help!

Social support isn't only important for its emotional value — it makes your body stronger, too! Even hugs affect your physiology by making you feel less stressed.

So seek out some allies. You don't need many. In fact, you only need about two solid allies to play SuperBetter with you. If you can't find them in person, look for them online, in discussion forums or social media groups. Remember, you're not alone: one-quarter of Americans report that they don't have people to discuss their personal issues with. The world is full of potential SuperBetter partners.

After finding some allies, the next step is to set out and complete some _quests_! Quests are simple, everyday activities that help you make progress in SuperBetter.

Pursuing a quest means taking meaningful, conscious action to get you where you want to go. A quest could be exercising a certain muscle for a minute, writing down a dream you had or simply remembering something that makes you happy. Quests help you develop new skills and strengthen your willpower.

Make at least one quest per day. Develop quests that suit you, your feelings and your values. For instance, write to five friends whenever you feel lonely. Just remember: a quest is anything that makes you feel better and stronger as a person.

> _"Naming your deepest values is the key to unlocking untapped sources of motivation, energy, and willpower."_

### 10. Develop a secret identity and go after epic wins. 

Your secret identity is something that empowers you. It plays off your personal strengths and makes SuperBetter more fun! That's why the next step in SuperBetter is to do some self-reflection and develop a secret identity of your own. 

A secret identity works like an avatar for the real world. The author thought of herself as _Jane the Concussion Slayer_ when she was recovering from her brain injury and _Jane of Willendorf_ (after _Venus of Willendorf_, the symbol of fertility) when she was trying to get pregnant. 

Such names can have a powerful impact. Secret identities keep you focused on your strengths — those qualities that make you _you_. 

So tell yourself a story where _you're_ the hero! Pick out some inspiring, heroic quotes (the author lifted some from _Buffy the Vampire Slayer_!) and design a logo for yourself or compose a theme song! Reveal your secret identity to your allies — it'll bring you closer together. 

The next step in SuperBetter is to aim for bigger victories: _epic wins_. An epic win could be sleeping one night without your iPod (if you suffer from insomnia) or going a week without your inhaler (if you suffer from asthma). Epic wins inspire you to believe in yourself. They make you less afraid of failure. 

Epic wins are challenging, but realistic. They energize you, too: just thinking about them gets you excited. 

It's okay if your epic wins are tasks that are easy for others, like sitting in a cinema for three hours. If you have back pain, that could be a big accomplishment! The point of an epic win is that it was difficult for _you_ to achieve, but you went for it anyway!

The best epic wins are often physical victories, like climbing a mountain. Physical victories make an especially big difference for people suffering from psychological trauma such as PTSD.

> _"If you are not the hero of your own story, then you're missing the whole point of your humanity."_

### 11. Outline your adventure and keep score as you make progress in it. 

Every video game hero has their own overarching _adventure_ that frames everything they do. For instance, in _Zelda_, Link is trying to save princess Zelda. 

Your quests, power-ups and bad guys all make up your adventure. Your adventure could be aimed at managing the stress of college admissions or becoming a vegetarian. The habits and skills you acquire from your daily quests move you toward achieving whatever your adventure is about. 

Imagine you're trying to become a vegetarian, for instance. If you find the smell of a cheeseburger appetizing, you'll have to develop the skill of resisting it. Why not team up with some allies working toward the same goal and create secret identities as veggie superheroes!

Design your adventure to last at least six weeks. Studies have shown that after six weeks of SuperBetter, players had significantly stronger social support networks, higher self-confidence and a more optimistic view of life. 

And while you play, keep score! Keeping score isn't about following rules; it's about tracking your own achievement and appreciating your own accomplishments!

You should ultimately aim to get at least three power-ups, battle one bad guy and tackle one quest every day. And give yourself some points for your daily achievements in terms of their "determination" value. After all, determination is what counts as you move toward your goals.

So if you're trying to overcome depression, for example, you might give yourself +20 in determination for calling your doctor. The figure could also be +100 or +0.001 — the purpose is just to acknowledge and reward your accomplishment. Remember: playing SuperBetter is supposed to be fun! You deserve points when you play it well.

### 12. Final summary 

The key message in this book:

**Games are a lot more than just fun. They boost your confidence, strengthen your will and bring you closer to the people you play them with. SuperBetter can help you overcome major challenges if you play it right! So outline your adventure, identify your power-ups and bad guys and seek out allies to support and guide you. SuperBetter won't just take you where you want to go; it'll make the journey fun, too!**

Actionable advice:

**Get an avatar that looks like you!**

If you play a video game designed to boost your confidence, make your avatar looks like you. That reinforces the idea that the person playing the game is _you_, not some mythical creature. This will maximize the benefits of play!

**Suggested further reading:** ** _Reality is Broken_** **by Jane McGonigal**

_Reality_ _is_ _Broken_ explains how games work, how they influence our everyday lives and what potential they have to improve our lived reality. Full of examples of different game styles and their effects on gamers' dispositions, it not only offers a broad perspective on what games are but also shows how game designers can use them to solve some of the world's most pressing problems. To find these blinks, press "I'm done" at the bottom of the screen.
---

### Jane McGonigal

Jane McGonigal is a professional game designer and writer on the science of games. She was the first person to receive a PhD on the psychological effects of playing games. _SuperBetter_ is her second best-selling book.

