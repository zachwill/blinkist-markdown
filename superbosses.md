---
id: 576b9b1be0dd250003d6158b
slug: superbosses-en
published_date: 2016-06-28T00:00:00.000+00:00
author: Sydney Finkelstein
title: Superbosses
subtitle: How Exceptional Leaders Master the Flow of Talent
main_color: ED522F
text_color: BA4025
---

# Superbosses

_How Exceptional Leaders Master the Flow of Talent_

**Sydney Finkelstein**

_Superbosses_ (2016) is the insider's guide to understanding how charismatic, often controversial but unforgettable leaders tick. These blinks reveal the patterns and strategies of top-performing bosses, and explain how you can help your employees succeed by becoming a superboss yourself!

---
### 1. What’s in it for me? Find your inner superboss and help your employees succeed! 

If you've ever worked at a large company, you've probably heard coworkers bad-mouth the boss in confidence during breaks. But the reality is that being a good boss is very difficult.

Leadership sometimes means making tough decisions, and employees won't always agree with the decisions you make. It's just a fact that a boss is seldom liked by everyone in a company. Still, some bosses can inspire workers despite being tough or demanding.

Which unique qualities define a "superboss" and what can you learn from other superbosses out there? These blinks will explain everything you need to know to earn your "superboss" title.

In these blinks, you'll discover

  * how to be an "inglorious bastard" at work;

  * why Larry Ellison's managerial success is built on ridicule; and

  * how flexibility in the workplace has nothing to do with touching your toes.

### 2. Superbosses come in three shades: iconoclasts, inglorious bastards and nurturers. 

What does an iconoclast, an "inglorious bastard" and a nurturer have in common? These are all types of "superbosses." Each type has a strength, and each can inspire workers in different ways.

Let's start with _iconoclasts_. Their unwavering focus on their vision makes them easy to identify. An iconoclast's ability to teach and share is a knock-on effect of the type's unwavering dedication. In sum, iconoclasts inspire without even intending to!

Jazz great Miles Davis, for example, collaborated with young musicians to keep his creative juices flowing. While his goal wasn't necessarily to help younger talents improve, he still was known as a great teacher.

_Inglorious bastards,_ on the other hand, are characterized by their drive to win. They aspire to greatness and will do whatever is necessary to get there, even if it means pushing people out of the way.

To achieve such goals, however, inglorious bastards need a top-notch support team. Employees are pushed to perform at their peak, as failure is not an option. Through this, workers learn a lot.

Inglorious bastards can push their teams to extremes. Oracle founder Larry Ellison is known for _management by ridicule_. He scares his employees to push them to succeed! While such a strategy seems draconian, it works for Oracle. In fact, Ellison was recently nominated by the magazine _BusinessWeek_ as one of the industry's most competitive individuals.

And on the other side, _nurturers_ are bosses who care and want to see employees grow. We can also call them _activist bosses_, as they're always ready to guide team members to ensure they thrive.

Star restaurateur Norman Brinker was known for his nurturing management style. He genuinely loved watching employees improve; today some 18 of his former team members are heads of major restaurant chains.

So how can each type of superboss take your career to the next level? We'll find out in the next blinks.

### 3. Every superboss has a clear, bold vision, is brutally competitive and fears nothing. 

To run a nimble, successful business, a superboss needs three key qualities: _vision, fearlessness_ and _competitiveness._

Such qualities aren't just things a superboss picks up during a long career, but instead come naturally and are deeply part of who a superboss is.

Where you might see a problem or obstacle, a superboss sees a solution or opportunity to innovate. This ability is what makes a superboss a _visionary_ individual.

Fashion icon Ralph Lauren was a true visionary, former colleague Marty Staff recalls. When the two would have dinner together, they didn't talk business. Instead, they'd discuss ambitions and dreams. "He would construct this world and then he would simply fill in the blanks," Staff said.

Superbosses must also be fearless — certainly easier said than done. Many superbosses seem to live lives even outside the office that embody this trait.

Oracle founder Larry Ellison loves racing sailboats. Restaurateur Norman Brinker enjoyed playing polo (until he almost died during a match). Intel co-founder Robert Noyce would sprint through the streets of Bali during monsoon season. He often wore a jacket with the words, "No guts, no glory."

Finally, superbosses crave competition. They seek it out, create it and take advantage of it. Former American financier and philanthropist Michael Milken, known for his role in the development of junk bonds, is also remembered by his college roommate for his competitive streak. Milken would time himself while performing chores, seeking new ways to improve his performance and go faster.

These three qualities are fundamental to the worldview of a superboss. But to be a truly effective superboss, there's still more.

### 4. The magnetic charisma of a superboss is based on an individual’s integrity and authenticity. 

What makes some people charismatic individuals, while other personalities are easily forgotten?

Superbosses have a certain something that creates a vortex around them, a personality that draws people in, energizing them and inspiring them. This "something" is often _integrity_ and _authenticity_.

Superbosses demonstrate integrity by staying true to their core vision. They aren't afraid to stand up for their beliefs and give 100 percent to the cause.

Jorma Panula, a conductor at a Viennese music festival, has always rejected the vanity and ego-stroking of his industry, unlike many of his contemporaries. Instead, Panula is renowned for his unwavering focus on the quality of the music he creates and the listener's experience.

Authenticity is another trait that makes a superboss someone you'd like to become. They're not afraid to show their true personalities at work, nor do they feel the need to sell themselves in a certain way. Instead, superbosses allow you to get to know them on a personal level, in everything they do.

By being simply themselves, superbosses are memorable, likable and always one of a kind.

Tommy Frist, the founder of Hospital Corporation of America (HCA), is remembered by an employee as a family man. During business trips, he'd often write letters to his children in college. Frist was always eager to talk about his family, and it was clear that he was deeply proud of his children.

Even HCA's senior vice president recalls how Frist would drop by his office to discuss his son's football game that week!

Now let's investigate how superbosses operate in the office and with employees.

### 5. Superbosses are looking for employees who “get it,” demonstrating intelligence and creativity. 

Superbosses want the best for themselves, and that's especially true when it comes to recruiting employees. Essentially, a superboss is looking for people who "get it."

What does this mean? A superboss seeks out employees who are _intelligent, creative and flexible_.

Intelligence is a top priority for superbosses when recruiting, as a smart employee is a catalyst for effective collaboration, competition and creativity within a team. Members of an intelligent team can contribute equally to bigger goals, without the fear of "slower" coworkers dragging the group down.

Ralph Lauren was always on the lookout for employees with "fashion intelligence." For every worker, a keen sense of style was mandatory, no matter which department one worked for. Lauren even once appointed a runway model as head of women's design, because he said "she _got_ the clothes."

Creativity is another example of an employee "getting it." Superbosses aren't interested in employees who think in the same way that they do. They're stimulated instead by team members who can think laterally and tackle challenges innovatively.

Superbosses like Norman Brinker, Larry Ellison and American director Roger Corman all listen to prospective employees' ideas. During interviews, they expect to learn something new!

Flexibility is also necessary if you want to catch the attention of a superboss. This doesn't just mean being able to change your schedule at short notice. For superbosses, flexibility is the talent to solve problems amid unfamiliar situations. To show an employee just how important flexibility is, superbosses often assign a new hire to a job that isn't tied to the person's previous experience or education.

American cartoonist Bill Sanders, for instance, would move new employees into different departments occasionally. Director Roger Corman would ask new actors to take positions on his film production team.

If you truly "get it," you too will be hired by a superboss. But intelligent, creative and flexible employees aren't much use unless a superboss can motivate them to perform. So how do superbosses get a team to excel? Find out in the next blink.

### 6. Superbosses challenge and encourage employees to push harder and strive for more. 

It's one thing to have a team full of geniuses, and another thing to get a team to give its all. What would a superboss do in such a situation?

Well, a superboss isn't just interested in what an employee _can_ do. Superbosses want to know where an employee's boundaries are, and just how much further they can push them.

Working with a superboss means you'll never be able to rest on your laurels. No matter how good the numbers are or how well your department is doing, a superboss will continually raise expectations. Pushing the business forward and upward is a constant for superbosses.

Kenny Thomas, a former employee of Ralph Lauren, recalls how young designers would yearn to work with Lauren because of his ability to show them what they were truly capable of — often levels of excellence of which they had barely dreamed!

Employees don't just perform well because they're forced to, but because the sheer presence of a superboss encourages them to do so. Superbosses are always confident; this trait inspires and motivates employees. And as a result, employee self-esteem grows, which in turn boosts performance, too.

Former San Francisco 49ers football player Dwight Clark remembers how the supreme confidence of coach Bill Walsh was contagious, making all the team's players confident too. Walsh knew this; it was his tool to get his players to believe in themselves, despite any doubts they might harbor individually.

The superboss strategy for motivating employees has two sides. On one, a superboss pushes employees to their limits and beyond. Then, to sustain this, superbosses motivate people by allowing the confidence to rub off on a whole team. But when a superboss is involved, there's even more to teamwork as we will see next.

### 7. Boost teamwork by creating powerful bonds and nurturing a little competitive spirit, too. 

Running a business is like match racing, with one boat always trying to pull ahead of the other, split-second decisions often determining the outcome of the race. To win, you'll need a team that is powerfully connected, working as a single unit to drive the boat to victory.

So how do you get your team there? By combining professional bonds with emotional ties.

Superbosses encourage two types of bonds in the workplace. _Vertical bonds_ tie leaders to employees while _horizontal bonds_ help employees feel connected to each other. These bonds leave room for the power of friendly rivalry, a way to boost performance and productivity.

Saturday Night Live producer Lorne Michaels worked to create a sense of family on set using this bonding strategy. At the same time, he ensured that there was always some competition among employee ranks, by hiring more cast members than was necessary. Not everyone could regularly perform on the show, so cast members worked hard to perfect their sketches, lifting the show's overall quality in the process.

But how do superbosses ensure that competition doesn't instead tear team bonds? By basing the initial connections on genuine, emotional relationships. This is why employees are often still fond of former superbosses long after leaving a job. These sorts of relationships reflect well on superbosses, too.

By keeping tight relations with a team, a superboss is free to collaborate with former employees and support them as they seek new opportunities elsewhere. Talented employees who make a great impression in a new position help build a superboss's reputation as a "talent brand." This, in turn, makes the superboss's company attractive to other young talents, creating opportunities for growth.

Roger Corman's biographer wrote that the movie director would encourage former employees to "go shine elsewhere," confident that their success would shine on him, too!

### 8. Use the three questions of the “superboss quotient” to lead you toward becoming a better boss. 

By now you have a comprehensive understanding of what it means to be a superboss. Are you ready to become one yourself? Use the _superboss quotient_ to guide you in every professional move you make.

The superboss quotient is based on three questions, the answers of which reveal your superboss ability.

Let's start with the first question. Do you have a vision for your business that inspires you, and do you use this vision to energize and inspire your team?

If you don't have a vision, let's backtrack a little. Why do you work? What makes a workday satisfying for you? What's the importance of your work in the bigger picture?

It's vital that you learn to express your vision in one sentence. This sentence is what you should return to every time you feel yourself losing motivation, or feel your team needs a boost.

For the second question, ask yourself: Do team members feel close to each other, are they comfortable with one another? You can find the answer by observing how much time your team spends socializing with each other outside the office, or whether they keep in touch with people who leave the company.

Finally, the third part of the superboss quotient: how much time do you spend helping employees learn? Do you allow them to tackle challenging tasks, despite the risks it might entail? How much faith do you have in your team's success?

While these might be tough questions, you still need to answer them honestly. With an accurate understanding of how you guide your business today, you'll be better equipped to start leading it like a superboss tomorrow!

### 9. Final summary 

The key message in this book:

**Whether a superboss is an iconoclast, an inglorious bastard or a nurturer, this sort of business leader is known for having a clear vision, being fearless and embodying a competitive spirit. Their integrity and authenticity, as well as an ability to push employees to do their best, is what attracts the intelligent, creative and flexible talent a company needs to succeed. A superboss knows how to create a unified, caring team, too. Become a superboss and your business will benefit!**

Actionable advice:

**Take a superboss day!**

Next time you feel your team needs a boost, make one day a "superboss day," in which you invest extra effort in applying superboss principles. Either as part of a meeting or even a business trip, adopt one of the superboss types: a nurturer, iconoclast or yes, even an inglorious bastard. Consider this experiment like acting, and take the opportunity to discover which style works best for you and your team.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Talent Magnetism_** **by Roberta Chinsky Matuson**

In _Talent Magnetism_, author Roberta Chinsky Matuson shows you how to transform your workplace into an environment that draws top talent like a magnet. The book offers practical advice on how to develop a strategy to stay ahead of the competition by identifying how evolving technology and a new generation of workers have changed business in the twenty-first century.
---

### Sydney Finkelstein

Sydney Finkelstein is a specialist in business leadership and strategy and has published 19 books, including _Why Smart Executives Fail_ (2003) and _Think Again: Why Good Leaders Make Bad Decisions and How to Keep It from Happening to You_ (2009). Finkelstein is also the Steven Roth Professor of Management and faculty director at the Tuck School of Business at Dartmouth College.

