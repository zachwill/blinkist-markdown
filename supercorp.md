---
id: 5a00d872b238e100067eaf38
slug: supercorp-en
published_date: 2017-11-09T00:00:00.000+00:00
author: Rosabeth Moss Kanter
title: SuperCorp
subtitle: How Vanguard Companies Create Innovation, Profits, Growth, and Social Good
main_color: F9E732
text_color: 7A7219
---

# SuperCorp

_How Vanguard Companies Create Innovation, Profits, Growth, and Social Good_

**Rosabeth Moss Kanter**

_SuperCorp_ (2009) is an introduction to a new breed of business: the vanguard company. Find out how these companies are able to make healthy profits while simultaneously meeting societal needs and acting as forces for good in their communities.

---
### 1. What’s in it for me? Meet the vanguard company. 

Today, many people feel conflicted about their work. They feel they have to choose between a high-paying, reputable job and one that's altruistic, where they can make a real difference in the world.

Thankfully, a new type of company has emerged which combines both of these qualities. Vanguard companies such as IBM, Procter & Gamble and Banco Real are profitable businesses that contribute to community projects, environmental protection and products and campaigns that promote the universal values of trust, tolerance and innovation.

In these blinks, you'll learn

  * how big business and altruism can work hand in hand;

  * why investing in good causes is profitable for companies; and

  * how vanguard companies replace hierarchy with human relationships.

### 2. Corporations are seen as greedy and corrupt, but a new socially conscious business model is emerging. 

When you drive past a giant factory with clouds of toxins billowing from its smokestacks, you probably don't get a warm feeling in your heart.

These days, it's common for people to cite corporations as a primary cause for many of the world's problems.

On any given day, you can read headlines about the growing obesity epidemic in the West, food shortages in Africa, oil spills in the oceans and rampant corruption — all of which can be traced back to greedy multinational corporations led by immoral CEOs with a lust for money that surpasses any concern for the well-being of the planet.

However, a new generation of businesses is emerging. They're known as _vanguard companies_, and they have a socially conscious business model that is both financially savvy and eco-friendly.

Vanguard companies differ from non-profit organizations in that they don't exist solely to take on issues like clean water or public housing. Make no mistake, these companies are competitive and out to make a profit, but they use their strong business sense to find solutions for social issues.

For vanguard companies, being socially conscious is part of business practice. So they do more than simply make charitable donations, though this could also be a part of their business model. And this business model is especially popular with those born in the 1980s and 1990s, the so-called _millennials_. This generation is eager to prove that you can both earn a big paycheck _and_ make the world a better place.

This attitude isn't without its precedents. IBM has a long history of helping communities recover from natural disasters. In 2004, the company responded to the tsunami that killed nearly a quarter of a million people in Indonesia, Sri Lanka, Thailand and India. A group of IBM employees rallied to provide technology that would expedite the delivery of relief supplies and speed up the recovery process.

No one was forcing them to take action — they had the means to make a positive impact and simply saw it as the right thing to do.

> _"Vanguard companies have the power to contribute to the world in positive ways, and they want to do so."_

### 3. Vanguard companies can adapt to change by focusing on culture and keeping their employees motivated. 

You don't have to be a business analyst to know that this century has gotten off to a bumpy and chaotic start. There have been plenty of economic hiccups and hang-ups, the most obvious being the massive financial crisis of 2008, which spread from America and brought the global financial system to its knees.

Today, it's easy for economic crises to quickly spread across the globe. There are two major reasons for this: the rapidity with which technology continues to evolve and the increasing reliance of businesses on foreign suppliers.

However, vanguard companies have shown a remarkable ability to adapt to rapid changes and remain unfazed by a volatile and unreliable economy.

They accomplish this by focusing on company culture.

Let's look at the digital marketing company Digitas, which was originally based in Boston. Since the 1990s, they have specialized in helping large traditional corporations adapt to the digital era. At first, everything went smoothly. They received great capital support and expanded throughout the United States and abroad. A few years in, their shares were worth $40. But after 9/11 and the dot-com bubble burst of 2001, that price fell to a measly 88 cents per share.

Digitas's CEO, David Kenny, recognized how unreliable the marketplace had become, so he decided to focus on aspects of his company that _were_ reliable: its values and corporate culture.

To strengthen these aspects, Kenny issued a voice mail to his employees each week. These messages honestly addressed the instability of the market and how it affected the company, for example, if lay-offs had to be considered. In so doing, Kenny's voice mails built trust and helped connect the employees to the executives. They also did an amazing job of uniting the employees as a team ready to face adversity.

Meanwhile, Kenny built strong relationships with tech giants such as Microsoft and Yahoo, alliances that allowed Digitas to stay on the cutting edge of technological changes.

Kenny's intelligent maneuvering kept Digitas afloat during the tough times and kept the employees committed to the company's cause.

### 4. A vanguard company builds a thriving business by following its core values. 

Any company can write a high-minded mission statement. But saying that your company cares about the environment or the betterment of mankind isn't enough. As everyone knows, actions speak louder than words.

A vanguard company is well aware of this, and all of their actions are therefore based on positive values and core principles.

This can even turn a troubled business into a thriving vanguard company. That's what happened when the Brazilian bank, Banco Real, was experiencing stagnation and hired a new CEO named Fabio Barbosa. Barbosa began revitalizing Banco Real by giving it a new identity. He improved values and made social and environmental responsibility the focus of corporate culture.

The bank started taking action by improving its own local environment and cleaning up the alley along the side of its headquarters in Sao Paulo.

Alleys like these were a common problem in Brazil. Poverty and crime were often on dismaying display. So Banco Real's employees volunteered to turn the alley into a brightly lit garden with two kiosks that would offer employment to disenfranchised young adults.

Barbosa made sure that Banco Real's commitment to helping the community went further than updating its website or printing out some flyers announcing good intentions. The values became so fundamentally ingrained that they informed everything the bank did. And this is what vanguard companies do: they live according to their values and culture.

What's so amazing, though, is that this approach leads to a competitive advantage in the marketplace. When companies stick to their values, they're recognized as being trustworthy, which makes them attractive to customers and investors.

Indeed, in the years after Barbosa came aboard, Banco Real far outpaced its rivals. Between 2001 and 2006, profits steadily increased by 20 percent each year.

### 5. Vanguard companies prioritize being innovative through collaboration. 

Today's unpredictable economy, along with the ever-changing and competitive marketplace, has made innovation a business imperative. The company that stops cooking up new ideas will quickly fall behind.

But vanguard companies never feel pressured to innovate, since innovation is simply one of their core characteristics. After all, vanguard companies are committed to making the world a better place, a mission that goes hand in hand with constant creativity.

Let's look at IBM again. Naturally, part of a computer company's mission is to innovate — but this innovation can be social as well as technological. In 1994, they saw an opportunity to innovate in education _and_ develop the technology needed to do so. They began a partnership with K-12 public schools to help them tackle some of their everyday problems. For instance, the partnership produced a tool called _Watch-Me!-Read_, software that aims to help children improve their reading.

Procter & Gamble (P&G), a global leader in consumer products, is another vanguard company. However, there were years when the P&G branch in Brazil, which specialized in the company's feminine hygiene products, was only earning tiny revenues.

The path to fixing the problem was clear. They had a main product — the _Always_ brand of panty liners. They just needed to devise a way to cut production costs while simultaneously increasing output.

To find the solution, P&G Brazil began a process of collaboration across all of their departments as well as inviting input from their external partners, such as their trade partners and advertising agencies. Together, they came up with an innovative idea: the packaging would be transparent with colorful wrapping inside.

By eliminating the outer packaging, they greatly reduced the cost of ink per package and, sure enough, within a year, the product was earning far greater profits.

### 6. Vanguard companies have flexible workplaces with empowered employees. 

During the industrial revolution, efficiency meant establishing an authoritarian hierarchy at the workplace, which emphasized tireless work with little regard for individuality.

There are organizations that still operate under this model, but many businesses are now offering their employees more personal freedom.

At most vanguard companies, employees are allowed to find the work-life balance that fits for them. They're offered flexible schedules and the option to work from home.

This establishes a trust between the managers at vanguard companies and their employees. By providing them with more freedom, employers entrust employees with the responsibilities of motivating themselves, performing well and following the company's mission for social change.

Every day, 40 percent of IBM's workforce operates away from the office, with some working from a customer's home or office, a plane or from the comfort of their own home.

Not only does this flexibility ensure that employees feel comfortable and therefore able to do their best work; it makes the company an attractive place to work. If someone prefers the quiet of their home, they're free to work there. And if someone is a single parent, their flexibility will allow them to work around their child's schedule so they can pick them up from school or day care.

Many vanguard companies also empower their employees with decision-making authority, creating a more horizontal or flat chain of command.

They've discovered that progress can be hindered when employees have to wait for their boss to approve something. Having to follow this traditional structure can also cause employees to always only think about what's best for their manager instead of what's best for the company.

So vanguard companies will not only give managers decision-making authority, employees will also have the power to move things forward, make choices and speak to customers on behalf of the company.

> _"Building on the importance of meaning, a newer idea is that how one works is a matter of individual choice."_

### 7. Vanguard companies recognize diversity and the importance of diversity awareness. 

If you were to travel back to the industrial era, you'd find companies staffed with more or less one kind of employee. Back then, there was no sense of diversity in the workplace.

But we live in a globalized economy now, and vanguard companies know that a diverse workforce will help them better understand the cultures of the various nations they're operating in.

Vanguard companies embrace the fact that different kinds of people exist. They know it's important to understand the differences between people, because for example, that might mean understanding emerging social issues in a new market.

Of course, understanding a new environment before you enter it can help you avoid the risk of ruining your chances before you've established your presence.

Let's say your company wants to expand into India or China. A vanguard company would reach out to these communities and hire people familiar with the market before expanding. What they wouldn't do is send out an all-American envoy, thinking it's business as usual.

To ensure your company has diversity awareness, you can see to it that employees know to respect the differences that exist among different groups and individuals. Such awareness is crucial to a well-functioning vanguard company as it eases interaction and collaboration and makes it easier to function as a singular entity and move beyond cultural or social differences.

When Banco Real made its changes and became a vanguard company, they formed a "diversity committee" and held events that highlighted the differences between the employees that were already working there.

For example, at one event, all the employees wrote a descriptive adjective that best described them on a name tag. So some people were labeled "goofy" or "anxious," making it clear that each person was different and unique.

If you're hoping to take your company to the vanguard, you won't get far without an enlightened workforce that can take your business to those limitless heights.

### 8. Final summary 

The key message in this book:

**Making money and making the world a better place don't have to be in opposition with one another. This is what vanguard companies have discovered by placing socially conscious values at the center of their business plans. But they don't just say one thing and do another; every step they take is informed by their commitment to those values and to being a force of good in their community.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Good Works!_** **by Philip Kotler, David Hessekiel and Nancy R. Lee**

_Good Works!_ (2012) offers an insightful look into the way doing good can actually help companies prosper. Based on contemporary, real-life examples, it provides business-minded people with the tools and strategies they need to make a difference in the world and turn a profit at the same time.
---

### Rosabeth Moss Kanter

Rosabeth Moss Kanter is a professor at the Harvard Business School specializing in strategy, leadership and innovation from the perspective of social change. She is the former editor of the _Harvard Business Review_ and the author of several books, including _Confidence: How Winning Streaks and Losing Streaks Begin and End_.

