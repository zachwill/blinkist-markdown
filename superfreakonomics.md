---
id: 55d1d519688f6700090000ab
slug: superfreakonomics-en
published_date: 2015-08-20T00:00:00.000+00:00
author: Steven D. Levitt and Stephen J. Dubner
title: SuperFreakonomics
subtitle: Global Cooling, Patriotic Prostitutes and Why Suicide Bombers Should Buy Life Insurance
main_color: B2D35B
text_color: 5D6E2F
---

# SuperFreakonomics

_Global Cooling, Patriotic Prostitutes and Why Suicide Bombers Should Buy Life Insurance_

**Steven D. Levitt and Stephen J. Dubner**

_SuperFreakonomics_ (2009) explains why thinking like an economist can help us understand our modern world. These blinks illustrate key economic principles and the importance of collecting data with colorful stories from human history, and offers surprising solutions for the global problems that we face today.

---
### 1. What’s in it for me? The answer to society’s ills lies in the data. 

When you think about society's problems, whether global warming, terrorism or disease, does the lack of solutions make you angry? Surely there are enough clever people in the world to solve any issue, so why are they failing so spectacularly?

The uncomfortable truth is that many experts aren't looking in the best place for the answers: cold, hard data. Too many of them rely on the imperfect recollections and experiences of individuals as a basis for their theories, which leads to misunderstandings and mistakes. On the other hand, statistics are unemotional, concrete and clear. 

These blinks show you just a few of the unusual answers that lie in the data; reading them may change the way you approach problems and look for solutions.

In these blinks, you'll discover

  * why modern prostitutes get paid much less than 19th century ones;

  * why additional taxes helped increase the rat population; and

  * why the solution to global warming might be to pump more pollution into the atmosphere.

### 2. Statistics can give us brilliant insights into our world. 

Are you one of those people who gets irritated when someone around you leaves behind their rubbish? It might leave you wondering what it is that makes them behave so inconsiderately. You might even wish you could get into their heads and find out. 

Of course, that's a thought we often brush aside; we can't get into other people's heads. Even so, we _can_ change their undesirable behaviors. After all, governments and public bodies do this all the time, by creating incentives that reward us for doing the right thing. 

Unfortunately, incentive schemes rarely work out as planned. More often than not, they have rather damaging knock-on effects, putting the _law of unintended consequences_ into action. 

Take the introduction of volume-based trash pickup fees. This was supposed to be an incentive for people to produce less waste. Instead, people came up with creative ways to avoid the fee. In Germany, many people started flushing uneaten food down the toilet, increasing the rat population as a result. 

But if we could predict people's reactions to incentives before we put them in place, we'd save ourselves a lot of time and trouble. And how can we do this? By using statistics to get inside their heads. 

By collecting and analyzing data and statistics, we can begin to understand why people behave the way they do. The following blinks reveal some fascinating and surprising stories gathered by the authors to highlight the importance of statistics in understanding human actions.

> _"But to change the world, you first have to understand it."_

### 3. Think like an economist and you’ll unlock the secrets to society. 

In times of economic collapse and crisis, economists are dealt heavy criticism, and rightly so! And yet, economists, or at least those who think like them, can actually be a great help to society at large. 

Though we tend to associate economic thinking with unscrupulous attempts to increase profits, it actually has more to do with attempts to analyze the outside world. The foundation of economics is nothing more than making assumptions with the help of solid data. This allows economists to stay objective, and discern between typical behavior and exceptions. 

In the US, the summer of 2001 became known as the "Summer of the Shark." Media coverage of the underwater predators was high, particularly thanks to the story of an eight-year-old who lost one arm and a piece of his thigh in an attack. This gave the impression that sharks were more dangerous than ever. 

But with objective statistical analysis, it became clear that shark attacks that year were consistent with the average numbers from previous years — public awareness was simply higher than usual. 

In this way, thinking like an economist will bring you closer to the truth. But that's not all — it'll also push you to be innovative. Examining even the most intractable problem rationally, carefully and from all angles will lead you to completely new solutions.

In the early 1900s, horse-drawn carriages were the main form of transportation. The resulting abundance of horse manure was becoming a large and rather smelly problem. But with no way to make horses somehow produce less manure, this problem seemed unsolvable. 

Instead of focusing on making horses eat less, rational-minded, investigative researchers began to approach the situation from a new perspective. Instead of trying to reduce manure, they developed something that replaced the horse itself: the car!

In much the same way, these blinks will view various aspects of our society from different angles, starting with prostitution.

> _"When the solution to a problem doesn't lay right before your eyes, it is easy to assume that no solution exists."_

### 4. Statistics behind prostitution reveal several economic principles at work. 

Today, most businesses are run by men, evident in the wage gap that many women still face. But there's one business where women have always dominated: prostitution. By examining the statistics of sex work, we find some very interesting trends. 

One hundred years ago, prostitution paid a far better wage. The Everleigh Butterfly Girls, who worked at a famous brothel in Chicago around 1900, were able to make a very lucrative $430,000 per year. A modern sex worker could only dream of this wage. So what changed?

In times when premarital sex was uncommon, men frequented prostitutes more often than in today's more liberated society. Working at a brothel was also illegal, so the possibility of arrest, as well as the social stigma attached to prostitution, resulted in higher wages to compensate for the risks and drawbacks. 

But in recent years, more women are willingly entering sex work, while less men are prepared to pay for sex. With more supply than there was demand, prices, and in turn wages, have declined. 

Demand in sex work is also susceptible to short-term events. A two-year study into the matter revealed that sex workers make use of _price discrimination._ This means that women were charging higher prices to customers who were recognizably wealthier, as evidenced by nicer clothing or sophisticated manners. 

Findings also demonstrated the way that sex workers react immediately to an increase in demand. For example, prices rose 30 percent in one neighborhood during Thanksgiving, in response to an influx of clients visiting their families and looking for a little extra fun. Even those who weren't sex workers took the opportunity to make a little extra money in the holiday season! 

Given how they react to demand, sex workers aren't so different from department store Santas: when they see a chance to earn more, they will work overtime to make the most out of the short-term job opportunity.

> _"And as the demand for paid sex decreased, so too did the wage of the people who provide it."_

### 5. Economic thinking can help us catch terrorists before they attack. 

Terrorism is one modern threat that frightens us all, particularly because targets and victims are chosen at random. This is also what makes terrorism so hard to prevent. However, data analysis could be of some use to us in identifying terrorists before they take action. 

To understand terrorists, you have to know what motivates them. Alan Krueger, professor of economics at Princeton University, analyzed data about terrorists in Lebanon as compared to the country's general population; what he found out surprised everyone. 

Contrary to popular belief, terrorists are actually more likely to be well educated and come from a middle-class, affluent family. What drives them is not poverty and personal gain, but the willingness to perform a political act.

By developing a greater understanding of why and how terrorists operate, we can begin to detect them. But established anti-terror measures, such as the monitoring of suspect's conversations, tend to be quite inefficient. 

That's why Ian Horsley, whose name is changed here for security reasons, developed an algorithm that analyzes banking details in order to track possible terrorists. Originally engineered to uncover fraudsters, the algorithm uses indicators to pick out behavior that might pinpoint a terrorist. 

Positive indicators are characteristics that terrorists often have in common, such as renting rather than owning a house, or being registered as a student. Similarly, a negative indicator constitutes something terrorists wouldn't normally do, such as investing in life insurance, which obviously doesn't pay in cases of suicide. 

Although the algorithm isn't perfect, it may prove a valuable tool in detecting suspects who would otherwise have gone unnoticed. On the other hand, terrorists might now consider paying for life insurance, just to be on the safe side!

### 6. In reality, people are neither truly altruistic nor totally apathetic. 

In 1964, 28-year-old Kitty Genovese was stabbed to death in New York, on a day when, according to _The_ _New York Times_, the three separate attacks that culminated in Genovese's death were witnessed by 38 passersby, none of whom called the police. 

This case became renowned as a perfect example of _bystander apathy_, a phenomenon that describes the way we see something worrisome and do nothing about it, because we assume someone else will take care of it. Critics viewed this as a loud and clear reminder that society was selfish and indifferent to its core. 

And yet, just 20 years later, popular opinion had turned on its head. Everyone became convinced that society was fundamentally altruistic. During the 1980s, game theory, the scientific combination of psychology and economics, was a leading trend. One experiment in particular provided a much-lauded insight into society. 

The Dictator Game measured a subject's willingness to divide their money with a stranger. The results were remarkable. Players across various countries and diverse settings strongly favored dividing the money evenly, debunking the myth of the selfish and ignorant human. 

Or did it? This result didn't sit right with prolific experimental economist John List. He decided to conduct a series of adapted, more realistic Dictator Games to put society's altruism to the test. 

In List's version, people could steal money from the hypothetical stranger as well as give it. And before the game was conducted, participants had to complete some menial tasks, such as putting stamps on letters. Ultimately, only 6 percent of participants chose to share, while 66 percent took the money for themselves.

### 7. Some of the trickiest problems tend to have the simplest solutions. 

Everyone knows the frustration of facing a stubborn problem that won't go away, regardless of what you try; this is exactly what scientists experience every day. What do they do when solutions are nowhere to be found? They gather data to evaluate the case objectively, and pin down its causes. 

This approach may even save lives, as it did in a Vienna hospital in 1847. At the time, one in six healthy women who gave birth at the hospital were liable to catch the deadly _puerperal fever_. Ignatz Semmelweis decided to tackle this problem by gathering as much information as he could. 

He discovered that women delivering at home or at midwives' wards were far less likely to catch the fever than those delivering at the male doctors' ward. Then came the news of a male doctor's death after accidentally cutting his finger during an autopsy and becoming infected with cadaverous particles. 

Male doctors who conducted autopsies went directly to the birth station afterwards, where they transferred cadaverous particles to delivering women. The solution was simple — washing their hands!

Gathering data also helps outline problems that may not have been apparent. Solving these newly uncovered problems is what leads to innovation, and this is exactly what happened when Henry Ford began to work with car crash data in the 1950s. 

At that time, crashes were a major cause of death, killing 40,000 people each year. Ford hired Robert Strange McNamara to collect and analyze data, in order to make driving safer. 

McNamara discovered that injuries were largely due to damage to the passengers' heads, as they slammed into steering wheels and the windshield during a crash. As a result, many designers had experimented with making steering wheels softer. 

But McNamara used the data to look at the problem from a different angle — why not simply protect the head from flying around in the first place? The result was a cheap and easy solution that reduced the risk of death by up to 70 percent: the seat belt!

> _"The most amazing thing about cheap and simple fixes is they often address problems that seem impervious to any solution."_

### 8. The confusion around global warming makes it hard to tackle. 

Global warming is, or so we are told, one of the greatest threats to our existence. So why aren't we doing more about it? Global warming is a new phenomenon for which human activity is to blame, we just don't know how and to what extent. 

Because we don't know how significant our impact is and if this will lead to a catastrophe or not, it is hard to lead an objective public debate about it. And it doesn't help that global warming discussions are influenced by persistent myths. 

For example, cars and industry are always portrayed as global warming's main causes. However, the world's ruminants, especially cows, are actually responsible for 50 percent more greenhouse gases than the entire transportation sector.

So why not analyze more data to identify the factors really influencing climate change? Well, the reality is that global warming is very complex, and there are several indicators to consider. Moreover, climate scientists are unable to conduct experiments, which means they can't test which measures successfully minimize global warming. 

_Negative externalities_ also hinder our efforts against global warming. Negative externalities are impacts that are felt by many people, but _not_ the people responsible for them. For instance, if the ocean rises and submerges a small island in the South Pacific, our excessive meat consumption in the West — and the resulting greenhouse gases produced by cows — is largely to blame. 

Sadly, if you're responsible but don't have to deal with the consequences, it's unlikely that you'll change your behavior for the better. Still, initiatives such as Al Gore's film _An Inconvenient Truth_ or his Alliance for Climate Protection have tried to raise awareness with varying success.

As long as we have no incentives to change our behavior, we won't, which is why global warming is so hard to deal with. But there may be a quick fix — have you heard about it? Learn more in the final blink!

> Fact: In the 1970s scientists were nervous about global temperature changes. But what they were talking about was not global warming - it was global cooling!

### 9. Statistics show that we can fight global warming with more pollution, not less. 

People don't like to invest money to avert a future problem, because there is always a chance that a cheaper, fast solution might appear. In fact, there might be a fast and cost-effective way to stop global warming — it just seems a bit counterintuitive at first. 

To explain how it works, we have to look back to 1991, when the volcano Mount Pinatubo erupted. The huge eruption left a haze in the air that took two years to subside. Interestingly enough, the earth cooled off and forests grew more vigorously during this period. 

After considering this, Nathan Myhrvold, then chief technology officer at Microsoft and now a researcher at Intellectual Ventures in Seattle, had an idea: why not try to imitate this process by influencing the weather?

Geoengineering, or intervening in the global climate system, could be our ticket to reversing global warming. In 1992, a report from the National Academy of Sciences revealed that blowing sulfur dioxide into the stratosphere would cool down the planet — it's just important to pump it higher than industry does!

When strategically placed, 100,000 tons of sulfur dioxide pumped into the atmosphere each year would reverse warming in the Arctic and reduce it in the Northern Hemisphere. All we'd need to do is make use of existing power plants and use a hose-shaped device to pump their emissions into the stratosphere.

The haze cover produced would be called Budyko's Blanket, named after a Russian climatologist. This method would not only be cheap and easy, but also reversible if it doesn't work as imagined.

Yes, it does seem a little strange to fight the consequences of air pollution with more pollution, but the data indicates that it could work. Also, consider that this would only cost $250 million, which is $50 million less than what Al Gore's foundation spends each year on increasing public awareness alone.

### 10. Final summary 

The key message in this book:

**With the help of statistics, human behavior can be understood much more effectively. By gathering data, asking the right questions and staying objective, you can discover solutions to persistent problems and can help make the world a better place.**

Actionable advice: 

**You can never have enough data!**

The next time you're confronted with a problem that you can't solve, start working on it by freeing yourself from any assumptions about it. Are you convinced that your cat pees on the floor because it's a Wednesday? It seems unlikely. Start collecting as much data as possible to find the most important indicators. What do you do every Wednesday, and what does your cat do? What does it eat? Then, try to find a way to influence these indicators to find a solution to your problem. Perhaps the long hours you work on Wednesdays make your cat lonely, and it is trying to get your attention.

**Suggested** **further** **reading:** ** _When to Rob a Bank_** **by Steven D. Levitt and Stephen J. Dubner**

_When To Rob a Bank_ (2015) presents a collection of articles published on the Freakonomics blog at freakonomics.com, which has now been going strong for ten years. Honing in on the unpredictable and downright strange, Levitt and Dubner cover everything from why you should avoid anyone whose middle name is Wayne to why some of us should be having more sex than others.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Steven D. Levitt and Stephen J. Dubner

Steven D. Levitt is an American economist with degrees from Harvard University and Massachusetts Institute of Technology, and has specialized in researching crime and corruption. He currently teaches at the University of Chicago.

Stephen J. Dubner is an American writer and journalist who focuses on economic subjects. He is also the author of _Choosing My Religion_ (previously known as _Turbulent Souls_ ) and _Confessions of a Hero-Worshiper_.

