---
id: 559bca123966610007b40000
slug: surprise-en
published_date: 2015-07-09T00:00:00.000+00:00
author: Tania Luna and LeeAnn Renninger
title: Surprise
subtitle: Embrace the Unpredictable and Engineer the Unexpected
main_color: 73B8A5
text_color: 436B60
---

# Surprise

_Embrace the Unpredictable and Engineer the Unexpected_

**Tania Luna and LeeAnn Renninger**

_Surprise_ (2015) takes a closer look at the very concept of surprise, how it works and how to embrace and create it. What's more, the authors show us how surprise can keep our relationships flourishing and allow us to live life to the fullest.

---
### 1. What’s in it for me? Discover the exciting possibilities of surprise. 

If you are like most people, control and predictability are your friends. It's nice when things go according to plan, and quite unpleasant when your day is suddenly turned upside-down.

But did you know that embracing the unpredictable is actually linked to happiness, growth and improved connections with others?

These blinks explore the unpredictable, and the inner workings of surprise. By gaining a deeper understanding of what exactly surprise is and how it occurs, you can learn how to thrive in contexts of uncertainty and start creating surprises for yourself and others.

In the blinks, you'll discover

  * how to trigger the _duh face_ ;

  * why people contact psychics; and

  * the benefits of a starry sky.

### 2. Surprise is a feeling triggered by the brain when it fails to predict what will happen next. 

In a world rife with uncertainty, we often experience the sensation of surprise, whether it delights us or upsets us. But how exactly does surprise work?

Surprise is activated when your brain fails to predict what will happen in the next moment.

Your brain is endlessly whirring away, trying to predict what is about to take place; if something unpredictable happens, it immediately shifts into action.

This function of the brain actually has an evolutionary advantage: when faced with a hungry predator, our prehistoric ancestors needed the sensation of surprise in order to react as soon as possible, and not end up as dinner.

So what goes on in our brain when we're surprised? Interestingly, the surprise response consists of several stages — a _surprise sequence_.

Imagine yourself coming home to a room full of people yelling "SURPRISE!" The surprise sequence would go something like this:

_Freeze phase:_ in a fraction of a second, the surprise takes away your cognitive resources and brings all of your attention to the object of surprise, leaving you with the well-known _duh face_.

_Find phase_ : Your brain clambers through all the available information to figure out what on Earth just happened. In this case your brain realizes, "My friends gathered to throw me a surprise party!"

_Shift phase_ : Now your framework for understanding your circumstances shifts; the way you viewed your situation before has been altered by the surprise. You might start to think, "I won't have a typical evening by myself tonight, but instead I'll be partying with my friends."

_Share phase_ : The previous three phases are hard work for the brain, so in order to lighten the workload, you share the experience with others. In this case, this means talking to your friends about it.

### 3. In today’s world, people need both surprise and predictability. 

We live with uncertainty on a daily basis, but the world is also framed and structured, which causes a certain degree of predictability. So, what does this contradiction do to us?

The unreliability of our world can induce anxiety, which in turn creates a need for predictability.

In the neverending effort to predict what will happen in the future, our brains end up working overtime. It's hard, if not impossible, to make accurate predictions about important parts of our lives such as health, politics, climate and so on. This leaves a lot of us troubled by anxiety.

So what do we look for to ease our worries? More control and more predictability — and we look for this in all sorts of places.

One survey revealed that 69 percent of women and 39 percent of men had contacted a psychic. This is just one way in which many people try to regain some kind of control over their otherwise changeable or erratic lives.

But the opposite also leaves us wanting; the fixed routines of our everyday lives can trigger boredom. Most of us have somewhat structured lives, with most aspects of our routine under control. We know our work schedules and the exact place we need to go to. If we go somewhere new, we find out how to get there with our smartphones before heading out the door.

All of this control that we seek is also accompanied by boredom, bringing us right back to the need for surprise and unpredictability.

It's clear, then, that surprise works like a seesaw: we need some surprise, but not so much that we're consumed by unpredictability, and we need some stability without being bored out of our minds.

### 4. In order to embrace the unpredictable, you have to build resilience. 

As you know, not all surprises are pleasant. But did you know that you can improve the way you cope with unpleasant surprises?

Moreover, if you can become more resilient to negative surprises, your life can become far more enjoyable.

Resilience means being able to deal effectively with the negative surprises that life throws at you, such as events being cancelled at the last minute, or losing something important to you. This way, you won't feel forced to retreat to your comfort zone, where you run the risk of missing out on the thrill of positive surprises.

Studies reveal that resilient people are happier and healthier than their non- or less resilient peers.

So how can you build resilience? Start by setting a stable foundation, identifying your advantages, allowing for both struggle and success and making changes in direction.

First, set the foundation for your life.

For instance, a community can create a stable environment in which each member has trustworthy relationships with his or her neighbors. If anything undesirable should happen, such as a drought, they are all well placed to help each other.

Second, identify the advantages of negative surprises.

This technique helped one woman who began losing her hair when battling cancer. In her view, she now had the chance to get a stylish wig and find out how she looked with red hair.

Next, focus on positive goals and understand that struggle is actually a step toward success.

For example, as students solve easy classroom exercises, teachers can set more challenging goals. Reaching these goals will often be hard work, but when they eventually get there, they'll feel all the more triumphant.

Lastly, remember that negative surprise is an opportunity to change direction.

Take Natalia Paruz. She was a ballet dancer, but when she suffered a serious foot injury, she was forced to leave the world of dance and decided to head in a radically different direction: she picked up the saw as a musical instrument. Today she is one of the best-known saw players in the world.

### 5. By reframing vulnerability as openness, and trying not to avoid or predict surprises, you’ll be able to embrace uncertainty. 

What does being vulnerable mean to you? Many people associate it with weakness — but in reality, we can benefit from being vulnerable.

Vulnerability means being open and surrendering some control. If you can do this, unpredictability becomes easier to embrace.

Being vulnerable also implies being open to others and to life in general. Doing so can help you develop as a person, and makes you more likely to receive positive responses from other people.

For instance, the contemporary artist Raghava KK was set to deliver a TED talk. Prior to going on stage, however, he found the other speakers to be rude, cold and unapproachable.

But when he finished his talk, he was overwhelmed by the applause and positive attention he received. Raghava KK's explanation for this response was that he let himself be vulnerable. He was able to charm the audience because he gave them access to his most personal feelings.

By sharing stories from his life and exposing his mistakes and insecurities, he put himself in a vulnerable position. By reframing vulnerability as openness, and opening up to the unpredictable, he gained positive attention and admiration.

If you accept the inevitability of surprises, and try not to avoid or predict them, you'll be in a far better position to live a fuller, richer life. It's an effective way of being open to the uncertainty and ambiguity of the world.

And if you learn to be a bit more relaxed without knowing and controlling all of your circumstances, you won't feel the urge to constantly seek solace in your comfort zone, which itself can lead to missed opportunities. Staying outside your comfort zone leaves you open to the excitement and happiness that surprises can provide.

### 6. In order to become a surprise engineer, you have to design surprises that are delightful and create experiences. 

When was the last time a loved one left you an affectionate note, or you got a free coffee from the café you stop into every morning on your way to work?

These delightfully unexpected moments can positively impact both your own life and the lives of others.

We often use the word "delight" to describe pleasant and positive surprises. When delightfully surprised, we revel in sharing our surprise with those around us, which often benefits others.

One hotel manager named Eva decided to place a basket containing sweets in each of her rooms. The guests were delightfully surprised, and shared the experience with their friends and family. So, this simple gesture not only benefited the guests, as well as their friends and families who got to hear about a good hotel, but the positive word-of-mouth publicity was also great for Eva's business.

Little surprises like this can make all the difference. One study based on hotel reviews on TripAdvisor revealed that guests who experienced a "delightful surprise" at a particular hotel were 58 percent more likely to return there or recommend it to others.

Another way you can engineer surprises is by creating experiences.

Those who have _experiences_ created for them by other people are often positively surprised. To make surprises even more effective, and as a surprise engineer, you should aim to activate and play to the receiver's senses.

There are many fantastic ways to do this, one of which includes the use of themes.

Surprise maker Hannah Kane has designed many theme parties for adults. Her company _Everybody's Invited!_ creates party experiences around themes such as space travel and marshmallows. Kane says that each new theme inspires people to interact differently and allows them to explore unpredictable territory while really savoring the surprises.

### 7. By being open to differences and balancing levels of surprise, you can improve the quality of your relationships. 

Healthy relationships have a positive effect on our well-being, and if you let more surprise into your relationships, they will often flourish as a result. So how can you do this?

Welcoming difference and complexity are key steps in allowing surprise to emerge. Being open to complexity means not staunchly defending your personal opinions on culture, politics, religion and so forth. Instead, embracing differences will encourage the element of surprise in your relationships.

Research carried out by conflict scholar Roi Ben-Yehuda shows that people are more likely to keep healthy relationships, as well as be more tolerant and better at dealing with conflict, if they're open to complexity in their relationships.

But there is also the flipside: to improve your relationships, you need to have some degree of predictability as well.

In a romantic relationship, two partners will inevitably get to know pretty much everything about one another. This can lead to a feeling of _oneness_, the sensation that both partners combined equal one whole. But eventually, their desire for each other can fade, due to a lack of surprise or _otherness_.

According to psychologist Esther Perel, people indeed seek stability and a sense of unity with their partners, but they're more drawn to their partner when they surprise them and/or appear mysterious and confident.

Too much oneness in a relationship usually results in boredom and a lack of passion, as each partner is no longer able to surprise the other. But with too little oneness, people may not feel safe and confident enough to be their true selves.

So, maintaining enough oneness and otherness is a balancing act. But when it's done well, it tends to result in relationships that are mutually beneficial.

### 8. To surprise yourself, collect novel experiences and seek awe. 

Did you know that you needn't wait for the world to surprise you? You're perfectly capable of surprising yourself!

One way you can continually surprise yourself is by collecting novelty, that is, gathering experiences you've never had before. New experiences are by their very nature inherently unpredictable, so they're packed with surprises.

Research has shown that the brain prefers to focus its attention on novel stimuli rather than on familiar or preferred stimuli. This is because novelty activates dopamine, which gives us sensations of pleasure and reward. In addition, it sets off the find phase of the surprise sequence, in which the brain accesses all the relevant information needed to come to a conclusion about what just happened. Dopamine also triggers the shift phase, in which one's framework for understanding something changes suddenly.

So how can you sustain novelty in your life? Well, you could go and meet new people, buy some music you wouldn't normally listen to or sign up for that dance class you always wanted to check out.

In addition to new experiences, seeking awe will help you attract surprise.

Awe is a surprise that comes from experiences that we find hard to fathom; it seems unbelievable even though it's right in front of you.

It might take the form of a natural phenomenon, such as a glittering night sky or a vast meteor crater, or it may come from observing extraordinary ideas and skills, like watching a ballet or listening to an inspiring lecture.

Because awe is rare, deliberately venturing out in search of it will increase your chances of experiencing delight and surprise.

### 9. Final summary 

The key message in this book:

**Many of us try to avoid surprise due to the fear of relinquishing control. But this prevents us from experiencing life to the fullest. The truth is, you can benefit from both positive and negative surprises — all it takes is the right tools and mindset.**

Actionable advice:

**Don't be cool.**

Next time you feel like dancing or singing or any other activity that has the potential to make you look foolish, stop worrying about how others might perceive you and just do it. If cool is about having everything figured out, then it isn't attainable. Today's world is filled with uncertainty, which makes life a continuous learning experience. You should stay open to new and possibly even strange things — even if you look foolish doing so!

**Suggested further reading:** ** _Fooled by Randomness_** **by Nassim Nicholas Taleb**

_Fooled by Randomness_ is a collection of essays on the impact of randomness on financial markets and life itself. Through a mixture of statistics, psychology and philosophical reflection, the author outlines how randomness dominates the world.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Tania Luna and LeeAnn Renninger

A self-proclaimed _surprisologist_, Tania Luna is a co-founder and CEO of Surprise Industries — the only company in the world specializing in surprise. Luna has given a TED talk and is one of the leaders at LifeLabs New York.

LeeAnn Renninger holds a PhD in social psychology and is a co-founder and director of training at LifeLabs New York.

