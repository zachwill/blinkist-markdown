---
id: 595a2897b238e100069fb392
slug: survival-of-the-prettiest-en
published_date: 2017-07-07T00:00:00.000+00:00
author: Nancy Etcoff
title: Survival of the Prettiest
subtitle: The Science of Beauty
main_color: A43332
text_color: A43332
---

# Survival of the Prettiest

_The Science of Beauty_

**Nancy Etcoff**

_Survival of the Prettiest_ (1999) explores why people prefer things that are beautiful, revealing that our aesthetic tastes are not merely a matter of environment and culture. For instance, what we find beautiful has a lot to do with our innate desire for a strong and healthy child. And even three-month-old babies know beauty when they see it!

---
### 1. What’s in it for me? Dive into the evolution of beauty. 

When Eleanor Roosevelt, wife of President Franklin D. Roosevelt, was asked whether she had any regrets in life, she said that she wished she'd been prettier. A poignant statement, especially coming from one of the most accomplished, respected and admired women in history. Sad but true: people are obsessed with beauty. Why?

Well, we have a deep-seated biological drive to identify what is beautiful, and to desire it. You might be thinking: Hasn't humanity moved away from such biological biases? But here's the thing: even though we've become more aware of our biases, they still influence us; they are present from babyhood onward, and trying to ignore them often only serves to enforce them.

In these blinks, you'll find out

  * how even small babies have a preference for people classed as beautiful;

  * why small children are so cute; and

  * that beautiful people are more impatient than less beautiful people.

### 2. The sciences have neglected the study of beauty – a topic that deserves a closer look. 

Modern-day science seems to have an answer for just about everything. However, it hasn't had much to say on the subject of beauty.

In 1954, American psychologist Gardner Lindzey wrote the classic _Handbook of Social Psychology_, which became a standard reference book for the field. But on the subject of beauty, there's only one entry, covering "physical factors."

This aversion to the subject probably stems from failed attempts to find connections between physical attributes and behavior.

For example, Johann Kaspar Lavater's 1772 _Essays on Physiognomy_ tried to tie certain facial features to specific character traits — a project that modern science thoroughly debunked.

Furthermore, social scientists have historically shown very little regard for the subject of beauty.

The _Standard Social Science Model_ (SSSM), which greatly influenced social sciences in the twentieth century, may be to blame for this disregard. The SSSM viewed the mind as a blank slate that is shaped exclusively by environmental factors and social conditioning; biology doesn't come into play at all.

An influential book by Naomi Wolf, _The Beauty Myth_, makes arguments in exactly this vein. Examining the subject of beauty through a feminist lens, Wolf argues that beauty is a purely social construct, that it's used to uphold a patriarchal society and generate profits for cosmetics companies.

But this is a limited view of beauty, since it focuses solely on modern perceptions. It omits an undeniable fact: the story of the formation of the human mind covers more than ten thousand years of evolutionary history.

But oversimplifications are typical of the SSSM model, which misguidedly separates biology and culture and ignores beauty's true complexity.

Even when considering cosmetics, we can see this complexity at work.

Roger Bingham, a science reporter, makes a connection between biology and beauty rituals: He suggests that when women apply makeup to their cheeks to imitate a natural blush, they are signaling nubility, youth and sexual innocence — a mix of culturally and biologically valued attributes.

While there's no denying that beauty has existed throughout history, the question remains: What _is_ beauty exactly?

> _"No pair of happy young lovers… probably ever courted each other without many a blush."_ \- Charles Darwin

### 3. Beauty can’t be precisely defined, but people recognize it instinctually. 

If someone asked you to define beauty, what would you say? When Aaron Spelling, the creator of _Baywatch_ and _Melrose Place_, was asked this question, he said, "I can't define it, but I know it when it walks into the room."

That's because our ability to recognize beauty is probably an inborn trait.

A study by psychologist Judith Langlois provides strong support for this argument.

Langlois collected hundreds of slides of people's faces and asked adults to rate their attractiveness. She then showed the photos to babies ranging from three to six months of age. And the babies, of their own volition, spent significantly more time staring at the faces which the adults had also found attractive.

Men, women, babies, African Americans, Asian Americans and Caucasians all rated the faces similarly. The babies weren't just attracted to pictures of people who looked like mom and dad, either, ruling out the possibility that a baby's idea of the beautiful is determined by parental appearance.

Ultimately, the findings suggest that we are born with preferences and detectors for beauty.

Beauty is often defined by certain qualities that make the object of beauty appealing, as well as by the physical effect beauty has on the viewer.

For example, _The Oxford English Dictionary_ defines the word "beautiful" thus: "excelling in grace of form, charm of coloring, and other qualities, which delight the eye and call forth admiration."

And then there are the colloquial terms we use to describe beauty, which often contain a physical reaction, such as "knockout," "drop-dead gorgeous," "breathtaking," "stunner" or "bombshell."

While people may disagree on what the dictionary definition means when it says "grace and form," everyone can agree that beauty grabs and holds the attention. So Aaron Spelling was right when he suggested that we know beauty when we see it.

### 4. The ability to detect beauty is a survival mechanism. 

Humans enter the world with many biological imperatives. For example, we immediately like sweet foods because they provide energy, which helps us survive. Same goes for our sense of beauty: it promotes the survival of our species.

Our innate response to beautiful babies is crucial. Their soft skin and hair, large eyes, petite noses and chubby cheeks inspire tender, protective feelings in us because we've evolved to recognize such traits as signs of helplessness and vulnerability.

These feelings exist throughout the animal kingdom: Anthropologist Jane Goodall discovered that baby chimps were safe as long as their tails had a pretty white tuft of hair. She suggested that such characteristics are biological indicators, telling adults not to harm the baby chimps.

But beauty doesn't just protect babies; it also helps adults find high-quality mates.

Looking to the animal kingdom once again: Male peacocks have beautiful, multicolored feathers with an eye-like pattern, and the ones with the most "eyes" have more success mating than other males.

Alas, this beauty comes at a price. For one, it makes the peacock more visible to predators. It also takes up vital energy to fan out this beautiful display of feathers. But considering the biological drive to mate, it's worth the risk.

But why do females prefer the more flamboyant peacocks in the first place?

Research shows that males with more elaborate ornamentation seem to pass on a stronger set of genes. This has also been detected in barn swallows with longer decorative tails and stickleback birds with more colorful red throats.

On the human side of things, we also see this behavior in studies that show the marriage rates of girls coming out of high school: Results show that not only are the prettiest girls more than ten times as likely to get married as the less pretty ones; the beauties also tend to marry men with more education and income than they have.

But what are the qualities that attract us to a beautiful person? Let's find out in the next blink.

### 5. What we universally perceive as beautiful is a result of evolution. 

So, what exactly is it that we're responding to when we find ourselves attracted to beauty? After all, humans don't exactly have the colorful plumage of a peacock.

Studies show that humans likely have a universal set of sensual preferences informing them what beauty is.

Going back to that study on baby reactions: it was shown that the babies stared longer at symmetrical rather than asymmetrical patterns, preferred soft as opposed to rough surfaces and melodic music over dissonance. Of course, many people enjoy dissonant music, but this is usually an acquired taste, not something inborn.

These early, universal preferences are seen as being linked to health and safety. For instance, symmetrical faces and bodies can be an indication of health, fitness and protective strength.

The preferences can also translate into what we perceive as beautiful as we get older.

Just as we can tell when our food is spoiling from its appearance, so can we perceive a potential mate's health and fertility from physical indicators such as robust, glossy hair and smooth, unblemished skin.

For women, a combination of a narrow waist with wide hips is seen as an especially good indicator of fertility. Biologically, these are signs of high levels of estrogen, low testosterone and favorable reproductive capability.

And there is science to back this up: A study conducted by a Dutch artificial insemination clinic showed that women with an hourglass figure, and a waist to hip ratio below 0.8, had twice the rate of becoming pregnant than women with a ratio above 0.8.

This biological attraction explains the long history of narrow waists in women's fashion.

Corsets were in fashion for over 500 years, while modern versions of waist- and hip-flaunting fashions include crop tops and form-fitting dresses.

So, our ability to read physical cues and recognize beauty has it's biological advantages, but let's take a closer look at the sociological impact of beauty.

### 6. Beauty comes with many social benefits. 

If you've ever heard a story about a pretty woman charming her way out of a speeding ticket, then you're familiar with some of the ways beauty can influence society.

Even research shows that the beautiful are seen as inherently good.

For example, Karen Dion, one of the pioneers of attractiveness research, found that adults treat good-looking children much better than they treat unattractive ones.

Dion's experiment showed that adults who observed a beautiful seven-year-old stomping on a dog's tail were likely to give that child the benefit of the doubt: maybe he was having a bad day or hadn't been properly taught by his parents.

But when unattractive kids were seen doing the same thing, they were immediately suspected of being up to no good or viewed as juvenile delinquents.

This preferred treatment extends to the beautiful adults among us, who tend to be on the receiving end of generous acts.

In 1977, scientists conducted a social experiment. They left a dime in a phone booth, and on separate occasions they had a beautiful woman and an unattractive woman go to the phone booth and ask the person inside, "Did I leave my dime there?"

Results showed that 87 percent of the people returned the dime to the beautiful woman, and only 64 percent returned it to the unattractive one.

This biased treatment reinforces a certain cycle of behavior: beautiful people end up getting used to this special treatment, and, as a result, they develop a sense of entitlement.

In another study, participants were placed in an interview with a psychologist. During the session, the psychologist was interrupted so as to keep the participant waiting.

The attractive people would wait for an average of 3.3 minutes before demanding attention; less attractive people patiently waited around for an average of 9 minutes.

> _"It is amazing how complete is the delusion that beauty is goodness."_ \- Leo Tolstoy.

### 7. Combining the biological and social construct theories is the best way to combat our beauty biases. 

As the saying goes, beauty is in the eye of the beholder. But this raises a complicated question: who's doing the beholding?

We see images of beautiful people every day. But who has selected these images and what prejudices do they reinforce? Biological beauty may be indifferent to race, but prejudice and class struggles are continually reflected in beauty trends and media images.

Sociologist Harry Hoetink studied race relations in the West Indies in the 1960s and found that beauty standards were always modeled after the dominant population.

We continue to see this in Brazil where the inequality between the indigenous Indian and African populations and the white Portuguese settlers can be traced back to the 1500s. Today, only 40 percent of the citizens are white, but since this group remains rich and powerful, the pages of Brazilian magazines continue to feature mostly fair-skinned models.

This damaging beauty hierarchy continues because it is so pervasive and deeply rooted, and its effects are hardly limited to women.

The biological impulse to give preferential treatment to the healthy and beautiful applies to our children, too. Adults and parents not only treat attractive children better; a disproportionate number of unattractive children get abused and neglected.

To combat this trend, we need to combine our understandings of the social construct of beauty and our biological preferences.

After all, it's impossible to ignore our biological preferences, and why would we even want to?

If we deprived ourselves of beauty, we would remove a basic human pleasure — one of life's true joys. We'd overlook beautiful works of art and kids would just be tiny adults with annoying needs.

When we recognize our natural human response to beauty, as well as our social responsibilities to it and to one another — only then will we truly understand the effects beauty has on our perception of the world around us.

### 8. Final summary 

The key message in this book:

**What we find beautiful isn't just a societal construct; it's an evolutionary and biological development of our species. Beauty is indicative of health, youth and fertility, which are the optimal traits in a potential partner. To have a complete understanding of how beauty affects us and our society, we need to embrace an approach rooted both in biology and culture.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Beauty Myth_** **by Naomi Wolf**

_The Beauty Myth_ (1991) will help you understand the anxiety that surrounds women, and the cult of beauty that dominates our society today. It identifies the patriarchal and economic forces that shape the unfair expectations and norms that women face. Learn how we can be better equipped to build a less biased future.
---

### Nancy Etcoff

Nancy Etcoff is a faculty member at Harvard Medical School. She has a Master of Education degree from Harvard, a PhD in psychology from Boston University and has studied the brain and cognitive sciences at Massachusetts Institute of Technology.

