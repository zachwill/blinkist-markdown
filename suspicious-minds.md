---
id: 56eff9eea3f302000700004c
slug: suspicious-minds-en
published_date: 2016-03-23T00:00:00.000+00:00
author: Rob Brotherton
title: Suspicious Minds
subtitle: Why We Believe Conspiracy Theories
main_color: FED033
text_color: 806819
---

# Suspicious Minds

_Why We Believe Conspiracy Theories_

**Rob Brotherton**

Conspiracy theorists are everywhere. In fact, you might be one of them yourself! Have you ever questioned the official accounts of, say, 9/11 or the assassination of John F. Kennedy? _Suspicious Minds_ (2015) reveals why we look for extreme answers to tragic events and explains that there's much more to conspiracy theories than tinfoil hats and UFOs.

---
### 1. What’s in it for me? Find out how conspiracy theories arise and spread. 

What comes to mind when you think of conspiracy theorists? Reclusive weirdos with greasy hair who still live in their mother's basements? Antisocial, friendless people who live in a crazy world of paranoia?

Well, it's time to jettison such stereotypes.

These blinks show you why conspiracy theories aren't confined to a handful of oddball outsiders. Indeed, conspiracy theories are all around us; you might even be a conspiracy theorist yourself!

In these blinks you'll find out

  * why we're all prone to conspiracy theory;

  * why conspiracy theories can actually be harmful; and

  * why the Illuminati are believed to be the root of all evil.

### 2. Conspiracy theories are the result of unanswered questions and our own natural instincts. 

Maybe someone has knowingly told you that 9/11 was an inside job planned by the US government; or that climate change isn't real; or that Elvis is still alive and working at a gas station in Kentucky. These are just a few of today's popular conspiracy theories. But have you ever wondered where these strange stories come from?

Conspiracy theories like these are the result of unanswered questions.

We're attracted to conspiracy theories because they provide an explanation to a question that has been left unresolved. Or maybe the question has been given an official answer, but there is still contrary evidence that hasn't yet been properly addressed.

For example, many popular conspiracy theories of our day revolve around the events of 9/11 and unanswered questions such as: Did Al Qaeda orchestrate the attack or did the US government need a reason to start a war in the Middle East? Or, was Osama Bin Laden really killed or is the CIA keeping him alive somewhere in Washington, DC?

Conspiracy theorists take questions like these and then furnish unconventional answers: Bin Laden died of natural causes soon after 9/11 but the US government covered this up in order to invade Iraq and Afghanistan.

It's natural to think that conspiracy theories like these are simply crazy. However, having such misgivings is also a natural human instinct.

You may have heard a conspiracy theorist's voice in your own head from time to time. Perhaps you've asked yourself: How can global warming be real when it's been so cold these past few years? Now just add some natural suspicion about the government and suddenly that thought becomes a conspiracy involving evil scientists trying to control world politics!

> Approximately 40 percent of US citizens believe that climate change was invented by scientists and the government as an excuse to infringe on civilian rights.

### 3. Conspiracy theories date back to ancient Roman civilization. 

People often link conspiracy theories to the advent of the internet. But they've actually been around for millennia.

Conspiracy theories can be traced all the way back to the Roman Empire.

People's love of gossip and questioning the official version of events goes back to 64 AD. In that year, a fire destroyed two-thirds of Rome, and conspiracy theorists asserted that Emperor Nero had planned the fire — that he'd dressed himself in a theatrically tragic costume and sung a funereal song, watching while hundreds of people burned.

Conspiracy theories continued into the 1700s, when a small, secret society called the Illuminati gained notoriety throughout Europe.

Founded in Bavaria in 1776, the Illuminati was started by philosopher Adam Weishaupt. Members were sworn to complete secrecy, a practice that inspired many conspiracy theories. Rumors started circulating. It was said that the Illuminati were the puppet masters of world events, that they'd precipitated the French Revolution.

But before long the group was disbanded and its members were hunted down and prosecuted. So, in reality, the Illuminati was destroyed before it became powerful enough to influence anything.

But this hasn't stopped conspiracy theorists around the world from keeping the Illuminati alive. The control of US government, the assassination of John F. Kennedy, the spilling of oil into the Gulf of Mexico — whatever it is, conspiracy theorists love to blame it on the Illuminati.

### 4. Conspiracy theories can cause massive damage and kill millions of people. 

While it can be fun to discuss conspiracy theories, the truth is that they can also have deadly consequences.

In the Middle Ages, for example, some conspiracy theorists blamed the Jews for the Black Death.

Between 1346 and 1353, 60 percent of the European population fell victim to the bubonic plague — commonly referred to as Black Death. Devastated and disoriented, survivors sought a scapegoat. Conspiracy theorists pointed to the Jews, and since they were already forced to wear yellow cloth for identification, they were easy victims. Many Jews were hung, burned alive or brutally killed — all because people believed them to be responsible for the plague.

The nineteenth century likewise nurtured its own theoretical boogeymen: the _Elders of Zion_ conspiracy theory.

This theory suggested that a small group of Jewish leaders, called the Elders of Zion, had conspired to influence global prosperity, spark wars and create a world run by Jews. Details and protocols of these meeting were even published in order to convince people of the conspiracy theory. World War I and the Great Depression, like the Black Death, inspired people to pinpoint a scapegoat. And so the devastation of war and poverty was blamed on the Elders.

As a result, Walther Rathenau, a Jewish-German politician, was assassinated by conspiracy theorists. Adolf Hitler even stated, in his book _Mein Kampf_, that the Elders of Zion theory was part of the rationale behind his attempt to exterminate the Jews.

### 5. Belief in one conspiracy theory leads to belief in many – even if they are contradictory. 

So, what do you think? Is Osama bin Laden still alive? Or did he die, from natural causes, just months after 9/11? 

Strangely enough, people who take issue with the official story — that bin Laden was killed by US troops in 2011 — are willing to believe either one of those conspiracy theories, even though they contradict each other.

This is what's called the _conspiracy mindset_ : the willingness to accept any story that goes against the mainstream explanation.

This mindset can even make room for competing theories — for instance, that Bin Laden died shortly after 9/11 or, alternatively, is alive and tucked away somewhere in Washington, DC. Once someone with the conspiracy mindset begins to doubt the official version of events, all other explanations start to become plausible.

Of course, most people don't believe that evildoers are lurking around every corner. Some people react negatively to any sort of conspiracy theory. But the majority of us fall somewhere in the middle: we don't believe that aliens are taking over the world, but we might be suspicious of events surrounding the deaths of Princess Diana or John F. Kennedy.

After all, there's no need to protect our thoughts from the government by wearing tinfoil hats.

Or is there!?

> Around 12 million people in the United States believe that the world is ruled by shape-shifting reptiles.

### 6. Conspiracy theories – like popular books and movies – follow basic storytelling templates. 

Some of the best stories feature a hero fighting against an evil force and saving the day. You can see this throughout history. Just think of _Harry Potter_ or _The Lord of the Rings_, or even the classic tale of _Beowulf_.

In fact, most of our popular tales follow a few basic storylines. 

Even though millions of different stories have been told — in movies, books and around campfires — they're not all that different from one another. Each one might seem unique and fascinating, but they're all just variations on the same basic storylines.

For example, _Romeo and Juliet_, the tale of two young, star-crossed lovers and their tragic end, has been repeated innumerable times. And have you noticed how the story of _Harry Potter_, about a young underdog who fights a much more powerful evil wizard bent on world domination, is very similar to _Star Wars_?

So, as you may have guessed, conspiracy theories work in a similar way.

Even though coincidences happen every day, our brain likes to think in terms of cause and effect, giving meaning to the events that led up to an incident. So, when tragedy strikes, in, for instance, the form of a school shooting or the assassination of the president, we try to find complex reasons for it. Our storytelling instinct kicks in. 

Conspiracy theories often fill in the gaps with a good-versus-evil tale that fits the enormity of the occasion. As with popular stories, these tales often feature an underdog fighting to uncover the truth and save the day. 

For example, some people refused to believe that it was a small group of rebels that assassinated Archduke Ferdinand and set the events of World War I in motion. It made more sense for conspiracy theorists to suggest that the assassination was orchestrated by an evil syndicate out to destroy the world.

### 7. Conspiracy theorists are biased and want to find a motive behind every event. 

It's natural for human beings to want to know what is happening around them. What is slightly more curious is the desire to create far-fetched and even ridiculous explanations for situations we don't understand.

While most humans seek to understand why something happened, conspiracy theorists take it to another level, finding hidden motives everywhere.

Cause and effect is crucial to our understanding. If a chair moves, you need to know what pushed it or else you'd be truly frightened. So, when, say, a plane crashes into the ocean, it's only natural that we try to understand how it happened.

But conspiracy theorists often look at mundane explanations as unacceptable. The plane didn't crash because of an electrical failure or bad weather; no, the cause must be as horrific as the event. And so conspiracy theorists come to believe that some evil group conspired to bring down the plane.

Once a conspiracy theorist comes up with a reason for a tragic event, he stubbornly clings to it.

For example, if you've ever tried discussing climate change or 9/11 with someone who believes them to be the result of an elaborate conspiracy, then you probably know how difficult it is to change his mind. No matter what you say to someone who believes in a conspiracy theory, he'll find a way to twist that argument and make it fit their worldview.

But, actually, we _all_ do that. Once our brains lock onto a belief, it can be near impossible to convince us otherwise.

We all have the ability to make new pieces of evidence support what we already believe. Conspiracy theorists can therefore take any new evidence regarding 9/11 and simply add it to the list of reasons that the government obviously _did_ do it. Because of this, conspiracy theories often become more and more outlandish.

### 8. The Final Summary 

The key message in this book:

**We are all conspiracy theorists. We can't help creating stories about how good and evil are in a constant struggle to rule the world, a propensity that leads to conspiracy theories. We find comfort in these stories, which allow us to understand tragic events not as uncontrollable accidents but, rather, as the work of evil governments and secret societies.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Why People Believe Weird Things_** **by Michael Shermer**

_Why People Believe Weird Things_ provides an overview of the most common pseudoscientific and supernatural theories. It'll teach you why so many people believe in them, why they're wrong, and what methods proponents of pseudosciences use to assert their incorrect theories. It also offers both rational arguments _for_ science, and rational arguments _against_ pseudoscience.
---

### Rob Brotherton

Rob Brotherton is a writer and academic psychologist. He is an Adjunct Assistant Professor at both Barnard College and Columbia University, and specializes in the psychology behind conspiracy theories.

