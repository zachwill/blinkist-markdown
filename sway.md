---
id: 546bc8063435300008220000
slug: sway-en
published_date: 2014-11-19T00:00:00.000+00:00
author: Ori Brafman
title: Sway
subtitle: The Irresistible Pull of Irrational Behavior
main_color: FFCC33
text_color: 806619
---

# Sway

_The Irresistible Pull of Irrational Behavior_

**Ori Brafman**

_Sway_ sheds some light on the mysterious forces that cause humans — usually rational and logical — to behave strangely or act without thinking. In short, the book explains why we often act _irrationally._

---
### 1. What’s in it for me? Learn how to prevent the mistakes you never knew you were making. 

The brain is a peculiar organ. Often when we think we're making a rational decision, based on logic, it turns out that instead, we're really acting irrationally.

The worst part is that you don't even know when you're doing it. There are several forces at play, in your mind and in your environment, that can make _you_ your own biggest risk.

These blinks will help you understand what factors are at play that lead you astray from logical decisions, and how they work. And with this knowledge, you can get a better sense of how to avoid the pitfalls of acting irrationally.

In the following blinks, you'll also discover:

  * why Russians will do anything to keep a contestant from winning on the television show _Who Wants to be a Millionaire_ ;

  * why you keep buying extra car rental insurance that you don't really need; and

  * how having negative thoughts about aging will actually make you older.

### 2. Nobody likes to lose; so to avoid a feeling of loss, we often act irrationally. 

We like to think of ourselves as logical, rational beings. Yet the truth is that all humans behave irrationally. Even experts, such as airplane pilots and surgeons, can make grave mistakes.

But what motivates us to act irrationally?

In short, nobody likes to lose. Our fear of loss, or _loss aversion,_ is one of the hidden forces that drives irrational behavior.

Interestingly, the pain of loss is a much stronger feeling than the joy of winning; so when faced with the prospect of losing, we will do anything we can to avoid it.

Our sensitivity to changes in the price of an item illustrates this crisis. As an increase in price feels like a loss, we react to this much more intensely than we do a decrease in price.

U.S. Department of Agriculture professor Daniel Putler studied egg sales to see how people reacted to price increases and decreases. He found that when prices decreased, people only slightly increased the number of eggs they purchased. However, when prices rose, people cut their consumption of eggs by _two and a half_ times.

Since a price increase feels like we're losing money, we react disproportionately strongly to the change.

Putler's study shows how we often rush to make sacrifices just to avoid loss. Sometimes, we're even willing to pay _more_ to prevent a loss.

If you've ever rented a car, then you've experienced this firsthand. Rental car damage waivers can be expensive and are in fact useless, as your own car insurance and credit card offer sufficient protections should an accident actually occur.

Yet the fear of loss makes you reconsider whether you are safe enough, and may well encourage you to sign up for the coverage regardless — a product designed specifically to take advantage of customers' irrational assumptions!

### 3. When we’ve got even more to lose, we don’t sober up but instead act more irrationally. 

Fear of losing is a major driver of irrational behavior. Yet when the stakes are high and we stand to lose significantly, we can act even more irrationally.

Paradoxically, when we're so eager to avoid a big loss that we'll do whatever it takes to avoid it, we often cause _more_ damage than we originally feared.

Investment strategies can often encourage irrational behavior. For example, a client decided to invest all his assets in a particular biotech company. When the price of the company's shares dropped from $47 to $38, the client refused to sell, saying he'd only do so when the price returned to $44.

The client was chasing a loss — meaning he was ignoring the reality of his situation and trying to recover his loss, no matter what the cost.

The result? The company's share price plummeted to 12 cents, and the client lost nearly everything.

While we'd like to think that the more committed we are, the more cautious we are — in fact, the opposite is true. The more we have on the line, the more irrational we become.

So what causes this, exactly?

Commitment to an idea often makes it hard to let go, even when that idea clearly isn't working.

Let's look at a particular example in American football. The Southeastern Conference of the U.S. college football league was once characterised by a conservative, defensive style of play.

University of Florida coach Steve Spurrier changed that. He transformed his team's strategy into an offensive one, and Florida became a winning team. Interestingly, no other coaches in the conference changed their tactics to try to beat this new method; despite losing game after game, the coaches were too committed to old methods to take the risk.

So our fear of loss leads us to act irrationally, sometimes overwhelmingly so. Yet what else can trigger such behavior? Find out in the next blinks.

> _"The deeper the hole they dig themselves into, the more they continue to dig."_

### 4. A great idea from a friend we might think is genius; a great idea from a foe, not so much. 

First impressions count. And indeed, whether it's a new pair of shoes, a new colleague or a new social situation, our first impression will almost definitely shape our later perceptions.

One major component of this phenomenon is _value attribution_ : our tendency to bestow certain qualities upon people or things based on the circumstances under which we first experienced them.

One such circumstance is the specific person or people involved. For instance, if we first hear of a business idea from someone we don't like, we may consider the idea a bad one — worse, that is, than if we had first heard the idea from someone we actually like.

In this way, that person's value — in this case their likeability — is attributed to our first impression of their business idea.

This effect was illustrated in 1916, when Nathan Handwerker started a hot dog stand on Coney Island in New York. Though his hot dog prices were less than half of those of his competitors, he still struggled to attract customers.

Handwerker came up with a clever solution: he hired doctors, wearing recognizable white lab coats, to hang around his stand and eat hot dogs.

Potential customers recognized that respected, knowledgeable doctors were also Handwerker's customers. Thus they'd attributed value to Handwerker's product, prompting as a result high demand for his hot dogs.

Value attribution also influences our judgment even in terms of entertainment. As part of a study at Ohio State University, 60 people purchased theater season tickets, yet the tickets' cost for each customer was randomized. Twenty people paid full price at $15; 20 people received a $2 discount; and the last 20 received a $7 discount.

In the end, those who paid full price for their season tickets actually attended _more_ shows than those who received a discount.

This is because those who purchased cheaper tickets assumed that the productions, as they were discounted, must have been inferior. This made the season ticket holders attribute less value to the shows, and in turn, they found them less enjoyable.

### 5. We often judge our partners on intuition, ignoring objective data; it’s why love is blind! 

Most people think they're a good judge of character. But how objective or reliable can our evaluations of other people really be?

To study this, a professor at the Massachusetts Institute of Technology hired a substitute teacher to run his class. Before doing so, he gave his students two different biographies of the substitute. One half of the class read that the substitute was "a warm person," while the other half learned that he was "cold."

Afterwards when the students were asked to assess the substitute, the two groups offered very different judgments. Those who read that the teacher was "warm" found him "good natured, considerate of others and sociable." The other group, however, thought he was "self-centred, unsociable, irritable and humorless." Same teacher, same class — yet two totally different viewpoints.

In the same vein, a Canadian study of college freshmen shows how intuition can blind us to the plain facts in front of us.

Students in a new relationship were asked about the quality of their relationship, and how long they thought it would last. Most were able to identify a few potential problems, but were regardless optimistic that their relationship would last for a long time.

A year later, researchers discovered that less than half of the students were still in the same relationship. While the students originally had correctly identified the problems that would later result in a breakup, they had regardless ignored those facts when estimating their love's longevity.

What's more, when researchers looked at how optimistic the students' roommates and parents had originally been about the relationships, they found the predictions of these groups to have been far more accurate. They had identified the same problems, but could consider them objectively without love's rose-colored glasses!

> _"A single word has the power to alter our whole perception of another person."_

### 6. We perform better when we’re told we’re winners; we often fail when we’re told we’re losers. 

Do you feel more attractive when a lover tells you that you look great? Or do you feel self-conscious about your weight if a friend suggests that you should work out more?

It's true that we are much more likely to take on a certain characteristic once we've been labelled with it. Doing so can can impact us either positively or negatively.

The _Pygmalion effect_ explains this effect in the positive: when expectations of us are higher, we perform better.

As an example, Israeli soldiers participated in a commander training program, in which they were told before the program started that they had been ranked: high, regular or unknown command potential.

Yet the rankings were simply made up. However, the soldiers who were told they were part of the high command potential group performed noticeably better — they had involuntarily assumed their superior ranking to the rest and acted on it.

But what about the other soldiers? They may have fallen under the _Golem effect_, in which a person takes on the negative traits with which they are labelled.

The Golem effect can have just as great an impact as its corollary, the Pygmalion effect. In fact, even having negative _feelings_ about aging can actually make people age faster.

Yale researchers witnessed this effect as part of a study at a retirement home. A group of senior citizens were first given a hearing test; then, they were asked to list five words that came to mind when they thought of old people.

Responses ranged from the positive (compassionate) to negative (feeble) and from external (white hair) to internal characteristics (experienced).

Three years later, the researchers conducted the test again. While the average score on the hearing test had declined as expected, scores curiously declined much more for those seniors who described old age as something external and negative.

The participants' perception of age was actually making them suffer the effects of aging more intensely!

### 7. What is fair play and what is not is often culturally defined, and is also often irrational. 

Whether playing sports, angling for a job promotion or winning the lottery, most of us feel that a sense of fairness is a crucial element in our interactions.

But bizarrely, in our insistence for fairness, we sometimes behave unfairly.

The behavior of audience members in the international television show, _Who Wants To Be a Millionaire_, is a curious example of this.

As part of the show, a contestant may ask the audience to help them with a question. This is what a French contestant chose to do, after being stuck on a surprisingly easy question: What body revolves around the earth — the moon, the sun or the planet Mars?

The French audience offered their "help": Two percent voted for Mars, 42 percent voted for the moon, and 56 percent voted for the sun.

Do you think the French don't know that the moon revolves around the earth? Of course they do!

What happened was that the audience upheld their idea of fairness: that is, if the contestant really didn't know the answer to such a basic question, then he doesn't deserve to win, and it wouldn't be fair to help him, either.

Fairness in certain cultures does appear to vary, however. Data shows that American audiences tend to help a contestant out, regardless of her abilities, as some 90% of answers given by the audience are correct.

In Russia, however, it's just the opposite. Russian audiences tend to offer incorrect answers, no matter how the contestant is doing.

### 8. We are more likely to accept an outcome, even if negative, if we feel the process was fair. 

We know that our behavior is heavily influenced by our perception of fairness.

But that's not all. Our satisfaction with the outcome of negotiations, for example, is also dependent on how fairly we feel that outcome was reached.

A study in Berlin isolated two people in separate rooms. One person received €10 and was told he could decide how to split the money with the other person. The second person was then asked if she would accept the split. If yes, the money was split accordingly; if no, neither would get a cent.

When the second person considered a suggested split unfair, she rejected it. So even though some money technically would be better than nothing at all, the perceived unfairness of the offer left her so dissatisfied with the outcome that she declined.

Interestingly, when subjects were told that their partner was a computer, they often accepted even the unfair offers that they probably would have rejected if from a human!

Why? Because a computer knows nothing of fairness. In its offers, it wasn't deliberately being unfair, so for the human partner, the offers weren't as insulting.

So what makes us feel that something is fair?

One major component of fairness is being able to voice an opinion. This was demonstrated in a study where researchers asked hundreds of felons to fill out a survey about the fairness of their trial.

In general, felons who received shorter sentences thought their trials were fair, while those given longer sentences didn't.

But, more important for all the people surveyed was the amount of time they were able to spend with their lawyer. Doing so helped them feel that they had a voice, which made the whole ordeal seem fairer, regardless if they received a longer sentence in the end.

### 9. Work to curb irrational behavior by always thinking about the big picture. 

You're driving to an important business meeting when suddenly, you get a flat tire. You quickly change the tire and get back on the road, but you're now running late. You remember a shortcut off the highway that could save some time, but you've never taken it before.

What do you do?

Typically, our first impulse would be to take the shortcut, as our irrational loss aversion comes into play. However, if we consider the big picture, it would be best to stay with the familiar route.

The familiar route you know will get you there, and in the long run, being 15 minutes late is not a crisis. Yet if you take the shortcut, you could make up time, but you could also get lost and miss the meeting entirely.

This scenario shows how important it is to put aside past mishaps and focus on the bigger picture.

On an organizational level this also holds true. In 1985, _Intel_ faced a serious competitive threat from Japanese high-quality but low-cost memory chips. As the company's main business was memory chips, it was difficult to assess the situation objectively.

Yet at the time, CEO Andy Grove and co-founder Gordon Moore realized the importance of looking at the big picture, from the present-day moment.

They asked themselves, what would a new board of directors do if they were hired today? The answer seemed straightforward: Get out of memory chips, no matter how painful the decision.

So Grove and Moore did just that. They refocused the company on microprocessors, and it was the right decision; Intel today is a huge success story in the business of global technology.

So whether you've just made a mistake or are facing a legacy of historical behavior, you need to always be willing to forget the past and think of the big picture, as you see it today.

### 10. Final summary 

The key message in this book:

**We're not as rational as we think we are. In fact, several unconscious factors contribute to our irrational behavior. We have an irrational fear of loss and allow our first impressions to dictate our views of others.**

Actionable advice:

**Take a look at problems with fresh eyes.**

Sometimes our past influences us more than we realize. We may, for example, do our job in a certain way, just because that's the way we've always done it. Habits like this breed inaction, and keep us from even considering a change. In a case like this, try to imagine you've just been hired and consider what you would think of your work habits from this new, fresh perspective.

**Suggested** **further** **reading:** ** _Predictably Irrational_** **by Dan Ariely**

_Predictably Irrational_ explains the fundamentally irrational ways we behave every day. Why do we decide to diet and then give it up as soon as we see a tasty dessert? Why would your mother be offended if you tried to pay her for a Sunday meal she lovingly prepared? Why is pain medication more effective when the patient thinks it is more expensive? The reasons and remedies for these and other irrationalities are explored and explained with studies and anecdotes.
---

### Ori Brafman

Ori Brafman is a renowned organizational expert and regular consultant with _Fortune 500_ corporations. Rom Brafman is an award-winning teacher. Originally from Israel, the two brothers are now based in California.

