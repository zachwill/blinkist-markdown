---
id: 516bbffde4b01b686deb9b1c
slug: switch-en
published_date: 2013-08-10T00:00:00.000+00:00
author: Chip Heath & Dan Heath
title: Switch
subtitle: How to Change Things When Change Is Hard
main_color: F66051
text_color: A84237
---

# Switch

_How to Change Things When Change Is Hard_

**Chip Heath & Dan Heath**

_Switch_ examines why it is often difficult for people to switch their behavior, and how, by understanding the mind, it is possible to find shortcuts that make change easier. Through scientific studies and anecdotes, _Switch_ provides simple yet effective tools for implementing changes.

---
### 1. Implementing change is like riding an elephant: choose a direction, give your elephant some peanuts and stick to an easy path. 

Changing our behavior is not always easy, as anyone who has resolved to quit smoking, eat healthier or start running in the mornings will know. So what is it that can make change so difficult?

An excellent analogy for examining behavioral change is that of _an elephant_ and _its rider_ trying to follow a certain _path_. The elephant, being a powerful, stubborn creature, represents the emotional side of people, looking for a quick payoff rather than long-term benefits. The rider in turn represents the rational side that knows what _should_ be done, and can tug at the elephant's reins to exert some small degree of control over it. Finally, the path represents the situation in which the change is to take place.

Consider a situation where you want to get up at 5:45 in the morning to go jogging. Your inner rider has rationally analyzed the situation and thinks this is good for you. But what happens when the alarm actually goes off in the morning? If you're like most people, your inner elephant will demand just a little more sleep, totally overpowering your rider, and you'll end up skipping the jog.

But what about the situational factors that help or hinder your noble quest? A comfy bed and bad weather outside won't help get that elephant moving, that's for sure. On the other hand, the smell of freshly brewed coffee just might. 

All three components influence whether change will be successful, whether progress along the path is made. From changing your own diet to influencing others' behavior, your success will depend on your ability to _direct the rider_, _motivate the elephant_ and _shape the path_.

**Implementing change is like riding an elephant: choose a direction, give your elephant some peanuts and stick to an easy path.**

### 2. Find the bright spots, learn from them and spread them around. 

Your inner rider is a terrific thinker and planner. Unfortunately, he is also prone to spinning his wheels: overanalyzing every aspect of a potential change without actually doing anything. Worse still, his analysis is totally problem-focused, obsessing over all the difficulties ahead.

But endless analysis of the obstacles in your way gets you nowhere; you must give the rider a clear direction to go in so he can put his planning and rational thinking to good use. Instead of looking at the problems, find and focus on the so-called _bright spots_ : specific situations or areas where change has already succeeded. Then figure out how change was achieved and leverage these lessons to make the change more widespread.

This approach was employed by Jerry Sternin in 1990, when the government of Vietnam invited him to help fight children's malnutrition. Rather than tackling the myriad and near-unsolvable causes of malnutrition (poverty, poor sanitation etc.), Sternin decided to look for bright spots. He found that in a small local village with near-universal malnutrition, some children were in fact well-nourished. Their families had _already_ solved the problem.

By observing these families, Sternin discovered that there were small but important differences in the way these children were being fed. For example, while the children received no more food than in other families, their mothers fed them smaller portions more often. Sternin managed to spread these behaviors to other families, who accepted them more readily because they came from their own community, not from outsiders.

The impact of these small changes was amazing: six months later, 65 percent of the children in the village were better nourished. The bright spots had spread considerably.

**Find the bright spots, learn from them and spread them around.**

### 3. The rider hates making decisions, so clearly script the critical moves needed for a change. 

When faced with a change situation, the rider can easily succumb to something called _decision paralysis_. Say you're trying to implement the vague behavioral change to "eat healthier." The rider will try to analyze all the possible alternatives of how to do this: eating more vegetables, eating less pasta, using less salt, frying with a different oil, etc. Any one of those changes would already help, but the rider ends up doing a lot of analysis and changing nothing.

This is a very human tendency. Studies have shown that, somewhat paradoxically, the more choices we have, the _less_ able we are to make a decision. Too many alternatives just confuse us. From the outside, this may look like resistance to change, but it's actually lack of clarity on what to do next.

The solution is to give the rider clear directions to follow. Ambiguity is the enemy, so you need to provide clear _behavioral goals_ and instructions. Think about the situations that are most crucial for the change and _script the_ _critical moves_ for those situations. For example, to eat healthier, you probably need to script the critical moves for shopping; what you buy is what you eat.

When health researchers wanted to persuade West Virginians to eat a healthier diet, they did not offer vague advice like "eat healthier." Instead, they provided crystal-clear instructions: "Next time you're buying milk, buy one percent milk instead of whole milk." That's it. This advice was so easy for consumers to implement, the market share of low-fat milk doubled, dramatically lowering consumers' fat consumption.

When scripted correctly, even small changes can bring big benefits.

**The rider hates making decisions, so clearly script the critical moves needed for a change**.

### 4. Use a destination postcard that appeals to both the rider and the elephant. 

When faced with change, the rider will often obsess about which direction to move in, wasting energy by overanalyzing every option. However, you can avoid this analysis paralysis by giving the rider a direction that he can navigate toward.

For example, when first-grade teacher Crystal Jones wanted to motivate her students, she told them that by the end of the year, they would all be honorary third graders: reading, writing, doing math and so forth at a third grade level. This was a powerful image because it spoke to the students' inner riders, giving them a clear goal to strive for, but it also engaged their inner elephant by promising a desirable goal: the "cool" third-grader status.

This is called a _destination postcard_, a vivid and attractive picture of the near future. To be most effective, it should appeal to both the rider and the elephant. Above all else, ensure that you script the critical moves in line with the destination postcard so they work in unison to further the desired change.

But what about when someone isn't really committed to change? Say you've decided to "eat healthier." You can try to adhere to a new diet, but sooner or later you'll start _rationalizing_ : "Well, I did eat a salad last week, so surely I can eat these six hot dogs now!"

To stop rationalizing, one solution might be to make the goals black and white, meaning they have no wiggle room. Instead of "eat healthier," you could say, "I'll never eat another hot dog again." This strict approach is less inspiring, but it does make it harder for you to slip from your new diet.

**Use a destination postcard that appeals to both the rider and the elephant.**

### 5. To get the elephant moving in the right direction, evoke strong emotions. 

While your inner rider may technically hold the reins of your inner elephant, in a battle of wills, the rider will only be able to command the powerful beast for so long before his strength runs out and the elephant veers off in the wrong direction. This is why successful changes usually demand motivating the elephant as well. Logical analysis and rational arguments that appeal to the rider are of no help here. Instead, to get the elephant moving in the right direction, a powerful emotion must be triggered.

When Jon Stegner wanted to convince the leaders of the manufacturing company he worked for that their purchasing function was hugely inefficient, he knew he needed to get their attention fast; charts and analysis would not be enough.

So Stegner tailored his presentation to the inner elephants of the management team. He obtained one pair of each type of glove used in the company's various factories, amassing 424 different types of gloves in total. He then piled them onto a conference table and invited the leadership of the firm to see this huge heap, prompting an emotional reaction: "Why are we buying so many different kinds of gloves? This is crazy!" Immediately, the whole management team agreed that Stegner should revamp the purchasing process.

The emotion evoked to get the elephant moving can be either positive, like desire, or negative, like fear. In general, a negative emotion can highlight a sense of urgency, narrowing people's focus to fix an obvious problem quickly — as the sense of shock and outrage did in Stegner's case.

But when problems are less clearly defined and the solutions aren't obvious, positive emotions are more productive, as they tend to broaden people's outlooks, helping them find new solutions.

**To get the elephant moving in the right direction, evoke strong emotions.**

### 6. To get the elephant to climb a mountain, lead it up a small hill first. 

Change often seems massive and daunting. For example, a person who is deeply in debt can feel like they'll never be able to pay it off. To the elephant, a massive change looks like a demoralizing mountain that it is expected to climb, and hence it remains stubbornly still.

So how do you get this elephant moving? You need to _shrink the change_ : show the elephant it just needs to climb a small hill first.

One solution is to demonstrate to the elephant that progress has already been made. Consider this study: When people were told they needed ten stamps on their car wash loyalty cards to get a free wash, only 19 percent completed their card. When another group was told they needed 12 stamps, but — here's the catch — the card already had two stamps, 34 percent completed the card. That's almost double the amount! Despite the identical effort needed, the second group was more motivated to complete the card, because they felt they had already started the journey. Hence, to motivate people, emphasize progress already made.

But the most important way to shrink change is to split it into smaller milestones or "inch-pebbles." Make it easy to attain _small wins._

This is the approach personal finance guru Dave Ramsey uses to help people out of debt. Contrary to the advice given by many accountants, Ramsey tells his customers to pay off their smallest debts first, because it is far more motivating to eliminate small debts entirely than reduce a larger debt by a fraction.

Small wins create hope that change is possible, and hope is like fuel for the elephant. As more and more milestones are hit and small wins accumulated, the elephant gathers momentum, making the change effort self-sustaining.

**To get the elephant to climb a mountain, lead it up a small hill first.**

### 7. Grow your people by cultivating a change-friendly identity and adopting a growth mindset to overcome failure. 

In 1977, most citizens of St. Lucia couldn't have cared less about their native and gravely endangered St. Lucia parrot. Unfortunately, without their support there was no way to protect the gorgeous turquoise and lime-green bird from extinction.

But 21-year-old Paul Butler was tasked with doing just that. There were no rational economic arguments to sway the natives with, so he instead targeted their national identity: with bumper stickers, T-shirts and volunteer action, Butler started making the bird a part of their national identity. Soon, a groundswell of public support helped pass strict laws that saved the magnificent bird from extinction.

This example highlights an important aspect of getting people to embrace change: Is it congruent with their _identity_, who they perceive themselves to be? For example, if people consider themselves "concerned citizens," they will probably commit to change that they think "concerned citizens like themselves" should commit to, such as protecting an endangered parrot.

Sometimes to drive change, you must instill new identities in people, as demonstrated by Butler.

But even if people embrace a new identity, sooner or later they are bound to have difficulty living up to it. Even "concerned citizens" sometimes act in inappropriate ways. How people deal with such adversity is a key factor in successful change. The response to it should not be to give up but rather to learn from it and grow.

Adopt a _growth mindset_ : accept that failure is inevitable but also useful. It teaches you how to improve. Think of your brains and abilities as muscles that are not fixed but can be trained to become more powerful. Studies have shown that a growth mindset helps produce better students, better business ideas and even better heart surgeons!

**Grow your people by cultivating a change-friendly identity and adopting a growth mindset to overcome failure.**

### 8. To change people’s behavior, give them an easy path to follow. 

Sometimes, change can succeed even with a confused rider and reluctant elephant. The reason lies with the path they are following: situational or environmental factors affect people's behavior. Hence, shaping the path to be a gentle, pleasant downhill stroll can be a great help in making changes.

People tend to underestimate the role of situational factors, especially when trying to explain the behavior of others. This is known as _fundamental attribution error_ : we think the behavior of others derives from _the way they are_ rather than _the situation they are in_.

But actually, situational factors play a huge part in the way we behave. Research indicates they can be even _more_ powerful in influencing our behavior than innate factors.

Consider a study where college students were rated by their friends on their innate charitableness and labeled accordingly as "saints" — the top half — or "jerks" — the bottom half. Next, the students were mailed letters asking them to contribute food to charity. To examine the effect of situational factors, half of them — chosen at random — received a very basic letter asking them to bring food to a well-known spot on campus, while the other half received more detailed instructions specifically requesting a can of beans and providing a map of the exact donation location. The basic letter yielded mediocre results: only eight percent of the saints donated and not one of the jerks. But in response to the detailed letter, a stunning 25 percent of the jerks donated food: three times more than the saints who got the basic letter!

The saints' innate charitableness was by far overshadowed by the effect of slightly manipulating the situation. Similarly to advance change, small alterations to the situation can help people change their behavior even if they are not really motivated.

**To change people's behavior, give them an easy path to follow.**

### 9. To get a “free ride” in implementing change, build new habits and make the environment enforce them. 

During the Vietnam War, the US government was concerned about rampant drug use among its troops. Some 20 percent of the soldiers developed a serious addiction in Vietnam, and there were concerns about the consequences when they brought their new habits back home.

Astonishingly, however, the government found that one year after returning, only one percent of the veterans were still addicted. Just as the environment in Vietnam had made so many of them addicts, their familiar environment and loved ones back home made them kick the habit.

This example highlights that one of the ways the environment affects our behavior is by enforcing or deterring habits. And habits are crucial for change because they are, in essence, behavioral autopilot. We engage in habits — both positive and negative — without thinking, which means they demand very little effort from the inner rider. If you can build habits that advance a desired change, you're basically getting a free ride.

But building new habits is not easy, so it is vital to shape the path — the environment — to help you adopt them.

One way to achieve this is to set environmental _action triggers_ for your habit: when A happens, you do B. In effect, this passes control of your behavior to the environment. For example, you could decide that whenever you drop your children off at school (the trigger), you will immediately go to the gym (the action).

Another effective tool is the humble checklist. By adhering to a step-by-step checklist, people are less likely to get sloppy when they are engaging in a habit. That's why pilots use checklists: so they don't take risky shortcuts in their routine. Lists help ensure you execute the habit as intended.

**To get a "free ride" in implementing change, build new habits and make the environment enforce them.**

### 10. Make the path look well-trodden: show people they are following the herd. 

At heart, humans are herd animals: in situations where we're not sure how to behave, we look to others for cues. For example, if you're at a fancy dinner and don't know which fork to use, you will look to other diners and follow their lead.

Behavior is contagious. This is why TV shows have laugh tracks and bartenders seed their tip jars at the beginning of an evening. We tend to follow the example set by others, laughing and tipping with the herd.

When trying to change people's behavior, you can take advantage of this tendency if you demonstrate that the majority of the herd is rallying around the change. For example, if you want everyone in your company to start using new time sheets but a small minority remains resistant, post the compliance lists publicly. Peer pressure will get the resisters to change their behavior, too.

Note, though, that this only works if the majority of the people are already using the new time sheets. If you face an oppositional _majority_, then things are different. Your task is then to find the minority who support your change and help them strengthen their case by giving them their own space within which to discuss the benefits of the change. For example, you could have the people who support the new time sheet meet regularly to discuss its benefits and find the language to describe those benefits ("more efficient time management," "better cost-control," etc). This will help them to sway the resisters.

Eventually, there will be an inevitable conflict between the "conservatives" and the "reformers." While this is not desirable, it is necessary. Think of it as organizational molting, after which a better organization will emerge.

**Make the path look well-trodden: show people they are following the herd.**

### 11. Final summary 

The key message in this book is:

**When you want to change your behavior, three key components affect your chances of success: The rider represents your analytical side, and he needs a clear direction to move in. The elephant represents your emotional side, and it needs to be motivated to walk in the rider's chosen direction. Finally, the path represents situational factors that are in play, which is why you must shape it to make it easy to follow.**

The questions this book answered:

**What components determine whether a behavioral change is successful?**

  * Implementing change is like riding an elephant: choose a direction, give your elephant some peanuts and stick to an easy path.

**How can you direct the rider?**

  * Find the bright spots, learn from them and spread them around.

  * The rider hates making decisions, so clearly script the critical moves needed for a change.

  * Use a destination postcard that appeals to both the rider and the elephant.

**How can you motivate the elephant?**

  * To get the elephant moving in the right direction, evoke strong emotions.

  * To get the elephant to climb a mountain, lead it up a small hill first.

  * Grow your people by cultivating a change-friendly identity and adopting a growth mindset to overcome failure.

**How can you shape the path?**

  * To change people's behavior, give them an easy path to follow.

  * To get a "free ride" in implementing change, build new habits and make the environment enforce them.

  * Make the path look well-trodden: show people they are following the herd.
---

### Chip Heath & Dan Heath

Chip Heath is a professor of organizational behavior at Stanford University, and his brother Dan Heath is a senior fellow at Duke University. They have co-authored two other bestsellers: _Made to Stick_ and _Decisive_.

