---
id: 56541ea86362640007490000
slug: syria-en
published_date: 2015-11-25T00:00:00.000+00:00
author: John McHugo
title: Syria
subtitle: A Recent History
main_color: None
text_color: None
---

# Syria

_A Recent History_

**John McHugo**

_Syria_ (2014) offers a detailed look at the history of Syria, from its ancient origins to its division after world wars and modern struggles under an authoritarian government. You'll learn that the country's brutal civil war and role as recruiting ground for militant groups like ISIS all have roots in Syria's turbulent past.

---
### 1. What’s in it for me? Find out where the roots of Syria's current chaos are. 

Since 2010, the international news has been overwhelmed with images and stories of Syria in turmoil. Following the events of the Arab Spring, Syria has been torn apart by a brutal civil war, a conflict that so far seems to have no obvious end. 

But why has Syria suffered so much, compared to many of its Arab neighbors? The answer can be found in the country's complex and mostly recent history. The history of Greater Syria in essence is a history of conflict: among nations, cultures and religions. 

These blinks will walk you through the rich history of an ancient land called Shaam and the various conflicts that have shaped and controlled its destiny. With this, you'll gain a new understanding into Syria's current civil strife and potential for solution.

In these blinks, you'll discover

  * how the French and British carved up ancient Syria for their own benefit;

  * how children writing on walls provided the spark for civil war; and 

  * why ISIS both unifies and divides in its ruthless tactics.

### 2. Modern day Syria was once a part of an ancient, turbulent and broader empire called Shaam. 

The country of Syria, as we know it today, is relatively young. Like many other nations, Syria's modern borders weren't drawn until the twentieth century. 

Before this, Syria was part of a larger empire called _Shaam_, or Greater Syria. Its borders comprised modern-day Syria, Lebanon, Palestine, Israel, Jordan and parts of Turkey. 

Because of its strategic location and Mediterranean coastline, however, Shaam was vulnerable to invasion. Ancient Egyptians invaded when the Nile River didn't flood and local crops failed; Alexander the Great as well as Roman caesars incorporated Shaam into their own empires.

After the Arab conquests of the 630s and 640s, Shaam became part of the Umayyad caliphate, and large swaths of the population converted to Islam. Centuries later, in 1516, the Ottoman Turks conquered the region and ruled until the end of World War I. 

In the nineteenth century, Greater Syria became better connected to the West, thanks to new technologies like the railroad and the telegraph. As people traveled to Europe to seek education, many eventually returned home with new ideas of liberalism, democracy and nationalism.

In 1856, for example, the sultanate of the Ottoman Empire wrote new, more egalitarian laws, abolishing the unequal treatment of non-Muslim subjects. The goal was to foster a sense of patriotism for the empire among its many different ethnic and religious groups. 

Yet by 1860, these goals were seen as clearly unrealistic. Long-brewing tensions between two ethnoreligious minority groups, the Maronite Christians and the Druze, a religion that splintered from Shi'a Islam in medieval times, erupted into full-scale war. 

Unrest among various groups only intensified in following decades, despite the sultanate's attempts at control. Yet more problems waited on the horizon, as Europeans turned their interest to the lands of Shaam.

### 3. After World War I, Greater Syria was divided and came under British and French rule. 

As the Ottoman Empire began to weaken, the interest of European imperialists in Greater Syria strengthened.

Britain and France were essentially first in line: the British were interested in Iraq's oil fields, while the French saw themselves as the protectors of the region's coastal Christian population. France was also the main export country for certain Ottoman goods, and had invested heavily in trade infrastructure. 

With the fall of the Ottoman Empire at the end of World War I, Greater Syria splintered. In 1916, however, Britain and France signed a secret agreement on how the two countries would carve up the region after the war. This would become known as the _Sykes-Picot agreement_. 

In 1917, British foreign secretary Arthur Balfour penned a document that would have huge repercussions for the entire Middle East. In the _Balfour Declaration,_ the foreign secretary pledged to a leader of a British Jewish community his support for the building of a Jewish state in Palestine. 

Crucially, the post-war Allied powers completely disregarded the wishes of the population of Greater Syria when they made such sweeping decisions. 

In fact, in 1919, a commission set up by the Allied powers found that the majority of people in Greater Syria wanted the nationalist Emir Faisal to establish the land as an independent monarchy. Faisal even attended the Versailles Peace Conference in 1918 to represent Arab interests, but was essentially ignored. 

At the San Remo Conference in 1920, Greater Syria was thus divided. The British controlled the mandate of Palestine, including Jordan, and the mandate of Iraq. The French in turn controlled the mandate of Syria and Lebanon.

### 4. France abused its mandate in Syria and many local groups rebelled, fighting social injustices. 

Under the mandate granted by the League of Nations (an intergovernmental organization established after World War I, and precursor to the United Nations), France was supposed to assist Syria in establishing its independence. 

While France agreed to do so, the country's actions were, in reality, quite different.

Out of some 5 billion francs that the French invested in Syria, 4 billion francs were allocated for defense. Little to no funds were dedicated to establishing schools or to the development of agriculture or local industry in Syria. French companies also received more concessions and contracts than did Syrian companies. 

Syrian nationalist Dr. Abdul Rahman Shahbandar founded the People's Party in 1925 in response to these injustices. The party demanded genuine independence and an end to foreign control of the economy. It also sought to eliminate religious and class divides.

That same year, the Druze revolted against the French's divide-and-rule tactics. 

The conflict escalated when French general Maurice Sarrail invited three Druze leaders to discuss grievances, but instead held them hostage in an attempt to encourage "good behavior" among the community. The Druze responded with force. Led by Sultan al-Atrash, Druze rebels attacked French troops and some 1,000 soldiers were killed. 

The rebels eventually took control of most of Damascus, making the city unsafe for French troops. Yet France responded by shelling Damascus for two days, destroying large parts of the historic city. It's estimated that around 1,500 Syrians, many of whom were women and children, were killed during the shelling.

By 1927, the French had managed to crush the uprising, yet its aftereffects would reverberate in Syria for years to come.

### 5. A growing Arab nationalism pushed Syria toward achieving independence from French rule. 

The uprisings against the French helped foster within the population a stronger sense of national identity. In the early 1930s, a new generation of Arab nationalists was born. 

Arab nationalists rejected the idea of cooperating with the French to secure independence for Syria. The effects of the Great Depression on the region also intensified public anger, especially toward the growing Jewish settlements in Palestine. 

The era saw a rise in Arab public figures who promoted ideas about pan-Arabism. One leader was Shukri al-Quwatli, who had previously been a member of Al-Fatat, a nationalist secret society. 

Al-Quwatli's role in the revolt against the French made him a national hero. He fled into exile after the French took over, as he was considered dangerous by the French regime. Al-Quwatli's main goal was the reunification of the lands of Syria and Palestine.

During World War II, Syria finally achieved independence, although Western powers continued to meddle. When France fell to Hitler's troops in 1941, French authorities in Syria and Lebanon joined the side of the collaborative Vichy regime. 

The Allied powers realized they had to do something to counter this, so on June 8, 1941, Allied planes dropped leaflets all over Syria, promising independence. Yet this was an empty promise.

Once the French regained control of the country, talk of independence was quickly silenced. French General Charles de Gaulle, for example, felt that Syrian independence was contingent on the British terminating its mandate for Iraq. 

By December 1943, Al-Quwatli had become Syrian prime minister and declared that the French mandate was no longer recognized. He then signed a new constitution for the Syrian nation. 

By 1946, the last French troops finally left Syria.

### 6. The first two decades of independence were marked by internal power struggles. 

Syria's early years of independence were turbulent, as different groups backed by competing ideologies fought for control. 

On the far right, Antun Sa'ada was the head of the fascist-leaning Syrian National Party. On the left, Syrians organized a Communist Party. Neither of these groups gained much political influence, however. Islamism, and in particular the Muslim Brotherhood, had more success. 

Founded in Egypt in 1928, the Muslim Brotherhood sought a return to the region's Islamic roots and encouraged its followers to reject the influence of Western nations. 

Yet Ba'athism, a political mixture of nationalism and socialism, would come to have the greatest influence in Syria. The movement's founder, Michel Aflaq, believed that Arabs across all regions should join together and take pride in their Islamic faith. 

The situation in Palestine and the advent of the Cold War further exacerbated the political situation in Syria. 

In 1947, the British gave up their mandate to Palestine to the United Nations, which then partitioned Palestinian lands into separate Jewish and Arab states. This sparked conflict on the ground between settlers and war ensued, with Arab forces joining in the fight for land. 

Yet by the end of this first wave of conflict, Arab troops had been soundly beaten and the newly formed state of Israel laid claim to some additional 66 kilometers of Palestinian territory. 

By 1955, the Cold War was in full effect. Britain, Turkey, Iran, Pakistan and Iraq agreed to sign the Baghdad Pact, which extended the influence of the North Atlantic Treaty Organization (NATO) further east in order to curtail Soviet influence in the region.

Yet Syria refused to sign, which effectively placed the country in the orbit of the Communist bloc of countries. 

In the following two decades, Syria was ruled by several different factions and successive governments were overthrown in various coups. 

In 1970, however, defence minister Hafez al-Assad won an internal power struggle within the Ba'ath party, taking power from former coup and military leader Salah Jadid. This marked the beginning of the reign of the Assad family in Syria.

### 7. While Hafez al-Assad did stabilize the country, he was a brutal leader of a corrupt regime. 

Syria enjoyed nearly 30 years of relative stability during Hafez al-Assad's reign. Yet Syrians paid a price for this peace, as Assad was a brutal leader.

Syria's relationship with Israel deteriorated even further during Assad's reign. In 1973, Syria and Egypt attacked Israel, aiming to retake the Golan Heights. Israel won the war, but the Syrian army proved to be much stronger than it had been six years earlier during the Six-Day War. 

What's more, Assad was unwilling to negotiate with Israel, unlike Anwar Sadat, his Egyptian counterpart. This only made the two countries' relationship worse. 

Assad brought about several positive changes in Syria, but his regime was corrupt and based on nepotism. He did increase the country's literacy rate, for example. In 1960, only a third of the population was literate; but by 1990, some 80 percent of 10-year-olds were able to read.

Assad was also a member of the secretive Alawi ethnic minority in Syria, a group much of the country's Sunni majority regarded as suspicious. Assad appointed Alawis to important positions during his reign. By 1992, seven of the army's nine divisions had Alawi commanders. 

The regime's corrupt, nepotistic practices eventually inspired a rebellion. In 1976, several prominent Alawi figures were assassinated. The attacks were inspired by Sayyid Qutb, a Sunni Islamist intellectual who saw Ba'athists as apostates who wanted to turn Syria into an atheist state.

The situation only grew worse and in 1982, a militant group backed by the Muslim Brotherhood tried to inspire a nationwide uprising to topple the Assad regime. 

The regime responded brutally to the rebellion, in particular laying siege to the city of Hama where many Muslim Brotherhood leaders were based. The estimates of those killed in Hama during the month-long fight vary from 5,000 to 40,000. 

After the massacre, the state secret police, or _mukhabarat_, appeared everywhere. And Syrian citizens lived in constant fear of their influence.

### 8. Initially promising, the regime of Bashar al-Assad turned ruthless following the events of the Arab Spring. 

Bashar al-Assad was only 34 years old when he took over the government after his father's death in June 2000. 

In his inaugural speech, Assad emphasized the need for national dialogue and a transparent government based on "democratic thinking." His promises turned out to be hollow, however. 

In Assad's early years, independent newspapers worked freely, many political prisoners were released and new civil rights groups were established. This era became known as the Damascus Spring. 

Corruption and nepotism within the government were still widespread, however. The economy was also in a terrible state when Bashar al-Assad took over. The US invasion of Iraq worsened the situation, as following UN sanctions against Iraq, Syria lost a lucrative trade partner. 

Assad's government enacted a series of economic reforms, such as allowing the growth of private banks and insurance companies. These measures enriched those close to Assad but did little to benefit the public. Assad's cousin, Rami Makhlouf, was said to control 60 percent of the Syrian economy. 

The events of the Arab Spring across the Middle East and northern Africa, which began late in 2010, were to set Syria on its present-day course.

In March 2011, a group of children graffitied the walls of their village school, demanding the downfall of the regime. They were arrested and brought to Damascus to be interrogated.

After two weeks, the children, aged from eight to 15, still hadn't been released. Demonstrations demanding news popped up all over the country. 

One month later, Assad publicly declared that the whole event had been a "foreign conspiracy." All but his staunchest supporters were furious, and local protests grew.

The conflict between the government and the people escalated when security forces began shooting at protesters. By June 2011, over 10,000 Syrians had already fled across the border to Turkey. By the end of July, 2,000 people were said to have died in the fighting.

### 9. The rise of ISIS and a reluctant international community has led to a spiral of violence in Syria. 

As the situation on the ground in Syria became increasingly violent, groups fighting the regime called for foreign intervention. The world, however, did not heed their call. 

On August 21, 2011, government forces under the command of Bashar al-Assad used chemical weapons in an attack against the towns of Ain Tarma and Zamalka, two suburbs of Damascus. Hundreds of civilians died as a result. 

US President Barack Obama had previously suggested that Syria's potential use of chemical weapons would necessitate US military intervention. There was little public support for such a move, however, so the president's hands were tied.

Yet many in the international community felt that the Syrian people were paying the price for the US's failed policies in Iraq. 

Another option to speed the use of international force in Syria would have been through the United Nations. Yet such an option was blocked by Russia's veto on the UN Security Council. 

Meanwhile, the escalating chaos and violence in Syria became the perfect recruiting ground for the militant Islamist group ISIS, or the Islamic State of Iraq and Shaam. 

In April 2013, ISIS troops overtook Raqqa, a town which the government had practically destroyed in bombing raids, in an attempt to oust rebel groups that had controlled it a month earlier. 

The ranks of ISIS grew rapidly, as the group was essentially a uniting force for disaffected Sunnis in eastern Syria. Soldiers' tactics were ruthless. Children and young women were considered spoils of war, and non-Muslim men were killed unless they converted to Islam. 

By December 2014, over 200,000 Syrians had died in the conflict and 3.2 million people had fled to other countries; what's more, 6.5 million people had been internally displaced. 

With no solution in sight, the future of Greater Syria and its people is in peril.

### 10. Final summary 

The key message in this book: 

**It's easy to feel overwhelmed by the Syrian crisis: a ruthless civil war, the brutality of both ISIS fighters and the Assad regime, and the largest refugee crisis since World War II. But Greater Syria has long been an area of unrest, the result of European imperialism, the region's strategic location and ethnic strife that has evolved and intensified under different empires. Sadly there are yet no signs that current events will bring an end to this vicious cycle in Syria.**

**Suggested further reading:** ** _ISIS_** **by Michael Weiss and Hassan Hassan**

_ISIS: Inside the Army of Terror_ (2015) charts the rapid rise of the Islamic State in the Middle East, from its early beginnings to its self-proclaimed caliphate in Iraq and Syria. Grippingly told, the story of ISIS's domination over al-Qaeda in Iraq and its slow but ruthless push in Syria also shines light on the failings of the West in dealing with this fanatical yet disciplined jihadi group.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### John McHugo

John McHugo is an Arabic linguist and international lawyer with over four decades of experience in the Middle East.

