---
id: 590b2a62b238e10007a619e1
slug: ta-ra-ra-boom-de-ay-en
published_date: 2017-05-11T00:00:00.000+00:00
author: Simon Napier-Bell
title: Ta-Ra-Ra-Boom-De-Ay
subtitle: The (Dodgy) Business of Popular Music
main_color: DC2C87
text_color: C22777
---

# Ta-Ra-Ra-Boom-De-Ay

_The (Dodgy) Business of Popular Music_

**Simon Napier-Bell**

_Ta-Ra-Ra-Boom-De-Ay_ (2014) takes its title from a late nineteenth-century vaudeville song and explores how the music industry has evolved and adapted to various changes and inventions that have continuously revolutionized what we listen to and how we listen to it.

---
### 1. What’s in it for me? Tune in to the rhythm of modern popular music history. 

Music is always a reflection of society and the times. When slaves in the American South belted out work songs, their tunes reflected the hardships of their lives and the injustice of the world at large. And when, in the 1970s, Emerson, Lake and Palmer let the chords on their Hammond organ ring out, these futuristic sounds signaled the inevitable and continuing integration of technology into music.

Music interacts, changes and develops with society, and specifically with the emergence of new technologies, laws and trends. But because societies are largely shaped by economic interests, music also has a lot to do with business.

These blinks will guide you through the history and business of modern popular music. You will dance to jukebox classics, step into the jazz clubs of Chicago and New York and join the masses at Woodstock.

You'll also learn

  * how copyright law changed the world of music;

  * how alcohol consumption boosted music sales; and

  * what the Hells Angels charged for security services in 1969.

### 2. The modern music business took its first steps with the invention of copyright law and the record player. 

Music has been around in one form or another for millennia. And yet, in the grand scheme of things, it was only recently that songs began to be seen as individual, rather than common, property.

This distinction emerged with the first copyright laws, which not only created a new branch of the legal system, but also a new publishing market for songs.

It began in 1710 when the British court of law first recognized authors as having rights over what they wrote. Suddenly, authors now owned their work and got royalties from each sale.

This led to the first music publishing house, Chappell & Co., which was founded in the early nineteenth century after the owner of a piano shop, Samuel Chappell, joined forces with the famous pianist Johann Baptist Cramer. Soon they were collecting, publishing and selling Cramer's sheet music to the public.

Naturally, it wasn't long before others noticed that publishing sheet music was an easier and more profitable way for artists to make money than playing concerts. And the general public loved it because they could buy the sheet music, play the songs in their homes and hear music that they would never have access to otherwise.

But the world of music publishing really exploded at the end of the nineteenth century, when an innovation known as the phonograph opened up the doors for a whole new revenue stream.

In 1877, Thomas Edison produced the first commercial record player, making it possible for people to play recorded music in their homes.

For publishing companies, this meant they could now turn their attention away from sheet music and to the much more profitable world of records.

Sheet music could last years before it became worn down and tattered enough for a family to buy another copy, whereas a record would get played and replayed at such a rate that buying another one was practically inevitable.

> _"The court had created the music industry."_

### 3. Irving Berlin revolutionized popular music by changing its beat. 

In the current age of instant access to music, it can be hard to recall or even imagine the thrill of bringing a record into your home for the first time. But when the public started buying records for home listening in mass quantities, the music industry exploded; before long, pop music was born.

Many new pop stars also emerged, and with songs like "Blue Skies," "What'll I do?" and "Always," Irving Berlin would go on to be considered the quintessential American singer-songwriter.

Born in 1888 as Israel Isidore Berlin, his family left Russia for the United States when he was just three years old. His father died when he was eight, prompting him to leave home with little more than the dream of being a singer in cafés.

It was a rough start: while living on the streets, he found meager pay selling newspapers and working odd jobs, enough to earn him a room in a derelict warehouse for 15 cents a night.

But his love of music never wavered, and he soon began writing his own songs, one of which, "Dorando," put him on the path to superstardom.

A string of successes followed, including a relatively spicy tune about a nice Jewish girl who becomes a stripper, driving her jealous boyfriend crazy.

These songs earned Berlin a standing contract with the Ted Snyder publishing house, which is when he decided to change his name to the catchier-sounding "Irving" Berlin.

However, Berlin's early efforts paled in comparison to his 1911 hit "Alexander's Ragtime Band," which would forever change the sound of pop music.

It did so by mixing different elements of American music, including military brass band trumpet, with the jazz and ragtime sound of New Orleans, making it appealing to both black and white audiences.

In particular, it was the beat of this song that really transformed popular music.

Prior to "Alexander's Ragtime Band," songs that relied on a four-beat structure used an "on-beat" rhythm that emphasizes the first and third beats (boom, bap, boom, bap, etc.). But Berlin's song inverted that, using the New Orleans "off-beat" rhythm that emphasizes the second and fourth beats (bap, boom, bap, boom, etc.).

This beat would come to define the future of American pop songs.

### 4. Due to their outsider status, Jewish and black artists thrived in the music business. 

Despite its popularity in the early twentieth century, the music business was not seen as a reputable line of work for much of the country's Anglo-Saxon population. So it was left to the nation's outsiders to push the industry forward.

Black artists were able to make a living by taking jazz songs to nightclubs around the country, helping the genre grow in popularity.

Between 1910 and 1930, many black musicians rose to prominence as jazz music traveled from New Orleans to the rest of the country. Duke Ellington and Louis Armstrong were just a couple of the many musicians who found success and helped raise the music's profile.

As jazz musicians toured the country, they also began signing recording and publishing deals, which spread their music even further. And with money to be made, more and more black artists were leaving the South to find success in the Northern cities of New York and Chicago.

Jewish people also had limited options for prosperous work, but in the music business, they found two such roles: as the owners of publishing companies and as songwriters, such as George Gershwin.

As such, they were some of the first to publish and record the songs of black artists, which would, in turn, influence the songs composed by Jewish songwriters.

In the aftermath of World War I, most songwriters were Jewish and the majority of popular songs incorporated elements of black culture, either through jazz or blues music, which originated from the work songs of Southern slaves.

One of the biggest hits of this time was "Memphis Blues," which has gone on to be recorded and interpreted by countless famous musicians over the years.

### 5. The advent of radio initially scared the music industry, but its potential soon became apparent. 

It's always hard to predict exactly how an invention will impact the world, especially with something as groundbreaking as the radio.

Indeed, when it was first made commercially available in the early 1920s, this device revolutionized the music industry.

For the first time, families had a cheap alternative to the record player and didn't need a big collection of records to be able to listen to different kinds of music all day. Now, you could just change the frequency to hear different styles and discover new artists.

At first, this presented a problem: there was a decline in record sales and panicked executives worried that if people had a radio, they would no longer buy records to hear their favorite songs.

But this drop in sales was only temporary. It wasn't long before the music industry recognized radio for what it was: a great marketing tool.

Rather than seeing radio as competition to records or sheet music, the music industry realized they could use it to push new products to audiences and convince them to buy records by offering samples of exciting new music.

This is when the publishers began working hand-in-hand with radio DJs.

As their name implies, DJs are disc _jockeys_, in that they jockey or steer audiences toward certain songs or albums.

Suddenly, it was apparent that the music industry had a perfect arrangement in place for getting the public hooked on a song and desperately eager to buy the album in stores.

> _"So radio, which to start looked like it would wipe out the record business, had ended up saving it by giving it the electric microphone."_

### 6. Following Prohibition, the music industry grew as bars, clubs and Hollywood all generated new revenue streams. 

For 13 years, the United States attempted to ban the production, distribution, and consumption of alcohol.

Much of the nation raised a glass when the order was repealed in 1933, including the music industry, which was happy to forge a mutually beneficial arrangement with the liquor business.

After Prohibition, the number of nightclubs and bars increased fivefold, and while some of them offered live music, nearly all of them featured a recent invention: the record-playing machine known as the jukebox.

The odd name comes from the slang word "jook," which, in Southern black culture, was another word for "party."

This machine could liven up any establishment as it held dozens of records with songs that could be played on demand for small change. And every two to three weeks, it would be updated with new tunes.

This didn't just encourage customers to buy more records; with 250,000 jukeboxes in the United States by 1934, filling these machines accounted for over half of all record sales in the country.

At the same time, there was another crucial factor in the music industry's increased prominence: Hollywood.

Just as Prohibition was fading into a fuzzy memory, musicals were becoming more and more popular, a trend that was led by the 1933 smash hit _42nd Street_, and 1935's even bigger blockbuster, _Top Hat_.

One man who knew how to profit from these musicals was Irving Berlin, who kept the rights to his songs rather than selling them to the studios.

This move would keep the money coming in every time a movie with his song was played, so it didn't matter that people began spending their money on movies rather than going to nightclubs or buying records.

### 7. Following World War II, music became more international, focusing on rock bands and full-length records. 

The next big shake-up in the music business happened after World War II when there was an increase in the exchange of music and culture between the United States and the United Kingdom. In the States, this led to what was known as the "British invasion" of music, with young people across the country succumbing to "Beatlemania."

The Beatles first topped the US charts in 1962 with "Please Please Me," which was quickly followed by "From Me to You" and "She Loves You."

Their songs were well-written and catchy, and the hits just kept on coming; their popularity skyrocketed even further when they finally touched down on their first visit to America.

Soon thereafter, a whole new style of songs started gaining popularity, one that further blended the elements of rock and roll with blues that had originated with black musicians.

Meanwhile, the music industry picked up on the popularity of the Fab Four by shifting their focus on how to package and sell both records and bands.

Prior to the 1960s, the focus was on selling singles — but now the whole idea of recording and sales began to emphasize full-length albums. As such, the music industry became known as the record industry.

The various publishing labels now began booking studios and hiring talent to record an entire record, rather than just one or two songs.

And with the success of The Beatles, the music industry saw how eagerly fans embraced a recognizable group of three to five musicians, so they shifted to this model from the big jazz ensemble.

Part of this new small band model meant matching the image and personality of the group to their music in a way that made them personable to the fans.

Over the next few years, the "British Invasion" continued with more bands coming from across the pond, such as The Who, Cream, Traffic, Blind Faith and Led Zeppelin, and the popular music scene became much more white as a result.

### 8. Two festivals in 1969 offered important lessons about live music. 

Two concerts at the very end of the 1960s changed the face of popular music for very different reasons.

The first was the fabled Woodstock festival, which, after considerable planning, took place over three days in the summer of 1969.

It went ahead with the blessing of the record industry, under the assumption that fans would buy tickets and there would be proper security. Alas, neither of these really happened, as the farmland in upper New York was descended upon by 180,000 fans who found plenty of gaps in the fences and proceeded to have a largely unsupervised good time.

The only reported deaths were one heroin overdose and the case of one visitor who fell asleep in a patch of long grass and was run over by a tractor combine.

The free love and peaceful vibes at Woodstock were not to be found on the other side of the country a few months later, when the Altamont Free Concert in San Francisco ended tragically.

The concert was organized by The Rolling Stones in 1969 as a grand finale to their US tour.

The hastily planned event was held at the Altamont Speedway, and for security, they paid a local chapter of the Hells Angels biker gang $500 worth of beer.

In contrast to Woodstock, 300,000 people showed up for this one-day free show, and as the day wore on, more and more people were getting high on drugs and alcohol — including the Hells Angels.

After a number of attempts by fans to rush the stage, the Hells Angels turned violent and an altercation ended with Meredith Hunter, an 18-year-old black man, being stabbed to death. Ultimately, there were three other deaths, as well as a number of injuries and property damage.

These events offered important lessons for the music industry. It was clear that there was an audience for big festivals, and that they could be profitable — as long as they were properly planned.

### 9. Over the years, many musical subcultures reached mainstream popularity, although they were often associated with drugs. 

As long as there has been music, there have been different styles and genres, and even subgenres. Many of these styles have been closely associated with alternative lifestyles, some of which stay underground, while others become worldwide trends and change the music industry for good.

Many of these same musical movements have been closely associated with different drugs.

While marijuana has been a staple of the history of rock and roll, it was especially prevalent during the hippie movement of the 1960s.

Those looking for a bigger and much more dangerous kick turned to heroin, which had been a popular drug for many jazz musicians, but influenced many other artists throughout the 1970s.

In the late 1980s, the underground rave culture was fueled by ecstasy and MDMA, and the movement would also eventually become a worldwide craze.

Electronic dance music — particularly led by DJs at music festivals in Ibiza and clubs in New York or London — was perfectly suited to these drugs. The relentless beats made dancers and partygoers fall into a hypnotic rhythm; the drugs created a euphoric sensation, with everyone feeling connected, as if they were part of a wave that ebbed and flowed along with the music.

Another movement that took the world by storm was hip hop.

This movement began with DJs who sampled music from previously released records, using their beats as a backdrop for MCs to deliver lyrical rhymes that often spoke to modern black culture.

The 1988 hit protest song, "Fuck tha Police" by N.W.A. — which stands for Niggaz Wit Attitudes — is often identified as the moment when rap became mainstream.

The song is also a perfect example of how the public perceived rap as a nonviolent musical outlet for an entire generation of disenfranchised black people in the United States.

Over time, it would also become the favored mode of music for disenfranchised people around the world.

### 10. Sharing music files over the internet required the music industry to develop a new business model. 

As we go down the list of major changes to the music industry, you might be able to guess where it's headed. That's right — the most recent wave of change in the industry began when music hit the internet and customers were able to stream any song at the push of a button without having to leave their homes.

The big turning point in this musical revolution was Napster, which was founded in 1999 by a couple of college kids by the names of Shawn Fanning and Sean Parker who created an online platform for sharing and discovering new music.

But they wouldn't have been able to do this with without the proper software and file formats, which were created in the 1980s and 90s by engineers who discovered how to compress a song into a relatively small and shareable file. This is how the mp3 file was born, and this key development opened the door for music files to be uploaded, downloaded and easily shared online.

Now, anyone with an internet connection could avoid paying the price of a CD by logging on to Napster. As long as they agreed to share their own archive of music, they would receive access to the combined archives of every other user, which, with over 50 million users in 2001, constituted a lot of music.

With sales in freefall, this was a distressing time for the music industry, and it would take another invention for the industry to start regaining control of their catalog of songs.

In 2001, Apple released the first iPod, a sleek and portable device that embraced digital music files rather than trying to fight their existence. But to go along with this, Apple created iTunes — and put forward the idea of selling songs for 99 cents apiece. This was a big drop from the $3.99 price tag that singles were going for at stores, but since there were no packaging or distribution costs, the music industry eventually went along with it.

There was no denying that while CD sales were plummeting, there was a steady increase in digital music sales, and the music industry was on its way to regaining control of what the courts had granted them over a century ago.

### 11. Final summary 

The key message in this book:

**The history of the music industry is intricately linked to technological innovations and the many other changes that society has experienced over the years. Through all of this, the industry has managed to adapt and often thrive by taking steps to ensure its continued success.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _How Music Got Free_** **by Stephen Witt**

_How Music Got Free_ (2015) tells the remarkable story of the mp3 file, from its inception in a German audio lab to its discovery by a man working in a North Carolina CD-pressing plant, who would eventually team up with a piracy group to bring the entire music industry to its knees.
---

### Simon Napier-Bell

Simon Napier-Bell has a long and storied career in the music industry as a producer, the manager of the Yardbirds and cowriter of Dusty Springfield's hit, "You Don't Have to Say You Love Me." As an author, he's also written the books _Black Vinyl, White Powder_ and _I'm Coming Down to Take You to Lunch_.

