---
id: 534e958e6531660007000000
slug: talent-is-overrated-en
published_date: 2014-04-23T09:09:38.871+00:00
author: Geoff Colvin
title: Talent is Overrated
subtitle: What Really Separates World-Class Performers from Everybody Else
main_color: 2FC6EC
text_color: 1B7187
---

# Talent is Overrated

_What Really Separates World-Class Performers from Everybody Else_

**Geoff Colvin**

_Talent is Overrated_ explores the top performers in a number of fields to get at the bottom of just what makes them great. Contrary to what most of us intuitively think about skill, this book offers enticing evidence that top performance in any field are not determined by their inborn talent, but by deliberate efforts over many years.

---
### 1. What’s in it for me? Discover the crucial differences between mediocrity and unparalleled greatness. 

With standards rapidly rising in virtually every discipline, the understanding of where great performance comes from is more important than ever before. In order to gain an edge on the competition, it's crucial to know how to refine your skills in the smartest way possible.

The first step is abandoning the belief that greatness is something people are born with.

In the following blinks, you'll learn about one man's strange quest to breed his very own chess prodigies, what motivated Benjamin Franklin to skip church on Sundays, how tennis players know where to run to return a serve without even looking at the ball, and why you don't have to be a genius to know which horse to put your money on.

Most importantly, the book will give you the tools necessary to turn your average performance into world-class performance, while driving home the hard truth that there are no shortcuts on the path to world-class performance.

### 2. Contrary to popular belief, neither inborn abilities nor experience alone determine extraordinary achievement. 

If you're like most people, you spend most of your waking hours at work. And, like many others, you probably perform your work just fine without being world-class at it. For example, if you're an accountant, you probably don't rank among the very best at your job _even if_ you've been crunching numbers eight hours a day for the past twenty years.

So if we devote most of our waking hours to our jobs, why aren't most of us amazing at what we do?

Surprisingly, because extraordinary achievement isn't determined by experience!

Extensive research shows that many people don't improve at their work even after many years of experience; in fact, some actually get _worse_ as they gain experience.

Studies have shown that experienced doctors score _lower_ on tests of medical knowledge than their less experienced peers. This trend also seems to hold true across many other professions: the same goes for auditors detecting fraud and stockbrokers recommending stocks. Those with a wealth of experience often perform no better than their less experienced peers — and some perform even _worse_.

And great achievement doesn't derive from _inborn talent_, i.e., the natural ability to succeed more easily, either.

This was shown in a study that sought out talented individuals, conducted in England in the 1990s. Researchers gathered vast amounts of data on 257 young people, all of whom had studied music. Surprisingly, they found that those who exhibited the greatest performance didn't seem to have any more inborn talent than the other students!

The top performers exhibited no signs of extraordinary achievement before they started their intensive music training, which would have otherwise indicated a natural talent. Nor did top performers benefit from greater gains with the same amount of practice, indicating that talent didn't manifest itself in the form of rapid improvements, either.

### 3. In many fields, the link between intelligence and performance is weak or nonexistent. 

What is an "intelligent" person? Someone who can solve complex math problems? Someone good at synthesizing information? Although there are different ways of being intelligent, we do have one especially popular method of measuring general intelligence: the IQ test.

It's a widely held belief that a high IQ score correlates with greater success in life. One reason might be that, overall, the average IQ of employees does actually increase with the complexity of their tasks. And, since complex tasks are generally better rewarded, it seemingly accounts for this success.

However, upon closer inspection, IQ scores don't account for great performance and success as well as we might think.

Consider, for example, one study conducted on the relationship between sales performance and IQ. They discovered that, when you ask bosses to rate their salespeople, they tend to believe that their more intelligent employees do a better job. However, when the researchers measured intelligence against _actual_ sales results, they found that there was no correlation, thus making intelligence useless as a predictor of sales performance.

And the results of this study aren't limited to sales performance. Another example can be found in the world of horse racing, where so-called handicappers make predictions about which horses will win the race.

In an experiment, researchers studied handicappers' abilities in relation to their IQ. They found that high-IQ handicappers were no better at making predictions than those with lower IQs in spite of the demanding nature of forecasting the complex odds involved in determining a horse's skill.

In fact, one of the best handicappers was a construction worker with an IQ of 85, earning the classification "dull normal"; among the worst was a "bright normal" lawyer with an IQ of 118.

Even in chess, where people often associate greatness with genius-level IQs, we find grandmasters with _below-_ average IQs.

### 4. Contrary to popular belief, most great innovators spent years in intensive preparation before making their breakthroughs. 

What did your last "aha" moment feel like? A sudden stroke of genius out of nowhere? If so, you're not alone, and that's because the notion that creative ideas ostensibly strike us out of the blue permeates our culture.

Think, for example, of the story of Archimedes, who realized, upon settling into his bath, that he could measure the volume of an irregular object by measuring how it displaces water.

And then there's Abraham Lincoln, who's said to have written the iconic Gettysburg Address in a burst of inspiration _on the train_ to Gettysburg.

The prevalence of such stories shows just how widespread the belief is that great innovators made their greatest creative breakthroughs after having sudden strokes of genius.

However, research has shown that creative breakthroughs almost never appear out of nowhere; rather, they come most readily to those who already exhibit mastery of their field.

One study, for example, looked at the works of seventy-six composers over a number of historical periods to determine when they produced their first notable works. They found that each composer required on average a _ten-year_ "preparatory period" before he was able to produce anything noteworthy. Another similar study found the same pattern when studying painters and poets.

In fact, research has shown that this "ten-year rule" holds for outstanding performers in _any domain_, which indicates that, whatever you do, producing noteworthy innovations requires a deep and intense immersion in a field over a period of time.

And what about the breakthroughs of Lincoln and Archimedes? In fact, drafts of Lincoln's Gettysburg Address have been found on White House writing paper, demonstrating that it did not come in a sudden burst of inspiration at all. And Archimedes himself never even hinted at the bathtub story in all of his vast writings, leading scholars to conclude that the story is a mere myth.

### 5. Deliberate practice is the key to achieving world-class performance. 

We all know the saying "practice makes perfect." If we want to improve at something, we have to practice, and practice often — whether we're working on our putt or trying to do better at work.

And it's an undeniable fact that there's a powerful correlation between the time spent practicing and increased performance.

This is best exemplified in one study that aimed to find out why some violinists are better than others. Researchers asked professors at a prestigious music academy to name their best violinists, and then collected all sorts of biographical data on those performers, e.g., how often they practiced, what teachers they had, when they started studying music, etc., and even performed tests and interviews with them.

In the end, the researchers found that practice was the only differentiating factor: by most accounts, the best violinists didn't actually differ much from their peers, _except_ that they spent more time practicing.

But to become a truly world-class performer, however, it's _how_ — not just how _much_ — you practice that makes the difference: it takes _deliberate practice_ to improve performance.

Deliberate practice means identifying the specific elements of performance that require improvement and then sharply focusing your efforts on those areas, practicing those activities _ad nauseum_ and getting continuous feedback on them to get better.

This is best demonstrated by one particularly dedicated psychologist, László Polgár, who wanted to show that great performers are made through this kind of intense practice. So, he set up his own experiment: he found a volunteer, Klara, to have children with him and help raise them to be world-class chess players.

In spite of the fact that neither László nor Klara were especially good at chess, their eccentric experiment worked! Their three daughters, who grew up completely immersed in chess — playing chess every day for hours on end and having huge chess libraries at their disposal — all became world-class chess players.

### 6. Deliberate practice makes the performer perceive, know and remember more – and even alters the brain and body. 

Have you ever watched an extraordinary performer, like an acrobat or ballerina, and had the feeling that they must be superhuman — someone fundamentally different from you and everybody you know — to be able to perform those feats?

Although they aren't superhuman, in a way, your feeling is true: the deliberate practice that exemplifies these great performers actually _does_ make them fundamentally different from most people in a number of ways.

First, deliberate practice helps people perceive more relevant information in their field of expertise. For example, research on top tennis players shows that when receiving a serve, they don't focus on the ball like a layperson would, but rather look at the server's body to judge where the serve is going _even before_ the serve is hit. Because they have deliberately practiced receiving tens of thousands of serves, they perceive the subtle cues given away by the opponent's position that are invisible to the layperson.

Secondly, deliberate practice helps people absorb and remember vast amounts of knowledge in their fields of expertise.

Consider that some master chess players can beat even computers at the game. But how can that be possible given that computers can evaluate 200 million chess positions _per second_?

Unlike the computer, a chess master has spent many years of deliberate practice accumulating vast amounts of knowledge of chess. Having studied the great chess masters before them, they have the _knowledge_ of which choices produce which consequences without having to make the calculations themselves. Thus, they prevail.

Finally, deliberate practice can physically alter a person's body and even brain.

In fact, research has shown that, after years of intensive training, the hearts of endurance runners actually grow in size. Not only that, but the composition of athletes' muscles also change through years of practice.

Our brains, too, can be altered through deliberate practice. For example, when children practice playing a musical instrument, the regions of their brains devoted to interpreting tones and controlling their fingers actually grow by assuming more brain territory.

### 7. Beginning deliberate practice early in life has clear advantages. 

Have you ever wondered why the theory of relativity wasn't conceived by a college physics student?

When we talk about "great achievements" in the realm of physics, we usually mean new discoveries. But to be able to make new discoveries, you first need to have an understanding of all the existing laws and theories. In other words: you need a lot of knowledge.

Consequently, making groundbreaking achievements becomes increasingly difficult in fields where knowledge is constantly advancing. Heavily knowledge-based fields, like physics and business, require more and more studying to fully understand concepts as time passes, making it ever harder to get to the cusp of new discoveries.

This is exemplified by the increasing age at which Nobel Prize winners make their outstanding achievements: the average age has risen by a whole six years within a one-hundred-year period! Why? Because it takes longer to master the continually growing body of knowledge in their fields in order to reach the point where discoveries can be made.

Because of this, an early start in deliberate practice offers several advantages that are unattainable for late starters.

First of all, children and adolescents don't have to deal with the time-consuming responsibilities of adulthood, such as work and family, so they can concentrate more on practicing.

Second, starting early offers the advantage of a support network: family. While not all families provide supportive and stimulating environments for developing skills, those that do are very beneficial for helping their children achieve great performance.

In fact, research has shown that the home environments of top performers tend to be child oriented, meaning their parents believe in them and are willing to make an effort to help them.

Finally, our mental faculties slow down as we get older. Studies have shown that it takes us about twice as long to solve unfamiliar problems in our sixties as in our twenties, once again illustrating the importance of starting early to achieve greatness.

### 8. Motivation to perform develops over time and eventually has to become a self-driven force. 

Getting in the amount of deliberate practice it takes to become a world-class performer is grueling work; without the proper motivation, it would be impossible to achieve. But where does our passion and motivation originate?

Let's start by examining a mechanism called the _multiplier effec_ t. The idea is that a small initial advantage in some field can create a snowball effect — e.g., receiving more support and better coaching — which in turn produces greater advantages. Imagine someone with a strong forearm and quick reflexes who takes pride in having a slight edge over his peers in baseball.

This satisfaction could positively affect him in a number of ways: perhaps it will motivate him to practice more, or maybe his coaches will take notice of him and provide him the opportunity to play on a team with more professional training, thereby further increasing his abilities.

The multiplier effect explains how the initial satisfaction you get from perceiving yourself as being even a little better than others can produce sufficient motivation to drive practice and improvement, thus multiplying your advantage over others.

Indeed, studies consistently show that, in order to reach high achievement in any field — be it baseball or the arts — you need an "inner drive," i.e., a long-lasting motivation to become good at something even when there is no external reward.

But while world-class achievers indeed have a strong motivation to improve, most didn't start out that way. They needed to be pushed in that direction.

One study of twenty-four highly acclaimed pianists found that lessons had been _forced_ upon them when they were children. And yet, these performers said that the drive for great achievement eventually did become _their_ _own_ — and that it was this passion that kept them going. Indeed, external motivators, such as forced lessons, can be highly effective as catalysts for inner drives during the early stages of learning.

### 9. Choose what you want to achieve and practice in the areas necessary for getting there. 

Unfortunately, we can't travel back in time to reap the benefits of an early start. However, we can still apply the principles of deliberate practice as adults to help us reach our goals.

First, since achieving exceptional performance is so demanding, you must know precisely what your goals are and be committed to reaching them even when the circumstances aren't ideal.

Take Ted Williams, baseball's greatest hitter, for example: he's said to have practiced until his hands bled.

But while you don't need to lose any blood in order to make great achievements, you _will_ need rock-solid determination in order to put in the amount of practice necessary to become great.

This determination can only come from _knowing_ what you want: simply "liking" baseball won't drive you to put in the practice necessary to become a world-class player.

Benjamin Franklin exemplified this sort of dedication. He was a diligent writer, writing before and after his work day as a printer's apprentice, and even on Sundays despite his Puritan upbringing. He simply _knew_ he wanted to be a great writer and therefore made time for it.

Second, achieving great performance requires you to identify the specific critical skills you need in order to improve, and then _practicing them directly_. Direct practice means working over and over on those specific aspects that need improvement instead of making only a few general run-throughs on what you want to become better at.

Looking back to Benjamin Franklin: he didn't become an extraordinary writer by merely writing lots of essays. Instead, he addressed precisely those things that needed improvement. For example, when he needed to work on his syntax, he repeatedly summarized and reformulated newspaper articles, and then compared the evolution of his sentences to get feedback and continue improving.

### 10. Final Summary 

The key message in this book:

**Most people believe that people's natural talents are what turn them into world-class performers. But, in fact, as this book shows, talent has virtually nothing to do with performance. True world-class performance is built over a long period of time using deliberate practice, i.e., zeroing in on the critical aspects of a skill with laser-sharp focus, practicing them repeatedly and getting quality feedback. With the proper motivation, you too can use deliberate practice to improve in any field.**

Actionable advice:

**Practice deliberately for the best results.**

If you want to become great at your work, remember to focus not as much on the amount of hours you practice, but rather _how_ you practice. In order to make the biggest improvements, you need to design a system of deliberate practice that focuses on those areas critical to your field and that offers you immediate feedback. For example, if you want to get better at public speaking, then you should analyze your speeches intensely, looking for ways to improve specific aspects (such as clarity or eloquence), and get feedback from public speaking experts.
---

### Geoff Colvin

Geoff Colvin is an editor and columnist for _Fortune_, and a highly regarded commentator and lecturer on subjects like business trends and leadership. He also contributes daily with business commentary on the CBS Radio Network.

