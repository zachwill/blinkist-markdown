---
id: 55062d3d666365000a870000
slug: talent-magnetism-en
published_date: 2015-03-19T00:00:00.000+00:00
author: Roberta Chinsky Matuson
title: Talent Magnetism
subtitle: How to Build a Workplace That Attracts and Keeps the Best
main_color: FB9F3C
text_color: 945E23
---

# Talent Magnetism

_How to Build a Workplace That Attracts and Keeps the Best_

**Roberta Chinsky Matuson**

In _Talent Magnetism_, author Roberta Chinsky Matuson shows you how to transform your workplace into an environment that draws top talent like a magnet. The book offers practical advice on how to develop a strategy to stay ahead of the competition by identifying how evolving technology and a new generation of workers have changed business in the twenty-first century.

---
### 1. What’s in it for me? Learn the secrets to good recruiting in the 21st century. 

As a boss, you know how hard it is to find the right employee. Whether you're looking for a good Java developer or a bookkeeping expert, sifting through resumes or querying colleagues is time consuming.

The kicker? As technology gets more complicated and the job market evolves, it's only going to get harder and harder to find the best potential employees to work for you.

This is where the magic of _magnetism_ can help you. Great recruiting strategies aren't really about your personal contacts or social media networks, but about attracting people to your company with a force so strong, it feels like you're not even recruiting at all.

In the following blinks, you'll learn

  * why like all good relationships, recruiting is about mutual love;

  * how Apple as a company has mastered magnetism; and

  * how LinkedIn rewarded its employees and caused a social media uproar.

### 2. Adapt and embrace the new, modern talent pool, and be savvy to what they need and want, too. 

Why is it so hard for CEOs and senior executives to find good talent today?

Since there are so many people looking for work, you'd think that this would signal an abundance of great talent. But this isn't the case.

Many positions remain vacant due to an _underlap_ between the skills people possess and the skills for which most companies are looking.

In sum: there aren't enough people with the right kind of skills to fill open positions. Why is that?

For one, it's extremely challenging for workers to keep their skills up to speed with changing technology, tech that today's companies need to use.

So companies should invest in programs today that will attract tomorrow's talent.

Statistics argue that by 2018, 1.4 million tech-related jobs will open up in the United States alone, yet only 400,000 college graduates will able to fill these positions.

To tackle this dilemma, savvy companies such as Neustar have already started to think ahead by sponsoring a technology, information and digital media center for young adults.

Another way to promote talent and make personal connections beneficial to your company's future is to sponsor or get involved with a club or program at a local university.

Bear in mind it's not just employees' skills that are evolving, it's also their needs and wants. Failing to take these issues into account may mean you are running your company on a myth that can hurt its prospects. We'll explore these concerns in the following blinks.

With dynamic economies all over the world and countless possibilities to grab potential employees, you need to stop betting that people will stick around even if you don't treat them well.

The days when you could smugly assume your employees were lucky to have _any_ job are gone, so eliminating such assumptions is your first step toward drawing and retaining great talent.

### 3. Know the difference between recruiting and attracting people. As a company, become a better catch! 

For a relationship between a company and an employee to gel, there needs to be mutual attraction.

So how do you make your company irresistible to a potential employee?

Employee rewards and recognition are pivotal in attracting and retaining talent. For instance, the CEO of LinkedIn gave all the company's employees an iPad Mini to show his gratitude for their results.

Straight away, the news swept over Twitter and Facebook as employees gushed about their gift. This generated more genuine buzz than any recruiting advertisement could have.

Such gestures make others want to work for such an organization. But such a strategy isn't just about having a big budget. Start by acknowledging top employees by giving them a few hours off, or gifting something memorable but not so expensive, such as concert tickets.

Even modest offerings go a long way, as they make employees feel appreciated.

Aside from actively recognizing your employees, create a magnetic organization that attracts only the best talent by standing out from the crowd. Make sure your company's benefits satisfy those who don't just want to pay the bills but who also are seeking challenges and training opportunities in addition to professional and personal growth.

Instead of squandering 20 to 30 percent of annual revenue on job boards and recruitment events, create a domino effect of employee satisfaction!

The next time you pass an Apple store, take a quick look inside. You'll notice a _magnetic organization_ where employees and customers are smiling and energetic.

Apple's happy employees are the reason they don't have a problem recruiting top talent. Their employee satisfaction leads them to work better, which leads to satisfied customers, which loops back to positive energy and a highly appealing organization.

It's no wonder so many people want to work for Apple!

### 4. Maximize and bring to light the magnetism your company may already have. 

While you may be the best and most innovative company around, do people know? Image is everything, so working effectively to influence how people perceive your company is vital.

This means targeting your _employment brand._ Your _employment brand_ is made up of how your current and past employees perceive your company.

To have a strong _employment brand_ that draws in employees, you need to ask yourself some questions. What makes your organization attractive? What keeps the best talent at your company? What makes your company better than others? Do you have a creative company environment?

How would you describe your company: is it innovative? Intense? Relaxed? Rewarding? How do people outside the company describe your employees? 

Enlist the help of employees, customers or even vendors to answer these questions. Then compare the results with how you believe your company's culture should be.

To further promote your brand, go online. Glossy brochures communicating your brand image no longer cut it. You need to get sophisticated.

Communicate your brand through social media outlets and through your website. Put together a team of employees who represent your brand and ask them to work together to design recruitment pages on the site.

It's also worth budgeting for the resources you need to project the image you want. This could mean employing videographers, professional photographers and so on.

Next, create a job board on your Facebook page where resumes can be uploaded directly, and tailor your advertising campaigns to the location, education and interests of the kind of talent you want to attract.

Recruiting and attracting people through social media is fairly low risk and cost-effective. And if it turns out that another approach better fits your organization, it's easy to drop this plan.

### 5. Get in front of rivals today. Don’t wait to come up with a “plan B” when you really need it. 

Did you ever think how your recruitment strategy can determine whether you are triumphant over your competitors?

While competing companies are battling to find the talent to match their short-term goals, you can get ahead by employing the right people from the outset.

You should aim for a _sustainable work environment_, where your recruitment strategy not only meets your organization's long-term goals but also keeps your employees happy and loyal.

When recruiting new employees, keep in mind they need to possess a blend of knowledge and technical skills in addition to well-rounded skills, which are crucial for long term success.

It's easy to spot well-rounded skills when employees go the extra mile to please clients. So your goal is to get your employees to want to make this difference.

For instance, the Ritz-Carlton hotel chain has a policy which allows employees to spend up to $2,000 to encourage guests to feel completely at ease and happy with the hotel's service.

It's not necessary to be quite so lavish, of course; on a smaller scale, this might mean offering a $5 certificate and an apology in an effort to correct a mistaken food order at a restaurant, for example.

Instead of waiting to change until your business is flailing or your competitor ups the ante, take the upper hand now by investing in your staff.

Show your employees that you trust them and cultivate a safe, comfortable environment where they can develop personally and professionally.

One way to do this is to make sure they feel that you respect and value their efforts. Nobody wants to let people down who believe in them!

This not only benefits the organization, but comes back around and benefits your employees, too.

### 6. Final summary 

The key message in this book:

**There must be mutual attraction for a employer-employee relationship to work. Through offering attractive, tailored benefit packages to employees, employers can be confident that the best talent will seek the company out. By building a magnetic company, firms can also cut recruiting costs and benefit from a constant influx of motivated, qualified young recruits, proficient in adapting to modern technology and a changing economy.**

Actionable advice:

**Don't lie.**

One way to be a trusted employer is to tell the truth. For example, don't say that you're "laying off" people when you're firing bad performers. Layoffs that are actually a euphemism for firing is toxic to your reputation and makes it hard to rehire. If a staff member asks you if there will be cuts in the future and you think it's a possibility, don't lie. Say the situation is being looked into, as opposed to making promises you can't deliver on.

**Suggested further reading:** ** _Reinventing Organizations_** **by Frederic Laloux**

_Reinventing Organizations_ discusses why companies around the world are getting rid of bosses, introducing flat hierarchies and pursuing purpose over profit. And ultimately, by adopting a non-hierarchical model, these organizations _thrive_.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Roberta Chinsky Matuson

Roberta Chinsky Matuson is the president of Matuson Consulting and an expert blogger for _Fast Company_ and _Forbes_. She has advised companies such as Best Buy, Monster and Staples on recruiting talent and company growth.

