---
id: 54f5b530646133000a330000
slug: talk-lean-en
published_date: 2015-03-06T00:00:00.000+00:00
author: Alan H. Palmer
title: Talk Lean
subtitle: Shorter Meetings. Quicker Results. Better Relations.
main_color: 2FC2E4
text_color: 1A6A7D
---

# Talk Lean

_Shorter Meetings. Quicker Results. Better Relations._

**Alan H. Palmer**

_Talk Lean_ will teach you how to express your thoughts in a direct, candid, yet courteous manner. The author shares effective, easy-to-apply tips for having a productive conversation that helps achieve your goals.

---
### 1. What’s in it for me? Learn how to get what you want out of a meeting. 

We have so many meetings. Why do so many feel like a waste of time?

Whether it's a team meeting, a one-on-one with your boss, colleague, or employee, or even the conversation when a boy likes a girl and doesn't know what to say to her, we all struggle with saying what we want to say.

That's where these blinks about _lean talks_ come in. Talking lean means saying something clear, direct and to-the-point, in a polite, calm, courteous manner. That's the best way to stop wasting everyone's time and get the most of meetings!

In this blinks, you'll find out

  * when to start a sentence with "You" vs. "I";

  * why rhetorical questions can kill a meeting; and

  * how to make sure what you say gets across.

### 2. Start every meeting by politely expressing exactly what’s on your mind. 

How many times have you had an unproductive meeting just because you were afraid of expressing what was really on your mind?

Well, you can change that by announcing your intentions from the very first minute of the meeting. It's important you make your purpose clear early on; the longer it is unclear, the more likely the other person or people will start to wonder or get suspicious that there's something you're not saying.

Express your purpose in language that's straightforward and direct without being brutal or rude. That way, you're setting a polite, courteous precedent for the rest of the conversation.

For example, imagine a manager has a talented but habitually tardy employee. She keeps reminding him to be on time, but since her tone is aggressive, he leaves remembering that his boss was rude, rather than remembering her point.

What not to say: "Why on earth do you insist on arriving so late day in, day out?"

Something better: "John, what can I do to ensure your punctuality in the future, so that we can avoid future confrontations on this matter?"

The second way, John has to think about his actions and propose a plan for improvement, likely leading to a positive outcome.

Preparation is important if you want your meetings to follow a similar trajectory. Think about what you want from the other person, and then work your way back to figure out the best way to achieve it. Then, you'll be able to lead the conversation in a way that invites desirable responses from the other person.

In other words, ask yourself, "What do I hope will happen at the end of the meeting?" Use the answer to structure your opening comments. This way, you'll be able to communicate exactly what you're thinking and ensure that the other person is following your message from the get-go.

### 3. For a productive meeting, create an environment of openness and listen carefully. 

Now that you know how to successfully open a meeting, here's how to proceed.

Often we go into a meeting with thoughts and feelings we don't have the courage to express. Instead of saying what's on our mind, we make blunders or inadvertently communicate those unsaid ideas through gestures, facial expressions or body language.

In other cases, we inhibit other people, preventing them from sharing their own perspectives on the matter. Irony and rhetorical questions can often have the effect of allowing us to impose a view of superiority, diminishing others' interest in contributing.

That's why the best approach for any meeting or conversation is to speak from your own point of view. To that end, start sentences with the personal pronoun _I_, not _you_. For instance, instead of saying, "You are wrong," you could say, "I disagree with you."

It's also helpful to listen carefully to yourself and others. It's not just a matter of listening with your ears: pay attention to the language other people use and let your intuition guide you.

It's easier said than done, because most of us tend to best remember information that has an emotional impact. Take notes and jot down the exact words and phrases others say, rather than paraphrasing or putting into your own words. Having other people's words in your notes will allow you to later construct an accurate account of the meeting, as well as respond appropriately in the moment. And if you notice that you don't fully understand something the other person said, just ask!

Of course, as we mentioned earlier, it's important to listen to yourself, too. While you're writing down your colleagues' words, ask yourself how you feel. And if you notice any doubt or unclarity in yourself, you can bring it up and talk it out.

### 4. Respond appropriately. 

As we just learned, listening carefully is an important part of having a productive meeting. But that's not all: responding appropriately to what the other person says is crucial, to elevate the conversation to a higher level.

After all, you don't want your response to block further discussion. This often occurs when people respond in a negative way, saying things tantamount to "That sucks!" Those kinds of responses leave little room for the other person to expand on their statements or further negotiate.

Essentially, you need to find the right balance between making others feel more comfortable and making your own needs clear. To this end, it helps to pay attention to your sentence structure.

For instance, if you don't understand something the other person said, it's a good idea to use the past tense and the pronoun, _you_ : "What did you mean by what you just said?" or "What led you to agree to see me today?"

On the other hand, if you want to project a certain amount of forcefulness, state your opinion using the present tense and the pronouns, _I_ or _me_, using phrases like "I need," "I like" and "I want."

Also, if you're problem-solving, the other person may likely have a good solution.

You want your language to create an invitation for the other person to take a position, make a proposal or unblock a situation. Either way, it can be elegantly done in order to move a meeting forward.

Along those lines, if you want to identify a solution together, it's best to use the future tense and the pronouns, _we_ or _us_. For example, "What do we do from here?" or "If I [fill in the blank], what will you do to [fill in the blank]?"

Ultimately, as long as you choose your words carefully and respond appropriately, you'll eventually be able to resolve the problems or tasks at hand.

> _"Slow down, listen to yourself and then think out loud."_

### 5. Be smart about body language and end each meeting with quality control. 

Are you wondering why your conversation partner spent the whole time staring at the ceiling while you were making your opening remarks?

We all do our best to interpret human gestures, but even after years of study and practice, there's no hard science to body language.

Thus, you'll never properly surmise what someone's thinking based on his or her body language. So, just ask!

After all, by being open and curious about what's happening around you, you'll gain more respect and trust from the other people at the meeting. That clarity is conducive to productivity.

By the same token, other people pick up on our body language as well. Your gestures and facial expressions will reveal your true thoughts no matter how hard you try to hide them. That's why whenever we express an idea that we really believe in, our message has a tremendous impact on others.

On the other hand, trying to conceal your true thoughts with gestures that don't match your ideas will expose your lack of conviction. That will garble your message and make others suspicious.

As we've seen, nonverbal communication can make things confusing, which is why it's helpful to conduct a quality control at the end of your meetings, to check the impact of the thoughts and solutions discussed. Make sure your ideas have been fully understood and that everyone is happy with how the meeting went.

Conducting quality control is easy. Ask these simple questions:

  1. What do you think of what I said here?

  2. What did you think of the meeting?

  3. In a more personal discussion, you may ask: What do you think about me?

> _"However much you may wish consciously to control your non-verbal communication, you will never be able to control all of it."_

### 6. Final summary 

**The key message in this book:**

Productive meetings stem from having the courage to state openly what you want out of them. Try to listen carefully to what others are saying too, and avoid responding negatively, as this will only block further discussion. Also pay attention to what signals your body language might be sending.

Actionable advice:

**Define your meeting goal.**

Next time you have a meeting planned, take a few minutes to think about exactly what you want to achieve with the people there. Then find a constructive way of expressing this goal and do so at the very start of the meeting.

**Suggested further reading:** ** _Crucial_** **_Conversations_** **by** **Kerry Patterson, Joseph Grenny, Ron McMillan and Al Switzler**

We've all been in situations where rational conversations get quickly out of hand, and _Crucial_ _Conversations_ investigates the root causes of this problem. You'll learn techniques to handle such conversations and shape them into becoming positive and solutions-oriented, while preventing your high-stakes conversations from turning into shouting matches.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Alan H. Palmer

Alan H. Palmer is an Oxford graduate who previously worked in the international advertising industry. Today, he conducts seminars and develops training programs all over the world to help people have productive conversations.

© [Alan Palmer: Talk Lean] copyright [2014], John Wiley & Sons [Inc. or Ltd. as applicable] Used by permission of John Wiley & Sons [Inc. or Ltd. as applicable] and shall not be made available to any unauthorized third parties.

