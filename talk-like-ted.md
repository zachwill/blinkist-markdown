---
id: 54100f7b3635620008010000
slug: talk-like-ted-en
published_date: 2014-09-09T00:00:00.000+00:00
author: Carmine Gallo
title: Talk Like TED
subtitle: The Nine Public-Speaking Secrets of the World's Top Minds
main_color: B63C24
text_color: B63C24
---

# Talk Like TED

_The Nine Public-Speaking Secrets of the World's Top Minds_

**Carmine Gallo**

In _Talk_ _like_ _TED_ (2014), you'll learn about presentation strategies used by the world's most influential public speakers. Author Carmine Gallo analyzed more than 500 TED talks to identify common features that make these talks so influential and appealing.

---
### 1. What’s in it for me? Learn the secrets to presenting like a pro. 

If you have a great idea, how do you share it with the world? Every day we are bombarded with information, so for any one idea to really shine, it needs to be pushed hard by the person who owns it. In other words, you have to _sell_ it.

In _Talk_ _Like_ _TED_, author Carmine Gallo reveals the presentation secrets of some of the world's most influential public speakers. Having analyzed over 500 popular talks presented at TED (Technology, Entertainment, Design) conferences, Gallo identifies the three common features that, he argues, distinguish those talks from the rest.

In all of the talks, the content of the presentations was _novel_ ; the speakers managed to _connect_ _emotionally_ to their audience; and the content was presented in a way that made it _easy_ _to_ _remember_.

You'll learn how to connect emotionally with an audience, make your presentations more memorable and weave stories into your talk. Also, you'll find out why passion is essential to making your talk persuasive, and why you should make sure your subject stimulates more than one of our five senses.

Finally, you'll learn a number of bread-and-butter tips on how to structure your presentation to make it stick in your audience's memories.

In the following blinks, you'll also discover

  * how unleashing a swarm of mosquitoes helped a TED talk go viral;

  * what the American Dream and the country of Denmark have in common; and

  * how new information stimulates the brain into remembering it.

### 2. TED talks can help you to improve in an important area of life: your presentation skills. 

In 1915, Dale Carnegie wrote the world's first self-help book, _The_ _Art_ _of_ _Public_ _Speaking._ In this work, Carnegie explained how people could get their point across in the most memorable and engaging way.

This suggests that even 100 years ago, people had to find a way to distinguish themselves from the crowd to succeed. And if this were true a century ago, it is especially so today.

In our globalized world, competition is tough. To make your idea stand out, you have to learn how to sell yourself.

The author Daniel Pink is well aware of this, claiming in his book _To_ _Sell_ _is_ _Human_ that today we're _all_ in sales, whether or not we want to be.

But where can you learn how to best sell yourself? What's the contemporary world's equivalent of _The_ _Art_ _of_ _Public_ _Speaking_?

The TED talk.

TED (Technology, Entertainment, Design) is a popular conference for leading thinkers and inventors to present their ideas. Each presentation, all of them freely available to watch online, is a model example of effective selling skills.

Started in 1984 as a one-off event, TED's popularity grew rapidly. Today, TED has a global presence. In fact, every single day, five TEDx events (an international franchise of the original event) take place in more than 130 countries.

Because TED talks feature some of the brightest and most successful people in the world, they're a valuable resource for anyone wanting to become a better public speaker. With that in mind, the author analyzed over 500 TED talks and was able to identify many of their common features.

But what are these commonalities? What are the skills that TED presenters use to get their message across? The upcoming blinks present the most important tools used by the best sales people from all over the world.

> _"There is nothing more inspiring than a bold idea delivered by a great speaker."_

### 3. Passion is the foundation of a persuasive and successful presentation. 

What do the most successful people in the world have in common?

Passion is the intense, positive feeling you get from pursuing activities that are deeply meaningful to you. Furthermore, pursuing something you are passionate about is essential to achieving success in that field.

Consider Tony Hsieh, founder of the successful online shoe retailer Zappos. Hsieh is extraordinarily passionate — but not for shoes. Rather, his passion is for making his customers and employees happy. This passion for increasing the happiness of others has helped to grow Zappos into a company famous for its outstanding customer service, and into a great working environment for its employees.

In addition to being the foundation of success, passion is also essential in giving excellent presentations.

In 2012, a group of researchers wanted to understand why investors give money to some start-ups and not to others. In their study, they observed how investors made their decisions as they evaluated the 15-minute presentations of several start-ups seeking funding.

As the researchers discovered, one of the crucial factors aiding investors' decisions was the passion conveyed by the presenters. In fact, this aspect was even more important than the education, experience or age of the entrepreneurs.

But what if passion doesn't naturally flow through your veins? Fortunately, anyone can learn to be a passionate speaker. It simply requires _practice_.

The human brain changes constantly in response to the input it receives. Indeed, a study of London cab drivers found that the region of the drivers' brains associated with navigation skills — the hippocampus — tended to be larger than average, explaining their above-average ability to plot the quickest route from A to B.

In the same way, if you put yourself in a position where you have to speak passionately on a regular basis, your brain will adapt to the task, and you'll improve at it.

So, what are you waiting for?

> _"People cannot inspire others unless and until they are inspired themselves."_

### 4. Storytelling helps you to connect emotionally with your audience. 

Most people would agree that Steve Jobs was one of the world's best public speakers. But what made his presentations so persuasive?

His talks were always brimming with _pathos_ — as _all_ successful, persuasive presentations are.

One of the first people to think deeply about persuasive communication was the Greek philosopher Aristotle. Aristotle believed that persuasion occurred only when three elements — _ethos_, _logos_ and _pathos_ _–_ overlapped.

The first element — ethos — touches on your character or values. It refers to your experiences and/or education, both of which can increase your audience's trust in your viewpoint.

The second — logos — refers to the logical basis of your argument, which can include statistics or other data to help convince your audience of the concrete reasons for your point of view.

The third element — pathos — is concerned with the emotional connection you make with your audience.

In the author's analysis of hundreds of TED talks, he found that the most popular presentations consist of 65 percent pathos, 25 percent logos and 10 percent ethos.

Clearly, pathos is the most essential feature of a persuasive presentation. But how do you inject more pathos into your talk?

The best way is through _storytelling_.

In general, storytelling helps you to connect with your audience by making your presentation less abstract and more identifiable. There are three types of stories that can help you accomplish this.

The first is the personal story. A powerful personal story is one that answers a question such as, "What's your earliest memory of childhood?"

The second kind is a story about other people — such as a story about a friend whose first idea for a start-up failed miserably, but whose next idea attracted many investors.

The third type is a story about successful brands, companies or organizations. In one TED talk, Ludwick Marshane — the inventor of DryBath, a skin gel that cleans without water — entertained the audience with the tale of how his brand helped people in countries suffering from water shortages.

### 5. An emotional connection happens only when a speaker’s voice, gestures and body language are in sync. 

Have you ever listened to a talk where the speaker spoke so slowly that you found yourself drifting off to sleep?

Yet speaking faster is not necessarily the solution. The rate at which a speaker should talk depends on what you — the listener — are doing at the time.

Say you're doing something else — such as driving a car — while listening to an audiobook. In this case, you'd want the narrator to speak relatively slowly. However, if you're able to devote your full attention, as in a presentation, you'll want the speaker to talk more quickly.

What's the perfect speed for a presentation? The author found that most speakers use around 190 words per minute.

However, mastering your voice is just the beginning. Connecting with your audience requires also paying attention to your body language.

We gain a lot of information from how we move. In one study, students were presented with either a video or an audio recording of the testimonies of criminal suspects, and were asked to decide which of the suspects they thought were lying.

Those students who only watched the video were far more likely to correctly identify the lying suspects (a 65 percent success rate) compared with those who only listened to the audio recordings (55 percent).

So, what sort of body language should you use?

US Commander Matt Eversman suggests that a leader should stand straight, displaying confidence, at all times. The same applies to public speakers: they should stand tall in front of their audience, demonstrating confidence in their ideas.

Another important feature of body language is _gesture_. Indeed, certain studies have associated a speaker's gestures with the amount of confidence an audience places in that speaker.

To use gesture effectively, you should limit your gesturing to the area between your eyes and your belly button, as gestures in this area have the greatest impact.

Also, save your most expansive gestures (such as spreading your arms at their widest) for emphasizing only your most important points.

So far, we've looked at how you can establish and maintain an emotional connection with your audience. Now we'll turn our attention to how you can make your presentation stand out.

### 6. To make your presentation surprising and unforgettable, give your audience new information. 

Think back to the last time you were genuinely surprised. This "Wow! Really?" moment probably caused you to pay even closer attention to the source of this new information — whether it was a book, a TV program or a lecture.

If you want to capture the attention and imagination of your audience, you should weave novel and surprising information into your presentation.

In one popular TED talk, the deep-sea explorer Robert Ballard presented persuasive arguments for why more money should be spent on deep-sea research.

Among many other facts that he shared, two in particular were attention-grabbing.

The first was that the _annual_ budget of the National Aeronautics and Space Administration (NASA) is equivalent to the National Oceanic and Atmospheric Administration's (NOAA) budget for _1,600_ _years_.

The second fact was that the greatest mountain range on Earth lies not above ground, but beneath the ocean.

New and interesting information makes people sit up and take notice. And the information can be remembered more easily, too.

That's because memory is dependent on dopamine, a chemical in the brain. When you learn something new, your brain releases dopamine which functions like a "save" button. The more novel and exciting the information, the more dopamine is released, and thus the greater our ability to remember it.

In another example, Susan Cain gave a TED talk in which she discussed the power of introverts, a topic that on the surface might seem somewhat boring or forgettable.

However, Cain knew she had to shake up her audience. So she told them that there was "no correlation between producing good ideas and being a good talker."

Immediately, the executives in her audience took notice: Cain's statement shattered their common belief that people who speak up the most in meetings are the most creative (based on the fact that such people are more likely than quiet introverts to be noticed in meetings).

Because she'd formulated her idea in a way that was new for her audience, she increased the possibility of her audience remembering it.

### 7. Make your presentation memorable by sharing an extreme moment or extraordinary statistic. 

What were you doing on September 11, 2001? What about September 11, _2002_?

Without a doubt, your memory of the former date will be far more vivid. Extreme moments are unforgettable — and if such a moment happens during your presentation, chances are your audience will remember your talk, and spread the word to boot.

In 2009, Bill Gates delivered a TED talk that subsequently went viral. His presentation even caught the attention of NBC News anchor Brian Williams, who — though it's unusual for such events to be treated as "news" — mentioned Gates' talk on the air.

What was it about this particular talk that made such an impression?

Gates' presentation was about how fatal diseases such as malaria are transmitted via mosquitoes. As he held up a jar filled with live mosquitoes for his audience to see, Gates said that he saw no reason why only poor people should be at risk of being infected this way — then he opened the jar and set the mosquitoes free!

Although Gates quickly added that these particular mosquitoes were free of malaria, his extreme action caused his talk to go viral. The presentation chalked up 2.5 million views on the TED website, and a Google search for the talk returns some 500,000 results.

But it's not only extreme actions that can distinguish your presentation from the rest. Shocking statistics also can grab your audience's attention.

While you prepare your presentation, it's worthwhile searching for interesting facts or statistics that illustrate your argument.

Here are two examples from popular TED talks:

"In 1972, there were 300,000 people in jails and prisons. Today, there are 2.3 million. The United States now has the highest rate of incarceration in the world." (Bryan Stevenson)

"One in 100 regular people is a psychopath. So there's 1,500 people in this room. Fifteen of you are psychopaths." (Jon Ronson)

Both of these statistics made the talks they were included in far more memorable and interesting.

### 8. Adding humor to your speech makes your audience see you in a more positive light. 

Think back to the last presentation you truly enjoyed. Was it at all funny?

Humor has a positive effect on our relations with others. Indeed, studies show that we attribute such desirable and positive traits as friendliness, intelligence and emotional stability to people who have a good sense of humor.

In a similar way, humor can play a crucial role in business settings or during a presentation.

In one study published in Harvard Business Review, humor was shown to reduce hostility, relieve tension and improve morale among colleagues.

Another study that examined the differences between average and outstanding business executives demonstrated that, on average, those executives ranked as outstanding used humor more than twice as often as did average executives.

Given the power of humor, it makes sense that you should learn to incorporate it in your own presentations. There are several ways to do this.

One approach is to share anecdotes. Perhaps something funny happened to you earlier in the day that you can talk about? It's not important to go for a big laugh; instead, your aim should be to elicit a smile and a chuckle from the audience.

In one TED talk, Dan Pallotta, who'd founded an organization to raise money for AIDS services and medical research, commented on his role at home. Being openly gay and a father of triplets, Pallotta suggested that his family life was by far the "most socially innovative and most socially entrepreneurial thing" he'd ever done.

Another approach is to use analogies and metaphors.

In a talk about the negative societal effects of economic inequality, University of Nottingham professor Richard Wilkinson described how Denmark has a low level of inequality and a healthy, happy population.

Not exactly the kind of material you'd expect to get a laugh with. However, Wilkinson found one with this incisive observation: "Americans who want to live the American dream should go to Denmark."

Now that you've learned how to distinguish your presentation, the following blinks will show you how to create presentations that will be remembered.

### 9. Presentations should cover no more than three aspects in 15 to 20 minutes. 

Ever noticed that after sitting through a long talk you feel exhausted and physically depleted? Your audience may face the same problem.

The solution? Keep your presentation short. This makes it far easier for audiences to remember the content.

Take, for example, Paul King, a professor at Texas Christian University, who breaks his weekly three-hour class down to three single sessions, each lasting 50 minutes.

The result? The students retain more information and often score higher on exams.

Presentations at TED conferences usually take 18 minutes, which is considered a good length as it fits perfectly in the optimum period of 15 to 20 minutes.

In addition to keeping your presentation short, keep in mind that it should not cover more than three separate themes.

Why?

In 1956, a researcher from Harvard discovered that most people have little trouble remembering seven pieces of new information.

Yet since then, researchers have revised this theory and have divided that figure into three or four basic information units — or "chunks."

For example, the number 2,222 is far easier to remember than 3,948.

The first number describes one chunk of information (the "2") and the second, _at_ _least_ two chunks (39 and 48).

So, the fewer the chunks, the easier it is to remember them — which is why a presentation should cover no more than three aspects.

These aspects can be organized in a _message_ _map_. To do this, you must first answer the question: "What's the single most-important message I want my audience to take away?"

Once you've got your answer, write that message at the top of a piece of paper — like a headline.

Next, you have to find the _three_ (or fewer) messages which support your headline message, and list them underneath it.

Finally, beneath each of these three supporting messages, you can outline their specific content: the "meat" of your presentation.

### 10. Stimulating all of the senses during a presentation helps your audience to remember your ideas. 

Think about the last time you were sunbathing. How did your skin feel? What did you smell? What did your surroundings look and sound like? How well can you remember all the details?

We remember things more vividly when we experience them with all of our senses.

Richard Mayer from the University of California at Santa Barbara sees the relation between multisensory stimulation and an improved memory as the next hot field for future research in cognitive psychology.

In Mayer's experience, students exposed to multisensory environments (videos, texts and images) are better able to recall information than students who receive information through a single sensory channel (seeing, or reading a text).

It follows that if you want to create memorable presentations, you should communicate in a way that appeals to more than just one sense. Here, we'll look at how to communicate information via the two central senses: sight and hearing.

There are basically two ways to communicate information via sight: pictures and text.

You've probably noticed that the best TED talks use pictures rather than PowerPoint presentations that are often dense with text.

This makes sense, as we have a limited capacity for absorbing information. Thus, a PowerPoint presentation filled with words can overwhelm and distract your audience.

Instead, use pictures to support your presentation, paired with a few focused keywords that support your argument.

Our sense of hearing can be stimulated by rhetorical devices, such as repetition.

Take Martin Luther King's famous speech, in which he repeated these four words: "I have a dream." These words are still remembered today, and we immediately associate them with King.

A more recent example is Barack Obama's famous phrase, "Yes, we can," which helped unite the electorate around his message and get him elected as president of the United States.

### 11. Final summary 

The key message in this book:

**The** **ability** **to** **present** **ideas** **in** **a** **persuasive** **way** **is** **one** **of** **the** **core** **skills** **needed** **in** **the** **twenty-first** **century.** **When** **you** **deliver** **a** **presentation,** **it's** **important** **to** **make** **it** **stand** **out;** **to** **do** **so,** **you** **need** **to** **connect** **emotionally** **with** **your** **audience.** **And** **if** **you** **want** **your** **audience** **to** **remember** **your** **talk,** **keep** **it** **short,** **cover** **no** **more** **than** **three** **themes** **and** **appeal** **to** **your** **audience's** **senses.**

**Suggested** **further** **reading:** **_The_** **_Presentation_** **_Secrets_** **_of_** **_Steve_** **_Jobs_** **by** **Carmine** **Gallo**

_The_ _Presentation_ _Secrets_ _of_ _Steve_ _Jobs_ explains how any presenter can be as convincing and inspiring as the legendary Steve Jobs. From planning to rehearsal and delivery, Carmine Gallo details the anatomy of a great presentation.
---

### Carmine Gallo

Carmine Gallo is a former anchor and correspondent for CNN and CBS as well as the author of a number of books, including _The_ _Presentation_ _Secrets_ _of_ _Steve_ _Jobs_ (also available in blinks) _._ He has worked as a business executive coach for for companies such as Coca-Cola and Intel, and is often a keynote speaker at major conferences.

