---
id: 56a6b069fb7e870007000033
slug: talking-to-crazy-en
published_date: 2016-01-29T00:00:00.000+00:00
author: Mark Goulston
title: Talking to Crazy
subtitle: How to Deal with the Irrational and Impossible People in Your Life
main_color: F25330
text_color: BF4226
---

# Talking to Crazy

_How to Deal with the Irrational and Impossible People in Your Life_

**Mark Goulston**

_Talking to Crazy_ (2015) acknowledges that each person has the potential to be a little crazy, giving into irrational behavior when the mood strikes. These blinks offer sound advice on how to empathize and communicate with a person in "crazy mode" so you can keep yourself from going off the deep end, too.

---
### 1. What’s in it for me? Learn to cope with craziness. 

Are you crazy? Consider this scenario. You're having a conversation with a loved one and out of nowhere, he says something that ticks you off. 

How dare he say that, you think. And you start to shout, perhaps even go completely bananas, as your pulse races and your face flushes. After your outburst, you need a full hour just to calm down. 

So, are you sure you're not crazy? How do you then explain such behavior? 

These blinks explore the crazy, irrational behavior to which everyone falls prey, every now and then. But why exactly do we act crazy, despite thinking we're fairly rational adults? And maybe more importantly, how do you deal with a crazy person, especially when that crazy person is yourself?

In these blinks, you'll learn

  * how to identify what triggers crazy behavior;

  * how to handle someone who simply won't take "no" for an answer; and

  * why some craziness is too crazy for you to handle alone.

### 2. Accept that everyone – even yourself – can sometimes act or simply be a little crazy. 

Even the most seemingly rational people have moments when they're surprised by their own irrationality. 

Really, anyone can have a moment in which the following statement holds true: You're crazy!

But what does "crazy" in this context really mean? We're not talking about the mentally ill, people who may be called "crazy" unfairly. You don't have to be clinically ill to have a period of craziness. 

In these blinks, the term _crazy_ refers to irrational behavior which can manifest in a few ways. A crazy person might have a distorted view of reality, or refuse to listen to reasonable arguments. This person could also say nonsensical things or act out against his own interests.

If you want to reduce this kind of behavior in yourself, the first step is admitting to your own craziness. Acknowledging your own issues is an important step in dealing with other crazy people!

When you understand your own crazy tendencies, you'll be able to empathize with the craziness in others. Because if you can't empathize with those people, you might just end up screaming or acting irrationally yourself. And who's the crazy one, then?

If on the other hand you can keep your cool when things get nuts, you might just help calm a crazy person down. Instead of getting upset, try to understand what exactly triggered the person's craziness. You'll then be less vulnerable to that person's attempts to drive you batty!

For example, imagine you have a problem where you completely shut down if someone questions your honesty. If you acknowledge this tic, you'll recognize it in others who might have a similar reaction. You'll then know how to work toward finding a solution, instead of making things worse.

> _"Unless you're the first entirely sane person on the planet, you're carrying around your own suitcase full of crazy."_

### 3. When a person goes into crazy mode, try to identify the trigger and empathize with him. 

You might not know how to respond when a person starts acting crazily. But here's an important point: don't try to argue. Arguing never helps.

When a person is having a crazy episode, it's usually because of some major event in that person's past. A lot of craziness, for example, is rooted in childhood experiences.

Individuals who didn't receive enough love while young, for example, tend to be more pessimistic. If you try to convince such people of the value of a certain idea, for example, they might insist your idea won't work, no matter how reasonable the idea or how convincingly you argue. 

We all have baggage like this. No amount of arguing can bring a triggered crazy person back to sanity. A person who's acting crazy just can't think clearly enough to listen to a logical argument. 

Instead of arguing, then, aim to identify the _modus operandi_ of the person's craziness. 

A person's _modus operandi_ is the specific thing he does when a period of craziness kicks in. The person might scream, launch into a personal attack or start ignoring you completely. Look for patterns in the person's crazy behavior, and try to uncover the exact trigger.

Once you've identified the modus operandi, try to empathize with the person. Attempt to put yourself in his crazy headspace by imagining what he's feeling. In this way, you'll be able to talk him back into a saner state. 

So when someone is being irrationally pessimistic about an issue, for example, don't just list the reasons why the issue will be resolved positively. First, you have to imagine you're in a world where _nothing_ works — then build your reasoning from there. 

All in all, communicating with a crazy person is a challenge — but it's certainly better than letting that person drive you nuts!

### 4. Don’t ever engage in a power struggle with a crazy person. Instead, empathize but set boundaries. 

Why do people snap into crazy behavior in the first place? These episodes are often about control. 

A person's craziness may well be rooted in his past, but such people often find themselves acting out because they want to feel control. 

A person might start shouting when he loses control or is pushed out of a conversation, for instance. Yelling is just a tactic to try to overpower the other people involved and regain a sense of control.

You can avoid these sorts of power struggles by cooperating with a crazy person instead of trying to overcome his brash tactics. There are a few ways to do this. 

The first strategy is to let a crazy person stay in charge. If you let him have the control he seeks, he'll be less motivated to continue acting crazily. 

The author once triggered a crazy person by accidentally cutting him off while driving down a road. The crazy driver then forced the author to the side of the road, and came at him in a physically threatening manner. Instead of showing he was willing to fight back, the author explained that he'd had such a bad day, he'd been waiting for someone to come kill him. The driver must be his guy!

Another approach at defusing a crazy situation would be to apologize, remain empathetic and try to preempt a crazy tirade by saying the angry things the crazy person might want to throw at you first. Beating the crazy person to the verbal punch in essence undercuts his need to be passively aggressive, and allows you both to move on.

So if your dog barks all night long, go visit your crazy neighbors before they visit you and tell them that you'd certainly feel terrible if a neighbor's dog kept you up at night, too. Tell them you'd be upset with that neighbor. And be specific.

It's a good idea to establish clear boundaries with crazy people. If a person shouts at you on the phone, for instance, tell him you'll hang up if he continues shouting. He'll realize that there's a line he can't cross, and as a result he'll be less likely to shout on the phone in the future.

### 5. Don’t shout at the crazy on the outside, try to connect with the sane person on the inside. 

It's easy to forget that craziness is a temporary state when you're confronted with a person who's screaming at you. But remember: no matter how crazy someone appears to be, part of that person is still sane. Tapping into that sanity is the key to calming him down.

So when a loved one drifts into craziness, remember that he's still a good person. His craziness doesn't define his entire being; he still has an inner sanity that is really listening to you. So speak to that sane person inside, and not the crazy wrapper! 

You can address a person's inner sanity by reminding him of something mundane, like cooking dinner. That might get his mind off whatever he's stressing about at the moment. 

Then wait until he's calmed down, and only then, ask about the earlier outburst. You can even ask for advice on how you should react to his crazy behavior in the future. 

You need patience to deal with a crazy person. You have to wait until the person's in a state where you can actually connect with them. A person in crazy mode will often lash out in any way he can — the goal is not to be constructive or even moderately rational. He might even scream about how much he hates you, or say that you're the worst thing that ever happened to him. 

Logical, rational people often respond to such behavior by removing themselves from the situation. If you're considering this, remember that crazy people often don't mean what they say. So think twice before ending a friendship or relationship over a bout of temporary craziness. 

Instead, wait until the person calms down and ask him if he really meant what he said during his crazy episode. More often than not, the answer will be "no."

> _"No one is totally crazy."_

### 6. Manipulators are often motivated by disappointment or anger, so help them deal with it. 

Craziness manifests itself in many forms. Screaming or crying is easier to identify, but manipulation is a kind of craziness, too. 

People often try to manipulate others because they refuse to take "no" for an answer. They might try to split two people apart as a way to vent anger, for instance. 

Imagine a teenager whose mother tells him he can't have pizza for dinner. He might respond by driving a wedge between his parents, running off to badmouth his mother to his father. When a friend calls you in the middle of the night to vent about a mutual friend, this is similar behavior.

Manipulators do this because they're hoping to change that initial "no" into a "yes." But don't let yourself be manipulated. Be clear with these crazy people, but maintain a gentle approach.

One way to deal with attempted manipulation is to tell the crazy person you're interested in what he has to say, yet you'd like to speak with the third person who's being badmouthed to discuss the issue.

Point out that the manipulator may be overreacting, or obsessing over getting what he wants. Don't get angry, however. Anger is always counterproductive.

When a person is afraid of getting a "no," he might also react by refusing to ask for help — or even refuse help, if you offer it first. A crazy person might act this way because he's been rejected when he needed help in the past, and is afraid of that happening again.

Deal with this kind of crazy person by directly telling him to ask you for help when he needs it. And check in with him regularly to make sure everything is okay, because he might not tell you if it's not.

### 7. Catch narcissistic and sarcastic people off guard with unexpected behavior or comments. 

We've learned that craziness is often motivated by a desire for control. Control is a big factor for another group of crazies, too: the _know-it-alls_.

There are a few ways to deal with a know-it-all. It might seem counterintuitive, but a good strategy is to boost a know-it-all's ego even further. This lessens the person's need to act superior. 

Don't alienate a crazy person who's exhibiting narcissistic behavior. Do the opposite of what the person would expect: agree with them. Lend some validation to his superior feelings.

So if a person starts belittling others to feed his own ego, tell him you feel lucky to have him on your side. Then once you've got his attention, explain how his know-it-all behavior may actually push people away.

This kind of crazy narcissism has some parallels with crazy sarcasm. Like narcissism, a crazy person's sarcasm is meant to be a defense mechanism, but usually just alienates others.

Think about the sarcasm of teenagers. Teenagers often push their parents away by making too many sarcastic remarks — but they don't do it out of hatred. Teenage sarcasm is usually just a reaction to stress. It's a way for teenagers to shield themselves.

If you know a chronically sarcastic person, aim to understand the sarcasm so you can predict when it might pop up. And when the person makes a sarcastic comment, throw him off by tossing the sarcasm back at him. 

Let's say you have a sarcastic boss who is always questioning your professional capabilities, for example. If he says something like, "I don't know why I'm asking you, of all people," respond with "Yeah, me neither."

A comment like that will grab his attention and perhaps even rattle his cage a bit. Only then can you talk to him in a sane fashion.

### 8. Respect and cooperate with your partner, even when you’re splitting up. Do it for the kids! 

Love is hard to find, but easy to lose. Love can also be torn apart by crazy behavior. Fortunately there are some tactics for protecting your relationship from crazy, irrational actions.

People often explain a breakup with off-handed comments like, "She demanded too much," or "He always stayed out late," but such problems are rarely the real reasons for a relationship's end. 

The true culprit is usually a couple's inability to deal with problems in general.

So when your partner makes a mistake, don't just get angry. Become your partner's _sponsor_ instead. This means committing to sharing daily hardships, frustrations, setbacks and victories with your lover. Support your partner no matter what. Becoming each other's sponsor is a good way to revive a relationship when it starts to founder. 

And if you do have to separate but have children together, don't let the process hurt them. Revitalizing a dead love is hard, but accepting that it's not possible is even harder. If you feel tempted to go on a crazy spree, remember the individuals who should always come first: your kids. 

Many couples become hateful or cruel when they go through the process of getting a divorce. They lose themselves in heated arguments or engage in custody battles. Such behavior is detrimental to the mental health of your children, so stop yourself and take a step back before crazy sets in.

You and your partner have to remain respectful and cooperative, even if you disagree on what's best for your children. Your goal shouldn't be to win custody of your child. Your goal should be to protect the child from getting scarred by your divorce.

### 9. When a loved one shuts down and won’t communicate, try to voice your partner’s thoughts instead. 

It's hard to deal with a crazy person who is screaming and yelling. However, it's also hard to deal with the opposite: a person who shuts down and won't tell you what's going on in his head. 

When a loved one behaves like this, it's important to voice the crazy thoughts you suspect he might be having. Feelings — especially negative feelings — can be overwhelming. We often look for ways to let our feelings out, but sometimes these feelings are so bad we bottle them up instead.

When a person suppresses negative feelings, that person ends up taking those feelings out on others — such as their partner, for instance. 

Imagine two parents who have a disabled child. They're so afraid of admitting that they might be angry with their child that they start acting crazy with each other. 

If your partner starts acting crazy, don't take it personally. Say what you think your partner is thinking and ask your partner if you're correct. The ensuing conversation might be difficult, but it's an important first step toward healing the crazy. 

Another strategy for getting your partner to say what's on his mind is to ask him to repeat things after you. 

Some people shut down when things are tough, so help your partner out by speaking a thought that you think is on his mind, and asking him to repeat it. He might be taken aback by the request, and hopefully, it will jump-start a conversation.

This strategy is also useful because it shows your partner how difficult silence is for you. When your partner sees how much you mean to him, he might make more effort to include you in his world.

### 10. A person with a real mental illness needs professional help; you can’t handle this alone. 

So far we've learned how to coax a crazy person out of a period of craziness. However, it's important to understand that sometimes this isn't always possible. 

If a person has a real mental illness, he will need professional help.

You can't cure someone with a serious mental illness, no matter how gentle and loving you are. A true mental illness is more than just temporary irrational behavior. If a loved one has a personality disorder or is suffering with addiction or suicidal thoughts, you can't address these problems alone.

There are different types of professional help available to those who need it.

_Medical-oriented psychiatry_ is essentially a form of first aid. It aims to stabilize a person so further steps can be taken. 

_Psychotherapy_ helps people deal with problems over a long period of time. Therapists help patients find coping mechanisms to change their view of life.

_Counseling_ is similar to psychotherapy, but less intense. Counseling sessions are shorter and less frequent than therapy sessions, for example.

_Psychosocial rehabilitation_ aims to prevent a patient from falling back into the illness or cycle of destructive behavior. It also helps a person deal with the challenges of re-entering normal life. 

Finally, _mentoring_ supports people by offering them the encouragement and motivation they might need to fight their mental illness or disease.

It can be hard to convince a mentally ill person to seek help, but you might succeed if you stay empathetic and establish a strong connection with that person. 

So always listen to what the person has to say. Aim to understand his feelings and see the world through his eyes. You should also agree with him as much as possible — tell him you'd probably feel the same way if you were in his situation.

### 11. Final summary 

The key message in this book:

**Virtually everyone gives in to bouts of craziness. So when you have to deal with a crazy person, try to empathize and avoid a power struggle. Stay respectful, deflect sarcasm and help the person voice his thoughts if he has trouble doing so himself. Your ultimate goal is to talk the person back to sanity, so you can have a productive conversation. And remember: if the person has a real mental illness, don't try to fix the person on your own. Encourage your loved one to seek professional help instead.**

Actionable advice:

**When a person's in crazy mode, look at his left eye.**

The left eye is connected to the right side of the brain, which is the more emotional side of the brain. Focusing on the person's emotional side might help you form a better connection, and it'll also keep you focused on your goal of finding sanity while the crazy person vents his anger. 

**Suggested further reading:** ** _Just_** **_Listen_** **by Mark Goulston**

_Just_ _Listen_ combines time-tested persuasion and listening techniques with new methods to help you get your message across to anybody. By learning how to be a better listener, how the brain works and how people think, you'll be able to motivate people to do what you want because you'll better understand their needs.
---

### Mark Goulston

Mark Goulston is a psychiatrist, business advisor and coach. He's also the author of the bestselling books, _Just Listen_ and _Get Out of Your Own Way_.

