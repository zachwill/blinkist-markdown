---
id: 584abb8666f3e20004cb68f5
slug: targeted-en
published_date: 2016-12-27T00:00:00.000+00:00
author: Mark Smith
title: Targeted
subtitle: How Technology is Revolutionizing Advertising and the Way Companies Reach Consumers
main_color: D52B3F
text_color: D52B3F
---

# Targeted

_How Technology is Revolutionizing Advertising and the Way Companies Reach Consumers_

**Mark Smith**

_Targeted_ (2014) takes you on a journey from the early days of internet advertising to today's complex online ad exchanges. On the way, the author reveals the vital importance of harnessing the power of Search Engine Marketing or SEM to target the consumers you need to make your company grow.

---
### 1. What’s in it for me? Get a glimpse into the secret world of modern advertising. 

When advertising and modern psychology came together in the 1950s, a new, more potent kind of advertising was born. At this point, advertising started getting under our skin. Consumers were no longer just sold a product; they were sold an _idea_ and a whole lifestyle based on their innermost desires. But, at the same time, they were all largely sold the same idea and lifestyle.

Since then, another revolution has taken place — the merging of modern technology with advertising. The one-message-for-all model is long gone. Advertising today is more sophisticated than ever before, targeting the individual directly.

But how does all this work behind the scenes?

In these blinks, you'll learn

  * how to increase your Click Through Rate;

  * how Bill Gross revolutionized online advertising; and

  * why you need cookies to attract customers.

### 2. Targeting is the art of giving the right person what they want at the right time. 

In the past ten years, computers, the internet and smartphones have created a digital revolution that has transformed our global economy. We now have the power to search for anything we want, anytime and anywhere.

This immediacy has created a new pressure on business to provide products to the right person at the right time — for the right price.

In one word: _Targeting_.

If a company is going to be successful in the digital economy, it must learn how to target.

This is because the variety of products on the market has exploded. Modern consumers have a clear idea of what they want, and won't accept a product that is _nearly_ right. But even if a company has created the perfect product for the consumer's needs, it must also make that product easy to find, or else no one will buy it.

This is done via _SEM_ or Search Engine Marketing. SEM means making the website that promotes a product as easy to find as possible so that the product is sold and consumers fulfill their needs.

This is done in two ways. First there is _SEO_, or Search Engine Optimization, in which strategic web design optimizes the chance of placing a site high in the search rankings. Then there's _paid search_, which we'll look at in the next blink.

This new world of SEM has opened up a whole new market for advertisers. By combining information collected from search histories and cookies, advertisers can provide ads that pinpoint what consumers want to buy. And if they can acquire space on a high-traffic website like Google — either a high search ranking or a paid search ad — advertisers can offer millions of people exactly what they want.

### 3. Paid search is a great way to attract potential customers. 

We've seen how important SEM is for companies who want to reach customers effectively. Now let's take a deeper look at the different types of _paid search_, and how they work.

Paid search ads attract consumers to a website. They can take many forms: pop-ups, images, banners and videos, but also the use of paid keywords that bring up ads in a search engine.

Paid search is big business. In fact, it's the biggest part of online advertising, with $19.9 billion spent on it in the United States in 2013.

Paid search is valuable because it directly connects people to what they want. This means that when someone searches for something, and then clicks on a paid ad, it's more likely that they'll actually buy the product. The Click Through Rate (CTR) of paid-search ads — that is, the percentage of people who click on the ad — can reach 10 percent. While that might not seem high, compare it with the CTR of ads shown when the customer doesn't search for anything: 0.1 percent!

However, paid search has some drawbacks. Its popularity has made it very expensive, with competitors battling over the top keywords. For example, the _New York Times_ reported that demand has pushed the price of the keyword "life insurance" from $1 in 2002 to $20 in 2012. And that's not the cost of keeping the ad on the website for a day or a week: that's the price of a single click!

Furthermore, devious businesses have found ways of using paid search against the competition. For instance, through _click fraud_, where competitors click over and over on a rival's ad, forcing them to pay fees for worthless clicks.

### 4. Adding the element of bidding to paid-search advertising created a dynamic form of SEM. 

Just as advertising has its own history, paid search has also evolved. Back in the early 1990s, paid search was completely static. It only allowed advertisers to buy a fixed ranking for a fixed period of time for a fixed price.

But in 1997, a digital mastermind was inspired by rate cards and auctions to make paid search dynamic.

Bill Gross realized that unlike search engines, traditional Yellow Pages charged for ads according to their size. He imagined a system where the more you paid, the more visible your advertisement would be. This led to the founding of GoTo.com, which invented _dynamic paid search_.

So how does it work? The first element is simple. An advertiser offers a website money for a keyword or phrase, and in exchange, the website shows their ad when someone searches for their purchased term.

But then the auction element comes in. As many advertisers are interested in the same terms, an auction allows them to bid as much as they are willing to pay, thereby creating a fair market price. And seeing as the internet allows people to scroll through content, many ads can be displayed, with the companies who bid the most getting their ads nearest the top.

And on top of being fair for the market, the dynamic paid-search system is fair for advertisers, too: they only pay when someone actually clicks on their ad. The win-win scenario that GoTo created laid the foundation for the industry standard that is still used today.

In the next blink, we'll take a deeper look at the latest evolution of dynamic paid-search advertising, _real-time bidding_.

### 5. Real-time bidding is a customer-centered, auction-like form of paid search. 

For the most part, when we talk about modern paid search, we're talking about real-time bidding. Let's look at how it works in more detail.

Real-time bidding takes bidding for advertising to the next level. Instead of competing for a specific space, advertisers battle for _specific users_. This is made possible by _automatic bidding_ and _data collection_. Let's look at automatic bidding first.

All bids take place on an _online ad exchange_ where advertisers bid for ad spaces from many sites. Just as a stock market presents all available stocks at any given time, the online ad exchange gathers all available ad spaces online.

Whenever a consumer types something into a search engine, an auction is created on the ad exchange. Thanks to the data that comes with the auction, advertisers have a great profile of the specific consumer's interests. And by determining in advance how much they are prepared to pay for specific profiles, advertisers can use automatic bidding software to raise the price as far as they are willing to go.

But where does the profile data come from?

From _cookies._

Cookies are recordings of browsing habits collected each time a consumer visits a specific website. By analyzing all of a consumer's cookies, advertisers can generate excellent consumer profiles.

Let's say you are looking for a new pair of hiking boots, and visit an online hiking store. During your visit, information about what kind of boots you look at, what brands you lean toward, etc., is stored in cookies on the advertiser's servers. The next time you search for hiking boots in a search engine, that webshop will have an incentive to put in a high bid, because they know you've already shown interest in their site.

### 6. Final summary 

The key message in this book:

**In the new digital economy of the internet, each company has to target its customer base carefully to make them aware of their product. The key to hitting the target is to make the most of Search Engine Marketing with paid-search ads and Search Engine Optimization.**

Actionable advice:

**Find out the three most important search keywords related to your company.**

To determine your three most important keywords, you must know the precise aim of your company. Generic phrases like "selling music production systems" or "providing lifestyle coaching" won't cut it. After you became very clear about what _exactly_ your company offers people, you can determine the three keywords you need to enhance your Search Engine Marketing.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Paid Attention_** **by Faris Yakob**

How can you get people interested in your brand in an age of ad-blockers, vanishing attention spans and colossal consumer choice? _Paid Attention_ (2015) discusses the fast-changing media landscape, and maps out strategies for success that reach beyond banner placement and pop-ups.
---

### Mark Smith

Mark Smith has 25 years of advertising experience working for companies like HBO, Forbes Magazine and Hearst.

