---
id: 583199ff8478e30004aeff0d
slug: team-genius-en
published_date: 2016-11-24T00:00:00.000+00:00
author: Rich Karlgaard and Michael S. Malone
title: Team Genius
subtitle: The New Science of High-Performing Organizations
main_color: FFE433
text_color: 665B14
---

# Team Genius

_The New Science of High-Performing Organizations_

**Rich Karlgaard and Michael S. Malone**

_Team Genius_ (2015) is a comprehensive guide to teamwork in business. These blinks explore the different forms of teamwork and how you can optimize your teams to keep them productive, motivated and innovative.

---
### 1. What’s in it for me? Team up for success. 

We're all familiar with the myth: the entrepreneurial giants of the world — people like Elon Musk and Bill Gates — had to fight their way to the top, single-handedly and against all odds. And of course that's the story we're fed. The tale of one person's trajectory is more engaging than an exploration of a team's dynamics. It's also easier to put one person's face on a magazine cover.

But such success stories mislead. Behind each successful individual is a team. Indeed, good teams are at the heart of every thriving business. But what is a good team? And how do you create one?

In these blinks, you'll learn

  * that there's no "I" in Steve Jobs;

  * what the cast of _Friends_ can teach us about teams; and

  * why you and your teammates should celebrate more.

### 2. With technology evolving rapidly, enterprises need maneuverability to adapt. 

Technology is developing faster than ever; our world is increasingly interconnected; life as we know it may never be the same again — you've heard it all before, right? These days, we've more or less accepted that the world is changing, and fast. But have we learned to keep up with it?

Businesses that want to survive within rapidly shifting markets must have one crucial skill: _Maneuverability_, that is, the ability to change direction quickly so that enterprise stays afloat.

To get an idea of what maneuverability looks like in action, let's take a look at Apple.

Back in 2002, the future of the company was uncertain. And yet, in 2012, as other companies such as US Airways, Enron and Adelphia Communication Corporation were struggling with poor performance and bankruptcy, Apple was worth a whopping $656 billion. How did they pull this off? Well, Apple had met the needs of changing markets by successfully introducing not one but _five_ groundbreaking products: the iPod, iTunes, the iPhone, the iPad and the Apple store.

Time and time again, Apple proved capable of discerning what customers wanted, and acting on this information _fast_. At the heart of the company is a culture of risk-taking, agility and efficiency; employees simply wouldn't dare approach Steve Jobs with a design or plan that was anything less than daring. This culture allowed the company to attract and keep talented employees through thick and thin.

Maneuverability, risk-taking and brilliant employees are the three key ingredients to Apple's recipe for success. But what brought it all together? Powerful teams. What exactly makes a team powerful? Find out in the coming blinks!

> _"Here is the uncomfortable truth: Humans run to a much slower evolutionary clock than our inventions."_

### 3. Humans work wonders when we work together. 

The Egyptian pyramids are one of the world's great mysteries. Not only are they filled with strange traps and untold treasures, their construction is also hard to fathom. Our best guess is that they were built with sheer manpower: 10,000 workers built them over the course of ten years.

The pyramids demonstrate that humans are meant to work together. Teamwork is a common thread that runs through our evolutionary history. By connecting and cooperating with others, humans have made their own survival possible.

This is even reflected in our very immune systems. Research shows that making genuine connections with other human beings boosts the genes that control our immune system. Further scientific investigations have also demonstrated that teamwork is what distinguishes us from other species.

In one study, chimpanzees, capuchin monkeys and human children were given a puzzle to be solved in three stages. The capuchin monkeys and chimpanzees, with, respectively, low and moderate levels of intelligence, attempted to solve the problem alone. The human children, however, chose to work in teams, teaching each other tricks to solve the problem together. They even shared the reward for solving the puzzle!

Of course, it's no secret that we can work wonders when we work together. Think of open-source software, where tech whizzes all over the world collaborate to build new computer programs. The HTTP protocol and the Mozilla Firefox, for instance, wouldn't exist without such collaborative efforts.

Wikipedia is another great example of success through teamwork. The most viewed site in the world, Wikipedia is the culmination of efforts made by volunteer writers and editors that work together to make knowledge about our world more understandable and accessible.

### 4. Powerful teams are built on diversity. 

Even our pop culture is filled with stories of great teams. Think of _Charmed_, _Friends_ or _The Three Musketeers_. Films and television shows that focus on the power of teamwork are often well loved for their portrayals of dissimilar individuals that come together and connect with each other. This is something companies can learn from, too!

The president of Nissan Design, Jerry Hirshberg, has taken this lesson to heart.

He kick-starts innovation by encouraging people with contrasting skills and abilities to collaborate. Their different ways of approaching a problem make them more likely to hit upon a unique solution, a process of innovation that's at the heart of many of Nissan's most popular vehicles.

Teams at Nissan are diverse — that is, every individual has a unique set of strengths and weaknesses, of cultural and personal experiences. This gives them a specific mode of cognition. Some of us are holistic thinkers, for instance, while others are analytical thinkers. Great solutions result from a combination of these approaches.

Holistic thinking entails seeing the big picture and then zooming in on particular details to solve problems. Solving problems analytically is a matter of focusing on the nitty-gritty details and then gradually building up a more comprehensive understanding. This contrast has been the subject of some interesting research.

One study saw Japanese and American participants view a 20 second clip from an animated underwater video. American subjects focused on the brightly colored fish — that is, the objects in the foreground. The Japanese subjects, however, were more interested in the background images and the relationships between different fish and plants in the scene. How can we interpret this?

Well, the American participants demonstrated analytical thinking, by focusing on specific details. Japanese participants had a more holistic approach, with their focus on the broader scene. Cultural differences like this should be a source of strength, not tension, in your team. With some members grasping the big picture while others pick out the details, there's no problem your team can't solve!

### 5. Partnerships turn ambitions into reality. 

Lovers and couples embody the most natural partnership humans can form: _pairs_. Pairs are common in businesses, too — particularly in stories of success.

Although we all dream of making it big on our own, we often need a partner to overcome unforeseen challenges. Starbucks Chairman and CEO Howard Schultz was determined to expand his business in the early '90s. After opening a few hundred new stores across the United States, however, his company began to suffer.

His ambitious expansion caused many problems; customer service deteriorated and communication between different levels of the company became poor. It was clear to Schultz that he needed someone that could return the art of customer service and employee engagement to Starbucks.

Enter Howard Behar, who became president of Starbucks in 1995. Behar, whose personality complemented Schultz's, had big ideas about employee culture, and he used his expertise to create a work environment that met the needs of employees. This inspired them to provide top-notch customer service, which made a world of difference at Starbucks. Thanks to the Schultz-Behar partnership, the company expanded internationally.

Partners can differ in two ways: they can have contrasting personalities or contrasting skillsets. While Behar and Schultz had complementary personalities, the pair of Steve Jobs and Steve Wozniak was a winning one because of complementary skillsets. Jobs was a charismatic, entrepreneurial leader, while Wozniak was a veritable tech genius.

However, even partners that aren't total opposites can achieve success: they can simply push each other to be the best they can be. Bill Gates and Paul Allen exemplify this. These two former computer nerds took control of their destiny when they teamed up to form Microsoft.

But, pairs in business don't always remain pairs. Sometimes you need another person in the mix to really get the ball rolling. Find out about the power of trios in the next blink.

### 6. Trios allow for an even more powerful form of teamwork. 

Trios are a rare constellation. Why? Well, they tend to become pairs, by losing a member, or quartets, when another member joins. But when a trio is maintained, it can thrive like any other team. Trios often begin as pairs, but the way they gain their third member varies. This in turn can shape the nature of their teamwork.

The _two plus one_ trio is particularly common. They often form after an original pair with similar skillsets joins an individual with different strengths to achieve a common goal. Take Walter Brattain and John Bardeen, for instance. Brilliant physicists at Bell Labs, Brattain and Bardeen made a good team. So when their boss, William Shockley, was tasked with designing a solid-state amplifier, he enlisted the help of the Brattain-Bardeen duo.

The two physicists worked together on the project, while Shockley used his supervisory and leadership skills to keep the project running smoothly. The result was their invention of the transistor in 1947. Less than a decade later, they were awarded the Nobel Prize as a trio for their efforts.

When a partnership welcomes a third person to collaborate with each member of the pair individually, another type of trio forms: the _parallel trio_. An example of this trio type is the collaboration between Federico Faggin, Masatoshi Shima and Stanley Mazor, who all worked together in the '70s at Intel to invent the microprocessor.

Faggin and Shima were responsible for hardware design, while Faggin and Mazor worked on the operating code to load into the chip itself. This trio dynamic is particularly powerful because of third members like Faggin, who act as a synthesizer between two contrasting partners, preventing one from dominating, and ensuring the trio members' objectives are aligned.

### 7. Teams of five to nine members are the most successful ones but require a discerning leader. 

Here's a question for you: How many digits can you memorize at once? Most humans can only manage somewhere between five and nine. And here's another question: How many team members can you handle at once? The answer is the same.

Teams that range from five to nine members are the most effective, and the most common. Why? Well, teams this size are very functional. With five or more members, your team is large enough to elect an internal leader without their role causing tension or becoming redundant.

Your team is also small enough that your leader is able to engage with each member personally. But it's not too small, allowing for diversity among members. A sweet spot indeed!

However, a leader of a five-to-nine team can sometimes be all too aware that he'll need to have direct contact with the members he selects for the team. This often results in leaders making biased decisions when finding new members.

But, in order to really make use of this optimized team model, a five-to-nine team leader must disregard personal preference, and choose members based on how they would benefit the whole team.

### 8. The start of any team should be celebrated. 

What do wedding receptions, baby showers and New Year's Eve parties have in common? Well, they all celebrate new beginnings. It's something we all love to do, so why not celebrate the beginning of a new team, too?

In a team's life cycle, the first and most crucial step is always the team's formation. The way a team begins will shape everything it does in the future, so it's worth considering the three factors that matter most: diversity, communication and size.

_Diversity_, as we learned in the previous blink, can give a team a major edge. A team with mixed strengths and backgrounds always has a better shot at success. But _only_ if these teams are able to develop a common culture that keeps them together.

_Communication_ is another vital element for new teams. Communication allows teams to build a shared culture, to resolve personal disagreements, to designate roles effectively and to ensure everyone is aligned toward the same goal. It's a simple solution, but regular meetings work wonders for team communication.

If your team is too big for one room, you could look into video conferencing. But you'd be better off forming a smaller team in the first place. This brings us to the third crucial factor for forming teams: _Size._ Smaller teams not only have an easier time communicating; they're also better at building strong bonds necessary for great teamwork.

The team leader also plays a central role in ensuring these bonds develop and flourish. Why not commemorate the day your team came into existence with a "birthday"? On the very first day that your team gets to work, gather everyone together so they can meet and greet, and share their stories and contact information.

This is also a great opportunity for the team leader to introduce the culture and values envisioned for the team project. But not by listing them in bullet points on a slideshow! Get your team engaged with its culture by designing a kickoff event that encourages the attitudes you want your team members to exercise during your project.

> _"Set the personality and the attitude of the team early, and you will spare yourself a lot of frustration and misery later."_

### 9. Final summary 

The key message in this book:

**Humans can achieve great things when they work together. So make the most of teamwork by building teams that are diverse, connected and motivated. This will give your business the edge when solving new problems and embarking on projects.**

Actionable advice:

**Show your team members you care!**

When a team member leaves a team, it's best to take the opportunity to acknowledge their contribution to the project and show your appreciation. This practice is a cornerstone of great team culture. Similarly, when a new team member joins, welcome them with an event that introduces them to their fellow members and reinforces a uniting, positive team culture.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Ideal Team Player_** **by Patrick Lencioni**

_The Ideal Team Player_ (2016) explores the role teamwork plays in today's business environment and shows you how to build a team geared for success. These blinks explain what makes a good team player, how to find them and which strategies you'll need to build a company around the concept of teamwork.
---

### Rich Karlgaard and Michael S. Malone

Rich Karlgaard is the publisher of _Forbes_ magazine, where he writes a featured column, covering business and leadership issues. He is a co-founder of _Upside_ magazine, Garage Technology Partners as well as Silicon Valley's premier public business forum, the 7,500-member Churchill Club.

Michael S. Malone, the author of the bestsellers _The Virtual Corporation_, _Bill & Dave_ and _The Intel Trinity_, is one of the world's best-known technology writers.

