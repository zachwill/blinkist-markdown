---
id: 56e6f8389854a50007000170
slug: team-of-rivals-en
published_date: 2016-03-18T00:00:00.000+00:00
author: Doris Kearns Goodwin
title: Team of Rivals
subtitle: The Political Genius of Abraham Lincoln
main_color: 8A6433
text_color: 8A6433
---

# Team of Rivals

_The Political Genius of Abraham Lincoln_

**Doris Kearns Goodwin**

_Team of Rivals_ (2005) charts the tumultuous events that took place during Abraham Lincoln's presidency. These blinks show how Lincoln was able to successfully keep the North united while putting an end to slavery and, eventually, the Civil War, by making his political rivals his closest advisors.

---
### 1. What’s in it for me? See what made Abraham Lincoln the greatest American President. 

Have you seen that movie starring Daniel Day Lewis as Martin Van Buren? What about the one with Henry Fonda as a young Rutherford B. Hayes? Of course you haven't!

When it comes to American presidents worthy of screen time, no one springs to mind quicker than Abraham Lincoln. Even today, over 150 years after his death, Lincoln continues to dominate American popular culture. But what is it that makes his legacy so strong?

These blinks, based on the eponymous bestselling biography, take a deeper look at Lincoln's life and career. They show how his political genius, ability to inspire and strength of vision changed the United States forever. 

You will also learn

  * why Lincoln's father used to burn his books;

  * how Lincoln came out of nowhere to win the 1860 election; and

  * why Lincoln waited on the Emancipation Proclamation.

### 2. Abraham Lincoln’s childhood made him a uniquely ambitious man. 

You might be familiar with the story of Abraham Lincoln being born in a log cabin and learning to read by candlelight. But his early years were filled with many more significant hardships.

Lincoln was born on February 12, 1809, and, as soon as he was able, he was put to work on the farm by his father, Thomas Lincoln. Young Abraham would help his father chop down and split trees, dig wells and plow fields.

His father was illiterate and often at odds with Abraham's desire to educate himself. Thomas was even known to burn Abraham's books so as to keep young Abraham from being distracted from getting his work done. If there was one admirable trait Thomas passed on to Abraham, it was his love of storytelling and keeping friends entertained with a good anecdote.

While he didn't get along with his father, Abraham had a loving relationship with his mother, Nancy Hanks Lincoln. Though little is known about Nancy Hanks, she was by all accounts a smart, strong-willed woman who helped teach Abraham to read and write.

Sadly, tragedy first struck the Lincoln family when Abraham was nine years old and his mother died of "milk sickness," a poisoning contracted from contaminated dairy. The death further strained the relationship between Abraham and his father.

More tragedy followed when, less than ten years later, Abraham's sister, Sarah, died during childbirth. And to make matters worse, his first love, Ann Rutledge, died in 1835, likely of typhoid fever.

But these hardships didn't break young Lincoln's spirit. Instead, they toughened his resolve and strengthened his ambitions.

His stepmother, Sarah Bush Lincoln, helped nurture his self-confidence and encouraged his education despite his father's attitude. To his stepmother and all of Lincoln's friends, it was clear that he had a spark of greatness that transcended the poverty and misfortune of his surroundings.

And so, in April of 1837, Lincoln moved to Springfield, Illinois, to start a career in law.

### 3. The Republican Party came together as the result of a turbulent political landscape. 

During his law career, Lincoln was elected to the Illinois House of Representatives in 1834. The following decades (1840s and 1850s) turned out to be combative ones for the United States. But many politicians were making a name for themselves by debating the issues.

The most pressing political issue was slavery.

The United States continued to expand as western territories were established. Consequently, there was a strong national divide between the northern "free states" and southern "slave states" as to whether new territories such as California or New Mexico should allow slavery.

Another big point of contention was the controversial Fugitive Slave Law, which required escaped slaves to be sent back to their masters, even if they sought refuge in a free state.

In an effort to defuse increasing tensions, The Compromise of 1850 was passed, keeping slavery out of California but strengthening the Fugitive Slave Law. Any palliative effect the compromise had was only temporary, as the debates started up again in 1854 in the territories of Kansas and Nebraska.

The Kansas-Nebraska Act allowed these territories to decide for themselves whether slavery should be allowed. This decision effectively repealed a previous statute that stated slavery would not be allowed north of the state of Missouri.

This turn of events lit a fire under Abraham Lincoln's political career and he became more active in his campaign against the spread of slavery.

The reignited debates ended up being the reason for the creation of the _Republican Party_ in 1854. The prominent political parties at the time, the Whigs and Democrats, were so divided on the issue that the anti-slavery advocates decided to band together as Republicans.

Ohio statesman Salmon Chase was among the party's high-profile members. He made headlines as one of the most outspoken anti-slavery advocates and became the nation's first Republican governor.

After Lincoln's Whig Party split apart, he joined the Republicans along with New York Senator William Henry Seward and prominent St. Louis statesman Edward Bates.

As leaders of the Republican Party, these men became rival candidates leading up to the 1860 presidential election.

### 4. To call Lincoln an underdog would have been an understatement. 

It may come as a surprise, but Lincoln wasn't initially the top choice when it came to picking a Republican nominee for the election of 1860. Lincoln had had a comparatively modest and brief career in politics compared to the other candidates.

First, there was William Henry Seward, who was considered an expert politician.

His exuberant personality made him well suited to his career as a lawyer. His enthralling senate speeches — considered the "rallying cries" for the Republican movement — made headlines around the country. It also didn't hurt that his long-time advisor was Thurlow Tweed, the prototypical political boss and campaign manager.

Then there was Salmon Chase, a trailblazer for the anti-slavery cause.

Chase had made a name for himself in many high-profile cases by defending escaped slaves and fighting against the Fugitive Slave Law. Though he would lose these cases, the language he used to claim that the law was unconstitutional would serve as a part of the Republican Party's foundation.

Finally, Edward Bates was one of the most esteemed candidates up for nomination.

At 66 years of age, Bates certainly had seniority over the other candidates. By 1860, he'd already had a long and storied law career in St. Louis and was a veteran of the War of 1812. On top of that, he was one of the men who drafted the state constitution of Missouri and was considered a national advocate for mending the division between North and South.

Meanwhile, all Lincoln had to offer was a humble law career and two failed bids at the Illinois state senate in 1854 and 1858. So it's no surprise that he wasn't initially a front-runner for the Republican nomination.

### 5. Lincoln’s nomination at the Republican Convention was a big surprise. 

In fact, none of Lincoln's rivals for the nomination considered him a threat. But, while the others were resting on their laurels, Lincoln was out building up momentum.

In 1858, Lincoln had his first taste of national exposure in the now legendary Lincoln-Douglas debates. Though he ended up losing the senate race to Douglas, he did win the popular vote. Plus, the speeches he gave in those debates would go on to be published and studied in debate classes for years to come.

Prior to the Republican Convention in 1860, Lincoln campaigned all over the north, including the important New England states, giving captivating and eloquent speeches that perfectly laid out the Republican agenda.

Lincoln remained clear and consistent with his anti-slavery stance and showed a willingness to work with the south and border states to resolve the issue. Perhaps most importantly, throughout his career he made friends, not enemies, wherever he went.

Seward and Chase, on the other hand, were overly confident and did not feel the need to campaign.

Seward decided to go on a tour of Europe in 1859 rather than campaign for votes. Chase could have capitalized on his absence, but he felt that he deserved the nomination for the work he'd already done.

Both of these candidates had always been ambitious in their careers and this meant that they had stepped on more than a few toes on their way up the political ladder. In other words, both candidates had enemies in prominent positions within the Republican Party.

For Bates, it may have been a crucial last-minute error that took away his chances.

Unlike Lincoln, Bates was inconsistent when asked to clarify his stance on slavery. Bates responded by being dismissive of the subject, preferring to talk about more unifying issues like the economy. Bates felt the issue of slavery was causing too much agitation and even suggested that politicians were engaging with it for their own political benefit. 

There's little doubt the Republican party wasn't happy with his statement.

All this led to Lincoln surprising his rivals and winning the nomination at the convention.

### 6. Despite putting together a balanced cabinet, President Lincoln underestimated the feelings of the South. 

Lincoln's victory in the 1860 presidential election was the greatest testimony to his intelligence as a politician to date. And when the time came to pick the members of his presidential cabinet, Lincoln kept thinking responsibly.

When selecting the department heads of his administration, his strategy was clear: to pick the men who were most qualified for the job, regardless of whether they had been Democrats or Whigs. This meant turning to his rivals: Chase, Seward and Bates.

Lincoln gave Seward the prominent position of Secretary of State, made the esteemed Edward Bates his Attorney General and named Salmon Chase the head of the Treasury Department.

For the other positions, he continued to select a healthy mix of important politicians.

To represent the influential state of Pennsylvania, he appointed former Democrat Simon Cameron as Secretary of War and Kentucky-born Montgomery Blair as Postmaster General. The Blair family had been a powerful force in the Democratic party.

With these surprising choices, Lincoln created a team of rivals.

Rather than taking the common path of choosing close friends and allies who would blindly support him in his opinions, Lincoln chose a group of smart and ambitious people who could voice their diverse perspectives on issues to help him make the best choices. 

Lincoln believed this mix of people could help him reunite the North and South, but he didn't realize how dire the situation would quickly become.

Immediately following Lincoln's election, the Great Secession Winter began. South Carolina voted to secede from the Union, citing the election of a "Black Republican" as the tipping point. Further exacerbating Southern grievances were political efforts to prevent slavery from spreading to new territories and attempts to nullify the Fugitive Slave Law.

This move was quickly followed by Mississippi, Louisiana, Florida, Alabama, Georgia and Texas.

> _"A cabinet which should agree at once on every such question would be no better or safer than one counsellor."_ \- William H. Seward

### 7. Lincoln’s challenges came quickly with the onset of the Civil War. 

Lincoln's presidential inauguration was on March 4, 1861. Relations between the North and South were already so bad that newspapers were calling Lincoln "the first president of the Northern Confederacy." It wasn't the best way to start, but Lincoln was up for whatever challenge came his way.

And the first one landed on his desk the following day.

Lincoln received an urgent letter informing him that South Carolina's Fort Sumter had been cut off from supplies and was in danger of being taken over by Southern Confederates. Lincoln needed to make a decision: send reinforcements and risk further agitation or surrender the fort which could be seen as a sign of weakness.

Lincoln didn't act hastily. He understood the significance of this first conflict and reached out to his cabinet members for their opinions. At first, only Seward opposed reinforcing Fort Sumter, feeling it would be better to take the hit and focus on saving other Southern outposts.

In the end, Lincoln attempted to send reinforcements but the plan went badly when conflicting orders were accidentally sent out. Although the mistake was caught and revised plans were dispatched, Confederate authorities intercepted the revised message and ordered a strike on Sumter before the reinforcements could arrive.

Fort Sumter was surrendered on April 13 and Lincoln took full blame for the outcome of the failed mission. Throughout Lincoln's presidency, his colleagues marveled at his magnanimous nature, his willingness to accept responsibility, and his ability to forgive others for their faults.

After the fall of Fort Sumter, Virginia, North Carolina, Arkansas and Tennessee seceded from the Union and the United States was on the verge of civil war.

The first blood was spilled in Baltimore on April 19, 1861, after a Massachusetts regiment was attacked while passing through. The northern Union was trying to mobilize troops as they quickly realized they were quite unprepared for war. The Union had little in the way of weapons, uniforms or horses.

Lincoln urged Chase and Cameron to quickly and efficiently get the Union military into fighting shape.

### 8. Lincoln had to keep the North united after dispiriting defeats. 

A bloody conflict between the North and South now appeared inevitable. But Lincoln saw an opportunity to unite the hearts and minds of the North by addressing Congress on July 4, 1861. He made it explicit to everyone that this war was not just about slavery but, much more, about fighting to keep the very idea of democracy alive.

Unfortunately for Lincoln, it would take some time before he would find the right general to lead his army and match his spirit.

The First Battle of Bull Run took place in Virginia on July 21, 1861. The battle was so close to Washington D.C. that it could be heard by residents in the city. But Confederate Gen. Thomas J. Jackson proved too much for the Union soldiers, sending them running in retreat and earning himself the nickname "Stonewall Jackson."

To try and rally the troops, Lincoln put General George McClellan in charge. He did well to organize the army but made the grave mistake of constantly overestimating the size of the Confederate army, often claiming there were twice as many troops as there actually were.

As a result, he refused orders from Lincoln and the Secretary of War to advance his troops, instead stalling and asking for more troops. This led to more defeats outside of Richmond, VA and in the Second Battle of Bull Run where his delay in providing reinforcements caused 10,000 casualties.

Lincoln knew changes had to be made and he looked to his own cabinet first.

Cameron proved unreliable, and his Department of War was rife with corruption. Cameron was unaware that associates had been pocketing contract money and wasting public funds. He was replaced by Edwin Stanton, a former Democrat and US Attorney General, who would remain in the position for the next six years.

### 9. Timing is everything: the Emancipation Proclamation shifted the course of the war. 

Lincoln knew it was time to take drastic action. He'd heard that the Confederate armies were using slaves to help their efforts and, for months, the question of whether to enlist black soldiers in the Union army was being debated among his cabinet.

So, in July of 1862, Lincoln presented the Emancipation Proclamation to his cabinet.

As an executive order in a time of war, it would allow Lincoln to circumvent Congress. With one blow, he could strike down the Fugitive Slave Law and declare the 3 to 4 million slaves in the United States free, as well as being eligible to serve in the army.

Seward wisely cautioned Lincoln against announcing the proclamation immediately, pointing out that, in the midst of all their losses, the proposal would come off as a last ditch effort and add to the Union's low morale. Lincoln took his advice and patiently waited for a Union victory.

Finally, following victory at the Battle of Antietam, the opportunity had arrived.

On September 17, 1862, Confederate General Robert E. Lee advanced into Union territory and forced General McClellan to take action. A horrific battle ensued, with over 20,000 casualties. Lee eventually retreated, but McClellan once again stalled, failing to advance on Lee's troops, allowing them to regroup.

It was McClellan's last action as Union general. Stanton considered him a traitor and Lincoln finally had him relieved from duty.

But the victory at Antietam was what the Union needed after many crushing defeats. At the beginning of 1863, Lincoln announced that he would enact the Emancipation Proclamation.

More success soon followed and Lincoln found a general he thought could lead the Union to victory.

General Ulysses S. Grant was proving to be the Union's best asset in a western campaign where he laid claim to the Mississippi River and divided the Confederate army. Thanks to his victories, and a hard-fought win at the Battle of Gettysburg, the war was finally shifting in the Union's favor.

### 10. Before Lincoln’s reelection, his cabinet provided an important victory but also disappointment. 

Sure enough, at the beginning of 1863, Lincoln remained true to his word and issued the Emancipation Proclamation. A regiment of 180,000 black soldiers formed to help fight a war that was still costing the country many lives.

The ongoing horrors of the war, including the 50,000 casualties at Gettysburg, meant that Lincoln's reelection was far from a sure thing. But Lincoln and his administration would make an important decision that would help bring the war to a close.

The Battle of Chicamauga was an intense two-day fight that took place in September of 1863. The Confederates lost over 18,000 troops and the Union just over 16,000. But, although the Union lost, they managed to hang on to the important city of Chattanooga.

Secretary of War Edwin Stanton immediately jumped at the chance to take advantage of this.

Calling an emergency session with Lincoln and the rest of the cabinet, he proposed a plan to quickly send 20,000 troops to reinforce Chattanooga and launch a new offensive before the Confederacy could regroup. With the cooperation of the railroad department, Stanton ensured that his plan could be carried out within seven days. Lincoln approved, and put General Grant in charge of the new offensive.

Stanton's plan — which was considered one of the fastest mobilizations in military history up till that point — was a success. Union forces pushed confederate troops out of Tennessee.

Meanwhile, not sensing that Lincoln's administration was inching the Union towards a victory over the Confederates, the Democrats were launching a misguided campaign against Lincoln. With former General McClellan as their candidate, they proposed making peace with the South at any cost.

But just three days after the Democrats announced this platform, Union General William T. Sherman won a crucial victory for the North, taking the city of Atlanta. This was quickly followed by the navy's capture of the Confederate port of Mobile Bay.

These victories crippled the Democrats' policy of compromise and secured Lincoln's reelection.

### 11. At the end of the war, Lincoln still exhibited his determination and goodwill. 

With his second term underway, Lincoln once again showcased his remarkable goodwill and loyalty to his colleagues.

Lincoln was aware that his ambitious Secretary of Treasury, Salmon Chase, had been campaigning for his own shot at the presidency during the recent elections. But Chase finally overstepped his bounds when he refused to acknowledge mistakes made while appointing friends within his own department.

Lincoln accepted Chase's resignation, but, as always, he was wise not to ruin what had been a fruitful relationship. Lincoln took the opportunity to appoint Chase as a Chief Justice in the US Supreme Court.

His goodwill to those who would even conspire behind his back never went unnoticed. Seward once proclaimed that, "his magnanimity is almost superhuman."

Meanwhile, Lincoln could see the end of the war approaching. And he knew that, in order to keep his Emancipation Proclamation alive during peacetime, he would have to push a constitutional amendment through Congress.

On January 6, 1865, he introduced the Thirteenth Amendment to abolish slavery once and for all.

Lincoln met individually with important members of Congress to stress the positive effect the amendment would have on ending the war and breaking the spirit of the Confederacy. And with the help of assistants campaigning on the amendment's behalf, he secured the five Democratic votes needed to push it through Congress.

During these first few months of 1865, representatives from both sides of the war, including Lincoln, were meeting to discuss an end to the war. Unfortunately, neither side was willing to compromise and the final battles of the war continued.

General Sherman succeeded in capturing the cities of Columbia, South Carolina, and Charleston, North Carolina. And, finally, on April 2, 1865, Union troops defeated General Lee in the Battle for Petersburg, leading Confederacy leaders to flee their capital, Richmond, Virginia.

To mark this occasion, Lincoln triumphantly walked the streets of Richmond, Virginia, surrounded by cheering former slaves.

The war was drawing to a close, and on April 9, Lee finally surrendered his 28,000 troops to Grant.

### 12. Lincoln’s death was also a loss for the south. 

Following the surrender of Lee's troops, Lincoln gave a rousing speech from the White House, expressing his hopefulness to quickly return the southern states to the Union and heal the wounds of the nation.

In the crowd that day was John Wilkes Booth and two of his fellow conspirators, Lewis Powell and George Atzerodt. The three men had hatched a plan to kidnap Lincoln and exchange him for Confederate prisoners of war.

But after hearing Lincoln's speech, and being so enraged by the idea of former slaves getting citizenship, Booth changed the plan to kill Lincoln, Vice President Andrew Johnson and Secretary of State William H. Seward.

The plan was set into motion on April 14, 1865, as Lincoln was watching the play _Our American Cousin_ at Ford's Theater. Booth, a professional actor familiar with the theater, showed his card and gained entrance to Lincoln's seating box.

Booth fired one shot into the back of Lincoln's head. Major William Rathbone, also seated in the box, tried to grab him, but Booth took out a knife, slashed Rathbone across the chest and jumped from the box to the stage below.

Meanwhile, Lewis Powell gained entrance to Steward's house, slashing and stabbing his way through the home, severely injuring many other people before he found the bedridden Seward. Powell stabbed Seward in the face but, luckily, Seward had just undergone surgery due to a near-fatal carriage accident. The metal used to repair his jaw likely saved his life.

George Atzerodt backed out of his part in the plan to kill Andrew Johnson. Kidnapping was one thing, but he was not a murderer.

Abraham Lincoln died the next morning and the entire nation, including the south, lost a valuable friend.

Both Ulysses S. Grant and the southern-born Montgomery Blair heard the news and felt that it was a tragic blow to the reconstruction of the United States.

Blair said, "Those of southern sympathies know they have lost a friend willing and more powerful to protect and serve them than they can now ever hope to find again."

### 13. The Final Summary 

The key message in this book:

**Part of the political genius of Abraham Lincoln was how he chose the members of his cabinet. He knew he could make the best decisions for the country by surrounding himself with his political rivals, i.e., people who disagreed with him and each other, but who represented the best minds to explore all sides of an issue.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Lincoln on Leadership_** **by Donald T. Phillips**

_Lincoln on Leadership_ is a detailed examination of the strategies that enabled Abraham Lincoln to lead so effectively before and during the American Civil War. The book gets past the myths of the legendary president and illustrates specific facets of his leadership ability to compare Lincoln's skills to successful strategies employed by modern leaders today.
---

### Doris Kearns Goodwin

Doris Kearns Goodwin is a Pulitzer Prize-winning author, historian and biographer. Her works include _No Ordinary Time: Franklin and Eleanor Roosevelt: The Home Front in World War II_ and _Lyndon Johnson and the American Dream_.

