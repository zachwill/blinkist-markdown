---
id: 55db3bc52647b100090000b6
slug: team-of-teams-en
published_date: 2015-08-28T00:00:00.000+00:00
author: General Stanley McChrystal with Tantum Collins, David Silverman and Chris Fussel
title: Team of Teams
subtitle: New Rules of Engagement for a Complex World
main_color: 8D8B41
text_color: 737135
---

# Team of Teams

_New Rules of Engagement for a Complex World_

**General Stanley McChrystal with Tantum Collins, David Silverman and Chris Fussel**

_Team of Teams_ (2015) lays out the many ways that even large organizations can benefit from the agility and savvy of small teams. By building a team of teams, companies can better manage the complex, interconnected issues that often mean life or death for a company.

---
### 1. What’s in it for me? Learn about the advantages of working in teams, and as a team of teams. 

In today's world, no one knows what tomorrow brings. Maybe your computer suddenly crashes, maybe an important business partner (or an entire country) goes bankrupt or maybe your competitor unexpectedly invents a powerful machine that causes setbacks across all of your business's departments — you never know. So how do you cope in such situations? You need to make sure you have the right teams in place.

These blinks summarize the authors' valuable advice on how organizations should be built and operate as a team of teams, appreciating the adaptability an organization needs to cope with unpredictable challenges and threats.

In these blinks, you'll discover

  * why today's madness for efficiency is indeed mad;

  * how to prepare your organization for unpredictable threats and challenges; and

  * what a plane crash at Portland's airport and an emergency landing in the Hudson River can teach you about the structure of your organization.

### 2. In our complex world, efficiency shouldn’t be the ultimate goal. 

No matter what your job, the concept of _efficiency_ won't be new to you. In fact, you probably use it even when you're just arranging your pencils on a desk!

Efficiency is widely recognized as something worth achieving, and most of today's organizations have set efficiency as their principal goal.

Even outside of business, our society is obsessed with efficiency. From organizing our leisure time and life hacks to the management of international companies, everyone is trying to achieve the best results with the least effort; in other words, they want to be as efficient as possible.

Our love for efficiency can be traced back to 1900, when Quaker Frederick Winslow Taylor laid the foundations of _scientific management_. By measuring various work processes in order to shave off every second he could throughout the production process, Taylor produced unprecedented results. 

But in a complex world, efficiency does not equal success.

The rapid development of information technology has changed the world we live in. Not only has it become much faster, but also more _interdependent_, meaning that a seemingly endless number of factors interact to produce highly unpredictable outcomes.

Much like the theory that a butterfly's wings in New York can start a hurricane in China, a YouTube video can start a revolution. In 2010, a man named Tarek protested police corruption in a small town in Tunisia by setting himself aflame. His cousin, who filmed the whole thing, posted it on YouTube where it was seen by millions of people.

These people then started their own protests, which eventually led to the end of the 30-year reign of President Mubarak in neighboring Egypt. This outcome was entirely unpredictable at the time of Tarek's protest.

Taylor's scientific management was designed for a world where an all-seeing manager could more or less accurately predict the outcomes of a given action. As our example shows, things are very different today.

> _"We have moved from data poor but fairly predictable settings to data-rich, uncertain ones."_

### 3. Success is achieved through resilience and adaptability. 

If today's world is complex, so are the threats that lurk within it. These threats can come out of nowhere, from any direction and in any form.

Looking back to the old way of doing things, Winslow Taylor's logic teaches us to prepare for every problem and protect ourselves with the help of robust mechanisms.

For example, if you live in a region where storms are frequent, build a backup generator in case the power goes out. Or if you have a poor relationship with your neighboring country, build a wall so they can't get in.

However, designing these protective mechanisms requires you to understand the nature of each threat. In a complex world, where you essentially don't know where setbacks will come from or what they will look like, you can't rely on these mechanisms alone.

So how can we hope to protect ourselves from these unpredictable threats? The answer lies in adaptability.

Modern-day organizations must be adaptable, able to respond quickly to unexpected dangers, or risk their doom.

We can look to the author's tenure as commander of the American Task Force in Afghanistan in 2003 as an example. Although this elite military unit had access to better resources than those of al-Qaeda in Iraq (AQI), and the American forces _could_ win battles, they were still losing the war.

They simply weren't able to adapt to a war against an enemy that didn't have a clear hierarchical structure, and that reacted to each attack with lightning fast reflexes. More firepower simply wasn't doing the trick when the enemy could re-group in the blink of an eye.

> _"Though we know far more about everything in it, the world has in many respects become less predictable."_

### 4. Teams are the perfect entities to produce solutions in a complex world. 

When you were in school, your teachers probably divided your class into groups for certain tasks. Why? Because teams can solve problems that one person alone cannot. It's no different for companies.

Working as a team ensures that companies can adapt to the many threats and challenges of the modern world.

Compare this with efficiency-minded organization, where companies are structured around commands; the only person with access to all structures is the head honcho, and everyone else just follows her orders. The problem with this type of organization is that a single person simply can't grasp the complexity of modern problems. 

That's exactly what happened in a 1978 plane crash at the Portland International Airport, which resulted in ten deaths. After a minor malfunction, the captain prolonged safety preparations until there was no more fuel, ignoring the advice of crew members telling him that the fuel was depleting.

Clearly, they should have worked as a team.

But what is it about teams that makes them so adept at navigating complexity? The trust and purpose that they share makes them perfect for solving complex problems.

On a structural level, teams function completely differently from command structures. Team members, as opposed to bosses and subordinates, share a common purpose, and their common experiences inspire trust in one another. 

This enables them to respond quickly in critical situations, with a shared understanding of which outcome is desirable and which actions each member must take to achieve that outcome.

For example, after the accident at Portland's airport, United Airlines recognized that the plane's technology was too complex to be handled by one person alone. So, they introduced the Crew Resource Management program as a way to build teams.

Did it work? Yes. Just compare the 1978 crash to the emergency landing on the Hudson River in 2009: although the crew only had a few minutes to land the plane, each team member immediately identified the specific actions they needed to take to ensure a safe landing without prior coordination.

> _"Anyone who has ever played or watched sports knows that...adaptability is essential to high-performing teams."_

### 5. When there are too many people to put in a team, form a team of teams instead. 

By now it's clear that teams are advantageous in any environment that requires specialized knowledge. But how do you go about creating a team from your company's thousands of employees? Simple. You build a team of teams! 

A single team can't include an endless number of members and still be effective. So, large companies must instead build multiple teams.

By definition, team members work together closely, and trust each other greatly. To work closely, these teams have to be small; you simply can't know and trust an endless number of people. Thus, companies with more than 150 people need to build multiple teams to stay effective.

Once you've built your smaller teams, they will then work together as a larger team, just as individual team members work within their smaller teams. 

Teams are a double-edged sword: Their structure allows organizations to enjoy the benefits of a quick, adaptable workflow. On the other hand, the close relationships built between the team members involves the necessary exclusion of those outside the team.

When you have multiple teams working in your company, you need them all to share an understanding of the overall purpose and work processes of the other teams. Otherwise, you'll only have half-finished solutions with pockets of incompetence that no one is overseeing.

The author's experiences in the American Task Force show what can go wrong when teams don't understand one another. For example, when he visited the Task Force's intelligence facilities, he found a storage room with unopened garbage bags full of intelligence that had been gathered on raids.

The operatives on the field simply lacked the understanding of how the intelligence team operated. As a result, the time-sensitive intelligence wasn't handled as such, and was thus delayed for several hours. When it was finally processed by an analyst, it was already too old.

Clearly, teams are essential to effective organizations. But how do you build teams in a way that maximizes their effectiveness? Keep reading to find out!

### 6. A team of teams needs to see the whole system in which it operates. 

People sometimes say that knowledge is power, and this is absolutely true. The right information can be a precious commodity.

Traditionally, companies keep information secret — even within the organization — unless sharing it is absolutely necessary.

For our purposes, _information_ covers everything from sensitive data, like sales and marketing strategies, to simple things like how individual workers perform and how salaries are calculated.

According to Taylor, sharing information is not only unnecessary, but also dangerous. Each employee needs to stay concentrated on their own part of the whole production process, meaning information should only be handled by the manager who oversees operations.

However, today's complexity demands a more open approach to handling information: it should be shared.

Sharing information, and thereby sharing an understanding of the complete system in which a team operates, is a prerequisite for good decision-making under complex conditions. If a team lacks a shared understanding of the overall context of their actions, they'll make decisions that are good for the team, but not necessarily good for the company as a whole. 

Think back to all the sensitive intelligence that was tucked away in garbage bags in a closet. The field operative thought they were doing their job well. After all, the raid was successful. They just lost sight of the context of their work: the success of the American Task Force!

The author made a point of sharing information once he took command of the American Task Force in Iraq. He set up a Joint Operation Center in their base in Balad, where Task Force members could access all relevant information about their operations. 

In addition, e-mails were sent to people who might be affected by certain operations, and the weekly Operations and Intelligence brief was televised to ensure that everyone in the Task Force could follow the development of operations.

This way, information was no longer wasted.

### 7. A team of teams needs to support the relationships among teams as strongly as those within a single team. 

For a team of teams to really work, the smaller teams need to form bonds just like those the individual team members have within their own teams.

As mentioned before, one of the reasons teams are so effective is because of the closeness and trust among team members, making them a tightly bonded group.

In contrast, simply throwing a bunch of people into a room and telling them to work together doesn't make them a team. Similarly, calling an organization a team of teams doesn't make it one.

Rather, the teams that comprise a team of teams must emulate the trusting relationships shared by members of a single team. The shared understanding, as well as the trust that the teams develop, form a kind of _shared consciousness_ that causes them work together just like a single team would. 

So how do you get entire teams to share the type of bond that is usually developed over the course of years between individuals? Well, the mechanics are basically the same: they simply need to share experiences. 

As an example, consider the challenge the author faced when he decided to develop relationships between the different parts of the American Task Force _and_ between the American Task Force and its partner agencies, such as the FBI and the CIA. Within the American Task Force, he started an exchange program, whereby members of different teams would spend at least six months in another division — a Navy SEAL would go to an Army Special Forces team, and vice versa. 

When looking at your teams, if a member of one team can't remember at least one friendly face from another team, it's probably a good idea to have them share more experiences. This way, they'll create tighter bonds.

### 8. A team of teams needs to have the power to make decisions. 

Imagine that you're part of a team of teams. Everyone is communicating and working well together, but then challenges emerge that the teams want to tackle as a unit.

As you begin to plan, your boss stops you in your tracks, loudly declaring that he alone makes the decisions. Strangely, that's often how decisions are made. But is it right?

In short, no. As we know, things change at amazing speed; to match that speed, companies must enable their teams to make decisions on their own.

Today's complex problems are the result of a fast-paced, interconnected world. Luckily, a team of teams with shared consciousness mirrors this interconnectedness. But that won't mean much for you if your teams don't enjoy a healthy amount of autonomy.

Take it from the author: after introducing the shared consciousness program, he soon realized that his approval was essentially a matter of protocol. Teams already had all the information they needed to make informed decisions, but they still needed his signature to act — waiting for this approval just slowed things down. So, he initiated a practice called _empowered execution_, which allowed the teams themselves to handle situations that required immediate action.

Empowered execution is simply the logical consequence of shared consciousness. However, it should be noted that empowering the teams to make decisions is the logical step _after_ arming them with the information they need to make the decisions.

Handing over control and power to a team that doesn't have sufficient information could be disastrous. Companies would thus be well advised to ensure a strong structure and free flow of information before delegating decisions to lower-ranked team members. Without a shared consciousness, empowered execution is just irresponsible.

But if your teams are empowered to act autonomously, why would they need a leader? Our final blink will reveal what the leader's role in all of this is!

> _"The less people's jobs can be automated, the more you need them to make initiative, innovate, and think creatively."_

### 9. The leader of a team of teams has to concentrate on the culture instead of the day-to-day operations. 

If the team already has all the information and control they need to make effective, informed decisions, do they even need a leader?

Yes, they do! But not a traditional leader. Rather, they need one who respects the team's autonomy.

Traditionally, leaders are viewed as commanders of an army, sitting in the background and barking orders to subordinates who dutifully carry out their tasks. Ultimately, the leaders are the ones who determine where everyone goes, what they do and by what means.

Such an approach doesn't take into account the amazing problem-solving power of a team of teams. What's the point of having dynamic, highly functional teams who handle enormous amounts of information, and are capable of making great decisions, if they're never allowed to act?

Even in the rare instance where there's enough time to consult a leader about their opinion, it would still be better to allow teams to make their own decisions. Not only is this more effective, but it also gets the team more invested in the execution of their plan.

So what role does the modern leader play, then? Their job is essentially to make sure that the culture, the glue that holds these teams of teams together, stays intact.

The leader's role is different than that of a puppet master. In a team of teams, it's the leader who sparks the conversations and makes sure everyone is participating in the discussion. She's the person who makes sure that teams are empowered to make their own decisions, and that even the lowest-ranked operatives are also part of this empowered execution.

In other words, the leader tends to the culture like a gardener would, while the team manages the day-to-day operations of the company.

### 10. Final summary 

The key message in this book:

**Today's complex world is highly unpredictable, and companies rely on the teamwork of those within their organization to make sense of this complexity. By organizing teams of teams, companies can turn their workforce into a single, highly adaptable working organism with a single vision.**

Actionable advice:

**Use whiteboards.**

PowerPoint presentations have their place, but the best way to get your teams ready for innovation is much more low-tech: whiteboards. Displaying information in this way gives your team the means to brainstorm ideas dynamically, while making sure that they understand the complexity of the problems at hand.

**Suggested further reading:** ** _Turn the Ship Around_** **by L. David Marquet**

_Turn the Ship Around_ reveals the story of how one United States Navy captain managed to turn a dissatisfied submarine crew into a formidable and respected team. But how did he do it? By changing the way we think about leadership, this story will show you that inside, we all have the power to be leaders.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### General Stanley McChrystal with Tantum Collins, David Silverman and Chris Fussel

General Stanley McChrystal served at the US Army for 34 years before retiring as a four-star general.

David Silverman and Chris Fussell are former US Navy SEAL officers and current senior executives at CrossLead.

Tantum Collins studies international relations as a Marshall Scholar at Cambridge University.

