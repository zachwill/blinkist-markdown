---
id: 5d00ff286cee070007e60015
slug: tell-me-more-en
published_date: 2019-06-14T00:00:00.000+00:00
author: Kelly Corrigan
title: Tell Me More
subtitle: Stories About the 12 Hardest Things I'm Learning to Say
main_color: 33B1AE
text_color: 217371
---

# Tell Me More

_Stories About the 12 Hardest Things I'm Learning to Say_

**Kelly Corrigan**

_Tell Me More_ (2019) is a meditation on the phrases that allow us to express love and connect with others. Built around a series of intimate personal essays, Kelly Corrigan's study of life's many frustrations and joys cements her critical reputation as a "poet laureate of the ordinary." Unflinchingly honest and often downright hilarious, Corrigan's reflections are an endlessly thought-provoking exploration of the meaning of death, friendship, parenthood and, above all, love.

---
### 1. What’s in it for me? A lexicon of life lessons. 

Love, an old song has it, makes the world go round. It's a nice idea, but unfortunately, there's more to it than that. After all, every relationship has its ups and downs. Things go wrong and we mess up. Expressing our love for others also means apologizing, admitting mistakes and learning to listen.

In these blinks, acclaimed memoirist and storyteller Kelly Corrigan takes a look at the phrases that keep our most important relationships on track. Drawing on her experiences as a daughter, mother and wife, Kelly uses her family life as a springboard to examine the power of choosing the right words at the right time.

From acknowledging that life is "just like this" to the profound admission that "I was wrong," these phrases are like grease on the wheels of life — they keep things turning. And, as Kelly shows, they can get us through crises, arguments and grief.

In the following blinks, you'll learn

  * why neither faith nor reason holds all the answers to life's mysteries;

  * what a poorly behaved family pet taught Kelly about apologies; and

  * why you don't need to be perfect to get the most out of your life.

### 2. When everything goes wrong, all we can do is accept that it’s “just like this.” 

One day, Kelly Corrigan's life fell apart — or rather, _she_ fell apart. Her father Greenie had died of cancer three months earlier, and the grief had become overwhelming. Everything left a sour taste in her mouth.

It was a regular morning in the Corrigan household. Kelly woke, as usual, to the smell of her husband Edward making himself bacon. But by the time she'd put her slippers on, her blood pressure had already spiked. The phone was ringing and no one was picking up. Edward couldn't hear it and their two kids — 14-year-old Claire and 16-year-old Georgia — were busy fighting in the hallway.

Kelly felt a wave of resentment as she realized she'd have to sort the girls out. Edward adopted the laissez-faire "let them fight" attitude that he'd come across in the only parenting book he'd read, which suited him down to the bone. At this moment, Kelly didn't recall that she'd actually fallen in love with "Easy Ed." Today, it only occurred to her that this was another brawl he'd miss, leaving her to deal with the fallout once again.

By the time everyone was ready to leave the house, Kelly was close to exploding. Surveying the kitchen, she saw slimy eggshells on the counter and clumps of the family dog's fur under the table. She hadn't even taken a sip of coffee, and there was Edward, shaven, showered, fed and ready for work. The fact that he was about to leave her to stew in spitefulness just made things worse.

Alone, she reflected on what had happened. She loved her family more than anything, and she hated the idea of becoming a nagging, self-pitying sourpuss that her husband couldn't wait to get away from in the morning. What had gone wrong?

That's when Kelly remembered the motto of her meditation teacher — sometimes, it's "just _like_ this." Life can suck. When it does, we lose our bearings and become disoriented and irritable. Deep down, we know our rage is futile — no one, after all, really believes that berating the gods will bring back a parent or make stretch marks disappear. But that doesn't stop us from falling into the trap that Kelly found herself in that day. All we can do is accept our current circumstances and weather the storm.

> "_Being in our lives as they are is probably one of the most common struggles people have going back thousands of years._ "

### 3. Sometimes the best way of helping someone you love is listening, rather than giving advice. 

Parents are problem-solvers. Before Claire or Georgia are halfway through explaining an issue, Kelly already has five ideas about what they should do to fix it. As kids grow up, however, they need to start figuring things out for themselves. Parents can help them do that by listening instead of presenting readymade answers.

That's hard. As Kelly's old college buddy and fellow parent Tracy puts it, it's a bit like watching someone labor over a puzzle while the final piece is in your pocket. Parents hate watching their kids suffer, so of course they want to give them that missing piece. Tracy and Kelly were discussing this during a long car ride to a college reunion party when Georgia called, then aged eleven. It was a remarkable coincidence.

Through sobs, Georgia told her mom that she hated 6th grade, that everything was unfair and everyone at school lied. Kelly was about to take on a familiar role — devil's advocate. Georgia must have done _something_, right? Luckily, Tracy was on hand. She reminded Kelly to let her daughter get it all out and continue her story. Georgia talked about another girl called Piper, who'd repeatedly been mean to her but was now claiming she'd started it. Kelly, who couldn't even remember anyone named Piper, was getting exasperated, but Tracy gave her another nudge — "just say it back to her."

Kelly summarized. "So everyone is mad at you for being mean to Piper, but you weren't." Georgia's voice brightened as she confirmed this interpretation. As she went on, Tracy fed Kelly more lines. Rather than getting into the details of this tween melodrama, Kelly affirmed her daughter's feelings with comments like, "that must feel unfair."

By the end of the call, Georgia had calmed down. Aided by Tracy's prompts, the conversation had gotten to the heart of the matter — Georgia felt rejected by her schoolmates. That, Kelly realized, was deeply relatable. She didn't know or much care who Piper was, but she _did_ understand the sting of rejection, and that her empathy was just what Georgia needed.

### 4. Neither faith nor reason hold the answers to life’s mysteries. 

Kelly's Irish-American parents both grew up in a devout Catholic community in 1940s Baltimore. As her father told her, faith was simple — you believed because everyone else did. The rest just fell into place. Folks ate fish on Fridays, went to mass and lived in the hope of receiving their ultimate reward in the afterlife. Doubt didn't come into it.

But for Kelly, things were different. Religion was still a social glue, but it wasn't a source of certainty about the nature of the universe. Church, for Kelly, was above all a sensory experience. She loved the taste of Communion, the smell of incense and the sound of the organ. And it was also the best place to meet boys!

By the time she got to college, however, faith had taken a back seat in her life. When both Kelly and her father Greenie were diagnosed with Stage 3 cancer, that generational difference stood out. Her parents turned to God. Kelly and Edward turned to Google.

Greenie's bladder cancer was more serious than Kelly's breast cancer. His age and the location of his lesions made his case more complicated. While Kelly's doctors were confident about her chances, Greenie's were telling his family that they should make the most of his final year on Earth.

But after nine months of chemotherapy and radiation, Greenie was back on his feet and bodysurfing at the Jersey shore. That, many said, was a miracle. Kelly's mom attributed it to prayers from Greenie's friends and family around the world. Kelly wasn't convinced — after all, she hadn't prayed. Calling on God in this moment of need, after snubbing her faith for so long, didn't sit well with her.

Her rational-minded friend Tracy had a different take. The inexplicable glory of God, she claimed, was neither here nor there. What had saved Kelly's father was human ingenuity and the many scientific breakthroughs culminating in the tiny scope and scissors with which surgeons had removed nine tumors from Greenie's bladder.

It's a good argument, but Kelly isn't so sure. Even the doctor in charge of Greenie's case was at a loss to explain his clean bill of health. Sometimes neither science nor religion offers a persuasive explanation of things. Like that doctor, all we can do is shrug our shoulders and say, "I don't know."

### 5. Saying no is a key part of healthy relationships. 

Nothing comes as easily to children as saying no. As we develop, however, we lose the ability to effortlessly reject ideas or requests. Saying no feels lazy, rude, mean or stingy. In fact, the term hardly has a single positive association. But it's a vital part of relationships. That's something Kelly discovered in her late thirties.

As a kid, Kelly was notorious for her obstinacy. Once, her mother Mary refused to buy dozens of expensive hoagie sandwiches for a birthday party, ordering pizza instead. Kelly was furious. Hoping to force her mom's hand, she declared that she didn't like cheese — a food she positively adored. She didn't back down, even after the ruse had failed. She didn't eat a single piece of cheese for the next ten years!

But she really learned the importance of saying no after her battle with cancer. Kelly had always planned on having four kids by age 40. Georgia and Claire had come easily and, at the age of 36, she was on track to meet her goal. Cancer derailed that plan. Chemotherapy suspended her fertility, a common side effect of the treatment. When a troubling growth was found on her ovaries two years later, the only option was an oophorectomy — a procedure to remove her ovaries.

It was a terrible blow. But with her cancer in remission, she was confident an alternative like adoption or egg and embryo donors held the solution. Sure she'd figured it out, Kelly presented her plans to Edward. His response crushed her hopes. She could tell from the look on his face that this wasn't a wish he could fulfill. He told her, gently but unequivocally, that he was happy. His wife was healthy again, their kids were doing well and he was in a good place with his career. He just wasn't ready for another battle.

Kelly cried, but she didn't fight him. Why? Well, at that moment she realized what a toll her cancer had also taken on her husband. The only way he could continue to support her was to take care of himself. Saying no was an act of self-preservation that guaranteed the future of their marriage.

> "_Sometimes the art of relationship is declaring your limits, protecting your boundaries, saying no_."

### 6. The only thing to say when you screw up is “I was wrong.” 

There's one member of the Corrigan household we haven't met yet — Hershey the dog. Kelly got her after a friend told her it was a great way to teach kids about responsibility. However, Kelly ended up taking charge of Hershey's training herself. It was a pretty bare-bones regimen, and Hershey's poor behavior is a hair-trigger issue for Kelly. Why? Well, it's a constant reminder of her own lack of discipline.

Hershey's worst habit is drinking out of the toilet. That's far from ideal at the best of times, but the Corrigans also happen to live in arid California. Being environmentally conscious, they try to save water when they can. This means only flushing their upstairs toilet every second time someone pees. Despite Kelly's constant reminders, however, Claire and Georgia occasionally forget to flush after other uses, too.

Unfortunately, dogs are known for _coprophagia_. That's a zoological term derived from the ancient Greek noun for "feces" and the verb "to eat." One evening, the Corrigans were sitting down to dinner when they heard the telltale clink of dog tags against porcelain. After rushing upstairs, Kelly began howling in anguish. What she'd found doesn't bear repeating — let's just say that she still has wrinkles in her forehead from that day.

Kelly exploded, and Georgia — already on parole for a similar offense the previous week — bore the brunt of the expletive-filled storm. After dispatching her to the bathroom with a roll of kitchen paper, Kelly thundered out for a walk, leaving a shell-shocked Edward and a weeping Claire in her wake.

A mile later, rage had turned to horror. Not only had she modeled the kind of uncontrolled overreaction she always railed against, but she'd also lost it in front of her husband and co-parent. Hardly mom-of-the-year material!

That wasn't even the worst of it. When she got home and apologized to Claire for her scary yelling, her daughter confessed that it was her fault, not Georgia's. Making things right with her eldest was going to take a near-perfect apology. How do you begin to apologize to a wrongly-accused teenager you've made scrub human feces off the bathroom floor?

The only thing Kelly could say is "I was wrong." These three words sustain the never-ending cycle of errors and forgiveness that define parenthood.

### 7. You don’t have to be perfect – “good enough” is plenty. 

Every Tuesday, Kelly takes a stroll through the hilly Oakland neighborhood of Piedmont with her friend Ariel, a psychotherapist whose kids attend the same school as Claire and Georgia. When Ariel's daughter Ruby celebrated her _bat mitzvah_, a religious initiation ceremony for Jewish girls, she invited Kelly along.

It was a remarkable occasion. Aged just 13, Ruby was calmness personified as she recited Hebrew texts, told stories from the Old Testament and delivered a _drash,_ or sermon, to the entire congregation. To get to that point, she'd spent years attending Hebrew school and one-on-one tutorials with her rabbi, and completed a major community project.

After the ceremony, Kelly got talking to a Rabbi named Michael. Why, she wondered, were mitzvahs performed at the age of 13? He explained that it's a pivotal age in every young person's life — a moment of incredible physical and intellectual development.

It's also the point when we leave the safe harbor of our childhoods and suddenly find ourselves navigating the vast oceans of our adult lives. The ceremony, Michael added, aims to give young people a sense of their power. It's a way of telling them that they're good enough not only to get by, but to become a force for good in the world.

That's something we all need to hear, whatever our faith. Take it from Kelly. A late bloomer, she only really got her ducks in a row in her mid-30s. Before that, she'd been adrift, aimlessly moving from one dead-end job to the next while her friends settled down, got married and bought houses. Throughout those years, Greenie provided Kelly with a sense of direction. When she was at her lowest and most doubtful, her father was there to tell her she'd figure it out eventually.

He was right. Later on, after she'd met and married Edward, had kids and become the person Greenie always thought she would be, Kelly asked her father how he'd known things would work out. His answer? You don't need to get it right every time — a couple of wins here and there is plenty.

That's all it takes. When someone we cherish believes in us and continues to do so even as we mess up, we eventually start believing in ourselves and our abilities.

### 8. Final summary 

The key message in these blinks:

**Life is often messy, complex and painful, but there is wisdom we can turn to when we're overwhelmed by its twists and turns. Whether it's the calming acceptance that comes from recognizing that things are "just like this," the humility of admitting "I don't know" or the pointed finality of the word "no," these phrases offer comfort, sanity and direction in trying times.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _The Art of Communicating,_** **by Thich Nhat Hanh**

Choosing the right words at the right moment isn't just a skill restricted to professional wordsmiths and storytellers like Kelly Corrigan — in fact, it's something anyone can learn. Sure, you might not win the Nobel Prize in Literature, at least not right away, but that's not the point. The important thing is to communicate your true feelings to those around you.

That's something the renowned mindfulness expert and Zen master Thich Nhat Hanh can help with. The key, he argues, is to learn to express yourself authentically. How? Well, take a look at our blinks to _The Art of Communicating_ to find out!
---

### Kelly Corrigan

Kelly Corrigan is a best-selling author based in Oakland, California. Hailed as the voice of her generation by _O: The Oprah Magazine_, her previous books include _The Middle Place_, _Lift_ and _Glitter and Glue_. Corrigan is also the creative director of the Nantucket Project, an annual conference bringing together some of today's most exciting thinkers and creatives.

