---
id: 56c1d105587c82000700001a
slug: tesla-en
published_date: 2016-02-15T00:00:00.000+00:00
author: Margaret Cheney
title: Tesla
subtitle: Man Out of Time
main_color: 4C495D
text_color: 4C495D
---

# Tesla

_Man Out of Time_

**Margaret Cheney**

_Tesla_ (1981) offers an enlightening and intimate account of the life and accomplishments of one of history's most thought-provoking and innovative inventors. These blinks chart the career of Nikola Tesla from his early battles with Thomas Edison to the controversial debate over the invention of radio communication.

---
### 1. What’s in it for me? The fascinating life story of the inventor’s inventor. 

Who springs to mind when you think of the archetypal inventor? For many people it's Thomas Edison. But for others — especially people who have themselves dabbled in inventing — it's Nikola Tesla.

When you look at Tesla's accomplishments, you realize that, in terms of innovation, Tesla outshines them all: he is the father of wireless communication, alternating current, radar and solar technology. And while Edison, Marconi et al made millions with their work, Tesla died alone in relative poverty.

These blinks explain how Nikola Tesla's ideas helped make the modern world what it is today, and why he never quite profited from his work.

They will also show you

  * why some of Tesla's experiments were a danger to horses;

  * which famous author featured in the world's first X-ray image; and

  * how the War of the Currents was fought.

### 2. Nikola Tesla’s eccentricities and love of science were formed early on. 

At exactly midnight, between July 9 and 10, 1856, Nikola Tesla was born in the Croatian village of Smiljan. Traditionally, most men born in this region were destined for a life as a minister in the Church or as an officer in the Austro-Hungarian Army. But young Tesla had other plans.

The beginning of his life was defined by three things: an illness, a family tragedy and the unique genetic gifts he inherited from his mother.

Tesla always claimed that his photographic memory and vivid imagination were passed down from his mother, Ðuka Mandíc. She could recite by heart long passages of poetry without hesitation and handcrafted her own tools to create fabrics with impressively intricate designs.

And when Nikola was just five years old, his 12-year-old brother Daniel died in a mysterious accident. The event deeply affected young Nikola and he tried to console his parents the only way he knew how: by becoming the best student he could be. By the age of eight, he was reading constantly and starting to grow interested in engineering and physics.

However, his studies were interrupted in his teenage years when he contracted cholera.

While bedridden, he pleaded with his father, the Reverend Milutin Tesla, to allow him to continue his studies in engineering instead of joining the clergy. His father eventually gave Nikola his blessing to continue his education, and only then did Nikola find the strength to recover from his illness.

It was around this time that young Tesla started to become afflicted with strange visions and phobias.

Throughout his entire life, Tesla would experience bright flashes of lights and hallucinatory-like images when his mind became excited. This appeared to be a side-effect of an imagination that was so powerful and vivid it allowed him to picture his inventions clearly enough in his mind's eye that he didn't need to draw them out.

He would also struggle with a profound aversion to germs, women's earrings and pearl jewelry. And perhaps most peculiar of all was his need to calculate the cubic volume of his food and beverages before he could enjoy his meals.

### 3. Tesla’s ambitious ideas about electricity took him to America. 

Tesla was an excellent student, even though he could get on some of his teachers' nerves. In 1875, at the Austrian Polytechnic School, he was at odds with one of his professors. 

In this particular professor's class, Tesla openly criticized the direct current motors being used around the world. He knew a better motor could be created to produce higher voltages and send them greater distances. Despite the professor's doubts, the ambitious Tesla tasked himself with finding a solution, but it would be many years before he arrived at one.

But by the end of 1879, Tesla was broke and had to drop out of school. His father had just passed away and he was forced to find work to support himself. 

In 1881, Tesla was working at a telegraph office and experiencing what would ultimately be diagnosed as a nervous breakdown. Tesla described it as the result of being overwhelmed by his powerfully acute senses. 

For example, he claimed he could hear a watch ticking from three rooms away, and that the vibrations of a train passing 20 miles away could cause him unbearable pain.

Upon his recovery that Tesla was struck by inspiration that would change his life.

While walking through a park one day, a vision for a new and improved motor came to him. The _induction motor_! Rather than the single circuit, direct current motor, this motor would use two or more alternating currents to create a rotating magnetic field to generate electricity.

He quickly realized that this motor could create a revolutionary new way to power the world: _a polyphase system_ capable of generating and distributing much greater power. He just had to find the means to actualize it.

His search for better work took him to Paris and the Continental Edison Company. It was here that Tesla's electrical expertise was first recognized and he was put to work fixing power plants in France and Germany.

His manager was so taken with Tesla's talents that he wrote him a letter of recommendation to take to Thomas Edison personally. It was just what Tesla needed. The letter in hand, he was off to America.

### 4. Tesla’s relationship with Thomas Edison was troubled from the start. 

When Tesla arrived in America in 1884, he immediately set out into the streets of New York City, ready to make a name for himself. And it didn't take him long to show up at the offices of the Edison Electric Company.

Thomas Edison's company was responsible for powering most of New York City. But his glitchy and unstable direct current (DC) generators were far from perfect and Tesla knew it.

When Tesla walked into Edison's office, he introduced himself, presented his letter of recommendation and quickly tried to convince Edison of the benefits of switching to his alternating current (AC) motor.

"Spare me that nonsense," Edison replied. "We're set up for direct current in America. People like it, and it's all I'll ever fool with. But maybe I could give you a job."

That same day, Edison put Tesla to work fixing up the lighting plant on the S.S. Oregon steamship. He made quick work of it, impressing Edison by finishing the repairs by morning. But the professional relationship didn't last long.

Edison offered Tesla a deal: If he could improve the design of his generators, there would be $50,000 in it for him.

To Tesla it sounded like a good proposition. And, over time, he redesigned and improved 24 of Edison's electrical generators, even to the point of installing automatic controls.

But, when it came time for Tesla to collect his money, Edison looked confused and told him, "Tesla, you don't understand our American humor." Edison instead offered to increase his pay from $18 to $28 a week. Tesla felt cheated and promptly resigned, discouraged and depressed about his chances in this new country.

### 5. George Westinghouse helped bring Tesla’s vision to the masses. 

Despite his negative experience with Edison, Tesla forged ahead on his own. Before long, his bright ideas caught the attention of the right people.

It started in 1887 with A.K. Brown, a manager for the Western Union Telegraph Company, who believed that alternating current would be the future of electrification. Together, he and Tesla founded the _Tesla Electric Company_ with the specific intention of developing Tesla's AC induction motor. 

By 1888, Tesla had received his first patent for his AC induction motor and, by 1891, he had a total of 40 patents related to it. His polyphase system was beginning to come to life.

This activity caught the attention of George Westinghouse, a powerful businessman looking to compete with Edison.

Westinghouse saw the brilliance in Tesla's new technology. With its use of electromagnetic induction, Tesla's AC motors did away with the grinding parts that would spark and wear down in Edison's DC motors.

Tesla and Westinghouse were soon in business, and the War of the Currents (between AC and DC) was on.

In the initial Westinghouse-Tesla contract, Tesla received $60,000 for patent licensing and was to get $2.50 in royalties for every unit of AC horsepower produced with his motors — a deal that would have made Tesla a billionaire. Unfortunately for Tesla, advisors told Westinghouse that this royalty deal was unrealistic and Tesla agreed to drop it to keep the partnership.

Tesla's big opportunity to show off his AC distribution system came in 1893.

Westinghouse underbid Edison's General Electric to secure the rights to power the 1893 World's Fair in Chicago. It served as a massive display of the effectiveness and reliability of AC electricity distribution. 

It also proved that, despite Edison's claims, AC power wasn't uncontrollably dangerous.

Twenty-five million people visited the Fair and Tesla put on his own sideshow. He wowed audiences by controlling high-frequency volts of electricity and wirelessly lighting up tubes as he safely sent currents through his own body.

Throughout the 1890s, Tesla would give such jaw-dropping presentations around the world.

### 6. At Niagara Falls, Tesla won a big victory in the War of the Currents. 

With the success of the 1893 World's Fair behind him, the future was looking bright for Nikola Tesla. And if there were any lingering doubts about the winner of the War of the Currents, Westinghouse and Tesla were about to put them to rest.

Ever since childhood, Tesla had dreamed of harnessing the power of Niagara Falls. One of his first projects in school had been to build a working model of a water turbine. 

His dream became a reality when he received word from George Westinghouse that he'd secured the contract to build the first electrical generators at Niagara Falls. Tesla oversaw the installation and, by 1896, the first powerhouse was up and running, sending 15,000 horsepower of electricity 26 miles to the neighboring city of Buffalo.

The project was such a success that even Edison's General Electric couldn't deny its effectiveness. They would end up using Tesla's patents in a second powerhouse that was eventually installed at the site.

Tesla and his revolutionary polyphase system were now the talk of the town. 

_The New York Times_ declared Tesla's "undisputed honor" in harnessing Niagara's power. The American Institute of Electrical Engineers awarded him a medal of honor.

Even other nations were taking notice. Both the Prince of Montenegro and England's Lord Kelvin bestowed their praises on Tesla. 

Indeed, it's hard to overstate the importance of Tesla's achievement as it would go on to set the foundation for the entire world's power systems. Tesla's polyphase AC power systems spread to other cities and, before long, to other countries.

### 7. A disaster in Tesla’s laboratory was an unthinkable setback for the inventor. 

Despite his achievements with the AC induction motor, Nikola Tesla was never one to rest on his laurels. He was constantly exploring new ideas and seeking out inspiration. So, after his work in Niagara Falls, he quickly went back to conducting experiments in his lab on 33-35 South Fifth Street (now West Broadway) in New York City. 

This lab was a hotbed of innovation in the early 1890s.

Even during the War of the Currents against Edison, Tesla kept himself busy at the lab. Many of his experiments during this time were focused on his dream of distributing electricity wirelessly around the world. Tesla believed that either the Earth itself, or the electrically charged upper stratosphere of the Earth, could be used as a conductor.

These ideas guided Tesla's groundbreaking progress on radio and remote control technology.

In 1893, at the National Electric Light Association in St. Louis, he gave the very first public demonstration of radio communication. Using a five-kilowatt spark transmitter he sent a wireless message to a receiver 30 feet away.

At the time Tesla also loved to entertain guests at his laboratory by putting on dazzling displays. 

Mark Twain was an admiring friend who would often visit Tesla's lab when he was in New York. In fact, a lab experiment involving a photograph Tesla took of Mark Twain was later revealed to be the first X-ray photograph taken in the United States.

But all this research and more was lost on the morning of March 13, 1895 when Tesla discovered that his lab had burned to the ground over night. It is thought that experiments involving the production of liquid oxygen may have caused the disaster.

It was particularly disastrous for Tesla as he had never prepared for such a situation, never having taken out insurance for his building or the equipment.

> _"The destruction of Nikola Tesla's workshop, with its wonderful contents, is something more that a private calamity. It is a misfortune to the whole world." - Charles A. Dana, New York Sun_

### 8. Tesla’s ideas were so grand that he often had trouble finding investors. 

As well as being a heartbreaking creative setback for Tesla, the loss of his lab hit him hard financially as well. He had no insurance for the building or his equipment, and his partnership with Westinghouse had just about run its course. He needed to find investors to make his next big invention a reality.

After mourning his losses, Tesla set about building a new lab with some help from the connections he'd made at the Niagara project. He picked up where he left off on his remote control experiments, believing that the technology could be profitable.

He filed patents and demonstrated the world's first remote-controlled boat at the first Electrical Exhibition in Madison Square Garden in 1898. He envisioned many uses for the technology but was particularly vocal about its military potential, describing a vision of remote-controlled robots fighting our wars and defending our shores. 

Though actually, in Tesla's opinion, the technology would bring about an end to war altogether.

But, despite the country's engagement in the Spanish-American War at the time, there were no takers.

Newspapers were doubtful about this new technology. Some wondered how secure the transmissions would be against interference or interception. They were completely unaware that Tesla had already developed and perfected secure lines of communication.

This and many other ideas proved to be far ahead of their time. 

For instance, around the same time he was presenting his remote controlled devices, he was promoting theories about harnessing solar energy. Tesla was also exploring what he called _telegeodynamics_, a field using the principles of vibration to send messages wirelessly through the earth or to detect underground oil or mineral deposits. 

Despite requests for funding, even friendly investors such as Westinghouse declined to help Tesla make these technologies become an everyday reality in his lifetime.

### 9. The wireless transmission of energy would forever elude Tesla. 

Despite his infinitely curious mind, Tesla was obsessed with a single idea for most of his adult life: being able to power the world with wireless electricity. 

His laboratory experiments in this field were becoming too dangerous for a crowded city but, luckily, in 1899, an associate found an isolated location in Colorado Springs. Tesla jumped at the chance to conduct some large-scale experiments.

Upon arriving in Colorado, Tesla immediately built what he called a _magnifying transmitter_ — essentially a huge Tesla coil that could generate high electric voltage. Activating it sent huge bolts of lightning up into the Colorado Springs sky, testing the possibilities of sending electricity wirelessly through the atmosphere.

He tested the possibility of sending electricity through the Earth's surface as well.

By experimenting with frequencies both high and low he sent massive amounts of voltage into the surrounding area. He sent electrical shocks through the earth that would scare local horses and wirelessly activate light bulbs in the ground at distances of as much as 26 miles away.

These experiments famously resulted in the July 3, 1899 blackout of the entire town of Colorado Springs when his transmitter overloaded the town's own generator.

For Tesla, the experiments were a success. The results made him believe that he could build a facility that would change the world.

When Tesla returned to New York in January of 1900, he proposed building a towering facility called Wardenclyffe. He was convinced it would allow multiple lines of wireless communication and electrical power to be sent around the world.

But again, funding proved difficult to find.

Westinghouse declined and all Tesla was able to put together was $150,000 from financier J. Pierpont Morgan. This was enough to build the structure and hire a staff but not enough to get it into operation.

Tesla would spend the better part of the next 17 years trying, but failing, to get Wardenclyffe up and running. His dream of wireless electricity would remain out of reach.

### 10. While others were getting rich off of his inventions, the fall of Wardenclyffe marked a dark period for Tesla. 

As the ambitious plans for Wardenclyffe show us, Tesla's life was often filled with controversy. But few issues lasted as long as the debate over who would be credited with inventing radio communication.

The fact of the matter is that Tesla filed his fundamental radio patent in 1897. But when Guglielmo Marconi sent the first transatlantic radio communication in December of 1901 it set off a series of legal battles that would last decades.

After this transatlantic signal was sent, Tesla responded by saying, "Marconi is a good fellow. Let him continue. He is using 17 of my patents."

What ended up hurting Tesla was that, during the legal battles that lasted 40 years, Marconi reaped plenty of financial gains while Tesla struggled to make ends meet.

It wasn't until 1943, eight months after Tesla's death, that the US Supreme Court finally ruled that Tesla's fundamental radio patent was to be upheld. 

Unfortunately, this did little to help Tesla back in 1916, when it was publicly revealed that Tesla couldn't afford to pay his taxes and had been allowed to live for many years at the Waldorf-Astoria on credit.

In a crushing blow, Tesla was forced by the courts to hand over Wardenclyffe to a New York attorney to prevent further actions against him and, in 1917, his dream project was demolished.

But even during these hardships he remained a visionary: in a 1917 article published in _The Electrical Experimenter_, Tesla accurately described modern radar technology. His description of a stream of electric charges bouncing off of objects to reveal their locations was the primary inspiration to Dr. Emil Girardeau, whose team built and installed radar stations in France in 1934.

### 11. Tesla’s final years were bittersweet. 

The 1920s found Tesla out of step with the emerging frenzy of modern society. During this period he spent more time with New York City's pigeons than its people.

In fact, he spoke fondly of a white pigeon that would follow him no matter where he went, saying, "As long as I had her, there was a purpose in my life."

Sadly, according to Tesla, one day the bird flew through his open window, stood on his desk and produced a "powerful, blinding" light from its eyes right before it died. After this event, Tesla said he knew that his life's work was finished.

Despite that, Tesla carried on working into his '70s, still proving to be full of prophetic ideas. 

In a 1926 interview with _Collier's_ magazine he spoke of the future saying, "Daily newspapers will be printed wirelessly in the home" and that people would use pocket-sized instruments considered to be "amazingly simple compared with our present telephone." 

But, even when honored, Tesla showed a distaste for modern science.

At his 75th birthday party in 1931, Tesla was honored by many peers including Nobel laureates and Albert Einstein. When interviewed at the party, Tesla explained he was working on two things: disproving Einstein's Theory of Relativity and a creating new source of energy that had nothing to do with "so-called atomic energy."

When Tesla died alone in his hotel room at the age of 86 on January 7, 1943, he left behind some unanswered questions.

Despite the fact that Tesla had been an American citizen since 1891, his papers and belongings were confiscated by the government's Office of Alien Property Custodian. It was rumored this may have been due to Tesla talking about his work on a powerful, weaponized particle beam technology that he offered as another solution to end war.

No papers on the details of any such beam were ever found. As with most of Tesla's inventions, the design most likely lived completely in his imagination.

> Tesla filed around 300 patents in his lifetime, many of which are still being used and studied by professional and amateur physicists and engineers to this day.

### 12. Final summary 

The key message in this book:

**Nikola Tesla was perhaps the greatest inventive mind America has ever known. Certainly, when it comes to the field of electricity, few have surpassed his boundless imagination. Even in the face of hardships that would defeat most other people, his creativity never waned.**

**Suggested** **further** **reading:** ** _Elon Musk_** **by Ashlee Vance**

_Elon Musk_ (2015) gives us an insight into the brilliant and difficult character of today's most innovative entrepreneur. Interwoven with details of his turbulent private life, these blinks reveal why Elon Musk is so determined to save the human race, how he's worked towards this goal so far, as well as what's on the horizon for potentially the richest and most powerful man of our future. To find these blinks, press "I'm done" at the bottom of the screen.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Margaret Cheney

Margaret Cheney numbers among the preeminent biographers of Nikola Tesla. Her works also include _Tesla: Master of Lightning_, co-written with Robert Uth, and a biography of jazz singer Mabel Mercer entitled _Midnight at Mabel's_.

