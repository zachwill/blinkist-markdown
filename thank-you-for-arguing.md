---
id: 5714bba5344e1b0007a4c878
slug: thank-you-for-arguing-en
published_date: 2016-04-21T00:00:00.000+00:00
author: Jay Heinrichs
title: Thank You for Arguing
subtitle: What Aristotle, Lincoln, and Homer Simpson Can Teach Us About the Art of Persuasion
main_color: 7BB8BB
text_color: 376B6E
---

# Thank You for Arguing

_What Aristotle, Lincoln, and Homer Simpson Can Teach Us About the Art of Persuasion_

**Jay Heinrichs**

_Thank You for Arguing_ (2013) is a guide to the art of rhetoric. These blinks explain what rhetoric really is, how persuasion works and how to win a debate by drawing on in-depth research, anecdotes and theories from the great orators of history.

---
### 1. What’s in it for me? Be like Aristotle and never lose an argument again. 

When you think about arguments, you probably think of two spouses yelling at each other about something in the kitchen. That's a shame, because arguments shouldn't be thought of merely as a verbal means of attack.

Arguments were originally conceived as an efficient and rather gratifying way for two or more parties to come to a conclusion together. Of course, arguments can become passionate and heated, but that doesn't mean they have to be solely associated with hatred and spite.

When arguments are approached with a different perspective, they become something more like a technique, like something that can be studied and improved.

Learning that technique will not only make arguments more enjoyable; it will also make you a more formidable debater! And that's what these blinks are about.

In these blinks, you'll learn

  * why Aristotle would've approved of Eminem's character in _8 Mile_ ;

  * what central rhetoric lesson John Kerry ignored in his 2004 presidential campaign; and

  * how successful marriages handle arguments, compared to ones that end in divorce.

### 2. Arguments are essential to human life, influencing our attitudes and driving our decisions. 

For many, the word "argument" conjures images of two people engaged in an angry screaming match. But _rhetoric_ — the art of argumentation — is much more than that. In essence, it's a nexus of skills and techniques that help the arguer persuade others, and its origins can be traced all the way back to ancient Greece. 

But what bearing does it have on contemporary society?

Well, even today, rhetoric shapes the way we think, without our even noticing it. The ancient Greeks held the discipline of rhetoric in such high esteem that it was the foundation of all education. They practiced this skill by making arguments. And arguments continue to play a key role in all human dealings: they're made in advertisements and political speeches, in books and blogs, in the kitchen and the courtroom.

A common misconception is that arguments ought to lead to an agreement. What they truly aim to achieve, however, is a _consensus_ — that is, complete shared faith in the outcome. So the goal of an argument is not to _win_, but to _win over_ your audience. 

The psychology professor John Gottman led a study that made this idea clear. In observing couples in therapy, he found that the pairs who stayed married had just as many disputes as those who broke up. But there was a crucial difference: partners in long-lasting marriages took the opportunity to solve their issues and reach a shared outcome. In other words, they _argued_ ; the couples that broke up simply _fought_. 

In other words, fighting, or being aggressive for the sake of winning an argument, isn't a good way to argue. It won't help you reach a consensus. So what's a better way?

The Greek philosopher Aristotle might have suggested seduction, which he considered the strongest kind of argumentation. Seducing your audience, persuading them to want what you want, is the easiest way to reach a consensus.

### 3. Don’t start arguing until you know what you intend to accomplish. 

When most people start an argument, their goal is to force their opponent to admit defeat. But if the argument ends without your opponent's mind being changed, then there's not much point of starting an argument in the first place.

Here's how to avoid that:

First of all, don't argue for argument's sake. Only argue when it will help you reach your goal. For instance, you might be trying to persuade a friend to do something. If that's the case, then achieving this end should be the benchmark for success. After all, the person who wins the argument isn't necessarily the one who silences the other. The winner is he who reaches his goal. 

Let's say a police officer pulls you over for doing 51 mph in a 50 mph zone. As the officer approaches your car, you may be tempted to respond rudely. But remember: your intention is to avoid getting a ticket. It therefore makes more sense to apologize respectfully and play the role of the law-abiding citizen. This will satisfy the officer, who will feel his authority was respected, and, hopefully, you'll dodge a pricey fine.

So keeping your goal in mind is crucial. Equally important is avoiding the common mistake of doing everything you can to score points. In other words, don't just focus on proving yourself right. If your opponent is fixated on scoring points and trying to humiliate you, let him have at it. After all, acting like that won't necessarily win him the argument. 

For instance, do you remember the 2004 presidential debates between John Kerry and George W Bush? Polls said that Kerry's logic won him the debates. But Bush won the election! That's because, in the end, Bush's seduction was more powerful than the points won by Kerry's logic.

### 4. Arguments revolve around core issues, and identifying them is essential to a peaceful resolution. 

Everyone has gotten into an argument that simply couldn't be resolved. Often, people don't understand how such a stalemate could occur. But, in fact, there's a simple explanation.

Many arguments end without resolution because the two arguers are debating two entirely different core issues. For instance, according to Aristotle, every argument is based on one of three possible issues:

First, there's _blame_, as in, "Who used all the toilet paper?"

Second, there's _values_, like, "Should the death penalty be legal?"

And, third, there's _choice_, represented by questions like, "Does it make sense to relocate to China?"

But why is it important to identify the issue at hand before arguing?

Well, if you don't, you'll never reach a positive outcome. For instance, imagine a couple sitting in their living room. She wants to read her book and he wants to listen to the Rolling Stones.

Clearly, their desires are at odds and an argument is inevitable. That's because the woman wants peace and quiet, an issue of choice. If she gets mad at the man for playing his music too loud, or starts criticizing his taste in music, the argument will shift to blame and values.

Then, as the discussion jumps from the quality of his favorite Rolling Stones record to the thickness of the apartment walls, the real issue — music vs. silence — will be forgotten.

But it's possible to move beyond the stalemate by using the relevant tense. For instance, in the prior example, the blame issue refers to the past: "You turned up the stereo when you knew I wanted to read!" — and the values issue is set in the present: "You're only against the Stones because you prefer The Beatles!" If the couple instead focussed on the future, for example, by the woman saying "Would it be better if I turned it down or played something else?", it focuses the argument on choice, thereby better enabling a resolution.

So besides knowing the core issues and staying focused on whichever one is relevant to the argument at hand, you can solve many intractable arguments by simply speaking in a common tense.

### 5. Using logic and appealing to the emotions of your audience are powerful argumentative strategies. 

Do you know Aristotle's three essential tools of persuasion?

There's _logos_, or argument by logic; _pathos_, or argument by emotion; and _ethos_, also known as argument by character, a strategy that relies on disputing the reputation and trustworthiness of your opponent. 

Here's how the first two work:

Logos is based on a series of techniques that employ structured reasoning, instead of brute force, to persuade your audience. One of these techniques, _concession_, is to agree with your opponent before shooting him down with a sharp reply. 

So, say you're in a political debate with a friend and are trying to get through the conversation without disagreement, hoping to avoid an escalation. But then your friend starts arguing that the world would be safer if there was more NSA surveillance of communication. You might concede that, yes, a safer world is good for everyone. But then you could get your shot in by asking whether he'd truly feel safer living in an Orwellian environment, where the government could watch his every move.

And pathos?

Pathos, the root of the Greek word for "sympathy," is about aligning yourself with your audience's feelings. After all, a classic argumentative error is attempting to force the other arguer to change his feelings, like telling a co-worker to cheer up after his salary was cut.

If you used pathos in this scenario you would instead be sad _with_ your colleague. This will get him to sympathize with you and, in turn, he will be more open to your thoughts. 

Now that you know how logos and pathos work, it's time to take a look at Aristotle's third strategy: ethos.

### 6. In an argument, who you are matters more than what you say. 

At this point, it might go without saying that Aristotle was the godfather of rhetoric. And for him, the most important argumentative appeal of the three was ethos. Here's why:

When it comes to rhetoric, a good reputation goes further than good reasoning. For instance, when Lincoln was fighting to end slavery in America, his ideas weren't exactly popular. However, people _liked_ him, and this made all the difference when he sought to abolish slavery. 

But to truly understand ethos we need to look at the origins of the word. In Greek, "ethos," or "ethics," originally meant "habitat" — the environment a person occupies. That means being an ethical person is about "being at home" with your audience, sharing their values, manners and tone, fitting in as snugly as a piece in a puzzle. 

The Romans also had a word for ethos — _decorum_, which refers to the way a speaker encapsulates the collective voice of a crowd. For instance, the movie _8 Mile_, a quasi-biopic of Eminem's rise to stardom, culminates in a hip-hop club where orators, depicted here as emcees, sling verbal attacks at each other. 

It's at this moment that, to the crowd's surprise, Eminem himself, nothing more than a white-trash bum, wins over the predominantly black crowd. His winning strategy is to tear down the street credibility of his opponent by pointing out that he comes from a wealthy family and attended private school. 

It worked for Eminem's character in the film, but be careful when using ethos. Remember that convincing an audience using decorum doesn't mean mimicking them. Rather, it's a matter of representing their ideal. For instance, when an ethical politician addresses a crowd, he has to seem as honest as possible, even if his constituents are committing fraud or cheating on their partners. 

Constituents, though far from perfect themselves, always want a perfect politician; and audiences always want a flawless orator!

### 7. Ethos works by turning your character into a weapon of persuasion. 

So, presenting an appealing character is essential to winning over your audience. Here's how to do it:

According to Aristotle, an ethos-based argument has three essential qualities. The first is _virtue_, which means you can persuade your audience by sharing their values. But to do that you first need to know what values your audience holds and how you can embody them. 

For instance, say you're reading a book while your teenage daughter listens to Taylor Swift's latest hit. You know that, at this age, young women consider independence paramount and that ordering her to turn off the music will certainly backfire. So, a better option is to give her a choice, thereby manipulating her sense of independence. Ask her whether she'd rather turn down the music or put on her headphones.

The second aspect of effective ethos is _practical wisdom_. That means you've got to look like a person who always knows what to do. For example, Jimmy Carter had the exceptional credentials that Americans look for in a presidential candidate and he should have done fantastically as president. He failed to secure a second term, however, because he lacked the necessary practical knowledge. 

In fact, showing off your real-world experience is pretty effective; indeed, street smarts are often more effective than book learning. For instance, in discussions about war, someone usually invokes their status as a veteran to gain the audience's trust. Naturally, this experience means a lot more than having studied war academically. 

And the final quality of ethos is _selflessness_, which, in this context, means showing your audience that you have their interest in mind above all else. You can do this by reaching an agreement that might appear to personally hurt you, but which is undeniably correct. 

For instance, say you want a project to go through at work. You might tell your boss that, even if you don't get credit for the project, you'll still work late to make it happen — it's just too good to be passed up.

### 8. Learn to identify and use the shortcomings of your opponent’s argument. 

Have you ever been convinced by a sleazy salesman? Well, they often rely on some of the most notorious rhetorical traps out there. 

Two such traps are bad logic and false comparisons. Kids love using them to get what they want from their parents. Just take the classic line, "But all the other kids get to do it!" Be careful not to do what many parents do, which is to rebut this with poor logic of their own by asking some inane question, like "And if all the other kids jumped off a cliff, would you follow them?" 

Another strategy people employ is to hurl insults at their opponents. When faced with such accusations you can use _labeling_ to attach positive connotations to their words. This is something politicians do all the time. 

For instance, if a friend accuses you of being "a liberal hippy" you can simply respond that if caring about people makes you a liberal hippy, then, yes, you _are_ a liberal hippy. The effect will be to put your opponent on the defensive and let you move more easily. 

And finally, it's especially important to watch for _bad examples_ and _tautologies_. Bad examples are usually easy to spot because they're disconnected from the argument they're meant to support. 

For instance, a mother might read about a child molester in the paper and decide that it means her kids aren't safe playing outside. In cases like this, the mother is using a poor example of a single arrest to support a general argument that her kids should not play outside. 

And tautologies?

They just repeat the obvious. For instance, "I'm sure the Warriors are going to be NBA champions this year because they've got the best team." The persuasiveness of such arguments is essentially nil when compared to a statement like, "I'm sure the Warriors will be NBA champions this year because Steve Kerr's become a much better coach."

If you keep an eye out for these tricks, and combine them with the rest of the techniques in these blinks, you'll find yourself enjoying arguments like Aristotle himself.

### 9. Final summary 

The key message in this book:

**Arguing sometimes gets a bad rap, but it's as old as ancient Greece and it's more about persuasion than picking a fight. Mastering the skills of rhetoric will help you communicate better, understand others and share your opinions without upsetting people.**

Actionable advice:

**Flip it.**

Aristotle used to say that there's a flip side to every point, and when he found himself in a tough argument, he'd search for a way to turn the tables, usually by using concession. For instance, say your spouse is upset that you two never go out anymore. You could respond by saying, "Yes, my love, but that's only because I want you all to myself." That should give you enough time to decide which restaurant to make a reservation at. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _Talking to Crazy_** **by Mark Goulston**

_Talking to Crazy_ (2015) acknowledges that each person has the potential to be a little crazy, giving into irrational behavior when the mood strikes. These blinks offer sound advice on how to empathize and communicate with a person in "crazy mode" so you can keep yourself from going off the deep end, too.
---

### Jay Heinrichs

Jay Heinrich is a former editor and publishing executive who dropped his first career in order to make rhetoric his full-time job. He blogs about rhetorical techniques and holds workshops aimed at popularizing argumentation.

