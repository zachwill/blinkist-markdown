---
id: 5a0981c5b238e1000655ceca
slug: thank-you-for-being-late-en
published_date: 2017-11-16T00:00:00.000+00:00
author: Thomas L. Friedman
title: Thank You for Being Late
subtitle: An Optimist's Guide to Thriving in the Age of Accelerations
main_color: A68536
text_color: 735C25
---

# Thank You for Being Late

_An Optimist's Guide to Thriving in the Age of Accelerations_

**Thomas L. Friedman**

_Thank You for Being Late_ (2016) is all about acceleration — of the economy, of technology and of our environment. These blinks explain why the world is moving at an increasingly rapid pace, outlining the dangers this trend can bring as well as what we stand to gain from it.

---
### 1. What’s in it for me? Discover why living in the Age of Acceleration might not be as bad as you think. 

We live in quickly evolving times. Think back to 2007, the year the first iPhone came out: could you have predicted back then that the world would have become so reliant on mobile technology, apps and globalization?

Unlike generations before us, people alive today must constantly adapt and readapt to rapid changes in technology, society and the economy. The good news is that some people have found ways to harness this new reality to improve the human condition.

Technology-induced globalization means scientists from all over the world can share big, innovative ideas at breakneck speed, helping us tackle problems like the gradual dwindling of natural resources and climate change. Meanwhile, social media in forms such as Twitter allows individual citizens to discuss problems that they have in common with others, thus helping us come together as societies, nations and communities.

In these blinks, you'll learn

  * how technology has contributed to — and accelerated — globalization;

  * why no one uses MySpace anymore; and

  * what Moore's Law is, and why it's important.

### 2. The age of acceleration began in 2007, launching an era of constant adaptation. 

Do you remember what 2007 was like? You might not have realized it, but that year was a turning point in human history. In fact, a great disruption occurred in 2007, in which three accelerating forces — technology, markets and climate change — all ramped up.

By way of example, 2007 was the year the iPhone hit shelves, when Twitter reached a global scale and when Airbnb was created in a San Francisco apartment. Not only that, but from January 2007 to December 2014, mobile data traffic through AT&T's national wireless network rose by over 100,000 percent!

A useful theory to explain this remarkable transformation is _Moore's Law_, which holds that the processing power of microchips will double approximately every two years, a truly astounding rate of growth.

In 2007, the technological acceleration described by Moore's law contributed to, and coincided with, accelerations in the market, evidenced by increased global commerce, rapidly growing social networks and an information tsunami. At the same time, mother nature was experiencing an acceleration in the form of climate change and population explosions.

So, what does that all mean for life on Earth?

Put simply, it means that this is a time of constant adaptation. If prior epochs of history were characterized by occasional destabilization, the modern world is one of near-constant destabilization; humans must constantly reevaluate their ecosystem, remaining agile in order to adapt to a rapidly changing world.

That doesn't mean you _can't_ attain stability in today's world. But rather than a static stability, you can expect a _dynamic stability_, like the one you experience while riding a bike. It's not the kind of stability that lets you stand still, but it will keep you afloat if you stay in motion.

It sounds tough, and living in this state will require adaptation — but while it may not be natural, it's the reality humans now face. In the blinks that follow, you'll learn more about the specific forms of accelerations at hand and how you can keep pace in a world that's moving faster than ever.

### 3. Technology is transforming at a rate never seen before. 

The author can remember working as a journalist abroad in 1978, when he had to wait in line for a British telephone to send news stories back to his editors in the United States. But today, he could just as easily send an email from Britain, or even Africa, and it could be published nearly instantaneously on the _New York Times_ website.

In fact, technology evolves so rapidly that, while conducting research for this book, the author had to interview each of the technologists he consulted at least twice just to stay up to speed.

It's clear that such growth isn't natural and we can already see it disrupting the world. For instance, a few years back, digitization disrupted dairy farms in upstate New York. For decades before that, milking cows involved manual labor on the part of farm workers, but now computers are increasingly being used to control and monitor udders, supply chains and milk flow.

As a result, the successful cow milkers of tomorrow may not match the image we often envision of a farmer sloshing around in the mud; the farmers of tomorrow may actually be sharply dressed data analysts.

Now, this could be good news. After all, digitizing milking means fresher milk and less manual labor; but it's also a depressing thought. It perfectly encapsulates a major problem, namely that countless mid-level jobs are disappearing and being supplanted by software programs and a few lowly laborers.

Just take a study done by the University of Oxford in 2013, which found that a whopping 47 percent of American jobs are at very high risk of being supplanted by computers in the next 20 years.

Beyond that, technology is advancing at such a rapid pace that it becomes obsolete every five to seven years. Consider Blackberry smartphones or MySpace — can you think of anyone who uses either of them anymore?

Probably not, but just ten years ago they were both the height of popularity. It just goes to show that such changes aren't always visible in day-to-day life, but thinking back a few years can quickly illustrate how shockingly fast technology is developing.

### 4. Market globalization has fostered an increasingly interconnected world. 

The globalized market isn't just about manufacturing and trading goods; it's also about exchanging information and making financial transactions online. Consider the flow of data through Facebook, the exchanges between renters and travelers on Airbnb and the incessant stream of opinions on Twitter.

Such global digital flows have made the world much more interdependent on all fronts: financially, culturally and politically. For instance, when the author spoke to Facebook in May 2017, Facebook Messenger was poised to add its one billionth user. In the same year, Twitter had 328 million active users every month. That's more than the entire US population!

It's this level of interconnectivity that enables products to go viral at a scale and speed that was unfathomable in past generations. For example, in 2012, a photo of Michelle Obama wearing a dress from the online fashion store ASOS was retweeted 816,000 times and the dress instantly sold out.

So, what are the economic implications of this change?

Well, it used to be that money was earned through carefully acquired knowledge; if you had valuable skills, you could deliver products or services based on those skills. As a result, an individual could get an education, enter the workforce and feel secure in her career for the rest of her life.

But today, things are dramatically different. The global flows of information and commerce are much more important than any education, and things change so quickly that the most successful products today can be old news by tomorrow.

Even long-established companies have had to adapt to this shift, and General Electric is a great example. Instead of relying on its own base of engineers in the United States, China, India and Israel, the company now taps into the global flows by running contests that invite ideas from all over the world.

> In a poll conducted by the Boston Consulting Group in 2015, 38 percent of respondents said they would rather give up sex for a year than give up their mobile phone.

### 5. Climate change is accelerating, threatening to turn the world upside down. 

You don't have to be a meteorologist to know that the weather has been out of control in recent years; a scorching, record-high heat index of 163 degrees Fahrenheit (74 degrees Celsius) was recorded in a city in Iran in 2015, while traditionally frigid winter locales are experiencing milder and milder temperatures over the years. It's a tangible reminder that climate change is here and it's not going anywhere.

You've already seen how the power of men, machines and flows has disrupted the workplace, politics and the economy — but these forces are also reshaping the entire biosphere. For instance, Bloomberg.com reported that the concentration of atmospheric CO2 is 35 percent higher than at its peak over the past 800,000 years; sea levels have reached their highest point in 115,000 years; and the nitrogen cycle is experiencing a more dramatic upheaval now than at any point in the last 2.5 billion years.

Or take reporting from _Discover_ magazine, which deemed July 2016 to be the hottest July on record. Since July is typically the hottest month globally, this was really the hottest of all 1,639 months ever recorded.

It's just one more sign that the global climate "sweet spot," which has afforded humanity such a hospitable environment, is rapidly dissolving. What are the other impacts of this cataclysmic change?

Well, Americans may be focused on the migration of people from the Middle East into Europe, but two-thirds of all migration comes from other places, and most frequently as a result of climate change. In fact, shifting global weather patterns, resulting in droughts in Africa, produce more migrants than any other factor or phenomenon. Terrorism, unemployment and generally frightening futures in these failing African states can be linked to worsening environmental degradation, and leads these countries' citizens to head for European shores.

To make matters worse, there's no sign that climate change will slow down. According to the United Nations, by 2050, the world population is expected to rise from around 7.2 billion today to 9.7 billion. All those additional people will mean more cars, more homes, more water and electricity consumption and a far greater carbon footprint.

To put it bluntly, over the course of two generations, humans have absolutely maxed out Earth's ability to sustain us.

> _"We have exactly enough time, starting now."_ \- Environmentalist Dana Meadows

### 6. The age of acceleration may bring up some issues, but it has also enabled individuals to work toward the common good. 

In the end, accelerations are a mixed bag; some of them bear fruit while others can provoke disaster. Even so, there's good reason to be optimistic.

Technology and globalization have made it possible for anyone with a basic education and access to the internet to help humanity make collaborative decisions, while also handling the challenges these same trends produce.

For instance, in 2016, the author met a man whose family founded New Media Inc., an information technology firm specializing in big data analytics and media monitoring for the Turkish government, among others.

The company was founded by the man's family in 2007 and now has 100 employees. Most of the important positions are filled by his family members and the vast majority of the company's employees have only a basic education.

Another reason we can be optimistic is that there's a clear antidote to being overwhelmed by change: working toward the common good through human cooperation. Just take the author's hometown of St. Louis Park, Minnesota. This suburb of Minneapolis-St. Paul went from being a white, anti-Semitic backwater to a progressive multicultural community thanks to decades of forward-looking social policies.

By acknowledging the need for change, visionary leaders in the community helped support a rapidly growing middle class through good jobs and a great school system. People helped each other overcome their problems and, pretty soon, partisanship, prejudice and pessimism were things of the past.

As a result, the community there today is thriving and harmonious. For instance, 70 percent of voters still support funding schools, even though only 15 percent of the population have children of school age.

We can all take an example from this incredible transformation. Each of us, no matter where we are, must abandon the selfish, narrow-minded outlook that has been instilled in us and help pave the way to an equitable, dignified society.

### 7. Final summary 

The key message in this book:

**The world is changing more quickly than ever. Rapidly evolving technology, global markets and climate change all imply significant accelerations to the pace of life — and these factors are exerting a major impact on our lives. In this wild new world, working together for the common good may be humanity's last hope.**

Actionable advice:

**Get out there and interact.**

When the author asked Surgeon General Vivek Murthy what the leading disease affecting Americans is today, he answered immediately, "It's not cancer. . . not heart disease. It's isolation." Ironically enough, while we're living in the most technologically networked and interconnected period in history, we are feeling more isolated than ever. In this environment, human-to-human interaction is essential to your health. So, put down your smartphone and strike up a conversation in the real world.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _In Praise of Slowness_** **by Carl Honoré**

_In Praise of Slowness_ (2005) offers both an indictment of and an alternative to the high-speed lifestyle that plagues many people today. It examines how the rat race impacts our minds, bodies and souls — and offers concrete tips on how to slow things down.
---

### Thomas L. Friedman

Thomas L. Friedman is a reporter, columnist and three-time Pulitzer Prize winner. He is currently a foreign affairs columnist for the _New York Times_ and is the author of such other books as _From Beirut to Jerusalem, The Lexus and the Olive Tree_ and _The World Is Flat._

