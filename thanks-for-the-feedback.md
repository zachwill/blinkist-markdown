---
id: 54b3ab7164663300094a0000
slug: thanks-for-the-feedback-en
published_date: 2015-01-14T00:00:00.000+00:00
author: Douglas Stone, Sheila Heen
title: Thanks for the Feedback
subtitle: The Science and Art of Receiving Feedback Well
main_color: D92B48
text_color: BF2640
---

# Thanks for the Feedback

_The Science and Art of Receiving Feedback Well_

**Douglas Stone, Sheila Heen**

_Thanks for the Feedback_ is about learning from people and experiences, whether at home or at work. It sheds light on different types of feedback and their importance, and how you can take any kind of feedback in a positive, constructive way and use it to better yourself in your career and relationships.

---
### 1. What’s in it for me? Learn how to give and receive the best feedback. 

Which is the better feedback? When your boss sits you down and tells you what's wrong with your work and how it can be fixed, or when he throws it at your face and calls you useless?

We probably all know that we should give people constructive feedback. And anyone who has experienced bad feedback will know that it is unhelpful and hurtful. But how can we ensure that the feedback we give is constructive and inoffensive?

These blinks show you how. They provide you with the knowledge you need not only to pass on the best, most productive feedback, but also to ensure that you receive the same level of evaluation from others.

In these blinks you'll discover

  * why the way we receive certain types of feedback is determined by our genes;

  * why we all think we are in the top ten percent of managers; and

  * how bad feedback can ruin a relationship.

### 2. There are three main types of feedback, and they serve different purposes. 

Have you ever gotten back an exam paper with a grade on it and asked yourself, "What do I make of this?" The grade might tell you where you stand, but it doesn't help you if you want to know how to improve. It's just the wrong kind of feedback.

There are three main types of feedback that actually work: _appreciation_, _coaching_ and _evaluation_.

They each have different functions. Appreciation motivates and encourages, coaching helps you improve, and evaluation helps you understand where you stand, and what's expected of you.

Before a basketball game, your coach might give you a pep talk — that's him _coaching_ you. During the match he might _encourage_ you from the sideline, by saying "nice shot" or "keep it up." After the game, he will probably _evaluate_ your team's strengths and weaknesses, so you'll know how best to continue training.

Different situations call for different types of feedback. When you've already given your best and feel exhausted, it is important to be appreciated for your effort. In this situation, it wouldn't help much to be coached on doing things better.

On the other hand, when you're in trouble and actually _need_ coaching, some cheerful appreciation like "you're doing just fine" won't help either. You need to learn what kind of feedback will help you in which situations, so you can go seek it out.

So tune into your needs and wants. If you want to write better papers in the future, ask your professor for some pointers. Then you can improve by listening to their feedback.

Learn to identify what feedback you need. It'll be much easier to get it.

### 3. Understand your feedback, so you can know how to respond to it. 

We've all been through this: someone tells you what you're doing wrong, but you know that _they're_ the one who's _really_ wrong. If we feel this way about someone's feedback, we often just ignore it. But sometimes feedback like this isn't "wrong" — we just don't understand it.

Try to understand the intentions of the person giving you feedback before you become defensive. Look for key points in the feedback you're receiving.

Sometimes, our feedback consists of generic comments that aren't precise. These comments can mean different things to the person saying them and the person hearing them.

Consider the sentence, "You're a reckless driver." It'd be much easier to understand that if you knew the context and the specific reasons that led to the comment. It might really mean "every time I'm in the car with you, you're on your phone!" If you receive a comment like that, ask yourself why someone thinks of you that way.

You also need to understand what the person wants you to do with the feedback. Should you drive more carefully in general? Or get a hands-free phone installed?

When evaluating feedback, really consider a person's view of you. We can't think of ourselves objectively.

A survey in 2007 illustrated this. It reported that 90 percent of managers thought their own performance was in the top ten percent. Clearly they're not all right, because all 90 percent of them can't be in that ten percent! It's difficult to see ourselves clearly.

Don't forget that other people's opinions are valuable, especially when they have access to more information than us.

Imagine someone criticizes your management style because they have access to new sales statistics, or they're being told by customers that your tone is unprofessional. If you felt you were doing fine, there's clearly a big difference between what you think and they think. Use this information to your advantage — don't just ignore it.

### 4. Feedback is the key to seeing your blind spots. 

Have you ever impersonated someone, only to hear that person say, "I don't do that," while everyone else laughs and agrees with you? We're often surprised to hear how others perceive us.

We literally don't _see_ ourselves the way others do. We can't see how our facial expressions give away a lot more than we think they might.

Say you want to be friendly to your coworkers, because they've complained that you're cold sometimes. So you give them a smile. But if you don't really mean it, they'll probably recognize that your smile is insincere.

We also can't hear our own tone of voice, which means we can't judge it.

If you tend to have an aggressive tone, for instance, you might not notice it because you're used to it. To you, it could seem normal.

We interpret things about ourselves differently than others do. If we make a mistake, we tend to blame the circumstances. Other people, of course, are more likely to blame us.

If someone complains that you're unfriendly, you'll probably find an excuse for it. You'll say you were stressed out that day (i.e., circumstances). But the other person might just think that's part of your character.

We also tend to judge ourselves by our intentions, while we judge others by their actions. Remember that insincere smile? You made a big effort to seem more friendly, while your co-workers thought you were a phony. They may think of you as _less_ friendly than before.

The point is that we generally view ourselves more positively than others do, so we have to stay wary of that. It's OK to be positive, but it makes us more likely to be upset when we receive negative feedback.

So use feedback to learn your blind spots, and build on that knowledge to gain even more from future feedback.

### 5. Relationships play a big role in the way we interpret feedback. 

Have you ever wondered why it's easy to accept some people's criticism, while others make you angry or dismissive?

The relationship we have with the person giving us feedback influences the way we interpret their opinions.

We don't take in all feedback equally. What matters is whether we think the person giving it is trustworthy and sincere.

You might reject the feedback of a colleague whom you've always thought wants your job, for instance. Does he really have the best intentions?

At the same time, you might dismiss your best friend's advice about your job because you think to yourself, "What do they know about it?"

These judgments are counterproductive, and prevent us from developing. Your colleague may be your competitor, but he could still have a valid point. And your best friend might have great ideas, regardless of their professional background.

Feedback often puts pressure on our closest relationships. We expect many things in a relationship — like feeling appreciated or having freedom to do things our own way. Feedback threatens both of these. When someone tells you what they think you should do, it often gives you the feeling they're trying to control you, or improve you — meaning you weren't perfect to them in the first place!

Personal relationship dynamics get mixed up in feedback as well. For example, imagine you complain about your partner giving you a gift you don't like. They might then complain about you being ungrateful. Who's right?

There are two sides to the situation: for you, the issue is that your partner doesn't care about your wishes. From their point of view, you're unappreciative.

If we let our frustration take over, neither side will gain any insight from the feedback. We won't be able to use it to develop, and both sides will feel resentful.

> Fact: Strangers give the best feedback. It is easier to take feedback from someone without any ulterior motives.

### 6. Relationships can create tension for giving and receiving feedback. 

Tension often occurs because people are simply different. You like to relax after dinner, while I want to get the dishes done right away. That's just how we are, right?

Sometimes we have to take a step back to see what influences our relationships. This makes it easier to accept feedback and change our behavior for the better.

Our roles in certain environments — like work or family — can cause problems in our communication with one another.

For instance, a policeman and a driver who's been caught speeding might actually have a lot in common, but their roles automatically make them adversaries. They don't allow for flexible interpretation, and tension can hardly be avoided.

If your close friend from work suddenly gets promoted and becomes your boss, your relationship can be tricky. They won't know exactly how they should treat you.

The environment you're in also affects how two people interact. In a car accident, for instance, you shouldn't just immediately blame one of the two drivers. Look at the whole picture. Maybe a third car was involved, or the street lights weren't working.

Approach relationships the same way: if a child is mean to other kids, maybe it's because dominance is rewarded in their family. Or perhaps another child is encouraging them to be mean.

This sort of analysis doesn't justify the child's behavior — it helps you understand it. Understanding is key to making the most of feedback.

So when listening to feedback, consider your feelings toward the person, and their feelings toward you. And remember to take two steps back, and look for other forces at play.

### 7. Our brain wiring and emotions affect the way we analyze feedback. 

Have you ever noticed how some people take feedback much better than others? Part of the way in which we accept feedback is based on our brain wiring.

Our genes define our _default level of well-being_. Some people are just naturally happier than others. Studies have shown that genes — rather than life experiences — cause at least 50 percent of these differences. People also have different ranges of emotions — some people are more emotional in general.

It is easy to observe this in children. Some kids are highly sensitive to the smallest things, like sharp noises, while others are not. Naturally, our sensitivity to feedback differs as well.

The higher your baseline of happiness, the more likely you are to respond well to positive feedback. When someone's baseline of happiness is lower, they're more likely to respond poorly to negative feedback.

Our brain also influences how fast we _recover_ from feedback.

In a study from 2002, a researcher named Richard Davidson found that the time over which we can sustain positive emotions, and the time we need to recover from negative emotions, can differ by up to 3,000 percent.

The left and right sides of our brains also process negative and positive feedback differently. People with stronger brain activity on their right side recover from negative feedback more slowly.

Interestingly, we all struggle with one problem unrelated to our brain structure. We all feel bad emotions more strongly than we feel good ones. This is because our brains are wired to detect threats, to help us survive.

Knowing how our brains are wired helps us accept ourselves. But of course, genes are just one part of the story. Our emotions, life experience and decisions make up the rest.

### 8. Establish a “growth mentality” so you can handle feedback well. 

You've probably been in this situation: someone starts criticizing you, and you get defensive and angry. Aside from our genes, what other factors make us perceive feedback as a threat? Well, it's also about our attitude.

We often simplify our identity to an "all or nothing" label, like "I'm a good person" or "I'm a hard worker." These labels don't allow for much flexibility.

So negative feedback can threaten our entire identity. If someone says, "You weren't helpful in our task yesterday," you might be hurt to learn you aren't a hard worker, and reject the feedback.

Instead, try moving from a _fixed mentality_ to a _growth mentality_. We tend to see our personality traits as _fixed_, but this isn't helpful. Instead, focus on how you can _grow_.

A researcher named Carol Dweck studied this by giving children a puzzle to solve. She found that the children with a growth-mentality coped much better when they failed at the puzzle, because they sought to improve. The children who thought their puzzle-solving skills were fixed became discouraged.

So if you perceive a challenge as an opportunity, and feedback as a way of learning, you will truly develop. If you apply yourself to something, you'll keep getting better at it.

If you have a growth mentality, you'll _believe_ you can get better, and you'll be motivated to keep going. A positive attitude makes a big difference in personal development.

Of course you won't become a tennis pro just by believing in yourself. Some things _are_ determined by our genes, as we've seen. But always remember that you can improve on many of the most important things in life — like intelligence, creativity, compassion and confidence — by keeping a positive attitude and healthy growth mentality.

### 9. Final summary 

The key message in this book:

**Nearly every kind of feedback — whether you agree with it or not — can be used to help you grow. The more we can understand feedback with an open mind, the more we'll develop. When you learn to process feedback effectively, there will be no limit to your personal development.**

Actionable advice:

**When someone gives you feedback, put yourself in their shoes.**

Don't just reject any negative feedback — even if it comes from a person you don't like. Consider their perspective, and how their role or relationship with you might be affecting what they say, and how you feel about it. Finally, remember that they're just one person — you don't have to re-evaluate your entire life because of one person's feedback.

**Suggested further reading:** ** _Getting to Yes_** **by Roger Fisher, William Ury and Bruce Patton**

_Getting to Yes_ is considered _the_ reference for successful negotiations. It presents proven tools and techniques that can help you to resolve any conflict and find win-win solutions.
---

### Douglas Stone, Sheila Heen

Douglas Stone and Sheila Heen are lecturers at Harvard Law School and the co-founders of Triad Consulting, which has advised big names from BAE Systems to HSBC. They are also the authors of _Feedback: Evaluation Challenge._

