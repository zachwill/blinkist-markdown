---
id: 557da7936533380007900000
slug: the-1-percent-windfall-en
published_date: 2015-06-17T00:00:00.000+00:00
author: Rafi Mohammed
title: The 1% Windfall
subtitle: How Successful Companies Use Price to Profit and Grow
main_color: 882820
text_color: 882820
---

# The 1% Windfall

_How Successful Companies Use Price to Profit and Grow_

**Rafi Mohammed**

_The 1% Windfall_ (2010) introduces the often-overlooked strategy of price setting and shows how companies can grow even further by making smart pricing decisions. How can a firm not only survive but also thrive amid stiff market competition or even inflationary periods or a recession? These blinks will help you find the path to attracting the customers you want and keeping those you have.

---
### 1. What’s in it for me? Price your products correctly and watch more profits roll in. 

If you ask business owners what they least enjoy, they'd probably say haggling over prices.

Finding the ideal price for your product is often a struggle. Most take the easy way out, looking simply at what it costs to make a product, and doubling that sum. But by not taking a serious look at your company's pricing strategy, you're potentially missing serious profit opportunities.

Consider this: A study showed that a 1 percent price increase can generate an 11 percent growth in operating profits! This is the secret behind "The 1 Percent Windfall," a strategy that will help you price your company's product to its best advantage.

These blinks pull from real-life examples to show you exactly how strategic pricing can transform your profits, crush the competition and help your company grab a broader market share.

In these blinks, you'll discover

  * why Disney World is comfortable with customers tiring of its services after three days;

  * what exactly is Morton's greatest beef steak trick; and

  * what pricing strategies from Ralph Lauren and American Express have in common.

### 2. When selling your product to a single customer, use the strategy of one-on-one pricing. 

How do you determine your product's price? If profit is your priority, the best way to do this is through _value pricing_.

_Value pricing_ uses the customer's next-best alternative as a base price and adds or subtracts cost, depending on the product's attributes. There are two ways to calculate this: _one-on-one pricing_ and _multi-customer pricing._

Which pricing strategy should you use, and when?

To set a _one-on-one price_, first identify your target customer. Then identify your customer's next-best alternative product, and the difference between your product and the other product. Based on this calculation, you can then determine your product's value.

One-on-one pricing is your best option when a product or service is sold to a single customer, such as selling a home. One-on-one pricing is also the most viable option when pricing a new product. 

Say that you and your neighbor are renting your homes. The only difference between the two homes is that your house has a pool. In this situation, there are three possible outcomes:

In the first outcome, the rental price your neighbor has set, $1,000, is reasonable. You know that people are willing to pay 20 percent more for a pool, so you then set your price at $1,200. 

In the second outcome, your neighbor's rental price, $2,000, is too high. But you can't charge above a reasonable market price just because competitors are doing so. Knowing that a reasonable price is $1,000, you add the value of the pool to come up with a price of $1,200.

In the third outcome, your neighbor has set a too-low rental price: $500. You'll have to accept this low price as a base. But since people can rent your house at such a discount, they'll be willing to pay more for a pool, say 30 percent more. With this calculation, your price is then $650.

Our next step is understanding how to sell to many customers: Read on for more!

### 3. Use a strategy of multi-customer pricing to set the ideal price for a good that is mass-produced. 

While one-on-one pricing works in specific circumstances, companies that produce products on a large scale for many customers need to employ a different strategy.

_Multi-customer pricing_ is the most effective strategy to set prices for a broader customer base.

Setting a value-based price means finding the optimum trade-off between margin and quantity sold. In general, a high price brings higher margins, but you'll sell less; a low price brings lower margins, but you'll sell more.

With these two things in mind, let's set a multi-customer price.

To start, follow the initial steps for one-on-one pricing. Identify your target customer, their next best product alternative, and then differentiate your product from that other product.

Here's where things are different. Now, you'll need to create a _demand curve_ and perform a comparative analysis to determine the most profitable price for your product.

A _demand curve_ is the correlation between a product's price and the corresponding number of customers willing to buy it at each price. Where do you get such information?

This is where market research comes in. Company XYZ, for example, identified that the highest and lowest possible prices for its product were $5 and $1. Research showed that among 100 customers, 20 customers would buy at $5; 40 would buy at $4; 60 at $3; 80 at $2; and 100 at $1.

With this, determine the most profitable price for your product by calculating revenues, costs and profits at different prices and production levels.

Let's say Company XYZ's production cost is $2 per unit. The company will lose $100 if it sets its price at $1, and will break-even if the price is set at $2.

What price would you set, and how many units would you make?

According to market research, the optimum profit is achieved when Company XYZ makes 40 units, which would costs $80, and then sells a unit at $4. This would bring in $80 of profit!

### 4. Use specialized pricing plans, like a success fee or peace-of-mind, to convert a “no” to a sale. 

Often your prospect will be close to buying, only to say "no" due to financial concerns. To avoid this, consider pricing strategies that will turn the "no" into a "yes."

If convincing your customer of the value of your product is the problem, consider offering a _success fee plan_, which has a lower base price but offers additional payments in the case when key success metrics are achieved.

For example, when the Boston Red Sox signed star pitcher Curt Schilling, the team offered a base salary of $8 million, with an extra $2 million if Schilling stayed in shape and $4 million more if he pitched the whole season without interruption.

What about price fluctuations? You can attract a customer by offering a _peace-of-mind guarantee_, which sets a fixed price for a product for a specific period.

High-end steakhouse chain Morton's has such a plan. The chain needs to have a steady supply of high-quality meat, and even a 5-10 percent unexpected price fluctuation in the market can easily destroy company profits. And if its supply chain is somehow interrupted, market share — not to mention reputation — can be seriously damaged.

So to secure both prices and supply, Morton's bought peace-of-mind guarantee contracts to ensure that 70 percent of the beef it purchases is locked into a certain price, no matter what the market conditions.

Sometimes customers want to buy a product but can't afford a big, one-time payment. _Financing_ _plans_ allow customers to spread payments over time.

Electronics company Best Buy offers a two-year financing plan without interest for customers who spend over $999. And since a minimum purchase was set, many customers actually added items to their cart to hit the minimum — which in turn raised Best Buy's sales volumes above Amazon's.

And importantly, the company was able to reach out to the low-end market without actually lowering prices.

> _"...the key is to offer customers a variety of pricing options. This strategy is win-win: profits to companies and choices for consumers."_

### 5. Vary your pricing plans to attract both high-end and budget-conscious customers. 

We all have our own comfort zones when it comes to prices, which explains why a jacket that's too pricey for you is a bargain for your fashion-minded friend!

_Differential pricing_ helps bring in higher profits from those customers willing to pay more, while at the same time winning new customers who'd prefer to pay a bit less.

New York's Omni Berkshire Hotel puts differential pricing in action by varying the price of its hotel suites in different listings.

On Priceline.com, the hotel lists suites with prices starting at $136 per night. But with hotel agent NY City Luxury Hotel, the very same suites are listed from $231 to $257, including the option to cancel without penalty. In this way, the hotel can attract a wider range of customers, depending on budget and need.

But how do you differentiate between customers? Differential pricing tactics are largely based on _customer characteristics_ and _selling characteristics_.

The price of insurance plans is affected by customer characteristics. All U.S. insurance companies offer data to the Comprehensive Loss Underwriting Exchange database. Companies can track a prospect's previous claims, to then offer an adequate insurance premium.

What about selling characteristics? Remember that the more units of a product a customer consumes, the lower the next unit is valued.

At Disney World, customers relish the first day's visit. A second day is equally exciting. The third, however, is tiring; a fifth and the thrill is gone.

Disney incorporated this _diminishing valuation_ of selling characteristics into the pricing of a five-day pass. A single-day pass costs $75; a second day costs $74; and a third costs $63. Yet a fourth day costs just $7 and a fifth day $3.

The idea to offer a fourth and fifth day for a hugely discounted price — instead, for example, of just offering a three-day pass — is that it brings Disney not only that extra $10 but also the additional revenues from food and souvenirs purchased on those extra days.

### 6. Think high-end and low-end when creating distinct versions of your product to suit different needs. 

It's hard to resist spending a few extra dollars for a premium package. But how exactly do companies convince us to do so?

The truth is that just a few minor modifications of a product can attract tons of new customers. We call this tactic _versioning –_ different versions of a product are created to meet basic, premium and unique customer needs.

Take American Express. The credit card company offers three different cards, each with unique benefits.

The basic option is its "green" card, at $95 per year. A "platinum" card costs $450 per year but comes with additional premium benefits. And the "black" card, with a price tag of $5,000 per year, meets the needs of those looking for a more exclusive option.

Simply by enhancing a product's features to make it faster, better or more accessible allows companies to both attract new customers and encourage existing ones to upgrade.

For example, in 1994 Ralph Lauren created its Purple Label menswear line, using only the finest quality cloth for its existing fashion designs. While a "regular" shirt from Ralph Lauren costs $79.50, a Purple Label shirt was priced at up to $365.

By creating a high-end version of its existing product, Ralph Lauren was able to compete in another market segment, with exclusive brands like Gucci and Armani.

While adding special features can attract high-end clients, stripping your product to its basic functions can draw in even the most price-sensitive customers.

The Los Angeles Lakers, for example, sell tickets to its basketball games ranging from $2,500 to sit courtside to $10 to watch the game near the top of the vast arena.

### 7. Price defensively during recessions and inflation. Innovative thinking will help keep profits stable. 

Economic recessions are something every business must cope with. What's your plan of action?

As demand for your products drops, lowering prices seems like a logical move. This is a double-edged sword, however: just think of how difficult it may be to raise your prices again.

To avoid this dilemma, arm yourself with a well-structured pricing strategy, as this will keep your organization afloat no matter what the world throws at it.

Instead of lowering prices on products that ought to cost more, a more effective recession strategy is the introduction of a _fighter brand_. This is a line specifically designed for times of crisis, to meet demand for low-priced products that have basic functions.

Guitar maker C.F. Martin & Co. suffered from a 20 percent sales drop during the 2008 recession. Though its most-popular models cost $2,000 to $3,000, these prices were simply too steep to attract new customers. So CEO Chris Martin introduced a new, basic model that cost less than $1,000.

The new budget guitar proved incredibly popular, and over 8,000 were sold within the first year.

This strategy boosted the company in three ways. While high-end customers continued purchasing more expensive models, price-sensitive clients had a budget option. And after the recession, the company withdrew its budget model, enticing its newly expanded customer base to upgrade to its line of higher-end guitars.

But what if recession isn't the issue, but a period of inflation? There are pricing strategies for this too.

One strategy companies use to _accommodate_ inflation is to reduce quantity yet keep prices stable.

For example, in 2008, prices were rising and companies had to react. Unilever decided to reduce the size of its Breyers Ice Cream containers, from 56 ounces to 48 ounces. This way, it could accommodate rising material costs without charging more, all with one subtle, almost unnoticeable change.

### 8. Final summary 

The key message in this book:

**No matter how big your market, elusive your prospects or deep the market recession, there are always strategies to help you get the most profit out of your products. Think ahead and plan for every market eventuality so your pricing strategies keep you growing even in the worst of times!**

**Suggested further reading:** ** _Simple Numbers, Straight Talk, Big Profits!_** **by Greg Crabtree and Beverly Blair Harzog**

_Simple Numbers, Straight Talk, Big Profits!_ (2011) outlines the essential, interconnected elements you need to know that affect your company's longevity and growth. Through a series of simple steps, you can create a more productive workplace to ultimately boost performance and build greater wealth.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Rafi Mohammed

Rafi Mohammed is the founder of Culture of Profit, a consultancy that helps businesses develop and improve pricing strategies. With some 20 years of experience in his specialty, Mohammed has also authored other business books, including _The Art of Pricing_.

