---
id: 523069aae4b0678a5bde879a
slug: the-100-dollars-startup-en
published_date: 2013-09-19T00:00:00.000+00:00
author: Chris Guillebeau
title: The $100 Startup
subtitle: Fire Your Boss, Do What You Love and Work Better to Live More
main_color: 397957
text_color: 397957
---

# The $100 Startup

_Fire Your Boss, Do What You Love and Work Better to Live More_

**Chris Guillebeau**

_The $100 Startup_ is a guide for people who want to leave their nine-to-five jobs to start their own business. Drawing from case studies of 50 entrepreneurs who have started microbusinesses with $100 or less, Guillebeau gives advice and tools on how to successfully define and sell a product, as well as how to grow your business from there.

---
### 1. Break away from the nine-to-five and become your own boss – it’s a lot simpler than you think. 

Have you ever dreamt of quitting your nine-to-five job and striking out on your own?

All around the world, more and more people are leaving the hierarchies and rules of traditional employment to follow their passion and start their own business.

They are creating _microbusinesses,_ which allow them a life of freedom and purpose. This form of employment, typically a one-person business, is nothing new: consider the street merchants of ancient Athens independently hawking their wares.

So what do you need to start a microbusiness?

Probably a lot less than you think. Every day, ordinary people are setting microbusinesses, often with no funding, no experience, no MBA and no detailed business plans!

Typical stories include that of two designers who, fresh out of school and on a whim, started selling beautiful custom-made maps online, and wound up making so much money they quit their day jobs after just nine months.

Or consider the piano teacher who created a piece of software to help him track his students and lesson schedules, and wound up turning it into a full-time source of income, generating over $30,000 a month.

There is no reason why your business can't attain similar success. The only things you need are

  * a product or service that you can sell,

  * customers who are willing to pay for it,

  * a method for them to pay with.

It's as simple as that.

The most important stage in the entire process is making the choice to act. Many successful entrepreneurs feel the greatest struggles on their journey did not come from competitors or market forces but from their internal insecurities and fears. Don't succumb to them. Instead, make the leap.

You already have what you need.

**Break away from the nine-to-five and become your own boss — it's a lot simpler than you think.**

### 2. Passion is not enough for business success; you also need customers and the right skills. 

Most people dream of making a living from something they're passionate about, such as traveling to exotic countries. But there's a catch: unless someone is actually willing to pay for it, your passion "business" is just a hobby.

The true recipe for success is to find the sweet spot where your _passion_ s _,_ your _skills_ and the _needs of others_ meet. But where do you start?

The most important part of any business is _providing value_ to customers. Therefore you must focus relentlessly on how you can help other people.

Usually this means that you can't turn your passion directly into a business but must work in an adjacent field where you help others follow that same passion. For example, one enthusiastic traveler, Gary Leff, started a microbusiness using his expertise to book the best possible trips for others with their bonus air miles, which many people struggle to find good use for.

But what if you lack the skills to pursue the business of your choice? Enter s _kill transformation_. Though you may not have the specific skills needed to pursue an opportunity, you may have related ones. You use these as best as you can and pick up the rest as you go, thus transforming your skillset.

Kat Alder, for example, was a waitress in London with great people skills. She eventually went on to start her own public relations company, and though she had no experience in the field, her people skills helped her as she learned the basics of the business.

Not every passion can become a business. No matter how much you enjoy eating pizza, you can't start a business out of it. But you may be able to start a pizzeria. With such opportunities, think hard about how you can provide value to people and what skills you have at your disposal.

**Passion is not enough for business success; you also need customers and the right skills.**

### 3. Understand your customers’ deepest needs and craft your offering accordingly. 

To build a successful business your offering needs to provide value to customers.

To understand whether this is the case, you need to know whom you are selling to. What are your target market's demographics? Don't lazily define your customers by age, gender or income; think hard about their shared personality. What are their passions, skills and values?

Once you understand your customers, the next question is: Are they interested in your would-be product? To find out, go ahead and ask them. Approach potential customers, either informally or via surveys, and find out if they would pay for your product. Do they have any other problems you could solve or tweak your product to address?

An even more prudent approach is to first advertise your product to see if you get enough orders to make it viable and only then produce what has already been sold. One car enthusiast advertised a guide for high-end cars via a magazine, and only after selling two for $900 each did he decide it was worth his while to write it.

Of course, keep in mind that sometimes what customers say they want may differ from what they actually want. For years, airlines have received complaints about the cramped seating on planes. But whenever an airline tries to offer more leg room at a slightly higher price, they inevitably find that people prefer to fly with cheaper, cramped competitors.

To truly be successful, you need to dig beyond superficially expressed needs and address even the unspoken ones. Consider Kyle Hepp, a wedding photographer who is often told by wedding couples that they don't want any traditional wedding photos whatsoever. She still takes a few anyway because she knows the bride and groom's families will be happy to have them.

**Understand your customers' deepest needs and craft your offering accordingly.**

### 4. Get creative with your marketing, and focus on the benefits that your product provides. 

Just as with creating compelling offerings, the key to great marketing is focusing on how you can provide value to people.

When crafting your messages, don't devote too much attention to the features of your product size, power or speed, etc. Instead focus on the _core benefits_ for the customer: how it will help them.

These core benefits often relate to the _emotions_ you evoke in customers, not the physical goods or services you deliver. Consider the V6 Ranch in California, whose owners emphatically state: "We're not selling horse rides, we're offering freedom." Similarly, a yoga retreat may not really sell yoga instruction but rather stress relief and tranquility.

Once you know what core benefits you're offering, how can you get the word out, considering your microbusiness budget?

You need to _hustle_. Hustling means getting creative with marketing (e.g., connecting with journalists, collaborating with other companies, writing guest posts for blogs) rather than paying for advertising. Typically, hustling will bring in far better results than traditional marketing, at a fraction of the cost; hence, it is crucial for microbusinesses.

Another creative and powerful marketing tool is _strategic giving_. Think about it; if you give your product away to customers or other stakeholders, or help them in other ways, what will they do? Most likely they'll thank you by spreading the word.

Consider John Morefield, an unemployed architect who started a "5-cent architect advice" shop in a Seattle farmers market. He really gave professional architectural advice for just a nickel, but many customers were so pleased with his work they commissioned him for more at full price. The story even got picked up by CNN and other news channels, creating lots of buzz and even more customers for him.

**Get creative with your marketing, and focus on the benefits that your product provides.**

### 5. Successful launches rely on thorough preparation and instilling a sense of urgency. 

Every entrepreneur's worst nightmare is to spend months working on a product and then not sell a single one.

To avoid this, it is essential to plan and prepare your launch meticulously. Much like the premiere of a Hollywood movie, a successful launch is advertised so heavily beforehand that people are drooling for it months in advance.

Before starting your launch campaign, you should hustle and self-promote like crazy to build up an audience of prospects and customers.

Engage them in good time before the launch; build up their anticipation by proactively telling them about the project you're working on, why it will be valuable to them and how the launch will unfold.

Keep reminding them of the details to keep them on their toes. Finally, when the big day arrives, make it as easy as possible for them to make a purchase; the decision to buy has hopefully already been made earlier.

For entrepreneurs, this is a nail-bitingly exciting time, and the sense of relief when the first sale comes in is incredible.

Plan the launch so that customers have a limited time to get the product at a particular low price, and remind them of this fact as the deadline approaches. That little extra nudge often turns hesitation into action. Once the window of opportunity is over, refuse to sell the product at that price anymore.

When the author launched his online business course, the Empire Building Kit, he was on board an Amtrak train called the Empire Builder, chugging from Chicago to Portland. The deadline for customers to seize the launch offer was when the train arrived in Portland after a 24-hour trip. The story of the train-ride launch was so compelling, it proved a hit with customers and brought in over $100,000 in 24 hours.

**Successful launches rely on thorough preparation and instilling a sense of urgency.**

### 6. A business needs to make money, so stay focused on your costs and income. 

No matter how passionate you are about your business or how much value it provides to others, it needs to make money. Otherwise, it's just a hobby.

This means that you need to consider things like your funding, your costs and, of course, your income.

The first point is the simplest to address: taking on debt to start a business is _no longer necessary_. For many, the cost of a laptop and a website is the only investment required, and some can make do with less than $100. Consider Chelly Vitry who started a food tour guide business in Denver with just $28 in startup costs, and wound up making $60,000 per year.

If you do need funding but have trouble getting it from the bank, you can always consider more unconventional methods such as crowdfunding. Shannon Okey raised $10,000 on Kickstarter for her craft publishing business when the bank refused her loan application.

The second financial concern ­– your costs ­– need close attention. You probably don't have a huge buffer of cash to tide you through low-income months, so keeping costs low is essential. Only invest in things that will _directly impact_ your sales. A glitzy website, for example, may look nice but unless you know for a fact it will bring in new sales, you don't need it.

Finally, to increase your income, be proactive about business development — don't just fight fires as they occur. Take time to think about developing your business and your offering. What new products are you working on? How can you make more sales? Are there any long-standing problems that need fixing? Make it a point to actively follow one or two key metrics, such as sales per day or average order prices, to keep focused on the lifeblood of your company.

**A business needs to make money, so stay focused on your costs and income.**

### 7. Get paid more than once, price your product ambitiously and use small tweaks to make a big impact in profits. 

How can you make your business as profitable as possible?

One key principle is _getting paid more than once_ by providing a _subscription service_. Just think: with just 400 subscribers, a $20-per-month subscription will bring in close to $100,000 every year! Bizarre sounding businesses like the Cupcake of the Month Club exist because this model is so profitable.

A second key factor is pricing: make sure your prices are based on the benefits you provide, not the cost of producing a service. Gary Leff, the travel consultant, who books trips for his customers using their bonus miles, charges a flat fee of $350 per booking regardless of whether it took him 5 minutes or 5 hours. This is because the _benefit_ to the customer is the same.

On a similar note, don't hesitate to periodically _raise_ your prices. Quite often customers react far more positively than you'd suspect, the tone being, "It's about time. You're worth much more than you charge!"

Also, to further maximize profits, give customers a _limited range of price_ rather than a single one. You don't want to confuse people with a dozen options, but some people always want "the premium edition" of everything, so why not give them a spruced up version of your basic product with a heftier price tag?

Once your business is up and running, you must continue to fine-tune or "tweak" it for optimum profitability. The main areas to focus on are: _traffic to your website_, _conversion from visitors to customers_ and _the price of an average sale_. These metrics each have a huge impact on your bottom line; therefore, you must keep experimenting with small tweaks (e.g., to your website copy) to find out what brings in the most profit.

**Get paid more than once, price your product ambitiously and use small tweaks to make a big impact in profits.**

### 8. Make your business as big as you want: stay small or grow by bringing in other people. 

Entrepreneurs differ greatly in their ambitions for their businesses. Some are happy with their one-person show, some dream of huge growth, while others still want something in between. Each option is perfectly acceptable.

If you do decide to grow your business, this will generally happen either _vertically_ or _horizontally_. Vertical growth means you start offering more services to the same customers, engaging them on a deeper level. For example, if you sell customers some piece of software, you could offer those same customers training on how to use it.

Horizontal growth, on the other hand, means you create new offerings that appeal to different customers. One website designer found that her customized designs were too expensive for some people, so she made some standard "themes" which she sold at a cheaper flat rate to more frugal customers.

As you grow, you may soon find that you simply can't manage everything by yourself. One solution is to _outsource_ certain tasks; anything from cleaning the home office to booking meetings to actually manufacturing the product. Outsourcing, however, divides opinions, and its suitability greatly depends on the type of business you run and your personality. If, for example, most of your business has to do with customer relationships or you are a control freak, outsourcing is likely to be disastrous.

If your dream is to someday sell your business and retire on an island in the Caribbean, your business must be _scalable_ : it must be _teachable_ so the buyer can scale up by training dozens more people in what you do, but also _valuable_ so that lots more customers can be found to gobble up the increased production.

**Make your business as big as you want: stay small or grow by bringing in other people**.

### 9. Keep plans simple so you can focus on the action. 

When it comes to starting and running a microbusiness, _action beats planning_ every time. Far too many would-be entrepreneurs spend their time making complicated plans that never materialize.

Therefore, when you do plan, keep it simple: your business plan should fit on one page and merely outline — not explain exhaustively — what you're going to do.

Jot down what you'll sell, whom you'll sell it to, why they would buy it and how you'll actually get paid. For example, your plan could simply be "to sell customized exercise plans to health-savvy professionals who need help with varying their routines, costing a monthly $20 subscription fee over PayPal."

You should also include any planned marketing tactics such as guest blogging, and key business metrics such as daily visitors to your site.

Finally, set yourself a deadline: "I will launch this project no later than XX." This vital addition helps ensure your plan turns into action.

On a similar note, your mission statement should also be short and simple — ideally, no longer than a Twitter message: 140 characters. Go ahead; try formulating one. This exercise will help you narrow down the purpose of your business without the corporate-lingo that plagues many mission statements. Your Twitter-length mission could be: "I help busy, health-savvy people plan their workouts so they can feel good and keep fit."

When leaving the workforce and starting your own business, you'll typically find a lot of people giving you unsolicited advice. While usually well-meaning, most of these people do not own their own businesses, so their advice is often unhelpful and even distracting.

Remember, you do not need anyone else's permission to succeed. Learn to trust your own judgment and take the leap. You won't believe how good making that first sale will feel.

**Keep plans simple so you can focus on the action.**

### 10. Final summary 

The key message in this book:

**Leaving the shackles of traditional employment and starting your own microbusiness demands a lot less than you think. Look at your passions and skills to find out where they converge with customers' needs. Focus on providing value both in your offering and your marketing, and just get going. You can optimize your profitability along the way.**

The questions this book answered:

**How can you get your own microbusiness off the ground?**

  * Break away from the nine-to-five and become your own boss — it's a lot simpler than you think.

  * Passion is not enough for business success; you also need customers and the right skills.

  * Understand your customers' deepest needs and craft your offering accordingly.

  * Get creative with your marketing, and focus on the benefits that your product provides.

  * Successful launches rely on thorough preparation and instilling a sense of urgency.

**How can you make your microbusiness financially viable?**

  * A business needs to make money, so stay focused on your costs and income.

  * Get paid more than once, price your product ambitiously and use small tweaks that make a big impact in profits.

**What should the end goal of your microbusiness be?**

  * Make your business as big as you want: stay small or grow by bringing in other people.

**How detailed and meticulous should your planning be?**

  * Keep plans simple so you can focus on the action.

**Suggested further reading: _Jab, Jab, Jab, Right Hook_ by Gary Vaynerchuk**

_Jab, Jab, Jab, Right Hook_ explains how managers, marketers and small businesses can capitalize on social media platforms like Facebook to increase their public profile. A great social media marketing campaign can deliver that fatal blow — the "right hook" that knocks consumers into buying their product. The author teaches you social media moves that'll have your product floating like a butterfly and stinging like a bee.
---

### Chris Guillebeau

Chris Guillebeau is an entrepreneur and a writer with a passion for travel. He has visited every country in the world, and is dedicated to self-employment and encouraging people to live fulfilling lives. His blog _The Art of Non-Conformity_ has over 300,000 visitors a month.

