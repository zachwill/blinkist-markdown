---
id: 58a8dbe995f5390004fb0fd5
slug: the-100-year-life-en
published_date: 2017-02-22T00:00:00.000+00:00
author: Lynda Gratton and Andrew Scott
title: The 100-Year Life
subtitle: Living and Working in an Age of Longevity
main_color: 67B0C1
text_color: 407380
---

# The 100-Year Life

_Living and Working in an Age of Longevity_

**Lynda Gratton and Andrew Scott**

_The 100-Year Life_ (2016) is your guide to thriving in a world in which people are living longer. These blinks explain how the working world has changed, what it means for your retirement and which adjustments you need to make to enjoy life into the triple digits.

---
### 1. What’s in it for me? Prepare to enjoy a life that spans a century. 

If you knew you would live past 100, would you live your life differently? This may be a tough concept to grasp, as for most of our lives, living to 80 was the upward limit for aging. But this is changing.

Improved medicine and living conditions in many parts of the world have led scientists to believe that living past 100 years will soon become normal — that is, 100 is becoming the new 80.

This will dramatically change how we live and particularly, how we spend our retirement years. These blinks offer a look at what living longer means practically and how you personally can get ready to rock a longer life with your centenarian friends.

In these blinks, you'll find out

  * why living longer has serious consequences for pension plans;

  * how new technology will change how we view work; and

  * why you need to know the answers to the "big five questions."

### 2. Medical breakthroughs, better hygiene, better sanitation and education have helped us all live longer. 

Modern society is replete with examples of happy, healthy children. Yet historically, this wasn't always the case. The jump in the world's birth rate is a result of a dramatic rise in human life expectancy, sparked by advances in the treatment of disease common to different stages of life.

Infancy is the first stage, and here medicine has made huge strides. Not so long ago, it was common for children to die before they reached adolescence.

Thanks to improvements in vaccinations, general hygiene and other breakthroughs in medicine like the discovery of antibiotics, many deadly childhood diseases such as smallpox have been mostly eradicated.

Society at large knows more about good nutrition and proper health care, too. All these elements taken together mean children live healthier, longer lives. A child born in 1914, for example, had a one percent chance of living to 100; a child born in 2014 has a 50 percent chance of living that long.

Middle age is the second life stage, and here, many common diseases are now better understood and treated. In the second half of the twentieth century, for example, medical science was able to develop more sophisticated ways to diagnose and treat illnesses such as cancer and cardiovascular disease.

Around the same time, new research pointed to ways people could further improve health, while better education got the word out. Smoking was finally seen for the killer it is, which resulted in regulations over tobacco ads and aggressive public health campaigns addressing the risks of smoking.

Today, science is examining the third life stage — old age. Breakthroughs here are certain to produce yet another increase in human life expectancy.

Old-age diseases such as Alzheimer's affect both the quality and length of a patient's life. Diligent research into diseases like this mean that we're seeing results, and elderly people are indeed living longer, healthier lives as well.

In 1950, for example, a 90-year-old man living in England had a 30 percent chance of dying within a year; today, that estimate is now 20 percent. Improved nutrition, advanced medical technology and better sanitation are all to thank for this extension of human life.

### 3. Rising life expectancy means standard pension schemes and old-age savings aren’t enough. 

You don't need a doctorate in economics to know that the working world has changed dramatically over the past few decades. Your life at the office is no doubt very different from that of your parents' generation.

But how do such differences relate to increasing life expectancy?

Your parents likely followed a straightforward, three-stage career, consisting of school, work and then a short period as a retiree. Members of this particular generation, or people born between the 1940s and 1960s and who finished school around the age of 20, could, in general, expect to retire comfortably.

Let's look at the example of Jack, who was born in 1945. He graduated from college at 20, entered the workforce during America's Golden Age following World War II, and retired at the age of 62, after 42 years of employment. He then died a few years later, at the age of 70.

Jack's finances were solid, largely because he received a pension from his employer and social security from the government. Since he only lived for eight years after retiring and worked for more than 40 overall, for every retirement year, he was financed by five years of active work — a healthy foundation.

But imagine if Jack had lived to be 100. With rising life expectancies, this isn't unrealistic. With those extra years, the solidity of Jack's original pension starts to fall apart, or disappear altogether.

In short, living longer is making it harder for people to fully retire, as the former three-stage social model to fund retirement years is breaking down. 

Here's a modern example. Jane was born in 1998. Her life expectancy is around 100 years, about 30 years longer than Jack's. If Jane retires at 65, she'll have worked for 44 years — but will spend 35 years in retirement.

Simply put, Jane's savings accumulated during her work years won't be able to support a long retirement.

Jane has just over a year of active work to pay for each year of her retirement — a fifth of what Jack had. What's more, since both government and private pensions are paying out less and less, Jane will need to put more and more of her income toward her retirement savings.

It's clear that increased life expectancy is starting to pose serious challenges for society. But as we'll see in the next blink, working people are grappling with other issues, too.

### 4. Artificial intelligence is changing the nature of work, but in some areas, humans remain essential. 

Your parents today may not even understand what it is that you do for a living — modern work has changed that much in a short time. And it certainly will continue to change as technology influences the workplace.

Developments in artificial intelligence (AI) are certainly changing work as we know it. While this technology is still young, we can expect its influence to grow, with the biggest changes yet to come.

Massachusetts Institute of Technology economist David Autor says jobs that involve routine tasks, whether manual or cognitive, soon will be performed by machines. At Amazon's warehouses, for example, robots move most inventory.

Tasks such as these can be automated, as they follow strict rules and don't require a person to make decisions. In fact, it doesn't matter how sophisticated a process is, as long as it follows a script. This could mean that shortly, robots could perform a complex but routine task such as a medical diagnosis.

While intelligent machines may replace many humans in jobs soon, there still are tasks for which humans are essential.

People can solve complex problems; we can also come up with new, innovative ideas. This second point is crucial, as it's innovation that creates value.

Apple's iPhone, for example, is assembled by Chinese company Foxconn, at a manufacturing cost that is five to seven percent of the phone's purchase price. Since Foxconn "makes" the iPhone, one might imagine that the company is responsible for creating all the value the product holds.

But in reality, a Foxconn employee produces only $2,000 of value, since one person can assemble only so many units. An Apple employee, on the other hand, creates $640,000 of value — because Apple, not Foxconn, is responsible for the phone's design, from which millions of units are sold.

Humans also excel at tasks that involve interpersonal interaction — a basic yet important point, as after all, people tend to like to interact and talk to other people.

In other words, while computers might be able to solve mathematical equations rapidly and accurately, they can't yet compete with people in terms of personal touch, perception or mobility.

> _"The value creation is the innovation, not the manufacture."_

### 5. Intangible assets like health and self-knowledge are becoming increasingly important. 

Are you someone who spends every free minute at the gym, working out to stay healthy? If so, good for you. Taking care of yourself may be more important than you think.

With life expectancy on the rise, staying healthy is all the more essential. For instance, let's say you develop back pain at age 50, and you're expected to live until 70. You'll have to manage your back pain for some 20 years — already a long period.

Now imagine that your life expectancy isn't 70 but 100 years. How do you feel about 50 years of agony?

So perhaps it's time to get to the gym or visit a doctor! Interestingly, working out isn't just good for addressing the issues of an aging body. Studies suggest that as we get older, lifestyle choices are responsible for 70 percent of our mental decline.

This means that physical exercise can keep you in good mental shape, too. The same holds for eating plenty of fish and fresh produce, and performing mental training exercises.

So now you know that caring for your physical body is important for your well-being, but did you know that being self-reflective is also important?

Today, you mostly determine who you are, not your family tree or tradition. Self-reflection is crucial in this context, as it helps you recognize your strengths, weaknesses and values, and determines how you see yourself and the world.

By taking time to understand yourself better, you can better reflect on important decisions and choose a path that's best suited for you. In turn, this will help you deal with change more productively.

Let's say you're dissatisfied with a job you've been at for years. Leaving might result in a lower income somewhere else and an uncertain future overall. But if you have a clear picture of your strengths and desires, you'll find a job that's a better fit.

### 6. Allowing for phases of exploration is becoming more important as our life spans increase. 

The world's economy has changed significantly, offering many different working realities for each successive generation. While your parents may have found a job in their 20s that they stuck with until retirement, today your options as a working individual are much broader.

Since young people today are no longer bound by the three traditional stages of schooling, work and retirement, there's more room for exploration, which is important. Being an explorer can mean learning more about other countries, but it also means exploring your environment and finding your place in it.

A longer life means the decisions you make early on can have a greater impact on the years to come. Thus it's essential to explore all of your options, in work and life, and find the ones that are right for you.

The best times to do this are between the ages of 18 and 30, again in your mid-40s and your 70s or 80s. These are the times when natural life transitions occur, and are therefore ripe for reflection. 

Keep in mind that while each person's exploration will be different, it's not uncommon for this experience to ignite a desire to create more or to become someone who produces independently. Following a path like this can offer substantial freedom from a "normal" working life.

When you work for an established company or build a start-up, your focus tends to be on money and other financial assets. But becoming an independent producer helps you focus on the process itself, whether you're designing a product or launching a service.

You could start a blog and create a product around it, like an online course, for example.

The key is that this type of activity is about experimentation — failing and learning by doing. But it can also be a way to build up your credentials, especially if you're just starting your career.

### 7. A comfortable retirement relies on financial literacy and smart financial planning. 

It's hard to think about retirement plans without thinking about money. While most people know that a comfortable retirement is built through smart accounting, it doesn't mean that putting your finances in order is an easy process.

In fact, planning for retirement is complex, as it's hard to anticipate future prices or needs. Yet there are a few tricks to help you plan a path toward a stress-free retirement.

First, you need to know what percentage of your salary will be sufficient for you to live on once you retire. Is 50 percent enough, or will you need more?

To figure this out, think about what you spend money on now and how much you spend in total.

Once you're retired, costs like commuting will no longer exist, and other activities, like eating at restaurants, will reduce as you'll have more time to cook. Other costs such as for prescription drugs or health care, however, could be higher.

While better understanding your finances is important, you'll also need to increase your financial literacy. The _Big 5 Questions_ test will help you do this. Take a minute to jot down the answer to the questions:

The first question is: Suppose you had $100 in savings with a two percent rate of annual interest. How much money would you have after five years?

For the second question, imagine your interest rate is one percent and inflation is two percent. After one year, would you be able to buy more, less, or the same amount of goods?

The third question is true or false: buying a single company's stock is usually safer than investing in a mutual fund.

And the fourth question is also true or false: a 15-year mortgage usually requires higher monthly payments than a 30-year mortgage, but the total interest over the life of the loan tends to be less.

And the last question: If interest rates rise, what will happen to the price of bonds?

Once you can answer these questions correctly, you'll be a smarter saver and retirement planner.

Here are the answers:

Question one: Just over $110; Question two: Less; Question three: False; Question four: True; Question five: They'll fall.

### 8. To get the most out of your life, you need to know who you are and develop plans that work for you. 

When you meet someone at a party, it's common to want to know all sorts of things about that person, from where they work to what music they like.

Oddly enough, we show considerably less curiosity when we turn that same lens on ourselves.

If you want to shape your life positively, it's good to start with the question, "Who am I?"

As human civilization has advanced, this question has become increasingly important. For most of history, life was short and dominated by basic struggles for food and shelter. Today such struggles for large parts of the world are less of an issue. Instead, we can expect to live longer and have a greater measure of security as we do.

Because of this, we're freer to shape our lives based on our sense of identity and held values. But before we can do this, we need to know who we are — which can be a real challenge.

Historically, traditional gender roles and inherited family or social status would often shape a person's identity. But society continues to change and evolve, and such pressures are no longer as relevant.

Now many people can build a life that matches their individual values, wishes and ambitions.

Whether your plans deal with finances, personal knowledge or social networks, it's essential you come up with a particular trajectory that fits who you are. The modern world is pushing people to become more individualistic, making experimentation in daily life a crucial activity.

The bottom line is that there are millions of things you can do with your life, and exploring different options is the only way to figure out what works for you and what doesn't.

So while living to 100 offers more freedom, it poses more challenges, too. Making your own choices and finding your true identity will help you build a life that makes the most out of your extra years in the world.

### 9. Final summary 

The key message in this book:

**Living longer offers new opportunities for an individual, but increasing life expectancy overall poses challenges for society. As traditional roles fall by the wayside, the standard life trajectory of schooling, work and retirement is less relevant, while a need for self-knowledge and experimentation is growing.**

Actionable advice:

**Build regenerative friendships.**

In contrast to your professional network, regenerative friendships support your well-being and vitality. You know such people in a variety of contexts, from family connections to spending time at the gym. Such a complexity of social situations adds depth to friendships, making them emotionally binding, and encouraging you to invest more time, energy and feeling into them.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Grit_** **by Angela Duckworth**

_Grit_ (2016) is about the elusive virtue that allows people to do what they love, find a purpose in life and, most importantly, stick with it long enough for them to truly flourish. Find out how you can discover your grit and use it to follow your calling in life — and to hang in there, even when the going gets tough.
---

### Lynda Gratton and Andrew Scott

Lynda Gratton is a professor of management practice at the London Business School. She founded the Future of Work Consortium, a networking event for business executives.

Andrew Scott is a professor of economics at the London Business School. He has served as an adviser to the Bank of England, the HM Treasury and the House of Commons. He holds a PhD from Oxford University and an MSc from the London School of Economics.

