---
id: 5921f5c0b238e100077ef9b5
slug: the-10x-rule-en
published_date: 2017-05-22T00:00:00.000+00:00
author: Grant Cardone
title: The 10X Rule
subtitle: The Only Difference Between Success and Failure
main_color: EACC36
text_color: 806F19
---

# The 10X Rule

_The Only Difference Between Success and Failure_

**Grant Cardone**

_The 10X Rule_ (2011) provides clear indications for how to best plan your road to success. These blinks will teach you why this little-known strategy works and how to put it into action, while also giving you the tools you need to become more successful than you ever thought possible.

---
### 1. What’s in it for me? Learn how pushing your limits and setting ambitious goals will unleash a wave of success. 

Success, however you might measure it, is often elusive. Over the years, many people have tried to distil the essence of success into easy catchphrases or simple mottos, but most of these have failed.

Let's face it, most lifestyle gurus are little more than salesmen for grandiose and dubious ideas; they know all the cliches and slogans, but they have no idea how to really lead people toward success.

These blinks present a real method to success, the _10X Rule._ Based on the experiences of a top sales trainer and business owner, they explain how anybody can apply one simple rule to set themselves up for the lasting successes and ambitious achievements that once seemed out of reach.

In these blinks, you'll find out

  * why we should emulate children when it comes to decision making;

  * why you need to go "all-in" in life; and

  * why you shouldn't really concern yourself with customer satisfaction.

### 2. The 10X rule means investing ten times more and reaching ten times further. 

Perusing the shelves of your average bookstore, you're bound to find a plethora of titles that promise you the secrets to a successful life. But with so many options, it can be hard to know which is the best one.

So, before you get sucked into another questionable formula for success, consider the _10X rule_ — a surefire way to succeed at whatever you want.

The 10X rule is based on the knowledge that success always requires more effort than you originally thought. You can look back on your life and easily see that your endeavors always demanded ten times more effort than you thought they would. Success in business pursuits always came when you invested ten times more than your competitors.

So, if you want to sell a product and think making ten phone calls a day will help you do so, you should probably aim for closer to 100 phone calls per day.

But putting in this added effort won't just help you achieve your daily goals; the 10X rule will help you accomplish a whole lot more.

This brings us to the second half of the 10X rule: all your goals should be ten times bigger than what's considered realistic.

In other words, reach for the heavens and pick a goal that's beyond your wildest dreams. It might seem silly, but if you aim too low you'll still be disappointed when you achieve your goal, and feel crushed if you fail. It's much more desirable to fail to meet an incredibly high expectation than to attain a mediocre goal.

For instance, say your dream is to be an author. Writing a novel should be your goal, not writing a two-page short story. Even if you successfully accomplish the latter, you still won't consider yourself a writer.

In this way, the 10X rule is about trying harder and dreaming bigger.

> _"Massive thoughts must be followed by massive actions."_

### 3. The 10X Rule can help you overcome the unexpected and reach your full potential. 

History has shown time and time again that merely having the best product isn't enough to make you a success. Real market success is a product of the right attitude and the readiness to handle unexpected issues.

That's why the 10X rule is also about being ready to confront unforeseen challenges.

By aiming as high as the 10X rule tells you to, you'll be working hard enough to deal with anything that comes your way. Imagine you aim to sell 100,000 items in your product line when most people would shoot for just 10,000. This gargantuan goal means that if you, say, experience a huge influx of orders, you'll succeed where others will fail because you've developed the capacity to handle a bigger operation.

But the 10X rule also offers some clear guidelines for success. The first has to do with the fact that people often think of success as something for others, but not for themselves. It's essential to disabuse yourself of this belief and remember that success has no restrictions.

No matter what other people say, success isn't a zero-sum game and there's plenty to go around. Anyone with a 10X mindset can achieve success, and it doesn't have to come at someone else's expense.

For example, if you succeed at bringing an improved cell phone to market, you'll have offered a positive contribution, making it a success that benefits everyone. Even if other businesses look on with envy at first, others will learn from your contribution and attitude to find their own success.

In this way, the 10X rule, by pushing you to achieve all you can, is a tool for reaching your true potential.

Just consider the author.

Until the age of 25, his life was all about alcohol and drugs. At a certain point, he realized he wasn't living a fulfilling life whatsoever and had no will to achieve anything at all. So, he transformed his mindset and pushed himself to levels of achievement he could never have imagined.

Next up, you'll learn how you can do the same by applying the 10X rule to your life.

### 4. When faced with a challenge, go all out. 

Have you heard about the four degrees of action? It's a pretty simple concept that states that, when confronted with a situation, you can respond in one of four ways.

You can either do nothing, retreat, take normal action or take _massive action_, and it's the last of these that is the key to success.

To see this principle in action, look no further than children. When they're faced with a challenge, they simply give it everything they've got; they don't pause to calculate or budget their effort.

So follow their lead. Instead of determining how many hours you'll need to devote, just make massive action your natural habit. Say you need to sell your product to a market influencer. Instead of skimping on budget or keeping your efforts measured, go all out to convince him. Do all the research you can, spare no expense and don't let up until he's sold.

However, following this route means taking responsibility. Remember, success isn't something that happens to you — it's something that happens because of you.

As such, it's key to avoid thinking of yourself as a victim, or someone who can be acted upon, and start thinking of yourself as an actor. Embrace challenges and see opportunities instead of risks and dangers.

Keep in mind that the word "average" by definition means less than extraordinary; it's your task to push beyond this boundary.

The fact is, the world is full of average everything. Just consider how most people aim to be middle class. Wanting to be average means thinking only about getting by, perhaps thinking about tomorrow or the day after, but not any further.

The danger here is that average can sink to below average very quickly. The 2008 financial crisis is a great example, as it hit the middle class hardest, forcing many into poverty.

It just goes to show that aiming for average isn't enough. Define what average means in your context, but only to shoot for ten times above it — doing so will make you a success for years to come.

### 5. To get the biggest results, you have to go all-in. 

Anyone who has been around a card table knows that going "all-in" in a game of poker is a risky move. You might win the round and take home the pot, but you could also lose everything you have.

Luckily, going all-in in life is a little bit less risky. When applying the 10X rule, going all-in just means investing effort, energy and ideas to reach your goal.

First, you need to identify goals that are big enough for you to be motivated to achieve them. To do so, begin by picking objectives that are out of your reach and go all-in to realize them.

But going all-in also means thinking outside the box to come up with solutions. After all, if you've chosen appropriate goals, the problems you'll face will be immense. Problems of this magnitude often crush other people who try to overcome them. You'll need to be clever, using every last drop of your effort and energy, to succeed. Whatever you do, don't follow in the footsteps of those who have failed.

To succeed against such odds, you have to be obsessed with your goals; they have to dominate your thoughts and become your mission.

This is another context in which kids can be great role models. Children are obsessed with every new thing they encounter, devoting their full energy to their new object or activity of interest. To succeed, you'll need to do the same thing.

When others see how obsessed you are, they'll be prepared to follow you on your journey to realize your wildly ambitious goals. On the other hand, if you fail to show this devotion, they'll be unlikely to believe in your optimism.

So, obsession with your goals is a crucial factor, but it's just as important to avoid obsessing over failures and to shy away from excuses. That is to say, you should only be obsessed with doing things right and not worry about when they go wrong.

### 6. Strive for growth and stay in control of your time and feelings. 

Living by the 10X rule means remembering that you always have to keep growing. After all, the road to success is paved with growth and it's better to fail while fighting than while retreating.

So, to keep your eyes on the prize, it's important to shake off outside influences that seek to control your actions; instead, keep pushing for the goals you want to attain.

For instance, in failing economies, people tend to panic and scramble to move their assets to safe havens. But you don't have to worry about what other people are experiencing, just about achieving your own goals.

Another important thing to keep in mind is that overexposure is nothing to fear. You might be worried that pushing ahead time after time will bore people or make them feel annoyed by you and your company. But the simple truth is that overexposure is far better than obscurity.

Just consider Coca-Cola or Facebook — has overexposure hurt them?

And finally, to truly be successful, you need to seize control of your workday and your feelings. Many people have concerns about time management and time, of course, _is_ limited. However, the real question isn't which one of any two given options you can accomplish, but rather how you can foster a mindset that makes everything possible.

The only workable strategy here is to strive to accomplish more in less time. Keep a close eye on how much time you have, then work harder and budget your time more effectively.

We can again look to the author for a good example. After his daughter was born, he started waking up one hour earlier to spend quality time with her and let his wife sleep in, all while doing the same amount of work as he was doing before.

In this way, the structure of your workday is critical, but so are your feelings. Emotions like fear and pride can hold you back; being scared of risk can prevent you from reaching your goals and too much pride can make you averse to constructive feedback.

So, when faced with such feelings, remember that it's your job to remain in control and keep marching toward your goal.

### 7. Set targets that are meaningful to you. 

Now you know just about everything you need to apply the 10X rule to your life. There's just one last thing to figure out: how will you choose your goal?

Well, what you definitely _shouldn't_ do is follow in the footsteps of others who simply adopt the targets imposed upon them by their social context.

Take the concept of customer satisfaction as an example. Even though people are constantly told that satisfied customers are of the utmost importance, most companies don't have enough customers in the first place; their real target should be to increase their customer base.

Not only that, but with the 10X mindset, you don't need to live in fear of poor customer satisfaction because you'll always deliver above and beyond people's expectations. As a result, you can focus your energy on finding new customers.

In fact, truly successful brands like Google and Apple always put customer acquisition before customer satisfaction. They know that the more people they get talking about their brand, the further they'll reach.

And keep in mind that feedback from a customer, even if negative, is always valuable, as it shows you how you can improve.

Another target that's good to keep in focus is name recognition. You should consider your name your most valuable asset and make it synonymous with what you stand for.

For instance, when people think of mp3 players, they think of Apple; when they think of app-based mobility services, they think of Uber. So, do as these successful companies have done and proudly advertise what you have to offer.

Then, once you've set your sights, jump into action. Simply list your goals, outline what you need to do next and give it everything you've got.

If your goal is to sell 100,000 units of your product, you should consider exactly how to get there. Do you need to increase your marketing budget, or should you develop a great advertising strategy?

After you write this list, you can keep checking on it and adapting it based on what works and what doesn't.

> _"The world no longer rewards talk. You and I must not just talk the talk but walk the walk."_

### 8. Final summary 

The key message in this book:

**The 10X rule is about shifting your goals and your focus to reach beyond what you thought possible and accomplish much more. With a 10X mindset, you'll think bigger, work harder, show greater commitment and be more inspired to reach targets that once seemed unthinkable.**

**What to read next:** ** _Great by Choice_** **,** **by Jim Collins & Morten T. Hansen**

You now know how to set the ambitious targets and goals that will help you grow as an individual. But what about your business? How can you ensure that your company survives and grows in a competitive market?

In _Great By Choice_, Jim Collins — a business guru's business guru if ever there was one — explains the strategies and techniques that underpin the success of the greatest companies. By understanding what makes the best of the best tick, you'll stand a much better chance of making your own business shine. To discover these winning secrets, and to find out what business leaders can learn from Polar explorer Roald Amundsen, read the blinks to _Great by Choice_. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Grant Cardone

Grant Cardone is a sales training expert who works with companies worldwide and owns three companies of his own. _The 10X Rule_ is his third best-selling book.

© Grant Cardone: The 10X Rule copyright 2011, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

