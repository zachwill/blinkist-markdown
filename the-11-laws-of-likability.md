---
id: 57500d447bea3d0003c715b7
slug: the-11-laws-of-likability-en
published_date: 2016-06-10T00:00:00.000+00:00
author: Michelle Tillis Lederman
title: The 11 Laws of Likability
subtitle: Relationship Networking … Because People Do Business with People They Like
main_color: A7CA4C
text_color: 5E7328
---

# The 11 Laws of Likability

_Relationship Networking … Because People Do Business with People They Like_

**Michelle Tillis Lederman**

_The 11 Laws of Likability_ (2011) is a guide to networking built on one simple fact: _people do business with people they like._ These blinks will explain how to discover your most likable characteristics, start conversations and keep them going, and make a lasting positive impression on people.

---
### 1. What’s in it for me? Make everyone like you. 

Our lives pretty much depend on our relationships. It'd be pretty rough without any friends or family, right? Humans are gregarious animals, after all.

So what is it that makes you like another person? And how likable are you? Well, these blinks will introduce you to all eleven rules of likability that'll help you establish meaningful relationships: _authenticity, self-image, perception, energy, curiosity, listening, similarity, mood memory, familiarity, giving_ and _patience._

In these blinks, you'll learn

  * why you should lose the "win people over" attitude when networking;

  * what constitutes a good question; and

  * why it's so difficult to be yourself around people you dislike.

### 2. To be likable, you have to focus on connecting with people honestly. 

Most of our lives depend on our relationships. And whether they're of a professional or personal nature, our relationships support and connect us. But how does that fact relate to _networking_, a term we so often see in the modern business world?

Since our networks develop as a result of our relationships, "networking" simply describes the way in which we build new relationships. It comes down to liking someone and getting her to like you.

So when you're networking, try to recognize what's likable about the other person as well as yourself. Find out what's likable about others by asking questions — whether about the person's life, opinions, beliefs, job or hobbies — and then listen actively.

Although likability is as subjective as everyone is different, the basic premises of likability are the same across the board. These basic forces can be boiled down to eleven laws of likability that we're about to delve into one by one.

Before we start, it's important to keep in mind that networking is about wanting to connect with others, not just achieving a particular result. In fact, some people react negatively to the word networking, thinking of it as a test they have to take to win people over.

So it's only logical that, if networking feels like a chore to you, it's hard to find the motivation to do it, much less to do it well. Contrary to the popular belief that you should have a specific objective in mind when beginning a relationship, you shouldn't be thinking about takeaways when getting to know someone.

In fact, instead of making networking about transactions, you should make it about connections. It will benefit both you and the people you speak to. After all, the only way to make yourself likable is by being honest and authentic.

### 3. To be likable, be your authentic self and look at others with unbiased eyes. 

Everyone has a different set of attitudes, beliefs, goals and values but authenticity — generally speaking — is the same for all people. It simply means being true to yourself. So how can you know if you're being authentic or not?

One way is to gauge whether you feel awkward or not in a situation: being authentic feels natural whereas being inauthentic doesn't. Usually, being authentic feels so natural that you don't notice it, which makes it hard to define. On the other hand, when you're not being authentic, you're likely painfully aware of that fact and feel uncomfortable and exhausted by it.

That's because you're forcing yourself to wear a mask and act differently than you normally do. This tends to happen when you're under pressure to behave a particular way, for instance, if you dislike the person you're talking to.

Just imagine scenarios where you think:

"I don't like this person, but I'm trying to be nice" or "This situation makes me feel uncomfortable, but I don't know what to do about it." In these situations, people tend to hide their true feelings by acting overly polite, but such a strategy can be painfully obvious.

Regardless of how you feel about the person you're speaking to, you've got to remain authentic.

Instead of sporting a fake smile, try looking at the people you meet with unbiased eyes. Maybe you can find something to appreciate about them. For instance, maybe they have skills in areas where you don't. Or maybe you can feel compassion for their actions.

This thought exercise will help you forge a new perspective to deal with the person authentically. Just remember, finding the good in a situation or a person will lead to a more genuine and productive interaction.

> "_The beauty of the law of authenticity is in its simplicity: Don't try, just be._ "

### 4. Skip events where you can’t be authentic. 

Have you ever felt obligated to go to an event you didn't want to attend? We've all been there and often we go because we think we should.

But unless you either want — or absolutely must — go to an event, you're better off not going. Your authentic self will only emerge when you make the choices that you want to make, not the choices you should make. By only attending events that you want to attend, you can stay true to your authentic self.

Generally speaking, there are two types of events: the ones you _get to_ attend, which make you feel happy and excited, and the ones you _have to_ attend because, say, your employer or family member, suggests you do.

The key here is to make wise choices between the two and remember that you _do_ have a choice. If there are events you "have to" attend that you can get out of, get out of them. Or, if there's a meeting that you don't really need to be in, skip it.

Making small adjustments to your attitude also goes a long way. In fact, success is all about maintaining a positive attitude. So if there are events you just can't get out of, try transforming them into events you _get to_ do by looking for aspects about them that are exciting.

For instance, if you're not in the mood to go to the birthday party of someone you don't know very well but feel obligated to do so, bring a friend along. It'll make you feel authentic and comfortable because the two of you can mingle in small groups and meet new people.

And remember, there's no right way to be at a social gathering. The best thing you can be is yourself.

> "_Being authentic will get you where you need and want to go, and it will be your path to building the most meaningful and enriching connections with others._ "

### 5. Be consistent and confident in your communication. 

Have you ever walked away from a conversation with someone and felt like he was totally fake?

Perhaps because his body language wasn't in sync with his words, an error that you should be certain to avoid.

Instead, you should work to communicate with consistency. Try using the three key components to communication, known as the _three V's_ :

The first V stands for _verbal_, or the words you say; the second is for _vocal_, or the tone of your voice; the third is _visual_, or your body language.

So keep your three V's in chorus whenever you communicate, meaning send the same message verbally, vocally and visually.

In his book, _Silent Messages_, the psychologist Albert Mehrabian says that someone's "total liking" is 7 percent verbal, 38 percent vocal and 55 percent visual. That means the words that come out of your mouth have close to no effect on your likability if they're not in sync with your body language and tone of voice.

But communication isn't just about being consistent and your three V's can sometimes fall out of line because of self-doubt. After all, if you don't believe what you're saying, nobody else will. Not just that, but if you don't feel good about yourself, your body language will communicate it.

So, how can you exude the confidence you need to communicate effectively?

First off, you need to recognize that the way you describe something also impacts the way you perceive it. Framing things positively will help you shed your negative thoughts. For instance, instead of saying to yourself, "I'm too slow and will never get this done," say, "I'm taking my time to make sure things get done correctly."

And, finally, concentrate on what you _can_ do, instead of what you can't. For example, you should turn thoughts like, "I have no idea how to do this" into "I'm excited to work on something new."

> "_My mother always told me, 'It's not what you say, but how you say it.'_ "

### 6. Be genuinely curious and ask questions to sustain a good conversation. 

For a lot of people, striking up a conversation with a total stranger can be terrifying. Maybe you're too focused on avoiding saying stupid things, or maybe you feel like you have nothing to talk about. Whatever the case is, don't sweat it: if you don't know what to say, just show an honest curiosity about the other person's life.

Showing genuine interest in someone's job, hobbies, opinions and life is a great way to start a conversation. So, if you don't know anything about the other person, just ask easy questions, such as about their favorite TV show or sport. Often all you need is one topic to get the ball rolling.

But the success of your conversation will also depend on the types of questions you ask. When it comes down to it, there are two basic types: _open-ended questions_ and _probing questions_.

The former, open-ended questions tend to start with _what, how, why_ or _how come_. They are great as conversation starters or for restarting a stalled dialogue.

If you're at a concert and you ask the person sitting next to you, "How did you hear about this concert?", the other person will be encouraged to give a full response. But if you ask "Did you hear about this concert on the band's website?", she'll most likely come back with a simple yes or no answer. So if you want to keep the conversation going, stick to open-ended questions.

Probing questions make great follow-ups to other questions you've already asked and can help sustain an engaged conversation. They come in three categories:

First are clarifying questions that begin "Do you mean…?". Second are rational questions like "I'm curious why you think that…". And finally, expansion probes that start out "Please elaborate…"

All three are equally effective at keeping a conversation moving.

> "_Curiosity may have killed the cat, but I can tell you it never killed a conversation._ "

### 7. Good communication is about good listening. 

Did you know that cultivating good listening skills boosts your natural likability? Well, it does, and you need to know how to do it.

There are three levels of listening: namely, _inward listening_, _outward listening_ and _listening intuitively_. All three serve different functions in a conversation and are important for fostering connections as they make people feel heard and understood.

_Inward listening_ is the basic level of listening in which you hear what the other person says from your perspective and relate it to your own experience. For instance, when your friend says, "I love Vietnamese food," you might answer, "Oh, me too" or "I prefer Thai." This form of listening helps find commonalities and shared opinions, a key aspect of likability.

_Outward listening_ is about the speaker and happens when you relate what you hear to what you know about them.

For instance, when the same friend tells you that she likes Vietnamese food, you instead respond, "Why do you like it?" or "Have you been to a good Vietnamese place recently?" By asking questions in this way, you can uncover more of the other person's interests and perspectives.

Finally, _listening intuitively_ entails not only focusing on the words a person says, but also the tone of their voice, their body language and even their energy. Simply put, it's about hearing more than just the words being said.

For example, when your friend says she likes Vietnamese food with an excited expression on her face, you might say, "You seem so excited when you talk about it. Are you considering a trip to Vietnam someday?"

### 8. Similarities and trust substantially boost likability. 

People are comforted by what they know. So finding similarities between yourself and others will make you feel more at ease with them and boost their likability.

So you can look for common friends, interests and backgrounds or shared experiences and beliefs to form a basis for connecting with the other person.

One very efficient way of doing this is writing a list of every organization you've ever been associated with so you have a few topics in mind before starting a conversation. Your list can include colleges, clubs, student exchange programs, sports teams, parent associations, volunteer activities and much more. The idea is just to uncover a greater number of things you have in common with other people.

But it's also important to remember that people trust sources they know. After all, people tend to think, "I like this person and will therefore probably like the people she likes, too." They then validate their choices through a trusted third party.

That's why people will listen to a friend's recommendation when hiring a new employee or give an interview to a candidate with whom a colleague worked with for years. The same goes for when your friends set you up on a blind date, or recommend a movie or restaurant.

Naturally, this information is also applicable to anyone seeking a job. It can be enormously helpful to you during your search to explore any ways you might be connected to a prospective employer, maybe by using LinkedIn.

If you do have common connections, you can use them to build a context for the conversation. Just research the company's activities and highlight similarities with the experiences on your resume.

Just remember, people like people who are like them, which means seeking out common interests or shared hobbies is a great way to connect with others.

> "_When we meet someone with whom we have strong similarities, our comfort level quickly increases; the conversation flows and the likability is palpable._ "

### 9. Final summary 

The key message in this book:

**Engaged, meaningful connections aren't about you, but about relationships. Connecting with people and forging productive networks require you to seek out shared interests, listen actively and set a foundation for trust.**

Actionable advice:

**Make a list of three new people you met in the past two weeks.**

Keeping track of the people you meet is a great way to keep your networking on point. After you make such a list, you can reach out to each of the people before the week is over. Just remember, every time you reach out to someone, even with a simple "How are you?", it reinforces your connection and you never know where that connection might take you.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Like Switch_** **by Jack Schafer and Marvin Karlins**

In _The Like Switch_, author Jack Schafer explores the realm of nonverbal social cues and other communication practices that draws people to one another. A former FBI agent and doctor of psychology, Schafer presents useful strategies to make new friends and influence people.
---

### Michelle Tillis Lederman

Michelle Tillis Lederman is a motivational speaker and professional coach. She is the founder and CEO of Executive Essentials, a firm that runs communication and leadership programs for businesses and professionals alike. She is the author of numerous bestsellers including _Nail the Interview, Land the Job._

