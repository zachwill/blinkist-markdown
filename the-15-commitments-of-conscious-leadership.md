---
id: 55ac313165363800075b0000
slug: the-15-commitments-of-conscious-leadership-en
published_date: 2015-07-22T00:00:00.000+00:00
author: Jim Dethmer, Diana Chapman, Kaley Warner Klemp
title: The 15 Commitments of Conscious Leadership
subtitle: A New Paradigm for Sustainable Success
main_color: EF4539
text_color: CC3B31
---

# The 15 Commitments of Conscious Leadership

_A New Paradigm for Sustainable Success_

**Jim Dethmer, Diana Chapman, Kaley Warner Klemp**

_The 15 Commitments of Conscious Leadership_ (2014) teaches you how to become a _conscious_ leader — a leader who inspires positive change, creates a great work atmosphere and builds close relationships in their personal life. Because leading is about a lot more than just giving orders.

---
### 1. What’s in it for me? Be a better leader and live a more fulfilling life by becoming more conscious. 

Have you ever had a boss who just didn't listen to you, who kept doing things the old way, repeating the same mistakes over and over again, heedless of all the good advice that could help grow the company and improve leadership? Unfortunately, these _unconscious_ leaders are quite common.

Although this kind of leadership can be effective, producing good results in the short run, it never leads to true growth over time — neither on a business nor a personal level.

As these blinks will show you, _conscious_ leaders, on the other hand, are fully present and committed, listening carefully to those around them, instead of regarding their colleagues as competition or, even worse, as enemies. But what else are they committed to? These blinks will show you.

In these blinks, you'll learn

  * why being a hero is not always a good thing;

  * that truly conscious leaders welcome emotion; and

  * how impersonating your mother can work wonders for the company atmosphere.

### 2. There are two kinds of leaders: conscious and unconscious. 

Many leaders consider themselves successful, even if their workplace has a stressful atmosphere or they don't have strong relationships in their personal lives. Success, however, doesn't have to come at the price of true contentment — you can find success _and_ personal happiness if you lead a conscious life.

A person can live — and lead — in one of two ways: _consciously_ or _unconsciously_.

_Unconscious_ leaders don't respond well to changes in their environment. They cling to old models and patterns, even when it's counterproductive — that's why they always view themselves as victims of circumstance. They think their success and personal happiness is produced by outside factors.

_Conscious_ leaders, on the other hand, are much better at living in the moment. They learn from whatever situation they're in and know they have the power to change it.

Don't think of conscious leading as a stage you reach. It's really a state of mind — and you can deliberately enter it at any time.

How?

By always staying honest with yourself and the position you're in. Living unconsciously might sound unpleasant, but it's actually the norm for most people. If you're happy, there's nothing wrong with unconscious living; however, if you want to be a leader who fosters creative energy in the workplace and builds strong relationships, you have to be honest with yourself about where you stand. After all, you have to make a conscious effort to lead a life of conscious leadership.

You can aim to be a conscious leader by using the _15 Commitments of Conscious Leadership_.

Contrary to popular belief, _commitments_ are not promises for the future. They're statements about your present reality. When you can determine the things you're committed to, you can assess whether you're a conscious or unconscious leader.

### 3. Conscious leaders take full responsibility for themselves. 

The first two of the 15 commitments are the core of conscious leadership.

The first commitment is about taking full responsibility for every aspect of your life. When things don't turn out as planned, unconscious leaders try to place the blame on something or someone else. To avoid acting in this way, hold yourself accountable for everything you do.

If quarterly numbers are lower than projected, for example, an unconscious CEO would look for where to place the blame. And, depending on their personality, they'd see themselves as either the _victim_ or the _hero_ or the _villain_ of the situation.

_Victims_ attribute their problems to the world's injustices; _villains_ point the finger at someone or something else; and _heroes_ try to take on more than their share of the responsibility. All three are equally detrimental to the group: _victims_ don't have the drive to change anything, _villains_ lose control of the situation and _heroes_ overwork themselves until they burn out.

Conscious leaders, however, are committed to taking full responsibility for themselves — neither more nor less. They understand that their actions have consequences, so they don't hasten to blame someone else.

The second commitment of conscious leadership is to learn by staying curious, rather than aiming to prove a point. Unconscious leaders don't seize the opportunity to learn from their mistakes when things go wrong. Instead, they get defensive and stubbornly insist that they're right — even if it's obvious they aren't.

Conscious leaders know that each experience — good or bad — is a learning opportunity. They're realistic about their own views; they know they aren't always right, and they're eager to learn new things. Instead of looking for something to blame, they look for a lesson to learn.

### 4. Conscious leaders are committed to accepting their feelings and learning from them. 

Conscious leaders are adept learners. They are students of both themselves and their environment. They also know how to learn _through_ themselves, by exploring their own feelings.

That's why the third commitment of conscious leaders is to not resist or _recycle_ any emotions.

There's little room for emotions in the workplace. People are taught to think with their heads only. That's even truer of leaders: unconscious leaders think feelings and emotions distract from work, so they try to repress them.

We repress our feelings in a lot of ways, and we all know how it feels. Just think about the last time you felt uncomfortable about something at work but didn't say anything about it.

Another way unconscious leaders resist dealing with their feelings is by _recycling_ them: getting stuck in a cycle where they feed their emotions with thoughts that support them, like a jealous person looking for evidence that their partner is cheating on them. This gets you stuck in a self-perpetuating cycle of bad feelings that you never actually address.

Conscious leaders, on the other hand, are committed to having high emotional intelligence. They know that feelings are a source of wisdom.

Anger, for instance, is a sign that something isn't harmonious — some part of the whole needs to be fixed. Sadness is your body's way of telling you to let something go. Joy is a sign that things are going well, that you need to celebrate!

That's why conscious leaders don't turn away from their emotions. They know that emotions are powerful tools for learning or bringing others together to overcome challenges. They accept and enjoy their emotions for what they are.

### 5. Conscious leaders communicate openly and honestly. 

The next two commitments of conscious leadership are all about communication.

The fourth commitment is to always speak and listen carefully. It's important to keep a realistic perspective, and it's impossible to do that alone. You have to consider other points of view if you want to get a well-rounded picture, and that means listening to what others say.

It's important to keep in mind, though, that people often withhold information, not out of malice, but because of things like fear or shame. This is dangerous, because when you withhold your feelings, you run the risk of judging others for the problems you're uncomfortable addressing with them.

For example, if you're upset with a friend because they cancelled an appointment, and you don't tell them, you'll probably just pull back and expect them to apologize. And when you withdraw like that, you'll view your friend through your own judgmental lens. You'll see them as a person who disrespected you, and think they're disrespectful in general.

Listening consciously is equally important. Leaders usually don't fully listen to others; they filter their words through their thoughts, adding their own meaning to them.

For example, if an employee says they have a problem with a colleague and their unconscious leader has an avoid-conflict filter, the leader will be looking for clues that the employee has good intentions. Conversely, conscious leaders listen to what the other person is _really_ saying.

The fifth commitment of conscious leadership is to avoid gossip. It's important to be truthful, but it's equally important to make sure you're sharing the truth at the appropriate times and with the appropriate people. Gossip is widespread in office culture, and though it's widely accepted, it's very toxic. It prevents employees from building trust and hinders their creative energy.

Gossip is harmful for everyone involved — the person gossiping, the person listening and the person being gossiped about. Everyone in the workplace should aim to reduce gossip. Don't be afraid to discuss the issue openly with your employees.

### 6. Conscious leaders have integrity and show appreciation for the important people in their lives. 

The sixth commitment of conscious leadership — living a life of integrity — is closely intertwined with the others.

Integrity is important to leaders, because leaders, when they slip up, demoralize their team. If a leader is dishonest or unreliable, it poisons the company as a whole.

A person has integrity when they're fully whole. That means they take complete responsibility for their actions, speak honestly and express their feelings openly.

The final part of integrity is keeping your word. Breaking even the smallest promise — like not buying juice when you said you would — can seriously disturb the group's energy flow.

So when you're making a promise or dealing with someone, be clear about your terms so that they know what to expect. Make sure you keep up with any arrangement you've made at least 90 percent of the time — and negotiate in the times when you absolutely can't. And if you ever do have to go back on your word, make it up to the person by asking if there's anything you can do.

The seventh commitment of conscious leadership is to be more appreciative. When you show your appreciation for your employees, for example, you help them gain appreciation for themselves and, in turn, each other. It allows you to see them in a new light, because you'll always be reminding yourself of why you value them in the first place.

Appreciating others is kind of like wine tasting: an experienced wine enthusiast is much better at distinguishing between subtle tastes. Likewise, an experienced appreciator is much better at discerning people's uniqueness, and giving them credit for it! A person who never thinks about wine can't tell the difference between a Chardonnay and a Pinot Noir. So think about what makes your employees special. It'll only help you see (or taste!) more.

### 7. Conscious leaders spend as much time as they can in their zone of genius. 

The eighth commitment of conscious leadership is to express your own brilliance as much as you can — and still have fun!

Most people work and operate in one of three zones: the zone of _incompetence_, the zone of _competence_ or the zone of _excellence_.

You're in the zone of _incompetence_ when you're doing something you're not good at and don't enjoy. In the zone of _competence_, you're good at what you do, but it isn't truly fulfilling.

When you can perform a task extremely well but you don't really enjoy it, you're in the zone of _excellence_. You might produce fantastic work, but you aren't being creative anymore.

We tend to stay in these zones because we subconsciously limit ourselves. Have you ever given up on something because you thought you'd never be good enough? Feelings like that are what keep you down.

If you want to be a conscious leader, you need to spend as much time as possible in the zone of your _genius_. That means doing what you love _and_ are good at — tasks you love so much they don't even feel like work!

This dovetails nicely with the ninth commitment of conscious leadership, which is to _play_ instead of struggle. Since conscious leaders spend so much time in their zone of genius, they naturally work in a more playful and fun manner. That doesn't mean that they don't work hard; they're just committed to working in a way that's more creative and laid-back.

There are always ways to combine work and play. That's why Dan Cawley, the CFO of Hopelab Inc., once presented the quarterly financials while impersonating his mother's Irish accent! Doing so not only added an element of fun; he got people to pay attention, too!

### 8. For conscious leaders, life is abundant. 

The next three commitments are the secret to how conscious leaders find their inner peace and happiness.

The tenth commitment is to be open to interpretations other than your own. Most of the pain in our lives comes from our wanting to change a situation that we perceive as bad. But conscious leaders know that experiences are never purely good or bad — those are just the labels we assign them.

That's why conscious leaders strive to explore alternative interpretations of their thoughts and surroundings. Jim Barnett, the founder of Turn Inc., struggled with this when he reached a point at which he no longer wanted to work in business, but felt that leaving his position as CEO would be irresponsible.

The authors helped him realize that leaving his position would, in fact, be a very responsible decision, as it would allow the position to go to someone with more passion for it.

The eleventh commitment of conscious leadership is to find _internal_ security, control and approval, rather than seeking it from the outside world.

Nearly every dream, goal or desire you've ever had was essentially about wanting approval, control or security. It's natural to want those things, but it's unhealthy to endlessly chase them. Conscious leaders know how to appreciate the approval, control and security they already have.

The twelfth commitment of conscious leadership is to recognize that you already have enough of everything. And it's not only your material things that are abundant — realize that you've also got plenty of time, money and love.

Most people assume that all resources are scarce. Conscious leaders, however, appreciate what they have, and that allows them to live in the moment.

### 9. Conscious leaders are always learning and looking for new solutions to the problems around them. 

The final three commitments of conscious leadership are about communication — both with individuals and the world as a whole.

The thirteenth commitment of conscious leadership is to view everyone and everything as a potential tool for personal growth.

Unconscious leaders tell themselves that other people or circumstances prevent them from reaching their goals. Conscious leaders, on the other hand, know that virtually everything they encounter in life helps them grow in some way.

For example, an unconscious leader might view a strike as nothing more than an obstacle to meeting his numbers, but a conscious leader would see it as a chance to improve her work environment.

The fourteenth commitment of conscious leadership is to create situations where everyone wins. Unconscious leaders assume that competition and compromise are the only ways to solve problems; conscious leaders know that this isn't true.

Consider what the authors did when they wanted to become more invested in _The Conscious Leadership Group_, but their colleague Kaley wanted to spend more time at home with her newborn daughter. Instead of just compromising on the situation, they talked through it carefully and found a new solution that worked well for everyone: ending the partnership on a high note.

That's why the final commitment of conscious leaders is to _become_ the solution to the problems in the world. Since conscious leaders see things in terms of abundance, they don't view problems in the world as the result of a _lack_ of something. When something is missing, they just see that as more potential space for other things. They can choose to become the solution to the problems they see.

### 10. Final summary 

The key message in this book:

**Conscious leadership is the key to living a fulfilling life, both in your professional life and at home. So never stop learning — about yourself, your environment and the people around you. Stay open to new ideas, take responsibility for yourself and appreciate what you have. Becoming a conscious leader benefits everyone — you, the people around you and the world as a whole.**

Actionable advice:

**Trick your brain into always staying happy.**

Whenever you experience something great — like getting a promotion or going on a fun date — do something ordinary afterwards, like the dishes! You'll trick yourself into thinking that happiness and success are normal, and be happier as a result.

**Suggested** **further** **reading:** ** _Start with Why_** **by Simon Sinek**

_Start With Why_ gets to the bottom of why certain people and businesses are far more innovative and successful than others — even in situations where everyone has access to the same technology, people and resources. The book shows you how to create a business that inspires customers and has satisfied employees.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Jim Dethmer, Diana Chapman, Kaley Warner Klemp

Jim Dethmer, who has worked with many Fortune 500 CEOS, is a professional speaker and coach. Diana Chapman, an advisor, has worked with over 700 organizational leaders. Kale Warner Klemp is a professional speaker and transformational executive coach.

