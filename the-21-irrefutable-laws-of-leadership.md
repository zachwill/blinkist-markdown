---
id: 583af051d4507000044fa59e
slug: the-21-irrefutable-laws-of-leadership-en
published_date: 2016-11-29T00:00:00.000+00:00
author: John C. Maxwell
title: The 21 Irrefutable Laws of Leadership
subtitle: Follow Them and People Will Follow You
main_color: B09B60
text_color: 736339
---

# The 21 Irrefutable Laws of Leadership

_Follow Them and People Will Follow You_

**John C. Maxwell**

_The 21 Irrefutable Laws of Leadership_ (1998) explains what it takes to become a great leader. These blinks highlight many of the traits, skills and characteristics that have given leaders around the world the power to attract loyal followers and lead them toward success. Find out what Ray Kroc, Winston Churchill and Mother Theresa all have in common — and what you can do to become a better leader yourself.

---
### 1. What’s in it for me? Turn yourself into a great leader. 

Do you consider yourself a born leader? Few people do — and for good reason. Even if you're a naturally imposing person and have headed up every single group you've been in since entering kindergarten, this only means that you're more dominant than others. Fact is, no one is born a leader.

Leadership is a skill and it's completely up to you to develop it.

It's all about getting others to follow you — and these blinks will show you how. They lay out the most important of the 21 laws of leadership, from which you'll glean the traits and skills that will boost your ability to lead, even if you focus on just a few of them.

And you'll also find out

  * how an employer's age affects whether he'll hire you or not;

  * why Ray Kroc, and not the McDonald brothers, turned McDonald's into a global player; and

  * how a woman who escaped slavery became a heroic leader.

### 2. The biggest factor in business success is quality leadership, which makes the boss the most accountable. 

There's a McDonald's on just about every corner these days. But do you know how this behemoth got started?

It began in 1937, in Pasadena, California, when brothers Dick and Maurice McDonald opened a small drive-in restaurant that actually specialized in hot dogs, not hamburgers. A few years later, they relocated to San Bernardino, California, where they reorganized their kitchen into a fast-moving assembly line and included hamburgers on the menu.

In 1952, the brothers decided to franchise their fast-food concept, but immediately ran into problems. 

This was due to the _Law of the Lid_, one of the 21 irrefutable laws of leadership. It states that the potential success of you and your company is limited by one single quality: your ability to lead.

And that's why the brothers had trouble franchising their business. Despite being great with customers and having innovative ideas, they were horribly inept leaders, which meant that very few franchisees were willing to put their faith in the concept.

This became even more apparent after they struck a deal with Ray Kroc to expand the business. A great leader in his own right, Kroc got to work fixing the apparent problems by recruiting other leaders for all the critical positions. And even though he couldn't really afford to pay them, he took out personal bank loans, cashed out his life insurance policy and sacrificed his own salary for years in order to get things running smoothly.

The results speak for themselves. While the McDonald brothers only made 15 franchise deals on their own, Ray Kroc closed 100 in his first four years with the company and nearly 400 additional ones in the four years that followed.

The vast importance of the leader also means that when an organization is hurting, it's typically the leader who's going to take the fall. For instance, when the consultants at the advisory firm Global Hospitality Resources are called in to rescue a struggling hotel, one of their first solutions is to fire the boss. For them, it's obvious that, if the hotel had a good leader, it wouldn't be asking for help in the first place.

> _"The higher you want to climb, the more you need leadership."_

### 3. Great leaders are influential and intuitive, and they have a track record of past successes. 

The Law of the Lid shows us that leaders are in a vulnerable position if they don't perform well. So how can you ensure that this doesn't happen to you?

Enter the next law — the _Law of Influence_.

If you want to hire someone who has shown previous leadership ability, you need to look for signs of the most important skill: the ability to influence others. Instead of just checking for appropriate job titles on a resume, look for followers. If someone is a true leader, they should have a long line of people that they've influenced.

Take Mother Teresa: She might have appeared small and frail, but she was an amazing leader who created a personal army of followers to help further her mission of feeding and assisting the poor. And whenever she spoke, people would stop and listen because they respected her impressive ability to lead.

These thousands of followers and volunteers formed her worldwide organization, the Missionaries of Charity. She even continued to gain followers during a time when most Catholic churches were struggling to hold on to their members.

So how can you learn to influence people?

You need to show intuition and pay attention to the intangible factors that can affect your followers.

For example, you should be able to spot when someone is feeling as if they're being treated unfairly. With intuition you can sense rising tensions and resolve issues before they really start hurting your business.

Another helpful method for boosting morale and convincing people to follow your lead is to highlight past successes. It will be easier to get people motivated and confident that they're heading in the right direction if they can see evidence of past successes rather than a trail of disappointments.

> _"If you can't influence people, then they will not follow you. And if people won't follow, you are not a leader."_

### 4. Leaders must be trustworthy, and they can lose that trust by breaking the rules they are supposed to enforce. 

To see what happens when leadership comes crumbling down, let's take a quick look at the Watergate scandal. In the early 1970s, members of President Nixon's administration were found guilty of illegal activities, including a burglary at the offices of the Democratic National Committee.

As a result, Nixon lost the trust of the American people and was forced to resign.

This brings us to the _Law of Solid Ground_, which states that leaders must work on a solid foundation of trust.

After all, people won't follow just anyone; they need someone they can put their trust in, someone who is reliable and treats his followers with honesty, fairness and good judgment.

One common way of losing trust is to take shortcuts instead of following the proper procedures. Most organizations have guidelines on how things should be done, whether it's giving someone a promotion or firing an employee. If a leader doesn't follow these rules, he can come across as disrespectful.

Leaders are supposed to be the ones who enforce the rules and hold others accountable for following them; therefore, if you suddenly decide to take shortcuts, you can end up losing the trust of your entire organization by giving the impression that you're above the rules.

This is what led to Nixon's downfall: by approving of something illegal, he gave the impression that the rules didn't apply to him.

The author gives a personal example as well. He was working as a senior pastor for the Skyline Church in San Diego, and during an especially hectic holiday season, he realized that it was necessary to let a staff member go immediately. The author made this decision on his own without talking it over beforehand with the community, and even though his intentions were good, his shortcut unsettled the church congregation, and he had to work hard to earn back their trust.

### 5. Leaders earn respect by being strong, courageous and loyal. 

Influence and trust are two important qualities for any leader, but it's also crucial that a leader enjoys the respect of her followers.

The _Law of Respect_ goes into effect when people encounter someone with strength, a leader who's more skilled, determined and courageous than they are — the exact kind of qualities that followers look for in a leader.

Harriet Tubman is a great example of a respected leader. Born a slave in 1820, she eventually escaped to the free state of Pennsylvania, where she began helping other slaves safely escape by becoming a leader in the Underground Railroad.

Tubman, with her impressively strong-will, was respected by everyone she worked with, especially the fugitive slaves she helped free by leading them on dangerous journeys to the North.

The risks that Tubman was willing to take played a big part in how she earned others' respect. Every time she traveled to the South to embark on another mission, she displayed courage and determination.

Failure and death threatened at every turn. Each time she freed more slaves, Southerners raised the bounty on her head. But these threats didn't deter Tubman, and respect for her only grew as she courageously continued to go on a total of at least 19 missions.

It's clear that Tubman was loyal to her cause, and this is also a powerful driver of respect.

As a leader, you can show your loyalty by being devoted to the welfare and goals of your followers.

This is a valuable quality, since, these days, loyalty can be hard to find. CEOs, professional athletes and coaches often go wherever the money is. Such shifting loyalties, however, can lead to their losing the respect of their followers.

On the other hand, leaders earn respect by hanging in there when the organization is struggling. That's why great athletes and coaches stick with their team even through a losing streak, and true business leaders will fight to save the jobs of their followers when the company hits a rough patch.

### 6. Leaders attract individuals who are similar to themselves. 

Now that you have your leadership qualities in place, it's time to make sure you have a good team.

First, imagine your ideal team. What kind of people do you see?

There's a good chance you see a group of people that are similar to you, in everything from age and background to personality and ambition. This is known as the _Law of Magnetism._

So, if you're 35, you are more likely to hire someone who's also in their thirties, instead of someone who's 16 or 65 years old.

We saw this law in action during the dot-com explosion of the 1990s, when thousands of businesses were founded and then staffed by entrepreneurial folks who were all in their twenties or thirties.

But it's not just age; it's personality as well. If you're a tidy perfectionist, chances are you won't want to hire someone who comes in all disheveled and makes a mess of your office, no matter how brilliant that person's résumé. Instead, you'd probably pick someone who's meticulous and detail-oriented, just like you.

Even Theodore Roosevelt followed the Law of Magnetism when he was recruiting people to fight in his infantry during the Spanish-American War.

Roosevelt grew up in a wealthy aristocratic family from the Northeast before moving west to the Dakotas and living the life of a cowboy and hunter. So, during the war, Teddy recruited people who came from similar backgrounds, resulting in a militia of Northeast aristocrats and Western cowboys.

Therefore, keep in mind: a leader's own traits can shape a team, or even an entire organization.

Back at the Skyline Church, the author's predecessor was Dr. Orval Butcher, a great leader and also a fantastic musician. When he put together a new staff for the church, Butcher gathered people like himself, and the church went on to be widely known for its terrific music.

### 7. A victorious leader is hungry for success and will create winning teams with diverse talents and a shared vision. 

Are you a poor loser? If so, don't feel too bad, because if you want to be a leader, you need to be a _terrible_ loser.

For great leaders, losing and giving up the fight are not valid options; this is called the _Law of Victory._

Winston Churchill is a perfect example of this kind of leadership. During World War II, he refused to accept defeat even when Nazi Germany controlled most of Europe and bombed England.

Even when all seemed lost he continued searching for ways to win, eventually joining forces with the United States and leading the Allies to take down Hitler's regime.

All great leaders should abhor defeat and always strive to find winning options like Churchill did.

Another secret to being a victorious leader is knowing how to create a winning team that is made up of diverse talents. A team can't win if everyone has the same skills, just as a soccer team made up only of goalies won't be any good at scoring. Therefore, successful leaders build teams made up of people with diverse skill sets that can face any number of challenges.

But you need to make sure that these different people all share the same vision. If everyone is focused on different things, talent can be wasted. Winning coaches know the importance of teamwork and getting their players to focus on overall success instead of individual glory. For example, they make sure that players will pass the ball if a teammate has a better chance of scoring a goal.

In the final blink, we'll explore the importance of timing and its role in leadership.

### 8. A great leader makes the right decision at the right time. 

If you've ever tended to a garden, you know the importance of planting the seeds at the right time. Too early, and the soil might freeze; too late, and you might end up with a poor harvest.

It's the same with leadership and the _Law of Timing_.

If a leader makes the wrong decision at the wrong time, it can often end in disaster. But it can be just as disastrous to make the _right_ decision at the wrong time.

This is what happened when hurricane Katrina hit New Orleans in 2005. Mayor Ray Nagin knew the right decision was to call for an evacuation, but he hesitated. Even as the cities around New Orleans issued mandatory orders for people to leave, it took another half day for Nagin to make the right move and turn his call for a voluntary evacuation into a mandatory one.

But by then it was far too late for citizens to safely leave the city, and many of them perished.

Successful leaders, on the other hand, know the right time to make the right decision. They are decisive and have the experience to recognize when they need to seize an opportunity or prevent a catastrophe.

Take Churchill again. He recognized the dangers of Hitler and reacted in time to increase the military preparedness of Great Britain, enabling it to persevere and eventually win.

By understanding these fundamental laws of leadership, you can greatly help your team surmount any challenge it may face.

### 9. Final summary 

The key message in this book:

**Leaders are trustworthy and influential individuals who command respect from those who follow them. To earn that kind of respect and loyalty, there are certain traits and skills you should have: You must be influential, strong and trustworthy. You also need to be loyal to your followers and pursue victory single-mindedly, no matter how bad the situation.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _5 Levels of Leadership_** **by John C. Maxwell**

_5 Levels of Leadership_ is a step-by-step guide to becoming a true leader with a lasting influence. Using engaging real-life anecdotes and inspiring quotes from successful leaders, it describes key pitfalls that may be holding you back and explains how to overcome them.
---

### John C. Maxwell

John C. Maxwell, a pastor and bestselling author, is a specialist on the qualities of leadership. A popular keynote speaker, his other books include _The 360° Leader_ and _The 5 Levels of Leadership_.

