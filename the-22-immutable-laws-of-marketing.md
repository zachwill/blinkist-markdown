---
id: 562422ae32646600074d0000
slug: the-22-immutable-laws-of-marketing-en
published_date: 2015-10-21T00:00:00.000+00:00
author: Al Ries and Jack Trout
title: The 22 Immutable Laws of Marketing
subtitle: Violate Them At Your Own Risk!
main_color: 2187A3
text_color: 1C748C
---

# The 22 Immutable Laws of Marketing

_Violate Them At Your Own Risk!_

**Al Ries and Jack Trout**

_The 22 Immutable Laws of Marketing_ (1994) gives you the essential knowledge to build powerful marketing strategies. With practical, real-world examples, these blinks show you how to avoid common mistakes while ensuring your marketing push will stand fast against the toughest competition.

---
### 1. What’s in it for me? Learn the marketing basics to change people’s perceptions. 

Marketing is all about perception. People don't buy a product because it's superior, but because they _think_ it's superior. 

Japanese manufacturer Honda, for example, in Japan is seen as only a motorcycle maker — thus the Japanese seldom buy Honda cars. Yet in the United States, Honda cars are among the top-selling automobile imports! The difference is one of perception. 

Yet many executives fail to realize this important point. In the face of sinking sales, a company will spend millions to upgrade a product in an attempt to meet what it thinks is consumer expectations — when really, all the company needs to do is tweak people's perceptions!

Marketing is the tool you need to do this effectively, and these blinks will tell you exactly how to go about making the competition's strength look like a weakness and how to build a steel-clad brand. 

In these blinks, you'll also discover

  * how preferring Pepsi to Coke reveals your age;

  * how being arrogant can keep you from marketing success; and

  * how a company can "own" a word (that isn't just its name).

### 2. Your product’s marketing success is largely determined by how quickly you can get it to market. 

So you want to be a market leader? First, you've got to place your product in the pole position. 

To do so, you need to make an unforgettable impression in the minds of your customers to get your product the attention it needs to succeed. 

But how do you go about making sure your product is first in line? 

The _Law of Leadership_ states that to win the race for market leadership, you need to ensure your product is the first of its kind on the market. It's a fact: first out of the gate often wins!

Yet while being first can help you establish yourself as a market leader, it's not the only way to the top. The _Law of the Mind_ states that for your product to be truly successful, it needs to be _first in the customer's mind_. 

Every time a customer thinks of a certain product, a particular brand will automatically pop into her head before any other — and often, that's the market leading product.

First impressions are key. Once a customer has made up her mind, it's tough to change it! 

But why is a set customer's mind so hard to influence? 

One reason is that brand names often become synonymous with their products. In the United States, people often say "Xerox" when they mean photocopier; and if you need a tissue, you might ask your friend for a "Kleenex." Market leaders always have powerful brand names.

Once a strong brand is etched in a customer's brain, it's tough to erase it. A better strategy for a company is to make a splash with a product early on, when the market is fluid and there are multiple brands vying for attention. 

Choose your product's name wisely, sticking to words that are short and catchy. For instance, let's say you're choosing a computer. Which brand would you pick, _Apple_ or _MITS Altair 8800_?

In sum, being first to market is a cornerstone of effective marketing strategy. Yet don't despair — if you've missed that train, there are other ways to market your product and brand effectively.

> _"Being first in the mind is everything in marketing."_

### 3. If you can’t be market leader, turn the market upside down and create your own product category. 

So whether you make computers or soft drinks, if another brand is the market leader in your product category, it will be a struggle to change that fact. 

But this doesn't mean you should close up shop. You just need to invent a _new category_ for your product. 

The _Law of Category_ states that when you come up with a new category for your product, you'll by default be the first in that category! And as you now know, being the first to release a product in a market category gives you a head start to becoming the market leader. 

What's more, starting your own category means that you won't have any competition, at least for a while. 

For example, in 1971 Charles Schwab opened a brokerage firm at a time when the field was already flooded with comparable companies. Yet to combat this, in 1975 Schwab came up with a new category: the discount brokerage. 

The result? Schwab became the market leader, with a client base that multiplied in just a few years!

But there's another option for companies that find themselves directly behind the market leader, in undesirable second place. The _Law of the Opposite_ states that to succeed, you need to compare your company with the market leader, painting your competition's _strengths as weaknesses_. 

If the competition has been in the leadership position forever, you could then describe the company as _old, outmoded_ and _conventional_. In contrast, your company is young, modern and revolutionary.

Pepsi used this strategy in its attempt to knock Coca-Cola from its primary market position. The soft drink company focused its message to appeal to hip, young people — who clearly wouldn't want to drink the same cola that their grandparents and great-grandparents enjoyed. 

In fact, it's common for one category of customers to prefer buying from a market leader while another will avoid it at all costs. 

So if you're not the market leader, don't pretend to be. Instead, seek out another group of customers!

### 4. Boost your brand by “owning” a word; but don’t get greedy and try to claim the words of others. 

Sure, a company can own physical goods like factories and machines. Companies also own their logos. Yet did you know that a company can also "own" a word? 

In this sense, a company "owns" a word when a customer says that word and immediately associates it with the particular company. 

For example, when a U.S. consumer thinks "ketchup," he probably thinks, "Heinz." This company's powerful brand has created an automatic association between its name and its product. 

This is the essence of the _Law of Focus._ To get potential customers to buy your product, you need a catchy, sticky way to describe it. Nothing makes for more powerful marketing than a simple word that a customer can immediately connect to your brand. 

So when your company "owns" a word, you've secured a place in your customer's mind. Words that work the best are often those that make clear a specific, focused benefit to a consumer. 

For example, Swedish car manufacturer Volvo is practically synonymous with the word "safety." 

But not all words are up for grabs. To stay clear of the courts, you need to remember the _Law of Exclusivity_, which states that you should never use words already "owned" by another company. 

It really isn't in your best interest to use words already associated with a different brand. But how could doing so really hurt a company? 

It's true that if you use other companies' words, potential customers might see your company as an imposter. But even if this isn't the case, it's close to impossible to reclaim words already owned by another brand. 

For instance, the "Energizer bunny," a plush pink rabbit powered by batteries that never died, was the battery maker's attempt to grab the concept of "long-lasting" from competitor Duracell. 

But the strategy was doomed. No matter how many funny bunny ads Energizer ran on television, consumers still associated Duracell with the words it already owned.

### 5. In marketing, less is more; but there’s a trick to staying ahead during inevitable product expansions. 

Customers love choice. While this fact may make expanding your company's product line an obvious decision, in doing so, you could lose both market clout and cash. But how?

According to the _Law of Sacrifice_, successful marketing means giving something up. So if you're a generalist, the smartest move is to reduce, not expand, your line of products. 

Remember this: the more products you offer, the less time you can dedicate to making one truly successful product. 

Brands that specialize in just a few products develop a stronger market profile. In retail, for example, the most successful companies are those with a specialty — like Foot Locker, which sells athletic shoes, and The Gap, which focuses on casual clothing. 

In contrast, the retailers that are suffering the most today are department stores that sell practically everything!

The _Law of Sacrifice_ also says to limit your target market. Trying to appeal to everyone will only hurt your product's reputation. 

Pepsi used to market its cola to teenagers, a focused strategy that was successful. Yet then it switched its message, attempting to appeal to the mass market. This strategy failed, as its main competitor, Coca-Cola, already had a generalist message. 

But even if you need to split and grow your product line, there's still a way to maintain market dominance. The _Law of Division_ states that over time, every product category will break into several different categories. So how do you keep your brand profile strong throughout? 

To stay relevant, give each new product category its own, distinct brand name. This works well, as consumers prefer buying different products or services from different companies or brands. 

General Motors for example created several new brands when it diversified its automobile lines, focusing on different customer categories with brands like Chevrolet, Pontiac, Oldsmobile and Cadillac. The result? The company remained a market leader in each of its categories.

### 6. Three things bring a brand down: blind arrogance; falsely predicting the future; belief in infallibility. 

A successful brand can find itself in a precarious position, as success can lead to arrogance. And an overabundance of arrogance can be even the most successful company's undoing. 

The _Law of Success_ states that being arrogant can blind a company, leading to decisions (or a lack of) that can weaken a strong brand.

For example, a company can become too sure of itself and its market position, thinking that its brand can sell anything. As a result, it expands its product line haphazardly, weakening the brand. 

Also, arrogant company executives can be guilty of shooting down ideas that aren't in line with their own, leading to company missteps. 

Ken Olsen, the founder of Digital Equipment Corporation (DEC), in the mid-1970s was offered an opportunity to focus on a new product category, the personal computer. Olsen, unconvinced of the tech's market potential and overly confident of his company's leading position, said no. As a result, DEC lost its edge as the computer market evolved. 

Thinking that you can predict the future is another mistake that can weaken a strong brand. 

The _Law of Unpredictability_ says that marketing strategies rooted in mid- or long-term predictions are rarely effective. Considering that meteorologists aren't able to forecast _the weather_ three days in advance, how can you expect to anticipate market behavior three years from now?

Well, you can't. So don't get caught up in making expensive marketing decisions based on market predictions.

Another important marketing law is the _Law of Failure_, a tough one that you'll have to accept. This law states that mistakes are inevitable, and you just have to deal with them. 

The problem is, countless companies pretend that they don't ever make mistakes — which means they don't drop bad projects in time to avoid major losses. 

Managers too who hate mistakes often avoid risk — a natural, necessary element in business. Avoiding risk also means potentially losing out on a lucrative opportunity, such as jumping into a new market segment as a revolutionary market leader!

### 7. Did your competitor catch you in a mistake or misstep? Admit it and your customers will reward you. 

Product hype can work a competitor into a lather. But don't let hype inspire hasty marketing decisions; often, buzz about a product might be more distraction than substance.

Remember that just because there's a ton of talk about the competition's product doesn't mean that they'll automatically become a market leader. 

According to the _Law of Hype_, huge amounts of publicity can be misleading. 

Often companies that perform well don't need marketing hype; but companies that hold a ton of press conferences might just be scrambling to reverse plummeting sales.

Remember too that journalists can be easily impressed, gobbling up a story featuring an exciting product without really doing the necessary research. Look at over-enthusiastic media coverage with a cool, discerning eye. 

There are plenty of examples of products that, despite serious hype, nonetheless fell flat. In 1948, the press went nuts over the _Tucker 48_, saying it was the first of a new generation of automobiles. But in the end, the company only sold 51 cars. 

Yet good press for a competitor isn't the only thing that can make you uneasy. Media attacks on your product from new rivals can also keep you up at night. 

The _Law of Candor_ can guide you here. When your competition catches you in a misstep, the best strategy is to simply admit the mistake. In essence, when you admit a negative, your prospects will reward you with a positive. 

Weird? But true. While denying a mistake can throw your credibility into question, admitting to a mistake will make you appear sympathetic and trustworthy. 

For instance, Listerine was once attacked in ads by Scope, a competing mouthwash brand. The competitor claimed Listerine tasted terrible; granted, the company knew there was no use denying the statement, as their mouthwash was quite unsavory. 

Instead, Listerine turned the taunt into a slogan that worked to the company's advantage, "The taste you hate twice a day." 

The strategy worked — customers sought out Listerine, thinking that the product's medicinal taste meant that it worked especially well!

### 8. Final summary 

The key message in this book:

**While having the cash to invest in marketing strategies is important, it's far more essential to understand the rules by which companies play. Successful marketing deals in perceptions — not products — so follow the laws of marketing to help you craft a strategy that will win every time.**

Actionable advice:

**For a fresh strategy, get out of the office.**

The next time you're thinking through a change in strategy, take a step back from the graphs and numbers. Go take a walk in the shoes of your target group. Ask people which brand they think of when contemplating your product category to get a sense of where your company stands on the market ladder. This will let you build a strategy based on objective information, and not just on what your ego wants to hear.

**Suggested** **further** **reading:** ** _Positioning_** **by Al Ries and Jack Trout**

_Positioning_ has become one of the most renowned, best-selling books about marketing strategies in the last few decades. It describes a revolutionary marketing concept developed by the authors in the 1970s after it became clear that classic advertising was no longer effective due to an increase in media and competition. _Positioning_ focuses on how to position your product in the market to become an industry leader.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Al Ries and Jack Trout

Al Ries and Jack Trout are two renowned marketing gurus who have published a number of marketing books, including _Marketing Warfare, Bottom-Up_ and _Horse Sense_. They have consulted for and worked at top American companies such as Apple, AT&T and Procter & Gamble as well as for high-performing companies in Latin America and Asia.

