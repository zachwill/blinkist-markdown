---
id: 5de12d436cee070008b71d64
slug: the-3-minute-rule-en
published_date: 2019-12-02T00:00:00.000+00:00
author: Brant Pinvidic
title: The 3-Minute Rule
subtitle: Say Less to Get More from Any Pitch or Presentation
main_color: CA2830
text_color: CA2830
---

# The 3-Minute Rule

_Say Less to Get More from Any Pitch or Presentation_

**Brant Pinvidic**

_"By the end of three minutes, your audience will already be leaning yes or no on your proposal. From that point on, you can continue yammering for another 57 minutes, but the die is already cast."_

_The 3-Minute Rule_ (2019) is an incisive guide to creating an ultra-concise, ultra-compelling pitch for any idea, product, service or company. Beginning with the provocative thesis that you have only three minutes to persuade a modern audience, it provides you with a blueprint for packing those three minutes with your best possible material.

---
### 1. What’s in it for me? Learn the secret to creating a persuasive three-minute pitch. 

Three minutes. That's not much time — barely enough to make a cup of coffee or brush your teeth in the morning. But according to Brant Pinvidic, that's all the time you need to deliver a successful pitch. In just three minutes, you can sell even the toughest of audiences on any good idea, product, service, or business. 

If you're skeptical, you're in good company. From Fortune 100 CEOs to small-business owners, Brant has coached hundreds of people on improving their pitches. Most of them start off thinking the same thing: "It's impossible. My subject is just too complicated." But Brant proves them wrong time and time again — nailing down each of their pitches into an airtight, three-minute presentation. 

What's his secret? Well, after being involved in nearly 10,000 pitches over the course of his two-decade career in Hollywood, Brant has figured out the essential elements a pitch needs to succeed. Since then, he's used his insights to sell over 300 TV and movie projects. Each of them was pitched in under three minutes, and all of them were developed using the ideas you're about to learn. 

In these blinks, you'll discover

  * why three minutes are all you have to persuade a modern audience; 

  * how to make the most out of those three minutes; and

  * what a "butt funnel" is.

### 2. Your pitch has three minutes to succeed. 

Imagine you're about to have a meeting with some potential investors, customers, partners or collaborators. You've got an amazing idea, product, service or company to pitch to them. You know it's a winner. The only problem is they don't even know what it is or how it works, let alone why it's so great. Heck, for all they know, you're just another person with a bridge to sell. 

In other words, you've got a lot of explaining and persuading to do — so you better pull out all the stops, right? Design an hour-long PowerPoint presentation that explains everything in meticulous detail. Fill it with a bunch of clever animations, jokes, one-liners and catchphrases. Practice all those tips and tricks you've learned about public speaking, sales, and persuasion. 

That's the traditional approach — but it gets everything backward. The truth is you'd do a whole lot better if you did the opposite. Simplify the PowerPoint — or even ditch it altogether. Cut out the fluff. Forget the gimmicks. Stop worrying so much about your delivery. Focus just on conveying your key information as clearly and concisely as possible — three minutes maximum. 

The alternative is to shoot yourself in the foot. That's because in today's fast-paced, digitally-connected world, people are constantly bombarded with information, advertising, and various other demands on their time, money, and mental bandwidth. As a result, their attention spans are short, and their patience is even shorter. They've got zero tolerance for hot air, long-windedness, gimmickry and anything else that wastes their time or insults their intelligence. They're savvy, skeptical, and quick to pass judgment on whether your message is credible, relevant, and interesting to them. 

So, sure, you might have an entire hour booked for your presentation. But by the end of three minutes, your audience will already be leaning yes or no on your proposal. From that point on, you can continue yammering for another 57 minutes, but the die is already cast. Your audience is going to filter the rest of your presentation through the prism of their initial judgment. If it's positive, they'll be eager to learn more, and they'll be receptive to what you have to say. If it's negative, they'll be doubtful, critical, resistant, bored or just plain tuned-out. Either way, you're unlikely to win them back. 

In other words, three minutes isn't just a suggestion; it's a rule. Whether you realize it or not, you only have three minutes to win over your audience. The question is simply this: Will you design your pitch around the three-minute rule to maximize your chances of success? Or will you ignore it at your peril? The choice is yours.

### 3. The three-minute rule also applies to your audience and the people they have to pitch to. 

The prospect of losing your audience is bad enough, but the problems of breaking the three-minute rule don't stop there. To see why, let's look at what happens even in the best-case scenario when your pitch goes long. 

Imagine you're pitching an idea for a business venture to a group of representatives from another company. Your pitch drags on for an hour, but you somehow manage to maintain their interest the entire time. 

So far, so good, but here comes the problem. Even if they're fully on board with your idea by the end of your presentation, they're probably not the only decision-makers who need to sign off on it. Usually, they'll have their own people they need to persuade. For example, they might have to get approval from their legal and finance teams. As a result, the success of your pitch ultimately depends on how well your audience pitches your idea to other people. If the legal team shoots it down, you're out of luck. 

Now, remember, your pitch is going to form the basis of their pitch. After all, that's where they're going to draw their information from. And here, once again, you're being your own worst enemy if you go long. Why? Because even if your one-hour presentation is brilliant, your audience won't be able to replicate it. They're going to forget most of the details and half-remember the rest. 

Plus, they won't have enough time to repeat it all in the first place — not by a long shot. If Jerry from legal stops one of them in the hallway and asks her for a recap of your presentation, she's not going to chew his ear off for an hour. They're probably going to have a three-minute conversation in passing. And during that conversation, she's just going to take her jumbled collection of fragmentary, muddled memories and reel them off the top of her head. 

The resulting "pitch" won't be very impressive, to say the least. Jerry is probably going to walk away confused and unconvinced. Now, imagine what would have happened if you'd given your audience a simple, snappy three-minute pitch to remember. Jerry would probably be a lot more on board with you right now, and your idea would be one step closer to becoming a reality. 

Once again, the lesson is clear: at the end of the day, your pitch has only three minutes to succeed.

### 4. You don’t need to say everything you think you need to say in a pitch. 

Okay, sure — a three-minute pitch would be ideal. But is it actually feasible? How on Earth do you squeeze all of your information into a mere three-minute presentation?

The short answer is: you don't. You're going to need to be much more selective with your content. The key is to realize that there's a major distinction between what you think you need to say and what you actually need to say when you're giving a pitch. And the problem is that many of us tend to think we need to say everything. 

For example, let's say you have a start-up, and you're pitching it to potential investors. In that case, your temptation would be to explain every single aspect of your company: what it does, how it does it, why it does it that way and so forth. 

Now, you know all the details of your company inside and out, and you know how they all interrelate. In your mind, they form an intricate tapestry of information. The more you trace the threads, the more complicated it all seems, and the more you feel like you need to convey that complexity to your audience. One detail leads to another and then another. Pretty soon, you end up with a convoluted one-hour lecture that's going to put everyone to sleep.

But in reality, most of those details are irrelevant to your audience — at least for now, when you're still at the stage of pitching to them. Remember, the point of a pitch is to win your audience over to the thing you're pitching — your company, in this case. To do that, you simply have to convey the general concept of it to them in a way that gets them interested in learning more about it. 

At that point, they'll want to know about the details, and you can dig into them during a follow-up presentation or a question-and-answer session. But until your audience is interested in the general concept of the thing you're pitching, most of the details are just going to seem like a bunch of boring facts and figures to them. If you start by focusing on them, you're putting the cart before the horse. 

In the following blinks, we'll look at how to put the horse back up front and lead it to where you want to go.

### 5. Your pitch needs to answer four questions: What is it? How does it work? Are you sure? And can you do it? 

In theory, the objective of a three-minute pitch is pretty simple. You just need to capture the basic concept of the thing you're pitching and communicate it compellingly. Of course, that's much easier said than done. How do you actually do it? 

The exact details are going to depend on your topic and your audience, but there is a general template to follow. The essential idea is that by the end of your pitch, you need to answer four fundamental questions about the thing you're pitching. 

The first two questions are: What is it and how does it work? These are the most fundamental questions to answer about your topic. By answering them, you're going to enable your audience to conceptualize the thing you're pitching. That's a prerequisite to getting them on board with it. After all, they're not going to sign up for something if they don't understand what they're being asked to sign up for. For instance, if you're asking them to invest in your new invention, they might need to understand what it does, what the point of it is, what the market for it looks like, how it operates, how you're going to manufacture it and so forth. 

Now, in answering the questions of what it is and how it works, you're going to be making some bold claims about the thing you're pitching. Naturally, your audience will want you to back them up. That's where the third question comes in: Are you sure? To answer this question, you're simply going to provide some facts and figures that will reinforce the claims you've previously made. For example, if you claimed the market for your invention was a certain size, you might provide some data to support that assertion. 

At this point, your audience should understand how and why the thing you're pitching represents a good opportunity for them. There's just one question left to answer: Can you do it? For example, based on your answers to the previous questions, your invention might sound like a great idea, but do you have the ability to bring it to market? By answering this question, you're going to reassure your audience that you can deliver on the thing you're proposing. 

So those are the four questions in a nutshell. In the next two blinks, we'll take a closer look at how to answer them effectively.

### 6. The four questions you answer in your pitch can be reinterpreted into a wide range of other useful questions. 

Imagine you're being profiled by a magazine, and the interviewer asks, "Who are you?" If you interpret the question narrowly, you might just say your name — a pretty boring response. But if you interpret it broadly, you might describe your personality, talk about your values or offer a concise version of your life story — potentially a lot more interesting. 

The same lesson applies to the questions you're answering in your pitch. To get the most bang for your buck with them, you need to be creative with the way you interpret them. And that means reinterpreting them into other, closely related questions that fit the thing you're pitching and the audience you're addressing. 

For example, consider the first question: What is it? You should now consider related questions your audience might have about the nature of what you're pitching. If it's a service, they might want you to explain what problems it solves, who it can help or what makes it unique. If it's a business venture, they might ask about the potential payoff, or why this is a good time to pursue it.

In the same vein, the question "How does it work?" should lead you to anticipate other questions about how you'll deliver on what you're promising. For example, if you're pitching a project, how long will it take? How will you accomplish it? What resources do you have at your disposal? 

Likewise, the question "Are you sure?" encompasses any concerns your audience might have about whether you can back up your claims. For example, if you said your service was the best in the industry, what do your reviews say about it? What kind of stats do you have? 

Finally, the question "Can you do it?" relates more broadly to your ability to deliver on your promises. For instance, if you claim you're the right person to lead a project, your audience might want to know about your training and background. They may also ask how you've dealt with similar challenges in the past. 

So that's how to think outside the box when you're interpreting the four main questions you're answering in your pitch. In the next blink, we'll look at how to be creative with your answers.

### 7. Make sure your pitch is filled with your most important and interesting information. 

Picture yourself in the mid-2000s. You work in Hollywood, and you've got an idea for a TV show to pitch to network executives. It's called _Pirate Master._

What is it? Here's the literal answer: it's a reality competition show that's sort of like _Survivor_, only it's set on a pirate boat. Here's the better answer: it's the latest idea from Mark Burnett, and he thinks it could be the next smash hit. 

Now, that might name not ring a bell, but to the network executives, it would be music to their ears. At the time, Burnett was the hottest producer in television. He was riding high on the success of _Survivor_ and _The Apprentice_. For executives hearing about _Pirate Master_ for the first time, the information about Burnett being at the helm would have been way more notable than the premise of the show itself.

As you're answering the questions of your pitch, take a page from this scenario and think about which information will be the most interesting for your audience. 

You'll also want to think about this while you narrow down your answers into your final pitch. For each of the many questions you generate from the four original questions, you should come up with a short, one-sentence answer. Look through these sentences, and cut the ones that aren't interesting or important enough to include in your pitch. Remember, you only have three minutes, and you want to pack this time with your most essential and compelling material. 

You should also leave out sentences that require too much explaining. You simply don't have time for anything that gets too into the weeds of your topic, like technical details. Leave these for your follow-up presentation or a question-and-answer session, when your audience will be more interested in them. 

In the end, you should cut your material down to 25 sentences. As a rule of thumb, you should aim to answer the question "What is it?" in nine sentences, "How does it work?" in seven, "Are you sure?" in six, and "Can you do it?" in one. The first two questions are the most essential ones to answer in your pitch, so they should receive the most attention.

### 8. Your pitch needs an opening. 

At this point, you should have 25 sentences, packed full of valuable information about the thing you're pitching. If you were to put them in a logical order and read them out, you'd already have a serviceable three-minute pitch. But to bring your pitch to life and maximize its impact, there are a few more elements you need to have in place. 

The first one is your _opening_. To start your pitch, you should begin by telling your audience about your _reason for being_. This is the story of how and why you became interested, invested or involved in the idea, product, service or company you're pitching to them. Now, you can't tell the entire story; you're just looking for a sentence or two here. With that in mind, try to remember your "aha" moment — the moment everything clicked and you realized you were onto something with whatever it is you're pitching. 

For an example of how to turn an "aha" moment into an opening, let's look at the pitch that Brant made for the TV show _Bar Rescue_. In case you're not familiar with it, _Bar Rescue_ is a hit reality TV show in which the host, Jon Taffer, helps to turn around bars and nightclubs that are failing. 

Brant's "aha" moment occurred when he realized something about Taffer: he's a man with a huge, over-the-top personality — but he also has a lot of expertise in his field. He wasn't just a character; he was also a longtime business owner and consultant in the food and beverage industry. It was this winning combination of personality and depth that modern audiences craved. And it was this same combination that led to the success of celebrities like Simon Cowell and Gordon Ramsay. 

So Brant talked about this in his opening. He simply walked into the room and said, "Hello, everyone, I'm here because I found you a talent with a big personality, but also a lot of depth." He then proceeded with his pitch, describing Taffer in more detail and laying out the premise of the show that would be built around him. 

So that was Brant's opening. If you need more help with figuring out your own "aha" moment, here are some questions to get you started: What makes you excited about the thing you're pitching? When did you discover it? And what surprised you when you started looking into it?

### 9. Your opening needs a callback. 

When did you start to believe you had a winning idea, product, service, or company on your hands? And when did you become convinced that your belief was correct? 

The answer to the first question provides the opening to your pitch, where you tell your audience about your reason for being. The answer to the second question provides your pitch with the next element that's going to push it over the top: the _callback_. This is a moment in your pitch where you return to your opening and tell an anecdote that helps to illustrate and confirm your reason for being. 

To see how this works, let's go back to the example of Brant's pitch for _Bar Rescue_. Remember, he opened with the idea that Jon Taffer, the would-be host of the show, was a man with a winning combination: a huge personality and a deep well of professional expertise. After describing Taffer and the premise for the show, Brant called back to his opening and drove it home with a simple but memorable anecdote. 

Here's the story: one day, Taffer was showing Brant a blueprint for a bar he was designing, and he pointed out something called a "butt funnel." Of course, with a name like that, Brant had to know more, so he asked what it was. It turns out a butt funnel is an area of a bar where a corridor becomes so narrow that patrons have to rub their butts against each other to scoot by. 

When they're designing a bar, experts like Taffer think about how the patrons' foot traffic will flow through the floor space, and they purposefully build a butt funnel into it. Why? Because it will boost the patrons' endorphins and foster a friendly, intimate and sexually charged atmosphere. And all of that lends itself to people buying more drinks. 

By the time Taffer finished explaining all of this, Brant was convinced: here was a man who knew his industry. 

So what was the moment you became convinced you were onto something? When did your belief turn into a conviction? It might not involve as catchy a name as a "butt funnel," but if you dig through your memories, you should be able to find a quick and compelling anecdote to tell your audience.

### 10. Preempt your audience’s skepticism by acknowledging the elephant in the room. 

You know that moment in a movie when the protagonists seem to be on the edge of defeat? It's called an _"all is lost" moment._ Inspired by Brant's experience in Hollywood, it's also the name of the next element that will help you take your pitch to the next level. 

To create your own "all is lost" moment, you simply tell your audience about a problem that jeopardized — or continues to jeopardize — the viability of the thing you're pitching. Then, you tell your audience the way you overcame or plan on overcoming the problem. For example, if you were pitching an app, you might talk about a major technical issue you encountered during your development phase, and then you'd talk about how you resolved it. 

The rationale here is that your audience wasn't born yesterday. They know that every major human endeavor faces challenges and setbacks, and they know that the road to success is a bumpy one. If you tell them that everything has been and will be hunky-dory with the thing you're pitching, they'll be skeptical. They'll start looking for problems. That means they're going to be approaching your pitch from a critical standpoint, rather than a receptive one. It also means they're no longer going to be fully listening to you; they'll be drifting off into their own thoughts, wondering what you're not telling them.

At the same time, you're also going to lose credibility with your audience, since it'll seem like you're trying to hide something from them. By the time you're done with your presentation, they might even feel resentful toward you. Meanwhile, they'll have thought of some problems on their own, and now you'll be in real trouble. They're going to ask you questions in a combative spirit, and they'll be suspicious and critical of your answers. 

So why not preempt all of this by admitting a problem upfront? The advantages are numerous. You set your audience's skeptical tendencies at ease. You nip their criticality in the bud. You make yourself seem credible. You secure their attention. You focus them on a problem you already have a solution for. And you thus transform the problem from a potential liability into an advantage. After all, the alternative is to wait until they ask about it — and by then, you'll have already turned them against you. 

To maximize the impact of this element of your pitch, ask yourself the following questions: What problem are you most hoping your audience won't see? What question are you most fearing they will ask? 

Make that your "all is lost" moment. Get ahead of it; don't let it come back to bite you.

### 11. Finish your pitch by making sure it has a correctly placed hook and an edge. 

Now that you have your opening, your callback and your "all is lost" moment, there are just two last elements of your pitch to make sure you have in place. The first is your _hook_ and the second is your _edge_. 

Your hook is simply the element of your pitch that will make your audience think, "Wow, that's cool!" Your edge then provides your audience with a vivid illustration of your hook. For example, consider a pitch by Jeff, the owner of a plumbing company. His hook was the fact that his company's innovative method of re-piping homes turned a previously major renovation into a minor one. His edge was an anecdote that illustrated how minor the renovation had become: once, his company was able to replace the pipes of an entire hotel while it remained open to guests. That's how inconspicuously they could do their work! 

To find your hook, just look at those 25 sentences you wrote and identify the one that makes you feel the most excited. Then think of a snappy anecdote to illustrate it. That's your edge. 

Finding your hook and your edge is usually pretty easy. The tricky part is using them effectively. The key is to avoid the temptation to start with your hook. Yes, it's your strongest piece of material, but you need to wind up to it. 

To see why, imagine if Jeff went up to his audience and said, "Hi, I'm Jeff. My plumbing company can take a previously major renovation and turn it into a minor one! Let me explain how." By doing this, Jeff is starting with a bold but unsupported claim, and now he needs to back it up. That puts his audience in a skeptical and adversarial mindset. They're thinking: "Oh yeah? Prove it." 

In contrast, imagine if Jeff explained how his company just drilled small holes into a house's walls, inserted flexible pipes into the pre-existing pipes, and left those old pipes behind, all in a single day. At this point, his hook would almost be a foregone conclusion — and that's precisely how you want your hook to seem. 

By the time you're done walking your audience through the core concept of what you're pitching them, they should already be on the verge of thinking, "Wow, that's cool." With your hook and your edge, you're just going to hammer down the nail you've already set.

### 12. Final summary 

The key message in these blinks:

**To persuade a skeptical, savvy and impatient modern audience, your pitch needs to be under three minutes. To create a persuasive pitch that fits into that time frame, it needs to consist of about 25 sentences that answer the following questions: What is it? How does it work? Are you sure? And can you do it? To maximize the impact of your pitch, you then need to make sure it has an opening, a callback, an "all is lost" moment, a hook and an edge.**

Actionable advice: 

**Put it all together.**

If you follow the instructions in these blinks, you'll have all the elements you need for your three-minute pitch. But how do you put them all together into a final pitch? Here's the order that Brant suggests. Obviously, you start with your opening. Then you convey the basic concept of it by answering the questions "What is it?", "How does it work?" and "Are you sure?" Then comes your "all is lost" moment. Follow that up by delivering your hook and your edge. Then do a callback. Finally, close your pitch with your answer to the question "Can you do it?" Keep in mind that some of these elements may go hand-in-hand with each other. For example, in Brant's pitch for _Bar Rescue_, Jon Taffer introducing the "butt funnel" was the edge, and the fact that it illustrated Taffer's expertise was the callback. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Pitch Anything_** **, by Oren Klaff**

Now that you know how to create a solid three-minute pitch, why not spice it up with some tips and tricks derived from modern psychology and neuroscience? Investment banker Oren Klaff has quite a few up his sleeve. They include techniques for securing people's attention, building and releasing tension, and framing your topic to your advantage. 

Oren himself has used these techniques to deliver successful pitches that have helped him raise more than $400 million of capital over the course of his career. Want to know the secrets to his success? Check out our blinks to _Pitch Anything_, by Oren Klaff.
---

### Brant Pinvidic

Brant Pinvidic is a veteran television producer, a C-level corporate consultant and an award-winning documentary film director. He has used his method of pitch development to successfully pitch over 300 TV and movie projects. They include the hit TV shows _Bar Rescue_ and _Extreme Makeover: Weight Loss Edition_, both of which he was the executive producer for. He is also a columnist for _Forbes_ magazine and the host of the popular podcast _Why I'm Not_. _The 3-Minute Rule_ is his first book.

