---
id: 5e541ab16cee0700079a22ba
slug: the-4-day-week-en
published_date: 2020-02-25T00:00:00.000+00:00
author: Andrew Barnes with Stephanie Jones
title: The 4 Day Week
subtitle: How the flexible work revolution can increase productivity, profitability, and wellbeing, and help create a sustainable future
main_color: None
text_color: None
---

# The 4 Day Week

_How the flexible work revolution can increase productivity, profitability, and wellbeing, and help create a sustainable future_

**Andrew Barnes with Stephanie Jones**

_The 4 Day Week_ shows us a better way of working — one in which employees are able to maintain flexibility, preserve their well-being, and increase productivity. By avoiding all the pitfalls of the precarious gig economy and relieving the stresses that we're inflicting on the planet, the four-day week is the future of work.

---
### 1. What’s in it for me? Learn how the four-day week can change the world. 

We're right in the middle of the Fourth Industrial Revolution. Technology has changed the way we work and the terms by which we're employed. Driven by digital technology and the internet, we've seen the rise of the _gig economy_ — which is characterized by temporary freelance jobs with flexible working hours and little in the way of benefits. Often this contract work is shaped by online communication rather than a worker being physically in an office. That means employees are constantly online and plugged into their latest assignments — they're always at work.

Because of these changes, many switch jobs often, work too much and have few of the benefits of secure employment. They're often overstressed and financially unstable. Simply put — the whole set-up is unsustainable.

Luckily, there's an alternative approach — the four-day week. By offering the flexibility of the gig economy _and_ secure, full-time pay, the four-day week could be the answer to many of the problems in the modern workplace. Read on to find out more about the future of work and how the four-day week can improve worker well-being and increase productivity and profitability.

You'll find out

  * what Bruce Springsteen shows us about work in the past; 

  * how a plane journey and a copy of _The Economist_ changed everything; and

  * why the four-day week could help save us from climate breakdown.

### 2. The way millions are employed in the twenty-first century has to change. 

If you listen to Bruce Springsteen's lyrics, you'll be transported back to an era of heavy industry — sweat and toil in some Midwestern lumber yard or steel factory. 

Though industrial work was hard, workers knew where they stood. They clocked in and clocked out at a definite time. They had a contract. If they were lucky, they got sick pay and a pension. 

How many of us can say the same today? Many of us work jobs that don't provide even the most basic security.

**The key message in this blink is: The way millions are employed in the twenty-first century has to change.**

With the advent of the gig economy, companies are no longer obligated to provide these basic guarantees. And though, theoretically, a gig contract provides freedom and flexibility for the employee, the reality is often a precarious existence. That's because many in the gig economy are categorized as freelancers even if they appear to work full-time for their employers. 

When you're a freelancer, your employer doesn't have to provide things like holiday pay or a pension scheme. It also means that you're readily expendable, and companies don't need to offer you redundancy pay. If you make the smallest mistake — like, say, a gig-contract courier arriving a few moments late with a package — you're liable to be fired on the spot. 

Overall, this amounts to an awful lot of stress for the modern precarious worker. Cumulatively, this employment situation can have terrible effects on the individual and society as a whole.

In fact, this instability is making us ill. Because gig-economy employees and those in other forms of temporary work mostly communicate with their workplaces via the internet, they have an "always-on" mentality. They're always on call. So the distinction between labor time and free time vanishes. Unsurprisingly, without time to unwind properly or to organize important routines — say, eating healthily or exercising — we become more susceptible to stress and illness. 

The problem is exacerbated by an ever-rising cost of living. As many workers can't afford to live near their workplaces, they often have to commute early in the morning and late at night on crowded public transport. Naturally, they're more likely to get run down or catch colds or flu. 

So what does this mean for the company that employs these workers? Well, employees who are overstressed or ill are prone to mistakes and absenteeism. Eventually, this kind of employment model kills productivity and becomes a drain on the organization itself.

### 3. We’re addicted to convenience, and it’s creating an unsustainable work culture. 

Think about all the things you can do with your smartphone. You can order a gourmet restaurant meal to your door within 20 minutes, or book a surfing break to Hawaii in even less time. 

Let's face it. Our society is addicted to saving time. Convenience comes above all else. Let's consider Uber, which had its origins in the idea of car-sharing. What was meant to reduce passengers' carbon footprints is now an enormous fleet of vehicles that achieves the exact opposite. Ethical concerns are quickly tossed aside when a convenient ride is on offer.

**The key message here is: We're addicted to convenience, and it's creating an unsustainable work culture.**

It turns out, the welfare of workers is also sacrificed in favor of convenience. 

Let's examine this with an example: you've ordered an Amazon package from another country around Christmastime. How does this package reach you on time? Firstly, a seasonal worker, with no benefits or entitlements, who can be fired at a moment's notice, will locate the product at the Amazon warehouse and package it up. Then, a truck driver on a gig contract will transport the parcel to the nearest airport. As she can be easily replaced, the driver's lack of leverage with her large employer means that her pay remains low. After arrival at the airport, the package will be flown to the customer's country, racking up a carbon footprint that the cheap purchasing cost doesn't cover. Then, it will be handed to another gig courier for urgent delivery. For this driver, delivery time is crucial. If she's late, she's out of her job. And because congestion is always a problem in the city in which she's based, she must work longer hours to ensure the parcel arrives on time. Sometimes, she's not home until long after dinner time.

This is the ultimate cost of convenience. Because our society is focused on getting what we want immediately when we want it and for as cheaply as possible, it means that, somewhere, a worker is paying the price. In the Amazon example, for you to benefit and receive your package in a few days' time with the click of a button, many workers suffer from job insecurity, poor pay, terrible hours, and no benefits. And this is true not just for Amazon, but for many different sectors in our economy. Surely there's a better way, right?

### 4. Organized labor helped improve working conditions, and this struggle is still relevant today. 

During the Industrial Revolution, workers were subject to the most appalling conditions. Some were forced to use factory machinery that was liable to take a limb, pushed up soot-filled chimneys, or made to work inhumane hours. Employment back then could take years off of your life.

But through combined effort, the workers fought back.

**So, the key message here is: Organized labor helped improve working conditions, and this struggle is still relevant today.**

Whether that's fighting for the eight-hour day or the minimum wage, pressure always has to come from below.

Through the formation of trade unions or concerted strike action, workers have forced their bosses to yield to fair demands. In the United Kingdom, for example, the appalling conditions of workers in the late nineteenth century led Keir Hardie to form the Labour Party. Eventually, after the party was elected to government, it legislated for sweeping reform. Over the decades, the Labour Party introduced the National Health Service, the minimum wage, and tax credits for low-paid workers. 

These struggles are just as relevant today. Take the example of Gary Smith, employed by Pimlico Plumbers in London. His employer tried to treat him as "self-employed" so that they would be exempt from certain responsibilities, like granting him holiday pay. Smith took Pimlico Plumbers to court and won his case to be considered a "worker," with all the entitlements of a full-time contract.

However, the modern gig economy poses a special problem for organized labor.

This is because the giant corporations that employ many gig workers are able to avoid national legislation by maintaining a digital presence. It's much harder to fight for your rights against a shadowy transnational behemoth that slips in and out of national jurisdictions than it is to take on a domestic business like Pimlico Plumbers.

For many of these corporate giants, workers are mere tools. And because of the often digital nature of gig-economy employment — think Uber drivers or online copywriters — workers are subject to surveillance and evaluation like never before. They're treated like algorithms — machines to deliver targeted outcomes. If they make a mistake — as human beings tend to — they can be replaced. And before they can mount any opposition, whole workforces can be dismissed at a single stroke. 

As we saw in the last blink, this instability leads to deep problems, first for the worker, then for society at large. If a solution isn't found quickly, we may well face a dystopian future.

### 5. The four-day week allows one extra rest day per week and can drastically improve worker productivity. 

While aboard a flight to London from his home in New Zealand, the author leafed through his copy of _The Economist_. Little did he know that sandwiched between the political commentary and financial stories was an article that would change his life. And, for that matter, potentially the lives of many others.

In that article were two studies of office workers in Canada and the United Kingdom. The revelation, which astonished the author, was that these office workers were only productive for 1.5 to 2.5 hours of a typical eight-hour day.

**The key message here is: The four-day week allows one extra rest day per week and can drastically improve worker productivity.**

As a business owner responsible for over 240 people, the author was amazed by the statistics. Suddenly, he understood that many of his _own_ employees were also only productive for a couple of hours per day.

And then he had a theory. If his employees were productive for an average of 2.5 hours per day, then he only needed to claw back 40 more productive minutes every day to get the same output from staff in a four-day week as in a five-day week.

This was the inception of the 100-80-100 ratio. This meant that staff receive 100 percent of their compensation and need to work only 80 percent of the time, provided they deliver 100 percent of the agreed productivity.

After a trial period at the author's own business — Perpetual Guardian, a corporate trustee company — the author became convinced that the four-day week could provide many of the solutions to the problems of modern work.

With an extra day to rest, spend time with loved ones, or pursue other interests and ambitions, the workforce improved their general well-being and attitudes at work. He found that a rested, contented workforce is more productive and also more likely to invest in the objectives of the company.

The four-day week also offers solutions to the crises of precarious work that we looked at in the previous blinks. By offering staff the certainty of contractual compensation in return for four days of productive work, the arrangement could be the answer to the stress of an "always-on" mentality and the absence of job security.

We need a new working arrangement to relieve the stresses and insecurities that are placed on the modern worker, and the four-day week is certainly one component of that. In the next blink, we'll look at how an organization can implement a four-day week successfully.

> _"[T]he five-day week is a nineteenth-century construct that is not fit for purpose in the twenty-first century."_

### 6. To successfully implement the four-day week, constant communication with the workforce is vital. 

For any organization, transitioning to the four-day week is _challenging_. There are no two ways about it. The two main obstacles are miscommunication and a lack of understanding between all levels of staff.

**The key message here is: To successfully implement the four-day week, constant communication with the workforce is vital.**

Importantly, the employees of any corporation must be engaged _throughout_ the entire process for it to be a success. With that being said, there are four key things that an employer must keep in mind for a successful rollout of the four-day week.

First off, management needs to be crystal clear about the objectives of the four-day week. It's important that staff understand that it has a broader purpose: it's not simply a long weekend. Its aims are to increase productivity, reduce absenteeism, attract and retain talented staff, and achieve better overall well-being and work culture. 

Secondly, employers need to engage employees in the process, so they're better able to achieve all of that. For instance, they should be asked, in an open and accepting forum, what will help _them_ be more productive. It might be that certain staff are able to perform different tasks more effectively on specific days, because of other commitments or personal needs. 

Thirdly, when considering ways to boost productivity, leadership should keep in mind the desires and requirements of individual staff. Flexibility is the watchword. Perhaps one employee has a religious rite to observe on a specific day or is undertaking supplementary studies. All of these details can be taken into account by the company to get the most out of a workforce in the four-day week. 

The fourth and most important thing a company should do is avoid top-down decision-making at all costs. 

If a company ignores one of these things, the transition to a four-day week can fail. Here's an example of a four-day week gone wrong. In one organization, management decided — without canvassing its staff — that Friday would _always_ be the day off, effectively gifting everyone a long weekend. Naturally, this rigidity didn't suit everyone in the organization, and after many issues, the trial was abandoned.

However, when mistakes like this occur, the experiment doesn't have to be over. If a company continues to communicate, the final objective — that is, a successful four-day week — can be reached. It helps to view organizations as organisms going through evolution, where mistakes are a necessary part of an ongoing process.

And crucially, from a leadership position, you should never ask an employee to do something that you yourself wouldn't be happy doing. Transitioning to a four-day week is a shared endeavor; it's a contract between _everyone._

### 7. Flexible Working Agreements combined with the four-day week are the future. 

Without flexibility, workers are forced to fit the rest of their lives around their work. This means that their lives outside of work are almost an afterthought. And with so many jobs taking place in the digital space, for a lot of us, the rigid nine-to-five, five-day week in the office doesn't fit anymore.

**The key message here is:** ** _Flexible Working Agreements_** **combined with the four-day week are the future.**

Flexibility is a key part of the four-day week _._ We're not talking about the same pseudo-flexibility offered by the gig economy, where, for instance, a courier is always on call even if they're not officially at work. We mean flexibility that allows for real time off and maintains _full-time pay_. This takes place under a _Flexible Work Arrangement_ or FWA.

The two main categories of the FWA are flexibility in _where_ work is performed and _when_ work is performed. In the context of the four-day week, this means allowing employees to select the day that they would prefer to keep free, and offering them the choice, if possible, of working from home or remotely.

Both the employer and employee can reap great rewards from this approach. 

Firstly, there's proof that FWAs are beneficial for the organizations that offer them to employees. In fact, companies that offer FWAs have found that they're well-placed to hire top talent. At Deloitte, the multinational professional services network, 80 percent of managers said that flexibility made a difference to their recruitment success.

And on a raw cost basis, FWAs can reduce operating costs for businesses. Letting employees work offsite sometimes will cut down on overheads and office-space requirements. For example, let's say someone's child is sick. If he's permitted to work from home while looking after his child, he doesn't need to take costly leave.

Just as important, FWAs are wonderful for employee satisfaction and wellbeing. At JP Morgan, for instance, the annual employee survey found that employees with the option of flexibility were much more likely to report overall satisfaction than others without it.

And on a very fundamental level, allowing employees to spend more time outside of work has a beneficial impact on their mental and physical health. They're able to exercise, read, cook, or get back to any hobbies or personal ambitions that they've probably been too busy to pursue. 

And while the option of flexibility improves the lives of employees, it feeds straight back into the company as well. Consciously or not, employees will be likely to go over and beyond agreements in their contracts, making the FWA and four-day-week model _more productive_ than those with rigid working arrangements.

### 8. Productivity must be at the center of the four-day week. 

When the author tours the world and gives speeches on the benefits of the four-day week, he has to remind his audiences that he's first and foremost a businessman. He runs Perpetual Guardian, which is a profit-making venture, not a charitable organization or a social enterprise.

He believes that, however much reform is needed, our existing, profit-seeking economic model is still the best way to sustain prosperity. The productive business is integral to that. And the four-day week is, foremost, a means to maximize productivity; it is another link in the economic chain, not its destroyer.

**The key message here is: Productivity must be at the center of the four-day week.**

Despite all the flaws in our current economic model, business leaders are still responsible for the livelihoods of the vast majority — from those at the lowest pay grade to the shareholders. They're responsible for providing the tax base that funds many of society's necessities, from healthcare to infrastructure to schools. They're the key innovators that improve our comfort and well-being. For all of the good things that we desire, businesses must flourish.

It follows, then, that employees must view the four-day week through that prism, and not simply as another day off.

The most effective way to do this is for employees to learn to value time as the most important currency. With time in such scarcity in the modern world, it should be valued and prioritized above all. This means that, for employees to receive the gift of time — that extra day off — productivity must be maintained. 

And if productivity slips, then the return of the five-day week will be necessary to maintain normal levels. This is the carrot-and-stick approach that allows the arrangement to work. Unlike the precarious gig worker, for whom a loss of productivity often means dismissal, here it would just involve an extra day at work. The organization could keep the five-day week for as long as it takes to get back on track, and then return to four days when everything runs productively again.

Lastly, as a return to the five-day week would be the result of a dip in productivity, it'd be a collective responsibility for the whole team to make the four-day week work. With time at stake, _everyone_ would be more motivated to succeed.

### 9. The four-day week is an urgent response to the inequality crisis. 

In the twenty-first century, the divide between the very wealthy and the vast majority has grown.

And this wealth inequality has echoes of the past. The largest economies, like the United States and the United Kingdom, rely on the exploitation of precarious workers, just as they did during the Industrial Revolution. And just like then, it is unsustainable for a healthy society.

**The key message here is: The four-day week is an urgent response to the inequality crisis.**

The United States, in particular, is on its way to becoming a quasi-feudal society, with the top 1 percent controlling about 20 percent of national income, and the bottom 50 percent controlling just 12 percent. The gig economy, especially, could be described as a new form of feudalism with a handful of rulers — that is, the CEOs, founders and entrepreneurs, investors, and shareholders — who are becoming even richer off the backs of the masses. 

In fact, American venture capitalist Nick Hanauer has compared this situation to eighteenth-century France in the years before the Revolution. He gives a stark warning: his conclusion is that there is _no_ example in human history where wealth accumulated like this and bloody revolution and war didn't break out. 

But the author believes there is a reasonable solution that could preserve healthy capitalism and offer workers the security they need: the four-day week.

Rather than the unsustainable precarity that many workers face today, the four-day week would allow for the space and freedom that the gig economy was _supposed_ to offer, along with increased job security. 

By offering these things, the four-day week provides us with a long-term plan through which our current system can survive. Currently, the gig economy consists of big corporations who avoid responsibility for their workforces and leave everyone else to pick up the tab for things they don't want to pay for, like healthcare, retirement, and even the very infrastructure that they use. This puts an impossible strain on any society's resources. And, eventually, such a situation will mean the warnings of those like Nick Hanauer may come true.

To improve this situation, we require a more responsible corporate culture all round, but the four-day week offers a bridge between an unaccountable gig economy and one that is more sustainable.

### 10. The four-day week can be part of a holistic plan to work toward a healthier planet. 

As the news of melting polar ice-caps and whole swaths of bushland on fire demonstrates: our planet is in trouble. We need to act _yesterday_.

The good news is that a four-day week can have an immediate impact on planetary health.

In a big city, the introduction of a four-day week would mean an immediate reduction in the number of cars on the road. Potentially, if the daily in-office headcount could be reduced by 20 percent (with different employees taking different days off), then the number of automobiles on the road could drop by up to 40 percent. Up to 29 percent of greenhouse gas emissions in the United States come from transportation, so this would mean an enormous drop in total emissions.

**The key message here is: The four-day week can be part of a holistic plan to work toward a healthier planet.**

In the United States alone, the adoption of the four-day week would mean a reduction in emissions that would be the equivalent of removing _10 million cars_ from the road! As an article from the HR department of the University of California, Davis puts it: "Not going into work could be one of the most environmentally sustainable things you can do as an individual employee."

And there are other, more direct ways that the four-day week can benefit the planet.

For instance, at the author's company, Perpetual Guardian, employees who opt into the four-day week scheme are required to give one of their free days to charity each quarter. They can then choose which charity to support, and the only requirement is that they volunteer in some way. When each employee at Perpetual Guardian donates four days a year of their time, that adds up to about 1,000 days of socially good activity annually. This obligation serves as a reminder to the employees that the objective of the four-day week is to enable them to be the best they can, at home and at work. And contributing to the community is integral to this.

If more businesses were to follow Perpetual Guardian's model, the potential to repair the damage we've done to the planet would be enormous. With joined-up action across corporations, we could see millions of employees planting trees, volunteering in environmental clean-ups, or educating their communities about sustainability. The four-day week could play a vital role in turning things around before it's too late.

### 11. Final summary 

The key message in these blinks:

**The way we work today is unsustainable: too many of us are in precarious employment that leaves us overstressed and unable to make ends meet. The four-day week offers a realistic solution to this crisis by increasing productivity and offering workers security and flexibility. Vitally, the four-day week could help save the planet, too, by reducing the number of people traveling to work and cutting their emissions.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _The 4-Hour Workweek,_** **by Timothy Ferriss**

You've just learned how a four-day week can transform the work environment and help us achieve better well-being and sustained productivity. Curious about how to take that less-is-more approach even further on a personal level? Then follow Tim Ferriss's blueprint for _The 4-Hour Workweek._

These blinks describe the life of those who've managed to free themselves from the toil of office work and build a life centered around personal well-being and satisfaction. Learn how to become like the New Rich who've whittled down their commitments and found a continuous source of income by checking out the blinks to _The 4-Hour Workweek_.
---

### Andrew Barnes with Stephanie Jones

Andrew Barnes is an entrepreneur and philanthropist who founded New Zealand's largest corporate trustee company, Perpetual Guardian. He pioneered the four-day week in his own company and brought the concept to the forefront of the conversation around work. He lives in New Zealand and enjoys restoring his classic yacht, _Ariki_, and cultivating his vineyards on Waiheke Island.

