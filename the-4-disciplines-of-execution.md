---
id: 558062f662323200072b0000
slug: the-4-disciplines-of-execution-en
published_date: 2015-06-17T00:00:00.000+00:00
author: Chris McChesney, Sean Covey, Jim Huling
title: The 4 Disciplines of Execution
subtitle: Achieving Your Wildly Important Goals
main_color: 247BB5
text_color: 1F6899
---

# The 4 Disciplines of Execution

_Achieving Your Wildly Important Goals_

**Chris McChesney, Sean Covey, Jim Huling**

_The 4 Disciplines of Execution_ (2012) is a manual for CEOs and managers, showing leaders how to execute their strategic goals by getting their staff to behave differently. By introducing the four disciplines of execution, you'll help motivate your team to achieve broader company goals.

---
### 1. What’s in it for me? Learn how to put your plans into practice. 

At the start of every year, many of us make bold promises to change our lives. We get a gym membership and decide we'll have those abs by summer. But then February rolls around, and we're as gym-shy as we were in December. Why is that? Quite simply, it's because we fail when it comes to execution.

The same is true for companies. Being capable of change is what makes companies succeed — but changing requires execution. So how do you do it?

As these blinks will show you, there are four disciplines you should try to follow to improve your execution and reach your goals. We aren't talking about screaming at your employees or implementing draconic strictures. If you learn to incorporate these disciplines into your company it will become clear what your most important goals really are and you'll succeed in one of the most difficult things there is: changing people's behavior.

In these blinks, you'll learn

  * what whirlwinds have to do with reaching your goals;

  * why fuzzy goals won't get you to the moon and back; and

  * why you should always have a scoreboard in the office.

### 2. Getting people to change is the real challenge of executing strategic goals. 

Change is good, especially from a business standpoint. Why? Well, look at it this way: If you aren't always improving, you're creating an opportunity for your competition to swoop in. Preventing that is a big challenge. And here's why:

Even though there are an infinite number of possible growth strategies, there are only two ways to execute those strategies: with the stroke of a pen or by changing human behavior.

Of course, stroke-of-the-pen actions are easy for executives. All they have to do is sign a paper and then someone, somewhere, will take care of the rest.

But these are normally quick-fix actions. Lasting change, on the other hand, requires people to alter their behavior. That's where most executives come up short — and not surprisingly. Anyone who's ever stopped smoking or gone on a diet will concur: change is hard. And these examples only involve changing yourself. Changing others is even harder!

After all, your staffers might not understand the company goal or have a clear sense of how changing their behavior will help achieve that goal. Alternatively, they might simply not care.

At first glance, it may seem like there are easy fixes to these problems. You could just hand out detailed descriptions of company goals, be precise about each team member's responsibilities and fire anyone who doesn't care. But the heart of the problem is far more complex.

All of these problems and decisions are called the _whirlwind_ — a term the authors use to describe the daily tasks that take up your time and drain your creative energy. The whirlwind is the biggest foe of change. Imagine you spend an hour persuading someone to make certain changes; meanwhile, they're busy thinking about the ten urgent things that need to be taken care of ASAP.

Although it's difficult, you can achieve major strategic goals despite the whirlwind. Mastering the four disciplines of execution makes it easier. And we'll explain those to you in the upcoming blinks.

> _"The real enemy of execution is your day job!"_

### 3. Focusing on specific, wildly important goals is the first discipline of execution. 

The first discipline of execution is to focus solely on what matters.

We understand the instinct to strive to do more. Since most executives are overachievers, they are particularly motivated in this respect. But the more you try to do, the less you'll be able to focus on and put effort into individual tasks. If you want to achieve something truly excellent, you have to concentrate on it.

Accordingly, your strategy should prioritize one or two _Wildly Important Goals_ (WIGs), which you can pursue from within the whirlwind.

Cutting costs by 20 percent by the end of the year is one example of a WIG, but here's a general rule of thumb when you're coming up with these goals: WIGs should be specific and have a huge impact on your team's performance.

Specificity is especially important because a WIG shouldn't function like a vision or a mission statement. Rather, it's about outlining a clear goal that the whole team will work toward.

To understand why specificity can be so powerful, consider this story: In 1958, NASA operated under the vague goal of expanding "human knowledge of phenomena in the atmosphere and in space." But that changed, in 1961, when President John F. Kennedy publically called on the agency to put a man on the moon and then return safely to Earth before the end of the decade. And that's how, with one clear goal and a defined timeframe, Neil Armstrong ended up setting foot on the moon on July 21, 1969.

As the NASA example makes clear, it's important to find WIGs that make a major impact. You don't want to use up all your blood, sweat and tears on something that doesn't really make a difference for your company.

> _"Basically, the more you try to do, the less you actually accomplish."_

### 4. The second discipline of execution: meet your goals by choosing measures that reflect current behavior. 

As we learned in the previous blink, you have to focus on achieving wildly important goals. And to do so, you should concentrate on measures that help you win, not measures that make you depressed when you come up short.

This is easier said than done, since most people naturally focus on _lag measures_. To clarify, lag measures reflect past performance, showing your position relative to your goal. Profit margins and customer satisfaction rankings are examples of lag measures.

And although concentrating on those measures is natural, it does little to help you achieve your goal. The point is, these indicators reflect past events you can no longer change, which is why focusing on them can be disheartening.

For instance, let's say you're trying to lose weight. It's not a great idea to wait until the end of the month to jump on the scale and measure the result. Because if you don't meet your goal, you'll feel like a loser. And, by that point, it'll be too late to do anything about it.

This is why, to achieve your wildly important goals, you should instead focus on _lead measures_. In contrast to lag measures, lead measures reflect current behavior, meaning you can still influence them to help meet your goal.

With the losing weight scenario, calorie counts and exercise metrics would be great lead measures, since they're predictive of the weight-loss goal. After all, if you monitor your diet and exercise, you'll most likely lose weight. Furthermore, these are two factors you have direct control over.

Of course, we all know that the path to weight loss involves eating less junk food and exercising more. But knowing and doing are two different things. And you're more likely to actually follow through on your plans when you keep track of lead measures, since these metrics show you how your current actions directly influence your goal.

> _"To achieve a goal you've never achieved before, you must do things you've never done before."_

### 5. Motivate your team by keeping score of their performance: the third discipline of execution. 

With the first two disciplines, you define your wildly important goal and identify which metrics will help you achieve it. The third discipline is about helping your staffers identify with the goal, thereby motivating them to help you achieve it!

To that end, you should have your team keep score of team-member performance. This will improve performance, because, after all, everyone likes winning. And people instantly become more engaged when there's a victory at stake. Accordingly, keeping score is a way of activating that game-face energy and motivation.

For instance, when you watch kids playing football in the park, you can probably tell right away whether they're keeping score — the way they cheer for every touchdown is all the information you need!

And that's why your team should create some kind of scoreboard tracking each person's progress on the WIG. Whether you use a sophisticated online tool or an old-school chalkboard, the point is to show everyone how they're performing.

Since motivation is the goal of the scoreboard, you should make sure it's comprehensible and also easy for your employees to manage autonomously. It should also include lead and lag measures, along with essential information about where the team _should_ be and where it is in reality. With all this information clearly laid out, every team member must be able to tell, at a glance, whether they're winning or losing.

Let's say that increasing production by 20 percent by year's end is one of your WIGs. And to meet that goal, your team has to increase production by five percent each month. Your scoreboard should show lag and lead measures related to this specific goal, so your team knows how they're actually performing.

> _"When winners are given data that shows that they are losing, they figure out a way to win."_

### 6. The fourth discipline of execution is establishing a culture of accountability. 

With the third discipline, we learned how to motivate staffers to be part of your wildly important goals. But the fourth discipline is the real heart of the execution process: It's about making team members commit long-term to the goal.

And in order for that to happen, your employees have to be accountable to _each other_ — not just to you. No one wants to disappoint their peers, so staffers will feel a greater sense of responsibility if they have to answer to their colleagues, too.

That's why holding regular WIG meetings will ensure a lasting commitment to the goals. These gatherings should include: 1) an overview of the commitments from last week, 2) a review of the scoreboard and 3) a plan for the following week.

These meetings will guarantee steady progress toward the WIG, since every team member will be responsible for setting and meeting weekly commitments that have an impact on the lead measures.

It's important to allow staffers to choose their commitments themselves, so they are engaged in the process. As team leader, your role is simply to make sure that commitments are specific and directly connected to the WIG.

Valet parking services company, Town Park, offers a great example of how this works in action. When they adopted the Four Disciplines method, team leaders decided on the WIG of increasing customer satisfaction. To this end, they identified "reducing retrieval time" (i.e. the amount of time between a customer's call for a car and the valet's delivery of it) as an important lead measure. By focusing on this metric, the valets came up with an innovative solution: They would rotate cars from the back of the parking lot to the front whenever they knew a customer would soon be calling for it.

So much for the four disciplines! But it's not quite so simple: It takes real effort and commitment to implement them in your own workplace. Read on to learn how!

### 7. Follow a step-by-step process to implement wildly important goals and identify useful predictive measures. 

Introducing the four disciplines to your own workplace might be tricky at first, but it will be well worth the effort! To install the first discipline (identifying a WIG), just follow these four easy steps:

Start by gathering many ideas. Have conversations with both leaders and staffers to make sure you're getting as much information as possible.

Then, for the second step, rank these ideas based on the impact they would have on the overall WIG.

For example, one big hotel chain's WIG was to increase total profit from $54 to $62 million by the end of the year. So as their secondary WIG, the hotel decided to strengthen the alliance between its restaurant and local sports and culture venues. They figured this would drive more customers to the restaurant and hotel, ultimately helping them meet their profit goals.

Next, test your ideas to ensure they're aligned with your overall WIG. The restaurant in the example above could have mistakenly decided to broaden their restaurant menu as their secondary WIG — but that wouldn't have necessarily boosted profit. That's why testing your WIGs is so important.

And then comes the final step, which is to identify a simple and clear definition of your goal. Ideally, you'd begin with a verb and follow by identifying the lag measure, a deliverable and a deadline. For instance: _Reduce_ (verb) _production costs_ (lag measure) _from_ _20 to 15 million_ (deliverable) _by December 31st_ (deadline). 

You can follow these exact same steps to select your lead measures, thus implementing the second discipline. It's especially useful to constantly re-evaluate and test your lead measures, thus making sure that they are predictive, precise and measurable.

### 8. Follow through on introducing a scoreboard and creating a cycle of accountability. 

As we saw in the previous blink, you can follow a few simple steps to install the first and second disciplines. You can't introduce the third and fourth disciplines with such methodic neatness — but some kind of plan is required.

Let's start with the third discipline. Setting a scoreboard requires less involvement from you than any other step. All you have to do is come up with the preliminary theme — a speedometer, a bar chart or something else entirely.

Then step back and ask your team to design and build the board. That way, they'll be even more engaged with the process. (Remember to tell them that a good scoreboard should only include essential information: the goal and the lead and lag measures.) And once the board is up, appoint someone to update the score on a regular basis.

Then comes the fourth discipline.

Installing a culture of accountability requires you to set an example for your team. This is a critical step — the fourth discipline is the heart of execution — but it's also a difficult one, because it requires true commitment and dedication. Remember: Once you decide on a weekly meeting, you have to attend each time.

Start the meeting by reporting on your own commitments, to show that it's not a scary or painful thing to do. Then, make sure to review the updated scoreboard and celebrate every success.

Keep in mind that the whirlwind has nothing to do with the WIG meeting, and make sure you're only discussing WIGs. All team members who don't meet their WIG commitment because of the whirlwind should be held accountable.

If they don't meet a commitment, make sure you handle it respectfully. Let the staffer know you value their work, but explain that it's crucial, for the sake of the whole team, that everyone follow through on their WIG commitments. And then, make sure to give them a chance to catch up on their commitment.

### 9. Follow a six-step process to involve all departments in the Four Disciplines model. 

If you want to implement the Four Disciplines at a large organization with more than ten different departments, make sure you plan it out carefully. This process involves six steps:

First, clearly define your overall primary WIG.

Next, involve the team leaders: Each department head should define one individual WIG (and appropriate lead measures) that aligns with the overall WIG. As the institution leader, you can veto certain goals that seem incompatible with the organization's broader goals, but make sure each manager has the freedom to choose her own WIG. Otherwise, you won't get the level of engagement that you're looking for.

Third, sit down with the team leaders to teach them about the Four Disciplines model. After all, they'll need to execute this process within their own departments!

After that, each team leader goes ahead and launches the process with her own team. They should also ask for feedback and get their department's approval for moving forward on the WIGs and lead measures.

In the fifth step, individual teams work on perfecting the method. Ideally, department heads should get additional coaching on the process for a period of at least three months.

And, finally, wrap it all up by setting up quarterly meetings with all the team leaders to discuss the progress you're making together as an organization.

And there you have it! You've implemented a process that allows you to execute your vision and achieve your goals.

### 10. Final summary 

The key message:

**Executing strategic goals requires changing people's behavior in the midst of a whirlwind of urgent daily tasks. But to execute their vision across the organization, company leaders should focus on just one or two strategic goals and useful key measures.**

Actionable advice:

**Choose a goal that will impact your performance.**

When you are choosing a wildly important goal, consider two sources: things within the whirlwind and things outside it. A wildly important goal from the whirlwind could be something that needs fixing as soon as possible or something your team already excels at and needs to use as leverage. Outside the whirlwind, you can usually choose among things that offer you the chance to gain strategic advantage, like including new product features.

**Suggested further reading:** ** _5 Levels of Leadership_** **by John C. Maxwell**

_5 Levels of Leadership_ is a step-by-step guide to becoming a true leader with a lasting influence. Using engaging real-life anecdotes and inspiring quotes from successful leaders, it describes key pitfalls that may be holding you back and explains how to overcome them.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Chris McChesney, Sean Covey, Jim Huling

Chris McChesney and Jim Huling are leaders at FranklinCovey, a company that helps individuals and businesses to improve their performance.

Sean Covey is an author, speaker and publishing executive. His work centers on time management and business leadership.

