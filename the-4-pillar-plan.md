---
id: 5a8c11eab238e10007431786
slug: the-4-pillar-plan-en
published_date: 2018-02-22T00:00:00.000+00:00
author: Rangan Chatterjee
title: The 4 Pillar Plan
subtitle: How to Relax, Eat, Move and Sleep Your Way to a Longer, Healthier Life
main_color: 488FA5
text_color: 488FA5
---

# The 4 Pillar Plan

_How to Relax, Eat, Move and Sleep Your Way to a Longer, Healthier Life_

**Rangan Chatterjee**

_The 4 Pillar Plan_ (2017) is a practical guide to living a healthy life. These blinks will show you what it takes to create the best possible conditions for both your body and your mind. It's all based on the four pillars of healthy behavior: relax, eat, move and sleep.

---
### 1. What’s in it for me? Relax, eat, move, sleep (repeat). 

If there's one thing we know about modern life, it's that we haven't got enough time — and this is especially the case when it comes to making positive changes to our lifestyles. It often feels as if there just aren't enough hours in a day to try to make yourself a better, healthier person.

But this assumption isn't true. As author Rangan Chatterjee demonstrates, our problem is that we've been taught to compartmentalize our health and diet, and to see both as burdens that demand excessive effort.

As it turns out, you can attain a healthier body and mind by trusting in Chatterjee's progressive approach to medicine — and it's not a snake oil cure that he's advocating. Rather, Chatterjee presents simple, practical steps that you can follow to make a noticeable improvement in your general health.

He bases his ideas on four basic pillars: relax, eat, move, sleep. While these hardly appear radical or groundbreaking at first glance, improvements in them can bring you a world of benefits.

In these blinks, you'll learn

  * what sugar does to your taste buds;

  * some helpful breathing exercises to promote your inner calm; and

  * why too much exercise can be bad for you.

### 2. Maintaining a healthy body isn’t just about treating symptoms of illness, it’s about a progressive approach to managing your lifestyle. 

What would you do if you woke up one day with a rash on your arm? You'd likely book an appointment with a doctor. The doctor would, in turn, probably prescribe you an ointment to soothe and treat it.

Situations like this might not strike you as odd, but they conceal a deeper truth: medicine today concerns itself almost exclusively with diagnosing and treating symptoms. What's forgotten is that the body is a complex, interconnected system, so the causes of an ailment may not be straightforward.

It's true of mental conditions too. Go to the doctor with symptoms of depression, and the doctor is likely to send you off with a prescription for antidepressants once he's said something about the chemical imbalance in your brain. However, according to the author, depression is caused by a variety of factors including poor diet, stress, or too much or too little physical activity.

The author believes doctors should stop focusing so much on symptoms. Instead, they should take a multipronged approach that recognizes the body's complexity and acknowledges that all its parts are connected. He calls this approach _progressive medicine_.

Let's return to the rash. A rash could indicate an overly reactive immune system; this, in turn, could be due to stress or food intolerance, or perhaps abnormal bacteria in the gut. 

You see, sometimes applying an ointment on the skin is not enough. If you're going to take progressive medicine seriously, then your lifestyle is where you can make a real difference. 

There are four pillars that the author considers critical for staying in good health: _relax, eat, move_ and _sleep_.

By encouraging his patients to adjust their lifestyles in one or more of these areas, Chatterjee says he has helped many of them out of depression, reversed type 2 diabetes, and even relieved symptoms of menopause.

So what exactly do these four pillars involve? Let's go into more details in the next blinks.

> _"The future of medicine will be about more doctors being super-generalists, rather than super-specialists."_

### 3. Relaxation keeps you healthy, so take 15 minutes for it every day. 

Admit it: you don't let yourself unwind enough, do you? Most people underestimate the importance of relaxing.

In fact, relaxing is as important to us as eating, moving, or sleeping. _Relax_ is the first pillar that the author points to, because not relaxing enough can, and likely will, damage your health.

Modern humans are still basically the same as the hunter-gatherers who walked around in forests and grasslands eons ago; we're not designed to sweat and toil in overcrowded offices, or pressed tightly by the thousands in modern metropolises.

Whenever hunter-gatherers encountered predators, the body's fight-or-flight response kicked in as a survival instinct. We still have that response — but our lifestyles are completely different. This means that our bodies are essentially in constant fight-or-flight mode, always under threat from the "danger" that is the twenty-first-century life.

When stressed, the body produces a hormone called cortisol. As the predator of modern life is always upon us, the result is that far too much cortisol is produced.

Increased cortisol levels lead to an increased heart rate, widened lung tubes, muscle contractions, and loss of appetite. Those are all fine responses if you're running at breakneck speed away from a saber-toothed tiger, but it's no way to live your day-to-day life.

An easy strategy for relaxation is to set aside 15 minutes each day for _me-time._ It shouldn't be a reward once you're done with work or chores; you should integrate it into your routine. If necessary, you can even set an alarm to remind you when to pause and take some time for yourself.

Relaxing can take many forms. It might be having a cup of tea in a nearby café, or reading a silly magazine. Whatever you choose, make sure it's something you do just for you and doesn't involve your phone, computer, or tablet.

And don't feel guilty about taking this time. Your health depends on it.

### 4. Stillness is good for your health, so make it part of your day with a simple breathing exercise. 

How noisy do you think the everyday lives of hunter-gatherers were?

It's no surprise to learn that their lives were very quiet for long stretches. During the day, they might have hunted in silence, sitting still with their bows primed. At night, they might have sat calmly around a fire.

That's what we're naturally geared toward: stillness and calm.

Our bodies and brains are not built for the noisy modern world, with all the distractions it has to offer. Hunter-gatherers had no social media, let alone a meeting schedule or a TV series to catch up on.

Stillness is beneficial for our health because it helps reduce daily stress. It achieves this by increasing the brain's grey matter, which is a critical part of the central nervous system, and which, in turn, controls muscles and sensory perception, which are involved in sight, hearing, and memory.

A fantastic way to incorporate stillness into your routine each day is through breathing exercises.

The author has designed a simple exercise called _3-4-5 breathing_. It helps foster stillness, by giving you time to focus only on breathing.

It's dead simple. First, breathe in for three seconds; then, hold your breath for four seconds; and finally, exhale over five seconds.

It doesn't matter whether you do it in your car, during your lunch break, at your office, or sitting cross-legged on the floor at home. It just matters that you do it and that you do it regularly.

Over time, you might want to do it for longer, over several minutes. But don't set your sights too high, too soon. Begin small, then slowly build up as you get more comfortable with the exercise.

### 5. Sugar does you and your tastebuds no good, so try to de-normalize it. 

Physiologically, we're all different. And thanks to different geographies and climates all over the world, we're each accustomed to different kinds of food.

That means there isn't one perfect diet that'll keep each of us perfectly healthy. However, where food's concerned, there's still some general advice that'll help keep you in good health. 

Which brings us to the second of the four pillars: _eat_.

By now, it's painfully clear that we consume far too much sugar. It ruins both our health and our taste buds.

In Britain alone, the number of people diagnosed with type two diabetes has soared from 1.4 million cases in 1996 to nearly 3.5 million today. And that doesn't even factor in the million or so undiagnosed cases.

As for taste, a 2016 study demonstrated the effect of sugar by comparing two sets of people on different diets. Initially, both groups ate the same sugary diet; then, one group moved to a low-sugar diet. When both groups were asked to rate the sweetness of the same dessert, the low-sugar diet group thought it was much sweeter. In fact, as their taste buds improved over the coming months, they became more and more sensitive to sweetness. 

What does this mean for you? Well, if you want to improve your dietary health and truly appreciate the flavors and nuances of the food you eat, you have to _de-normalize sugar_, which means reducing the amount of sugar in your diet.

Clear all the sugary treats you find from your cupboards. And, in the future, be sure to check everything you buy for its sugar content. All the information you need about this should be on the label.

Be vigilant, though — some foods, like meat, which you wouldn't even think to check, can contain sugar too! 

Before too long, you'll have altered your relationship with sugar, and your body will thank you for it.

### 6. Fasting helps your body clean itself up, so work micro-fasts into your daily routine. 

Did you know that as you go about your daily business, your body gets a little messy inside?

Imagine your house after a long day: clothes litter the floor, unwashed dishes fester in the kitchen sink, and children's toys are strewn all over the place. That's perfectly normal. "Mess" is a byproduct of living your life.

That said, your body, like your house, needs a daily tidying up.

Fasting helps your body do just that. This tidying process is called _autophagy_, and it keeps everything ticking over nicely — from cellular repair to immune system repair.

If you consume your meals within a 12-hour time slot every day and fast the remaining 12, you'll find autophagy's clean-up processes improve.

Studies suggest that this might be down to the liver ceasing to pump glucose into the blood after a few hours of fasting. Instead, the glucose is used for cell repair.

For you, this means developing a daily routine of micro-fasting.

Choose a 12-hour window that suits you best. It should begin with your first meal and end with the last. Make sure to keep it the same each day.

Outside of that window, you should fast. Limit yourself only to water, tea, and coffee.

It might sound difficult, but if others — like family members, housemates or colleagues — are involved too, it's much easier to stick to the plan.

Also, don't panic if there are a couple of wayward days that interrupt your micro-fast routine. That's fine. The important thing is to try to get into the rhythm and stick with it.

If it's going really well, you could even reduce the 12-hour eating window to 11, 10, or fewer hours still. This will give your body even better conditions for the clean-up.

### 7. Instead of exercising too much or too little, we should incorporate movement into our daily lives. 

When it comes to exercise, many people know full well that they're not doing enough.

However, the problem isn't just that many people don't do enough, it's also that some really overdo it. Neither situation is ideal.

According to the World Health Organization (WHO), some 50 percent of women and 40 percent of men in the United States and Europe aren't active enough. In comparison, in Southeast Asia, the figures are 15 and 19 percent for women and men respectively.

Another WHO study showed that inactivity accounts for five percent of all deaths worldwide.

Conversely, too much exercise is also damaging. An increasing number of cardiologists believe that taking part in endurance exercises like marathons too regularly may negatively affect the heart. 

More troublingly, a recent study of United States army personnel showed that when exercise is taken too far, the result can be a so-called "leaky gut." This refers to a condition in which the digestive tract leaks undigested food and other waste material and bacteria into the bloodstream. This, in turn, puts the immune system under severe stress.

These are extreme cases, but we can still learn from them. We only have so much energy to spend during each day. If our natural state is dominated by stress as we run from one meeting at work to the next, then we'll just exhaust ourselves even more by piling a punitive fitness program on top of that.

Instead of worrying about exercise in the abstract, we should be incorporating _movement_, the third pillar of a healthy lifestyle, into our daily lives.

Think about it: our bodies are designed to move — it's something we should do as a matter of course. Exercise is not just about pumping iron or making it to an extra spin class.

We shouldn't even call it exercise at all; movement is a much better word for it.

Let's now look at how you can incorporate more movement into your routine.

> "_We need to stop thinking about getting our exercise chore done and start thinking about making it a part of our daily lives."_

### 8. Make walking and strength training part of your everyday life. 

There are many ways you can make movement a more prominent part of your life.

First, try walking. You can begin with 10,000 steps a day. In the end, the number itself isn't actually that important; it's just an initial goal to get you going. It shouldn't take more than ten minutes to walk 1,000 steps, and you can use a pedometer to keep track of your progress.

A good way to ensure you stick to your target is to make yourself stand up and walk around after each hour of sitting. You can set an alarm to make sure you don't sit for longer, and you can use this break time productively by fetching a glass of water.

Also, try to walk whenever it's an option. Walking is healthier than taking the bus, and the stairs are better than the lift. Your colleague across the hall might even enjoy seeing you in person rather than getting an email!

Another way to integrate movement into your life is through strength training. There's no need to dash to the gym, though. Instead, be creative with your surroundings, whether it's the kitchen or office.

Here's an example of the kind of exercises you could do at least twice a week, no matter where you are:

Five to ten squats. Squats involve keeping both feet flat on the floor while lowering yourself down. Keep your back straight.

Five to ten calf raises. Stand up straight and slowly lift your heels, pushing yourself up on tiptoe.

Five to ten press-ups. Lie face down on the floor with both hands shoulder-width apart. Brace yourself on hands and feet. Lower your chest toward the floor and then push yourself back up.

Five to ten tricep dips. Stand with your back to a table or chair, and put your palms on the edge of the surface. Then, lower and raise your body by bending at the elbow.

Finally, try five to ten lunges. Begin standing upright, then put one foot in front of the other leg and bend both knees. Keep your torso straight, and lower your body. Then lift yourself up again.

These quick, simple exercises can have a major impact on your overall well-being.

### 9. Proper sleep is good for your mental and physical health. 

Without sleep, we can't function, which is why we spend a third of our lives asleep. Its necessity is what makes it the fourth and final pillar of a healthy lifestyle.

Sleep is critical for ensuring that mind and body work properly because, when asleep, the body repairs itself. It clears away the waste that builds up in your cells while you're awake.

Even though sleep is still not fully understood on a scientific level, we know that it has multiple benefits. Good sleep makes you more energetic, improves your attention span, and increases your ability to learn. It also reduces stress and the risks associated with being overweight.

So, sleep is fundamental to good health — but it's not just something to be measured in hours. The quality of sleep is important too.

There are three measures that are indicative of the quality of sleep you're getting.

First, you should feel refreshed when you wake up. This is also a good indicator of your general health.

Second, you should wake up at more or less the same time every day without an alarm. This shows your biological rhythm — or the internal clock that controls the body's biological cycle — is in top condition.

Finally, you should be able to fall asleep within about 30 minutes of lying down. If you can, this demonstrates that your everyday routine is geared toward helping you fall asleep.

Take a moment to rate yourself on these three measures. Give yourself 0 if they never or rarely apply, 1 for occasionally, and 2 if they are almost always true.

If you score 6, that's great; otherwise, there's definitely room for improvement.

But there's no need to panic. Quality of sleep is related to lifestyle, and this can be altered. Let's see how in the final blink.

### 10. To improve your sleep, immerse yourself in darkness and follow a bedtime routine. 

How can you get better sleep?

The first hack is to embrace darkness.

Darkness is, in essence, a signal to your body that it should rest. At night and in darkness, a hormone called melatonin is activated, which helps you fall asleep.

That's why it's a good idea to avoid light pollution, no matter how little it seems. Street lights, alarm clocks, and TV LEDs all pollute your sleep environment. So, if possible, eliminate this problem. You could put blackout blinds on your windows, tape over standby lights, or simply unplug all electronic devices in your bedroom.

The second hack is to follow a set bedtime routine.

Both a fixed bedtime and a fixed time for getting up are important factors in improving your health, as they help your body adhere to a daily rhythm. If, on some days, you go to bed or get up very late relative to your normal routine, you could throw your rhythm completely off-balance.

Because we're all different, no one routine is going to work for everyone. Still a repeated routine will, nevertheless, help and so is encouraged. Additionally, you should stop using any electronic devices at least 90 minutes before bedtime because the light from them can be too stimulating.

These are the rules by which the author himself abides.

By 6:30 p.m., he makes sure he's done with exercise. Then, he begins what he calls his "No-Tech 90" at 8:30 p.m. He turns off electronic devices such as computers, mobile phones, and TVs.

At this time, his dim red night-light is switched on.

In the hour between 8:30 and 9:30 p.m., he limits himself to only relaxing activities like stretching, listening to calming music, or doing breathing exercises.

His bedtime is 9:30 p.m. In the dim light, he reads until he feels sleepy — at which point he turns off the light and a gentle sleep falls upon him.

As the author's example demonstrates, sleeping is also a discipline that you can hone if you want to improve your general health.

### 11. Final summary 

The key message in these blinks:

**A healthy lifestyle is within your reach. By adopting a few simple practices based on the four fundamental pillars of relaxation, food, movement, and sleep, you can establish solid routines that will give your mental and physical health a big boost. So, start taking 15-minute relaxation breaks each day, engage in 3-4-5 breathing, get rid of sugar in your diet, and fast 12 hours each day. Try walking more and doing simple movements to work out your muscles wherever you are. Last but not least, to improve your sleep, keep your bedroom as dark as possible, stick religiously to a bedtime routine, and avoid any screens before going to bed.**

Actionable advice:

**Make Sunday a screen-free day.**

If you want to reap the benefits of relaxation, you might want to turn off your screen and phones once a week. Depending on where you live, a Friday, Saturday, or Sunday might be best for that. If you manage it, you'll have the time to relax and be present in whatever you do — whether you're with family or friends, or just reading a book. After all, we know how enticing the beep or vibration of our mobile phone can be. So, just turn it off.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _Follow Your Gut_** **by Rob Knight with Brendan Buhler**

_Follow Your Gut_ (2015) puts the world of microbes under the microscope, showing just how much influence the little things — in this case, bacteria — have on our life. The fact is, we're crawling with bacteria, both inside and out — and if we weren't, life wouldn't be so great. Bacteria serve many important functions, like keeping us happy and healthy. It's time to learn how to treat them well!
---

### Rangan Chatterjee

Rangan Chatterjee is a medical doctor based in the United Kingdom. After working as a general practitioner for nearly 20 years, he decided to become an author and teach others about what he has learned over the course of his career in medicine.

