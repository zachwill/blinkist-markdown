---
id: 54eaf036393366000a490000
slug: the-5-choices-en
published_date: 2015-02-25T00:00:00.000+00:00
author: Kory Kogon, Adam Merrill and Leena Rinne
title: The 5 Choices
subtitle: The Path to Extraordinary Productivity
main_color: 96C93E
text_color: 577328
---

# The 5 Choices

_The Path to Extraordinary Productivity_

**Kory Kogon, Adam Merrill and Leena Rinne**

_The 5 Choices_ shows us that extraordinary productivity isn't actually about working harder; it's about working better. By learning how to streamline your decision-making process, focus on important work and manage your energy, you'll see an enormous difference in your level of productivity and work quality.

---
### 1. What’s in it for me? Learn the to be amazingly productive. 

How many productivity apps do you have on your smartphone? Maybe one, maybe more! Most of us have used technology to try and get more done. For some, it may have worked. For most, the difference is negligible.

The truth is, extraordinary productivity (accomplishment without burnout) cannot be the product of coding and electronics. Only the awesome power of the human brain can make us more productive. We need to break free from machines and start using what's in our heads.

These five blinks outline steps to unleash your extraordinary productivity. In these blinks you'll find out:

  * the four types of information;

  * why urgency is not the same as importance; and

  * why it's more important to treat your body, rather than your smartphone, with love.

### 2. To be truly productive, focus on important but non-urgent tasks. 

Like most of us, you probably spend half your day at work checking email. Well, as you surely know, such distractions take a major toll on your productivity.

To avoid distraction, organize your tasks using a _time matrix_, a productivity tool consisting of four quadrants, each accounting for a different portion of your time.

Q1 includes important, urgent work. For example, this is the time you spend handling emergencies or last-minute requests.

Q2 consists of time spent working on important tasks that aren't urgent: that report for an important strategy meeting next week, for example.

Q3 time is for work that's urgent but not important; e.g., constantly checking your email.

And finally, Q4 is the time you waste on pointless things, like playing games on your phone or checking Facebook.

All in all, you should spend most of your time in Q2. After all, although you may _feel_ productive when you're in Q1 and Q3 — you're handling urgent business! — that's not actually the case. Because we too often mistake the urgent for the vital, rarely leading to top-notch work. (For proof, just consider the misspelled, hastily constructed emails you've surely sent out in your life.)

On the other hand, we do our best work in Q2. It's where we can focus and think, instead of simply reacting to whatever comes our way.

So, in order to stay in this quadrant, use the _Pause-Clarify-Decide_ method to determine rationally whether the task is important.

For instance, before opening a new email, pause for a moment and think: _Who is this email from? What's the subject? Will this need immediate action?_ If the answer to the last question is "no," go concentrate on something else instead.

> Fact: On average, only about 30 % of our time is spent in the most productive Q2 quadrant.

### 3. Be productive and complete important work by defining roles and setting achievable goals. 

We've just learned that we should spend most of our time focusing on important work. But what _is_ important work, exactly? Does it involve operational tasks? Or creative ones?

Well, it depends. Because ultimately, most jobs involve more than just one single role or task. So, on different days, different roles will be more important.

Ideally, you should focus on work that's most vital on any given day. For instance, on some days you may need to focus on a client. And then on other days, your employees are the priority.

To effectively juggle these disparate roles and get your work done, it helps to create a _role title_ and _role statement_ for each hat you wear.

Let's define those terms. A role title defines your current focus, and a role statement should lay out where you want to be.

So let's say that in addition to being a line manager and a fire safety steward, you are also a web developer. For the latter position, your role title might be, Professional Web Developer. Then, you'll want to create a dynamic role statement such as, "I strive to be an excellent web developer by mastering two programming languages."

Set achievable goals to stay on track with your statement. In this case, your goal might be, "I will learn two programming languages in the next two months." In two months, assess how productively you worked to meet your goals.

Now that you've spent some time improving your decision management by clearly defining your roles, their titles and goals, let's develop your attention-management skills.

### 4. Create a schedule to complete important work in the upcoming week. 

Imagine there's a landslide blocking a main road and you're asked to clear all the boulders and rocks. How would you go about doing that?

Well, you could start by moving the largest boulder. Although that's hard work, it's also the fastest way to clear the road. On the other hand, you could start by moving the gravel and the small rocks. That's easier, for sure, but it'll take longer.

In this scenario, the most productive option would be to concentrate on the big rocks first. However, as soon as you get tired, you may start shifting gravel instead.

This is a good metaphor for the experience of working in Q2: even when we make plans to only focus on the most important tasks, we often get tired and turn to the smaller, less important ones.

How can you avoid this productivity slide? Start by designing a plan you can stick to: a Master Task List where you can plan and track important work for the week ahead. 

Either on paper or electronically, plan everything you need to do. For instance, schedule blocks of time to spend learning programming languages. And then, create space each day for the non-scheduled bits (the gravel) that come up, like "urgent" emails and calls.

Ultimately, the Master Task List will allow you to get everything out of your head and onto paper (or a computer), where you can see things more clearly.

This method also makes it easier to stick to your plans, because when you schedule a task with a date and time, it's easier to feel that you have the time to get it done.

But of course, you need time to manage the task list, too! So, make sure you schedule 30 minutes per week and 10 minutes per day for planning.

> _"The key shift in thinking for Choice 3 is to realize that you can never get ahead by just sorting through the gravel faster."_

### 5. Streamline your decision-making process so you can focus on what really matters. 

Would you ever let a computer make big life decisions for you? Obviously not. After all, only _you_ can truly know what's important to you. But then, what _can_ you actually do to streamline and optimize your decision-making process?

Before we answer that question, let's first discuss the four types of input that comprise any decision:

  1. _Scheduled tasks_ : appointments, like the department meeting next week

  2. _Unscheduled tasks_ : going into your boss's office to ask for a raise

  3. _Contacts_ : information about clients, employees and competitors

  4. _Notes:_ information about your tasks — like a leaked report from a rival firm — that you want to discuss at the next meeting

These four inputs represent different types of information and it's up to you to sort each one into the appropriate place, onto a calendar, to-do list or contact list. Then, it's important to collate all these different lists in one place and link everything together.

For example, you'll want to connect your calendar — which includes a list of your upcoming meetings as well as related notes — with your contact list, so you can quickly reach fellow attendees.

Once you've figured out how to manage this information, you can start looking for appropriate technological tools to aid your organizational process. 

For instance, you can set up an email filter to automatically route messages. After all, the average office worker receives 121 emails a day, a lot of which is time-wasting spam.

In other words, let technology handle the most basic decisions for you, so you can concentrate on what's important.

### 6. The most effective productivity system is useless if you aren’t taking care of your body and mind. 

We've already covered a lot of valuable productivity techniques, but there's one crucial thing we haven't covered yet: the most effective productivity system is useless if you aren't taking care of yourself.

Because, ultimately, a working and functioning body is vital to productivity. And the best way to look after yourself — and especially your brain — is through energy management.

Although your brain only takes up two percent of your body mass, it consumes 20 percent of your energy. Thus, you need to make sure you have enough energy to feed your brain! But, unfortunately, modern life can be draining in this respect. Most of us work too much, sleep too little, eat poorly and stress out constantly. In short, we fail to manage our energy levels.

However, it doesn't have to be this way. In order to turn things around, we need to understand that energy comes from three sources.

The first source of energy is powerful purpose. This refers to the intrinsic motivation you get from doing productive work. This can come from doing something rewarding (like charity work) or from the satisfaction of a job well done.

But although you can mine this source if you have a job you value and enjoy doing, that alone is not enough. You also have to draw energy from your physical body. We weren't designed to sit around all day; our bodies require _movement_ to keep energy levels up.

Finally, _connections_ are the third important energy source. Although you might not realize it, energy largely depends on the health of our social connections (in other words, relationships and friendships). Thus, it's crucial that you take the time to develop and maintain strong connections with other people.

### 7. Final summary 

The key message:

**As long as you're willing to change, you can become more productive. It's not about working longer or harder, but rather about working better and concentrating on what's important. And also, it's a matter of taking care of your body and mind, so you have the energy to be productive.**

Actionable advice:

**If you're feeling overwhelmed by email, take an hour to organize your inbox.**

Archive the older messages and sort the rest. Then set up some automatic filters to handle the sorting for you in the future. That way, you won't become overwhelmed again.

**Make sure you exercise and eat well, even when you're busy.**

It's tempting to let your exercise routine slide when you've got a lot going on. But that oversight will backfire. Extraordinarily productive people take care of themselves and create time for daily wellness.

**What to read next:** ** _The 7 Habits of Highly Effective People_** **,** **by Stephen R. Covey**

So you've just discovered how to revolutionize your day-to-day productivity. But how can you ensure that the goals you're working toward are the right ones? After all, there's no point in maximizing your productivity if you're headed in the wrong direction.

This is where _The 7 Habits of Highly Effective People_ comes in. Stephen R. Covey's best-selling blockbuster explains how you can build habits and goals around the values that matter to you. To learn more, including why everyone should open an emotional bank account, check out the blinks to _The 7 Habits of Highly Effective People_.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Kory Kogon, Adam Merrill and Leena Rinne

Kory Kogon is a former executive and author specializing in time management, project management and communication skills.

Adam Merrill is an executive and author specializing in innovation, productivity and leadership.

Leena Rinne is a senior-level consultant working in international business. _The 5 Choices_ is her first bestselling book.

