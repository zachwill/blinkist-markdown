---
id: 59ec7f64b238e1000737fddc
slug: the-5-second-rule-en
published_date: 2017-10-26T00:00:00.000+00:00
author: Mel Robbins
title: The 5 Second Rule
subtitle: Transform Your Life, Work, and Confidence with Everyday Courage
main_color: 653917
text_color: 653917
---

# The 5 Second Rule

_Transform Your Life, Work, and Confidence with Everyday Courage_

**Mel Robbins**

_The 5 Second Rule_ (2017) is a transformative guidebook to taking action, changing your behavior and living life with less fear and more courage. The tips contained within are memorable, easy to implement and instantly effective. Anyone can start using them today to take control of their life and move confidently toward a brighter tomorrow.

---
### 1. What’s in it for me? Unlock your potential in five seconds. 

_The 5 Second Rule_ (2017) explains through concrete examples why so many people procrastinate and put off pursuing their dreams. These blinks present a technique that will help you take action when you need to.

In these blinks, you'll also find out

  * How five-second decisions had an impact on the civil rights movement;

  * what percentage of employees are waiting to talk to their bosses about needed change; and

  * how the five-second rule can help you stop worrying.

### 2. The five-second rule is a simple decision-making tool that can change your behavior. 

It was six in the morning on a dark winter's day in Boston when the author was rudely awakened by the sound of her alarm clock.

There were days when Mel Robbins greeted the morning with a smile, but this was not one of them. She was unemployed, burdened with money troubles and had a drinking problem. She'd also developed the habit of hitting the snooze button and delaying the day ahead for as long as possible.

But this particular morning was different; instead of the snooze button, Robbins began her day by counting down from five.

This is called _the five-second rule_, and it's used to prevent acting on detrimental urges.

So instead of rolling over and sleeping on, Robbins counted silently to herself, "five, four, three, two, one." This simple act distracts you from your anxieties and redirects your attention to what you should be doing, instead of submitting to instantly gratifying urges. By continually doing this, you can break a negative cycle and create new and better habits.

Robbins also used the five-second rule to stop avoiding exercise and to push herself outdoors for regular jogs. And when she felt like procrastinating on updating her résumé, it was, "five, four, three, two, one" ...and she got to work.

This tool is especially useful if you're the kind of person who sits around waiting for inspiration to strike.

In 1954, psychologist Julian Rotter coined a concept known as _the locus of control_. It explains the feeling people perceive of outside forces controlling their lives, and that those who feel more in control tend to be more productive.

We're often told to keep an eye out for opportunities and to take them when they appear. But the better advice is to assert control over your own destiny and create opportunities yourself.

Robbins calls this _the power of the push_ and the five-second rule might just be the push you need.

### 3. The five-second rule can unlock your inner courageous side. 

On a cold December night, in 1955, Rosa Parks was sitting on a city bus and refused to stand up and give her seat to a white man. This relatively small act of defiance was nevertheless a historic moment of great courage in the fight for civil rights; and it shows us that it doesn't necessarily take grand gestures to spark significant change.

This is the same kind of philosophy that makes the five-second rule effective. Counting backward from five isn't a dramatic lifestyle change, but it can push you toward being a more courageous person.

The actions of Rosa Parks led to another small decision that would change the course of history.

Four days after Parks was arrested, people began to organize a boycott of segregated buses, and they wanted a 26-year-old preacher to be the voice of their protest. This preacher would later write: "It happened so quickly that I did not have time to think it through. It is probable that if I had, I would have declined the nomination." That preacher's name was Martin Luther King, Jr.

Parks and King didn't consider themselves courageous people in their everyday lives, so their instincts weren't to fight against injustice. Yet that's what they did. They both encountered a moment when their instincts collided with their beliefs and goals and they felt the power of the push.

Most of us have instincts that tell us to play it safe and not be courageous. But the five-second rule can give us just enough time to move in the direction that can open us up to life's opportunities.

Each day presents a chance to move toward greatness or stick with a safe and mundane routine. If you want to live an exceptional life, you'll have to make the choice of taking five seconds to push yourself out of your comfort zone.

There's no reason to see the great people throughout history as any different than yourself. Rosa Parks was a shy and introverted woman and Dr. King struggled with self-doubt. They just pushed past these fears, and you can, too.

### 4. Stop waiting for the right time and start pursuing your dreams now. 

If you want to change the perception your coworkers have of you, all you need to do is raise your hand during a meeting and speak your mind. If you want to add some joy to someone's day, all you have to do is take a moment to compliment them.

All these acts require is for you to make a decision, which is something you can always put off by telling yourself that today isn't the right day. Eventually, this can be your life's story, forever waiting for the right moment to arrive.

So why not let this very moment be the one you're waiting for? Let the five-second rule be the tool that allows you to make the choices you've always wanted to make.

No one dreams of being a person who didn't do anything. But we tend to wait "for the right time" even though our perfect scenario may never arrive. According to a recent survey, 85 percent of professional service employees are keeping feedback from their managers because it isn't the "right time."

Even the most talented among us need a push to get on the right track.

Steve Wozniak, cofounder of Apple, was plagued with uncertainty after he and Steve Jobs were offered funding to start their own business in 1977. Wozniak wanted to hold off for a while and worried about quitting his day job until his friends convinced him to take the leap.

Wozniak certainly reaped the rewards the world has to offer to those who stop playing the waiting game.

The author E.L. James didn't wait for a lucrative publishing deal before writing the overwhelmingly popular _Fifty Shades of Grey_ trilogy. She was a working mother with a passion, and she created her own opportunity by self-publishing the book that she'd managed to write in her free time.

_Fifty Shades of Grey_ went on to sell a million copies in just four days, but it never would've happened if she hadn't taken the initiative.

### 5. You can’t control your feelings, but you can always control your actions. 

To many people, professional athletes are extremely inspirational. As they should be; athletes show us what's possible when you put your mind and body through rigorous training and practice.

Some pro athletes seem practically inhuman, but their ability to push past most people's physical limits is determined by one simple trait. Despite feeling too exhausted to continue, an athlete has the ability to separate from this feeling and continue to run or swim or cycle.

Athletes know that feelings are only suggestions and sometimes it's best to ignore those suggestions, especially when you're trying to reach a goal.

This is a good lesson to take to heart, since most of us base our decisions on feelings rather than goal-oriented logic.

According to neuroscientist Antonio Damasio our emotions are the deciding factor for 95 percent of our decisions. So rather than "thinking and acting," we generally "feel and act."

Part of Damasio's research involved brain-damaged people who were unable to experience emotions. Even though they could list the pros and cons of any given choice, they were unable to make decisions.

Damasio's work led him to believe that human beings aren't "thinking machines that feel," but rather "feeling machines that think."

Armed with this understanding of how much dominion your emotions have over your choices, you can put the five-second rule to work.

Instead of letting fears and worries stop you from fulfilling your dreams in life, catch yourself and take five seconds to make the decision that brings you closer to your goal.

This is what's known as psychological intervention, which dates back to Aristotle, who summed it up as: "Do good, be good." In other words, you have to change your behavior before you can change how you feel about yourself. The five-second rule is such a useful tool because it's focused on doing just that. It changes your behavior and provides you with the courage to overcome the psychological obstacles that stand in your way.

### 6. The five-second rule is a useful weapon in the battle against procrastination. 

There's an irony to modern technology like smartphones and tablets. They were designed to make us more productive, yet they often end up distracting us, and they've exponentially increased our ability to procrastinate.

But there's a difference between innocent procrastination and _destructive procrastination_.

When we avoid getting things done, even when we know that serious trouble will follow, it's destructive procrastination.

Experts used to think of all procrastination as being the result of bad time management and a lack of willpower or proper self-discipline. But it's now understood that procrastination isn't just laziness, but also a side effect of how we deal with stress.

According to Timothy Pychyl, a psychology professor at Carleton University, procrastination is a result of our powerful subconscious desire for instant gratification. Since procrastination offers an immediate, albeit temporary, relief from the stress of life, we're constantly drawn to it.

The stress we long to escape from isn't limited to meeting a deadline, either. Most of us face money or relationship issues that hover over us — it's just a fact of life. So putting life on pause to shop for a new pair of shoes is just a way of easing the pressure, just for a little bit.

To overcome the temptation of procrastination, use the five-second rule. Let it be your new, healthier habit.

Since the best way to complete a task is to "just get started," as Dr. Pychyl puts it, you'll want to start the five-second rule the moment you start to sense the urge to procrastinate or do something else.

Once you begin to use it for this purpose, you'll immediately begin improving your _locus of control_. After all, procrastination is just another way of giving up control. So, instead, start the countdown and reassert control over your life.

### 7. Don’t regret all that time spent worrying; redirect those emotions and start feeling grateful. 

There's no big mystery to why we're all so prone to worrying. As children, most of us were constantly being taught to worry by over-concerned parents telling us to "be careful" and "put a coat on or you'll catch a cold."

As a result, we've become adults who spend far too much time worrying about things that are beyond our control. And the fact is, we'll likely come to regret all this time spent sweating the small stuff.

Dr. Karl Pillemer is a Cornell University professor who has spent over ten years discussing the meaning of life with more than 1,200 senior citizens. Through these conversations, one thing has become clear: most senior citizens believe they've wasted too much time worrying.

Life is far too precious a thing to spend living in fear and anxiety, so before you regret another minute, use the five-second rule to take control and live life to its fullest.

Pay attention to your mood, and when you feel your mind begin to give itself over to worry, take five seconds to peacefully count down from five so you can reassert control.

As soon as you reach "one," ask yourself these two questions: "What am I grateful for in this moment?" and "What do I want to remember?"

Answering these questions will help you shift your focus away from the worry and onto the more uplifting and positive aspects of life. Remember, there are truly important and precious parts of life, such as your relationships and the things you are working on, so use the five-second rule to remind yourself of the big picture.

In any given moment, there are bound to be more things to be grateful for than to be anxious about.

### 8. Final summary 

The key message in these blinks:

**Nothing is set in stone. Your habits, mind-set and personality traits are flexible and subject to change. Once you realize this, your life can start changing for the better. To help facilitate this change, use the five-second rule, a simple tool that can help you adjust your "default" reactions by counting down from five. By changing the way we make decisions, this relatively small act can add up to redefine who you are, how you feel and what you do with your life.**

Actionable advice:

**Reframe your anxiety as excitement.**

Next time you're feeling nervous about something, such as a job interview or speaking publicly, don't tell yourself to "calm down." Instead, say, "I'm excited!" Anxiety is a state of physiological arousal and you can flip it around to make it positive instead of letting the fear pull you in. When you tell yourself that you're excited, it provides a valid positive alternative that allows you to stay in control.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Getting Things Done_** **by David Allen**

In _Getting Things Done_ (2001), David Allen introduces his famous _productivity system_, aimed at helping people work on multiple projects at once — and to do so with confidence, clear objectives and a sense of control.
---

### Mel Robbins

Mel Robbins is a television host and frequent commentator on CNN. She is also a motivational speaker and contributing editor for _Success_ magazine. Her previous book was the best seller _Stop Saying You're Fine_, which dealt with the psychology behind getting what you want.

