---
id: 55fffd1d615cd1000900009e
slug: the-8th-habit-en
published_date: 2015-09-24T00:00:00.000+00:00
author: Stephen R. Covey
title: The 8th Habit
subtitle: From Effectiveness to Greatness
main_color: 245AB3
text_color: 245AB3
---

# The 8th Habit

_From Effectiveness to Greatness_

**Stephen R. Covey**

_The 8th Habit_ (2004) helps you find your inner voice and thereby lead a more fulfilled life. Covey explains why we struggle to feel motivated and passionate (particularly in our working life) and how we can go about changing that.

---
### 1. What’s in it for me? Learn how to be the greatest you in your work. 

Consider your own voice. Not your singing voice or your speaking voice, but _your voice_ — the significance, the endless potential and greatness of _you_. Have you found it?

Finding your voice at work, whether you're a regular employee or a top leader, is not necessarily easy. And if you've already managed to find it, there's still no guarantee that you've managed to consistently use it. In _The 8th Habit_, the author gently pushes you to find your voice. He fills you in on how to take action and find it, and, importantly, how to inspire the people around you to find their voices as well.

In these blinks, you'll discover

  * how to develop the four intelligences that'll help you find your voice;

  * that freedom of choice allows you to unfold your true potential; and

  * the key to building mutual trust.

### 2. The 8th habit is all about finding a way to greatness in today's Information/Knowledge Worker Age. 

Ever feel like you don't make much of a difference? Or that what you do doesn't really matter? Many people seem to feel this way. But why?

Well, many organizations can't keep up with the constant changes of our modern world. And therein lies the problem.

We currently live in the Information/Knowledge Worker Age, the successor of the Industrial Age. Many organizations find it difficult to accommodate the shifting approaches to work that this new era gave rise to, and they still operate with an Industrial Age mindset, governed by a domineering top-down style.

This kind of control functions poorly in the current age, which is all about unleashing the potential of our workers.

Consider the value of quality in today's IT industry. An outstanding programer, for instance, is 1000 times more productive than an average programer. During the Industrial Age, such disparities between individual levels of productivity were impossible.

It's therefore necessary to abolish the controlling top-down approach, as it limits employee potential. We must produce the kind of quality we expect of today's businesses. 

Most importantly, employees should be encouraged to find their own voice. 

Everyone wants to be great at their job, but only those who find their voice and make a habit of using their strengths can enjoy true success. Finding that inner voice is known as the 8th habit. So how do you do it?

It starts with treating employees respectfully so that they can make their own choices, use their creativity and feel significant in their workplace. Everyone must be able to find their own voice and, in turn, inspire others to find theirs.

### 3. You can find your voice by using the gifts you were given at birth. 

Did it ever occur to you that your greatest gift is your freedom of choice? Each of us is born with this freedom, along with the gifts of intelligence. The combination of these gifts equips us to find our own voice.

Freedom to choose is precious. While we can't always control what happens to us, we _can_ always choose how we react to life's challenges. When used wisely, you can use it to broaden your horizons and regain control of your life.

The principle of freedom is applicable to every part of your life, including your work. For example, if your boss is treating you like a doormat, do something about it! It's easy to forget that you have the power to choose how you react — but you _do_. So, why not choose to talk to your boss directly about it?

Now let's look at the gifts of intelligence.

There are four kinds of intelligence, each of which we can improve and cultivate. By working on your intelligence, you'll discover your strengths, and that discovery will guide you to your voice.

_Physical intelligence_ allows our body to function and regulate itself without our conscious input. An example of this would be our heartbeat.

_Mental intelligence_ is our ability to think abstractly and analyze things, people and situations.

_Emotional intelligence_ enables us to empathize, so that we can communicate with and relate to others in an effective, friendly way.

Finally, _spiritual intelligence_ is something most people don't spend much time thinking about, but is of great value nonetheless. In fact, it's the very basis of the other forms of intelligence; it's the true north of our moral compass, the imperative that drives us to seek meaning in life.

### 4. To lead is to inspire others and help them find their own voice. 

Think about the leaders and managers in your organization. Are they effective? Are you yourself a good leader? Every organization has leaders — and yet, as we all know, some are far better than others. What exactly is it, then, that makes a good leader?

A good leader is able to play not one but four leadership roles.

If you're a leader who wants to help others find their voice and recognize their own potential, you must _lead_ your employees, rather than _managing_ and _controlling_ them _._ Doing this is central to any leader's purpose. Filling the four roles listed below will get you well on your way.

First, you'll need a vision and strategy that establishes a direction for your employees. Second, you'll need to set an example by executing your own ideas in a disciplined fashion. Third, be passionate about what you do so that through your enthusiasm you can establish a shared organizational culture. Finally, you should be able to manage and maintain structures inside your organization.

Now you've started down the path of leadership, but to become a truly great leader, you must also enlist your four intelligences.

We employ our intelligences in order to achieve individual greatness; a leader should apply the same paradigm to her company, a tactic that will make many common problems disappear. Think of the four intelligences as body parts of the company: if one is missing or neglected, it will undermine the structure as a whole.

Imagine a company that ignores the need for spirit — something like a shared organizational culture, for example. A dearth of communal spirit could lead to a lack of trust, which in turn could mean that some employees feel totally excluded and alone.

### 5. Don’t just sit around and wait for your old habits to change. Do something about it. 

Do you ever think, "I'm sure things will get better soon. I'll just have to wait and see"? If so, you're not the only one. It's not uncommon for us to sit around and wait for change. But things rarely change in this way. And as you have the freedom to choose, you're in the perfect position to rectify your attitude!

In order to discover your voice, you must choose to change your motto so that "wait and see" becomes "take action." 

Picture, for instance, that you despise your job because your boss is a total control freak and rejects all the ideas you have. You consider quitting; however, you can't afford to lose your job. So you decide to just grin and bear it from Monday to Friday.

But there is another way. 

You could acknowledge that you're not a victim. You have a choice. The vast majority of the time, you can choose to change something about your situation. All it takes is some initiative and responsibility. When you adopt this mindset and start using your voice, you'll see that even if your boss is your leader in a formal sense, you still have the power to make a real difference in your own job.

If something is really out of your hands, though, you can decide how to react to the situation. Sure, there are certain external things that you just can't control or influence. Say, for example, that your boss wants to hire someone who rubs you the wrong way, but your job position doesn't allow you to interfere with hiring decisions. Even in cases like these, you can still choose not to let it bring you down. And if there in fact is something you would like to change and that clearly is part of your role, then take the reins and do it!

### 6. Mutual trust is the key to personal and business relationships. 

You've heard it before — but for good reason: if you want to be successful, it's not only about having talent and drive but about who you are as a person. And a large part of how you measure up as a person is your ability to inspire trust in others. 

But how do you ensure that people trust you professionally and personally? By sticking to your word, being friendly and knowing when to say sorry.

The following simple rules will help you do these things. Once you start doing them consistently, you'll soon see what a positive impact they have.

First, be sure to always make good on your promises. If you are not 100 percent sure you can deliver, don't make a promise! Failing to follow through is the quickest way to lose other people's trust.

Next, being kind and friendly to others — simply saying "thank you," "please" and asking, "can I help?" — can make all the difference. Also, watch your words about people who are not in the vicinity, as gossiping is toxic and detrimental to instilling trust. Instead, use warm and friendly words. It will go a long way.

Furthermore, being able to say sorry is also incredibly impactful; a genuine apology can earn you back whatever trust you've lost in your less-than-admirable moments.

Remember, though, that trust is not a one-way street. Sometimes all you need to do is trust _another_ person. In fact, the verb _to trust_ nicely sums up what you're trying to do when you attempt to help others find their voice. If you genuinely trust someone, you show them that you notice their potential and their value. Granting them this trust will show them their own potential, too.

Building a foundation of trust and trusting others in your work and personal life will improve all of your relationships. And the great thing is, it's not hard: it just takes a bit of effort.

### 7. Being able to compromise is important when dealing with conflicts. 

Think of the last time you tried to resolve a conflict with someone. How did it go? Did you talk incessantly at each other or did you try to understand your interlocutor's perspective first?

It's sometimes easier said than done, but the best way to quell a conflict is to truly listen to the other person.

Most of us think we're already pretty good at listening because it feels like we do it all the time. However, there is a big difference between listening as we usually do and _empathetic_ listening.

Say you're trying to decide on a logo for your project and your colleague is presenting their point of view. In order to really listen, try to see what your colleague is seeing and why they are seeing it that way. Try to perceive it from within your colleague's frame of reference.

It's useful to bear in mind that conflict and misunderstandings are often caused by differences in semantics. All of us have our own way of using words and even a single word can hold different meanings and connotations for different people. Therefore, to better hear and understand your colleague, try to grasp what they mean when they use particular words.

After you've truly listened to your colleagues' idea, you can begin explaining your point of view and looking for a third alternative that fits for both of you. After both parties have offered their view, an ideal solution might simply crop up, as the conflict may have only arose because you didn't understand the other person properly.

Sometimes, however, a compromise is necessary. This doesn't mean that one side loses; rather, there should be mutual understanding and open-mindedness, the natural result of talking to one another and understanding differing opinions. This will make a third alternative far more palatable — maybe even make it the best solution from all perspectives.

### 8. All organizations should have a central value system that employees can align themselves with. 

What's it like in your company? Do all the workers know the core values of the organization? Are they aligned with them? If you're a leader and can't answer yes to these questions, your company might be suffering.

If your employees aren't familiar with the organization's core values, it's very likely that the business itself stands on a shaky foundation.

For example, imagine asking the CEO of a company about the values of her organization. She might mention that cooperation is a key tenet. Maybe there was even a company training that aimed to help employees cooperate more frequently and in a better way. But workers within the company don't seem to cooperate all that well. Upon closer inspection of the issue, you may discern that although the company's core value is cooperation, there's also a great deal of competitiveness, caused by a reward system for top performing employees. And these two values work against each other.

This kind of approach makes for serious misalignment and confuses employees. Some employees might opt for cooperation because it makes them happier and more efficient; others may choose to be competitive because it pushes them to achieve better results and promises rewards.

So how do you deal with this as a leader? Start giving feedback to ensure that your employees are aligned with the company's values.

Not a single person or organization is on track all the time, so don't fret. Don't punish your employees for this, but provide feedback that helps them get back on target and achieve the desired goal.

For instance, how about arranging a monthly team meeting? This would give you an opportunity to let your employees know what you appreciated that month, as well as what could be improved. It will give your employees the chance to offer feedback, too.

### 9. Empower your employees to find and use their passion and talent. 

Who controls the planning and evaluation of tasks in organizations? The leaders, right? This is often the case. But if leaders run an overly tight ship, they often do more harm than good, both to the staff and to the company itself.

Many workers aren't really passionate about their jobs, which usually leads to sloppy results. Employees lose motivation when they don't have enough freedom or responsibility. Usually the manager takes care of all the planning and evaluating; the workers are expected to do as they're told.

If, however, workers share responsibility and control with their leader, they'll start to feel more motivated and, as their full potential and talents start to bloom, eventually find their voices. When a leader cedes a little control, workers are empowered and supported, because they feel trusted.

Consider a cleaning company with a lot of unmotivated workers whose only task is to clean a large building daily. How could they be motivated? A simple idea to motivate them and help them find their voice is to allow them to make some important decisions — and then evaluate their success. Staff might try out different products, for example, such as different vacuum cleaners, and then evaluate which ones work the best.

Having a slightly more hands-off approach may not feel like the right thing to do, as leaders are traditionally the ones who hold the power. But the most important thing is to do the best work possible and inspire the workers to find their own voices in the organization. If you can do this, you, your employees and your company will all reap the rewards.

### 10. Final summary 

The key message in this book:

**If you want to thrive in any part of your life, you must find your voice and inspire others to find theirs. Leaders in business are in a prime position to do this, and by instilling trust, communicating effectively and relinquishing some control, they can benefit the entire company.**

Actionable advice: 

**Don't be afraid to give up some power.**

As a leader, try handing over some responsibility and control to your staff. When employees are empowered, have more say in their tasks and are allowed to make their own decisions, they grow both personally and professionally, which makes the company as a whole more effective.

**Suggested** **further** **reading:** ** _The Speed of Trust_** **by Stephen M.R. Covey with Rebecca R. Merrill**

_The Speed of Trust_ is about the importance of trust and how it can improve all aspects of our lives, from personal relationships to productivity in the office. Trust improves communication, and in doing so, speeds up efficiency and lowers cost at the same time. Throughout this book, the authors offer us tips on exactly what to do to increase trust in our lives.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Stephen R. Covey

Stephen R. Covey co-founded and served as vice chairman of FranklinCovey Co. Over the course of his life, he was awarded 12 honorary doctorate degrees and wrote several books, including the bestselling _The 7 Habits of Highly Effective People_ (also available on Blinkist), which Forbes listed as one of the top ten most influential management books ever. _TIME_ magazine also included Covey in their list of the 25 most influential Americans.

