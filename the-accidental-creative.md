---
id: 5a6f0753b238e10009732042
slug: the-accidental-creative-en
published_date: 2018-02-01T00:00:00.000+00:00
author: Todd Henry
title: The Accidental Creative
subtitle: How to Be Brilliant at a Moment's Notice
main_color: FEC933
text_color: FEC933
---

# The Accidental Creative

_How to Be Brilliant at a Moment's Notice_

**Todd Henry**

_The Accidental Creative_ (2011) explains how you can unleash and manage your individual creativity. The book posits that everyone has the potential to be creative, though many lack the tools to act on this creativity. This book describes how to achieve maximum creativity and provides you with techniques to help you create consistently and effortlessly.

---
### 1. What’s in it for me? Learn how to fill that creativity tank. 

You don't need to be working a creative job to be creative. Creativity is a part of everyone's life, whether you're a globe-trotting rock star or simply someone who enjoys a spot of watercolor painting on a Sunday afternoon.

Exploring your creativity can be incredibly fulfilling. However, pressures across different areas of your life — particularly in the workplace — can make it difficult to maintain that creativity.

These blinks will help you battle these pressures and unleash your creative potential. You'll learn how to ensure you're never short on creativity again.

You'll also discover

  * how to prove to your boss that you're a valuable employee;

  * why keeping FRESH will help your creativity; and

  * how the saying "you are what you eat" applies in a creative context.

### 2. The pressures and expectations of today’s workplace can stifle creativity. 

Creativity is a highly valued skill. Indeed, it's becoming increasingly necessary in more and more jobs. However, unlike traditional workplace skills, creativity fluctuates, and this can make it incredibly difficult for employees to deliver when their employer expects them to deliver the creative goods.

To prove to your boss that you're a viable creative asset, you need to be prolific, brilliant and healthy. Unfortunately, most of us are deficient in at least one of these areas, and because of that our work and reputation suffers.

Maybe you're prolific and brilliant, but you have an unhealthy approach to work that puts you at risk of overexertion and burnout. Or maybe you're both brilliant and healthy but aren't being prolific, which results in nobody wanting to work with you because you're not pulling your weight. Or maybe you're healthy and prolific but not brilliant, which puts you at risk of losing your job since an absence of quality in your work is irredeemable.

Another pressure of the modern workplace is that employees must be creative and innovative, while at the same time consistently meeting deadlines.

This expectation shows that most companies are more concerned with results than with the creative processes of individual employees. For example, a client of the author, preoccupied with whether her managers would approve her work, found herself ignoring her creative impulses and playing it safe instead. A CEO told the author that he calls this tactic _bunting for singles_ ; just as in baseball, it's safer to go for smaller "bases" at work than risk a strikeout by going for the bigger home runs.

Furthermore, companies also tend to focus more on predictable outcomes and less on giving room for irregular waves of creativity, a tendency that takes a toll on innovative thinking. A friend of the author said that the creative types at his company refer to the decision-makers as "vampires" because they suck all the life from the room.

As you can see, managerial expectations are often at odds with the needs of employees when it comes to creativity.

> _"In the effort to cut off the troughs we inadvertently cut off the peaks as well."_

### 3. Beware of the three “assassins” of creativity: dissonance, fear and expectation escalation. 

Often, we aren't aware when our creative process is under threat. If we learn to recognize the warning signs, however, we'll be better able to combat the three pesky "assassins" that try to sneak in and kill our creativity.

The first assassin we must arm ourselves against is _dissonance_, which usually arises when a company's purpose for action — "the why" of the company — is not aligned with the actions it takes — the company's "what."

Here's an example of dissonance: The author once worked with a design firm whose creative leaders didn't clearly express the expectations of the clients to the designers. Instead of a brief detailing the concepts underlying the client's requests, the designers were given vague cosmetic requests. This caused the work to suffer. When there's no "why" behind a particular "what," the result is usually confusion and misunderstanding.

The second assassin is _fear_, and it usually pops up when we're imagining the possible outcomes of an idea.

A particular experiment demonstrates this well. People were first asked to imagine walking along a twenty-foot plank on the floor, and most of them were confident they could do it without falling off. They were then asked to imagine walking along the same plank, but this time with it suspended between two buildings 100 feet above ground — and most of them didn't believe they could do it. In short, when the potential outcome of a particular action became frightening, people were less likely to think they could succeed.

Similarly, many people are dissuaded from undertaking a creative venture because they fear what _might_ happen.

The third and final assassin is _expectation escalation_, and it refers to the inflation of expectations, which often puts a damper on creativity.

When you fixate on the outcome of a project, you may fail to see different possibilities. The higher your expectations, the more you become consumed with realizing them, letting everything else fall by the wayside.

> _''The more opaque the decision-making process, the more likely that misinterpretation and misalignment will follow."_

### 4. The key to creative insights is focus. 

Now that we're familiar with some potential creativity killers, we can move on to practices that'll keep us engaged. The best way to do this is to stay FRESH — by attending to our _focus_, _relationships_, _energy_, _stimuli_ and _hours_. Let's begin with focus.

A lack of focus comes from two key factors: unhealthy assumptions and the _ping_.

According to neuroscientist Jeff Hawkins, unhealthy assumptions arise because our brains are constantly predicting what will happen based on our previous experiences. Though these assumptions are usually helpful, they can also be faulty.

For example, the author once found a spider in the garden shed he used as a home office. After that, he started spending one minute every morning checking for spiders. That's a total of six wasted hours every year — all because of something that happened once!

The second focuser destroyer, the ping, is the sudden and uncontrollable urge to divert your focus and respond to a new email or text message.

In order to fight the ping and unhealthy assumptions, you must do three things: _define_ and _refine_ your work and _cluster_ your tasks.

To define your work, the author suggests assigning four to six "challenges" to any task. The challenges can be anything, such as, "How can we make our brand stand out from the crowd?" or "How can we lower costs?" When you address these questions specifically, it's easier to see where you should be directing your focus.

The next step is to refine your work using _The Big Three_. Identify the three projects that are most in need of critical attention; this will inevitably guide your focus to these tasks.

The final step is to cluster similar tasks, such as meetings or project planning, and spend a minimum of 30 minutes working on each cluster. This exercise is similar to the retail practice of _intelligent adjacency_, whereby you place related products next to each other, like toothbrushes and toothpaste, increasing the chance of someone buying both at the same time.

Following these steps will have you managing your focus in no time. Next, we'll learn exactly where you should be focusing your newfound attention, starting with _relationships_.

### 5. Creative work can be lonely, so inspire yourself by forging strong networks and relationships. 

Creatives are usually introverted, since most of the time their work needs to be completed alone. But this doesn't mean creatives don't like being around people; it simply means that they usually derive their energy from themselves rather than from others.

Inevitably, introversion and isolation limit one's outlook and thus one's creativity, but this can be counteracted by the next part of FRESH: relationships. Author Steven Johnson says that the best ideas of all time were sparked by people sharing and collaborating. The more minds that are present, the more creative avenues will open up. In the same vein, professor Louis Cozolino said that we need perspectives from other people to keep our creativity fresh.

To build strong relationships, there are three main strategies: _start a circle_, _head to heads_ and _establishing a core team_.

First, you must regularly gather creatives of your choice in an informal circle. These can be friends or coworkers or both. Take turns discussing your current projects, what's inspiring you and what you would like advice on. Sharing your insights will be mutually beneficial.

The next step is to construct one-on-one meetings with other creatives, preferably with those who work in the same area as you. The aim of these meetings is to promote a healthy dose of competition between you and your partner that will push you both to be bold and innovative. The author sums this up with a quote by TV personality Diane Sawyer: "Competition is easier to accept if you realize it is not an act of oppression or abrasion," but rather a challenge to help us grow.

Finally, the core team requires a long-term student-and-master relationship. Seek the advice and counsel of two or three professionals whom you look up to — people outside your company and from different industries and positions. This will broaden your perspective and encourage learning.

Following these strategies will help you grow your network and build strong and healthy relationships. Next on our FRESH list — energy.

### 6. For your creative insights to gain momentum, you need to strategically manage your energy. 

Though one of the most important of the five key areas, effective energy management is often neglected.

Though the brain is only about 2 percent of our total body weight, it uses about 20 percent of available energy. Therefore, if we're tired, and low on energy, we won't be able to function properly.

But refilling your energy tanks isn't that straightforward; people wrongly assume that energy is a bottomless resource and that you simply need time to refill it.

Author Tony Schwartz says that if we practice good time and resource management but neglect our energy levels, our productivity will suffer. He adds that we're most productive when switching "between periods of high focus and intermittent rest."

So, to effectively manage your energy, you need to first manage your whole life, which should be done in weekly, monthly and quarterly segments.

During the week, try to balance out energy-consuming tasks, such as long phone calls, with buffers, such as listening to five minutes of music. Buffers can also be handy during the transition from work to personal life. You could, for instance, stop by a bookstore on your way home.

Each month you should take note of where your focus is needed. If you have a new project coming up, for example, you will need to readjust your personal commitments, and vice versa.

Quarterly, you can look at your patterns and priorities as a whole. Do you have some birthdays coming up, or perhaps an anniversary? When is the best time to start that big project that's been looming in the background?

The final step in managing your energy is to "prune" away the less effective activities in your life.

In vineyards, vine keepers are always pruning new growth to ensure that the older, fruit-bearing parts receive the necessary nutrients. This same cutting back process is applicable to energy management. Every month, identify the _least_ effective and most energy draining activity in your life, and then eliminate it.

When you're able to effectively manage your energy, you can be sure that your time will be used to its full potential. Once you master that, you can move on and see how to curate what you consume: stimuli.

> _"You can have anything you want, but you can't have everything you want."_

### 7. To maintain your creativity, you need stimuli that are challenging, relevant and diverse. 

We've all heard the saying, "You are what you eat," but did you know that this can also be applied to creativity improvement?

To maintain a healthy creative output, you need to make sure you receive a healthy intake of _challenging, relevant_ and _diverse stimuli_ — which is what the S in FRESH stands for.

Sure, pop culture is a good source for trends, but to expand your worldview and get creative, you need to look to academia and other more complex sources.

You also need to ensure that the sources are relevant — not only to specific projects and goals, but to the overall, long-term skills you wish to develop. One good daily practice is to seek out resources that encourage personal growth — such as, say, classes that teach a specific skill — and professional growth, such as trade magazines.

Remember to vary the stimuli you consume. Consider topics and opinions you normally wouldn't, and remember that breakthroughs usually come from unexpected places! A good example is Larry Keller of the advertising agency FKM, who came up with the idea of a steak restaurant as a fine-dining experience while on an art museum excursion.

To help you manage your challenging, relevant and diverse stimuli, there are three things you should do: _cultivate_, _process_ and _experience_.

Make a quarterly study plan and allocate 25 percent to areas where you lack information needed for your job; 25 percent to what will benefit you in a wider sense, like educational blind spots and deficiencies; and 50 percent to things you're curious about, like history or gardening, focusing on your passions, not your obligations. Then take notes on your insights, review them and see if a pattern emerges. You can do what Founding Father John Adams did and fill in the margins of books with your notes, as if reading was a conversation and not a monologue.

But keep in mind that, though it's great to study and take notes, you should get out there and experience the world. Go to museums and lectures, take walks — it all adds to your creativity!

And now we've reached the final key to staying creative — managing your hours.

### 8. Great creative work is not defined by the number of hours but by the quality of those hours. 

The final area of FRESH, your hours — that is, time — is the most significant pressure point for many people because it is the most tangible resource.

Look at your time like a portfolio, not a slot machine; aim for quality over quantity.

Time management guides are usually only concerned with you getting _through_ your work. This resolve bears resemblance to playing the slot machine, where you pull the lever over and over again in the hopes of a win that may never come. But that doesn't work if you're a creative.

What you need instead is something that enhances your routine in the long term. This is known as the portfolio investment, and there are two main steps to it.

The first step is to establish your idea. Most people, when they hear the word "brainstorming," think of a group activity. However, this mental exercise also works just as well solo. Note that at this stage we are neither considering current designs nor concerned with execution; right now, it's only about coming up with new ideas. To start off, try giving yourself one hour per week to conceptualize new ideas. Look back at your _Big Three_ for inspiration, and define your problems as _challenges_, as we did earlier. Make sure you keep a record of everything that happens in each brainstorming session.

The second step is to practice _unnecessary creating_, which refers to creating for the sake of creating, and not out of obligation to anyone else. It will get your brain in motion and actually clear your head, allowing you to be more productive in other areas.

For example, a creative director named Robert told the author of the stresses and pressures of working for a large brand-design firm. The author recommended that Robert rediscover his old hobby of watercolor painting. In the following weeks, Robert regained his enthusiasm for work, all because he started doing something for himself.

You may feel like you don't have time for fun activities anymore, but take the author's advice and try to incorporate them into your routine. You'll be sure to see the benefits.

### 9. Final summary 

The key message in this book:

**By incorporating these practices into your life, you'll release your creative capacity naturally and easily, rather than struggling to hang onto it under the pressures of the workplace. To optimize our creativity, we need to keep FRESH and attend to our** ** _focus, relationships, energy, stimuli_** **and** ** _hours_** **. Aim for long-term development, not short-term outcomes, and stick to the goals you set for yourself!**

Actionable advice:

**Start today!**

Whatever it is that you wish to achieve, make sure that you give it all you've got each and every day. If you don't, you'll regret it in the end. A friend of the author once said that cemeteries are the most valuable land on the planet, as they are filled with unquantifiable amounts of unrealized potential and ideas. If you employ the practices outlined in these blinks and strive to consistently maximize your potential, you can be sure to "die empty."

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Accidental Genius_** **by Mark Levy**

_Accidental Genius_ (2010) outlines techniques, ideas and exercises that utilize _freewriting._ It's a method that many people use to organize their thoughts, solve problems and access the great ideas buried in their minds. The techniques and tips detailed here can be used to achieve better concentration, bring order to disorder and free up creative capacity.
---

### Todd Henry

Todd Henry is an international speaker, coach and author, as well as the founder and CEO of Accidental Creative, a consultancy firm that promotes personal development and creativity. In 2006, he began _The Accidental Creative_, a highly successful business podcast, before publishing his ideas in book form.

