---
id: 5746ddf9b49ff200039c4dcc
slug: the-accidental-superpower-en
published_date: 2016-06-02T00:00:00.000+00:00
author: Peter Zeihan
title: The Accidental Superpower
subtitle: The Next Generation of American Preeminence and the Coming Global Disorder
main_color: 2A71A3
text_color: 2A71A3
---

# The Accidental Superpower

_The Next Generation of American Preeminence and the Coming Global Disorder_

**Peter Zeihan**

Today, the United States has a stronghold as _the_ global superpower, but the world is changing at a historically unprecedented rate. These blinks outline the reasons the United States came to politically and economically dominate the planet, and what we can expect in the coming decades, both in the United States and the world at large.

---
### 1. What’s in it for me? Look into the future of world politics. 

Should the United States be the world's police force? This question divides opinion, but one thing's for sure: throughout the last 70 years, the US has seized upon the role.

These days are drawing to a close, though. In the near future, the country will decrease its commitments and withdraw from its massive involvement in world politics. That's partly because, thanks to their growing shale oil industry, the US no longer depends on imported fuel, which was its main incentive to maintain safe naval routes and a stable Middle East.

But what about the world? How will it fare without US interventions? Before we answer these questions about the future, we'll go way back in time to explore the origins of superpowers, from the Egyptian empire to the United States. You'll understand why there's a rather gloomy, if thought-provoking, prognosis. And it's one you won't necessarily agree with.

In these blinks, you'll discover

  * why traveling might become really difficult in the next few decades;

  * how the Mississippi River contributed to the power of the United States; and

  * why another great European war isn't out of the question.

### 2. Empires rise or fall depending on their geographic and geopolitical position. 

How did the ancient Egyptians build an empire that dominated their regions for hundreds of years?

Let's start with geography. An empire or nation flourishes both culturally and economically when it has an advantageous geographic position. Before widespread globalization, nations were heavily dependent on the natural resources they had nearby, so regions with forests, rivers and oceans were more prosperous. Communities in harsh, mountainous, desert-like, or snowy areas struggled just to survive.

That's why so many ancient empires were situated in regions with abundant food and water. A surplus of food and water allows a community to trade, grow their wealth and focus more effort on developing their cultural life and military power.

In ancient Egypt, for example, the Nile provided them with a constant water supply and easy trade routes; the surrounding desert kept the community together while serving as a barrier to outsiders. Meanwhile, artistic and cultural development was a central focus. The government forced people to build huge monuments attesting to the greatness of the pharaohs.

Another important factor in how a nation gains power and influence is an advantageous geopolitical position, that is, how their relations with other nations was influenced by geographic factors. After all, it's difficult for a society to dominate a region if they're surrounded by competing societies, as was the case in modern-day Germany and France. That's a big part of the reason why the Egyptian empire was so powerful for so long — enemy nations rarely challenged them because doing so would mean crossing the sea or the desert.

These geographic and geopolitical factors are also apparent in the rise of other ancient empires, like the Ottoman and Roman empires. All in all, an empire's power rested largely on factors outside their control, like fertile soil and natural barriers. The same holds true for the modern United States.

### 3. The United States' supreme global position is largely the result of its geopolitical situation. 

The United States is now an economic, political and military powerhouse. Why? Plenty of luck.

The United States has a great geopolitical situation. Its borders have been mostly protected from neighboring populations for hundreds of years. While European powers fought each other for centuries across the Atlantic, they ignored the United States, which became more and more organized and stable.

The most important geographical factor in US global dominance is its _waterway network_, however. The United States has 12 interconnected rivers, including the Mississippi, which is the longest navigable river in the world — a perfect route for trade.

These waterways allow for a lot of trade, which bolstered the nation's economy. The country's fertile land, ideal for growing large amounts of crops, also brought the country great wealth and more goods to trade.

And of course, a wealthy nation can build up its military, which gives it more sway in the global political landscape.

The United States was also careful about addressing outside political threats early on. The US bought Alaska in 1867 and annexed the Hawaiian Islands in 1898, so it already had defenses against dangers from outsiders before entering the Second World War.

After the war, the creation of NATO brought even more protection to the United States, as countries like the Faroes and Cyprus could serve as launching points for American troops into Europe.

Today, no nation on earth possesses the manpower, industry and naval power necessary to conquer the United States. It's in a very secure and comfortable geopolitical situation.

### 4. The United States is considering abolishing the policy that brought them global supremacy. 

In 1944, the United States did something unheard of. After proving military and economic power during the war they proposed the _Bretton Woods Agreement_, which was aimed at reshaping the world in the wake of World War Two.

It ushered in a new era of globalization and allowed the US to establish a degree of global economic stability between the former Allies. The agreement contained several important points. It established a network of free trade and introduced a shared monetary system based on the value of the dollar, which in turn was linked to the price of gold. The agreement also granted the US Navy safe travel on trade routes all around the world.

The United States' overall global influence increased tremendously. As a result, the US could manage international trade and help foster growth in potential new allies, like China, Japan and Germany. Plus, the American military gained the right to station itself all around the globe.

Overall, the effects of the Bretton Woods Agreement were so widespread that some countries redesigned their entire economic system around it. For example, free, safe trade meant some countries could reorient much of their production toward exports and start relying on imported raw material. They relied on American supremacy, then, to keep import/export routes safe and open to keep their economies stable.

But the days of the Bretton Woods Agreement are coming to an end. As it turns away from global trade and is less and less interested in protecting other nations' trade routes, the US is reconsidering upholding the agreement.

If it does pull out of the agreement, the US will soon return to its status as a superpower acting mostly independently from other countries.

> The US Navy costs around $150 billion annually.

### 5. Demographic changes will destabilize many currently powerful economies. 

Humans are living longer than ever in the modern era, thanks to a high level of nutrition and excellent medical care. That's great, but it also means that global demographics are shifting wildly and creating problems we've never faced before.

In most modern nations, the proportion of older citizens is growing very rapidly, meaning governments have to find a way to take care of them.

In Japan, for instance, one-third of the population is already over 60. People rarely have children after a certain age, so if a large percentage of the population is over that age, the trend is unlikely to reverse. Governments in industrialized nations, like Japan's, now have to support an unprecedentedly high percentage of older citizens.

This demographic change is going to cause a lot of strong economies to struggle. An aging population, of course, means there are fewer people to serve in the labor force and contribute to the nation's wealth. So as the Baby Boomer generation continues to phase out of the workforce, the next generations will shoulder the responsibility of supporting their pensions and emergent health care needs, while also keeping the economy running.

The problem is that these following generations aren't as large as the Baby Boomer generation. Generation X — people born between 1960-1980 — is 25% smaller, to name just one. These generations won't be able to generate as much capital as their parents and grandparents did, and therefore won't have the capital to buy what the economy is producing. That will lead to chronic economic slowdowns around the world.

Even countries with large surpluses will struggle to handle this economic transformation. About 30 of the world's largest economies will reach the limits of consumer market growth within the next decade. These countries include China, the United Kingdom, Germany and Norway. That will result in a decrease in exports, and a lower standard of living for the sectors of the population that can no longer support themselves.

Technology development will slow down too, as there will be a shortage of capital and resources to conduct research.

From this perspective, it's hard not to feel like we're on the brink of a new era of unprecedented change and disorder.

### 6. Europe as we know it will cease to exist and may even erupt into war. 

As we've seen, the world is changing faster than ever. Globalization, demographic shifts and climate change tell just part of the story. What does all this mean for Europe? It's going to change completely.

First, the demographic changes we learned about in the previous blink will chip away at European countries' productivity.

Second, the European Union is falling apart. EU leaders are increasingly quarrelsome when it comes to managing the euro, for example, while countries like Greece struggle to recover from recent financial crises. Germany and France remain on the fence about bailout programs, and the United Kingdom seems poised to move away from the EU alliance entirely.

We've become accustomed to thinking of Europe as a peaceful and prosperous place, but it's only been 70 years since European countries nearly destroyed each other in World War Two.

Germany is going to become a powerful player in the region again. The country is now very export-oriented but since the United States looks ready to abandon the Bretton Woods Agreement, Germany will soon have to deal with the consequences.

That means it'll have to find other ways to get crude oil and metal, and maintain control of political and economic trends in Europe. Germany isn't a superpower at the moment, but it also wasn't a superpower just a decade before the war!

And there could be another war on the horizon between Germany and France. The two countries have a long history of waging war and competing for domination in the region. If the EU collapses and Europe falls into disorder, their old rivalry could re-emerge.

> _"In short, Europe is going to be a mess..."_

### 7. The United States will continue to thrive and forge new alliances as other nations fall apart. 

Europe and Japan are poised to face serious problems in the coming decades, and we've just seen that Europe may even fall into war. Does that mean the United States will fall apart too?

Doubtful.

The US will remain strong because it depends very little on food, technology and energy resources from other parts of the world. The American market will remain stable because the US will fare much better than other Western countries in the face of demographic change. Why? Well, partly because of immigration and partly because the country still has a lot of potential to grow more domestic, internal markets.

Its location is also an advantage, as the Navy can easily protect its trade routes. Plus, if the Bretton Woods system falls apart, the United States won't have to spend as much on military power anymore, as the country will no longer be responsible for protecting international trade.

Other countries will be desperate to align with the US which will have its pick of potential alliances — some of which will be new. There will be new opportunities to increase global influence, particularly in Southeast Asia. Southeast Asia has a young, urbanized and relatively cheap labor force that could be extremely valuable to the US.

Lastly, if Iran gains control of its neighbors' oil supplies after the fall of Bretton Woods, it could still become a very profitable ally for the United States. Iran is in a good position to control the surrounding region and if it became the United States' ally, the United States would no longer have to consider it a military threat.

> _"Simply put, the world is indeed going to hell, but the Americans are going to sit this one out."_ ** __**

### 8. The weakening of the EU and the withdrawal of US overseas power will transform migration and foster terrorism. 

It's fairly easy for Westerners to travel the Western world these days. Trade agreements and the EU allow people to move from country to country looking for work.

This means more and more people are not living in the same place they were born. Without Bretton Woods and the EU, however, migration will become much more difficult, dangerous and expensive. Travel will become a privilege for the highly skilled or wealthy.

We'll see an increase in travel restrictions as more countries begin requiring outsiders to apply for residency permits and expensive visas.

When the United States stops guarding the naval lanes, other nations or the travel industry will have to step in to ensure naval safety, which is very costly to maintain. Individual travel costs will increase as a result and few people will be able to afford to go overseas.

Only a small number of elites will be able to move abroad, or escape from distressed countries like Syria or Greece. Moreover, fewer countries will be desirable as relocation destinations. The US will be one of the last. The brain drain of people privileged enough to travel will make things even worse for the countries they flee.

Certain countries, like Pakistan, will also see an upsurge in terrorism once the United States withdraws its power. Pakistan has a great deal of internal conflict, and the US has been paying it considerable sums to protect US routes to Afghanistan. US bribes amount to roughly 8 percent of Pakistani GDP, and the US also provides cheap loans and military equipment.

When the United States withdraws this support, it will weaken the Pakistani state even further and make it harder for it to control militant groups like Al Qaeda.

### 9. Final summary 

The key message in this book:

**Today, the United States dominates the globe both politically and economically, but we're on the brink of some major changes. The US will withdraw its protection of global trade, and demographic and political shifts are going to weaken industrialized areas like Europe and Japan. Migration will decrease and instability will rise, but the US will seek out new allies and remain a superpower thanks largely to its unique geopolitical position in the world.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _World Order_** **by Henry Kissinger**

_World Order_ (2014) is a guide to the complex mechanisms that have governed international relations throughout history. These blinks explain how different countries conceive of different world orders and how they are held in balance or brought into conflict.
---

### Peter Zeihan

Peter Zeihan is an expert in geopolitics, the study of how geographical location impacts economic, cultural, political and military developments. His work has featured in _Forbes_, _Bloomberg_, the _New York Times_ and the _Wall Street Journal._

