---
id: 55f6e27d29161a00090000a2
slug: the-achievement-habit-en
published_date: 2015-09-16T00:00:00.000+00:00
author: Bernard Roth
title: The Achievement Habit
subtitle: Stop Wishing, Start Doing, and Take Command of Your Life
main_color: F5C733
text_color: 80681B
---

# The Achievement Habit

_Stop Wishing, Start Doing, and Take Command of Your Life_

**Bernard Roth**

Big dreams that never come true? Long list of things to do but you spent half the morning on Facebook? _The Achievement Habit_ (2015) is all you need to light a metaphorical rocket under your desk chair. With its down-to-earth revelations and simple instructions, it's easy to apply to the life you already have, and bound to get you closer to the life you want.

---
### 1. What’s in it for me? Discover the key to becoming a true achiever. 

Some people seem to have an almost eerie ability to get things done and reach their goals in life. What is their secret? Are they aliens?

Nope, and they don't have achievement written in their genes, either. Anyone can learn to be an achiever. These blinks are about learning to see that the obstacles to achievement are often just excuses and reasons we make up in our heads.

These reasons and excuses get in the way of changing the actual problem: our behavior. As a result we end up making the same mistakes over and over again. Luckily, as these blinks will show you, by learning to see these reasons for the illusions they really are, you can truly start changing your behavior and take the first step to becoming an achiever. But what are the next steps? These blinks will show you.

In these blinks, you'll learn

  * what Oprah learned from losing her first job;

  * that failure is just a word; and

  * why networking is overrated.

### 2. The Achievement Habit is about closing the gap between what you want and what you do. 

Anyone can become an achiever. But first, you have to learn how to just _do_ things. 

Think about how many people dream of starting their own company, but end up working for someone else. Well, you can learn to close the gap between what you want to do and what actually gets done. 

Look at it this way: there's a fundamental difference between trying and doing. 

Imagine you want to take something from someone's hand: merely _trying_ gets you bupkis — the other person sees you reaching and strengthens their grip. In this case, you didn't use enough force and you didn't act quickly. If, instead of trying, you had simply _done_, the other person wouldn't have had the time to resist you. 

The point is, the only obstacle is you. Everything else is just an excuse or a mental limit. If you're not happy with some aspect of your life, you can change it. 

Because here's the thing: life is basically an exercise in problem-solving — and that's not a bad thing. Problems are opportunities to learn and move forward. And learning how to use problems to your advantage is exactly what the Achievement Habit is all about. 

One important aspect of the Achievement Habit comes from _Design Thinking_, a set of practices designers often use in their work, namely an openness to failure.

Failure defines pretty much every success story. Even Oprah was fired from her first job as a television anchor. But failure didn't stop her, it motivated her! 

Of course for most of us, motivation is the hard part. School trains us to work in order to earn high grades and degrees, but there are no report cards in life. So in adulthood, most of us have to train ourselves to be self-motivated.

> _"[Achievement] is a muscle, and once you learn to flex it, there's no end to what you can accomplish in life."_

### 3. To become an achiever, stop assigning a fixed meaning to everything in your life. 

We assign meaning to everything. Whether we're thinking about an incompetent boss or home ownership, we tend to forget the fact that our perception is subjective. Meaning isn't fixed; it's a matter of perspective.

For instance, your incompetent boss might wow you tomorrow with some breakthrough idea. And you yourself are similarly capable of reinvention. You might want to say it out loud: no one is just one thing, not you, your boss, your spouse, your children or your parents. 

And if you can get over the impulse to assign a fixed meaning to everything, you can start to channel failure more productively. Because failure is only a catastrophe if that's what you call it. No one's keeping a scorecard of your failures and successes — only you can give your life meaning. It's an empowering truth: you have control over your own experience. 

The author realized this fact when two family friends were diagnosed with Alzheimer's. The family of the first patient tried to stay positive and have fun despite the disease; they always looked forward to visiting their relative. The other family positioned themselves as victims of a horrible fate and were consumed by loss; visiting their loved one was always painful and uncomfortable. 

As you can see, the meaning these two families assigned to the disease had a huge impact on their experience of it.

You can become an achiever by using this insight to stop labeling the world. After all, since labels are fixed, they just stand in the way of achievement. 

Imagine losing your job. Calling yourself a "loser" would be pointless, since that's just a meaningless label and one that prevents you from moving past the setback.

### 4. Reasons are just excuses to avoid making decisions and changing your behavior. 

We always seem to have reasons for everything. Imagine you held the door open for an old lady in the morning. Then later in the day, you win the lottery. You might tell yourself that you were rewarded for doing a good deed. But really, your behavior had nothing to do with it. The relationship between those two incidents is correlative, not causal. 

You might understand that fact on a rational level. And yet, chances are, whenever you're late to a meeting, you probably hunt for some external reason — like traffic on the interstate. But in reality, you _knew_ there would be heavy traffic on your route. You're late simply because you didn't make the meeting a priority. 

The problem is, blaming external factors prevents you from actively making decisions and changing your behavior. Making decisions is tough, but can be easier thanks to a method called the _Gun Test_. 

Whenever a big decision comes up in the workplace, for example, start by laying out the pros and cons for each option. Then point your finger at whoever has to decide and give them 15 seconds. This rapid-fire approach works because chances are, they've already decided — they're just reluctant to commit. But additional reasoning probably won't change the outcome or lead to a better decision, so it's better to just make the decision and move forward.

After all, if you lay out the typical "life journey" — asking what will happen as a result of a decision — you'll realize that pretty much all roads lead in the same direction: children, house, death. But you can't possibly predict all the other things that'll come your way. That's why you should adopt a bias toward actions and a tolerance for failure.

> _"Many reasons are simply excuses to hide the fact that we are not willing to give something a high enough priority in our lives."_

### 5. The best way to solve a problem is by reframing it. 

We often think the road toward some desirable destination is blocked by lack of money, other people, or something else. But as we've suggested, the best way to overcome a perceived obstacle is by reframing our perspective on it. 

Of course, for this to work, you have to be dealing with the actual problem and not something else. Let's say you think you're looking for a spouse. But is it possible that what you're actually looking for is companionship, and that finding a spouse seems like the easiest way?

So, once you've determined the true nature of your problem, searching for a solution might seem like the natural next step. But before you do that, try reframing the problem.

For example, a friend of the author complained that he couldn't sleep because his bed was broken. He tried solving the problem by fixing the bed one way after another, but each failed. That's when he asked himself, "What do I actually want? To repair _this_ bed or to have a good night's sleep in any bed?" That's when he realized that his problem had nothing to do with the old broken bed. Since he wanted to sleep well, he just went ahead and bought himself a new one. 

Similarly, sometimes obstacles are just a matter of language. If you change the way you describe something, it might cease to be an obstacle.

This method is as simple as swapping out the word "but" for "and." If you say, "I want to go to the movies, _but_ I have to work," you've created a conflict that may not necessarily exist. Instead, you can phrase it thusly: "I want to go to the movies _and_ I have to work."

Along the same lines, saying that you "want to" do something will motivate you much more than saying that you "have to" do something.

> _"Often the things we strive for only represent more of something we already have: money, fame, appreciation, love."_

### 6. Achievement is rarely solitary: Find ways to learn from others and share your own knowledge. 

Chances are you won't be able to achieve anything on your own. You'll need help, so seek out people whose accomplishments match your goals. 

You don't have to follow their path, but you can learn from them. And don't just focus on the positive attributes: paying attention to negative qualities is also productive. 

To that end, consider Gandhi: he was an awe-inspiring teacher, but a bad father. His wife and sons reportedly felt that he never really cared for them. So you can view Gandhi as a role model teacher, but take his personal life as an example of something you _don't_ want for yourself. 

Taking a step back, when we talk about learning from others, it's easy to slip into networking mode. But instead of keeping everything on the professional surface, you'd be better off sharing your ideas earnestly and looking for true friendship. 

The point is, if you want to have a fulfilling life, seek out genuine human relationships instead of using others to climb the ladder. Get to know the people around you. Also, there's no reason to fiercely guard your ideas and resources — share your knowledge with others! 

Unfortunately the working world is often at odds with true cooperation and creativity. But you can overcome those norms by changing the physical conditions around you, like having people sit in circles during meetings. Start moving toward a flat hierarchy to facilitate open and creative exchange. Create space for play, even during a serious conference. Taking these steps will make the final outcome better for all involved. 

You can also establish a positive group feedback and discussion system using a _1) "I like," 2) "I like," 3) "I wish," structure_. This means that each person notes two things they really appreciated about someone's performance and then adds (without a "but") one point of constructive feedback.

### 7. If you want to achieve great things, start identifying as an achiever. 

What do you think of yourself? Are you an achiever? Well, you should start calling yourself one. Because ultimately, achievement is a matter of self-image. Meaning, if you start identifying as an achiever, chances are you'll start achieving great things pronto. 

As children, we construct our self-image based on what our parents and teachers tell us. But as adults, it's important for us to truly own our self-image. 

To understand the difference between how you see yourself and how others perceive you, ask five friends to jot down five characteristics of your personality. Compare their responses to your own list: you'll see some variety but also a lot of overlap. That overlap will include some characteristics you'll want to retain and others you'll want to change. 

It's also important to understand your intentions. What do you want to achieve? Ask yourself what you would do if you only had ten minutes, ten hours, and so on. That way, you'll be clear on what you really want. 

Identifying your life goals is trickier. Because although you need a general sense of what you want out of life, you shouldn't be too rigid about your path. 

For instance, you don't have to stay within the prescribed bounds of your profession. It won't be easy to take an untraveled route — especially when family and society expects something else of you — but that needn't deter you. There'll be failure on any path you take, so don't let that get in your way.

It's worth noting that you can always leave the path you're currently traveling on if it stops making sense. To make sure you're on the right track, just keep asking yourself, _Who am I?_ and _What do I really want?_

There you have it! Now you're ready to take control of your life and start achieving your goals.

### 8. Final summary 

The key message:

**There are no "losers" or "lucky people." Achievement can be learned when fixed labels are the only thing holding you back. So to start getting what you want, rethink your goals and the obstacles in your path. And then start doing something about it!**

Actionable advice:

**Stop thinking about who's wrong and who's right.**

Playing the right-and-wrong game is a waste of time. It just makes us angry and it doesn't produce anything useful. Since "right" and "wrong" are just a matter of perspective, you lose from the moment you start playing the game. So instead of clinging to false certainty, accept the fact that different people can have different opinions.

**Suggested further reading:** ** _Creative_** **_Confidence_** **by Tom and David Kelley**

_Creative_ _Confidence_ shows us the amazing value and impact that creativity has in our everyday lives. In fact, being able to think creatively can increase your happiness and success in both your professional and personal spheres. Luckily, artists and musicians don't have a monopoly on creativity. With the right techniques and mind-set, anyone can think creatively.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Bernard Roth

Bernard Roth is a professor of engineering and the academic director of the Hasso Plattner Institute of Design at Stanford University. He is one of the world's pioneers in robotics and an expert in kinematics, the science of motion.

