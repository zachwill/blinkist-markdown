---
id: 56e6dc119854a500070000d1
slug: the-adhd-advantage-en
published_date: 2016-03-15T00:00:00.000+00:00
author: Dale Archer, MD
title: The ADHD Advantage
subtitle: What you Thought Was a Diagnosis May Be Your Greatest Strength
main_color: 5A79A3
text_color: 4D688C
---

# The ADHD Advantage

_What you Thought Was a Diagnosis May Be Your Greatest Strength_

**Dale Archer, MD**

_The ADHD Advantage_ (2015) provides new insights into ADHD, debunking false assumptions and unveiling the positive sides of this condition. These blinks explore how anyone with ADHD — children, young people and professionals alike — can be nurtured and supported to reach their full potential.

---
### 1. What’s in it for me? Overcome your prejudices about ADHD. 

Since it was introduced as a diagnosis in 1994, over 6 million Americans have been diagnosed with Attention Deficit Hyperactivity Disorder, or ADHD. During the last decade, diagnoses have skyrocketed. So what's really behind this trend? Are all these people truly "sick"? 

Although more and more people "have" ADHD, it's still stigmatized, its most common symptoms are seen as obstacles to a normal life. As a result, vast quantities of drugs are prescribed every year. Though manufactured to ease the symptoms of ADHD, these drugs can sometimes have serious, even deadly, side effects.

It's high time we drop the stigma that surrounds ADHD. Because, as you'll discover in these blinks, the list of successful people with ADHD is as varied as it is long, showing that there really can be an ADHD advantage.

In these blinks, you'll learn

  * how ADHD helped our ancestors survive;

  * why a tendency to forget can be an advantage; and

  * why there is no Richard Branson without ADHD.

### 2. ADHD is seen as a terrible affliction and is extremely overdiagnosed. 

ADHD is often considered a serious medical epidemic that's currently sweeping America. Being diagnosed with ADHD is viewed as a curse that can break up relationships and tear families apart.

However, all is not as it seems when it comes to this disorder. 

The reality is that ADHD is massively overdiagnosed. There are, of course, clinical cases of ADHD that should be treated as such. But the number of misdiagnoses is shocking. Research has revealed that as many as 1.1 _million_ children and young people in the United States have been diagnosed inappropriately.

Why does this happen?

Well, the reasons are fairly easy to pinpoint. For one, the criteria used in diagnosing ADHD are poorly formulated, and based on the assumption that cases of ADHD are black-and-white. If an individual exhibits five out of twelve possible ADHD symptoms, they don't have ADHD. But just one additional point leads to a positive diagnosis. This leaves no room for the fluctuating and diverse nature of the condition. ADHD occurs on a spectrum, and so should be diagnosed according to a continuous scale. 

On top of this, the list of symptoms itself is flawed. ADHD symptoms are based on subjective observations by parents and doctors. Evaluative statements such as "Is often easily distracted" or "Often fails to pay close attention to details" are based on how a child's behavior appears to the observer, which often makes diagnoses unclear and uncertain. Moreover, some symptoms don't belong on the list at all. Hyperactivity — typically associated with ADHD — is something exhibited by nearly every child, and most grow out of it. 

Finally, there are far too few ADHD specialists. There are only about 8,300 of them in the United States. Compare that with the number of family doctors — roughly 54,000 — and you see the problem. Because of this, family doctors often make diagnoses, and their lack of experience in the field makes a misdiagnosis all the more likely.

### 3. Wrongly prescribed ADHD medication wreaks havoc on the lives of young people. 

The dangers of ADHD misdiagnoses become clear when we consider how many children are inappropriately prescribed medication. Between 1994 and 2010, this number grew to 800,000. How did this happen?

Financial pressure from drug manufacturers is one reason. Pro-drug medical research into ADHD is often funded by manufacturers — some of whom market their drugs directly to children. One manufacturer even subsidized comic books about the benefits of medication. 

And yet, these benefits are often outweighed by the dangers they pose. ADHD medication is addictive and all too easy to abuse. Consider the case of 24-year-old medical student and class president Richard Fee. He got hooked on Adderall and persuaded his doctors to keep increasing his dosage, despite his obvious addiction. Tragically, Fee hung himself two weeks after his last prescription ran out.

And there are other dangerous physiological side effects, too. The National Institute of Drug Abuse confirmed that, on average, children aged 7-10 grew 2 cm less and weighed 2-7 kg less than kids not taking ADHD medications. 

Some argue that prescribing medication is the only option. Without it, children with ADHD are unable to focus in school and get the education they need. But this isn't quite true. Children with ADHD may learn differently, but with a little experimentation we can help them thrive in the classroom. 

For example, ADHDers learn better in short bursts. Shorter school periods would help these students learn more effectively. If this isn't possible, teachers could assign them the role of "project manager," allowing them to check-in with and focus on groups of students at short intervals. 

It's also proven that ADHD kids learn much better after exercising. Physical activity calms their busy minds and allows them to focus. Beginning the school day with a 20-minute dance class or team game could help enormously. Or teachers could let students with ADHD stand up in class while they work, or walk around the room as they solve problems.

### 4. Think about ADHD from a different perspective to see its advantages. 

A most crucial step in managing ADHD more effectively is _changing your perspective_. Rather than seeing ADHD as a disadvantage, look at its symptoms as _advantages_. 

In this way, difficulty focusing on one task can be seen instead as the _ability to multitask_. ADHDers' minds are constantly shifting from one idea to the next — an ability that allows them to keep track of several tasks simultaneously. This is incredibly useful in both business and professional sports (more on this in the coming blink!).

Similarly, the inability to pay sustained attention becomes _lateral thinking_. ADHDers' brains are able to make surprising and creative connections that other brains are incapable of. Jumping from idea to idea and responding to changes in environments in new and innovative ways can come in handy in many creative fields, such as photography or improv. 

Inattention to detail, for those with ADHD, transforms into _able to thrive in chaos_. While the majority of people tend to want a break when there's too much going on, chaos is an ADHDer's wheelhouse. Their brains can process information and make key decisions at incredible speed, making them suited to high-pressure jobs, such as professional chef. 

Finally, forgetfulness is another symptom that has its advantages when seen as a kind of _resilience_. Rather than dwelling on past mistakes, ADHDers are able to move forward with ease, remaining motivated and energized for challenges ahead. This makes them resilient and well suited to entrepreneurship and other pursuits where failure is an inevitable hurdle on the road to success.

### 5. Many people have become very successful precisely because of their ADHD. 

It's plain to see that ADHD has its advantages. But how much of an advantage does this really give ADHDers in the real world? Let's explore some case studies where ADHD has helped individuals to live up to their ambitions. 

In the previous blink, we learned that ADHDers are resilient, creative multitaskers. They often act intuitively, without spending too much time worrying about consequences. This can prove extremely useful in the world of business, where uncertain situations and momentary opportunities call for snap decisions. 

In his business endeavors, entrepreneur Sir Richard Branson has made the most of his ADHD traits. His multitasking and fearless, inventive decision-making allowed Branson to avoid failure and continually move forward, thus capitalizing on a huge range of business opportunities. From air travel to entertainment to media, Branson has done it all and continues to manage his projects with ease. 

ADHD also has its benefits for athletics. Michael Phelps, Terry Bradshaw and Peter Rose are all successful athletes with an ADHD diagnosis. As ADHDers, their qualities allowed them to flourish and thrive in competitive environments.

How?

Well, imagine you're an NFL quarterback. It's your job to assess your team's distance to the next down, consider the score, the time left on the clock and which team members need a pep talk from you. It's no surprise that ADHD multitasking abilities are a huge advantage here! And, when it comes to acting quickly, confidently and faultlessly in front of hundreds of screaming fans and even more viewers in front of the TV, the ADHD ability to be calm under pressure is another plus. 

If you find the right environment, your ADHD brain can help you thrive. Indeed, ADHDers have strengths that others don't — so get out there and make the most of the advantage!

### 6. Final summary 

The key message in this book:

**ADHD is not a mental health epidemic, nor is it a reason to overprescribe drugs to children. Physicians, parents and teachers alike can all strive for a more conscientious approach to diagnosis, as well as a broader recognition of the needs and strengths of people with ADHD.**

Actionable advice:

**Help your ADHD child at home!**

Maybe your child has ADHD and struggles with homework. Rather than getting frustrated, why not treat this as a creative challenge for you both. Experiment with unconventional methods to help them stay engaged while making the most of their multitasking abilities. Try letting them work with the TV on, or with music playing, or break homework into 15-minute chunks. Keep trying new things and you'll soon find a unique approach that suits your child. Adult ADHDers can apply these tactics to their own lives, too! 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** _ **How Children Succeed** _**by Paul Tough**

These blinks explore the reasons why some people struggle in school and later on in life, and why others thrive and prosper. Using scientific studies and data from real schools, the blinks dive into the hidden factors that affect the success of children.
---

### Dale Archer, MD

Dale Archer is a Medical Doctor, a board-certified Psychiatrist and a Distinguished Fellow of the American Psychiatric Association. He has run his own private psychiatric practice for 25 years, and was recently appointed by the Governor of Louisiana to serve on the Medical Advisory Board. In 2013, he published _Better Than Normal: How What Makes You Different Can Make You Exceptional_, a _New York Times_ best seller.

