---
id: 533af8903961370007000000
slug: the-advantage-en
published_date: 2014-04-01T11:16:37.000+00:00
author: Patrick M. Lencioni
title: The Advantage
subtitle: Why Organizational Health Trumps Everything Else in Business
main_color: C82831
text_color: C82831
---

# The Advantage

_Why Organizational Health Trumps Everything Else in Business_

**Patrick M. Lencioni**

These blinks outline the key principles for building a healthy organization where all the employees pull together in the same direction following the same objectives. This enables organizations to achieve their full potential, while unhealthy competitors waste resources in internal squabbles.

---
### 1. What’s in it for me: find out why your company can’t afford to be unhealthy anymore. 

Anyone who has ever laughed at a Dilbert comic can probably recognize some of the ridiculous consequences that working in an unhealthy organization can have for employees. Amusing though the comic strips may be, the real life organizations find themselves in deep trouble. They waste resources and waste employees because they simply do not work well as a team. Sooner or later, they lose out to healthy competitors.

To avoid this happening to your company, you need to join the winning side by building a healthy organization.

In these blinks, you'll discover how to do this.

You'll learn why many leaders ignore the health of their organization, even though it's killing them.

You'll also understand why the best leadership teams are the ones whose members can admit to each other, "I screwed up."

You'll also see why everyone's favorite waste of time — meetings — are actually crucial to healthy organizations, and how you can make them more effective.

Finally, you'll find out why quarterly executive team retreats play an essential part in tackling the company's strategic challenges.

### 2. To be successful, organizations need to be smart and healthy. 

As long as there have been companies, there have been business leaders pondering the secret of a successful organization.

For an organization to become successful, two criteria must be fulfilled: the organization must be _smart_ and _healthy_.

Being _smart_ means having the technical competence to deal with aspects of the company like strategy, marketing, finance and technology, while being _healthy_ means having high morale and productivity as well as minimal conflicts, confusion and employee churn.

As you will discover in these blinks, it's organizational health that most affects the company's success.

Why?

First of all, healthy organizations can at least become smart over time, but unhealthy organizations can't.

This is because leaders in unhealthy organizations are reluctant to admit their own flaws and therefore won't ask others for advice — which impedes learning. In healthy organizations, leaders are open about mistakes and therefore quicker to overcome them and learn from them.

Consider the analogy of a family: In healthy families where parents raise their children through discipline, affection and time spent together, the children tend to be able to overcome the difficulties they face. Unhealthy families, on the other hand, often produce children who struggle no matter how smart they are.

Second, in unhealthy organizations even smart people often make stupid decisions. Many companies have smart leaders who have graduated from top universities, but their firms still fail because they are riddled with politics, misalignment and inconsistencies.

In fact, it turns out that having smart leaders is not that important: many healthy companies run by relatively average people still make wise decisions that make them more successful than their "smarter" competitors.

Of course, the financial costs of an unhealthy organization are undeniable: squabbling staff and inefficient work results in wasted resources and time as customers and employees leave. This means a healthy organization is in fact at a competitive advantage, the benefits of which can be seen in the bottom line as well as in the happiness of employees.

But how can you create a healthy organization and why do leaders often neglect this vital aspect of business?

### 3. Leaders neglect organizational health as they think they’re too sophisticated, busy or analytical to bother with it. 

When thinking about organizational health, most leaders stumble into one of the three biases: _the_ _sophistication_ _bias,_ _the_ _adrenaline_ _bias_ or _the_ _quantification_ _bias._

First, there's the sophistication bias _:_ Achieving organizational health is seen as too simple a task by many well-educated business executives, as all it demands are high levels of discipline, persistence, courage and a healthy dose of common sense. This can be off-putting if the leader believes that dramatic improvements can only come from sophisticated, complex endeavors.

Second, there's the adrenaline bia _s:_ Many business leaders prefer to spend their time on more exhilarating tasks — like solving urgent problems — than on the long process of becoming a healthy organization. Somehow they never get around to the tasks that are "just" critical, not urgent. But these leaders would do well to remember an old race-car driver saying: "You have to go slow to go fast."

Finally, there's the _quantification_ _bias:_ Though there's no doubt that a healthy organization delivers many benefits, it's difficult to accurately quantify them, and many leaders don't accept this. It is just plain hard to measure the financial impact of changes like having higher morale or improved relationships between departments.

In addition to the three biases, many leaders may also fall into the trap of only looking for answers where they are most comfortable. They are not comfortable with the subjective and awkward conversations required to understand organizational health, so they look where there is _better_ _light_ — meaning where there are measurable, objective facts and data.

Business leaders tend to underestimate the importance of organizational health because they become suspicious of anything that sounds even remotely "touchy feely." However, when organizational health is presented in the right context, it becomes one of the greatest opportunities for gaining a competitive advantage that a company has.

Next, let's look at what organizations can do to become healthier.

### 4. Build a cohesive leadership team to create a healthy and cohesive organization. 

At the end of the day, all companies are made up of people and the relationships between them. This means that no company can achieve its full potential when the relationships between its leaders and various departments are dysfunctional.

So how can this unfortunate state of affairs be avoided?

The first step is to build a small executive team that works well together.

To see how this can be achieved, it is first important to understand the concept of a _real_ _team_. A real team means a group of people who share a common goal and are collectively responsible for attaining it. This could, for example, be a basketball team where the individuals work together, selflessly, to achieve the common goal of the team winning.

So how can leadership teams be built on the principles of real teams?

First of all, leadership teams are best kept small — optimally between three and 12 people. The small size helps team members quickly and efficiently share their own ideas and opinions, as well as ask questions of others.

Second, the sense of collective responsibility should be enhanced by ensuring the team members share sacrifices. This means all members must be willing to make tangible sacrifices — for example, by being willing to relinquish some of their organization resources to other departments. In addition to this, though, team members must also share intangible sacrifices, like everyone putting in equal amounts of time and effort for the good of the team, with no one slacking off.

Finally, to be able to properly manage the priorities of the organization, all the members of the leadership team must share the same goal. Just as a basketball team will suffer if one player cares more about his personal point tally than the team winning, so too will an organization suffer if its leaders pull in different directions. One concrete way to help ensure that this does not happen is to base part of the leadership team's compensation on the achievement of the common goal.

### 5. From trust to results-orientation, there are five behavioral principles that teams must embrace to build a cohesive team. 

Anyone who has been on any kind of team knows that _cohesion_ greatly contributes to that team's efficacy. And for a leadership team to be cohesive, it must embrace five behavioral principles:

First, great leadership teams must have _trust_ — more specifically _vulnerability-based_ _trust_, the kind of trust that stems from openly sharing one's vulnerabilities and mistakes. This allows team members to say things like, "I need help,""Your idea is better than mine," "I screwed up," and even, "I'm sorry." Such honesty requires a strong bond among team members, but saves a lot of time and energy for all parties.

Second, teams must not fear c _onflict_. Whenever a team tries to find the best possible solution to a problem, conflicts will undoubtedly arise. Yet these must not be feared, as they are a key part of the problem-solving process. As the saying goes, "No pain, no gain."

What's more, avoiding the conflicts at hand out of fear usually results in frustration and more intense disagreements down the line.

Third and fourth, team members must be _committed_ to decisions made in the team, and share _accountability_ for those decisions.

For team members to commit to a decision, they must be given the opportunity to provide their own input, but they must also understand the rationales behind the decisions made. Otherwise they won't feel like active participants and won't feel committed to the decision.

For a decision to stick, it is crucial that the whole team feels accountable for it. This is also true of goals: teams pursue them more vigorously when they are held accountable for them.

Finally, great teams always deliver the _results_ they set out to achieve. And to get there, all the team members must share the same common goal. For example, a soccer team must share the goal of winning. If they have excellent offence but lose due to poor defence, the offensive coach can't think, "At least my goals are being fulfilled," because the team is still losing.

### 6. Leadership teams need to build cohesion by asking themselves simple questions about who they are and what they’ll do. 

Like phalanges in ancient Greece, a team achieves maximum efficacy when members are maximally aligned with each other in thought and action.

To eliminate even small discrepancies in their thinking, a leadership team must answer _six_ _critical_ _questions._

The first question is simply, "Why do we exist?" This deceptively simple question is in fact vital: the team needs to identify the core purpose of the company. This is because employees throughout the company need to know which larger aspiration they are working toward. This question is also useful because it clarifies who your clients are and what your market is.

The second question is, "How do we behave?" This question is meant to clarify the character — the values — of the company. This helps employees know how to behave in accordance with them without having to micromanage, which is both inefficient and disheartening.

The third and fourth questions are, "What do we do?" and, "How we will succeed?"

The former obviously clarifies what the business does to turn a profit. Even here, discrepancies sometimes crop up.

The latter question, on the other hand, helps the company determine what strategy it will use to achieve success.

The final two questions that the leadership team should answer are, "What is most important right now?" and, "Who must do what?"

Both aim at turning ideas into action. First, the team needs to set one top priority to achieve in a given time as this will align and focus the organization. Second, the team must agree on a division of labor to pursue this priority, so that all the critical areas are covered.

Once the leadership team can agree on answers to these questions, they will have attained a high level of cohesion.

### 7. Employees should be continuously reminded of the company’s reason for existence, core values, strategy and priorities. 

Once the leadership has established clarity and alignment over the six critical questions presented in the previous blink, they must communicate their answers to the rest of the company. In fact, they must _overc_ ommunicate them.

Great leaders should consider themselves _Chief_ _Reminding_ _Officers_ — yes, they must plot a strategic course for the company, but one of their top priorities must also be to remind people of it on a regular basis.

This is because people tend to be skeptical about what they are told unless they hear it constantly. It has been shown that leaders who say the same thing in a consistent way are seen by employees as more serious, authentic and committed to what they are saying.

Repeating the message like this is counterintuitive for many leaders, because they have been trained to avoid redundancy. But they must understand that repetition helps employees fully embrace the message.

In addition to communicating effectively with employees, the leadership team must do the same among themselves so they are all aware of what message they will present to the rest of the organization.

An example of poor communication could be seen when one company struggled to cut costs and decided in an executive staff meeting to instate an immediate hiring freeze. Shortly after the meeting, the head of human resources sent out a global announcement of the freeze. To her surprise, within five minutes two of her executive peers were in her office protesting the new policy, because they hadn't realized it would apply to their departments as well. This drama could have been avoided by taking five minutes at the end of the meeting to reiterate what would be communicated to the rest of the company.

Thus clarity in communication is just as important as cohesiveness in the leadership team.

### 8. Human systems in the company must be simple and built on the answers to the six key questions. 

No company can survive without its employees. Therefore, a key part of any company is its _human_ _systems_ — meaning the processes that involve managing people, from hiring and performance management to training and compensation.

To effectively design and lead human systems, leaders must ensure the systems reflect the special nature of the culture of their organization. In turn, they can only determine this special nature by answering the six critical questions.

Once they know what they want, they should design simple systems that directly enforce the behaviors that the organization values.

This is best understood by looking at the _logical_ _life_ _cycle_ _of_ _an_ _employee_, meaning the path from recruiting and hiring to orientation, performance management, compensation and rewards, recognition, and, if need be, firing.

In each of these phases, leaders must ensure that people are adhering to the right behaviors:

First, in the hiring process, potential candidates should be identified by how they fit the company's behavioral values.

Then, when an employee is in orientation, care should be taken to constantly keep the new employee on the right path, as the first days and weeks in a new job are very important in the long term.

Next, when the time comes to manage the employee's performance, employees should be given clear direction and proper coaching as well as regular information on how they are doing.

On the compensation front, incentives and recognition should be used to remind them of what is best for the organization.

Finally, if it seems like an employee fits the organization's values but isn't performing well, the company should first look at how the employee is being managed and then give them another chance to succeed. If, on the other hand, the employee does not fit the company's values, he or she should be let go, as this act actually reinforces the company's values throughout the organization.

In the next blinks you'll discover why meetings are crucial for building and maintaining healthy organizations.

### 9. No action, activity or process is more central to a healthy organization than the way it holds meetings. 

It is a great irony that many leaders consider the meetings of the leadership team to be a waste of time. In fact they are the single best indicator of organizational health.

Leadership team meetings are where cohesion, clarity and communication originate, or, alternatively, where the seeds of an unhealthy organization are sown.

This is because meetings are the time when values are established, discussed and exemplified. They are also where decisions around strategies and tactics are considered, made and reviewed. One simply cannot overstate the importance of meetings.

To make the most of them, leadership meetings should occur regularly and should allow time for discussing each important issue, as well as defining what needs to be done so that it is understood by all.

But as mentioned earlier, despite their importance, leadership meetings are perceived by many executives as boring, wasteful and frustrating.

Quite often these problems arise from the fact that leaders tend to save all their issues to be discussed in a single meeting, believing this makes the meeting more efficient. But of course, this merely muddles things up, as the human brain is incapable of dealing with a broad variety of issues in one setting, so no issues get the attention they deserve.

An example of this could be seen at a large company where they held a biweekly staff meeting to discuss all the issues that had arisen during that time. This was so overwhelming that in every meeting they had to postpone the decisions on most of the issues and could only decide on the most urgent ones. They ended up "firefighting," meaning they merely dealt with the most pressing emergencies and totally lacked a strategic focus, which in turn demoralized the leadership team.

As you'll see in the next blink, the remedy for this kind of issue congestion is simple: To be effective, the leadership team must arrange many different kinds of meetings to resolve different kinds of issues.

### 10. Leaders use four different kinds of meetings – daily, weekly, monthly and quarterly – for different levels of granularity. 

Now that we have seen that not all issues should be discussed in one meeting, let's look at four different kinds of meetings, each differing in its objectives and the frequency with which it is held.

First, there are the daily _Check-In_ meetings, where teams make a habit of gathering once a day for ten minutes to exchange information. This saves a lot of time because many issues can really be resolved in less than a minute if all the necessary stakeholders are in the room, whereas chasing after the necessary inputs one-by-one would take hours.

Second, there are the _weekly_ _tactical_ _staff_ _meetings_. These are in fact the most valuable activity in any organization, as they help build a cohesive team and a healthy organization.

Somewhat counterintuitively, they will run much more smoothly without an agenda. Instead, every member should report on the activities and issues they feel are their top priority. The organization-level priorities will then determine which of the topics raised should be discussed further.

Third, at least once a month, meetings should be held that dig into the critical issues that have a long-term impact for the company, or take a significant amount of time and energy to resolve. These could include things like a deficiency in a product or a major competitive threat.

Too often leadership teams try to resolve these important issues in 15 minutes as part of the staff meeting, which should deal with more tactical and administrative topics. This is not efficient.

Finally, the fourth type of meeting is the _quarterly_ _off-site_ _review_, meant to allow the leadership team to get a fresh perspective on their business.

Such a meeting should include reviewing the organization's strategic goals, assessing the performance of key employees, discussing any changes to the company's environment like major emerging trends and identifying any competitive threats on the horizon.

Of course, to avoid the dangers of miscommunication, with the exception of the daily check-ins, every meeting should end with executive team members clarifying what they've agreed to and what they will communicate to the rest of the organization.

In the last blink, you'll discover why leaders are important for healthy organizations.

### 11. Leaders and the teams around them are the key component of building healthy organizations. 

At the end of the day, the main factor helping an organization become healthier is the genuine commitment and active involvement of the leader.

Far too often, however, leaders think that tasks related to organizational health should be handled by others. This is a mistake, as the leader must lead through example.

Leaders should be out in front of their employees as active and tenacious drivers of the behavior they want to encourage. They must demand that their leadership teams satisfactorily answer the six questions. They must also be the first to tackle the difficult tasks demanded for organizational health, and the first to do the hardest things such as demonstrating vulnerability, provoking conflict or confronting people about their misaligned behavior. These are responsibilities they cannot delegate to others.

The leadership team as a whole can help the organization become healthy by taking two critical initial steps — requiring from one to six months to complete in total — depending on the energy allocated to them.

First, the leadership team should dedicate two days to work on building team cohesion and fostering clarity among themselves by working together somewhere out of the office.

Second, they should put together a "playbook" or a short summary of answers to the six questions and how the team will work together to achieve them — for example, who does what.

Of course, the success of the leader's and the leadership team's efforts relies on effective communication. Thus the team should put in place a system that reinforces key behaviors by embedding reminders about them in every process.

This is not a project though, it is an ongoing process. Building a healthy organization is like a building a healthy marriage: the work is never done. It requires open-ended attention and effort. Happily, the leaders of healthy organizations do come to enjoy the work required because they witness the extraordinary benefits that a healthy organization demonstrates.

### 12. Final Summary 

The key message in these blinks:

**For a company to succeed, it is not enough for it to have smart leaders. It must also be healthy, and the responsibility for this lies with its leadership team. By defining and communicating the organization's values and priorities clearly, they can attain the competitive advantage that a healthy organization has over squabbling unhealthy ones.**

Actionable advice:

**Don´t fear conflicts.**

If you're in a leadership position at a meeting and see people disagreeing about something, interrupt them for a moment to remind them that what they are doing is good. Honest, open conflicts lead to better decisions and hence should not be feared. This encouragement helps them develop better rapport.
---

### Patrick M. Lencioni

Patrick Lencioni is the founder and president of The Table Group, a management consultancy specializing in organizational health and the development of executive teams. He has authored ten business books that together have sold over three million copies globally. His best-selling book is _The Five Dysfunctions of a Team_, the key ideas of which are also available in blinks.

Patrick M. Lencioni: The Advantage copyright 2012, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

