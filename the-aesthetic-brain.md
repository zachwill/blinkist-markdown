---
id: 58d8f35f54572a0004c51747
slug: the-aesthetic-brain-en
published_date: 2017-03-27T00:00:00.000+00:00
author: Anjan Chatterjee
title: The Aesthetic Brain
subtitle: How We Evolved to Desire Beauty and Enjoy Art
main_color: E8662E
text_color: B24F24
---

# The Aesthetic Brain

_How We Evolved to Desire Beauty and Enjoy Art_

**Anjan Chatterjee**

_The Aesthetic Brain_ (2014) explains how and why the human brain responds to beauty and art. These blinks break down the reasons why we instinctively prefer some faces to others, what art does to our brains and how we started making art in the first place.

---
### 1. What’s in it for me? Discover why your brain craves art and beauty. 

Like other animals, we humans desire the things we need to survive and procreate — food, water, companionship and sex. But why is it that we also crave beauty in our lives? Why do people spend so much time dancing, singing or painting rather than simply looking after ourselves and procreating?

Through advances in neuroscience and evolutionary biology, we're gradually unearthing what's behind our strange fascination with beauty. It's a beautiful story itself!

In these blinks, you'll learn

  * about the relationship between beauty and parasites;

  * why we all have a fondness for the African savannah, even if we've never been there; and

  * what it means to be "disinterested" in a great work of art.

### 2. There are universal principles that make a face appear beautiful. 

What do Winona Ryder's face and the African savannah have in common? They're both beautiful! But what is it exactly that makes them so?

Well, the human brain is wired to respond automatically to beautiful faces and bodies, sometimes without us even being aware of it. In fact, studies have found that it's nearly impossible to look at a face without considering its attractiveness.

In one experiment, people compared two computer-generated faces. They were asked to judge how similar they were to one another. Even though participants were not asked to judge the beauty of either face, their visual cortices showed increased activity when presented with an attractive person; in other words, their brains were unconsciously and automatically reacting to pretty faces.

This instant appraisal also leads us to have higher opinions of good-looking people, including those we hardly know. Studies have even shown that attractive people receive higher grades, land better jobs and have better salaries.

But the question still remains as to why exactly we find certain people beautiful. To answer this fundamental question, scientists have devised three basic parameters that constitute an attractive face: averageness, symmetry and _sexual dimorphism_, that is, physical features that distinguish one gender from another.

The first characteristic refers to the tendency for humans to find statistically average facial features more appealing than atypical ones. This means that we usually prefer medium-sized noses to big or very small ones, and eyes that are neither too close together nor too far apart.

The second aspect, symmetry, has been found to be essential to our appraisal of beauty, since facial symmetry is considered an indicator of good health and a robust immune system. As we'll see in the next blink, both of these features are also aesthetically appealing.

Finally, the importance of sexual dimorphism means that people who have typically male or female features are considered more attractive. For example, a large chin is often considered a typically manly feature; just think of Brad Pitt's chiseled chin and try to say he's not an attractive man!

It's thus clear that our brains seek out and admire beauty — but what exactly is beauty, and how can it be defined? Let's take a closer look in the next blink.

### 3. Pursuing beauty in faces and places helped our ancestors survive. 

Most parts of the human body serve a basic, primitive function. We have mouths so we can consume energy in the form of food, opposable thumbs to help us grasp objects and legs to carry us through the world. Similarly, the human brain has distinct regions that maintain specific functions, like learning languages and making decisions.

Each of these developed because of the survival benefits they afforded our ancestors, which, in turn, helped them live long enough to pass these benefits on to their descendents. So the question is: what's the survival benefit in finding something beautiful?

The answer is simple: the reason we're so obsessed with attractive people is that we instinctively interpret their looks as signs of health and fitness. This makes perfect sense evolutionarily, as mating with healthy partners tends to produce healthier offspring.

Amazingly enough, we can get all this information just from a pretty face. Consider the trait of symmetry, which we learned in the previous blink is a central feature of facial beauty.

Diseases transmitted by parasites often produce asymmetrical physical features and abnormalities in the face and body; people with these types of diseases also tend to have weak immune systems. In contrast, symmetrical faces and bodies are indicative of a healthy immune system and a fit body.

Another curious aspect of our aesthetic preferences is a gravitation toward wide landscapes. Just like with faces, the landscapes we find beautiful tend to be those that were most conducive to our ancestors' survival.

As a result, we prefer wide, open spaces in general, but the setting we seem hardwired to desire most of all is the African savanna. Studies have actually found that no matter their age or ethnicity, people enjoy pictures of the savanna on average more than images of any other location — even if they've never been there.

This makes perfect sense, as the survival benefits of the savanna are plain to see: it's wide and flat, which makes it easy to detect potential predators long before they arrive. Plus, there are sporadically scattered trees to climb in case a predator gets too close, and plenty of peaceful mammals that offer an abundant source of food.

### 4. External circumstances influence our sense of beauty, while culture exaggerates traits we find naturally attractive. 

Looking at centuries-old baroque paintings, one thing is clear: the idealized women depicted in these pictures are much more curvaceous than the pencil-thin supermodels we see on magazine covers today. What changed?

While our sense of beauty is largely determined by our brain's structure, the society we live in also influences what we consider beautiful. Scientists have found that the body styles men look for in partners are affected by the food supply of the community in which they live.

Simply put, if a country has lots of food, and therefore a fatter population, thin women are more desirable. But if food is scarce, men will favor larger women. This difference is likely due to the fact that fat reserves allow a woman to safely bear and nurture children when food is scarce, as it was during the baroque era.

Beyond that, all human cultures and civilizations have overemphasized the features we find naturally beautiful. In fact, if we find certain visual aspects stimulating and rewarding, we sometimes enhance them to increase the reward they offer.

Just take the way comic book characters are drawn with hugely exaggerated sexually dimorphic features. Lots of superheroes have hypermasculine features like tremendously broad shoulders and chiseled square chins, while the female characters in these comics have big breasts and wide hips.

Or consider the huge eyes of animated manga characters. This is no accident; big eyes also happen to be one of the cute, baby-like features that we naturally prefer in faces.

Finally, consider the obsession in our culture with youthfulness. An entire industry of personal care products exists based solely on the notion that younger-looking people are preferable to ones who look older.

As a result, if you measure the facial dimensions of a supermodel's face, you'll discover that they're eerily close to those of a ten-year-old girl!

> "_Fat countries like thin women."_

### 5. Making art is a universal human behavior, but art itself is hard to define. 

Art is everywhere you look, from the streets and alleyways of your city to the kindergarten classroom down the block, full of kids happily scribbling, singing, dancing and telling stories. Creativity seems to come naturally to children — but art is an essential aspect of human nature in general.

As a result, we can't talk about human history without coming across some form of art. For instance, some cave paintings date back as far as 30,000 years, while the oldest man-made sculpture, the _Venus of Tan-Tan_, which was discovered in North Africa, is around 400,000 years old!

Even our hominid ancestors like the neanderthals and _Homo erectus_ expressed clear, albeit rudimentary, artistic behavior. They decorated their burial sites with flower petals, colored pigments, feathers and bones.

But what actually makes something art?

Up until now you've learned mostly about why humans perceive objects as beautiful — but art implies much more than that. While many works of art are harmonious, proportionate and produce a sense of beauty in their beholder, art can also be tragic, delicate and moving.

Just take Edvard Munch's painting, _The Scream_. It's rather fear-inducing, as are many of Alfred Hitchcock's movies.

Beauty isn't the only essential factor and, as the British philosopher Edmund Burke pointed out, art isn't just beautiful, it's also sublime. Art can be overwhelming and even painful, as it can confront us with our own insignificance.

While there have been many attempts to define the subject beyond these terms, art remains truly indefinable, which has everything to do with its inherent tendency to disregard rules, norms and cliches. For example, one of the most revolutionary and widely known pieces of art was produced by Marcel Duchamp as part of his involvement in the Dadaist movement, which intentionally tested the boundaries of what could be considered art.

In 1917, he declared a porcelain urinal an artwork, an act that changed the understanding of art forever. It was so impactful that 44 years later, an Italian artist named Piero Manzoni canned his own feces and presented it as a work of art called _Artist's Shit_. On May 23, 2007, one of these cans sold at Sotheby's for €124,000.

### 6. Art engages the sensual, emotional and cognitive networks of the brain. 

The human brain has fixed regions for certain abilities like linguistic acquisition and logical thinking. Art, on the other hand, has no such set location in the brain.

Instead, many different neural networks work together to create and process art. This is only logical, since our experience of art involves our senses, emotions and thinking, all in equal proportions.

The first component, an engagement of our senses, is fairly straightforward. Art activates senses like vision, along with their neural counterparts, so that when people look at visually striking works of art, the visual parts of their brains become more activated than normal.

But what's most interesting is that certain visual properties appear to captivate us with near certainty. Just take the mathematical proportions defined as the _golden ratio_. This ratio is present in plenty of objects we consider beautiful, from the Egyptian pyramids to the Parthenon.

When visual information is presented in accordance with the golden ratio, the visual area of the brain has an easier time processing this information, while the area that "judges" whether or not such information is appealing becomes more active.

The second response art can trigger is our emotions. If we consider an artwork that depicts an emotion like fear, disgust, happiness or sadness, it will affect us as if we're actually experiencing something frightening, revolting, happy or sad. This response is virtually identical in every person.

On a deeper level, particularly if a piece of art is more abstract and based on association, it will affect individuals differently. So, based on personal neurological structures, the same song that makes you happy might make your friend feel depressed. This is a bit of a mystery, as scientific research hasn't yet determined why these individual differences occur.

Finally, our cognitive structures are also deeply involved in processing art as we endeavor to make sense of it. This hard work can be very rewarding; figuring out the meaning or intention behind a piece of art can feel as satisfying as solving a difficult crossword puzzle — and sometimes much more.

### 7. Aesthetic experience is defined by liking without wanting. 

What's the biggest difference between a great painting and an ad for men's underwear? Both are created with the utmost care and are made to be as visually appealing as possible. But while they're both intended to please us, they trigger different responses in our brains.

There are two rather distinct reward systems in the human brain. One is responsible for _liking_ and one is in charge of _wanting_. Since we usually want what we like and like what we want, these two networks tend to work together. But when it comes to aesthetic experiences, like the enjoyment of art, they can become uncoupled.

The two networks are distinguished by the neurotransmitters they employ; both make us feel pleasure, but of different types.

For instance, opioids and cannabinoids are involved when we like things, while dopamines come into play when we want something. These chemicals often collaborate, such as when you like ice cream and therefore want to eat it.

But when art enters the equation, things change. Enjoying art means liking without wanting, which is one of the defining aspects of the aesthetic experience. When you enjoy the stunning _Birth of Venus_ by Botticelli, it would likely never occur to you to try and buy it.

This points to one of art's special traits: it doesn't have any purpose beyond pleasing and engaging us. This interesting observation isn't just a result of modern neuroscience — it was actually described hundreds of years ago by the German philosopher Immanuel Kant who, in his _Critique of Judgement_ published in 1790, wrote that the aesthetic experience is one of "disinterested interest."

In other words, we're interested because we enjoy art, but we're disinterested in that we don't want anything from it. As we consider works of art, our imaginations and thoughts play freely, without any need to own or consume.

### 8. Art is widely considered to be either a natural instinct or a by-product of human evolution. 

People have been pondering why humans created art for eons and, at this point, there are essentially two schools of thought on the matter.

The first considers art to be an instinct, which implies that it has an evolutionary purpose — that humans invented it because it helped them survive in some way or another. An underlying argument of this position holds that art enhanced human chances for survival by strengthening the social bonds in groups.

This idea makes sense since art is often connected to rituals, like singing and dancing, that bring people together. After all, a group that has a sense of cooperation is naturally more likely to survive.

However, not every form of art offers this benefit. Just take lonesome and isolated painters or writers; they're not exactly building community and camaraderie with others.

The second argument is that art is a by-product of evolution, that is to say, art has no purpose of its own, but is essentially an accident of nature. This argument, which also has its flaws, goes like this:

Nature gave us brains with tremendous cognitive powers. Since our bodies aren't as strong as some other animals, our relatively large brains enable us to outsmart predators like tigers and steer clear of other natural threats.

These huge brains of ours are so powerful and flexible, they also happen to enjoy painting, writing, composing and all manner of other arts, just because they can. There's no deeper purpose to art; it is not a product of nature, but rather a human artifact.

Then again, considering how fundamental art appears to be to our human nature, it seems highly unlikely that it's a mere accident. After all, there's not a culture in the world that doesn't embrace art, and children across the globe readily begin drawing and singing on their own. They don't need to be taught these skills, but instead seem to possess them inherently.

So, neither of these explanations are particularly satisfying. But there is a third theory, and you'll learn all about it in the final blink.

### 9. The less we needed to fight for our survival, the more we could focus on art. 

Although we might not always think of it this way, humans aren't the only species on earth that sings. Birds are also magnificent vocalists, and the story of one singing bird in particular, the Bengalese finch, can show us how nature brought art into existence.

Over the course of human evolution, people rapidly developed mental capacities for art, but were only able to express them when in a secure environment. This development was parallel to the way the Bengalese finch began to sing.

Birds generally sing for an evolutionary reason: by chirping their beautiful songs, birds can find mates, defend their territories and identify other winged friends of the same species. For these basic goals, the most effective songs are relatively simple or stereotypical ones — they don't have to be fancy or elaborate to get the job done.

The Bengalese finch, on the other hand, is different. It's a domesticated species that has been bred in Japan for 250 years. The way these birds sing has in no way been influenced by selective pressures, since their survival is ensured and they're free to sing however they wish.

Over these past 250 years of breeding, something astonishing happened. Starting from a rather ordinary tune, the songs of these finches began to vary. Today, they have learned to freely improvise their songs, and even make them echo the sounds of their current environment. Instead of using just one brain network, the Bengalese finch can apply many different ones in unison.

And just like the finch's song, human art also comes from two sources. First, our brains developed the powerful abilities needed to handle evolutionary demands — things like imagination, the capacity for abstract thinking, varied emotions and an instinct for play.

None of these were intended to be used to make art. But, as evolutionary pressures subsided, we gained the second key element: the freedom to use these abilities however we wanted, resulting in the birth of art.

### 10. Final summary 

The key message in this book:

**Humans perceive certain things as beautiful because of the evolutionary benefits these specific things afford us. This principle is evident in everything from pretty faces to attractive landscapes. Similarly, art emerged from multiple evolutionary faculties that, when combined in a safe setting, led to creative expression and artistic outcomes.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Beauty Myth_** **by Naomi Wolf**

_The Beauty Myth_ (1991) will help you understand the anxiety that surrounds women, and the cult of beauty that dominates our society today. It identifies the patriarchal and economic forces that shape the unfair expectations and norms that women face. Learn how we can be better equipped to build a less biased future.
---

### Anjan Chatterjee

Anjan Chatterjee is a professor and Chief of Neurology at the Pennsylvania Hospital, a private, non-profit hospital affiliated with the University of Pennsylvania. Chatterjee is the former president of the International Association of Empirical Aesthetics as well as the Behavioral and Cognitive Neurology Society, and was the 2002 recipient of the Norman Geschwind Prize in Behavioral and Cognitive Neurology.

