---
id: 5506253a666365000a2f0000
slug: the-age-of-cryptocurrency-en
published_date: 2015-03-17T00:00:00.000+00:00
author: Paul Vigna and Michael J. Casey
title: The Age of Cryptocurrency
subtitle: How Bitcoin and Digital Money Are Challenging the Global Economic Order
main_color: ED2F38
text_color: CC2930
---

# The Age of Cryptocurrency

_How Bitcoin and Digital Money Are Challenging the Global Economic Order_

**Paul Vigna and Michael J. Casey**

_The Age of Cryptocurrency_ gives an overview of the history and nature of Bitcoin. It explores the definition of "money" and explains the dramatic impacts that cryptocurrencies like Bitcoin will have on our economy and the world at large.

---
### 1. What’s in it for me? Learn the real stories behind Bitcoin and how it works. 

Whether you heard from the news or online or from a friend who claimed it's the best investment for the future, you're probably aware of Bitcoins by now. This is the digital currency sweeping into conversations from the government to Wall Street to chat rooms.

How does this currency work without any paper money? How do you know if you can trust it to have any value? How can you be sure it won't be hacked, turning your Bitcoins into worthless numbers on a screen?

Read on to find answers to all those questions, as well as a brief history of Bitcoins.

In these blinks, you'll discover

  * how the first Bitcoins used to buy pizza are now worth $5 million;

  * how people use Bitcoin to hire hitmen; and

  * why companies spent close to $100 million on Bitcoins.

### 2. Money has value when it’s based on a system of trust. 

What is money, exactly? It's not the bills in your wallet. Those bills don't have any inherent value — they're just bits of paper. So why can you buy goods and services with them?

For money to have value, there needs to be a shared agreement on its use as a _medium of exchange_.

In money's earliest days, gold or silver was often used to make coins. These coins were different from our modern bills, because gold and silver have intrinsic value. However, there was only one reason why these coins functioned as _money_ : the people using them valued gold and silver, and agreed they could be used to buy things.

If you traded with a culture that didn't value gold or silver, your coins were worthless. Cultures don't always value the same things.

The Micronesian island of Yap, for example, had a peculiar currency system that puzzled early visitors from Europe. They used huge stone wheels called _fei_ as currency. These stones were so heavy that they often remained with the previous owner after an exchange.

The system worked because the Yapese agreed that the ownership (or partial ownership) of fei could be used to settle debts.

A society needs some kind of trust in its money to have a controlled supply of currency. If just anyone could create new money, money would lose its value. There needs to be a limited amount of it for the system to work.

In the 1920s, the Weimar Republic learned this the hard way. Germany had tremendous debt following the Versailles Treaty and tried to pay it by printing more and more bills. The value of the bills became so low that people began using them as wallpaper because it was cheaper than buying actual wallpaper. This hyperinflation caused the economy to collapse, and people lost their faith in the monetary system.

> _"The eye has never seen, nor the hand touched a dollar" — Alfred Mitchell Innes._

### 3. Bitcoin is money because people agree it can be used as a unit of exchange. 

The number of Bitcoin supporters is growing every day, but many people still have a hard time viewing it as "real" because you can't see or touch individual Bitcoins. Bitcoin has already proved itself to be a viable currency, however.

Bitcoin functions as money because people trust that it has value — just like "normal" money. When people started to see that real things could be bought using it, trust increased.

Bitcoin's rising price reflects this trust. In the first three months of 2013, the value of a single Bitcoin rose 800 percent, from $129 to $1,165.

Also, Bitcoin doesn't have a central bank like other currencies. Bitcoins are _mined_, which keeps them from spiraling out of control. We'll learn more about this later in these blinks.

Ultimately, we can know that Bitcoin is money simply because people accept Bitcoins as units of exchange.

Consider the story of Lazlo Hanyecz, a coder from Florida. On 21 May 2010, he made an unusual purchase. His purchase wasn't unusual because of what he bought, but rather the way he did it.

At the time, Bitcoin had only existed for a year but Hanyecz was an early adopter. In 2010 he owned about half of all Bitcoin in the world.

No one accepted Bitcoin then and Hanyecz didn't know what to do with his "money." He decided to pay 10,000 Bitcoins (worth about $41 at the time) for two Papa John pizzas. He found a fellow Bitcoiner through the Bitcoin Forum and had him buy two pizzas using a credit card. Hanyecz then paid him in Bitcoin for the pizza. Bitcoin was accepted as a unit of exchange.

By August 2014, the value of the 10,000 Bitcoin Hanyecz spent on the pizza had risen to roughly $5 million.

### 4. Bitcoins are mined and there’s a public record of all transactions called the blockchain. 

You can't create gold at home — you have to work hard finding it and mining it. Bitcoin works in a similar way. Instead of pickaxes, however, Bitcoins are mined with computers.

Computers mine Bitcoins by solving highly complex mathematical problems.

Solving these problems requires significant amounts of computational power. When a problem is solved, a Bitcoin reward is given to the miner and a new problem is issued.

So the faster your computer, the more likely you are to be rewarded. The number of Bitcoins awarded is also halved every fourth year, so there's an incentive to mine as many as you can before they run out. In total, 21 million Bitcoins will be released. According to one estimate, the last Bitcoin will be mined in 2040.

Every time a new Bitcoin is created, the _blockchain_ is updated. The blockchain is the public record of all transactions ever made in the network.

When a new Bitcoin is mined, a new block is created, validated and added to the chain.

Just as a bank keeps a careful record of every account balance, there's a Bitcoin record of all owners' balances and transactions to ensure the same Bitcoin isn't spent twice. This is the purpose of the blockchain, which everyone has access to.

Every Bitcoin owner has an address — a unique and encrypted number assigned to them in the Bitcoin network. Addresses help keep track of who is who.

So if you buy a coffee at a café that accepts Bitcoin, the network will register a request to send BTC.0.008 (or one 8,000th of a Bitcoin) from your address to the café's.

> _"Money is like muck, not good except it be spread." — Francis Bacon._

### 5. Bitcoin removes all middlemen and keeps both the sellers and buyers anonymous. 

Every time you swipe your credit card or transfer money, banks and credit card companies skim a bit off the top. Wouldn't it be great to get rid of this?

Bitcoin does. It removes the middleman and makes transactions cheaper and more efficient.

In the fourteenth century, the Medici family served as a middleman between savers and borrowers. They kept a careful record of their accounts and transactions — for a fee, of course. This was the birth of the banking system and it led to an explosion in economic activity. It also made the Medici family one of the richest and most influential in Europe.

Since then, banks have only become more powerful. They have a huge impact on our society, especially because they now influence our politicians through lobbying.

Bitcoin sprung from a movement that wanted to change this system by giving power back to the people. Through the blockchain, everyone has access to the distributed network that Bitcoin is based on. This ensures that no single person or institution can control the system as a whole.

Although buyers don't always pay a fee, sellers often do, which means that this value has to be added to the price. This is why shops often won't accept cards for purchases under a certain amount. Without a middleman collecting fees, however, Bitcoin makes transactions cheaper and faster.

With cards, there's also a hugely complex and time-consuming process behind the scenes of every transaction. When you buy a coffee at Starbucks with your credit card, it usually takes three business days for them to receive the money. With Bitcoin, the transaction is completed almost instantly.

Bitcoins are important because they obscure the identities of both the buyer and seller. Indeed, a key feature of _cryptocurrencies_ is their anonymity. "Crypto" means hidden. Bitcoin protects its users by keeping their identities secret.

### 6. Bitcoin has become a global business. 

Bitcoin has only been around a few years, but the number of Bitcoin believers has grown quite rapidly. All across the globe, people are devoting themselves to Bitcoin. It's becoming very profitable to do so.

Huge amounts of money are now invested in Bitcoin mining every year. According to one estimate, over $1 billion was spent on building "rigs" of super-fast computers designed specifically for mining Bitcoins between April 2013 and April 2014.

The processors now used to mine Bitcoins are about three million times faster than they were when Bitcoin was founded. The computational expansion of the industry is simply unparalleled. Manufacturers of these super computers have a hard time keeping up with the demand.

Some people even claim that if the industry keeps growing at its current rate, it'll cause an environmental catastrophe because it uses up so much electricity.

Bitcoin has spawned entire new areas of innovation and investors are catching on. Communities where people come together to work on Bitcoin-related projects are springing up all over the world. 20Mission in San Francisco, founded in 2012 by Bitcoin enthusiast Jered Kenna, is one example. It's become a hub where young Bitcoin entrepreneurs can work, sleep and socialize.

Innovations created at 20Mission have included MaidSafe, a solution that lets users rent out their free disk space over a decentralized network, and ZeroBlock, an app that shows Bitcoin prices in dollars along with notifications when there are any price changes.

Investors were initially wary of putting their money into these kinds of projects, but this attitude has changed dramatically. Surveys conducted by the news site Coin Desk have shown that the amount of venture capital going into Bitcoin-related companies increased from $2 million to $88 million between 2012 and 2013.

### 7. Bitcoin could have a huge and positive impact on the developing world. 

There are about 2.5 billion people worldwide who don't have money in banks. They lack many of the freedoms people in developing countries take for granted. Bitcoin could change all this.

Bitcoin can give people in developing countries more economic freedom. Fatima, a mother of five who lives in a refugee camp in Mali is an example of this.

Mali is one of the poorest countries in the world. Like many Malians, Fatima's husband went to the Ivory Coast to find work and sends money back to her. Since neither of them can access a bank account, he sends her cash, which often disappears along the way.

Once they have smartphones, however, they'll be able to send each other money using Bitcoin. They'll be able to send and receive it without banks or other institutions taking any of it away. Phone companies are investing heavily now in sending their products to more of the developing world.

Bitcoin will also help people to keep their money more securely — a crucial step toward escaping poverty.

Bitcoin can also empower women around the world and help increase equality.

Parisa Ahmadi, a young girl in Afghanistan, has already benefited from this. She took part in a class held by Film Annex, an arts project based in the United States that pays about 300,000 filmmakers and bloggers to produce small movies and write blog posts.

Ahmadi is an avid film lover, and began publishing movies about her life on the site. She also started writing reviews of other films and earning a small income from them.

However, Ahmadi didn't have a bank account, like most women in Afghanistan. So the founder of Film Annex started paying her in Bitcoin. He also set up an e-commerce site where people can buy gifts from Amazon using Bitcoins. Ahmadi used hers to buy a new laptop.

### 8. Bitcoin still has many weaknesses and is difficult to regulate. 

So, given all these benefits, what are the downsides of Bitcoin?

Bitcoin software is still far from bulletproof, which means that its price is very volatile.

People learned this the hard way on 10 February 2014. Gavin Andersen, the chief scientist at the Bitcoin Foundation and the developer behind Bitcoin's core software, got flooded with panicked messages. Mt. Gox, one of the biggest Bitcoin exchanges in the world, was on its knees. A bug had been revealed in Bitcoin's software that made it possible to create fake transactions and receive unwarranted payments.

Andersen tried to remedy the situation, but it was too late. Hackers started exploiting the vulnerability, Mt. Gox collapsed and the price of a single Bitcoin plummeted from $703 to $535 in just one day. Imagine the consequences if a major currency like the dollar lost this much value overnight.

Bitcoin is also difficult to control, because of its _distributed network_.

Paul Baran, a computing pioneer, developed this concept. In a distributed network, every point is connected to all other points, so the information is sent across the entire web. This means it's practically impossible to shut down. There's no Bitcoin CEO or CTO who can get slapped with a subpoena.

Another problem is that Bitcoin can be used for criminal purposes, like selling drugs or even hiring hitmen.

Silk Road is an example of this. It's an anonymous online marketplace that uses Bitcoin as currency. After it was founded it quickly became a hotspot for trading all kinds of illegal substances.

Since the buyers' and sellers' identities are hidden, it's very difficult for law enforcement agencies to investigate these crimes.

### 9. Final summary 

The key message in this book:

**Anything can function as money as long as people agree it has value. Bitcoin has only existed for a few years, but the industry has boomed and peoples' trust in Bitcoin as a form of currency has only increased. It has the potential to radically change our global economy by removing middlemen and providing anonymity. Despite their currently volatile nature and other downsides, cryptocurrencies are undoubtedly the future.**

**Suggested further reading:** ** _The Death of Money_** **by James Rickards**

_Death_ _of_ _Money_ examines the current global monetary system, centered around the dollar. If current policies continue, a total collapse is imminent. You should prepare for the worst.
---

### Paul Vigna and Michael J. Casey

Paul Vigna and Michael J. Casey are both journalists at the _Wall Street Journal_. Vigna is also an anchor on _MoneyBeat_, and Casey is the author of _Che's Afterlife_ and _The Unfair Trade_. Casey is also a regular commentator on the BBC.

