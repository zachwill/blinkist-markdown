---
id: 57c154504a1a18000344dc9d
slug: the-age-of-empathy-en
published_date: 2016-08-29T00:00:00.000+00:00
author: Frans de Waal
title: The Age of Empathy
subtitle: Nature's Lessons for a Kinder Society
main_color: AE2445
text_color: AE2445
---

# The Age of Empathy

_Nature's Lessons for a Kinder Society_

**Frans de Waal**

_The Age of Empathy_ (2009) debunks popular theories which suggest that human nature is inherently selfish, cut-throat and prone to violence. Evidence provided by biology, history and science makes clear that cooperation, peace and empathy are qualities that are as natural and innate to us as our less desirable traits.

---
### 1. What’s in it for me? Discover the universal empathy of humanity. 

Throughout history, authors, philosophers, as well as political and religious leaders — have told us to be wary of the evil at the core of human nature. People are egotistical, these thinkers claim, always searching to improve their position, regardless of the negative impact their actions have on others. The atrocities of war and genocide seem to bear out such assertions. But what if this is just a small part of the story?

What if the things that have kept us alive and evolving are empathy and connection to our fellow humans? What if, instead of a desire to kill other people, what really drives soldiers to enlist is a desire to belong? Let's explore these possibilities and take a closer look at the empathy within us all.

In these blinks, you'll learn

  * the real reason the walls of Jericho were built;

  * what herd instinct is and how it has affected our evolution; and

  * how to tell that a couple has been together for a long time.

### 2. There is a popular but misguided tendency to view human nature as inherently selfish. 

The idea that human nature is fundamentally selfish is widespread. Even pop culture propagates this idea: "Greed is good," proclaims Michael Douglas's character in the 1987 movie, _Wall Street_. "Greed is right. Greed works. Greed clarifies, cuts through and captures the essence of the evolutionary spirit."

Ideas like this are popular in both cultural and political theory, and they've kept alive the myth that humanity is inherently self-centered.

This notion is also backed up by the theory of _Social Darwinism_. Introduced by nineteenth-century British political philosopher Herbert Spencer, it offers a "survival of the fittest" outlook on life between the "haves" and the "have-nots."

Social Darwinism further suggests that it's counterproductive for those who succeed in life to feel obligated to help, as they can get dragged down by those who are struggling.

This kind of ideology has found its way into the business world as well.

For example, in the early twentieth century, business tycoon John D. Rockefeller, Jr., regarded the expansion of big business at the expense of smaller businesses as "merely the working out of a law of nature."

These are all misunderstandings of human nature and are especially dangerous when they fuel self-fulfilling prophecies.

We saw this in the notorious case of ENRON, an energy company that believed humanity was driven by two things: fear and greed. This created a horrible corporate environment as well as a brutal system ENRON called _Rank and Yank_, in which managers ranked employees on a five-point scale and fired anyone who received a five. This system led to 20 percent of the workforce being fired every year.

ENRON's cold-blooded practices extended beyond employee relations, too. In order to raise the price of energy costs, the company caused artificial blackouts and shortages, showing no concern for the harm that these tactics could cause people in elevators or on respirators.

But this cold-hearted business philosophy eventually backfired, and ENRON collapsed in 2001.

> _"The whole effort of nature is to get rid of such [the "unfit"], to clear the world of them, and make room for better."_ \- Herbert Spencer

### 3. History shows that war and violence haven’t always been part of the human experience. 

Winston Churchill, the famous British Prime Minister, once said, "The story of the human race is War. Except for brief and precarious interludes, there has never been peace in the world; and before history began, murderous strife was universal and unending." But how much truth is there in this point of view?

A closer look at science and history shows that warfare was once not as pervasive as you might think.

Contrary to Churchill's opinion, it's probably more accurate to suggest that human history is comprised of long stretches of peace and harmony with brief episodes of violence.

For instance, the walls of Jericho, which you may have read about in the Old Testament, have long been considered a defensive structure and among the earliest evidence of human warfare.

However, modern research suggests that these ancient tales aren't historically accurate and that the walls were likely built as protection against mudflows.

There's also the fact that our ancient ancestors were constantly at risk of extinction, living in small, widely dispersed groups with a global population of only a couple thousand. Under such conditions, it's likely that warfare wasn't a common concern at all.

These ancestors were hunter-gatherers and their life was like the modern-day Bushmen of Africa. So, while violent confrontations did occur, they were infrequent interruptions during otherwise peaceful times.

Likewise, the warfare and organized combat of modern times is not a result of any natural proclivity for violence and aggression. Rather, our military involvement is fueled by our natural herd instinct.

When you consider Napoleon's army marching across the freezing expanses of Russia, or American soldiers flying to the Middle East, it's not a desire for bloodshed that is motivating them.

Following the orders of a general that everyone else is obeying or falling into a lockstep march with the thousands of soldiers beside you are instinctual actions that come naturally to all of us. But these actions are driven by the same herd instinct that leads to other, pleasurable coordinated activities, like chanting, singing, dancing or playing certain sports.

### 4. The herd instinct plays a vital role in bonding experienced by both humans and animals. 

Have you ever wondered why yawning is so contagious? The mere mention of it is often enough to elicit it!

This is due to _unconscious synchrony,_ which is another term for the herd instinct that is found in many aspects of our life and the lives of other animals.

Synchrony and the herd instinct stem from the sense of interconnectedness that exists in humans and elsewhere in the animal kingdom.

For example, it's not only humans who find yawning contagious. Researchers at Kyoto University showed videotaped footage of yawning chimpanzees to some apes in their lab, and, sure enough, before long the apes were yawning like crazy.

This synchrony is also part of the survival mechanism that compels a flock of birds to fly in formation and head in the same direction.

And it's of vital importance. Imagine you're a member of that flock and suddenly all your bird pals fly off. There's no time to stop and figure out what's going on; so, without unconscious synchrony, you could end up dead!

The same applies to migrating animals that need to stop for food and rest.

The instinctual coordination of these activities ensures that everyone sticks together and survives, as there is often only one chance to eat and rest during the long journeys.

Furthermore, being in sync with others allows for important bonds to form.

This manifests as subtle mimicry. When out on a date, you'll respond better to a date partner who subtly mimics your actions — who seems relaxed when you're relaxed, who takes a sip of water when you do and who shares your smiles or frowns.

Synchrony can even change how you feel about the service you receive.

Experiments have shown that a waiter can double his tips simply by repeating a client's order instead of just saying "You got it!" or "Good choice!"

But, as we'll see in the next blink, the bonding that we experience through synchrony has more value than just a few good tips.

### 5. It is natural for humans to bond with one another as it leads to longer and happier lives. 

There's a reason why solitary confinement is considered one of the worst punishments short of death. It can be so difficult to endure that some confined inmates will cause trouble just so they can interact with the guards.

So, it's another misunderstanding of history to suggest that society is the creation of autonomous beings.

In the eighteenth century, philosopher Jean-Jacques Rousseau referred to this origin myth as a _social contract_ — society is a compromise, requiring people to give up part of their "freedom" in exchange for increased safety.

However, suggesting that our ancestors didn't rely on other people for survival and happiness is utter speculation.

Humans are highly dependent on one another, both emotionally and physically, and without the company of others we can become hopelessly depressed. It would be strange to assume it wasn't always so.

Nor is companionship only good for happiness; research has shown that the most reliable way to extend one's life expectancy is to get married and stay married.

So strong are the bonds of marriage that they can literally change us physically. Scientists did a study where they showed people two sets of photos, one of individual men and women on their wedding day, the other of individual men and women who'd been married for 25 years.

When the participants were then asked who was married to whom within each set, they had no problem pairing the couples who'd been married for 25 years; however, they flunked attempts to match the newlyweds. The study showed that married couples tend to look like one another not because they pick partners who resemble themselves but because, after years of bonding, the couple's features end up converging.

This physical similarity was strongest between couples who were reportedly the happiest and spoke of sharing their emotions on a regular basis. It's this kind of bonding that allows one partner to "internalize" the other, and vice versa, to the point that it becomes apparent to any observer that they are indeed a couple.

### 6. When we deny our natural instincts to be nurturing, it can have tragic consequences. 

How much control do you think you have over your own decisions, impulses and desires? It might be tempting to believe that we can choose what is best for us, an outlook encapsulated in a theory called _behaviorism_, which regards the human mind as a blank slate that we have complete control over.

John Watson, the father of behaviorism, attempted to prove his philosophy by experimenting on a little boy named Albert.

Watson was able to condition "Little Albert" to whimper every time he saw a rabbit by banging loud steel objects together and producing a horrible noise whenever the boy was handed a rabbit.

Watson regarded this as a triumph of behaviorism over human nature, and his devotion to the power of conditioning led him to ignore our inherent biological wiring.

For example, Watson was skeptical of maternal love, and believed society required less nurturing and more structure.

However, attempts to put these ideas into practice were disastrous. Psychologists studied orphaned children who were kept in cribs separated by white sheets and who received no visual stimulation or bodily contact. These children ended up resembling zombies, blank-faced with wide-open, unmoving eyes.

If behaviorism was right, they should have been thriving; but instead, they were near death's door.

Deprived of the nurturing that builds babies' natural resistance to disease, many of the children needlessly died.

All of this makes clear that, from the moment we're born, we need nurturing, human connection and empathy in order to survive. It's simply a biological imperative.

As mammals, it's critical that we receive maternal care. This initial bond is so important that it continues to reverberate through our lives as we get older.

For instance, when we show love to our partners we might feed them a piece of food or employ the same kind of "baby talk" language a mother uses with her child.

### 7. Empathy comes naturally to us since it plays an important part in ensuring our survival. 

Chances are you've lent someone a helping hand at some point in your life, and it's likely that you didn't need to be conditioned in order to do so.

Indeed, as biology and history show, our sense of empathy and cooperation come naturally to us.

After all, we wouldn't be here if our natural disposition was to be competitive and insensitive.

This is especially the case for motherhood and good parenting, where empathy is key. Over the course of 200 million years of evolution, parents have developed a natural sensitivity to the concerns of their offspring in order to keep them healthy and safe.

This sense of empathy is crucial since the chances of a helpless baby surviving if its parents are instinctively cold and uncaring aren't very good. Therefore, empathy is a big part of why we're here today.

But it is also something we have little control over.

When Swedish psychologist Ulf Dimberg was researching involuntary empathy in the 1990s, he conducted an experiment to see how participants reacted to pictures of happy and sad faces.

As you might expect, people frowned when they were shown angry faces and smiled at happy ones. But, surprisingly, people had the same reaction when the pictures were flashed on the screen so briefly that there was no time to consciously register them.

So, even though the participants were unaware of whether they were shown happy or sad faces, they unconsciously responded in the appropriate manner.

Other than psychopaths, who by definition are incapable of feeling empathy, no one is emotionally immune to the plight of another's situation.

So, the next time someone tries to tell you that human nature is fundamentally evil, hopefully you'll have enough information to prove them wrong!

### 8. Final summary 

The key message in this book:

**Empathy and caring for our fellow human beings comes more naturally to us. While society tends to emphasize our proclivity toward negative behavior, biology and science show that living harmoniously and helping one another are equally inherent traits. It's what we choose to see and focus on in ourselves that often becomes the reality.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Our Inner Ape_** **by Frans de Waal**

Human beings are just as closely related to the gentle bonobos as they are to the aggressive chimpanzees. Frans de Waal compares the lifestyle of these two species of apes, in whose groups opposing characteristics such as sympathy and violence, fairness and greed, and dominance and community spirit clash with one another. Their sexual behavior tells us that we need to rethink the origins of our morality.
---

### Frans de Waal

Frans de Waal is one of the world's most renowned primatologists. A professor at Emory University, he has authored many other books, including _Primates and Philosophers_, _Our Inner Ape_ and _Chimpanzee Politics_. In 2007 he was named as one of _Time_ magazine's 100 most influential people.

