---
id: 5a817cb5b238e100072f7082
slug: the-age-of-spiritual-machines-en
published_date: 2018-02-16T00:00:00.000+00:00
author: Ray Kurzweil
title: The Age of Spiritual Machines
subtitle: When Computers Exceed Human Intelligence
main_color: DDBADB
text_color: 8C628A
---

# The Age of Spiritual Machines

_When Computers Exceed Human Intelligence_

**Ray Kurzweil**

_The Age of Spiritual Machines_ (1999) is your guide to the future. These blinks explain the new age of machines and what robotic intelligence will mean for life as we know it.

---
### 1. What’s in it for me? Prepare for a world run by artificial intelligence. 

What passed for a major technological advancement in the 1500s would be a joke today and it's no secret that technological progress is accelerating at an exponential rate. While it used to take decades or even centuries for a world-changing technology to emerge, we now experience such shifts every few years.

It's this exponential growth that will inevitably lead to a world run by intelligent machines. Interestingly enough, the development of artificial intelligence is in many ways a continuation of our own human intelligence. One could argue that AI is what will allow humans to surpass the limits of biological evolution and continue to follow the developmental trajectory of life on Earth.

In these blinks, you'll learn

  * how the universe was formed and how life on Earth began;

  * what computers will be capable of in the year 2099; and

  * how we humans can transcend the limits of our own biology.

### 2. Time slowed down when the universe first developed but sped up as life-forms evolved. 

Technology is accelerating at an ever-greater pace. We now live in a world of artificial intelligence, drones and 3D printing, but all this rapid-fire change stands in stark contrast to the origins of the universe. In fact, as the universe began to grow, time actually slowed down.

Here's the story:

About 15 billion years ago, the universe was born. From there, it took just 10-43 seconds, a tiny fraction of a blink of an eye, for the newly created cosmos to cool down to what was still a whopping hundred million trillion trillion degrees, a temperature low enough for gravity to develop.

About 10-34 seconds after the development of gravity, the universe had cooled to a billion billion billion degrees, which allowed matter — subatomic particles like electrons and quarks — to arise. Approximately 10-10 seconds after matter was created, electromagnetic forces like light began to emerge. And, finally, 10-5 seconds after electromagnetic forces were born, the temperature cooled to a paltry trillion degrees. Quarks could then form protons and neutrons.

But then things began to slow down. As the universe expanded, events became more spaced out. From tiny fractions of seconds to hours, years, hundreds of thousands of years, and so on.

The basic building blocks of the universe popped into existence in no time at all, but the larger pieces came into existence much more slowly — it was hundreds of thousands of years before the first atom formed, and hundreds of millions before the formation of galaxies. Earth didn't exist until nine billion years later!

As life-forms began to evolve on Earth, however, time began to speed up. The first of these single-celled organisms was formed some 3.4 billion years ago, about a billion years after the formation of Earth. From that point on, evolution gained momentum.

Seven hundred million years ago, plants and animals — the first multicellular creatures — evolved. In the 130 million years that followed, the basic physiological structures of animals, like spinal cords, appeared. These new structures not only enabled primordial fish to propel themselves through the water, they also marked a significant acceleration in evolutionary time.

As this pattern continued, the primates emerged and, pretty soon, evolution could be measured in tens of millions of years, rather than hundreds.

### 3. Technology and life-forms evolve exponentially thanks to a shared ability. 

Evolution may be a complex, confusing process, but there's one specific factor at play in the evolution of all life. It's called _computation_ : the ability to store information and use it to solve problems. This ability is what enables exponential evolutionary development.

For instance, in regards to the evolution of life on Earth, organs emerged as a means of maintaining the internal conditions of the body and responding to external stimuli. These useful structures were the result of an increasingly evolved and complex nervous system that eventually made it possible for us to store memories and recognize patterns, to which we could then respond.

But computation obviously doesn't apply to biology alone; it's central to technology as well. As humans developed and refined technology, machines were eventually created that could maintain themselves, store information and identify patterns even better than humans.

Just consider the evolution of the mechanical calculating machine. This technology was first created in the 1600s, and it became increasingly complex in the years that followed. It had evolved into an electrical tabulating system that stored data on punched cards. This machine, invented by Herman Hollerith, was powerful enough to automate the entire 1890 US Census.

Unsurprisingly, computation has allowed technology to evolve exponentially, just like the people who created it. This rapid evolution began in earnest during the nineteenth century, which saw the advent of photography and telephonic communication, as well as the mass construction of railroads.

Perhaps most interesting of all is the fact that this process is still accelerating at an exponential rate. For example, the technological progress achieved during the first two decades of the twentieth century exceeded that of the entire nineteenth century. Nowadays, in the twenty-first century, it takes just a couple of years to see major shifts in technological growth.

### 4. Time and order are inversely proportional, producing incredible results. 

Now you know that time has variously sped up and slowed down since the birth of the universe. But how is this possible?

It all depends on the degree of chaos in the world and how it impacts evolution. (In this case, "chaos" simply means the prevalence of random events). A general rule of thumb is that time expands exponentially — that is, it slows down dramatically — in response to exponential increases in the chaos of a closed system. Such a system is one that isn't impacted by any external influences.

This rule is called the _Law of Increasing Chaos_ and a good example of it is the development of the universe. At the moment the universe started expanding, it was empty and singular. Then, chaos began to exponentially grow through the emergence of significant events like the emergence of gravity, matter, and light.

Eventually, time intervals began to expand, meaning that the amount of time necessary for further development to occur increased exponentially.

So it is that today the universe is home to billions of galaxies, all united in a vast, seemingly never-ending space. Such a picture may seem about as chaotic as can be, but the truth is that the universe is still far from its peak, which is likely billions of years away.

In stark contrast to the Law of Increasing Chaos is _The Law of Accelerating Returns_, which means that time exponentially contracts, or accelerates, as order exponentially increases in any given process.

This law regulates evolution, both of our species and of technology.

Because of the order that these processes foster, evolution can increase its rate. As an example, consider the evolution of basic life-forms. Once single-celled organisms evolved, life began to develop at an exponential rate. This is because DNA established a starting point for future attempts. The orderly structure provided by DNA means that evolution doesn't have to start from scratch every time, and this reduces the amount of chaos.

That's why this principle is called the Law of Increasing Returns. It refers to the product of the evolutionary process, which accelerates exponentially relative to the increases in order.

So now you know how this logic applies to human and technological evolution. In the next blink, you'll learn how it relates to something in between.

### 5. Human evolution is limited but machine intelligence can transcend its roadblocks. 

How do you construct an intelligent machine?

It certainly requires a number of material resources. But it also requires knowledge. Just like humans, machines can't solve problems if they lack the knowledge to work through them.

So how can a machine acquire this knowledge?

One option is for humans to feed information into machines in big chunks, but this approach doesn't work for all knowledge. A better route is to design computers that can learn for themselves, just like humans.

To accomplish this, neural nets are used to design computing systems that mimic the processes of the human brain. Such structures enable machines to learn from information gathered from the environment and transform it into knowledge.

For instance, computers will eventually be able to read and comprehend any written document. They'll be able to understand the content of literature and eventually be so advanced that they'll gather knowledge and share it among themselves.

But knowledge isn't all that smart machines need. They also rely on computation.

As you learned earlier, the human brain is capable of computation, but only at a slow and limited pace. After all, with limited memory capacity, people struggle to remember even a dozen phone numbers. Machines, on the other hand, have no trouble searching a database of millions of numbers and recalling them all in an instant.

Ultimately, that's why DNA needs to be removed from the equation. While DNA has enabled life to evolve from a single cell into the powerful form of human beings, its limited neural capacity will never serve as the foundation for new and better designs.

Interestingly enough, evolution has solved this problem itself. In the form of human beings, it has created organisms that are capable of creating technology that doesn't rely on DNA for its evolution.

### 6. By 2029, computers will be capable of much more, transforming basic spheres of life. 

At this point, you know that machine intelligence is evolving just like that of living organisms — and with much greater potential. So what are computers going to look like in the year 2029?

It probably goes without saying that computers in the future are going to be capable of a whole lot more than they were in the past. For instance, while a computing device could be expected to run 20 million billion calculations per second by 2019, a computer in 2029 will likely be able to run a thousand times more. That means that a computer worth $1,000 will be as powerful as a thousand human brains.

In fact, computing is largely done on massive parallel neural nets that are in large part modeled on those of the human brain. Such a process is a great example of _reverse engineering_, in that computers have been designed to "think" like humans. The circuitry of computers is even based on that of the human brain.

In the future, computers will play a greater role in innumerable arenas of life, education among them. That doesn't mean more computers in classrooms; rather, it means human teachers will mostly be replaced by virtual ones.

It's even likely that neural implants, tiny computers that are surgically inserted into the human brain, will enhance learning by increasing the human capacities for perception and memory. That being said, in 2029, knowledge won't just be downloaded into our brains; we'll still have to work to learn new material.

And, finally, computers will also dramatically affect the field of communications. For instance, to supplement virtual reality technology, we'll likely improve on forms of holographic and sonic communication. That means that people will be able to sit around the dinner table and converse with their families, even when they are all physically in different parts of the world.

But for the most part, communication will take place between two computers. If a human _does_ enter the picture, it'll likely be to communicate with a machine of some type.

Now that you've got an idea of what to expect from computers in the near future, let's look even further ahead.

### 7. By the year 2099, humans and computers will begin to merge. 

At the close of the twenty-first century, in the year 2099, we'll be in for a serious shift: human thought and computer intelligence will begin to merge.

By this point, we can expect to have scanned and analyzed every single aspect of human neural function; the reverse engineering of the human brain will be complete.

This accomplishment, combined with the capacity and speed of machine intelligence, will mean that smart machines will be more competitive than humans. The number of humans who use software to function will exceed by a wide margin those that still function solely on organic computation.

Even this latter group will likely have neural implants to improve cognition and perception. In fact, if they don't, they'll be incapable of communicating meaningfully with those who do use such technology.

In short, the entire definition of humanity will have changed. The innumerable ways that machine intelligence and human biology will eventually combine will require a redefinition of what it means to be human.

And it won't be long before intelligent machines begin to argue that, since they were created from human intelligence, they too are human, even if their brains are mere metal and silicon copies of the real human brain.

This discussion will be accompanied by a debate about guaranteeing particular rights and powers to machine intelligence. In fact, such an argument will likely be one of the most pressing political and philosophical discussions of the time.

In short, as technology evolves, one thing can be expected: the merger of humans and machines. And this will transform humanity as we know it.

### 8. Final summary 

The key message in this book:

**The evolution of humanity from single-celled organisms occurred at an exponential rate, but there's only so far that humans can develop given our biological limitations. In creating technology, humanity has begun to transcend these roadblocks and will eventually produce machines even smarter than people. In all probability, humans and machines will merge by the end of the twenty-first century.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _How to Create a Mind_** **by Ray Kurzweil**

_How to Create a Mind_ (2012) offers an intimate examination of the nuts and bolts behind how the brain works. Once we understand exactly how people think, perceive the world and decide to take action, the creation of true artificial intelligence seems a possibility that's just around the corner.
---

### Ray Kurzweil

Ray Kurzweil is an inventor, computer scientist and futurist. He's the inventor of a reading machine for blind people, speech-recognition technology and multiple music synthesizers. Among other titles, he's the author of _The Singularity is Near_, _How to Create a Mind_, and _The Age of Intelligent Machines._

