---
id: 5a5163f3b238e10006337087
slug: the-airbnb-story-en
published_date: 2018-01-09T00:00:00.000+00:00
author: Leigh Gallagher
title: The Airbnb Story
subtitle: How Three Ordinary Guys Disrupted an Industry, Made Billions . . . and Created Plenty of Controversy
main_color: 1D5A92
text_color: 1D5A92
---

# The Airbnb Story

_How Three Ordinary Guys Disrupted an Industry, Made Billions . . . and Created Plenty of Controversy_

**Leigh Gallagher**

_The Airbnb Story_ (2017) tells the extraordinary tale behind the rise of Airbnb. These blinks describe how, within about a decade, three recent college graduates went from being behind on their rent to developing the most popular vacation accommodation platform in history.

---
### 1. What’s in it for me? Learn how Airbnb became the business it is today. 

Most of us were taught as kids to never get into a stranger's car, yet today we have no problem at all sleeping in a stranger's house. Without Airbnb, this probably would not have become such a common occurrence.

For those unfamiliar with Airbnb, it's a platform that helps people rent out spare rooms or whole apartments to tourists or other people visiting a city. Though a relatively new company, investors estimated its revenue for 2016 at $1.6 billion, and predict $2.8 billion for 2017, climbing to $8.5 billion by 2020.

So how did this company come about and how did it become a dominant player in the hospitality business? These blinks will tell you the story.

You will also find out

  * how cereal helped Airbnb in a big way;

  * why Airbnb spent six months finding their first employee; and

  * how Airbnb has tackled concerns about safety, discrimination and legality.

### 2. Airbnb began as a way for two broke designers to cover their rent. 

In October of 2007, Brian Chesky and Joe Gebbia, both graduates of the Rhode Island School of Design, were trying to establish themselves in San Francisco. Despite their design degrees, the two young men were struggling to pay their monthly rent of $1,150, and were faced with a simple choice: make more money or go back to their hometowns.

While studying, the pair had learned that any problem can be overcome through creative thinking and, after some brainstorming, resolved to focus their energy on the upcoming conference of the Industrial Designers Society of America in San Francisco.

They knew there would be a shortage of hotel rooms during the event and decided to rent out some space in their apartment where people could sleep on one of their three air mattresses for $80 a night.

They called the venture AirBed & Breakfast and promoted it on design blogs with ads that focused on their apartment's features, like its "design library."

In just a few days, three customers had made bookings. From there, after receiving positive feedback, they started thinking about how to act as middlemen, using other people's apartments to make money. Their first venues for this project were the South by Southwest festival in Austin, Texas and the 2008 Democratic National Convention, or DNC, in Denver, Colorado.

For both events, the designers wanted to bring in an engineer who would add a user-friendly interface to the operation, as well as the skills of a groundbreaking coder. For this role, the choice was Nathan Blecharczyk, with whom Gebbia had already worked.

But despite their crack squad, the trio had trouble getting people to open their homes up to strangers. They needed an in and, in the case of the DNC, decided to take out ads on small local blogs.

Amazingly, this move resulted in the quirky little service attracting press attention, with great results. In fact, after the _New York Times_ ran an article on it, some 800 people signed up as hosts, resulting in 80 successful bookings.

### 3. Airbnb took off after launching a cereal-based marketing stunt and adopting greater flexibility. 

The trio had experienced what looked like success as a result of their work around the DNC. But following the event, their website was getting no traffic. They were starting to get desperate.

Since they were technically offering a bed and breakfast service, they came up with the funny idea to serve Obama O's and Cap'n McCain cereals. They simply reboxed cheap cereal and earned nearly $30,000 selling their new products as collectors' items.

But more importantly, they got the attention of Paul Graham, a tech billionaire and founder of Y Combinator, a popular launchpad for Silicon Valley start-ups. While Graham didn't like the idea of AirBed & Breakfast, after hearing about the cereal, he appreciated the tenacity of the team and gave them a three-month mentoring period.

Graham helped the trio realize that they should spend time in New York City, where the majority of their limited user base lived. After temporarily relocating to the city, Chesky and Gebbia began working to improve the photographs used by hosts, while Blecharczyk fine tuned the technical side of things.

During this period, the team started getting creative with their business model. For instance, users who made inquiries about renting entire apartments convinced Chesky and Gebbia that they would have more widespread appeal if they dropped the air mattress and breakfast requirements. As a result, AirBed & Breakfast became Airbnb.

It was also during these early days that Chesky and Gebbia spent much time out in the field, organizing host meetups and showing people how they could make money by renting out their apartments. Their new ideas were panning out and they were soon bringing in $1,000 a week in revenue.

This success grabbed the attention of Greg McAdoo at the investment firm Sequoia. He had previously invested in Google and Apple, and was well aware that the vacation rental business was worth a whopping $40 billion. With a $585,000 investment from Sequoia, Airbnb rapidly found itself valued at $2.4 million, propelled by the renewed confidence of McAdoo's faith in their idea.

> _"'The moment Sequoia funded us, the rocket ship took off,' says Chesky."_

### 4. As Airbnb grew, the company stayed true to its mission, which was central to its success. 

So, a couple of guys renting three blow-up mattresses in their San Francisco apartment had turned into thousands of people renting out their living spaces all over the United States. Airbnb had blossomed as an adventurous, offbeat, affordable alternative to traditional accommodation, and was especially attractive for millennials.

To maintain this success, Chesky, who stepped into the role of CEO, made it Airbnb's mission to put a unique human exchange at the heart of everything the company did. For instance, the company required that each apartment have a kind and personal touch to it, whether it be a homemade welcome packet or a host being on site to greet their guest.

Fundamentally, Chesky believed that such a human connection was key to Airbnb's culture. That's why the company's core values include a passion for humanity, along with a commitment to being hardworking, helpful and playful.

To keep such values intact, hiring people who embodied them, as well as the company's overall mission, took on fundamental importance — and this was no easy task. In fact, hiring the first employee took a full six months, as Chesky believed this first hire would determine the company's DNA.

He believed that once they secured their first recruit, other like-minded people would come on board. This magic moment happened in the Summer of 2009, when Nick Grandy joined the founders' team as the first hired engineer.

Getting hosts on board with Airbnb's values was an easier task. The people opening up their homes to paying guests were generally eager to take part in the company's mission and happily contributed to its growth. This was another vital factor, since there would be no Airbnb without hosts opening their homes, and this would never happen without trust between hosts and travelers.

At the heart of this exchange was Airbnb's motto, Belong Anywhere. Hosts were expected to go above and beyond to share their favorite hangouts, art galleries and nightclubs. As a result, travelers explored places they would have never found had they chosen to stay in hotels.

### 5. Designing a well-oiled payment and booking interface posed an early challenge for Airbnb. 

Airbnb employs around 2,500 people, most of them in San Francisco. But what's more impressive are the company's millions of users worldwide.

At this point, there are 3 million active Airbnb offers in 191 countries, with only 20 percent of them in North America. Beyond that, since the company's launch, 140 million guests have checked in. And finally, even after eight years of steady growth, the company is still expanding its user base, with 1.4 million new users joining per week at the end of 2016.

That being said, for this whole operation to run smoothly, a seamless user interface is essential. To make this a reality, Chesky and Gebbia followed Steve Jobs's "three clicks away from music" rule when developing Apple products, meaning in Airbnb's case that it should take a user only three clicks to book their accommodation.

The concept is ingenious, but in the process of working out the payment procedure, customer service and reviews turned out to be a major challenge. The final product was developed over a long period, but eventually became a highly respected accomplishment among website engineers.

Here's how it works:

When a customer reserves a place, they pay for it up front. Airbnb charges a service fee of 6 to 12 percent to customers and 3 percent to hosts. That part is simple enough, but the tricky part is that the rest of the money has to be put on hold, and not actually transferred until the customer is satisfied with the accommodation.

Designing a mechanism for the money to be held for 24 hours and then passed onto the host was no easy feat, but Blecharczyk eventually devised a solution; he tailored PayPal into a uniquely sophisticated tool that was capable of operating seamlessly, 24 hours a day, across global markets and currencies.

### 6. Safety issues once posed a major issue for Airbnb. 

The majority of people operate with positive intentions, but there will always be those who don't. So, while Airbnb takes safety very seriously, accidents do happen.

For instance, in June of 2011, the San Francisco apartment of an Airbnb host named EJ was trashed by renters. On her blog, she wrote that her spirit itself had been stolen, but she loved Airbnb and, even after the accident, still believed that 97 percent of users were honest.

The problem was that Airbnb's customer service didn't immediately respond to her email, forcing EJ to wait until the next day. However, when Airbnb did get in touch, it was to offer emotional and financial support. In the end, EJ was absolutely thrilled with the way the company handled the situation.

Then, in August of the same year, Chesky publicly apologized on behalf of the company, taking full responsibility for the slow response. He raised the Host Guarantee insurance offered by Airbnb from $5,000 to $50,000 and launched a 24-hour customer service hotline.

But for the company, one accident was one too many. To deal with this serious issue, they devised a sophisticated system of safety precautions. The first, and likely most effective, is the user review system introduced by the company's founders.

Airbnb also runs background checks on all its US users and, since 2013, users have been able to choose whether to host or stay with other verified ID users — those whose virtual presence has been matched to their real-life self.

A third safety measure utilized by the company is its triage system, through which Airbnb assesses the level of danger and safety of all reported concerns to prioritize them.

And finally, the company maintains a Trust and Safety team of 250 people who work to identify any suspicious behavior and eliminate potential risks before they materialize. The team includes data scientists who use behavioral models to determine the probability that a person will commit a crime, alongside crisis managers and victim-advocacy professionals who are trained to intervene in and deescalate conflicts.

### 7. Racial discrimination has rocked Airbnb. 

In 2011, Michael Luca, an assistant professor of business administration at the Harvard Business School, began studying shared market platforms like Airbnb, Amazon and eBay.

All of these sites use personal profiles and user photographs to build trust, a fact that Luca thought might result in discrimination. As it turns out, he was right — and in the case of Airbnb, that discrimination is racially motivated.

Luca uncovered this in a 2014 study, showing that nonblack hosts could charge up to 12 percent more than black hosts for the same accommodation. Airbnb was quick to dismiss the results, saying that the study used old data from just one city.

But in 2016, Luca's team came out with another study, confirming rampant discrimination against guests based simply on the fact that they had common African-American names.

This time around, the study garnered a lot more attention, as it was released concurrently with an NPR program telling the story of an African-American businesswoman named Quirtina Crittenden, who was repeatedly rejected by Airbnb hosts until she changed her profile picture to a landscape and her name to Tina.

The Twitter hashtag she coined, #AirbnbWhileBlack, was a vehicle for others to share similar experiences, and the discrimination she encountered turned out to be a widespread phenomenon.

Airbnb had to act fast and make sure that everyone felt like they belonged, but they first had to accept that the racial discrimination so present in the real world doesn't disappear when people go online. Chesky made a public apology and admitted that the "three white guys" who founded the company failed to consider how profiles could result in discrimination.

From there, Airbnb began making changes. First, they issued an Open Door policy that offers an equivalent accommodation to any user who experiences discrimination. The company also assists users who experienced past discrimination when making new bookings.

Now, photographs are being taken out of the spotlight, while greater focus is being placed on user reviews. And finally, a plan is in the works to train hosts to recognize and avoid unconscious bias, in the process earning a badge on their profiles that indicates completion of such training.

### 8. As it has grown, Airbnb has faced legal hurdles. 

In many places, it's illegal to rent out apartments on a short-term basis and, as Airbnb scaled, it came up against a number of legal issues.

For instance, certain Airbnb listings violated local rental laws or building regulations. Luckily, the company was able to sign agreements with some municipalities to bring these rentals up to snuff. Such new frameworks for short-term rentals, including the right to tax them, were proposed in cities from Chicago to Paris.

But the company soon faced another issue: certain cities oppose its very existence because of the disruptions they see Airbnb creating. A few of these anti-Airbnb cities are San Francisco, Berlin and Barcelona, but the toughest opposition is in New York, which is also Airbnb's largest US market, worth $450 million annually. There, the company gets grief from elected officials, affordable housing activists and even representatives from the hotel industry.

These major actors have been joined in their criticism by local residents, some of whom complain that the peace and safety of their neighborhoods is being compromised by drunk and disorderly weekend guests.

As a result, in 2010, New York changed its Multiple Dwelling law, making it illegal to rent out apartments in buildings with over three units for more than 30 days without a permanent occupant present.

Then, in 2013, Eric Schneiderman, Attorney General of New York State, went to battle with landlords who were using their buildings as hotels, requesting records of all of Airbnb's transactions. Airbnb appealed the process to protect user privacy, but in the end turned over the details of 500,000 anonymized bookings. The report compiled from this data found that 94 percent of hosts had one or two apartments listed, with the remaining 6 percent being commercial hosts.

That said, Airbnb has never supported corporations and is devoted to helping people pay their mortgages while adding traffic to non-touristy areas. Because of this, in the fall of 2015, Airbnb issued a Community Compact pledge, ensuring that its activities don't negatively impact housing affordability in New York, while also adopting a One Host, One Home policy in the city.

### 9. With a rapidly expanding customer base and new products coming online, Airbnb’s future looks bright. 

When Airbnb started out, it had a hard time overcoming the general suspicion people had about sleeping in a stranger's home. But now, those days are long gone; the number of Airbnb users is rising every year and the company is set to become one of the biggest players in the hospitality industry.

Even so, the market research firm Cowen found in a survey that, while Airbnb is pretty big, only half of those polled knew about the company. More significantly, just 10 percent of those surveyed had used Airbnb.

In other words, there's tremendous potential for the company to bring in new hosts and guests by building its brand. This is further supported by the fact that 80 percent of people who knew about Airbnb were interested in trying it and 66 percent were ready to use the service within the next year.

Beyond that, foreign markets are rapidly expanding for the company. For instance, Airbnb use by Chinese travelers increased by 700 percent in 2015. In 2016, the company added 45,000 listings and 1.4 million travelers a week, while its total guest arrivals hit 160 million in February 2017.

More importantly, despite the rapid growth of its primary product, Airbnb is continuing to explore new products to stay relevant. As part of this process, Chesky studied successful technology companies like Google, Amazon and Apple. In the end, he reached two conclusions:

First, Airbnb needs to find a way to offer new products, like Amazon has succeeded in doing. And second, the CEO needs to be personally involved in defining such ventures.

Ergo, since 2014, Chesky has been working on new products for his company with promising results. For instance, in 2016, Airbnb Open introduced Experiences, offering uniquely tailored activities curated by local experts.

A good example is Willy, Elite Runner, a guide that brings travelers on a multiday trip to a high-altitude runners' training facility in Kenya. While the tour guide idea isn't exactly new, neither was sleeping on an air mattress! And with such a vast user base, many Airbnb customers are eager to try something different.

### 10. Final summary 

The key message in this book:

**Before becoming the wildly successful apartment rental service it is today, Airbnb started as a creative solution by young college graduates who couldn't afford their rent. Their innovative idea, born of necessity, eventually grew to become the most popular accommodation platform in history.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Raw Deal_** **by Steven Hill**

_Raw Deal_ (2015) reveals the ugly truth behind the new sharing economy and the harm that companies like Uber or Airbnb are inflicting upon societies around the world. There's a major crisis on the horizon, and it will affect not only these companies' exploited employees. We're all at risk, and we'll need to choose our next steps wisely to prevent an economic collapse.
---

### Leigh Gallagher

Leigh Gallagher is the assistant managing editor at _Fortune_. She is the host of _Fortune Live_ and a regular guest on _Marketplace_, _CBS This Morning_, _CNBC_ and other programs.

