---
id: 5a35c4a4b238e1000633a30b
slug: the-american-presidency-en
published_date: 2017-12-20T00:00:00.000+00:00
author: Charles O. Jones
title: The American Presidency
subtitle: A Very Short Introduction
main_color: D28F75
text_color: 99624D
---

# The American Presidency

_A Very Short Introduction_

**Charles O. Jones**

_The American Presidency_ (2007) offers an introduction to the US presidency and the unique role each president must play in world politics. Find out what kind of thinking went into the creation of this job and how it has changed over the years. America's Founding Fathers created a uniquely experimental government when they broke free from British influence; even today, their experiment continues to surprise us.

---
### 1. What’s in it for me? Take a peek into the office of president. 

From dropping the first atomic bomb on Hiroshima in 1945 to signing the Civil Rights Act two decades later, American presidents have had an incalculable influence on the world we live in. In fact, it can sometimes seem like the US president wields utterly unchecked power — like he can simply do as he pleases.

But is this the case?

Not by a long shot. The US president is severely limited in his powers by the American system and the American Constitution. The Founding Fathers, made wary by the tyrannical rule of Britain, designed a system that barred any single person from becoming all-powerful.

So what are the rules that govern a president, and how were they instated? These blinks take you through the long and changing history of the American presidency.

You'll also learn

  * that the American presidency was really an experiment;

  * what the White House was originally called; and

  * why a separation of offices leads to unity.

### 2. The American presidency was part of an experimental concept of governance. 

In 1783, rebel forces in the New World of America defeated the British monarchy and won their independence. After this victory, it fell to a group of men commonly referred to as the Founding Fathers to come up with a constitution and set the rules of government for the newly established republic.

It took some time, but in 1787, they unveiled the new structure for the United States government — a constitutional democracy with a president occupying the highest executive position.

The president was to be somewhat like a monarch, somewhat like a prime minister. He would lead the government, and take care of day-to-day decision-making, as well as be the face of the nation.

The creation of this position was an experiment in governance, and so rather than the old title of "governor," which had been tainted by British colonials, they chose the more neutral "president." This stems from the Latin term _praesidere_, meaning "to preside," so it was free of any British association.

The Founding Fathers also wanted to move away from the system of British nobility. So, to appoint this new kind of leader, they came up with an experimental new electoral system.

They based the system on the principle of _separating to unify_ — that is, they divided power among the executive, legislative and judicial branches of government, so that no single individual or organization could ever seize control and establish a monarchy or some other despotic form of government.

But disagreement arose over how this system should operate. There were two ideological camps. On the one hand, there were the Anti-Federalists. They wanted power to be given to the states, and suggested the president be elected by whichever party had the most state representatives in Congress. On the other hand were the Federalists. Who wanted the president to be elected by the people. They believed this process would match the centralized, executive authority of the presidency.

In the end, a compromise was made in the form of the _electoral college_.

This experimental institution is made up of members, or "electors," who are chosen by the citizens of the state. Each state is permitted a number of electors proportional to its population. The presidential candidate who wins the majority of the electoral votes wins the election. If no candidate wins a majority, the vote moves to congress.

This is the process that was agreed upon in the original Constitution, and it's still in place today.

> _"The two branches were to be independently elected, if interdependently empowered."_

### 3. From the start, the role of the presidency and the branches of government were clearly defined. 

Early on, US leaders believed their new nation needed its own capital city, separate from any one state, where the president would reside, in a house that matched the grand stature of the position.

This was decided in 1790, when Washington, DC was born through the legislation of the Residency Act. Congress eventually agreed on the centrally-located land between Maryland and Virginia, along the Potomac River.

Along with the White House, which represents the executive branch of the government, Washington, DC would also be home to the other two branches of government. The Capitol Building was the home of Congress and the legislative branch, and the Supreme Court represents the highest level of the judicial branch.

For a while, the White House was simply called the President's House, but after being damaged in the War of 1812 and painted white upon restoration, it has been referred to as the White House.

The Constitution explicitly specifies that the three branches of government should be separate, and the role of the president is no less clearly defined. As the Constitution was being written, however, there were three different ideas about how much power the president should have.

The first is known as the _presidential presidency_, which gives supreme authority to the executive role.

The second is a _congressional presidency_, which gives Congress the power to approve or veto any presidential decision.

The third is a _separated presidency_, where Congress and the president work together, yet independently of one another. And this was the role that was decided on by the Founding Fathers at the end of 1787, just over a year before George Washington became the first president of the United States.

The ultimate goal of this experimental new way of governing was to be efficient while avoiding tyranny. In this system, no one branch of government has to rely on another to get things done, yet neither the courts, Congress nor the president can run amok with too much power.

In this system of checks and balances, two of the main tools are the power of veto and the confirmation process. If the president is against a bill being passed by Congress, he can veto it. Likewise, with a two-thirds majority, Congress can override a presidential decision. And while presidents can appoint their own cabinet members, no one gets the job until they receive congressional approval.

### 4. The US election process has changed over the years. 

As you'd expect with an innovative new government, there were some wrinkles that needed to be ironed out early on.

One of the bigger wrinkles was the process for electing a vice president. Originally, the role of vice president went to the candidate who got the second most votes from the electoral college. But as you can imagine, this would tend to provide the executive office with two people from different political parties, such as in 1796, when fierce rivals John Adams and Thomas Jefferson were president and vice president, respectively.

To fix this problem, the Twelfth Amendment was proposed in 1803. It put the president and vice president on the same ballot, as part of the same political ticket.

Further procedures were developed for candidate selection as well. Previously, members of Congress nominated the candidates, but as US citizens became increasingly well-informed and aligned themselves with various political parties, this responsibility was passed to the delegates of those parties.

In 1831, the first major political conventions were held, and delegates from each state convened under one roof to formally nominate their respective candidates for the next election.

In the years that followed, the election process continued to develop — and, in 1901, the first presidential primary was held. Presidential primaries are local elections held in the lead-up to the party convention, as a way to determine which candidates are the most popular with the actual citizens, and not just the delegates.

The first primary was held in the state of Florida, but the process quickly caught on and, by 1920, primaries were being held in twenty states. Primaries continued to grow in importance from there, and, by the 1950s, any candidate with a shot at the presidency was expected to campaign hard during the primaries in order to solidify his chances of winning the nomination.

### 5. Due to the unique nature of the US government it can be difficult for a president to take action. 

Remarkably, America's interdependent system of government, which was devised in the eighteenth century, is still in place today. But that doesn't mean it always runs as smoothly as the Founding Fathers hoped.

If a president has any hope of creating a new law, he needs to secure the cooperation of every branch of the government, which, to say the least, isn't so easy.

When a president is elected, he arrives in office with an agenda that may or may not be warmly received by Congress, which is responsible for legislating the president's proposals. The chances are even slimmer for the president when an opposing party has the majority in the Senate or the House of Representatives.

If a new law is passed by Congress, and signed by the president, there's still the chance that it might not make it through the judicial branch. If the law is challenged, it might go all the way to the Supreme Court and be judged as unconstitutional, in which case the whole process begins again.

Over the years, different presidents have used different methods to get Congress to enact their laws.

Before Lyndon Johnson became president in 1963, he'd already served 24 years in Congress, so he understood better than most how Congress worked. Thus, he was able to make a good many backdoor deals with senators and congressmen to push through his agenda. This is how the Democrat pushed through his "Great Society" agenda, which featured huge, bipartisan reforms such as the Medicare program. It also ensured that many of these changes remained in place when his Republican successor, Richard Nixon, took office.

President Barack Obama, on the other hand, had only served three years in Congress, and so he relied purely on a partisan Democratic Congress to push his agenda through. This backfired once the Democrats lost control of Congress and he was faced with a majority of Republicans unwilling to bend to his agenda.

With Congress closed off, Obama switched his focus to foreign policy and the use of executive orders to enact many of his domestic initiatives, which did not require Congressional approval but did cause much debate over executive overreach.

> _"Presidents are decision makers who work at a job structured by the tension between what is expected and what can be delivered."_

### 6. The nature of the US presidency has evolved, and will continue to do so. 

We've seen why the United States government has long been referred to as "the great experiment." It's an appropriate description, and as the nation continues through its third century, many are wondering what lies ahead for this unique brand of government.

While there have been many changes to the US government over the years, one aspect that continues to evolve on a regular basis is the role of the president.

Even the date for when a new president starts his first term has changed. In 1933, the first day in office moved up from March 4th to January 20th. This makes a lot of sense when you think about how uncomfortable it must have been for the outgoing president to sit in office for six months after losing an election, waiting for his replacement to move into the White House. Chances are high that not much is going to get done during that time.

There was also a time when the American president could try to hold onto office for more than eight years. But in 1947, after Franklin Delano Roosevelt served three consecutive four-year terms, the 22nd Amendment was passed, putting a two-term limit on the job.

Roosevelt used his last term to expand presidential powers, so this new Amendment was seen as offering some much-needed balance on executive control. Without this new limit, it's very likely that we would have seen popular, younger presidents like Barack Obama or Bill Clinton going on to more terms.

As with FDR and the 22nd Amendment, it's very likely that another president will make an executive action that may necessitate another constitutional change.

While there haven't been many significant changes to the rules of the presidency since 1947, conditions of global politics have altered the scope of the American president.

This was extremely apparent in the events following 9/11, when George W. Bush essentially declared war without congressional approval, something the Constitution strictly prohibits. But since groups like Al-Qaeda and ISIS aren't sovereign nations, there was a loophole that the Bush administration could use.

In the new age of modern terrorism, there are many questions surrounding the rules of drone strikes and how many military operations can be ordered without a Congressional vote. As the world continues to change, so will the role of the American president.

### 7. Final summary 

The key message in this book:

**After the American Revolution, the young nation undertook an experiment in government: a constitutional democracy overseen by a president. This new system relied on a separation of powers within the government. Over the years, the office has evolved, but it nonetheless remains anchored by Congress. Thanks to the solid framework provided by the US Constitution, it's safe to say the role of the American president, despite the flux of world politics, will remain stable, too.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Fight to Vote_** **by Michael Waldman**

_The Fight to Vote_ (2016) is about the struggle for democracy in the United States, from the American Revolutionary War right up to the present day. These blinks detail the battle that has been waged over generations to guarantee the right to vote, and explain how this right continues to be undermined.
---

### Charles O. Jones

Charles Jones is a political scholar who works at the University of Virginia's Miller Center of Public Affairs, as well as the Governmental Studies Program at The Brookings Institution. He is the author of many books, including _Separate But Equal Branches: Congress and the Presidency_ and _An Introduction to Public Policy_.

