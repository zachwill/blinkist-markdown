---
id: 5a11e326b238e10006bd0f37
slug: the-american-spirit-en
published_date: 2017-11-22T00:00:00.000+00:00
author: David McCullough
title: The American Spirit
subtitle: Who We Are and What We Stand For
main_color: C32736
text_color: C32736
---

# The American Spirit

_Who We Are and What We Stand For_

**David McCullough**

_The American Spirit_ (2017) is a collection of five speeches given by historian David McCullough on various occasions, including graduation commencements, Independence Day and the anniversary of the White House. Taken together, these speeches capture the American spirit, and offer a fond look back at its troubled history, as well as the people who have fought to make the nation a better place.

---
### 1. What’s in it for me? Get inspired by the unique history of the United States of America. 

The study of history can be experienced in many forms, from school textbooks and biographies to TV documentaries and feature films. But reliving history via the speeches of one of the United States's greatest historians is something else entirely.

Historian David McCullough, a recipient of the Presidential Medal of Freedom, has given numerous inspiring speeches at some of the most renowned venues in the nation, including the White House and before a joint session of Congress.

While the content of each speech varies, there is one overriding theme: the history of the United States is one that is sure to inspire. And that is precisely what these blinks have to offer; they recall a selection of the greatest, most inspiring stories from the history of the United States.

In these blinks, you'll learn

  * who the first president to live in the White House was;

  * how we can save the American city; and

  * why reading and education are of the utmost importance for the future of the country.

### 2. The achievements of Congress and John Quincy Adams, despite his size, are worth remembering. 

Are you familiar with the _Car of History?_ It's a sculpture by the Italian artist, Carlo Franzoni, and it can be found in the United States Capitol, the home of the US Congress in Washington, DC.

The sculpture portrays Clio, the Greek muse of history, in a winged chariot. The wheel of the chariot is a clock made by Simon Willard, a legendary clockmaker from Massachusetts. This sculpture, with its emphasis on time and its unique location, should serve as a reminder of all that's been achieved in the United States by its many formidable politicians.

One of the most significant of these formidable politicians was John Quincy Adams, the sixth president of the United States, in office from 1825 to 1829. His legacy should always serve as a reminder that a person is more than his appearance. After his four years as president, Adams served another 17 years as a Massachusetts congressman.

Despite being short and appearing quite frail, Adams was no less afraid to speak his mind and fiercely defend his beliefs. As a result, he achieved a great many things.

Adams was an especially strong voice on behalf of science, and he was an important figure in the creation of the Smithsonian Institution. Founded in 1846, the Smithsonian has become a beacon of science and knowledge for curious minds around the globe.

Adams was also a determined ally in the fight for equal rights, and played a vital role in the repeal of the _Gag Rule_, a regulation meant to quiet anyone who protested against slavery.

However, while the legacy of Adams is strong, we must not forget the many other men and women of Congress who battled to improve the lives of their fellow Americans.

With their help, child labor was eliminated in the early twentieth century and, in 1862, the Homestead Act was passed, helping poor farmers buy land and stopping the expansion of slavery.

> _"John Quincy Adams is a reminder that giants come in all shapes and sizes. . ."_

### 3. The American city is central to the American spirit, and we must save it from its current deterioration. 

If someone were to ask you which city best represents American strength and spirit, what would you say?

While it might not top your list, Pittsburgh should rank pretty high in anyone's estimation.

Like all of America's biggest cities, it is a center for both finance and culture, where people can be exposed to great food and art and find career opportunities. It's also a place where the principles of education and science are emphasized.

Just take the University of Pittsburgh; it is a shining example of how a city can benefit from an institute of learning, while the school itself benefits from being in such a great cultural hub. They are both made stronger by one another.

Unfortunately, these days, American cities across the country are slowly deteriorating.

The diseases of violence, crime and drugs have spread, leaving no city uninfected. Poverty also has greatly endangered many, especially children. These afflictions have taken a toll not just on the cities, but the very idea of the traditional American way of life.

On a trip through Pittsburgh, you can't help but ask, why are there so many homeless people? And how did it come to this?

Cities like Pittsburgh can be saved if we properly fund universities so that they can explore the problem in greater depth.

Institutions like the University of Pittsburgh can help us understand why certain problems arise and, more importantly, how we can fix them. With proper funding, in-depth, historical investigations can be conducted, and with the problems located right outside the researchers' doors, proposals for solutions can be discussed and perhaps even implemented.

Financial support could come from anyone interested in a healthy city — corporations, private businesses and even public funds such as _peace dividends_, funds consisting of money that comes from reduced defense budgets.

### 4. Thomas Jefferson captured the American spirit in the Declaration of Independence, which today remains as relevant as ever. 

In Philadelphia, back in 1776, a 33-year-old Thomas Jefferson was preparing himself to write the Declaration of Independence.

Jefferson wasn't alone, however; there were four others in the independence committee, including Benjamin Franklin and John Adams. But Jefferson was the only one being tasked with actually writing out the document by hand.

This was no ordinary legal document, as Jefferson's intent was to somehow capture the American spirit within it.

Jefferson wanted this declaration to convey, in a simple and straightforward manner, that the independence of the United States was common sense. He wanted to ensure that every reader would understand the message and accept it as truth. Amazingly enough, he managed to do just that.

The document perfectly captures the American spirit by declaring America to be a democratic and independent nation centered on equality, with people having a right to life, liberty and the pursuit of happiness..

The document was also written quite broadly, which turned out to be very useful, since it allowed people to one day say that the words "all men are created equal" could be interpreted to include women and black citizens.

Whatever Jefferson's intentions were, he successfully created a document that conveys the American spirit.

Today, this document applies to all Americans and still conveys the empowering idea that independence and democracy are relevant, powerful and meaningful concepts worth defending.

Since he put pen to paper, freedoms such as those regarding speech and religion, and the belief that all are considered equal before the law, have come to be taken for granted by many Americans. But it's important not to forget that these rights and freedoms were once novel.

Nor should we ever forget the fact that a bloody, violent war of independence had to be won for these freedoms to be gained.

### 5. The White House was first inhabited by John Adams, a man worth remembering. 

When you think of the White House, you might just see a symbol, and not the main residence and workplace of dozens of presidents and families throughout the years.

The White House has its own interesting history.

For a long time, Washington, DC, wasn't really a city. When the Capitol and White House were in the midst of being built in the 1790s, the "city" was mostly just swampland with a few houses and hotels.

The second president of the United States, John Adams, was the first to inhabit the White House. But don't consider him lucky — the place was a total mess when he moved in. There were only a handful of livable spaces, and since the plaster was still wet, every fireplace in the big house had to be used to dry out the rooms.

But there is a great deal else John Adams should be remembered for, aside from being the first resident of the White House.

Prior to his presidency, Adams was a key figure in financing the American Revolutionary War that lasted from 1775-83. He did this in part by appealing to the leading statesmen and financiers in the wealthy Dutch Republic.

As president, from 1797 to 1801, Adams made sure that the United States didn't get dragged into France's revolutionary war. Adams feared that their involvement in Revolutionary France's battles against its European enemies would lead to America's own collapse.

Adams should also be remembered as a great talent scout. After all, he was the one who encouraged Thomas Jefferson to write the Declaration of Independence. He was also the one who appointed John Marshall as Chief Justice of the Supreme Court and insisted that George Washington be made the Chief Commander of the Continental Army.

Those three figures are considered founding fathers, and would go on to help shape the United States we know today. Adams recognized their tremendous potential and made sure others did as well.

### 6. We must keep learning, as it can improve our lives and the lives of others. 

Did you know that the bookshelves in the Library of Congress would stretch out 650 miles if they were all placed in a single row? That's a lot of books!

Or how about this one: did you know the index finger of the Statute of Liberty is 8 feet long. That's one giant finger!

Education and learning can lead you to some pretty amazing (and fun) facts. Indeed, what we learn can make all the difference in our quality of life, so it's important to continue reading and learning, with a passion.

Back in 1811, Charles Sumner was a senator in Boston, Massachusetts, and his life story is a great example of the power of education.

Sumner went to Harvard University, where he earned a law degree — but instead of practicing law, he wanted to continue his education. For this, he went all the way to Paris, France, to study at the Sorbonne University. Here, Sumner learned everything from natural history and medicine to classical studies.

But more importantly, he had his horizons broadened.

In Paris, Sumner saw black students being treated in the same manner as white students, and it made him realize that Americans were being taught a great many falsehoods about black people. It was clear: the differences between blacks and whites that he'd been taught about didn't exist at all.

Sumner returned to the United States with this revelation in mind, and when he eventually became a senator, he was in a position to be a leader in the fight against slavery.

This should be a reminder to us all that a continued education is a privilege that we shouldn't neglect. And it makes it all the sadder to read surveys that tell us a third of all college graduates in the country don't even read one book a year — not even a short story, poem or novel.

This needs to be fixed. Education doesn't stop when we graduate from college, because there is still so much to learn. As the life of Charles Sumner shows us, what we discover before, during and after college, and throughout our lives, can change the course of history.

> _"Make the love of learning central to your life. What a difference it can mean."_

### 7. Final summary 

The key message in this book:

**The United States has a long history and one that is packed with people, opinions and achievements — many of which still impact our lives today. Even so, some of the American spirit is currently in decline, and we must take action to remember and preserve it.**

Actionable advice:

**Read a literary masterpiece or two or three.**

To educate yourself and really understand your American heritage, aside from studying American history, you could dive into some of the American literary masterpieces written by the likes of Mark Twain or Ernest Hemingway. You might think of them as old and dusty, but there's a reason they've become classics and are still beloved by many to this day.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Glass House_** **by Brian Alexander**

_Glass House_ (2017) tells the cautionary tale of Lancaster, Ohio, a town that went from boom to bust over the course of the past fifty years. At the heart of this downfall is the Anchor Hocking glass factory, a major source of employment that turned into a bitter disappointment. This story is essential reading for anyone wishing to understand the current state of affairs in American society and politics.
---

### David McCullough

David McCullough is a historian and successful author and narrator. He has received two Pulitzer Prizes, one for his writing about the thirty-third US president, Harry S. Truman, and the other for his biography about the second US president, John Adams.

