---
id: 5e593b046cee07000673bee0
slug: the-anarchy-en
published_date: 2020-03-04T00:00:00.000+00:00
author: William Dalrymple
title: The Anarchy
subtitle: The East India Company, Corporate Violence, and the Pillage of an Empire
main_color: None
text_color: None
---

# The Anarchy

_The East India Company, Corporate Violence, and the Pillage of an Empire_

**William Dalrymple**

_The Anarchy_ (2019) details how the East India Company, an English joint-stock corporation, came to rule the British economy — and the fates of 200 million South Asians. From its founding in 1599 by privateers and pirates to its time as master of the largest standing army in South Asia, the Company fanned the flames of anarchy, then used the resulting chaos as an opportunity to loot an empire.

---
### 1. What’s in it for me? Learn how a single corporation took over an empire in just 47 years. 

You've heard of "too big to fail," right? Well, the British East India Company epitomized this phrase, to a tee. Beginning as a joint-stock company, it transformed from trade entity to political power to territorial conqueror. In pursuit of a singular goal — making its investors rich — the Company plundered millennia of accumulated wealth in the Indian subcontinent in a matter of decades. It caused widespread famine, chaos, and cultural devastation from which the subcontinent never fully recovered. 

The Company succeeded largely because of timing; it received crucial military backing from the British Crown and financial backing from European and Indian financiers at exactly the right moments. Perhaps more importantly, its directors shrewdly took advantage of a series of chaotic power struggles in the constellation of Indian political powers to lead the Company to the top. If you want to understand how a single company came to rule 200 million people, these are the blinks for you.

In these blinks, you'll learn

  * how a country with only 5 percent of the world's population came to rule a country four times its size;

  * how one company reversed the global trade flow; and

  * why "loot" became one of the first Indian words to enter the English language.

### 2. By the eighteenth century, the East India Company had transformed from a trading concern to a militarized entity bent on exploitation. 

The East India Company was founded in 1599 as a _joint-stock company_. The joint-stock company was a sixteenth-century English innovation that allowed collectives of medieval craft guilds to combine their purchasing power. The aim of this new type of corporation was to enrich shareholders and investors. Spurred by lucrative colonial profiteering in the Caribbean, Queen Elizabeth I gave the Company a royal charter, granting it monopoly over trade in the East Indies. 

The first English men stepped foot on Indian soil in 1608. Their mission? Establish trade with the Mughal empire, which had been the dominant political power on the subcontinent for nearly a century. Dripping in jewels and draped in lavish fabrics, the Mughals stunned the Europeans with their wealth and sophistication; these first interactions are the origin of the modern-day word "mogul." 

But it wasn't until 1614 that the Mughal emperor, Jahangir, received English emissary Thomas Roe. Roe tried to focus the conversation on trade, but Jahangir — bored of talking about money — wanted to discuss art and astronomy for hours.

Finally, Roe got permission to build a trading fort in Madras. Soon after, another fort was built in Bombay. And then another, in Calcutta. 

At first, it was a beneficial relationship for everyone. Company trade blossomed, and more European investors soon lined up. Drawn by the stable trade in India, thousands of artisans and merchants moved to the Company's new settlements. By the 1690s — just 30 years after the Company built its first fort — Bombay had grown from a backwater port to a commercial capital that was home to 60,000 people.

Many migrants to the cities were seeking refuge from an increasingly unstable countryside. The Mughal emperor Aurangzeb died in 1707, leaving no clear heir. His rivals pounced. First came the Marathas, Hindu peasant guerrilla fighters. Then Afghan warlord Nader Shah ruthlessly sacked Delhi in 1737. The many wars against these enemies drained the Mughal coffers.

Mughal power was in chaos. The East India Company and its rival French counterpart, Le Compagnie, took advantage by surreptitiously building more forts. When the Mughals tried to punish this impudence, 700 French _sepoys_ — European-trained Indian soldiers — trounced 10,000 Mughals at the Battle of the Aydar River in 1745.

It was now clear that eighteenth-century European military tactics could best anything in the Mughal armory. The English and French trading concerns in India transformed into increasingly bellicose, militarized entities. 

India was primed to be violently transformed into the richest colony the world had ever known.

### 3. As the Mughal empire crumbled, several factions angled for power. 

By the 1750s, the Company was the most advanced capitalist organization in the world, generating an eighth of Britain's import trade. Company officer Robert Clive would go on to oversee its transformation into a political power, usurping the increasingly anarchic Mughal empire.

As a child, Clive had been a violent bully. To keep him out of trouble in England, his father had sent him to India, where he developed a hatred for the country that would stay with him his whole life. 

Amid Anglo-French tensions in India, he began distinguishing himself as an audacious, creative military tactician. In 1745, the Company saw France amassing troops in India as a potential threat. It made Clive the Deputy Governor of Madras, the fort and capital of regional trade, and put him at the head of an army.

Meanwhile, the Company's Indian rivals were scrambling for control of the lucrative Bengal province.

Siraj ud-Daula was the new governor, or _Nawab,_ of Bengal; he was reviled by all as a sadist and megalomaniac. From his private skiff, he would capsize ferry boats just to scare people — most of whom didn't know how to swim. As a young prince, he had volunteered to torture criminals to death, and he was a lifelong rapist. Politically, he was disrespectful to his generals and an important family of financiers, the Jagat Seths. 

Siraj didn't trust the Company. In 1756, he found an opportunity — the Company's unauthorized repairs to the ramparts of Calcutta, its principal Bengal fort — to attack it. 

Across the country, Shah Alam — heir to the Mughal throne — was simultaneously working to consolidate his power. Where Siraj was horrible, Shah Alam was noble. He was an accomplished poet. He was sensitive and intellectual, not to mention handsome. Exiled from Delhi after a coup attempt, he tried to seize power in former Mughal taxation provinces, including Bengal. Supporters and Mughal nobles flocked to the popular Shah.

But both Shah Alam and Siraj ud-Daula were too late. Robert Clive was already poised to snatch up Bengal.

### 4. Clive’s victory at the Battle of Plassey resulted in the Company acquiring political power for the first time. 

With his massive army and bad attitude, Siraj set the stage for a Mughal-Company confrontation that would be a critical turning point in Indian history. In 1756, he showed up at the gates of Calcutta with 70,000 soldiers — surprising the incompetent governor Roger Drake and his army of 515. 

Needless to say, Siraj's troops looted Calcutta. The British prisoners were herded into a tiny cell called The Black Hole, where most of them suffocated to death.

The sack of Calcutta was economically catastrophic for the Company, as well as for its officers whose personal fortunes were tied up in Company shares. Immediate resettlement of the colony was everyone's highest priority. 

Enter Robert Clive, who had just landed with three regiments of British soldiers. The day after the siege, Clive declared war on Siraj in the name of the Company. This was the first time the Company had ever declared war on an Indian prince.

Meanwhile, the Jagat Seth financiers were growing increasingly disillusioned with the unpopular Siraj. They reached out to the Company for support in a potential coup, backing Siraj's Iraqi general Mir Jafar as the new Nawab.

Ever attuned to an opportunity for personal enrichment, Clive accepted the Jagat Seths' bribes. He justified the coup to British command by saying that deposing Siraj — an ally of the French — would be the best way to preserve their monopoly on trade in India.

Clive's forces met Siraj at the Battle of Plassey in 1757, where a sudden monsoon changed the course of the conflict. Company men took care to keep their artillery dry; the Mughals, who weren't accustomed to such weapons, didn't. Siraj was killed at age 25, and the traitorous Mir Jafar was named Nawab. Clive's deal with the Jagat Seths made him one of the richest men in Europe. 

Plassey forced the Mughals into a surrender that complied with all Company demands. In addition to a normalization of trade, the Company got massive spoils and new privileges like landholding rights and a mint. 

Having undermined an empire that had ruled for two centuries, this was the moment a corporation first acquired political power. The Company began a period of unbridled looting and asset-stripping in India.

### 5. After Plassey, Company officers mercilessly looted the countryside. 

The new arrangement between the Company and the puppet Nawab of Bengal was one of convenience — not of mutual appreciation, or even comprehension. Mir Jafar didn't understand he was dealing with a corporate board, rather than a single sovereign ruler. In letters Clive referred to him as "the old fool."

For his part, Robert Clive was smugly reveling in his victory. In 1760, he returned home with a fortune greater than anything Europe had seen since Spanish conquistador Cortès returned from plundering the Americas. He set London gossips atwitter with his shopping spree of estates and townhouses. 

Back in Bengal, Mir Jafar — the new Nawab — was an uneducated soldier from Najaf, Iraq. A brilliant tactician but clumsy leader, he exacerbated the chaos Bengal was already experiencing. He didn't pay his armies, while he wore seven bracelets of different gems on each wrist. Rebellions broke out regularly, among soldiers and nobles alike. The Company further undermined Jafar, flouting agreed-upon trade regulations and exacting taxes from the population by "shaking the pagoda tree" — extorting them via egregious, systematic human rights violations. 

Warren Hastings, a rising star in the Company, was appalled by his colleagues' actions. He recognized that chaos would ensue if Mir Jafar's reign collapsed and devoted himself to preventing it.

Meanwhile, nominal Mughal emperor Shah Alam still roamed the countryside, getting by on his charm and good looks. When he came to Bengal, old Mughal nobility abandoned Mir Jafar and flocked to his side, along with their personal militias. As heir to the Mughal Empire, he was a symbol that still held power in India. As such, he posed a threat to both the Company and Mir Jafar. They tried to capture him at the Battle of Helsa in 1761, but the lucky Shah escaped. 

Mir Jafar, though, had run his course. The Company installed his son-in-law Mir Qasim, who was better suited to the role, in his place. He quickly put Bengal in better order than it had been for years — but he was less pliable to Company direction and less inclined to overlook its traders' flagrant behavior. 

When the Company did nothing in response to Mir Qasim's complaints about its representatives, he abolished all taxes and customs duties as a last ditch effort to level the playing field between Company men and local traders. What's more, he started seizing Company property.

The Nawabs and the Company were soon at war.

### 6. At the Battle of Buxar, the Company defeated three Indian powers and set the stage for the territorial conquest of the whole country. 

Like his father-in-law Mir Jafar, Mir Qasim was a clever military tactician. In advance of the coming battle with the Company, he allied himself with Mughal emperor Shah Alam — whose power was mostly symbolic — as well as with the Nawab of the neighboring Avadh province, Shuja ud-Daula — whose power was very real. Shuja was huge in stature and immensely strong; by 1763 he was past his prime but could still sever the head of a young bull with one stroke of his sword.

Despite almost besting the Company at a siege in Patna, Mir Qasim and his assembled armies lost their ultimate battle at the Buxar river in 1764. Mir Qasim fled; he later died in poverty. Shuja was installed as the Company's puppet. Shah Alam, knowing his symbolic value, allied himself with the Company. 

Having defeated the three remaining Mughal armies, the Company was now the dominant force in northern India. This moment laid the groundwork for the Company's territorial conquest of the whole South Asian subcontinent. 

After the debacle of the siege at Patna, uneasy Company investors unanimously voted to send Robert Clive — who had recently been made an MP and lord — back to India to secure their investments. 

Clive knew that the most important thing to Shah Alam was reinstatement to his throne, and he promised to support the emperor in this goal. In exchange, Clive demanded the Diwani — the office of economic management of the Mughal provinces, including the cash cow Bengal province. Having control of the Diwani effectively meant having total control over the region.

Shah Alam agreed. In 1765, in exchange for 2.6 million rupees and Clive's cynical promise that he would govern according to the "rules of Mahomed," Shah Alam handed over financial control of all northeast India. The British would go on to rule Bengal for the next 200 years.

"Nothing remains to [the Bengal Nawab] but the Name and the Shadow of Authority," wrote Clive.

From then on, India was treated like a huge plantation, with all profits shipped overseas to London. Nothing was left behind for the average citizen. Centuries-old artistic traditions died with their last practitioners; there was no middle-class market for their crafts. Anyone who refused to work below market rate was violently punished.

For the shareholders, it was a boon. The Company's share price doubled in eight months. 

But for the people of Bengal it was a catastrophe. And the worst was yet to come.

### 7. Distracted by economic turmoil and executive infighting, the Company was unprepared for its worst military challenge in decades. 

In 1770, catastrophe struck. A two-year drought made the price of rice skyrocket. Bengal was racked with famine. The Company did almost nothing to help. In fact, it continued to enforce tax collection — and in some cases even increased it. All in all, 1.2 million Bengalis died of starvation or disease.

As word of the horror made it back to England, public opinion of the Company soured. Then, in the European financial crisis of 1772, Company stock plummeted. Company directors were forced to ask the Bank of England for a bailout of over £1.5 million. 

It had become clear that Indian wealth was propelling the British economy. Parliament bailed out the Company because it was simply too big to fail — it generated half of Britain's trade. There was also a clear _quid pro quo_ : 40 percent of MPs owned Company stock. 

Robert Clive was interrogated in Parliament but ultimately avoided official censure. However, he was now a deeply unpopular figure in London, parodied in the press as "Lord Vulture." In 1774, at the age of 49, he killed himself. 

Back in India, Warren Hastings — that principled Company officer who had tried to bring the worst offenses in line — was put in charge. His deputy was Philip Francis. 

The two got off to a bad start. For one, Francis was offended that Hastings didn't wear a ruffled shirt to their first meeting. What's more, Francis hated India and deeply mistrusted Hastings's obvious appreciation for the country. Their ensuing power struggle paralyzed the Company's governance in India.

Two of the Company's old foes took note of the gridlock in Bengal: the Marathas and the Sultan of Mysore, Haidar Ali, along with his ferocious son Tipu Sultan. Each had a secret weapon. The Marathas had the brilliant general Nana Phadnavis — the so-called Maratha Machiavelli. The Mysore Sultans had French military training and weapons. 

While the Marathas and the Mysore Sultans were forging a dangerous alliance, the squabble between Hastings and Francis reached a climax; neither saw the storm coming. Soon they were at war with the Indian alliance. At the Battle of Pollilur, the Triple Alliance — Maratha, Mysore, and France — delivered a devastating blow to the Company. 

But over the next few months, Hastings made an astonishing comeback through a combination of military action and diplomacy. This led to the Treaty of Salbai, which he signed in 1782. 

Never again would the Company's enemies have such an opportunity to defeat them.

### 8. Shah Alam’s desperation to return to his ancestral throne and his reliance on the Marathas ended in disaster. 

By 1771, Shah Alam had had enough of the Company's and the Nawabs' vain promises to install him back on his ancestral Peacock Throne in Delhi. He knew his worth as the symbolic leader of India, and he was determined to go home.

He made an alliance with the Marathas, who helped him raise an army of 16,000 to escort him on the dangerous journey back to Delhi. The Marathas also promised to support him once he settled on the throne, in a court filled with intrigue and potential usurpers. 

The march back to Delhi was almost too easy. Shah Alam reentered the city and reclaimed the Peacock Throne. But soon, he began to feel dissatisfied with his achievement. With the help of his new secret weapon — the intelligent and capable general Najaf Khan — he reasoned that he could also easily retake his ancestral tribute lands, which had stopped sending him tithes amid the chaos. 

In 1772, the Shah went to war to get his lands back, and soon he was sending a steady supply of spoils back to Delhi. Shah Alam put the funds toward a revival of Delhi's cultural scene, which had lain dormant in the chaotic years since he'd left. He also took care of his growing court, which included a new adopted son, Ghulam Qadir. This relationship would come to define the rest of his life.

Shah Alam doted on the boy, who was by all accounts unexceptional — aside from his rugged good looks and unbridled arrogance. This gave rise to court gossip that Shah Alam had taken the boy as his nonconsensual lover. It's unclear if this was true, but whatever happened between them, Qadir felt begrudged. 

Najaf Khan's untimely death in 1782 threw Shah Alam's court into turmoil. Without the spoils of war, he became totally reliant on the Marathas, whose priorities were elsewhere. Sometimes his family didn't even have food to eat because the Marathas hadn't provided a palace budget.

Amid the chaos in 1788, Ghulam Qadir carried out his terrible vengeance. He attacked Shah Alam's court, raped and murdered his household, looted the palace of all remaining treasure, and cut out the old emperor's eyes. 

The Marathas swept in and saved Shah Alam's life, but it was too late; the emperor was left broken and in despair. The last Mughal lion had lost his teeth.

### 9. Consolidating its finances and bureaucracy allowed the Company to defeat its remaining challengers. 

In 1776, the American Revolution shook colonial Britain. Neither the Company nor the Crown could afford the same thing to happen in India. They sent General Cornwallis, who had just been defeated in America, to ensure India remained under British control. 

Cornwallis arrived to boomtown Calcutta — population 400,000 — in 1786. The Company was back in business after a period of uncertainty, which meant it was able to groom its sizable army.

The British worried about the emergence of a settler colonial class descended from Europeans that could threaten British rule, as it had in the Thirteen Colonies. Cornwallis instituted baldly racist legislation preventing people with mixed European and Indian ancestry from holding Company positions and owning land. A pro-British Hindu service gentry emerged, supporting the British in public offices.

Influential and vastly wealthy families of Indian financiers also allied themselves with the Company. Ultimately it was the unlimited lines of credit extended by the financiers that allowed Britain to trounce any remaining Indian resistance.

The first of the last challenges came from Mysore.

The ruler of Mysore, Tipu Sultan, had inherited his position from his father, Haidar Ali. But while he had brought wealth to Mysore, he lacked diplomacy and brutally dealt with his enemies — including the Marathas. 

In 1791, Tipu engaged the Company in the Third Anglo-Mysore War. Things started off well for him. His armies were extremely agile compared to the British — whose officers each traveled with a minimum of six servants, a complete set of camp furniture, 24 suits, and more. 

It took the personal intervention of Cornwallis, leading Maratha reinforcements, to seal the deal for the British. As the price of surrender, Cornwallis exacted half of Tipu's territory. With this chunk of Mysore, the Company was the majority territorial power in India.

When the Company replaced Cornwallis with Richard Wellesley, though, Tipu saw an opportunity. He successfully made overtures to Napoleon, but the French-Mysore alliance was short-lived. When Napoleon's fleet was destroyed in Egypt, Tipu was left high and dry. He had neither allies nor money to support his army. By contrast, the Company had unlimited credit lines from its Indian financier allies.

But Tipu was ready to take on the British, come what may. He died with his sword in his hand, in the Fourth Anglo-Mysore War, in 1799. When Wellesley heard of Tipu's death, he raised his glass and said: "I drink to the corpse of India."

### 10. For the Company to finally secure the support of Mughal emperor Shah Alam, it had to go through the Marathas and the French. 

The last Indian challengers to Company dominion were the mercurial Marathas, who had been both allies and foes of the British over the decades. With the support of the French, the Marathas had held onto Delhi — as well as the blind and unhappy Shah Alam, nominal Mughal emperor, whose sole remaining pleasure was writing poetry. 

In 1800, the death of Maratha politician Nana Phadnavis brought chaos. "The Maratha princes," writes one modern observer, "bore less resemblance to a confederacy than to a bag of ferrets." 

Amid the chaos, three teenage princes emerged to prominence, each at the head of his own militia. But at a time when an alliance was needed the most, all the three of them could do was squabble. Eventually, in 1802, Wellesley succeeded in getting one of them, Baji Rao, to sign an alliance binding his followers to the British.

The Company's army was flourishing, having grown to 155,000 soldiers. Employing the Indian financiers' lines of credit, Wellesley was able to pay his army fully and on time. The Marathas, by contrast, were all but bankrupt after decades of civil war had devastated their tax base.

Wellesley schemed to fight each Maratha prince individually, but the two remaining princes managed to join armies. In 1803, they challenged the Company at the Battle of Assaye. Later, Wellesley would say they presented a greater challenge than Napoleon did at Waterloo. Ultimately, though, the Company won the day. 

The Marathas had been defeated forever.

Their French backers, on the other hand, were still kicking. Shah Alam knew the Company wouldn't leave the French to their own devices in India. When he learned the Marathas had been defeated, the old Shah made an overture to Wellesley, choosing the British as allies over the French. It was his last act in a career of survival diplomacy.

The last time the British faced the French in India was at the decisive Battle of Delhi, in 1803. British victory meant that India would remain in British hands for another 144 years. 

Shah Alam, aged 77, listened to the Battle of Delhi from the roof of the Red Fort — the city's historic Mughal stronghold. When it was over, he presented himself for a formal transfer of power. He was a pitiful figure, blind and impoverished. Finally, after a long and bitter life, his struggle was over.

### 11. Philip Francis launched a smear campaign against his old enemy, Warren Hastings; it failed and instead led to the downfall of the Company. 

In London, former Company governor Warren Hastings was impeached by Parliament in 1788. He, and by proxy the Company itself, was accused of nothing less than the "rape of India." Practically speaking, these crimes included extortion, cruelty, and abuse of power. Company rule, argued the prosecutor, had done nothing for India but asset-strip it, impoverishing and destroying its population in the process.

The trial spectators — including the Queen, as well as commoners who had slept on the streets to catch the scandalous proceedings — were convinced of the Company's guilt. The trouble was, they had impeached the wrong man.

As a Company official, Hastings was no angel. But over his career he had worked harder than anyone else to rein in the worst excesses. His impeachment was the fruit of a systematic smear campaign by his old nemesis Philip Francis, who had tirelessly lobbied Parliament for years to bring Hastings down. 

After a 7-year trial, Hastings was cleared of all charges. But it had sapped his strength at the end of his life, and he never fully recovered. Still, the trial was useful in the sense that it laid bare the Company's corruption and misdeeds.

From Hasting's trial until the outright nationalization of the Company in 1858, the British government took an ever more active role in governing what had become its most important colony. The public began to question how Britain had ever allowed India to be run by a corporate board based on another continent.

Parliament agreed. In 1813, it abolished the Company's monopoly of trade with India. This allowed other merchants, agencies, and corporations to set up trading concerns in Calcutta and Bombay. It also set the stage for an impassioned fight over whether the Company should be allowed to exist at all. 

The final outrage came in 1857. In the bloodiest episode in the history of British colonialism, the Company's private army murdered tens of thousands of suspected rebels in towns up and down the Ganges River. 

Two years later, Parliament voted to disband the Company's private army and navy. All Company territories passed to the Crown. This made Queen Victoria the ruler of India, rather than a board of directors. The Company's charter quietly expired in 1874.

### 12. Final summary 

The key message in these blinks:

**The eighteenth century was a period of unrest in India. Through military innovation, Parliamentary backing, and financial support from both European and Indian financiers, the East India Company emerged from the anarchy as the sole economic, political, and territorial ruler of an entire subcontinent. In their quest for power and personal fortune, the Company and its officers pillaged the country — causing widespread death, devastation, and cultural upheaval.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _The Heartbeat of Wounded Knee_** **, by David Treuer**

You've just learned how one company, in just 47 years, usurped an empire that had been in place for centuries. Author William Dalrymple's revised narrative of this famous history earned _The Anarchy_ a spot among Barack Obama's top books of 2019.

Another book on Obama's 2019 list that revisits a controversial narrative is _The Heartbeat of Wounded Knee_, by David Treuer. With his account of the modern Native American experience Treuer seeks to establish a new narrative of Native America: resistance and resilience that is very much alive. So if you're interested in Native American history, head over to our blinks to _The Heartbeat of Wounded Knee_, by David Treuer.
---

### William Dalrymple

William Dalrymple is an acclaimed Scottish travel writer and historian whose work focuses on South Asia and the Middle East. In addition to writing over a dozen award-winning books, he has created TV series, curated museum exhibits and music compilations, and received honorary doctorates from three universities. He has lived in India on and off since 1989.

