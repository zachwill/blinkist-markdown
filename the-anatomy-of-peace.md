---
id: 59450deab238e10005eece5e
slug: the-anatomy-of-peace-en
published_date: 2017-06-21T00:00:00.000+00:00
author: The Arbinger Institute
title: The Anatomy of Peace
subtitle: How to Resolve the Heart of Conflict
main_color: 1F629C
text_color: 1F629C
---

# The Anatomy of Peace

_How to Resolve the Heart of Conflict_

**The Arbinger Institute**

_The Anatomy of Peace_ (2006) addresses an unpleasant fact: how common it is for people to resort to conflict as a way of resolving differences at home, in the workplace and in the world at large. Learn the reasons behind this mindset and how we can find a better way to move forward. There's no reason to think things have to be the way they are today. With a little understanding, we can all choose the path of peace.

---
### 1. What’s in it for me? Find out how – and how not – to resolve conflicts. 

Life would be so much easier if people would only stop being so difficult! Pretty much everyone has, at one time or another, been entrenched in a conflict with a troubled teen, a disenchanted spouse or a pestering neighbor. And, as you probably know, confronting the difficult person usually leads to all the blame being put on you.

Truth be told, it's difficult to sustain a conflict when one person chooses not to play along. So, if there are feuds or quarrels in your life, you're at least partially responsible. But how can you pinpoint where your responsibility begins? And how can you remove yourself from conflict? That's what these blinks are about.

As you'll find out, even your well-intentioned efforts — trying to help someone overcome a drug habit, for instance — can result in a never-ending cycle of reproach and resistance. The crucial part in this is your mindset or, more specifically, how you see and address people. This is good news, because your mindset is something that _you_ can change.

In these blinks, you'll discover

  * how the sultan Saladin won a war with a peaceful heart;

  * what it means to be in an "I-deserve box" and why that's a bad thing; and

  * why your partner never does what you tell him or her to do.

### 2. There are two ways to view those around you: with either war or peace in your heart. 

Let's imagine that you were raised to fear and hate left-handed people, because two thousand years ago a group of left-handed people attacked your family's right-handed ancestors. 

But one day, you notice a man leaving a store that sells left-handed goods. As he's in the middle of crossing the street, he falls and drops his bag, spilling his belongings. What do you do?

There are basically two options: you can have a heart of peace or a heart of war.

To have a heart of war is to see the person in need of help as inferior, or as an object rather than a human being, which is a sure way to perpetuate conflicts.

Your inner-voice may tell you not to help him because you were taught that his people hate your people. But this frames him as only being part of a group or concept, rather than recognizing him as an individual.

The heart of war suppresses our sense of compassion and urge to help; it's the kind of mindset that creates and perpetuates hate, conflict and war.

The better option is to have a heart of peace and listen to your sense of compassion — to see those around you as human beings.

Even if you are forced into a war, you can still treat your opponent with compassion, which is what the sultan Saladin did in the twelfth century after the Crusaders massacred the people of Jerusalem.

Eventually, Saladin's Muslim army retook the city from the Christians, but he did not slaughter the innocent, as the Christians had a century before. He forbade his people to harm them, provided them with safe passage and even kept the city open to Christian pilgrims in the years that followed.

This is what a heart of peace looks like: you choose compassion and treat others as human beings, burdened by their own fears and desires.

Once this happens, others will be more likely to treat you the same way, increasing the likelihood of peace.

> _"When we start seeing others as objects, we begin provoking them to make our lives difficult."_

### 3. We actively perpetuate conflicts by refusing to accept other perspectives. 

No one enjoys getting into an argument with their spouse, child or coworker. Yet all too often we end up starting and restarting conflicts by engaging in the very behaviors that we don't like. 

This is what happens when we have a heart of war and see those around us as enemies. We go around thinking everyone is trying to cheat us or work against us — a mentality that inevitably leads to conflict.

Making matters worse, this approach to life results in our attracting like-minded people who support our mentality, which only draws more people into the conflict.

We can see this play out on the world stage as well, with groups of people being demonized and categorized as "others" who can only do harm.

Many of these conflicts escalate to the point where solutions are virtually out of reach.

Even when negotiations can be arranged, one nation will often believe that the talks were actually set up as a subversive trick. This is basically the situation that Israel and Palestine find themselves in.

And when two people have a heart of war, it can result in _collusion_ — when two conflicting parties inadvertently perpetuate the conflict.

This happens when two people deny the fact that the other person can have a different yet equally valid perspective. Furthermore, they'll refuse to admit that their assumptions may be wrong and insist that they're always right. All of which means that negotiations, and any hope of finding common ground, are repeatedly crushed. 

This happens in the home as well, when parents insist that they always know what's best for their children.

But let's not forget that children are individuals with their own thoughts, just like anyone else. And they actually know a lot about their own needs, which we can learn about if we treat them respectfully and listen to their perspective.

> The 2013 Executive Coaching Survey by Stanford University found that the biggest challenges for CEOs are their conflict-management skills.

### 4. Instead of trying to change someone, improve things by changing the environment. 

When was the last time someone said you did something wrong? How did you react? 

Oftentimes, people will try to change a person when they find something wrong, but this only ends in further conflicts.

Let's say your partner hasn't done his or her share of the chores in the past month. Your natural inclination is to say something about it, but that doesn't mean you should try to force your partner to behave or think in a certain way.

This will only be taken as criticism — something nobody likes. Indeed, criticizing usually only makes matters worse.

Whether you realize it or not, your partner has a reason for thinking or behaving as he or she does. So try to listen to your partner, to relate to whatever he or she has to say about the issue at hand.

Remember, we can't force people to change, but we can create an environment that will invite them to change.

Let's take a more serious problem and say your partner is addicted to drugs or alcohol. Instead of trying to stop the behavior by force, you can improve a vital part of the environment by reaching out and being compassionate, which will strengthen your relationship.

This will work far better than blaming or trying to change your partner, because now the drugs or alcohol won't be used as a way to prove that you don't have all the control. Once you move away from arguing about who's right and who's wrong, you can start listening to each other and find out what your partner needs to feel better.

Similar situations can arise in the workplace when employees are discontent and unmotivated.

This can happen when individuals are being isolated for praise or blame, which can divide people and create envy. Again, rather than trying to fix individuals, create a harmonious work environment and always listen to what your employees have to say.

> _"A heart at war needs enemies to justify its warring. It needs enemies and mistreatment more than it wants peace."_

### 5. We justify our behavior by putting ourselves in boxes, but this can be avoided. 

The human capacity for self-delusion is spectacular. How often have you convinced yourself that some hurtful actions were completely justified?

One strategy that we use to justify our poor behavior, and to blind ourselves to the perspectives of others, is to create boxes for ourselves.

One of the more popular boxes is the _Better-than Box_.

This is built when we tell ourselves that we're more special, gifted or talented than others. As a result, we'll look at others as being "less-than" we are — less important, worthy or respectable. This outlook justifies our treating others poorly.

Or we can put ourselves in the _Victim Box_, which works in the other direction, causing us to see everyone else as privileged and the world as a bad or unfair place.

Since we have it so bad, we take it out on everyone else around us, and we're justified because the world has shortchanged us and we deserve so much more than we get. Naturally, this can also be called the _I-Deserve Box_.

Building a box not only justifies unpleasant behavior. It also prevents us from seeing anything other than our own perspective, which is how conflicts are created and perpetuated.

But we can get out of these boxes and approach the situation in a new way.

One of the best ways to escape the box is to think of these situations from the perspective of others.

Does your behavior seem justifiable when you put yourself in the other person's shoes? If you tend to treat the customers at your job with disrespect, just imagine going to a business and receiving the same service you provide.

Once you step out of your box, you can start to see the world with some much needed perspective.

In the end, whether you're surrounded by war or peace depends on how you deal with situations and where your heart truly lies.

If your life needs some change, the first step is to leave your box. Then you can start seeing the world with clear eyes and a heart full of peace.

### 6. Final summary 

The key message in this book:

**Our normal approach to conflict is to fight for our position and correct the other person. But this doesn't lead to peace and success; it only perpetuates the chance of further conflict. Instead, we have to change the state of our heart and how we view the people around us. The path to peace is to overcome the justifications we create for our poor behavior and to see others as individual human beings.**

Actionable advice:

**Get out of the** ** _Better-than Box_** **!**

Sometimes we won't help someone because we tell ourselves it wouldn't be polite or we don't want to create an embarrassing situation. But this can also be a different way of us staying in the _Better-than Box_ and seeing the other person as a lesser-than who doesn't deserve help. Don't prevent yourself from humanizing those around you. Once you see others as people with feeling and emotions, you'll be more willing to help, and it might be the event that changes their lives for the better.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Outward Mindset_** **by The Arbinger Institute**

_The Outward Mindset_ (2016) explains how changing your perspective can transform the world. These blinks discuss how a focus on personal needs obscures the needs of others and how, by identifying collective goals, people can make powerful collective changes in society.
---

### The Arbinger Institute

Founded by scholars in 1979, the Arbinger Institute is dedicated to spreading a message of peace through books, trainings and other means. The Institute has over 300 members, including coaches and staff members who work in offices located in eighteen different countries.

