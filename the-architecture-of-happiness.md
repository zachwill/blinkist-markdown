---
id: 5885e0f65806db0004d7fec1
slug: the-architecture-of-happiness-en
published_date: 2017-01-26T00:00:00.000+00:00
author: Alain de Botton
title: The Architecture of Happiness
subtitle: None
main_color: 735D89
text_color: 735D89
---

# The Architecture of Happiness

_None_

**Alain de Botton**

_The Architecture of Happiness_ (2006) is about how humans relate to architecture and design. These blinks demystify the power of architecture by explaining why different people prefer specific buildings, how design speaks to us and how we can use architecture to bring out our best.

---
### 1. What’s in it for me? See what architecture evokes in you. 

Since the Tower of Babel and the construction of the first Egyptian pyramid, architecture has been considered not only a craft, but an art — and where there is art, there is beauty.

Today, we can appreciate the architectural beauty of ancient temples, medieval cathedrals, bourgeois mansions and modern skyscrapers. But what defines architectural beauty? What makes us admire buildings, and what does our sense of architectural beauty tell us about ourselves?

In these blinks, you will see how buildings evoke memories, how they remind us of people and how they speak to us. You will learn about architectural beauty and how architecture can bring out specific aspects of your personality.

You'll also learn

  * why socialist activists do not eat with ornate crockery;

  * how a whitewashed loft may tame anarchistic thoughts; and

  * why we arrange the wooden planks on our floor in grid-like patterns.

### 2. Standards of architectural beauty have changed over time. 

It's common for one person to find a building stunningly beautiful while another thinks it's the most atrocious thing they've ever seen. But are there rules that define architectural beauty?

Well, historically there certainly have been. For centuries, the standard of architectural achievement was called the _classical style_, a form that endeavors to reproduce the main characteristics of Greek buildings. Just think of the Athenian temple, with its wide, symmetrical facade, finely detailed columns and repetitious geometric shapes.

The Romans were deeply inspired by this architectural style and applied the principles of Greek architecture to their own cities. Then, nearly 1,000 years later, the classical style was brought back into the limelight by the Renaissance class of Italy.

From there, it spread like wildfire across Europe and even to the United States. For instance, Thomas Jefferson's campus at the University of Virginia, built in 1826, displays a distinctly Roman style.

While classical architecture returned to become wildly popular long after it first appeared, in the 1800s, it wasn't the only legitimate standard of architectural beauty. There was also the Gothic style, which originated with the castles and cathedrals of the Middle Ages, and which experienced a revival in the late-eighteenth century.

This came about because of Horace Walpole, the son of then-British Prime Minister, Sir Robert Walpole. Between 1750 and 1792, he built a massive Gothic residence for himself in London's Strawberry Hill. With this resurgence, architectural beauty remained fixed within the categories of either classical or Gothic for some time. The furthest divergence came from certain architects who combined the two styles in one project, only to face harsh criticism.

But that all changed when industrial engineers formed their own ideas about architectural beauty. This transformation came about with the new machinery of the Industrial Revolution, which gave engineers more and more influence over new buildings' design.

Their opinion was that buildings should be as efficient as possible. For instance, the ideal bridge would be the one that was lightest, cheapest and longest. For some, like the Swiss-French architect Le Corbusier, efficiency and simplicity were the essential elements of architectural beauty.

### 3. Objects and buildings speak to us, reminding us of people and evoking memories. 

So, there are different styles of architecture, each with its own set of rules. But why do we enjoy the aesthetics of some buildings and objects more than others?

One reason is that any designed object, whether it's a teacup, a building or a chair, speaks to us about values, both psychological and moral. Just consider how a plain Scandinavian crockery set might suggest a modest and graceful lifestyle, while an ornate one would imply a ceremonial lifestyle focused on class.

Simply put, we find a piece of design attractive or beautiful if the values and lifestyle it conveys appeal to us. In this way, a socialist organizer might find the ornate crockery repulsive as it smacks of wealth inequality.

Beyond that, the way objects and buildings are designed can also remind us of personality types. This should come as no surprise, since making human associations with objects is a natural tendency. Just consider the tall, thin, pointed arches of a Gothic cathedral; they might remind you of an intense, brutal person who's prone to emotional outbursts.

On the other hand, a round, wide, spacious archway built into a classical facade might remind you of someone who is sturdy, willing to stand up to the challenges of life unflinchingly and who shows little emotion or enthusiasm. In the same way, as we choose objects based on our values, the buildings we gravitate toward are generally those that describe the type of person we're most attracted to.

And finally, buildings also evoke memories. For example, you might find Gothic architecture unbearably ugly because it reminds you of the disgusting cafeteria at your elementary school. Or, you could find a tall, narrow window beautiful because it reminds you of the slender gateways of the ancient Egyptian temples you saw during a wonderful family vacation.

> _"There are as many styles of beauty as there are visions of happiness."_ — Stendhal

### 4. Buildings bring out specific aspects of our personalities. 

Now you know that the architecture you find beautiful is the architecture that reflects your values — but why should you care whether your environment is in step with your beliefs?

Because your surroundings can help bring out parts of your personality. This is important since humans have loads of intricately connected psychological layers, and some aren't so readily accessible.

For instance, you can't simply tap into your spontaneous, creative nature anytime you want. Rather, you need to be in the appropriate environment to get creative; a beautiful park is naturally more inspiring than a grimy public toilet.

This simple fact has already been put to use in a myriad of ways. For instance, religious architecture employs the above principle to bring out people's spiritual side.

Religious architects, knowing that people's surroundings influence them, wanted to use their buildings to communicate information about their religious ideology of worship, spirituality and contemplation. So, they built cathedrals with high ceilings, vaulted archways and sunlit stained glass to help foster the spiritual and contemplative sides of the people who would enter them.

These design elements work because beautiful high ceilings suggest a greater power, while the light shining through stained glass evokes the light people see when passing into the afterlife. Many people who have walked through a stunning cathedral can attest to the fact that being in such a space really does make Christian beliefs feel more plausible.

But these techniques weren't just applied to Christian architecture. Muslim builders also used geometry to evoke God. The guiding principle for these architects was that, when an observer witnessed the impossibly symmetrical, geometric patterns of a mosque's mosaic, they would feel moved by God's perfection.

And even secular homes make use of such strategies to bring out certain aspects of one's personality. Just consider how an orderly, bright and comfortable home can accentuate your calm, authentic and patient side. Such an environment might serve an essential function as a place of refuge after a long, stressful day at work.

### 5. Architecture has been used to falsely project ideals, but it can also improve people. 

Buildings can influence the way we feel and behave, which also explains why architecture has been utilized throughout history as a means to show off, and for people to project a particular image of themselves to the outside world.

For instance, a classical mansion with a grand facade and massive, stoic columns can evoke classical ideals of dignity, nobility and grace. As a result, people who see such a mansion may well associate its owner with classical values.

But we all know this is just an illusion. Living in an ostentatious classical mansion can't magically bestow classical values upon its inhabitant, just like owning lots of books doesn't necessarily mean you possess all the knowledge they contain.

However, that's not to say architectural ideals can't help us improve. While a home built according to classical ideals might not magically make you noble, it still has the power to bring out and encourage the more noble side of your personality.

For instance, your classical surroundings might inspire you to live a more dignified life. Or, in your darker hours, could remind you of the noble ideals to which you aspire.

As such, it still makes sense to design your surroundings based on ideals, which explains why architecture continues to be rife with them. But what types of ideals do we look for in architecture?

Simply put, ones that improve us. One theory holds that people want to be around buildings that represent qualities they feel themselves to be lacking.

For example, an impeccably put-together, whitewashed loft might be home to a person who's desperately working to tame their unusually powerful anarchistic thoughts. In contrast, someone living in a rough, black brick building with plain steel doors might be fleeing the guilt he associates with the excess of his society.

### 6. We like order, but not too much of it. 

At this point, we've learned that people find beauty in all different forms, which largely have to do with their own ideals. But despite this subjectivity, is there any one aspect that could serve as a universal standard of beauty in architecture?

Well, one thing that all humans need is at least a little bit of order. This need makes sense since order appeals to our most rational selves; we appreciate order because we understand that nature could never produce it.

In this way, order protects us from the confusion of the world. For instance, a neat Parisian boulevard reminds people that the world isn't all chaos and anarchy.

However, too much order can also lead us astray. So, while a single block of carefully laid-out apartments might be appealing, a whole district of identical buildings starts to look pretty ugly. When taken to such extremes, people can start to find such symmetry boring rather than beautiful.

This is precisely why order requires balance, and complexity serves this function by helping us appreciate the order that already exists. Just take a wooden floor in which each of the planks is unique, but the whole thing is arranged in a grid-like pattern.

The whirls and eddies of the wood remind us of the beautiful complexity that nature breeds, while the pattern helps tame that complexity, making it neither overwhelming nor chaotic. By contrast, a wooden floor composed of random piles of untreated tree trunks would certainly not be convenient, but it wouldn't be attractive either; after all, it would be an instance of pure chaos rather than an ordered complexity.

In this way, balance is an architectural virtue in and of itself that applies to every aspect of the field. While we need materials to be balanced, such as an appropriate ratio of brick to wood, we also need balance in dimensions, since correct height and width are essential to the aesthetics and structure of a building.

> _"Architecture should have the confidence and the kindness to be a little boring."_

### 7. We appreciate architecture that’s elegant, coherent and which indulges our human nature. 

Elegance is a much-loved trait among humans, whether it comes in the form of a beautiful but simple garment, a pair of shoes or a thought. It should thus come as no surprise that people appreciate elegant buildings, too. But what makes architecture elegant?

Elegance in architecture often comes about as a result of making a difficult task appear simple. For instance, we prefer a sleek bridge with a minimum of visible supports to a clunky heavy one, because the slender bridge makes this feat of engineering appear easy; thus, it strikes us as more elegant.

The same principle can be applied to writing. Just take the phrase "seize the day," which is much more elegant and attractive than "you should try and make the most of the time you have by enjoying yourself." Both expressions convey more or less the same idea — but the former does so with much greater elegance.

So elegance is important, but it isn't all we want from architecture. We also look for coherence, that is, for buildings that combine styles in a reasonable way.

Many people would consider a tall, yet wide tower with thick white bands across its horizontal axis unattractive. But why?

Because the horizontal visual element is at odds with the building's main feature, its height, which is why attractive skyscrapers accentuate this defining quality. Just take the massive buildings of New York, which feature tall windows and pointed roofs.

Finally, humans desire surroundings that understand us. After all, we're complex creatures with eccentric and often subtle needs. As a result, architecture that ignores our quirks tends to disappoint.

For example, a city that's designed in a perfectly rational manner with clearly defined residential, commercial and industrial districts, all connected and separated by long avenues, will tend to deprive us of the unexpected pleasures daily life can offer. Instead, we prefer surroundings that remain sensitive to our nature in a way that a purely rational plan cannot.

### 8. Final summary 

The key message in this book:

**Architecture and design have a much greater impact on our well-being than we might think. By paying closer attention to our surroundings and understanding their effects, we can deepen our self-understanding and improve our lives.**

Actionable advice:

**Dare to care!**

New buildings too often follow dull, tried and tested architectural designs; a perfect example would be the identical legions of American suburban homes. Knowing how such monotony can affect us as people, we should dare to think more creatively. We can do both ourselves and the earth a favor by creating beautiful and intelligent architecture.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The News_** **by Alain de Botton**

_The News_ (2014) reads between the lines of the constant stream of today's news — news to which many readers are becoming increasingly indifferent. This indifference isn't so much the reader's fault as the media's. Constant competition in a crowded market results in news outlets failing to package stories in a way that's appealing, engaging and, most of all, informative.
---

### Alain de Botton

Alain de Botton is a Swiss-born author and television presenter based in London. He published his first book, _Essays in Love,_ at the age of 23 and, in 2008, co-founded the School of Life, an organization focused on emotional education, particularly in the areas of work and relationships.

