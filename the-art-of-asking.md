---
id: 559256cb65613800071b0000
slug: the-art-of-asking-en
published_date: 2015-07-01T00:00:00.000+00:00
author: Amanda Palmer
title: The Art of Asking
subtitle: or How I Learned to Stop Worrying and Let People Help
main_color: 6A323F
text_color: 6A323F
---

# The Art of Asking

_or How I Learned to Stop Worrying and Let People Help_

**Amanda Palmer**

_The Art of Asking_ (2014) is Amanda Palmer's personal account of how she developed her philosophy of asking, sharing and connecting. Through these blinks you'll gain an in-depth understanding of how to accept help, reciprocate the generosity of others and build a tight-knit, family-like fanbase to support you in whatever you do.

---
### 1. What’s in it for me? Find out how Amanda Palmer reached her dreams, and what advice she has for you. 

Whether you're a Dresden Dolls fan, or know of her from social media or any of her captivating and sometimes controversial side projects, once you've heard of Amanda Palmer, you always want to know what she's up to. Her tweets, shows and interactions have reached millions of people — and most of them are now fans and supporters.

But in this book, she explains that simply performing and tweeting isn't the whole story. In fact, building a successful career as an artist and performer has just as much to do with knowing how to ask for things, network and treat people right. These blinks explain Palmer's journey from street performer to world-renowned artist, and show how we, too, can achieve what we desire.

In these blinks, you'll learn

  * how Palmer orchestrated the greatest success in music crowdfunding ever;

  * what prompted someone to send her a death threat; and

  * how sometimes just seeing someone is the greatest gift they can receive.

### 2. By accepting someone’s help, you might be helping them. 

Are you ever ashamed to ask for the financial support of your friends, family or associates? It's common to feel uncomfortable asking for the aid of those around you, but being able to accept help when it's offered is actually productive for both you and the person lending a hand.

Take the author's relationship with her friend, former neighbor and mentor, Anthony. He helped the author by giving her moral support and advice throughout her teenage years and regularly when she was on the road.

But their relationship wasn't as one-sided as it might seem: Anthony, who suffered through an abusive childhood, doesn't like speaking about himself or his problems. In fact, he finds joy in talking others through their issues. In this way, offering his assistance to the author was beneficial to both of them.

Try it out. When you, as an artist, start accepting the help of others, you'll be amazed at how people support artists they admire.

Before the author became a professional musician, she worked as a living statue street performer, standing still for hours on end. Nicknamed "The Bride," she would wear a white wedding dress, white face paint and a black wig while handing out flowers in exchange for money. Her performance quickly made her a local Boston celebrity and attracted many supporters.

Among her supporters were those who were either personally touched by her work, or simply enjoyed participating in an art project that captivated the public. For instance, the owner of the ice cream shop where the author worked part-time let her keep her costume in the store's basement, which also served as a changing room.

In addition to the support of her boss, the author was aided by a flower shop employee who gave her a good deal on the flowers for her show, the owner of a burrito shop who offered her food and the owner of a coffee shop that provided a calm space for her to take her breaks.

> _"If you love people enough, they'll give you everything."_

### 3. When someone helps you, give them a gift in return. 

In addition to doing her act as "The Bride," the author performed as a variety of other characters. When playing "Princess Roulette," a white-faced ballerina, she would respond to people giving her money by doing a dance, turning and jumping at random to different spots.

Each spot indicated a different gift the person would receive, things like candy and plastic toys. But there was a problem: the author realized that the gifts she gave away cost too much relative to the donations she received for her performance.

Still wanting to offer something in return, the author opted for reasonably priced flowers, which eventually became a cornerstone of her performance as "The Bride". But some people just weren't interested in flowers and the author came to a realization: gifts don't need to be physical objects.

Here's how it happened:

She would be upset when her audience members refused a flower, and at other times would be concerned when homeless people, who clearly had much less than she did, gave her a dollar in exchange for a flower. She began to realize that the value of her service was of a different nature.

Her realization showed her that by looking deeply into the eyes of her viewers she was giving them the feeling that they were loved by another human. So it was also a fair deal for her homeless audience members because she made _them_ feel seen, a luxury that society rarely affords them. The author could also relate to the feeling of being unseen: she had previously worked as a stripper and knew the value of being looked in the eyes, of how it made you feel like a real person.

Just as her friend Anthony had once told her: you can't always give people what they want, but what you can give them is empathy and understanding. And that means a lot.

### 4. Accept the generosity of others. 

Have you ever been to a performance where the artist doesn't ask for donations? Why is it that some people have trouble asking for help? The reason is, many find it more difficult to accept something given to them as an individual than a gift made to a collective they're a part of.

For example, a musician friend of the author's once told her that she'd been offered a paid gig but didn't feel that she deserved it. As a solo musician and not a band, it seemed wrong to accept payment for her performance. The author then realized the same tendency in herself. She didn't feel entitled to ask for and accept money until she teamed up with Brian Viglione to form the band The Dresden Dolls.

This dilemma affects women in particular. A 2010 study by Emily Amanatullah showed that when negotiating a salary, women tend to ask for less money than their male counterparts. However, when negotiating on behalf of a friend, they're comfortable asking for as much as a man does.

It's essential to overcome this and always accept help when it's offered. Even so, if you can't accept assistance because of who is offering it or because you think you don't need it, it's likely the problem you're facing isn't too serious — yet.

For instance, the author has no problem accepting things when they're offered and is not ashamed to ask for things others might find embarrassing, like loudly requesting a tampon in a restroom. That changed when she married Neil Gaiman, a well-renowned and wealthy novelist. Although she had accepted loans from others in the past, she couldn't take her husband's money to support herself between producing and touring.

But she changed her mind when her problems got serious. Her good friend Anthony was diagnosed with an aggressive cancer and the author accepted Neil's money to pay her staff, thereby freeing up time for her to spend with Anthony.

### 5. Asking is collaborative by nature; make your requests unconditional by accepting the potential to be denied. 

When the author first began handing out flowers as "The Bride," some people didn't want to take her gifts. She felt dejected until she realized that her gift could only be a gift if she allowed people the option to decline it.

The greater realization the author had in this moment was that asking is a collaborative act, because the person who is asked can choose to respond with a yes or a no.

Therefore, in order to be comfortable asking, you must embrace "no."

It's helpful to start by defining the difference between asking and begging:

Asking is a reciprocal act and can only take place when there's mutual respect between the parties. On the other hand, begging is demanding something without giving the other person the option to deny your demand. In short, begging is a one-way street.

For this reason, truly asking must be unconditional. This is what the author realized when she gave people flowers in exchange for money, only to see many viewers refuse her gifts or give them back. It was only when the author began handing out her gifts unconditionally, meaning people were free to take them or return them, that she became comfortable with either response.

But giving others the option to turn down your request and accepting the painful sting of "no" can be difficult.

For instance, the author had a friend whose mother and aunt had not spoken for years because of a quarrel over part of their inheritance. As his mother developed cancer and was getting weaker by the day, the author's friend overcame his usual fear of asking and requested that his aunt call his mother. She refused and it crushed him.

So, the next time you're sad when someone denies your request, remember that giving the other person the option to turn it down is what really counts.

> _"If we don't allow for that no, we're not actually asking, we're either begging or demanding."_

### 6. Asking works best as a reciprocal practice within a community. 

So, making your requests unconditional is one key to truly asking. Another is to ask within your community. Here's how:

First off, be sure to ask the right people. The author built strong ties within her community by dedicating a lot of time to her family, friends and fans. She developed a network of people who trust her by keeping in touch via email and other forms of communication.

So when she and Viglione wanted to record their first Dresden Dolls album, the author knew she could ask her family and close fans to lend her the necessary money. Because of the tight-knit community she had built, it was easy to secure the money and, further proving her trustworthiness and work ethic, she quickly paid back what ended up being about $20,000 to her supporters.

Knowing who you can ask for favors is essential when asking within your community, but equally important is knowing how to reciprocate the generosity of others.

For example, the author and Viglione would often spend more time giving autographs after shows than on the performance itself. Not only would they grant every autograph request, once even signing a fan's butt, but they would also listen to their fans' stories with empathy, comforting them or giving minute-long hugs.

But the author's reciprocity to her fans went beyond her shows.

On one occasion, despite her busy schedule, the author visited a sick fan who was having a difficult time in the hospital. The two went on to become good friends.

In another instance of reciprocity, the author offered her support to a fan struggling with suicidal thoughts. The author corresponded with the fan regularly and even took the time to go on a walk with her after a show. The author was so invested in her, that she even enlisted other fans to help her through this difficult time.

By making it a priority to personally engage with her fans, the author earned their trust and developed a strong, supportive community.

> _"The emphasis is on collectivism; you throw the problem out there to your circles to see what solutions will arise."_

### 7. Build a tight, family-like net. 

You might be wondering how the author was so successful at building a loyal group of followers. One key was that she focused on making friends, not customers.

When she and Viglione began touring as The Dresden Dolls around the year 2000, the author took care of all the band's networking and management. She meticulously collected email addresses to craft what she called her "golden mailing list."

The golden mailing list was her first crowdsourcing tool and she used it to organize every detail of the band's shows. At the time, email was a relatively new form of communication, but the author used it to do everything from announcing gigs, to finding couches to crash on, to supporting the shows of various other musicians and artists.

For the author, emailing hundreds of people felt like having hundreds of pen pals. She didn't just build a fanbase, but a constantly expanding family. When people within that family needed help, like the author's suicidal fan, there were always others there to heed their call.

Another key to the author's success with building a group of followers was to never sell out her friends.

For instance, the record label that signed The Dresden Dolls wanted the author to make her communication with fans more efficient, discouraging online communication with fans unless there were upcoming shows or announcements to make. No matter how hard she tried, the author couldn't make the label understand that these people weren't just her fans — they were like her family and friends and she couldn't just stop talking to them.

The label also couldn't understand that slowly expanding her family of fans by personally fostering connections was essential to the author's attitude toward her work. Instead, they wanted the author to grow her fanbase quickly through large-scale, impersonal public relations methods.

Realizing that she couldn't trust the label with her fans, the author denied her managers access to the golden mailing list, knowing that they would use it for purely commercial purposes, like selling merchandise.

> _"If you want to make someone your real friend, ask them for a favor."_

### 8. “Crowd” everything: surfing, sourcing and funding. 

Organizing crowds has come a long way since the author first used her golden mailing list to secure sofas to sleep on.

And while crowdsurfing is excellent fun, crowd _sourcing_ can actually be an excellent means for gathering information and fundraising. The best part? It's easily organized using social media.

Later in her career, when the author was working as a solo musician, her husband introduced her to Twitter. The site quickly became one of the author's favorite modes of communication and she uses it frequently to keep in touch with people around the world.

But in addition to fostering global communication, Twitter became the author's number one tool for crowdsourcing. When she's on the road, she uses the site to seek advice on everything from song lyrics to illnesses. For instance, she once tweeted a photo of a red rash on her thigh thinking it was just a bug bite. Luckily, her Twitter followers informed her that it could be a dangerous staph infection, and they turned out to be right!

But crowdsourcing is good for more than diagnosing rashes. It's easy to crowd _fund_ if you've built your crowd the right way.

After years of making music, touring and managing their band together, the author and Viglione parted ways, dissolving The Dresden Dolls.

At the same time, the author decided that she would never again be dependent on a label and decided to crowdfund her first solo album through Kickstarter. The author launched her Kickstarter campaign asking for $100,000, and was shocked when she raised over $1.2 million in exchange for pre-ordered albums, private concerts and house parties with her.

The key to the author's success?

By constantly communicating with fans, she had built such a tight-knit, trusting community around her that the money practically raised itself. When other artists experience less success with crowdfunding, it's not necessarily because they aren't talented, but rather because they lack the social aptitude necessary to connect with people in a deeper way.

> _"Twitter is the ultimate crowdsourcing tool for the traveling musician; it's like having a Swiss Army knife made up of a million people in your pocket."_

### 9. Build trust by being honest. 

There's one last key to the author's networking success. She knew that simply staying in touch with your network isn't enough. You also need to be as honest as possible.

How?

Start by being transparent.

Remember the author's friend Anthony? He was diagnosed with cancer shortly after the author's huge crowdfunding success. The author knew she had an obligation to deliver her supporters' rewards as soon as possible, and trying to decide between being with Anthony and reciprocating her fans' generosity tore her apart.

Finally, when she made the decision to cancel her tour, she told her fans as openly as possible about the personal circumstances that led to the decision. The author's decision was met with understanding, with some fans even sending gifts and words of support to Anthony.

Other artists who failed to deliver on their promised crowdfunding rewards because of personal reasons found their actions met with much less understanding. To keep the trust of others, be sure to communicate honestly with them when plans change.

It's also essential to share everything.

For instance, the author shared everything from personal stories, to sadness, to love, to photos of normal imperfect bellies. She even shared the hate mail she received, creating a website filled with profane, hurtful messages.

By sharing everything, the author gave her fans access to her unfiltered self, thereby making herself more trustworthy.

Even so, she still suffered from trolling, hatred and even a death threat after tweeting a poem following the Boston Marathon bombings. Meanwhile, she received a lot of criticism for failing to pay musicians who volunteered to perform with her and her paid band, despite the extraordinary sum she had crowdfunded. All in all, she felt terrible.

What her critics didn't understand was that the money raised from crowdfunding was in exchange for services she had yet to provide. Therefore, the money could not be budgeted for volunteers. But, as usual, the author made sure a hat was passed around for them at every show.

> _"Fame doesn't buy trust. Only connection does that."_

### 10. Final summary 

The key message in this book:

**Instead of demanding help from others, accept assistance when it's offered. By building a closely-knit network based on personal communication, honesty and trust, you can ensure you'll always have a supportive community to rely on.**

**Suggested further reading:** ** _The Gifts of Imperfection_** **by Brené Brown**

_The Gifts of Imperfection_ offers an accessible and engaging walk through the ten principles that you can follow to live a more fulfilling life, defined by courage, connection and compassion towards others. Filled with relatable anecdotes and actionable advice, the book is a useful resource for readers both young and old.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Amanda Palmer

Amanda Palmer is a musician, writer and performer. Starting out as a street performer, Palmer later achieved huge musical success as one part of the duo The Dresden Dolls. Her TED Talk from 2013, "The Art of Asking," has been viewed more than eight million times.

