---
id: 546a027a30343600092a0000
slug: the-art-of-choosing-en
published_date: 2014-11-17T00:00:00.000+00:00
author: Sheena Iyengar
title: The Art of Choosing
subtitle: None
main_color: 311259
text_color: 311259
---

# The Art of Choosing

_None_

**Sheena Iyengar**

_The Art of Choosing_ draws on the results of fascinating psychological experiments in order to offer you insight into how we make decisions. In this book, you'll discover the common pitfalls that prevent us from making the right choices, and you'll receive practical tips for making better decisions in the future.

---
### 1. What’s in it for me? Uncover the hidden reasons behind your decisions. 

Most of us like to think of ourselves as sober, rational people whose logical decisions are not easily swayed by our decidedly irrational emotions. We look at ourselves and see intelligent, prudent individuals, whose actions are entirely congruent with our beliefs.

Wouldn't that be nice!

The reality is that we are highly complicated — and contradictory. Our decision making isn't based on cold, factual analysis, but instead on a myriad of fickle, irrational emotions and subconscious mechanisms.

All of our decisions, from the cars we buy to the careers we choose, are products of a long line of influences over which we often have absolutely no power.

In these blinks, you'll learn all about these influences, how they affect you and what you can do to become a better decision maker.

After reading these blinks, you'll know

  * why you might not notice the huge, hairy gorilla in the room;

  * how resisting one delicious marshmallow can indicate greater success in life; and

  * what love at first sight has in common with the fear of falling.

### 2. Our choices are determined by two opposing systems: the automatic and the reflective. 

How exactly do we make decisions? Most of us would like to think that we weigh alternatives and arrive at rational, well-thought-out conclusions. However, the reality is far more complicated.

To study choice, researchers had children sit at a table with a tasty marshmallow positioned in front of them. They told the kids: "You can have one marshmallow right now. But if you wait until I come back, you can have two."

For the kids who decided to eat the marshmallow immediately, their _automatic system_, which subconsciously and continuously analyzes sensory data to produce automatic reactions, was predominant.

When these kids smelled the marshmallow — i.e., experienced sensory stimulus — they responded with an automatic reaction, grabbing the marshmallow and greedily eating it.

Choices dictated by the automatic system happen so fast that people find themselves acting even before they have an opportunity to consciously consider them. While it won't help you abstain from eating marshmallows, we should thank the automatic system for enabling us to make quick decisions in the face of danger, e.g., jumping away from a moving car.

Some kids in the experiment, however, elected _not_ to eat the marshmallow immediately. For them, the _reflective system_, driven by reason and logic, was predominant.

The reflective system allows us to consider the future consequences of our choices and factor them into our decision making.

In the marshmallow experiment, 30 percent of the children chose to resist the marshmallow temptation for an entire 15 minutes, at which point they were rewarded with the second marshmallow.

Interestingly, when the researchers did follow-up studies on these kids as adults, they discovered that those who had chosen to wait for their second marshmallow as children developed stronger friendships and were healthier and more successful, both academically and financially.

So, while the reflective system is linked to greater longterm success, we still need both systems to make the right choice at the right time.

### 3. We often use rules of thumb to help us make decisions, but these can be faulty. 

If you're like most people, then you've probably considered casually inviting your ex out while you're having a lonely night at the bar. But how do you decide if it's the right choice or not?

Again, rather than laying out all possible pros and cons, we instead rely on _heuristics_, or "rules of thumb," to make decisions. These short rules are important because they help save us time and energy, and simplify the decision-making process by making certain options off-limits.

Heuristics often work like if-then statements, such as: "If you've had a couple of drinks, then don't call your ex." Some heuristics, like the one about exes, are conscious decisions. However, our automatic systems also use heuristic rules to make quick decisions. For example, _if_ you see a bear in the woods, _then_ run away.

However, although heuristics are useful, they can be biased. For instance, when trying to make a sound judgement, you might become influenced by the _availability bias_, which describes the tendency to believe that the truth is whatever is easily available to your memory.

As an example, imagine that you're out buying a tie for your colleague as a secret Santa gift. But which kind of tie should you get him? To simplify the process, you use the heuristic that "a color he wears often is a safe color for his tie."

However, the color you remember best might not be the color he actually wears most.

We have a better memory for things that excite our senses, such as bright colors, so even if he wears a grey tie almost every day, you'll likely only remember the one time he wore a red tie. And so, when you see his disappointed face as he unwraps his new scarlet tie, you'll know you've been a victim of the availability bias.

### 4. We want to make unique choices – as long as they aren’t too unique. 

Why is it that you sometimes want to change your order at a restaurant after discovering that someone at your table chose the same thing? The sad truth is that we want our choices to be unique so that we can feel special.

In fact, for many people, being unique is extremely important — even when our uniqueness is completely arbitrary.

Consider this experiment, in which researchers asked participants to estimate the number of dots on a video screen. After they made their estimates, participants were informed that the vast majority of people (75 to 80 percent) overestimated the number of dots.

But none of the participants were actually told how well _they_ fared in estimating the dots. Instead, half were told they'd overestimated the number of dots, and the other half that they'd underestimated.

Those who were told that they overestimated the number (i.e., were part of the "majority"), reported feeling unsatisfied with themselves. The others, however, did not, despite also being told that their estimations were incorrect.

In essence, participants weren't bothered that they were wrong. Rather, they were bothered that they weren't wrong in a special way.

Despite our desire to be different, we also don't want our choices to be _absolutely_ unique. We can see this in a modified version of the above experiment, carried out by the same researchers.

As in the previous experiment, they told some participants that they were in the overestimating majority and others that they were in the underestimating minority. The rest were told that their scores were so odd that the researchers were unable to classify them.

Just as before, the "overestimators" reported a decrease in self-esteem, whereas the "underestimators" experienced the opposite. Those whose results were allegedly "too unique," however, also suffered a decrease in their self-esteem!

So, while we all want our choices to be unique, our desire to be special still has limits.

### 5. Our culture has great influence over our choices. 

Strangely, we aren't the sole actors when it comes to decision making. In fact, our choices are also heavily influenced by our cultural heritage. This is seen most easily when examining the very different preferences of people from Western and Eastern cultures.

Unsurprisingly, people from more individualistic cultures prefer to be in charge of decision making, while collectivistic cultures want others to make their choices for them.

We can even see these preferences at a very early age, as shown by this experiment.

Asian-American and Anglo-American children were either allowed to choose a toy or were given one by their mothers.

The Anglo-American children, who come from a more individualistic culture, played longer with the toy they chose themselves, whereas the Asian-American children, whose cultural background is more heavily rooted in collectivism, preferred playing with the toy chosen for them by their mothers.

Understanding your preference in choice is not trivial. In fact, your success depends on whether your particular needs for choice are met.

For example, in another experiment, Asian- and Anglo-American children took a math test and later played a computer game called _Space Quest_, which had been designed to improve their math skills. In the game, they were able to choose the color and name of their spaceship, but with slight variations: one group could choose these customizations freely, while the other was given the settings that most of their classmates chose.

After playing _Space Quest_, they took another math test to see how much their skills had improved. Surprisingly, this seemingly unrelated variation in the experiment had a major impact.

Anglo-American children improved by 18 percent when they chose the settings themselves, and showed _no_ improvement when others made their choice. In contrast, Asian-American children improved by 18 percent when they were given the settings, and by only 11 percent when they made the choice themselves.

### 6. We miss most of the things that go on around us, yet are still subconsciously influenced by them. 

Do you ever become so engrossed in what you are doing that you completely forget the world around you? This _selective attention effect_ is especially noticeable when you're working on a task that involves concentration.

We can see it in action in an experiment called "The Invisible Gorilla."

Researchers showed participants a video in which two teams, wearing black or white shirts, passed around a basketball. During the video, participants were tasked with counting how many times the white team passed the ball.

Afterward, they were asked a different question: Did you notice the hairy ape wandering onto the set? Half of them hadn't seen the person in an ape costume walking slowly through the scene. The gorilla had even stopped for a moment to pound its chest!

Because participants were so focused on counting white team passes, they completely missed the "gorilla in the room."

Yet even if we don't pay attention to all of the information around us, it can still _prime_ our behavior, meaning it has a measurable but subconscious effect on us.

Take this study conducted by John Bargh, for instance, in which he gave 30 college students lists of five words in random order and asked them to use these words to build grammatically correct sentences.

For one group, he used words that are normally associated with the elderly, such as "wise," "retired," "old" and "gray," while the other group didn't have the same thematically related words.

He then measured how quickly the students walked to the elevator after the experiment. He observed that the group with the "elderly" words had been primed to walk more slowly than the other group, taking an average of 15 percent longer to reach the elevator.

Indeed, even the tiniest things can have profound influence on our choices and behavior.

Now that you have a better understanding of the mechanisms that affect our choices, the following blinks will examine how these choices affect us.

### 7. Having choices – or even the illusion of choice – makes us healthier. 

Imagine a life in which you have no choices at all, where every activity, every meal, every thing is determined for you. Sounds pretty miserable, right? Rightly so! Indeed, having choice can have great effects on our general well-being.

For example, in the famous Whitehall studies, Michael Marmot followed more than 10,000 British civil servants for a decade starting in 1967 in order to learn more about how work affects our happiness.

When he measured their health against their pay grade, he found that higher-paid employees were healthier despite having jobs that involved much greater pressure. In contrast, employees in the lowest pay grade were _three times_ more likely to die from heart disease than their bosses.

The researchers concluded that it wasn't the salary, but the freedom of choice in structuring their tasks that had such a positive effect on higher-paid employees.

In fact, choice is so important that even the mere _perception_ of choice can produce health benefits. We can see this clearly in an experiment aimed at investigating how choice could benefit the lives of elderly people in a nursing home.

Upon arrival, researchers created two sets of "house rules" for the residents, who were divided into two groups. The first group of residents were assigned a schedule with pre-determined slots for movie time, and were told that they were _allowed_ to visit other floors.

The second group were told everything was their _choice_ — when to watch the movie, how they would manage their time, and so on.

Functionally, their schedules were the same: all residents were basically free to do whatever they wanted. However, the language used by researchers made it appear as if the first group's well-being was the responsibility of staff, not of the residents themselves. And this mattered.

In a follow-up visit three weeks after the initial test, residents with the ability to "choose" reported feeling happier, while the health of the group with "no" choices had deteriorated.

### 8. We often rely on our gut feelings, but they’re actually bad for decision making. 

When we're faced with a tough decision, many of us consult our _feelings_ in the hope that our intuition will guide us to wisdom. However, our feelings aren't as reliable as we might assume.

In part, this is due to the fact that our feelings are influenced by our environment.

Take the famous study "Love on a Suspension Bridge," in which a female researcher stopped male sightseers and asked them a series of questions on the middle of either a dangerous-looking suspension bridge or on a stable bridge.

The researcher then asked the subjects to write a short story about a picture of a woman. She also gave participants her phone number in case they "wanted to talk further about the purpose of the study."

50 percent of the "suspension-bridge group" called to "talk about the study," compared to only 12.5 percent of the other group. Interestingly, the suspension-bridge groups stories also contained more sexual innuendo.

But what could explain this discrepancy?

Researchers concluded that participants confused their feelings of anxiety about being on a dangerous bridge with romantic feelings for the researcher. In this way, we can easily see how our environment can affect our emotions, and thus our decisions.

Not only are our emotions fickle, but we also sometimes overestimate their intensity.

Just consider this study, which followed the extremely close presidential election race in 2000 between George W. Bush and Al Gore. Researchers asked participants how they felt immediately following Gore's concession speech and then four months after the speech.

Four months following Gore's concession, both Gore and Bush supporters remembered experiencing much stronger feelings than they'd actually reported immediately after his speech, with Gore supporters remembering a deeper sadness and Bush supporters remembering elation.

Researchers concluded that we often fabricate our emotions according to our beliefs — "I was a Gore supporter, therefore I must have been sad." — and thus overestimate our past emotions.

> _"Love at first sight might actually have a lot in common with the fear of falling."_

### 9. When making choices we often change our mind – without even noticing it. 

Have you ever been called out by a friend for "flip-flopping"? Many of us change our stance on issues in order to back up the choices we've made that conflict with those stances.

Say, for example, that you're concerned about your health and therefore only purchase organic foods and won't take painkillers. However, you're also a very social person who enjoys having a drink (or two, or three) at the bar with friends.

One night, one of your friends calls you out on this apparent hypocrisy, citing the detrimental effects of alcohol on your health. You then experience _cognitive dissonance_ — the uncomfortable feeling that arises when we realize that we hold contradictory beliefs.

To combat this cognitive dissonance, you might try to downplay the negative effects of alcohol, and in doing so slightly modify your strict position regarding the consumption of dangerous substances.

This doesn't make you fickle — it merely verifies your humanity. Most of us want to have a consistent view of ourselves. We can't change our past choices, so we instead change our stance in order to back up the choices we've already made.

What's more, we often change our minds without even being aware of it.

For instance, when researchers asked hundreds of college seniors to rank the importance of different job attributes during their first post-college job search, students preferred to have the "freedom to make decisions" and wanted "opportunities for creativity."

However, as the months went by and the students became more "realistic" in their job search, they tended to prefer more practical attributes, like "job security."

In the survey's final round, nearly all the students considered "income" as their priority. When the researchers then asked the students about this shift in priorities, the students were convinced that they had _always_ held these priorities, and hadn't, in fact, changed their minds!

Now that you know how the ability to choose affects you, our final blinks will offer tips on how you can make better choices.

### 10. Our attention span is limited, so limited options help us make decisions. 

You're standing in the supermarket cereal aisle, totally overwhelmed: How do you choose the _one_ cereal from the 45 other possible choices? Many people experience similar situations in which they become paralyzed by the sheer number of available options.

As it turns out, we aren't really designed to handle that many options. In fact, most of us can't handle more than seven.

In studies where participants are shown differently sized shapes for a short period and then asked to arrange them in order according to their size, they're able to make generally accurate arrangements until there are seven sizes or more.

At this point they begin to make errors — our attention span is simply too limited to handle more than seven options.

This information is especially useful in sales; you can influence consumers' behavior by limiting their available choices.

In a famous study involving jelly, the author and her colleagues offered various jellies at a tasting booth in a supermarket in two rounds. During the first round, they offered 24 varieties of jellies, and in the second they offered only six.

During their experiment, 60 percent of shoppers were drawn to the booth with 24 jams, but only three percent of them actually bought a jar of jam. Most of them just stood puzzled in front of the shelf only to walk away moments later.

In contrast, only 40 percent of shoppers approached the booth with six jellies, but a hefty 30 percent of them actually purchased a jar.

In this case, it's clear that, at least in terms of motivating shoppers to shell out a few extra dollars, less is actually more.

### 11. Placing smart limits on your choices can make you a better decision maker. 

As you learned in the previous blink, we can easily become bogged down by an abundance of choice. Luckily, there are steps we can take to prevent ourselves from becoming overwhelmed.

First, being clear about your preferences places healthy limits on your choices, thus making decisions easier.

For example, if you're in the market to buy a car and are overwhelmed by the multitude of different options, you can refocus by making a list of your preferences.

If you're looking for just "a car," then your choices are near limitless. However, by refining your search, you can limit your choices immensely. In fact, you probably don't want just any old car. You might, for instance, be looking for a station wagon for under $30,000, outfitted with a folding rear seat and a sun roof.

By being clear about your preferences, you effectively limit your options, and thus make it easier to make the right decision.

Yet even if we limit our choices, we still often have trouble distinguishing similar items. In this case, we can use categorization to aid our decision making.

Consider this study, in which participants were able to distinguish seven different audio tones when they differed only in frequency, but could distinguish up to 150 different tones when other dimensions were added, such as intensity, spatial location and duration.

These dimensions worked like categories — by categorizing tones with reference to these dimensions, participants were thus able to store more information about the tones, making them more easily distinguishable.

Similarly, if you are able to categorize your various car options — in terms of color, size, cost, type, etc. — then it becomes easier to recognize the differences between individual cars.

> _"Sometimes choice isn't enough, and sometimes choice is too much."_

### 12. We often feel better when others make choices for us, but only if we are properly informed. 

Do you remember the anxiety you felt the last time you had to make a very difficult decision? Did you wish that someone else could choose for you? You might have been happier if that had happened. In fact, sometimes we are happier when we put decisions in other people's hands.

When we face difficult choices, we run the risk of regretting them. Take this heartbreaking series of interviews conducted with American and French parents who had lost an infant.

Their children had all been terminally ill, kept alive in an indefinite vegetative state only with the assistance of medical treatment. Eventually, the American parents had to decide to withdraw the treatment. In France, however, such decisions are made by doctors, unless they are explicitly opposed by the parents.

Months later, both groups continued to grieve, but the French parents were more convinced by the inevitability of the outcome. In contrast, the American parents, who had made the decision to terminate treatment on their own, felt more regret, doubt and resentment.

Yet even when others make our decisions for us, we only feel better if we are informed about the decision.

This is exemplified in a study called _The Julie Dilemma_, in which participants read about the terminally ill child Julie, whom they had to imagine was their own.

The experiment devised three possible scenarios:

  1. Parents are not informed about Julie's chances of survival. The doctors stop the treatment and Julie dies.

  2. Parents are informed about her chances of survival (60 percent, but with severe neurological impairment). The doctors stop the treatment and Julie dies.

  3. Parents are informed about her chances and must decide how to proceed themselves.

Researchers found that the second group, the informed non-choosers, expressed fewer negative emotions than the choosers from the third group. Surprisingly, the first group of uninformed non-choosers had just as many negative feelings as the choosers.

> _"Though we always want the best for our loved ones, the dizzying array of qualitative choices is enough to drive a person crazy."_

### 13. Final summary 

The key message in this book:

**Decision making is a lot less straightforward than simply weighing the pros and cons of a given set of choices. In fact, our decisions are influenced by external factors far beyond mere rationality. Luckily, with a little bit of knowledge on the art of choosing, you can learn to become a better chooser.**

Actionable advice:

**Keep a choice diary.**

Whenever you make difficult decisions, be sure to log your available options, motivations and expectations for the future. By keeping a diary, you can more accurately assess your choices in hindsight, adjust your decision-making process in the future and avoid making the same mistakes over and over again.

**Suggested further reading:** ** _The Paradox of Choice_** **by** **Barry Schwartz**

The abundance of choice that modern society presents us with is commonly believed to result in better options and greater satisfaction. However, author Barry Schwartz argues that too many choices can be detrimental to our psychological and emotional well-being. Through arguments based on current research in the social sciences, he demonstrates how more might actually be less.
---

### Sheena Iyengar

Sheena Iyengar is S. T. Lee Professor of Business at Columbia Business School. Her extensive research on how and why we choose has been cited by popular magazines such as _Time_ and _Fortune_, and she was voted among the top 50 Most Influential Business Thinkers by _Forbes_ in 2011.

