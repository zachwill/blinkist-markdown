---
id: 548fe6e365323500095e0100
slug: the-art-of-communicating-en
published_date: 2015-01-12T00:00:00.000+00:00
author: Thich Nhat Hanh
title: The Art of Communicating
subtitle: None
main_color: E2755E
text_color: 994D3D
---

# The Art of Communicating

_None_

**Thich Nhat Hanh**

_The Art of Communicating_ offers valuable insight on how you can become a more effective communicator by practicing _mindfulness_. Drawing on Buddhist wisdom, it outlines ways you become a respectful listener, express yourself well, and ultimately improve your relationships with your loved ones.

---
### 1. What’s in it for me? Learn how to improve any relationship through effective communication. 

Have you ever asked yourself what could be the biggest problem in any of your relationships, whether at home, work or with your closest friends? It could be that your communication is unhealthy.

What does that even mean, though?

Author Thich Nhat Hanh explains what kind of communication is good for you and which kind should be avoided. He shows that in order to communicate well with another person, you first need to understand yourself. However most of us rarely communicate with ourselves which is why we have so much trouble communicating with others.

In these blinks, you will

  * start to think about whether you truly understand the people around you; and

  * learn how to start your work day off right.

### 2. We communicate using two kinds of speech: nourishing and toxic speech. 

From personal relationships to multibillion dollar business deals, healthy communication is vital. But what exactly is "healthy" communication?

It's best to think of communication like food. Some of it is nourishing, and some is toxic and poisonous.

_Nourishing speech_ is understanding and positive, while _toxic speech_ fills people with negative emotions like anger and frustration.

Imagine you're waiting for feedback on a project, when your boss comes in and says, "This is absolutely terrible. You're a useless waste of space." That would certainly be toxic speech.

On the other hand, if she said, "I think there are some things we could improve here," that would be nourishing speech. You could use it constructively.

So, how can you work more on your nourishing speech, and develop a healthier communication style?

The first step is to understand _your_ _own_ way of communication. You have to practice _mindfulness_.

Mindfulness means fully concentrating on yourself, your body and your breathing. It allows you to communicate clearly without judgement.

When you're in a mindful state, you'll feel detached, which allows you to examine your communication objectively. You'll realize some things you might want to say are toxic, so you'll be able to stop yourself before you say them.

Mindfulness also helps you become immune to the toxic speech of others. When you're being mindful, you can judge people less. You will understand them, and see that their toxic speech really is a result of their suffering. You'll feel more compassionate, so you'll know not to take their hurtful words personally.

So, to make sure your conversations are healthy and nourishing, practice being mindful.

> _"With mindfulness we can produce thoughts, speech, and actions that will feed our relationships and help them grow and thrive."_

### 3. If you want to communicate well with others, you have to communicate with yourself first. 

You probably spend a good portion of your day communicating with others, whether it's in person or online. But how much time do you spend communicating with _yourself_? Probably very little.

Communicating with yourself is essential for mindfulness, which means it's also essential for healthy communication.

You communicate with yourself when you listen to your own mind and body. It can be as simple as sitting down and concentrating on your breathing.

When you do this, you'll be able to focus completely on the moment, not the past or the future. Just you, your health and your emotional state at the time.

This is especially helpful when you're suffering, because the reasons for our suffering are not always immediately apparent. You'll get more in tune with what's wrong in your mind or body.

When you master the art of communicating with yourself, you'll get better at communicating with others in a nourishing way. Understanding your own thinking allows you to understand it in others.

Picture, for example, a person who is unhappy and feels unable to improve her state. Let's say she's suffering due to problems in her relationship, but can't put her finger on what exactly it is that upsets her.

What she needs is mindfulness. If she can understand her own suffering (that she's upset because her partner doesn't take care of the house, for example), she'll feel more compassion for _his_ suffering (maybe that he's overworked and too exhausted when he comes home).

So mindfulness will endow her with a deeper understanding of her partner. She'll be better able to use loving and nourishing speech to work towards finding a solution with him. We can't communicate effectively with others until we get in touch with ourselves first.

> _"If you can't accept yourself… how can you love another person and communicate love to him or her?"_

### 4. Listen to the suffering of others, to help them feel better. 

Have you ever discovered something completely new about a long-term partner? Something you can't believe you missed?

We don't always fully understand people we're close to. This is often because we don't _listen_ to each other.

Sometimes our minds are just somewhere else when our partner is talking. Your spouse might be describing a serious problem in your marriage, but you're thinking about cleaning the pool or paying the electricity bill.

Even when we _do_ listen to our partners, sometimes we interrupt them to say why we think they're wrong.

The good news is that there's a way to solve this: _mindful listening_. Mindful listening means carefully taking in what others say without judging them.

When someone's telling you about their suffering, you might be tempted to interrupt them, especially if you want to correct their perceptions. However, this might lead to a discussion where you aren't truly focusing on their feelings, which is what you _should_ be doing.

Mindful listening also means not blaming the person for anything. For example, imagine you're listening to a friend whose girlfriend has just left him. Even if you think it was partly his fault, you have to recognize that this isn't the time to say that, because it'll make him suffer more.

The purpose of mindful listening is to help the other person, so let them say what they need to. You can correct any misunderstandings they have later.

When the person you're communicating with sees how much you care about understanding them, that alone will lessen their pain.

> _"Deep listening has only one purpose: to help others suffer less."_

### 5. You can express your love and appreciation for others by using mantras. 

Avoiding toxic speech is essential, but what about nourishing speech? How can we evoke it?

A good strategy for keeping your speech nourishing is to use _mantras_. Mantras are set phrases that help you express certain emotions. In Buddhism, there are three mantras for letting people know you love and appreciate them.

"_I am here for you"_ is the first mantra. It clearly establishes the base of your love, because you have to be present in someone's life in order to love them.

Be sure to use the first mantra when communicating with people you care about. Being there for someone is the greatest gift you can give them. If you say this mantra with mindfulness and compassion, they'll truly appreciate it.

The second mantra is _"I know you are there, and I am very happy."_ It's vital to let your loved ones know their presence is important to you.

Imagine you're sitting next to your partner in a car, for instance. It wouldn't be unusual in this situation to find yourself thinking about everything except the person next to you. That might make them feel a bit invisible. But if you take the time to let them know you're paying attention to them, and their presence brings you joy, they'll feel loved and valued.

You can say the third mantra, _"I know you suffer, and this is why I am here for you,"_ when someone you love is in pain. Like the first mantra, it shows the person you're there to support them. It also emphasizes that their feelings matter to you, which is a crucial part of mindful listening.

### 6. There are three more mantras that help bring you happiness. 

The first three mantras focus on helping _others_ suffer less, but your emotions are also important. The next three mantras, discussed here, help bring happiness to _you_.

The fourth mantra is _"I suffer, please help."_ Use it to let others know when you need support.

When someone causes us pain, we're often too proud or afraid to let them know. If someone insults you without realizing it, you might turn away, or act like you don't need them anymore. You might even try to "punish" the person, consciously or unconsciously.

When you practice mindfulness, however, you'll be more compassionate. If someone hurts you, you'll seek to understand why, instead of just shutting them out.

"_This is a happy moment,"_ is the fifth mantra. Sometimes we forget to acknowledge happiness, and this can be a powerful reminder.

When you say this mantra to someone you love, it'll remind both of you how lucky you are to be together.

It's important to remember that you don't need to wait for an unusually special moment to say this. Take time to appreciate smaller things, like the beautiful sunset or the simple fact that you're both alive.

The sixth mantra, _"You are partly right,"_ is helpful when someone's criticizing or praising you.

The sixth mantra emphasizes that there are many aspects to a person — some positive and some negative. This mantra allows you to express that you appreciate the other person's compliments or criticisms, but it also reminds them you have other qualities as well.

Using the sixth mantra will help you keep a good perspective on situations. You'll be more objective and less judgmental, which is crucial for mindful awareness.

You can also say it to yourself. When someone criticizes something about you, remember it's just one part of you. Maybe their criticism is partially true, but you can objectively see that it's not a cause to feel insecure or overly upset, and maybe it's helpful information.

> _"With mindfulness you can recognize that it is possible to be happy right here and right now."_

### 7. You can nourish others with loving speech by always being honest and compassionate. 

In addition to the six mantras, there's another important tool for keeping your communication nourishing. It's called _loving speech._ Follow these few rules to make sure you use it effectively.

The first rule of loving speech: you must always tell the truth. Following this rule can be tough, especially when the truth is painful. If you speak in a warm-hearted way, though, then telling the truth is healthier — and will feel better — than lying.

Telling the truth might be painful at first, but it builds trust in the long-run. Ultimately, it makes the other person feel safe. If you lie to someone and they uncover the truth later, they'll be hurt to learn you were dishonest, and they won't feel secure with you in the future.

Imagine you discover that your best friend's boyfriend is cheating on her. This news will certainly be painful for your friend, but if you speak to her honestly and compassionately, she'll suffer less in the end.

To nourish others, you must also understand that all people are different. That means they all need to be spoken to differently.

Each person has their own way of perceiving and coping with the world. When you're communicating with someone, make sure you're always doing it in a way they understand.

There's a story about the Buddha that illustrates this well. A person once asked him where he would go when he died, and the Buddha answered that he wouldn't go anywhere. Later, another person asked the same question, and he answered differently.

When asked why he gave two different answers to the same question, he replied that he answered depending on the person's ability to understand. Think, for example, about how you would explain any event in world history to a fifth grader versus an adult.

> _"When we're able to produce a compassionate thought, this thought begins to heal us, heal the other, and heal the world."_

### 8. A group of people can create an effective and satisfying community by practicing mindfulness together. 

Mastering healthy and effective communication isn't just about talking with one other person. We often need to communicate with groups of people, which can be an added challenge.

Many people are confronted with toxic speech at work, for instance. Are you? You can improve toxic environments, such as at your workplace, with mindfulness.

Be a role model for mindfulness, and suggest working on improvements together.

There are simple steps you can take towards improving your work environment. Just changing your thinking during your morning commute will make a difference.

When we're on the way to work, we're usually already thinking about what we have to do that day. That means we're already stressed out when we arrive.

Instead, practice mindfulness on your journey. Focus on your breathing, and enjoying the present moment. You'll focus better when you finally get there.

We're usually stressed at work, and that affects our productivity. It's a good idea to take some time to practice mindfulness with your colleagues. You could get together before a meeting, for instance, and focus on your breathing together. You'll enjoy the meeting more, and it'll probably be more productive.

Even if your colleagues don't want to join you, practice mindfulness yourself. Hopefully at some point they'll follow your example.

Mindful communities have great power for change in the world. Even if a group of people is united by a strong cause, like environmentalism, they'll only achieve their goals if their community is strongly connected and has the right energy.

So take the time to practice mindfulness together. Your community will learn to _listen_ to itself, and also the world at large.

### 9. Final summary 

The key message in this book:

**Strive to communicate lovingly, by using loving speech. Listen mindfully when your loved ones speak, and practice mindfulness with yourself as well. When you learn to avoid toxic speech and stay honest and compassionate, you'll build stronger bonds with those around you. You'll improve, not only your relationships, but also your community at large.**

Actionable advice:

**Breathe. Listen.**

Take time to pause, and focus on the moment. Clear your head and concentrate on your breathing. You can do this anytime, like on your morning commute to work. And be sure to listen, whether you're listening to a loved one express herself, or just listening to yourself.

**Suggested further reading:** ** _Savor_** **by Thich Nhat Hanh and Dr. Lilian Cheung**

This book provides advice and inspiration on how to find inner peace, joy and strength — especially for those trying to sustainably lose weight — with Buddhist teachings and techniques for appreciating the richness of life in the present moment. It also draws on the latest nutritional science research on the best ways to eat and exercise, presenting readers with a holistic method for improving their physical, psychological and spiritual well-being, and thereby transforming their lives.
---

### Thich Nhat Hanh

Thich Nhat Hanh is a Buddhist monk from Vietnam. He's published over 100 books, including _Anger,_ which was a New York bestseller. In 1967, he was nominated for the Nobel Peace Prize.

