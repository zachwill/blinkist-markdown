---
id: 5c28e1b36cee070008e7a3b7
slug: the-art-of-gathering-en
published_date: 2018-12-31T00:00:00.000+00:00
author: Priya Parker
title: The Art of Gathering
subtitle: How We Meet and Why It Matters
main_color: D32A4D
text_color: D32A4D
---

# The Art of Gathering

_How We Meet and Why It Matters_

**Priya Parker**

In _The Art of Gathering_ (2018), Priya Parker argues that the gatherings in our lives — from business meetings to dinner parties — are lackluster, routine and lacking in purpose. Parker sets out a bold new approach to gathering that focuses on distinctiveness, purpose and real human connection, and shows how simple steps can invigorate any gathering of people.

---
### 1. What’s in it for me? Learn the art of gathering well. 

Have you ever stopped to think what makes for a good gathering? We spend a large part of our lives gathering, from Monday morning team catch-ups to Friday night cocktails, book groups and board meetings. But few of us really take the time to consider the ingredients that go into a successful, meaningful or exciting gathering.

And when we do try to plan, we tend to be focused on the practicalities. We look to Pinterest for decorative suggestions, or we spend time focused on the logistics of a business event. Rarely do we stop to think about the deeper purpose that lies behind our gathering, get under the skin of how people connect with one another and consider how to design gatherings in a way that encourages better human connections.

These blinks are the perfect antidote to lackluster, boring gatherings. If you've ever felt that your dinner party lost a little energy during dessert or that your corporate strategy meetings could be more focused, here you can find out how to pep things up.

In these blinks, you'll learn

  * why crossing people off the invite list can sometimes be the key to a successful gathering;

  * why not letting guests pour themselves a drink might be the key to a fun party; and

  * how you can end a gathering magically.

### 2. Gatherings are important to the human experience, but too often we don’t give them much thought. 

We spend our lives gathering — first in our families, with friends, in playgroups and at schools. Then, with adult life, come weddings, business meetings, class reunions and dinner parties. And as our lives end, our families and loved ones gather at our funerals.

Gatherings are a huge part of life, and they're a part of the human experience. But the time we spend in them is often underwhelming and uninspiring.

At work, we moan about conferences and meetings. Duncan Green, a specialist in international development, spoke for many of us when he wrote in his 2016 article in _the Guardian_ about his views on conferences. With some few exceptions, he said, his usual mood at conferences ranges from boredom, to despair and all the way to rage. And a 2015 survey presented in the _State of Enterprise Work Report_ found that employees cited wasteful meetings as their top barrier to getting work done.

Non-professional gatherings are also increasingly disappointing. As traditional religion holds less and less appeal to younger people, gatherings built around church communities are in decline. But we also don't even seem to be particularly happy with the time we are spending with friends. A 2013 study into the state of friendship in America found that 75 percent of people were dissatisfied with their platonic relationships.

Given this state of affairs, you might think more people would take the time to think about how to change things. But instead, we keep taking the same approach.

It's rare to go to a conference, or a drinks party, and find that the event organizers have given serious thought to how guests will connect with each other and get something meaningful from the gathering. That's perhaps because when we do seek out advice for hosting, we tend to focus on the mechanics. What is fundamentally a human challenge — how to bring people together in a way that is meaningful, interesting or thought-provoking — becomes a logistical one. We focus on Powerpoints, AV equipment, table decorations and menu choices, more than we think about people and human connection. 

But the good news is there are steps you can follow to ensure that your gatherings really work. And anyone can follow them. You don't need to be an extrovert or have a fancy house or location for hosting your event. You just need to read on.

> _"Gathering — the conscious bringing together of people for a reason — shapes the way we think, feel, and make sense of our world."_

### 3. Committing to a clear purpose for your gathering is the first step to making it great. 

Have you ever stopped to think about why you gather in the way that you do, through events such as quarterly board meetings, lectures and charity fundraising galas?

If you did, you might come to find that too many of our gatherings follow ritualized formats, rather than being built around a clear purpose.

That's why we spend so much of our working lives in meetings. Nevermind that a quick email exchange could be more effective than a weekly catch-up meeting. That's the way things have always been done, so the ritual continues.

The same is true in our social lives, as the author came to find when she was pregnant. Her friends wanted to throw her a baby shower. As baby showers are associated with their own sets of rituals, neither the organizers nor the author herself really stopped to think about its purpose, until the author talked to her husband.

It turned out that he too wanted to be involved, despite tradition suggesting that baby showers are for women only. At that point, the author realized that she and her husband were making a transition together, from couple to family, and they needed a gathering that supported them both in that transition. The ritualized format of women gathering together was irrelevant to their aspiration to parent equally, as well as their desire to make this aspiration known.

So when you are organizing a gathering, try to think less about the what and more about the why. Perhaps your birthday is coming up, and you want, as usual, to host a gathering to mark it. Really drilling down into why you want to do that might help create a better, more satisfying event.

Perhaps you want to surround yourself with the people who make you feel happy. If this is your purpose, then you might choose an intimate dinner with your closest friends, rather than a barbecue with everyone from Aunt Mary to Bob from accounts. Or maybe you find that your real purpose is to use the occasion of your birthday to step out of the everyday routine — in which case, bungee jumping with a friend could be a better alternative to a party.

Setting a clear purpose for your event will help you make the rest of the decisions about how to organize it. And among the first things to consider is who to invite.

### 4. Being willing to exclude people is a key step toward building a meaningful gathering. 

The concept of "the more the merrier" is deeply rooted in society. Most of us have heard those words since childhood.

So when we think about guest lists, we often focus on _inclusion_. But sometimes _exclusion_ is just as important, even if it can feel a little uncomfortable.

The author had to grapple with this with her workout group of six friends who met with a paid personal trainer twice a week in a park, where they gathered to tone-up and swap gossip. When one friend planned a vacation, he suggested that a friend of his would take his place, which would allow him to avoid losing the money he'd already paid for the trainer that week.

The others took a while to work out why they were uncomfortable with the idea. Eventually, one of them figured out that the real, unspoken primary purpose of the group was not to take a paid-for exercise class, but to socialize while exercising. Because the proposed substitute was an unknown person to them, his attendance would have threatened the meetup's real purpose, which was providing a fun and intimate environment to catch up as friends.

It's not easy to say "no" in these situations, because politeness takes over. But while exclusion may feel impolite, inclusion of the _wrong_ people is a form of impoliteness to the other people involved in it.

Another reason that some people avoid an exclusive approach to invitations is for fear that narrowing a group will reduce its diversity. But often, drawing your invitation list tightly can be the best way to bring together an interesting mix of attendees. Consider Judson Manor, a retirement community in Ohio that limits its residents in an exclusive way. The only people allowed to live there? Retirees, of course, and five college music students, who are given free accommodation in exchange for recitals and participation in the community.

The benefits for both groups are clear. The students enjoy cost-free rooms and a captive audience. The older residents meanwhile enjoy regular live music and the presence of energetic young people, which can act as a remedy against isolation, dementia and perhaps even high blood pressure.

The criteria for the group are exclusive. The setup wouldn't have worked with, say, business majors. Over-including would have killed what made the combination great.

Being willing to make slightly uncomfortable decisions is a key part of hosting well, as we'll see again in the next blink.

> _"You will have begun to gather with purpose when you learn to exclude with purpose."_

### 5. Hosts who act with generous authority will always deliver better events than hosts who are overly laid-back. 

In modern life, being chill is often treated as a virtue. As a result, we're hesitant to tell people what to do, even at events we are hosting ourselves. Being laid back, not imposing on our guests, feels like the right thing to do.

But when it comes to gatherings, being a "chill" host is an abdication of your responsibility to your gathering and guests, and it's a sure-fire way to let things fizzle out.

On one occasion, the author was at a housewarming party in Brooklyn. After guests had eaten, the party suffered something of a lull, with guests unsure where the party was going and whether they should just leave. The author suggested to the hosts that they initiate a game of Werewolf — a dynamic group game that could bring the guests together and inject some much needed energy into the evening.

But the host was skeptical about whether _all_ guests would enjoy it, and she wasn't willing to exercise her power as host to get them on board. Doing nothing was an easier option. The moment passed, and the party soon broke up.

Rather than following a hands-off approach to hosting, it's better to embrace _generous authority_. That means running events with authority, but selflessly, in the interests of your guests.

The author once facilitated a conference, the purpose of which was to bring together people involved in rearing cattle and selling grass-fed beef. The author knew that a key way to get the 120 attendees, who mostly didn't know each other, to think of themselves as a group would be to connect with as many new people as possible.

So she decided that after each speech, every attendee would get up and move to another table. The organizers and attendees were reluctant. It seemed like an imposition. And guests grumbled, initially.

But by the end of the day, the mood was different. Guests told the author that they had appreciated making surprising connections with new people and not just talking with the friends they'd sat down next to at 9:00 a.m. The author had exercised generous authority, acting on behalf of the guests' real interests, rather than their desire for comfort.

So remember, while it may seem counterintuitive, exercising authority can really be the best way to get people to enjoy themselves and obtain value from a gathering.

Now let's take a look at another aspect of authority: rules.

> _"Hosting is inevitably an exercise of power."_

### 6. Having explicit rules for your events can be surprisingly liberating. 

Rules have a bad reputation. We associate them with school, overbearing parents, boredom and rigidity. But that reputation is unfair, because if you get them right, rules can unleash experimentation, playfulness and truly meaningful gatherings.

A great example of how rules can enhance, rather than restrict, comes from the Latitude Society, a secretive networking organization that hosted underground, invitation-only gatherings in San Francisco, before it disbanded in 2015. The society's rules were designed to encourage bonding and belonging. 

For example, attendees weren't allowed to pour themselves a drink, meaning they would have to ask someone else to pour it for them. This simple rule forced people to interact, but in a playful, easygoing way. This helped them overcome the slight awkwardness associated with approaching someone new and striking up a conversation.

Rules, then, can help make things different; they can take us out of our everyday way of doing things. That's particularly important today, when technology can be so dominant in our lives.

Anyone hosting a gathering of any kind today has to deal with the fact that being distracted by technology is a reality of modern life. A Deloitte study has shown that people check their phones an average of 150 times a day. How do you ensure that 50 of those aren't happening at your gathering? How do you ensure that your attendees are truly present?

The author and her husband created a weekend event for their friends called "I am here" days, which were based around exploring a new neighborhood of New York. The group would meet, walk many blocks, talk to locals and share their experiences of living in the city. When the gathering grew larger, they set out some rules, one of which was that no technology was allowed.

The rules came to not only be accepted by those who joined, but valued. Why? Because they forced a sense of presence that is rare in modern day, tech-addled, distraction-filled New York. As comedian Baratunde Thurston, a participant in "I am here" days has said, smartphones mean we all carry with us the ability to be anywhere. As a result, making the active choice to do just one thing in a day, with a set of people, and to focus on that and on being present, feels significant and meaningful.

In a world that gives us almost indefinite choices, enforcing focus on just one thing is really an act of liberation.

> _"Rules-based gatherings, controlling as they might seem, are actually bringing new freedom and openness to our gatherings."_

### 7. Priming your guests well and honoring them on arrival will help get your gathering off to a great start. 

Now you've got a clear purpose for your gathering, you know who to invite and you know how to host. How can you kick things off in just the right way? 

Firstly, recognize that people will form impressions about what to expect from your event before it's even started. So prime them with the right expectations.

Priming can be a simple business, and it may just mean making a small request of your guests. Michel Laprise, a director at Cirque du Soleil, wanted to host a pre-Christmas gathering for colleagues after a long, tiring tour. The day before, he fired off a quick message to all his guests, asking them to send photographs of two happy occasions they'd had in the year past.

The process of searching through their photos led his guests to expect a celebratory evening. So when they arrived to find a Christmas tree decorated with their photos — of scuba-diving trips, babies and post-show selfies — the mood was set perfectly for a joyous evening.

Laprise's Christmas tree points to another great way to start a gathering: making your guests feel welcome and honored to be there.

The author once had a teacher, Sugata Roychowdhury, who on the first day of class, recorded attendance in an incredible way. Instead of reading through and checking off the list of 70 students' names, he paced the room. One by one, he held eye contact with the students, pointed at them and stated their full name.

Roychowdhury and his students had never met before. He'd taken the entire class attendance from memory, presumably having studied photos and names for hours. The students were mesmerized and immediately felt both honored and excited to be in his class.

So think about how you can honor your guests as they arrive. This could be as simple as beautifully decorating the table before a friend comes over for what they think is a casual lunch. Or perhaps you can think of a way to introduce your dinner guests not by their occupation or other banal facts, but with a story that expresses something unique about them.

That kind of meaningful introduction is also a great way to encourage authenticity. And as we'll see now, that's not always easy to achieve.

### 8. It’s possible to design your gatherings so that they encourage people to bring out their authentic selves. 

Too often, we try to present our best selves, rather than our real selves to the world. On social media, we show off a glossy, idealized spin on our real lives. At business conferences, we talk of our achievements and successes, rarely dwelling on our vulnerabilities. But all too often this leads to dull, impersonal conversations.

So how can you get your guests to reveal their whole, authentic selves?

When the author was asked to facilitate a World Economic Forum dinner for CEOs and other high flyers, she came up with a theme and format that would avoid dry exchanges of professional boasting. As her theme, she chose the idea of the good life.

And to encourage guests to speak up, she asked them all, at some point in the evening, to stand up and give a toast to the good life, and to start their toast by telling a personal story from their own lives. This, she figured, would stop the guests droning on about their achievements and force them to get real.

The result? A dinner focused on real human emotion. One guest talked of her work in disaster-relief, becoming emotional about her experiences. Another shared the dying words of her mother, who said she spent 90 percent of her time worrying about things that didn't matter and told her daughter not to do the same.

This prompted another guest to reveal something she had never shared with anyone before — that every morning, she does what she calls a death meditation, when she reflects on everything she would leave behind if she died, while savoring her gratitude for being alive. As the evening went on, stories were shared, many of them never told before, and tears were shed. Thus, this group of people, used to interacting on the basis of their job titles and CVs, engaged with each other with a raw honesty.

So how can you bring this authenticity to your own gatherings?

Firstly, ask for stories. People intuitively know that an interesting story is about vulnerability, risk and emotion. You can't tell an interesting story about how successful you are.

Secondly, reveal yourself. One of the reasons the author's dinner was such a success was because she made a raw, honest toast herself, recounting how she got her first period and how her mother reacted with joy and celebration. It was, she told the group, a time when she felt that she was seen and that she mattered, as a result of her mother's reaction. 

If you want your guests to share something personal, you have to be prepared to expose yourself first. Take the lead, and others will follow.

### 9. Too many events fizzle out rather than ending with a bang, but there are simple ways to end gatherings well. 

A drama teacher, Dave Sawyer, once shared with the author the secret of telling great actors from merely good ones: watch not just their _entrances_ on stage, but their _exits_. Truly great actors focus as much on their exits as on their dramatic entrances, knowing that how you end things shapes people's experiences and memories.

Well, a great host should take the same approach. The first step to a great ending is to avoid things simply fizzling out.

In bars around the world, barmen shout out "last call" a little while before closing. Why? To allow the clientele to resolve their unfinished business — whether that's ordering one for the road or winding down their heated debates about French philosophy. It nudges them toward a clear ending.

You can implement a kind of last call in your own home. A common problem with dinner parties is that, after dessert, some guests may want to head home tired, while others would prefer to hit the brandy till the small hours. Faced with this situation, you can thank everyone for a wonderful evening and make clear that guests can feel free to leave.

At the same time, you can also emphasize that anyone who wants to stay should do so by retreating to the living room. It's the dinner party equivalent of a last call; guests can choose whether to order another round or to call it a night. 

The other important element of an ending is to ensure your gathering is remembered — for the right reasons.

The author's father-in-law teaches a class on management consulting at George Washington University in Washington, D.C. In the last class of the semester, he avoids doing a boring recap. Instead, he takes a moment to remind his students of the difficulties of a life in management consulting and the need to maintain balance and purpose in life.

Then he shows them a card trick. It looks like magic, he tells them, but really it's just a technique. And he tells the students that he hopes that they can master the techniques that he has taught them, until it looks and feels like magic.

It's an ending that ensures that his gathering is remembered for purposeful reasons, long after it has drifted into the past.

If we left all of our gatherings in such a way, wouldn't life be a richer experience?

### 10. Final summary 

The key message in these blinks:

**The gatherings in our lives are too often lackluster and lacking in purpose. But it doesn't have to be this way! When we break out of the rituals and routines that surround our gatherings, embrace the generous authority of being a host, set some rules and encourage people to be their authentic selves, it's simple to hold gatherings that are meaningful and memorable.**

Actionable advice:

**Think carefully about your location.**

The best gathering locations are ones that inspire and embody the true purpose of your gathering. So think carefully about where to do things, and break out of your normal spaces. For instance, you could consider hosting a college reunion in a cemetery, to remind your old classmates that they should get on with fulfilling the dreams and ideals of their youth. Or if you are organizing a sales training, you could get your colleagues to spend a day with a subway busker, connecting them with the purest form of their work. Thinking about location creatively and with a focus on your gathering's purpose will help generate fresh and memorable ideas.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Coffee Lunch Coffee_** **, by Alana** **Muller**

Now that you've got a little inspiration for how to craft gatherings with a spark, why not take the next step and learn more about the art of building lasting relationships? In _Coffee Lunch Coffee_, Alana Muller offers a practical guide to the art of networking.

As you've just learned, humans desire meaning, purpose and bonding through their gatherings and meetings. Muller, a popular blogger and president of a non-profit that trains entrepreneurs, shows how you can use this desire for connection to your advantage. These blinks offer practical tips for building new relationships that will improve your personal and professional lives.
---

### Priya Parker

Priya Parker is a facilitator who works with corporate executives, activists, educators and more to create transformative gatherings. Trained in conflict resolution, Parker has worked on peace processes in the Middle East, Africa and India. She has been a member of the World Economic Forum's New Models of Leadership Council, and her TEDx talk on purpose has been viewed over a million times.

