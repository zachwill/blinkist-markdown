---
id: 510170ffe4b0991857b92611
slug: the-art-of-happiness-en
published_date: 2013-01-03T00:00:00.000+00:00
author: Dalai Lama
title: The Art of Happiness
subtitle: A Handbook for Living
main_color: E2A628
text_color: 805D13
---

# The Art of Happiness

_A Handbook for Living_

**Dalai Lama**

_The Art of Happiness_ (1998) is based on interviews of His Holiness the Dalai Lama conducted by the psychiatrist Howard C. Cutler. The combination of Tibetan Buddhist spiritual tradition with Dr. Cutler's knowledge of Western therapeutic methods and scientific studies makes this a very accessible guide to everyday happiness. The book spent 97 weeks on the _New York Times_ bestseller list.

---
### 1. External circumstances cannot create lasting happiness – the right state of mind can. 

How can we achieve lasting happiness?

Most people would readily agree that the purpose of life is to seek _happiness_. Yet for some reason we often see happiness itself as something mysterious and hard to define, and we have a poor understanding of what makes us happy. According to the Dalai Lama, by training one's mind it is possible to learn how to be happier.

External events can affect a person's happiness in the short-term, but our level of happiness tends to revert back to a certain baseline soon after the event. For example, winning the lottery only produces a short-lived happiness "high," which usually subsides relatively quickly. Similarly, people who face sudden and tragic health problems like a diagnosis of cancer or paralysis typically — after a period of grieving — recover their previous level of happiness. Hence, it seems no specific external conditions can really affect our happiness in the long run.

But the mind is a powerful tool; our mental state greatly affects how we perceive the world. Consider, for example, how negative emotions skew our view of other people: when we're angry, even dear friends can seem annoying, cold and hostile.

According to the Dalai Lama, it is possible to systematically train your mind so that you identify and cultivate positive mental states while eliminating the negative ones. Though this is a slow, gradual process, it eventually brings a calmness that allows you to live a happy, joyous life no matter what the external situation.

**External circumstances cannot create lasting happiness — the right state of mind can.**

In the next three blinks, you'll learn why compassion, intimacy and spirituality are important for a happier life.

### 2. Cultivating universal compassion is a way to a healthier, happier life. 

The Dalai Lama places great emphasis on developing and cultivating _compassion_. It is an important component of not only Buddhist spiritual development but also of robust, lasting happiness.

Compassion can be roughly defined as a state of mind that is nonaggressive: a wish to see others free from suffering. In true compassion, this wish is deep and universal, not related to personal feelings or to attachments to particular people. Rather, it applies to all living creatures, including friends, enemies, and even a fish writhing on a hook.

The mental and physical benefits of a compassionate attitude have been well documented by research. These range from experiencing an emotional "high" after helping others to gaining a longer life expectancy themselves. But the most striking trait of a compassionate person is how widely their warmth to others is spread; they feel a strong affinity with all others, no matter whether they are rich or poor, close friends or total strangers.

To cultivate compassion, you must try to be empathetic toward others and actively try to understand things from their perspective. An effective method for this is to understand their backgrounds and focus on the commonalties you share. Say, for example, your cab driver tries to overcharge you. Instead of getting angry, you could think about what you and the driver have in common, like that you are both tired, hungry and want to get back to your families. Then, try to examine yourself in their shoes: How would you feel? This usually helps you develop empathy and reduce the anger you would feel, leading to more compassion and a happier life.

**Cultivating universal compassion is a way to a healthier, happier life.**

### 3. The Western notion of romantic love can be limiting and is often not enough for a lasting relationship. 

Having close, intimate relationships with other people promotes both physical and mental well-being, but the Western viewpoint that deep intimacy can only be achieved through a romantic relationship can be problematic. People who don't find such a relationship often feel lonely and unhappy.

But in fact, the concept and limitations of intimacy have varied greatly across different times and cultures, and a wealth of intimacy lies beyond the exclusively romantic Western definition. The Dalai Lama himself said he felt an intimate connection with a wide array of people around him, for example his tutors and cooks; he even went as far as discussing state affairs with a cleaner sweeping the floors. By embracing the countless opportunities to connect to other people every day, we can lead happier lives.

Often, we run into problems in our involvements with others. In these cases, it is vital to understand the underlying basis of the relationships. For example, romantic relationships based on sexual desire or based on the Western ideal of "being swept off your feet by love" are unlikely to last if they have no other more permanent basis.

Lasting relationships, on the other hand, are based on respect and appreciation of the other person. This kind of relationship requires knowing the deeper nature of the other person, which requires time. As Mark Twain said, "No man or woman really knows what perfect love is until they have been married a quarter of a century."

**The Western notion of romantic love can be limiting and is often not enough for a lasting relationship.**

### 4. Though religious beliefs can be beneficial to happiness, you can cultivate basic spirituality without them. 

An important ingredient of leading a happier life is _spirituality_.

The benefits of a strong religious conviction are well-documented in numerous studies and range from happier families to better health. But, contrary to what many people believe, spirituality is not dependent on any specific religion; the Dalai Lama believes any of the world's major religions can offer people the opportunity for a happier life.

In fact, there is also a kind of spirituality that exists completely outside of the sphere of religious belief: _basic spirituality_ comprises basic human qualities like goodness, compassion and caring for one another, and it is therefore attainable by atheists and religious people alike. Embracing these qualities brings us closer to all of humanity, helping us become calmer, happier and more peaceful.

The Dalai Lama spends about four hours each day in religious routines, but basic spirituality can be practiced in everyday life without prayers or mantras. For example, if you find yourself in a situation where you are tempted to insult someone, you can practice basic spirituality by challenging that wish and restraining yourself from indulging it. In this way, you can constantly train your capacity for basic spirituality without praying for hours each day.

**Though religious beliefs can be beneficial to happiness, you can cultivate basic spirituality without them.**

In the last five blinks, you'll discover how to overcome suffering and negative states of mind.

### 5. Suffering is a natural part of life, but we often increase it unnecessarily. 

_Suffering_ is a universal and perfectly natural quality of life. Eastern cultures seem to be more accepting of this fact, perhaps because people in Asia tend to live in closer proximity to poverty and daily suffering than their Western counterparts. Westerners tend not to understand that suffering is a part of life, and often see themselves as victims of some malign force when something goes wrong.

But suffering is inevitable; for example, all of us will grow old and die. Trying to avoid or ignore this fact is only a temporary solution. When you inevitably do encounter suffering in one form or another, your _mental attitude_ becomes of paramount importance. If you fear suffering as something unnatural and unfair, you will feel like a victim and assign blame when you should be trying to eliminate the mental root causes of suffering.

Suffering may be natural, but we often inadvertently magnify it ourselves by actively subjecting ourselves to unnecessary anguish.

For example, one mistake Westerners often make is to resist change and cling on to the things we care about or possess. But change is a constant and universal force — resisting it will inevitably result in suffering as we lose the things we have clung to.

Another common source of unnecessary suffering is unnecessarily hanging onto past negative events, mentally replaying them and perpetuating the pain. For example, some divorcees still seethe with anger toward their ex-spouses even decades after their divorce.

By accepting that suffering is natural, you can confront and analyze its causes — including whether you may be partially creating it — and begin to lead a happier life.

**Suffering is a natural part of life, but we often increase it unnecessarily.**

### 6. We can eliminate negative attitudes, feelings and habits only through sustained effort. 

The Dalai Lama believes that _negative states of mind_ like anger and fear are obstructions that stop us from achieving our natural, happy state. They are poisons. But certain positive states of mind — love, compassion, patience, generosity — can act as _antidotes_ to them, eliminating harmful emotions, attitudes and behaviors. Hence, to eliminate negativity, positive emotions and behaviors should be habitually cultivated.

This resembles the essential ideas behind Western cognitive therapy, where maladaptive behaviors and thinking are identified and, in a sense, corrected. Depressed people, for example, often have a distorted way of thinking: they may focus their thoughts exclusively on negative things, such as trouble at work or financial difficulties, and completely overlook the fact that they have much to be happy about, like good health and a charming family. Studies have proven that correcting these distorted modes of thinking can make people happier.

The process of ridding oneself of negative, destructive emotions and behaviors and replacing them with positive ones is long and gradual. To successfully instill good habits, you must understand why a change is needed and then translate that reason into the conviction and determination to change. Then, through sustained effort, it is possible to implement the change.

The process of habituation can take many years, and expectations of a "quick fix" are unrealistic. The Dalai Lama himself says it has taken him 40 years of practice to develop a deep appreciation for Buddhist principles and practices. His daily prayers, all four hours of them, are reminders of how he wishes to live his life. Through similarly determined efforts and frequent reminders, you too can eventually establish new behaviors.

**We can eliminate negative attitudes, feelings and habits only through sustained effort.**

### 7. Learn to shift perspectives and find the good in every situation. 

When people encounter a negative situation, they tend to see it, very rigidly, as 100 percent negative. Generally though, most situations contain both positive and negative elements and can be viewed from several alternative angles. For example, you might consider having to sit next to an annoying, flatulent person on a plane as a purely negative situation, or you could see it as an opportunity to practice patience and tolerance.

Such a switch can also help you _find meaning in pain and suffering_. When you next encounter obstacles in your life, do not wallow in self-pity and cry "Why me?!" but instead consider it a chance to become stronger. Find purpose in suffering, and thrive.

The ability to shift perspectives is facilitated by having a so-called _supple mind_ : a certain mental flexibility. Anyone can develop this flexibility by deliberately trying to shift perspectives as we encounter unpleasant events in life.

People with supple minds are sometimes seen as indecisive and inconsistent. After all, how can you abide by a rigid value system and yet remain flexible?

The Dalai Lama's solution has been to reduce his value system to its most basic principles that can be applied in a vast array of daily situations, rather than to specific rules that might be unnecessarily constraining and inappropriate in some cases.

It takes time and effort to learn to see the good in negative events when they occur. Therefore, you should start practicing immediately. Just as a tree cannot grow strong roots at the last minute to survive a storm on the horizon, you cannot suddenly decide to find meaning in a cancer diagnosis you were handed only moments ago.

**Learn to shift perspectives and find the good in every situation.**

### 8. Confront and analyze your feelings of anger and hatred, and replace them with patience and tolerance. 

Of all the negative mental states, _anger_ and _hatred_ are the greatest obstacles to happiness. When a feeling of anger or hatred arises in us, it rapidly destroys our peace of mind. It also obliterates our judgment, often leading us to take actions that only worsen the situation and make us even angrier. Scientific studies have clearly demonstrated that tendencies toward anger, rage and hostility have negative health effects too; for example, they substantially increase a person's risk of heart disease.

Anger and hatred cannot be overcome by simply suppressing them. On the other hand, venting anger, such as through raging and shouting, tends to increase negative feelings, not reduce them. Hence, the correct response to anger is to learn how to use the antidotes of patience and tolerance against it, and to cultivate them for example through meditative exercises.

Since anger tends to arise from a mind that is discontented, the first step toward dealing with it is to build a mindset of inner contentment. Studies have shown that stress decreases the threshold of feeling anger; hence, reducing stress by cultivating calmness and contentment can help reduce feelings of anger.

When you feel angry, the correct response in the eyes of both the Dalai Lama and Western scientific studies is to simply take a time-out: pause to analyze the situation. Where did the anger come from? What factors created it? Is it destructive or constructive? By applying such a logical appraisal to the anger, and by trying to replace negative feelings with thoughts of patience and tolerance, the anger often dilutes.

**Confront and analyze your feelings of anger and hatred, and replace them with patience and tolerance.**

### 9. Combat anxiety and low self-confidence by examining your thoughts, motives and capabilities honestly. 

Fear, anxiety and worry are things that all people experience from time to time — they are natural responses to certain circumstances — but when they become excessive or constant they can cause serious mental and even physical symptoms, such as weakened immune responses and heart disease.

The sources of anxiety are many, as are the preventative measures that can be applied. Much like the Western psychiatric practice of cognitive intervention, the Dalai Lama favors challenging the thoughts that generate anxiety and replacing them with positive ones.

Sometimes a specific situation may cause anxiety, for example, asking someone you like out on a date. In such a case, it can be helpful to examine the reason you are taking this action. Realizing that your motivation is proper and sincere — for example, that you wish to be kind to the other person — usually reduces fear and anxiety.

Excessive anxiety is often related to poor self-confidence, and the Dalai Lama feels the antidote for this is to be honest with yourself and others about your capabilities and limitations. If you're comfortable with your own limits, you can confidently admit when you cannot do something or do not know something, and not lose your self-esteem by doing so.

Sometimes low self-esteem can reach the extreme of _self-hatred_, where a person feels completely unworthy and may even contemplate suicide. The antidote to such an extreme mental state is to remind yourself of the marvelous intellect and potential for development within every single human being, including you. Tibetans contemplate this routinely in their daily meditations, which is perhaps why self-hatred is a virtually unknown concept in their society.

**Combat anxiety and low self-confidence by examining your thoughts, motives and capabilities honestly.**

### 10. Final summary 

The key message in this book:

**You can achieve lasting happiness but only through inner mental discipline — not as a result of external circumstances like wealth or good fortune. Cultivate compassion, spirituality and a supple mind, for they will help you deal with pain and suffering when they arise.**

The questions this book answered:

**How can we achieve lasting happiness?**

  * External circumstances cannot create lasting happiness — the right state of mind can.

**Why are compassion, intimacy and spirituality important for a happier life?**

  * Cultivating universal compassion is a way to a healthier, happier life.

  * The Western notion of romantic love can be limiting and is often not enough for a lasting relationship.

  * Though religious beliefs can be beneficial to happiness, you can cultivate basic spirituality without them.

**How can we overcome suffering and negative states of mind?**

  * Suffering is a natural part of life, but we often increase it unnecessarily.

  * We can eliminate negative attitudes, feelings and habits only through sustained effort.

  * Learn to shift perspectives and find the good in every situation.

  * Confront and analyze your feelings of anger and hatred, and replace them with patience and tolerance.

  * Combat anxiety and low self-confidence by examining your thoughts, motives and capabilities honestly.

**Suggested further reading: _The Art of Communicating by_ Thich Nhat Hanh**

_he Art of Communicating_ offers valuable insight on how you can become a more effective communicator by practicing _mindfulness_. Drawing on Buddhist wisdom, it outlines ways you become a respectful listener, express yourself well, and ultimately improve your relationships with your loved ones. To find these blinks, press "I'm done" at the bottom of the screen.
---

### Dalai Lama

His Holiness the Dalai Lama is a Nobel Peace Prize recipient and the spiritual leader of the Tibetan people. He has lived in exile in Dharamsala, India, since Chinese forces invaded and annexed Tibet in 1959, and he acted as the Tibetan head of state until his retirement in 2011.

Dr. Howard C. Cutler is an American psychiatrist who has studied Tibetan medicine and interviewed the Dalai Lama on several occasions.

