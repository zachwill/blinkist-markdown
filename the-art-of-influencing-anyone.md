---
id: 542815c43338660008300000
slug: the-art-of-influencing-anyone-en
published_date: 2014-09-29T00:15:00.000+00:00
author: Niall Cassidy
title: The Art of Influencing Anyone
subtitle: None
main_color: F17B30
text_color: 805D13
---

# The Art of Influencing Anyone

_None_

**Niall Cassidy**

_The Art of Influencing Anyone_ provides detailed information on how to influence people to do what you want. An invaluable read for salespeople, it is also useful for anyone who wants to learn how to sound more convincing and persuasive.

---
### 1. What’s in it for me? Discover how to influence the people around you. 

Contrary to popular belief, when we are being persuaded by others what we _want_ is more important to us than what makes sense to us rationally. When we understand this tendency, it is easy to see how we can influence others.

Whether you are a salesperson who wants to increase sales or you just want to be more convincing in everyday life, _The Art of Influencing Anyone_ provides clear instructions how to. Niall Cassidy shows us different methods to get friends, family, colleagues and customers to believe in you or your product.

In these blinks you will

  * learn how to come up with the perfect alibi;

  * discover the secret of fortune-tellers; and

  * see how a football club earned money by hiring actors.

### 2. Persuasion is not about logical arguments but about communication and appearance. 

Have you ever been in a position in which someone who didn't work hard and was under-qualified got promoted instead of you at work?

This frustrating situation is more common than you might think. It is because the decisions we make — including the decisions our bosses make — are not always based on logic. In other words, why isn't the most obvious means of persuasion also the most effective?

It's because instead of paying attention to logical arguments, we put more faith in the things we _want_ to hear.

Take two people presenting their ideas to their boss to start a business project. One of them shares a perfectly reasonable explanation as to why they should take it slowly, step-by-step. The other person is brimming with enthusiasm, saying that it's better to dive straight in, but doesn't present any real logical arguments to do so.

Who do you think the boss will select as project leader? Most likely the second one — the person who was more engaging and was enthusiastic about what the boss wanted to hear.

This common scenario demonstrates that the speaker's personality is in many ways more important than the content or rationale.

This is also evident in article selection for academic journals. When researchers want to publish their findings, they send their papers to academic journals, hoping that one of them will select their work for publication. So what do the people at the journals look for in an article?

First they look at the author's name. Second, the quality of the content. This was once proven by a professor who submitted an article which was utter nonsense. Shockingly, the article was published because he mentioned his PhD; people often reason that someone who holds a doctorate _must_ know what they are talking about!

> _"... We all like to convince others by using logic, but logic alone is hardly enough to change people."_

### 3. Always be warm and friendly and pretend you know what you are talking about. 

You've just robbed a bank and the police are about to arrest you. The only way you can get out of this is to have a convincing alibi. Unfortunately, the only person you can find to help you is a shady character who no one will take seriously. Is there a way to make him appear credible and trustworthy? Surprisingly, yes.

By offering as much detailed information as he can, he will come off as well-informed, and therefore, more convincing.

If you want to lie your way out of the situation, use it to your advantage. Although lying poses a risk, if you want to do it persuasively, make sure you provide as much detail and information as possible.

In the case of your alibi for the bank robbery, have your alibi accomplice provide precise details of what you were doing that day, which pubs you went to, what clothes you were wearing, who you met up with and so forth. All of these details will make your story more plausible.

If you are having trouble coming up with enough detail, behave in a friendly and humorous manner, as this will help to conceal any gaps in your knowledge.

This strategy proved effective in one famous experiment in which an actor pretended to be a well-informed professor and presented a talk for an audience of experts. The talk was repetitive and even included contradictory statements but remarkably, the audience stated that the talk was highly informative. This was because the actor was trained to appear warm and friendly and to share humorous anecdotes in his presentation. The actor's charming yet fake professor character concealed holes in his knowledge and made him appear reliable.

> _"You don't really need to be a real expert, because as long as you look convincing, people will still believe in you."_

### 4. Influence others by simplifying and organizing information so it’s easier to understand. 

So far we have talked about what makes people more persuasive. In this blink we'll get a bit more specific. Let's take a look at how businesses can be more persuasive.

If you are a salesperson and are addressing a customer, you should limit the customer's choice to just two items or services.

Although we know detailed information is important for credibility, no one likes to be bombarded with too much information.

We tend to shy away from too many options, but we do like to compare a couple of options in order to decide whether we want to buy something or not.

Imagine your customer is after a new jacket. In this case it plays to your advantage to show them only two different jackets and then share with them the pros and cons of each one.

After showing the jackets with their pros and cons, come to a conclusion and show them which jacket you think would be ideal. This way the customer feels that you have weighed up options and come to a specific conclusion that suits her particular desires.

Another method you can use to persuade people is to organize information into categories.

If you are a salesperson explaining the advantages of a particular washing machine, for example, it's a bad idea to just list them randomly because it can confuse the customer. Instead, try coming up with specific categories to structure your thoughts, enabling you to present them in a more digestible way.

Using categories like "environmental advantages" (saves water, requires less energy) and "efficiency" (faster than other machines, bigger so you can wash more clothes in one load) will help you order the information you wish to provide the customer and help the customer make a decision that suits her needs.

> **Fact** :   

Most of us can remember six random words, but by grouping them in categories (like words beginning with the same letter) we can remember far more.

### 5. Talking vaguely makes your customers think you know more about them than you do. 

Do you believe in fortune-telling? Maybe not, but most likely you've looked at your horoscope before and found that it somehow sounded plausible. So how is this possible?

Many fortune-tellers employ a technique called _cold reading_. So let's take a look at how we can use this to persuade in business as well.

Building a relationship with your customers is important, as this enables them to trust you. In order to do so, it helps for you to give the impression that you know a lot about them and their needs and wants, even if you don't.

The cold reading technique is actually very simple. All it takes is some vague sentences, which use a lot of statements that could be interpreted in many different ways.

So let's say you approach a customer who is looking at washing machines and you strike up a conversation. The first thing you should say is a general statement so that your customer agrees with you from the start. This could be "Doing laundry is a real pain, isn't it?"

After this, try to refer to a specific situation and your customer, by saying something like "I'm sure you want to spend less time doing your laundry." As most people can relate to this, your customer is likely to agree with you.

This statement doesn't require any mind-reading abilities, but using this method will make you appear to really understand and empathize with your customer.

You can further encourage this feeling in your customers by saying things like: "Working in homeware, it is important for me to really understand what customers like you need."

Again, this is vague, but it tells the customer that they can rely on you because you know so much about them and can relate to their needs and wants.

You don't need to read people's minds to look like you know everything about who you're talking to. Speaking about things broadly is all you need.

### 6. Curiosity is the key to persuasion. 

Did you know that boring commercials filmed in black and white are extremely effective?

How can this be? Well, this statement is actually wrong. But because it was strange, it caught your attention. It is essential to remember that without catching anyone's attention, you won't be able to convince people or sell anything.

Catching attention is vital because we are impatient and tend to lose interest quickly.

Living in the information age, there are so many things vying for our attention, such as Facebook notifications and a mass of other updates on our smartphones. This means that we have to make frequent, rapid decisions on whether we want something or not.

Interestingly, researchers discovered that only one in five people read further than the headline of an advertisement. So if you can't ignite curiosity within the first few minutes of talking with potential customers, they will most likely not buy your product.

To make sure you spark curiosity, begin your conversation with an interesting or surprising statement.

If you head straight into your argument, your customer will get bored. You should either start off saying something humorous, surprising or even controversial. The most important thing is that you've grabbed your listener's attention. (Just be sure to explain yourself later; otherwise you will seem untrustworthy).

One example of attention-grabbing is in a commercial in which a boy plays tennis against an adult man. Amazingly, the boy looks like he is scoring all the points. It's unusual and surprising, and therefore piques the viewer's interest. As the viewers are confused, they keep watching to the end — as the advertiser wants — because they want to know why the little boy is so good. In the end, it is revealed that he is the son of Steffi Graf and Andre Agassi.

> _"When you approach a prospect, you must make him interested as soon as possible, so that he is willing to give you more time to continue, instead of turning away."_

### 7. People don’t buy what they need, but what they want. 

Imagine you've just seen a commercial for a fantastic new smartphone. Even though you already have a great phone, we can all relate to the feeling of still considering buying it. Why?

The fact is, if you can evoke desire for your product, people will buy it regardless of whether they need it or not.

Most salespeople try to seek out a way to give their customers precisely what they need. However, it can be more effective to focus on what they _want_ instead.

You can spot this phenomenon in everyday life. Why do teenagers smoke, for example? It's not because they need nicotine, but because they want to fit in and be accepted by their peers. Therefore rational arguments about why smoking is bad for them won't stop them from buying cigarettes.

Instead of providing logical or rational reasons why customers should buy your product, give them reasons that provoke what they desire.

One way to spark their desire is to alter their perspective.

When you alter someone's perspective, specifically their self-perspective, you can also change the way they think and behave.

This is because we subconsciously want our behavior to line up with our attitude (or self-perception). So if you change one, the other can also change.

For instance, researchers at a swimming pool asked people about their water-saving habits (their attitude) and, as most people wanted to paint a positive picture of themselves, they stated that saving water was important to them. Following this, the researchers secretly timed how long people at the pool showered after swimming (their behavior). Those who had been asked about their habits took significantly less time in the shower.

By making them think about their attitude towards saving water, their self-perspective was changed, in turn shifting their behavior to align with their attitude.

### 8. Influencing people depends on covering up your true intentions so you seem less desperate. 

Say you're trying to decide which laptop you want to buy. Who would you trust more: a salesperson or a friend who's used both computers?

If you want to have more sales success, get your customers to sell for you. If you can get your customers to recommend to others, this will increase your sales and make you appear far less needy.

A few years ago, an English football club created a service that sends text messages to fans every time something newsworthy happened in the club. However, as it cost money to receive the texts, only a few people registered for the alerts. So, the club decided to hire actors posing as customers to go to pubs and bars during the day of a match and enthuse to other fans about this fantastic service. The result? An increase in the subscription rate from about 20 people to 120 people.

We tend to trust the opinions of other customers rather than the opinions of salespeople, so we should therefore be trying to think of ways to advertise products through the opinions of others.

But you don't always have to use someone else; you can do it yourself. For instance, when you meet someone socially who could be a potential customer, tell them a story about your product in a way that is not obvious to them that you are plugging your own business.

In scenarios like these, small talk is more appropriate than business topics. But don't let these opportunities go to waste. Instead, it's a good idea to have prepared an absorbing story connected to your business beforehand. That way, you have an opportunity to promote your business in a more covert way.

For example, if you are an insurance broker, you could relay a tragic story about one of your clients who died in an accident and because they had no life insurance, their family was left with nothing.

> _"Storytelling is essential [for] a good salesman ... you can bring your message across ... without looking as though you are hard-selling."_

### 9. When trying to persuade someone, don’t fight their resistance; utilize it. 

We've seen earlier that we can grab someone's attention by saying something surprising or provocative. To take this further, because unexpected things garner attention, you can also _behave_ in an unusual way to keep others interested in what you are telling them.

Many of us expect salespeople to ignore any resistance they might encounter. So if you are a salesperson, surprise the people you're selling to by embracing their resistance.

When we are faced with the question of whether we should purchase something, most of us reject the product first, which then tends to make the salesperson try extra hard to convince us that we're wrong.

Unfortunately, a lot of salespeople try too hard to sell and their attempts are pretty transparent, which is why we have a hard time trusting them.

Many studies have shown that if you try really hard to convince somebody, he is likely to become defensive and will become even more sure about his own opinion.

So if fighting someone's resistance is useless, what can you do to overcome it? The solution is to take advantage of it and use it to address and resolve the customer's doubts.

Imagine you are a car salesperson. Eliminating all the negative thoughts your customers have towards your products can be extremely difficult, so why not use them to your advantage? The first thing you should do is agree with your customer's concerns. Perhaps they think the car you recommended is too small. If you side with them on this, they will be pleasantly surprised and will start to trust you. After that, find a way to overcome their negative thoughts by showing why, even after all things considered, your product is great. You could perhaps point out that because the car is small, it's far easier to park it.

Because they now trust you, this gives them the sense that your argumentation is objective and they will therefore be more easily persuaded.

> _"You can sometimes practice a bit of 'mental judo' to reframe that initial resistance into a driving force to change the person."_

### 10. Final summary 

The key message in this book:

**We all have the ability to persuade others. By using effective strategies in how we interact, we can influence those around us and be more convincing not only about ourselves, but also about the products we want to sell.**

Actionable advice:

**Be surprising and controversial.**

Catching people's attention is vital if you want to sell something. When you approach customers, start your conversation with a surprising, humorous or even controversial one-liner to pique their curiosity.

**Agree with your customers when they point out your product's disadvantages.**

These days the language of hard-selling is pretty transparent and off-putting for customers. The next time you want to sell something, side with the customer when they mention some of the product's negative aspects. Just make sure you still point out the advantages and conclude with why it's an excellent buy anyway.

**Suggested** **further** **reading:** ** _Influence_** **by Robert Cialdini**

_Influence_ explains in detail the fundamental principles of persuasion that get us to say yes, including how they are used against us by compliance professionals like salesmen, advertisers and con artists. Knowing these principles will allow you both to become a skilled persuader yourself and to defend yourself against manipulation attempts.
---

### Niall Cassidy

Niall Cassidy is a successful salesman with years of firsthand experience in the banking and insurance industries. Drawing from his direct contact with customers, he has uncovered how to communicate effectively and persuasively.

