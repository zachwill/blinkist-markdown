---
id: 5681bc7a704a880007000064
slug: the-art-of-learning-en
published_date: 2016-01-01T00:00:00.000+00:00
author: Josh Waitzkin
title: The Art of Learning
subtitle: An Inner Journey To Optimal Performance
main_color: BE4C26
text_color: BE4C26
---

# The Art of Learning

_An Inner Journey To Optimal Performance_

**Josh Waitzkin**

_The Art of Learning_ (2007) offers a crash course in improving your mental performance. In these blinks, the author draws on experiences from his chess career and martial arts practice to present a range of methods and techniques to make your brain work harder, faster and more effectively.

---
### 1. What’s in it for me? Become a superior performer. 

"You win some, you lose some;" you've probably heard the phrase a million times. But even though there might be some truth to it, wouldn't it be better to not lose at all? This is exactly what _The Art of Learning_ is all about: how to outperform your toughest competition and become a superior performer.

With multiple chess championships and years of martial arts practice under his belt, the author is an ideal source for some great advice on how to improve your performance, including some useful techniques and methods.

In these blinks, you'll discover

  * why losing will make you a winner;

  * the best approach to the process of learning; and

  * how to handle those annoying distractions that impede your performance.

### 2. In order to win, you have to experience losing first. 

Sure, none of us like to lose. Whether it's a tennis match, the fight for a promotion or a game of Monopoly, losing isn't something we look forward to. But should we look upon losing so negatively?

The truth is, losing has its benefits. This is a lesson the author first learned when he was ten years old, when he began to compete in adult chess tournaments. He started off losing matches, which was frustrating at first. But he then started reflecting on his performance: why weren't his skills up to scratch?

The author realized that he was losing matches because of a lack of concentration. In adult tournaments, matches were twice as long. At such a young age, he simply couldn't match the focus and concentration of his older opponents. Losing helped him realize that endurance was his main weakness, so he began to work to improve it. 

If you want to improve your performance, you need to seek out opponents that are better than you. By investing in loss, you can welcome the opportunity to learn. This is true no matter what your specialty or field, and it's even true for children, too. 

These days, many parents and teachers believe that competition is unhealthy for children. But the opposite is true: just the right amount of competition can equip children to cope with obstacles later in life. So how much competition is the right amount? One way to approach it is by using short-term goals to nurture a long-term goal. 

If a child loses a sporting match or a competition in a hobby they care about, parents should first make sure to assure their child that it's okay if they feel sad or disappointed. Parents should also show the child how proud they are of her, and help her identify ways to improve. From this, the child can develop the short-term goal of learning new skills and developing new strengths before the next competition.

### 3. Prepare to feel vulnerable during the learning process. 

Natural talent can only take you so far. If you want to be the best, you've got to be ready and willing to _learn_, which means facing your own vulnerability and mistakes. This can be a little daunting, but it's a natural part of the learning process. 

When we're in a learning phase, we often feel weak, exhausted or hopeless, and this is something we can also experience physically. A boxer with a great right hand but a weak left hand will take plenty of punches and go through some tough rounds while learning to use his left side more skillfully. 

Although learning through trial, error and slow improvement is tough, we should be careful not to doubt ourselves. This can quickly lead to a _downward spiral,_ in which we berate ourselves for every mistake, and the possibility of achieving our goals seems to shrink at every turn. 

Luckily, it's simple to avoid this vicious cycle; it all comes down to taking a step back each time you make a mistake. 

If something goes wrong in your training or practice, find your own personal trick to regain clarity of mind. You could take a few deep breaths, splash cold water on your face or even sprint 50 yards! Whatever it is that works for you, it's vital to have this technique on hand so you can keep your positive energy flowing.

### 4. Stay on track with an incremental approach to learning. 

Out of all the motivated and talented people in the world, only a few really make it big. Why? Unfortunately, all too many people veer off their path to success after one mistake that scares them away. This is part of an _entity approach_ to learning, and it's one to avoid. 

When we see our intelligence, skill or talent as a fixed entity, success or failure seems to be the result of how much of this _entity_ you possess. This makes us far more likely to quit when faced with difficult challenges, as we believe that if we can't overcome them the first time, we simply don't have the ability to overcome them at all. 

In short, an entity approach to learning prevents you from doing any real learning at all. If you want to gain from the inevitable mistakes and shortcomings that you will experience in your training, you'll need to develop an _incremental approach_ to learning. 

By taking the incremental approach, we recognize that we have the ability to grasp any concept or skill, as long as we put in the necessary hard work. Unlike those employing an entity approach, people with an incremental approach are far more likely to rise to a challenge. 

The difference between these two learning approaches was revealed in a study where children were given easy math problems to solve. All of them solved the problems correctly and progressed to the next round, where they received much more difficult problems that they were unable to solve.

Children with an incremental approach were excited about the challenge, while children with an entity approach reported feeling discouraged.

In the final round, the children were presented with easy problems once again. Those with an incremental approach solved them easily; in contrast, the children with an entity approach, suffering from decreased self-esteem after the second round, were unable to complete problems they would easily have been able to solve beforehand.

### 5. Practice turns learned techniques into intuitive responses. 

For most of us, the incredible speed and agility of a martial arts master seems thoroughly unattainable. But is it really? These masters have simply trained so much that they have reached the point of fighting _intuitively_. You too can get this good at any skill — but how? 

As the cliché goes, practice makes perfect. Any technical information, from patterns to strategies to techniques, can feel like natural, instinctual intelligence if you practice and apply them enough. For example, every chess beginner learns that the pieces have numerical equivalents — a bishop, for instance, is worth three pawns.

At first, players will count the equivalents in their heads, but this eventually stops once they manage to improve their skill level. What happens? Well, something that was once seen mathematically is now felt intuitively. And once certain patterns become intuitive to you, that's when the fun really begins.

Skilled chess players are able to play with patterns, making small adjustments to confuse their opponents and gain advantages. One fundamental pattern or principle in chess is _central control_, whereby a player who dominates the middle of a chessboard has the strategic advantage.

But, if you're a chess star like Michael Adams, you can win even when all your pieces are along the sides of the board, having twisted the classic central control pattern to your advantage. The greatest benefit of training your intuition, however, is being able to free up your conscious mind. When you don't have to remind yourself of how certain patterns work, you can zoom in on different details.

A superior martial artist can use his extra focus to look for the subtlest weaknesses in his opponent's position, or even monitor their blinking in order to attack at just the right second. Yes, these are things that the human mind really is capable of!

### 6. Embrace disruptions as opportunities to cultivate resilience. 

Picture this: you're in a state of perfect concentration — and suddenly, your phone rings. Or your child comes to play with you. Or your partner has to ask you an important question about something. Would you get annoyed? 

When we react to disruptions with irritation, stress or anger, it means we're in the _hard zone_. In these situations, we feel that the world should cooperate with us and our current state of mind. But the world simply doesn't work that way! We should be able to function even under less-than-ideal circumstances. 

How? 

By entering the _soft zone_. This means rolling with the punches and accepting whatever circumstances we face, no matter how frustrating they might be. This, in turn, allows us to cultivate _resilience_ when disaster strikes. 

For example, the author once lost an important chess tournament because of a song that was constantly distracting him — it wasn't playing in the background, but it was stuck in his head. Even this was enough to shake the supreme concentration required to manage calculations during a match. 

Recognizing this as a problem, the author started playing music while practicing at home. But rather than trying to block them out, he learned to think within the rhythm of the songs instead! This boosted his concentration so much that he even started singing songs in his head on purpose during tournaments, just to keep his mental energy up. 

You can cultivate your mental resilience like the author by deliberately putting yourself into situations that challenge your concentration. If you're a writer, why not open a window and welcome the sound of your neighbors mowing their lawn as you work? By challenging your brain, you can only make it stronger.

### 7. Learn efficient recovery techniques to boost your performance. 

We all know that we perform at our best after a good night's sleep. Despite this, it's often too tempting to sacrifice some shut-eye and study late into the night instead. Resist it! 

Insufficient rest leads to repetitive, inefficient and imprecise thinking. Why? Because our mind is only able to stay focused on one thing for a limited time. After resting, our minds are better able to snap back into powerful concentration. The more able you are to clear your mind within a break, the better your performance will be upon returning. 

Superior performers can actually make full mental recoveries in short periods of time, as short as one minute breaks between chess match rounds. Psychologists at the Human Performance Institute (HPI) in Orlando discovered that training the ability to relax in short moments of inactivity is a decisive factor in progress toward becoming a leading performer. 

So how can you practice your recoveries? Consider taking up cardiovascular interval training. This means performing a series of short, high-intensity workouts with regular rests distributed throughout. HPI discovered that there is a physiological connection between cardiovascular interval training and the ability to quickly release stress and recover from mental exhaustion.

You can even practice cardiovascular interval training on a stationary bike. First, ride with high resistance, which will rocket your heart rate. Then slow down for a minute on low resistance, thus lowering your heart rate. Then sprint again until your heart rate is high. Then slow down again. And repeat!

The more you do it, the longer it'll take to raise your heart rate and the quicker you'll be at lowering it. With intensive training you will have better endurance, helping you relax more effectively when you need it most.

### 8. Create a routine to get into the zone. 

Do you have problems getting into the zone? Do you find yourself easily distracted in important meetings or when faced with a deadline? These situations can be incredibly frustrating. Wouldn't it be great if there was a switch you could flick that instantly took you into peak performance mode? 

Good news: that's not as impossible as it sounds! By creating and practicing a routine, you can summon your best performance. Think of a moment, situation or activity during which you experience complete and serene focus — moments that make you feel like nothing else in the world exists. 

Suppose this moment for you is swimming. Keep this in mind as you create a four- or five-step routine to precede this activity. Let's say you eat a light snack, do a 15-minute breathing exercise or other meditation practice, stretch for ten minutes and listen to your favorite song. Then go swimming!

If you repeat this routine everyday, you create a powerful physiological connection between the routine and the performance you give afterward in the swimming pool. As this routine becomes completely natural to you, you can transplant it from swimming to work, practicing it before important meetings or crucial tasks. 

Finally, once you've got your routine down, you can condense it into a shorter time frame. Often, we don't have an hour before any important moment to go through our routine. But we can make our routines more flexible, by altering the steps a little. 

You could exchange the light snack for heavier breakfast to have more energy for longer, or shorten the meditation or stretching outside by a minute, and listen to the song on your way to work.

By slowly and gradually adjusting the routine, you can condense it to few minutes. And with constant practice, the masters of this technique reach a state where only thinking about a part of the routine triggers their high-performance state.

### 9. Final summary 

The key message in this book:

**Anybody can achieve superior performance with the right mind-set, perseverance, dedication and strategy. Using performance psychology methods, you can learn to manage your ability to focus and relax, switching between them as needed.**

Actionable advice: 

**There's more to appreciate your personal best!**

Many of us make the mistake of only celebrating moments of peak performance; the rest of life becomes a waiting period between competitive events and opportunities to strut your stuff. But it shouldn't be this way! Enjoy life as we're meant to by engaging with the quieter, simpler moments in everyday situations. This will keep you grounded and focused throughout your journey to honing your skills.

**Suggested** **further** **reading:** ** _The Talent Code_** **by Daniel Coyle**

_The Talent Code_ uses recent neurological findings to explain how talent can be trained through deep practice. It shows how nurturing our cellular insulation — called _myelin_ — influences the development of our skills, and explains why certain methods of practice and coaching have been used in "talent hotbeds" around the world to great success.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Josh Waitzkin

Josh Waitzkin is an eight-time national chess champion and the winner of numerous world and national championship titles in martial arts. He gives seminars and presentations on performance psychology and is the president of the non-profit JW Foundation, devoted to maximizing students' potential through enriched educational processes.

