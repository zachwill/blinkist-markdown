---
id: 569c986ce37b120007000002
slug: the-art-of-loving-en
published_date: 2016-01-19T00:00:00.000+00:00
author: Erich Fromm
title: The Art of Loving
subtitle: None
main_color: F89077
text_color: 995949
---

# The Art of Loving

_None_

**Erich Fromm**

_The Art of Loving_ (1956) argues that love, like any other creative art, is something humans must practice in order to master. German-born psychologist and philosopher Erich Fromm describes various forms of love and highlights threats posed to them by capitalist society.

---
### 1. What’s in it for me? A classic exploration of the ingredients of love. 

Whether represented in Shakespeare's _Romeo and Juliet_ or James Cameron's _Titanic,_ love is a subject we find eternally fascinating, and part of the fascination doubtless stems from our inability to comprehend what it actually is. Is it merely a chemical reaction in the brain? Or is it something magical and, therefore, ultimately unknowable?

These blinks, based on Erich Fromm's classic exploration of love, shed light on why and how we love. They take you on a journey through the various forms of loving. You'll have experienced some of these forms; others may be new to you. You'll also gain insight into how to maximize your ability to love — a skill that is truly priceless.

In these blinks, you'll also discover

  * the difference between motherly and fatherly love;

  * how modern society is damaging our ability to love; and

  * why practicing love is like practicing a musical instrument.

### 2. Love is an art – but few people see it this way. 

What is love? An overwhelming sensation that we hope to experience? A chance encounter? All we need in life? While it can be all of these things, there's more to love than passion and fate. Much like any other form of art, love must be _learned_.

And yet, most of us think there's little to be learned about love. Why? Well, we assume that love gets complicated when it comes to _receiving_ it. When relationships fail, we often blame ourselves: we simply weren't lovable enough. 

On top of this, we live in a culture of consumption, a culture that's made love into just another commodity to be traded on the market. So we approach it with a market mindset. When two people fall in love, they feel that they've found the best object available on the market in light of their own exchange value. 

Finally, we're continually confusing the different states of love. Are we _falling_ in love, or are we _being_ in love? That sudden and stunning period of intimacy we feel after falling in love is often the result of sexual attraction. Yet, when it disappears, we often feel that love itself has disappeared, too!

These false conceptions of love must be overcome.

We can start doing this by changing our perspective on love — to _learn_ it as we would learn to paint or play the piano. And just like painting or piano playing, it all starts with theory. 

So, in the following blinks, let's explore what the theory of love entails and how we can put that theory into action.

> _"Is love an art? Then it requires knowledge and effort."_

### 3. Love is an act of giving, and it may be unconditional or conditional. 

Love is universal, and it's often one of the very first things we experience. In loving relationships, two people can become as one and yet remain independent and free. How? By being mutually willing to give love. 

Love is all about giving, and it has four key elements: _care, responsibility, respect_ and _knowledge_. Demonstrating each of these elements is what makes your love an active, giving love. 

Care is epitomized by a mother's love for her child, in her active concern for the well-being of her baby.

Love also requires responsibility, which entails staying aware and reacting to the needs of another, whether these are the physical needs of an infant or the emotional needs of a partner. 

Respect is the third key element of love. It's what brings us to accept our loved ones for who they are and resist changing them into what we want them to be.

And the fourth and final element of love is knowledge. It's not facts and figures we're referring to here, but emotional knowledge: knowing when our loved ones are nervous, worried or lonely. Being able to read the subtle signals of our loved ones is central to the act of loving.

With this in mind, let's take a closer look at the caring love we see in a mother-child relationship. Motherly love is _unconditional_. A mother loves her child simply because it is her child. But fathers usually don't love this way. For them, love is usually _conditional._ While a mother bonds with a child in its very first moments of life, fathers and children often don't bond until the child is a little older. 

Unfortunately, this means that fatherly love is often only bestowed once the child lives up to certain expectations. Conditional love can only be received if the giver feels their loved one deserves it. Unlike unconditional love, conditional love can also be lost.

> _"Without love, humanity could not exist for a day."_

### 4. There are other types of love: brotherly love, erotic love, self-love and the love of God. 

Maternal love and paternal love aren't the only two forms of love, of course. Love comes in all shapes and sizes. Brotherly love, erotic love, self-love and the love of God are four further manifestations of love. Let's explore them and see how they compare with maternal love and with each other.

_Brotherly love_ provides the foundation for all other forms of love. It means directing the four key elements — caring, responsibility, respect and knowledge — toward _any_ other human being. Brotherly love is what allows us to feel the human solidarity that is at the heart of many religions. As is written in the New Testament: _love thy neighbor as thyself._

But brotherly love differs from unconditional maternal love in that the relationship between mother and child is one of inequality; the mother has more responsibility to give love and care than the child. In contrast, brotherly love is a universal love shared between equals.

_Erotic love_ is yet another crucial form of love. While motherly love evolves toward separation and growth, as the child grows into an independent individual, erotic love works in the opposite direction, bringing two separate individuals together. Erotic love aims to unite two people both physically and emotionally. Erotic love occurs between two individuals. It's exclusive and, therefore, not a universal love in the way that brotherly love is.

_Self-love_ is simply one's respect for his or her personal integrity and uniqueness. Like maternal love, self-love also requires care. By caring for our own health and happiness, we demonstrate self-love. It is important, however, to make the distinction between selfishness and self-love. Selfishness arises when self-love occurs in isolation, and a person does not give any other forms of love.

Finally, there is the _love of God_. This is a religious form of love. It emerges from the human need to find meaning in our world and aims for a spiritual union with an all-knowing, transcendent being.

### 5. Modern capitalism turns our loving relationships into profitable exchanges. 

In the first blink, we learned how a consumerist culture warps our conception of love. Let's explore this further and get to the bottom of how Western capitalism harms our ability to give and receive love. 

Modern capitalism is defined by its radical division of labor. In large businesses, where employees perform highly specialized tasks, workers become nothing more than small cogs in a huge machine. In the auto industry, for instance, this specialization even limits workers to a single action. One worker may be responsible for mounting car doors on the assembly line, and nothing else. 

Limited to their small tasks, workers have little opportunity to find meaning or fulfillment in their jobs. They not only feel alienated from themselves but from their fellow humans, too. How can we expect humans to give and receive love when they feel like alienated automatons? It's no surprise that we regard human relationships as an investment to profit from. 

As a result, love is superseded by forms of pseudo-love in Western society. Take the idea of a happy marriage. Such partnerships have often been described as a smoothly functioning team, which sounds disturbingly similar to descriptions of ideal corporate partnerships. 

She should compliment his new shirt and his cooking, for example. He, in turn, should listen attentively when she talks about her troubles at work and forgive her coming home late and stressed out. This may be a great alliance, but it certainly isn't love and intimacy. 

False conceptions of love are widespread, and they hinder our ability to experience real relationships. It's time to start working against pseudo-love by practicing the _art_ of loving.

> _"The principle underlying capitalistic society and the principle of love are incompatible."_

### 6. There are three principles at the heart of loving: discipline, concentration and patience. 

If you were hoping for an easy-to-apply formula for loving in this final blink — well, sorry! Loving isn't that simple. But, there are three principles that will guide you as you learn the art of loving, or, for that matter, any other art form!

The first is discipline. As in any art, discipline is crucial, and you'll need it if you want to love well. We can bring discipline to our life by getting out of bed at a regular hour and devoting a set amount of time each week to activities that help us grow, such as meditation, reading or exercise. And we can try not to indulge too much in escapism, by watching movies or substituting a mystery novel for our real life. 

Concentration is the second principle that will keep you on the right track. Concentration is clearly vital to developing any new skill. Yet, it seems to be a rather rare asset in Western culture. We love to multitask and hate to waste time. However, this means we'll never reap the benefits of true concentration. 

The first step toward learning (or relearning) concentration is being comfortable with solitude. Being able to concentrate is impossible if you're unable to be alone with yourself and your thoughts. Try closing your eyes and imagining a blank, white screen. If you find it hard to block out interfering images and thoughts, you've got some work to do!

The third and final principle for practicing the art of loving is patience. Patience, like concentration, is hardly a fixture of today's culture. We want quick results and maximum output with minimum effort. To foster patience, try slowing down during the interactions you have in life.

### 7. Final summary 

The key message in this book:

**Love isn't just something we receive, but something we actively give, too. To love well, we must deepen our understanding of the different forms of love, and stay aware of consumerist forms of pseudo-love that threaten our relationships. By treating loving with dedication and discipline, we can strengthen our relationships with children, friends, lovers and strangers.**

**Suggested further reading: _A General Theory of Love_, by Thomas Lewis, Fari Amini and Richard Lannon.**

In _A General Theory of Love_, three psychiatrists take a scientific look at the phenomenon of love. Arguing that our emotional experience in adulthood is profoundly influenced by our childhood relationships, the authors suggest ways to undo this emotional "programing" and establish healthier relationships with friends and romantic partners.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Erich Fromm

Erich Fromm (1900-1980) was a world-renowned psychologist, sociologist and philosopher. Fromm represented the idea of a humanistic, democratic socialism. Born in Frankfurt am Main, Germany, he was the child of orthodox Jewish parents. In 1934, after the Nazis took over power in Germany, he moved to New York, later becoming a US citizen and professor at several universities including Columbia and Yale.

