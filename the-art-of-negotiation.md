---
id: 54b3b95464663300098c0000
slug: the-art-of-negotiation-en
published_date: 2015-01-15T00:00:00.000+00:00
author: Michael Wheeler
title: The Art of Negotiation
subtitle: How to Improvise Agreement in a Chaotic World
main_color: 235CAB
text_color: 235CAB
---

# The Art of Negotiation

_How to Improvise Agreement in a Chaotic World_

**Michael Wheeler**

_The Art of Negotiation_ offers a radical approach not found in most books on negotiation: no two negotiations are the same, and thus there is no silver-bullet strategy to negotiation. Rather, it offers insights on how to develop the skills to succeed in dynamic and unpredictable negotiations.

---
### 1. What’s in it for me? Learn how to become an awesome negotiator. 

What ever you do in life, the chances are that your job will contain an element of negotiation. Of course, for salesman and bankers the need to negotiate the best price is clear. But negotiation is important in other jobs too. When you need a pay raise you have to negotiate; if you want to buy a new home, you have to negotiate.

In short, we could all benefit from learning a few negotiation skills. These blinks will show you those skills and how you can put them to use.

In these blinks you'll discover

  * why having the wrong map is better than having no map at all;

  * what jazz musicians can teach you about negotiation; and

  * why negotiation is all about being flexible.

### 2. The secret to successful negotiation is to prepare a plan beforehand, but not necessarily stick to it. 

Imagine you're in an important salary negotiation, and you know exactly what you want: you _know_ that you're worth $120,000 per year — not less! — and so you naturally demand this amount. But what do you do when they say no? More than likely, you'll lose confidence and control of the negotiation, and consequently leave the negotiating table with far less than you had expected.

Well, it's good that you knew what you wanted. But you hadn't prepared a _map_ that considers many acceptable paths and outcomes so that you can make the most of your negotiations.

Your map should start by first identifying what your ultimate target is, i.e., your _stretch goal_, along with several _baselines_, or minimum acceptable outcomes. This way, if your negotiating partner doesn't accept your stretch goal, then you have something else to fall back on.

Baselines are important, as your negotiating partner has their _own_ goals which often contradict with your own.

Let's look at that hypothetical salary negotiation: if your stretch goal of $120,000 is rejected, then you can fall back on your baselines. Your baselines might be things like "a yearly salary of $100,000" or "$90,000 plus stock options."

While maps are practically useful, they are also useful in another crucial way: just feeling like you know where you are going can give you the confidence you need to succeed.

It was this very principle that saved a military patrol unit from certain death after becoming lost in the Swiss Alps back in the 1930s. The unit was in danger of dying in the wilderness when one soldier found a map in his pocket, which the team then followed to safety. The interesting thing here is that this wasn't even a map of the Alps, but the Pyrenees!

Yet, the assumption that they knew where they were heading nevertheless gave them the confidence to make it out alive.

### 3. While preparing for a negotiation, consider various scenarios and don’t forget your Plan B. 

Just like you wouldn't make a recipe without first preparing the ingredients, you should also thoroughly prepare yourself before negotiations.

Before approaching your negotiation partner, ask yourself: "When is the best time to negotiate this particular deal?" You should always try to schedule negotiations at the most advantageous time. You wouldn't, for example, try to sell your ski equipment in the spring! You'd be more prudent to wait until fall, when people are more interested in skiing, and thus your offer.

Then ask: "How likely is it that we will come to an agreement? How much would I benefit from this agreement?" You should seriously reconsider putting a whole lot of effort into a deal that you think is unlikely to succeed. It would be better to spend those resources on something with a stronger chance of success.

For example, you won't get very far if you try to haggle down the price of vegetables at a supermarket. But you might at your local farmers' market. If you're in the mood to haggle, then that's where you should concentrate your efforts.

But even with the most thorough preparation, things won't always go as planned. For this reason, it's important to stay flexible and creative, and willing to come up with a Plan B.

In the author's workshops on negotiation, he often uses the example of a small business owner who wanted to expand his business by buying another small company in town. Unfortunately for him, the largest offer he could possibly make was still much less than the minimum price than the other owner expected.

So, instead of buying the other company, the small business owner then offered to sell his _own_ company to his negotiation partner, and made a very profitable deal out of it.

As this example shows, when your plan has no chance of succeeding, then you sometimes have the opportunity to consider options that you hadn't even considered before.

> _"Whoever sits across the table from us may be just as smart, determined, and fallible as we are."_

### 4. Good negotiators are mentally and emotionally prepared. 

Despite their knack for putting on a good poker face, professional negotiators get nervous, too. Sometimes they're unsure about their odds of success or fear that they might embarrass themselves.

Nevertheless, as professionals they are able to control their emotional reactions, which is crucial in negotiations.

While you can't control the circumstances you find yourself in, you _can_ control your emotional response. Good negotiators must be able to embody several characteristics at once: they need to be calm and alert, patient and proactive, practical and creative.

While each of these pairs seems like a direct contradiction, in reality they go hand-in-hand. A surgeon, for example, has to be calm and confident in his skills while also staying alert to the possibility of complications. And just like a surgeon, a good negotiator must be in an emotionally strong state of mind at all times.

A good way to make sure you're in the right emotional space is to identify the circumstances that trigger negative emotions, and avoid them altogether. For example, if you're the kind of person who becomes angry when stuck in traffic, then you should plan in extra time on mornings when you need to negotiate.

In addition, good negotiators are careful not to fall into "autopilot." A routine that has helped you in the past can hinder you in the present, so never be too sure about anything.

One great way to keep yourself from dozing off or turning on autopilot is to focus on more than just what your conversation partner says. Pay attention not only to what they say, but how they say it and what their body language is saying.

So far, you've seen how to successfully prepare for negotiations. Our following blinks will show you what to do when things don't go as planned.

### 5. Don’t hesitate to improvise! Harness your inner actor or jazz musician. 

Successful negotiators know that they should always expect the unexpected. But how can you prepare yourself for these inevitable surprises?

You'll need to learn how to think on your feet — to _improvise_ — and the best way to learn how is to think like an actor.

_Improvisation_ is an acting technique that forgoes learning text by heart, but instead focuses on spontaneous and impromptu interactions. The golden rule of improvisation is to "never say no." If one actor opens by saying "Hi Patrick, how was Australia?" no one would reply with "Actually, my name is Andrew, and I just came back from India." You work with what you're given!

The same goes for negotiation. Try not to say "no" unless something is truly unworthy of discussion. Instead, try to work with the offer.

Moreover, actors also stay relaxed under pressure, and don't frantically wonder what they are supposed to say next. Don't try to find the perfect answer at the negotiating table. Not only is this unrealistic, but it will also cause panic and hesitation.

Instead, approach negotiations as if you are an improvising actor, and trust that, with your preparation and mental presence, you will always be able to come up with a good answer, even if it's not the best.

You can also learn a lot about improvisation from jazz musicians. While orchestral musicians read notes from a sheet, jazz musicians can improvise a masterpiece without any sheet music simply by listening, adapting and influencing each other via melodies and rhythm.

Likewise, adapting to circumstances, listening carefully and influencing others are fundamental for success in negotiations. It's absolutely essential that you understand what the other party wants and expects from the negotiation. Only by paying close attention to subtle cues, such as the tone of their voice or nonverbal signs of anxiety, will you know when to address or avoid certain topics during the conversation.

> _"Negotiation is like Jazz. It is improvisation on a theme. You know where you want to go, but you don't know how to get there. It's not linear."_

### 6. Foresight and good observational skills help to make good decisions. 

What do chess and negotiation have in common? Success in both require experience and foresight.

Take this psychological experiment, for example, in which chess pieces were arranged in random order on a chessboard and then given to participants. Participants had only a glimpse of the board before they were asked then to recreate the arrangement of chess pieces.

The results revealed that all of professional chess players' knowledge and experience did nothing to help them recreate the board. In fact, they did no better than the average of participants.

But when the experiment was repeated with arrangements from an actual chess game, professional players were much better at recreating them. They remember similar games from the past and were able to use those experiences to recreate the board.

The same applies to negotiation: your ability to interpret the other side's offers or tactical moves depends on your knowledge and experiences. And while your experience grows with every negotiation you handle, you can supplement your skills by doing things like reading books on negotiation, taking part in workshops and observing other people negotiate as often as you can.

Taking it back to chess: even the best chess players can't anticipate all possible moves. After 80 moves, they would have more options to consider than there are stars in our galaxy!

It's impossible to follow possible lines of play. So, instead, chess masters anticipate the lines of play that seem to have the best chances of winning the game.

In a negotiation, this means considering those options which have the greatest possible benefits and the best odds of agreement (based on what has worked in the past).

Nevertheless, your decision can always turn out to be wrong. Your opponent — both in chess and in negotiation — might see through your tactical moves, which would thus require you to change your strategy altogether.

That's why it's so important to be able to improvise!

> _"Deciding and acting gives you the edge in framing choices for your counterparts, rather than their framing the choices for you."_

### 7. The first few minutes at the negotiating table are the most critical. 

People often say that "you never get a second chance to make a first impression," and this is especially true in negotiation. First impressions establish both the atmosphere of the negotiation as well as your relationship with the negotiation partner. As such, it's important that you follow these guidelines:

First, watch your language! Use words and phrases that emphasize your shared interests. For example, rather than saying "I need to do X," say things like "Let's solve this problem together."

Second, be aware of your posture. Though you might not think it, your posture can have a great effect on your mood. If you have a confident and positive posture, like standing up straight and not crossing your arms, then you'll feel confident and positive, and others will get that impression of you.

Once you've made a good first impression, then you're in the position to either accept or reject an offer.

Only say "no" directly when their offer itself is totally unworkable. If the offer is workable but suboptimal, then it's often worth continuing the conversation. If this unsatisfactory offer is their _first_ offer, then there's most likely room for improvement. Just be aware that pushing the other side too far could result in no deal.

If you are in a situation where you have to reject an offer but don't want to lose the deal altogether, then you can try the _yes-no-yes_ approach.

Imagine, for example, that a working mom's boss asks her to work on a new project over the weekend.

In order to say "yes" to her responsibilities as a parent, she'll have to say "no" to her boss. But if she's creative, then she can still say "yes" to her boss' needs, for example, by offering to create a system whereby occasional weekend work is rewarded with free afternoons during the week.

By using the yes-no-yes approach, you can stay true to your own wishes but also concede to the other person's interests, thus allowing the negotiation to continue.

### 8. Many seemingly unsolvable problems can be solved with creativity and a fresh perspective. 

Sometimes you will get stuck in your negotiation. So, what should you do? Give in, or get creative?

Thinking outside the box is more than just a cliché — it's essential. Sometimes, it's the difference between success and months of wasted work.

The writer commissioned to write FDR's biography handed over a 1400-page manuscript to his publisher. The publisher asked him to significantly reduce its length, and because he didn't want to waste all that work, the contract was canceled.

Luckily, he later found another publishing company that liked his manuscript the way it was, and they suggested that he split it into two volumes.

Both volumes were a huge success and are still in print today, but this was only possible because of the second publisher's willingness to think beyond the traditional "one person one biography" and split the material into two books.

Creativity also involves bringing in an outsider's perspective. In fact, research has shown that people are better at solving problems for _others_ than for themselves. This _psychological distance_ between us and others can help to see things in a different light.

Just think about how easy it is for you to give advice to your friends when they are struggling, yet you likewise struggle when you're in the same situation. By being removed from the situation, your perspective may present more insight.

It's also important to think outside of financial terms. When we think of value in terms of money, a negotiation becomes often a zero-sum-game, whereby the more one party profits, the worse off the other party becomes. However, there is _always_ a way to ensure that all parties benefit! One way is by offering security.

For example, if you're a basketball player, you might be more willing to accept an offer for less money if it means that your contract is for a longer term. What you lose in money you gain in security, thus sweetening the deal.

### 9. Now it’s time to close the deal. 

All your hard work leading up to the negotiation is worthless if you don't know how to close a deal and persuade your negotiation partner to say yes to you in the future.

It's important to always be honest and polite. Not only will this help you close the deal, but it will also make your negotiation partner more likely to agree with you in the future.

When it boils down to it, would you want to make a deal with someone you felt was trying to deceive you? Even when it's hard to be honest, it's nonetheless important in forging the kinds of relationships that result in repeated business.

But nicety alone won't close the deal. You should also adopt these basic rules:

First, emphasize loss. Studies on loss aversion have shown that people are far more likely to agree with you if you focus more on what they might lose than on what they might get.

Imagine, for example, that you want to convince your boss that your company should get on board a new project. You can increase your chances of success by pointing out not _only_ potential benefits, such as gaining new clients, but also the negative consequences of not getting on board, like the possibility of losing clients to innovative competitors.

Second, keep it simple. Research shows that people are less likely to come to any decision at all when they're faced with too many options.

In one famous study, for example, supermarket customers were offered tastes of different jams. Of those who were offered only a few different types, 30 percent decided to buy a jar. But of those offered a selection of 24 different varieties, only three percent decided to buy any!

Don't leave your negotiation partner befuddled by an over-abundance of choice. Just stick with the few options you think have the best chances of success.

### 10. Act according to your personal values and treat others like you want to be treated. 

Imagine that you want to buy a cabin in the woods. One day you discover a "For Sale" sign on a beautiful little house, where the owner, a kind looking elderly woman, invites you in for tea.

Her asking price is surprisingly low, meaning that you could easily afford it.

So, what do you do? Accept the offer immediately, make her a counteroffer for less or tell this kind old woman her house is worth far more?

Let's assume you buy the house for the asking price without reservations. Yet, some years in the future you decide to sell the house because you've discovered that a new highway is planned for construction nearby. A young couple is interested in the house and asks you why you want to sell in the first place.

What do you do? Do you admit the truth or lie to them about your motivations?

While these questions might be difficult to answer, they do offer some insight into the negotiation process.

Firstly, as a buyer, make sure to always ask specific questions about the condition of the item and the circumstances surrounding the sale. Most people tell the truth when asked directly.

Secondly, as a seller, always consider what information you can give potential buyers. But try to keep things in perspective: think about whether you would recommend someone else to do the same in your situation, or whether you would want others to treat you the same way. And don't forget, the way people feel about the deal will be the way they remember you forever!

If the old woman was your grandmother, wouldn't you want buyers to pay her a fair price? And if you were this young couple, wouldn't you expect to be informed about the planned construction?

Ultimately, the way you decide to handle the ethics of negotiation is up to you. Just think about how you would feel if the situation were reversed.

### 11. Final summary 

The key message in this book:

**Just like no two people are the same, likewise no two negotiations are the same. Each comes with its own quirks, which means that the ability to think on your feet and improvise is of utmost importance.**

Actionable advice:

**Make some time to meditate this evening.**

Studies have shown that yoga and meditation not only help to reduce stress, but can also help increase your problem-solving skills and productivity — both of which are essential to negotiations.

**Suggested further reading:** ** _Crucial Conversations_** **by Kerry Patterson, Joseph Grenny, Ron McMillan and Al Switzler**

We've all been in situations where rational conversations get quickly out of hand, and _Crucial_ _Conversations_ investigates the root causes of this problem. You'll learn techniques to handle such conversations and shape them into becoming positive and solutions-oriented, while preventing your high-stakes conversations from turning into shouting matches.
---

### Michael Wheeler

Michael Wheeler is professor at the Harvard Business School who has taught negotiation techniques to thousands of students. In addition, he is editor of the _Negotiation Journal_, published by Harvard Law School and lives in Gloucester, Massachusetts.

