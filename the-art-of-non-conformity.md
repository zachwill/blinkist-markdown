---
id: 530493623439370008630000
slug: the-art-of-non-conformity-en
published_date: 2014-02-19T11:42:31.000+00:00
author: Chris Guillebeau
title: The Art Of Non-Conformity
subtitle: Set your own rules, live the life you want and change the world
main_color: BD263F
text_color: BD263F
---

# The Art Of Non-Conformity

_Set your own rules, live the life you want and change the world_

**Chris Guillebeau**

Based on the author's blog and online manifesto, "A Brief Guide to World Domination,"_The Art of Non-Conformity_ (2010) __ deals with ways of pursuing unconventional living and offers tools for setting your own rules, succeeding with your passions and leaving a legacy.

---
### 1. What’s in it for me? 

We'd all like to live an autonomous and fulfilling life. Often, though, we hold ourselves back from achieving this by being unclear about what we want from life, or being afraid of making the necessary changes to our daily existence. So, how can you free yourself from the shackles of an average and unfulfilling life?

_The_ _Art_ _of_ _Non-Conformity_ will show you how to live your life in a way that enables you to break free from a conformist, unfulfilling existence and achieve greatness. The book achieves this by presenting the most common obstacles to living such a life — for example, authority figures or "gatekeepers" — and showing you how to overcome them.

It also offers a variety of useful ways to set your own rules, structure your life and finances, and by doing so accomplish your life goals so you can live a fulfilled and unique life.

### 2. You break free from an unfulfilled existence when you find out exactly what you want to get out of life. 

It's an unfortunate fact: most people don't know what they want from life. Instead they amble around without a larger life goal or a clear sense of purpose.

Sadly, not having a clear idea of what you want from life holds you back from greatness and often leads to an unremarkably average life where you do the same as everyone else because that's what everyone else is doing.

There are many ways to be unremarkably average, but a typical example is sitting at a desk for 40 hours a week, for an average of ten hours of productive work, while getting the largest mortgage you qualify for and spending 30 years paying it off.

Such a life insulates you from challenge and risk. Common ways to ensure we avoid risky and uncomfortable encounters are by traveling to only English-speaking countries, or thinking vaguely about starting a business but never actually doing it. Playing it safe like this holds us back from achieving greatness in our lives.

On the other hand, deciding what you want from life — and taking this decision seriously — can change your life for the better, increasing your commitment to living a fulfilled life.

Consider Bernard Lopez, a former cubicle-dweller who quit his comfortable job to ride his bike across the United States. He'd realized that, regardless of the skepticism and discouragement around him, he needed to commit to his adventurous dream and act on it. The result? An unmissable adventure and a subsequent feeling of invincibility that encouraged him to begin a rewarding new career and experience the exciting possibility of traveling every summer. In short: a fulfilled life.

The nagging question, "Is this all there is?" is an indication that you are insulating yourself from greatness. So if you often catch yourself wondering if there's more to life, you might start to think hard about what it is you want to get out of it.

### 3. Don’t let fear keep you in an unfulfilling life; conquer that fear instead of avoiding or ignoring it. 

It's normal to be afraid of change. For most of us, taking a leap that will change our lives can be very frightening.

Have you ever had a voice in the back of your head saying, "You're not good enough to do that," or "You'll never succeed at anything significant"? If so, you know how it feels when fear tries to stop you breaking free from an unfulfilled life.

Clearly that fear needs to be conquered, and the first step is to acknowledge your fear.

Consider Sloane Berrent, a charity worker in her mid-20s from the United States, who — despite fearing the commitment — left an executive job to volunteer for the organization Kiva. This eventually led her to work in the Philippines, helping out where she could and doing something she felt was meaningful.

Her decision required first that she acknowledge her fear and transform it into a positive. She said that fear heightens the senses and that this can be used to zone in on one's intuition.

Once you've acknowledged the fear, the next step is to mentally prepare yourself, as this will increase the odds of successfully conquering that fear.

For example, asking yourself, "What's the worst that can happen?" can help to put any big decisions in perspective, and — because the worst-case scenario is usually not that bad — this can empower you.

Another way to ready yourself is simply to "give yourself a carrot." Linking achievements with rewards is a great motivator, so promising yourself a reward for conquering your fear will make this difficult process more appealing.

Breaking through the wall of fear requires you to acknowledge that fear and prepare to conquer it. Once you're on the other side of that wall you'll feel rewarded, competent and ready to rise to any challenges you set yourself.

### 4. Contrary to what most people think, the best job security is your own competence. 

Most people believe that having job security means working for someone else. Indeed, the general advice on how to achieve job security and live a good, comfortable life is, "Stick to a reliable paycheck!"

But, in fact, deriving that sense of security from your own competence — such as your skills and abilities — is less risky than entrusting someone else to look after your career. No one cares more about your well-being than you do.

One example of this principle at work is when one decides to become an entrepreneur. Consider the former newscaster who, after she was fired, started a "Yoga at Work" business for just $9 — the cost of the domain registration. Within six months she was earning $2,000 monthly, and — because she felt very competent at this work — had more job security than before.

But working for yourself is just one way to gain job security based on your own competence. You don't necessarily have to go it alone.

Take, for example, Allan Bacon. Allan had a "good job" — nice salary, benefits, etc. — but hated his work environment. So he started a series of "Life Experiments" — small- to medium-sized actions that would provide him with increased competence and make his work more enjoyable.

These actions ranged from little things, like visiting art museums on his lunch break to much more significant actions like deliberately downsizing himself to a lower-level job that he felt offered more opportunities to develop professionally.

What was the result of these experiments? More responsibility and an even higher salary, allowing him to feel more competent and gain greater job security without sacrificing his relationship with a company he liked.

As you can see, your primary job security can be found in your own competence. You can choose to set your own rules instead of leaving your security in the hands of others.

### 5. Challenge the authority figures that seek to limit your choices and stand in the way of you living a fulfilled and unconventional life. 

From time to time, everyone has encountered authority figures who function as _gatekeepers_ that seek to limit your choices.

Gatekeepers are effective at telling you which choices you have, so you get the illusion you're free to set your own rules. Meanwhile, your access to what really matters is blocked.

This was the case when environmental activist Tim DeChristopher tried to stop the unfair land auctions in Utah, which only big oil companies had a chance at winning. He wanted to prevent these companies from harming the land, but he encountered a gatekeeper: the Bureau of Land Management.

The bureau pretended the auctions were fair and open to everyone, when actually the "cost of entry" was $1.7 million, limiting participation in the auctions to Big Oil and blocking out everyone else — including activists like Tim.

But though gatekeepers often have a lot of power, if you're unhappy with the restrictions they impose you're free to challenge them.

The first step is understanding that few options are truly democratic. For instance, Tim decided to challenge The Bureau once he realized that such auctions were neither fair nor democratic, but, in fact, illegal.

The next step is to challenge authority figures by changing the rules of the game. One way is to look for alternative approaches that might work better for you. Tim's alternative approach was to register as a bidder — which, usually, only company executives do — and then unexpectedly beat out every other bidder for the land, even though students obviously don't have a spare $1.7 million.

Tim's unconventional approach caused the decision to auction the land to be rescinded and the results of the sale declared invalid. Now the 22,000 acres are protected by a federal mandate.

Gatekeepers can be obstacles in the way of a free and fulfilled life. But, luckily, you can challenge them by changing the rules of the game.

In the next blinks you'll find out how to set your own rules and succeed at getting what you want out of life.

### 6. To accomplish your goals in life you must recruit your own small army of followers and find out how to fulfil their needs. 

What do international charities, musicians and small businesses have in common? They need a small army of followers to be able to succeed.

And the same goes for you: once you've figured out the life goals you're committed to achieving, you need an army of followers to help you along.

If you dream of becoming a working artist, for instance, you'll need the support of fans and patrons. And if your goal is to succeed with an entrepreneurial project, you'll need a loyal customer base to ensure a reliable income over time.

To recruit your small army, think carefully about how you can help people get what they want. People will follow you only if your work helps them in some way.

When the author decided to create a website chronicling his journey around the world, a friend asked, "What does it do for someone like me?"

So instead of including only travel essays, he decided to add topics focused on achieving big goals and creating a life of personal freedom, thereby also meeting other people's needs.

With freedom as the primary goal, his followers can travel like he does or pursue something else more meaningful to them. This attracted many more followers to the website — so many, in fact, that he is now able to make a living from it.

And if your work inspires and uplifts people, you're also on the right track to recruiting your army.

For instance, in under one year, the blog _Zen_ _Habits_ secured more than 100,000 subscribers. The blog's author writes full-time about simplicity and setting goals, attracting his formidable army of followers through his humble and uplifting self-presentation.

Therefore, you should find out how you can get your own small army of followers to help you achieve your goals by first and foremost asking yourself how _you_ can help _them_.

### 7. Determine what your own values are and align your spending habits accordingly. 

Many people believe money can bring them happiness. The truth is that money and happiness _are_ correlated up to a certain point, but, beyond that limit, not so much.

In fact, studies show that this limit is relatively low — one estimate states $40,000 a year.

Having more money, then, doesn't automatically make you happier. Rather, it's what you do with your money that counts, and the key is to stick to your values.

Only when you know what you really want and value can you know how to make it happen and plan accordingly. For example, the author ensures that his spending is aligned with his values by prioritizing traveling over the "stuff" he doesn't really need.

And when your values are clear, you can begin to make the conscious choice to spend money _only_ on what you value. For example, the author is able to spend a significant percentage of his annual income on travelling (20 percent), because he doesn't have a car, spends only $100 a year on clothes and ensures that he doesn't have any debts to pay off.

If, on the other hand, you spend your money without a clear purpose, you'll likely end up heavily in debt.

For example, one young couple with stable jobs hadn't defined clear financial goals for themselves. By not giving any consideration to their own values, they'd amassed massive debts: $50,000 in student loans, two car loans, a loan from their parents and several credit cards.

But when their first child was born, they seriously considered their actual values, and began to control their spending and save money. Their clear purpose — and careful budgeting — enabled them to pay off their debts and start investing much more in unique life experiences than in the conventional "stuff."

No matter how you decide to manage your money, being clear about your values is a huge help. This will give you room for everything you've always wanted to do.

### 8. Make room for everything you’ve always wanted to do by eliminating the unnecessary things in your everyday life. 

People often lament the fact that they don't have enough time to do what they care about in their lives.

But there's a good chance that we can achieve anything we want. It just requires a little life planning.

To do this, first you need to take a careful look at your current obligations to determine which are actually necessary. Then it's time to wave goodbye to the unnecessary ones.

When you start to redefine how you spend your time, you should start applying a filter to all the everyday responsibilities on your plate, asking yourself, "Why should I do this?" and "What will happen if I don't?"

It also helps to remember that some obligations are easier to ax than others.

Take Seth Godin, who writes the most popular business blog in the world. When he's asked how he has time to respond to every email he gets, his answer is that he doesn't watch TV or go to meetings, giving him four or five more hours per day.

Once you've restructured your time in this way, you can start opening up to everything you've always wanted to do. Filling your days to the brim with things that you enjoy doing will enrich your life.

The best-selling writer Haruki Murakami is a great example of a man that embraced life to the fullest by ordering his life around the one thing he really wanted: to improve his writing.

In one book, he explains how doing this has made him focus on building a relationship with his readership, rather than with specific people, such as family and friends.

Some would consider it rude to order one's life around a few key priorities, as Murakami did. But the fact is that in order to devote most of your time to what you enjoy, you'll need to be forceful about dropping a lot of other things.

### 9. Focus on producing legacy work – work that will outlive you and make the world a better place for others. 

While you can do almost anything you want with your life, you probably wouldn't be satisfied with a life that revolves only around you.

So after you have thought hard about what you really want to get out of life, you'll want to begin to consider how you can improve the world for others by doing work that leaves a legacy and makes an impact.

This is where _legacy_ _work_ comes in — it offers you meaning and fulfilment through deeds which have a lasting impact on others.

Regardless of what you've done before or where you are in life now, you can help others in a unique way that would've been impossible without your influence. That's what legacy work is all about.

There are many ways to do legacy work. You just need to answer the question: "How will this really help people?"

Take, for instance, Dr. Gary Parker, who lives in Africa doing free reconstructive surgery for patients who lack adequate medical care. It wasn't until he first decided to help others there that his work started giving him meaning and fulfillment.

And staying focused on legacy work can be achieved in several ways:

You can do what the author did and create a continual metric for your most important work — for example a 1,000-words-a-day standard if your legacy work consists of writing, or one set of sketches a day if you're an artist.

Alternatively, you could use Jim Collins' method (author of classic business-strategy books) for ensuring he spends most of his time on legacy work: He carries a stopwatch with three separate timers to adhere to the strict goal of spending 50 percent of his time on research and writing, 30 percent on teaching and 20 percent on "other" tasks.

Legacy work will ensure that what you bring to the world will continue to be valuable for a long time. Are you up for it?

### 10. Final summary 

The key message in this book:

**To break free of an unfulfilled existence you have to find out what you really want to get out of life and what you can offer the world that no one else can. Breaking free can be scary, but don't let fear stand in your way. Instead, remember that you don't have to live your life as other people expect you to — and strive to find security in your own competence.**

Actionable advice from the book: 

**Make a "life list."**

One way of figuring out what you really want to get out of life is to write down your idealized, perfect day and then make a "life list" — the things you would like to do at some point in your life. This will tie the structure of your ideal day (e.g., spending the majority of your time writing) to your focus on the larger goals (publishing a great novel within the next two years).

**Make a "to-stop-doing list."**

One method to stop spending time on unnecessary distractions is to make a "to-stop-doing list" for tasks that bring you down without giving fulfillment or helping anyone. Try to come up with three to five things you currently do that drain your time and what you'll probably discover is that many of these tasks can be removed without any serious repercussions. What's more: getting them out the way will leave you with extra time for projects and activities you actually enjoy.

**Suggested further reading: _The Happiness of Pursuit_ by Chris Guillebeau**

_The Happiness of Pursuit_ is about embarking on a quest to fill your life with a sense of purpose. It explains how the process of finding and following a personal journey can be deeply rewarding and life-affirming. The reader can expect to come across many interesting case studies of people who have followed their passions — from a young environmentalist who spent a year living in a Tasmanian tree to an Oklahoma woman who dedicated herself to cooking a meal from every country in the world.
---

### Chris Guillebeau

Chris Guillebeau is an expert on creative self-employment and international travel who recently accomplished his goal of visiting every country in the world. He is also the author of the _New York Times'_ bestseller _The $100 Startup_ and a regular contributor to CNN.com, _Business Week_, the _Huffington Post_ and other outlets.

