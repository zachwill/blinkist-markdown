---
id: 5bf9d2786cee070007cf02e5
slug: the-art-of-public-speaking-en
published_date: 2018-11-26T00:00:00.000+00:00
author: Dale Carnegie, with J.B. Esenwein
title: The Art of Public Speaking
subtitle: None
main_color: B97E25
text_color: 805719
---

# The Art of Public Speaking

_None_

**Dale Carnegie, with J.B. Esenwein**

_The Art of Public Speaking_ (1915) remains one of the most popular and widely-read guides to making effective public speeches. Its approachable language and applicable tips have been turning would-be speech-givers into master orators for more than a century.

---
### 1. What’s in it for me? Master the art of public speaking. 

Spiders, heights, public speaking. We've all got our phobias. But even if you'd rather walk across a spider-infested tightrope strung between two skyscrapers than give a speech before an expectant audience, public speaking needn't be such a terrifying thing.

As these blinks explain, the ability to speak in public is a matter of practice. It'll be uncomfortable at first, but there's only one way to overcome that discomfort — you've got to plunge right in.

Once you've done that, the rest is a relative breeze. Indeed, mastering the oratory art is a fun and fantastic endeavor. Sure, there's a great deal of practice involved, but, in the end, successful speaking hinges on sincere feeling and a belief in the subject you're presenting. These blinks will give you a range of practical tips — from how to overcome stage fright to how to arrange an audience — with which to undergird your personal sincerity and belief.

Along the way, you'll also learn

  * how arranging an audience is like building a campfire;

  * why you can't nail one tree's branches to another tree's trunk; and

  * what basketball players have in common with public speakers.

### 2. Becoming a skilled speech-giver is a matter of practice, and stage fright can be conquered. 

Do you remember how you learned to swim? Did you go to the library, study a manual on the art of swimming and then, brimming with hard-won knowledge, confidently don your swimming gear and plunge fearlessly into the nearest body of water?

Probably not. Whether you remember it or not, you learned to swim by, well, swimming — and there was likely a lot of awkward thrashing about and water up the nose before you felt at home in a pool.

Why dwell on your introduction to the aquatic realm? Well, mastering the art of public speaking is much like learning to swim. Giving speeches is the only way to become a skilled orator. And that means jumping into the proverbial deep end.

At first, you'll probably be filled with apprehension when standing before an audience. But don't fret. Many great speakers, from the British statesman William Gladstone to the American clergyman Henry Ward Beecher, never ceased to suffer from stage fright.

But speech-giving isn't a matter of becoming fearless; it's a matter of mastering your fear. Here are three ways to do that.

The first is to banish feelings of self-consciousness by letting yourself _be absorbed by the subject of your speech_. If you're utterly focused on the idea or message that you're trying to communicate, there'll be little room for idle worries about your appearance or how the audience perceives you. Subordinate yourself to the content of your speech, and all undue concerns about self-presentation will vanish.

It's equally important to _have something to say_. Many speakers fail because they approach the podium unprepared. If you haven't prepared your material and practiced your speech, doubts and misgivings are sure to assail you the minute you open your mouth. To avoid this, the author advises memorizing at least the first few sentences of your speech.

Then, _after preparing for success,_ _expect it_. Expecting success doesn't mean you should be overconfident and complacent. Rather, maintain your humility — not a cowering, servile humility, but a sort of vibrant humbleness, an eternal openness to improvement. Cast aside your self-involved fears, and be _willing_ to succeed.

Your first few speeches may feel like a sort of drowning — but keep practicing, and they'll soon be going swimmingly.

> _"If you believe you will fail, there is no hope for you. You will."_

### 3. Use emphasis to vanquish monotony. 

Imagine you're a pianist. Whether you're playing your own songs or the compositions of someone else, there are countless ways to interpret the music. You could play slowly or quickly, softly or loudly; with wild flourishes or with rigid uniformity. There are, in short, no hard-and-fast rules for _how_ music should be played.

The same can be said of public speaking. There is no end to the number of ways to give a speech successfully, but first, you'll need to master the speech-giving basics.

In speech as in music, monotony is the enemy. Imagine trying to play a Bach concerto on a one-keyed piano. No amount of determination or ingenuity could keep your monotone performance from being as dull as death. So how can you avoid monotony? Well, you've got to equip your public-speaking instrument with an array of new notes.

The first key (pun intended) to a dynamic speech is _emphasis_. Emphasis is a matter of comparing and contrasting your speech's central ideas, and a basic way to do that is to stress important words.

Consider, for example, the following sentences: "Destiny is not a matter of chance. It is a matter of choice." What would be the clearest way to utter these sentences?

Well, rather than emphasize each word equally, you'd stress the word "destiny," since it's the subject of the first sentence. Then you should stress the word "not," to emphasize the negation. And "chance" certainly needs emphasis, since you're going to juxtapose it with the next sentence's central word, "choice."

Now, emphasizing a word doesn't necessarily mean saying it loudly. If you've been speaking at high volume, you might whisper the noteworthy word, or if you've been speaking in a resonant tenor voice, you might rumble it forth in a deep bass.

Indeed, _changing your pitch_ is the first of three techniques for stressing a speech's central ideas. The second and third are _changing your pace_ and _pausing_.

In day-to-day speech, people naturally speak faster when relating exciting events and slower when delivering momentous facts. And they often incorporate pauses for dramatic effect.

So you may want to pause directly before, or right after, a significant word or phrase. Or you could rush through the first, less important part of a sentence and then slowly enunciate the crucial, concluding words.

Some of your instrument's keys are now before you. How you play them is a decision you'll have to make.

> _"Monotony is poverty, whether in speech or in life."_

### 4. An ability to arouse emotion in your listeners is the fulcrum of public speaking. 

Imagine two speakers, each delivering anti-slavery speeches in pre-Emancipation Proclamation America. The first is a white politician, a man with a solid record of anti-slavery activism. The second is a black mother on the auction block, a woman who's just watched her son get sold down the river.

Whose speech do you think would be more stirring?

Well, the jury isn't out on this one. Many of American history's most heartrending speeches were given by just such women — enslaved black mothers decrying the inhumanity of slavery. These women had no formal training in public speaking. But they possessed something that neither study nor practice can bestow: the force of feeling.

Feelings guide us through life. Why do we sleep in soft beds or drink cold water on a hot day? We don't use logic and reason to make such decisions; they simply feel right.

All aspiring orators should take this fact to heart. Arousing the feelings of your listeners, if only for a moment, will do more to win them over than hours of ingenious, rational argument.

This truth is driven home by a little advertising experiment conducted by a New York watchmaker. He launched two ad campaigns. The first emphasized a watch's many attributes, from durability to functionality to design. The other detailed the ways in which owning it would bring pleasure and pride, as summed up in the campaign's slogan: "a watch to be proud of."

It'll come as no surprise that the second campaign did better than the first, selling twice as many watches.

So how can you infuse your speeches with feeling?

We won't sugarcoat the matter: it takes work. Whenever you give a speech, you must fully _enter into_ its subject. What does this mean, precisely?

Well, pretend you're an actor and you're speaking through your character. No matter the cause you're arguing for or the case you're making, you must, in a certain sense, _become it_. Occupy it so fully that you wear it like a costume, so that it possesses you like a spirit.

Many actors forbid others to speak to them for hours prior to a performance. Try something similar. If you're able to transform yourself into your subject, then you'll be sure to inspire both interest and emotion in your listeners.

### 5. Gestures can be learned, but they must spring from real feeling. 

What are you going to do about that tree? You know, the gnarled apple tree in your backyard with the stunted, leafless branches? Here's a possible solution. You could dash to the garage, grab your chainsaw, saw off the branches of your neighbor's towering oak tree, haul them to your yard and nail them gloriously to your tree's trunk!

Ah, if only horticultural difficulties were so easily overcome.

You don't need a green thumb to know that a tree's outward appearance depends on its inward condition. But it takes a leap of the imagination to extend that truth to the art of gesture.

When giving a speech, your movements and gesticulations must emanate from the real emotions you experience when occupying your speech's subject. Theatrical, affected gestures will look as ridiculous as oak branches nailed to an apple tree.

So gesture must be the spontaneous outgrowth of true feeling — but that doesn't mean you can't practice it.

Now, you can't prepare each and every gesture that's going to accompany your speech. Effective gesture must fit the occasion, emerging organically and spontaneously each time you give a speech. If you've ever watched a talented speaker give the same speech twice, you'll have noted that gesture changes from delivery to delivery.

But spontaneity doesn't ensure quality. Indeed, organic gestures are often awkward. So, to make your movements effective, prepare for each speech by watching yourself in a mirror. Note gestures that seem awkward or unnecessary, and adjust accordingly.

Gesticulation is like pronunciation. The more you practice it, the less you'll have to think about it. Practice, practice, practice, and your gestures will become effortless and natural, emerging spontaneously at the correct moments.

Furthermore, keep in mind that too much movement tends to distract from a speech's message. Do your best to eliminate all unnecessary gestures.

Also, make sure your gestures accompany your message. It would be odd to say, "There he goes," and then, after a one-second pause, point after a fleeing gentleman.

And remember, facial expression and posture are both a kind of gesture. Be sure that both your posture and your expression match your speech's spirit!

Once you've put in the practice, you can rely on your own good sense. Let the speech's subject be your guide, and your gestures will soon be as powerful as your words.

> _"Nature does not always provide the same kind of sunsets or snow flakes, and the movements of a good speaker vary almost as much as the creations of nature."_

### 6. A good voice requires good health. 

What do you think basketball players have in common with public speakers? Sure, both ought to be comfortable in front of crowds — but there's something else. They've both got to be in good cardiovascular condition!

Whether you're running in for a slam dunk or trying to make yourself heard in a large auditorium, a strong pair of lungs can only help.

Good lungs are crucial to a resonant, powerful voice. Indeed, the author knew one successful orator who would practice his speeches while running, thus forcing himself to take deep breaths and improve his lung power.

But what if you're not the athletic type? Well, here's one easy exercise that will both help your lungs and train you to breathe using your diaphragm, which is the best way to get the most air.

Stand with your hands on your waist. Now, keeping your hands in place, try to touch the fingers of one hand to the fingers of the other, thereby squeezing all air from your lungs. Inhale deeply into your stomach without raising your shoulders. Repeat.

But lung capacity isn't the only criterion for a strong voice. Relaxation is equally important. If you want your voice to carry across a room, your throat must be open. There are some simple exercises that'll train you to deal with unhelpful tension.

With your waist functioning as a pivot, move your torso around in horizontal circles. As you do so, relax your neck, letting your head fall forward. This will help your throat open and relax.

To increase your throat's openness, pretend that you're yawning. You'll notice that, as you do, your throat naturally opens. Now, instead of concluding the yawn, try to speak. This should result in increased volume and richness of tone.

Vocal carrying power is not only achieved through volume; it's also a matter of placement. People at the back of a theater will have no trouble hearing the crackle of a piece of paper being crumpled on stage, though it's by no means a thunderous sound. You can make even a whisper audible to all if you place your voice correctly.

The way to do this is to pitch it _forward_.

Practice this by holding your hand before your face and forcefully saying words such as "crash," "dash," "whirl" and "buzz." Do this until you can literally _feel_ the tones hitting your hand.

### 7. Arrange your audience to increase the influence of your speech. 

Ah, fresh air! Chirping crickets! The star-strewn night sky! You're camping, and all you need now is to start a fire and get some hot dogs roasting. You collect some nice dry sticks, cast them about at random, light a match and apply it to the nearest bit of kindling.

If you have any camping know-how at all, you'll have already noticed a critical flaw in this fire-making choreography. Your stick arrangement is all wrong. If you want to achieve a hearty blaze, you'll need to place your sticks in a pile so the flame can pass from one to the next.

Let's say a speaker is a match and her speech's influence is the flame. If she wanted to ignite the hearts and minds of her listeners, how would she want to arrange her audience?

Setting metaphors aside for a moment, let's look at why situating audience members close together can increase the influence of your speech.

If you arrange the audience so it's clustered in a dense mass, you may transform it into a crowd. A crowd is nothing but a peaceful mob, and mobs, as the nineteenth-century social thinker John Ruskin once noted, are prone to "think by infection." In other words, if you can transform your audience into a crowd, your opinion will catch "like a cold."

In addition to this first crowd-creating strategy, you can also unite individual listeners by rallying them around common concerns. Appeal to their needs and fears, their aspirations and feelings. Once they individually perceive that their preoccupations are shared by their fellow audience members, they'll congeal into a crowd.

But maybe you doubt that crowds are truly prone to such mental contagion? Well, have you ever gone to a concert and had this experience — the music ceases, someone starts clapping and then, within seconds, everyone erupts into full-blown applause, even though the silence was just a break between movements?

Contagion.

Or let's take a leaf from history's book: many autocratic governments, such as the Soviets, recognized the potency of crowd mentality and banned citizens from congregating in public spaces.

Why? Fear of contagion.

These governments worried that anti-authoritarian sentiment would catch and spread like sickness.

Once you've honed the ability to create a crowd, your public message will begin to spread like — to rekindle our metaphor — wildfire.

### 8. Strengthen your power of argumentation by testing your arguments. 

There was once a king who wanted to rule the world. And he had a marvelous skill: he could build impregnable castles. But this king also had a fatal failing: he was incapable of toppling his enemies' fortifications.

Building an irrefutable argument will get you nowhere if you're unable to refute the arguments that might be used against you. Indeed, if you can't poke holes in the counterarguments of potential disputants, their assertions will be, as far as you're concerned, as unassailable as yours.

If you wish to be an effective speaker, you must be capable of both building arguments _and_ tearing them down. For, sooner or later, every speech-giver will find his views being challenged.

The author explains how to build and demolish arguments in an interesting way. Rather than prescribing a bunch of argumentative dos and don'ts, he provides a list of helpful questions.

Every argument has four parts: _the question under discussion_, _the evidence_, _the reasoning_ and _inferences_. Below are eight questions — two for each part — that'll help you test any argument.

For the question under discussion, first ask whether it's stated in clear terms. This entails ensuring that all of the keywords mean the same thing to each disputant. For example, if an opponent uses the word "gentleman," be sure his definition of the word is the same as yours. Second, ask whether it's stated fairly. Maybe there's too little information — or perhaps the argument's formulation contains a trap.

For the evidence of the argument, first ask which experts are being cited. Are they impartial? What makes them an expert? Are their opinions clear, reliable and unbiased? Second, ask which facts are being mentioned. Are there enough? Do they support or contradict one another? Are they confirmed or debatable?

For the reasoning of the argument, first ask whether the facts presented might support a _different_ conclusion than that being offered. Second, ask whether all counterarguments have been shown to be relatively weak.

And for inferences, here are two helpful questions: first, are you guilty of _non sequitur_ — that is, offering an argumentative conclusion that doesn't follow from the evidence? And, second, do all your pieces of evidence harmonize with each other?

Remember, don't only ensure that your own arguments are impervious to these questions. Direct them at your opponents' arguments, too. That way, you'll be a double threat: as invincible as our king in his castle, but with some fortification-felling clout of your own.

### 9. Use the imagination to your public-speaking advantage. 

Argument is the foundation on which all convincing speeches are built. But if a speech is merely a well-wrought chain of logical links, it will lack luster and life. It will be sturdy, no doubt — but who will want to listen to it?

That's why you should harness the power of the imagination before you approach the public-speaking platform.

The first way to do that is to employ _figurative language._

Let's say that your speech's argument is that alcoholism is a destroyer of happy homes. Now, you could stand before your audience, announce your thesis and then list a long, tedious list of statistics that back it up. This might work if you find yourself speaking before a crowd of fact-loving data analysts. But let's be frank: it would put most people to sleep.

The better option is to inflame your listeners' imaginations with a story.

Weave a tale of a drunkard returning from his weekend binge, yelling at his wife and hitting his children. This is not only more likely to grab your listeners' attention; it will also stick with them in a way that cold numbers and bland generalizations simply won't.

The next way to use your imagination is to _image_ the speech you're about to give.

In other words, prior to delivering a speech, you should create mental images of how that speech will go. This includes "imaging" your audience. Imagine the crowd you're going to speak to, as well as their reactions, both positive and negative. This will both reduce anxiety and prepare you for potential mishaps.

Now, with your audience firmly before your mental eye, run through your speech. Imagine what you'll say, how you might say it and which gestures you may use. If you can picture it in images, you'll be less likely to forget something and more likely to make a compelling delivery.

Imagery is the backbone of poetry. What many public speakers forget is that they, too, are a kind of poet. Your speeches will be sure to stand out if you keep this in mind and let them unfold in _images._

> _"All in all, master your images — let not them master you."_ __

### 10. Final summary 

The key message in these blinks:

**There's only one way to become an effective public speaker: practice, practice, practice. That said, there are techniques that'll help you succeed. For starters, avoid monotony by effectively using emphasis, let your gestures emanate from sincere feeling, turn your audience into a crowd and improve your voice by attending to your cardiovascular health. Finally, test your arguments and the arguments of your opponents, and use imagery both to prepare for and to construct your speeches.**

Actionable advice:

**Build your vocabulary.**

Your speeches will be considerably more effective if you have a large vocabulary. A strong command of language is the only way to forcefully communicate ideas. Now, if you truly want to broaden your vocab, you must _use new words_. So next time you pick up a copy of Montaigne's essays or Wordsworth's poetry, note down some unfamiliar words. And then — this is the crucial part — incorporate them into your next speech.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Talk like TED_** **by Carmine Gallo**

In _Talk_ _like_ _TED_ (2014), you'll learn about presentation strategies used by the world's most influential public speakers. Author Carmine Gallo analyzed more than 500 TED talks to identify common features that make these talks so influential and appealing.
---

### Dale Carnegie, with J.B. Esenwein

An American writer and lecturer, Dale Carnegie was a master of public speaking. He dedicated his career to improving the lives and abilities of his many readers and listeners. His other books include _How to Win Friends and Influence People_ and _How to Enjoy Your Life and Your Job_.

J.B. Esenwein was an American academic and writer. He served as editor for multiple US publications and taught English at Pennsylvania Military College.

