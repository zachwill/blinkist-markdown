---
id: 58beac988eba16000496c80f
slug: the-art-of-rivalry-en
published_date: 2017-03-09T00:00:00.000+00:00
author: Sebastian Smee
title: The Art of Rivalry
subtitle: Four Friendships, Betrayals, and Breakthroughs in Modern Art
main_color: 197D43
text_color: 197D43
---

# The Art of Rivalry

_Four Friendships, Betrayals, and Breakthroughs in Modern Art_

**Sebastian Smee**

_The Art of Rivalry_ (2016) details the remarkable accomplishments of some of history's greatest artists and the personal relationships with their peers that propelled them to creative success. These blinks explain how rivalry between artists drives innovation and how relationships have been central to the growth of the arts in general.

---
### 1. What’s in it for me? Understand how rivalries helped create some of the world’s best art. 

In business, rivalry and competition are often seen as crucial factors in the creation of the best products and services. In the artistic fields, however, competition is often portrayed as a drama of creative geniuses pitted against each other in bitter feuds. But is this really the case?

In these blinks, we'll look at the relationships between some of history's greatest painters: Edouard Manet and Edgar Degas, who were both part of the Impressionist movement in the nineteenth century; Henri Matisse and Pablo Picasso, who redefined art in the early decades of the twentieth century; Lucian Freud and Francis Bacon, whose portraits and grotesque paintings, respectively, have granted them a special place in art history; and Willem de Kooning and Jackson Pollock, who were both key players in the advent of modern and abstract painting.

The intense meetings of minds between these artistic peers has profoundly affected art history as we know it today, so let's explore the artful rivalry among these renowned luminaries.

You'll also find out

  * about the impact extroverted painters had on more introverted ones;

  * how Matisse played an integral role in Picasso's development of Cubism; and

  * why Pollock's personality influenced the quiet de Kooning.

### 2. Many famous artists have engaged in friendly rivalries, despite far more malicious fights among their supporters. 

Have you ever heard the term _frenemies_? It refers to people who are close, but who are constantly at each other's throats. Since the general public love to hear about heated competition between creative geniuses, the relationships between artists often seem to turn out this way. But despite what people want to see or read about, artists themselves aren't always so malicious.

Just take the rivalry between Henri Matisse and Pablo Picasso. People were absolutely thrilled at the idea of these two painters hating each other.

In fact, Picasso's supporters would spread graffiti across Paris with government-style warnings regarding the health dangers posed by Matisse's art. They even shot rubber-headed arrows at a portrait of Matisse's daughter, Marguerite, that Matisse had given Picasso.

You can imagine how disappointed these fans were when they discovered that the two artists would often pay each other studio visits or walk together in the Tuileries Gardens.

Or take Willem de Kooning and Jackson Pollock, who were pitted against one another by the rival critics Harold Rosenberg and Clement Greenberg. While this critic-provoked animosity added tension to their creative processes, the men were still great admirers of each other's work and shared tremendous mutual respect.

That being said, Pollock, who was a bit of a wild card, would show his respect for de Kooning in an unusual way; he would bait the other artist into a fight, or shout abuse at him during exhibition openings. On other occasions, he would simply praise his fellow painter's work in the press.

But regardless of how they show it, the personal development of artists depends on the respect of their peers. After all, by influencing those in his field, an artist can become more widely recognized and thus broaden his success.

For instance, Picasso was inspired by Matisse's deconstruction of form, known as deformation. This technique worked by changing the normal proportions of a figure to create a more visceral impact.

The technique became widely recognizable within Matisse's work, eventually causing Picasso to open his mind and reconsider his entire approach to painting. As a result, Picasso developed Cubism, a style that revolutionized painting and for which he would forever be recognized.

### 3. In friendships between artists, the older, more outgoing of the two tends to dominate – but that’s not to say their influence isn’t reciprocated. 

Most relationships experience some degree of asymmetrical balance of power, and those between famous artists are no exception. For example, Francis Bacon and Edouard Manet both held an upper hand of sorts in their friendships with their peers Lucian Freud and Edgar Degas, respectively.

While Bacon and Manet were both older and more established when these friendships began, in both cases they were also the ones with the more extroverted personality as compared to their younger colleagues.

Bacon was known to be a charming man who was often the life and soul of the party. This trait made a lasting impression on Freud, who was more quiet, yet prone to reacting impulsively — and sometimes even aggressively — in certain situations. Through his relationship with Bacon, Freud learned that the use of charm could often be far more productive than that of brute force.

Or take Manet, who loved being out in the streets and cafés of Paris to meet other artists and discuss the current climate of the art world. His impulse for socializing made him the unofficial leader of the _Batignolles group_, which had Claude Monet, Pierre-Auguste Renoir and Degas as members.

Beyond this extroverted personality, Manet also had a strong sense of self and a firm belief in his own value. This perspective had a profound effect on Degas, helping the younger artist to come out of his shell.

But that doesn't mean the less dominant individuals in these relationships had no effect whatsoever; for instance, Freud was always known for his portraits, which were a signature aspect of his style from the beginning.

It can be inferred that Bacon's later success with portraiture was a result of Freud's influence. But what's particularly telling is that, while the friendship between Freud and Bacon eventually came to an end, Bacon continued painting portraits of Freud for the rest of his life.

### 4. Matisse and Picasso both profoundly influenced each other. 

In much the same way that Bacon's extroverted character made him a role model to Freud, Matisse, too, had a profound influence on his more introverted peer, Picasso. But that dynamic wasn't set in stone; Picasso's profound desire to emerge from Matisse's shadow eventually pushed him to invent Cubism together with Georges Braque, a landmark movement in art history.

Picasso felt insecure around Matisse who, through his native French, would regularly captivate crowds of people at the soirées held by his friends and collectors, Gertrude and Leo Stein. Picasso, a Spaniard, spoke rather broken French and could never hope to match Matisse's charisma or hold an audience of potential patrons as he did.

Because of this, he felt held back by Matisse; just when he thought he was close to matching the other man's level of invention and risk-taking, Matisse would take his work to another level, leaving Picasso to play catch-up once again.

This didn't change until Picasso painted _Les Demoiselles d'Avignon,_ a piece that established him as an equally great painter, albeit a stylistically different one. Even so, Picasso owed much of what he used to create his own ingenious style to Matisse.

Matisse had shown him an African statue at one of the Steins' parties, pointing out its unique value to Picasso. The statue had inspired Picasso's approach to art and led him to his great invention of Cubism.

Following Picasso's breakthrough, Matisse also began to use Cubism in his work. For instance, until then, Matisse had always painted with strong colors and soft lines, but by 1913, he was sacrificing color and using sharp, cutting lines — all central elements of Cubism.

Matisse went on to paint a portrait of his daughter Marguerite in a style totally unlike any he had employed in his previous renderings of her. It was clear that Cubism was the spark that set this transformation in motion.

> _"At times, Picasso was looking more intently at Matisse; at other times, it was the other way around."_

### 5. More dominant artists often inspired their traditional counterparts to work with greater freedom. 

In many friendships among painters, the extroverts tended to dominate the relationship — but they also worked in a different way, tending to paint with greater freedom. In turn, this inclination had a major impact on their peers.

In most cases, the technically superior of the two would be inspired by the other to adopt a freer style. Just take Freud, Degas, de Kooning and Picasso, all of whom were superb draftsmen, with technical expertise far surpassing that of their rivals. All of them were encouraged to approach their work with greater freedom, of both hand and mind, neither of which came naturally to them.

For instance, Bacon was known for "ambushing" his paintings in a frenzy, with zero regard for the final outcome. Freud, on the other hand, would work methodically and with great attention to detail.

Bacon would pounce on the canvas, painting a significant portion before pausing to consider it, while Freud would calculate the effect of every stroke. Bacon's proclivity for this method meant that he often threw away entire canvasses, starting over from scratch. Even so, Freud eventually came to embrace Bacon's unchained spontaneity.

Or take de Kooning, whose masterpiece, _Excavation,_ is noticeably inspired by the drip painting technique of Jackson Pollock. Although de Kooning returned to a relatively more traditional form of painting after completing this piece, his work retained a sense of freedom it had previously lacked. In fact, de Kooning actively suppressed his tremendous drawing skills, feeling that the freedom Pollock exhibited was the future of painting, and that his dependence on talent and technique was keeping him in the past.

By producing work that was both unique and contemporary, the meeting of more traditional minds with those of maverick artists allowed the field to continually grow and evolve. But the influence of these mavericks didn't stop with the other artists they inspired. We'll see what other effects they had on society in the next blink.

### 6. Pollock was a particularly dominant character who changed art forever. 

The great abstract-expressionist painter Jackson Pollock first became interested in art after his brother, Charles, took it up. His early teachers considered Jackson to be essentially talentless, but they failed to see his true ingenuity, which would come to transform the history of art.

Pollock was a wild child of the 1940s and 50s New York art scene. To be blunt, he was an alcoholic with a penchant for violent outbursts.

As a result, he had a volatile relationship with Lee Krasner, a fellow artist who had sacrificed her own career to help Pollock's. Krasner was so in awe of Pollock's genius that she supported him despite his frequent abuse.

Willem de Kooning, a good friend and rival of Pollock's, saw the painter's recklessness and spontaneity reflected in his work. Finding this passion incredibly inspiring, de Kooning endeavored to incorporate these same qualities into his own art, and managed to adopt such passion professionally without letting it damage his personal life.

But Pollock's freedom didn't just inspire de Kooning — it changed modern art forever.

While Pollock never broke free of his demons — he died in a single-car accident while driving drunk — he did inspire artists all over the world to create their work with freedom. His influence can be seen in everything from performance art to contemporary dance and even poetry; in other words, his legacy extends well beyond visual art.

This spillover is in large part attributable to Pollock's use of a decentralized focus in his work, also known as an "all-over" composition. This now-commonplace term describes an important shift from a clear subject or narrative to the overall, visceral effect of the work.

So, while the eccentricities and extreme personalities of artists like Pollock might seem disruptive, we should be grateful for these quirks as they enrich our culture, giving us endless food for thought.

### 7. The relationships between artists supported the advancement of their field. 

The artists you've learned about in these blinks are some of the most influential of all time. In fact, they were so important that their genius inspires people all over the world to this day.

And, while it might not have felt this way at the time, the relationships they had with each other were a driving force of the creativity that cemented their status as legends. After all, competition between these artists pushed them well beyond what they would have been capable of on their own. In the process, these very rivalries advanced the field of art as a whole.

Without each other, these artists wouldn't have built such unique and powerful identities. The respect they felt for their peers led them to adopt some of the other artists' best qualities into their work, yet they remained steadfast in their commitment to making their own unique mark on the history of art.

Take de Kooning's experimentation with Pollock's drip-painting style. This foray led to recognition for the artist after years of being sidelined.

But beyond having an effect on the trajectory of art, these artists had a major impact on one another. Just consider Freud, who kept Bacon's painting _Two Figures_ in his private collection for his entire life, even though the two had a falling out in the 1950s. Or consider Picasso, who kept the portrait of Marguerite that Matisse had given him until his death.

De Kooning was so affected by his relationship with Pollock that he moved to a house opposite Pollock's burial site and even dated Pollock's former lover, Ruth Kligman, after the artist passed away.

And finally, Degas, after giving Manet a portrait of Manet and his wife that Manet later attempted to destroy, took back the gift, keeping it in his studio for the remainder of his life.

If not for the relationships between these great artists and the inspiration they spurred on in each other, we might not ever have experienced the tremendous works of art they produced — art that will continue to touch people long after the artists who made it are gone.

### 8. Final summary 

The key message in this book:

**The giants of modern art would not have made it nearly as far as they did had they not formed relationships and rivalries with their peers. Without these friendly competitions, the landscape of modern art would be entirely different and likely much less interesting.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Steal Like an Artist_** **by Austin Kleon**

_Steal Like an Artist_ (2012) will help you unlock the secret to creating great art: theft. No artist creates their work in a vacuum: all art is influenced by the art that came before it. _Steal Like an Artist_ teaches you how to "steal" from the work of your heroes, and use it to create something new and unique. It also provides important advice on using the internet to launch your career, so others can enjoy your creativity!
---

### Sebastian Smee

Sebastian Smee writes for the _Boston Globe_ as an art critic and is a recipient of the Pulitzer Prize. He has contributed articles to the _Guardian_ and the _Daily Telegraph_, among other publications, and is the author of _Freud_ (2015).

