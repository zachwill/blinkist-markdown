---
id: 5e3f7f616cee070006b5f8f3
slug: the-art-of-simple-living-en
published_date: 2020-02-11T00:00:00.000+00:00
author: Shunmyo Masuno
title: The Art of Simple Living
subtitle: 100 Daily Practices from a Japanese Zen Monk for a Lifetime of Calm and Joy
main_color: None
text_color: None
---

# The Art of Simple Living

_100 Daily Practices from a Japanese Zen Monk for a Lifetime of Calm and Joy_

**Shunmyo Masuno**

_The Art of Simple Living_ (2019) explores the little habits that will make a big difference in your daily life. It explains the teachings of Zen Buddhism and reveals how to put them into practice. Packed with useful tips, this is your how-to guide for a more tranquil life.

---
### 1. What’s in it for me? When it comes to happiness, less is more. 

Life can be a stressful undertaking. In our competitive world, we are told to win big, stay hungry, and maximize our productivity. And if we don't measure up, then stress, anxiety, and a feeling of failure come knocking. But what if there were a different way to live? A way that were simpler and more joyful? 

In these blinks, you'll learn over 20 of the best daily practices from Zen Buddhism — practices that can help you step back from the maelstrom. They'll help you uncover the habits and choices that will bring a greater sense of peace and fulfillment to your everyday life. From your morning rituals to your diet to your relationships, you'll discover how to revamp your attitude, and usher in a permanent sense of calm and well-being. 

In these blinks, you'll learn

  * why monks go barefoot;

  * the difference between simplicity and frugality; and

  * what calligraphy can do for your soul.

### 2. Transforming your own attitudes is the key to getting along with people. 

Is there anything more complicated than other people? Whether you're dealing with a rude coworker or a critical parent, some human relationships are far from simple. So how can we bring a sense of calm to even the most strained interactions? 

**The key message here is: Transforming your own attitudes is the key to getting along with people.**

But what does this mean? Well, it starts by changing where you put your focus. 

All too often, we concentrate on someone's bad points. But what if you focused on his good qualities instead? When it comes to making character assessments, take your inspiration from the way in which the author — a Zen gardener — thinks about trees. When he considers where to plant a tree in his garden, he considers that tree as an individual with its own identity. He looks at how the tree bendsand asks himself what mood the tree evokes. Finally, he decides where to place it in relation to his garden's other elements, so that its unique beauty will be revealed. 

The author understands that the features in a Zen garden must be placed in a particular way if they are to exist in harmony. In the same way, if you want a harmonious relationship, you must appreciate the other person's unique qualities, and learn how they interact with your own. 

Of course, to understand someone, we must take the time to get to know him. Unfortunately, in the modern world, we tend to concentrate on the sheer number of people that we loosely know. We feel good about the _breadth_ of our social network, rather than the _depth_. 

But Zen Buddhism teaches us a more joyful approach to relationships. 

_Ichi-go ichi-e_ is a saying in Zen philosophy. It translates as _once in a lifetime_, and means that every social encounter is precious, because you may never see that person again. With this in mind, you should concentrate on deepening your connection with each person you meet. After all, it may be your only chance to get to know him. 

But there will always be some people you don't get along with. Even in a Zen temple, there are monks who dislike each other. When you find a connection faltering, don't try too hard to make it work. Instead, remember that when a tree comes into blossom, the birds are attracted to its branches. If _you_ are happy and blossoming, the right people will come to you.

### 3. Treasure what you already have. 

In a world filled with shopping malls and credit cards, we are encouraged to spend, spend, spend. Excessive consumption has become something to be proud of — even to celebrate. But there is an alternative way to live, and it will cost you a lot less money — and a lot less pain.

**The key message here is: Treasure what you already have.**

Buddhism teaches us that greed is an addictive poison. We may get what we want, but we are never satisfied for long — we quickly desire even more.

As long as you let greed seep into your life, you won't find peace. Your only hope of living a free and contented life is to cast greed off, and embrace a simpler existence. 

You can reject greed by paying attention to the Buddhist teaching of _chisoku_, which means "be satisfied." Practice chisoku by firmly telling yourself "this is enough" once you acquire the minimum amount to satisfy your needs. Don't let your _desires_ enter into the equation. Chisoku is the antidote to greed's poison because it lets us recognize that we are already fulfilled. Once you embrace this practice, your mind will become calm and serene. 

So the next time you're dissatisfied, hold up your frustrated hopes and dreams and ask yourself, "Do I really need them to come true?" 

This being said, living a simple life is not the same as living a frugal one. 

Frugality means living with cheap possessions that you don't care about. However, living a simple life involves having only what you need, but loving and treasuring everything you own. For example, you might just have one cup for coffee — it could even be an expensive cup. But it will be needed, used and enjoyed. 

Finally, simplicity doesn't mean running to the store every time you need something. Instead, a fundamental tenet of simple living is using what you already have and putting it to use in imaginative ways. 

When you're trying to do more with less, take inspiration from the Zen gardens in Kyoto. Known as dry landscape gardens, they do not contain any water features. Yet they are so creatively arranged that the viewer believes she can hear the sound of a mountain stream. The gardens don't need this stream to be effective — the viewer imagines it instead. This just goes to show that a free mind and a creative spirit make almost anything possible. No credit card required.

### 4. Changing the way you eat, drink and dress can give you more energy. 

Would you like to have more energy? Endless rounds of work, household chores and family commitments can wear us down. But how do we escape? It might not be possible to cut down your workload or take a long vacation. Luckily, there are simple ways in which you can make yourself feel better. 

**The key message here is: Changing the way you eat, drink and dress can give you more energy.**

One way to feel invigorated is to take off your shoes and socks. Believe it or not, monks go barefoot every single day. After a while, this practice strengthens their bodies. This is why monks don't often get colds. 

It might not be practical to go barefoot outside your home, so slip on a pair of thong sandals instead. 

This simple footwear has many health benefits. This is because the skin between your big toe and second toe is a pressure point for your internal organs. Wearing thong sandals means you're massaging this pressure point every time you go for a walk. 

You can also gain vitality by eating less meat. 

When you base your diet around vegetables instead of animal products, your mind becomes more tranquil. And because mind and body are connected, your skin will be clearer and more radiant, too. On the flip side, eating a lot of meat makes your spirit troubled and argumentative. Your skin also suffers, and becomes discolored. Of course, it's not easy to get rid of meat in your diet, so make vegetables the main event for just one day a week at first. Then gradually increase your vegetable intake from there. 

Still in need of an energy boost? Then try doing _more_, not less. You might assume that less effort equals more energy, but actually, lingering over a task can be very invigorating. 

Just consider your morning cup of coffee. At home, you use a coffee maker. On your commute, you visit a café and pick up a cardboard cup. But how would you feel if you worked for your coffee instead? If you went to the woods at dawn and gathered firewood. If you made a fire to boil water. What if you ground your coffee beans under the sky as the morning sun shone on your face? That cup of coffee would be more invigorating than anything you could buy at the store. Why? Because you have breathed life into each step of its creation. And a vital life, like a good cup of coffee, demands time and effort.

### 5. Simple changes to your morning routine bring calm and focus to the rest of your day. 

What's your go-to reply when someone asks how you're doing? Many of us automatically respond by saying, "I'm fine, just very busy!" Can we ever hope to change this hectic status quo? Yes, we can. And it all starts with adjusting your alarm clock. 

**The key message here is: Simple changes to your morning routine bring calm and focus to the rest of your day.**

You probably think you're busy because you lack space in your schedule. But are you lacking something else, too? Interestingly, the Japanese character for _busy_ is represented by symbols meaning _lose_ and _heart_. So when you say you're too busy to do something, what you're really saying is that you have no more room in your heart. 

Luckily, there's one small change that will calm your hectic mind and make you feel less busy. All it takes is waking up a little earlier each morning. 

Setting your alarm clock fifteen minutes earlier will give you some time for a peaceful cup of tea or coffee. Try to sit by an open window if you can. As you hear the birds greeting the sun, your spirits will be lifted. And for those precious fifteen minutes, you will have freed yourself, and your heart, from busyness. 

Another simple way to clear your mind in the morning is literally to clear the things around you. 

Follow the example of Buddhist monks who live in Zen temples. Each morning, they make time to clean every inch of their space. Of course, the temple is never particularly dirty, but that's not the point. The real reason the monks clean so much is because they believe it clears and enlightens their minds, too. In fact, the Japanese word for enlightenment literally means _clean mind._ Every time the monks use the broom, they are also sweeping the cobwebs from the corners of their minds. When they buff the floors, their inner selves get a polishing, too. 

As you carry out your own cleaning ritual, take special care over your shoe collection. 

Zen Buddhism teaches us the importance of noticing our footsteps. Otherwise, how can we know in what direction our life is heading? You can pay more attention to your own footsteps by simply lining up your shoes. By doing so, you'll ensure that your next step in life is orderly and beautiful.

### 6. Art and calligraphy can improve your life in simple yet powerful ways. 

The art world can be an intimidating place, full of exclusive galleries and exorbitant price tags. But when you take a Zen approach, you'll realize that you yourself can create powerful works of art — and own them, too. 

**The key message here is: Art and calligraphy can improve your life in simple yet powerful ways.**

Celebrated Zen artists can teach us a lot about the simple joys of picking up a pen. 

Consider the fifteenth-century Zen monks Ikkyu and Sesshu, who are still celebrated today for their intense calligraphy and exquisite paintings. Although their works are highly prized, these masterpieces were not made to be appreciated by others. Instead, these monks drew and painted so that they could connect with their true selves. In fact, the real value of Zen art is that it allows the artist to express the essence of his or her character. 

You can embark on your own journey of self-discovery by practicing calligraphy or painting a picture. Tell yourself that no one will ever see what you are going to create. Then, with great care, move your pen or brush across the surface of the paper. Who knows what you will discover about yourself in the process! 

You won't need a lot of tools to become a Zen artist, either. 

Western artists make use of the whole color spectrum, but Zen art makes do with a single pot of ink. In a Zen painting, the swirling ocean and the sun-drenched sky could all be painted in black ink. But this doesn't make the image one-dimensional. Instead, the viewer _imagines_ all the other colors. And is there anything more colorful than our own imagination? 

You don't need to make your own Zen art to appreciate it, though. Just looking is enough. 

In days gone by, every Japanese home had a small alcove. Known as a _tokonoma_, it contained a single piece of Zen art or calligraphy on the wall. The people who lived there would go to the tokonoma every day, to reflect on the wisdom of their chosen piece. Make your own home a more reflective place by hanging calligraphy depicting a great saying, and remember to ponder its meaning every day. With this simple act, you'll have brought powerful art into your life.

> _"In the landscape paintings of Sesshu, the waves of blurred ink contain the essence of his spirit."_

### 7. Integrating nature into your daily routine will bring you joy. 

When did you last wander through a forest, or bend down to smell a wildflower? If forests and flowers aren't part of your daily routine, then you're not alone. But it doesn't have to be this way. Living a Zen lifestyle means staying in touch with the natural world.

**The key message here is: Integrating nature into your daily routine will bring you joy.**

Working a nine-to-five job can make us feel stuck in a rut. Every day it's the same commute, the same office, and often the same work. But taking a short walk each morning shows you that, while you might be repeating yourself, every day can be refreshingly different from the one before.

When the author takes his daily walk through his temple gardens, he notices that nature is in constant flux. The leaves are a different color from one week to the next; one day brings rain and the next brings sunshine. Nothing stays the same — nature is never stuck in a rut. 

The cycles of nature hold lessons on how we should live our own lives, too. 

Just consider our human tendency to dwell on the past. Whether it's our past mistakes or our past grievances, many people can't stop looking over their shoulders. But nature shows us that there's no point worrying. Why? Because each day brings change and renewal. Tomorrow there will be new colors in the garden, and you will be different, too. Nature does not dwell on what has been and gone.

We can also commune with the natural world on a much smaller scale.

As a celebrated garden designer, the author helps schoolchildren create their own miniature gardens in spaces that are not much bigger than a shoebox. He tells the children to imagine being with nature. Then he asks them to reproduce their mental images by arranging soil, twigs, and pools of water. The result? The children's spaces become little patches of serenity — tiny havens of peace. 

You can create a miniature garden in your own home. A balcony is more than enough space. Even a window ledge will do. To gain inspiration, conjure up a mental picture of magnificent mountains and churning rivers. Before you know it, a corner of your home will be transformed into the landscape of your imagination.

### 8. Your time and your opportunities are limited. Make the most of them. 

Will you be ready when opportunity comes knocking? 

A Zen parable tells the story of two plum trees that needed the warm summer wind for their flowers to bloom. One tree spent the whole winter growing its flowers. When the summer wind blew, the tree blossomed. But the other plum tree only started preparing its flowers when the summer breeze arrived. By the time it was ready, the breeze was gone. 

**The key message here is: Your time and your opportunities are limited. Make the most of them.**

Like the unfortunate plum tree, we often delay our personal growth. We assume we'll always have more time for self-improvement in the future. But Zen Buddhists understand that life is a finite resource. 

The Japanese word _shoji_ describes the Buddhist concept of death and rebirth. Shoji encourages us to consider life and death as two sides of the same coin. Just as you consider how to live, you should also consider your death. 

Although it might sound morbid, thinking about our own mortality can help us achieve more. If you're not convinced, then try to imagine knowing that you only have six months to live. How would you spend that time? Would you lie around watching television, or would you start ticking things off your bucket list, making the most of your time left on Earth? Hopefully you'd go for the second option. 

Once we think of death as a concrete certainty, we realize that we need to be the first plum tree — the tree that wasted no time in helping its flowers to bloom. 

Finally, perhaps the most profound daily practice of all is to remember that while your life is yours alone, it is not something you can truly possess. This belief is captured in the Buddhist word _jomyo_ — meaning a predetermined lifespan. 

We each have our own jomyo — the length of our life is determined when we are born. And yet none of us knows how long it will be. Jomyo teaches us that life is a gift that we have been given, not something we own. Although it is not fair that some people have a bigger gift than others, Zen Buddhists know that's nothing to worry about. After all, the value of our lives does not hinge on how long they last. Instead, what matters is how we _use_ the time that has been granted to us.

### 9. Final summary 

The key message in these blinks:

**Life doesn't have to be a whirlwind of stress and activity. Every time you plead "I'm too busy," you're making a choice to live like that. You can make better choices by rejecting greed and embracing a simpler existence. Remember, life is short. You may not have time to waste. Simplify your life today so that you can wake up happier tomorrow.**

Actionable advice:

**Focus on your food.**

When it comes to food, we often rush. We eat breakfast on the run and eat dinner in front of the television. But Zen Buddhism teaches us to "eat with our whole hearts." This means that we should focus entirely on our meals when we eat. So the next time you're having dinner, leave the TV off, and savor your food in quiet contemplation instead. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** **_Zen and the Art of Motorcycle Maintenance,_** **by Robert M. Pirsig.**

Now that you know how Zen Buddhism can simplify your daily routine, why not learn how this philosophy can transform every other aspect of your life too, by checking out the blinks to _Zen and the Art of Motorcycle Maintenance_? These blinks use the allegory of a motorcycle road trip to guide you through the big questions on the philosophical and metaphysical order of the world. 

Packed with insight and original thought, this classic text is an accessible and exhilarating introduction to Zen Buddhism. So for a powerful meditation on what it means to live a good life, head over to the blinks to _Zen and the Art of Motorcycle Maintenance._
---

### Shunmyo Masuno

Shunmyo Masuno is a Japanese Zen monk and a celebrated garden designer. Not only is Masuno the head priest of the Zen Buddhist temple Kenko-ji, he is also a professor of Environmental Design at Tokyo's Tama Art University.

