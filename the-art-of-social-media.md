---
id: 55093dcd326432000a2e0000
slug: the-art-of-social-media-en
published_date: 2015-03-20T00:00:00.000+00:00
author: Guy Kawasaki and Peg Fitzpatrick
title: The Art of Social Media
subtitle: Power Tips for Power Users
main_color: 69BED3
text_color: 3F7380
---

# The Art of Social Media

_Power Tips for Power Users_

**Guy Kawasaki and Peg Fitzpatrick**

_The Art of Social Media_ reveals the most effective ways to promote yourself or your product professionally on social media platforms. The authors explain how to get the most of the many dominant social media platforms today, including Google+, Facebook, Twitter and others.

---
### 1. What’s in it for me? Learn inside tips on how to best harness the power of social media networks. 

Social media site Facebook has some 1.3 billion users from around the world. Twitter's numbers are not so far behind.

With so many ears and eyes in one place, a business would be foolish to not take advantage of such an audience. Yet there's a big difference between just posting and posting with purpose.

These blinks will show you exactly how you or your company can not only reach out to but keep the interest of people on social media networks. You'll learn too that with a deeper insight into how social media networks really work, the way you approach Facebook and Twitter may change forever!

In the following blinks, you'll discover:

  * why you'll come to regret your goofy Facebook username like a hangover;

  * how joining blogging networks can expand your blog's reach exponentially; and

  * how you can produce a wildly popular TV show out of your own living room.

### 2. Drunken profile pic and handle, “MartiniMom?” Think about how you present yourself online. 

The first things you see on any social media platform are a picture and a name.

With these two tiny pieces of information, people make a snap decision: to follow, or not?

If you want to give yourself a headstart on social media, you need to take seriously the importance of screen names and profile pictures.

First off, today's clever username may be tomorrow's regret. A name like MartiniMom or HatTrickHank might be funny now, but potentially embarrassing if you want to connect with serious businesses in the future!

The best screen names are simple, memorable and logical. The best option? _Your own name_. And in full! A screen name like "Guy Kawasaki" is a cut above something like "G.T. Kawasaki."

Once you've chosen a screen name, you'll need a _vanity URL_ for the social network you're using.

This way, people will see your name in the URL of your profile, such as plus.google.com/+JohnDoe/. Without a vanity URL, your profile will be identified by just a random string of numbers.

Depending on the social network, it might be too late to get a unique URL that includes your name, but you should try and get something as close as possible. If you want to be remembered, you'll need something better than a string of numbers!

Next, consider your profile picture. For one, a picture proves that the page really belongs to you. There might be hundreds of people with your name, so you'll need a profile picture to make yourself recognizable.

Profile pictures can also communicate more than just your identity. They should ideally give the impression of someone likeable, trustworthy and competent. So choose one where you're smiling and your face is clearly visible.

> _"An effective profile is vital, because people use it to make a snap judgment about your account."_

### 3. Snappy titles, gripping pictures, relevant content: Share interesting bits and people will follow. 

Ever googled "how to get more followers?" There are millions of results! Before attempting to wade through tons of articles, consider some simple advice.

To get people interested in you, you need to give them content that _they'll want to share_.

Depending on how you want to make money, you'll need to attract different kinds of people. So before you share anything, find out what your audience wants to read! Explore which of your posts are shared (and therefore appreciated) the most, and provide content that matches it.

However, sometimes you might feel you've got nothing worth sharing. Instead, you can turn to online services that provide content to stimulate you and your followers. Such services include _Alltop.com_, which presents content in topics ranging from A to Z; _TheAtlantic.com_, for attention-grabbing photography on various themes; and _StumbleUpon_, which brings you website and other curios from all over the Internet.

There certainly is a lot of interesting content out there, but never forget the value of posts written by you! This will make your account unique, especially if you're creative with your topics.

For example, smartphone companies looking for more followers shouldn't just go on and on about phones. Writing about a topic such as, "The 100 best Android apps of 2014," would generate more interest and draw a greater audience.

You'll want to sustain that interest, so your posts should be kept short and sweet, and never longer than 1,000 words.

"Eye candy" also helps to hold people's attention, so try and include a graphic, photo or video when and where you can. You'll find this greatly increases views.

And once you've written a story, always pick a catchy title for it. Right now, openers like "How to…," "Top 10 of…" or "The Ultimate…" are hard for social media users to resist!

> Total views increase by 94 percent, if a published article contains a relevant photograph or infographic.

### 4. Make sure your blog and your social media accounts talk to each other; you’ll get more views. 

The Internet has provided us with not one but two great ways to reach out to potential fans: the blog and social media.

By using one of them, you're off to a good start. But by combining the two, you'll gain far more.

So how can you connect your social profile with your blog? It's simple. Promote your blog on your social media profile and vice versa. Out of all the content you share on social media, what you want people to notice most are your blog posts.

You can make it easier for people to follow and share your posts by adding sharing buttons. ShareThis.com lets you set up multiple buttons for various social media platforms. ClickToTweet.com lets you embed a link in blog posts, allowing readers to immediately tweet your content.

You should also promote your social media profile on your blog. In fact, you can embed your social media posts into the blog itself, and enable social media logins. By doing so, you allow people to sign up for your service by using their credentials from social media platforms. 

Embedding social media links in websites has become very popular, with even traditional companies such as fine jeweler _Tiffany & Co.,_ using embedded links on its site.

You can also engage your followers by creating a mailing list, so people can subscribe to receive regular email posts and updates. This lets you reach people who may not be on social media.

Consider too joining a blogging network. A network is home to groups of bloggers who share each other's blogs, and in doing so, amplify each member's reach online.

For example, _Triberr.com_ shares posts with people in your "tribe." Tribe members in turn share your posts with their social networks. Through tribe members, it's possible to reach millions of people!

Blogging networks can hugely boost your blog's readership, so make use of them.

### 5. Make use of services such as Google+ Hangout On Air and Twitter Chat to promote yourself. 

Social media is constantly developing. Today there are several new platforms that allow you to promote yourself in unique ways.

Let's explore two of the most innovative developments in this field.

Ever wanted to broadcast your own television channel? Now you can with Google+ Hangout on Air (HOA). An HOA is broadcast through your Google+ and YouTube channel, and afterwards is automatically archived to your YouTube channel.

The right equipment for a professional looking video is vital if you want your work to be watched by thousands of people! A proper webcam and microphone, as well as a good lighting setup, will make a world of difference.

You should also always plan out what you're going to talk about during an HOA. And to increase views, advertise your HOA beforehand. You could do this by creating an event page with information about your program and presenters; announcing your upcoming video via your social media profiles and blog; or creating a trailer and sharing it!

Another exciting development is Twitter Chat. This is a live event in which a host tweets questions with a hashtag, and a guest responds with the same hashtag. Audience members can see the discussion by searching for the hashtag and can then chime in with comments.

However, if you want to participate in a Twitter chat, you should be able to type quickly. If you can't, you might want to recruit a typist to enter your comments.

When you respond to a comment, always mention the person to whom you're responding with an @ before his or her Twitter name. This way, the audience will know who you're addressing.

> _"HOAs are a magical way to rock social media. How else could Desmond Tutu and the Dalai Lama reach thousands of people in one event?"_

### 6. Final summary 

The key message in this book:

**If you want to promote your business, social media is your best bet for success. But if you want to take full advantage of its reach, there are a few crucial tips you should remember to make your presence professional, attractive and importantly, widespread. If you stick to it, you can gain a wider reach than you've ever dreamed of.**

Actionable advice:

**Go pro.**

If you want to promote yourself via social media, get serious and think of the time you spend online as more than just a hobby. If you're producing video, make it look professional and invest in the equipment to do so. If you're writing, be sure to thoughtfully consider your arguments and be active in following up questions and comments by readers. Your followers will know quality when they see it, and help you in turn to expand your network.

**Suggested further reading: _Social Media is Bullshit_ by B.J. Mendelsohn**

_Social Media Is Bullshit_ puts a damper on the hype around social media by unveiling the economy behind it and addressing the commonly held misconceptions about the value of social networks for business. While social media can be a valuable asset for certain companies, it is _not_ the cure-all that marketers and social media gurus would have you believe!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Guy Kawasaki and Peg Fitzpatrick

Guy Kawasaki is the chief evangelist at online design service Canva, and an executive fellow at the Haas School of Business at the University of California at Berkeley. Previously he was the chief evangelist at Apple and special adviser to the CEO of the Motorola business unit at Google. He has 1.44 million followers on Twitter and 6.8 million on Google+.

Peg Fitzpatrick is a social-media strategist and director of digital media for Kreussler Inc. She's spearheaded successful social media campaigns for Motorola, Google, Audi, Canva and Virgin.

