---
id: 53722a156264390007020000
slug: the-art-of-thinking-clearly-en
published_date: 2014-05-13T10:48:48.000+00:00
author: Rolf Dobelli
title: The Art Of Thinking Clearly
subtitle: None
main_color: CCD6AC
text_color: 5F6647
---

# The Art Of Thinking Clearly

_None_

**Rolf Dobelli**

_The Art Of Thinking Clearly_ aims to illuminate our day-to-day thinking "hiccups" so that we can better avoid them and start making improved choices. Using both psychological studies and everyday examples, the author provides us with an entertaining collection of all of our most common fallacies.

---
### 1. What’s in it for me? Find out how irrational you are every day. 

You're probably a rational person, right? You're probably pretty good at assessing your own abilities, too. Unfortunately, this is pretty unlikely! But don't worry, you're in good company: we are _all_ far less rational and far more capricious in our decision-making than we believe ourselves to be.

Like it or not, our brains are a mishmash of shortcuts and rules-of-thumb that helped our ancient ancestors avoid becoming lion lunch and stay alive long enough to pass on these traits to posterity.

These days, however, these shortcuts lead to many fallacies and biases that hurt us more than they help us.

These blinks will explain some of the main traps you probably fall into every single day, and along the way will provide you with tips on how to steer your way around them and start thinking clearly.

You'll learn why you should never bring your most beautiful friend along to a club if you're trying to get lucky.

You'll also discover why 84 percent of Frenchmen erroneously believe they are above-average lovers, as well as how a little sunshine (or lack thereof) can lead to booms and busts in the stock market.

And finally, you'll find out that you might not want to trust yourself to spot a gorilla, even if it's standing right in front of you!

### 2. We systematically overestimate our abilities in many areas of life. 

Do you feel that you have a pretty realistic grasp of your abilities? That, while others might delude themselves into overestimating their abilities, you don't? If so, you aren't alone: we _all_ tend to view ourselves through rose-tinted glasses.

Research has shown that we are overconfident in many areas of life.

For example, studies have shown that 84 percent of Frenchmen consider themselves to be above-average lovers. In reality, it's only possible for 50 percent to be considered "above average," since, statistically speaking, 50 percent should rank higher and the other half should rank lower.

Similarly, research has shown that 93 percent of US students ranked themselves as "above-average" drivers, and 68 percent of University of Nebraska faculty ranked their own teaching abilities in the top quartile.

These numbers show that the majority of us rate our abilities higher than they probably are.

Not only that, but we also mistakenly attribute successes to our own abilities and failures to external factors.

Researchers even tested this by having a group of subjects take a personality test, and then assigning arbitrary scores to the tests. When the subjects were later interviewed, they found that those with "good" scores believed that the test results had fairly reflected their true abilities, thus successfully assessing their great personalities.

Those who received "bad" scores, however, found the ratings to be useless, and that the test itself ‒ and not their personality ‒ was garbage.

Have you ever had a similar experience? If you got an A on a high school exam, for example, you probably felt that _you_ were responsible for your success. If you flunked, you probably thought that it wasn't your fault, and that the test was unfair, or some other circumstance caused your failure.

Knowing this, you should therefore be aware of our tendency to overestimate our knowledge and attribute all our success to our own skills. A good way to overcome this might be to invite an honest friend out to coffee and ask for their candid opinion on your strengths and weaknesses.

> _"In daily life, because triumph is made more visible than failure, you systematically overestimate your chances of succeeding."_

### 3. We can control and predict much less than we think in life. 

Have you ever thought about why people at casinos throw their dice harder if they want a high number, and gently if they need a low one to win big?

These gamblers are suffering from the _illusion_ _of_ _control_ — i.e., the belief that we can influence things that we in fact cannot control.

The _illusion_ _of_ _control_ offers us hope: if we believe that we can exert some kind of control over our situation, then we can better endure life's many sufferings.

This was demonstrated in one study in which subjects were placed in booths to test their acoustic sensitivity to pain. Amazingly, they could withstand significantly more noise if the booth was equipped with a red "panic" button.

The button, however, had literally no function. Participants simply had the illusion that they were in control of the situation, and were thus able to endure more pain.

So it would make sense that "placebo buttons" are installed in all sorts of areas in order to create an illusory but ultimately useful sense of control.

For example: those buttons you press at the crosswalk at a busy intersection? Most do nothing more than simply give us the feeling that we are influencing our situation — making it easier for us to wait for the light to change.

The same is true for some "door-open" and "door-close" buttons in elevators, which often aren't even connected to the electrical panel!

Furthermore, in addition to having much less influence than we think, we are also quite overconfident about our ability to make predictions.

Consider, for example, this ten-year study that evaluated 28,361 predictions from 284 self-described professionals across a number of fields, such as economics. These "expert" predictions were only marginally better than the predictions made by a random forecast generator.

It's therefore in your best interest to be critical of predictions and to focus your energy on a few things of importance that you truly can influence.

> _"We prefer wrong information to no information."_

### 4. We tend to follow what the group does – and we’ll conform to prevent ourselves from being excluded. 

When a soloist at a concert puts on a particularly riveting performance, it's not uncommon for someone in the audience to spontaneously burst into applause. Suddenly everyone else joins the chorus — including you! But why?

This is due to a phenomenon called _social_ _proof_, which makes us feel like our behavior is correct when it matches other people's.

In fact, social proof is rooted in the genes of our ancestors, who copied others' behavior to ensure their own survival.

Imagine, for example, that you're traveling with your hunter-gatherer friends, and they all suddenly started sprinting. If you decided to act individually by staying put and pondering whether the creature staring at you is _really_ a lion, then you'll end up being lion lunch, and thus exit the gene pool.

If, however, you follow your group without hesitation, then you'll have a better chance of surviving another day. And since following others was a good survival strategy for our ancestors, it is still deeply rooted in us today.

One consequence of this "herd instinct" is that the more people follow an idea, the better we believe that idea to be. We see examples of this everywhere: from fashion and diets to stock market panic and collective suicides.

Moreover, we don't just _do_ the same things as the group; we also change our opinions in order to stay part of the group.

This kind of social proof is called _groupthink_. You can see it at work, for instance, whenever we bite our tongue in a meeting because we don't want to be the naysayer who points out flaws in the commonly accepted reasoning and disrupts group unity.

A perfect example of this was the demise of the world-class carrier Swissair: they had built a strong consensus about their success that suppressed even rational reservations, and they therefore missed the warning signs of the imminent financial danger that ultimately led to their demise.

### 5. We interpret information so that it fits with our self-image and our pre-existing beliefs. 

Do you consider yourself a good judge of character, whose first impressions of people often prove true later? Many people think this way about themselves, but in fact, it's likely that they are just the victims of _confirmation_ _bias._

In actuality, we _all_ suffer from confirmation bias, i.e., the tendency to interpret new information in such a fashion that our previous conclusions remain intact. Indeed, it is so common that it is even said to be "the mother of all misconceptions."

One example of confirmation bias in action is when we peruse our favorite news sites and blogs on the internet for analysis of recent events, forgetting, however, that our favorite sites mirror our own values.

In doing this, we inevitably find communities of like-minded people, thus further entrenching our convictions.

In addition, confirmation bias causes us to accept external information about ourselves that matches our existing self-image, and then unconsciously filter out everything else.

This bias is the reason people believe that pseudosciences such as astrology and tarot-card reading work so well: we can't help but see the many applications to our own lives in their universal descriptions.

To explore this phenomenon, the psychologist Bertram Forer crafted fake personality readings from a mishmash of different astrology columns from various magazines, and then gave them to his students under the pretence that they were individual, personalized assessments. Afterwards the students were asked to rate these "personalized" descriptions, and on average judged them to be 86 percent accurate!

This study indicated that we interpret information so it corresponds to our pre-existing self-image, and has since been aptly named the _Forer_ _effect_.

Knowing that we are unconsciously influenced by our confirmation bias, we should instead set out to find contrary opinions and evidence in order to form more balanced convictions.

### 6. We determine the value of things based on their availability and by comparing them to others. 

Have you ever gone to a club with a much more attractive friend, looking to meet someone but instead striking out all evening? Why is this? Quite simply: your friend makes people find you less attractive than you actually are.

As it turns out, we are not very good at making _absolute_ judgments, relying instead on comparisons.

This is exemplified by the classic experiment involving only two buckets of water: one filled with lukewarm and the other with ice water. If you first place one hand in the cold water, and then place both hands in the lukewarm water, then the lukewarm water will feel extremely hot to the hand that was in the iced water.

This is the _contrast-effect_ at work, and it's the reason why you appear far less attractive than you truly are when standing next to your ultra-attractive friend.

The contrast-effect is also the reason discounts in business are successful. For example, we perceive a product that has been reduced from $100 to $70 to be better value than one that has always cost $70, even though the starting price plays no role in a product's actual value.

Yet another instance in which we misjudge something's value occurs when we perceive scarcity.

This phenomenon has been verified in one test involving cookies. In the experiment, subjects were divided into two groups: in one, each person received an entire box of cookies, and in the other, a mere two cookies.

They then rated the cookies. The subjects that had received only two rated them much more highly than the other group.

Businesses also take advantage of this lapse in our judgment by creating the feeling of scarcity, using phrases such as "today only" or "only while supplies last" in order to drive sales.

Luckily, we can circumvent these comparison and scarcity biases by assessing something's value based solely on its costs and benefits. By doing so, you'll make much better choices.

### 7. We tend to be engrossed by the interesting: whether compelling stories or exotic explanations. 

Do you find it difficult to remember the five items on the shopping list you composed only ten minutes ago, yet have no trouble at all remembering the intricate details of the plot of the movie you saw last week?

This is because we need information to form meaningful stories before it makes sense to us; conversely, we are repelled by abstract details.

We find this phenomenon strongly reflected in the media, where relevant facts take a back seat to entertaining narratives.

For example, if a car drives over a bridge that suddenly collapses, we'll probably hear much more about the unlucky driver than about the mundane details of the bridge's faulty construction. Juicy facts about the person attract more readers than abstract information about how the accident could have been prevented, and media outlets reflect this in their reporting.

In addition, we love exotic — and therefore exciting — stories. In fact, we are far more likely to believe exotic explanations to mundane ones, even though mundane explanations are more probable.

As an example, reflect on this headline for a moment: "A young man is stabbed and fatally injured."

In your estimation, is the attacker more likely to be a middle-class American or a Russian immigrant who illegally imports combat knives?

Most people would place their bet on the latter, but this assessment contradicts the fact that there are _a_ _million_ _times_ more middle-class Americans than Russian knife smugglers, and thus the overall probability of the perpetrator being American is far higher. Unfortunately, we are simply so attracted to enticing descriptions that we often overlook more probable explanations for the story.

This thinking error can be fatal in the medical field. For this reason, doctors are taught not to be seduced into thinking that symptoms might be caused by some exotic disease, and instead always investigate the most likely ailments first.

They follow the motto: "When you hear hoofbeats, don't expect a zebra." Even if a zebra would be far more exciting than a mere horse.

> _"Stories are dubious entities. They simplify and distort reality, and filter things that don't fit."_

### 8. Our attention is very selective and narrow. 

If something strange was happening right in front of you, like a gorilla running around, you'd notice, right? In fact, you probably wouldn't if you were focusing on something else.

It turns out that our focus is very narrow, and we miss everything that occurs outside it.

Consider, for example, a Harvard study that demonstrated this _illusion_ _of_ _attention_ : subjects watched a video of students passing basketballs back and forth, and were asked to count how many times the players in white T-shirts passed the ball. At the end, the subjects were asked if anything unusual caught their attention.

Half the viewers shook their heads, totally unaware that in the middle of the video someone dressed as a gorilla had walked into the room, pounded his chest, and then disappeared.

This is the reason we should never use cellphones while driving. Studies show that drivers' attention is too overstretched to react to danger — just as slow, in fact, as when under the influence of alcohol or drugs.

In addition, _what_ we focus on is influenced by outside factors: when presented with a long stream of information, we pay much more attention to the information that comes first or last at the expense of everything in the middle.

Consider this question: Who would you rather be stuck in an elevator with? Allan, who is smart, ambitious, good looking, critical and jealous? Or Ben, who is jealous, critical, good looking, ambitious and smart?

Most people choose Allan. Even though the descriptions are identical, we are fooled by the _primacy_ _effect_, which causes us to focus on first impressions that then shape our overall assessments.

However, if our impressions were formed in the past, then the _recency_ _effect_ controls our attention: the more recently we received the information, the better we are at remembering it.

For example, if you listened to a speech a few weeks ago, then you'll remember the final point better than either your first impression or the content sandwiched between.

### 9. Making decisions can be tiresome, especially when you are faced with many possible choices. 

In today's world we face limitless choices about products and lifestyle. Be it finding the right wine or the best university, you are bombarded with options. For most people it's difficult or even impossible to manage this cornucopia.

Indeed, a large selection leads to an inability to come to a decision, and we often just give up instead.

This _paradox_ _of_ _choice_ was tested in one supermarket where researchers set up a stand with different jelly samples for people to try and then buy at a discount.

The experiment was conducted over two days, with 24 varieties of jelly on the first day, and only six on the second.

The results showed that they sold _ten_ _times_ more jelly on day two, indicating that too much choice inhibited customers' ability to make a decision and that they thus opted to not buy anything.

A similar study on decisions made when picking out potential partners on online dating sites even showed that the stress of being presented with an overwhelming variety of potential partners causes the male brain to reduce the decision to a single criterion: physical attractiveness.

Furthermore, research has shown that decision-making can also be exhausting, resulting in _decision_ _fatigue_.

This was tested by one psychologist who presented two groups with pairs of items; one group had to deliberate over which they preferred, while the other group simply wrote down what they thought about the items.

Immediately thereafter, they put their hand in ice-cold water for as long as possible. The first group could keep their hand submerged in the water for a much shorter amount of time than the second, thus indicating that their willpower was exhausted by this intensive decision-making.

So in order to circumvent these traps in decision-making, you should realize that the "perfect decision" is impossible, and instead learn to love a "good" choice, rather than striving for the "best" choice.

### 10. We like others if they are attractive, flatter us or remind us of ourselves. 

How many beautiful, smiling faces do you see plastered on billboards on a daily basis? It's no mistake: businesses know that we are all susceptible to good looks and charm.

In fact, a single quality — whether beauty, social status, age, etc. — produces in us a positive or negative impression that outshines everything else. This is called the _halo_ _effect_.

Beauty's disproportionate effect on how we judge others has been studied more closely than any of these competing qualities. In fact, there is a scientific consensus that we automatically regard good-looking people as more pleasant, honest and intelligent.

This effect has been identified in both schools and workplaces: attractive people enjoy easier professional lives, and teachers even unconsciously award good-looking students better grades.

While the halo effect is great for advertisers, it can also lead to stereotyping when nationality, gender, or race becomes the single feature that we focus on. And it's not only racists and sexists who fall victim to this; we _all_ use easily identifiable details to formulate our opinions.

For example, if the new CEO of your company is an attractive female, you might immediately use this information to assess how she got her job _without_ looking at the other possible factors. You might think that it was her looks that landed her the post and not her outstanding education and experience in leadership.

In addition, we generally fall prey to the _liking_ _bias_ — i.e., liking people when they are similar to us and if _they_ like _us_.

This is one reason why salespeople flatter potential customers. Receiving compliments, such as "you look amazing in that dress!" makes us far more inclined to want to buy from that person, because they make us feel liked and happy.

Another technique salespeople use is "mirroring," or copying the gestures, facial expressions and language of the client. This makes the seller appear more similar to the client, thus more likeable and more likely to close the deal.

### 11. Feelings guide our decisions more than we think. 

Would you consider yourself to be rational decision maker? Try this exercise, and see: decide right now whether or not you are in favor of genetically modified food.

How did you come to your decision? A truly rational approach would be to separately consider both the advantages and disadvantages of genetically modified foods: first, assess each "pro" in terms of importance, and then multiply each by the probability that those advantages will actually occur. Then, do the same with each "con."

Voilà! The sum of the pros minus the sum of the cons is your answer: if the value is greater than zero, then there are more pros, and thus you have a favorable view of genetically modified food.

But, if you are like most people, you have neither the time nor the energy to make these kinds of meticulous assessments. In other words, we are not ultra-rational in our decision-making!

On the contrary, our decisions are rarely rational and thought out; rather, we rely on mental shortcuts guided by our emotions to make decisions.

For example, when we hear the words "genetically modified," positive or negative emotional reactions are triggered, which affect how we assess the concept's risks and benefits. So if your initial judgment is that the concept is appalling, then you will probably judge the risks (e.g., environmental hazards) as being greater and the benefits (e.g., pest resistance) as being smaller than they might actually be.

In this way, our decisions are limited to what springs into our minds first. And as puppets to our feelings we are prevented from making rational decisions, which can be problematic in areas where rationality is highly valued.

Even the markets aren't untouched by emotional influence. In fact, one study found that daily market performance in 26 major stock exchanges was influenced by the amount of morning sun: when the sun shone first thing, the stock market rose during the day, thus indicating that the positive emotions triggered by sunshine influenced the flow of billions of dollars.

### 12. Final Summary 

The key message in this book:

**We think we are better than we actually are and we automatically seek out information that confirms us in our pre-existing beliefs. We also have a preference for the exotic, beautiful people and a small rather than a large selection of things. We notice only a limited amount of the things in front of us, and don't know how to make absolute judgments. And our decisions are guided by our emotions and by the way that people around us are behaving.**

Actionable advice:

**Get an honest opinion about yourself**.

Whether you like it or not, you overestimate your abilities just like everyone else. A good way to combat this tendency is to ask your friend (or even better: your enemy) over for coffee and ask for their honest opinion about your strengths and weaknesses.

**Don't be fooled by "limited time only"**.

These kinds of sales pitches play on your tendency to value things more when their availability is decreasing. Instead, remind yourself that, in this day and age, virtually anything can be found online. This will help you focus on the actual benefits the product provides, rather than the possibility that it will disappear forever.
---

### Rolf Dobelli

Rolf Dobelli is a writer and entrepreneur with a PhD in philosophy, as well as the founder of Zurich.Minds, a community of high-profile thinkers. He is also a regular contributor at esteemed European newspapers like _Die Zeit_ and _FAZ_ and has written six novels.

