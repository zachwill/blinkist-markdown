---
id: 58e2004ffe685600046c9341
slug: the-art-of-waiting-en
published_date: 2017-04-06T00:00:00.000+00:00
author: Belle Boggs
title: The Art of Waiting
subtitle: On Fertility, Medicine, and Motherhood
main_color: BBD975
text_color: 586637
---

# The Art of Waiting

_On Fertility, Medicine, and Motherhood_

**Belle Boggs**

_The Art of Waiting_ (2016) details the social narratives surrounding birth, pregnancy and parenting. These blinks offer poignant personal anecdotes alongside historical examples to shift the spotlight onto the often unheard stories of adoption, _in vitro_ fertilization and forced sterilization.

---
### 1. What’s in it for me? Get a new perspective on what it means to have children. 

For many people, having children is their chief purpose and biggest joy in life — some would even say that it's the meaning of life itself. Regardless of whether you agree or not, parenthood plays a pivotal role in the lives of those who have children, but also in the lives of those who want to have children but are unable to.

Here we'll look at lesser-known stories of the struggles some people go through to have children and the methods, such as adoption and _in vitro_ fertilization, they employ to become parents even when their anatomy seems to be working against them.

In these blinks, you'll find out

  * how one-third of American women will have had an abortion before they turn 45;

  * why there are so many misunderstandings regarding infertility; and

  * how adoption might not be a silver bullet to childlessness.

### 2. The narratives around birth and pregnancy are contradictory. 

Do you remember those awkward sex-ed classes we had to sit through in high school?

Whether we knew it or not, courses like these were meant to teach us about the human anatomy and the privileged place that the concept of birth occupies in our society. In fact, from an early age, most kids are taught to value reproduction and child-rearing; indeed, cultures uphold the belief that children are the center of all life, as evidenced by sayings like "the children are the future."

Beyond that, the desire for biological children is seamlessly coded into most world religions and art. Just consider the Hebrew Bible, which commands Adam and Eve to be fruitful and multiply, or the Hindu belief that children are gifts and a reflection of karma.

Even the earliest known works of figurative art, produced around 35,000 years ago, feature exaggerated sexual characteristics like wide hips, voluptuous breasts and prominent vulvas. These artworks are thought to depict fertility goddesses.

So, due to millennia of cultural conditioning, most people now assume that their futures should include children. This belief is so ingrained that, during her time as a K-12 teacher, the author found that most of her young students saw themselves as future parents.

However, we're also taught about contraception and the importance of protection against both sexually transmitted diseases and unwanted births. As a result, 62 percent of American women of childbearing age use some form of birth control and, at current rates, 30 percent of them will have had abortions by their forty-fifth birthdays.

Yet despite this contradiction between the simultaneous promotion of fertility and contraception, humanity has managed to reduce births with great success. We've been so successful that, compared to other members of the animal kingdom, humans have relatively few children, at a worldwide average of just 2.5 kids per woman.

These numbers are lower in developed countries and higher in poorer societies — but not in any extraordinary way. In developing nations, women have around four to six children on average, about half of whom die before sexual maturity.

### 3. There’s considerable scientific debate about the evolutionary drive behind childbearing. 

Why is having kids so appealing to some women? Is this desire socially conditioned or biologically driven?

Well, the cultural importance of this longing is deeply rooted, although different cultures call it by different names. For instance, the English refer to women with this desire as "broody," a word borrowed from the concept of brooding hens who refuse to rest or roost, instead sitting on their clutch of eggs. Meanwhile, on the other side of the Atlantic, an American woman might focus on the time limit governing this urge by saying that her "biological clock is ticking."

But despite the deep cultural position of longing for children, this matter is hotly debated by scientists. For instance, there's no consensus as to whether something like a "childbearing instinct" exists in human nature.

Just take the first evolutionary psychologist, Edward Westermarck, who, in his 1891 book _The History of Human Nature_, argued that all humans have such an instinct. In opposition to this claim, the sexologist Havelock Ellis argued that this instinct was simply the sex drive and that no instinct driving procreation existed, since having two reproductive drivers would be redundant — a rarity in evolution.

In response to this counterargument, Westermarck removed all claims regarding this "childbearing instinct" from future editions of his book.

More recently, the contemporary Finnish sociologist Anna Rotkirch led a study on what is known by Scandinavians as "baby fever." She found that while women who had always wanted children indeed confessed to feelings of baby fever, so did those who hadn't wanted children. In fact, lots of people would complain about feeling this urge, saying that it arrived at inconvenient times for them.

Interestingly enough, it wasn't just those who grew up planning on having children who felt this impulse; all types of people felt the pressure of baby fever, which often manifests as an all-encompassing emotional and involuntary desire for kids, despite conflicting plans.

Paired with the information that Finland is a low-fertility country that puts particular emphasis on individualism and education, Rotkirch's study could suggest a deeper, biological imperative underlying the human desire for children.

### 4. Infertility often causes deep emotional pain. 

The debate around abortion rights garners lots of attention in the United States. But a topic that gets less publicity is the plight of people who want children and can't have them.

This glossing over is a result of a general misconception about infertility and the pain it can produce. Infertility is often dismissed as a white, upper-middle-class problem, but it actually disproportionately affects minorities, the poor and those with less of an educational background. We also tend to think of infertility as a woman's problem, when it affects both sexes equally.

People often believe that trouble with fertility is a rare and unnatural issue, while the reality is that one out of every eight couples have difficulty conceiving. But despite the widespread prevalence of this issue, it still conjures unimaginable shame for those affected by it.

Just take the therapist Marni Rosner, who specializes in reproductive trauma and longing, and who studied women who wanted children but couldn't have them. Rosner describes these patients as experiencing _disenfranchised grief_ — suffering that can't be explicitly stated, openly grieved or supported by friends and family. Fertility fits perfectly into this category because people don't feel comfortable discussing it.

While this modern shame around infertility is terrible, what's much more horrific is the fact that, throughout history, this condition has been forced upon certain groups through sterilization. For instance, starting in the 1920s, 33 American states actively attempted to sterilize their poorest and most vulnerable citizens.

Among the people targeted were those suspected of being promiscuous or having mental deficits. These citizens were often pressured into "consenting" to sterilization through the threat of losing their welfare benefits.

The author spoke to some of the survivors of these eugenics programs in her home state of North Carolina, where a sterilization program ran from 1933 into the 1970s. One such survivor was a veteran by the name of Willis Lynch. Willis was raised in a big family, which made the opportunity to have his own kids extremely valuable to him. This opportunity was snatched away from him at the age of 14 when he was sterilized by the state.

### 5. Adoption is much more challenging than most people expect. 

So, infertility is a widespread issue, but why don't these people just adopt children who would otherwise have more difficult lives? Well, it's not that simple.

For starters, the adoption process is exceedingly complex, expensive and uncertain. Cost is one issue, as adoption can often run tens of thousands of dollars, but time is another. Parents wishing to adopt usually wait years for their applications to clear.

Beyond that, most people want to adopt newborns, not older children who tend to pose more challenges in terms of emotions and bonding. This is a problem, since there are far fewer newborns up for adoption than there are adoptive parents. Even when parents _do_ match with an unborn child, the birth mother frequently backs out of the arrangement following delivery.

In short, the process is a trying one and there are plenty of nightmare stories about adoption. Just take one couple that adopted a child from a mother who failed to disclose that the child's father was in prison. While incarcerated, he had no custody rights over the child. But once he got out, he claimed the child as his own, going against the wishes of the mother and causing great despair to the adoptive parents.

That being said, plenty of adoptions still occur. In the United States alone, around 120,000 adoptions take place every year, and some three-quarters of the parents who seek to adopt are infertile.

LGBT couples commonly adopt as well and, for them, the process can be even more trying. Adoptive rights are deeply intertwined with marriage rights and many adoption agencies, whether explicitly or behind closed doors, favor heterosexual, married couples during the adoption process.

This changed somewhat in 2015 after the US Supreme Court ruled in Obergefell v. Hodges to legalize same-sex marriage. Because of this ruling, adoption became more accessible to LGBT parents in every state but Mississippi, which forbade same-sex adoption until a federal court ruled the state-wide ban unconstitutional.

But even today, just seven states take explicit steps to protect the rights of LGBT families in the foster care and adoption systems.

> Many of the most common countries of origin for adopted children — Guatemala, China, Ethiopia, South Korea and Russia — explicitly forbid same-sex couples and single parents from adopting.

### 6. Humans, like animals, will go to extreme lengths to have offspring. 

Observing animals in the wild makes clear that they'll do whatever it takes to ensure they reproduce. Just take the male blue-backed manakin bird, who practices his mating dance for some eight or nine years before unveiling it to prospective mates. Or consider the male barn swallow, whose long tail feathers attract more females, but also make him more vulnerable to predators.

And we can't forget the Pacific salmon, who swims up freshwater streams to spawn, even though the freshwater causes their flesh to decay.

So, animals will go to tremendous, nearly suicidal, lengths to mate. But what about us?

Well, humans have other options — but those still require a huge commitment. For instance, developments in science mean that, in addition to adoption and surrogate pregnancy, we also have the choice to use _in vitro fertilization_ or _IVF_.

This procedure, which was developed in the 1970s, hormonally controls a woman's cycle to harvest and fertilize her eggs, which are then transferred as embryos into her uterus. Over 5 million babies have been born this way and, today, IVF is a multibillion-dollar industry.

Every year, in the United States alone, 60,000 babies are born using IVF. These children are often referred to as "test-tube babies," although most parents of IVF children resent this term as it diminishes a process that's actually quite grueling.

To prepare for IVF, mothers have to spend months doing research, making financial preparations and taking all manner of drugs. And even with all this work, there's no guarantee that the procedure will be successful.

Beyond that, IVF is incredibly expensive and rarely covered by insurance. The average cycle of IVF treatment costs around $10,000 and most people complete multiple cycles before succeeding or giving up.

When all is said and done, the whole process can end up costing between $50,000 and $100,000, depending on the type of insurance the patient has and the plan they select. This tremendous cost barrier prevents many people from attempting IVF, but also goes to show how much people are willing to sacrifice to have children.

### 7. Final summary 

The key message in this book:

**Children are often at the center of human societies, and we learn from a young age that we should desire kids of our own. However, birth and parenting are extraordinarily complex processes, and while many cultures tend to emphasize traditional birth narratives over others, they're all equally important.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Selfish Reasons to Have More Kids_** **by Bryan Caplan**

_Selfish_ _Reasons_ _to_ _Have_ _More_ _Kids_ examines the demands of modern parenting and why people today are choosing to have fewer and fewer kids. The author argues that this trend is due to modern parents placing too high expectations on themselves, even when a far more relaxed style of parenting would get the job done just as well and make the whole experience more enjoyable.
---

### Belle Boggs

Belle Boggs' stories and essays have appeared in _Harper's_, the _Paris Review,_ _Orion, Slate_ and many other publications. She is a professor in North Carolina State University's MFA program and the author of _Mattaponi Queen_.

