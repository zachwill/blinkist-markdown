---
id: 519b9614e4b034cd5017bab8
slug: the-art-of-war-en
published_date: 2013-06-12T00:00:00.000+00:00
author: Sun Tzu
title: The Art of War
subtitle: None
main_color: A62121
text_color: A62121
---

# The Art of War

_None_

**Sun Tzu**

Thousands of years old, _The Art of War_ is a Chinese military treatise that is considered the definitive work of military tactics and strategy. It has greatly influenced military tactics as well as business and legal strategy in both the East and West. Leaders such as general Douglas MacArthur and Mao Zedong have drawn inspiration from it.

---
### 1. Planning, calculating and comparing armies leads to victory. 

If a state goes to war, it is fighting for its very survival. Hence, all efforts must be made to understand the art of war, and, in the event of war, that knowledge must be used to make plans.

The general who draws meticulous plans before battle will defeat the general who makes none. Therefore, you must always plan and deliberate before battle. By comparing the opposing armies on seven considerations, you can forecast victory or defeat:

  1. Which of the two rulers of the states at war commands complete accord and obedience from his people so that they will follow him even to their deaths?

  2. Which of the two generals is more capable?

  3. Which side has advantages of heaven and earth, meaning of circumstances like the weather, distances to be covered and the nature of the terrain?

  4. Which side enforces the discipline of its men more strongly?

  5. Which side has the stronger army?

  6. Which side has the better trained officers and men?

  7. Which side has more consistency in giving rewards and punishments to enforce discipline?

Carefully compare the army of your enemy to your own army so that you will know where he is strong and where he is weak. Then plan according to the circumstances. If you know your enemy and know yourself, you will always be victorious.

**Planning, calculating and comparing armies leads to victory.**

### 2. Secure yourself against defeat, and wait for an opportunity for victory. 

Successful strategists only enter battles they know they will win, whereas unsuccessful ones enter frays and only then begin to think of how they can win.

A skillful fighter avoids battles he may lose, thus ensuring he is never defeated. But even the most brilliant general cannot say exactly when victory will come, for he must wait for the enemy to make a mistake and provide him the opportunity for victory.

A successful general knows that to seize victory, there are five essential rules:

  1. You must know when to fight and when not to fight.

  2. You must know how to deal with forces both inferior and superior to your own.

  3. Your army must have a strong, uniform fighting spirit and discipline throughout its ranks.

  4. You must fight so that you are prepared and the enemy is unprepared.

  5. You must have the military capacity and freedom to command your troops without interference from a sovereign.

Be cautious. Attack only when you have the advantage. Avoid your enemy where he is strong and attack him where he is weak.

Avoid the enemy's army when its spirit is keen, its columns and banners are in perfect order or when it has a more advantageous position such as higher ground.

Never enter battle simply out of anger; there must always be something to be won. Your anger will eventually fade, but a kingdom once destroyed can never be brought back to life.

Avoid the traps your enemy will try to draw you into. Do not lead your army into places where your supplies cannot reach you or where you do not know the terrain or your allies well.

**Secure yourself against defeat, and wait for an opportunity for victory.**

### 3. Warfare is only successful if sovereigns and generals do not cause their own defeat. 

In war, an army is commanded by a general, but a general is commanded by a sovereign. Hence, by his commands, a sovereign can impede his army. The most disastrous ways he can do this are by commanding them to advance or retreat when such action is impossible, by attempting to govern the army as laxly as he does his state or by placing officers in inappropriate roles.

These errors shake the confidence of soldiers and can cause defeat.

However, a general can also exhibit dangerous faults. He can be reckless and lead his army to destruction, or he can be a coward and be captured; he can be so choleric or proud that he is provoked by insults and slander from the enemy; or he can be too concerned with the comfort of his own men and let such considerations hinder military tactics.

The general is also responsible if any of these six calamities befall an army:

  1. If he hurls his army against a force ten times its size, causing his soldiers to flee.

  2. If his soldiers are too strong in relation to the officers, causing insubordination.

  3. If the soldiers are too weak, leading to them being worn down by officers and collapsing.

  4. If the higher officers are angry and undisciplined, leading them to attack on their own accord and cause the ruin of the army.

  5. If the general is weak and indecisive, resulting in a weak, disorganized army.

  6. If the general is unable to estimate an enemy's strength and hurls an inferior force against a superior one, leading to overwhelming defeat.

**Warfare is only successful if sovereigns and generals do not cause their own defeat.**

### 4. Conserve your resources through stratagems, foraging and espionage. 

Maintaining an army is expensive: a host of 100,000 men can cost 1,000 ounces of silver a day for provisions like food, chariots, spears, arrows, armor and oxen.

Prolonged warfare can exhaust the resources of any state, leaving it weak and vulnerable. Hence, aim for quick and decisive victories, not prolonged campaigns.

Avoid besieging walled cities, because this usually takes month of preparations, and many impatient generals will squander their men in pointless attacks.

The best way to lessen the cost of warfare is to capture the enemy's country, city or army whole and intact rather than to destroy it through costly battle. To achieve this, you need a much larger force than your enemy's.

A skillful general will subdue his enemies without any fighting, which constitutes the ultimate triumph. This is known as attacking by _stratagem_. Great fighters excel not only at winning but at winning with ease.

Another way to conserve the state's resources is to take them from your enemy by foraging locally and augmenting your own strength with the enemy's weapons, armor and men. This saves the cost of supplying your army from home and spares your peasants the burden of maintaining your army.

As single battles can end wars, you should engage spies: they provide decisive information about the enemy's disposition as well as carry false secrets back to him.

Maintain intimate relations with your spies and reward them well. The cost is very small relative to the prolonged war they can help avoid.

If you build a stratagem around a secret that a spy has told you, kill him as well as any other people he has told the secret to, so your stratagem will not lose its power.

**Conserve your resources through stratagems, foraging and espionage.**

### 5. Deceive your enemy and impose your will on him. 

The art of war is based on deception. You must mask strength with weakness, courage with timidity and order with disorder. Confuse your enemy and let him grow careless.

Have your troops feign disorder when in reality they are highly disciplined. When you draw close to your enemy, make it seem like you are far away. When you are able to attack, make it seem like you are unable.

Play with your enemy as a cat plays with a mouse. If he has a temper, irritate him. If he is at ease, harass him; if well supplied, starve him; if quietly encamped, force him to move. If you wish the enemy to advance, hold out bait to him; and if you wish him to retreat, inflict damage on him.

A clever combatant seizes the initiative and imposes his will on the enemy.

Attack the enemy in poorly defended points that he must rush to defend. Force him to reveal himself so you can seek out his vulnerabilities.

Keep your enemy guessing as to where you will attack, forcing him to splinter and spread out his forces: numerical weakness comes not only from absolute numbers but also from having to prepare for attacks on many fronts.

**Deceive your enemy and impose your will on him.**

### 6. Observe the terrain and your enemy, then adapt accordingly. 

A good general knows that there are always positions that cannot be held, roads that must not be followed and commands from the sovereign that must be disobeyed.

Just as water shapes its course according to the ground it flows over, so you too must adapt to the situation, to the terrain and to the enemy's disposition.

Observe the terrain to take advantage of its natural advantages and avoid its disadvantages. In order to fight, do not climb heights, go upstream or travel away from water and shelter.

Avoid places where there are precipitous cliffs, confined places or quagmires where a small force can destroy an entire army. Look for startled birds or beasts; they indicate you are approaching an ambush.

Observe the enemy, too. When his soldiers lean on their spears when standing, they are faint from starvation. When the soldiers he sends to gather water start by drinking it themselves, they are suffering from thirst.

And when they start to eat their own cattle, neglect to hang their cooking pots above the camp fires, and act as though they will not return to their tents, know that they are willing to fight to the death.

Adapt your tactics as needed to these circumstances and take advantage of opportunities as they appear.

**Observe the terrain and your enemy, then adapt accordingly.**

### 7. To wage war successfully, manage your troops sternly, keep them in uncertainty and make them fight to the death. 

Managing and controlling a large army is no different than managing a small one: you must simply divide your men into smaller numbers and then use signals such as gongs, drums, banners and signal fires to control your forces.

They will move as one, and the cowardly will not dare to retreat nor will the brave be left to charge alone. A skilled general leads his army as if he was leading a single man by the hand.

Treat your soldiers like beloved sons and they will stand by you to their deaths. If, however, you are unable to command them with authority, they will be as useless as spoilt children.

Iron discipline among your soldiers is a sure road to victory. But for discipline to be effective, your soldiers must grow attached to you. Thus, you must treat them humanely while also keeping them under control with discipline and punishments.

As a general, you must be secretive. Keep your soldiers ignorant and change your plans frequently to keep both your soldiers and the enemy guessing.

Change camps and take long circuitous routes instead of direct ones. Only reveal your hand once you're deep in hostile country.

When the situation looks bright, tell your soldiers about it; but when the situation is poor, keep this knowledge to yourself.

The further you penetrate into hostile country, the more your soldiers will feel solidarity.

Put them into desperate situations where there is no escape, and they will lose all sense of fear and fight with utmost strength, even to their deaths.

**To wage war successfully, manage your troops sternly, keep them in uncertainty and make them fight to the death.**

### 8. Final summary 

The key message in this book:

**Warfare is a matter of life and death for the state, and so meticulous planning and estimating must go into the conduct of war. A skilled general chooses to fight only when he knows victory is secure; thus, he is never defeated. He is observant, resourceful and adaptable. He imposes his will on the enemy, deceiving and irritating him to drive him to make a fatal error.**

The questions this book answered:

**How can you secure against defeat and ensure victory?**

  * Planning, calculating and comparing armies leads to victory.

  * Secure yourself against defeat, and wait for an opportunity for victory.

  * Warfare is only successful if sovereigns and generals do not cause their own defeat.

**How can you achieve advantages over your enemy?**

  * Conserve your resources through stratagems, foraging and espionage.

  * Deceive your enemy and impose your will on him.

  * Observe the terrain and your enemy, then adapt accordingly.

**How must you manage your troops?**

  * To wage war successfully, manage your troops sternly, keep them in uncertainty and make them fight to the death.

**Suggested further reading: _The Prince_ by Niccolò Machiavelli**

_The Prince_ is a 16th century guide on how to be an autocratic leader of a country. It explains why ends like glory and power always justify even brutal means for princes. Thanks to this book, the word "Machiavellian" came to mean using deceit and cunning to one's advantage.
---

### Sun Tzu

Sun Tzu was a military general, strategist and philosopher during the Zhou Dynasty of ancient China in ca. 500 BC.

