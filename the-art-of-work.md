---
id: 5589250334666100075d0000
slug: the-art-of-work-en
published_date: 2015-06-24T00:00:00.000+00:00
author: Jeff Goins
title: The Art of Work
subtitle: A Proven Path to Discovering What You Were Meant to Do
main_color: 3D4B7C
text_color: 3D4B7C
---

# The Art of Work

_A Proven Path to Discovering What You Were Meant to Do_

**Jeff Goins**

The _Art of Work_ (2015) is about finding your _calling_ — that special goal that brings you joy and imbues your life with meaning. These blinks will teach you how to find your calling and how to live by it once you've found it. Your calling is the secret to living a fulfilling life — so don't let it get away!

---
### 1. What’s in it for me? Finding your calling and your legacy. 

How do you feel when you wake up in the morning? Do you struggle to get out of bed and dread yet another unhappy day at work? If so, don't worry! Many of us feel this way, because many of us haven't yet found our calling.

Your calling is the reason you get up in the morning, the reason you strive to achieve. When you find yours, you'll be bounding out of bed everyday, ready to make a difference in the world. These blinks lay out the benefits of finding your calling, and give clear guidelines on how to find it.

In these blinks, you'll discover

  * how having a calling can help you live on after death;

  * why failure can make you a better person; and

  * why there is no such thing as a self-made man.

### 2. Your calling gives your life meaning. 

Do you dislike your job? Well, you're not alone. Only 13 percent of the world's population actually enjoy their work. So how can we go about changing that statistic? Perhaps by searching for work that makes you happy and refusing to do anything else?

Not exactly. The best way to achieve happiness in the workplace is to find your _calling_ — a force larger than yourself that drives your every action. And to do that, you can't think only in terms of happiness and pain.

Humans shouldn't be mere pleasure seekers or discomfort avoiders; instead, they should strive to imbue their lives with meaning.

If you're busy steering clear of pain and failure, you'll never find your calling. Inevitably, you'll face challenges along the way to achieving your dreams, and accepting these difficulties as a natural part of discovering what you really love is part of maintaining forward movement.

Viktor Frankl, the renowned psychologist and Holocaust survivor, knew this well. He wrote that everyone needs a reason to keep living, a cause to push them forward.

But how do you find this cause? That's where _awareness_ comes in. Awareness helps you recognize your calling when you come across it.

People discover their callings when some event or feeling triggers their awareness of it. For instance, Luke Skywalker knew he wanted to be a Jedi after Obi-Wan Kenobi introduced him to the art.

So, though it can be difficult, you need to keep an eye out for this event. Awareness is being ready for the moment so you can seize it when it comes.

Another good way to find your calling is to make a list of all the major events in your life, even the ones that might not seem like the most important. When did you feel the most fulfilled, happy or accomplished? Look for similarities between these moments — your calling might be something that ties them all together.

> _"Failure is a friend dressed up like an enemy."_

### 3. Find good mentors and never stop practicing. 

Finding and following your calling is a personal journey, but that doesn't mean you should do it alone. Getting support from others is a very important part of it!

It's tempting to view successful people as self-made; in reality, however, almost no one achieves success alone. In fact, the most successful people are those who know how to find good coaches and mentors. Mentors guide them as they get closer to their calling.

Every person you meet and every experience you have is a chance to learn something about yourself. Life is your school. Just think about Steve Jobs: He left college to pursue his dream and sneaked into college classes he found interesting or valuable. The rest he learned by working with people in the outside world.

Once you've found the people who can help you move toward your goal, it's time to get to work. You can only master your calling by practicing it.

And the best way to practice is to never stop striving toward knowledge and self-improvement. That means you have to accustom yourself to failure.

Practice isn't about doing the same simple task over and over again. It's about pushing yourself into new frontiers — making mistakes and learning from them.

Think of practicing as muscle building. Muscles grow when you regularly push them a little too far, which causes them to tear and then rebuild themselves.

It's important to note, though, that you can't will yourself to achieve everything. There are some things that some of us just can't do, and that's all right. So if you find you can't make any progress, maybe you haven't found the right calling yet.

> _"Comfort never leads to excellence."_

### 4. Don't stop moving toward your goal and always learn from your mistakes. 

So what do you do once you know where you're headed? Sit back and see where things take you?

Far from it. Your journey is a flight of stairs — not an escalator. Keep taking steps until you reach the top.

Say your calling is teaching, for instance. You shouldn't stop working toward your goal: keep teaching classes, watch other teachers work and study techniques in the library. If you stop for even a short period, it might be difficult to start again. Always keep improving yourself!

But don't worry about making a few missteps. Failure doesn't take you away from success — it leads you there!

Everyone who pursues their dream experiences setbacks at some point. Accept these hardships as opportunities to learn and better yourself.

This is exactly what happened to Steve Jobs when he was kicked out of Apple, his very own company. Instead of dwelling on his misfortune, he got involved with Pixar. He learned from his mistakes at Apple and turned Pixar into a highly profitable business; eventually, he returned to Apple and turned it into one of the biggest companies in the world.

A good way to think about your mistakes is to use the _pivot foot_. In basketball, when you take two steps without dribbling, the foot you land on becomes your pivot foot, meaning you can't take it off the floor. But you can move your body around it, looking for chances to pass or shoot.

In other words, you can still act even when you think you've come to a stopping point. No roadblock is the end. You've got to be the one to take yourself where you want to go.

### 5. Live a portfolio life that's filled with interesting challenges and variety. 

Have you ever heard the term _portfolio life_? Probably not. But you might already be living one!

In a portfolio life, your identity is based on a wide range of things, not just one. The author, for example, isn't only a writer — he's a father and husband, too.

There are four main areas in a portfolio life: _work, home, play_ and _purpose_.

_Work_ doesn't have to refer only to your main job; it can include other projects you work on as well. _Home_ is all about your family and friends — that's where much of the meaning in your life comes from. When you do things just for the joy of it, that's your _play_. Finally, your _purpose_ is the main goal of your life — what you're prepared to take risks for.

The growing number of freelancers illustrates the popularity of the portfolio life. By 2020, between 40 and 50 percent of the American workforce will be freelancers, and by 2030, they'll comprise the majority.

Why do people like freelancing so much? Because humans aren't robots programed to do a single thing. Our interests are myriad and we feel better when we can allot time to them all. Variety is part of a meaningful life.

Mihály Csíkszentmihályi, a psychologist, argues that we should all aim for a mental state called _flow_. Your flow lies at the intersection of what you're good at and what challenges you. A task isn't interesting if it's too easy, and if it's too difficult, you'll get anxious. Flow is about finding that spot in the middle.

So seek out different sorts of challenges that will bring variety to your portfolio life, because living a portfolio life means living for your calling.

### 6. Your calling is your legacy – and you'll never stop living for it. 

Your calling is a lot bigger than doing something like writing a novel, because it isn't just about you!

Your calling is more than a personal goal — it's your legacy, the work that will continue to inspire others long after you're finished with it.

If you're doing something for yourself alone, that's not your calling. So don't aim to produce one masterpiece — your calling is more like a magnum opus. It's your entire body of work.

When you think of Mozart, for example, you might think of _The_ _Magic Flute_ or some particular sonata. But naming one piece of music doesn't capture Mozart's artistry; it's his life's work that has influenced people and inspired them for centuries.

To understand your calling you must acknowledge death, because a calling is also a legacy. You'll never finish with your calling, no matter how hard you try. And a fear of death can in fact be useful — it drives many to keep creating until the end. While on his deathbed, Einstein even asked for his glasses because he was so desperate to finish his work in time.

Unfortunately, you can't stop death. You have to accept that you'll probably never finish everything. But even after you're gone, your legacy will live on through your work.

Albert Uderzo, the creator of the comic _Asterix_, understood this well. He was the only person who made _Asterix_, and yet he wanted his comic to outlive him, so he passed the series to a new author, Jean-Yves Ferri. That was difficult for him, but it ensured his legacy would live on.

Living for your calling isn't just about mastering the art of work. It's also about the art of living and dying in peace.

### 7. Final summary 

The key message in this book:

**Everyone has a calling — an idea that imbues their life with meaning and pushes them forward. So develop the awareness to find yours and strive to live a portfolio life that will give you the happiness and balance you need. When you live for your calling, you won't just produce work you'll be proud of — you'll feel more fulfilled and leave behind a legacy that will continue to inspire others even after you've passed on.**

Actionable advice:

**Take risks and make mistakes.**

It's good when something challenges you — that's how you grow. So don't be afraid to embrace new things, and if you make any mistakes, take advantage of them. Use them as learning opportunities that can bring you closer to your goal. Following your calling is all about leaving your comfort zone and exploring the unknown.

**Suggested** **further** **reading:** ** _The In-Between_** **by Jeff Goins**

Most people spend their lives impatiently waiting for the next big thing to happen. But what if those moments between big events were actually hugely influential on our identities and lives? What if instead of focusing on those big events, we slow down and savor those in-between moments? This book comprises a collection of Jeff Goins' life-changing experiences that happened in the in-between, which he would like to share with readers.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Jeff Goins

Jeff Goins is the bestselling author of _The In-Between_, _Wrecked_ and _You Are A Writer_. His work mostly deals with pinpointing your dreams and turning them into a reality.

