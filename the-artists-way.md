---
id: 568168f1704a88000700002c
slug: the-artists-way-en
published_date: 2015-12-28T00:00:00.000+00:00
author: Julia Cameron
title: The Artist's Way
subtitle: A Spiritual Path to Higher Creativity
main_color: FB3932
text_color: CC2F29
---

# The Artist's Way

_A Spiritual Path to Higher Creativity_

**Julia Cameron**

Everyone has the potential to create great art. The problem is, we're often held back by our doubts, past experiences and even the people around us. _The Artist's Way_ (1994) is full of advice about connecting with your artistic side so you can realize your creative dreams.

---
### 1. What’s in it for me? Recover your creative self. 

When you were a child, could you contentedly draw or sing for hours? Did you spend nights with a flashlight under your covers, secretly writing in your diary when you were supposed to be asleep?

And what about today: is your life still full of artistic passion? Or did you become a shadow artist, yearning to be creative yet stuck at some stuffy office? Maybe you did succeed in making a living as an artist, but continue to struggle with creative blocks.

Whatever has held back your inner artist — these blinks will show you how to free her.

One quick tip: the advice in these blinks is based on a twelve-week program. If you want to apply these blinks to your life, try focusing on one tip each week. You'll find two tips in every blink.

In these blinks, you'll also find out

  * why it actually makes sense to ask yourself out for a date;

  * why you should fear and avoid the strange species called _crazymakers_ ; and

  * the connection between God's wishes and great movie plots.

### 2. Connect with your creative side by writing regularly and exploring new things. 

If you're looking to rekindle your creativity, you don't have to start by writing your magnum opus! Instead, start out with some simpler first steps, like writing _morning pages_.

Morning pages help get your creativity flowing when you first wake up. Just sit down, let your thoughts wander and write whatever comes to mind. Your morning pages don't need to be artistic or brilliant. Think of them as a form of meditation: shut out the outside world and focus on what you're doing right at that moment. If you don't know what to write, write about the fact that you don't know what to write.

When working on your morning pages, aim to suppress the critical, logical part of your brain. Let your _artist-brain_ take over instead. Don't worry about making mistakes or the pressure to write something brilliant. The artist-brain wants to play and experiment — let it go!

You also need to take your artistic side on a date now and then. Schedule an _artist date_ with yourself: set aside a couple of hours per week to focus on your inner artist. Go to the beach, watch an old movie, visit a gallery or just go for a walk. The point is to relax by yourself and allow your mind to drift.

You'll find that the more you explore yourself and the world around you, the more you connect with your inner artist. That's why it's so important to learn as much as you can about yourself and your surroundings. Always seek out new sounds, sights, smells or tastes. 

Whether it's taking a different route back home so you can see a new part of town, or stopping to appreciate the sky at dusk or the flowers on your path to work — explore something new whenever you can!

### 3. Build your confidence by overcoming your demons and self-doubt. 

We all struggle with tough times in our lives. But if you want to be more creative, you can't run away from them.

Creative people often have to face and overcome difficult circumstances. For example, lots of parents discourage their talented children from pursuing the arts because they're afraid they won't be able to support themselves financially.

These people often become _shadow artists._ By settling for unfulfilling jobs, even if they go on to become financially successful, shadow artists will always be haunted by the creative life they never had.

One shadow artist the author spoke with is a millionaire named Edwin. Edwin's father pressured him into finance; now he surrounds himself with artists and art to compensate for the art career he missed.

Shadow artists often end up in fields related to the art they love. A shadow poet might work as a copywriter, for instance.

So if you've been pushed away from the arts, you need to let go of the stigma associated with being an artist and focus on the positive aspects instead. If you were told artists are crazy, you have to consciously tell yourself: "Artists are sane."

You also have to consider deeply what holds you back. Sometimes thoughts hold creative people hostage! Thoughts like doubt can be devastating. Imagine a writer, for example, who has so much self-doubt that she never submits her masterful manuscript.

But thoughts are just one problem. People can bring you down, too. You could fall prey to a _crazymaker,_ a person who feeds on your attention while belittling you at the same time. Crazymakers are often successful artists themselves. They tend to surround themselves with talented artists who are less confident than they are.

Don't let a crazymaker intimidate you. Remember: the only person in charge of your creativity is you.

### 4. The recovery process is emotionally challenging but it's the only way to reconnect with your creative self. 

If you haven't been creative for a long time, it's not always easy to recover your artistic side. But don't worry! Once you start digging into your creative inner world, you'll start uncovering strange thoughts and feelings. These discoveries can be scary, but they're also pivotal.

Anger, for example, is certainly unpleasant but not necessarily something to run away from. Anger can motivate you to move in the right direction. For example, you might watch a movie and feel angry because you think you could've done a better job than the actors who got the roles. If that's how you feel, take your feeling seriously. Write your thoughts down and work toward that goal!

Art can also lead you into some touchy subjects, both for you and your audience. People might lash out at you if your work pulls them out of their comfort zone, as a politically controversial screenplay might. If others try to make you feel ashamed of your creative work, don't let it get you down. You have to keep believing in yourself. One way to do that is to keep positive reviews from old projects on hand. Don't let the negativity drag you down.

You'll also discover new strengths and weaknesses as you learn more about yourself. Even after writing a dozen novels, a writer might still fear the moment when she has to confront a blank page. If you're feeling blocked, consider asking someone for a prompt. Maybe you'll feel more comfortable when someone tells you what to write, like if your editor asks you to write a story about Prague. There's nothing wrong with asking for a prompt!

Anger, negativity, blocks — you're on the right track if you're uncovering these kinds of weaknesses. They mean your recovery process is working. Stay patient and keep doing your morning pages. They'll keep you anchored to your goal of reconnecting with the creative genius inside of you.

### 5. You don't generate ideas out of nowhere; you find them in the universe and help them grow. 

When Michelangelo produced his famous David statue, he said he _found_ David — he didn't create him. Like many great artists, he thought of himself as a mere vessel delivering something much larger than he. He was right.

Artists don't generate their own ideas. They find them out in the universe. Once you accept this, you'll no longer suffer from creative blocks.

The pressure to come up with a brilliant idea can feel overwhelming. The thing is, you don't create an idea any more than you'd create a tree. Like a tree, an idea starts as a small seed and you're just responsible for overseeing its growth.

The "seeds" of all movies, novels, songs, paintings and other creative work are already out there in the universe. When you find one, take care of it. Tend to it so it can grow. That's all an artist really has to do.

If you keep trying to take care of these ideas, you might even find that there's a higher power out there who's helping you.

When you're passionate about your artistic vision, new opportunities will start to present themselves to you. You might get offered a new acting gig or meet a publisher at a bar.

God will help you if you try but you're still the one on the front lines of your creative life. If you're unhappy with your current situation, you're the one who needs to make the change.

The author knew a writer named Cara, for instance, who had an abusive agent. He was so prestigious, however, that she was reluctant to leave him. After a particularly bad phone call with him, she finally fired him. On that very same day, her husband received a number from another agent. Cara called the new agent and they've been working together ever since.

> _"More than anything else, creative recovery is about keeping an open mind."_

### 6. Perfectionism, workaholism and excessive competitiveness will only block your creative flow. 

Fear is an artist's worst enemy. It can prevent you from pursuing your dreams, block your creativity and make you doubt yourself even when things are going well.

Usually, fear of failure is rooted in childhood experiences. One unsupportive parent or teacher who told you you'd never amount to anything could instill a lifelong fear of failure.

But you can also bring fear upon yourself. How? By asking too much of yourself. 

Unrealistic goals — like aiming to write a novel over the Christmas holiday — are very dangerous. You won't meet these goals, and then you'll blame yourself. You'll start to feel regret, grief, jealousy and self-hatred, making it impossible to have any creative flow. Then you'll start putting yourself down for not accomplishing anything, undermining your own self-confidence to the point where you're afraid of trying another project — and you'll give up.

Scary, right?

But besides the fear you can bring upon yourself, there are two dangerous and unproductive habits you have to look out for.

The first is _workaholism_. Some people respond to creative feelings by overworking themselves. And if you think of creativity as God's energy flowing through you, it might make sense that some people respond with such a strong emotional response and work too much. 

But workaholism is counterproductive. Working too much just makes it impossible to explore God's energy flowing through you. It's better to let your brain calmly explore ideas naturally, and not force it.

_Competitiveness_ is also counterproductive for creativity. It distracts you by calling your attention to the wrong questions, like "Why did _his_ film get made and not mine?" Avoid this trap by asking yourself positive questions, like "Did I work on my screenplay today?"

> _"A successful creative career is always built on successful creative failures. The trick is to survive them."_

### 7. Make a conscious effort to bring your creative self out into the open. 

Every artist needs a bit of self-confidence. So if you want to resurrect your artistic self, you'll have to reexamine your past to see what may have damaged your self-confidence, and how you can revive it.

Start by looking back and picking out three past experiences that lowered your sense of self. Any experience or memory counts, even ones that might seem silly. If Anna hurt you when she insulted your drawings in third grade, put it on the list.

Identifying the moments that hurt your self-confidence will help you face up to them and overcome them. That's the only way you can heal from the hurt they caused.

You can also build your confidence with _imagination exercises_, like visualizing your perfect creative day. What would you do when you first wake up? What would you have accomplished at the end of the day?

Visualize yourself overcoming challenges too. Remind yourself you can!

_Affirmations_ are another good way to boost your artistic self. Every week, pick five affirmative sentences and remind yourself of them regularly. For example: "I'm a gifted person," "My creativity is a treasure," "I have a lot to offer," and so on.

Develop helpful habits too. If you find yourself frustrated when you don't accomplish enough in one day, try to get into the habit of stopping and taking a deep breath. Pause, then think realistically about what you could accomplish in the rest of the day.

You should also develop a habit of surrounding yourself with more of life's small joys. Fill your house with nice smells or wear your favorite outfit for no particular reason. Write positive reminders for yourself, like, "Treating myself like a precious object will make me strong."

You'll be back in a creative groove in no time!

### 8. Final summary 

The key message in this book:

**It's not easy to activate your creative potential, but everyone can do it. If it seems daunting, just start with a few simple steps: write morning pages, explore new topics and take yourself on a date now and then. Once you start looking within and focusing on your artistic goals, you'll find your creative self again!**

Actionable advice:

**Have fun!**

The creative process should be enjoyable! So the next time you feel stuck, treat yourself to something fun. Play basketball, ride a bike, go rock climbing or just stay home and watch a film. Sometimes you need to relax so you can jumpstart your creativity.

**Suggested** **further** **reading:** ** _Daily Rituals_** **by Mason Currey**

_Daily Rituals_ (2013) is an entertaining and illuminating collection of the daily routines of great minds and artists. Including the work habits of people such as Jane Austen, Ludwig van Beethoven and Pablo Picasso, it offers insights into the best ways to maximize efficiency and prevent writer's block, as well as tips on how to get by in the creative world.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Julia Cameron

Julia Cameron is a poet, playwright, fiction writer, essayist and award-winning journalist. She is also the bestselling author of _The Vein of Gold._

