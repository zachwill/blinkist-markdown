---
id: 549160756263300009150000
slug: the-as-if-principle-en
published_date: 2014-12-22T00:00:00.000+00:00
author: Richard Wiseman
title: The As If Principle
subtitle: The Radically New Approach to Changing Your Life
main_color: DC2F2C
text_color: B22624
---

# The As If Principle

_The Radically New Approach to Changing Your Life_

**Richard Wiseman**

_The As If Principle_ explains how our emotions and thoughts are _reactions_ to our bodily sensations, and not the other way around. Thus we feel happy _because_ we smile! Once you learn how to take advantage of this simple rule, you can improve many facets of your life — from how you eat to how you feel about yourself to even how your body ages.

---
### 1. What’s in it for me? Learn how changing small behaviors can have an enormous effect on your life. 

You're not smiling because you're happy; you're happy because you're smiling.

While the concept is simple, the effect is profound. Our behaviors — or what our body is physically doing — can actually trigger our emotions, and not the other way around.

Think about it: it means that running away from something could actually make you more afraid!

New scientific research has confirmed just how powerful our behavior can be in influencing our emotions. So if you make just a few changes to how you act, you may be able to better stick to that diet, look younger and even be happier. The As If Principle will show you how.

In the following blinks, you'll also discover:

  * how if you pretend you're 18 again you'll actually feel physically younger;

  * why Chinese children have no fear of tonsil surgery; and

  * how by acting in a romantic movie, Brad Pitt and Angelina Jolie fell in love.

### 2. You don’t smile because you’re happy, you’re happy because you smile. 

Do you smile when you're happy? Research has shown that if you smile first, happiness will follow.

Philosopher William James first theorized this idea, saying, "If you want to have a quality, act _as if_ you already have it."

James's ideas followed work by Charles Darwin, who established the theory that a person is able to discern another person's feelings by observing their facial expressions.

James in turn wanted to see if facial expressions actually _caused_ those particular feelings. He postulated that people don't smile because they're happy, but that they're happy _because_ they smile. Similarly, you become afraid by running away from a threat, and not the other way around.

Studies have shown that people actually feel happier when they force themselves to smile. In one experiment, participants were told the study was examining electrical activity in muscles.

Researchers attached electrodes to each participant's face and measured their reactions as they made different faces. The researchers discovered that when participants smiled, they started to feel happier, even though they knew they were just smiling for an experiment.

American psychologist Paul Ekman found that this sort of principle exists all over the world, regardless of culture, from the United States to remote islands in Indonesia.

When people look frightened, their heart rate speeds up and their skin temperature drops. When they smile, their heart rate falls and their skin temperature rises.

So if you act as if you're experiencing an emotion, it doesn't just affect how you feel — it affects your body. So if you want to feel happy, act _as if_ you already are. This is the "as if" principle.

The "as if" principle can apply to many aspects of our lives. Psychologist Sara Snodgrass found that people who take longer steps, walk with a slight bounce and swing their arms, tend to feel happier than people who take small steps and slouch.

> _"If you want to be happy and you know it, clap your hands."_

### 3. You can force the flames of passion by acting as if you’re in love. 

So can acting like you're in love make you actually fall in love?

Different emotions are associated with different bodily sensations. If we replicate those physical sensations, we can make ourselves feel in certain ways.

People once thought that a singular emotion caused a particular physical reaction. Psychologist Stanley Schachter refuted this idea in the 1960s when he extended the "as if" principle to bodily sensations.

When someone shouts at you, your heart beats faster, and you assume you're feeling anger or fear. Yet if your heart races when you see an attractive person, you assume it's because you feel desire.

In one study, however, researchers had two groups of men consider a woman's attractiveness. One group had to jog in place for two minutes before looking at the woman. With an increased heart rate, the men in the group found the woman more attractive than the other group; the reason being that their bodies interpreted the message of an increased heart rate as desire.

So it follows that if you behave like you're in love, you'll increase the chance of love actually happening. Moving closer to a person, looking into their eyes or playing footsie can all make you more attracted to a potential partner.

A group of Harvard University researchers studied this effect by preparing participants to play a poker game. Certain male-female pairs were taught to cheat by tapping a secret code on each other's feet.

After the game, the researchers asked participants whether they found their poker partner attractive. The pairs who practised played footsie found each other more attractive than those partners who didn't make foot contact!

Interestingly, many actors have fallen in love after playing lovers in a film. Richard Burton and Elizabeth Taylor, Brad Pitt and Angelina Jolie, and Warren Beatty and Annette Benning are all couples who first met as lovers on screen.

> _"It is possible to conjure up Cupid."_

### 4. If you say it, you may believe what you’re saying. The “as if” principle is a powerful persuader. 

Saying is believing. If you behave like you believe in something, your opinions can actually change.

Former U.S. President Lyndon Johnson used this to his advantage during the Vietnam War, a conflict that caused much strain in U.S. society and many casualties on both sides.

Some administration officials disagreed with Johnson's methods in handling the war. Instead of trying to convince these men of his point of view, Johnson instead sent them personally on fact-finding missions to Vietnam, and in the process they had to give high-profile speeches about the war, defending the government's position.

In the process of hearing themselves repeat the government's arguments for war, many of the officials came to actually believe what they were saying — the "as if" principle at work.

The "as if" principle can create positive or negative change. If you can get a person to act a certain way, it's possible that they'll begin to think in a certain way, too.

A group of American prisoners of war experienced this firsthand during the Korean War. Spending time in camps, they were encouraged by their captors to express their support for communism, first by writing down positive sentences about communism, then by reading the statements out loud and debating their merits.

Eventually, prisoners were rewarded with fresh food or fruit if they penned essays extolling the virtues of communism. Through this process, many servicemen came to truly believe what they were saying, and some even chose to stay in North Korea to live under communism after the war was over.

> _"Behavior influences what people believe."_

### 5. Don’t rely on rewards to motivate; incorporate the “as if” principle to stimulate peoples’ interest. 

Rewarding people for work done isn't a great long-term strategy, as rewards encourage people to behave as if they don't want to do the activity for which they're being rewarded.

The logic is simple. We think, "I'm being given money for something I don't want to do. Therefore, this thing can't be fun." The prospect of a pay-off can actually turn play into drudgery!

Psychologist Edward Devi studied this phenomena by having volunteers solve a puzzle for 30 minutes. Some volunteers were told that they'd get money for solving the puzzle.

Interestingly, the people who expected cash for finishing worked _less_ hard on the puzzle. In comparison, the other volunteers worked harder as they were doing it for their own enjoyment.

Even thinking about a reward can be detrimental. Another researcher found that when writers were asked to think about the financial success of bestselling authors, their work was less creative. They wrote better when they focused on the intrinsic pleasure of writing.

Instead of offering a reward, you can use the "as if" principle to motivate people. When someone sees themselves acting in a certain way, they'll be motivated to keep acting in that way.

A small change can have a big impact, as researcher Patricia Pliner illustrated in a charity experiment. A group of people in a neighborhood were approached by volunteers to donate to a cancer charity. Of the first group asked directly for money, some 46 percent decided to donate.

Another group of residents, however, had had a visit two weeks earlier, when a volunteer asked them to simply wear a pin to support the cause. They didn't ask for a donation at the time. Yet when volunteers came around a second time to ask for a donation, over 90 percent of residents agreed.

They felt that they were already supporting the charity, and so they wanted to continue!

### 6. How you act can change who you are. The “as if” principle shows how malleable we can be. 

We've seen how physical behavior can inspire emotions. But can behavior alter your personality?

Psychologist James Laird was interested to test whether a person who suffered a demeaning experience would have as a result lowered self-esteem. His results, as well as those of other experimenters, showed just how powerful the "as if" principle can be.

In his experiment, Laird divided his team into two groups. Through flipping a coin, the groups would determine what they had to do: either lift heavy weights, or eat a worm.

Just before the group stuck eating the worm were about to dig in, one of Laird's associates rushed in the room and told the group they didn't have to eat them after all. Yet interestingly, only 20 percent of the worm-eaters agreed to put their forks down — the rest didn't fight their fate.

What happened? Faced with a degrading task, the participants took on the mantle of the degraded, and their self-esteem suffered as a result. (Yet no one really had to eat a worm, in the end.)

Questioning Laird's results, other psychologists performed similar experiments yet replaced the worms with caterpillars. Curiously, the results were still the same.

The way you dress can also influence your self-perception. Wearing a black shirt can make you more authoritarian and aggressive, while wearing baggy, comfortable clothing can make you more tolerant.

Cornell researcher Mark Frank studied the behavior of athletes when wearing a series of different colored uniforms. When players wore black, Frank found that they were more aggressive, regardless of the sport.

Role-playing certain characteristics can also make you develop those characteristics as your own. Psychologist Philip Zimbardo's infamous "Stanford prison experiment" in the 1970s illustrated this dramatically.

Zimbardo had volunteers role-play a prison situation, where half were prisoners and half were guards. The guards became abusive and violent almost immediately; and Zimbardo stopped the experiment after just six days, even though it was planned to go for two weeks.

While the volunteers at the start merely _pretended_ to be abusive guards, their feelings and behaviors adapted almost immediately to the new situation, and they became abusive guards in reality.

> _"Just by changing the way you behave, you could, for example, quickly become less aggressive, especially likeable, and far more confident."_

### 7. You can improve your mental or emotional health through the way you act. 

Millions of people across the globe struggle with psychological disorders, such as phobias or depression.

Could the "as if" principle help them? Absolutely. It can help reduce feelings we don't want.

In the 1990s, a group of researchers discovered that people who had had botox injections — a beauty treatment that essentially paralyzes the muscles of the face, to combat wrinkles — felt less of an emotional reaction when they watched certain video clips.

This experience is in line with the "as if" principle: if your body doesn't physically express an emotion, you'll feel less emotion. So if you feel pain but you act like you don't, you'll actually feel the pain less.

In the 1970s, a British doctor named Peter Brown noticed that children in China seem unafraid of having their tonsils surgically removed. They had been taught to view the procedure positively, and so before the operation, they were relaxed and smiled often. As a result, they felt less pain.

The "as if" principle can help minimize other negative emotions, too. You can reduce anger by acting calm, for instance. In fact, research has shown that couples who are verbally aggressive with each other are more likely to become physically aggressive.

This works the other way around: acting depressed makes us more depressed. One researcher was able to help people suffering from anxiety and panic attacks by teaching them how to relax, and to see how their bodies were reacting in a more positive light.

So when you feel anxious, think about how this could benefit you. Nervousness before an exam could help you study, and a bit of adrenaline might be good in an interview.

Act the way you want to feel, and the feeling will become a reality.

> _"By changing their behavior one small step at a time, they will slowly change their minds forever."_

### 8. Old age is truly a state of mind; if you act younger, you’ll feel younger. Act healthy, be healthy! 

Using the "as if" principle can help you reach your dietary goals, too, as behaving in a healthy way can actually improve your health.

This is especially true with food. A researcher in the 1960s found that we eat for two reasons: either we get an _internal_ signal from our body, telling us we're hungry, or we get an _external_ signal, such as seeing a delicious cake in a shop window.

Slimmer people tend to follow their internal signals, while overweight folks follow external ones.

This means that if you act like you're hungry, or you see things that make you _feel_ like you're hungry, you'll eat even if you don't need to.

So if you want to diet, avoid external signals and focus on yourself and what your body needs. Eat when you're hungry, and stop when you feel full.

Also, don't watch TV, listen to music or read when you eat. Focus on yourself alone and your food. If self-discipline with food is hard for you, try eating in front of a mirror or with your non-dominant hand.

What's more, acting younger can even slow the effects of aging. Psychologist Ellen Langer studied this phenomena with men in their 70s and 80s. One group was asked to remember their lives in 1959. They listened to radio broadcasts from the period and talked about their memories in the present tense. The other group was asked instead to share stories about their current lives.

After a week, the first group showed improvements in dexterity, speed, memory, eyesight, hearing and blood pressure. In an intelligence test, some 60 percent of the first group did better, while only 40 percent of the second group improved.

Acting like they were "younger" actually knocked years off their bodies and minds!

> _"Next time you are confronted with a plate of high-calorie snacks, simply push it away. This will make the temptation fade."_

### 9. Final summary 

The key message in this book:

**Our emotions don't control our behavior; instead, our behavior controls our emotions. If you smile, you'll feel happy, and if you act younger, you'll feel younger too! If you apply the "as if" principle, you can fundamentally change who you are — not only emotionally but also physically.**

Actionable advice:

**Tempted to eat a plate of cookies? Just push it away.**

If you physically push a plate of cookies away from you, it signals to your body and your brain that you're not interested in it. In response, your body will feel less hunger for the cookies. So if you know you don't want something, act like it, and your resolve will strengthen.

**Suggested further reading:** ** _The Magic of Thinking Big_** **by David J. Schwartz**

_The_ _Magic_ _of_ _Thinking_ _Big_ unveils why believing in ourselves is a pivotal key to success, and how we're each capable of achieving any of the goals we've always dreamed of. The author's methodology is supported by his work as a professor and leadership counselor, as well as by his innumerable interactions with people and businesses that have seen both sides of the success-failure coin.
---

### Richard Wiseman

Richard Wiseman is a psychologist and writer, and holds the only chair of the Public Understanding of Psychology at the University of Hertfordshire in the United Kingdom. He's written several best-selling books, including _The Luck Factor, Quirkology, 59 Seconds_ and _Paranormality_.

