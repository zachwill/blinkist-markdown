---
id: 50cf18c1e4b0921d8ef3b70a
slug: the-ascent-of-money-en
published_date: 2013-05-06T00:00:00.000+00:00
author: Niall Ferguson
title: The Ascent of Money
subtitle: A Financial History of the World
main_color: 5D768A
text_color: 455766
---

# The Ascent of Money

_A Financial History of the World_

**Niall Ferguson**

_The Ascent of Money_ is an explanation of how different historical events led to the development of the current financial system.

It aims to show how, despite its proneness to crises and inequality, the financial system and money itself are drivers of human history and progress.

---
### 1. The financial system is evolutionary in nature. 

Charles Darwin discovered that life evolved through a process known as natural selection. Life forms most suited to their environments prospered, whilst those less successful died out.

In most environments there are only enough resources to support a limited amount of life. The fight for these resources is vicious, as failure will often result in extinction.

The life forms which were most prosperous were able to spread their genes through reproduction and thus multiply in number, eventually taking the place of the declining species.

The process is continuous; species are always dying out and being replaced by new species. Usually these changes are localized to small environments, but sometimes events happen that cause extinctions on a large scale.

For example, 64 million years ago a meteor wiped out almost all life on earth, including most dinosaurs. In situations like this, new species will grow in the spaces left by the extinct ones, just as mammals took the place of dinosaurs after the meteor disaster.

The financial system evolves in a similar way, through _market selection_ : the financial equivalent of natural selection. The financial world is constantly changing through new techniques and innovations in the same way the natural world constantly evolves. The firms that adapt to these new situations grow and prosper and are imitated by others. This is the financial equivalent of gene reproduction. 

On the other hand, firms that cling to outdated practices and fail to produce sufficient returns are doomed to extinction.

Some events, like the financial collapses of 1929 and 2008, lead to a mass extinction of financial firms and practices. In situations like this, space is created for new types of financial firms and practices to spring up. 

**The financial system is evolutionary in nature.**

### 2. The value of money comes from the trust we as societies place in it, not from its intrinsic worth. 

When the Spanish conquistadors swept through Central and South America in the sixteenth century, they were largely motivated by the quest for gold and silver. They thought that the more precious metal they could acquire, the more coins they could mint, and therefore the more money they would have. What happened though was that the increased supply of coinage actually led to its depreciation in value, regardless of what rare metal it was made of.

The conquistadors failed to realize that the power and value of money comes not from its physical worth but from what people are willing to give you for it. An overabundance of money will always lead to inflation.

It doesn't matter what it is made of, as long as society trusts in its value. 

Trust is a crucial element in the value of money. We trust that the money we use will hold its value and that, unlike the Spaniards, our central banks won't overproduce it. We trust that banks will keep our money safe, and they trust us to pay our loans back. Without this trust, money is not worth the paper it's printed on.

Today, physical money itself is worthless, as paper and base metal coins hold very little intrinsic value. The vast majority of money in the world doesn't even physically exist; it is entirely virtual and can be transferred electronically across the world without ever materializing physically.

These worthless tokens are accepted as holders of value because together we trust in them.

**The value of money comes from the trust we as societies place in it, not from its intrinsic worth.**

### 3. The system of credit and debit funds the financial system through the creation of money. 

The invention of _credit and debit_ was one of the most important developments in human history. It stands alongside technological and scientific advancements as a driving force in human progress.

The earliest forms of borrowing and lending are found in ancient Mesopotamia. As payment for deposits, lenders were given clay tablets inscribed with how much they were owed, similar to IOU ("I owe you") notes.

Driven by developments in the practice of banking, this simple process of borrowing and lending eventually evolved into the system of credit as we know it today.

Banks are the primary creators of credit, which in turn funds the financial system through the expansion of money.

The expansion of money means credit actually creates money. Say you deposit €100 into the bank. They will probably keep a little of this money in reserve and lend, say, €90 to someone else. That person may place this loaned money in another bank, which then repeats the first bank's process, keeping a little and loaning out the rest, e.g., €80.

The system has therefore created €270 from the original €100, and the money supply has effectively been widened.

Monetary expansion has been crucial in the funding of the financial system, as the money created can be used to fund investments or buy goods. Without this basic relationship between debtor and creditor, the financial system would stall because the supply of money would become stagnant.

**The system of credit and debit funds the financial system through the creation of money.**

### 4. The financial system is a combination of interconnected financial markets and institutions developed over centuries. 

The modern financial system was largely developed in Western Europe and North America.

In medieval Italy, increased trade with the Arab world brought with it financial rewards and better systems of accountancy. This led to the creation of the first banks, because merchants needed access to credit in order to fund their trade.

Banks are crucial to the financial system because they fund the system by facilitating credit.

War was the catalyst for the next financial development: the invention of _bonds_ and _the bond market_.

Again, this development first appeared in medieval Italy. Italian city states were constantly at war with each other and thus constantly in need of funds to pay for soldiers and weapons. They raised these funds through the sale of bonds: loans to the government that paid a fixed rate of interest. These government IOUs could be traded on the bond market.

The next development of the financial system first appeared in seventeenth-century Holland: _the joint-stock company_. Companies funded themselves by selling shares of ownership, which could be traded on the stock market. This particular development proved very popular and spread quickly across the globe.

In the eighteenth century, _insurance companies_ used analysis of the financial markets to create huge investment portfolios that helped manage risk. Governments also greatly expanded their role in the area in the twentieth century.

Finally, beginning in the 1920s, the _real estate market_ was expanded massively through deregulation and government incentives. This was done for political aims, to try and create a stable and self-reliant property-owning democracy. 

Together, the relationships between these financial markets and institutions make up the modern financial system.

**The financial system is a combination of interconnected financial markets and institutions developed over centuries.**

### 5. Finance, in particular access to credit, is the most effective way of providing a route out of poverty. 

We are often told stories about 'greedy bankers': financial vampires who prey on the poor and unfortunate. They supposedly exploit the weaknesses and lack of financial knowledge in others to suck wealth from the bottom of society and hoard it at the top.

Yet the financial system is in fact the best way for nations, communities and individuals to escape poverty and prosper with a sound financial basis.

Banks, for example, allow savers to deposit money that can then be loaned to those in need. If the money were not put in the bank, it would lie dormant until its owner spent it. By depositing it in the bank, the money is made available to others. It can be transformed from its static state into a more dynamic form (credit) and thus be transferred from the "idle to the industrious."

Without access to reliable credit, poor people have to borrow from less-reputable sources. Loan sharks often prey on those who are desperate for money but who can't borrow from reliable financial institutions. Astronomical interest rates are charged, and punishments for late payments can even take the form of violence.

Access to credit allows people to make long-term plans and decisions such as buying property, starting a business or investing in an enterprise.

As an example, consider microfinance in poor areas. Through microfinance, relatively tiny amounts are lent, usually to poor women with little or no collateral. These loans can fund the purchases of expensive items such as livestock or can help the borrowers start their own micro-businesses. Just a small amount of affordable credit can make a dramatic difference in areas of poverty. 

**Finance, in particular access to credit, is the most effective way of providing a route out of poverty.**

### 6. Societies with a strong financial system will prosper at the expense of those that are financially inefficient. 

In inefficient financial systems, such as the communist command economy or medieval feudalism, capital is unable to flow freely. Bureaucrats control financial resources inefficiently to fulfill political targets, or they hoard wealth to enforce social hierarchy. The situation is static, with little incentive for financial development.

The financial system that developed in Western Europe allows the freest and most efficient flow of capital when compared to other existing systems. Competition is fierce; financial institutions need to be profitable and reliable, otherwise they will be swept aside by better ones.

Societies that use the Western financial model have been able to prosper and spread at the cost of more backward economies.

As the more efficient nations spread their geographical reach in search of more resources and sources of profit, they increasingly came into contact with nations that were unable to manage their own resources properly. This manifested itself primarily as the rise of European Empires, whose power lay mostly in their financial might. Yet, because competition was so fierce, these Empires eventually crumbled and were replaced by economically stronger entities. In the twentieth century, the spread of the Western financial system continued through globalization.

This development can be highlighted by several historical examples:

Between the eighteenth and twentieth centuries, Western nations with highly developed financial systems were able to dominate Asian countries, which, although rich in resources, had an inefficient, absolutist financial system.

In seventeenth-century Europe, the Netherlands, the birthplace of the stock market and a major developer of modern banking, was able to free itself from the much larger Spanish Habsburg Empire. Although the Empire was more abundant in gold and silver deposits, it had little economic understanding, which allowed the Netherlands to constantly outperform its rival economically. 

**Societies with a strong financial system will prosper at the expense of those that are financially inefficient.**

### 7. The financial system mirrors human nature and is therefore deeply irrational and unequal. 

No financial system is perfect. This is because the people who control it and invest in it are human beings, and humans are irrational by nature.

Humans tend to swing between different moods, from optimism and euphoria to pessimism and despair. Where money is concerned, it tends to magnify these mood swings, making us even more irrational.

By nature, humans feel more comfortable in a crowd. We prefer to see what other people do, observing their successes and failures before making our own decisions.

Human societies also tend to be unequal. We all have different strengths and characteristics, and these are judged by others on their perceived importance. Society does not value everyone equally.

These irrational faults and inequalities are mirrored and even intensified in the financial system. 

Firstly, financial rewards are not shared out equally. Those with the right attitude and skills to succeed in the financial world can gain wealth and power, while others will not earn the same rewards. Inequality lies at the heart of the financial system, just as it lies at the heart of the human condition. 

Just as people experience mood swings, the financial markets also crash from highs to lows very quickly. Investor confidence is especially fragile. People are easily scared when their money is involved and will tend to overreact to anything that affects their financial situation.

People will also watch to see what other investors do in certain situations and then copy them. This tends to make financial markets unstable, because people rush in and out of investments en masse.

**The financial system mirrors human nature and is therefore deeply irrational and unequal.**

### 8. The stock market tends to develop bubbles, all of which eventually burst. 

The stock market is like a balloon. When confidence is high, the prices of stocks and shares rises and the market inflates. When confidence is low, investors take their money out of the market and it deflates.

Sometimes the market will inflate at such a sharp rate that prices become unsustainable. Like an overfilled balloon, the pressure bursts the market, making prices fall dramatically. This is known as a _stock market bubble_.

Stock market bubbles are a relatively common financial phenomenon. They also generally follow the same pattern. Nevertheless, investors are often surprised by them, because they fail to recognize unsustainable growth in the market. Many lose substantial amounts of money when the bubble bursts. 

There are many reasons why bubbles develop and why people fail to recognize them.

The average financial career of a Wall Street CEO lasts 25 years — not long enough for most to grasp the up-and-down-nature of the market. Many feel that the good times will go on forever and fail to understand the downside of high growth.

Sometimes unscrupulous executives will exploit naïve investors by falsely convincing them of the company's success and profitability, thus artificially pumping up share prices and reaping large bonuses. When such fraud is eventually unmasked, the share price collapses, leaving stockholders to bear the cost.

The final reason for bubbles is that people don't fully understand the financial system; many are lured in by tales of high profit margins and extraordinary growth.

These elements combine to produce unsustainable growth in market. As more and more people are sucked in, the market grows and grows, until suddenly the bubble bursts, leaving investors with devastating losses. 

**The stock market tends to develop bubbles, all of which eventually burst.**

### 9. Inflation and hyperinflation are often caused by the political mismanagement of a currency. 

_Inflation_ means the weakening of the value of money. A small amount of inflation is to be expected in any economy as more money is always being created, but excessive inflation, or _hyperinflation_, can be very dangerous.

It is particularly perilous for those holding government bonds and savings in a particular currency, because these forms of wealth become less and less valuable.

Hyperinflation is often the result of systematic mismanagement of a nation's currency and monetary system. It can only occur, though, if a nation prints enormous amounts of money and fails to keep its national debt under control.

The responsibility for this phenomenon lies with the central bank or government; therefore, its root causes are political.

Quite often there are deliberate reasons behind currency depreciation and hyperinflation. They can be effective ways for a government to wipe out its domestic debts, because they significantly decrease the value of these debts.

At other times, hyperinflation is the result of bad government policy stemming from political instability or the failure to solve long-term central economic problems.

Whatever the reasons for hyperinflation, it will always carry far-reaching, negative effects for society as a whole. It destroys the wealth of domestic creditors and savers whilst also hurting those on fixed salaries. It also severely weakens the credibility of the currency because outside investors will want to charge higher interest rates to compensate for lending to an unreliable borrower. This will also hurt domestic borrowers.

Hyperinflation demonstrates the power that states ultimately hold over the financial markets, but it also demonstrates the dangers of trying to fight against the market. 

**Inflation and hyperinflation are often caused by the political mismanagement of a currency.**

### 10. Both private insurance and the welfare state are imperfect systems for minimizing financial risk and uncertainty. 

Modern private insurance was invented in eighteenth-century Scotland by two hard-drinking Protestant ministers who wanted to provide financial aid to the families of deceased vicars. They used advancements in social and statistical analysis to create the first insurance scheme in which a member's premiums were pooled and invested to provide returns. These returns could then be given to members in need.

This new system of private insurance provided financial help to the unfortunate and partially mitigated the element of financial risk in life. It proved to be very popular, and systems of insurance spread rapidly in the nineteenth century.

Nevertheless, insurance coverage was far from universal. There were always people who were too poor or lazy to take out insurance. For them, the loss of financial security lead to extreme poverty or forced them to enter the workhouse, a prison-like institution for those unable to support themselves. 

Before long, politicians realized that by providing insurance for these people, they could promote social stability and win over voters.

The _welfare state_ aims at minimizing financial risk through policies such as universal health care, old-age pensions and free education. There is, however, a downside: it has been argued that high taxes and universal coverage remove incentives to work hard and save money. Many welfare states face skyrocketing healthcare costs and pensions due to an aging population.

Over the last few decades, there has been a backlash against the welfare state. Some governments have attempted to dismantle parts of the welfare system to encourage people to take financial risks once again.

To protect themselves from risk, individuals have to choose between expensive, uncertain private insurance and the hope that the shrinking welfare state will be able to care for them.

**Both private insurance and the welfare state are imperfect systems for minimizing financial risk and uncertainty.**

### 11. The deregulation of the real estate market shows how political decisions can have dramatic financial repercussions. 

Throughout history, political aims and financial reality have clashed dramatically. This is perhaps best demonstrated by the developments in the real estate market in the twentieth century.

Governments, especially those in the English-speaking world, have pursued policies to increase the percentage of home owners. The rationale behind this lies in the belief that property owners are more self-reliant and secure than people who rent their homes.

Yet, whilst this goal of a property-owning democracy was politically advantageous, its financial ramifications have been unbalancing.

Consider for instance the 2008 financial crisis, the largest since the Wall Street Crash of 1929. It led to bank failures, national bailouts and a global debt crisis, and its cause lay in the deregulation of the US housing market.

In an attempt to increase property ownership in the USA, subprime lending was promoted by the George W. Bush administration in 2003. A subprime loan is a loan to someone who would traditionally have been thought too risky to lend to.

Politically, the loans were a positive step towards widening property ownership, especially amongst ethnic minorities.

Financially, however, subprime lending was unsustainable; it was only possible through a form of financial alchemy. Those who gave out the original loans knew how likely the borrowers were to default, so they repackaged and resold the loans across the world to those who did not understand their risky nature.

The illusion was only temporary; as soon as the borrowers began to default on their loans, house prices tumbled. Across the globe, institutions such as pension funds and even some municipal governments in Norway were left with worthless debt, and the crisis then spread further throughout the financial system. 

The global financial system collapsed due to the manipulation of the housing market for political gain. 

**The deregulation of the real estate market shows how political decisions can have dramatic financial repercussions.**

### 12. Final summary 

The key message of this book:

**The modern financial system, developed over centuries, is irrational, unequal, unstable and difficult to control. Nevertheless, it provides the most efficient method for the creation and allocation of capital. This allocation of money to the areas it is most needed is crucial to economic development and human progress.**

The questions this book answered:

**What is the financial system, and how was it developed?**

  * The financial system is evolutionary in nature.

  * The value of money comes from the trust we as societies place in it, not from its intrinsic worth.

  * The system of credit and debit funds the financial system through the creation of money.

  * The financial system is a combination of interconnected financial markets and institutions developed over centuries.

**How is the financial system a driving force behind human progress?**

  * Finance, in particular access to credit, is the most effective way of providing a route out of poverty.

  * Societies with a strong financial system will prosper at the expense of those that are financially inefficient.

**What are the flaws in the financial system?**

  * The financial system mirrors human nature and, therefore, it is deeply irrational and unequal.

  * The stock market tends to develop bubbles, all of which eventually burst.

**What is the relationship between politics and the financial system?**

  * Inflation and hyperinflation are often caused by the political mismanagement of a currency.

  * Both private insurance and the welfare state are imperfect systems for minimizing financial risk and uncertainty.

  * The deregulation of the real estate market shows how political decisions can have dramatic financial repercussions.
---

### Niall Ferguson

Niall Ferguson is one of Britain's most renowned historians. He has worked for numerous institutions in Britain and the USA, with a focus primarily on international and economic history.

In addition to his academic career, he is also known as a commentator and writer for numerous newspapers and magazines. He has written and presented various television documentary series, including a television adaptation of the _Ascent of Money._

