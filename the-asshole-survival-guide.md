---
id: 5a37bee2b238e100061c35d6
slug: the-asshole-survival-guide-en
published_date: 2017-12-22T00:00:00.000+00:00
author: Robert I. Sutton
title: The Asshole Survival Guide
subtitle: How to Deal With People Who Treat You Like Dirt
main_color: E13C2D
text_color: B23024
---

# The Asshole Survival Guide

_How to Deal With People Who Treat You Like Dirt_

**Robert I. Sutton**

_The Asshole Survival Guide_ (2017) is a guide to how you can identify and deal with the assholes of the world. People who treat others badly come in all shapes, sizes and levels of awfulness, and the author provides the tools we need to recognize them, fight back and avoid becoming assholes ourselves.

---
### 1. What’s in it for me? Combat the assholes of the world. 

Whatever path you follow in life, whatever type of work you do, there's one thing you can be sure of: no matter where you go, there'll always be some asshole trying to make your life more complicated. Or put it this way: there's always a jerk in every group.

So how do we deal with this? How do we handle this uncomfortable truth? Well, there's only one thing you _can_ do: accept that jerks will be jerks and deal with them.

And in these blinks, you'll learn exactly how to do that. Whether you're dealing with an occasional jerk or a permanent asshole, you'll learn all the right tricks and preparatory tactics to survive and thrive in the toxicity they spread.

You'll also learn

  * when being an asshole can be justifiable;

  * how you can "catch" someone's bad attitude like a cold; and

  * what wealth has to do with being an asshole.

### 2. Assholes are people who cause emotional unease, and some are worse than others. 

What makes someone an asshole? The author asked people to share their experiences with assholes in the workplace, and some pretty repellent behaviors emerged: a boss who only invites his favorite employees to the office Christmas party; someone who constantly interrupts others; and a coworker who, after flashing a warm smile, leans in and whispers, "I'm going to take you down."

Of course, some assholes are worse than others, and knowing how to separate the annoying ones from the more serious cases can be very useful.

So the first question you can ask yourself is this: Do encounters leave you feeling oppressed or demeaned?

As the poet laureate Maya Angelou once put it, "At the end of the day people won't remember what you said or did, they will remember how you made them feel." And when we mentally categorize someone as an asshole, it's usually after that person has made us feel hurt, discouraged or unsettled.

Understanding how someone makes you feel is the first step toward gaining some control of the situation and learning to lessen the impact they have on your life, especially when it's someone you work with. You can then prepare yourself, anticipate certain responses and even consider how your reaction may be linked to past events in your life.

If you find yourself facing these oppressive or demeaning encounters, the next question to ask is whether this person is a _temporary_ or a _continual asshole_.

If your boss is usually a gentle, kind person but transforms into a raging asshole every once in awhile, he may be using a leadership strategy since such behavior is shown to be effective.

In a study of coaching in college basketball, researchers found that when mild-mannered coaches turned aggressive during a halftime pep talk, it regularly sparked a boost in performance. On the other hand, coaches who were always unpleasant didn't have the same effect during their angry halftime speeches.

So there are excuses for your boss being a temporary asshole, but there's no excuse for being a permanent one.

### 3. Get away from assholes to avoid growing used to mistreatment. 

****It may sound strange, but some people get so used to assholes that they don't even realize how poorly they're being treated. They suffer from _asshole blindness_.

The author knows an IT employee who spent eight years at a job even though his boss was a continual, unsympathetic asshole who never admitted to being wrong but always suspected others of wrongdoing.

This is a serious but typical case of asshole blindness, as the victim wasn't just putting up with the asshole behavior; he genuinely no longer recognized it either. It's kind of like a really bad smell — you might catch a whiff at first, but after some time it no longer registers. The same can be true for asshole behavior — the asshole's bad attitude may cause some initial disturbances, but over time you become _habituated_ to it and begin to think of it as normal.

This is dangerous since it can lead to gross misconduct being seen as acceptable. Scientists also attribute this acceptance to the _sunk cost fallacy_. This is when you've put so much time, effort or money into an endeavor that you convince yourself it's worth continuing even when it's clearly doomed. In the case of workplace assholes, you may feel like you've already invested so much of your emotional well-being to put up with their behavior that you may as well stick with the job. In short, not dealing with repugnant behavior at first may lead to you ignoring it entirely in the end.

If the scenario of the IT employee sounds all too familiar, it doesn't mean you need to quit your job, but it does mean you need a new boss.

Google has conducted leadership studies and found that companies generally have both good and bad bosses, so it may be possible to keep your job and simply relocate within the organization. The US software company salesforce.com is aware that sometimes people don't work well together, so it doesn't require employees to get permission from their boss before moving to a different division. However, when these moves occur, the company investigates to find out if it was a personality clash or if the boss really was an asshole.

It's time to stop putting up with asshole bosses, so take the initiative and move on!

> _"Fantasies about dramatic exits are good fun — but acting on them may hurt you more than your tormentors."_

### 4. If you want to avoid “catching” their behavior, keep a safe distance from assholes 

It's not always possible to rid your life of assholes. In fact, if you're stuck sitting close to an asshole every day, you may be at risk of being "infected" with their bad attitude.

Indeed, assholish behavior can be similar to a contagious disease; too much exposure and you might "catch" it.

There is plenty of scientific evidence showing that we tend to take on the negative thoughts, behaviors and emotions of others.

A study at the University of British Columbia found that when students have a teacher who is experiencing emotional exhaustion, they're more likely to have elevated stress hormones than students whose teacher isn't emotionally burned-out.

And at the University of Florida, researchers found that it takes only one incident of rude behavior, such as reading an insulting email, for a person to become a "carrier" and pass on the negative behavior to others. The study concluded that negative behavior can spread "like the common cold."

So to avoid catching the bug, it's best to limit your exposure by distancing yourself from those who are showing symptoms. The key is to keep a safe distance. Move to a new desk, floor or building, if you can.

In the 1970s, MIT professor Tom Allen found that coworkers who sat in close proximity to each other spent more time talking and corresponding across all mediums of communication. And since we have far more means of communicating now, with texts, social media and e-mail, this presents an even greater danger today.

In one interview, the author asked a former Apple employee who'd worked under Steve Jobs for 15 years how he'd managed to cope with Jobs, who was notorious for his asshole behavior. The answer: he kept his distance, sitting as far away as possible in meetings and even avoiding elevator rides with him.

### 5. Reframe asshole behavior as something that is not your fault. 

Sometimes, under extreme conditions, we must develop defenses against assholes.

For instance, former US military cadet Becky Margiotta had to develop strong mental defenses to endure ritualistic hazing from her senior cadets that involved verbal abuse and other forms of humiliation.

To keep her morale up, Becky dealt with this group of super-aggressive assholes by _reframing_ their behavior.

Reframing is a psychological technique used in cognitive behavioral therapy, and it allows patients to recontextualize their problems in a positive light and, in a way, turn lemons into lemonade. Or, in this case, assholes into allies, since Becky reframed the hazing as being imaginative and funny, and not threatening in the least. On occasion, she even laughed at her tormentors' inventiveness as they hazed her.

Simply put, with the right frame of mind, you can turn a negative or threatening situation into an enjoyable challenge, and this can help you do better in it. A study of African-American students taking a math test found that the framing of the test affected test performance. African-American students are sometimes stereotyped as being at an educational disadvantage, and this stereotype is sometimes internalized by students themselves. But when the test wasn't framed as a measurement of intelligence, but rather an interesting and challenging questionnaire, students didn't focus on this stereotype, and results improved.

If you want to reframe an asshole's behavior, start by understanding that you're not to blame. Instead, imagine that there might be a perfectly plausible reason for their actions. For example, perhaps something stressful is going on in their personal life that you're unaware of. This form of reframing is called _reappraisal_, and there's science to back up its effectiveness as well.

In a Stanford University study, students had their reactions analyzed after being shown "upsetting" photographs of angry people. After the first batch of photographs, some students were then given reappraisal training, where they were told to consider that maybe the angry people in the pictures had just had a bad day and that the real source of their anger was someone else. These students were significantly less upset by the subsequent photos than those who weren't given the chance to reappraise.

So put reappraisal to use in your life too!

### 6. Before confronting an asshole, collect evidence of their behavior and evaluate how to confront them. 

If you're forced to endure an asshole every day, you've probably thought about fighting back against their annoying or harmful behavior. You should know, however, that starting a war with an asshole is a risky business fraught with pitfalls. So here are some tips to help you strategize.

First of all, prepare documentation of the person's past behavior. Rock-solid evidence of asshole behavior will prevent you from entering a he-said-she-said situation.

When Fox News anchor Gretchen Carlson accused the powerful network head, Roger Ailes, of firing her because she turned down his sexual advances, she did so with strong documentation in hand. Carlson had recordings of over 18 months of meetings with Ailes that contained a multitude of inappropriate remarks. Thanks to this evidence, Aisles was fired, Fox News apologized and Carlson was awarded $20 million in compensation.

But what if you're not looking to file a complaint or start a lawsuit, and simply want the asshole to stop acting like an asshole? Well, there are two ways to go about it.

The first is a _calm and rational confrontation_, which involves taking the asshole aside in a civilized manner and gently voicing your concerns. This works best with both temporary and clueless assholes who don't realize the negative effect their behavior is having. It's also effective with the egotistical sort who would be mortified to think that people are calling them assholes behind their backs.

The other option is _aggressive confrontation_, which you can think of as fighting fire with fire and giving the asshole a taste of their own medicine. Being an asshole to an asshole is shown to work with selfish, Machiavellian personalities who view others as a tool for their success. A 2015 study showed these types tend to back off when confronted by a similarly selfish and uncooperative person. So it seems that sometimes to defeat an asshole, you need to act like one yourself.

### 7. You might be an asshole, especially if you’re rich, so make sure you practice self-awareness. 

There are two main philosophies regarding how to deal with assholes: we want to avoid them whenever possible, and we don't want our loved ones to have to deal with them either. To be able to check off both of these boxes, you need to make sure _you_ aren't an unwitting asshole yourself.

There's an old joke that goes, "Every group has an asshole, and if you don't know who it is, it's probably you!" Unfortunately, identifying and acknowledging your own asshole tendencies isn't easy.

In fact, only one percent of people confess to being assholes, even though over 50 percent of Americans have reported persistent bullying, either of themselves or others. So, if these numbers are correct, there are clearly a great many oblivious assholes.

But if you are an asshole, it's important to realize this. Psychologist Heidi Grant Halvorson believes that an integral part of our self-awareness, and happiness, depends on acknowledging and accepting how others perceive us, however painful that reality may be. The closer we bring our self-perception to the perceptions of others, the happier and healthier our relationships with those people will be.

If you happen to be rich, self-awareness becomes even more important, because the power that wealth bestows is a risk factor for becoming an asshole.

A study at the University of California in Berkeley found that drivers with the most expensive cars would cut in front of fellow drivers 30 percent of the time and refuse to stop for pedestrians 50 percent of the time. Meanwhile, drivers of the cheapest cars always stopped for pedestrians and only cut off fellow drivers 10 percent of the time. In other words, wealthy drivers tended to behave like assholes.

So remember, even if you encounter great success in life and find yourself in a position of power, don't go the same route that so many others have gone. Break the cycle and stay considerate of your fellow human beings.

As a reader wrote to the author, no one on their deathbed wishes they would've been meaner!

### 8. Final summary 

The key message in this book:

**An asshole is a person whose behavior consistently makes you feel demoralized. The best way to deal with them is by not allowing their behavior to become normal. Remove yourself from environments filled with assholes, but if you're stuck, there are ways to reduce their influence. Keep as much distance as you can from them and try to positively reframe their negative behavior. Otherwise, collect evidence of their behavior and confront them with their misdeeds. And don't forget to hone your self-awareness to avoid becoming an asshole yourself!**

Actionable advice

**Test for assholes.**

To determine if someone is a self-absorbed asshole, ask yourself the following questions:

  * During a conversation do they allow you to get a word in edgewise?

  * Is there a healthy ratio between statements and questions?

  * Do they take any interest in what you're saying?

If all the answers are a strong "No," then there's a good chance you have an asshole on your hands.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The No Asshole Rule_** **by Robert I. Sutton**

_The No Asshole Rule_ delves into the problem of bullying or aggressive co-workers, who in many cases rise to management positions. Sutton provocatively labels them _assholes_.

The book lays out the effect these employees can have on a business, and gives advice on how to develop an asshole-free environment.
---

### Robert I. Sutton

Robert I. Sutton is a professor of management science at Stanford University, where he has contributed several articles to publications such as _Harvard Business Review,_ the _Financial Times_ and the _McKinsey Quarterly_. He is also the best-selling author of _The No Asshole Rule_ and _Good Boss, Bad Boss: How to Be the Best and Learn from the Worst_.

