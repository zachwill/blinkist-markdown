---
id: 5717a5b53f35690007554e30
slug: the-attackers-advantage-en
published_date: 2016-04-27T00:00:00.000+00:00
author: Ram Charan
title: The Attacker's Advantage
subtitle: Turning Uncertainty into Breakthrough Opportunities
main_color: 89CCDF
text_color: 4E7580
---

# The Attacker's Advantage

_Turning Uncertainty into Breakthrough Opportunities_

**Ram Charan**

_The Attacker's Advantage_ (2015) provides you with a game plan for success in times of uncertainty. These blinks show you how to spot opportunities ahead of the pack, allowing you to shake up your industry for the better. Your company will stay on its toes and beat the competition every time!

---
### 1. What’s in it for me? Start attacking your way to business success. 

The meek might inherit the earth, but they won't get very far in business. Business leaders who hide in the shadows, waiting their turn, will never succeed. 

Today's dynamic, ever-changing economy requires a businessperson to be aggressive to keep up. Once-thriving markets can disappear at the drop of a hat, and previously ignored backwaters can transform into money-making tidal waves in moments. Only those with their finger on the pulse of industry, executives poised to attack the next hot opportunity, will succeed.

So how do you learn how to attack and win an advantage in business? These blinks will give you the tools you need to seize the day.

In these blinks, you'll discover

  * how Ted Turner's mind-set is what made him a media mogul;

  * why Finnish technology giant Nokia lost its market edge; and

  * how passive executives can block your best attempts at forward thinking.

### 2. Ignore the signs of structural uncertainty at your company’s peril. Always take them seriously! 

The world economy will grow by $30 trillion over the next decade — great news for any business! 

To make the most of this growth, however, you'll need to know in which industries to invest and which technologies will dominate the market. 

This isn't easy. Why? Because every industry faces _structural uncertainty._

Structural uncertainty arises from external circumstances, and by nature cannot be controlled. For example, internal issues concerning supply or human resources create _operational uncertainties_, which are easier to spot and prevent. Structural uncertainties, on the other hand, can land even long-established business leaders in trouble. 

A technological innovation that turns an industry inside-out is a classic case of structural uncertainty. For example, computer manufacturer Dell was a market leader and enjoyed massive profit margins. That is, until the 2000s when Apple introduced the tablet.

This new technology triggered a drastic drop in demand for personal computers and laptops, Dell's bread and butter. This structural uncertainty resulted in Dell's share price plummeting. And Dell never saw it coming! 

Though structural uncertainties often take a company by surprise, it is possible to spot them ahead of time. Signals and hints are often right under our noses. The thing is, leaders fail to take them seriously. 

This was Nokia's mistake in the wake of the market introduction of the smartphone, a structural uncertainty about which the company was undoubtedly long aware. 

Nokia executives had been researching smartphone technology, and knew that Apple was also filing a series of patents. It was the company's failure to act on these warning signs that led to it relinquishing its dominant position in the cell phone market. 

If you don't want to become a Dell or a Nokia, you need to learn how to spot uncertainty — and how to act quickly when you do. But what's the first step? We'll explore some strategies in the following blinks.

### 3. A catalyst is a visionary person who can spot the potential for innovation before anyone else. 

What do you call someone who responds to a change before it even happens? Not a psychic. Individuals who can spot developments early in an industry and do something about them are called _catalysts_. 

Catalysts have the ability to pick out patterns in a mess of possibilities to see how and why industry-wide change might occur. We call this _perceptual acuity_. 

So how do catalysts use perceptual acuity to their advantage?

A catalyst starts by identifying _seeds_. Seeds are the events that could grow and flourish into a truly innovative idea. Patents are great examples of seeds. Filed and granted, but never widely used, patents for underrated innovations often provide the spark for any massive shift in how an industry works. 

It is these sorts of innovations that catalysts identify and work to build upon. 

Catalyst Paul Breedlove rediscovered a particular speech recognition technology that was invented in the 1960s by researcher Bishnu Atal of Bell Labs in New Jersey. Atal had won awards at the time, but his technology was later forgotten. 

Breedlove however decided to hire Atal as a consultant in the late 1970s, investing $25,000 with the goal of creating a children's digital learning game called "Speak and Spell." The device was a huge success, yet it wouldn't have been possible without Atal's seed technology. 

A seed in the form of a patent is relatively easy to find and identify. But catalysts with sophisticated perceptual acuity can find seeds in the most unexpected places. This can happen when a catalyst uses a seed to bring two unrelated industries together. 

Thanks to the perceptual acuity of Ted Turner, we enjoy the innovation that is satellite TV. Before Turner, satellites were used to transmit data to research centers, while television cable networks were still an early industry. 

Turner saw a seed in the potential merger of these two unrelated technologies, creating a nationwide television broadcast system to beam signals to satellites and transmit them via cable to television sets.

### 4. To boost your perceptual acuity, reach across industries to share ideas with other savvy executives. 

As a catalyst, you'd be a great asset for any growing, innovative business. But what if your perceptual acuity isn't quite up to scratch? Here are three strategies to improve your seed-spotting ability. 

Start by testing your perceptive acuity by chatting with people from other industries or executives who deal with different challenges than you do on a daily basis. 

One CEO of a $10 billion business did just this, bringing together four other CEOs from different sectors, including consumer goods, finance, information technology and heavy industry. 

These executives decided to meet four times a year to bounce ideas off each other and share perspectives. By combining their knowledge specialities, each business leader individually boosted his or her ability to see the "big picture," gaining excellent foresight as a result. 

It wasn't long before these same CEOs made a habit of seeking advice from experts from different sectors all the time. This helped them spot potential structural uncertainties ahead of the competition. 

Reading broadly is another way to gather the knowledge you need to hone your perceptual acuity. 

Warren Buffett reads 500 transcripts of investor calls, in which companies discuss financial performance and corporate vision, every year. This incredible amount of information allows Buffett to make insightful decisions across industries. 

You can hone your perceptual acuity by continuously keeping your eyes peeled for possibilities. Blackstone Group CEO Steve Schwarzman dedicates each Monday morning to identifying anomalies in the external business landscape. He invites other sharp-sighted guests to these meetings, to share what they've noticed, what they feel is "new" and who key industry catalysts are.

### 5. Succeeding in a changing world means staying on the attack and letting no one stand in your way. 

You've probably heard that the best defense is a good offence. This statement is especially true when it comes to structural uncertainties. You might think you don't need to use aggressive tactics, as your industry seems so stable. The reality is, no industry is truly stable or safe. 

Every industry can suffer structural uncertainty. Change is inevitable, so you need to learn how to grapple with change when it happens. How? By making change your own. 

Cloud computing started to emerge in the early 2000s. Adobe CEO Shantanu Narayen early on recognized its potential for his company. He realised that cloud computing would reduce fixed costs for customers, replacing Adobe's existing model of providing hard copies of licensed software. 

He encouraged his company's board to invest early in fostering new computing capabilities and successfully acquired some smaller cloud computing companies before they got big. 

And as Narayen predicted, the era of the cloud finally arrived. Yet Adobe was ahead of the game and not only crushed the competition, but also made cloud computing its own. 

Despite the advantages of pursuing aggressive tactics, not every company or individual will be able to adopt such a strategy. Yet people who are passive can create blockages and slow your business down. 

The author tells the story of Tiptop Snacks CEO Gail Jones, who was concerned about a declining sweets market and growing competition. She hired a consultant with the resolve to get her managers thinking more about attack than defense with regard to the company's strategy. 

All her managers developed a new, aggressive mind-set and applied courageous lateral thinking to day-to-day operations and plans for future growth — except three managers who didn't feel up to the challenge. Although they were valued employees, their passivity was holding the company back and they were let go.

### 6. Identify crucial decision nodes in your firm and fill them with employees who have the right talents. 

Have you worked out where to find the seeds in your industry and how to map a path forward? Great! Now you need to make sure your company's internal decision-making is aligned along this path, too. 

Every person who makes key decisions in your company needs to know _which_ pivotal choices your business will be making. On top of this, _nodes_, or points _where_ decisions are made, must be identified. 

To identify decision nodes, we start with the action that needs to be taken, and work backward. 

For instance, to succeed in a foreign market such as Guatemala, choices about a company's product mix and pricing should be made by local managers — and not by executives far away in New York City. Expert knowledge of the local market makes local Guatemalan managers a crucial decision node for successful company growth. 

After identifying your nodes, you need to ensure your employees have the skills they need to succeed. Crucial decisions require expertise and a range of talents, from communications to negotiations to marketing. 

Employees in decision nodes must also be in agreement with company goals, and should not use their power to prevent changes they don't like or don't want to see. The assumptions made and factors influencing these decisions should also be observed and evaluated. 

Communications company Acme Media used this process to ensure it moved smoothly from print to digital. Countless meetings helped prime the company for this switch, yet for some reason, things weren't moving along efficiently. 

The company CEO finally identified the senior officer who was the stick in the mud. This executive was responsible for driving the digital transition, but had made her name in print media and lacked the skills and desire to make the shift. New talent was brought in; and the transition proceeded smoothly.

### 7. Get ahead of the game and reach your goals quicker by encouraging your team to come with you. 

It's great to have innovative ideas, but there's no point if you can't turn them into reality. Of course, working out how to implement a radically new idea is by no means easy! But there are ways to do it. 

Your plan should begin with your final destination. If you know what you want to achieve, work backward to identify the necessary steps along the way. These steps are your _short-term goals_. 

Though working toward these goals might hinder short-term performance, this is only temporary. Transition is never easy, but it's worth it if you want your company to thrive long term. The final destination, not just quarterly sales numbers, is what forward-thinking business leaders keep in mind.

In 2014, Tata Consultancy planned to create a new business to help clients make the digital shift. The journey to this destination was made possible through the mapping of short-term goals, such as recruiting software experts and negotiating contracts. 

Tata's Seeta Hariharan recruited 300 engineers within seven months, a pivotal short-term step that made further progress toward the company's end goal possible. A great plan needs great team members to make it happen! 

By maintaining constant communication among leaders, managers and team members, you can ensure your company stays on track. 

Verizon CEO Ivan Seidenberg in 2004 proposed a visionary plan of creating a nationwide fiber optic cable network in the United States. This project was very expensive. 

To ensure costs didn't bring the plan immediately to a halt, Seidenberg held multiple meetings to ensure board members were updated on changing external factors, including competitor activity, emerging technologies and new regulations. 

Seidenberg also invited experts to talk about new opportunities and ideas for future developments. By keeping tabs on anything and everything that could impact the project, Seidenberg made sure that the company stayed on track and was able to reach its end goal.

### 8. Final summary 

The key message in this book:

**By developing your perceptual acuity, you can learn to identify the seeds of structural uncertainties that may shake up the market in the future. Use your knowledge to push for change in your company ahead of time, and work to embed this sort of attack attitude in management circles.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _Bold_** **by Peter H. Diamandis and Steven Kotler**

_Bold_ (2015) is a guide to creating wealth by using today's most impactful, cutting-edge tools: exponential technologies. Using real-life examples and step-by-step guides, the blinks explore how to transform start-up concepts into billion-dollar companies.
---

### Ram Charan

Renowned corporate advisor Ram Charan holds a doctorate and MBA from Harvard Business School. He is the author of many bestselling books, including _Execution: The Discipline of Getting Things Done_.

