---
id: 59612401b238e10006ad56ef
slug: the-attention-merchants-en
published_date: 2017-07-14T00:00:00.000+00:00
author: Tim Wu
title: The Attention Merchants
subtitle: The Epic Scramble to Get Inside Our Heads
main_color: 3A818F
text_color: 295C66
---

# The Attention Merchants

_The Epic Scramble to Get Inside Our Heads_

**Tim Wu**

_The Attention Merchants_ (2016) details the history of the fascinating field of advertising. These blinks will teach you all about the "attention industry," offering a historical account of how advertising has arrived at its modern incarnation.

---
### 1. What’s in it for me? Learn about the history of monetized attention. 

Can you imagine a world in which you weren't being bombarded by ads everywhere — on the subway, television, radio and even on our phones? Advertising has become such a regular part of our lives that it's difficult to imagine a time when we weren't being peddled products or services every minute of the day.

Over the past century, advertising has changed quite a bit. Today's advertising departments, for instance, are very different from those in the days of the cigar-smoking, cocktail-drinking ad men you may have seen on _Mad Men_. And yet, despite these differences, the goal of advertising remains pretty much the same: find something that gets people's attention, and piggy-back on it.

In these blinks, we'll take a look at the various attention-grabbing products that have helped advertisers to reel us in.

In these blinks, you'll also learn

  * about the subtle inner workings of branding;

  * why social media is so addictive; and

  * why being starstruck by celebrities is in our blood.

### 2. Newspaper advertisements used to be strictly informational until one New York City paper changed the game forever. 

Nowadays, if you look at a news source, whether online or in print, you're just about certain to stumble upon a litany of advertisements. But this wasn't always the case.

In fact, ads in the earliest newspapers, which date back to the start of the eighteenth century, were, for the most part, informational. They weren't persuasive or loaded with gripping rhetoric; instead, they were simply there to impart facts. Much like the classifieds of today, they included vacancy notices, as well as a lost and found.

Then, in 1833, a young journalist and businessman named Benjamin Day changed everything. He set out to launch his own newspaper, called the _New York Sun_ and, to reach a large audience, began selling each copy for just a penny. With rival papers like the _New York Times_, the _Morning Courier_ and the _New York Enquirer_ each selling for six cents per copy, Day's paper was a much more affordable option.

However, selling a paper so cheaply was bound to generate a loss, as the cost of production would exceed the income from sales. To overcome this obstacle, Day invited businesses to place ads in his paper, charging them a fee for the exposure.

Within just a few months, the paper had become an enormous success, selling thousands of copies a day. By the end of the year, the advertisements were bringing in a massive surplus and, within two years, the paper was number one in New York City.

Day had shown, somewhat unintentionally, how a newspaper could be about more than just news; it could also do business and literally resell the attention of its audience.

### 3. Advertising was used to make a fortune on useless medicine and reinforce the British Army during World War I. 

It's not exactly common knowledge, but early in the twentieth century, advertising for commercial over-the-counter medicine, or _patent_ _medicine_ as it was then known, was revolutionized. This shift was the work of Claude C. Hopkins, an innovative ad man and copywriter, then colloquially referred to as a "scheme man."

For several years, Hopkins wrote ads for Dr. Shoop's Restorative, a company that sold patent medicine. While there, he came up with a plan for "direct mail" advertising, what we now lovingly call "spam mail."

From there he went to Chicago, where he was recruited by Douglas Smith to advertise his product, Liquozone. Liquozone was comparable to most other patent medicines in that it promised to cure just about whatever ailed you, from a cough to cancer and even malaria. Hopkins then continued innovating by coming up with the idea of free samples.

Millions were lured into making purchases only to later realize how useless the medicine actually was. Nonetheless, Hopkins was content; by 1904, he and Smith had raked in the modern-day equivalent of $100 million in revenue.

But attention harvesting wasn't just a cash cow for patent medicine. As World War I ignited across Europe in 1914, advertising was even used to raise an army.

Leading up to the invasion of Germany, Britain was in dire need of reinforcements for its military. With a series of victories under its belt, the German Imperial Army was almost 4.5 million strong compared to Britain's army of just a few hundred thousand.

So, the British army expanded its forces through a series of comprehensive, state-run advertising campaigns, also known as propaganda. They plastered the country's buses, buildings and telephone poles with posters that declared the country's need for army volunteers to ensure the nation's survival. The plan worked perfectly and, by 1915, around 2.75 million men had enlisted of their own free will.

### 4. In the 1920s, advertising and branding became more methodical. 

In the years after World War I, advertising moved forward into a new age.

Twenty years into the twentieth century, a new term became quite popular: _scientific advertising_. It referred to a new view of advertising as a calculated practice involving specific approaches that could be sure to grab an audience's attention.

From then on, there was no element of chance about it; advertising was considered to be a science like any other.

One of these "scientific" approaches was _demand engineering_, which wasn't just about creating demand for a product, but about advertising a problem. Generally, these problems would be ones that people didn't even know existed, and were often entirely fabricated. Naturally, the perceived problem was then utilized as a means to get people to buy a product that claimed to solve it.

A prime example of this concept in action was the case of a toothpaste produced by Listerine, which launched an advertising campaign that used a clinical sounding term, "halitosis," to refer to bad breath and suggested that having this condition would make you deeply unpopular.

Demand for the product skyrocketed as a result, as did the company's revenue: Listerine's annual earnings went from $115,000 in 1922 to a whopping $8 million in 1929.

And demand wasn't all that was being engineered in the 1920s — reputations were too. While we now simply call this practice branding and see it as an essential aspect of any business, it was the advertisers of the 1920s who realized that a good reputation could simply be engineered.

A company didn't have to put in years of hard work and dedication to earn the trust of its customers; instead, a brand could simply be devised and broadcast.

For instance, the copywriter Theodore MacManus brought Cadillac cars to the summit of their market through the subtle branding that Cadillacs carried "The Penalty of Leadership." The implication was that no product could become a world standard without inspiring, and having to deal with, the envy of others. To see how well this worked, you need only consider the fact that the word "Cadillac" is to this day used as an adjective to describe something of the highest quality in any context.

### 5. Radio and television were the successful attention-grabbers of the twentieth century. 

During the first few decades of the twentieth century, advertising was primarily focused on public space. But as the 1920s came to an end, that all changed.

Radio advertising was becoming the new big thing, piping its message right into the homes of consumers. Sellers could now sponsor radio content like serial programs or orchestral performances that aired regularly. This was to the tremendous benefit of sponsors, since the mere mention of a company's name was a great way to attract attention.

Take the toothpaste brand, Pepsodent. When the public found out that Pepsodent toothpaste didn't actually contain any cleaning agents, sales of the product plummeted. Not only that, but toward the end of the 1920s, competition was getting stiffer in their sector, with many more dental hygiene products entering an increasingly lucrative market.

So, to reclaim its place in the market, the general manager of Pepsodent, Walter Templin, invested in and began sponsoring _Amos 'n' Andy_, one of the earliest "prime-time" serial radio programs, which turned out to be a tremendous success.

By 1931, 40 to 50 million listeners out of a total US population of around 120 million were tuning in every night to follow this enthralling soap opera. And every night, those same listeners heard the sponsor's message, praising the incredible new formula of Pepsodent.

But radio's time in the spotlight wouldn't last; just a few decades later, television stepped into its place. By the 1950s, TVs were in the homes of countless Americans, grabbing their attention in a most captivating way.

The number of home televisions in the United States rose from just nine percent in 1950 to 72 percent in 1956. And TV was so good at holding the attention of its audience that, by the close of the 1950s, the average viewer was spending five hours a day in front of the tube! An advertiser's dream!

### 6. Email made it easier to reach people and grab their attention. 

So TV transformed advertising in the 1950s, but it wouldn't be long before a new ad revolution began to swell with the launch of the internet. This groundbreaking technology entered its infancy just two decades later, in the 1970s.

However, at its outset, the internet wasn't very useful — it would take the invention of email to begin connecting people in a truly revolutionary way.

It began in 1969, when the internet formed a network that allowed research organizations to share files between universities like UCLA and Stanford, as well as companies like IBM and government contractors like Bolt, Beranek and Newman, or BBN.

From there, in 1971, a young computer scientist at BBN by the name of Ray Tomlinson took the fledgling internet a step further by launching a system for sending not just files, but messages too.

It wasn't long before it took off and a 1973 survey found that emails comprised 75 percent of all network traffic.

But why is email so effective at grabbing our attention?

Because emails act like rewards. Consider the theory developed by the renowned scientist B.F. Skinner, who states that human behavior, just like that of animals, is controlled by stimuli, like rewards and punishments. When such stimuli occur, the actions that prompted them are reinforced or discouraged accordingly. This theory is known as _operant conditioning_.

But how does this relate to email? According to scientist Tom Stafford, every time we get a nice email or digital message, we get a little boost, like a reward. As this feels good, we continue searching for another one, meaning we constantly refresh our inboxes in the vain hope we'll get another reward soon.

It's no wonder advertisers love to send us email after email.

### 7. The internet offered a new opportunity to harvest attention, but it wasn’t immediately clear how to use it. 

Google has become utterly ubiquitous in modern life, so much so that it has, of course, become its own verb. And it's rise has been no accident.

Google was founded in the late 1990s, at a time when the internet was becoming more and more crowded and relevant content was becoming harder to find. It was nothing like TV, where you could easily move from channel to channel, knowing what you were looking for. Google thus made a name for itself as a way of solving this issue.

Its search engine went above and beyond competitors like Yahoo!, Lycos and AltaVista by way of a powerful algorithm, tight code and simple design. As a result, it rapidly became the number-one choice for internet users.

As a result, Google could harvest loads of attention. While it wasn't immediately clear to the founders, Sergey Brin and Larry Page, how to monetize that attention, they soon found a way: advertising.

But this was not to be the regular, nonstop bombardment style of traditional ads. Google's founders were determined to maintain their transparent, algorithm-driven search engine and keep it free of bias. In other words, they didn't want companies to be able to buy their way to the top of search results.

So, in collaboration with the Canadian mathematician Erik Veach, Brin and Page invented a new system known as _Adwords_, an advertising tool that only displays advertisements that are relevant to a user's interests. Therefore, if a Google search isn't clear enough to identify a specific need or interest, no ads will appear.

### 8. Celebrities capture our attention like no other entity. 

What do Leonardo DiCaprio, Matt Damon and Angelina Jolie all have in common besides their good looks? Well, since even those of us who pay zero attention to celebrities or movies will still know who they are, they all share tremendous worldwide recognition.

Celebrities are powerful attention harvesters around which a lucrative industry is centered. Consider the incredible success of _People_ magazine, a publication focused solely on celebrities, published by the renowned Time-Life Inc _._ When the first issue of _People_ hit the newsstands in 1974, it sold around 1 million copies.

From 1976 to 1980, the publication proceeded to quadruple its income. And, since the 1990s, it has been the single most profitable magazine in the world. As a result, running a full-page ad in People will cost you about $350,000.

After all, even those of us who aren't interested in celebrities whatsoever would still be excited to spot their favorite writer or actor at a neighborhood cafe!

But despite decades of academic inquiry, there are still no convincing explanations for why celebrities captivate our attention so thoroughly. That being said, the powerful reaction that celebrities prompt in average humans has led many scholars to draw parallels with traditions of worship that are commonly known to involve descriptions of transcending the banality of life and getting a glimpse of the extraordinary or the infinite.

For instance, in his book, _The Face of Garbo_, Roland Barthes describes the amazement prompted by the actress Greta Garbo; people were absolutely captivated by a mere glance at her stunning face. Barthes also points to the widely adored actor, Valentino, whose 1926 death resulted in some of his fans taking their own lives.

So, even though we aren't sure exactly why, the lives of celebrities remain a constant pull on our attentions.

### 9. Final summary 

The key message in this book:

**Over the course of the modern era, the business of advertising has experienced several dramatic shifts. From the small, informational notices that used to populate newspapers to the algorithmically driven internet ads of today, advertising is a field with a rich and compelling history.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Master Switch_** **by Tim Wu**

In _The Master Switch_, author Tim Wu traces the development of information technology such as radio, film and television and illustrates how great innovations always come to be controlled by big corporations. Critically, Wu asks whether the internet will succumb to the same fate, or if its inherent design could help it avoid corporate domination.
---

### Tim Wu

Tim Wu is a policy advocate, law professor at Columbia Law School and frequent contributor to NewYorker.com. He's the author of _The Master Switch_ and head of the Poliak Center at the Columbia University School of Journalism in New York.

