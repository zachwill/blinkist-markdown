---
id: 509d1bbee4b0a13d41357e78
slug: the-audacity-of-hope-en
published_date: 2013-01-13T00:00:00.000+00:00
author: Barack Obama
title: The Audacity of Hope
subtitle: Thoughts on Reclaiming the American Dream
main_color: 835E38
text_color: 835E38
---

# The Audacity of Hope

_Thoughts on Reclaiming the American Dream_

**Barack Obama**

_The Audacity of Hope_ is based on a keynote speech Barack Obama delivered at the 2004 Democratic Convention, which launched him into the spotlight of the nation. It contains many of the subjects of Obama's 2008 campaign for the presidency.

---
### 1. All Americans share a fundamental set of values reflecting the founding fathers’ intent. 

The founding fathers had the foresight to write two masterpieces, which directly reflect how they envisioned the American ideal: the _Declaration of Independence_ and the _Constitution_.

At the very core of these documents is the idea that every man and woman is born free, with the same rights and chances in society.

Still, the founding fathers faced the reality that liberty was not without its challenges: never before in human history had a stable democracy worked for a large nation for any long period of time.

Thus personal freedom seemed only possible in a society which also upholds communal values, such as family, religion, patriotism. Another prerequisite was that power be diffused and all absolute authority rejected. Never in America's history should a king, general, or pope alone be able to shape the path of the American people.

Still today, all Americans subscribe to two main notions:

  * The idea of individual liberty and affording every citizen the same chances, regardless of race, religion, or class

  * Communal values bringing people together, keeping their democracy alive.

The attitudes of Americans today continue to be strongly shaped by the founding fathers' spirit. These shared values are often considered so natural that people take them for granted.

### 2. America is currently suffering from a lack of empathy. 

The Republican party promotes an _ownership society_ which tells its citizens "You're on your own," rather than, "We're all in it together."

This world-view reflects the lack of empathy that America is suffering from today. Instead of acknowledging each others' needs and focusing on values shared by all Americans, an _ownership society_ highlights individual differences and promotes conflict.

Sadly, it is the weakest groups in American society that suffer the most from this lack of empathy.

Would Americans accept the hopeless state of inner-city schools if they had to ask themselves how they would feel about their own children attending them?

Would CEOs consider raising their own wages while cutting back the company's health-care benefits if they considered themselves equal to their employees?

Would politicians fight so fiercely over every issue across the political spectrum if they considered admitting that the opposing side might also have a valid point?

Many of America's biggest problems would cease to exist if people would simply make more of an effort to see the world through each others' eyes.

To create a better democracy and tilt politics toward the needs of weaker members of society, all Americans, whether they are politicians, businessmen or ordinary people, should commit to a stronger sense of empathy and ask each other more often "How would this make you feel?"

### 3. Politics are strongly influenced by money, interest groups and the media. 

In the United States, a successful political campaign requires a lot of money. Thus politicians either need to have a lot of money themselves, or they must ask the wealthy to sponsor their campaigns.

One consequence of fund-raising is that politicians become more like their donors. They simply have to spend a lot more time talking to the people in the top 1 percent of the national income distribution, and the longer they are in politics, the more limited their interactions become. Their donors are rich, most of their fellow politicians are rich, and crossing the country in private jets makes the problems of ordinary Americans far less tangible for them.

At the same time, politicians are dependent on special interest groups such as labor unions for Democrats, and big business and the NRA for Republicans. Naturally, these groups are interested in their own agendas, meaning that politicians must learn how to please interest groups and satisfy their specific needs in order to maintain their support.

Finally, there's the media. Politicians are entirely dependent on the media to reach their audiences. For many voters, the media's narrative forms their reality, hence politicians are perceived exactly as the media portray them.

The media like simple, absolute truths, and dislike consensus because it makes for boring news. Politicians quickly learn that civility and honesty will not get them air-time. The media want politicians to disagree with each other, so that's what they do to get more exposure.

We could reduce the extreme influence of powerful special interest groups and the media by mandating public campaign funding and providing free TV and radio time.

### 4. The challenges of the globalized economy are mainly shouldered by ordinary workers. 

During the last few decades, the global economy has changed drastically.

While globalization has brought prosperity to many societies, it also brings its share of challenges, particularly to the ordinary American worker.

The American economy is still strong today, leading the world in important sectors like software. This is thanks to the world-class American education network of colleges and universities which generate a skilled elite of leaders, businesspeople and engineers.

Nevertheless, today American companies face global competition. Every product sold by US companies has to compete with thousands of products from all over the world.

To stay competitive in this global marketplace and to keep their shareholders happy, companies often outsource and automate labor while cutting back on their benefit and health care programs.

While living costs have steadily increased, the average income of the ordinary worker hasn't. Between 1971 and 2001 their median wage showed literally zero increase.

Why is this so? It's partly due to global competition, but also to the current "winner takes all" economy. Ordinary workers take the risks and bear the burden, but once a company succeeds, the rewards are usually not distributed equally. A swell of profits does not mean more wealth for all, it means more wealth for those already rich.

### 5. America needs a social security system and tax code which ensure risks and benefits are spread equally in society. 

Americans are hard-working people, willing to take personal risks. Their culture is a business culture. At its core is the idea that through work and virtue anyone can achieve wealth and prosperity.

Since Americans value hard work, they believe that anyone working full-time should be able to make a living and support a family.

But in the last few decades, ordinary American workers have grown poorer. While the top 1 percent of the income distribution has consistently grown wealthier, the average American finds it harder to make ends meet every month.

Still, Republicans keep calling for corporate tax breaks, real estate taxes that are tailored for the top 1 percent, and reductions in social security and health care programs.

Thus, they ignore the fact that those who benefit the most from the economy should also shoulder their share of its obligations.

They also neglect the idea that capitalism only works as long as it has the consent of the people, and as long as its benefits and risks are shared equally.

This is not the case in an _ownership society_.

### 6. Faith plays a key role in America’s diverse society, helping Americans overcome some of their most pressing problems. 

America's laws are based on Judeo-Christian moral tradition. Most of America's great reformers were motivated by faith and they used religious language to fight for their cause.

Today, America is no longer just a Christian nation. It is a diverse society encompassing all kinds of religions as well as nonbelievers.

In this pluralistic society, it is important to acknowledge the power of faith and discuss its role within society.

Reason and faith are different ways of thinking. In politics, it is important to argue on the basis of principles that are accessible to all members of society, whether religious or not. It is not enough to simply point out the teachings of one's church to argue for a cause.

On the other hand, religion still teaches the values that Americans share: honesty, sympathy and discipline. The majority of Americans rely on faith to assure themselves of a deeper purpose. Without this guiding force, many people are led astray morally, which can be seen, for example, in vicious gang-killings.

Religious organizations like churches help overcome some of the most urgent problems in American society: They sponsor day-care and senior centers. They mobilize members against cuts in social programs and growing inequality. They teach us to think in terms of "thou" and not just "I".

### 7. True equality can be reached through nondiscriminatory laws, scholarships and more emphasis on social justice. 

Predictions show that shortly after 2050, whites will no longer be the majority in America. Even though many Americans are skeptical about large-scale immigration from Latin America, they should remember that not too long ago immigrants from Italy, Ireland and Eastern Europe faced the same skepticism.

The ability to welcome and absorb newcomers has always been a key part of America's greatness. The core idea of the Constitution is that every citizen is equal, and America's economic system has always provided opportunities to anyone willing to work hard.

A lot of progress has been made in terms of equality within a single generation: For example, black poverty has been cut in half and the black middle class has grown consistently.

Still, black and Latino Americans are underrepresented in corporate boardrooms and politics, and their wages are generally only 75 percent of the salaries their fellow white citizens enjoy.

To provide them with the same chances as white Americans, nondiscriminatory laws must be enforced: Wherever whites are consistently preferred when applying for jobs, housing or loans, the government must intervene.

Also, scholarships for minorities can help America tap into a broader pool of talent. In the technology-based economy of the 21st century, America can't afford keeping talented, but poor children out of universities.

But one of the biggest problems blacks and Latino Americans face isn't race-specific at all. Working and middle-class people across all races are suffering from a lack of social justice; wage stagnation, poor social security and public schools that don't teach children the skills they need.

### 8. The problems of poor inner-city neighborhoods can be solved through education and equal opportunities. 

Many Americans look at the situation in poor inner-city neighborhoods with frustration. The situation is often seen as a hopeless cause and change seems impossible.

Rising frustration often leads to people calling for an end to welfare, a stricter police force and harsher judges. The logic seems to be that if the problem cannot be solved, then at least its effect on the life of the working tax-payer should be minimized.

But change _is_ possible. The problems of inner-city neighborhoods are mostly due to a lack of opportunities. People who are born into poor neighborhoods suffer from broken families, a dismal education and often don't have access to proper health care. They stay poor and far too often end up as teenage mothers and criminals.

One of the best ways to break out of this vicious cycle would be to encourage teenage girls to finish high school and avoid becoming teenage mothers.

Since most inner-city kids are already behind their peers when entering the school-system, they also need access to medical care, pre-school programs and mothers who are educated enough to know what their children need.

Despite the fact that many believe a life of crime and drugs is a viable option for young inner-city men, it's not. Dealing drugs is a low-wage business and most of those involved would switch to lawful work if it were available.

If the inner-city poor were provided with the same opportunities as people outside their neighborhoods, improved education and job prospects would help many young men lead lives of structure and dignity, instead of becoming criminals.

### 9. Because the structure of the family-unit has changed, America needs better school programs and more support for parents. 

Many people are talking about the decline of American family values.

A lot of changes have occurred in the last 50 years. Marriage rates have declined, more children are growing up without their biological fathers and parents in general have less time to spend with their children.

Since the cost of living has risen while wages have remained stagnant, the average family can no longer live on a single income. The fact that most women are working these days is not only a sign of increasing equality, but also indicates the growing need for two breadwinners in most households.

For many people this means being a good parent has become much harder: They have too little time for their children and can't afford proper day-care or pre-schooling.

So if Americans are serious about family values, they need policies that make good parenting easier. Everybody should have access to pre-schooling, after-school programs and summer schools.

And since children raised by single mothers in poor neighborhoods are especially vulnerable to the vicious cycle of poverty, teenage motherhood and crime, they should be given the same access to high-quality education as their more fortunate peers.

### 10. A vibrant free market needs governmental regulation to ensure the same opportunities for everyone. 

The Republican idea of an ownership society tells us that the American economy needs less regulation, less taxes, less government.

However, laissez-faire economic policies lead to a divided American society, where a prosperous elite, the _knowledge class_, secures increasing shares of the economic pie, while the lower-paid majority struggles to make ends meet.

The simple belief that a free market should not be regulated so that it can magically take care of itself is wrong. It leads to a layered society, where people are born wealthy or poor and stay that way.

The ownership society mindset neglects the fact that a working free market is always the result of a painful process of trial and error, where the government attempts to balance fairness and efficiency.

It also neglects the fact that both a working free market and American society are based on the idea of _opportunity_, meaning that everyone should be able to succeed through their own efforts, regardless of their background.

Opportunity is only possible when the free market is government-regulated to ensure the economic pie is equally distributed. Only if all people have the opportunity to succeed, can they contribute to a vibrant free market.

Just as the government stepped in after the financial crash of 1929 to help structure the social contract between employers and employees, it should always intervene when the free market isn't functioning fairly.

### 11. To become more competitive, America must invest in education, science and technology. 

If America wants to be competitive in the global economy, it shouldn't cut costs or create trade barriers. Instead it should strive to create a vibrant free market in which great companies can develop innovative products.

For such a free market to work, the government must lay a strong foundation for it. To succeed, a free market needs the right infrastructure as well as educated people who have a legitimate chance to succeed through their own efforts. Only people with this kind of _social mobility_ can contribute to a thriving economy.

In the 21st century, social mobility is only possible when everyone has access to high-quality education. This is not the case in America today. High-school drop-out rates are higher than ever, college fees are rising steadily and most inner-city schools are in an almost hopeless state. Congress has made it both harder and more expensive for students to get a high-quality education. And since a good education is so expensive, the well-educated go into high-paying jobs to pay off their debts. None of them wind up being teachers in inner-city schools, and far too few become engineers or researchers.

To be competitive, America needs more engineers and better teachers, not more lawyers. To develop more companies like Google, it must invest in its education system, fund research and train more scientists and engineers.

### 12. America should invest in alternative energy sources and higher fuel efficiency standards to become energy-independent. 

Dependency on foreign oil drastically undermines the future of the American economy and threatens national security as well.

While America controls only 3 percent of the world's oil reserves, it accounts for 25 percent of total global consumption. Since worldwide demand is rising and supply disruptions are more likely than ever, an economy depending on foreign oil is extremely vulnerable. Terrorists who want to harm America can simply do so by attacking oil fields anywhere in the world.

Additionally, a large share of the 800 million dollars America spends on foreign oil every day actually directly subsidizes unstable regimes.

Even though oil companies are making record profits, the government has subsidized them in the past, instead of acknowledging the fact that the only way to energy independence is through investing in cleaner 21st century energy sources.

America should also invest in higher fuel efficiency standards. Even China has higher standards than the US, and US automakers have waited far too long before shifting focus to more efficient cars.

Investing in new energy sources and higher standards in fuel-efficiency can also lead to thousands of new jobs in the US and help create new industries for the 21st century.

### 13. America’s 21st century foreign policy must focus on the current global battle of ideas and rely more on multilateral action. 

American foreign policy has been strongly influenced by the threats of the Cold War.

But today, it is not threatened by countries with large armies like the Soviet Union, but rather smaller, militant organizations such as international terrorist networks.

In the aftermath of September 11, America should have completely reframed its foreign policy. Instead, the government revived its Cold War strategy, but instead of the Soviet Union, the new enemies were small, despotic countries like Iraq.

The war in Iraq has not only generated tremendous costs, both in military spending and in casualties, it has also fuelled anti-American sentiment across the world.

Since in the 21st century, threats are no longer caused by rival states but rather by ideologies, America has to engage in a "battle of ideas" with these ideologies to promote the American ideals of democracy and liberty. This is a battle which cannot be won through military interventions: Democracy is always a result of a social awakening and cannot be forced upon a nation.

In this battle of ideas, America has to lead by example, by perfecting its own democracy. It's hard to press for human rights as long as America detains suspects without trial.

In cases where a military intervention is absolutely necessary, America should rely on multilateral rather than unilateral actions, the same way it did in the First Gulf War.

Multilateral action not only helps reduce anti-American sentiment, it also brings down the costs America must bear.

### 14. Final summary 

**The key message in this book is that the fundamental values Americans share are the basis for achieving more social justice, a healthy economy as well as establishing a 21st century foreign policy.**

The questions this book answered:

**What is America's status quo and what are its problems?**

  * All Americans share a fundamental set of values reflecting the founding fathers' intent.

  * America is currently suffering from a lack of empathy.

  * Politics are strongly influenced by money, interest groups and the media.

  * The challenges of the globalized economy are mainly shouldered by ordinary workers.

**How can increasing equality and social justice help solve these problems?**

  * America needs a social security system and tax code which ensure risks and benefits are spread equally in society.

  * Faith plays a key role in America's diverse society, helping Americans overcome some of their most pressing problems.

  * True equality can be reached through nondiscriminatory laws, scholarships and more emphasis on social justice.

  * The problems of poor inner-city neighborhoods can be solved through education and equal opportunities.

  * Because the structure of the family-unit has changed, America needs better school programs and more support for parents.

**How can America reinvigorate its economy and create a 21st century agenda for its foreign policy?**

  * A vibrant free market needs governmental regulation to ensure the same opportunities for everyone.

  * To become more competitive, America must invest in education, science and technology.

  * America should invest in alternative energy sources and higher fuel-efficiency standards to become energy-independent.

  * America's 21st century foreign policy must focus on the current global battle of ideas and rely more on multilateral action.
---

### Barack Obama

Barack Hussein Obama II is the 44th president of the United States. He was a community organizer in Chicago before earning his law degree and working as a civil rights attorney. He taught constitutional law at the University of Chicago, served in the Illinois Senate and later represented Illinois in the US Senate. 2008 he became the first African American president and was re-elected in 2012.

In 2009 he was awarded the Nobel peace prize "for his extraordinary efforts to strengthen international diplomacy and cooperation between peoples."

