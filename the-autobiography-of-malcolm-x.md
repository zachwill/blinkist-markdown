---
id: 571e1609da6afc00037e2599
slug: the-autobiography-of-malcolm-x-en
published_date: 2016-04-28T00:00:00.000+00:00
author: Alex Haley, Malcolm X
title: The Autobiography of Malcolm X
subtitle: As told to Alex Haley
main_color: D32E4A
text_color: D32E4A
---

# The Autobiography of Malcolm X

_As told to Alex Haley_

**Alex Haley, Malcolm X**

_The Autobiography of Malcolm X_ (1965) is a profound and personal account of one man's journey from dropping out of school and entering a life of crime and drug addiction to finding redemption through human rights activism. These blinks tell the story of a curious and evolving mind: a man who dedicated his life to helping African-Americans gain identity and freedom from oppression by any means necessary.

---
### 1. What’s in it for me? Get to know one of the twentieth century’s most influential Americans. 

With the exception of Martin Luther King Jr, Malcolm X is probably the most well-known name in the American Civil Rights movement of the 1960s. But whereas King's "I have a dream" speech and his tragic murder are common knowledge, many people know little of what Malcolm X said or wrote or the story of his life.

In these blinks, we get to know the man from his own perspective: where he came from; how he joined the Nation of Islam; his travels to the Middle East and Africa. These are all crucial pieces of the puzzle behind one of the most influential African-Americans of the last century.

In these blinks, you'll find out

  * what the X stands for and how Malcolm came to adopt it;

  * how Malcolm X's life changed after the assassination of John F. Kennedy; and

  * why he broke with the Nation of Islam.

**Note** : The following blinks contain strong, offensive language. A racist term is used to illustrate the author's experience, which is central to his story.

### 2. Malcolm lost his father and his mother at a young age. 

Malcolm X was born Malcolm Little on May 19, 1925.

His father, Reverend Earl Little, was a Baptist preacher who helped spread the teaching of Marcus Garvey, founder of the Universal Negro Improvement Association (UNIA).

Malcolm was the seventh of eight children and his mother, Louise, struggled to look after them all. Louise was born in the West Indies, the product of her mother's rape by a white slave master, which meant that Louise had very fair skin that sometimes resulted in her being mistaken for a white woman.

This also meant that Malcolm was born with reddish hair and a light complexion — the lightest of any of his siblings. Malcolm believed it was this difference in his appearance that led his mother to be harsher on him than her other children. In her eyes, he was a living reminder of the white rapist in the family's past.

Meanwhile, perhaps because of this same difference, his father favored Malcolm, frequently taking him to UNIA meetings.

But his father's efforts to create a sense of pride and identity in the black community led to a tragic end.

One of Malcolm's first memories dated from when they were living in Lansing, Michigan. He woke up in the middle of the night in a panic. A white supremacist group, The Black Legion, had set fire to the Littles' house, but luckily the family escaped unharmed.

Predictably, it got worse: When Malcolm was six years old, his father was murdered. Despite having been fatally beaten, the police called his death an accident.

After that, Louise struggled to keep the family together as a single mother. She was prideful and didn't like resorting to government aid, but eventually had to.

This meant dealing with the government's child welfare officers, who were especially cruel to Louise, trying to turn the children against her. They finally succeeded, putting Louise in a state mental hospital when Malcolm was 12 and sending the kids to live with different families.

> _"Soon the state people were making plans to take over all my mother's children."_

### 3. Malcolm’s rocky school years exposed him to racism, but Boston revealed another world. 

Malcolm didn't have an easy time in school, and at the age of 13, got into trouble after pulling a prank on one of his teachers.

After being scolded for wearing a hat in class, he placed a tack on his teacher's chair when the teacher wasn't looking.

As a result, Malcolm was expelled from school and sent to a detention home. The people who ran the home treated him well, but they freely used the word "nigger" around him and talked about him as if he couldn't understand what they were saying.

It was the first time Malcolm had lived with white people, and he began to see that it was common for them to treat black people as though they couldn't possibly have the same intellect or sensitivity as whites.

A year later, Malcolm entered junior high, where this sort of treatment continued.

He was one of only a few black students in the school, and did his best to integrate with his white classmates. He joined the basketball team but wasn't allowed to dance in the presence of any white girls at the post-game parties.

Malcolm was even elected class president that year, but he came to believe that his classmates were treating him as a mascot, not an equal.

When his teacher asked him what he wanted to be, Malcolm answered, "a lawyer." Malcolm was hurt when the teacher told him he should be more realistic and think about being a carpenter instead.

But Malcolm soon saw a new world on a visit to Boston.

The summer after seventh grade, Malcolm accepted an invitation to visit his half-sister Ella, who lived in the Roxbury area of the city. For the first time, he saw black people proudly being themselves in their own neighborhood and not trying to be white.

When Malcolm returned to Lansing, he could no longer tolerate the racist jokes of his teachers and classmates; he knew there was a better place for him.

> _"All praise is due to Allah that I went to Boston when I did. If I hadn't, I'd probably still be a brainwashed black Christian."_

### 4. Malcolm was introduced to modern black culture in Roxbury and Harlem. 

Luckily, his sister Ella was able to make herself Malcolm's legal guardian, allowing him to move to Roxbury, where he quickly learned about street life.

By coincidence, one of the first people Malcolm met was a man named Shorty, who just happened to be from Lansing, Michigan.

Shorty took Malcolm under his wing, pointing out the finer details of Roxbury's seedier side and getting him a job shining shoes at the legendary _Roseland Ballroom_ jazz club.

Not only did young Malcolm shine the shoes of musicians like Duke Ellington and Count Basie, but he also learned how to hustle: the job of the shoeshine boy also entailed providing musicians and customers with booze, marijuana or the phone numbers of local prostitutes.

During this time, Malcolm himself indulged in booze, marijuana, flashy clothes and dancing.

Shorty showed Malcolm how to _conk_ his hair — a painful process of using hot lye to straighten the curls. But he would later come to view conked hair as an emblem of self-degradation, "the brainwashed black man" damaging his hair in an effort to make it "look white."

Still not yet 18 years old, Malcolm bounced from job to job before landing steady work as a porter on trains, selling food and drink to passengers.

Working on the line from Boston to New York City, Malcolm had his first opportunity to visit Harlem. In just one night he fell in love with the city, especially the huge Savoy nightclub, twice the size of the Roseland.

Malcolm decided to move and, in 1942, became a waiter at Small's Paradise, a Harlem restaurant and popular cultural landmark.

> _"In one night, New York — Harlem — had just about narcotized me."_

### 5. After losing his job, Malcolm took up a life of crime in 1940s Harlem. 

Working at Small's Paradise offered Malcolm the chance to quickly learn how lots of people made a living on the streets of Harlem: by hustling. Malcolm discovered who to trust, who to avoid, and the ins and outs of all kinds of criminal activities, including robbery, pimping and gambling.

Malcolm would soon put this knowledge to work when he lost his job at Small's Paradise after making the mistake of offering a prostitute's number to an undercover cop.

After getting fired, he turned to a friend known as "Sammy the Pimp," who suggested he might make money selling marijuana.

From his time at the Roseland and Savoy, Malcolm had many musician friends who were reliable customers. On a good day, the 17-year-old Malcolm would make $50 to $60. And when the police began to suspect Malcolm of dealing in Harlem, he took his business on the road, traveling on tour with the musicians and keeping them in supply.

But by 1943, things were getting tougher.

The police temporarily closed down the Savoy and rumors that a black soldier had been shot by a white cop almost resulted in a riot.

This effectively stopped what little money white people were bringing into Harlem and increased the police presence. So, Malcolm found work "steering," or escorting white customers to secret locations in Harlem where their sexual needs could be serviced.

Through all of these experiences, Malcolm saw that Harlem was nothing more than a "den of sin" to white people.

Clearly, Malcolm was on the wrong path, and that path was about to come to an end.

> _"Since most New Yorkers had never heard of Lansing, I would name Detroit. Gradually, I began to be called 'Detroit Red'..."_

### 6. Malcolm’s criminal activities finally landed him in prison where he had a profound awakening. 

By 1945, 20-year-old Malcolm was falling into the same trap as many hustlers: he was taking more dangerous chances to make money and increasing his drug intake in order to boost his confidence.

Things began to get truly bad when a gambling dispute forced him out of Harlem.

Malcolm was accused of cheating after winning a bet placed by a man named "West Indian Archie." Archie gave Malcolm a deadline to pay him back — on pain of death. As a result, Malcolm spiraled into a paranoid drug haze of opium, cocaine and Benzedrine.

He left Harlem and returned to Boston, hoping things would cool off.

But Malcolm kept hustling. In Boston he teamed up with Shorty and two white girlfriends to rob rich households. Their crime spree came to an end when he was arrested trying to pawn a stolen watch.

This was Malcolm's first criminal offense, which usually would have resulted in a two-year sentence. But the judge was particularly upset about Malcolm conspiring with two white girls. So, in February 1946, Malcolm was sentenced to ten years in prison.

It was in prison that Malcolm had a spiritual awakening.

Malcolm was impressed by an old convict named Bimbi, who showed him that you could command respect by being well spoken. Bimbi encouraged him to use the prison library and Malcolm quickly became obsessed with reading. He read everything from English and Latin dictionaries to philosophy and world history.

Malcolm would spend entire nights reading, and as a result of the bad light, developed astigmatism that required corrective lenses.

It was at this time that two of Malcolm's brothers wrote to him, telling him about the Nation of Islam: a religion that was trying to help the black man reclaim his long-forgotten identity.

While in prison, Malcolm passionately took to the message of the Nation of Islam, praying for the first time and reading more and more about the tragic history of African-Americans.

> _"No university would ask any student to devour literature as I did."_

### 7. Malcolm left prison dedicated to the Nation of Islam and ready to spread its message. 

Prison proved to be a good training ground for Malcolm to find his public speaking voice. While there, he took part in staged debates in which two people would argue opposing sides of an issue.

In these debates, Malcolm found many opportunities to spread the message of the Nation of Islam and what he was learning in history books. He would condemn the atrocities that the white man had committed on the non-white people of the world in the name of Christianity and profit.

He made a particularly memorable impact during one debate when he challenged the idea that Jesus was the pale, blond, blue-eyed image that the white man prays to, and eventually got his debate opponent to concede that "Jesus was brown."

When Malcolm was released in 1952, he moved to Detroit to stay with his brother Wilfred. Malcolm was eager to devote his life to the Nation of Islam.

Before he left prison, Malcolm had been writing daily letters to Elijah Muhammad, the Nation's leader. Muhammad noticed Malcolm's dedication and at the first opportunity invited him to dinner.

During the meal, Malcolm freely offered his services to the Nation of Islam.

Malcolm immediately began a recruitment drive in Detroit, slowly gaining more followers. His success was noted by other ministers in the Nation who asked him to stop by and speak during services.

Malcolm proved adept at passionately spreading the teachings of Elijah Muhammad and the founder of the Nation of Islam, W. D. Fard.

These teachings included the idea that the "original man" was black and that African-Americans are all descendants of African Muslims who have had their true identities taken away by white men.

Malcolm was proving himself to be a natural activist and speaker.

### 8. As a Nation of Islam minister he became Malcolm X and gained national attention. 

Malcolm was quickly made an official minister of the Nation of Islam. Like other ministers, he was given the last name of X, which stood for the true ancestral family name that had been forever lost.

Malcolm X soon began establishing new Nation of Islam temples around the country.

He opened them in places like Boston, Philadelphia and Atlanta. And in many of these cities, Malcolm attracted new members by catching people coming out of Christian churches and convincing them to hear about a faith that wasn't a "white man's religion."

Eventually, Malcolm X was made minister of his own temple in New York City.

Being back in New York City after nine years away offered Malcolm the chance to sit down with West Indian Archie and talk. He ended up thanking Archie for forcing him to leave New York City, likely saving his life.

By the end of the 1950s, the Nation of Islam was making headlines.

In 1957, a member called Brother Hinton was attacked by police in Harlem who were breaking up a fight in which he was not involved.

Malcolm heard about the incident and led 50 members of his congregation to the police station. He found Brother Hinton covered in blood and demanded he be sent to a hospital, which the police eventually allowed.

Brother Hinton recovered and the Nation of Islam helped him successfully sue the city of New York for over $70,000.

Soon after, television and newspapers were reporting on this incident of police brutality and bringing national attention to the Nation of Islam.

> In January 1958, Malcolm married Sister Betty X and they had the first of their six daughters.

### 9. The attention Malcolm X received eventually led to him being at odds with the Nation of Islam. 

By 1961, the Nation of Islam was flourishing. Huge rallies were being held and, as the press were drawn to Malcolm's passionate personality, he used interviews to get his message across.

Malcolm wanted to set the record straight about the Nation of Islam. The Nation wasn't about "black supremacy," it was about empowering the black man and giving him a sense of pride and dignity in his identity.

Malcolm also answered many questions about why he used the term "the white devil." He explained that it was not about spreading hate, but stating the facts about the "devilish" behavior that the European and American white man had shown to non-white races throughout history. To Malcolm, it begged the question: Why should the black man try to integrate with such a people?

The second point Malcolm tried to make to the press was to remind everyone he wasn't teaching his own message, but that of Elijah Muhammad. He would even turn down interview requests, telling people to address their questions to Muhammad instead.

To Malcolm, Muhammad was unassailable — the man responsible for his salvation.

So it came as quite a shock when, in 1963, Malcolm received disturbing news about his mentor.

It turned out that two of Elijah Muhammad's secretaries were filing paternity suits against him for fathering their children. Malcolm felt betrayed.

This rift with Elijah Muhammad deepened as Malcolm's fame increased. To Elijah and the other leaders of the Nation of Islam, Malcolm was now a threat.

Following the assassination of John F. Kennedy at the end of 1963, Malcolm once again made national headlines by calling the event a sign that "the chickens had come home to roost."

Immediately following this remark, the Nation of Islam publicly banned Malcolm from speaking for 90 days. Then people Malcolm knew within the Nation informed him that orders had been given for his death.

### 10. A pilgrimage to Mecca opened up Malcolm’s eyes to Muslim brotherhood. 

Malcolm X had to reassess everything at this point. The man to whom he'd dedicated his life had let him down and was now prepared to kill him. When Malcolm's assistant told him that he'd been asked to plant a bomb in his car, X knew it was serious.

To escape the threats and reaffirm his spiritual beliefs, Malcolm decided to make a pilgrimage to Mecca.

Malcolm was interested in broadening his knowledge of Islam. Over the years, people had spoken to Malcolm about "true Islam" and how it differed from Elijah Muhammad's teaching. So, Malcolm was eager to make the pilgrimage to the holy city of Mecca, a sacred duty that every Muslim is asked to do at some point in his life.

The trip was an eye-opening experience. On his journey, Malcolm quickly learned that the orthodox Muslim religion was indeed quite different from what he'd been led to believe.

As he toured the holy land he saw Muslims of all colors. He was particularly impressed when he was given brotherly respect and hospitality by blue-eyed, blond-haired people who would be considered white back in the United States.

He met Saudi Arabia's Prince Faisal and was given books to read and told not to be misled by false prophets.

Moved by his experiences, he wrote a letter to the US press expressing his astonishment at the displays of brotherhood that he'd encountered among all races and that he must reconsider his previously held beliefs.

After Mecca and Cairo, Malcolm traveled to Beirut, Nigeria and Ghana. Along the way he made public appearances at colleges and met with politicians.

He tried to rally support from these countries, arguing that just as much effort should be made to help African-Americans as there was to help black South Africans.

> _Malcolm signed the "Letter from Mecca", El-Hajj Malik El-Shabazz, a name he had legally adopted around the time of his marriage to Betty._

### 11. Malcolm signed the “Letter from Mecca”, El-Hajj Malik El-Shabazz, a name he had legally adopted around the time of his marriage to Betty. 

Malcolm returned to New York on May 21, 1964, two days after his 39th birthday. The press had many questions upon his arrival.

Malcolm quickly elaborated on his newfound perspective.

He explained that he was beginning to understand that white people were not inherently racist. But he still felt that white "so-called Christian" society had planted the feeling of superiority into generations of white people and that this had led to a destructive problem.

This could be seen in the riots breaking out in ghettos around the United States.

These ghettos had formed as a result of generations of racism and mistreatment by white society. Malcolm saw it as "sociological dynamite" that had been planted by whites and said that unless action was taken to improve the situation, it was only a matter of time before it exploded.

To solve this problem, Malcolm understood that it was time for a new all-inclusive message to be spread.

To spread the word and make his break with the Nation of Islam public, Malcolm formed the organization Muslim Mosque, Inc. But he knew that more conventional efforts would be needed to create the kind of sociological change needed to free black people from the ghetto.

In an effort to be more inclusive he started the Organization of Afro-American Unity (OAAU). While white people couldn't join OAAU, Malcolm had constructive advice for those who wanted to help.

One of his biggest regrets was an incident years before when a white college girl had asked what she could do and he'd responded: "Nothing."

He explained that he would now tell that girl to start her own organization in her own neighborhood, to spread the word of anti-racism and anti-violence among white people.

> _"And what is the greatest single reason for this Christian church's failure? It is the failure to combat racism."_

### 12. Though Malcolm X was at peace with the possibility of dying, his murder was a tragic loss. 

The death threats that surrounded Malcolm X gave all his actions an urgency. After all, his father and four out of six of his uncles died as a result of violence and Malcolm believed he would be killed by a white racist or the Nation of Islam.

While he was at peace with this possibility, he was not at peace with his family being threatened.

He was especially upset when violence was brought into his home.

Malcolm was fighting a lawsuit brought by the Nation of Islam, who were trying to force his family out of the home given to them by the Nation many years previously . On the night of February 13, 1965, Malcolm and his wife Betty, who was pregnant with their sixth child, were startled awake when a Molotov cocktail was thrown through their front window.

But that was only a prelude to the tragedy of February 21, 1965.

On this day, Malcolm X's OAAU organization was holding a meeting at the Audubon Ballroom in New York City. Malcolm's wife and children were in the audience.

When Malcolm walked onto the stage, three gunmen from the Nation of Islam opened fire, killing him almost instantly.

Betty protected their children, covering them with her own body during the shooting. But after the shooters fled the scene she collapsed next to the body of her husband, crying, "They killed him."

Actor and friend Ossie Davis delivered a moving eulogy at his funeral.

Davis predicted that some people would think of Malcolm X as a racist or a man of hate, but he pointed out that Malcolm actually never associated with any violence in his life. And if people were to listen closely to what he was saying, they would hear the words of a man who simply wanted the best for his people.

To Davis, Malcolm represented a proud example of a strong, uncompromising black man.

> _"And, in honoring him [Malcolm X], we honor the best in ourselves."_ -Ossie Davis

### 13. Final summary 

The key message in this book:

**Malcolm X was a complex individual. Like many people he struggled to understand himself and the world around him. But he remained diligent and throughout his life he stayed curious and searched for the truth. Through hard work and commitment, he proved that redemption is possible in one's lifetime.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The New Jim Crow_** **by Michelle Alexander**

_The New Jim Crow_ (2010) unveils an appalling system of discrimination in the United States that has led to the unprecedented mass incarceration of African-Americans. The so-called War on Drugs, under the jurisdiction of an ostensibly colorblind justice system, has only perpetuated the problem through unconscious racial bias in judgments and sentencing.
---

### Alex Haley, Malcolm X

Malcolm X was one of the most important activists in the history of African-American rights. Malcolm X Day is celebrated on 19 May each year in many cities including Berkeley, California, where offices and schools are officially closed. He has been portrayed in numerous movies by actors such as Morgan Freeman and Denzel Washington.

Pulitzer Prize and National Book Award winner Alex Haley wrote _Roots: The Saga of an American Family_. He was also senior editor of _Reader's Digest_, a respected journalist and groundbreaking interviewer. He ghostwrote _The Autobiography of Malcolm X_ from extensive interviews with his subject.

