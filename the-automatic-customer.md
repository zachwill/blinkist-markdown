---
id: 552467343066650007630000
slug: the-automatic-customer-en
published_date: 2015-04-09T00:00:00.000+00:00
author: John Warrillow
title: The Automatic Customer
subtitle: Creating a Subscription Business in Any Industry
main_color: 2F6184
text_color: 2F6184
---

# The Automatic Customer

_Creating a Subscription Business in Any Industry_

**John Warrillow**

From groceries to ski-slope access to MP3s — today all kinds of businesses operate by subscription, and everyone from giants like Amazon to small local firms are benefitting. _The Automatic Customer_ breaks down the multiple models you can use to tap into the power of subscriptions, explains how to measure your new success, and gives you tips on keeping up the good work.

---
### 1. What’s in it for me? Find out why more and more companies are turning to subscription models. 

Sometimes trends aren't trends because of buzz or visibility, but because they actually work. Sometimes those trends work so well, they start spreading across all industries.

Enter subscriptions, the "new" business model that's actually been around for centuries. It's always been standard practice for newspaper and magazine customers to pay for their products on a periodic basis. Although the subscription model diminished at the onset of the internet, it's come roaring back.

All you have to do is look at WhatsApp, the messaging app that built a 450 million-strong customer base with ZERO advertising. How? Because their subscription service was so genius — $1 a year after the first year — that friends starting getting other friends to sign up by the millions. Customers had become automatic!

These blinks explain how you can learn from WhatsApp and dozens of other companies, both big and small, that have used subscription models to improve their bottom lines and outpace the competition.

In these blinks, you'll discover

  * why Dollar Shave Club names its razors;

  * what painful lesson gamers taught World of Warcraft; and

  * how Amazon used subscriptions to go after Walmart and Target.

### 2. Subscription-based services are a major part of today’s business world. 

You may have noticed that a lot of the companies you're familiar with are suddenly offering subscription services. Subscription services are certainly very hot at the moment. Why?

There are three main reasons the subscription model is going through a renaissance.

First, there's the rise of the _access generation_. The access generation favors _access_ to material rather than ownership of it.

Second, people aren't afraid to make financial transactions online anymore. In the dawn of the internet most people only trusted big companies with their credit card information. Those days are long gone.

Finally, data has become a key asset in today's economy — and subscription-based companies rely on data collection even more than most other businesses do.

These changes have convinced a number of the world's most successful companies to shift their business strategies and make more use of subscriptions. Apple, for example, launched Joint Venture in 2011. Through Joint Venture, Apple helps companies transition to using Mac products in exchange for a yearly subscription.

Microsoft did something similar by offering their Office software through subscription rather than distributing it in stores.

The new subscription economy also allows businesses to offer specialized services, which adds new competition to the marketplace.

Amazon, for instance, has had a lot of success with subscription through Amazon Prime. Amazon Prime allows subscribers to pay $99 a year to stream movies and get free two-day shipping on most purchases. Prime subscribers often prefer to buy a pair of running shoes on Amazon instead of at the local sports store.

They've also just opened a grocery service called AmazonFresh, where customers can have grocery purchases over $35 delivered to their home with no shipping charge.

Amazon's subscription services allow it to compete with small businesses and big businesses alike. They can now compete with major companies like Walmart and Target.

### 3. Subscription-based models make your company more valuable and enjoyable to run. 

The financial perks aren't the only reasons to shift into using subscriptions. Subscriptions actually make your life as an entrepreneur _much_ easier.

First off, subscriptions significantly increase your company's financial value. For potential investors, subscriptions are a predictable stream of cash flow.

In the long run, a monthly subscription of $10 is much better than a single sale of $100. Think of a home security business. Investors won't be that excited about the revenue generated from doing a one-time installation. They'd much rather get a monthly charge for monitoring the property.

Subscriptions also make many parts of running a business much simpler. When you have steady subscribers, you don't have to estimate the demand for your products or services — which is always a difficult challenge.

When you know in advance how many subscribers you'll have, you can optimize your labor and raw materials, decrease your inventory and plan ahead much more easily.

Subscription fees are also a much more reliable form of payment. When people use their cards, the money is usually paid on the day it's supposed to be.

Finally, recurring revenue offers powerful protection against any potential recessions.

Subscriptions also improve the relationship you have with your clients. They're really based on loyalty: the customer offers a consistent payment because they trust that you'll offer a consistent service.

They also increase the overall contact you have with your customers, because they have to pay you regularly.

Of course, this sort of customer loyalty may also lead them to seek other services from you. Subscriptions ensure that they won't just be one-time customers.

So subscription services are certainly beneficial, but how do you switch from theory to practice?

### 4. If you work in e-commerce, try the consumables or surprise box models. 

If you sell goods online, there are two models you can follow to implement subscription services: the _consumables model_ and the _surprise box model_.

In the consumables model, a company offers a subscription for a product that customers need to replenish regularly.

The Dollar Shave Club is a perfect example of this. Its members pay to get disposable razor blades sent directly to their homes by mail.

The Dollar Shave Club provides more than just shaving equipment. Like many smaller businesses, they've managed to survive being taken over by building their brand. They add a bit of fun to their service by giving their blades nicknames like the _Humble Twin_ and the _Executive_.

They've also managed to take control of their brand and service even though they don't manufacture the blades they sell. That's why they never display the manufacturer's name on their boxes: their image would suffer if they appeared to be just a middleman.

In the surprise box model, a company regularly ships a themed package of goods to their subscribers. It might be a different selection of goods each time — the important thing is that the customers are passionate about the theme.

Bark Box, for instance, is a subscription service that sends out various dog treats and toys. They essentially act as a curator, deciding which products to ship out each time.

However, bear in mind that you may face challenges if you want to offer completely different products from different manufacturers each month. Some manufacturers won't be flexible enough to agree to one-time orders. Imagine if you had to find a different manufacturer of specialty chocolates each month.

Surprise boxes are also useful because they can serve as Trojan horses. Conscious Box, for example, offers a monthly subscription for samples of all-natural products. Their real aim, of course, is to entice people to buy the full-size products online.

### 5. If you want to build a community, provide networks for people to use. 

There are different models to follow if your goal is to build a community around your product or service.

In the _private club model_, a company offers a special service or experience to wealthier customers. The customers benefit from the network, goods or services it provides.

The Genius Network, created by Joe Polish, is an example of this. Polish charges an annual fee of $25,000 for entrepreneurs and innovators to gather three times a year to share ideas.

Private club subscriptions also allow customers to buy social status, which is why the fees are so high. Exclusive Resorts, for example, offers a portfolio of luxury holiday homes. It requires a six-figure membership fee.

Ultimately, the private club model aims to keep the community exclusive. The _network model_, on the other hand, does the opposite.

In the network model, each new subscription makes the service more beneficial for everyone. The community should be growing constantly.

Each member should benefit from the growth of the network, which also means that members function as marketers. WhatsApp works this way. If you convince your friends to subscribe, you can all basically communicate for free.

Building a network like this is challenging, however. Your subscribers need to be in the same area, physically or online. The car-sharing company Zipcar learned this early on. Their first potential customers were skeptical because they thought there wouldn't be enough cars available. So Zipcar focused on building up areas one at a time. Eventually they could connect them all and scale up their network more easily.

However, remember that having a large army of free marketers can backfire if dissatisfaction grows. This happened to the online game World of Warcraft (WoW). After it expanded rapidly through word-of-mouth advertising, WoW lost millions of players when its early advocates turned into critics.

### 6. If you specialize in easing your clients’ lives, keep it simple and focus on offering peace of mind. 

Some companies focus on helping their customers in times of need. You might be surprised, but such companies can offer subscription services too.

This is where the _simplifier model_ comes in. The simplifier model aims to ease the lives of busy (and relatively affluent) customers by helping them with various ongoing tasks.

Hassle Free Home Service uses this model. They offer a subscription service where customers pay for a technician to visit them each month. They take care of routine tasks that pile up in every home, like changing the light bulbs or servicing the doors and windows.

This ongoing relationship usually involves frequent contact between the provider and subscriber, which means it's also an opportunity for cross-selling. Hassle Free Home Services works this way. The technicians who come by also offer to take care of tasks that aren't included in the basic subscription service, for an extra fee.

Simplifier subscription services don't have to involve close contact, however, and in some cases customers prefer this. The Mosquito Squad, for instance, is a company that keeps bugs off people's patios. Their subscribers don't have to call them, they just come by to spray insecticide on a scheduled basis.

You can also ease your customers' lives with the _peace-of-mind model_, which offers insurance against something they hope never to face.

Naturally, this model works best when it's focused on something your customers deeply care about, like their pets. The pet-tracking service Tagg, for instance, monitors pets and sends alerts if they leave designated areas.

As its name suggests, this model is about offering peace of mind — it's not about performing tasks like the previous model. Site24x7 is a great example. It monitors your website to ensure it doesn't go down.

The main challenge here is to charge more for the subscription than the cost of performing the required services. So you need a very accurate estimation of how often your customers will need them.

### 7. If you sell content, use memberships and all-you-can-eat models. 

What if your company is based on offering something less concrete? If you sell content or information, the following models might be right for you.

If your customers come to you because of your expertise, you can use the _membership website model_. In this model, you make your expertise available behind a paywall.

The most successful membership website subscriptions usually offer information on mastering a skill or improving a business. ContractorSelling.com, for example, offers information and advice for contracting businesses in exchange for a monthly fee of $89.

It's best to focus on _business-to-business_ sales for this model, because people are more willing to regularly pay for information when it applies directly to their business rather than a hobby or personal skill. DanceStudioOwner.com was like this: it started as a small website offering insights on how to successfully manage a studio. Revolution Dancewear, a giant of the dance apparel business, acquired it less than four years later.

The _all-you-can-eat library model_ is also based on offering information. Through this model, customers gain unlimited access to a wide selection of content. Although it's impossible for them to ever consume all the content, the appeal lies in the unlimited choice.

Online music providers are the best illustrations of this model and its recent rapid expansion. Consumers are now eschewing MP3s in favor of online music streaming services like Spotify.

Though this model may seem more suited to media giants, it's still possible for smaller organizations to build a library of content. New Masters Academy does this. It was launched by a 28-year-old who wanted to offer people more democratized access to art classes. New Masters Academy began by offering recorded sessions with art instructors who were paid later by subscriptions.

### 8. Use subscriptions to offer elite customers priority access. 

Have you ever seen people go to the front of the line at airport security because they're part of an elite group of flyers? They benefit from the _front-of-the-line model_ of subscriptions. In front-of-the-line models, a company offers priority access to something for people who subscribe.

This sort of offer is particularly attractive to people who can't solve certain problems on their own. Salesforce.com, for example, provides support to its customers, but prioritizes them by the kind of package they've bought. Customers who bought the most expensive package get a response within fifteen minutes.

This model doesn't just apply to software companies — it can be used in any industry where customers might want to jump a service queue.

Thriveworks operates in this way. Life coaches or counseling offices might take days or even weeks to call you back, but Thriveworks provides patients with a counselor they can meet within 24 hours to discuss anything they'd like, in exchange for a $99 subscription fee.

When you set up a front-of-the-line subscription model, remember to think about the way your customers are flagged and dealt with. You might want to have a special counter for welcoming them, or a particular phone line they can use. United Airlines has an exclusive phone line for its Mileage Plus customers.

So treat your front-of-the-line customers well, but don't forget that your company should have a good reputation for basic customer service, too! The front-of-the-line customers should just be getting extra treatment and services.

Even if you charge a small fee for prioritizing certain customers, your front-of-the-line subscription service can become a big source of incoming money if that group of customers grows.

### 9. You need to learn a new set of metrics for subscriptions. 

If you've ever subscribed to something yourself, you'll know that while there are plenty of benefits, subscriptions can be complicated. As a subscription _provider_, one of your biggest challenges is learning how to measure progress.

Traditional metrics don't work with subscriptions. The _profit-and-loss statement_ (P&L), which displays the amount of money a business retains after its expenses are paid, will become misleading. In a year, the clients' payments will be a fraction of the original amount (1/12), while the expenses will stay the same.

Since the P&L's bottom line is meaningless with subscriptions, you'll need a new indicator to assess the company's viability threshold.

Experienced venture capitalists have come up with a rule of thumb for subscription businesses: customers should be worth three times what it cost to win them.

Naturally, this means it can be hard to find funding while you're waiting for your revenue from subscriptions to build up. Fortunately, there are a few options here.

First, you can reinvest all your profits into the subscription business. That's how the software company Basecamp started out. It earned money from various web-design projects before transitioning to subscriptions.

You can also raise capital outside the company. Don't give up too much capital or power, however. Outside investors may bring insight and experience, but be aware that venture capital also comes with risks. One study found that in more than half of venture-backed company buyouts, the founder was left with nothing.

The third option is to simply charge up front for a whole year of subscription. The elite investment club TIGER 21 charges its members for the whole year in January.

### 10. Use tricks to convince potential customers who may be reluctant to buy subscriptions. 

So if you've assessed your situation and decided you want to incorporate a subscription model into your business plan, what's the next step? How do you acquire new customers and limit their loss?

Subscribing to something is a serious commitment, but there are a number of tricks you can use to convince even the most reluctant potential customers.

First, use comparisons. You won't convince many people by telling them they'll save ten percent on your product, but what if it's _ten times_ cheaper? New Masters Academy's $29 monthly subscription fee looks much better than the $600 — $800 cost of a private art course.

You can also set an ultimatum, maybe by making your subscription a unique offer. John Warrillow did this when his consultancy company began offering subscriptions rather than billing each assignment. Clients only subscribed when the original billing service was completely off the table.

Another good strategy is to offer people a trial period so they can experience your service firsthand and understand its value. The private ski club Osler Bluff offers a trial membership for $2,500, so their clients can decide if they want to pay for the full membership of $57,000 per year.

It's also necessary to reduce the number of customers who _churn_, or cancel their subscriptions.

One way to do this is make your product essential to your customers. The more subscribers interact with your product, the less likely they are to quit. Dollar Shave Club customers, for instance, use their products every day — unless they're growing a beard!

Charging up front can also work. It might prompt your customers to use your service more, which could make them appreciate it more in turn. Wild Apricot, a software company, learned this after giving ten percent off to customers who prepaid. And you know what? Those customers were much more committed to using their products.

### 11. Final summary 

The key message in this book:

**Big and small companies alike are discovering the benefits of implementing subscriptions. They're convenient for your clients and they make things easier for you, too. So figure out which model works for your business, do your research and attract the right customers. You have to understand and use subscriptions if you want to keep up.**

Actionable advice:

**Let go of traditional business practices.**

Subscriptions have become popular because of some big shifts in the business world, like the increased importance of data collection and the rise of the access generation. So don't get bogged down by older practices. Don't rely on traditional metrics either — you'll need a new method of analysis for this new kind of service.

**Suggested further reading:** ** _Built to Sell_** **by John Warrillow**

_Built to Sell_ details key strategies for growing a small service company and preparing the business for a future sale. These blinks illustrate these insights by telling the story of Alex Stapleton, owner of a marketing agency, and his advice-giving friend Ted Gordon, who is a successful entrepreneur.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### John Warrillow

John Warrillow is the bestselling author of _Built to Sell: Creating a Business That Can Thrive Without You_. He's also the founder of _The Value Builder System_, a company aimed at helping businesses improve their value.

