---
id: 57592319496d1c0003e86257
slug: the-beauty-myth-en
published_date: 2016-06-13T00:00:00.000+00:00
author: Naomi Wolf
title: The Beauty Myth
subtitle: How Images of Beauty Are Used Against Women
main_color: E62E4C
text_color: CC2943
---

# The Beauty Myth

_How Images of Beauty Are Used Against Women_

**Naomi Wolf**

_The Beauty Myth_ (1991) will help you understand the anxiety that surrounds women, and the cult of beauty that dominates our society today. It identifies the patriarchal and economic forces that shape the unfair expectations and norms that women face. Learn how we can be better equipped to build a less biased future.

---
### 1. What’s in it for me? Understand the social construct called beauty. 

Beauty, the poets tell us, is in the eye of the beholder. But have you ever stopped to consider this phrase? What is viewed as beautiful has changed throughout history, and it's different in different societies, which suggests beauty really is judged based on norms and ideals.

Okay, so, big deal. That doesn't really change anything, right?

Not so fast. The myth of beauty enforces normativity, especially for women: in our society, it's a myth that perpetuates the subjugation of women to men. It drives women to purchase makeup and clothes, to undergo plastic surgery and even practice starvation. Let's explore the history of the beauty myth and learn what a stronghold it still has on society today.

In these blinks, you'll discover

  * that women's joining the workforce enforced the beauty myth;

  * why the beauty myth functions like a cult; and

  * how the beauty myth helped create the income gap between men and women.

### 2. Current standards of beauty are a political means of controlling women and maintaining the patriarchy. 

The women's rights movement has made significant progress in the past few decades. But even though women have gained legal and reproductive rights, as well as more freedom to pursue higher education and enter previously male-dominated professions, there has also been some backlash.

As women have made legal and economic strides over the past 50 years, we've also seen an increase in detrimental portrayals of female beauty. These images are being used with growing efficiency to work against the progress women have made, creating what is known as a _beauty myth_.

The beauty myth claims that there is an elusive quality called "beauty" — a quality that women should do everything in their power to pursue, achieve and maintain.

The myth enforces certain views. For instance, that beauty is an inherently good and beneficial quality and that, without it, women are worthless and invisible.

Consider women who are at the top of their professions. Even with all their achievements and success, the pressure to look good doesn't cease. If anything, women with more power and status are scrutinized _more_ and the pressure to look beautiful only gets worse.

The beauty myth is the final mechanism of cultural ideology with the power to control women, women who have otherwise been liberated to freely pursue their desires.

Before the Industrial Revolution, the social value of women was measured largely by their work within the domestic sphere. Their lives took shape around attributes like work aptitude, physical strength and fertility — not beauty.

With industrialization came increasing freedom for women. To maintain the patriarchy, a new, subtler and more deceptive method for undermining women's freedom and power was needed.

This is what the beauty myth does: it keeps women trapped, self-destructively competing with themselves and each other. And since images and standards of beauty change, the identity of women is constantly vulnerable and in need of external validation.

In the next blink, we'll see how this myth plays out in the workplace.

### 3. The workplace nurtures and sustains the beauty myth. 

After World War II, only 32.8 percent of US women were employed. By 1984, the percentage had jumped to 53.4, an increase that threatened the male-dominated power structures. To the patriarchy, women constituted a huge and destabilizing group demanding equal opportunities.

To counteract this influx, a discriminatory system came into existence, singling out appearance as a woman's most desirable trait.

The author refers to this system as _Professional Beauty Qualification_ (PBQ).

Of course, PBQ professions — actresses, models, dancers — have been around for a long time. In 1972, for instance, a former Playboy Club waitress named Margarita St. Cross brought a case against _Playboy_ for firing her on the grounds that she had "lost her _Bunny Image_."

One look at the Bunny Image ranking system, and you can see how ridiculous the standards are:

  1. Flawless (face, figure, grooming)

  2. An exceptionally beautiful girl

  3. Marginal (is aging or has developed a correctable appearance problem)

  4. Has lost Bunny Image (either through aging or an uncorrectable appearance problem)

The New York State Human Rights Appeal Board ruled in favor of _Playboy_ and their right to determine whether a woman was pretty enough to work. Following the _Playboy_ ruling, an abundance of other cases emerged and the PBQ system spread into other workplaces.

Working women began to face a no-win situation of discrimination.

For instance, women could be fired for being _too_ attractive. The ruling in Barnes v. Costle determined that women, if their appearance was deemed to provoke unwelcome advances, could legally be blamed for inviting sexual harassment.

On the other hand, we have Hopkins v. Price-Waterhouse: Ms Hopkins was denied partnership at the accounting firm because she needed to "walk, talk, and dress more femininely, and wear makeup."

As you can see, the convoluted rules of the PBQ system make it impossible for women to win.

> _"All women are Bunnies."_ \- Gloria Steinem, American feminist, journalist and activist

### 4. The beauty myth is fueled by advertising and beauty services and products. 

Do ads or spreads featuring "beautiful" models in magazines ever make you feel bad about your self-image? If so, the ads are working.

After all, advertisers determine the content of most publications, including women's magazines.

These magazines, as well as most mainstream media, perpetuate images and standards of beauty that work in favor of the companies that pay for ads. This ensures that the companies continue to make a profit and, therefore, continue to buy ad space.

In the 1950s and '60s, the popular image was of the happily dedicated and busy housewife, accompanied by ads for housewares. These ads glorified homemaking to make it feel like a profession, keeping housewives content so they wouldn't feel the need to pursue a career.

As Betty Friedan described in her trailblazing book _The Feminist Mystique,_ American housewives' lack of identity and purpose was turned into a massive business by companies and advertisers selling household items.

Today, magazines have turned to "beauty" products to earn their money.

When it became clear that women had become a permanent part of the workforce, magazines and advertisers shifted gears. Instead of selling housewares, they started using images of "perfection" to sell "beauty products" like cosmetics, diet supplements and anti-aging creams, all in an effort to embrace the beauty myth.

Unable to live up to the impossible standards in these images, women feel inferior, which only leads them to buy more of the beauty-myth products.

The results speak for themselves: between 1968 and 1972, the number of diet related articles rose by 70 percent. And in 1989, magazines earned $650 million in revenue from cosmetic companies, while former favorites like housewares yielded only one-tenth of that amount.

In 1991, the beauty myth was fueling some booming industries: the diet industry, which was making $33 billion per year; the cosmetics industry, making $20 billion; the cosmetic surgery industry, making $300 million; and the pornography industry, making $7 billion.

It's clear that "beauty" is a strong currency. As we'll see in the next blink, however, it derives its real power from keeping our society patriarchal.

> _"No picture of a woman goes unretouched."_ \- Bob Ciano, former art director of _Life_ magazine

### 5. The beauty myth has dire consequences for women – physically, mentally and economically. 

Perhaps you're wondering just how harmful the beauty myth can be. For women, it can have devastating effects. For the oppressors, however, it can bring great benefits.

In the workplace, the beauty myth does more than perpetuate discrimination against women based on appearance; it also reinforces a double standard, ensuring that women are underpaid and that men make the big money.

This discrepancy in pay occurs in every profession.

In 1991, on average, male lawyers between the ages of 25 and 34 earned $27,563, while women made $20,573. In that same year, retail salesmen earned $24,002 and saleswomen made only $7,479. Even female hairdressers earn $7,603 less than their male counterparts.

And today, women in the United States still earn, on average, $0.79 for every dollar a man makes.

On top of the discrimination, modern women's pursuit of "beauty" has a huge impact on their overall health.

It's no secret that the beauty myth defines the perfect woman as being remarkably skinny. As a result, anorexia rates have soared, creating a serious epidemic that affects five to ten percent of all American women. Being fueled by the beauty myth, it's no wonder that between 90 and 95 percent of American anorexics and bulimics are women.

The unhealthy effects of the beauty myth also keep women isolated and in competition with each other.

Often, beautiful women are viewed with envy and mistrust by others, and you can hear the negativity in the language women use to describe each other, such as: "Oh she's so perfect, I could just die!" Or, "Don't you hate women who can eat like that?" And don't forget the famous advertising tag line, "Don't hate me because I'm beautiful."

This lack of female solidarity also affects women economically.

Fifty percent of working women in the United Kingdom are not unionized, but that number jumps up to 86 percent in the United States. Many economists believe that unions are the solution for the "feminization of poverty," since women who are unionized earn, on average, 30 percent more than those who aren't.

Imagine what women could achieve if, instead of working against one another, they worked together, free from the confines of the beauty myth.

> _"Men look at women. Women watch themselves being looked at. This determines not only the relations of men to women, but the relation of women to themselves."_ \- John Berger

### 6. Shattering the beauty myth is a process of awakening: we change our perception of beauty and women. 

So, how can we liberate ourselves from the oppressive beauty myth?

The first step toward combating the beauty myth is becoming aware of all the forces that keep it alive.

The beauty myth is sort of like a cult — alluring, persuasive and manipulative. And it isn't afraid to use guilt and victim-blaming to keep you in its grasp.

Consider the language of cosmetics: Clinique's "scientific" table lists four categories for problematic facial lines, which are: Very Many, Several, Few and Very Few. So, according to Clinique, it isn't even possible to have no problematic facial lines. In the eyes of the cosmetic world, being a woman means you're guilty of the "original sin" of imperfection.

Clarins cosmetic products also use victim-blaming and guilt tactics when they tell us, "Even the most innocent expressions — including squinting, blinking and smiling — take a toll." And, "Do you laugh, cry, frown, worry, speak?"

And then there's the food industry. Their ads often accuse women of doing something "naughty" when they eat "forbidden" foods.

This is a tactic that Wheat Thins capitalizes on with their slogan, "You don't have to hate yourself in the morning," which suggests that hating yourself for eating something is a normal part of a woman's life.

The second step is to reject the beauty myth altogether.

This doesn't mean that women have to stop dressing up or wearing makeup. Women should have the option to wear what they want and to decide this on their own terms, not out of fear of falling outside the norm.

This also means that we need to strengthen female solidarity and relationships.

Instead of reserving their charm for men, women should feel free to direct it toward fellow women and compliment each other.

Most importantly, the beauty myth needs to be recognized for the destructive, discriminatory, double standard that it is. Only then will we defeat it.

To help with this, we must listen to women's voices and expand our culture to include their true perspectives and wishes.

> _"It is far more difficult to murder a phantom than a reality."_ \- Virginia Woolf

### 7. Final summary 

The key message in this book:

**"Beauty" is a construct that benefits men politically, economically and culturally. This myth of attainable beauty is kept alive so that women remain weak, vulnerable and divided. The way to overcome this is by becoming aware of the beauty myth and fighting against its degrading standards of beauty.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _We Should All Be Feminists_** **by Chimamanda Ngozi Adichie**

In _We Should All Be Feminists_ (2014), Chimamanda Ngozi Adichie expands on her much admired TEDx talk to address our deepest misconceptions about feminism. By masterfully interweaving personal anecdotes, philosophy and her talent for prose, she explains how men and women are far from being equal, how women are systematically discriminated against and what we can do about it.
---

### Naomi Wolf

Naomi Wolf graduated from Yale University and continued her studies as a Rhodes scholar at Oxford University. Her other books include _Promiscuities_, _Fire with Fire_ and _Misconceptions_, all of which were international best sellers.

