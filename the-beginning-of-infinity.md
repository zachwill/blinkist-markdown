---
id: 565c7e8c9906750007000051
slug: the-beginning-of-infinity-en
published_date: 2015-12-02T00:00:00.000+00:00
author: David Deutsch
title: The Beginning of Infinity
subtitle: Explanations That Transform the World
main_color: 195E7C
text_color: 195E7C
---

# The Beginning of Infinity

_Explanations That Transform the World_

**David Deutsch**

Everyday, we benefit from huge advances in both scientific theory and practice. What triggered this progress? In _The Beginning of Infinity_ (2011) — a journey through the fundamental fields of science and philosophy — physicist David Deutsch argues that all progress results from one single human activity: the quest for explanations. Human creativity opens up limitless opportunities for progress, making knowledge the "beginning of infinity."

---
### 1. What’s in it for me? Be taken aback by the power of knowledge. 

A good explanation can make all the puzzle pieces fall into place, but that's not all it can do. Explanations don't just enlighten us; they are also at the base of human knowledge — and of human progress. In other words, explanations make this world!

In these blinks, you'll be taken on a journey through science and culture to learn how knowledge is formed and improved, and how this process underpins potentially boundless progress — also known as _the beginning of infinity_. 

You'll also learn

  * why your senses can't tell you if the sun will rise tomorrow;

  * what a good joke has in common with a gene; and

  * how _memes_ decide a culture.

### 2. Knowledge is not only derived from experience, but also from theories. 

No human has ever stepped on the surface of a star, let alone visited its core. And yet scientists know a lot about what's happening deep inside stars, light years away. 

How come?

Because experiencing is not the only way of knowing.

_Empiricism_ is a theory that claims we derive all our knowledge from sensory experience — but as you'll see, this is wrong.

Empiricists imagine that our mind is like a blank piece of paper on which sensory experiences are written. This would mean that we are the passive recipients of knowledge, not its creators.

But contrary to the empiricist view, our knowledge can't be based on individual observations alone.

For instance, even though you've seen the sun rise repeatedly, you don't base your knowledge that it will rise tomorrow solely on these observations. If you did, on a cloudy day you'd assume the sun was not rising because you couldn't see it!

Moreover, appearances can be deceptive. For example, the Earth looks and feels as if it is immobile, even though it is really rotating.

So although experience is essential to science, it is not the _source_ from which knowledge is derived. If it was, our knowledge of the stars, for example, would be limited to what we learn from gazing at the night skies.

Instead, the real source of our knowledge is _theory_ and _conjecture_.

Consider the stars again: the fact that at their core is an energy source equivalent to billions of power plants is something we can't see. We know it only from theory.

Such scientific theories are derived from conjectures — guesswork and speculation — which can be _tested_ by experience in the form of observations and experiments.

### 3. Genes and ideas both spread by replicating themselves. 

Human brains and DNA molecules have many functions, but one of the most fundamental is the storage of information. And what's more, the types of information they store — ideas and genes respectively — tend to spread by replicating themselves.

What does this mean?

At a fundamental level, evolution is based on the idea of a _replicator_ — meaning anything that contributes to itself being copied.

For instance, a gene that confers the ability to digest a certain type of food causes an organism to stay healthy. Hence it increases the organism's chance of surviving and having offspring who inherit and spread copies of the gene.

But _ideas_ can be replicators too. 

For example, a good joke is a replicator. When a joke is lodged in a person's mind, it's likely that person will tell it to others, thus passing a replica of it into their minds. Nearly all long-lasting ideas such as language, scientific theories or religious beliefs are replicators in this sense.

So, both human knowledge and biological adaptations are created and spread through replication. However, there's an important difference between biological adaptations and human knowledge: knowledge is only replicated when _expressed,_ while genes can be replicated when _dormant._

If an idea is just stored passively in our minds and never expressed in behavior or speech, it cannot be picked up by anyone else. But genes can pass from one generation to the next without ever being expressed in behavior.

Why?

Because in sexual reproduction, a random selection of genes is chosen from both parents, including genes that no longer create active behavior. These genes can be replicated for many generations, before either dying out, or resurfacing in expressed behavior.

### 4. Cultures are static or dynamic depending on the memes that define them. 

People like telling each other amusing stories — some of them true, some invented. Some of these stories contain ideas that are interesting enough to be retold to _other_ people, some of whom retell them, in turn, over the course of generations. This process of creating and receiving ideas is fundamental to cultures.

In fact, cultures consist of long-lived ideas that spread from person to person. These are called _memes_, and they cause their holders to behave alike in certain ways. Examples of groups of memes that define behavior are the shared values of a nation, the ability to communicate in a certain language and the appreciation of a given musical style.

So different groups of memes mean different cultures.

But memes make cultures differ in another way. Depending on whether their memes change or not, cultures are either _static_ or _dynamic_. 

In static cultures, memes either don't change or the change is too slow to be noticed. Static cultures have customs, laws or taboos that prevent their memes from changing. They enforce the maintenance of the existing memes, forbid the enactment of variants and suppress criticism of the status quo.

They are dominated by memes that survive by disabling their holders' critical faculties, which gives them their name: _anti-rational memes_. North Korea requires unconditional obedience to a supreme ruler — a good example of an unchanging, anti-rational meme. 

In contrast, dynamic societies are dominated by _rational memes_, which are created by rational and critical thought, like the scientific method.

People in many developed and liberal societies can modify memes through critical thought and pass on modified variants. For example, critical thinking and scientific examination could one day produce a new meme regarding the origin of the universe, and this would change the prevalent culture. This makes them dynamic societies. In fact, Western society is a bit of an anomaly; it's the only society that's both rapidly changing and long-lived.

### 5. Systems of knowledge develop incrementally until they jump to universality. 

If you were reading these blinks instead of listening to them, think about the alphabet that permits you to derive meaning from what you see on your screen. What can this _writing system_ teach us about the creation of knowledge?

Systems of knowledge, like writing systems, develop incrementally. 

To understand this, consider the earliest writing system; it used stylized pictures — called pictograms — to represent words. A symbol like "◌" might stand for "sun," and "↥" for "tree." 

Later, when adding new words to the writing system, scribes found it easier to add new rules rather than new pictograms.

For example, they added rules such as, if a word sounds like two words in a row, it can be represented by the pictograms of those two words. If English were written in pictograms, that would mean the word "treason" written as "↥◌" (tree-sun) could be added to the writing system. 

Adding such a rule improves the writing system because more words can be expressed without the need for more pictograms.

But one particular improvement in a system of knowledge can lead to a sudden increase of explanatory power: _the jump to universality_.

Let's consider the writing system again so we can grasp this.

Pictograms can only represent words by combining symbols that are limited to specific meanings. But one day someone invented a way of combining symbols into _any meaning._

The alphabet.

The genius of the alphabet is that it can cover not only every word but every _possible_ word in its language. Such an improvement causes a sudden increase of reach in the system and makes possible the creation of new forms of knowledge.

### 6. Rational and democratic decision-making in groups has been proven impossible. 

In 1951, the economist Kenneth Arrow did something radical.

He proved a theorem that appears to deny the very possibility of representative democracy. His argument was that _joint decision-making,_ the process of multiple people agreeing on something for a group, is necessarily irrational.

To prove his theorem, Arrow laid down five elementary principles he deemed necessary for groups to make rational and democratic decisions about their preferences in a way that reflects the "will of the people." 

For example, one of these is the "no-dictator" principle. It says that the preference of one individual cannot be taken as the preference of the group.

So if you have a preference for, say, hamburgers, but everyone else prefers pizza, then the group's preference cannot be for hamburgers. 

Another principle is that if the members of the group have identical preferences, then the group must have those preferences too. If every individual wants pizza, the group's preference must be for pizza. 

However, Arrow proved that it is in fact impossible to define the group's preferences in a way that satisfies all five principles, making joint decision-making necessarily irrational. This finding actually won him a Nobel Prize!

Common sense says that when we make a decision, we weigh the evidence that each option presents. But there is something fundamentally wrong with this perception of decision-making: it conceives of decision-making as a process of selecting from existing options. However, at the heart of decision-making is the creation of _new_ options and the abandonment or modification of existing ones. So it's not a matter of mere rational comparison.

### 7. Because we cannot know what we will discover in the future, optimists believe anything is possible. 

In 1789, the economist Thomas Malthus argued that humanity would stop progressing once and for all in the nineteenth century. 

He calculated that the exponentially growing population at the time was reaching the limit of the planet's capacity to produce food. 

But Malthus was misled by a fundamental fact of the human condition: _We do not yet know what we have not yet discovered_. And as we know, we've made plenty of progress since Malthus's day.

To understand this, consider the following story: A prisoner has been sentenced to death by a tyrannical king, but gains a stay of execution by promising to teach the king's favorite horse to talk within a year. 

A fellow prisoner asks him: "What possessed you to make such a bargain?"

The prisoner replies: "A lot can happen in a year. The horse might die. The king might die. I might die. Or I might get the horse to talk!"

This story is a picture of optimism, because although the prisoner doesn't know how to teach the horse to talk at that point in time, he hopes he might be able to in the future. He doesn't believe there are limitations to the creation of knowledge.

In the end, he may or may not discover how to teach the horse to talk. But he may discover something else instead. He may learn how to make the horse look like it's talking, or think of something that would make the king even happier than a talking horse.

> _"The harm that can flow from any innovation that does not destroy the growth of knowledge is always finite; the good can be unlimited."_

### 8. Knowledge makes humanity significant in the cosmic scheme of things. 

The famous physicist Stephen Hawking once said that humans are "just a chemical scum on the surface of a typical planet that's in orbit round a typical star on the outskirts of a typical galaxy." In short, there is nothing significant about humans on a cosmic scale. 

But what about the way humans transformed the Earth by means of their knowledge?

The Earth's biosphere was incapable of supporting wide-scale human life, but people supported themselves by creating knowledge.

We are accustomed to thinking of the Earth as hospitable. However, nearly the whole of the Earth's biosphere in its primeval state was incapable of keeping an unprotected human alive for long. 

Even the Great Rift Valley in Eastern Africa, where our species evolved, wasn't particularly hospitable: it had no safe water supply, comfortable shelter, or medical equipment, and was infested with parasites, predators and bacteria.

But by using our ability to create knowledge, humans invented science and technology, and converted the Earth into a hospitable place.

And it's this amazing knowledge that makes humanity significant in the cosmic scheme of things.

When you look at cosmic phenomena, such as immense stellar explosions, our knowledge seems quite insignificant. But think about the long run: one day human knowledge might help us colonize other solar systems, and even learn how to control powerful physical processes such as stellar explosions — one day we might even choose to prevent a stellar explosion if it threatens a planet colonized by humans. This would very concretely mean exerting our influence on a cosmic level.

And from the immense scale of solar systems, we'll now jump down to the smallest known scale: the subatomic.

> _"It is true that we are on a (somewhat) typical planet of a typical star in a typical galaxy. But we are far from typical of the matter in the universe."_

### 9. According to quantum theory, the physical world comprises multiple universes with differing histories. 

A "doppelgänger" or "double" of a person is a frequent theme in science fiction. _Star Trek_ once featured a doppelgänger story where the starship's teleportation device malfunctioned, creating a copy of the whole crew. 

But in quantum theory — the modern physical explanation of particles on an atomic and subatomic scale — the idea of a doppelgänger is not fictitious, but real. 

In fact, according to quantum theory, the physical world is a _multiverse_.

Traditional physics imagines the universe as a singular entity. According to quantum physics, however, the universe is much more complicated — it's a multiverse, that is, an infinite number of universes, each of which is initially identical to all the others. In fact, there is an infinite number of universes, which are all initially perfectly identical.

But even though the multiple universes start out identical, and are subject to the same physical laws, they can have different histories.

How?

Let's use the _Star Trek_ example as a starting point: imagine just two identical universes, each including a version of the starship, its crew and its transporter — and the whole of space.

Now, when one of the transporters malfunctions, there could be a small voltage surge in the transported objects, and this could occur _in one of the universes and not the other_.

The voltage surge that happens in one of the two universes could cause some of the neurons in a passenger's brain to misfire. As a result, in that universe, that passenger spills a cup of intergalactic coffee on another passenger. They then have a shared experience which they do not have in the other universe, which can lead to anything — maybe a romance or a new course of history. In fact, quantum theory states that every individual universe in the multiverse will have its own unique history.

> The "many-universes interpretation" of quantum theory which the author supports remains a minority view among physicists.

### 10. Scientific discoveries aren’t final truths because future findings are unforeseeable. 

In 1965, the prominent physicist Richard Feynman wrote: "We are lucky to live in an age in which we are still making discoveries. It is like the discovery of America — you only discover it once." But Feynman made a common reasoning error: He forgot that the very concept of a "law" of nature is not carved in stone.

In other words, scientific findings aren't ultimate truths. 

In the late nineteenth century, the classical physical laws discovered by Newton were conceived as ultimate, fundamental truths, with little left to be discovered.

For example, in 1884, the physicist Albert Michelson predicted that "the possibility . . . of new discoveries is exceedingly remote . . . Our future discoveries must be looked for in the sixth place of decimals." Although physicists were already aware of some theoretical problems concerning Newton's laws, it was thought that they could be solved with a few minor tweaks in some equations. 

But this was not the case, as Einstein's revolutionary explanations would soon show.

Even today, scientific theories aren't absolute truths, because our best theories don't get along.

Physics currently has two interpretations of the world: quantum theory, which explains the behavior of particles on an atomic and subatomic scale, and Einstein's general theory of relativity, which (roughly put) describes gravity. But since the two theories operate on vastly different scales, they are radically incompatible at the moment.

And we cannot know which scientific discovery will clear up this inconsistency, because future scientific discoveries are not yet imaginable.

For example, Michelson would not even have put the expansion of the universe or the existence of parallel universes on a list of "exceedingly remotely" possible discoveries. He simply couldn't conceive of them altogether — because the future is unforeseeable. 

Nevertheless, wondering what is beyond the present leads to conjecture and speculation. This is vital for scientific advancement, since every unforeseeable idea starts as a spark of speculation.

### 11. The Final summary 

The key message in this book:

**Human progress depends on the creation of knowledge. We derive our knowledge not only from sensory experiences, but from the quest for critical thought, conjecture and good explanations. There is no necessary limitation to human progress and the creation of knowledge. This boundlessness of the creation of knowledge is the "beginning of infinity" that makes humans significant in the cosmic scheme of things.**

Actionable advice:

**To overcome problems, be an optimist!**

The author believes in two fundamental principles:

  * Problems are inevitable.

  * Problems are soluble.

These two principles encapsulate the optimistic way of thinking. Being an optimist does not mean denying that problems will occur. Rather, it means believing that all existing evils are due to insufficient knowledge. With the right knowledge, all problems — and thus all evils — can be overcome.

This open-mindedness itself leads to new ways of creating knowledge, so if you want to make progress, start looking at the bright side of life!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### David Deutsch

David Deutsch is a highly influential researcher in the field of quantum physics. He lives and works in Oxford where he has been a visiting professor of physics since 1999. In 1998, Deutsch was awarded the Institute of Physics' Paul Dirac Prize and Medal, one of the top awards for theoretical physics.

