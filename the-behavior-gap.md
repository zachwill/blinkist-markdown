---
id: 5432753b38353300084f0000
slug: the-behavior-gap-en
published_date: 2014-10-07T00:00:00.000+00:00
author: Carl Richards
title: The Behavior Gap
subtitle: Simple Ways to Stop Doing Dumb Things with Money
main_color: 299FCB
text_color: 1F7899
---

# The Behavior Gap

_Simple Ways to Stop Doing Dumb Things with Money_

**Carl Richards**

These blinks will help you to make better decisions about money. By defining the _behavior gap_ — the rift between what we _should do_ and what we _actually do_ — and explaining how to close it, Richards offers guidelines for making smart financial decisions for life.

---
### 1. What’s in it for me? Learn how to make smart financial decisions (and stop worrying about money so much). 

We're saturated with financial advice and information — so why do so many people keep losing money and winding up in dire straits? In his work as a financial planner, author Carl Richards saw the effects of this firsthand. Everywhere he looked, people were making lousy decisions and suffering as a consequence. This is what drove Richards to write _The Behavior Gap_. These blinks show you how tuning out all the noise and focusing on your own personal goals will help you invest wisely, secure your financial future and live a happier life.

In addition, you'll learn

  * why you shouldn't always follow _The Economist_ 's advice;

  * why trying to predict the future could damage your finances; and

  * why you should talk to your kids about money.

### 2. The behavior gap arises whenever there’s a rift between what we should do and what we actually do. 

Do you ever find yourself doing something that you shouldn't, like having a third piece of cake even though you want to lose weight?

This is called the _behavior gap_ : a rift between what we know we _should_ do, and what we _actually_ do.

This phenomenon is tied to our natural desire to avoid pain and seek pleasure, which leads us to act irrationally.

One effect is that we often adopt a _herd mentality_, where we behave exactly like those around us without pausing to make our own decisions. This is because we assume it's safer to do what everyone else is doing.

This was seen in the 1990s dot-com boom. It seemed everyone was reaping huge profits on investments, so even ordinary people began borrowing money against their home equity, and ended up investing over $44 billion in stocks. Then when the NASDAQ shed half its value, people lost their investments and found themselves deeply in debt. This kind of reasoning can be found at the root of any market bubble.

These situations produce a behavior gap: Instead of acting rationally, people get excited and let emotions guide their choices. To close the gap, think beyond today's trends and remember surprising past events: the dot-com crash, the housing bubble, the 2008 debt crisis.

All these incidents underscore the importance of investing carefully, without getting caught up in boom-and-bust cycles. Thinking about them will help you close the behavior gap by making the consequences of herd mentality more concrete.

Also, avoid overconfidence. In the 1990s, Long-Term Capital, a hedge fund run by Nobel Prize winners, learned this lesson. Blinded by its own pedigree, the company's board of directors was positive the firm could never lose more than $35 million per day. But one day, to their horror, the firm lost $553 million. Eventually, the company had to be bailed out by the Federal Reserve.

We can't predict the future; we can only control our own behavior.

### 3. Instead of hunting for the world’s best investment, make financial decisions based on personal goals. 

Have you ever wondered, _What's the world's best investment?_ If so, you're asking the wrong question.

Here's why: Planning for your future entails a delicate balance between living for today and saving for later.

In this balancing act of spending and saving, the _investment_ — that is, an asset expected to earn value over time — is only a small part of your financial future.

In addition to seeking out high return investments, you could also secure your finances by saving more money, retiring later or pursuing a second career.

In fact, since it's impossible to predict how investments will fare over time, no single investment can categorically be "the best." Instead of chasing this mythical unicorn, judge financial products by how well they help you reach your goals.

No single investment can ever satisfy everyone, so ask yourself: What is the best investment _for me_? The answer will depend on personal factors — your goals, your personality, your existing holdings, even your credit card.

For example, if you want to pay for your kids' college in the future, you could open an educational account or invest in mutual funds at least 18 years before college enrollment. You should aim to save around $240,000.

Of course, the college fund won't be any use if your kids aren't accepted somewhere, so don't forget to help them with their homework!

As you can see, wise investments are pieces of a larger puzzle.

Some people don't take this approach. Instead, they deal with their portfolio like baseball card collectors, buying shares based on magazine recommendations, with no cohesive strategy.

Don't make this mistake. You're not a collector, you're an investor. Judge financial products according to how they help you reach your goals. If they don't help you, don't invest in them — end of story.

This way, your financial decisions will be based on principles, not emotions.

> _"If we are going to make good financial decisions, we have to make them in the broader context of our lives."_

### 4. Ignore most of the generic financial advice you hear in the media; instead, make your own financial decisions. 

Most of us love giving advice. It makes us feel important and wise.

But that's exactly why most advice is fundamentally useless.

Harsh words? Not really. Most of the advice we hear is generic, which means it doesn't necessarily apply to our specific situation.

This is especially true when it comes to financial advice. A good financial plan is personal and unique, so what's right for you might be a catastrophe for someone else.

Of course, plenty of people don't know that — or care. Millions read _The Economist_ thinking they can beat the market by following the expert advice within.

But generic advice can fail us. Here's an example: People who want to lose weight are told to work out everyday. That might be good advice for many, but it won't help someone with a heart condition.

Additionally, most "expert" advisors are prone to conflicts of interest.

Most of us intuitively understand this. That's why we would never expect a salesman at a Toyota dealership to tell us that Honda makes the greatest cars in the world.

So there are many reasons to be skeptical. The good news is, once you can detect unsound and irrelevant advice, you can then find advice that's actually useful to you.

And to that end, you can stop chasing the perfect investment or portfolio. Instead, you can design your portfolio to suit your current understanding of the market and adapt when things change.

Think about it: You wouldn't want someone telling you what to wear each morning. Instead, you trust your own sense of style and your own knowledge of your life!

Similarly, there's no reason to try to become the next Warren Buffett. You are you.

With that in mind: In the following blink, you'll find out why exploring your own goals, strengths and weaknesses is essential for your financial strategy.

### 5. Money can only buy so much happiness; beyond that, it’s merely a tool for pursuing personal goals. 

Why do so many of us strive for financial security? We have good reasons: We just want to be happy and provide a good life for our loved ones.

Happiness is very important, but we should always try to remember that true happiness is more about expectations and desire than how much we earn.

There's proof: A recent study by Nobel Prize winner Daniel Kahneman and Princeton professor Angus Deaton found that happiness is correlated to income only up to an individual earnings level of $75,000 per year. After that threshold, those with higher incomes don't have commensurately higher levels of emotional well-being.

This means most people are overemphasizing their finances. If money can only buy you so much happiness, then beyond this point, money is merely _a tool_ for pursuing things that bring true satisfaction to your life.

For example, money can allow you to travel and see the world, buy a huge garden, or support a cause you deeply believe in.

So in order to make truly wise financial decisions, you have to know yourself. That way, you can align the use of your assets with your personal values.

David Brooks, op-ed columnist at the _New York Times_, is a proponent of this approach — but on a broader scale. He has argued that instead of tracking economic trends, governments should focus more on measuring whether social programs and institutions directly increase overall happiness levels.

We can apply this principle on an individual level: Stop worrying _so much_ about money. Instead, pursue your personal goals. This will allow you to make the best financial choices.

To do this, look inward rather than outward.

In the upcoming blinks, you'll learn to cut through all the noise about the economy and maintain focus.

### 6. Mainstream media propagates a herd mentality, which can distance you from your real financial goals. 

We all want to be informed and knowledgeable about financial markets and the economy. But listening to mainstream media can actually lead you astray.

That's because mainstream media often propagates a herd mentality: making us feel safer in numbers. So when we follow the media's advice, we take comfort in knowing that we're behaving the same way as everyone else.

Intuitively, we might understand that just because everyone's doing it, doesn't mean it's right — but we humans are anxious creatures so the herd is very appealing.

While understandable, following the crowd can be costly. Take the _subprime mortgage crisis_ of 2006, when housing prices fell 30 percent on average. Worse yet, stock market prices fell 57 percent from their October 2007 peak. These collapses resulted from the fact that both assets had been massively overvalued, because everyone was "following the herd" and buying them, driving their prices up.

So be wise: Don't follow the herd. Ignore (almost) everything the media says.

The media does, of course, report some sound information; however, the sheer quantity makes it impossible to sift through and find what really matters. This saturation makes it very easy to follow the wrong advice.

Examples of media misinformation abound. Here's just one: In a 2010 article, _The Economist_ happily declared, "America's Back!" The feature provided a long list of positive indications of economic recovery. But as it turned out, their predictions were wrong — the economy stumbled again.

Luckily, there's a way to deal with all this financial misinformation: Be more aware of what's driving all the news and rumors you encounter in mainstream media and, more generally, worry less about money. When you clear your mind of all that clutter and tune out the noise, you can get back in touch with your real goals.

That way, when you have to think about money, you'll think in terms of what matters to you.

### 7. Stay on track with your financial goals by preparing for surprises and making adjustments when necessary. 

Do you know the difference between _planning_ and _formulating a_ _plan_?

There's a distinction: A _plan_ makes predictive assumptions about the future. But no matter how hard we try to predict the future, we'll be wrong.

On the other hand, _planning_ isn't about predicting the future, it's about preparing for it. Planning involves making decisions based on what is happening now, not assuming you know what's to come. And it's especially valuable because it leaves room for surprises.

For example: In 2004 the author bought a $575,000 house, for which the previous owner had paid $400,000. When the value of the house rose to $1 million in 2007, the author expected house prices to continue rising, so he didn't try to sell it to make a profit. But then when the market crashed, the author found himself owing more than the house was worth.

Unfortunately, in a turbulent financial market, we will all face surprises like these some day. So how can you deal with them?

Just make constant course corrections to stay on track with your financial goals, adjusting your planning as circumstances change.

Think about it this way: Imagine you're piloting a cross-country flight from Los Angeles to Miami. If you're just a smidgen off-course when you take off, it'll still seem like you're flying in the right direction, but you'll eventually end up in Maine. Most pilots wouldn't let that happen.

Similarly, staying on course for financial goals requires you to pay attention to where you are and to make small adjustments to your trajectory.

To do that, it's best to think in shorter time frames: Focus on the next three years, not the next 15. That way, you're working towards a larger goal without getting overwhelmed by the big picture.

### 8. Rational investing requires you to be honest, to get rid of stagnating assets and only invest according to personal goals. 

Emotions make us human, but there's a high cost to allowing your feelings to shape your financial decisions. So how can you avoid letting emotions dictate your finances?

First of all, be honest with yourself: Great investments require just as much luck as skill. Sometimes you make smart decisions that lead to successful investments. Other times, you're just lucky.

Another way to avoid making emotional decisions is the _Overnight Test_ : Ask yourself, if someone sold all your investments overnight, which stocks would you buy again tomorrow?

In this theoretical situation, you might let a few investments go. We often hang on to investments just because we're used to them, the same way we might hang on to a loveless relationship just because we're comfortable with it. This is where the _Overnight Test_ helps us cut through our pre-existing biases and re-evaluate our investments objectively.

So if you're unsure of whether to keep an investment, ask yourself: _Would I buy it again tomorrow?_ If the answer is _no_, toss it.

A third way to ensure your emotions aren't guiding your finances: Always ensure the decision could potentially help you reach personal goals.

Some people don't do this. Instead, they buy a stock because it's "the next big thing." But investing in something just because it's trendy isn't really a financial choice. Here's what is: Building savings for retirement.

So think about what you actually want for yourself and your family in the future. Then ask yourself whether the potential investment plays a clear role in the portfolio you need to help meet these financial goals.

Of course, its always possible that the rational decision won't have the expected results. Because even if you take these precautions, investing always forces us to make decisions in the midst of uncertainty.

The only thing you can do: Make the best possible decision today and take responsibility for the potential results.

> _"Investment decisions should be based on what we know, not how we feel."_

### 9. Take responsibility for your decisions, but remember: You’re never fully in control of outcomes. 

Children are taught to admit when they've done something wrong. But do we adults follow our own advice?

If you're not used to taking responsibility for your actions, it can be hard. But it's essential to improving your finances.

For example, an acquaintance of the author believed that buying expensive clothes and driving a BMW would make him more successful. But there was nothing beneath the surface of all this shiny stuff, and the cost of keeping up these appearances drove him into serious debt.

Instead of faking success, he should have diligently worked his way up the career ladder. That's hard work, and it requires patience and discipline. But to make a positive change, hard work is necessary.

Another key to improving your finances is to keep examining your assumptions about what really impacts your financial situation. Too often we focus on irrelevant things.

For example, one day the author took a long drive to get cheap gas. Midway through, he did some mental calculations: He realized that if he saved ten cents a gallon on a 20-gallon tank, the savings would add up to $2. That figure barely covered the amount of gas he used on the drive. If he had realized this sooner, he would have saved himself a long drive.

As you can see, even financially solid-seeming plans like buying cheaper gas require closer scrutiny.

However, although a thorough examination of your decisions helps ensure positive outcomes, you also need to realize you're never fully in control.

For example, if you send your daughter to Harvard, you're making a huge investment in her financial future.

But after graduation, your daughter might decide to teach underprivileged kids at an urban high school for $19,000 a year. For her, this is the right outcome. However, it's not exactly what you expected when you decided to invest in her education.

As these kinds of surprises can await any of us, it's better to accept them beforehand.

### 10. Talking about money with friends and family can be uncomfortable, but it’s also important. 

Many people feel that it's still taboo to talk openly about money with friends and relatives. Since everyone has a different background and attitude toward it, money can sometimes feel like a dangerous topic. But if you're committed to making sound financial decisions, it's too important to skirt around.

So if we want to avoid misunderstandings, we need to consider our own biases and strive to find a common language.

Here's a misunderstanding around money that arose in the author's life: He and his wife were talking to a friend about the friend's recent kitchen renovation, and as he detailed all the work that had been done, the author automatically started calculating the costs of the project. Later, he told his wife that they couldn't afford their own renovation.

His wife was confused, because she'd just been chatting about someone else's kitchen renovation — not planning to embark on an identical project. And this after 15 years of marriage!

If it gets this confusing for husbands and wives to talk about money, it can be even more upsetting to discuss the subject with our kids. But don't let your discomfort stop you.

Another example from the author's life: His friend wanted to be honest with her children about finances. Whenever the children demanded an unreasonable purchase, she told her children, "We can't afford that."

Far from being awkward, this tactic actually resulted in a fruitful family discussion: Their 14-year-old son asked how poor the family really was, on a scale from one to ten. As his parents quickly realized, the boy wasn't disappointed in his family's financial situation, but rather concerned about their well-being.

So overall, talking about money with your family and friends can be truly beneficial, even when it's uncomfortable. And in the end, these conversations can be treated in a frank, simple way.

### 11. When it comes to making financial choices, keep it simple and be boring. 

Have you ever heard the phrase, "Simplicity is the ultimate form of sophistication?" It also applies to your finances.

Simple solutions can bring us closer to our goals, while complexity slows us down.

For example, people spend $40 billion a year on weight loss programs even though the do-it-yourself approach — consuming fewer calories and exercising — is the most effective. Regardless, people get lost in complex commercial diet plans and products, and often don't reach their goals. If they did, weight loss wouldn't be a $40 billion industry.

This is where simplicity comes in: Always keep it simple. Instead of buying expensive products, go running every morning.

This principle can also guide your finances. Don't buy into the latest fad, spend your money carefully and save gradually. This approach is called _slow and steady capital_.

Building slow and steady capital requires you to avoid the temptations of instant gratification.

In fact, delaying gratification is hugely important to long-term success overall. In the 1950s, Stanford launched a decade-long study that looked at people's ability to delay gratification, and found that people who were better able to postpone the fulfillment of their desires experienced greater long-term success than those who gave in to temptation.

So when it comes to accumulating your slow and steady capital, don't look for huge short-term gains; instead, takes steps to accumulate long-term wealth slowly. This will help you avoid big losses and ensure financial security.

Sounds boring? Good. _Be boring in your financial habits._

Spend less than you earn. Set money aside. Pay your debt. Steer clear of large losses.

This conservative approach to money can lead to big results over time.

For example, the author once met a gentleman who had turned a relatively small inheritance into an impressive fortune. The author asked this man about the secret to his amazing financial success, and he replied, confused, that there was no secret. He had just avoided extravagant purchases and instead bought "boring things," paying for them gradually like everyone else.

> _"We often resist simple solutions because they require us to change our behavior."_

### 12. Final summary 

The key message in this book:

**Sometimes there's a rift between what we should do and what we actually do. This is called the behavior gap and it often prevents us from making smart financial decisions. We have to take steps to avoid this — and close the behavior gap — by being honest with ourselves and thoroughly examining the factors involved in our decisions. The keys to sustainable investments are keeping things simple and taking responsibility for our actions.**

Actionable advice:

**Take time each month to reflect on your personal goals and make sure your investments line up accordingly.**

The best way to meet your investment goals is to align them with your personal goals. That way your financial decisions and your personal values will support, rather than work against each other.

Let's say you're trying to decide whether to invest your savings in a bank account or in stock. If one of your personal goals is to buy a motorcycle in six months, then the bank account is probably a better option, since it's less volatile than stocks and also easier to withdraw money from.

But if your personal goal is to save up for your children's college fund, and you only intend to withdraw money in 20 years time, then stocks are probably a better option, as they will generate higher profits.

**Suggested** **further** **reading:** ** _Thinking, Fast and Slow_** **by Daniel Kahneman**

Daniel Kahneman's _Thinking, Fast and Slow_ — a recapitulation of the decades of research that led to his winning the Nobel Prize — explains his contributions to our current understanding of psychology and behavioral economics. Thanks to him, we now have a better understanding of how decisions are made, why certain judgment errors are so common and how we can improve ourselves.
---

### Carl Richards

Certified financial planner Carl Richards is director of investor education for BAM ALLIANCE, a group of more than 130 independent wealth management advisors. He has contributed to the _New York Times_ and been featured on Marketplace Money, Oprah.com and Forbes.com.

