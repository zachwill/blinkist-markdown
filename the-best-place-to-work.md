---
id: 54cf8c1e323335000a4a0000
slug: the-best-place-to-work-en
published_date: 2015-02-04T00:00:00.000+00:00
author: Ron Friedman
title: The Best Place to Work
subtitle: The Art and Science of Creating an Extraordinary Workplace
main_color: AA272E
text_color: AA272E
---

# The Best Place to Work

_The Art and Science of Creating an Extraordinary Workplace_

**Ron Friedman**

If you want to design the best possible workplace, improve employee satisfaction and ultimately motivate your staff to deliver better results, look no further. _The Best Place to Work_ will guide you through the process of improving your working environment, leading to more enthusiastic and productive employees.

---
### 1. What’s in it for me? Find out how a better workplace leads to more enthusiastic people and a more successful organization. 

Do you like your job? If you do, then you may be in the minority. According to one study, 84 percent of all US workers feel disengaged, and that percentage has stayed more or less the same since 2002.

Many companies don't seem to be doing anything about this. Very few seriously consider ways to improve their working environment. They should.

It is clear that a happy and more enthusiastic workforce will perform better, and customers will be happier as a result. These blinks help explain how businesses — both large and small — can make the positive changes to their environments, allowing staff to break the trend and start enjoying work.

In these blinks you'll discover

  * how to exploit the power of napping;

  * why staff hate sitting with their back to the room; and

  * why winning an Oscar can prolong your life.

### 2. To boost workplace productivity, managers should promote schedule flexibility and embrace failure. 

What's the best way for you as a workplace leader to manage the productivity of your staff? According to traditional thinking, productivity is best achieved when employees spend nearly all their time working as hard as possible.

But although this old-fashioned approach was well-suited to the mechanical work that proliferated during the Industrial Revolution, today it's seriously outdated.

Instead, modern society's most successful managers eschew the rigid nine-to-five formula, encouraging staff to carve out flexible work schedules. In other words, they let employees work when and where they feel most comfortable.

For instance, old-fashioned managers might view napping as a waste of time. But in fact, napping is a great way for staff to keep their energy levels up for longer. (Of course, we're talking about a 20-minute power-nap — not a long, deep sleep.)

So to that end, a manager wanting to promote productivity might skip the new espresso machine and buy a cozy futon instead.

Similarly, managers should create firm boundaries between work and home life. For example, Volkswagen and Daimler cut off e-mail access after business hours, allowing employees to recharge at night and be more productive during the day.

Practices like these are sure to boost productivity, but if you really want to ensure that your employees achieve at the highest level, you also have to embrace failure.

After all, no one can work at their full potential if they feel pressured to produce flawless work hour after hour. That's simply too much stress!

A stress-free environment isn't the only advantage of embracing failure: This attitude will also promote experimentation, leading to a culture of innovation.

To really understand the link between failure and innovation, consider this: The greatest inventions in human history followed repeated failure. Edison, for example, spent years trying and failing before he finally managed to invent the lightbulb.

> _"One thing we can predict with some certainty is that the Failure CV of most high achievers tends to be surprisingly lengthy."_

### 3. Establishing an atmosphere of play will encourage creativity and give your staff an incentive to work hard. 

Imagine this scenario: Your boss expects you to come up with a creative solution by the end of the day. Working under pressure, you try as hard as you can, and yet the solution you're looking for couldn't be farther away.

Unfortunately, this is an all-too-common workplace dilemma. Because after all, it's hard to be creative when you're under pressure. That's why as a manager you should establish an environment that promotes play; this will take the pressure off and relax your staff, ultimately encouraging creativity.

What's the best way to do this? Well, you can start by freeing up your staff's timetables and carving out space for games and athletic activity: For instance, your company can organize a running club or a football team. You can install some game consoles or simply buy a few board games.

Although there are benefits to bringing actual games into the office, it's also advantageous to incorporate the logic of play into your working processes.

Think about it like this: One big reason games are so compelling is that they provide instant feedback. For instance, when you successfully play a video game, you're instantly rewarded: You pass to ever-more advanced levels and secure high scores.

Everyone likes this kind of recognition because, simply put, it makes us feel good about ourselves. And in fact, it can even make us healthier. Because believe it or not, studies have shown that Oscar-winning actors outlive mere nominees by an average of four years!

Bearing that in mind, make sure that your staff's hard work is noticed and recognized. And don't rely on that old standby, the Employee of the Month Award, because recognizing one person at a time is too limited. Instead, take advantage of social media to call out great work and recognize each and every success, whenever it happens!

### 4. Transform your workplace and improve productivity by promoting positive emotions. 

Try to recall a childhood experience you loved wholeheartedly. Maybe it was a vacation at the beach, maybe it was a sporting event. Whatever it was, you probably get a warm feeling when you remember it. And it probably makes you feel more energetic.

Well, companies should promote these kinds of positive emotions in their staff, as that will lead to increased productivity. There are many ways to do this: You can hold office events during holidays like Easter or Halloween, or you can organize sports teams and book clubs.

These events needn't be costly or grandiose. In fact, instead of focusing on extravagance, simply focus on frequency. That way, you can provide staff with _many_ positive emotions and memories, large or small — ultimately creating a positive environment.

Think it's impossible to create a pleasant atmosphere at a boring old office? Well, think again: _Any_ environment can be a positive one with just a few simple tweaks.

For example, casinos are depressing because they aim to enable patrons to lose as much money as possible. However, if you swap out money for valueless plastic chips and offer free drinks, a casino becomes a fun, pleasant place.

Of course, people can't be happy all the time, because we all have a dark side. But that's not a bad thing — embrace it!

Think about it like this: When positive emotions are overly emphasized and negative emotions are disregarded, people stop thinking critically and become less receptive to substantive feedback. As a result, the whole company becomes more error-prone.

It's also worth noting that not every department requires the same level of positivity to promote good results. Because although an uplifting atmosphere benefits salespeople — who need positive energy to build successful relationships with clients — it would not necessarily benefit accountants, who need to be more meticulous with their work.

### 5. Cultivating friendship in the workplace is a powerful way to motivate your employees. 

Although it's common knowledge that effective teamwork is an essential component of any company's success, many managers mistakenly believe that friendship isn't necessarily conducive to this kind of collaboration. But in fact, truly high-performing teams are made up of friends, not mere acquaintances.

And there are many reasons that's true. Friends are far less likely to let each other down, which means they support each other and deliver on their promises. From a business perspective, that means friendship-centric teams are less likely to fail.

Friendship also negates loneliness. This is not a trivial point, as loneliness can have devastating effects on your body and your work. But unfortunately, loneliness is too common in many offices when employees feel alienated from their colleagues — the emotional equivalent of working in a cubicle. It does not promote happiness and productivity among staff.

So what can you do to mitigate the negative effects of loneliness and cultivate friendship among your employees? Well, it helps to understand how people become friends in the first place, and that is through physical proximity, familiarity and by reaffirming similarity. In other words, people become friends when they work together on the same tasks again and again.

You can make sure your workplace is conducive to this process by eliminating cubicles and any other barriers. Additionally, you can organize group activities like obstacle races or running competitions. Physical activities like these promote friendship by producing a rush of adrenaline, a powerful hormone that makes it easier to connect with those around you.

Ultimately, an environment that supports friendship will help generate a communal feeling of pride in the organization itself. And when your team is proud of the work they do, they'll be motivated to produce better work.

> _"Pride also influences people's willingness to tell their friends and neighbors about where they work."_

### 6. Boost productivity by designing a comfortable, pleasant office that your employees love. 

Although looks aren't everything, humans are visual creatures. So to that end, what does your workplace look like? Do you feel happy when you look around? Or do you feel depressed every time you step into the office?

These are important questions to ask, but few companies bother. Most managers assume that a good employee can do the same good work anywhere.

But this kind of thinking is seriously misguided. After all, just think how much more inspired you'd feel at the Sistine Chapel — with its majestic frescos and high ceilings — than in an office cubicle, with its dark, oppressive barriers.

But unfortunately, too many offices are negligent on this point, settling for drab, badly designed spaces that depress workers and damage productivity.

However, there are a few easy guidelines you can follow to improve your office space for the benefit of your employees.

First, allow human evolution to guide your design process. So for instance, today we don't like exposing our backs because the same position would have left our distant ancestors vulnerable to a predator attack. Therefore, to make your staff more comfortable, don't make them sit with the back of their chairs facing the open room.

Also, humans have an deep-seated love of nature, so make sure your office has lots of windows and that your staff can see outside.

It's also worth noting that no matter how well you design your office, the best way to make your staff comfortable is by letting them choose their own workplace. Optimally, employees should have a choice of working in three different rooms, as well as the option to work from home. In other words, create lots of alternatives and cede control to your employees.

After all, creating a comfortable, pleasant environment will boost workplace productivity — so do your best to create an office people love.

> _"Sometimes, the less management tries to control, the better the results."_

### 7. Create an environment of trust by being a good listener and setting a good example. 

No matter how decisive and visionary you are as a leader, you also need to be a good communicator if you want to carry out your plans and ensure that your workplace operates smoothly.

So how do you ensure that you're a good communicator? The key is to listen and be nonjudgmental.

And when it comes to listening, you need to be an _active listener_, which is all about showing the other person that you're listening carefully, in order to build trust. To achieve this, maintain direct eye contact and make sure that your body language shows that you're paying attention. Also, make sure you only speak half as much as the other person.

And to stay nonjudgmental, keep your emotions in check. That way, any feedback you give won't be taken personally.

Being an active, nonjudgmental listener is an important part of being an effective leader, but it's not the only thing: You also have to set a good example for your employees.

After all, people naturally mimic their superiors. When we see success in other people, we automatically try to emulate it in order to become more successful ourselves.

In other words, the way you conduct yourself as a leader will trickle down to employees. And in fact, studies have shown that CEOs who are naturally warm lead more cohesive teams; similarly, curious-minded CEOs tend to have flexible and risk-tolerant teams.

This means you should take steps to ensure that you're promoting good behavior. For example, behave calmly and never get angry, even when confronted with frustrating challenges. This will create an environment of trust, leading to a calm and happy staff.

So now that we've discussed all the factors that make a workplace great, let's talk about what you need to do to find the right people.

> _"People only change when they are accepted for who they are."_

### 8. Make personality and performance central to your recruiting process. 

How would you hire a new employee? Well, most of us create a job ad, wait for applications and then interview the candidates who have the best technical skills and experience.

That's the traditional recruiting process, and it's pretty popular. However, it's hardly ideal.

After all, this process only targets job-seekers, narrowing your search to candidates who are unemployed or unhappy with their current jobs.

Another problem with the traditional recruiting process is that it's hard to get an accurate sense of candidates' actual technical expertise from the interview alone. Because believe it or not, studies show that 81 percent of interviewees lie.

Furthermore, it's difficult to accurately gauge expertise because you, the interviewer, are probably biased. Even when you focus on evaluating technical skills, subconsciously you're making your decision according to other factors. For instance, people who are tall and have deep voices are more likely to get hired, because interviewers connect these qualities with good health.

Since there are many problems with the traditional recruiting process, you need to change your approach. We recommend getting creative with interviews and also giving your employees a larger role in the process.

So when you need to make a new hire, rally your employees. Let them ask around and tap their own networks. This way, you stand a better chance of finding great employees outside the basic applicant pool.

And then, once you have your candidates, introduce creativity into the interview process. To avoid the lies, avoid questions relating to technical expertise. Instead, ask questions to determine whether candidates would fit in with your team.

For example, while interviewed for a job, the author's wife had to answer the question, "If you could be any fabric, what fabric would you be?" She answered velvet, because that fabric is warm on the inside and smooth on the outside. Ultimately, this answer showed her interviewer that she was a team player — and she got the job!

> _"The best candidates are not searching the internet for a job. More often than not, they are perfectly satisfied in their current position."_

### 9. Final summary 

The key message in this book:

**Every workplace can improve — and as a leader, you can change yours for the better. All it takes is a willingness to prioritize your employees' happiness and well-being. You have to be seriously committed in order to create a more positive environment, but it will be well worth it, leading to increased productivity and better results.**

Actionable advice:

**Encourage your staff to take breaks, naps and sometimes work from home.**

Although some people fear that these practices only waste time, creating a more flexible approach to scheduling and workspace will lead to better productivity.

**Suggested further reading:** ** _Drive_** **by Daniel Pink**

In _Drive_, Daniel Pink describes the characteristics of extrinsic and intrinsic motivation. He reveals that many companies rely on extrinsic motivation, even though this is often counterproductive. The book explains clearly how we can best motivate ourselves and others by understanding intrinsic motivation.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Ron Friedman

Ron Friedman is an award-winning psychologist and consultant who studies motivation. _The Best Place to Work_ is his bestselling first book, and has featured in global media from NPR to the _Guardian_.

