---
id: 53319f403862370007fe0000
slug: the-better-angels-of-our-nature-en
published_date: 2014-03-25T15:00:00.000+00:00
author: Steven Pinker
title: The Better Angels of Our Nature
subtitle: Why Violence Has Declined
main_color: D9372D
text_color: B22D25
---

# The Better Angels of Our Nature

_Why Violence Has Declined_

**Steven Pinker**

_The_ _Better_ _Angels_ _of_ _Our_ _Nature_ (2012) takes a close look at the history of violence in human society, explaining both our motivations to use violence on certain occasions and the factors that increasingly restrain us from using it — and how these factors have resulted in massive reductions in violence.

---
### 1. What’s in it for me? Discover why humanity is more peaceful than ever, even though most of us fantasize about murder. 

Have you ever fantasized about killing someone? If you're like most people, you probably have — even in the last year.

All humans have the capacity for violence as this has been built into our genes as a crude way of getting what we want. Yet, despite popular belief, as humanity develops, it actually uses less and less violence.

In these blinks you'll find out exactly why that is.

First you'll discover our "inner demons" — the five primal motivators behind our propensity for violence. For example, you'll find out why exacting revenge on someone might feel like taking cocaine.

Second, you'll discover the better angels of our nature — four other motivators that encourage us to refrain from violence. For instance, you'll understand why an average teenager today would have been a genius in 1920 and far less violent because of it.

Finally, you'll see how six major historical shifts reduced violence drastically by accommodating the better angels of our nature while curbing the sway of our inner demons. Along the way, you'll come to understand, for example, why headbutting a cat to death is no longer considered wholesome, fun entertainment.

Though we're evidently living in the most peaceful time ever, this is not to argue that violence has been eradicated from human life — self-congratulations are not in order quite yet. Rather, we must continue our efforts to reduce violence by nurturing and promoting the better angels of our nature.

### 2. Predation: Violence is a simple, natural way to get what we want, but it’s also risky and crude. 

In the first five blinks, let's look at what motivates humans to commit violence — our so-called "inner demons."

The first of these is rooted in the fact that violence is a simple way to gain an evolutionary advantage.

Through natural selection, all organisms have evolved to compete with one another for the survival of their genes.

In this competition, organisms are sometimes forced to oppose one another — for instance, when resources are limited or there's a short supply of potential mates. Using physical force is an effective way of securing those resources, so organisms prone to violence do have an advantage. This kind of instrumental violence is called _predation_ and it's a pragmatic means to getting what we want.

For humans, too, this violent tendency is natural and commonplace. In fact, it can even be seen in young children: studies have shown that the most violent stage of development is toddlerhood, when children express behavior like biting, kicking and hitting.

The tendency persists as we mature, too: in a survey of university students, up to 90 percent of men and 80 percent of women admitted that they'd fantasized about killing someone in the past year.

This violent streak seems to have a neurological basis. Studies have found that artificially stimulating a certain area of the brain known as the "rage circuit" triggers feelings of aggression.

But even though we have a natural propensity for violence, from an evolutionary standpoint our instincts for violence need to be kept in check, because acting on them is often a bad idea:

Harming our kin, for example, would be counterproductive because they have inherited our genes.

Also, violence is risky, because even if a particular organism wins a battle, it may still suffer injuries which could lower its chances of surviving and passing on its genes in the long run.

For this reason, humans tend to employ violence selectively.

### 3. Dominance: Violence can also be used to bolster one’s social position, making access to resources or mates easier. 

As we've seen, violence tends to be a risky strategy for getting what you want, because even if an organism wins, there's a high risk of injury, which could diminish its chances of passing on its genes.

It makes sense then that most social species would try to avoid unnecessary battle. Many of them accomplish this by establishing _dominance hierarchies_. These are hierarchies whose order is based on who _would_ win if a confrontation were to occur, and their main benefit is that fights, and the injuries that result from them, can be completely avoided.

In social species, this hierarchy generally favors males, because it's based on relative size and strength.

The male's position in the hierarchy determines his access to females, with the males at the top being able to impregnate as many as possible. This means that males are incentivized to use violence not only to secure resources, but also to gain a higher status and position in the hierarchy.

Furthermore, remnants of this phenomenon can be seen in humans too. Not only is it still mainly men who pursue and compete for women, but men also tend to value prestige and status more highly than women do.

However, when hunter-gatherer tribes were established around 1 or 1.5 million years ago, females became less concerned with being impregnated by the dominant male, and more interested in the male's loyalty and ability to provide for his family.

This is because at this time, males were responsible for hunting, while the females stayed behind to care for their families.

Because raising a family means feeding them, evolution led females to prefer males who were loyal to their family and returned from their hunting trips with food for everyone, rather than males who merely tried to impregnate as many females as possible. This development lead to our being generous and loyal to our loved ones, a crucial part of our evolutionary success.

### 4. The desire for revenge fuels violence around the world and it probably evolved as a deterrent. 

The desire for revenge — something most of us have felt — appears to be a major and almost ubiquitous motivator of violence.

In this context, revenge doesn't refer just to _immediate_ retaliation, but also to people's tendency to hold grudges and seek retaliation over the long term.

And it's a near-universal phenomenon: almost all of the cultures of the world explicitly advocate blood vengeance ("an eye for an eye, a tooth for a tooth"), and up to 20 percent of the world's homicides have revenge as their motive.

Even if we're just fantasizing about murdering someone, vengeance is usually a factor: in the previously mentioned survey revealing how frequently university students fantasized about killing, most of their fantasies were ones of revenge.

But why is the desire for revenge so common? One reason is simply that it feels good. In studies of lab rats, taking revenge has been found to create a pleasurable response in the brain, somewhat similar to that caused by cocaine or chocolate.

Nevertheless, our desire for revenge is baffling. The feeling lingers on even after any battle has been fought or any insult flung, and usually we don't gain any material value from exacting revenge on someone.

So why has it evolved at all? Perhaps because, from an evolutionary standpoint, revenge can be understood as a deterrent. If a would-be attacker is aware that revenge is possible, he'll have to consider the long-term costs of following through with his attack. These considerations might dissuade him.

It's also noteworthy that, as we'll see later, our desire for revenge can be motivated by our moral understanding — our sense of what's right and what's wrong. According to this understanding, we tend to think that it's not only good actions which should be reciprocated, but bad ones too.

### 5. Sadism is a rare and perplexing phenomenon which seems to be an acquired taste. 

The next driver of human violence we'll look at is not only bizarre but also monstrous: _sadism_ — deliberately inflicting pain on someone just because the sight of the suffering it causes is experienced as enjoyable.

Nowadays sadism is fortunately very rare in its pure form, and we're rather more likely to encounter sadism coupled with other motivators for violence, such as revenge.

In the past, however, it appears that sadism was far more common. For example, in ancient Rome and in medieval times it wasn't unusual for prisoners to be tortured to death in huge stadiums solely for the pleasure of a public audience. Still, even back then, sadism was far less common than the other motivators for violence.

Looking more closely at the phenomenon, sadism appears to be something of an "acquired taste." It takes some getting used to, but once that happens it can even become addictive.

How can this be? Normally, seeing another creature suffer for no reason feels inherently objectionable. However, it seems that once any initial revulsion is conquered, sadistic behavior can become highly pleasurable, even addictive; just think of serial killers, for example.

It's difficult for us to be certain of the role of sadism in human evolution, but we might speculate that it has developed as an instinct designed to take effect only in extremely violent conditions. In such a brutal environment, having a sadistic instinct could potentially aid our survival, and the surrounding violence might then trigger this instinct.

In any case, we can only be thankful that this perplexing and horrific phenomenon is uncommon today.

### 6. Ideology: Well-meaning efforts to bring about a better world have spawned horrific amounts of bloodshed. 

Let's now take a look at the final motivator for violence: _ideology_ — a shared belief that some greater good is so utopian that achieving it warrants limitless violence.

In this sense, ideological violence is similar to predation: it is instrumental to our getting what we want. The difference is that the motive for ideological violence transcends the motives of the individuals of a group: Ideology strives to create a better world.

Unfortunately, it appears that humans have numerous traits which make us likely to be influenced by violent ideologies. For example:

  * Humans like to separate the world's people into _ingroups_ (those groups we belong to) and _outgroups_ (groups to which we don't belong). We then tend to develop hostility toward outgroups and affinity with our ingroups.

  * We also tend to fall victim to _polarization_. When people with similar ideas are placed together in a group, their ideas tend to become even more closely aligned and, ultimately, more extreme. This leads often to the creation of potentially dangerous ideologies.

  * We're often swayed by _groupthink_, which refers to group members' unwillingness to question the group's ideas for fear of creating disharmony. This means even very toxic ideologies may go unchallenged.

  * Our desire for s _ocial conformity_ means those who don't conform to an ideology are punished, which can lead to their being converted by means of intimidation or violence.

Sadly, traits like these make it possible for a single person to engender dangerous ideologies.

How?

For example: one person suggests that by eliminating a certain demonized outgroup, a vaunted ingroup group might attain some utopian ideal. A small group of individuals agree and spread the idea, punishing those who disagree. Intimidated, larger groups of people begin to endorse the idea, while skeptics are isolated or silenced. Followers are so adept at rationalizing their actions that they easily act against their better judgment, and so the means of suppression grow increasingly severe.

Many atrocities have been committed because of such ideologies, as can be seen in examples like the Holocaust and the purges of Stalin.

All of these motivators for violence paint a grim picture of human nature. Happily, though, as you'll see in the next blinks, we do also have tendencies that lead us away from violence.

### 7. Empathy evolved for us to take care of our families, but we can also learn to have empathy for other groups. 

The first better angel of our nature that decreases our tendency toward violence is _empathy_ : having an altruistic concern for the well-being of others _._

But why did we evolve into empathic beings?

From an evolutionary standpoint, empathy developed initially to encourage people to care for their kin, especially their children.

One result of this is that we tend to feel empathic toward those creatures that have the facial symmetry of human children. For example, baby seals evoke more empathy than rats, and baby-faced convicts are given more lenient prison sentences than other criminals.

Later in our evolution, empathy also helped us to develop reciprocal and altruistic relationships with non-relatives, allowing us to engage in mutually beneficial favors, like trading resources.

It should be pointed out, however, that human empathy is not aroused by mere cold and precise exchange of resources; rather, a looser, more enduring relationship must be established, much like that between family members.

In addition to being able to generate empathy for those close to them, there's evidence to suggest that it's possible for humans to learn how to extend their empathy to others.

For instance, we might be able to cultivate empathy by drawing attention to the similarities between different groups of people. This is because, as studies show, due to the familial roots of empathy, we do tend to act empathically toward people with whom we have things in common.

Furthermore, we can enhance our empathy by learning more about others, particularly if we learn about them through their own unique perspectives on the world.

However, empathy also has a dark side, as it can lead to unfairness: In one study, researchers told subjects about Sheri, a ten-year-old girl who was suffering from a serious illness. As a result of the empathy they had for Sheri, the subjects were willing to let her skip the line for medical treatment, even though this meant passing those who were in more urgent need of it.

### 8. Self-control helps us resist violent impulses and it can be strengthened or weakened by practice. 

Everyone knows the depiction of temptation and conscience as an angel and a demon on a person's shoulders, where the person tries to resist being tempted by the demon's words.

In fact, this analogy to actual human behavior may be more apt than you might first think: studies suggest that our impulses and our resistance to them do come from entirely different parts of the brain.

We use the _prefrontal cortex_ — which governs complex decision making and rational thinking — to make rational decisions about things like rewards. But sometimes another, more primitive part of the brain called the _limbic system_ kicks in. For example, when an immediate reward is offered, the limbic system sends an impulse to seize the opportunity, whatever the cost — even when this makes no rational sense.

The prefrontal cortex must control such impulses so we can make a rational decision. In essence, it is the neurological basis for our _self-control,_ or willpower.

Moreover, data indicates that without such self-control, we're more apt to behave violently: brain scans of violent people show shrunken prefrontal cortexes.

Fortunately studies also suggest that willpower can be strengthened through practice.

Subjects who were instructed to engage in various self-control exercises — like logging everything they ate, or enrolling in physical exercise regimens — demonstrated an increased resistance to the depletion of willpower and greater overall self-control.

This applies to violence, too: historically, when governments outlawed violence citizens had to learn to keep their aggressive impulses in check. This means they had to train and strengthen their willpower, which resulted in them becoming less violent.

However, it seems that willpower can also be _weakened_ through, for example, poor nutrition. Studies indicate that low blood sugar levels negatively affect our willpower.

This would suggest that, as the overall state of nutrition of the world is improving, violence should also decline as self-control gets stronger.

### 9. Our sense of morality can encourage or discourage violence, but thankfully it’s tilting toward the latter. 

Humanity's _sense of morality_ plays an interesting dual role in violent behavior:

Over the years, certain people like homosexuals and heretics have been killed in the name of what's considered right and wrong. But on the other hand, our moral sense has also led to a reduction in violence, through, for example, our striving for racial equality.

Such a duality can be best understood by looking at four themes or _modes_ around which much of our moral behavior revolves:

_Communal sharing_ : Within a group or community, it is good to be loyal to the group, sharing resources and ideas. This theme promotes peace and cooperation, but unfortunately can also motivate the persecution of non-conformers, in some cases leading even to genocidal ideologies.

_Authority ranking_ : Authorities deserve the obedience and loyalty of the people they govern, and, in return, authorities are obliged to protect their servants. This can advance the welfare of humanity — for example, when a queen ensures the safety of her subjects. But, on the other hand, this same mode can justify more destructive hierarchies too, such as slavery.

_Equality matching_, which refers to the reciprocity of humans — in other words, performing and returning favors. This mode encourages peace because it has increased global trade, discouraging nations from conflicts that can damage their economies. However, equality matching is also a _cause_ of violence, as it spurs revenge.

Finally, there's _market pricing_, which concerns the impersonal transactions so prevalent today with, for example, retailers and service providers. Most people would say it's wrong to use these services — for instance, dining at a restaurant — without paying. Like equality matching, market pricing can help maximize overall well-being but can also drive amoral profit-seeking through, for instance, slavery and human trafficking.

While our moral sense can both direct us toward and steer us away from violence, thankfully it tilts increasingly toward the latter as increased mobility and the spread of new ideas are negating the downsides of many modes.

### 10. Reason drives us to seek peace and nonviolence, and our faculty for reason is increasing. 

You might think that reason isn't inherently good on its own. After all, it can be directed toward, for example, determining how to best use violence to acquire what one wants.

But in fact reason only develops in creatures who, due to natural selection, value their own life and welfare. To that end, they are more likely to use reason to pursue a world of peace, not one of violence.

This is achieved in various ways.

First, politicians use reason in their attempts to maximize peace and minimize violence. In the Cuban Missile Crisis, for example, Kennedy and Khrushchev intentionally used reason to reframe their problem as a shared trap that they both had to escape without losing face. This put them on the same side.

Also, reason is an effective method for debunking superstitions which incite violence — like the belief that witches cast spells and must be burned at the stake.

Finally, reason is necessarily impersonal, so it enables us to look at things impartially, rather than from subjective and, sometimes, self-serving perspectives. Instead of thinking in terms of ourselves, reason helps us to think in terms of "X's and Y's," which serves the common good.

Fortunately our capacity for reason actually seems to be increasing.

In fact, according to the Flynn Effect, discovered by philosopher James Flynn, IQ scores rise by an average of three points per decade. This means that if an average teenager today were to time travel back to 1910, she would be smarter than 98 percent of her contemporaries.

The increase is particularly evident in abstract reasoning, which is essential to the ability to see things from another's perspective. Reasons for this shift include increased schooling and a greater emphasis on critical thinking.

Happily, better reasoning seems to lead to a reduction in violence. Indeed, research indicates that people who are good reasoners are less violent, more cooperative and hold views on politics and the economy that are more compatible with peace.

Now that we understand how the better angels of our nature help to keep us from committing violence, let's look at the six major historical trends of the past that have enhanced the impact of our better angels, while restricting that of our inner demons.

### 11. The pacification process began 5,000 years ago, when states began to monopolize violence. 

Some 5,000 years ago, people began to transition from small, nomadic hunter-gatherer societies to agricultural societies with formal governments. This _pacification process_ brought with it a reduction in violence.

Why?

Contrary to the popular belief that nomadic tribes were "peaceful stewards of the land," they were in fact violent. For a long time, this violence was downplayed by anthropologists, because they remembered all too well the atrocities — like slavery — that were committed against primitive tribes when they were considered "savages." Instead, anthropologists began to highlight the tribes' peaceful nature, even when confronted with contrary evidence.

But in fact, hunter-gatherers battled over hunting spots and watering holes; they kidnapped women from neighboring tribes and they even took violent revenge on others to deter them from future attacks. Indeed, statistics suggest that approximately 15 percent of deaths in hunter-gatherer tribes are violent, in contrast to one percent in state societies.

From this chaos, the rise of agricultural states with formal governments brought a _reduction_ in violence because the governments _monopolized_ it.

In such a society, only the state itself may exact violence on others, while individuals are punished for doing so.

Initially, the reason that states wanted to reduce violence was not to protect their citizens from harm. In fact, states were the single largest perpetrator of violence against their own citizens. They also inflicted significant violence on other states.

Rather, they understood that societal conflict takes time and resources away from production, and a drop in productivity would have diminished revenues for the ruler of the state. Thus the ensuing reduction in violence was beneficial to both rulers and citizens alike.

With the passing of time, governments have become increasingly concerned with reducing violence for the common good. Of course, state-on-state violence remains a problem to be tackled in many parts of the world.

### 12. As states grew and trade flourished, gaining wealth through honest toil became more attractive than through violence. 

The second major historical shift that reduced violence was the _civilizing_ process. Beginning in fifteenth-century Western Europe and continuing right up to the present day, this process has been driven by two forces.

The first is the increase in the size and strength of European states during the period.

In the Middle Ages, life was violent and states were just small estates, ruled by knights. These knights were in constant battle with one another, each trying to kill the peasants of the other to deprive him of revenue.

Aside from this, society itself was extremely brutal, a fact seen even in its choice of entertainment. One popular game involved players competing to kill a cat nailed to a post. The catch was that the players' hands were tied behind their backs, so they'd use their heads to clobber the animal, risking having their eyes clawed out by the frantic beast.

But advances in weapons and military strategy enabled some knights to gain increasing power and thus consolidate the many disparate estates. In fact, from the fifteenth century to 1953, the number of these independent estates in Europe fell from 5,000 to just 30.

As these knights became kings of larger domains, they also became interested in limiting the revenue-draining conflicts of their subjects, and so began to punish violence.

Meanwhile, the second force behind the civilization process was in effect that trade networks became larger and more profitable.

This development was driven by advances in technology and infrastructures: In order to rule over these larger areas, the roads were made safer and otherwise improved, encouraging trade. Also, certain inventions, such as clocks, windmills, horseshoes and a national currency increased productivity and made trading much easier.

But how did this reduce violence? First, trading provided an attractive, nonviolent way to gain wealth, so many gave up violence in favor of it.

Second, trading discouraged violence because it depended on reciprocal relationships: put simply, the person you trade with is more valuable to you alive than dead.

### 13. The Humanitarian Revolution reduced violence by preaching that even despised people deserved humane treatment. 

The third major decline in violence began in seventeenth- and eighteenth-century Europe, before spreading elsewhere. It was caused by the emergence of a humanitarian philosophy whereby people who'd previously been treated cruelly or persecuted were beginning to be treated humanely.

The effects of this _humanitarian_ _revolution_ were seen broadly in a number of areas.

One is in the decline of killings in the name of superstition and religion. This can be seen in the seventeenth-century outlawing of witch-burning in most of Europe, which had been a common practice earlier. Also, after the Wars of Religion between Protestants and Catholics had ravaged Europe, the 1648 Peace of Westphalia finally allowed some degree of religious freedom and protection from persecution.

Another consequence around the same period was the decline of slavery — a common practice since the beginning of civilization that had inflicted unfathomable misery on millions.

The final major effect was that criminals too began to be treated with more humanity. In earlier times, they were routinely tortured and mutilated, and capital punishment was used even for trivial crimes like stealing cabbages or gossiping. From the seventeenth century onwards, however, the torture of criminals was abolished, and in the eighteenth century use of capital punishment began to decline as well.

All these decreases in violence were in fact connected, as they stemmed from a philosophy that emerged during the sixteenth–eighteenth centuries: _humanism_.

Humanism can be defined as a philosophy which values human life and happiness above all other things, and uses reason and empirical evidence as the driving force behind the design of institutions.

One probable cause of humanism's growth was the sixteenth-century invention of the printing press. It enabled the dissemination of ideas and allowed people to learn about other people, offering them a chance to see things from their perspective, and thus stimulating empathy and the development of reason.

### 14. The long peace after World War II followed an overall 500-year decline in conflict. 

Spanning the period from the end of the Second World War to the present day, the _long_ _peace_ was another historical shift away from violence.

During this period, violent warfare decreased greatly. From 1945 to 1955, the number of deaths around the world that resulted from military conflict was approximately 17 in every 100,000 people. Yet in the past decade, this number has reached a low point of one per 100,000 people.

This is due to the fact that the major powers of the world are simply fighting less, especially against one another — a trend also visible in the historical view of the past 500 years.

During the sixteenth and seventeenth centuries, the world's great powers were in battle with each other approximately 85 percent of the time. Between 1850 and 1950, this figure drops to about 15 percent, and in the latter half of the twentieth century, the major powers have not fought each other _at_ _all_. In fact, by the mid-1980s, the world's great powers had been at peace with each other for the longest period of time since the Roman Empire.

What's behind this development? A few things.

First, the civilizing process discussed earlier resulted in fewer small states vying for power and fighting. Also, it rendered trade more profitable, discouraging warfare further.

Second, the increasing separation of church and state during this time has meant a decline in religion being used as a reason for war.

Finally, Enlightenment philosophers advocating the importance of reason and human well-being began to argue that the role of the government should not be to increase the glory of the ruler, which has traditionally meant going to war, but rather to enhance the lives of its citizens, which entails _avoiding_ war.

Together these factors greatly reduced the inclination to mass-scale warfare, and the violence that it spawns.

### 15. The New Peace after the Cold War has seen further reductions in conflict, genocide and terrorism. 

The fifth major historical shift to reduce violence occurred in the post-Cold War period, from 1991 up to the present day.

During this period we've seen broad reductions in violence, as even small-scale conflict and warfare decreased.

While the major powers didn't directly engage each other during the Cold War, _proxy wars_ did occur in smaller states, where the United States or Soviet Union financed parties sympathetic to their cause. But as the Cold War came to an end, the reasons for these conflicts ended too.

At the same time, civil wars have declined as an increasing number of countries have adopted democratic reforms, eradicating the need for such conflict. The international community has helped here by offering financial support in return for democratic reforms and by establishing peacekeeping forces to maintain ceasefires in conflict zones.

Another decrease in violence stems from monstrosities like genocides becoming rarer, though sadly they do still happen occasionally — as in Rwanda and Yugoslavia. The main reason for this decline is that the ideologies which led to past massacres (e.g., the violent Marxism that spawned the atrocities of Pol Pot) have been curbed. Another reason is that democratic governments tend not to engage in such slaughter, thus the increase of democracy around the world has led to fewer mass murders.

Finally, there's the decline in terrorism. Yes, it has declined, despite popular belief to the contrary, fuelled by events like 9/11.

Political terrorism was in fact relatively frequent in the 1960s and 1970s through organizations like the Black Liberation Army, the Jewish Defense League and the Weather Underground.

While the number of terrorism-related deaths peaked in the early 1980s, at about 0.2 deaths per 100,000, by 2009 that figure had halved. This decrease was driven by the end of the Cold War and the support given to terrorists by Cold War parties. Happily, there have also been recent signs that since 2007, support for terrorism is also on the decline in the Muslim world.

### 16. The Rights Revolutions of the 1960s decreased violence against marginalized groups, like women and racial minorities. 

The final historical shift which decreased violence is the era of the Rights Revolutions, which emerged out of the 1950s and 1960s civil rights movements and has lasted until today. This era brought a decrease in violence against numerous minorities and marginalized groups.

This decrease can be seen as a continuation of the Humanitarian Revolution, which saw individual humans becoming of central importance, regardless of race, sex or other bodily attributes.

For instance, the civil rights movements diminished racial discrimination and racist attitudes, thereby reducing violence. While in the early 1960s nearly 50 percent of people claimed they would move house if their neighbors were black, by the 1980s the percentage of such respondents declined to single figures. Correspondingly, murders and assaults motivated by racism have decreased and continue to decline.

Also, the women's rights movement has furthered equality between the sexes. Earlier, women were considered the mere property of their husbands, which meant that, for example, marital rape and domestic abuse weren't considered crimes at all. Thankfully, the past quarter century has greatly reduced violence against women in all its forms.

Furthermore, the promotion of children's rights led to less violence. As late as the turn of the twentieth century, the corporal punishment of children was commonplace — for example, in Germany, where stubborn children were put on red-hot stoves or tied to their bedposts for several days. But in the past few decades, the physical abuse of children has nearly halved in many parts of the world.

Moreover, abuse and violence towards homosexuals — earlier rampant and even government sanctioned — is also declining in the West. The gay rights movement has led to a revolution in Western attitudes, resulting in homosexuality being decriminalized in almost 120 countries. Sadly, elsewhere in the world, violence against homosexuals continues to be a major problem.

The rights revolutions even extended to animals, whose abuse in, for example, animal testing has been curbed thanks to the rights afforded to them.

### 17. Final summary 

The key message in this book:

**Despite what most people think, we are not living in particularly violent times. Violence is actually at a historical all-time low because the motivators which keep us from committing violence are increasingly gaining ground on the more primitive, brutal motivators which encourage us to commit violence. This dynamic has resulted in ever-decreasing conflict between countries and between groups of people.**

Actionable advice:

**Try seeing things through others' eyes to reduce any violent urges.**

Research indicates that violent fantasies are fairly common, even for idealistic college students, and they mostly involve revenge. The next time you're upset by a perceived slight or insult (perhaps even to the point of a violent fantasy), try to imagine the scenario from the other person's perspective. Could it be that it was a misunderstanding? Could it be that they were having a bad day? Trying to see things from another person's perspective develops empathy and decreases the urge to use violence.

**Use reason and look at "unfair" situations impersonally.**

The next time you feel you are being treated unfairly, try to put the situation in impersonal terms: Imagine the same situation happening to unknown people and think about what you would consider fair. This helps you look at the matter through the lens of reason, understanding it from a more neutral perspective.

**Remember that violence still exists, so you should play your part in its continuing decline.**

Keep in mind that although violence has decreased, it has _not_ disappeared, and thus we're not yet at the time for self-congratulation. Rather, we must continue to practice and promote those factors that have led to the decline of violence so far.

**Suggested further reading: _Wisdom of Psychopaths_ by Kevin Dutton**

Not all psychopaths are locked away in maximum-security prisons and mental hospitals. Many of them live among us, in the midst of society. Indeed, a great number of highly successful political and financial leaders exhibit psychopathic traits. This book investigates why they are so successful, what makes them different from psychopathic criminals and what all of us can learn from them.
---

### Steven Pinker

Steven Pinker is an experimental psychologist, linguist and cognitive scientist with a professorship at Harvard University. His other bestsellers include _How_ _the_ _Mind_ _Works_ and _The_ _Stuff_ _of_ _Thought_.

