---
id: 572605baf2d1b1000391f2df
slug: the-big-disconnect-en
published_date: 2016-05-06T00:00:00.000+00:00
author: Catherine Steiner-Adair
title: The Big Disconnect
subtitle: Protecting Childhood and Family Relationships in the Digital Age
main_color: 79C3B6
text_color: 3F665F
---

# The Big Disconnect

_Protecting Childhood and Family Relationships in the Digital Age_

**Catherine Steiner-Adair**

_The Big Disconnect_ (2013) is about the current generation of babies, toddlers and children growing up in the digital world. Digital media, from online games to social networking sites, have a profound impact on a child's development, both intellectually and socially. These blinks outline the reasons why, and what parents can do to try to keep their children safe from these developmental hindrances.

---
### 1. What’s in it for me? Learn how to raise your children in the digital age. 

The digital revolution has fundamentally changed our lives. The ubiquity of smartphones and tablets has deeply embedded the internet into our daily routine — we text our friends via WhatsApp, we post our vacation snapshots on Facebook, and we stream our favorite music and films online. But the rise of digital and social media doesn't only change us, it also changes our children and how we raise them. In short, it changes parenthood.

How much exposure to digital media is appropriate for a child? How can we save our children from sexually explicit or violence-glorifying content? And what do we do when our children are bullied on Facebook?

In these blinks, you'll learn about the risks that child-rearing in the digital age entails and, crucially, how to effectively protect your children from cyber hazards.

You'll also learn

  * why people insult us so much in online chat rooms;

  * why educational DVDs aren't all they're cracked up to be; and

  * how Facebook can traumatize teenagers.

### 2. Excessive exposure to digital media interferes with a child’s development. 

If you've ever used a chat room, message board or social media site, you've probably been insulted by a stranger. These digital interactions aren't meaningless: they have an impact on our socialization, especially children's.

Children develop empathy by interacting with other people. According to the psychiatrist Dan Siegel, children aren't born with empathy: they develop it as they mature. They build social skills and learn to understand each other's emotions as they fight, play and make friends.

Spending a lot of time online disrupts this process. And if children spend _too much_ time in the digital world, it can actually _decrease_ their empathy.

A group of Stanford researchers did over 70 studies on this phenomenon with college students. They did a systematic review of their studies based on standard empathy tests and found that, between 1979 and 2009, empathy in US college students decreased by 40 percent.

The decline was particularly strong in the last ten years of that time frame, and technology was pinpointed as one of the main causes.

Overexposure to digital media doesn't only make people less empathetic. The fast pace of the digital world also makes it harder for children to concentrate.

In 2006, the _Kaiser Family Foundation_ conducted a survey of teenagers and found that those who did their homework on the computer were far less focused on their work. In fact, they spent at least two-thirds of their time doing something else entirely.

Digital media has a big impact on a child's development. It's making it harder for children to focus in school, and the rude comments you've seen online probably come from young people whose empathy has been stunted.

> "_Stimulation has replaced connection, and I think that's what you need to watch out for_." — Ned Hallowell, psychiatrist

### 3. Babies suffer when their parents are distracted by their devices. 

Any new parents can tell you how magical it is to look into their baby's eyes or hear their child's voice for the first time. So what happens when parents are too distracted by their smartphones to spend enough time with their babies? It could have devastating effects on their development.

Babies need a lot of sensory attention from their parents. After all, that's how they develop, both emotionally and intellectually.

Sensory interaction is about _mirroring_, the process whereby a parent looks at baby, smiles and laughs, allowing the baby to mimic her actions and learn what she means.

A team of researchers at the University of California found that when a parent fully engages with her child, it stimulates the parts of the child's brain associated with language and abstract thought.

Toddlers don't get the same benefits from educational TV shows. Digital media gives babies and young children a lot of visual stimulation, but it doesn't activate all the neurological areas necessary to develop speaking and reading skills.

Children also suffer when their parents spend too much time on their devices.

Patricia Kuhl from the University of Washington found that babies experience a lot of distress when they see an emotionless expression on their parent's face. In the past, babies would interpret that type of expression as a sign of depression; today, it happens to be the same face a person has when they're staring at a screen. As a result, babies who often see their parents with that face grow up in an emotionally insecure environment.

And, when they're finally ready for preschool after their first few years of living at home, new technology troubles await them.

> "_The brain of the child is shaped by the interactions they have with parents — that's just absolutely clear. We need to be in the physical and relational world before we reduce it down to screens._ " — Dan Siegel, psychiatrist

### 4. Preschoolers need to play with each other to develop critical social skills. 

What was your favorite childhood game? Hide and seek? Playing dress up? Computer games might allow children to dress avatars up with clothes and accessories, but is it ever the same?

Probably not. In fact, digital gaming seems to make preschoolers less creative and playful overall.

A lot of preschool teachers report that children now just wait to receive instruction or mimic behavior from online games instead of engaging with each other. They've noticed two major trends: children are less creative now, and they're less interested in playing.

Children are starting to favor simpler and more repetitive games, like crashing objects into each other over and over. They spend less time on more complex, imaginative games like having tea parties or searching for buried treasure.

Preschoolers are also less persistent about playing. Games like building paths for marbles with towers, bridges and chutes aren't very popular anymore, as children no longer have the attention span for them.

It's harmful to children when they play less because they have to interact with each other to develop critical social skills.

In his book _Yardsticks_, educator Chip Wood presented studies illustrating that the most important factor for a young child's happiness and success in primary school is a positive relationship with his teacher and classmates.

When children are drilled in reading and writing and discouraged from socializing, they're worse off in the long term. They don't perform as well in primary school as do children who were encouraged to play and develop their emotional intelligence in preschool.

Children can only develop their social skills by socializing: games can't replace play in real life. The children who are given time to play are much better off in the end.

> "_It isn't that tech is necessarily a bad thing for a child's mind, it's that you have a window of time in a child's development where touch, imagination, movement and language come together. There has to be time to develop it_." — Janice Toben, school consultant

### 5. Children need protection from harmful media as they get older. 

You probably remember a few instances from your childhood where you were exposed to images you weren't old enough for yet. Maybe Freddy Krueger or the clown from _It_ gave you nightmares or made it hard for you to fall asleep for a while.

Children are at a much higher risk of this happening today. Technology makes it a lot easier for scary monsters or people to enter their world.

Children can be traumatized by hurtful or abusive online interactions, for instance. The author had a ten-year-old client, Trevor, who once started receiving strange e-mails from an address he didn't know. The e-mails were full of sexualized insults he couldn't understand.

An investigation revealed that the culprit was a ten-year-old girl Trevor had once made fun of. She created an e-mail address purely to harass him as revenge, and Trevor suffered from anxiety and depression for months.

Children also need to be protected from media content that reinforces sexist and racist stereotypes.

In her 2006 book _Full of Ourselves_, the author presents studies illustrating that computer games and TV cause children as young as three to have negative opinions of anyone who's overweight. Moreover, most online media that targets girls focuses on fashion and beauty, reinforcing the idea that beauty gives them their worth.

In 2007, researchers Nicole Martins and Kristen Harrison did a study on the effects of racist and sexist stereotypes in video games. They found that white boys have higher self-esteem after playing them, whereas girls and non-white boys felt worse afterward because of the negative ways women and non-white men are depicted.

> "_You will never again have greater control over your home environment, the tech your child uses, and the content he or she sees than when they are five to ten years old._ "

### 6. Social media and the internet can make adolescence even more complicated. 

Did you ever have nightmares of being naked at school when you were a kid? Thanks to social media, that nightmare can now be a reality. Social media can exacerbate a child's emotional instability by publicly exposing them in ways they can't control.

In one such case, three 11-year-old girls had lunch with three boys of the same age, and one of the boys took a photo of them. The girls didn't mind at the time, but the boy ended up cropping their heads onto another photo of three naked women and circulating the new image online. It spread around their school, which was mortifying for the young girls.

The internet also complicates sexual issues for teens and preteens. According to a 2012 Pew Internet survey, young teenagers often play age-inappropriate video games online. In fact, half the boys and 14 percent of the girls named a mature or adult-only game as their personal favorite.

So young people are exposed to highly sexualized content at a time when they're still very impressionable, which might lead them to believe that unusual practices, like violent fetishes, are normal.

The internet has other ways of complicating sexuality. Take 13-year-old Alexa, for example, who once accidentally called a forgotten acquaintance and then had a tense conversation with him.

Soon after, Alexa began receiving sexualized text messages. The acquaintance had posted her number on Craigslist as revenge, along with a suggestive photograph and text. The ordeal forced Alexa to get a new number in the end.

And the problems don't end when children grow out of their preteen years, either. As preteens mature into teenagers, the internet can skew their ideas about sexuality even more.

> "_Don't let your kids have computers in their room before age thirteen — you lose them if you let them have them earlier_." Dave, 15-years-old

### 7. Digital technology makes it harder for some teenagers to develop healthy identities and relationships. 

When you were a teenager, did you ever lie about your sexual experience to fit in? Young people want approval from their peers, and social media now offers them the chance to embellish their identities.

It's easy to create an alternative persona for yourself online, but digital personas can be harmful to young people.

Consider the story of three high school friends who kept in close touch online. One of them, Jill, preferred to text when discussing anything very personal.

For two years, Jill had a volatile relationship with a boy she'd met at a summer camp and often sought advice from her two friends, who supported her. However, after Jill posted a photo of her boyfriend on Facebook, her friends soon found out the boyfriend didn't really exist. Jill had made the whole thing up.

The lie ended up costing Jill two of her closest friendships. They didn't want to be friends with her anymore after discovering she had lied to them for so long.

Texting and chatting can also make relationships harder for teenagers. We've seen that online interactions often lack empathy, and when it comes to online exchanges between hormonal teenagers, that can lead them to reduce each other to their sexuality.

Fifteen-year-old Nora had a bad experience with this when she became interested in a classmate named Mike. In a text, Mike asked her if she would be his girlfriend so he could get her to perform oral sex on him.

When teenagers are subjected to demeaning comments such as these, it hinders their ability to develop healthy relationships. Digital media increases their exposure to relationships lacking in empathy.

> "_My generation is so comfortable at communicating electronically, but we are terrible at actual relationships._ " Charlotte, 18-years-old

### 8. Parents have to earn their children’s trust so they come to them with their media problems. 

Remember how embarrassing your parents were when you were a teenager? Maybe they got into a fight with your teacher or tried to befriend your secret crush. Whatever the reason, children often want to keep their problems to themselves.

But it's important that parents earn their children's trust, which is key in protecting them from danger online. Children only tell their parents about their online problems if they feel they can rely on them.

So how do you build that kind of confidence? You have to respect the same rules you want them to respect.

Let's take a father who drives his two daughters to school every day. He constantly goes over the speed limit and often texts while driving. And, every time the frightened daughters protest, he snaps at them to be quiet.

That sort of behavior only undermines trust. If the father doesn't follow the rules about driving and texting, he can't expect his daughters to, either. They'll probably just mimic his behavior.

Parents also have to refrain from overreacting when their children do come to them with media-related problems. Children shy away from their parents if they're afraid their parents will get upset or punish them. In the worst of cases, that could lead them to refrain from speaking up about bullying, pornography or online stalkers.

The author had a 15-year-old client, for example, who didn't want to show his father e-mails he exchanged with a teacher about their lessons. He knew if his father saw that the teacher didn't always respond right away, the father would call the teacher himself.

When parents are pushy like that, it makes their children less likely to confide in them. So stay open with your kids. The best thing you can do is be trustworthy, calm and approachable.

### 9. Final summary 

The key message in this book:

**Children are better off without digital media until they're at least five years old. It's better for them to focus on socializing and playing, so they can develop critical emotional skills. As they grow up, parents need to protect them from online bullying and age-inappropriate images by maintaining a healthy relationship with them so their children feel comfortable coming forward with their problems. Technology affords children a lot of great opportunities, but it's important to use it safely and wisely.**

Actionable advice:

**Leave your devices at home when you go into nature.**

Take breaks from technology sometimes. Take your kids camping or on a trip to the sea. Get away from your smartphones and laptops for a while — it'll be good for all of you.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _How to Raise an Adult_** **by Julie Lythcott-Haims**

_How to Raise an Adul_ t (2015) reveals the ways in which the most common parenting method today, helicopter parenting, is doing more harm than good, both for parents and kids. These blinks outline a better way to parent — one that actually raises children to become truly independent adults.
---

### Catherine Steiner-Adair

Catherine Steiner Adair, EdD, is a clinical psychologist who specializes in child and family therapy. She's also an instructor at Harvard Medical School and a consultant for a number of schools.

