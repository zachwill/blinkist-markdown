---
id: 5a7824b5b238e100065411e0
slug: the-big-leap-en
published_date: 2018-02-08T00:00:00.000+00:00
author: Gay Hendricks
title: The Big Leap
subtitle: Conquer Your Hidden Fear and Take Life to the Next Level
main_color: 7675B3
text_color: 656499
---

# The Big Leap

_Conquer Your Hidden Fear and Take Life to the Next Level_

**Gay Hendricks**

_The Big Leap_ (2009) is a treasure trove of valuable insights on how to overcome the fears and tendencies that stand in the way of lasting success. Hendricks shows us the many ways we can self-sabotage our own dreams and offers practical ways of avoiding these all-too-common pitfalls.

---
### 1. What’s in it for me? Stop being your own worst enemy and start enjoying what life has to offer. 

If you've ever watched a cliff diver plummet past a wall of jagged rock, pierce the surface of the sea and come up smiling, then you have a basic idea of what it's like — quite literally — to take the big leap. You probably saw them and thought, no way I'll ever do that! Yet people are taking similar leaps every day and making the most out of life, while those who give into fear remain stuck complaining about being in the same rut, day after day.

It's time to take control of your life and be done with the excuses once and for all. And the great thing is, it doesn't involve any death-defying plunges from a hundred-foot cliff. All it takes is some confidence and a willingness to put an end to bad habits.

In these blinks, you'll learn

  * how some deep breaths can set you on the right path;

  * why worrying is a sign of self-sabotage; and

  * how you might need to put your workplace on a complaining diet.

### 2. People often resist happiness, but controlled breathing can help us overcome this fear. 

No one said life would be easy, and sometimes it can really feel like an endless swamp of problems, but ask yourself this: Are you really prepared for a life of happiness? Can you even imagine an entire day without having something to complain about?

Everyone has their own inner resistance to happiness, an oddly human trait that deserves some close scrutiny.

Despite spending a lot of time and energy striving for happiness, human beings aren't especially comfortable or skilled at feeling good or being at peace. School teaches us a lot of things, but there are no high school classes on how to deal with success and happiness.

If you were to take the time to look within and uncover why you resist happiness, the answer would likely be related to fear — a fear of achieving your full potential. Because here's the thing: when you become the best you can be, that means there's no more excuse for why you aren't making your dreams come true.

This is a fear that must be conquered if you truly want success and happiness. Taking the _big leap_ into a life of happiness requires a great deal of confidence.

So let's explore the ways you can learn to overcome this fear and start taking the right kinds of risks.

The first technique for breaking the fear barrier is breathing.

Back in the mid-1900s, psychiatrist Fritz Perls developed _Gestalt therapy_, which recognized that fear is, essentially, a sort of breathless excitement. With some focused breathing, you can transform that fear into a positive and powerful excitement that can be used to make great things happen.

Let's say you're about to take the stage for a performance or to give a speech. The common reaction to a scary situation like this is for our breath to become constricted, which only makes the fear stronger. But if you take a moment to breathe deeply, you can take control and transform the fear into the powerful energy that lets you take the stage and captivate your audience.

> _"If you say yes to the big leap, you have done the hardest part."_

### 3. Many believe they only deserve limited happiness, and they’ll sabotage themselves when times are “too” good. 

You probably have some dreams that you keep to yourself because you think they're impractical or out of reach.

This common feeling is called an _upper-limit mindset_, and it's the next hurdle to overcome.

An upper-limit mindset is a lot like having a warning system that tells us that we can only achieve a certain amount of happiness and nothing more. With this mindset in place, you might be experiencing a period of success where everything is going well, but some part of you will begin to feel uncomfortable with this winning streak. So, in order to get things back to "normal," you start to create needless obstacles and drama in your life.

This is a routine form of self-sabotage, and the problems tend to be created in areas of life separate from where we've been experiencing success. For example, if your love life's going great then you might decide to make a risky investment that puts your financial life in crisis.

But this doesn't have to happen. Generally, the upper-limit problem will only appear if you let your guard down after a great achievement.

Take a client of the author's named Lois. She was in her fifties and had a successful business, but she wasn't doing so well in the relationship department. Lois had come to believe that love simply wasn't something she could "do." But after a few therapy sessions with the author, Lois decided to give it one more shot.

Sure enough, a loving relationship did enter her life. But then, as Lois was pleased with both her romantic and professional life, she decided to let her guard down and stop her therapy sessions. Thinking she'd had the breakthrough she was after, Lois felt she didn't need any more help. In a mere six months, her relationship was on the verge of falling apart.

Fortunately, Lois resumed therapy and managed to save the relationship. It was at this point that she began to believe that she deserved to have both love and success, and became willing to work hard at both of these parts of her life.

### 4. Improve your life by letting go of useless worries. 

These days we have more than our fair share of distractions, which is why it's perhaps more important than ever to pay attention to where we're going. And this is about more than keeping our eyes on the road when we're driving. As we move along the road of life, we need to be aware of how often we're getting in our own way.

A great way to recognize when an upper-limit mindset is trying to sabotage your progress is to catch yourself when you start worrying — and then to question the source of the worry.

The next time you're worrying, ask yourself: Is this an issue I have control over? More often than not, our worries are pointless, since they're about something we simply can't control.

In the event that you _do_ have control over the issue, don't prolong the worry any more than is absolutely necessary. Instead, take action and put that worry to rest!

When you start to question your worries, you'll quickly see that most of them aren't about real problems.

For example, the author had a billionaire client who was always stressed about money and minute things like the price of toilet paper. Even though he could have lived a lavish lifestyle, something within the man wouldn't allow him to enjoy the money. Instead, the billions of dollars were a constant source of worry and misery. This is a textbook example of upper-limit syndrome.

Whatever success we may find in life, it won't change the deeper problems that may lurk within and continue to go unresolved.

For the billionaire in question, he was dealing with issues that stemmed from his parents, who routinely fought with each other over money even though the family was extremely wealthy. As a result, he not only felt like he didn't deserve his inherited riches; he was constantly criticizing his wife's spending and putting an unhealthy strain on their relationship, too.

During a session, the billionaire was asked to refrain from criticizing his wife for a week, to see what would happen. When the following week's session began, he looked ten years younger. Not only was his marriage in better shape; he was making considerable progress toward accepting his wealth, too.

### 5. Success lies in the Zone of Genius, so find out what you love to do. 

When was the last time that you got so immersed in something and were so "in the zone" that you lost track of time? However long ago it was, you'd probably like to spend more time enjoying that state of flow.

For this to happen, and for your big leap to really take off, you need to commit yourself to working in your _Zone of Genius_, otherwise known as the job you're perfectly suited for.

Finding such a job can feel frightening because then you'd really have no excuse not to be your best at work. To help you overcome this fear, here's a helpful affirmation that you can repeat to gain some extra inspiration and strength: "I commit to dedicating all my purpose and energy to my personal Zone of Genius, in which I can contribute the most to the world."

You can first repeat this affirmation softly to yourself, and then repeat it loudly and confidently, as if you were letting the universe know your intentions. By doing this, you may soon find things falling into place for you, almost as if by magic.

Even if you're not sure what you're meant to do in life, stay true to your Zone of Genius and you'll be sure to find clarity. Maybe you're torn between medicine, psychiatry and engineering. Well, if you pay attention to your studies, you're sure to notice which subject brings you to that state of effortless flow. And the more time you spend in that field, the closer you'll get to the actual job you're meant to do.

Once you discover what it is that you love to do the most, you'll also discover how easy it is to be engaged with your work — so much so that it won't even feel like work anymore! That's the Zone of Genius.

The author is most in the Zone when taking meaningful and transformational ideas on life and making them accessible to everyone. For others, it might be finding ways to make meetings more fun or simply fully committing to practicing the piano and turning it into a career. The possibilities are endless.

### 6. Use a success mantra and the Enlightened No to stay on the right track. 

No one enjoys the feeling of confinement. It's much better to break free and start ascending a never-ending ladder of success.

When you find your Zone of Genius, this is closer to what life is like. And to keep you moving onward and upward, it helps to have a mantra for success.

Repeating a mantra is a simple form of meditation. As you repeat the words, you'll begin to focus your attention, energy and entire mind and body on what you're saying — all while visualizing the message and its intention. You can think of a mantra like a unique software program that you are downloading to your mind's hard drive.

If you want to get really serious and achieve the best results possible, you could meditate for multiple hours every day. But you'll see positive results even if you only put in 30 minutes in the morning and the evening.

So, you're probably wondering, what exactly is this success mantra that will help me fulfill my dreams? Just take a deep breath and repeat the following: "I expand in abundance, success and love every day, as I inspire those around me to do the same."

After memorizing this mantra, you can then go a step further by practicing the _Enlightened No_, which is a useful method for avoiding things that aren't in alignment with your Zone of Genius.

For example, imagine a colleague approaches you with an investment opportunity. For a reasonable price that lies within your budget, you could stand to earn back $50,000 through a new product that has the potential to do a lot of good. Let's say the product is a new biofeedback machine that will help people who've been paralyzed.

The Enlightened No is a way of staying focused by asking yourself: Does this opportunity align with my Zone of Genius? If your Zone is neuroscience, then yes, but if you're Zone is making music, then you should politely decline.

This way, you won't get sidetracked and end up involved in something that distracts you from your goals.

### 7. To master time, stop complaining and don’t let problems grow. 

Back at the start of the twentieth century, Albert Einstein made history with his theory of relativity, which explained how time and space relate to one another. Well, you don't need to know all the details of Einstein's laws in order to put them to use and become a master of time.

That's right, rather than being at the mercy of time, you can take control of it. And it all starts with taking responsibility and acknowledging that _you_ are in charge.

Remember the advice about worrying, and letting go of things that are out of your control? At its core, this advice is about being honest with yourself and your responsibilities and taking control of aspects of your life that you may have a history of avoiding.

When you avoid your responsibilities, what may have started out as small problems will only get worse. As time progresses, the stress over these issues will only increase. The trick is to reverse all of this.

Let's say your child has a drinking problem. You may ignore this, thinking they'll outgrow it. But the longer you avoid dealing with the issue, the bigger it'll grow. Not only will your child suffer for a longer period but you yourself will also likely need to spend much more time dealing with the issue in the future. The minute you take control and begin dealing with the problem is the moment it will begin to shrink, and you'll instantly be giving yourself more time in the future.

Another way to take control of your time is to enforce a _complaint diet_.

Think about it: how much time is wasted every day complaining about this, that and the other? To realize the real extent, take a week to observe your colleagues' conversations and notice how much of it revolves around complaining — whether it's about a client, a coworker or lack of sleep.

When you hear your coworkers complaining, point it out to them and explain that it doesn't solve anything. It's much more constructive to take responsibility for the problem and fix it.

But don't forget to also be on the lookout for when you complain yourself. A classic complaint that should be immediately prohibited is saying you don't have enough time to do something. Not only do you have the time, but if you do it now, rather than putting it off, you'll save time. Generally speaking, the more we put things off, the more time they'll end up taking.

### 8. Success is often accompanied by relationship problems, especially when couples avoid responsibilities. 

When you find yourself in a loving relationship, you may think to yourself, "This is too good to be true!" But some people will act as though it really _is_ too good to be true and start doubting the value of their relationship and mess it up.

Whether you're successful in other areas of your life or not, relationship challenges are common. Statistics show that financial success can put a major strain on love.

Around 20 years ago, researchers John Cuber and Peggy Harroff conducted an influential study that showed how 80 percent of highly successful people were unhappy in their relationships. Many couples were still living together even though the love was no longer there, while others had never really been in love in the first place, and felt their partnership was more of a business arrangement than a romantic one. The rest had relationships that could best be described as being in constant conflict.

Though this study is decades old, there's little reason to believe much has changed. Highly successful people still feel like they must choose between work and love, and work usually wins out. But for those willing to work on a balanced life, there is hope.

One significant way to improve relationships is to avoid projecting your problems onto others.

Projection occurs when you avoid responsibility and blame others for your personal issues. For example, a woman might blame her unhappiness on her husband's passivity. But a closer look might reveal that she actually enjoys being the dominant personality in her relationships and has always ended up with passive men.

On the other hand, a man might complain that his domineering wife prevents him from expressing himself. But if he were honest about this projection, he would realize that he has trouble accepting his responsibility for ultimately being in control of his own life.

For a relationship to flourish, each partner needs to take full responsibility for their lives and not blame the other for whatever shortcomings they may have.

Ultimately, life's too short for such petty disputes. We need to stay focused on supporting each other in doing what we love and flourishing as best we can.

### 9. Final summary 

The key message in this book:

**Most people go through life believing that they're only allowed a limited amount of happiness and success. This isn't true. When you take the big leap to fulfill your full potential, you'll discover that there are no limits and there's an unlimited supply of happiness and success for each and every person willing to overcome their fears.**

Actionable advice:

**Discard the personas that are standing in the way of your success.**

Often, adults believe that a certain trait is an inherent part of their personality. For example, if your were shy in school, you may continue to carry that shyness far into adulthood. But remember, you are 100 percent in charge of who you choose to be. You can discard this shy persona and allow yourself to be outgoing, charismatic and extroverted if that is what you want and need for success.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Mastery_** **by George Leonard**

_Mastery_ (1992) reveals how you can shift your mindset to achieve long-term success in new pursuits. Drawing on real-life examples from sports, psychology and mindfulness teachings, these blinks explain the five essential elements for achieving mastery in any discipline and give us the tools we need to bounce back from pitfalls along the way.
---

### Gay Hendricks

Gay Hendricks is a psychologist who has long specialized in couple's therapy and promoting the use of conscious breathing as a tool to save relationships. He's also the best-selling author of many self-help books, including _Five Wishes_ and _Conscious Living_.

