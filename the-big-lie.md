---
id: 5a509c5eb238e100072988fd
slug: the-big-lie-en
published_date: 2018-01-08T00:00:00.000+00:00
author: Dinesh D’Souza
title: The Big Lie
subtitle: Exposing the Nazi Roots of the American Left
main_color: 92657C
text_color: 664757
---

# The Big Lie

_Exposing the Nazi Roots of the American Left_

**Dinesh D’Souza**

_The Big Lie_ (2017) is a right-wing account of current American political events. Author Dinesh D'Souza thinks that attacks against Trump from the Left are unfair. The progressive Left claim that Trump is a racist and a fascist, and have likened him to a Nazi, but this book radically upends these accusations. For D'Souza, the American Left is recycling one big lie. It's the Democrats who are the real Nazis, the true fascists and racists.

---
### 1. What’s in it for me? Hear the conservative truth. 

Have you ever been exposed to a version of the _big lie_? For instance, have you ever been accused by someone of committing wrongs that, in fact, the person accusing you is guilty of? Maybe your messy partner thinks that you're to blame for all the chaos in your shared apartment?

According to political pundit Dinesh D'Souza, this is the kind of thinking that conservatives, Republicans and right-wing voters must deal with every day. But it's on a much bigger scale. In fact, D'Souza argues, the leftist accusation that Trump and his supporters are fascists is one such _big lie_ — because it's the people on the Left who are the fascists!

In these blinks, you'll be led through D'Souza's corrective version of history, as he sets about exposing Democrats as the real Nazis, the true fascists and actual racists. From breaking with national traditions to eerie procedures of eugenics, you'll find out all the things that the American Left — Democrats, progressives, socialists and Marxists — has in common with Nazis and fascists.

You'll also learn

  * that Mussolini had impeccable socialist credentials;

  * why the German concentration camps are a Democratic legacy; and

  * how Franklin D. Roosevelt set the American Left on the path to fascism.

### 2. The Left spins the big lie that President Trump and the Republicans are racists and Nazis. 

Our story begins with Sigmund Freud, the founder of psychoanalysis. Early in his career, Freud became aware of a phenomenon he named _transference_.

This is what happens when, for instance, a criminal blames his misdeeds on his victims. It's not just distorting the truth; it's turning the truth on its head. Put simply, it's a _big lie_.

D'Souza believes that big lies play a major role in today's politics, the worst big lie being the notion, propagated by the Left, that racism and Nazism are inherently right-wing. Even American Republicans and conservatives have started believing this.

People on the Left see racism and Nazism as ideologies of the Right, with Democrats and progressives being the historical forces of emancipation, equality and civil rights.

But the author thinks otherwise. For him, the American Left were proponents of slavery and the extermination of Native Americans. They promoted racial segregation and even opposed the Civil Rights Movement.

The author also believes that the American Left is spreading a big lie by accusing President Trump and the Republican Party of racism and fascism.

And, in his view, academia, Hollywood and the media are helping to spread it.

The left-wing author Chris Hedges claimed that Trump's presidency was the "dress rehearsal for fascism." Meanwhile, the Hollywood actor Ashley Judd, in response to Trump's election, declared that "Hitler is in these streets."

The charge of fascism has also been leveled by Democrats. The Democratic presidential candidate Martin O'Malley even accused Trump of bringing "fascist appeal right into the White House."

To D'Souza, the American Left is just playing the fascism and Nazi cards. It's an alarmist and sensational way to condemn conservatism.

The author wants to show that racism and Nazism have nothing to do with Republicans or conservatives. For him, as these blinks try to explain, the progressive political Left are the real fascists.

> _"In a sick inversion, the real fascists in American politics masquerade as anti-fascists and accuse the real anti-fascists of being fascists."_

### 3. President Trump is no fascist, Nazi or racist. 

Where do the terms _right wing_ and _left wing_ originate? They actually date back to the 1789 French Revolution, where revolutionary partisans sat on the left side of the National Assembly. On the other side of the aisle, the French "right wing" rallied to the king's defense and, consequently, became known as _conservatives_.

The author believes that being conservative in America means defending the principles of the American Revolution. That is, economic freedom in the form of capitalism, political freedom and democracy, as well as the freedoms of speech and religion.

In contrast, the Left distrust the free-market system and crave greater federal control.

So what about _fascism_, then? The term comes from the Italian _fasci_. These were groups of political agitators in twentieth-century Italy. Fascism therefore represents the idea that groups are stronger than individuals. In short, the author claims, it's the equivalent of _collectivism_.

Now, given the author's definitions, it seems that President Trump isn't a fascist, a Nazi or a racist.

Sure, Trump has been endorsed by white supremacists like Richard Spencer. But does this make Trump a racist? Trump didn't endorse Spencer's policies, after all. And just because racists approve Trump's policies on things like immigration, that doesn't make Trump a racist.

Trump's also been accused of authoritarianism in the mode of fascist dictators like Mussolini or Hitler. But authoritarianism isn't the same as fascism.

Progressives also like to say Trump is a fascist because fascism and Nazism were nationalist movements. They love linking this form of nationalism to the patriotism of the American Right.

But American nationalism is different. It's about using traditional symbols like the American flag. That's very different from the Italian fascists and the Nazis, both of which broke with national traditions. Mussolini scoffed at the Italian flag, while Hitler introduced the Nazi swastika flag.

### 4. Fascism grew out of socialism and is thus a product of the Left. 

In 1922, Italian Prime Minister Benito Mussolini instituted the first fascist regime. Even today, when we think of fascism we associate it with Mussolini and Hitler. We think of fascism on the right of the political spectrum.

D'Souza argues that this is all a big lie. For him, the first fascists were actually Marxists.

Mussolini, after all, had irreproachable socialist credentials. He was born into a socialist family and, by the time he was twenty-one, he knew the orthodoxies of Marx and Engels inside out. He wrote for _Il Proletario_, a socialist weekly published in New York. By 1912, he was on the board of directors of the Italian Socialist Party. Mussolini wasn't just _a_ socialist, either. He was _the_ leader of the Italian socialist movement.

Hitler was a socialist, too. That's why he called it the National _Socialist_ German Workers' Party.

Hitler and Mussolini didn't switch from socialism to fascism. They were always going to head that way because fascism and socialism have common origins.

In fact, the rise of fascism was a response to Marxism's failure to materialize properly. Karl Marx had predicted that, as capitalism matured, the working class would inevitably rise up against the bourgeois class.

However, even as capitalism took hold in the nineteenth and early twentieth centuries in Europe, Marx's forecasts failed to come true. Instead, the living conditions for the working classes improved. The proletariat and the bourgeoisie were actually on amiable terms.

The theoretical basis of Marxism was shaken to its core. But, as Mussolini was quick to realize during World War I, the working classes had more of an identity based on nationality than on class. He therefore set about developing fascism as a new, practical form of socialism.

For the author, it's very simple: fascism is a legacy of the Left.

> _"Hitler was, like Mussolini, a man of the Left."_

### 5. The Democrats are responsible for black slavery and the uprooting of Native Americans; they even inspired the Holocaust. 

Hitler hated America's unrestrained "Jewish" capitalism. But there was one thing he did admire: American racism. On top of that, Hitler's policy for the acquisition of new land for the German race, known as _Lebensraum_, was inspired by the Democratic Party.

The Democrat Andrew Jackson, who served as president between 1829 and 1837, regarded Native Americans as alien occupiers of land destined to belong to white settlers. The Democrats needed a solution to their "Indian problem."

The aim of Jackson's Indian Removal Act of 1830 was to displace the Native Americans. The threat of genocide was made overt. The Democratic Governor of California, Peter Burnett, even openly called for a "war of extermination."

Hitler saw how the American government had displaced North America's native inhabitants. D'Souza therefore argues that Hitler was inspired by the actions of the Democrats, and that he applied their strategies to Eastern Europeans, Russians and, most of all, Jews.

The author asserts that Nazi concentration camps are a legacy of the Democratic Party. After all, slavery was an institution of the Democratic American South, whereas the Republican Party had been founded as an anti-slavery party prior to the Civil War. He claims that, in 1860, no Republican owned a slave.

For D'Souza, the Democratic plantations and the Nazi concentration camps were very similar. They were both systems based upon an ideology of inferiority. The Nazis saw the Jews as _Untermenschen_ — literally "subhumans" — while slave owners thought blacks were animals.

The author also believes the work routines for black slaves and Jewish prisoners were similar. Both toiled in groups from dawn to dusk.

Genocide? Concentration camps? These are the dark secrets of the Democratic Party.

> _"Nazi DNA was in the Democratic Party from the very beginning. The Democrats — not the Nazis — are the originators of the politics of hate."_

### 6. The Nazi Nuremberg Laws were inspired by the Democrats and anti-capitalism is inherently anti-Semitic. 

In 1934, the Nazi Ministry of Justice concocted the Nuremberg Laws to oppress German Jews. A young lawyer, Heinrich Krieger, was their prime source. Krieger had researched US race law at the University of Arkansas. And it was Krieger who perceived how Democratic segregation laws could be used against the Jews.

The Nazis were quick to realize that US segregation and miscegenation laws could be reformulated for their own purposes.

US segregation laws forbade interracial marriage, and they segregated accommodation and even drinking fountains. What the Nazis particularly liked was how the US laws created two classes of citizen — first- and second-class.

It was the Democrats who enacted and enforced segregation laws in the American South. Therefore, the author says the Democrats inspired the Nuremberg Laws that denied German citizenship to Jews and prohibited Jews from marrying or having sex with "citizens of German or kindred blood."

On top of all of that, D'Souza believes that the roots of anti-Semitism are to be found in anti-capitalism and leftist movements. He says that Nazi anti-Semitism and the anti-capitalist ideology of what he terms the Democratic Left in America both stem from the same source.

Hitler's anti-Semitism is the result of anti-capitalism: he saw capitalism and entrepreneurship as consequences of Jewish immorality. According to the author, Marx also thought that capitalism was a Jewish a phenomenon: socialism would free society from Jewish capitalism.

And, just like the American Left today, Hitler differentiated between two sorts of capitalism — productive capitalism and finance capitalism. Hitler associated finance capitalism with the "swindling" Jew. D'Souza argues that the Left's denouncement of capitalists as greedy "profiteers" is similarly anti-Semitic.

> _"Only the anti-Semitism of the Nazi era matches the racism of the Democrats."_

### 7. Like the Nazis, American progressives promote eugenics. 

No one represents the horrors of the Nazis more than Josef Mengele, the physician at the Auschwitz extermination camp. He performed gruesome experiments on the children there, conducting electroshock treatments and extracting eyeballs.

Mengele was driven by _eugenics_, the notion that the genetic qualities of the human race could be improved by preventing those with less genetically desirable traits from reproducing.

America actually had its own Mengele. His name is Kermit Gosnell. Starting in 1979, Gosnell ran an abortion clinic in West Philadelphia called the Women's Medical Society. There, Gosnell performed late-term abortions. And if a child was still somehow born alive, he killed it with a process he named "ensuring fetal demise." Gosnell's method was to drug the infants and then stick scissors into their necks.

Progressives today claim to have no connection with Gosnell. But D'Souza sees Gosnell's actions and the "pro-choice" rhetoric of the American Left as cut from the same cloth. To him, Nazi eugenics, Gosnell and progressive causes are all linked.

On top of that, in the early twentieth century, American progressives forced certain people to get sterilized, just as Nazi eugenicists would later do. In 1907, a law was passed in Indiana that forced sterilization upon "confirmed criminals, idiots, imbeciles and rapists." Twenty-six other states went on to pass similar laws. Over the next 30 years, around 65,000 people were sterilized against their will.

But it would be wrong to say that eugenicist thought has since vanished in the United States. The author claims that Planned Parenthood, an organization that provides reproductive health care, is bound together with the eugenics movement. Its founder, Margaret Sanger, defended sterilization and demanded that prospective parents obtain permits "to protect society against the propagation and increase of the unfit."

Therefore, D'Souza sees the American Left as complicit in the ideology of eugenics because they support Planned Parenthood and promote birth control.

### 8. Franklin D. Roosevelt was the first American führer and Woodrow Wilson was a proto-fascist. 

Franklin D. Roosevelt is a hero of the American Left. According to their narrative, he should be lauded because he resurrected the American economy after the Great Depression and defeated Nazi Germany.

To the author, however, FDR was the first American führer. It was he who set the American Left on its fascist path.

How, you ask, can FDR be a fascist if he defeated Hitler? First of all, the author claims that FDR did not defeat Hitler. Actually, the Soviet Union is the primary reason that the Nazis lost. FDR's US forces only helped facilitate their surrender.

The author also thinks FDR's New Deal policies were fascist because they championed state-run capitalism, just like the Nazis and Italian fascists.

Policies like the National Recovery Act (NRA) damaged the American free market, since they meant the government could set production targets, wages, prices and working hours across all industries.

The policies were depicted as the "middle ground" between socialism and capitalism, but the author thinks it was actually the American work ethic and its entrepreneurial skills that rescued the country.

Plus, Mussolini and Hitler saw FDR as a fellow dictator. The Nazi newspaper _Völkischer Beobachter_ even praised the New Deal for being fascist in tone.

But then, claims the author, FDR had learned his tricks from the American proto-fascist, President Woodrow Wilson. FDR had served as a navy secretary in the Wilson administration that lasted from 1913 to 1921 and so predated fascism proper.

D'Souza insists that Wilson helped revitalize the Ku Klux Klan. Wilson arranged a screening of David W. Griffith's infamous film _The Birth of a Nation_, which depicts the Klan as the savior of the South. Wilson realized the film's message was "terribly true" and, soon thereafter, new Klan chapters were founded in Oregon, Colorado, Wisconsin, Ohio, Pennsylvania and New Jersey.

### 9. The American Left exercises its intolerance and power through academia, the media and Hollywood. 

Even now, the Left continues to insist that they won't stop fighting to get Trump out of the White House. The progressives simply won't let up. So where do they get their power from?

D'Souza believes the American Left controls three powerful institutions: academia, the media and Hollywood.

The political Left have a tight grip on _all_ the humanities and social-sciences departments across America, not just those in Ivy League schools such as Yale and Harvard. In fact, the Oregon Association of Scholars has reported that universities "weed out" conservative professors and even refuse to hire them.

Conservatives have been systematically deprived of a voice in the media, too. TV and cable networks like CNN and HBO, and newspapers such as the _New York Times_ and the _Washington Post_ are all left-leaning.

The author imagines a conveyer belt of leftist propaganda lies. They begin in academia, are disseminated by the media and then are perpetuated by Hollywood sitcoms and motion pictures.

D'Souza is therefore appalled by the idea that a communist like Che Guevara or a "street hoodlum" like Trayvon Martin can become martyrs or celebrities to the Left.

What's more, the author sees the Left as intolerant of all opposing viewpoints.

The Left's intolerance derives from the ideas of the progressive intellectual and anti-capitalist Herbert Marcuse, who fled from the Nazi regime in 1933. Marcuse coined the term _repressive tolerance_. This concept is often summed up by the mantra "no toleration of the intolerant." Specifically, Marcuse called for little or no tolerance against right-wing movements, and toleration of those from the left.

To the author, this means that patriots, Republicans, conservatives, capitalists and Christians are not tolerated. But he also believes organizations such as Black Lives Matter and anti-fascists groups are tolerated when they shouldn't be.

### 10. Conservatives must defeat the fascism of the American Left. 

European fascism fell in World War II, thanks to outside military force. But actually it would have been possible to stop Hitler and Mussolini from within, without the outbreak of war.

In Germany, the political opposition made the mistake of trying to "accommodate" Hitler. The author believes the American conservatives and the Republican Party should do their utmost not to repeat this appeasement strategy and so permit the fascism inherent in the Left. He thinks there are a couple of ways conservatives can fight the fascist Left.

First, it's essential to show everyone that people on the American Left are the real fascists.

Second, America needs a Republican Party that can unite the conservative movement. There's no space for accommodating the fascist Left.

The author then wants the government to be shrunk. Obamacare must go. The Dodd-Frank Act, which holds back banking and investment, must be repealed. Also, only those who are really in need should receive welfare like food stamps. In fact, the author believes that the Obama administration deliberately inflated the number of welfare recipients to ensure they became dependent on the government.

Next, the Republicans should pass comprehensive tax reforms: corporate tax should be massively cut, and as many government functions as possible should be privatized.

And to ensure that conservative policies are kept in place, the Supreme Court should be stacked with conservative judges. Simply put, to counter Democratic judges you need ideologically-sound Republican conservatives in place.

To really seal the deal, the Republican Party needs to win the votes of blue-collar workers and other minorities who voted Democratic in the past.

Finally, when protests by groups like Antifa or Black Lives Matter — which the author believes to be fascist — become violent and amount to riots or domestic terrorism, the police should not be afraid to intervene and enforce the law.

It's only such a radical, strong agenda — twinned with an ideologically uncompromising judiciary and powerful law enforcement — that will ensure perpetual conservative supremacy at the ballot box and defeat the terror of the progressive Left.

### 11. Final summary 

The key message in this book:

**The American Left has spun a big lie, trying to make us believe that people on the Right are Nazis, fascists and racists. This is a full inversion of the truth. The American Left has much more in common with Nazism and fascism than it would ever admit. And the German Nazis even learned from the American Democratic Left. It is time for American conservatives to fight back and unmask this big lie.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Bloodlands_** **by Timothy Snyder**

In _Bloodlands_ (2010), author Timothy Snyder tells the tragic story of the people caught in the crossfire between Nazi Germany and the Soviet Union during World War II. The victims of the "bloodlands," or territories that after the war became the Eastern Bloc, were pushed and pulled by two ruthless powers and treated like pawns both before the conflict and afterward.
---

### Dinesh D’Souza

Dinesh D'Souza is a conservative author, speaker and filmmaker. His three films — _Hillary's America_, _America_ and _2016_ — were all greatly successful. His recent books include _Obama's America_ and _America: Imagine a World Without Her_.

