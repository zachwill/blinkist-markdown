---
id: 578a1fa4cc52f4000350f603
slug: the-big-necessity-en
published_date: 2016-07-18T00:00:00.000+00:00
author: Rose George
title: The Big Necessity
subtitle: The Unmentionable World of Human Waste and Why It Matters
main_color: D53139
text_color: B22930
---

# The Big Necessity

_The Unmentionable World of Human Waste and Why It Matters_

**Rose George**

_The Big Necessity_ (2008) takes a detailed look at the issues surrounding human excrement. Most people would rather ignore these issues — but turning a deaf ear is precisely what's led to the sanitation crises plaguing the world today. Sanitation is too important to dismiss; a lack of it is causing thousands of needless deaths worldwide. Find out what can be done to help in these blinks.

---
### 1. What’s in it for me? Learn about the features and future of feces. 

Many parents have read _Everyone Poops_ to their children — or some other book that unveils the mysteries of defecation. Post-potty training, however, most of us don't give much thought to the machinations or implications of our bowel movements, a disregard that's at the core of some major issues.

Substandard sanitation is one of the biggest causes of death and disease in the world. Furthermore, the way we dispose of our feces is, so to speak, ass-backward; fecal matter is a resource that could be put to great use for a number of things that, in today's world, are hard to even imagine. So let's explore the many little-known facts about, and uses for, human waste.

In these blinks, you'll find out

  * how many children die every year due to nonexistent sanitation;

  * why a $1 investment in sanitation can give a huge return; and

  * how China used their shit in a good way.

### 2. Lack of sanitation is a huge global health problem with deadly consequences. 

Most people prefer not to think about what happens to all the stuff we flush down the toilet. As the saying goes: out of sight, out of mind. That said, many people in the world don't have this luxury.

In fact, 2.6 billion people live in environments utterly devoid of sanitation.

This doesn't mean they're forced to use a public toilet or even a dirty old outhouse that empties into a drain. It means that 2.6 billion people have no access to a toilet of any kind. They have to defecate where they can — in public, in the woods or even in plastic bags which they then discard in the narrow alleyways of overcrowded slums.

People in such conditions constantly walk on feces and get it on their clothes, which can result in it getting into food and drinking water.

And this is just the tip of the iceberg. Just consider this upsetting fact: Four out of ten people live every day in the midst of human excrement.

This global sanitation shortage causes a staggering amount of disease: One out of every ten illnesses in the world can be traced back to a lack of sanitation, which can lead to anything from poor hygiene to contaminated water. In fact, the number of children who have died from diarrhea in the past decade is greater than the combined death toll of all armed conflict since World War II.

The reason for this is simple. One gram of feces can contain ten million viruses, one million bacteria, 1000 parasite cysts and 100 worm eggs — and sanitation experts estimate that people living without sanitation sometimes consume as much as ten grams of fecal matter _in one day_.

The result of this?

Every 15 seconds, a child dies from diarrhea. And since approximately 90 percent of diarrhea is due to fecally contaminated food or water, it's no wonder that UNICEF considers it the biggest barrier to survival for a small child in a developing country, more of threat than AIDS, tuberculosis or malaria.

### 3. Proper sanitation can save millions of lives and billions of dollars. 

There have been a lot of important medical developments over the past 200 years, from antibiotics and penicillin to birth control and synthetic insulin. But when the _British Medical Journal_ asked its readers to pick the most important medical innovation, guess what they chose: sanitation.

And they're not alone in thinking that sanitation is the biggest medical advancement in recent history.

After all, due to the poor conditions in nineteenth-century London, one out of every two children met an early death. It wasn't until proper toilets, sewers and hand-washing came along that the child mortality rate dropped by one-fifth, making it the most drastic decrease in child mortality in British history.

And UNICEF isn't alone in its assessment of the deadliness of diarrhea. The fact is that it claims about 2.2 million lives each year.

But there's a major solution to this major problem: By disposing of human feces properly, we could reduce diarrhea in developing countries by nearly 40 percent.

Harvard geneticist Gary Ruvkun suggests that having a toilet can add 20 years to the average human life, making it the single biggest factor in living a long and healthy life.

And good sanitation makes sense both medically and economically. Proper sanitation means healthy workers and saving money on hospital visits and medical bills. So, on average, for every dollar invested in sanitation, seven dollars are earned through productivity and avoiding health care costs.

To go a step further: If we spent the $95 billion required for worldwide sanitation, we would end up saving $660 billion.

Let's take a look at Peru's 1991 cholera outbreak to get an idea of how much economic sense this makes.

While it cost Peru $1 billion to contain the outbreak, it could have been prevented if the country had made the $100 million investment for a better sanitation system.

Moreover, in the first ten weeks of the epidemic, the country saw losses in agriculture and tourism revenue that was three times greater than what was spent on sanitation over the past ten years.

So, if sanitation can make people healthier and wealthier, why isn't it at the top of the political agenda?

### 4. Sanitation is a taboo subject, making it even more difficult for people to address the problem. 

Just as people don't like to think about what happens after they flush the toilet, they don't like to talk about sanitation, either. Even finding the right words can be a struggle. But when did this become such a roadblock to progress?

It may have to do with the limitations of the English language.

In our coarser moments, we say "shit." When speaking in medical terms, we use words like "stool" and "bowel movement." And then there's the formal language, like "feces" and "excrement."

Yet, there's no neutral term to use in civilized conversation, and there may be historical reasons for this.

Anthropologist Norbert Elias studied the development of defecation in his book _The Civilizing Process_. For instance, it was once an honor to greet a king seated on his chamber pot, whereas we now prefer doing our business behind closed doors.

We can see how our language reflects this modern attitude. To sanitize the dirty implication of the toilet, we use clean-sounding terms like "water closet" or "bathroom."

This trend can also be seen in the language of international policy. Rather than talking about shit-related diseases, people talk about "water-related diseases."

And when they talk about the destructive cycle of poor sanitary conditions, they clean it up by referring to _the five F's_ : feces in fields and foods, and on fingers and flies.

But despite all the creative lingo, human waste remains a low priority in government budgets.

Take Pakistan, which spends 47 times more on its military than on clean water and sanitation, despite the fact that they lose 120,000 people per year to diarrhea.

Furthermore, the foreign aid agency USAID has a limited sanitation-related budget: 90 percent goes to supplying water, even though the cleaner water only reduces diarrhea by 16 to 20 percent. If that money were spent on sanitation, it could reduce it by 40 percent.

> _"...HIV/AIDS cannot be discussed without talking frankly about sex… the problem of sanitation cannot be discussed without talking frankly about shit."_ \- Umesh Panday, Nepali sanitation activist

### 5. Shifting people’s habits and attitudes toward sanitation is essential for effective activism. 

Every day the equivalent of 155,000 truckloads of feces are deposited out in the open and public areas of India. That's 200,000 tons of raw, untreated sewage that people have to walk through and live around. There must be a better way, right?

This problem of _open-defecation_ hasn't gone unnoticed. Between 1986 and 1999, 9.45 million latrines were installed by the India Central Rural Sanitation Program, increasing their availability by 15 percent.

Unfortunately, getting people to use toilets is another problem. Millions of these latrines have gone unused or been misused.

Many people simply don't have a water source, so they prefer open-defecation over carrying a bucket of water back to the latrine.

The problem with the state-issued latrines is that it was a top-down approach that didn't consider the people's perspective.

Activists, on the other hand, use community outreach and creative psychology to tackle the problem, speaking directly to the villagers and telling them about the health hazards of open-defecation and the advantages of using toilets.

Kamal Kar, an Indian consultant for the non-profit organization WaterAid, visited the Bangladeshi village of Mosmoil in 1991 to dissuade the population from open-defecation.

Even though WaterAid had been building latrines in Bangladesh for years, 40 percent of the country's diseases were still related to feces.

Kar walked around with the villagers and, together, they calculated the 120,000 tons of excrement that were all around them. Some of the villagers, realizing how much fecal matter was ending up in their food, literally vomited from shock.

Kar's visit sparked a village-wide initiative to work together to improve their situation.

These tactics quickly gained traction. The effective method of getting people to _want_ sanitation has been widely adopted in India and elsewhere, and it's even earned its own name: Community-Led Total Sanitation (CLTS).

### 6. Sanitation is a worldwide problem that no one should ignore. 

At this point, you might be thinking that sanitation is only a problem for poor and underdeveloped countries. Alas, this is far from the truth.

Well-developed countries and prosperous cities are also facing sewage problems.

Let's start with some basic facts: The United States may be the most powerful country in the world — and yet 1.7 million people are still without sanitation.

Every few years, the American Society of Civil Engineers grades US infrastructure on a scale of A to F. In 2000, its wastewater system received a D. And things didn't improve. In 2005, it slipped down to a D-.

Nor is the problem limited to wastewater. In 2000, the US Environmental Protection Agency estimated that a quarter of US sewer pipes were either in poor or very poor condition. They also estimate that by 2020, half of the country's sewage pipes will likely be in danger of falling apart.

Even New York City isn't immune. The sewage system was built using a model that funnels all sources of water, from streets and bathrooms and anywhere else, into the same system. While this works fine under normal conditions, the system becomes vulnerable when it rains.

That's what happened on August 8, 2007. In just two hours, 3.5 inches of rain fell in Manhattan, while 4.26 inches of rain hit Brooklyn. The faulty design of the city's sewers caused the subway system to fail as the tracks flooded with more water than the pumps could handle.

But it's not just public transit that's affected; the sewer system is designed to discharge its overflow of waste into the nearest body of water.

According to environmental group Riverkeeper _,_ these discharges occur once a week and average around 500 million gallons. That's nearly 800 Olympic-size swimming pools of sewage.

And when we look outside New York City to nationwide statistics, the wastewater industry discharges 1.46 trillion gallons into various waterways and oceans.

> _"Civilized people ought to know how to dispose of sewage in some way other than putting it into the drinking water."_ \- Teddy Roosevelt

### 7. Human “waste” is a useful resource, as China is making clear. 

So, what can we do with all our waste? Obviously, just dumping it into our water isn't the best solution.

Martin Luther, a leader of the Protestant Reformation in the sixteenth century, reportedly ate a spoonful of his own shit every day and wrote that he "couldn't understand the generosity of a God who freely gave such important and useful remedies." While this is not actionable advice, Luther was definitely onto something.

After all, shit can be useful.

Even the term "human waste" isn't entirely accurate, since feces can be reused. We can turn it into bricks and roads, and the nitrogen and phosphates it contains can feed plants.

It also has medical uses: Patients suffering from a severe form of bacterial infection have shown a 90-percent recovery rate after receiving what's known as a fecal transplant.

China has been especially inventive, using human waste to produce _biogas_.

Biogas is energy harnessed by fermenting any organic material, from plants to human excrement.

Upon last count, 15.4 million rural Chinese households have a _biogas digester_ connected to their toilets, converting feces into energy used for heating and lighting.

Naturally, this cuts down on conventional energy costs, including electricity; one cubic meter of biogas equals six hour's worth of light from a 60 to 100 watt bulb.

Additionally, the remains of the biogas conversion process can be used as fertilizer, eliminating the need to buy expensive artificial fertilizers. On top of all that, China's Research Institute of Medical Military Scientists claims that vegetable yields increase by 50 to 60 percent when fertilized with the biogas byproduct.

Finally, there's the additional benefit of saving natural resources.

Wood is the most common source of fuel in rural China, so much so that a 1991 survey found that a five-person family in China's Guangxi region used 2.3 tons of wood for fuel each year.

Replacing this practice with biogas saves forests as well as time: Meals cooked using biogas stoves can be ready in 20 minutes, as opposed to the two hours it takes to heat on an iron stove with wood and rice stalks.

### 8. Recycling human waste has many complexities and challenges, much like our attitude toward human waste. 

If these possibilities for reusing human "waste" seem too good to be true, that's because they sort of are. While biogas has a lot of potential, it's far from perfect; we're not living in a utopian wonderland quite yet.

Reusing human waste comes with a mess of issues, many of them illustrated, once again, by China.

Many farmers in rural China rejected biogas. The new, fancy toilets required maintenance, and many of the farmers, lacking training, went back to using unsanitary, hole-in-the-ground latrines.

They also went back to using wood for fire and fuel, and deforestation levels spiked to alarming heights for the federal government in Beijing.

The leftover biogas slurry that was being used for fertilizer also caused controversy. Some experts suggest that any harmful pathogens are killed during the four-week digestion process the biogas undergoes; others, however, think the leftovers could still be harmful.

As we can see, there are no quick and easy answers for the global sanitation problem. But the first step toward a solution is to get more comfortable discussing the subject so that we can engage in productive discourse.

The reality is this: everyone poops. In fact, the average person spends about three years of their life using the toilet.

So, there's no reason we can't have an open and positive attitude when it comes to discussing sanitation.

Even the French architect Le Corbusier believed the toilet to be "one of the most beautiful objects industry has ever invented," and author Rudyard Kipling considered sewers more compelling than literature.

According to Indian sanitarian Dr. Bindeshwar Pathak, discussions about sanitation are also discussions about actions and institutions central to our humanness: the national economy, politics, the role of the media, our cultural preferences and so on.

So what are we waiting for? Let's get the discussion going and start saving lives!

> _"To make [the role of excretions in human life] more accessible… is not only a courageous but also a meritorious undertaking."_ \- Sigmund Freud

### 9. Final summary 

The key message in this book:

**The topic of toilets and sanitation, though often avoided, is vital to the state of global health and wellness, particularly for developing countries. By neglecting sanitation as a priority issue that affects cities, states and nations across the globe, we are only perpetuating diseases and ignoring important human rights issues. Changing our attitude is necessary if we want to solve these issues.**

Actionable advice:

**Next time you go to the bathroom, cover the lid when you flush.**

It might sound silly, but flushing with the lid up leads to a lot of germs being sprayed all over the area around the toilet. Of course, this advice is for those of you who are lucky enough to have a toilet to flush in the first place!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Gut_** **by Giulia Enders**

_Gut_ (2015) takes an entertaining yet scientific look at an organ that is just as interesting and important as the brain — the gut. By tracking a piece of cake as it works its way through the digestive system, you'll come to appreciate the gut for the sophisticated and impressive ecosystem that it is.
---

### Rose George

Rose George's writings have appeared in _The New York Times_, _Slate_, _The Guardian_ and _Scientific American_. Her other books include _Ninety Percent of Everything_ and _A Life Removed_.

