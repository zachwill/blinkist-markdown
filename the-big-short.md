---
id: 534e99806531660007260000
slug: the-big-short-en
published_date: 2014-04-23T08:47:58.000+00:00
author: Michael Lewis
title: The Big Short
subtitle: Inside the Doomsday Machine
main_color: None
text_color: None
---

# The Big Short

_Inside the Doomsday Machine_

**Michael Lewis**

These blinks examine the causes of one of the biggest financial crashes in history, and what it uncovers is shocking. What's more, it delves into how a few individuals spotted the storm on the horizon and actually managed to profit from the crash.

---
### 1. What’s in it for me? Find out how you can profit from a financial crash. 

The financial crisis that brought the global economy to its knees in 2008 affected most countries in the world, and bankrupted many of the oldest and most well-renowned financial companies like AIG and Lehman Brothers.

In these blinks, you'll discover how greed and mismanagement stirred up the crisis, while indications of the impending collapse were being ignored and even hidden.

You'll also see how the entities designed to protect the economy failed dismally.

Finally, you'll find out how one rogue neurologist and a few others were able to make millions while the world around them came tumbling down.

### 2. Subprime mortgages were at the root of the financial crisis. 

After the financial crisis of 2008, you probably started to notice lots of media commentators throwing around the term _subprime mortgage bonds._

At which point you may have asked yourself what, precisely, they were.

Basically, mortgage bonds are giant packages of mortgage loans where individual mortgages, like the one you have if you own a house, are combined into one big package. Slices of this package — i.e., bonds — are then sold to investors so that, when homeowners make payments on their mortgages, the investors earn interest.

However, the _subprime_ mortgage bonds we're talking about comprised risky mortgages: the homeowners in question were poor and had little chance of ever paying back their loans. When they started defaulting in droves, the investors who had bought the bonds lost a lot of money: Morgan Stanley's trader Howie Huber lost a whopping $9 billion.

But if these loans were so risky, why were they given out in the first place? And why did investors want to buy bonds in them?

The popular reasoning at the time was that this mechanism allowed people in even lower income classes to get credit and buy homes. The entity that extended the mortgage to poor people was able to offer them lower interest rates because they knew they could also turn a profit by selling those mortgages onward to investment banks that would then sell them to investors as bonds.

However, this reasoning was seriously flawed, and it created a ticking time bomb of future risk. In most cases, the seemingly low interest rates were, in fact, only temporary, and were meant to increase significantly after two years. In other words, many people were seduced into taking on a mortgage with low monthly payments in the beginning, only to find that, after two years, their mortgage payments became untenable, and so they defaulted.

The inherent riskiness of subprime mortgage bonds was unfortunately ignored, and their popularity among investors would eventually imperil the entire financial system.

### 3. The risks of subprime loans were hidden in collateralized debt obligations. 

At this stage, you may already be wondering how on earth the regulatory authorities or anyone in the financial industry could have failed to realize that these risky subprime mortgage bonds would lead to a disaster.

Well, the short answer is that their riskiness was deliberately hidden, and one of the major ways this was done was through collateralized debt obligations (CDOs).

CDOs were invented by the investment bank Salomon Brothers in order to address the problem of borrowers defaulting on their mortgages, which made earnings from mortgage bonds unpredictable.

In CDOs, mortgage bonds were basically divided into tranches depending on their risk level, and investors could then choose which tranche suited their risk profile. Together, these tranches were referred to as a "tower," where the bottom tranches were the riskiest mortgages from the least creditworthy people. To compensate for the high risk of default, these tranches carried the highest interest rates.

The topmost tranches, however, consisted of mortgages to creditworthy people who paid lower interest rates, making the safer but less lucrative — a steady trickle of income.

But while this may seem like a sensible way to match risky investments with investors willing to take risks, CDOs were also a vehicle for hiding risks.

For example, consider the so-called "mezzanine CDO." This particular instrument was very complex and opaque, and actually comprised the lowest tranches of several mortgage bonds, making even the higher tranches extremely risky, though investors did not know this.

The popularity of such CDOs were one of the factors fueling the rapid growth in the trade of subprime mortgage bonds: the total trading volume went from $55 billion in 2000 to $507 billion in 2005.

### 4. Rating agencies failed to objectively evaluate the risks of subprime mortgage bonds. 

As subprime mortgage bonds were busy making the financial system ever riskier, you might wonder just what the various supervisory entities — whose responsibility was to regulate and manage the financial system — were doing.

Well, some such _rating agencies_ were involved, but they only managed to worsen the situation.

The role of rating agencies is to objectively determine the risk of financial instruments, such as mortgage bonds, so investors know what they're buying. They do this by giving instruments grades ranging from "BB" (the riskiest junk instruments) to "AAA" (the best instruments).

Unfortunately, the mortgage bond traders working at the big investment banks were able to manipulate the rating models used by the agencies to get higher ratings for their bonds.

One weakness in the agencies' models was that they didn't analyze individual mortgage loans, and instead focused only on the average properties of the whole pool of loans related to a mortgage bond.

This means that, for example, a rating agency would award the same rating to a mortgage bond whether it comprised:

a) 50 percent ultra-risky loans and 50 percent ultra-safe loans or

b) 100 percent of loans of average risk.

The mortgage bond in the first scenario clearly deserves a far worse rating than the one in the second because the risky lenders will likely default, producing disproportionate losses.

Some mortgage bond traders knew this and deliberately pooled loans together in this way to mask the most risky, "toxic" ones that were bound to default.

Furthermore, investment banks paid a fee to the agencies for every rating they made, creating a huge conflict of interest. Exposing the traders' murky tactics would have constituted biting the hand that fed them.

One employee at the rating agency Moody's admitted that the company did not allow her to downgrade the rating of certain mortgage bonds she felt were risky. So much for objectivity!

### 5. The US government failed to stop or punish those responsible for the financial crash. 

Unfortunately, not even the US government was able to sort out to the questionable practices of the banks.

One clear example of this failure was the lack of regulation of so-called _floating rate credits_, an extremely dubious practice used by mortgage lenders.

As mentioned earlier, CDOs and subprime mortgage bonds were being traded in massive volumes, and so there was an increasing demand for them. Of course, to generate new mortgage bonds, you need people to take on more mortgages, and this was achieved through the aforementioned floating rate credits.

Basically, floating rate credits are mortgages where the borrower is enticed with low "teaser" interest rates, making the loan payments seem feasible at first. These rates then increase dramatically after a few years, at which stage the borrowers often find themselves unable to pay and then they default, losing their homes.

Sadly, the government did little to regulate this predatory lending practice, despite the fact that it was brought to light as early as 2002, when a lender known as the Household Finance Corporation was sued for using the practice and forced to settle out of court for $484 million.

Even after this, the federal government refrained from intervening, thereby enabling the boom of risky subprime mortgages.

When the financial system finally crashed in 2008 along with the entire American economy, you'd think that the government would hold those responsible for it accountable — alas, it did not.

In fact, most investment banks that had contributed to the problem by deliberately hiding risks in the mortgage bonds they sold were saved from bankruptcy by the government through generous "handouts." One such handout was the _Troubled Asset Relief Program_ (TARP), comprising $700 billion in taxpayers' money to guarantee the full original value of the now almost worthless subprime mortgage bonds. This action is the only thing that saved, e.g., the bank Citigroup from bankruptcy.

### 6. As long as traders generated immediate profits, no one cared about their dubious tactics. 

Let's turn our gaze to investment banks and the bankers who worked for them. Why did they use such questionable tactics?

Because short-term profits trumped all their other concerns — and their highest priority was to chase them by any means necessary.

As mentioned before, this led to the traders disguising the risks of subprime mortgage bonds and CDOs that they sold to investors. This was done even in the common cases where pension funds or unions were the investors, and the losses thus had a clear human impact.

One particularly despicable case of this was that of Wing Chau, who advised customers on CDOs on behalf of the investment firm Harding Advisory. In early 2007, he conceded that his sole interest was generating the biggest possible cash flow for the banks, even though he was supposed to look after the safety of the investors.

Of course, as long as the traders kept generating massive profits with their dubious methods, no one was too interested in how they did it. Which was to their advantage since nobody could find out anyway: they were the only ones who knew how the actual system worked.

Almost nobody — not the investors, the government, the rating agencies or the shareholders of the investment banks — knew exactly what a subprime mortgage bond or a CDO was made up of because the products were so complex and opaque. Even most investment bank CEOs didn't quite know what their traders were up to.

But thanks to the profits they were raking in, star traders were granted lots of freedoms and perks to prevent them from leaving to start their own company, and they used these liberties to engage in questionable practices.

Consider Howie Hubler, a trader at investment bank Morgan Stanley: to keep him in the company, he was granted his own special trading group of which he owned 50 percent, and within which he could act pretty much as he pleased without the usual corporate controls.

### 7. A few Wall Street outsiders saw the crisis brewing. 

Of course, every secret is bound to be discovered and, indeed, a few individuals outside of Wall Street managed to catch a glimpse of the rotten core of the financial system, spotting small details everyone else had — willfully or not — missed.

One such person was Steven Eisman, a Harvard Law graduate who worked as a financial analyst before delving into the world of bonds. Eisman was an eccentric figure renowned for his rudeness and disheveled appearance.

And yet, Eismann was the first person to unveil the substantial risks of the subprime mortgage bond market in 1997.

Back in the 1990s, there had been a boom in subprime mortgage bonds similar to the one that began in 2003. It was during this comparably smaller boom that Eisman discovered that weak mortgages were being sold through shady practices. After publishing his report in 1997, the subprime mortgage bond market crashed.

Michael Burry was another person who saw the danger ahead. An autist and sociophobe with a glass eye, Burry had been working 16-hour days as a neurologist when he got his start in the bond market, eventually managing to turn his autistic tendency of obsessiveness towards focusing on the financial market. He quit his job in 2004 to fully concentrate on the bond market.

That same year, he discovered that the subprime mortgage borrowers had such poor creditworthiness that the only thing keeping them from defaulting was the fact that house prices were rising, so they could constantly get new mortgages.

If house prices were to decline, the whole bond market would implode due to mass defaults. Burry also saw that the housing market was massively overheating as new houses were being built left and right, so he knew the implosion was not far off.

### 8. The few who saw the impending subprime collapse were ignored and managed to profit from the crash. 

So what did people, like Eisman and Burry, who knew that the subprime mortgage bond market was heading for disaster, do with their knowledge?

Simply put, they profited.

And they did so by betting _against_ the market. This kind of negative speculation is known as "going short," and when investors go short, they profit when the value of an instrument decreases.

Most of them also founded _hedge funds_, i.e., funds that invest borrowed capital into speculative investments. Thanks to their founders' knowledge of the subprime market's impending crash, they were highly successful.

Eisman once met the infamous CDO-selling manager Wing Chau and knew from the conversation that Chau didn't care about the riskiness of the bonds he sold to investors. Eisman therefore made it a policy to bet against every single bond that passed through the hands of Chau.

But Eisman and Burry also tried to warn others about the impending disaster. Unfortunately, at the time, most people didn't take their warnings seriously: they were either ridiculed as unduly pessimistic fools or simply ignored as insignificant, small-time players.

Their unusual demeanor no doubt played some part in this, too.

For example, the autistic Burry often provoked discontent or even rejection with his communication style. That fact paired with his unconventional investment strategy made it so even the investors in his hedge fund continually lamented his leadership and second-guessed his decisions.

### 9. Credit default swaps allowed investors to bet against the subprime market. 

By now you know that some savvy investors managed to profit from the crash of the subprime market by betting against it.

But how, exactly, did they manage to short the market in this way?

One key instrument was the _credit default swap_ (CDS), essentially an insurance you take out on a loan or bond, which pays out in case the borrower defaults.

Say you've bought a bond and you're worried that the borrower will default on it. To protect yourself against that, you can buy a CDS: you pay an annual fee to the seller or the CDS and they, in turn, pay you a predetermined sum if the borrower defaults.

But you don't even have to own a bond to do this; you can just buy the CDS. Thus, you're effectively betting that the borrower will default and the CDS payout will constitute your "winnings."

It was through this dynamic that people profited from the subprime crash: they bought CDSs for highly overvalued subprime mortgage bonds, and thus secured a high value for the CDS payout.

In 2005, Mike Burry realized this opportunity. He managed to convince Deutsche Bank and Goldman Sachs to issue CDSs on subprime loans for him, even hand-picking the most horrendously low-quality bonds for them to insure. But no one seemed to care, which demonstrates how completely ignorant the market was about the risks. This left him with very valuable insurance for bonds that were bound to default.

And that's how a few individuals profited from one of the biggest financial crises in history.

### 10. Final Summary 

The key message in this book:

**Contrary to popular belief, the world of finance did not suddenly and unexpectedly implode in 2008: the crisis was foreshadowed years earlier. Greed, ignorance and incompetence allowed for the subprime mortgage market to rise up and collapse like a house of cards, while a few savvy investors profited from seeing the looming disaster in advance.**

Actionable advice:

**Be extra cautious when investing in bonds.**

If you want to make money by investing in bonds, be extremely careful in scrutinizing your investments. Keep in mind that the bond market is not regulated in the same way as the stock market, so it's up to you translate the traders' fanciful claims into reality. Don't hesitate to act against the wisdom of the crowd if your analysis says you should, as you could end up profiting much like Eisman and Burry.
---

### Michael Lewis

Michael Lewis is a former Wall Street trader who worked for the now-defunct investment bank Salomon Brothers in the mid 80s. His previous bestsellers include "Liar's Poker", "Moneyball" and "The Blind Side".

