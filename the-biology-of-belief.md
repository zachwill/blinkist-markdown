---
id: 53bceba23633360007080000
slug: the-biology-of-belief-en
published_date: 2014-07-08T00:00:00.000+00:00
author: Bruce H. Lipton
title: The Biology of Belief
subtitle: Unleashing the Power of Consciousness, Matter and Miracles
main_color: 296A9A
text_color: 296A9A
---

# The Biology of Belief

_Unleashing the Power of Consciousness, Matter and Miracles_

**Bruce H. Lipton**

_The_ _Biology_ _of_ _Belief_ describes a revolutionary change in biology and explores a new approach to the connection between mind and matter. Using easily accessible examples and explanations, Lipton offers a radical alternative to our understanding of the influence of genes in determining our behavior and identity.

---
### 1. What’s in it for me? Learn why there may be a spiritual explanation to life. 

Recent research in biology presents evidence that throughout the twentieth century, the power of genes has been grossly overestimated. It appears that the environment and our perceptions are far more influential than previously thought. _The_ _Biology_ _of_ _Belief_ explains the fundamental differences between traditional biology and new biology and how, based on these insights, practical and even spiritual conclusions can be drawn from modern research in the field.

In the following blinks, you will discover:

  * what switches your genes on and off,

  * why we should make peace, not war, and

  * why we are made in the image of God/the universe.

### 2. Cooperation rather than competition should be the central tenet in our theory of evolution. 

Who discovered the idea of evolution? Charles Darwin? You might think so, but you'd be wrong. It was actually discovered a few decades before Darwin, by a French biologist named Jean-Baptiste Lamarck.

Lamarck's idea of evolution, however, was slightly different than Darwin's.

Unlike Darwin, who viewed evolution as a battle between species, Lamarck conceptualized evolution in kinder terms. To Lamarck, cooperation between species and individuals was extremely important to evolution.

And unlike Darwinist theory which describes random genetic mutations, some of which help an individual survive in their environment and in turn encourage evolutionary progress, Lamarck suggested that species evolve as they learn to fit their surroundings.

And in many ways, Lamarck's view is closer to our current understanding of evolution.

For example, when we look at how the immune system functions, we can see how organisms adapt to their environment and pass this knowledge onto their offspring.

When a virus enters our body, our antibodies fight it. When antibodies are successful, they "remember" the virus and how to kill it. This memory is then passed on to the antibody's daughter cells.

Lamarck's notion that organisms often cooperate, rather than constantly fight, can also be demonstrated by modern scientific research. And we are not just referring to members of the same species. There are many _symbiotic_ _relationships_ in nature, where different species appear to cooperate with one another.

For example, our digestive system contains billions of bacteria to help it function. Without these bacteria, we wouldn't be able to digest the food we eat.

Furthermore, interspecies cooperation even affects genes.

Science has revealed that genes don't necessarily have to be passed on through reproduction from individual to individual, but that they can be shared with members of other species.

The following blinks will show how our understanding of the functioning of human cell biology confirms this Lamarckian idea of cooperation.

### 3. The brain of a cell, which controls cell behavior, is not its nucleus but its membrane. 

Lamarck's theory points to the importance of cooperation in biology. Let's explore this by looking at nature's remarkable survivor — the cell — whose functions are the result of many elements working together.

There is not a single function in our bodies, from digestion to the workings of our immune system, that can't be found in a single cell.

Cells are incredibly smart and were among the first life forms on our planet. Their intelligence has enabled them to survive, while the vast majority of creatures have died out.

One example of their intelligence can be seen when individual cells are removed from the body and grown in a culture. When this happens, cells will actively seek out environments where they can survive and avoid environments that won't foster their growth.

But where does this smartness come from and what guides cell behavior?

Most people would probably guess that it comes from the cell's _nucleus,_ where genetic information — the DNA — is kept in the form of chromosomes. However, if this were true, a cell should die as soon as its nucleus is removed.

Yet we know that when the nucleus is removed, a cell will continue to live and function as if it had a brain. This is because the nucleus of a cell is actually where reproduction takes place. Rather than its brain, the nucleus is actually the cell's gonad, or sexual organ.

In fact, the cell's actual brain is more likely its membrane, which surrounds the outside of the cell.

Inside this membrane there are two types of proteins, _receptor_ _proteins_ and _effector_ _proteins_, which react with the environment to trigger the action of the cell. These receptors pick up signals from the environment, and the effectors turn the signals into action.

If these proteins were removed from the cell, it would become "brain dead" and unable to respond to its surroundings.

> _"Confusing the gonad with the brain is an understandable error, because science has always been and still is a patriarchal endeavor."_

### 4. Despite what some Darwinists believe, there is a lot of evidence to suggest that genes do not determine our development. 

In the previous blinks, we found that Darwin's theory of evolution is not always supported by science.

Remarkably, even Darwin himself had doubts about his work. In a letter he wrote near the end of his life, he questioned whether he had given enough consideration to the role of environmental factors in evolution.

It turns out there are plenty of weak spots to be found in the evolutionary theories developed by Darwinian scientists.

One such theory is _genetic_ _determinism,_ which centers on the belief that genes govern biology. More specifically, genes determine the production of proteins that form an organism's body.

However, this theory fails to hold up in one area: if our biology is determined by our genes alone, then the human genome would need a gene for each protein, which would lead to a minimum of 120,000 genes. Yet we have far fewer — the human genome contains only 25,000 genes.

So there must be other aspects that determine our biology.

Rather than genes deciding our destiny, it has been discovered that our environment also plays a key role.

Inside our cells, we have a series of _regulatory_ _proteins_ which surround the DNA in the nucleus. These proteins react with signals from the environment to help determine the destiny of the cell. They do this by only allowing certain codes of DNA to become activated.

To illustrate this, imagine someone has a gene which makes them more likely to develop a certain disease, such as Parkinson's disease. Even though they possess the particular gene, however, it doesn't mean they will develop the disease. Instead, it depends on whether the regulatory proteins allow the gene to be activated.

So what our cells actually become hinges on their environment, which means that a more deterministic or Darwinian view is far from the truth.

### 5. Theories in medical science are outdated and this puts us in danger. 

You might be surprised to learn that illnesses caused by medical treatments are one of the biggest causes of death in the Western world. In the United States in 2003, illnesses stemming from medical treatments were in fact the number-one killer. So why is this happening?

The answer can be found in the world of physics.

At the beginning of the twentieth-century, there was a radical change in our understanding of physics. The old, Newtonian view that determined the linear relationships between cause and effect (A always leads to B, which always leads to C) was replaced by Einstein's theory of a complex web of interactions between energy and matter (A sometimes leads to B, but it can also lead to C).

Unlike physics, however, biology is still based on outmoded principles from the Newtonian world. For example, once the likely cause of an illness is identified, the treatment provided for it stays the same.

However, experiments have shown that the organisms also work according to Einstein's notion of a more interactive process.

For example, by observing the cells of a fruit fly, scientists have discovered that the reactions between proteins are not linear but rather a series of interlinking reactions.

And these reactions can have a domino effect. Picture the relationship here as a complex web — tweak one area and other areas also change. Moreover, some areas will be more affected than others.

So this could be an explanation as to why so many people suffer side effects from medical treatment, as the same treatment can't always meet the unique needs of every individual.

Therefore, it would be advantageous for biological science to look at alternative treatments such as acupuncture, as one example, instead of using the same treatments for everyone. However, the power of pharmaceutical companies means that at least for the moment, we are stuck with potentially harmful pills.

### 6. Our minds play a crucial role in our physical health. 

You have probably heard of the _placebo_ _effect._ This is where people recover from an illness after receiving a "fake" treatment, such as taking a sugar pill. This effect suggests that the power of recovery lies at least partly in our minds; we get better because we _think_ we will.

So how could this be?

Well, we know that the mind plays a crucial role in regulating our body. We are not just talking about the conscious mind; that is, we don't just get better because we _will_ it. Rather, the conscious works alongside our subconscious mind, which is many times more powerful.

Scientist Candace Pert discovered one such way in which the mind is fascinatingly powerful. She discovered that the mind is not contained just in our heads, but is dispersed throughout our body through _signal_ _molecules_. While these signal molecules send information to the brain, the brain can also subvert them and transfer information in the other direction.

Furthermore, Pert found that our conscious mind can create _molecules_ _of_ _emotion_, which can program our body to feel better.

Our ability to use our conscious mind to supersede our automatic responses to our environment is what makes us who we are, yet it can be problematic.

We are capable of acting beyond our base instincts and, unlike animals, we can program our own behavior. But this ability can end up with us being programmed in a way that can be harmful. This happens, for instance, when we are given negative messages from our parents or teachers.

For example, say a teacher tells you repeatedly that you are stupid. This message could become part of your programming, which could lead you to refrain from doing "intelligent" things, choosing a "difficult" career or sharing your thoughts with others.

The positive _and_ negative effects that might result from the control over our beliefs can take over our biology and lead us to some interesting conclusions. We will look at these in the following blinks.

> _"Beliefs control biology!"_

### 7. Evolution has equipped us with two basic survival mechanisms: growth and protection. 

Life on earth has endured for billions of years. But how? Our bodies display two types of behavior that have helped perpetuate our existence: growth and protection.

We can see this by looking at our cells. In one study, scientists placed cloned human cells in a culture dish. When the cells were confronted with toxins, they moved as far away as possible. This is the _protection_ _response._ Yet when a nutritious substance was introduced, the cells gravitated toward it. This is the _growth_ _response._

Because these two behaviors are completely opposed to each other, they cannot happen at the same time.

Growth occurs when we are in a healthy state. When we are in a state of protection, though, in response to a threat or a stressor, we don't grow. Being in a state of growth is fairly straightforward. So let's explore the more complicated protection state.

The protection response contains more than one mechanism. One of these mechanisms is the immune system, which takes care of the body's defenses against internal threats like bacteria and viruses.

Then there is the more powerful mechanism; the _Hypothalamus-Pituitary-Adrenal_ _Axis_ (HPA axis), which shields us against external threats but also suppresses our immune system.

The protection response of the HPA axis is based on our nervous system, and is popularly referred to as the _fight-or-flight_ _response._ For example, when we come face-to-face with a lion, our body will either prepare to confront the animal or flee from it.

This mechanism is stronger because of our frequent exposure to external threats throughout the evolutionary process.

The problem is that the HPA axis is rather unsophisticated. In stressful situations, it is easily triggered. This is why we become unnecessarily fearful when we have to give presentations, for example, or sit exams.

So, in order to thrive, we must learn to control the response of our HPA axis, or put simply: we need to control our stress levels.

> _"Between 75 and 90 percent of primary-care physician visits have stress as a major contributing factor."_

### 8. From the point of conception, parental behavior determines how their children will think and act. 

We have learned quite a lot so far, so let's take a minute to recap: we know that signals from the environment impact our cells. We know how the body copes with stress and we are aware that we can reprogram ourselves.

But how can we utilize this knowledge in our own lives? One area where we can apply it is in parenting.

Not many of us realize that the development of a child is affected by its environment right from its conception.

The Darwinian idea of genetic determinism would have us believe that parents are not a crucial partner in a child's development because it is our genes that determine what becomes of us. But research shows that this is false.

During the time a fetus is developing in the womb, it is actually being influenced by its surroundings. Some scientists believe that conditions in the womb can determine whether we are susceptible to poor health, such as contracting diabetes or suffering from neurosis and strokes, later in life.

Parents therefore need to ensure that they give their babies the best possible start by making the womb an ideal place to flourish.

They should, for example, eat a healthy diet and ensure that they "program" or set up their child for the best start in life.

A parent's actions help determine how a child experiences the world, like what things they will be scared of and what things they will feel most comfortable doing. Therefore, they should make sure the child is not programmed with unnecessary fears or stresses.

For example, parents should never label their child as "weak" or "stupid," as these messages will become programmed into the child, a stain that they may carry into adulthood and potentially through the rest of their life.

But this shouldn't scare us. Because we still have the power, even later in life, to program ourselves to override our instincts and achieve great things.

### 9. Cooperation, not competition, is the most effective force for development. 

What is the overarching message of evolution? "Survival of the fittest," perhaps? Actually, a more accurate expression is "make peace, not war." But why is this so resonant?

For billions of years, cells have cooperated and developed systems that have allowed them to survive.

At the start of life on earth, there were many single-celled organisms competing with each other for scarce resources. Eventually they learned that they could achieve much more if they worked together. And so, multicellular life was born.

Now, look at the human body. It holds around 100 trillion individual cells, each living a good life. In a healthy body, each cell has a job and a place to live. Not a single cell is cast out to fend for itself!

Just imagine what human society could achieve if we took some inspiration from the remarkable cell and made cooperating with others a key objective in our lives.

Although it is often thought that humans are genetically programmed to be selfish, selfishness is not apparent in the animal kingdom. And we are not so far removed from our animal cousins.

Even the wild baboon, considered one of the most violent species on earth, is not genetically programmed to only look out for itself.

If even baboons can cooperate, surely people _can_ and _should_ manage to work together.

If we continue to go about the world chasing our own selfish goals, eventually we will run into more and more conflict as our population continues to grow.

The only way we can successfully prepare for the future is to communicate with one another and develop common strategies based on our shared goals and values. After all, we all desire a livable, harmonious planet.

We need to be aware that we no longer have to accept the notion that we are programmed to act selfishly.

> _"When chimps...become agitated, they don't engage in bloody fights, they diffuse their divisive energy by having sex."_

### 10. There is scientific evidence that we are made in the image of the universe and that we live on after death. 

There are some religions that declare we are made in the image of God. For the atheists among us, this concept can be a little hard to swallow. However, if we take "God" to mean the universe or the totality of our environment, there is indeed evidence to suggest that we are formed from the universe or from what some may call "God."

That is to say, the proteins in every cell in our body respond to signals from our surroundings. This informs how they behave and therefore forms our identity. And because we are made up of cells, we can say we are created from our environment.

Being made in this way is not the only spiritual idea that can be supported by science. There is also evidence to suggest that we continue to exist after death.

To explain: our cells' membranes are covered with _identity_ _receptors,_ which make them (and therefore us) unique. Like antennae, these receptors pick up signals from our environment and in doing so, create our identity.

Take the analogy of a television broadcast. Imagine our body is a TV set and our identity is the image that is broadcast onto the screen. If the TV (our body) breaks down, does this mean that the broadcasted image (our identity) is also dead? Of course not — if you get another TV, the image reappears.

So even though our body dies, the imprint of our identity is still present in the environment.

If someone appeared with exactly the same identity receptors as you, they would pick up the same broadcast and you would exist once more.

In summary, to appreciate this spiritual notion, we need to understand the idea that nothing can function in our body without our cells picking up signals from our environment.

> **Fact:  

** When someone receives a donor organ, they can pick up some of the donor's identity.

### 11. Final Summary 

The key message in this book:

**The** **idea** **that** **genes** **control** **our** **lives** **is** **no** **longer** **supported** **by** **the** **latest** **scientific** **research.** **The** **new** **biology** **has** **discovered** **that** **we** **have** **instead** **a** **profound** **influence** **over** **our** **genes.** **Scientists** **involved** **in** **the** **new** **biology** **should** **therefore** **dedicate** **their** **efforts** **to** **better** **understanding** **these** **mechanisms** **to** **help** **humanity** **overcome** **its** **problems.**

Actionable advice:

**Don't** **automatically** **accept** **the** **same** **medical** **treatment** **as** **everyone** **else.**

If you or anyone you know are diagnosed by a physician as having a so-called incurable disease or disorder, remember that it depends on the method the physician applies as to whether the disease is curable or not. We are all unique; thus many alternative approaches to illness should be explored so we can discover the most effective treatment methods.

**Deal** **with** **stress** **and** **negative** **programming** **by** **meditating.**

When you are stressed, your immune system can't function properly. So the next time you are in a situation of chronic stress, consider meditating. This also helps reduce negative subconscious thoughts (programming) that can wreak havoc on our health.
---

### Bruce H. Lipton

Stem cell biologist Bruce H. Lipton, Ph.D., is a key figure in new biology. Known for his work combining science and spirituality, he has taught Cell Biology at the University of Wisconsin and conducted leading-edge research at Stanford University's School of Medicine. In 2009, he received the Goi Peace Award.

