---
id: 519de7e0e4b0a17389a8ac10
slug: the-black-jacobins-en
published_date: 2014-01-22T11:45:06.000+00:00
author: C.L.R. James
title: The Black Jacobins
subtitle: Toussaint L'Ouverture and the San Domingo Revolution
main_color: 2353B1
text_color: 2353B1
---

# The Black Jacobins

_Toussaint L'Ouverture and the San Domingo Revolution_

**C.L.R. James**

_The Black Jacobins_ traces the remarkable history of the revolution in the French colony of San Domingo (modern day Haiti). It describes the events that helped the revolution become the first successful slave rebellion in history.

In particular, _The Black Jacobins_ views the events through the prism of the revolution's greatest figure, Toussaint L'Ouverture. It shows how he, a former slave who was inspired by the ideals of the French Revolution, successfully defeated the European empires and helped to destroy the brutal practice of slavery in San Domingo.

---
### 1. The very prosperous French colony of San Domingo was sustained by the brutal practice of slavery. 

In the late seventeenth century, France acquired the colony of San Domingo (modern-day Haiti) in the West Indies. Over the next century, this colony was to prove very prosperous for the French. Its soil was fertile, allowing crops like indigo, cotton, sugar and coffee to be grown.

Yet, there was a problem: to grow and harvest these crops, a huge amount of labor was needed, and this wasn't readily available. The original native population of the island had all but been wiped out by European colonists, and the climate made it almost impossible for European laborers to carry out the back-breaking work.

The solution to this problem was to use African slaves. In San Domingo, along with the other European colonies in the Americas, huge numbers of African people were forcibly transported from their homelands to work as slaves. The scale of the slave trade was enormous; by the late eighteenth century, there were about half-a-million slaves in San Domingo alone.

Life for the slaves was incredibly brutal. Although the French government laid down rules about how they must be treated, these rules were more often than not ignored. The colonists saw the slaves as their property to do with as they wished. They forced the slaves who worked in the fields to do incredibly exhausting tasks from dawn until dusk in the intense heat.

They also subjected their "property" to the most inhumane treatment. Punishments for even the smallest of misdemeanours were harsh, and whippings and beatings became very common. For worse crimes, the punishments were horrific; for example, there were even cases of slaves being filled with gunpowder and blown up.

This violent and degrading system kept the colony of San Domingo prosperous in its raw materials, allowing many settlers to become very rich.

### 2. The “free” population in San Domingo was made up of various competing social classes. 

In San Domingo, the slaves did all the work, which made the colony prosperous. Above them were a whole host of social classes which profited from this labor.

At the top of the social structure were members of the French bureaucracy, which included the governor of the island and his administrators. They ran the colony on behalf of the French crown. 

Then there was the "white" population of San Domingo, which numbered approximately 30,000 people.

The most prosperous colonists were the _big whites._ These included the Europeans who owned the plantations on which the slaves worked. They became very rich from the crops that were produced. Also included in this class were the merchants who traded both the slaves and the crops the slaves produced. They disliked the bureaucracy because it had the monopoly on power in the colony. 

Under the _big whites_ social class were the _small whites._ These were the poorer members of the European community and included the managers of the slave estates, tradesmen and professionals like draftsmen and carpenters, as well as unskilled laborers. Many of these people came to the colony because here the color of their skin made them important. In Europe they were seen as the lower classes, but in San Domingo they held power over the people of colour.

Then there was the mixed-race and "free" black population, which numbered about 40,000 people. Although many of these people were hard working and prosperous, they were denied rights or certain positions in society due to the colour of their skin. For example, a white man could trespass on the property of a mixed-race man and even seduce his wife, but if the mixed-race man complained, he ran the risk of being lynched. Resentment therefore built up in this class at their lack of rights and privileges.

### 3. Although the slaves in San Domingo had often fought against the system, they lacked the leadership needed to succeed. 

For as long as the system of African slavery had been in place, the slaves themselves had resisted it. It is a common misconception today that slaves were servile and resigned to their fate; in fact, many were not.

At every stage of their journey from capture in Africa to working on the plantations, slaves fought against their captivity. For example, at the port in Africa where the slaves were loaded onto the ships bound for the Americas, so many fought back that they had to be chained together for the entire journey. 

On the plantations too, many refused to live a life of slavery. Suicide was common; many thought that it was better to die and deprive the owner of his property than to be forced to live as a slave. Some managed to run away. In the mountains and woods of San Domingo, these escapees, known as _maroons_, formed together in groups and terrorized the colonial settlers.

Yet, despite these acts of resistance, the slaves couldn't hope to overturn the system of slavery. This was partly because the slaves lacked a strong and intelligent leader to unite them. Although they vastly outnumbered the colonists, they needed someone to bring them together as a powerful force. Also, the system of slavery itself was too strong. The French were dependent on it for their prosperity and so they maintained it ruthlessly.

For a slave revolt to succeed, two things needed to happen: the slaves would need to find a suitable leader, and the commitment of the French government to slavery would have to weaken.

### 4. The French Revolutionary ideals of liberté, egalité and fraternité spread to San Domingo with explosive results. 

The French Revolution, which began in 1789, had three main ideals: _liberte, egalite et fraternite_ (liberty, equality and fraternity) _._ It was largely in pursuit of these ideals that the French middle- and lower-classes challenged the autocratic French monarchy.

The ideals soon spread to San Domingo, and with them came the revolution. The poorer white colonists along with many of the richer ones challenged the French bureaucracy for power. They demanded, as their brothers in France had, increased political and economic rights. And as there were very few French royal forces on the island, they soon overcame any resistance.

Yet, a problem soon arose: The three revolutionary ideals claimed equality and liberty for all, not only for those with white skin. If the revolution was to remain faithful to these principles, then shouldn't the mixed-race population of the island get equality? And, further still, shouldn't the slaves get their liberty?

The colonists on San Domingo couldn't let this happen. They were greatly outnumbered by the slave and mixed-race population and were reluctant to let their position of complete dominance slide. For them, the revolutionary ideals must stop at the colonists' liberty and equality and go no further.

However, they couldn't stop what they had begun. The ideals of the French revolution had been picked up by the people of colour throughout the colony, and the mixed-race population was the first to rebel and demand their rights. Under a prosperous mixed-race leader called Ogé, they launched a rebellion.

They weren't strong enough, however; the rebellion was brutally quashed, and Ogé was tortured to death.

This initial rebellion might have been crushed, but the ideals that inspired it had not. Very soon, the slaves began their own desperate fight for _libert_ é _, egalit_ é and _fraternit_ é _._

### 5. In Toussaint L’Ouverture the slaves found someone with the leadership qualities to challenge the system of slavery. 

The French revolution had challenged the ideas of slavery and racism with its central aim of _liberte, egalite_ and _fraternite._ Now the slaves needed a strong leader to unite them and turn them into a powerful force.

They found this leader in a slave called Toussaint L'Ouverture.

Toussaint was born a slave in San Domingo, but he had the fortune to be born into a relatively privileged family of slaves. This was vital in his later life as it allowed Toussaint to gain an education, something beyond the means of most slaves. This spared him from having to work in the fields; instead, he was given the job of steward of livestock on his plantation, a role usually carried out by a white man.

This job helped Toussaint become a great leader, providing him with crucial skills in administration and experience in working with men of power. Additionally, as his education gave him the ability to read, he was able to learn about historical leaders and their strategies. For example, he is known to have read some of Julius Caesar's works.

Another great leadership skill Toussaint possessed was a strong command of his own mind and body. Through childhood and adolescence, he was known for his frailty; his nickname was even "Little Stick." But he worked to overcome this, and as an adult he instead became known for his strength and stamina. His mental strength was just as great. He never wavered from his ideals and never compromised on them either. Such qualities in strength of character and physique are vital for leadership.

These qualities and experiences meant that in Toussaint L'Ouverture, the slaves on San Domingo had found someone who could challenge the brutal practice of slavery and lead them to freedom.

### 6. In 1791, the slaves of San Domingo rebelled, and Toussaint L’Ouverture soon became one of their leaders. 

After seeing the white and mixed-race colonists take up arms for their own freedom, the slaves of San Domingo, the lowest members of colonial society, fought back. In July, 1791, they revolted, overrunning the plantations and villages of the colonists in many areas of the island.

The slaves were ruthless in their destruction. The brutal system of slavery had taught them the power of repression, violence and murder, and in their rebellion they replied in the same spirit. Terrible violence was committed by both the revolting slaves and the colonists who resisted them.

The slaves did have initial successes, but these victories had everything to do with their superior numbers rather than good organization and leadership. Many of the rebellious slaves wore rags, some were naked, and there were few modern weapons. Their military strategy was also weak; for example, some slaves bravely put their arms inside loaded cannons after the fuse was lit in order to pull the balls out, a tactic that could not possibly work.

What was needed was a well-ordered and disciplined army, and Toussaint L'Ouverture was to help provide one.

Toussaint had joined the rebellion as a physician, but he soon left this post to become a military commander. He set about recruiting and training a formidable army, and began by hand-picking a few hundred men, whom he turned into a well-drilled military unit. These soon became the best revolutionary soldiers on the island. Eventually this unit was to grow in size as more and more slaves joined what they saw was the strongest army in the revolt.

From this position, Toussaint grew in strength as leader of the San Domingo slave revolt, and he soon became its main military commander.

### 7. Toussaint L’Ouverture never sacrificed his aim of the abolition of slavery on San Domingo. 

Very early in his role in the San Domingo revolution, Toussaint L'Ouverture determined that he would never sacrifice or compromise on one central ideal: he would settle for nothing less than the abolition of slavery.

This important central idea influenced his actions throughout the revolution. In fact, because of it, he and his armies fought alongside different "sides" throughout the conflict. For example, early on in the rebellion, the French government had backed the colonists against the slave revolt. To give his armies the best chance of succeeding against the French, Toussaint allied himself with the Spanish monarchy, which occupied the eastern half of San Domingo (modern-day Dominican Republic).

Toussaint didn't maintain this alliance for long. As the revolution in France became more radical, the French government's position towards slavery changed. In 1794, the French government abolished slavery, and Toussaint took no time in changing sides to fight for the French. He would, from then on, protect this freedom vigorously.

To this end, Toussaint never really trusted any of the European powers. He knew how profitable slavery was to them. They might say that they would help the slaves in their struggle for freedom, but how long could they be trusted? He knew that many of them would use friendship only as ruse to get themselves established on the highly prosperous island. To Toussaint, it seemed highly likely that one of them would capture it for itself and reestablish the brutal practice of slavery. 

Because of his cherished central aim, Toussaint L'Ouverture wouldn't rest until the slaves had their freedom, and he wouldn't flinch in his protection of it. This meant that he wouldn't let himself or his army be controlled by the European powers, a decision which, in time, would lead to the end of slavery forever on San Domingo and the island's eventual independence.

### 8. Toussaint L’Ouverture soon became the leading figure in San Domingo. 

After the French Republic declared that slavery was abolished, Toussaint L'Ouverture allied himself with them. He worked under the colony's French rulers, including a charismatic commissioner called Sonthonax who was popular among the black population. Together, they made for a powerful combination.

One of the enemies they fought together was Britain, the global superpower of its age. The British had their greedy eyes on the riches of San Domingo and were prepared to fight the French for it. Toussaint and his armies fought bravely against the British and were able to inflict defeat after defeat upon them. This resistance, coupled with diseases such yellow fever, cost the British just under 100,000 men, and in the end they were forced to give up their campaign.

Toussaint fought equally as hard against France's internal enemies in the colony. For example, in 1796 there was a rebellion against French rule by some rich mixed-race leaders. They, protective of their riches, decided to take British help and launch a coup d'etat against the governor. Toussaint helped ensure that this attempt failed, and in gratitude the governor made him his deputy.

The power and prestige of Toussaint L'Ouverture had grown exponentially. He had moved from being a slave to being the deputy commander on the island. He had a large and powerful army which had defeated the most powerful empire on the globe, the British Empire. Most importantly of all, he had the support of the masses. The former slaves looked to him to preserve their liberty; _he_ was _their_ leader.

And his rise didn't stop there. In 1797, he expelled his former ally, Sonthonax, from the colony. His reasons for doing this aren't clear, but the result was that Toussaint became the most powerful force on the island. His assent had reached its peak, and the former slave had no equal in San Domingo.

### 9. Toussaint L’Ouverture attempted to create a prosperous and highly cultured society in San Domingo. 

It took a few more years for Toussaint L'Ouverture to fully establish his dominance over San Domingo. During this period, he had beaten another mixed-race rebellion and conquered the Spanish colony on the eastern half of the island, freeing the slaves there. It was only after this that he could begin to concentrate on reordering the society of San Domingo.

The difficulty for Toussaint was that the island had gone through 12 years of brutal civil war. In total, two thirds of the white population had been killed or left the island, and about 200,000 black people had died. A great many of the plantations and farms that had made the colony so prosperous lay in ruins.

What could he do to transform this war-torn island for the better?

His first aim was to reestablish agriculture across the island. To do this, he had to force many of the former slaves back into the fields. Although many of them didn't want this, it was vital to for a secure economy. There was, of course, one major difference from the former system: the workers were now treated fairly and rewarded for their efforts.

He also reformed the island's administration and promoted free trade by abolishing many unnecessary duties and taxes. He also strengthened the rule of law on the island, setting up new courts as well as a maritime police force to curb smuggling. 

Finally, Toussaint promoted a civilized and peaceful society. He was a devout Catholic and expected others to follow his strict morals. For example, he encouraged his soldiers and officials to marry and to refrain from having concubines. To this end, he also had schools built throughout the island to educate the masses.

### 10. Toussaint L’Ouverture’s desire to create a multi-racial society in San Domingo alienated some of his black supporters. 

It might be expected that Toussaint, a former slave who commanded an army made up mostly of former slaves, would seek his revenge on the whites.

Yet, throughout his leadership of San Domingo, he tried to protect the position of the white population in the colony. He promoted a position of racial tolerance on the island. For example, he would often be confronted by black supporters asking why white people still held positions of power. He would answer by mixing red wine and water in a glass and then, showing them the results, state, "We must all live together."

This conciliatory attitude seems to have been born partly from a hatred of unnecessary bloodshed. Throughout his role in the rebellion, he was lenient with enemies compared to the other commanders.

Yet something else also motivated his attitude towards the white population: he was aware that they were the most experienced and capable members of San Domingan society. With this in mind, he encouraged them to play a variety of roles in his administration. For example, many officers in the army were white. Also, driven largely by his desire to improve agriculture, he allowed many of the white plantation owners to keep their estates.

However, while Toussaint understood the need to work with the white population, many of his supporters did not. What they saw was the very people who had kept them as slaves or fought against them in the rebellion being allowed to keep their lands and wealth. They thought this unfair.

This unhappiness grew, and there was a rebellion by some former slaves against the white population in 1801. General Moïse, one of Toussaints nephews and most loyal commanders, was implicated in the rebellion and thus executed by Toussaint. He had been a popular figure among the black population, and his death further weakened support for Toussaint.

### 11. Under Napoleon, the French government invaded San Domingo with disastrous results for Toussaint L’Ouverture. 

Just as the position of Toussaint L'Ouverture was being weakened by a loss of support, a French army of 20,000 men sailed to San Domingo. This force had been sent by Napoleon Bonaparte, who had become the dictator of France.

Despite the independence of his actions, Toussaint had never severed the link between San Domingo and France. He respected French culture and the ideals of the revolution, and above all he cherished the 1794 decree of the French Republic abolishing slavery.

But, this was now a very different French government. The revolutionary ideals had died with the rise of Napoleon's military dictatorship. There were also plans, kept secret at first, to bring back the highly profitable system of slavery to San Domingo. Toussaint, while not aware of this specific plan, knew that the French army were coming to subdue the population of the island; therefore, he resolved to fight it.

Unfortunately for him, his actions in executing Moïse had weakened his support among the masses. Thus, after the French army landed, it quickly gathered a foothold and support. Although Toussaint's forces fought bravely and inflicted heavy losses on the French forces, they were gradually worn down. Toussaint was shocked by the destruction that the war was having on San Domingo.

In the end, he was impelled to make peace, and so he surrendered. He was given leave to retire to his land in the countryside. However, this freedom was not to last.

On June 7, 1802, Toussaint was arrested by French forces and put on a ship bound for France. But Toussaint never received a trial in France; instead, he was taken to the cold and desolate Fort de Joux. Here, his health was deliberately allowed to deteriorate until, on April 7, 1803, he died.

### 12. The arrest of Toussaint and French attempts to restore slavery led the population of San Domingo to declare independence. 

After Toussaint L'Ouverture's arrest in 1802, the masses in San Domingo grew wary. Whatever many felt about his actions towards the end of his period in power, he was still seen as the man who had given the slaves their freedom.

French actions on a neighboring island were soon to make the situation much worse. Just as they had in San Domingo, the French overthrew a local leader in Guadeloupe, another of their colonies. But this time, they also began to reinstate slavery. Once this news got to San Domingo, the tense atmosphere on the island exploded into full-scale rebellion.

Freedom and liberty were things the people of San Domingo were prepared to protect at all costs, and so they launched themselves into revolt.

This rebellion, led by Toussaint's former subordinate Dessalines, harried the French forces across the island. As they pressed on one side, disease pushed from the other. The French forces were shrinking: of the 34,000 troops that had landed on San Domingo, 24,000 were dead and 8,000 were in the hospital. Their position was untenable.

So once again, an army made up of ex-slaves had defeated a European power. Yet, unlike Toussaint who had tried to maintain the link with France, Dessaline decided to cut it forever. San Domingo was to become an independent nation: the nation of Haiti.

There was yet another difference between the leadership of Dessalines and the leadership of Toussaint. Whereas Toussaint had promoted conciliation, Dessalines resolved on vengeance. In 1805, he ordered the massacre of the white population in Haiti. The island now had its freedom, but not in the way that Toussaint L'Ouverture had envisioned it.

### 13. The slave revolution was the result of the ideas of the French Revolution and the unique skills of Toussaint L’Ouverture. 

We often think of the French Revolution purely in European terms. We think of the terror of the guillotine in Paris and the battle between effeminate aristocrats and French peasants dressed in rags. We rarely think about its impact on a wider level.

Far from just being a European affair, the French Revolution inspired people across the globe. The revolution in San Domingo is a perfect example of this. It was here that the ideas of _libert_ é _, egalit_ é and _fraternit_ é propelled one of the most oppressed populations in history to rise up and win their own freedom.

Yet this could not have been achieved without another factor: Toussaint L'Ouverture. If the French Revolution provided the impetus for rebellion, then he provided the strong leadership to carry it through. Without him, there is a strong chance that the revolution would have petered out or maybe compromised on central issues like the total abolition of slavery.

Toussaint not only had the leadership qualities that made him a strong military commander, he also possessed skills that enabled him to out-think his enemies, who were usually far more educated. Many of these enemies thought that a black person, especially a former slave, was incapable of leadership, and so they underestimated his abilities, especially on the field of battle, which often led to their defeat. 

Together, the French Revolution and the leadership of Toussaint L'Ouverture led to the first successful slave revolt in history, which helped end both the brutal practice of slavery and the idea that only white people were capable of leadership. This is how revolution and Toussaint changed the world by inspiring enslaved people across the globe to call for their own liberty and freedom.

### 14. Final summary 

The key message of this book:

**Toussaint L'Ouverture led the first successful slave revolt in history. His unique skills of leadership combined with the powerful ideals of the French Revolution inspired the slaves of San Domingo to rebel against their oppressors. He enabled this poor and subjugated community to defeat some of the richest and most powerful societies in the world, and in doing so win their freedom.**

**Actionable advice**

**If you want to know why people rebel, look at the San Domingo revolution.**

Many people are scared of revolutions. They see them as dangerous events initiated by tyrants and demigods. Yet, of you look at the San Domingo revolution you can see the real reason why people rebel. They don't do it for their own power or for the love of destruction, but for the liberty of themselves and their community. Once you see this, you can begin to see the same thing happening in revolutions and rebellions across history and even in the modern world.

**Don't rely on one account of history; look at events from every side.**

Today, many histories laud the abolition of slavery as a great event in European history. They teach us about politicians in the parliaments of Europe acting on humanitarian principles to rid the world of the evil of slavery. In doing this, they miss out the duplicity of the European governments at the time. Britain, for example, abolished slavery only when it was no longer profitable for them. In France, after the abolition of slavery, Napoleon tried to bring it back. This shows that you shouldn't rely on any one history; make sure to look at many accounts instead.

**Never underestimate the power of a great idea**

Powerful ideas can change the world. The three central aims of the French Revolution, _liberte, egalite et fraternite_, inspired people across the globe to act. From the peasants and artisans in France to the slaves of San Domingo, people were called to action by these noble ideals.
---

### C.L.R. James

C.L.R. James (1901–1989) was a Trinidadian academic and writer. He was the author of many books on Marxism, history and cricket. He was a pioneer in the field of postcolonial literature and an influential political activist.

In recognition of his work, he was awarded Trinidad and Tobago's highest honour, the Trinity Cross.

