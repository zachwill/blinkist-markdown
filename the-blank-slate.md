---
id: 5a65b674b238e100076c4ece
slug: the-blank-slate-en
published_date: 2018-01-25T00:00:00.000+00:00
author: Steven Pinker
title: The Blank Slate
subtitle: The Modern Denial of Human Nature
main_color: 8B571D
text_color: 8B571D
---

# The Blank Slate

_The Modern Denial of Human Nature_

**Steven Pinker**

_The Blank Slate_ (2002) is about the huge role that evolution and genetics play in making us who we are. Steven Pinker makes a strong case against the belief that everyone is born a blank slate and influenced only by their upbringing, arguing instead that biology is a far more important factor in shaping our behaviors and personalities.

---
### 1. What’s in it for me? Find out what’s really going on in human nature. 

The question of nature versus nurture has been around for a very long time already, and it may never completely be resolved. These blinks will take you deep into the world of behavioral genetics to examine the wealth of evidence suggesting that much of who we are, from our political beliefs to our inclination toward violence, is inherited.

This emphasis on genetics goes against one of the more popular theories of the past century, the Blank Slate. According to this theory, we're all born innocent and equal, with no personality or behavioral traits. But as you'll find out, there's very little evidence to support this theory or the similar ones that claim our mind isn't a product of generations of evolution.

In these blinks, you'll discover

  * why genetics has been a political and religious issue;

  * how identical twins have been such a source of insight; and

  * why our imperfections needn't be such a big deal.

### 2. There have been three popular but faulty theories about human nature. 

The mysteries of the human mind have proved a fascinating subject for millennia, as we've striven to understand and anticipate people's behavior.

Many of our older, popular theories about human nature came from religion: In Judeo-Christian tradition, for example, the mind is considered separate from our physical form and lives on after the body dies.

These days, science is less influenced by religion, and we've ended up with three main theories regarding the nature of the human mind.

The first is known as _the Blank Slate theory_, and it's often attributed to seventeenth-century philosopher John Locke. It suggests that there is no inherent human nature — that we're born with a clean slate, so to speak, and everything that makes us who we are gets picked up along the way.

This theory emphasizes the role of social influences in forming the customs, thoughts and behaviors we exhibit. So, any behavioral patterns that groups of people share, whether they're of a particular race, ethnic group or gender, are all gained through experience. This means that if an individual grew up with another set of parents and a different education routine, _and_ was exposed to different media, they would turn out to be a completely different person.

Another popular belief is known as the _Noble Savage theory_, which is often attributed to the eighteenth-century French philosopher Jean-Jacques Rousseau. He believed that humans are inherently selfless and peaceful, but this natural state gets corrupted by civilized societies that promote greed and lead to violence.

The third concept is known as the _Ghost in the Machine theory_, which dates back to the seventeenth-century and the philosopher René Descartes. He believed that humans are composed of two separate systems, one that relates to the physical body and one that relates to the mind. People who adhered to this belief thought that the mind was complex in ways that simple mechanical terms couldn't explain.

The Blank Slate, the Noble Savage, and the Ghost in the Machine are also known as _empiricism_, _romanticism_ and _dualism_. But as we'll see in the next blink, there's another theory that can explain how the mind works.

### 3. Cultural behaviors can be the result of evolution and genetics. 

For much of the twentieth century, social scientists were attracted to the Blank Slate theory since it meant that humans didn't have any predisposition to be racist or sexist. Any display of these behaviors could be blamed on some outside force causing people to learn them. With the Blank Slate, cultural behavior could exist neatly separate from the realm of the brain, genetics and evolution.

But today we know this is not the case. Cultural behavior is indeed part of our evolutionary development, the same remarkable process that allows every creature to survive, thrive and pass on their genes to the next generation.

It's true that some specific forms of cultural behavior are arbitrary, such as whether a nation's people drive on the left or right side of the road, but there are underlying reasons behind these decisions too. After all, in situations like driving, it's in everyone's best interest that people act in a coordinated fashion. Therefore, we can see cultures as a collection of practices and conventions designed to coordinate our lives.

Blank Slate theorists have suggested that the mind contains nothing when we're born, but when we look at what we learn early on, we can see that this is not the case.

Language is perhaps the best example of a learned cultural skill, and it also shows us that a child must be born with more than a blank slate. If the mind were blank, all it would be capable of doing is passively recording sight and sound like a video camera. But a child must have a mental inclination for extracting meaning and purpose from the words they hear since they will soon understand and form new sentences that they've never heard before.

This is just one of the differences between a child and a parrot. While a parrot repeats only the exact words it hears, a human child comes with a genetic disposition to learn the nuances of language.

As we'll see in the next blink, our genes are only part of the story; environmental conditions can also play a part in our cultural behaviors.

> _"The idea that minds are shaped by culture served as a bulwark against racism and was the theory one ought to prefer on moral grounds."_

### 4. Modern science has found bridges between the physical world and the mind. 

Over the past few centuries, science has made a series of amazing breakthroughs in unifying various fields of knowledge to explain life's mysteries.

It began in the seventeenth century with Isaac Newton, who broke down the wall between the Earth and the cosmos with his law of gravity. This law perfectly described how all objects in the universe move.

In the nineteenth century, another wall fell, this one between the living and the dead, as scientists discovered that life doesn't require any magic or supernatural spark. All it takes is the right combination of ordinary chemical compounds.

This brings us to the last wall standing — the one between the physical world and the mental world. However, this barrier, along with the Blank Slate theory, is starting to crumble away.

In recent years, science has been building bridges between the worlds of matter and the mind.

One such bridge is cognitive science or the science of mind.

This field was founded in the 1950s on the understanding that it's impossible for the mind to ever be a blank slate since this would imply that the mind isn't doing anything when we're born, and we know this isn't the case. As with language, you can try to teach or inscribe all kinds of things upon a blank slate, but it can't be put to use without some underlying system to sort and apply this information.

Another bridge is behavioral genetics, a field that explores the different ways genes affect our behavior.

Scientists in this field have determined that there's no such thing as a blank slate since we're born with certain genes that determine how smart, shy or happy a person is — to name just a few distinguishing characteristics.

Perhaps the best example of this is identical twins who are separated at birth. Even though they're raised in different environments, they'll talk alike, have the same aptitude for math and be just as introverted or polite as one another.

Facts like this also contradict the Ghost in the Machine theory, since identical twins will even grow up to prefer the same brands of cigarettes, indicating that even such preferences stem from genetics, not from one's upbringing.

As for the Noble Savage theory, this too is crumbling under the evidence for genetic personality traits. A number of studies have shown that people inherit a certain willingness toward antisocial behavior such as thievery, violence or heightened aggression, so it is not merely society that teaches these.

### 5. The defenses of the Blank Slate theory are not sound. 

You might think that these advancements in genetic science would be enough to sink the Blank Slate theory for good. But those who latched onto this theory aren't ready to give up yet.

In 2001, when the entire human genome was first decoded, blank slate theorists thought they had the proof for which they'd been searching. Many scientists were surprised when the full genome was published, and it only contained 34,000 genes, instead of the 50,000 to 100,000 genes that most geneticists were expecting.

Those clinging to Blank Slate thinking were quick to point to this smaller number as proof that human beings aren't so complex, and therefore the mind might indeed be similar to a blank slate. After all, 34,000 is only twice as many genes as a roundworm!

However, the number itself is misleading. When you consider all the possible combinations and different interactions these genes can make, the true complexity becomes apparent.

But that's not all; two further defenses attempt to show how humans could be born with a blank slate and develop more complex minds as they grow.

The first defense is known as _connectionism_, which suggests that brain's neural networks are similar to computer models that can learn and grow connections by recognizing patterns.

But as artificial intelligence has shown us, there are limits to connectionism that don't apply to humans. For example, humans can tell the difference between the general idea of something, like an object or an animal, and different examples within that group. If a person sees a duck swimming and an image of Donald Duck, they'll recognize both of these as valid representations of a duck, which is something an artificial-intelligence system using connectionism can't do.

A similar blank slate defense is _neural plasticity_, where the brain changes its form over a lifetime.

In violinists, for example, the area of the cortex related to controlling the left hand's movement will be more developed than in other people. However, there's no evidence that learning a skill like playing the violin will change the behaviors to which genes have been linked. For example, gay people can't learn to be straight, or vice versa.

### 6. Many of the defenses for the Blank Slate theory are based on politics and religion. 

One of the reasons the blank slate theory is so appealing is that it means everyone is truly born equal. That's why the fact that we're born with biological differences has been met with serious political opposition.

In the 1970s, there was a _radical science_ movement from the left that attacked the possibility of human nature being more complex than the blank slate.

These "radical scientists" invoked Marxist theory and claimed that efforts to disprove the Blank Slate theory were actually part of a conspiracy to support a discriminatory political system. Therefore, any evidence of genetics playing a role in shaping the brain was dismissed as "determinism" and "reductionism."

But no sensible scientist believes that our genes are a hundred percent responsible for our body and mind, or that any one set of genes can determine the character traits of every human being.

The Noble Savage theory got renewed support in the 1970s from other radical scientists who opposed the idea that anyone might have a genetic predisposition to violence.

In his 1975 book, _Sociobiology_, the esteemed biologist E. O. Wilson noted that tribal warfare was a common occurrence in prehistoric times. But this claim was vehemently rejected by radical scientists who said that anyone willing to suggest that one tribe wiping out another tribe was representative of human nature must be in favor of genocide.

As for the political right, they were most vocal in defending the Ghost in the Machine theory and its implication that the mind could live on after the body.

This idea is in line with religious groups like Christian fundamentalists, who were particularly vocal in its defense.

Naturally, anyone who refuses to believe in evolution will refuse to believe that a human mind is the result of an evolutionary process. And those who believe the mind contains an ethereal soul will hardly be inclined to agree that thoughts and feelings are the result of a chemical process in the brain.

### 7. Abandoning the blank slate provokes fears of inequality and imperfection. 

It can be troubling to face the reality of a situation, which is exactly what abandoning the blank slate requires. In the 1970s, centuries of slavery followed by the horrors of the Holocaust were still fresh in the minds of people around the world. Little wonder that the Blank Slate theory offered a comforting vision of equality.

Instead, people had to face the fact that people are innately different, and this gave rise to many fears that genetics would be used to justify inequality.

One of the biggest fears relates to _Social Darwinism_. People worried that differences in crime rates and income levels would be used as evidence to justify social discrimination and to suggest that certain people are inferior to others.

But just because there are small genetic differences among sexual, racial and genetic groups, doesn't mean we have to opt for Social Darwinism.

First of all, someone's genetic differences are far from being the _only_ factor determining their social status. And even if someone was born with a genetic disadvantage, it's the responsibility of a just society to have social programs in place to help this person, not discriminate against them.

Another fear that comes with abandoning the blank slate is dealing with _imperfectibility_.

If humans are born with a certain tendency toward immoral and selfish acts, and this is their "natural" inclination, why bother fighting it?

The idea that men are born with selfish sexual impulses that can result in sexual assault is a primary concern in feminism. But even though one could argue that certain abhorrent behavior might be considered "natural," it is also "natural" for women to not want to be forced into having sex. Our value system is based on the idea that a person's desires aren't more important than the right to control one's own body. So, just because an urge could be part of human nature, doesn't mean it must be tolerated.

### 8. Abandoning the blank slate also evokes fears of determinism and nihilism. 

Another comforting aspect of the blank slate was the amount of control it provided over how a person turned out. With perfectly loving parents and the best schooling, an infant was guaranteed to become a model child — in theory, anyway.

But once we acknowledge the important role genetics plays in shaping our behaviors, we're faced with an existential dilemma of how much control we really have over our destiny. Are we doomed to turn out like our mothers or fathers?

This brings us to another fear: _determinism_. This one raises the question of how much accountability we can place on someone if their actions are determined by their genetics. If someone is born with an inclination toward violence, then wouldn't biology serve as the ultimate excuse for their misdeeds? Wouldn't law and morality become obsolete?

These were the questions some scientists were asking as the Blank Slate theory was abandoned. They argued that no behavior was completely predictable and that only levels of probability exist.

But again, the discussion was largely missing the point, since people were confusing the need to _explain_ a person's behavior with _excusing_ that behavior. There's a big difference between the two.

Our understanding of human nature shouldn't be seen as interfering with our understanding of what's right and wrong. We have laws and a sense of morality in place to deter and punish those who perform harmful actions, and there's no need to think that our legal and justice systems will become obsolete due to a better understanding of why those actions occurred.

The final, and perhaps most existential, fear that stems from this new understanding is _nihilism_. If our inherited genes are responsible for so much, and we're nothing but machines designed to pass these genes on to another generation, it's hardly a vision that makes life worth living.

People generally strive for a much higher truth and meaning for life, and the cold hard facts of biology can't provide this. But this doesn't mean one has to fall into the black hole of nihilism.

The biological impulses in our genes might provide a basic purpose to life, but they don't have to replace the search for higher meaning. If you strive for the pursuit of happiness and a more satisfying meaning to life, there's no reason for this urge to be negated by biology.

> _"The starting point for acknowledging human nature is a sheer awe and humility in the face of the staggering complexity of its source, the brain."_

### 9. The human mind is great at categorizing, but this can lead to racism. 

With a hundred billion neurons linked by a hundred _trillion_ connections, the human brain is an organ of staggering complexity. But what's it all for?

The brain's primary purpose is to help us process the world around us in a way that gives our species the best chance for survival. But more than that, it also constructs reality for us.

In keeping us safe and building a reality, the human mind puts things into categories. But there are differing opinions as to precisely how reality is constructed.

Some theorists suggest that the majority of our perceived reality is a social construct, and this applies to, for example, human stereotypes. They say that racism exists because society continues to perpetuate racial stereotypes that we continue to categorize in our brain as reality. Therefore, to end racism once and for all, they suggest we only need to deny that it exists and the social construct will collapse.

It's true that some things like money, tenure and citizenship are socially constructed concepts — they exist because everyone agrees they do. But some stereotypes, like the ones that fuel racism and sexism, are a different matter, as they stem from the way our brain operates.

Our brain is designed to put things into categories quickly, and this extends to whether we perceive someone's intentions as being good or bad. The brain also likes a good shortcut, so it'll quickly agree that all arts students are more liberal than business students, for example, and so a stereotype is created. A similar process underlies racist and sexist stereotypes.

And while some stereotypes can be backed up by statistics, others simply stem from our brain's drive to categorize things and people. Either way, they are not mere social constructs.

While the brain is great at putting things and people into categories, it isn't so great at making sense of the abstract world.

We have no intuitive understanding of modern physics, mathematics and the world of genetics because abstract modern science is too new. It hasn't been around long enough for people to develop this skill and pass it on genetically to their children.

Education has been developed precisely to fill in this gap.

### 10. Genetics dictate our strongest desires to help, and our moral emotions can be irrational. 

Over millennia, different groups have tried to put blanket labels on humanity. Classical economists have characterized humans as amoral and driven only by self-interest, while utopian socialists have argued that humans want to look out for one another and be united in one cause.

The truth is, neither of these labels is a perfect fit. We're not exactly selfish egoists or self _less_ altruists.

Our social needs and desires have evolved along with everything else, and they're formed due to the fact that people who cooperated as part of a tribe had better chances of survival than misanthropes. For this reason, natural selection has given us a mind capable of genuine compassion and emotion, but this compassion has its limits, and it isn't necessarily in favor of communal living.

Since our biological imperative is to pass on our genes, the more closely we're related to someone, the more inclined we are to help them.

If we look at the history of Israeli kibbutzim, we can see the limits of our communal desires. These kibbutzim were designed to be perfect socialist communities, but in some ways, genetics eventually proved stronger than ideals. For example, parents wanted their children to sleep near them and not in a communal dormitory.

It's also important to acknowledge that our elaborate moral sense, impressive as it may be, isn't immune to errors and quirks.

Consider this story: After a family's dog is killed by a car, there is a moment of mourning, and then they consider what to do with the body. Since dog meat is considered a delicacy in some nations, they decide to clean, prepare and eat the dog for dinner.

Your immediate reaction may be to judge this behavior as wrong, but can you rationally explain why? Any moral philosopher would likely say that there's nothing wrong with this since everyone is consenting to the act and no one is being hurt.

Oftentimes, our opposition to someone's actions — even when no one is being hurt by them — is hard to rationally explain. That's because our judgment isn't based on someone being hurt, but rather our evolved moral emotions.

### 11. Political preferences and violent tendencies can also be traced to genetics. 

As you can tell by now, the debate over the science of human nature has been a crowded minefield. There are religious implications as well as issues of sexual identity. And there's also the curious fact that our political leanings may be hereditary.

Remember the behavioral similarities between identical twins who were separated at birth? Not only can they grow up to prefer the same cigarette brand, but they also tend to have the same political preferences.

More precisely, on a scale from -1, which is completely opposed, to +1, which is completely identical, the political views of identical twins come in at an average of 0.62.

This isn't to say that our political views are inscribed in our DNA, but it does show how political parties have come to be populated by people with specific characteristics. Socially liberal Democrats and conservative Republicans have, over the years, come to have their own traits. Conservatives, for example, tend to be more conscientious, authoritarian and stricter followers of rules.

Studies have also shown that violence is more a hereditary inclination and less the result of social conditions than previously believed.

In today's world, it seems like there's always a war happening somewhere, but this isn't necessarily a new development. Although certain theorists want to believe that violence is not part of human nature, the pre-historic archeological record is littered with the evidence of bloody conflicts.

Previously it was thought that a person's violent tendencies were strictly a learned behavior, brought on by social conditions that included discrimination, poverty and disease.

Scientists believed they understood the conditions that produced violence, but the truth is we have very little concrete knowledge. US crimes rates over the past 50 years show wild ups and downs that defy any easy explanation.

So, it would be wrong to reject the possibility that violent tendencies are a combination of social conditions and genetic heredity.

Any parent will tell you that toddlers love to hit, bite and kick — so much so that toddlerhood is, in fact, _the_ most violent age. And since these children are violent before they've ever learned about guns, kung fu or war, it's a strong clue that violence really is in our DNA.

### 12. The minds of women and men aren’t interchangeable, but this shouldn’t stand in the way of feminism. 

A relatively short time ago, women were expected to be no more than obedient housewives and doting mothers. While this perception has changed in the United States, women are still experiencing discrimination, condescension and sexual harassment every day.

While the women's liberation movement and feminism are rightfully considered major historic achievements, there is at least one branch of feminism that has refuted the modern understanding of human nature. This is _gender feminism_, which argues that the only difference between boys and girls is their genitalia.

But the idea that the minds of women and men are interchangeable is flawed in many ways.

First of all, there's the structure of the brain itself. There are so many visible differences between the brains of men and women that it's only logical that they have different cognitive capacities.

Research backs up this assertion by telling us that men, on average, take more risks and have a stronger capacity for the mental manipulation of three-dimensional objects. While women are, for starters, far better at spelling, matching shapes and reading facial expression and body language.

None of this should imply that one sex is superior to the other. Both show equal levels of general intelligence and think in the same way while feeling the same basic emotions.

From a gene's perspective, there are equally good strategies for being in the body of a male or a female. That's why both sexes have been refined over time through natural selection, resulting in equally complex bodies and minds.

Therefore, one can agree that the two sexes have different minds and still be a feminist since there's nothing discriminatory in having biological differences.

There's also no excuse for discrimination or the gender pay gap. With the information these differences provide, we might get a better idea of what factors besides discrimination are causing the gap and how society can compensate. If a society values a job that requires skills that match male characteristics rather than female traits, men will tend to do better. If another society values jobs with traditionally feminine skill matches, women will usually do better.

> _"To ignore gender would be to ignore a major part of the human condition."_

### 13. Three laws of behavioral genetics show the influence of genes, parenting and unique environment on human nature. 

In 2000, the psychologist Eric Turkheimer proposed three basic laws of behavioral genetics based on the empirical results of many tests and, perhaps unsurprisingly, they don't support the blank slate theory.

The first law of behavioral genetics is that "all human behavioral traits are heritable."

What exactly falls under the category of a behavioral trait is debatable, but most agree it is anything that can be accurately tested in someone, like language proficiency, strength of religious beliefs, or how conservative or liberal you are. The first law says that all of these things are inheritable.

There are, of course, many traits that are not heritable, such as your native language and your religion.

The second law of behavioral genetics states that "the effect of being raised in the same family is smaller than the effect of the genes."

This goes back to the studies on separated twins but also includes _virtual twins_, meaning when one brother or sister is adopted or genetically unrelated. Studies show that in these cases, the adopted child's personality will be predominantly determined by their genes and that the influence from the adoptive family is so small that it's often negligible. So, even if siblings are raised under the same roof from day one, the adopted child will still turn out completely differently.

The third law of behavioral genetics states that "a substantial part of the variation in complex human behavioral traits is not accounted for by the effects of genes or families."

This is a mouthful, but it means there is a third significant influence, outside of family and genes, and it's the unique environment in which a person grows up, whether it's a particular neighborhood or a certain group of friends.

The current estimate as to how much effect these different influences have is: genes at 40-50 percent, shared environment or family at just 0-10 percent, and unique environment at 50 percent.

### 14. The arts are also in our genes, and it’s beauty that’s been missing in modern art. 

For a while now, American schools have been cutting back on subjects like art and music while focusing more on science and technology. Many advocates for the arts have protested this development in US education and believe the arts are in danger of disappearing. But some signs may help them rest easy.

It turns out that the arts aren't suffering much at all these days, likely because art is in our genes.

Art has always been our way of expressing human nature, and the output of art has been growing right alongside the world's population. Art can be found in every single culture in the world, whether it's singing and dancing, painting and sculpting or telling stories. It's quite easy to see how art is part of our human nature.

Our desire to produce art might stem from our drive to mate, since creativity is widely recognized as a sign of a person's intelligence, and therefore the quality and attractiveness of their genes.

However, research suggests that some works of art provide more pleasure than others, and this might explain the mistaken belief that the arts are dying.

People generally respond more favorably to images of traditional beauty and panoramic landscapes. Yet modern and postmodern art have forsaken these images in favor of more abstract ideas.

So, lately, rather than flowers, ballerinas and sun-dappled vistas, we have the splatters of Jackson Pollock and disturbing abstract images that take more than a passing glance to appreciate. Similarly, modern music has also abandoned more traditional rhythms and melodies in favor of dissonance and atonal compositions.

It's mainly this kind of modern art, which denies traditional beauty in favor of abstract images and sounds, that is declining. Our sense of beauty is like our other senses and part of our evolutionary adaptation. It's likely that anyone who's complaining about there being less art is really saying that there's less traditional art being made.

Art and beauty both play a significant role in human nature, so it's no surprise that they remain closely entwined.

### 15. Final summary 

The key message in this book:

**The human mind does not begin as a blank slate, nor is it a ghost in the machine, working independently from our other biological processes. Nor is it true that humans are born as noble savages who are only corrupted by the trappings of modern society. Instead, the human mind comes fitted with a complex structure that helps us make sense of the world and gives us the instincts to survive and thrive in it.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Better Angels of Our Nature_** **by Steven Pinker**

_The_ _Better_ _Angels_ _of_ _Our_ _Nature_ (2012) takes a close look at the history of violence in human society, explaining both our motivations to use violence on certain occasions and the factors that increasingly restrain us from using it — and how these factors have resulted in massive reductions in violence.
---

### Steven Pinker

Steven Pinker is a cognitive psychologist and one of the world's most renowned writers on language, the human mind and nature. Pinker has won numerous awards from organizations such as the American Humanist Association and has been included in _Time_ magazine's "The 100 Most Influential People in the World." Pinker is a professor of psychology at Harvard University, and his influential books include _The Language Instinct_, _How the Mind Works_ and _The Better Angels of Our Nature_.

