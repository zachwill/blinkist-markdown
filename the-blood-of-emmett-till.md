---
id: 5a9eac7fb238e100079a3b08
slug: the-blood-of-emmett-till-en
published_date: 2018-03-08T00:00:00.000+00:00
author: Timothy B. Tyson
title: The Blood of Emmett Till
subtitle: None
main_color: BC2F26
text_color: BC2F26
---

# The Blood of Emmett Till

_None_

**Timothy B. Tyson**

_The New York Times_ bestseller, _The Blood of Emmett Till_ (2017), retells the horrific details of the abduction and lynching of a 14-year-old African-American boy in 1955. Emmett Till's murder highlighted the realities of white terrorism in America, and helped catalyze the civil rights movement.

---
### 1. What’s in it for me? Learn how one boy’s death inspired a movement. 

You are probably familiar with Rosa Parks, who in 1955 refused to obey a bus driver's command to give up her seat for a white man. Her act became a symbol for the struggle against white oppression in the US and helped ignite the Civil Rights Movement.

Less known, but no less important, is an event that occured a few months earlier; the gruesome murder of Emmett Till, a 14-year-old African-American boy from Chicago. In fact, when Rosa Parks was committing her act of defiance on the bus, she had the tragic fate of Emmett Till in mind.

Through the lens of Till's murder, and the subsequent trial, these blinks tell the story of a country plagued by centuries of racism and white oppression.

In this pack, you'll learn

  * how modern media made the civil rights movement possible;

  * how white supremacists terrorized whites as well as blacks; and

  * that the "end of segregation" did not really mean the end of segregation.

### 2. Racists in a segregated America were not afraid to kill for their ideology. 

Throughout the twentieth century, the United States struggled to confront its history of slavery and to counteract prevalent racial prejudices. In the southern States, segregation, both legal and illegal, was ubiquitous.

Consequently, between 1910 and 1970, millions of African-Americans fled and migrated to the North from the American South. They settled in cities like Chicago, hoping for a better life.

But in these northern cities, too, separation of communities along race lines was common.

Such racial ghettoization was clearly indicative of the discrimination blacks faced. Violence against them was not uncommon.

Between 1910 and 1930, the number of African-Americans in Chicago doubled. Competition for housing and work was tough, especially with European immigrants.

Blacks were forced to live in ghettoized parts of Chicago's South Side. Eventually, housing shortages forced black residents to move out into adjoining neighborhoods, which had previously been entirely white.

In 1949, a mob of whites, about 2000-strong, attacked a building belonging to a black couple in a white neighborhood on the South Side.

If you were black, any supposed transgression in this atmosphere could cost you your life.

This is how Emmett Till met his end, at the age of 14. In 1955, Till caught a train from Chicago to Mississippi to visit his great uncle and cousins. Till was unused to the extreme racism of the South, and made the mistake of flirting with Carolyn Bryant, a white woman, while she was working at her husband's store.

She later told her attorney that Till had squeezed her hand, asked her out and boasted to her about sleeping with white women in the past.

Within days of the incident, Carolyn's husband, Roy Bryant, and her brother-in-law, J. W. Milam, arrived at the house of Reverend Mose Wright, Till's great uncle. They were brandishing guns. They were there for Till.

After lynching Emmett Till, they threw his body into the Tallahatchie River.

Carolyn Bryant, however, never expressed utter certainty that Till was the boy who'd talked her up at the store.

> _"Till had been at the wrong place at the wrong time and made the wrong choice."_

### 3. Racism and bigotry are learned from a young age, whether you’re the victim or the perpetrator. 

For African-American kids growing up in the early twentieth century, racial segregation was just part of life, and the dangers of crossing the divide were all too real.

Emmett Till's mother, Mamie Bradley, who was raised in Argo, Illinois, grew up hearing a particularly chilling cautionary tale. In Mississippi, there was once a little black girl who befriended the daughter of the white woman for whom her mother worked.

One day, the girls had a fight, and the white girl went crying to her father. Recompense was swift: in a fit of rage, the father shook the black girl like a rag doll. She died soon after.

In Bradley's hometown, the children were told to avoid taking shortcuts through white neighborhoods and warned against talking to white strangers. For all its harshness, the lesson was clear: black children were not safe in the hands of whites.

Conversely, white children were also familiar with the ideology of racial segregation.

Carolyn Bryant, the woman who accused Till, later recounted an episode that occurred when she was 14.

She had had a black friend called Barnes, and one day, as he cycled past, Bryant asked if she could hop on the back of his bike. But when her aunt Mabel saw her perched there, she ordered her to dismount and get back in the house immediately.

Bryant was taught from a young age that being seen with black children was unacceptable. Her aunt was blunt: "you don't need to be riding with [black] boys… people will talk."

Young Carolyn went on to marry Roy Bryant, whose family was made up of gun-toting, hard-drinking white supremacists.

Early in their courtship, Roy invited Carolyn to see "the hanging tree," where blacks who'd dared to contravene the unwritten racial laws were hanged.

And Carolyn _had_ to visit the ghastly local monument. To do otherwise would have been to break the racial code.

### 4. Till’s family were hell bent on justice, even putting themselves at risk. 

When Mamie Bradley received a call from a relative about her son's abduction, she knew she couldn't rely on the authorities in Mississippi. So she took matters into her own hands and contacted the Chicago press, especially the black newspapers.

The stakes were raised as soon as Till's body was found in the Tallahatchie River. He'd been weighed down with cotton processing machinery and had barbed wire wrapped around his neck.

The local sheriff, H. C. Strider, attempted to conceal what had happened. He wanted Till's body buried immediately, without an autopsy.

However, Bradley was on the case. She had her mind set on getting her son's body back to Chicago.

And thanks to the courage and determination of Till's other family members, she succeeded.

Though he, himself had received multiple death threats, Till's great uncle, Mose Wright, took the stand. Before Judge Curtis M. Swango, Wright testified against Bryant and J.W. Milam, both of whom had been indicted for Till's murder.

The threat against Wright was real. He was forced to carry weapons for self-protection at night, and he had to rotate where he slept during this time for his own safety: he even resorted to bedding down in the cemetery.

The trial itself produced one of the most famous images of the civil rights movement. When District Attorney Chatham asked Wright if he recognized anyone from the night of the killing, Wright raised his hand and pointed straight at Bryant and Milam. It was a brave act; it was unheard of in Mississippi for blacks to testify against whites.

Wright had put his life on the line for justice.

### 5. Thanks to the power of the media, news of Till’s death prompted people to action. 

News of Till's lynching eventually began to spread beyond black newspapers. Newspapers, radios and TVs were soon abuzz with the news, especially in Chicago.

More than ever before, the media had the power to spread news and shift opinion. And it did.

Thanks to TV, still a new medium at the time, the story found its way into the homes of white, middle-class Americans, and stirred national interest in the racial injustice of Till's murder. Other lynchings had gone unnoticed, but news of this one spread irrepressibly.

Bradley further publicized the murder by insisting on an open-casket viewing that anyone could attend.

On the first day, more than 40,000 people paid their respects. Over the next few days, a quarter of a million people saw Till's mutilated body. His tongue lolled from his mouth, his right eye dangled out of its socket and a bullet hole could be clearly seen above his right ear.

The rage was palpable. African-Americans united in the fight against segregation, and in the name of justice for Till. Thousands of aggrieved citizens joined the movement.

One of them was Amzie Moore. He'd been drafted during World War II, during which, as a black soldier, he'd been denied basic amenities.

Ironically, during the war, he'd been assigned by military authorities to convince his fellow black soldiers that a better life — a desegregated life — awaited them back home. He knew it was nonsense, though, as whites were forming "home guards," supposedly to "protect" themselves from demobilized black soldiers.

In 1951, Moore formed an activist group, The Regional Council of Negro Leadership (RCNL). The idea was to educate and register new black members, because Moore was convinced that, with a united voice, blacks would be able to engineer change. At one point the organization had 100,000 members.

When Till first went missing, Moore was one of the first on the scene, searching the cotton fields for any sign of the boy.

### 6. White supremacists carried out campaigns of domestic terrorism against blacks. 

On May 21, 1954, the US Supreme Court gave one of its most historic rulings. In _Brown vs. Board of Education_, state-sanctioned racial segregation of blacks and whites into separate schools was declared unconstitutional.

The backlash from white nationalists came swiftly.

In short order, a network of white supremacist groups called the Citizens' Council had formed. They published _Black Monday,_ a racist diatribe by the segregationist leader Judge Thomas Pickens Brady. He sought to terrify white Mississippians, expounding on the supposed black threat. He rallied against the imaginary desires of black men for white women, whom he described as "the loveliest and purest of God's creatures."

The fear-mongering segregationists hoped to convince whites of the "danger" of public-school integration and black voting rights.

Meanwhile, the Citizens' Council used intimidation tactics against civil-rights activists and members of the National Association for the Advancement of Colored People (NAACP).

In addition to violence, they also blacklisted African-American activists, compelling the activists' employers to give them permanent "vacation" unless they stopped agitating. The employees were threatened, too: they could have bank credit withdrawn or supply chains broken.

But the activists stayed strong, despite the great cost to themselves. Reverend George Lee, for example, fought hard to get more blacks to vote, and as a result, he became familiar with receiving death threats. In May 1955, not long before Till's murder, two white men made good on those threats and gunned Lee down. It was certainly murder; a coroner's jury even identified the cause of Lee's death — buckshot in his jaw.

And yet Sheriff I. J. Shelton claimed the metal fragments were just dental fillings. No search for the killers was ever conducted by the state authorities. The FBI did eventually look into the matter, but it went nowhere.

It goes to show, despite the verdict of the Supreme Court, entrenched white privilege, especially in the South, showed no signs of budging.

### 7. The failure of justice only served to further ignite the civil rights movement 

Bryant and Milam's trial for Till's murder, overseen by Judge Swango, can hardly be called an example of fair justice in action.

For starters, the jury was all white.

And the defense unabashedly wove a web of lies. When Carolyn Bryant took the stand, she regurgitated the standard racist bilge: black men systematically sought to rape white women. She even lied about Till and claimed he'd squeezed her hand, and touched her waist.

We know this was untrue because, in an interview conducted 50 years later, Bryant admitted that she'd lied.

Interestingly, despite her lying, this tells us something about Bryant's reluctance to identify Till. It would seem to suggest that she was pressured into lying, but at the same time wanted to protect Till from any violence her husband and family might mete out.

A physician, Dr. L. B. Otken, who was also white, testified as an expert witness for the defense. He claimed that he could not even tell if the corpse was black or white, let alone if it was Till, as it was in such poor condition. This was clearly nonsense. Otken had actually attended to Till's body in the river and had it sent to "the colored funeral home."

It was this last lie about the identity of the corpse that ensured the killers' acquittal. The whites in the courtroom applauded as the verdict — not guilty — was read.

However, even though justice had been grossly miscarried, the fight was far from over. In September 1955, over 50 thousand people protested on the streets of Detroit. They were there to demonstrate against the failure of justice and Till's murderers.

It was the first spark — the first of many protests across the United States. Ten thousand gathered in Chicago and a further 50 thousand in New York.

A collective cry of rage against injustice rang out. The first great stage of the civil rights movement had begun, sparked by Till's murder and the racist system that had acquitted his murderers. A whole generation had been fired into action.

> _"Like other great episodes in the battle for equality and justice, this trial has rocked the world, and nothing can ever be quite the same again..."_

### 8. Final summary 

The key message in this book:

**Even though the lynching of Emmett Till happened over half a century ago, the racism that inspired it still persists in America today. The iniquities that sparked the civil rights movement, a movement catalyzed by Till's murder, have yet to be resolved. Systemic racism killed Till, and systemic racism lives on.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _On the Run_** **by Alice Goffman**

In _On_ _the_ _Run_, author Alice Goffman dives into a dangerous world unknown to most Americans: the poor, predominantly black and crime-ridden neighborhood of Sixth Street in Philadelphia. Living in the area for six years, Goffman witnessed the daily life of the neighborhood and thus gained a unique insight into a crime-plagued society, its members constantly "on the run."
---

### Timothy B. Tyson

Timothy B. Tyson is the author of _Blood Done Sign my Name_ (2005), which was a finalist for the National Book Critics Circle Award. He is also the winner of the Grawemeyer Award in Religion and the Southern Book Award for Nonfiction. As a professor of Afro-American studies at the University of Wisconsin-Madison, and Senior Research Scholar at the Center for Documentary Studies at Duke University, he teaches about religion, racial issues and civil rights in the South.

