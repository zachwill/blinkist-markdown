---
id: 53bcf64d63323400070c0000
slug: the-blue-sweater-en
published_date: 2014-07-08T00:00:00.000+00:00
author: Jacqueline Novogratz
title: The Blue Sweater
subtitle: Bridging the Gap Between Rich and Poor in an Interconnected World
main_color: 40876D
text_color: 36735D
---

# The Blue Sweater

_Bridging the Gap Between Rich and Poor in an Interconnected World_

**Jacqueline Novogratz**

_The_ _Blue_ _Sweater_ is an autobiographical look at the author's travels in Africa and how they helped her understand the failures of traditional charity. These blinks also outline why a new type of philanthropic investing, called "patient capital," developed by the author, may be part of the answer.

---
### 1. What’s in it for me? Find out why traditional charity is failing, and what can be done about it. 

After just three years as a Wall Street banker, Jacqueline Novogratz decided that she wanted to make a bigger difference in the world. She abandoned her promising career to try to help people suffering from poverty in Africa.

At first, she was a rather naive Westerner with little idea of what people in the developing world really needed or desired, but soon she began to understand. She found that experts in the West who tend to assume they know what poor people need are often wrong: aid and charity frequently wind up going to the wrong people or is squandered on poorly planned projects.

What people in the developing world want is not handouts, but rather the dignity that comes from being able to help themselves.

As the author traveled through Rwanda, Kenya and Tanzania, among other countries stricken by poverty, she realized that making people in developing countries accountable for their aid projects makes them far more likely to succeed.

In the following blinks, you'll find out:

  * why it's actually more difficult to give away your money efficiently than it is to earn it in the first place,

  * how a man named Innocent was anything but, and

  * how development aid can work better if you demand the recipient pays it back.

### 2. Local women play a vital role in eliminating poverty in developing countries. 

Anyone who has watched the news knows the devastations of poverty in the developing world. People in the West have tried to lift the developing world out of poverty with various approaches like government aid and charitable organizations, yet, time after time, they have failed. Why?

Often, aid and charity is not directed to the right people — women. In the developing world, it is women who generally decide how a family should spend its money, which makes them the most responsible spenders in their communities. Giving them the aid is most likely to yield effective results.

Novogratz came to this conclusion when she wanted to help the poor in Rwanda. She had helped found a micro-credit company called Duterimbere, which makes very small loans to poor people who wouldn't qualify for traditional bank loans. Soon she found that many women in developing countries were taking out loans to expand their tiny enterprises, mostly involving selling vegetables like tomatoes and onions.

Duterimbere turned out to be very successful, and its success was largely because it let local women, rather than so-called experts, decide where money should be spent. This method went against the prevailing aid industry logic that dictated that experts should direct aid, with locals getting little say.

Although these micro loans effectively helped poor communities rise out of poverty, Duterimbere was faced with the problem that a lot of women were not paying them back.

Novogratz found a solution in making borrowers more accountable.

> _"The psychology of poverty is so complex...so often the people who know the greatest suffering are the most resilient."_

### 3. In order to lift people out of poverty, you have to make them accountable. 

Many poor people face a frustrating catch-22: they can't get bank loans because they are considered too poor to qualify, but without a loan, they can't start an enterprise to earn a proper livelihood.

This vicious cycle is partly because of a prevailing bank (mis)conception that poor people suffer from a _culture_ _of_ _poverty:_ they don't even want to better themselves, so they would squander any loan given to them and never pay it back.

The vast success enjoyed by micro-creditors has shown this perception is, in fact, incorrect: poor people can and do pay back their debts, as long as there is _accountability_.

Accountability was the key that enabled Duterimbere to raise its loan repayment rates.

In the beginning, the company found very few women paid back their loans because they saw no reason to: why would a finance company backed by rich Westerners miss a small loan?

For example, one rice seller claimed she couldn't pay back the loan as her rice stock had been stolen. Yet when a Duterimbere representative visited her house, she found a huge sack of rice that was hidden. The borrower had figured she could easily get away with not paying back the loan.

To combat such faults, the company sent a message to borrowers that they absolutely _had_ to pay back loans. Duterimbere said that if a borrower fails to repay, she cannot borrow again — no excuses.

The policy worked. As soon as the aforementioned rice seller heard about the new rule, she repaid her loan in full and subsequently became a reliable client.

What's more, the accountability tactic helped make Duterimbere not just another gullible Western charity in the eyes of the women, but a serious financial company that could help get them out of poverty.

### 4. Give poor people dignity by letting them work themselves out of poverty. 

Many people think that those in poverty can lift themselves out if they just receive enough charity. But in fact, money is not enough. People need to feel dignity to be truly encouraged to transform their lives. The only way to give them dignity is by letting them work their own way out of poverty.

Consider a small bakery that was set up in Kigali by the Rwandan government for poor women to work in. The bakery was backed by two charities, and it sold its goods to government offices while employing 20 women.

While it may sound good on paper, it was not working: the bakery generated continuous losses of $650 a month, and could thus only survive while the charities supported it. At the same time, the women working there earned 50 cents a day, which wasn't enough to raise them out of poverty.

The solution? Empowerment and accountability.

Novogratz stepped in and insisted the bakery be run like a business, where the women would be accountable for its costs and sales. They would earn a base wage, and get commission for the total sales made, effectively connecting their wages to the project's outcome.

Gradually the employees started to feel like they had a stake in the bakery's success, and began making decisions and improvements themselves. They called it the Blue Bakery, and it soon grew, so much so that the women quadrupled their income, earning two dollars a day, which was much more than the typical earnings of Kigali women.

The success and sense of ownership gave the women confidence and dignity to no longer rely on support from others. They felt confident in choosing for themselves what they wanted.

In the next two blinks, you'll discover what we can learn from the failures of traditional aid programs.

### 5. Where there is a lack of accountability, corruption is allowed to stifle progress. 

While there are many unfounded myths about developing countries, one is true: corruption is rife and it is stifling prosperity.

Where does corruption stem from? Simply put, it stems from a lack of accountability.

For example if no one tracks where a charity's money goes or how productively it is being used, it's highly likely that someone is taking advantage to profit.

Novogratz discovered this trend when, after her time in Rwanda, she travelled to Kenya to write a report for UNICEF about women's groups funded by foreign aid. There she found that a lack of accountability had made the Kenyan government highly corrupt. It was in the position to choose which projects should be funded based on a bidding competition, and blatantly abused its power by asking for a "fee" of 20 percent of the funding to secure the winning bid.

Corruption and lack of accountability lead to bad services, as money is skimmed from project funding and no one feels responsible for seeing that the project delivers what it's supposed to. Corruption and lack of accountability mean that donor-funded projects, started with the best of intentions, often fail. For example, maize mills that have been built throughout Africa as part of a plan to energize local economies have been found idle, because there's no fuel to run them or no one in the community knows how to fix them.

In many cases, projects never produce enough benefits to cover their initial investment.

Having those involved fully accountable means earnings are more likely to return on the investment and projects are more likely to have the positive impact the investors, donors, and recipients intended.

> _"Is corruption a cause of poverty? Or is poverty a cause of corruption?"_

### 6. Trust is a vital commodity for building a better society. 

People in the developed world often take trust for granted. When, for example, you hire a builder, you probably trust her to deliver a house that is built according to regulations. And if the builder cheats you, the justice system will hold her accountable.

But in the developing world, trust cannot be taken for granted in the same way, there being a lack of regulatory agency or lawful judicial participation.

Novogratz experienced this different standard of trust firsthand, when the guard she had hired to watch her house stole her money, jewelry and clothes. This was after she had given him about two months' extra salary to cover his children's school fees. Ironically, the guard's name was Innocent.

Faced with this betrayal, she naturally wanted to report Innocent to the police. But doing so in Rwanda would have been an unfairly severe punishment, for the local criminal justice system is brutal and unreliable. Because she could trust neither Innocent nor the justice system, her only option was to fire him.

The Rwandan government exemplifies and reinforces this lack of trust by going to public places regularly to spy on what people are saying about politicians.

This underpinning of a lack of trust severely hurts a nation's potential for development.

Novogratz found that in Karachi, Pakistan, though half the population lives in slums, they don't sign on for affordable housing planning because they lack trust.

In the past, housing developers promised affordable housing and never delivered, so poor people today are skeptical of being swindled.

One manager for such a project even had to live on the construction site in order to build trust with the locals who watched him closely.

In the last blink, you'll discover an alternative philanthropy tool to traditional aid.

### 7. Patient capital combines the benefits of traditional aid and business investment. 

One truth that often surprises people is that giving your money away so that it is used efficiently is actually much harder than earning it! This problem is evidenced by the frequent failures of philanthropic schemes in the developing world.

And not only is philanthropy failing to lift people out of poverty: traditional financial investment is too. Investors are falling short because they often refuse to lend to poor people, viewing them as unlikely to provide immediate returns.

However, there is a third way: neither philanthropy nor traditional business, but a combination of the two.

This approach is called _patient_ _capital:_ money is invested into projects over long periods of time with the understanding that returns may be below market expectations. This approach focuses on developing and supporting the enterprise, not giving handouts or earning quick returns.

The author's venture capital fund Acumen does exactly this. It invests, often large amounts, into entrepreneurial schemes that aim to deliver important social benefits, like safe water systems and low-cost, quality housing to impoverished areas.

The patient capital approach combines the best parts of philanthropic and market approaches. As for a normal investment, patient capital expects to see returns from the funding it provides. This approach avoids the pitfall of giving money without attaching accountability.

What's more, patient capital entrusts funds to local people instead of allowing outsiders, so-called experts, to decide where the money is spent. This way the people who are most familiar with their local problems can be in control of where to invest in solutions.

In a housing project in Pakistan, for example, its organiser was a local man, Tasneem Siddiqui, who had a unique understanding of the local people, the kind of understanding that comes with being part of the community. He knew what they wanted: first to build small homes for themselves and then gradually expand as they could afford to. He also helped Acumen decide which land to build on, as he knew more about the land than an outside expert could. And he helped Acumen gain support in the community. Only an organizer with such local knowledge could prove so crucial to a project's success.

### 8. Final Summary 

The key message in this book:

**Traditional** **development** **aid** **often** **fails** **because** **the** **recipients** **of** **the** **aid** **are** **not** **held** **accountable,** **and** **because** **local** **people** **are** **not** **empowered** **to** **decide** **how** **to** **best** **use** **it.** **A** **new** **form** **of** **philanthropic** **investment** **called** **patient** **capital** **has** **shown** **some** **promising** **results** **in** **addressing** **these** **issues.**

Actionable advice:

**Put** **yourself** **in** **the** **shoes** **of** **others.**

Often when we think of helping others, we develop our own ideas about what would work best and then follow through on our own perception of solutions. But, to truly help others, we should first find out what they want and need, not what we _think_ they want and need. To truly understand others, we can put ourselves in others' shoes. Live with those you are trying to help, and face the daily adversities they face.
---

### Jacqueline Novogratz

Jacqueline Novogratz is a former Wall Street banker who later went on to found and head _Acumen_ _Fund,_ a non-profit venture capital firm that invests in sustainable enterprises with the goal of lifting people out of poverty.

