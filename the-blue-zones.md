---
id: 56407c523866330007590000
slug: the-blue-zones-en
published_date: 2015-11-12T00:00:00.000+00:00
author: Dan Buettner
title: The Blue Zones
subtitle: 9 Lessons for Living Longer From the People Who've Lived Longest
main_color: 206AA0
text_color: 206AA0
---

# The Blue Zones

_9 Lessons for Living Longer From the People Who've Lived Longest_

**Dan Buettner**

_The Blue Zones_ (2012, first published in 2008) whisks you through the regions of the world with the highest concentrations of healthy centenarians. By examining how people in these regions live and interact, we gain insight into how to extend our own lifespans.

**This is a Blinkist staff pick**

_"If you're eager to learn about far-flung spots around the world, and, on top of that, are curious to listen to what the oldest among us have to say about living a long and healthy life, I recommend_ The Blue Zones _. The communities featured in these_ _blinks have one unique thing in common: the longevity of their residents. Read on to find out what sets them apart from the rest of the planet."_

– Emily, User Engagement Lead at Blinkist

---
### 1. What’s in it for me? Discover the secret to living longer. 

Throughout history, countless people have sought the proverbial fountain of youth and dreamed of immortality. Who knows, perhaps we'll actually find it one of these days, but until then we'll have to content ourselves with devising ways of living longer. But how?

Studying the areas in the world where people live the longest seems like a commonsensical first step. And by doing exactly that, we've already gained much understanding of what contributes to a long and healthy life.

As these blinks will show you, the key to living longer is neither magic nor rocket science. All you've got to do to significantly increase your life expectancy is make a few adjustments to your daily schedule. Some of them, like giving up smoking and exercising more, are already common knowledge. But there are also other, less obvious ways to live longer. These blinks will show you what they are.

You'll also learn

  * what Danish twins can tell us about living longer;

  * why a liberal attitude toward sex increases life expectancy; and

  * that if you want to live to be 100, you should grow a garden.

**This is a Blinkist staff pick**

_"If you're eager to learn about far-flung spots around the world, and, on top of that, are curious to listen to what the oldest among us have to say about living a long and healthy life, I recommend The Blue Zones. The communities featured in these blinks have one unique thing in common: the longevity of their residents. Read on to find out what sets them apart from the rest of the planet."_

– Emily, User Engagement Lead at Blinkist

### 2. Blue Zones are the areas in the world where people get oldest and stay healthiest. 

The world is full of unique civilizations and cultures, each with its own unique habits and customs, all adding to our global health and wellness. If you want to live longer, it's worth investigating _Blue Zones_, the areas in the world where individuals, thanks to their healthy habits, grow older than people anywhere else. 

One of the first areas to earn attention for its unusually long-living and healthy inhabitants was the Italian island of Sardinia. Research from 2011 found that, on average, around one in 600 people in the mountainous Barbagia region of Sardinia live to the age of 100, in contrast to one in 4,000 in the United States.

The Sardinians, and people in other Blue Zones, have a lot to teach us about how to live a long life — if we're willing to listen.

As you'll see, it's not your genes, but your lifestyle that has the most significant affect on your lifespan. We see evidence of this in a Danish study on over 2,500 twins; it revealed that, out of all the factors contributing to longevity, our genes account for only about 25 percent.

The truth about living longer is that there are no lifestyle changes or magic pills that will stop the aging process. You will always age, every day, no matter what. However, the aging process can be accelerated or slowed down, and the people in Blue Zones have demonstrated effective ways of putting on the brakes.

In the end, the right lifestyle may give you an extra decade of quality life. Interestingly, centenarians are the fastest growing global age demographic, especially in the West.

And getting to be over 100 isn't about getting the right vitamin supplements or planning out your physical activities. Rather, it's about a more natural way of life, moving around naturally and eating more fruits and vegetables. All this becomes clear in examining the world's Blue Zones.

> _"The brutal reality about aging is that it has only an accelerator pedal."_

### 3. Your food and drink have a significant impact on how long you’ll live. 

We've all heard the saying "You are what you eat." Well, that commonplace is truer than you might think.

Let's start with the liquids. Be sure to drink enough water — and say yes to some red wine once in a while.

The Blue Zone of Loma Linda, a settlement of Seventh Day Adventists outside Los Angeles, California, is the only Blue Zone in the United States, and it shows just how important it is to drink enough water. Instead of other beverages, they drink water — five to six glasses a day. As a result, the rate of heart diseases in this town is much lower, and their life expectancy much higher.

But you don't have to stick to only water to live longer. Some red wine is helpful, too. The first area to be identified as a Blue Zone, the mountainous Barbagia region on the Italian island of Sardinia, boasts a high life expectancy for men. And their daily meals are always eaten with some red wine.

Wine's secret lies in its flavonoids, which can also be found in dark chocolate or brightly colored fruits and vegetables. Flavonoids reduce the incidence of certain cancers and of heart disease, so, if you do imbibe, go for the red wine.

Of course, you can't live on drink alone. You also have to eat, and what you eat has a major effect on how long you'll live.

Most people in Blue Zones have low-calorie diets, usually vegetarian or vegan. Their main meal is either breakfast or lunch; dinner is always light. 

In the Japanese Blue Zone of Okinawa they have a special philosophy concerning food: they purposefully restrict the calorie intake and remind themselves to eat only until they are 80 percent full. This slows down the body's metabolism, which helps reduce the amount of damaging oxidants in the body.

> _"Drinking wine along with a plant-based meal increased flavonoid absorption and doubled the healthful effects of drinking wine alone."_

### 4. Living longer means putting your family first and having a clear purpose to continue living. 

Living longer isn't all about what you put into your body, however. It's also about how you exercise your mind. You need a driving force, a purpose!

Statistics show that more Americans die in the first year of retirement than in the last year of work. This fact suggests that living on without a purpose can be harmful to your health. You've got to have something that motivates you to get out of bed every morning.

In the Costa Rican Blue Zone of the Nicoya peninsula, everyone feels that they have a _plan de vida_, a plan for their life. In essence, they don't stop living at the age of 60, or 66, or 70 or at any age — their life has a continued trajectory. And this has a positive side effect: by keeping themselves so busy, they don't get into conflicts with others — there's simply no time for that.

But keeping busy isn't only a means to an end. They all love to work, because the fruit of their labor provides for their families. For them, creating a happy and healthy family is a core purpose.

In all Blue Zones that have been identified so far, the family has a special role, and all centenarians put their families first.

In Sardinia, the most important thing is to live with your family. Of the inhabitants who make it to 100, 95 percent are able to do so only because they have a daughter or granddaughter to care for them. And the centenarians reciprocate in this relationship by providing love, childcare, motivation and often even financial help. 

The family doesn't only operate as a support network; there is a connection between respecting elders and longevity, too. Only when elders are expected to provide help do they feel needed and have a clear purpose.

### 5. You’ll live longer if you are part of a community. 

The Blue Zones of the world are always small and functioning communities. But why does this social structure contribute to longevity?

You learned in the last blink how important a family support network is for Blue Zones. Similarly, the wider community provides a sense of belonging — a reason to go on.

In Okinawa, this is formalized in the system of _Moai._ Originally this meant meeting regularly for common purposes, such as the harvest or the arrangement of local festivities. Today, it's more a ritualized vehicle for companionship. Strong community provides a purpose for the Okinawans, which also reduces stress.

There's also Ikaria, an island in Greece. In this Blue Zone, when goods are left over from a religious or cultural holiday, they are given to the poor as a way of imparting a sense of community.

And in Sardinia men grow older than in most other places due to the fact that they leave the family business to the women in order to gather together as a community.

This sense of community is often tied to some kind of spirituality.

For example, the Seventh Day Adventists in Loma Linda maintain a diet according to religious rules — they drink no caffeine, for example, and discourage smoking. Their culture of longevity stems from religious belief.

Part of their belief is to keep the Sabbath, during which they get physical exercise and meet with family, friends and other community members. By simply adhering to their beliefs, they've found a way to live longer.

Similarly, spirituality in Okinawa centers around living in the moment and leaving the past untouched. All past hardships are viewed as necessary to enjoy the moment, so the populace doesn't suffer the stress and anxiety of lamenting past decisions. In turn, they lead longer lives.

Spirituality doesn't have to be a fixed religious belief. It could also be a set of common practices that includes physical activities and ways to reduce stress.

> _"You don't have to convert to their faith to admire how they have generated a Blue Zone out of whole cloth."_

### 6. Physical activity and stress reduction should be part of your daily habits if you want to live longer. 

We've spent a lot of time talking about the things you should do to extend your lifetime. In this final blink, we'll talk about something that you should absolutely avoid: stress.

All Blue Zones use methods to reduce stress, each of them different. For example, in Sardinia stress is kept in check by a sense of community and a good sense of humor. People don't take themselves too seriously there, and they can all enjoy a hearty laugh at their own expense.

In Loma Linda, the Sabbath plays the special role of keeping stress to a minimum. The Sabbath is an opportunity to find time for others and for family, to give back and to laugh. Laughter is extremely healthy; it decreases the risk of heart attacks and lowers blood pressure. It's also just relaxing and a good way to overcome stress.

Sex is another great stress reducer.

In the Costa Rican Blue Zone, people have very liberal attitudes toward sex, and many have children out of wedlock. This sexual liberation makes them more relaxed and helps keep stress levels down. 

Aside from cultural disposition, physical activity offers a great means of cutting stress. Not in the sense of gym training, but rather as a natural part of your everyday life.

In Sardinia, most centenarians either were or are shepherds, and their job requires them to walk roughly six miles per day. 

In Loma Linda, they undertake all kinds of activity throughout the day — things like walking, swimming and hiking, all of which exercise the body in different ways.

This natural physical activity has the added benefit of increased exposure to the sun. Exposure to the sun produces vitamin D, which works against many types of cancer and diabetes. This is why there are so many healthier and more independent centenarians in Okinawa than in the United States or Europe — they literally live on the sunnier side of the world!

### 7. Final summary 

The key message in this book:

**Many people wish they could live forever. Unfortunately, they can't. But they can live to be over 100, provided they take action to develop healthy physical, spiritual and social habits.**

Actionable advice:

**Don't be a Scrooge!**

Studies in the Blue Zones show that people who are more likeable tend to have larger social networks, more frequent visitors and, when they're older, more caregivers. 

**Suggested further reading:** ** _Eat_** **_to_** **_Live_** **by Joel Fuhrman**

_Eat_ _to_ _Live_ gives readers a comprehensive overview of human nutrition, a re-evaluation of conventional nutritional wisdom, personal case studies and a practical dietary program with lots of recommendations. The reader can expect to learn about a number of different nutritional studies as well as the health benefits and repercussions of basic foods such as meat, milk, fish, vegetables and fruits.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Dan Buettner

Dan Buettner, whose work focuses on the secrets of long life, is a bestselling author and National Geographic Fellow. His next big project is to introduce the principles of long life that he's discovered into several US cities.

