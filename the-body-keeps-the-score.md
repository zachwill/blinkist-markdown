---
id: 5855971f1219060004019dec
slug: the-body-keeps-the-score-en
published_date: 2016-12-20T00:00:00.000+00:00
author: Bessel van der Kolk
title: The Body Keeps the Score
subtitle: Mind, Brain and the Body in the Transformation of Trauma
main_color: 226EAC
text_color: 226EAC
---

# The Body Keeps the Score

_Mind, Brain and the Body in the Transformation of Trauma_

**Bessel van der Kolk**

_The Body Keeps the Score_ (2014) explains what trauma is and how it can change our lives for the worse. These blinks investigate the wide-ranging effects experienced not only by traumatized people, but also those around them. Nevertheless, while trauma presents a number of challenges, there are ways to heal.

---
### 1. What’s in it for me? Learn how trauma affects your body and mind. 

We're all familiar with stories of traumatized war veterans who have experienced cruelty and pain, who have killed other people during combat or who have witnessed the death of a fellow soldier. Often, traumatized soldiers cannot find their way back into society. They struggle with their memories, develop hypersensitive reactions and become strangers to themselves and their loved ones.

What happens in the body and mind of people who have experienced trauma? And why is it so difficult to find relief from it?

In these blinks, you'll learn why traumatic experiences haunt us. You'll learn how trauma patients perceive their environment. Finally, you'll learn why there's hope for traumatized people and how trauma can be healed.

You'll also find out

  * why war veterans only trust other war veterans;

  * why an ordinary picture in a magazine can trigger horrifying thoughts; and

  * how yoga relieves trauma patients of their pain.

### 2. Trauma is incredibly common in our society. 

Trauma isn't just something faced by war veterans — it's far more prevalent in our society than we realize. The truth is that trauma can happen to anyone, and it's time we found out what this really means.

Traumas result from an experience of extreme stress or pain that leaves an individual feeling helpless, or too overwhelmed, to cope with adversity. Experiences involving war typically result in traumas, but violent crimes and accidents cause them too.

Rape and child abuse are terrible events, and they are also unfortunately more common than you might think. Reports reveal that 12 million women were victims of rape in the United States in 2014 alone, and that more than 50 percent of those women were under the age of 15 at the time of the assault. Every year in the United States, there are 3 million cases of child abuse.

These traumatic experiences can change the lives of those affected, as well as the lives of their friends and family. Traumatized people often suffer from post-traumatic stress disorder (PTSD), which can lead to depression and substance abuse.

In addition, traumatized people tend to mistrust anyone who hasn't experienced the same suffering they have, and assume that nobody can understand them. This was illustrated in one of the therapy groups the author set up for Vietnam veterans.

While the group helped the veterans find friends and share their experiences, those who weren't traumatized by the war were considered outsiders by the group — including the author. It took weeks of listening, empathizing and building trust with the veterans for them to accept him.

Establishing a rapport with someone suffering from PTSD is a challenge on its own, so just imagine trying to maintain a marriage, a close friendship or a stable parent-child relationship. Traumatized people find it difficult to trust even those who love them most, including partners and kids. This can be very tough on friends and families, often leading to estrangement or divorce.

> _"People who have not shared the traumatic experience cannot be trusted, because they can't understand it."_

### 3. Flashbacks cause people to relive the mental and physical experience of trauma when they’re reminded of it. 

Do you ever remember something embarrassing you did and feel yourself squirm or blush? Then you've got a tiny insight into how memories of trauma can impact the body.

When a PTSD sufferer is reminded of their trauma, their body and brain enter a high-stress mode, since they experience the memory as if it were real. This is called a flashback, an impact of trauma that the author studied in an experiment he carried out with his patients.

Each patient agreed to listen to a recording of a script that recreated their traumatic experience. As the script played, participants inhaled air with a tiny concentration of radioactive particles. This air would be visible in a brain scan, allowing the author to see which areas of the brain were active when patients remembered their trauma.

Marsha, a 40-year-old teacher, was first up for the experiment. Her script took her back to the tragic accident that caused her to lose both her five-year-old daughter and the unborn child she was pregnant with at the time.

As Marsha listened to the script, her blood pressure and heart rate rose sharply. Activity in the left half of her brain, the side responsible for rational thinking, slowed down and effectively "deactivated." A deactivation like this makes it difficult for PTSD sufferers to realize that the things they hear, see and feel during a flashback aren't real.

In Marsha's brain, the _Broca's area_, the area responsible responsible for speaking, showed a significant decrease in activity, leaving her unable to speak. Her stress hormone levels shot up and stayed high. For the mentally healthy, stress hormones will spike and then decrease as soon as a threat has passed. But for those with trauma in their past, these hormones take much longer to return to normal levels.

This goes to show that being reminded of trauma can be almost as horrifying as experiencing the traumatic situation itself.

### 4. Childhood trauma has negative impacts, not just in a person’s youth but well into adulthood, too. 

Traumatic experiences are hard enough to deal with as an adult, but there is nothing more difficult than facing trauma as a young child. With brains that aren't even fully developed, children who undergo a trauma are at greater risk to experience a wide range of negative consequences. These consequences surface in the years immediately following their experiences and later in adulthood.

Traumatized children often expect bad things to happen. The author demonstrated this in an experiment in which cards with pictures from magazines were shown to children who had experienced trauma, and to those who hadn't.

One of the cards showed two children watching their father fix a car as he lay underneath it. While children without trauma imagined a story based on the image where the father successfully repaired the car and took his kids to McDonald's, the traumatized children imagined much darker scenarios.

One girl said that one of the children in the images would smash the father's head with the hammer he was holding. Another child said the car would fall, crushing the father's body. For these children, the pictures contained a number of triggers that led them to imagine the scene ending violently.

These thinking patterns often persist into adulthood.

Take Marilyn, one of the author's patients and a former nurse. She told the author she had a happy childhood, but this wasn't true. Marilyn was sexually abused as a child, a traumatic experience that shaped her life as an adult.

She was prone to lashing out when men touched her, even in her sleep. She also developed an autoimmune disease that damaged her vision, which likely emerged as a result of the stress her trauma caused on her body.

Marilyn's case may sound extreme, but she isn't alone. Many others who were traumatized as children continue to suffer in their adult lives.

### 5. While normal memories fade and change, traumatic memories are vivid, unchanging and easily triggered. 

When we tell stories, we tend to embellish, exaggerate or omit parts of our experiences. By the fifth time you've told a story, chances are it'll be quite different from the first version. We even remember things differently over time. Why is this?

In general, we don't tend to memorize the sensory details of events. Most of us remember what we did or how we felt in general, but don't store vivid memories about the smell of the room we were in or the exact details of someone's face. But it's a different story when it comes to traumatic memories — we remember these situations vividly, and the memories don't change over time.

The author demonstrated the difference between these two ways of remembering by asking participants to recall important but nontraumatic events in their lives, like the birth of their child or their wedding day. In these cases, participants could recall their general feelings, like happiness or nervousness, but they didn't have a detailed image of how their partner's hair looked at their marriage, for instance.

However, when participants were asked to recall traumatic memories, smell, taste, touch and hearing played a far more important role. One participant who was raped said that a specific smell of alcohol reminded her instantly of her trauma, so much so that she couldn't go to parties anymore.

We also recall traumatic memories consistently, without changes or revision. In a study conducted at Harvard Medical School, 200 men were tested on a regular basis from their first joining the experiment, which they did between 1939 and 1945, up until the present day. The subject of these tests were their memories, and how trauma, or lack thereof, shaped them.

Many participants were World War II veterans and subsequent PTSD sufferers. While the memories of participants who weren't traumatized by the war changed over time, the veterans' memories didn't change at all. They remained consistent for well over 45 years after the war ended.

Trauma stays with you, both in your body and your brain. So how do people learn to live with it?

### 6. EMDR allows patients to integrate their memories and restores a sense of agency over their mind and body. 

It may sound simple, but one of the most effective techniques the author uses involves only a finger moving back and forth across a patient's field of vision. While the patient follows the finger with their eyes, they're guided through a traumatic memory and encouraged to make new associations along the way.

This technique is known as EMDR, or _eye movement desensitization and reprocessing_.

While EMDR may sound simple enough, there is some mystery surrounding it, since little is known about how or why it's so effective. What is clear, however, is that it helps patients by _integrating_ traumatic memories.

This is important, because if memories aren't integrated, they can still play out before the eyes of a PTSD patient as if it's happening in real time. Once a memory is integrated, it can finally become another past event in the life of a patient and cease having a troubling life of its own.

EMDR, as with other techniques described in these blinks, helps a patient control their relationship to traumatic events by allowing them to integrate it into their memories. In doing so, the patient can develop a new and healthier relationship to a traumatic memory, along with a sense of control over their mind, body and lives.

Astounding results have been achieved through EMDR.

Many years ago, Kathy approached the author at 21 years of age, after having just attempted suicide for the third time. Kathy had been forced into prostitution by her father, during which time she'd been gang raped by her father and his friends and sexually assaulted with beer bottles.

With the help of EMDR, Kathy was able to observe her traumatic memories and repackage them in a way that gave her agency over them. The key to this process was that the sessions allowed her to bring new, imaginative associations to the surface.

For example, during one session she pictured a bulldozer crushing her childhood home, destroying the scene of so many traumatic memories. In another, she imagined locking her father out of a cafe, making him feel helpless and watching everyone around him laugh.

After eight sessions of progress like this, Kathy made a remarkable recovery. And 15 years later, when the author reconnected with Kathy, he was happy to see a healthy and happy woman who was considering adopting a third child.

### 7. Yoga offers trauma sufferers a safe way to explore the relationship between their body and mind. 

Our body and mind share a close relationship. In order to live a balanced, stable life, we need to understand how our emotions work, and how they impact our bodies. Unfortunately, trauma can make this very difficult.

Trauma often leaves people with a hypersensitive alarm system in their bodies. Those who suffered sexual abuse as children, for example, find that they experience crippling panic in harmless situations, such as cuddling with their partner.

To avoid this, traumatized people often attempt to numb their own feelings by drinking too much, taking drugs and even by overloading themselves with work. These provide a temporary solution, but tend to do more damage than good to a person's mental health. Thankfully, there's a healthy way to cope with overwhelming emotions in the aftermath of trauma: yoga. 

For trauma sufferers, yoga offers a safe way to get in touch with their emotions and understand how the body experiences them. Annie, one of the author's patients, decided to give it a try. As a rape victim and PTSD sufferer, the first yoga classes were incredibly difficult for her. Even a gentle pat on the back could trigger her brain's alarm system.

Despite this, Annie stuck with yoga, refusing to give up. Soon enough, she noticed that her body was constantly sending her signals about her emotional state. In particular, Annie struggled with the yoga position of the "happy baby," which requires you to lie on your back with your knees bent and your feet up in the air.

Though Annie felt incredible pain, vulnerability and sadness in positions like these, she didn't push those feelings away, choosing to explore and accept them instead. Yoga helped Annie come to terms with negative sensations like these, and helped her realize that she could deal with them head-on, rather than repressing them.

> _"Trauma robs you of the feeling that you are in charge of yourself."_

### 8. Mindfulness and supportive relationships are essential to trauma recovery. 

Mindfulness is a pretty trendy concept right now, but it's not just a fad — it's an incredibly effective lifestyle choice. It also constitutes a powerful tool for trauma recovery, but how does it work?

Mindfulness is all about maintaining a conscious awareness of your body and your emotions, rather than denying them. This is especially tough after trauma, as painful memories cause us to repress our emotions rather than address them.

None of us like to feel sad, angry or broken, especially when these feelings are triggered by memories of trauma. But by pushing these feelings away, you also lose the opportunity to confront your trauma and start the healing process.

Mindfulness can alleviate the psychological and physiological impacts of trauma, from depression to stress to psychosomatic conditions like chronic pain. It can also improve immune responses, activate regions of the brain that help regulate emotions and balance out stress hormone levels.

Aside from mindfulness, supportive personal relationships are indispensable on the road to recovery from trauma. By building a network of family members, friends and mental health professionals, patients can ensure they always have someone to turn to when they need help. These networks can be formed through AA meetings, religious congregations and veterans' organizations, to name a few.

> _"Trauma robs you of the feeling that you are in charge of yourself."_

### 9. Neurofeedback helps trauma sufferers rewire their brains. 

Did you know that electrical signals are responsible for just about everything that goes on in your brain? These _brain waves_ govern our thought processes, so they're quite important. Unfortunately, they can also be damaged by trauma. Let's find out how.

There are many different types of brain waves, one of these being _alpha_ waves, which are triggered when we feel calm and relaxed. A recent study at the University of Adelaide in Australia examined soldiers who served in Iraq or Afghanistan, and revealed that the longer they spent in the war zone, the fewer alpha waves their brains produced. Instead, soldiers produced brain waves similar to those of children diagnosed with ADHD, hampering their ability to relax, stay calm and focus.

Fortunately, the brain has the potential to recover. How? Through a process called _neurofeedback_.

Neurofeedback allows traumatized people to change the brain waves they produce, and encourage the production of alpha waves to help them relax and keep calm. By displaying the patient's brainwaves to them in real-time on a screen, they can see when they need to make a conscious effort to relax. Once they do, they'll see their alpha waves being produced and even be rewarded through an interface that can feel like a video game.

Take Lisa, a 27-year-old woman and one of the author's patients. Lisa's father abandoned the family when she was three, and her mother was abusive and cruel. Lisa ran away from home twice and passed through several foster homes, mental hospitals and even spent time living on the street.

Years of trauma left Lisa with strong self-destructive urges. She'd hurt herself and destroy the things around her, with little ability to regulate her emotions. But once she began neurofeedback treatment, things changed dramatically. With her newfound ability to produce alpha waves and consciously make herself relax, Lisa was able to talk about and work through the traumatic events from her childhood.

Though neurofeedback has proven extremely effective, it's still rarely applied. Overall, our society has a long way to go when it comes to understanding and dealing with trauma. But with a broader acceptance of mindfulness and improved knowledge about mental illnesses in recent years, there is good reason to be optimistic about the future of mental health treatment.

### 10. Final summary 

The key message in these blinks:

**Although trauma can happen to anyone, not many of us know how traumatic experiences impact our mental and physical health, even decades after the event. Mindfulness, support networks, EMDR, yoga and new techniques like neurofeedback are all essential tools for trauma sufferers as they learn to accept, cope with and recover from their trauma.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Brain's Way of Healing_** **by Norman Doidge**

_The Brain's Way of Healing_ (2015) highlights the human brain's amazing ability to change its structure and develop new ways of coping with disorders. The brain, whether by being "rewired" to process information in new ways or by being "trained" through repetitive exercises, can overcome debilitating diseases and heal itself.
---

### Bessel van der Kolk

Bessel van der Kolk, MD, is a physician, researcher and teacher specializing in post-traumatic stress. His other books include _Psychological Trauma_ and _Traumatic Stress: The Effects of Overwhelming Experience on Mind, Body, and Society._ Van der Kolk is a professor of psychiatry at the Boston University School of Medicine and directed the Trauma Center at Justice Resource Institute.

