---
id: 5aa7bb21b238e10007af44aa
slug: the-book-en
published_date: 2018-03-16T00:00:00.000+00:00
author: Alan W. Watts
title: The Book
subtitle: On the Taboo Against Knowing Who You Are
main_color: 89945A
text_color: 6A7346
---

# The Book

_On the Taboo Against Knowing Who You Are_

**Alan W. Watts**

_The Book_ (1966) is about the big questions in life. What's the meaning of it all? Where do we really exist in the universe? Author Alan Watts guides the reader on a voyage of discovery that questions popular assumptions about what's important in life, how the universe functions, and the nature of God.

---
### 1. What’s in it for me? Learn the wisdom of being one with the world. 

We often talk about how many distractions there are today and how they can keep us from living in the present and truly connecting with the people around us. Back in the 1960s, when there were no pop-up ads and social media notifications, Alan Watts already thought the people around him were failing to connect with each other as human beings or tackle the bigger questions in life.

Indeed, struggling to understand one's place in the universe and find spiritual fulfillment is nothing new. So the words of wisdom Alan Watts provides are still relevant today. Maybe more relevant than ever!

His message is one of connectivity — and not just with our fellow women and men, but with all of creation. After all, the same forces that made us also made the grass, trees, birds, animals and other creatures, so we are all connected at a very fundamental level, and the sooner we embrace this, the better off we'll be.

Watts was ahead of his time but now's the perfect moment to dive into his refreshing philosophy and start living a more fulfilling life.

In these blinks, you'll discover

  * a glimpse into the ancient wisdom of the Vedanta;

  * what language can tell us about people's perceptions; and

  * why the inevitability of death needn't be a cause for concern.

### 2. Few taboos remain in Western society, except the truth about what it means to be human. 

As society moves from one generation to the next, we tend to shed taboos along the way.

In many societies, sex was the major taboo. In Japan, families were far too embarrassed to speak openly about sexual matters. Instead, young newly-wed couples were traditionally given a "pillow book" for their honeymoon, containing details of sexual positions and letting the couple know everything they needed to know about lovemaking.

In today's Western culture, sex is hardly a taboo anymore. Instead, you can find it being used on billboards to sell products and referred to in most pop songs.

But one important taboo has remained, and it's the discussion about what it means to be human.

Often, the way in which we define and understand ourselves as human beings is wrong. We think that each person is a unique organism, living a life that is completely separate from those of others and the environment.

This misunderstanding of our place in the world is apparent in how we speak: Phrases like "we came into the world," or a person who "must face reality," all suggest that we see ourselves as belonging to some entirely distinct and unique world or other reality.

But this is all an illusion and self-deception on our part. The truth is, we're born _from_ the world, not into it.

Much as the ocean creates waves, the planet earth creates people. While each wave is a movement happening on the surface of the ocean, it isn't separate from the ocean or created any differently than another wave. The same is true of every person on the planet.

Nevertheless, we continue to think of ourselves as unique, independent individuals.

In the next blink, we'll take an even closer look at the ways in which we deceive ourselves.

> _"We do not need a new religion or a new Bible. We need a new experience — a new feeling of what it is to be 'I.'"_

### 3. Human beings have long ignored the truth about identity because the rational mind cannot grasp it. 

When you consider the big thinkers who've tried to get to the bottom of human identity, you might think of Freud or Jung and their groundbreaking research on the unconscious mind.

However, these psychologists, as well as the majority of people throughout history, have been ignoring a basic truth about human identity. And this truth is _so_ simple that it's difficult to even express it with words.

Germans have a word for truths like this: _Hintergedanken_. This is something everyone intuitively knows and agrees upon while not liking to think about.

However, popular culture has helped prop up the egocentric idea of the rugged individual as a lonely speck in the universe. And now, this notion is so strong that it can seem impossible to accept the truth, which is: _an individual is not just a drop of water in the ocean_. Rather, we are part of an entire universe of energy responsible for the creation of everything, including human beings, nuclear energy fields and outer space.

Therefore, our real identity — what we talk about when we say "I" — is ancient and infinite. It is the energy vibrations that flow in and out and continue to take on many forms.

Yes, this isn't exactly an easy concept for the rational or intellectual mind to grasp, and that's a major reason why it continues to go largely unacknowledged.

Trying to intellectually understand what "I" stands for is a lot like trying to look at your own eyes without a mirror — it's impossible. Or, if you are looking in the mirror, it's like trying to determine what color the mirror is. You might see green leaves or a blue sky, but these don't pertain to the mirror itself.

However, as we'll see in the next blink, there is a helpful tool for partly understanding the human identity: it's the concept of God.

> _"When the game has gone on long enough, all of us will wake up, and remember that we are all one single Self."_

### 4. People have long understood that God is all-pervasive, but few have truly experienced this truth. 

When people think of "God," many imagine an all-seeing person who lives in the sky and has the power to control everything. And yet, throughout the ages, there have been many other ideas of what God is.

In ancient India, many people followed the Hindu philosophy of Vedanta, which teaches that God is everywhere and part of everything, rather than existing in some separate realm.

The texts of Vedanta are called the _Upanishads_, and they contain dialogues, poetry and stories dating back to 800 B.C. They offer a revealing look at how an ancient civilization viewed the divine.

Since Hindus at this time recognized God within everything and everyone, being an expression of God wasn't something to be prideful of or something that set you apart. Rather, knowing that God could be found in everything else gave people a strong incentive to be respectful of other people and animals — even plants and minerals.

However, when it comes to achieving spiritual enlightenment, recognizing God in all things isn't a means to an end. According to the Vedanta, this comes from _experiencing_ the all-pervasive presence of God. And this isn't something you can force or make happen, though meditation is known to bring people closer to it.

As you might imagine, experiencing the presence of God is a life-changing and perception-altering experience that very few have actually been through.

It's the kind of experience that Jesus likely had, and the one he was referring to when he said: "When you make the two one, and when you make the inner as the outer… then shall you enter the Kingdom." We can interpret this as Christ's way of saying that when you recognize that you and the world are one in the same, and begin to unite yourself with the world around you, then you can experience enlightenment.

### 5. We are misguided in our belief in the illusion of cause and effect. 

We tend to think of a lot of things in binary terms. There's either light or darkness, sound or silence. There's one particular binary relationship of which we're extra fond: _cause and effect_.

This is the belief that everything we see and everything that happens is the result of some other prior event. But if this were really the case, it would mean that we had no free will, because there would always be some earlier event causing the current one to happen. This means we're always reacting and never making a decision that isn't influenced by some other decision. Pretty crazy, right?

Nevertheless, we buy into the concept and carry on with a narrow outlook on the world, like viewing it through a small hole in a fence.

Think of it this way: What if you never saw a cat before, but then one day you're looking through that small hole, and suddenly you see the head of a cat pass by, then a second later you see the body of the cat, and then you see its tail. If you keep looking, the cat will likely walk by again, and you'd see the same sequence: first the head, then the body and, finally, the tail.

Subscribing to the theory of cause and effect, you could think that one event causes the other, so when the head appears it causes the tail to arrive shortly afterward. But this is nonsense. It's not cause and effect. The head and tail are part of the same thing!

We waste so much time trying to break things down and wondering why something happens. It's better to see everything as part of the same universe. When you open your eyes to take in the big picture and recognize the world as one interconnected organism, you'll save yourself a lot of confusion.

### 6. Our limited perspective and attention create the illusion that life has to be either one way or another. 

To understand how to see the big picture, it makes sense to first understand why and how we get bogged down in the details.

To begin with, looking at something is an active process since there are always different things to notice, even when they're all part of the same object. So it's not just a flower, there are the roots, stem and petals, along with the different colors of the stem, petals and so on.

This is how our attention works: it's a process of selecting or choosing what exactly we're going to focus on. After we decide what to focus on, we then have to document our findings. To do that, we need a symbolic system like writing to give an accurate representation. This way, we can look at what's sprouting from the branch of the conifer tree and precisely describe it by using the words "needle-like."

By studying the words that are available in different languages and the depth of a person's vocabulary, we can see how perceptive someone is and to what he pays attention. The famous example here is the various Inuit languages that have an abundance of words to describe different kinds of snowfall. It makes sense that people in the Arctic are going to pay more attention to snow and therefore have a bigger vocabulary for it than the English language does.

The Japanese have _Yugen_, which describes the sensation of aimlessly wandering through a forest. The English language has no translation for this word, suggesting that we don't particularly notice this feeling or, at the very least, give it far less attention than the Japanese do.

What is clear is that our selective attention creates the illusion that life is made up of opposites and either/or scenarios.

For example, common logic says that during the day there is only light or daylight, while at night there is only darkness. But in fact, there is plenty of light and darkness no matter what hour it happens to be; it's just a matter of our perception and what we're capable of recognizing. During the day there are more light waves — so many that we can't consciously pick up on the darkness between them. But it's still there, just as there's still plenty of light during the evening.

So there's no reason to think of day and night as opposites. Instead, just think of them as two parts of the same thing.

### 7. Some cultures and religions make us fear death, but we can benefit from embracing the inevitable. 

Death is another persistent taboo in Western society. No one wants to talk about death or even think about it. If it were up to them, most people would stay eternally young.

You might think it's understandable to be scared of death, but the real reason it frightens us is due to the way it's presented in Western religions.

Christianity is responsible for the popular notion that death is followed by a Final Judgement, wherein anyone who hasn't lived a flawless life may get a ticket to Hell. But the Church's vision of Heaven isn't exactly paradise. Here we're supposedly being watched over at all times by a bearded God, and the most fun anyone has is playing a harp and singing hymns.

Most people think that the only other option is the atheist afterlife — or nothingness. And given that neither of these scenarios is particularly comforting, it's hardly a shock that people fear death, or that doctors and friends will keep trying to reassure patients that they won't die.

However, there are alternative scenarios that haven't been embraced by Western cultures. In these, death provides us with a doorway to spiritual growth. So, in these belief systems, an ailing patient could instead be encouraged to face death with the understanding that it's an opportunity. It's an opportunity to let go of the ego and the identity that he has fooled himself into thinking was so special.

The early twentieth-century Greek spiritual teacher, G. I. Gurdjieff, believed that people would greatly benefit from having a constant reminder of death's inevitability — not just for their lives but for the lives of everything around them.

After all, the only time people tend to let go of their egos is when they're at death's door. So, it's in this frame of mind that people can live as their true selves, as part of a universe with an infinite and endless God.

### 8. Our universe is rapidly changing the way we live, but the presence of God will remain constant. 

There's a good reason science fiction is so popular. It gives us the chance to discover parallel universes, explore space with brilliant androids — the possibilities are endless. Every day seems to come with new scientific breakthroughs, so the question on many people's minds is: what's in store for human beings?

In one sense, technology will make us more connected.

Already in the author's day, vast amounts of information from around the world was available directly in peoples' homes. Whether it was a peek into a foreign culture, a sports event or a musical concert, television and radio gave people the chance to experience it vicariously.

The author believed that this nebulous electronic engagement would continue to extend the reach of humans and allow them to connect with other people in a virtual space. Without even leaving home, they could connect to the things they needed and communicate directly with the people they wanted to meet. And if we continued further down that path he thought it was quite possible that the human race would eventually resemble a colossal organism with one shared central nervous system. Sounds a little like a prediction of the internet, doesn't it? The author offered a warning.

As our world gets increasingly interconnected, human beings are bound to become more homogeneous since everyone will be living in the same virtual life experience. Plus, it's only natural that privacy will disappear in this environment — so much so that our thoughts may no longer be our own. Instead, they will belong to one incredibly powerful, technological hive mind.

But even if technological progress seems out of control and worrisome, you can take solace in knowing that God is still present in everything that is created.

Remember, the universe, and everything in it, is a game that God has devised, and if people make everything too homogenous, predictable and controlled, then change is bound to come. No one, including God, wants to play a boring game. So chances are, God will find new ways to be recognized.

### 9. Final summary 

The key message in this book:

**Most humans are still driven by their egos, so they like to think that they are special and unique individuals separated from the rest of the universe. Not only is this thinking wrong, but it's also very spiritually unfulfilling. So embrace the reality: We are all part of the huge universe of energy that created us and everything else around us. Once you start to connect with the world, you will come closer to your true identity.**

Actionable advice:

**Don't take things too seriously.**

To have humor and be able to laugh at yourself is one of the essential skills in a world that is essentially a game of hide-and-seek with God. So be sure to lighten up whenever possible. The truth as we experience it is very relative and subjective. Remember: all things pass and will appear again.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Wisdom of Insecurity_** **by Alan Watts**

In _The Wisdom of Insecurity_ (1951), author Alan Watts discusses the paradoxical nature of modern life: we pursue goals and covet material goods that promise happiness, but which leave us feeling empty and more anxious than ever. As we indulge in unproductive thoughts about the future or the past, we tend to forget about what is most meaningful — the present moment.
---

### Alan W. Watts

Alan Watts was an alternative theologian and new-age philosopher who died in 1973. He was a specialist in Zen Buddhism and other Eastern philosophies and religions. His many publications include _The Joyous Cosmology_, _The Way of Zen_ and _The Wisdom of Insecurity_.

