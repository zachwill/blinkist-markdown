---
id: 56fe54708249800007000039
slug: the-botany-of-desire-en
published_date: 2016-04-05T00:00:00.000+00:00
author: Michael Pollan
title: The Botany of Desire
subtitle: A Plant's-Eye View of the World
main_color: E87C36
text_color: 995224
---

# The Botany of Desire

_A Plant's-Eye View of the World_

**Michael Pollan**

_The Botany of Desire_ (2001) explores the complex and fascinating relationship between humans and plants. In these blinks, we'll see how plants manipulate humans by taking advantage of our four basic desires for sweetness, beauty, intoxication and control, and how, in turn, we help plants reproduce and even grow stronger.

---
### 1. What’s in it for me? Discover how plants make us serve them. 

That pretty potted plant on your desk may look all passive and innocent. But, just like you, it comes from a long line of cunning survivors. And humans play a decisive role in the survival strategies of plants. In these blinks you'll learn more about this phenomenon, exemplified by the cultural history of a handful of the most popular plants in the world — the apple, the tulip, cannabis and the potato.

You'll also find out

  * how humans first discovered the mind-altering qualities of cannabis;

  * about a time when one tulip bulb could make you wealthy; and

  * how the war against drugs helped to improve marijuana hugely.

### 2. Just as humans benefit from plants, plants benefit from humans. 

Chances are you've been taught the story of the birds and the bees and know how animals and insects help spread pollen, allowing plants to reproduce. But do you know the role humans play in helping plants?

As a matter of fact, plants can use humans to serve their needs.

As humans, we like to think of ourselves as the _subjects_ : the ones who think for ourselves and are in charge of everything. And we like to think of plants as the _objects_ : passive things that exist to serve our purposes. After all, we choose to plant the seeds, right?

But what if the plant is actually the subject, causing us to act in its interest?

For example, the bee could appear to be the subject by using the flower's nectar for food. But the plant is really in charge, attracting the bee that will spread its pollen and using the insect to reproduce. And humans are helping plants in just the same way by spreading and planting seeds.

Plants do this by appealing to our basic desires.

We can see this in the farmer who plants an apple tree; by offering a sweet apple that humans can use for food, the plant is employing us to plant trees, thereby ensuring the species' continued survival.

Humans have four basic desires that plants can take advantage of — sweetness, beauty, intoxication and control. Many domesticated plants can address one of these desires. For example, the apple tree uses sweetness.

Plants have developed this ability for a simple reason: they can't travel on their own to spread their seeds.

Therefore, plants produce sweet nectars that bees desire and food that humans crave, and thus ensure that their seeds will continue to be dispersed. Likewise, an oak will produce acorns that squirrels bury for safekeeping and often forget about — and so another tree is planted.

So now that we know more about how plants take advantage of our desires. In the next blink, let's have a closer look at the relationship between humans and apples.

### 3. Johnny Appleseed brought apples and cider to the American frontier. 

You might think that North America has always been home to a wide variety of apples. But actually, only one kind is native to the region: the barely edible crabapple. While colonists tried to introduce European apples to America, most did not survive in the harsh new climates.

The numerous apple orchards throughout North America today are largely the result of one man: John Chapman, a.k.a. Johnny Appleseed.

The US climate was unsuitable for European trees. But John Chapman realized that every apple seed contained a combination of genes. And if you plant enough of them, the odds are that some will grow and prove perfect for the new environment.

So, starting around 1800, Chapman collected and planted apple seeds in huge numbers and set up many nurseries along the western frontier. By the time he died in 1845, he had contributed to 1,200 acres containing millions of apple trees. Because of his work you can now find apple trees everywhere in North America. 

Chapman succeeded because he was able to predict where the North American frontier would next expand and get there ahead of time to plant his trees. In this way he was like a real estate developer, but instead of land he made his fortune selling trees to the settlers who would eventually arrive. His business was helped by a law requiring settlers to plant at least 50 apple or pear trees on their new land.

Chapman's success, and that of the apple, was also due to sweetness.

Sugar was still a luxury good that most early nineteenth-century Americans couldn't afford. But the apple provided a naturally sweet food that could be made into cider, which also proved very popular and added to Chapman's good fortune.

Americans living in the time of Johnny Appleseed could choose from a wide variety of apples that are simply no longer available. In the next blink we'll see how and why this has changed.

### 4. Today, variety in apples has decreased and only two things matter: beauty and sweetness. 

Nowadays supermarkets are filled with countless cereals and sodas. Just a century ago you could go to a market and find thousands of different kinds of apples, too — but that's no longer the case. 

So what happened?

You can still find these varieties in places like the Plant Genetic Resources Unit in Geneva, New York. As part of the US Department of Agriculture, the service aims to collect and preserve fruit and vegetable crops. This includes an orchard with around 2,500 different kinds of apples.

Some of the trees that can be found here come from Kazakhstan, where the apple likely originated. But botanists are not simply trying to create an apple museum, they are trying to preserve and increase the genetic variety of apples. This is because the more apple genes we can keep alive, the more natural defenses we can use to help apples fight disease and pests. 

Unfortunately the market for apples today has limited this abundance to focus on two things: beauty and sweetness.

What we consider desirable in an apple nowadays has been reduced to a few basic characteristics: a flawless red color and an appealing shape. Likewise, our desire for sweetness has resulted in apples that have a satisfying sugary flavor.

It used to be rare for apples to even have a hint of sweetness to them. Such apples were considered so precious that they inspired Jonathan Swift to write that they contained, "sweetness and light . . . the two noblest things." 

Now that sugar is abundantly available, the sweetness that comes from apples can be considered bland compared to the sugary snack foods that can be found everywhere. It has led us to no longer appreciate the subtle sweetness the apple once provided. 

So, over time we've reduced the once-abundant variety to only the most intensely sweet specimens such as the Red and Golden Delicious. 

While apples show how our relationship with plants is affected by our sense of taste, in the next blink we'll see how plants have also tapped into our visual desire for beauty.

> _"Could it be that sweetness is the prototype of all desires?"_

### 5. We can’t resist beautiful flowers – they catch our attention and can even ruin us. 

What is it that makes flowers so meaningful to us? Compared to apples they aren't that delicious. Yet, we're more tempted to give a loved one a single red rose over a basket of apples. So what makes flowers so special?

Of course, it is our desire for beauty that has always attracted us to flowers.

In fact, finding beauty in flowers is considered so natural that psychiatrists find a patient's indifference to flowers a sign of clinical depression.

Many cultures throughout history have looked at flowers as the embodiment of beauty itself. The Egyptians would make sure their dead had flowers with them to take on their final journey. 

It's even possible that an appreciation of plants and flowers was crucial to our ancestor's survival. By being able to identify blossoms and know when plants were going to bear fruit, they increased their chances of making it through another year.

But people have also gone crazy over the beauty of flowers.

During Holland's Renaissance in the seventeenth century, the Dutch experienced "tulipmania" and it destroyed lives and nearly ruined the economy. 

In 1634, the price of tulips began to soar, and by 1635, trade in tulips was done through _promissory notes_ : vouchers that ensured the buyer a specific tulip delivered on a certain date. Quickly, a bubble emerged as the notes began to be treated like currency.

Everyone was using notes and some even invested their entire savings in tulip trading. The price began to escalate and at the height of the madness a single bulb of a Semper Augustus tulip was worth as much as the most expensive canalside house in Amsterdam. 

But the bubble burst in 1637 when an auction took place and people refused to buy tulips anymore. It ruined many people who were unable to trade back the enormous amount of money they'd invested.

So, obviously the desire for beauty is strong in humans. But as we'll see in the next blink, there are other desires besides taste and beauty that a plant can take advantage of.

### 6. Marijuana satisfies our desire for intoxication and prohibition has made it even more potent. 

Since the Garden of Eden, there have been plants that we refer to as forbidden fruit. Even now we consider marijuana to be one of these taboo plants. So what is it that keeps drawing us to them?

Humans have a deep, in-built desire to alter our state of consciousness — even children love to spin around and get thoroughly dizzy.

Plants have often helped us achieve these altered states; aside from the people who live in regions where certain plants don't grow, every culture has used psychoactive plants to alter perceptions.

Marijuana, or cannabis, is a plant that satisfies our desire for intoxication.

Marijuana can ease the mental and physical pain in our lives and create a cheerful mood. Those who use it also find that they perceive moments more intensely, even feeling as though they are experiencing things for the first time. This can lead to paranoia and some stupid behavior, but it can also result in creativity.

As it turns out, the laws against marijuana ended up improving it.

Before the 1980s, most marijuana was grown outdoors. But then the government started cracking down in a war against drugs, and so the plant was cultivated in secret indoors. And growers started to notice that nature was actually holding back the potential of the plant.

As growers started to recreate natural conditions indoors, they experimented with the five growth factors — nutrients, light, water, heat and carbon dioxide levels. In doing so, they found they could change the plant and increase the levels of _THC_, marijuana's primary psychoactive component, from 2–3 percent up to 15–20 percent.

But aside from changing the consciousness of people, marijuana research has also led us to further understand the human brain.

### 7. Research on cannabis taught us much about the human brain. 

Did you know pigeons love to get high? They actually have a habit of snacking on cannabis plants and it is likely that we first realized the mind-altering nature of cannabis by observing intoxicated birds.

In the 1930s, cannabis was prescribed by doctors to treat things like pain and nausea. But as it grew in popularity, research was intensified to find out more about its effects on our brain.

In the 1960s, researchers discovered the psychoactive property of cannabis: _delta-9-tetrahydrocannabinol_, otherwise known as THC. 

Then, in 1988, pharmacologist Allyn Howlett revealed that there is an entire network of neurons in our brain that is specifically receptive to THC. 

Neurologists were aware that our brain has many different specialized neural networks related to certain "feel-good" chemicals like endorphins, dopamine or serotonin. These networks get activated when _receptor cells_ are triggered by one of these chemicals.

But while it makes sense that the brain has special cells related to specific chemicals, it was surprising to find out the brain has special receptors for a plant substance like THC.

Also surprising was a discovery in the 1990s made by organic chemist Raphael Mechoulam: he found that the brain produces a similar substance to THC, a cannabinoid he named _anandamide_. Mechoulam borrowed the name from the Sanskrit word for "inner bliss."

This means that the brain has unique receptors that not only respond to a plant-based substance, but also to a cannabinoid that the brain itself produces.

Researchers are still looking into how this cannabinoid network works and its purpose. One theory is that it's related to pain relief and short-term memory loss, both of which are helpful for women in labor. This theory is supported by the presence of cannabinoid receptors in the uterus.

Another possibility suggests the network helps us get through the monotonous and boring parts of our life.

Whatever the case may be, it is clear that cannabis is targeting our desire for intoxication. In the next blink, we'll find out about how the potato pleases our desire for control.

> _"Anandamide may not only dull the pain of childbirth, but help women forget it later."_

### 8. The potato satisfies our desire for control – even more so now that we control its genes. 

You might think of the potato as a boring staple food but history shows us that this vegetable is extremely important to us.

Times were very tough before the introduction of the potato; many people experienced malnutrition and famine, especially in northern European countries. 

Finally, in the sixteenth century, the potato was introduced to Europe and people could satisfy their desire for control over their food. 

On a small patch of land people could grow their own potatoes and feed their family. They were no longer dependent on being able to buy bread, and with milk and potatoes they could meet their nutritional needs.

The nations that adopted the potato saw an end to malnutrition and famine and as a result they grew stronger and more powerful. It's even possible that the introduction of the potato may have led to power shifting from the southern European countries to those in the north.

Our desire has even led us to control the potato through genetic modification.

Potatoes are naturally vulnerable to the _Colorado potato beetle_, which eats the leaves of a potato plant, causing the growing tuber to starve. But science has brought us the _New Leaf Potato_ : An invention of the Monsanto biological corporation, which has been genetically engineered to produce a toxin that kills potato beetles.

Today there are 50 million acres of US farmland producing _genetically modified organisms_ (GMOs) like the New Leaf Potato. And with creations like this it seems clear that we are continuing to assert our desire for control over our food.

But on the other hand, it is uncertain if we've taken this desire too far. We still don't know the risks of growing and eating GMOs, and many consumers distrust these products.

### 9. Final summary 

The key message in this book:

**Our relationship with plants is not as one-sided as we think.**

**We rely on plants just as they rely on us: they use us to spread their genes, and we use them for food, beauty and even power. Don't be fooled by plants that may appear dull and useless — they may know more about your own desires than you do.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Omnivore's Dilemma_** **by Michael Pollan**

We face an overwhelming abundance of choices when it comes to what we eat. Should you opt for the local, grass-fed beef, or save time and money with cheap chicken nuggets? Organic asparagus shipped from Argentina, or kale picked from your neighbor's garden? _The Omnivore's Dilemma_ examines how food in America is produced today and what alternatives to those production methods are available.
---

### Michael Pollan

Michael Pollan is a writer and professor of journalism at the UC Berkeley. His other books include _In Defense of Food, Food Rules_ and _The Omnivore's Dilemma,_ which was named one of the Ten Best Books of 2006 by _The New York Times._

