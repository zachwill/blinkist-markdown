---
id: 52e7caa93635320008050000
slug: the-bottom-billion-en
published_date: 2014-01-29T09:00:00.000+00:00
author: Paul Collier
title: The Bottom Billion
subtitle: Why the Poorest Countries Are Failing and What Can Be Done About It
main_color: 415A34
text_color: 415A34
---

# The Bottom Billion

_Why the Poorest Countries Are Failing and What Can Be Done About It_

**Paul Collier**

_The Bottom Billion_ (2007) focuses on the specific problems of the 50 poorest states in the world and the traps that keep them impoverished. These states are drastically behind even developing nations and are in serious need of help from wealthier nations if they are to ever achieve economic self-determination. Drawing on his original research, Collier points out the pitfalls of the conventional methods for dealing with this extreme poverty and offers unique policy recommendations that cater to the unique struggles faced by the world's poorest nations.

---
### 1. For the world’s poorest countries, it is impossible to escape poverty without securing economic growth. 

When watching the news, we are often met with shocking images of extreme desperation and poverty from the world's poorest countries. We are also left wondering why they are so poor. Is it the result of conflict, corruption or lack of industry? 

While all these factors contribute to poverty, in reality the key ingredient to continual poverty is in fact poverty itself. The effects poverty has on a country, such as government corruption or poor infrastructure, make economic growth more difficult, and this lack of growth subsequently makes the country poorer. This vicious cycle is known as _cyclical poverty_, and economic stagnation is both its cause and effect.

War demonstrates this concept perfectly. Economic stagnation can imbue a population with desperation and hopelessness, both of which make it easier for the military or rebels to recruit, and thereby for megalomaniacs to wage war and squander resources. As an effect, war itself reduces economic growth by a factor of 2.3 percent per year, intensifying the economic stagnation that helped to fuel war in the first place.

So what measures can a country take to get itself out of cyclical poverty?

Essentially, it needs to find a way to achieve growth. Only through economic growth can the absolute poorest nations attack the roots of poverty itself: economic stagnation and the desperation that accompanies it.

But how can these nations spur economic growth? Firstly, they need to attract foreign money in the form of aid or imports. Secondly, this money needs to be directed not at the symptoms of poverty but at areas that accelerate economic growth, such as transportation infrastructure (roads and ports) or industrial development.

### 2. The longer poor nations remain poor, the harder it is to integrate them into the global economy. 

We often hear the term "developing world" used to describe all countries that are not part of the international elite such as the United States or Western Europe. 

Yet, this simplistic way of thinking obscures the huge differences between the poverty faced by developing nations and the poverty of the very poorest nations.

In fact, the world's poorest are considerably worse off than the other developing countries. For example, the average life expectancy in developing countries is 67 years, while in the poorest regions it is a mere 50. Likewise, a staggering 36 percent of the population of extremely poor nations suffer long-term malnutrition, compared to 20 percent in the rest of the developing world.

The primary reason for this disparity is that the poorest nations simply aren't developing. With each passing year, this lack of development causes the wealth gap between the poorest nations and the rest of the world to widen.

One major reason for this is that many of the poorest nations have already "missed the globalization boat."

In the past, developing nations like China and India were able to enjoy high-speed industrialization and an influx of capital as wealthier nations looked for cheaper manufacturing options. However, the world's poorest nations were left behind during this wave of globalization, and have not been able to catch up since.

As this gap widens, the potential for the poorest nations to catch up becomes ever harder.

This is because when industrialized countries trade with each other, they share access to technological innovations, allowing them all to constantly improve their economic potential. The poorest countries, however, can't do this, since they often have very little industry. So as the rest of the world continues to grow and develop, the poorest countries are effectively standing still.

### 3. Plagued by persistent and bloody conflict, the poorest nations have a harder time escaping poverty. 

Nothing wreaks more havoc on a country's economy than war. The sheer cost of violent conflict — a staggering $64 billion on average — and the accompanying humanitarian crises that affect the entire region make war an extremely costly undertaking.

And unfortunately for the world's poorest countries, they are far more likely to experience such ongoing, costly conflict than wealthier nations. In fact, an extremely poor country stands a depressing 1/6 chance of falling into a conflict over a five-year period.

This is due to the strong correlation between poverty, growth and war.

Conflict is often caused by economic stagnation. This is because economic stagnation hurts income, which leads to civil unrest and a desperate population. Unfortunately, the wars brought by poverty wreck the economy further, which in turn reproduces the conditions for more conflict.

It is because of this depressing cycle of bloodshed that poor countries that have just gone through a conflict have little better than a 50 percent chance of maintaining peace during the first decade.

One reason why such unending conflict is so damaging to the economies of poorer nations is the fact that violent conflict disrupts all legitimate means of producing revenue. In order to support war, all means of production are commandeered for military concerns, and this prevents goods from seeing it to market, thus further harming growth.

For example, during times of war, food commodities are either donated by or stolen from farmers who would otherwise have taken those goods to market and thus helped the nation's economic growth — a growth that is necessary to lifting an embattled country out of the cycle of violence. War's disastrous effects on economic growth keep the poorest nations poor.

### 4. The revenues from newly discovered natural resources can do the world’s poorest nations more harm than good. 

Although we might intuitively think that additional revenue, especially from a much in-demand resource such as oil or diamonds, could only help a country to achieve economic self-determination, the reality is considerably more complicated — not to mention depressing.

In fact, discovering high-value natural resources can actually damage a country, both economically and politically. 

The discovery of natural resources can affect a poor country's economy through a phenomenon known as _Dutch disease_. Dutch disease occurs when the high demand for a country's newly discovered resource hurts its other exports.

For example, say a country discovers a huge diamond deposit, which it then mines and sells on the international market. As buyers clamor to gain access to this resource, more and more foreign money flows into their economy, making their currency more expensive and causing higher prices.

This kills their economy, because less sought-after exports are unable to stay competitive on the international market due to their now higher prices. While this country might be experiencing amazing revenues from mining diamonds, the wider economy will be stagnant.

Receiving huge amounts of revenue from natural resources damages more than just the economy; this wealth can also end up enhancing the power of corrupt politicians.

In governments without sufficient transparency, these excess revenues provide money for private patronage, whereby aspiring — and corrupt — politicians bribe citizens or opinion leaders in order to achieve power.

Once these corrupt politicians achieve power, they are then free to create a system that favors them.

The poorest nations often have little budgetary transparency, meaning that it is easy for corrupt politicians to divert funds into the hands of a few. Once this culture of corruption has been established, it can infect the entire system, moving away from the stealing of commodity revenues to other sectors, such as the arms industry or construction.

### 5. Poor landlocked nations rely heavily upon their neighbors for economic growth. 

In this global marketplace, economies are closely connected to each other, and the success of one country flows over the borders of its neighbors. This is especially true for landlocked countries; for example, if a country's growth increases by 1 percent, then on average its landlocked neighbors' economies grow by 0.7 percent.

These, however, are _global_ statistics; they don't reflect the realities for the world's poorest nations. These nations enjoy only a 0.2 percent growth for every 1 percent gained by their neighbors.

Why do poor landlocked nations appear to be at such a disadvantage here?

One reason is that poor landlocked countries with little industry are extremely reliant upon their neighbors' transportation infrastructure. Moreover, their neighbors have little to offer in terms of functioning transport corridors, because they too are often suffering from extreme poverty. Other nations that have access to the sea cannot easily provide goods to landlocked nations whose neighbors have poor roads, which therefore increases shipping costs and slows growth. 

Because of their reliance on the success of their often poor neighbors, strategies for overcoming poverty are limited for landlocked nations.

One possibility is to lobby for aid for their neighbors in order to improve the transport corridors to their own country. This would therefore lower the cost of transporting goods in and out of the country, granting them greater access to the global market and helping to stimulate growth.

Another possibility is to avoid being "air-locked" by developing high-quality airports. This would allow them to circumvent the transport infrastructures of their dysfunctional neighbors' entirely, thus giving them greater access to the global market by making it easier to import goods.

While being landlocked does not doom a nation to poverty, it offers no advantages and ties the fate of a nation to the fates of its neighbors.

### 6. A corrupt political system causes governments to squander precious resources and stifles attempts at reform. 

Systems of checks and balances, governmental transparency and fair elections are standards for functioning democracies, partly because they put limits on the ways government corruption can affect societies.

But in some societies, these checks and balances do not exist — and the opportunity for government corruption is very high.

This incentive for corruption in countries with minimal or dysfunctional government is especially high where there is also extreme poverty. One reason for this is that, in these countries, minimal governmental transparency is combined with unchecked access to high resource revenues or large amounts of international aid. Corruption is therefore relatively easy and profitable in these countries.

A great example of such a country is Chad, a country with poor checks on government and where scrutiny of public finances in minimal. In 2004, only 1 percent of money released by the Ministry of Finance actually made it to the rural aid clinics for which the money was allocated. The wastefulness in this case is simply too extreme to chalk up to typical bureaucratic inefficiency — the money was clearly stolen.

Unfortunately, it is very difficult to break the cycle of corruption once it has begun. Even when informed citizens want to enact change, it is an uphill battle. Politicians entrenched in corruption have little to gain from reform, and thus have little incentive to campaign for it. This is because they directly benefit from deficient government; for them, reform is simply a salary cut.

This is also reflected in statistics. The longer a leader has been in charge in a failing state, the less likely the country is to make an economic turnaround. The reasoning here seems almost obvious:

Why kill the goose that lays the golden egg?

### 7. Financial aid packages work only when they are catered to the specific needs of poor nations. 

Money that is given by wealthier governments to poorer nations, with the aim to ease economic or humanitarian crises, is called _financial aid_. It is the largest form of assistance for the world's poorest nations. 

However, despite its popularity, it is not always the best way to transfer wealth from wealthy to poor countries.

This is partly because of where the aid is distributed. In the poorest nations, there is often a lack of government transparency, and sometimes there is even a complete lack of government. This means that the aid doesn't always make it into the hands of those who need it most; it is either stolen or put to another, less humanitarian use.

For example, it is estimated that, on average, 40 percent of the total military budget of Africa — the continent that receives the most aid by far — is inadvertently funded by aid. Unfortunately, that money, once handed over, is impossible to recoup.

This isn't to say that aid is necessarily bad — it's just that it must be very precisely delivered.

One way to ensure that aid makes it to its intended destination is to bypass corrupt or inept government altogether.

For example, we could do this by using aid to fund an _independent service authority_ : an internationally run organization that, through aid, provides government services such as education and medical care, and helps to develop infrastructure, without being accountable to a corrupt government.

Such independent service authorities would be composed of an international group of technical specialists and would be completely open to public scrutiny. This means that the independent service authority would be competent, unbiased and trustworthy. It could therefore serve as a benchmark for how proper government should function once the country in crisis no longer requires such intensive aid. Knowing this, citizens could hold future government accountable to such guidelines.

### 8. Deploying international peacekeepers can help create an environment for peace and reform without unnecessary bloodshed. 

The reason for deploying international peacekeeping forces is not necessarily to defeat an opposing army; the mere presence of a committed foreign military could help to bring peace by preventing conflict from resuming.

This is because foreign militaries act as an independent and neutral source of power, which makes warring parties think twice about reinitiating violence. 

One way that foreign forces help the peace process is by limiting the power of domestic armies. Often, the domestic military is the strongest power in post-conflict or failing states, and they can sometimes use this power to undermine legitimate government or even attempt a coup d'etat. The presence of foreign forces can prevent the domestic military from trying this.

The presence of a foreign military can also keep governments from inadvertently sabotaging the peace process.

For example, often after the end of a civil conflict there is an uneasy truce between government and rebels. Both sides expect the other to break the truce, and the government tries desperately to replenish its forces and prepare them for future conflicts. The mixture of high tensions and renewed military might is a recipe for disaster that can send post-conflict nations spiraling into a cycle of violence.

However, since the presence of committed foreign militaries acts as a protection against coups and rebellion, reform nations can allocate funds towards projects that help to stabilize and rebuild the nation rather than spending them on rebuilding the military.

But these strategies only work when the foreign military is willing to stand up to demagogues. Take, for example, the French Ivory Coast, where French forces didn't want to draw blood and allowed a coup to succeed simply because the coup's leader promised to hold elections. Unsurprisingly, he ended up being an autocrat. This situation could have been prevented had the French military simply intervened.

### 9. International charters provide incentives for dysfunctional governments to enact positive reforms. 

Because the wealthy Western nations tend to be the most economically powerful, they are the ones who establish the norms that the rest of the world must adhere to in order to trade with them.

This immense power gives wealthy countries the opportunity to play a decisive role helping the poorest regions to rise out of poverty. 

One way that wealthier countries may do this is to use _international charters_. These are a collection of rules that govern trade. These charters could be focused in a way that encourages positive changes in the governance of poorer nations.

For example, wealthier nations could establish an _international charter for natural resource revenues_, which would aim to alleviate the problems associated with the discovery of natural resources. They could order member nations to only allow companies to mine their resources if they have won a bid through a free and open tender invitation. This should reduce corruption and the waste of resource revenues.

However, just because an international charter exists doesn't mean that anyone wants to adhere to it. In fact, charters are sometimes met with great resistance from corrupt politicians. 

To combat this, countries that adhere to charters could receive preferential treatment over those that don't. 

For example, joining the European Union provides a nation with huge benefits such as a huge free market. Yet in order to join the European Union and benefit from membership, countries must adhere to certain agreed-upon norms, like democratic and accountable government. Not adhering to these rules forces a nation to remain outside the gates of this prosperous community.

Furthermore, Non-Governmental Organisations (NGOs), which are active in many nations, can use adherence to norms as prerequisites for receiving aid. If countries want the aid, then they must adhere to international norms in order to get it.

These norms, when used in conjunction with other tools for development, are powerful weapons against poverty.

### 10. Final Summary 

The main message in this book:

**The world's poorest population — the one billion people trapped in economic turmoil — are at risk of being permanently left behind in terms of development. Economic stagnation, coupled with specific economic "traps," cause the disparity of wealth between the world's poorest countries and developing countries to widen.**

**The Western world has the tools to combat this extreme poverty, but it must seriously rethink its strategies if it is to help the world's poorest countries reach economic self-determination.**

Actionable ideas from this book in blinks

**Before you choose to donate to an aid agency, research and understand their goals.** Do their goals contribute to economic growth, or do they merely treat the symptoms of economic stagnation?

**Campaign for initiatives that aim to increase awareness about the importance of growth.** Campaigns for food and medicine are easy to sell because they are so easy to empathize with, but ensuring economic growth is just as important in the war against poverty.

**Take into account how aid is spent in the countries you intend to donate to.** Does the money actually reach its intended destination? If not, perhaps consider donating to a different agency.
---

### Paul Collier

Paul Collier, formerly Director of Development Research at the World Bank, is Professor of Economics and Director of the Centre for the Study of African Economies at Oxford Universities and author of _Wars, Guns, and Votes: Democracy in Dangerous Places_.

