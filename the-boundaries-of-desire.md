---
id: 57af7c4c9400350003a45f2c
slug: the-boundaries-of-desire-en
published_date: 2016-08-17T00:00:00.000+00:00
author: Eric Berkowitz
title: The Boundaries of Desire
subtitle: A Century of Bad Laws, Good Sex and Changing Identities
main_color: A3925A
text_color: 665B38
---

# The Boundaries of Desire

_A Century of Bad Laws, Good Sex and Changing Identities_

**Eric Berkowitz**

_The Boundaries of Desire_ (2015) explores the checkered history of sexual relations and the law in the United States. These blinks show how women have struggled with sexual harassment and abuse, pointing out that a system run by men for men has simply perpetuated systemic injustice. What's more, you'll learn how US law historically has failed black people, homosexuals and children, too.

---
### 1. What’s in it for me? Understand why American law doesn’t do justice in matters of sex. 

Do we live in a world dominated by men? For centuries, men held the most influential positions in society, from politics to business. Yet only recently have issues of equality between the sexes truly become a hot point that society needs to take seriously.

When it comes to the law, however, the rules that govern relationships between the sexes were mostly written by men — most often to the detriment of women.

In these blinks, you'll learn how the law has failed to protect women who have suffered at the hands of abusive husbands. Male-dominated courts often protect their own in cases of sexual offense against women, blaming the victims and letting the guilty walk.

What's more, justice in America has fallen short in cases in which homosexuals, blacks and even children face discrimination or sexual abuse. All in all, the ills and sexual mores of modern society have outpaced the law, and it's high time that justice be served for all.

In these blinks, you'll also learn

  * how a husband in America could rape his wife without penalty;

  * why many registered sex offenders are minors; and

  * why sex for money while being filmed isn't considered prostitution.

### 2. Women in America have only recently earned freedom from the complete control of a husband. 

The struggle for women's liberation has been long, and replete with hardship. The bounds of marriage, not to mention rules over employment, have changed dramatically for women in recent generations.

Just a century ago, marriage could be a nightmare for a woman. At the time, marriage wasn't necessarily a sacred bond between two loving people, but more often a legal contract that favored the man.

A marriage essentially gave a husband the legal protection to abuse his wife, if he so chose. Further, courts rarely took serious action when a married woman was beaten or raped by her husband.

In 1874 in North Carolina, for example, a man named Richard Oliver whipped his wife with switches because he didn't like the bacon she had prepared for breakfast. As "punishment," Oliver was fined $10.

Historically, in both the United States and the United Kingdom, it was considered a wife's duty to provide sex for her husband whenever he wanted it. This "duty" was enshrined into law — essentially giving carte blanche for husbands to rape their wives — and was called the _marital rape exemption._

This law was abolished in the United Kingdom in 1991, and was taken off the books in the United States in 1993.

It wasn't until the second half of the twentieth century when husbands started to lose the absolute power they had held over their wives.

Women finally earned the right to work, which gave them more financial freedom, as they didn't have to depend solely on a husband for food or shelter. This freedom also meant that a woman could choose a partner based on mutual attraction, and not just financial security.

At the same time, women's attitudes toward sex also evolved, especially with the development of birth control. New forms of birth control allowed women to delay pregnancy, and in doing so, they had more freedom to explore relationships and sexuality, while also having the time to further their careers.

> _"If you can't rape your wife, who can you rape?" — Bob Wilson, California state senator, 1979_

### 3. Male-dominated courts have historically failed to protect women from sexual harassment or abuse. 

It's no secret that men have been the authors of the majority of American laws. It's because of this bias that so many women have been denied justice in court, especially when it comes to rape cases.

Until the mid-twentieth century, about half of American female rape victims ended up in prison themselves.

The judges of such cases were often men. They believed that women secretly wanted to be raped, so when a woman said "no" to a sexual advance, she really meant "yes." Based on this belief, many judges have tried to "protect" rapists from "malicious" or "false" women, even going so far as to punish a woman for libel against an attacker.

The credibility of a rape case also depended on the relationship between the rapist and victim. A woman's accusations were viewed as less credible the closer the relationship between her and her attacker.

In 1984 in North Carolina, for example, a man raped his girlfriend when she tried to break up with him. The court didn't believe the assault could be considered rape, as the couple had been in a relationship.

The case was subsequently dropped.

It won't come as a surprise that even back in 1736, Chief Justice of England Lord Matthew Hale said that rape was easy to perpetrate, hard to prove and easy to defend against if one was accused of it.

The courts have also failed to protect women from workplace harassment. After women entered the workforce en masse, sexual harassment became an issue. Yet cases brought to court of bosses sexually harassing or abusing female employees were often dropped.

In 1975, for example, two women who worked at the American pharmaceutical firm Bausch and Lomb tried to sue the company after a boss made several sexually inappropriate comments to them and sexual advances at them.

The court suggested that the boss did nothing wrong, justifying his behavior as merely "[satisfying] a personal urge."

> _"…the rules regarding sexual harassment have followed a similar track as rape law, adopting nearly all the_ 'she wanted it, don't believe her, she's crazy' _defences…"_

### 4. The legal system has failed to protect children from sexual abuse, and unjustly accused children of it. 

The law hasn't only failed to protect women from sexual assault, but it has failed children, too.

In the United States alone, over 17,000 underage victims have accused some 6,000 Catholic priests of sexual abuse in multiple cases. Yet few of these abusive priests have been registered as sex offenders and many cases against the priests have been subsequently dropped.

Courts, judges and the law have also failed teenagers in abuse cases, often misjudging a victim's relationship with the adult in question.

In fact, US law as late as 1998 stated that underage girls could only be considered the victim of a rape if the girl had had no prior sexual experience. In several cases in which a victim had already been sexually active, the rapist was not prosecuted.

The basis for such judgments was that the courts assumed a teenager sought out the sexual contact in the first place. In a case in 1914, even after a man admitted his guilt in raping a 14-year-old girl, the courts declared he was innocent as the girl had chosen to follow the perpetrator into his office.

Courts have also unfairly targeted children in cases of sexual assault. Parents naturally want to protect their children, but this feeling can manifest as paranoia about anyone who comes near the children, regardless of age.

In the late 1980s, people began to believe that children who displayed aggressive sexual behavior would grow up to be sex offenders. As a result, these children were then categorized with legal labels meant for adults.

According to a 2009 analysis by the US Justice Department, most people registered as having committed a sexual offence against a minor were themselves minors. One 12-year-old boy in 1996 was required to register as a sex offender after he gave his younger stepbrother a bath.

> _"The lamentable fact is that children are being treated as sex criminals for doing what kids have always done."_

### 5. Homosexuality was long a punishable offense, and gay rights didn’t become an issue until the 1980s. 

In the first half of the twentieth century, homosexuality was considered a mental illness that needed to be cured.

One popular theory suggested that homosexuality resulted from defects in a person's nervous system, which caused the person to develop improperly and "misdirect" sexual energy toward members of the same sex.

In the United States, homosexuality was a punishable offense. Gay men, if arrested, were often hospitalized or placed in mental institutions. Sodomy, which included anal sex, was not only criminalized in the United States, but also in Germany, Britain, Sweden, Austria and Denmark.

Lesbians were targeted as well, but for different reasons. They were considered too masculine, and thus likely to commit crimes of passion or, bizarrely, to have a taste for cigars. Women could even be arrested if they dressed like a man. In Chicago, for instance, it was a crime to wear clothes of the opposite sex in public.

In the 1980s, however, the gay community began to call for equal rights and equal protection under the law. Around this time, AIDS first appeared and was quickly associated with the homosexual community. While it is true that many of the first AIDS victims were gay men, early on the media claimed that _only_ gay men could be infected.

The community fought vocally against this misconception, and the fight spilled over into a larger call for equal rights for gays and lesbians, too.

The US Supreme Court struck sodomy laws off the books in 2003. As of January 1, 2015, some 36 American states allowed gays and lesbians to legally marry. In the summer of that same year, the Supreme Court ruled same-sex marriage to be legal in all 50 states.

> In 1920, the psychiatrist Edward Kempf coined the phrase "homosexual panic" — a term for anxiety "due to the pressure of uncontrollable perverse sexual cravings."

### 6. Nudity is tolerated in the context of art; sex work equally only if a camera is involved. 

Nudity can be considered a crime, but it can also be viewed as art. It all depends on where the naked person is, and who is watching.

Whether we as a society tolerate nudity depends on the context in which we see a naked body.

After World War I, performances by the famous dancer Isadora Duncan featured naked dancers who were modeled after Greek statues. Audiences enjoyed her performances as they invoked themes of the ancient world, in which sexuality was less restrictive.

Art performances that involved nudity were often considered high culture, so eroticism in performances was tolerated.

Erotic dancing in strip clubs, in contrast, was considered an unacceptable form of nudity. American law enforcement shut down many strip clubs, sending riot squads to arrest dancers and owners accused of "criminal conspiracy to violate public indecency laws."

There are plenty of double standards when it comes to sex and eroticism. For example, many countries allow pornography but forbid the practice of prostitution.

Prostitution is illegal in many parts of the world. But what is the difference between prostitution and adult pornography? In both cases, consenting adults are paid for sex. The only difference is that with pornography, there's also a camera involved.

Thus a paid sexual encounter isn't prostitution if it's being filmed. In fact, porn producer and director Harold Freedman was accused of pimping when he hired several models to have anal, oral and vaginal sex for a film called _Caught from Behind Part II_.

His case was excused, however, when the California Supreme Court claimed his work wasn't prostitution because it happened in front of a camera! The models, the court ruled, were merely paid to _simulate_ pleasure as part of their work.

> _"So long as sex remains a great and mysterious motive force in human life and a subject of absorbing interest to mankind, it's depictions will cause vexation."_

### 7. Sex workers are often discriminated against, though sex work historically has been a means of survival. 

We've seen that laws concerning sexual abuse often blame the innocent, and the same is true when it comes to prostitution.

Prostitution is now legal in many countries, including Australia, New Zealand, Switzerland, Canada, Germany and the Netherlands. It's still a crime in many other countries, however.

Prostitution in the early twentieth century was considered not just immoral but also unhealthy. Sex workers were thought to have serious mental conditions and were deemed unfit to reproduce.

This discrimination in some countries went so far as to force sex workers to be sterilized. Between 1930 and 1970, 60,000 female sex workers in Sweden alone were sterilized without their consent.

Different kinds of sex work are still criminalized. Some therapists, for instance, provide paid sexual services to clients who suffer from a psychological or physical handicap as part of treatment. Such services should be considered part of their work, but in France, for example, certain lawyers and government officials still seek to prosecute these therapists.

Throughout history, many women have turned to sex work for survival. In World War II, for instance, sex work was for many women the only means of escaping poverty and hunger.

In Germany, the Nazis even encouraged prostitution to increase productivity among male prisoners in concentration camps. Prostitutes were selected according to race and promised food and safety, as long as they cooperated.

One such woman was Magdalena Walter, a German prisoner who was selected to serve as a prostitute at the concentration camp in Buchenwald. She recounts how prisoners were allowed to visit her for sex every night, for a period of two hours. Each man was allowed 15 minutes.

> _"_ _In Zurich … authorities took a brilliant step in 2013 to outlawing streetwalking and installing drive-in sex boxes where prostitutes and johns can park and do their business."_

### 8. Throughout American history, many non-white people have faced discrimination in society and in the courts. 

Even though slavery in the United States was legally abolished in 1863, white people still found new ways to deny rights to and further oppress the black community.

In both the north and south, people widely believed that the end of slavery had turned black men into sexual criminals eager to target white women. George T. Winston, a white American educator, went so far as to say that freedom had transformed black men into "brutes."

As part of this irrational paranoia, white men worked hard to keep white women away from black men. As a result, interracial marriage was forbidden until 1967, as whites believed blacks had "inferior blood" and that offspring of interracial marriages threatened the white race as well as "Western civilization."

White people were frightened of black children, too. In the 1950s and 1960s, heated battles for civil rights took place in school yards. When a court ordered a school in Arkansas to admit nine black students in 1957, white parents demonstrated in front of the school.

Armed troops tried to prevent the black children from entering the building, and white students jumped out of the school's windows out of fear.

Other racial minorities in America have also suffered from racism. Before World War II, most anti-Asian racism was directed at Chinese people. Chinese men were thought to be latent rapists, and Chinese women were thought to carry sexual diseases that they themselves were immune to, but that were deadly to white men.

Evidently, sexism and racism are deeply embedded in American history, and the country's laws have reflected this.

> _"A host of theories…held that race was_ a biological _rather than social construction and demanded the ban of sex between races."_

### 9. Final summary 

The key message in this book:

**American laws on sexuality and gender have a checkered past. As laws and courts were historically run by men, the justice system was designed to benefit men at the expense of women. Thus women in society had virtually no protection from abuse or harassment. Historically, the legal system has also fallen short in its protection of children, black people, homosexuals and sex workers.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Sex at Dawn_** **by Christopher Ryan & Cacilda Jethá**

_Sex_ _At_ _Dawn_ argues that the idealization of monogamy in Western societies is essentially incompatible with human nature. The book makes a compelling case for our innately promiscuous nature by exploring the history and evolution of human sexuality, with a strong focus on our primate ancestors and the invention of agriculture. Arguing that our distorted view of sexuality ruins our health and keeps us from being happy, _Sex_ _At_ _Dawn_ explains how returning to a more casual approach to sex could benefit interpersonal relationships and societies in general.
---

### Eric Berkowitz

Eric Berkowitz is an author and human-rights lawyer. His work has appeared in a number of publications, including the _Los Angeles Times_ and the _Washington Post_.

