---
id: 56e696ce9854a50007000004
slug: the-boys-in-the-boat-en
published_date: 2016-03-14T00:00:00.000+00:00
author: Daniel James Brown
title: The Boys in the Boat
subtitle: An Epic Journey to the Heart of Hitler's Berlin
main_color: C09D59
text_color: 80683B
---

# The Boys in the Boat

_An Epic Journey to the Heart of Hitler's Berlin_

**Daniel James Brown**

_The Boys in the Boat_ (2013) tells the story of how a group of unassuming college boys from the University of Washington went from struggling through the Great Depression to securing a victory in the 1936 Berlin Olympics.

---
### 1. What’s in it for me? Follow a few young men in a boat as they beat Nazi Germany. 

From antiquity to the present day, politics have always played their part in the Olympic Games. But rarely have there been games so fraught with political propaganda as the 1936 Berlin Olympics. 

As if to spur on the Nazis' hubris, German oarsmen won five gold medals and one silver in six races. However, in sport, everything can change and the grand finale — the seventh race — was yet to come.

The story you'll find in these gripping blinks climaxes in Germany, but it starts on the other side of the pond. 

You'll learn about the struggles of eight working-class kids fighting their way to college, to the Olympic team and, finally, to the winner's podium.

On our way from Seattle to Berlin, you'll also find out 

  * about a lucky Stetson and the man attached to it;

  * about something called "the swing" and the difficulties and blisses of finding it; and

  * how a few hearty meals and some germs almost changed the course of a legendary race.

### 2. In 1933, many young men competed for spots on the University of Washington rowing crew, but few made it. 

In 1933, the United States was right in the middle of the Great Depression. Back then, ten million people — a quarter of the population — were unemployed and roughly two million were homeless.

People could barely afford to spend money on food, let alone their educations. But the University of Washington offered part-time campus jobs to anyone who made the rowing team, so would-be rowers were even more motivated to try out.

The competition was high. On Monday, 9 October 1933, 175 young men showed up to try out for the freshmen team and go through the series of grueling tests designed by the coaches. The head coach was Al Ulbrickson, a no-nonsense man with two national championships to his name. The freshmen coach, Tom Bolles, was dubbed "the professor" by the sports media. He was pursuing his masters degree, and he wore a beat-up Stetson hat for good luck.

The candidates spent the next few weeks discovering the physical demands of competitive rowing. Their muscles, bones and lungs were pushed to their limits — all under the unpredictable conditions of the Seattle weather.

By 30 October, the number of candidates had dropped from 175 to 80. 

Two of these candidates were Joe Rantz and Roger Morris, who both studied engineering. Many of the candidates were city boys, but Rantz grew up in a rural area and had built up his strength working with heavy equipment. 

Morris, on the other hand, was one of the only candidates with prior rowing experience. When he was 12, he'd rowed 15 miles from his family's summer home on Bainbridge Island all the way to Seattle.

### 3. Joe Rantz had a difficult childhood but kept his spirits up and did well in the end. 

Joe Rantz was born in 1914 in the logging town of Spokane, Washington. His father Harry ran an automobile repair shop. Joe's only brother, Fred, was 15 years older.

When he was four years old, Joe's mother died of throat cancer, which, unfortunately, was but the first of many setbacks. After his mother died, his father left for Canada and young Joe bounced back and forth between living with his aunt and living with his brother. 

Harry returned in 1921, remarried and tried to resume his parental duties with Joe. Harry and his new wife quickly had two more kids, Harry Jr. and Mike. 

Thula, Joe's stepmother, rejected him, seeing him as nothing more than a painful reminder of her husband's first marriage. Harry kept moving the family from mining towns to logging towns looking for work, and when Thula got pregnant with their third child, she gave Harry an ultimatum: either Joe had to go, or she would. Unfortunately for Joe, his father chose Thula. 

The 15-year-old Joe was left to fend for himself in the town of Sequim, Washington while the rest of his family moved on. 

Young Joe was able to keep his spirits up, however. He poached fish to sell to local shops and look advantage of Prohibition laws by building a lucrative bootleg alcohol business. He kept himself in school, played banjo in a local band and eventually fell in love with a young woman named Joyce. 

Joe's hard work paid off in the end: he graduated high school with honors and enrolled at the University of Washington.

### 4. The eight rowers have to work together as a unit, so it’s challenging to put together the perfect team. 

You can't master the art of rowing overnight, as University of Washington rowing candidates would soon find out. 

Perfect rowing technique comes down to getting the right _swing_, when the rowers are all in harmony with each other. The candidates for the freshmen team were competing for spots on a boat that would include eight rowers and one _coxswain_, the person who sits at the back of the boat and shouts orders to the person in the _bow,_ the rower at the front who sets the pace. 

It's crucial for the rowers to act as a unit. If just one person gets off rhythm, it'll throw off the whole boat. When each rower is in sync, they fall into their _swing_ : the blissful state when the whole team rows in unison and the boat coasts along smoothly and powerfully.

The faster and harder the rowing, the more difficult it is to maintain the swing. So the coach has to assemble just the right team members who can row in time with each other. 

Every year the University of Washington coaches put together three teams: a _freshmen team_, a _junior varsity_ team, and a _varsity_ team. The process would take months as the coaches ran several trial races swapping out the different rowers until they found the best team of eight.

On 28 November 1933, Coach Bolles read out the list of names for the freshman team's first boat assignments. Roger Morris was in the first position at bow, Shorty Hunt was in the number two seat and Joe Rantz was number three.

### 5. George Pocock and his boats had a huge influence on rowing. 

A good team also needs a good boat and George Pocock made the best. The legendary story of Pocock starts in England and ends in Washington. 

Rowing's legacy is closely connected with London's River Thames, where the Pocock family first began building their boats. George's grandfather, uncle and father were all in on the family business, and his father specialized in building racing shells for Eton College. 

George did more than just join the family business. He took up rowing as a teenager and studied racing technique, using his expert knowledge to design even better racing shells.

He eventually left London and relocated to Canada to work for the Vancouver Rowing Club, where in 1912 he caught the eye of Hiram Conibear, the "father of Washington rowing".

Conibear convinced Pocock to produce boats for the University of Washington, but George went even further: he helped Conibear refine a more efficient rowing style that became known as the _Conibear stroke_.

The Conibear stroke, which relied on shorter but more powerful pulls in the water, quickly led the university to be even more successful. But George Pocock had another trick up his sleeve: cedar.

By 1927, George was already handcrafting what were widely considered the best racing shells in rowing, but that year he made an even more revolutionary discovery. Racing shells had traditionally been made of spruce and mahogany, but George, always looking for improvements, soon discovered _western red cedar_. 

George noticed that Native Americans made their canoes from western red cedar, which is abundant in Washington. Its low density made it buoyant, light and easy to shape. It also polished very well, so it could glide along the water with very little friction. 

George's boats, which brought the team even more success, are still made and sold in Washington today.

### 6. The freshman competitions set the bar for the University of Washington team. 

The University of Washington rowing team's strenuous preparation all came down to two races: the Pacific Coast Regatta that takes place every April, and the Poughkeepsie Regatta, the national championship in June.

There was a lot on the line from the very beginning. Tom Bolles, the freshmen coach, hadn't lost a Pacific Coast Regatta in six years on the job. And the team's biggest rival, the University of California at Berkeley, coached by Kyle Bright, had won an Olympic victory in 1932. 

Bolles was still struggling to get his team in shape and he was worried that Joe Rantz might not be up for the job. To make things worse, the other teammates picked on Joe for his clothes and banjo playing, calling him "Hobo Joe."

Despite Bolles's concerns, the freshmen team did well. In the lead up to the Pacific Coast Regatta, they impressed him by beating the junior varsity team by two full lengths in a training race. 

When the Pacific Coast Regatta finally arrived, the University of Washington team shocked everyone by beating Berkeley by four and a half lengths, setting a new freshman course record. 

This pumped them up for the upcoming competition in New York, where they had even more to prove. 

In 1934, the Poughkeepsie Regatta was just as popular as the Kentucky Derby. Social status made tensions even higher, as the wealthier and more "sophisticated" East Coast crews competed against the boys from West Coast areas they associated with backward lumberjacks. 

In the end, the University of Washington team prevailed. They beat their closest competitors, who came from Syracuse, by five full lengths.

### 7. The team’s sophomore year started out with Olympic dreams but uneasy team dynamics. 

After their unprecedented victories in the Pacific Coast and Poughkeepsie Regattas, the University of Washington freshman team was singled out by the press as bound for the 1936 Berlin Olympics. But nothing was guaranteed.

The following year when the teammates were sophomores, Ulbrickson gathered them in the boathouse and told them that getting to the Olympics was now officially their goal. They cheered when they heard the news. 

But the sophomores still struggled to find the right swing. Tensions were high among the rowers: the other University of Washington teams now felt they had to prove themselves against the sophomores and started regularly beating them in practice.

Ulbrickson eventually pulled the group into his office and told them they were in danger of losing their chance at the Olympics. They were shaken by his talk but it strengthened the friendship between Roger Morris, Shorty Hunt and Joe Rantz, who were determined to make it to the Olympic boat. Shorty, who sat behind Joe in the third seat, liked to say "I got your back, Joe."

The sophomores eventually proved themselves to their coach. In April, Ulbrickson still wasn't sure who should compete in the main varsity race at the Pacific Coast Regatta but he took a chance with the sophomore boat. 

At the regatta, the new freshmen won their race by three lengths and the junior varsity team won theirs by an impressive eight lengths. The varsity race was a nail biter and spectators debated which team finished first, but the judges called it: the Washington University sophomores won by six feet.

> _"Like so much in life, crew was partly about confidence, partly about knowing your own heart."_

### 8. Despite the early winning streak, it was still unclear if they could send a boat to the Olympics. 

The sophomores prevailed at the Pacific Coast Regatta but it was the junior varsity team that left the biggest impression on everyone. With the Poughkeepsie Regatta right around the corner, Coach Ulbrickson was still unhappy with the sophomores' unreliable results.

The sophomore team's fate rested on their training for the upcoming Poughkeepsie race. Their delight at their win in California didn't last long: as soon as the teams started training for the next race, it was clear the junior varsity boat was stronger.

So Ulbrickson once again told the oarsmen that their varsity status was on the line. The results eventually spoke for themselves: just six days before the Poughkeepsie race, the junior varsity team beat Joe Rantz's sophomore team by eight full lengths. That made Ulbrickson's decision for him. He promoted the junior varsity boat to the varsity spot and then told the sophomores they would row in the second boat (i.e., as junior varsity) at Poughkeepsie.

The Poughkeepsie race changed the dynamics again, however. Tom Bolles's freshmen team aced their race in New York, winning by four lengths. Joe Rantz's junior varsity team also secured a clear victory by two lengths, but the newly promoted varsity boat fell painfully behind.

They finished two lengths behind California and Cornell, dashing Ulbrickson's hopes for another Washington sweep at the Poughkeepsie Regatta. 

The rowers were talented but still out of shape and unsteady with their results. It was clear that Ulbrickson and his teams had a lot of work to do the following year if they wanted to make it to the Berlin Olympics.

### 9. While the team trained, Hitler was preparing for a grandiose Berlin Olympics. 

The United States was still unsure of what to make of Adolph Hitler and the Nazi party in the lead up to the Berlin Olympics of 1936. However, the country was still recovering from the Great Depression and the losses of World War One, so most Americans wanted to stay out of another foreign war. 

But there was still a lot of opposition to Nazism and many called for a boycott of the Olympic Games. On 8 December 1935, the Amateur Athletic Union voted on whether or not they should boycott the games, but the union decided by a slim margin that they would compete. 

For Nazi Germany, the 1936 Olympics would be a grand display of propaganda. 

Leni Riefenstahl, a prominent filmmaker, had made a big impression on Hitler and Joseph Goebbels, the Nazi minister of propaganda, when she directed the groundbreaking film _Triumph of the Will_, which documented the 1934 Nazi party rally in Nuremberg. Hitler and Goebbels saw the 1936 Olympics as a chance to show the entire world the grandeur of the Nazi party, this time on an even bigger scale, so they hired her again to make a film that would be called _Olympia_. 

Hitler also saw the Olympics as an opportunity to deceive the outside world about the true nature of his rule. So in the months leading up to the Olympics, he rounded up Roma families and moved them to undisclosed locations. He also had anti-Jewish signs removed from public spaces and temporarily halted production of anti-Semitic propaganda pamphlets. 

After the Olympics finished, however, the anti-Jewish signs would be returned and the leaflets went back into print.

### 10. Ulbrickson mixed up the oarsmen and eventually found the perfect team. 

In January 1936, Al Ulbrickson gathered his teams and told them that anyone hoping to get to the varsity boat that year would have to face a grueling training season. 

Ulbrickson was prepared to shake things up. The only thing he was certain of was that Bobby Moch would be the coxswain, steering the others. Bobby was a year ahead of Joe, Roger and Shorty and was widely considered one of the most talented people on the team. 

Ulbrickson kept switching up the oarsmen looking for the right combination and Joe Rantz was nervous to find himself consistently left out. 

Finally, Ulbrickson made his decision in March: Don Hume was in the first position, followed by Joe Rantz, Shorty Hunt, Stub McMillin, Johnny White, Gordy Adam, Chuck Day and Roger Morris.

Joe Rantz was the last to make the cut. He didn't know Don Hume or Gordy Adam very well, but the team members all welcomed him when Ulbrickson made his choice. Joe was already close to Chuck Day and Johnny White, as the three had spent their break from school doing construction on the Grand Coulee dam. 

The oarsmen all had similar upbringings too. None of them were from cities or well-off families; they all had to struggle to get by, working tough jobs to earn money to stay in school. That made them fiercely determined to succeed. 

All in all, the eight team members were a great fit. Their close relationships and similar ways of thinking would help them find the perfect swing.

> _"All were merged into one smoothly working machine . . . a poem of motion, a symphony of swinging blades."_

### 11. The final US races tested the team’s drive, but their victory at Princeton secured the Olympic route. 

After Joe was finally added to the team, the newly christened _Husky Clipper_ varsity boat set out on the water. 

The year started off well for the boys: the team aced both the Pacific Coast and the Poughkeepsie Regattas. 

At the Pacific Coast Regatta, the Husky Clipper beat the Californian team by three lengths. In New York, the team kept rowing in perfect harmony, and Bobby Moch had a trick up his sleeve.

Moch kept the pace slow and easy in the beginning, so the oarsmen could save their energy for the end. He held them back longer than Ulbrickson anticipated, however, and the coach started to get nervous.

Washington was still four lengths behind with a mile left in the race, but in the final stretch Moch shouted out, "Give me ten hard ones for Ulbrickson!" It worked! The varsity boat surged ahead and won by a full length. So the team was off to Princeton for the Olympic qualifying race. 

They had a rocky start in Princeton. A few strokes in, Gordy Adam and Stub McMillin both threw the boat off with a bad pull. The error proved to be just a hiccup, however: the team held on, kept steady in the beginning and in the end, Bobby Moch yelled at Don Hume to increase the rate. 

The Husky Clipper surged past California and Pennsylvania, finishing, once again, a full length ahead. The boys were officially Olympic-bound.

Later on, Shorty Hunt would say that the last 20 strokes of the team's Princeton race were "the best [he] felt in any boat."

### 12. The Berlin Olympic race was the most difficult challenge the team had ever faced. 

After making the journey on the ocean liner the _SS Manhattan_, the boys finally arrived in Berlin on 23 July 1936.

Trouble started as soon as they got there. Everyone except Bobby Moch had gained weight on the trip over, thanks to the on-board buffet. Even worse, Don Hume contracted a respiratory infection. He could hardly get out of bed, let alone practice. 

The team was out of shape and the odds were stacked against them.

Before the race, Ulbrickson got some bad news. The team would be rowing in lane six, the outermost lane most exposed to the wind and weather. They'd have to row harder than all the others, and it probably wasn't a coincidence that Germany and Italy were in the well-protected first two lanes.

Bobby Moch put Tom Bolles's lucky Stetson hat under his seat as they waited at the start. As the race began, the crew pulled away at a steady pace, hoping to save energy for the end.

But Moch noticed that Don Hume's face was sickly pale and his eyes were closed. He yelled out, "Don! Are you OK?" but Hume didn't respond.

The boat struggled as they neared the end, five seconds off the lead. Moch kept screaming at Hume, urging him to pick up the pace, but Hume kept his eyes closed and still didn't respond. 

Finally, a miracle happened: Hume snapped back into it, opened his eyes and looked at Moch. Moch screamed, "Pick 'er up! One length to make up! Six hundred meters!"

Moch kept shouting "Higher! Higher!" as the team neared the finish line, pushing them to a stroke rate they'd never hit before. In the end, they prevailed: the American boat finished six-tenths of a second ahead of the Italian boat and one second ahead of the Germans.

### 13. Final summary 

The key message in this book:

**The eight boys in the** ** _Husky Clipper_** **who secured a rowing gold at the 1936 Berlin Olympics hardly had an easy start in life. Though they came from poorer, rural areas and started their education during the Great Depression, their determination, rigorous training, dedicated coaches and the fine Pocock boats they raced in gave them the edge they needed. Though their first oarsman was ill and they were given the most disadvantageous lane in the race, they pulled ahead to victory in the end.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Will It Make the Boat Go Faster?_** **by Ben Hunt-Davis and Harriet Beveridge**

_Will It Make the Boat Go Faster?_ (2011) shares with you the inspiration and strategies you need to succeed every day, as told through the emotionally charged experiences of a member of the gold medal-winning British rowing team at the 2000 Summer Olympics in Sydney.
---

### Daniel James Brown

Daniel James Brown is an award-winning historical narrative non-fiction writer. He's also the author of _Under the Flaming Sky: The Great Hinckley Firestorm of 1894_ and _T_ _he Indifferent Stars Above: The Harrowing Saga of a Donner Party Bride_.

