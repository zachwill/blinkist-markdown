---
id: 55bf5a8d63646100073a0000
slug: the-brain-sell-en
published_date: 2015-08-04T00:00:00.000+00:00
author: David Lewis
title: The Brain Sell
subtitle: When Science Meets Shopping
main_color: F57131
text_color: B25224
---

# The Brain Sell

_When Science Meets Shopping_

**David Lewis**

_The Brain Sell_ (2013) reveals innovative tactics that'll help marketers draw in customers by using the powers of neuroscience. These blinks explore strategies of psychological marketing, body language and sensory allure that every retailer should know, and that every customer should know how to avoid!

---
### 1. What’s in it for me? Learn how retailers use your brain to sell their products. 

Most of us know that the world of marketing and advertising is a dark and sinister place. We understand that dirty tricks and tactics are constantly being employed to get us to buy as much as we possibly can.

But do we know precisely what these strategies are? These blinks will give you a clear picture of how retailers use the latest neuroscience research to make you shop until you drop.

In these blinks, you'll learn

  * the difference between _going_ shopping and _doing_ shopping;

  * why no clerk should ever cross their legs; and

  * why using your dog's name as your password is just asking for trouble.

### 2. There are two ways of shopping: Going shopping and doing shopping. 

Shopping: it's a pretty simple task, right? You need something, you have the money for it, you buy it. Yet for decades, psychologists, neuroscientists and behavioral analysts have all studied the way we shop, and they've all drawn the same conclusion: it's not as simple as we think it is.

For starters, did you know that there are two types of shopper? Those who _go shopping_ and those who _do shopping._ What kind are you?

Some of us _go_ shopping. We do it to have fun, to experience brands, service and entertainment. We welcome shopping as a chance not just to gain new possessions, but to enjoy a break from the daily grind amid the bustling crowd. _Going_ shopping is, in general, highly pleasurable, and not at all something we _need_ to do.

On the other hand, there are those who _do_ shopping, and regard it as a chore. If someone tells you they've got to "do the shopping," you know that they'll be buying things they absolutely need, but don't necessarily want. They may even treat the experience as a necessary evil to be done as quickly and efficiently as possible. They certainly don't enjoy the experience.

When you _do_ the shopping you're like a one-person SWAT team rescuing a hostage. You rush in, grab the hostage (or, in this case, the milk, toilet paper and cat food) and get out ASAP.

Conversely, those who _go_ shopping linger in the store and admire the products, and are therefore naturally beloved of advertisers, manufacturers and retailers. These are the people you should attract to your brand. How? The next blinks reveal all.

### 3. Make your product a want-need with scarcity and clever marketing. 

Ever seen a new pair of sneakers and thought, "I have to have them!" You desire them so much that you can't keep them out of your mind over the next few days, and you're constantly struggling with the urge to buy.

In such situations, the thing you _want_ (the sneakers) has turned into something you _need_ — or, as we say in advertising-speak, the product has become a _want-need_.

One way you can elevate your product to the _want-need_ stage is by restricting its supply. Just consider the thousands of people who line up outside the Apple Store awaiting the release of the latest iPhone! Why doesn't Apple simply meet demand by increasing the number of iPhones sold during the first few days after release?

Because then the excitement and uniqueness would be lost. Because there's a limited number of iPhones available, the people who do manage to get their hands on one build the hype by showing it off to their friends and discussing it online. These early owners whet the desire of others, and, for a few days at least, get to feel like trendsetters.

But what about those who don't have the newest iPhone? They may start to feel a little inadequate — an experience you can also use to draw customers in. How? By making them feel they won't quite fit into their environment until they purchase your product.

This advertising technique goes all the way back to the 1920s, when Lambert Pharmaceutical Co. realized that many Americans struggled with an unpleasant problem: bad breath. The worst part being that you never knew if you had it, because people were too embarrassed to tell you.

Thus was born Listerine. The ads for the strong antiseptic mouthwash depicted dejected men and women who were eager for love but foiled by their foul breath. Listerine cashed in on customers' anxieties regarding love and acceptance, and the company's revenues increased from $115,000 to over $8 million over the course of 7 years.

### 4. Body language impacts how well you sell – and how likely you are to buy. 

If you thought body language was something you could forget about after your job interview was over, you've got it wrong. It's incredibly important in retail.

Imagine entering a store only to see the customer advisor leaning against the wall in the corner, arms folded across his chest. Doesn't look like he's too likely to help you out, right? Why is this?

Though we might not realize it, as customers we've got a keen eye for _high-power poses_ and _low-power poses_ that allow us to judge a sales representative as competent (or not!).

Wide stances or hands on hips create high-power poses, and may evoke self-confidence, competence or sometimes aggression. Compare this to low-power poses: folded arms or crossed legs. These poses are more relaxed but can also seem incompetent or dismissive — not great for sales staff! 

It's clear that posture can significantly impact your decisions as a customer, but would you have guessed that the way you're made to bend your arms in stores can change your buying choices, too?

Over our lifetimes we learn to associate bending our arms, or _arm flexion_, with a desire to acquire, while _arm extension_ is linked with rejection. Think about how you'd welcome your best friend — by flexing your arm to hug him. Or what about when someone you don't like comes too close — you'd extend your arm to push her away.

Because of this, companies should place products on lower shelves so customers don't have to reach out their arms to grab what they want. Arm-extension just feels counter-intuitive in this context! Even supplying shopping baskets instead of trolleys, which make people bend their arms rather than extend them, can encourage customers to pick up more items from the shelves.

### 5. Tempt customers with sounds and smells. 

The nutty aroma of freshly popped popcorn, the boom of an epic orchestra — we associate these sensory experiences with a night at the cinema. Such is the power of sound and smell, two things of which marketers should be highly aware!

Music affects our mood, and, in turn, our buying behavior. Many retailers use this fact to their advantage. If a company wants us to spend lots of time shopping in their store, they'll play slow-tempo music, and we unconsciously respond by moving slowly. One department store tried playing slower tunes and found their daily sales increased by 38 percent.

Even the genre of music can affect our buying choices. For example, when a wine store played classical music, they found that their customers did not buy more, but bought more expensive varieties. 

While our ears are being marketed to without our realizing, what's going on with our nose? Much the same thing. Just as popcorn brings a cinema to mind, certain other smells evoke feelings in customers that might help companies sell more.

A particularly innovative example of aromatic advertising comes from South Korea, where Dunkin' Donuts arranged to have the smell of coffee waft through public buses as their advertisements played onscreen. This made people aware that they were selling coffee, not just donuts. As a result, visits to their stores increased by 16 percent, while sales grew 29 percent.

Another interesting approach to aroma in retail makes use of the hormone _oxytocin_ — the very same chemical that reinforces the bond between mother and child. Some companies may use it to generate trust in customers that inhale it. Casino owners, for example, would be all for it!

By now, we've seen several ways that companies can encourage customers to be better shoppers. But what if you're a customer, and you want to avoid falling into these same traps? Then read on!

### 6. Defend yourself against marketers’ tricks by disrupting your thoughts. 

What can we do in the face of underhanded retailer tactics? Are we powerless — or can we fight back? We can! Here's how:

First, eliminate _impulse shopping_. You're most at risk at times when you're unhappy, or on vacation. The numbers back this up — around £24 billion are earned by retailers in the US and UK because of impulse purchases every year.

If you feel blue and want to go shopping to cheer yourself up, think twice. Do you really _need_ what you want to buy? Your sinking bank balance won't improve your mood! The same goes for vacations. Though you might be feeling incredibly happy and excited to be away, it doesn't mean you have to buy all five of those souvenir shirts!

But if you have trouble merely remembering to think twice, don't despair. There's a technique that may help.

The trick is to bring a fantastical image to mind. Such a thought can stop impulse shopping and free you from obsessive habits.

How?

Well, our conscious mind can only hold one major idea at a time. By giving your brain something crazy and vivid to imagine, you'll prevent it from dwelling on anything else, even your current purchase options. Though this distraction can only last a few seconds, it is enough for you to step back from the brink of buying and reconsider.

Psychologists often recommend imagining a pink elephant diving into a bowl of blue custard. Give it a go next time you're on the verge of buying your hundredth pair of sneakers! You'll find it easier to put them down and walk out of the store.

But what if you aren't shopping in a store, but are online instead? It's a whole different game of risks. Read on to find out how to guard yourself against them in the final blink!

### 7. Shopping online is dangerous, so be sure to take precautions! 

Online shopping — it's one of the most popular, yet most dangerous ways to buy today. Any careless click can bring us straight into the traps of thieves, frauds, scammers or worse. Want to stay safe on the web? Then follow this advice!

Use complex passwords, and change them regularly. Yes, it's a lesson you've heard before. But how many of you are still using your first dog's name or your mother's maiden name in your passwords? This simply won't cut it.

Even the name of your favorite band isn't a good choice — all it takes is a simple scan of your social media profile for hackers to uncover those crucial little details about you. A password that is abstract, combines letters and numbers and is regularly updated will protect you a bit more against the less scrupulous users of the web.

It's vital to remain aware of hazards on the internet, especially if you have kids. Children today are at risk of exposure to very violent pornography or of being groomed by pedophiles pretending to be children on social networks.

This doesn't necessarily mean you should deny your children access to computers, phones and other digital devices — they've got to know how to use them for life in the 21st century! Instead, help them to learn responsible online behavior, and monitor their access. This way, you'll encourage positive internet practices in their future, and you may even learn some new tricks yourself!

### 8. Final summary 

The key message in this book:

**A scientific approach to marketing — from calculated psychological tactics to increase demand to drawing in your customers with sensory experiences — can take your sales to the next level. As a customer, you'll have to watch your back, whether shopping in a store or online!**

Actionable advice:

**Get creative with those passwords!**

Next time you change the password of your email account think of some unusual ones such as a long sentence including numbers and punctuation marks.

**Suggested** **further** **reading:** ** _Why We Buy_** **by Paco Underhill**

_Why We Buy_ draws on observations of real shoppers' behavior to understand the way people make purchases. It presents advice on how to design and tweak stores to optimize the shopping experience for customers, and thereby increase sales.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### David Lewis

Dr. David Lewis, the "father of neuromarketing," is the foremost psychologist to apply the findings of neuroscience to the buying-brain. As co-founder and Director of Research at Mindlab International, he has worked with Fortune 500 companies and authored multiple bestselling books, including _The Soul of the New Consumer: Authenticity - What We Buy and Why in the New Economy_.

