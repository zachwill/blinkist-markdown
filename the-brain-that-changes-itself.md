---
id: 5678629a0bac9b0007000001
slug: the-brain-that-changes-itself-en
published_date: 2015-12-22T00:00:00.000+00:00
author: Norman Doidge
title: The Brain that Changes Itself
subtitle: Stories of Personal Triumph from the Frontiers of Brain Science
main_color: 835E38
text_color: 57663F
---

# The Brain that Changes Itself

_Stories of Personal Triumph from the Frontiers of Brain Science_

**Norman Doidge**

How can stroke victims who become paralyzed start using a fork or buttoning their shirts again? Well, contrary to what was believed for so long, the brain is not hardwired. It can change, regenerate and grow. Drawing on real-life cases of scientists, doctors and patients, _The Brain that Changes Itself_ (2007) shows us how, rather than relying on surgery and medicine, we can alter our brains through thought and behavior.

---
### 1. What’s in it for me? Discover the awesome regenerative power of your brain. 

Doctor Who and your brain have a lot more in common than you might think. Your brain, just like the Doctor, is able to regenerate itself time and time again. It can heal itself after injury, it can grow from experience and it can even change the physical strength of your body.

These blinks showcase the incredible elasticity of the brain. They explain just how well the organ can change and develop itself in response to injury, threats and stimuli — which is a big part of what makes us humans so adaptable.

In these blinks, you'll discover

  * why you're not stuck with the libido you're born with;

  * why you can think some of your problems away; and

  * how actively thinking may help us beat dementia one day.

### 2. The brain changes itself through processes like “unmasking.” 

For years it was thought that the brain, once fully formed, was fixed until it deteriorated with age. But with the rise of _neuroplasticity,_ we're discovering this couldn't be further from the truth. 

Neuroplasticity is the ability of the brain to continually change itself. The prefix "neuro" refers to neurons, i.e., the nerve cells in our brains and nervous systems, and the suffix "plastic" means changeable. The brain, then, alters its nerve structure and function through thought and activity.

But how exactly does the brain reorganize itself? One way is through _unmasking_.

Unmasking describes what happens when one neural pathway is shut off and a secondary one is exposed, the latter becoming stronger with repeated use.

Cheryl Schiltz is a great example of this phenomenon. For five years, each time she stood up, she would lose her balance. She had almost entirely lost the vestibular system of her brain — the area needed for balance. That is, until one of the pioneers of neuroplasticity, Paul Bach-y-Rita, designed a special device that Schiltz could wear.

The device, called an accelerometer, sent signals to a plastic strip containing electrodes, which had been placed on Schiltz's tongue. The sensations on her tongue were then redirected to the area in Schiltz's brain that processed balance, rather than going where they normally went: to the sensory cortex, the area that processes touch.

After much practice with the device, a new pathway in Schiltz's brain was unmasked and strengthened, and she began to regain her balance on her own.

> _"[Nature] has given us a brain that survives in a changing world by changing itself."_

### 3. Stimulating activities can change the structure of the brain. 

It's great that the brain can change, but don't we need some sophisticated gadgetry to help us? Actually, no. We can change our brain by simply being in stimulating environments where we can exercise our brain, not just our body. 

Activity can actually change the structure of our brain. Mark Rosenzweig at the University of California, Berkeley, was one of the first people to demonstrate this in his experiments with rats.

Rosenzweig found that rats in stimulating environments possessed a higher amount of neurotransmitters, weighed more and had better blood supply than rats in duller environments. From this we know that the brain can be modified by stimulation.

One woman named Barbara Arrowsmith Young took advantage of this revelation. Though in some ways she was brilliant — i.e., in her auditory and visual memory — she wasn't so advanced in others. 

Her biggest challenges included comprehending grammar, math concepts, logic and cause and effect. As a result, she was unable to read a clock because the relationship between the hands was baffling to her. When it came to conversation, she would think through simple conversations 20 times because by the time she got to the end of a sentence, she had forgotten how it had started.

After discovering the plasticity of the brain, Young set about conducting her own mental exercises which she carried out for weeks in the hope of changing her brain.

One of her exercises involved reading hundreds of cards picturing clock faces displaying different times. She would shuffle the cards so memorizing them wasn't possible, turn a card over, try to tell the time, check the answer on the back of the card and then quickly move to the next card. Each time she got a card wrong, she spent hours with a real clock, trying to understand why the numbers and the image on the clock remained the same. 

Her efforts paid off. At the end of her training, she was able to read clocks faster than the average person.

### 4. The brain is plastic, when the body changes the brain changes too. 

Eminent brain scientist Michael Merzenich is best known for his work with brain maps. By using brain maps, we can train specific processing areas in our brain, reorganizing it and altering how we think and perceive. But what are brain maps exactly?

Brain maps show which sections of the brain control which parts of the body and how the movements of those parts are processed.

In the 1930s, neurosurgeon Dr. Wilder Penfield discovered that areas adjacent to each other on the body were also generally adjacent to one another on the brain map. For example, the foot and genital brain maps are near to one another, which could be why some people have foot fetishes, as these brain areas can be linked!

Merzenich discovered that brain maps vary in their borders and sizes from person to person, and change depending on our activities throughout our lives. This is because a brain map can reorganize its structure in response to abnormal input. But how?

Brain maps compete for precious resources, so if a nerve no longer functions, other nerves will take over the unused map space for their own input. Merzenich and Jon Kaas discovered this in an experiment with monkeys.

The researchers severed the median nerve in a monkey's arm and, after two months, they found that the area of the brain map that served the median nerve was inactive when they touched the part of the hand that normally corresponded to it. However, when they stroked the outside of the hand, the median nerve map was triggered. The size of the brain map in the monkey had nearly doubled and had taken over what used to be the median nerve map.

From this, it was clear that the adult brain was indeed plastic.

### 5. Our sexual libido is also plastic and changeable. 

We've seen how we can recover and improve our skills, but brain plasticity doesn't stop there. Our sex drive is also flexible. The human libido can be easily shaped by psychology and past sexual encounters. But how does this work?

Plasticity is a characteristic of all brain tissue, though some brain regions are more plastic than others.

Both the _hypothalamus_, which regulates instinctive behaviors like sex, and the _amygdala_, which processes emotion, are plastic. So if these regions are changeable, then so is our sexual taste.

Sexual preferences are often learned during critical childhood periods, but they can also be learned later in life. In our critical periods, we can develop sexual and romantic preferences or inclinations that become wired into our brains and continue to impact us for the rest of our life. This is why, for example, people with cold or distant parents often choose partners displaying these traits, or sometimes take on these traits themselves.

But our preferences, romantic or sexual, can change.

Take modern pornography, which often combines a variety of different sexual preferences and imagery. Viewers may have all their latent sexual preferences stimulated while watching it. This unmasks early neural networks formed in critical childhood periods and either develops a new network in the brain or strengthens an existing one. Then, because the activity causes the release of dopamine, making the activity intensely pleasurable, the viewer repeats it, which strengthens the new network. This can result in a new kind of sexuality or a strengthened latent one.

The viewer might go on to develop a tolerance to the pornography. So if, say, the pleasure of sexual release is combined with aggressive imagery, it must then be augmented by the pleasure of aggressive release. Sexual and aggressive imagery then become intertwined and, as the behavior is repeated, it strengthens the networks in the brain. This phenomenon explains the increasing popularity of sadomasochistic themes in porn.

### 6. Brains can be rewired through shaping and brain exercises. 

Dr. Bernstein was an eye surgeon who lost the use of his left hand after he suffered a stroke at age 54. Hoping for some improvement, Bernstein began using Edward Taub's constraint-induced (CI) movement therapy. When he started, he couldn't lift a spoon to his mouth, or button up his shirt. By the end of his therapy, he was writing with his left hand and enjoying tennis three times a week. 

Bernstein had healed himself through simple exercises like wiping tables and cleaning windows — repetitive actions that rewired his brain.

Taub developed his therapy after experimenting on monkeys. He found that monkeys who'd lost spinal reflex and received no sensory input from their limbs, including their arms, continued to use their limbs; they'd simply never learned that their arms didn't function!

Interestingly, this doesn't happen when only _one_ part of the body loses input. Instead of using the limb that has lost sensory input, the monkeys, like us, use the arm which still receives sensory input. This is due to _spinal shock_, which happens when neurons have difficulty firing, and we consequently learn to not use the body part which has lost input. Spinal shock occurs immediately after losing input — e.g, due to surgery — and can last from two to six months. Taub called this _learned nonuse._

Taub believed that stroke patients might suffer from this but that the motor programs for movement could still be found in the nervous system. His idea was to constrain the use of the working limb and force the injured one to move. And it worked!

_Shaping_ is another helpful technique that incrementally molds new behavior. Instead of administering a reward only when a task is finished — e.g., reaching for food — rewards are given for _every_ movement made towards it. This makes the training remarkably effective, especially if the skill is practiced daily and concentrated into a short span of time, known as _massed practice_.

### 7. Brain scans enable us to develop plasticity-based treatments to help OCD sufferers. 

We all worry. But for someone affected by anxiety or obsessive-compulsive disorder (OCD), worrying escalates to degrees that can feel almost impossible to get a grip on. Thankfully, understanding the plasticity of our brain can help break the anxiety cycle and a host of other undesirable habits.

Brain scans can give us a better understanding of afflictions like OCD. They show a considerable difference between the brains of OCD patients and those unaffected by it, as revealed by UCLA psychiatrist Jeffrey M. Schwartz.

When most of us make a mistake, we feel that something is wrong, and a little anxiety builds as we seek to correct it. Then, after we've rectified the situation, our "something-is-wrong" feeling and anxiety dissipate. For OCD sufferers, it's a different story. This third stage never occurs, so the worrying persists. Brain scans have shown that the culprit is a malfunctioning caudate nucleus, which is the region of the brain responsible for turning worry off.

Using these scans, we can develop effective treatments for cases of OCD based on our knowledge of plasticity. For example, we can voluntarily switch on the caudate nucleus ourselves by purposely focusing on something else, like helping someone or playing a musical instrument. Activities that involve another person are especially conducive to holding the patient's focus. But even if you're alone in your car and OCD strikes, you could have an audiobook on hand to take your mind off it.

As a new pleasure-giving brain circuit forms, it rewards the new activity and creates more neuronal connections. The new circuit then competes with the older circuit and, after repeated use, the stronger circuit remains.

> _"With obsessions and compulsions, the more you do it, the more you want to do it; the less you do it, the less you want to do it."_

### 8. Imagination alone can help the brain overcome the pain of lost limbs. 

Losing a limb is a devastating event. But what if your missing limb caused you pain on top of that? This is known as phantom pain, feeling pain in a limb that is no longer there.

Phantom pain often afflicts soldiers with amputations and those who have lost limbs in accidents, but it's part of a larger group of pains that have no source in the body.

For example, some women suffer from menstrual cramps and labor pains after the removal of their uterus, and men can still experience ulcer pain after the ulcer and nerves have been removed.

So what can be done about it? Advances in neuroplasticity have found a solution.

Neuroplastician V. S. Ramachandran posited that the brain map for the lost limb, still eager for input, releases nerve growth factors inviting neurons from a nearby map to send connections to them.

So Ramachandran decided to fight one illusion with another, and find a way to transmit signals to the brain to make the patient think that the nonexistent limb was moving. In this way, they could then unlearn the phantom pain. 

To go about doing this, Ramachandran invented a mirror box to trick the patient's brain. The box would show a patient the mirror image of their working hand to fool their brain into believing their amputated hand had been somehow brought back to life.

One patient helped by this method was Philip Martinez. He had endured so much pain in his phantom arm after his motorcycle accident that he had even contemplated suicide. But, after four weeks of using the box for ten minutes each day — when it had felt like the phantom arm moved every time he looked at the box and saw the mirror image of his functioning arm — his phantom arm and associated pain completely disappeared.

### 9. Imaginative thinking can change the brain by improving performance and strengthening muscles. 

According to Alvaro Pascual-Leone from Harvard Medical School, we can change our brain simply by using our imagination. 

For example, we can use _visualization_ to improve our performance of an activity. In one experiment, Pascual-Leone assembled two groups of people who had never played piano before. One group sat in front of a piano, twice a day, five days a week, and imagined playing and hearing a piano sequence. The other group actually practiced piano for the same amount of time. 

People in both groups had their brains mapped before, during and after the experiment. At the end, both groups played through the piano sequence they had studied and a computer recorded the accuracy of their performance. 

Astonishingly, mental practice _alone_ had caused the same physical changes in the participants' motor systems as those who had actually practiced, and both groups showed comparable brain maps and approximately the same skills! 

How is that possible? Well, from a neuroscientific point of view, imagining an action and performing an action aren't really that different.

Brain scans show that numerous areas in the brain are activated through both imagination and action. For example, the primary visual cortex of the brain is activated when people visualize the letter A with their eyes closed, as well as when they look at the letter A. In this way, visualizing is a powerful tool for improving performance.

But it goes further than that: you can use imagination to actually strengthen the muscles in your body. In one study, doctors Guang Yue and Kelly Cole observed two groups, one that did physical exercise over four weeks — 15 finger contractions with a 20 second rest between them — and the other just imagined it, including a voice shouting "Harder! Harder! Harder!". At the end of the four weeks, the group that actually performed the physical exercise increased their muscular strength by 30 percent. However, the group that visualized the exercise also increased their strength by 22 percent!

### 10. Psychoanalysis, or talk therapy, is a neuroplastic therapy. 

Psychotherapy is sometimes scoffed at, but people often forget that Sigmund Freud, the father of psychoanalysis, was already using concepts based on plasticity in the nineteenth century. 

Though this insight came to be associated with Donald Hebb 60 years later, it was Freud who stated that when two neurons fire simultaneously, this makes them continue to work or "wire" together. In psychotherapy, this was already demonstrated through _free-association_ — when a patient says everything that comes to mind after being prompted by a word or cue. Free-association uncovered many interesting connections in the patient's mind.

Psychotherapy also argued that events that occurred in early childhood — a critical developmental period — affect our ability to love and relate to people in adulthood. Plastic change after this critical period was considered harder to achieve.

Furthermore, psychotherapy viewed memory as plastic, meaning memory could be changed and rewritten by subsequent events. Some people who had been molested as children, for instance, weren't disturbed by it at the time, but began to recall it negatively as they matured sexually.

Underlying neuronal networks and associated memories could also be altered with the help of psychotherapy, with the experience of past traumas changing as the patient gained a healthier understanding of what happened. 

For instance, Mr. L. was depressed for 40 years and couldn't get close to a woman because he felt he was betraying his mother, who had died when he was very young. But once he accepted his mother's death, he was able to build a close relationship with another woman. 

Once Mr. L. understood the cause of his habits and his view of himself and the world, he could take advantage of his plasticity, despite his age.

### 11. Neuronal stem cells can help us preserve our brains in old age. 

For years it was thought that the brain couldn't regenerate itself the way other organs, like skin, can. We thought that, as we got older, millions upon millions of our irreplaceable neurons simply died out. That is, until we discovered neuronal stem cells: brain cells that don't age. 

Stem cells are cells that haven't yet divided or differentiated into neurons or glial cells, which support neurons in the brain. Stem cells look identical to one another and what is amazing about them is that they can continually replicate copies of themselves without any signs of aging. This process, known as _neurogenesis_, carries on until we die. 

So far, we have found neuronal stem cells active in various brain regions, including the hippocampus, which controls memory, and the olfactory bulb, a region involved in processing smell. They have also been found dormant and inactive in the septum, which processes emotion, the striatum, which processes movement, and in the spinal cord. 

But the use of neuronal stem cells is just one way in which the brain can regenerate itself.

We can also _increase_ the amount of neurons and _extend_ their life in the hippocampus: 

To increase the number of our neurons, we can expose ourselves to new environments where we learn new things rather than simply repeating skills we're already proficient at. Furthermore, we can extend the life of our neurons by performing physical exercise, which not only creates new neurons but supplies the brain with oxygen.

An added benefit of engaging in physically and mentally stimulating activities is that it makes us less likely to develop Alzheimer's disease or dementia later in life. But bear in mind that not all activities are equal. The activities most helpful to us and more likely to stave off dementia are those that involve concentration, such as learning a musical instrument, reading and dancing.

### 12. Mirror region takeover is another kind of brain plasticity. 

We've seen that the brain is remarkably plastic and has a number of ways of changing. But there is another type of plasticity that we haven't talked about yet: _mirror region takeover._

Mirror region takeover is when one part of our brain hemisphere fails in some way and the region in the opposite hemisphere — its "mirror" region — attempts to take control of its function.

One example of this can be seen in Michelle Mack, who has only a right brain hemisphere and empty space where the left hemisphere would normally be. Mack's case is not just an excellent example of mirror region takeover, but the most profound illustration of neuroplasticity in general. 

Mack generally functions as a "normal" person, though she does experience some problems with abstraction, such as understanding the meaning of sayings like "Don't cry over spilt milk." However, she excels in skills like memorizing dates. She knows, for example, the day of the week for every date as far back as 1984.

This goes against the idea that the right and left hemispheres only perform certain tasks, because without the left, Mack's right hemisphere was forced to do the things her left would have ordinarily been responsible for, like language processing.

Interestingly, though, the migration of mental functions to the opposite brain hemisphere, as seen in Mack's case, can occur early on in our development. In fact, when we are very young, our hemispheres are similar, with specialization coming later on. For instance, brain scans of babies in their first year show that they process new sounds in both hemispheres. It's only around age two when the left hemisphere begins to specialize in speech and sound starts being processed here.

### 13. Final summary 

The key message in this book:

**Our brains are remarkably changeable. We can grow new neurons and extend the life of existing ones through changing our thought processes and adapting our behavior. Understanding the plasticity of our brain means that we have the ability to overcome psychological disorders and recover from physical injury.**

Actionable advice:

**Don't get old, get active.**

There's a risk of getting Alzheimer's or dementia as we age, even if we can live longer than ever before. Most of us know that the key to warding off degenerative disorders is to stay active, but doing so actually generates and maintains neurons in your brain, which stops it from deteriorating. To keep your mental and physical faculties in check, choose something challenging like picking up a new language, joining a Tai Chi class, or learning a musical instrument. Benjamin Franklin provides some inspiration in this camp: he invented bifocal spectacles at the age of 78!

**Suggested further reading:** ** _Incognito_** **by David Eagleman**

Unbeknownst to you, a subconscious part of your brain is constantly whirring away and wielding a tremendous influence on your thoughts, feelings and behavior. These blinks are your guide to the other side of your brain, and how it shapes your life.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Norman Doidge

Norman Doidge is a psychiatrist, psychoanalyst and researcher at the Columbia University Center for Psychoanalytic Training and Research and the University of Toronto's Department of Psychiatry. His work has been published in many popular media outlets including _The Wall Street Journal_, _TIME_ and _The Guardian_. Doidge has appeared frequently on TV and radio and is a _New York Times_ bestselling author.

