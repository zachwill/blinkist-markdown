---
id: 57bb6372003a4b0003f1a8ce
slug: the-brains-way-of-healing-en
published_date: 2016-08-24T00:00:00.000+00:00
author: Norman Doidge
title: The Brain's Way of Healing
subtitle: Stories of Remarkable Recoveries and Discoveries
main_color: 388A3F
text_color: 2F7334
---

# The Brain's Way of Healing

_Stories of Remarkable Recoveries and Discoveries_

**Norman Doidge**

_The Brain's Way of Healing_ (2015) highlights the human brain's amazing ability to change its structure and develop new ways of coping with disorders. The brain, whether by being "rewired" to process information in new ways or by being "trained" through repetitive exercises, can overcome debilitating diseases and heal itself.

---
### 1. What’s in it for me? Read stories of miraculous cures to neurological disorders. 

The progress that's been made in the medical sciences over the past few decades has been incredible. Modern medicine allows us to cure a variety of diseases that have plagued humanity for thousands of years. Flu infections are now easily prevented and victims of extreme accidents can be given new limbs or organs — an arm, a leg, even a heart.

Still, a number of ailments continue to baffle modern medicine. Take chronic pain, Parkinson's disease, multiple sclerosis or loss of vision — doctors regularly throw up their hands when faced with such ailments, since there are no established methods of treatment. However, this doesn't mean that such diseases cannot be treated.

In these blinks, you'll learn about alternative ways of treating ailments that have confounded conventional medicine. You'll hear personal stories of miraculous cures, and how we can use simple techniques to stimulate the brain and encourage the healing process.

You'll also learn

  * why less is more when it comes to physical exercise and healing the body;

  * why our tongue may be the key to rewiring our brain; and

  * how Broadway singer Ron Husmann regained his voice after years of voicelessness.

### 2. Chronic pain is a neurological disease that can be reversed with visualization exercises. 

If you suffer from chronic pain, or know someone who does, then you're fully aware of how debilitating and frustrating it is. Often, the source of chronic pain is unclear, which makes it difficult to treat, sometimes even leading to a patient's concerns being dismissed by doctors.

So, both patients and doctors are faced with the question: Where does chronic pain come from, and how can it be treated?

Essentially, chronic pain is a neurological disease that causes the body to send unnecessary pain signals to the brain.

To see how this can happen, let's look at the case of psychiatrist Michael Moskowitz, who, in 1994, injured his neck in a water-skiing accident. For 13 years after this, he suffered from chronic pain.

When you sustain an injury, a signal is sent from the nervous system to the brain; this signal alerts you to the fact that some tissue is damaged and in need of attention. We perceive this signal as pain. However, in some cases, like Dr. Moskowitz's, the nerve cells themselves get damaged, which causes them to continue sending pain signals even after the body has healed.

This is how chronic pain can last 13 years. But the question still remains: Can it be successfully treated?

As it turns out, chronic pain can actually be reversed through visualization techniques.

In trying to treat his own pain, Dr. Moskowitz found books and articles on neuroscience which revealed that the areas of the brain that process pain can also be stimulated by visual information. Equipped with this knowledge, he began working on a technique to replace pain through visualization.

It worked like this: Whenever he felt a spasm of pain, Moskowitz would visualize a map of his brain and focus on the areas where the number of neurons processing pain increased. He would then visualize these pain neurons being transformed back into regular neurons.

And, sure enough, it worked! After sticking to this exercise for three weeks, Moskowitz began experiencing some relief. After a year, he was practically pain-free and ready to begin sharing his discovery with other patients.

> _"I wondered, Is it a placebo? But the pain still hasn't come back." - Jan Sandin, patient of Michael Moskowitz_

### 3. Parkinson’s disease can be defeated through moderate walking and conscious movement. 

Our brain's ability to overcome ailments doesn't stop at chronic pain.

For another impressive achievement, let's look at South African accountant John Pepper. After being diagnosed with Parkinson's disease, John believed he could fight the disease by increasing his exercise regimen. Yet, despite his best efforts, every week he had to lower the weight he lifted and reduce the time he spent on the treadmill.

But John wasn't wrong to think that exercise could help, and he eventually realized that moderate walking was the way to help him fight Parkinson's.

When John found out about an exercise program called Walk for Life, he initially thought it wasn't strenuous enough, but, thankfully, John's wife, Shirley, convinced him to take part.

The program allowed John to moderately build up his strength over the course of several months, to the point where he was walking eight kilometers per session at an average speed of 7 minutes per kilometer.

John also found that _conscious_ movement is an especially good technique for Parkinson's, as it enables the brain to fight degeneration.

During his walks, John began to consciously observe the way he moved. With this focused attention, he taught himself how to gradually correct his limping gait by straightening his arm posture, coordinating his arm swings and putting more weight on his left foot.

It was through this conscious attention to movement that John was able to alleviate the symptoms of his Parkinson's disease.

Parkinson's primarily targets the area of the brain known as the _basal ganglia_, which is responsible for the automatic functions we perform without thinking, like walking, eating and brushing our teeth. So, by consciously thinking about them, John began moving control of these movements from the basal ganglia to areas of the brain that are associated with conscious actions, such as the prefrontal cortex.

This way, it didn't matter that Parkinson's was causing his basal ganglia to malfunction, as he could continue to function properly by using and developing his prefrontal cortex.

> _"The central problem in all Parkinsonian disorders is passivity." - Oliver Sacks_

### 4. The Feldenkrais technique can cure stroke ailments through its slow, conscious movement exercises. 

There are other exercises that can help rewire our brains, and one of them comes from a rather unexpected source.

In June, 1940, a physicist named Moshé Feldenkrais fled Nazi-occupied Paris and resettled in England. On this journey, Feldenkrais carried secret papers and plans for building bombs, which, doubtless, was a nerve-wracking experience — and perhaps that's why he dedicated the rest of his life to a method based on soothing movements.

He developed a method known as the _Feldenkrais technique_, and it's based on slow and conscious repetition of movement patterns.

For example, a simple exercise is to lie flat on your back and make micro-movements with your head, using the least amount of effort possible, while focusing your attention on the left side of you body.

After this exercise, people find that the left side of the body becomes more relaxed and aligned than the right side. It shows that, by paying conscious attention to certain parts of the body, connections between the brain and the nerves of these body parts are stimulated, allowing our tense muscles to relax.

The Feldenkrais technique has proved so successful that even stroke patients use it to recover their abilities.

In Switzerland during the 1970s, Feldenkrais gave his full attention to a patient named Nora, who suffered a stroke that damaged the left hemisphere of her brain, making it impossible for her to differentiate between right and left.

Feldenkrais helped Nora's brain relearn left from right by having her lie on her back and perform some of his gentle movement repetitions. Then he gently touched different parts of the right side of her body, saying, "this is the right ear, this is the right hand," and so on.

This method was repeated in different positions and orientations, and, eventually, Nora's brain began to heal itself and once again recognize left from right.

> _"Errors are essential, and there is no right way to move, only better ways." - Moshé Feldenkrais_

### 5. Simple eye exercises can improve eyesight, but, first, the eyes need to relax. 

We've heard some pretty astounding stories about how the brain can recover from serious setbacks. But what about other types of ailments, like blindness?

This is the question that IT consultant David Webber was faced with after losing his eyesight and his job. Luckily for David, he found that there are indeed eye exercises that can remedy vision failure when practiced diligently.

In his search for a cure, David came across the story of Meir Schneider, an Israeli who was born blind and recovered his sight after five failed surgeries. How is this possible?

Schneider used eye exercises that were developed by William Bates, a controversial nineteenth-century ophthalmologist whose methods continue to be shunned by most of the medical community.

Bates's exercises are relatively simple. For several minutes, patients lie on their backs with their palms over their eyes, giving the eyes an opportunity to rest by blocking out all light. This is followed by exercises to strengthen the eyes, such as focusing on different objects placed at varying distances.

By engaging in Bates's exercises for up to 13 hours each day, Schneider eventually attained 70 percent of his full vision.

But, for some eye conditions, such as David Webber's, the eyes need to relax before training exercises can begin.

At first, David's eyes were too irritated to be able to perform any of Bates's exercises. So he began his path to recovery by using eye relaxation exercises developed my Moshé Feldenkrais.

He would lie down, close his eyes and perform gentle head and eye movements that helped him focus and let go of the tension. This also relaxed his eyes.

After this, he was able to move forward with some of Bates's exercises. And seven years later, in July of 2009, David managed to restore 50 percent of the normal vision in his left eye, and his right eye improved from complete blindness to blurry vision.

> _"The eye standeth not still, but moveth incessantly." — Andreas Laurentius, medieval physician_

### 6. Light therapy and laser acupuncture are powerful treatment methods. 

If you've ever heard someone talk about "the healing powers of the sun's rays," chances are they weren't talking about its power to heal brain injuries. But new discoveries are suggesting that this is exactly what targeted exposure to light can do.

Just take the case of a Canadian professor who sustained a serious brain injury in a car crash.

This injury resulted in a speech impediment and the loss of two different foreign languages, as well as the ability to focus on tasks. All this led to her losing her job and attempting suicide.

Fortunately, she survived and contacted Anita Saltmarche, a brain-injury specialist in Ontario who specializes in laser therapy. Upon examination, Saltmarche identified eight areas of the professor's brain that could be helped with low-impact laser treatment.

And the effects were felt immediately. After the first session, the professor was able to sleep for periods of 18 hours, the first deep sleep she'd had since the accident.

As the treatment progressed, she regained her cognitive skills and, with them, her job. However, to stay healthy, she had to continue with regular laser treatment.

The healing power of light is also gaining traction in the world of acupuncture.

The practice of acupuncture is age-old, as are the charts that correspond external points on the body to internal organs. But, in recent years, it has also been found that these points are just as receptive to the light energy of lasers as they are to traditional needles.

Dr. Margaret Naser, a neurology professor from the Boston University School of Medicine, traveled to China to learn more about this new technique and brought it back to the United States for further study.

Through her research, Dr. Naser discovered that patients suffering from stroke paralysis made significant progress in recovering their motor functions with the help of laser acupuncture. For the therapy to be effective, however, at least 50 percent of the neurons in the patient's brain had to be working correctly.

> _"It is the result of my experience with the sick, that second only to their need of fresh air, is their need of light." - Florence Nightingale_

### 7. A high-tech tongue stimulator cured a singer’s multiple sclerosis and could be used for many other brain diseases too. 

Lasers aren't the only technological advancement that can help the brain heal.

Ron Husmann, a celebrated Broadway singer during the 1970s and 1980s, with a voice that didn't need a microphone to enthrall audiences, is a case in point. His voice started to fail him in his 40s, and he discovered he had multiple sclerosis (MS), a disease where the immune system attacks the central nervous system.

Remarkably, he found a way to sing again, and to reverse the neurological damage caused by MS, with the help of tongue stimulation.

Disappointed with the results of conventional treatment, Ron followed the advice of a fellow singer and made an appointment at a Wisconsin University research lab.

Once there, a team of neuroscientists gave him a small gadget to place on his tongue. The device was equipped with 144 electrodes which provided low-frequency stimulation for the many nerve sensors on the tongue.

This treatment turned out to be remarkably effective. After just 20 minutes, Ron could hum a tune again, and after a full week of treatment, he was back to belting out Broadway tunes.

By stimulating the tongue, this device actually activates the entire brain, which is what makes it so effective. You can think of the tongue as a direct pathway to the brain: there are 48 different types of sensory receptors on the tongue alone, the highest concentration on the body, and they all connect up to different parts of the brain.

So, when this device stimulates the tongue, it activates the entire brain, including the areas responsible for movement, thoughts, feelings and sensation. And that's why it could prove useful in curing a variety of brain diseases.

For instance, when neurologists are unsure which part of the brain has been damaged by an accident, they could use the tongue stimulator, which, by activating every area of the brain, is also sure to treat the affected part.

### 8. Dyslexia can be treated with filtered music that rewires the brain. 

As the old saying goes, "Music soothes the soul." But, nowadays, we know from research that music can do a lot more than that. In fact, it can help heal the brain.

Music is proving especially helpful in healing dyslexia, a learning disorder that can affect a person's ability to read and write.

One such case was Simon. In 2008, Simon's mother became concerned, because the boy still wasn't responding to any verbal or physical cues, even though he was already three years old. In desperation, she contacted child therapist Paul Madaule, a music-therapy specialist. Madaule confirmed that Simon showed symptoms related to dyslexia, but he was confident he could treat them.

Madaule played special music for Simon that had his mother's voice mixed in at a higher frequency. And the results were indisputable: Simon was soon listening and communicating with others, and in just five years time he was one of the most socially well-adjusted kids in his class.

The secret to music's healing powers is it's ability to use both high and low frequencies to rewire the brain.

The science behind this goes back to the 1940s, when Dr. Alfred Tomatis invented the method known as the _Electronic Ear_. This is a device that conveys two audio channels to the listener, one set to a low frequency that imitates the sounds heard by someone with a hearing disorder, and the other set to a high frequency that corresponds to what an average human hears.

The Electronic Ear works by jumping between these two frequencies at unpredictable times. Each time it switches to the higher frequency, it activates and stimulates the communication centers of the patient's brain. During the lower frequency periods, the patient's brain gets a rest.

By using this clever system, the ears and brains of autistic and dyslexic children can be trained to pick up on normal communication signals and learn to respond more naturally.

As we've seen, by activating the brain and nervous system, doctors and researchers have discovered the potential to unlock a number of disorders that many have considered incurable. Here's hoping the discoveries never stop!

> _"And therefore, musical training is a more potent instrument than any other." - Plato, The Republic_

### 9. Final summary 

The key message in this book:

**The brain is not an unchangeable object. It's organic, and it can grow and expand, just as it can wither or disintegrate. The good news is, this means that brain diseases are not irreversible. To cure these disorders, it just takes patience and open-mindedness to find the right method and get those neurons back into gear.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Brain that Changes Itself_** **by Norman Doidge**

How can stroke victims who become paralyzed start using a fork or buttoning their shirts again? Well, contrary to what was believed for so long, the brain is not hardwired. It can change, regenerate and grow. Drawing on real-life cases of scientists, doctors and patients, _The Brain that Changes Itself_ (2007) shows us how, rather than relying on surgery and medicine, we can alter our brains through thought and behavior.
---

### Norman Doidge

Norman Doidge is a psychiatrist specializing in neuroscience and the use of alternative methods for stimulating the brain. He is a graduate of the University of Toronto, a former resident of Columbia University's Department of Psychiatry and author of the New York Times best-selling book _The Brain That Changes Itself_.

