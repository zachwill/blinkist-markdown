---
id: 54733c223635610009200000
slug: the-brand-gap-en
published_date: 2014-11-25T00:00:00.000+00:00
author: Marty Neumeier
title: The Brand Gap
subtitle: How to Bridge the Distance Between Business Strategy and Design
main_color: F3B431
text_color: 805E1A
---

# The Brand Gap

_How to Bridge the Distance Between Business Strategy and Design_

**Marty Neumeier**

In _The Brand Gap,_ you'll get the inside scoop on how a strong brand can give your company a competitive edge. When you learn how to implement the five branding disciplines outlined in this book, you'll understand that in closing the gap between strategy and creativity, you'll be able to build an irresistible brand that will make customers take notice.

---
### 1. What’s in it for me? Learn to close the gap between creativity and strategy to make your brand irresistible. 

Understand this: a brand is not a logo, a "corporate identity system" or a product.

A brand is a gut feeling. And you also need to understand that you can't perfect a brand or control it.

What you _can_ do, however, is ensure your company does business in a way that customers understand and appreciate. When you do this, you'll find that your customers will develop a gut feeling about your business — and that feeling is your brand.

These blinks will explain the five branding disciplines you need to follow to instill in your customers that positive gut feeling about your company that you want them to have.

In the following blinks, you'll also learn:

  * why John Deere doesn't offer real estate;

  * why ducks should swim like ducks; and

  * why _Coca-Cola's_ brand represents more than half its actual value.

### 2. Become a charismatic brand by closing the “brand gap” between strategy and creativity. 

Believe it or not, the Coca-Cola brand is worth $70 billion — a figure that represents 60 percent of the company's total value!

And Coca-Cola isn't alone. Today, the measure of any company's value lies in the strength of its brand.

So how do you go about building a strong brand? Start by closing the _brand gap_, or the gap between strategy and creativity.

Here's how the brand gap works in many companies. Left-brained strategy people work together, perhaps in the marketing department. These employees tend to be analytical, verbal and logical.

Meanwhile, the design department is exclusively populated by right-brained creatives. These employees are more intuitive, with strong spatial and visual skills.

And whenever tension happens between these two polarized groups, you end up with a brand gap.

Perhaps you've experienced this in your own workplace. Have you ever developed a highly sophisticated strategy that, upon execution, just doesn't connect with customers? That's likely due to the brand gap.

Ultimately, this phenomenon poses a major problem, because without a unified brand, your company simply won't be able to compete in the marketplace.

Look at it this way: Companies with a brand gap can't communicate who they are, so they don't build a strong relationship to customers. And since customers don't know what to expect, they won't consistently buy the company's products.

But in the case of _charismatic brands_, there's little so-called _psychic distance_ between customers and companies. In other words, customers have a solid relationship with a company and know what to expect from its products.

For example, companies like _Coca-Cola_, _Apple_ and _Nike_ have achieved this by establishing brands that communicate ideas of joy, beauty and style — all things people want for themselves. The goal of any brand is to develop this kind of aspirational, appealing identity and to communicate it consistently.

Each and every charismatic brand today has mastered the five disciplines of branding, which you'll learn about in the upcoming blinks.

> _"A brand is a person's gut feeling about a product, service or company."_

### 3. Successful brands know what they do and why it matters, and importantly, they stick to it! 

Humans are hardwired to pay attention to the unexpected. For our ancestors, this was a survival strategy, a way of scoping out potential threats.

Today your brand can use this impulse to attain a competitive advantage. That's why _differentiation –_ being unusual and different — is the first discipline of any successful brand.

Differentiation can be summed up by three questions. If you can't answer these in a way that's appealing to customers, then you don't have a brand:

  1. _Who are you?_ This question's easy for most companies. You're _John_ _Deere_! 

  2. _What do you do?_ We make tractors and related equipment.

  3. _Why does it matter?_ Hmm. Here's where it gets tricky.

If you answer the third question by saying, "We make important products," you've got bubkes — nothing. So what? _Lots_ of companies make important products. And if you say you sell the "best selection" of important products, well — bubkes again. _Everyone_ says that.   

To find a convincing answer, let's turn to John Deere. The company would argue that they matter because generations of farmers trust their equipment.

Overall, John Deere's responses to these three questions are simple and connected.

This leads to a key aspect of differentiation, which is: stay focused. A focused brand knows what it is, why it's different and why it's appealing. If John Deere started selling houses, it would be just another real estate company and it would lose its differentiation.

Some executives are uncomfortable with being focused, because it implies narrowing, as if you're cutting yourself off from potential opportunities. But actually, being focused is the only way to be truly competitive.

Take _Volvo_ : The company's brand differentiation was that their heavy, boxy cars were the safest ones on the market. That's what they were known for. And yet they still decided to expand their product line and develop sexier, more unsafe models, and their brand suffered for it.

Why give up your competitive niche when you've worked so hard to establish it? If your brand isn't somehow differentiated, you have nothing. So stay focused!

> _"Differentiate or die." — Jack Trout_

### 4. Effective collaboration is crucial to building a charismatic brand. 

A brand can't develop in isolation. It requires many different people with all sorts of skills and specialties to come together and build it.

And that's why _collaboration_ is the second discipline of branding.

There are three key ways you can build brand collaboration in your company _._

First, you can _outsource_ branding to another company. These "one-stop shops" handle everything, from events and public relations to product design and packaging. Doing this is convenient, but there are drawbacks: Not only is it unlikely that one company has strong expertise in every area you want to pursue, but also it's risky to fully cede control of your brand to an outsider.

Another way to build brand collaboration is to _work with an agency_. A branding agency will assemble a team of experts and sub-contractors suited to handle your brand's various needs. Although the agency can ensure a uniform message across outlets, you're still taking a risk by outsourcing.

The third way is to create an _integrated marketing team_, where you hire a team of experts to work inside your company, focusing on advertising, identity, product design, strategy and so on. From marketing to design, everyone comes together as a "superteam."

Although a superteam demands a lot of resources and investment, following this strategy allows you to hold onto all the accruing brand knowledge within your own company.

Still, it's worth noting that collaboration models may be changing in the future with the rise of _network organizations_. This is when separate companies and organizations come together to deliver a particular product or service to customers.

The best example of this is in Hollywood, where directors, actors and other specialists come together to make a movie, then when the project is done, split up and move on to different projects.

> _"It takes a village to build a brand."_

### 5. To ensure that your brand stands out, look for innovative ways to express your novel ideas. 

It would be nice if you could woo your customers with a logical argument, explaining why yours is the superior product. But unfortunately, that's not an effective approach.

To really win over consumers, you have to inspire them.

And that's where the third discipline comes in: to become a leader in your market, you can't just follow what others are doing. You have to _innovate_.

If everybody else is zigging, you have to zag — because otherwise, why should consumers notice your product?

If you're committed to developing an innovative idea, you do have to look for new ways to express ideas. However, you don't actually have to reinvent the wheel.

As Industrial designer Raymond Loewy phrased it, you're looking for MAYA — that is, the "Most Advanced Yet Acceptable Solution."

The Beatles are a fantastic example of this approach. The British pop group innovated consistently across all of their records, over a long period of time. They started out in the early 1960s with upbeat songs that conformed to mainstream tastes. But over time, they introduced unusual sounds and experimented with Indian instruments.

Opportunities for this kind of gradual innovation are everywhere. It might be a matter of changing your logo. Or maybe you could redesign your website or your packaging. You could even change your name! The point is, you should take advantage of every outlet to innovate your brand.

Of course, innovation is risky territory. As Americans like to say, "Don't rock the boat." Japanese businesspeople have a similar expression: "The nail that sticks up gets hammered down."

Generally speaking, people are afraid of innovation. So when your product triggers fear in others, that's how you know you've hit on something innovative and are on-track to grabbing attention.

_Volkswagen_ straddled this line between risk and innovation with its humorous, attention-grabbing VW Bug automobile.

> _"A combination of good strategy and poor execution is like a Ferrari with flat tires."_

### 6. Use validation tests to figure out whether your branding elements and messages work. 

In the old days, brands mostly engaged in one-way communication with consumers. But today, brand-customer communication is more of a feedback loop. When your company sends out a message, you get a response!

And this more interactive mode of communication is exactly why the fourth brand discipline, _validation,_ is so critical.

You need to make sure your brand resonates in the real world, not just in your company's brainstorming sessions. So make sure you validate your brand constantly.

There are a number of ways you can do this. One way is called the _concept test_. It involves presenting prototypes of a branding element, like your name, to at least ten people outside your company. It helps to do this individually (not in a group setting) and to have a smaller number of prototypes.

The goal is to understand whether more people prefer one prototype over another. So ask probing questions like, "What kind of company do you associate with this name?" Or, "Which of these names seems more valuable to you?" And always follow up by asking, "Why?"

_Swap tests_ are another way to validate elements of your brand. Swap one aspect of your branding, like the logo or the name, with that of a competing brand. If the result is better than or equal to your icon, you'll know that your brand needs work.

Whenever you're doing a validation test, there are five key things you're looking for:

  1. _Distinctiveness_ : You want to stand out from competitors.

  2. _Relevance_ : Make sure your brand's products and messages are appropriate for its identity. For example, it would be weird if Nike started making laundry soap.

  3. _Memorability_ : Is your brand sticky? One way to ensure that people will remember your brand is to connect it with an emotion.

  4. _Expandability_ : Would you be able to expand your brand to other message types and products?

  5. _Depth_ : Can you communicate about your brand with different kinds of customers, across various channels, using different kinds of styles and emotions?

### 7. By cultivating your brand, you can ensure it perpetuates itself without becoming stale or boring. 

How do brands perpetuate themselves? Once you have a handle on the first four brand disciplines, you have to _cultivate_ your brand and keep it alive.

Even for the world's most famous companies, it isn't enough to have an iconic brand — you have to have a _living brand_.

As individuals, we're changing all the time. Consider that most of us don't even wear the same shirt two days in a row. So why can't brands also adapt and change when appropriate?

In fact, brands that are always the same are viewed with suspicion. So let your brand live! Make mistakes and be human. It's okay if your brand has inconsistent elements, as long as it holds true to its defining characteristics.

Another way to ensure that you have a living brand is to see it as a collaborative performance, one that requires input and cooperation from executives, employees and everyone in between.

The ultimate goal is for your brand's external actions to align with its internal culture. This will make your brand much more authentic, which customers will intuitively sense and respond to.

And although it's important to be adaptable, you should also take steps to protect your brand's core. Make it a priority for all employees, not just the marketing department, to maintain the brand.

One way to instill a sense of brand identity is to organize brand-related workshops and seminars. This will ensure that everything employees do is guided by one question: Is this good or bad for the brand?

Another way to protect your brand is by hiring a Chief Brand Officer. These super-experienced professionals are able to close the gap between strategy and creativity by managing the five disciplines of branding: differentiation, innovation, collaboration, validation and cultivation.

### 8. Final summary 

The key message in this book:

**The only way you can create a charismatic brand is by creating a cohesive, collaborative working environment that unites strategic and creative components. You can do this by managing the five disciplines of branding: differentiation, innovation, collaboration, validation and cultivation. Once you do, you'll have no trouble reaching customers and dominating your market niche.**

Actionable advice:

**Avoid "featuritis," or adding too many design features to your website.**

When it comes to web design, bells and whistles like animation, widgets and graphics are easy to develop and implement. The real skill lies in knowing which ones are actually relevant to your brand, and which are just clutter. So when it comes to web design, focus on subtraction, not addition.

**Suggested further reading:** ** _How Brands Grow_** **by Byron Sharp**

In _How Brands Grow_, Byron Sharp tackles conventional marketing wisdom, disproving many of the conventional marketing myths with scientific facts and establishing some scientifically proven principles marketers should use.
---

### Marty Neumeier

Marty Neumeier is a branding expert who has worked with _Apple_, _Netscape_, _Eastman_ _Kodak_ and other iconic companies.

