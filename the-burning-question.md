---
id: 53aa7cb43263390007000000
slug: the-burning-question-en
published_date: 2014-06-24T00:00:00.000+00:00
author: Mike Berners-Lee and Duncan Clark
title: The Burning Question
subtitle: We can't burn half the world's oil, coal and gas. So how do we quit?
main_color: 29A2CD
text_color: 1F7999
---

# The Burning Question

_We can't burn half the world's oil, coal and gas. So how do we quit?_

**Mike Berners-Lee and Duncan Clark**

_The_ _Burning_ _Question_ deals with our generation's most pressing problem: climate change. The book discusses why it's important to make drastic changes in our politics, markets and society, and what we have to do to achieve a sustainable future for ourselves and our grandchildren. The authors not only explain how we've failed so far but also point towards the source of the problem.

---
### 1. What’s in it for me? Learn about the catastrophic consequences of climate change – and how you can help prevent it. 

Everyone has already heard about climate change, but not everyone _believes_ in it. Many people are still unconvinced by the evidence, which they consider imprecise and uncertain. Although no reputable scientist claims to know the exact impact of our emissions on the climate, they do consider — without exception — that climate change is a fact that will influence our lives and the lives of all future generations.

This book confronts readers with the latest scientific facts of climate change that will alter your opinion on the issue forever. The authors explain the social, economic and psychological factors that keep us from tackling the problem head on and expose those who don't want us to see the problem in front of our very eyes.

In these blinks, you'll learn about the disastrous consequences that an increase of a mere 2 degrees may have — and why it seems very likely that we'll exceed this number.

You'll also discover why most governmental pledges and local carbon footprint reductions have proven ineffective so far — and why a global approach is needed to tackle the problem head on.

Finally, you'll find out what we, as a society and as individuals, have to do to create a sustainable future for everyone.

> _"Fossil fuel is so last century."_

### 2. Climatic change is not a scientific theory anymore – it’s a fact. 

To this day, many people still believe that global warming is a theory that has yet to be proven. Fact is, the climate of our planet is changing. And it's happening a lot faster than we anticipated.

In the last couple of years, the whole world has reported a rapid change in weather.

Moreover, in June 2012, Saudi Arabia reported that, despite a temperature of 43 degrees Celsius (109 degrees Fahrenheit), it had actually _rained_ in Mecca, making it the hottest rainfall in the history of the planet! And the list goes on: at the end of that year, England had announced that it'd had the wettest year on record, and Australia suffered from such a severe heat wave that it had to add two new colors to its temperature maps.

These reports aren't coming from scattered sources: every single reputable scientific institution in the world agrees that our planet is getting warmer, and that greenhouse gases emitted by human activity are almost certainly the main cause.

In fact, we've known since the nineteenth century that additional greenhouse gases in the air, such as carbon dioxide and methane, will warm the planet. And over the last century, we've been releasing those gases into the atmosphere, where they've been continuously accumulating. One study performed at Lancaster University even showed that our man-made carbon dioxide emissions are _exponentially_ rising.

### 3. Even a small temperature increase could lead to disastrous global effects. 

In 2009, world leaders finally agreed to try limiting global warming to a maximum of 2 degrees Celsius (3.6 degrees Fahrenheit) — a figure considered reasonably safe at the time. But recent events have shown that it simply isn't enough.

Why not?

Because even the current average 0.8-degree Celsius increase in temperature from the preindustrial level has caused far more damage than most scientists initially predicted: one-third of the summer sea ice is gone from the Arctic Ocean, the oceans worldwide contain 30 percent more acid than at the preindustrial level and, since hot air holds more water vapor than cold, the atmosphere over the oceans is 5 percent more humid, which raises the chances of catastrophic floods.

According to Kerry Emanuel of MIT, a leading authority on hurricanes, "Any number much above one degree involves a gamble."

Another major danger is that _tipping_ _points_ in our eco-system could trigger climatic processes we can't control. A "tipping point" in climate change is the point where a _positive_ _feedback_ _loop_ kicks in, which could flood the air with an uncontrollable stream of greenhouse gases.

One such feedback loop is the emission of greenhouse gases stored in the Arctic soils: when soils melt, they emit gas, which leads to higher temperatures and, in turn, to more melted soil — and the cycle starts again. The melting of sea ice is another: this decreases the reflectivity of the earth's surface, which causes more warmth from the sunlight to be absorbed and more ice to melt.

We know these tipping points exist, and as more and more scientists state their worries about the 2-degree-Celsius limit, fears of hitting a tipping point are growing in the scientific community. Veteran climatologist Jim Hansen of NASA has even described the idea of letting the world get 2 degrees Celsius warmer as a "prescription for disaster."

> _"If we're seeing what we're seeing today at 0.8 degrees Celsius, two degrees is simply too much."_   

(Thomas Lovejoy)

### 4. Judging from today’s developments, the 2-degree target is too tough to achieve. 

We've all heard that the natural reserves of fossil fuels are running low, but actually, there's too _much_ oil rather than too little — at least when we're talking about climate change.

Why?

Because even though we know that the 2-degree increase is very risky for our planet's climate, recent developments raise doubts that we'll even be able to keep the temperature below that — in no small part because of our constant consumption of fossil fuels.

If we want to stay beneath 2 degrees of global warming, we have to stay within the _all-time_ _carbon_ _dioxide_ _budget_ — the amount of carbon dioxide we can pump into the atmosphere over time.

Why _all-time_?

Because carbon dioxide stays in the atmosphere for so long that the eventual temperature rise of the planet will not depend on when we emit carbon gases, but how much we emit in total over the years. A landmark study from 2009 calculated that our "all-time carbon budget," which would give us a 50 percent chance of staying within the 2-degree target, is around 3,700 billion tons of carbon dioxide — more than half of which we've already used!

And then there's the oil problem: we've already extracted a whole lot more oil than what can be safely used — and there's still much more in the ground. In fact, all the major energy companies have ready-to-use fossil fuels equalling 3,000 billion tons of carbon dioxide — nearly twice our remaining carbon budget — and the reserves in the ground dwarf this number.

According to JPMorgan, staying within the budget would mean not using the equivalent of $27 trillion in fuel reserves that have already been extracted — which businessmen are going to find hard to resist.

> _"Our problem is one of abundance: Just the fossil fuels we've already extracted are way too much for us to safely burn."_

### 5. Government pledges, greener behavior and energy efficiency alone can’t stop the imminent climate catastrophe. 

Most people believe that climate conferences and the new wave of green technologies will lead the way to a sustainable future.

Unfortunately, this may just be wishful thinking.

First off, the world's governments are far from meeting their own targets. Those that attended the 2009 United Nations Climate Change Conference in Copenhagen voluntarily made pledges in order to cap the increase in temperature to 2 degrees. But since most of these countries haven't implemented their promised changes, and the carbon curve has been growing exponentially since then, even this scenario has become unrealistic, as scientists are predicting a rise of 3.3 degrees.

And what about green technology? Can't it solve the climate problem?

Well, although green energy's higher energy efficiency and carbon savings do play an important role in stopping climate change, its short-term improvements and efficiency gains have proven that they "bounce back."

Such _rebound_ _effects_ happen when the positive effects of a new technology are canceled out by its negative effects. In this case, green technology has had practically no effect on the carbon dioxide emission so far — its original goal. Let's look at two rebound principles to clarify.

The first rebound principle: When something is produced more cheaply, we tend to use it more. For example, since the invention of the light bulb — and recently LEDs — the production of light has become much more efficient. But this has made lighting cheaper and easier to install, which has led to the creation of more lighting and even more energy usage.

The second rebound principle: If we actually manage to save energy and money thanks to a new technology, we start investing it in the production of new products, which has its own energy impact — and cancels out the original energy gains.

### 6. The complexity of the climate problem and lobbyist schemes prevent us from taking action. 

Have you ever asked yourself why most people don't undertake drastic actions when the threat of climate change is so imminent?

One reason is that, due to climate change's complexity, most people are struck by disbelief and confusion.

Climate change is, after all, built up by gases we can't see, smell or taste, and the effects are so complex that even scientists with supercomputers can't make definite predictions about its consequences. Plus, the fact that the most severe impacts are years away and can't be linked to concrete actions taking place right now makes the whole issue feel intangible and abstract — all of which fosters doubt.

Another reason why most people don't take action is because they're simply unaware of the problem.

Why?

Because certain corporation shareholders are protecting tens of trillions of dollars worth of fossil fuel reserves and infrastructure by intentionally misinforming the public.

For decades, many interest groups linked to the fossil fuel business have pumped money into lobby groups, PR agencies and think tanks with the goal of persuading politicians and citizens that climate change either doesn't matter, doesn't exist or will be inconceivably expensive to solve.

This misinformation has taken many different forms in TV advertising, textbooks and even "astroturfing", in which PR professionals leave "disinterested" comments attacking climate change on online articles and blogs. But this approach is not limited to the issue of climate change: as Naomi Oreskes and Erik Conway pointed out in their book _Merchants_ _of_ _Doubt_, lobbyists use this approach to undermine the links between industrial pollution and acid rain, passive smoking and lung cancer, and CFCS and the ozone layer.

> _"With hundreds of trillions of dollars' worth of fossil fuels and infrastructure, it is almost understandable that certain business individuals want us to look away."_

### 7. Saving the climate might mean a setback in economic growth – which might not be as bad as it sounds. 

As we've seen, government pledges and green technology alone are not enough to solve the problem of climate change. It looks like the only way forward is to implement global constraints on carbon emissions — even though this would massively devalue existing fossil fuel supplies and infrastructures.

And drastic measures must be taken. In a 2011 study, researchers found that if we want a fair chance of staying within the 2-degree target, we need to annually reduce emissions by 5 to 6 percent starting in 2016 — a massive reduction.

This would require moving away from dirty energies. Consequently, fossil fuels and the related infrastructure, such as heavy machinery, cars and refineries, would be devalued, which would cause a temporary — but hard-hitting — global recession.

Some believe this would be a catastrophe, but accepting the economic setback and shifting our attention from short-term economic growth to long-time criteria may actually improve our lives.

Why?

Because using economic growth as the central measure of human progress is deeply flawed.

For example, a huge body of research has shown that once average incomes are above a certain level, there is no correlated improvement in well-being or life satisfaction. And focusing on short-term growth always happens at the expense of the long term: short-term growth, which is counted in a country's GDP, depends on the natural capital of our planet. But if this growth damages our natural capital — as it currently does — soon we won't be able to make any kind of capital at all!

### 8. To tackle the climate issue head on, we have to minimize the oil industry’s influence on politics and public opinion. 

So what can be done to counter the political influence of the fossil fuel industry?

One way is to stop spending on fossil fuel reserves and infrastructure, and _divest_ — the opposite of invest — out of the oil industry.

This is a necessary step seeing as our society's massive investments in fossil fuels makes it harder to take the next steps towards a low-carbon future. Some campaigners, pension funds and cultural institutions have already started taking action by divesting and cancelling their sponsorships — which sent a strong message to political and business leaders. But with politics still struggling to take the necessary drastic steps, everyone needs to express discontent and spread the word to create a society-level shift.

Because without divestment, the carbon bubble will continue to grow, leading to an even larger economic collapse when the necessary carbon regulations are implemented. And if investors don't realize that the carbon bubble has to burst, they'll continue investing in fossil fuels — which will have catastrophic repercussions on citizens around the world in the long run.

### 9. To battle climate change, we have to simultaneously constrain global carbon emissions and invest in green technology. 

So what can be done considering that voluntary pledges to reduce carbon emissions have proven ineffective?

Answer: countries have to be forced to respect the planet.

To date, no environmental meeting between the global leaders has had any significant impact on the carbon emission curve. As a result, scientists have proclaimed that the only chance we have of keeping the temperature under the two-degree target would be a mandatory, global system regulating carbon emissions for every country.

But that alone won't be enough, either.

We also need to push certain key green technologies and massively improve our energy efficiency.

For example, carbon capture technology — which captures carbon from the air and stores it underground — is our best chance of reversing the effects of our industrial age despite the controversy about potential leaks.

Renewable energy technology also plays an important role in decreasing carbon emissions and replacing fossil energy. For example, the state-wide installation of photovoltaic panels in Germany is a prime example of a successful renewable energy project.

The bottom line is this: the only way we will come close to making the changes necessary to save the planet will be with a concerted effort.

### 10. Politicians aren’t the only ones who can have an impact on climate change – we all can. 

As mentioned before, one of the key factors for creating change is raising a global awareness of the issue. And, like every revolution, it starts with the individuals of a society — which means we all have to take part.

How?

By accepting our roles as _everyday_ _leaders_.

Leadership can take many different forms and we can all play a small part. For example, voters can pressure their representatives, investors can reconsider their energy portfolios, families can decrease their carbon footprint, and business leaders can make the bold move of demanding political change.

Each of these steps may seem small, but added together they can bring about big changes. In fact, although we often feel too insignificant to make a difference, the ripple effects of our efforts are much more powerful than we think. After all, human society is an incredibly complex system in which each and every one of us influences everyone else. And if our ripples can touch enough people — especially prominent political figures — change might just be within our grasp.

This step-by-step principle applies to words as much as it does to direct action: every time someone challenges a falsehood about climate change, it becomes a fraction easier for us all to do the same.

### 11. Final summary 

The key message in this book:

**Climate** **change** **is** **no** **longer** **a** **debated** **scientific** **theory,** **but** **an** **acknowledged** **threat** **for** **our** **future** **as** **a** **planet.** **To** **tackle** **climate** **change** **head** **on,** **we** **have** **to** **tackle** **fossil** **fuel** **lobbyism,** **constrain** **global** **carbon** **emissions** **and** **invest** **in** **low-carbon** **technology.**

Actionable advice:

**Challenge** **falsehoods.**

Next time someone tells a lie about climate change, be sure to speak out!
---

### Mike Berners-Lee and Duncan Clark

Duncan Clark is a journalist and author who specializes in climate change. He works as a consultant editor at _The_ _Guardian_ **'** s environment desk and co-runs Kiln, a company that creates new ways to tell stories and disseminate information about environmental issues.

Mike Berners-Lee is a leading expert in carbon footprinting and director at Small World Consulting. He is also the author of _How_ _Bad_ _are_ _Bananas_ _–_ _The_ _Carbon_ _Footprint_ _of_ _Everything_.

