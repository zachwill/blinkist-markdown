---
id: 5800e595230ff80003a1b88f
slug: the-business-of-good-en
published_date: 2016-10-21T00:00:00.000+00:00
author: Jason Haber
title: The Business of Good
subtitle: Social Entrepreneurship and the New Bottom Line
main_color: B3CF78
text_color: 637343
---

# The Business of Good

_Social Entrepreneurship and the New Bottom Line_

**Jason Haber**

_The Business of Good_ (2016) is your guide to social entrepreneurship and earning a living while making a difference in the world. These blinks explain how communities benefit from this new way of doing business and how you gain, too, in becoming a successful social entrepreneur.

---
### 1. What’s in it for me? Learn how capitalism 2.0 will improve how we do business today. 

Capitalism might be great for creating wealth, but it falls flat in addressing some of the largest challenges of our time, such as climate change.

This is where capitalism 2.0 comes in. This new way of doing business aims not only to create wealth but also save the world — and it's the clearest path to solving many of our global problems.

Social entrepreneurship combines profit motives with social problem solving. Businesses don't just sell products; they sell with the added aim of curbing poverty, fighting disease and providing essential infrastructure so everyone, company and customer, benefits.

So forget about making that quick buck. These blinks show how capitalism 2.0 is about focusing on long-term wealth and prosperity — which won't happen if we're destroying the world.

In these blinks, you'll learn

  * why social entrepreneurship is more effective than charitable giving;

  * why today's social entrepreneurs come from the millennial generation; and

  * how successful social entrepreneurs focus on the cause of a problem, not the symptoms.

### 2. A social entrepreneur can make money while having a positive impact and growing a business. 

What do you know about the concept of social entrepreneurship?

You might question whether such a concept is even possible, as it certainly costs plenty of money to tackle issues such as climate change or poverty.

Social entrepreneurs seek out investors who are interested in making both a social impact and seeing a return on their investment. Such investments are called _impact investments_.

An impact investment differs from a regular investment because the _intent_ behind the investment is rooted in the desire for social change. In other words, the goal of an impact investment is to create a beneficial social impact.

To measure success, it's necessary to account for profits, as well as the amount of social good inspired by the investment.

A survey by the Global Impact Investing Network (GIIN) and JPMorgan Chase found that in 2014, some 146 impact investors invested a total of $10.6 billion in projects.

According to the report, 91 percent of impact investors experienced returns that met or exceeded expectations and a whopping 98 percent said the social impact of the investment was in line with or exceeded what they expected.

So from an investor's perspective, impact investments are effective. When it comes to companies, however, it's also possible to turn a profit while changing the world for the better.

Just like an impact investor, a business can make a social impact, too.

While many companies already have sustainability departments or donate to charity, social entrepreneurship works differently by making the social impact of charity as central to the business as profits.

The author's real estate agency, for example, had a social mission. For every deal it closed, a portion of proceeds went to a charity that builds wells in communities without access to clean water.

Focusing on this social good gave the young company a competitive advantage, and helped it attract new clients. This example proves that it's possible to make money while addressing social issues.

> "_I like to define social entrepreneurship broadly as the mechanism by which private sector actors solve public and private sector problems."_

### 3. Mindless charitable giving is ineffective and in certain communities, can even make things worse. 

When you think of the word "charity," you undoubtedly think positive thoughts. And why wouldn't you? Giving money to a needy cause is a good thing, right?

The truth is that charities aren't always up to snuff. In fact, many established charities aren't effective at all, as their operations focus on costs rather than social change.

Why is this the case? When people donate to a charity, they want their money to do good and not just get lost in the bureaucratic process. This pressure makes charities focus on minimizing costs, affecting even the necessary overheads that can be essential to producing the desired impact.

In short, random cost-cutting can be detrimental to a charity's mission. So charities should instead focus on measuring social impact and not just on trimming administrative fat.

When charities don't directly measure the impact of donations, they may start to feel as if they're pouring money into a black hole, getting nothing in return. This doesn't make them very attractive to prospective donors.

But it's not just that charities fall short in creating a positive impact; sometimes they can have negative effects on a community.

For instance, in his book _Banker to the Poor_, Bangladeshi microfinancer Muhammad Yunus says that charity in general is a way of diverting responsibility rather than ending poverty.

Here's another example: since 1997, the National Football League has shipped T-shirts of the losing Super Bowl team to Africa as a charitable act. Yet those free T-shirts have destroyed the local clothing industry in many communities, leaving people even poorer than before.

Having learned from its mistakes, today the NFL works in conjunction with local distributors to send shirts only to communities that truly need and want them.

### 4. The poorer communities of the world represent a vast, untapped market for social entrepreneurs. 

When social entrepreneurs talk about the _bottom of the pyramid_, they're referring to the four billion people who live in developing countries and earn less than $1,500 a year.

This demographic represents a huge potential market. How huge? Approximately $5 trillion.

Even though this group doesn't earn much, they do account for over 50 percent of the world's population and are therefore the largest untapped market in the world.

Given these astounding facts, producing products that meet the needs of the bottom of the pyramid might not be such a terrible idea.

While it might feel "wrong" to make products that people with little money should buy, remember that poor people also want products that make their lives simpler and safer.

Some companies have already targeted this market. Multinational company Unilever maintains over 400 brands, covering food, personal care and cleaning products, and takes in over $60 billion in annual revenue. In 2015, 60 percent of company sales derived from emerging markets.

In another example, doctors in Vietnam were treating babies for jaundice using donated phototherapy devices, machines that use light to cure illness. Yet they were getting poor results.

The Medical Technology Transfer and Services (MTTS), a for-profit company, wondered why this was the case. Company researchers found that the doctors were treating two babies with one device, meaning that neither child was getting the required amount of light to effect a cure.

Inspired, MTTS engineers developed a sleek device that specifically could hold only one baby at a time. Their design was a success; the doctors and patients liked the stylish machines, and more babies received the correct treatment.

Remember that the people at the bottom of the pyramid want the same things that every person wants: quality products and simple design. If your product satisfies those conditions, you can tap a vast market while providing products to people who need them.

### 5. Socially-minded investors, or kickstarters, provide the funds for millennials to change the world. 

Have you ever tried kicking a habit such as smoking? When you work to change a behavior, there's always a risk of falling back into old patterns. In today's society, however, there are two groups that have the power to sustain a lasting transformation to capitalism 2.0: _kickstarters_ and _millennials_.

Unlike generations of wealthy donors before them, kickstarters aren't interested in just writing checks and checking out. Kickstarters have learned a lot from old capitalist habits, which means this group is ready for results and will gladly contribute to making those results happen.

Instead, kickstarters want to make sure that their money actually has an impact on the world.

Consequently, kickstarter investors often do everything in their power to arm a social entrepreneur with the tools he or she needs to succeed. EBay co-founder Jeff Skoll, for example, formed the Skoll Foundation to invest over $500 million in social ventures.

Recipients of the annual Skoll Awards for social entrepreneurs don't just get money but also enjoy institutional support for three years, as well as access to the network of previous Skoll Award recipients.

While kickstarters are crucial to financially supporting social ventures, today's social entrepreneurs come from the group of people born between 1980 and 2000, known as the _millennials_.

This generation grew up in an era in which powerful mass technology such as smartphones was common, offering them instant connections with the world, and instant gratification at their fingertips. Simultaneously, this group also came of age at a time when societal problems, such as global warming, appear to have reached crisis point.

Because of this, many of the millennial generation want to change the world for the better, and they want that change to happen now.

This desire means a majority of millennials aspire to entrepreneurship. In 2014, Bentley University released a study that found only 13 percent of students wanted to climb the corporate ladder, while 67 percent wanted to launch their own companies.

Let's learn how an aspiring entrepreneur can build a social enterprise.

### 6. Successful social entrepreneurs focus on the cause, not the symptoms, and measure their impact. 

If you want to bake a carrot cake, you can't use a cheesecake recipe. In a similar vein, just because a social enterprise is a business, doesn't mean it can follow the same rules as a regular, for-profit company.

When it comes to launching a social enterprise, there are several factors for which you should account. Let's examine two of them.

First, a social entrepreneur should focus on the cause of a problem, not the symptoms.

To have a real impact, you need to get to the root of an issue. In Africa, many people were falling ill as open cooking fires and poor ventilation made the air in their homes unsafe to breathe. The company African Clean Energy looked at the problem logically. Instead of focusing on simply treating sick people, the company designed a new, environmentally friendly stove to replace the polluting open cooking fires.

Importantly, when your company offers a solution to an underlying problem, people will jump to purchase your product. Families were happy to find a product that could keep them and their children healthy and meet a daily need efficiently.

Second, be sure to measure the impact of your activities. Metrics are your holy grail. No responsible social entrepreneur should be unaware of to what extent his product or service is making an impact. To this end, it's important to measure the _effectiveness_ and _efficiency_ of your actions.

Effectiveness refers to the extent to which problems are solved; efficiency considers how much in resources or cash was spent to achieve a result.

For instance, if you want to bring electricity to a community off the grid, your company's effectiveness would be the number of people that have working lights thanks to your efforts. Your efficiency would be the amount of money you spent relative to the income generated by selling your product.

Once you've measured your success, the final step for a social entrepreneur is to scale up.

> "_Today's change makers are expected to have a measurable impact on the problem they seek to correct."_

### 7. Scale your socially-minded business to create a lasting impact, and try to curb your fear of failure. 

Every business faces challenges regardless of industry. The biggest hurdle for social entrepreneurs, however, is how to ensure a lasting impact.

If you don't scale your business, the issue you're tackling will not go away, or could get even get worse.

If you want to make a difference, you need to make your product widely available. One strategy is to create an inexpensive product and offer it to lower-income people at the bottom of the pyramid.

For instance, social enterprise _d.light_ created a solar-powered light for people living without electricity by taking advantage of the dropping costs of solar panels, batteries and LEDs. By designing an affordable light, the company was able to sell it widely, and today, d.light has over 51 million customers in 60 countries.

It's also important to let go of any fear that you'll fail. Remember that failure can be the next step to a brighter future.

New York City officials lamented that some 50 percent of youth offenders after serving time at Rikers Island prison returned to jail after committing more crimes. The city approached Goldman Sachs to invest $9.6 million in a government program aimed at lowering recidivism rates.

This was not a loan in the traditional sense; instead, the returns on this _social impact bond_ were tied to the program's success. If the city could cut recidivism rates by more than 10 percent, for example, Goldman Sachs would make a profit of up to $2.1 million.

In the end, the program wasn't able to achieve the desired reductions, yet investors didn't see the experiment as a complete loss — clients' interest in social impact bonds grew, inspiring Goldman Sachs to pursue other social impact programs, such as one to improve the reading levels of preschoolers.

> _"It's great to make a difference. But for a social entrepreneur to be successful, that difference needs to have a multiplier effect."_

### 8. Final summary 

The key message in this book:

**It's possible to make money while working on social causes, and many investors and entrepreneurs are already making a difference while enjoying impressive profits. Social entrepreneurship in a capitalism 2.0 world is here to stay, and we as a global society will all benefit.**

Actionable advice: 

**Measure your impact with IRIS!**

Measuring your firm's social impact is crucial. A useful tool is IRIS, or Impact Reporting and Investment Standards. IRIS is a free, online public tool that provides impact investors with performance metrics for measuring return on investment.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Building Social Business_** **by Muhammad Yunus**

_Building Social Business_ (2010) is a guide to social businesses, that is, companies that do good. These blinks explain everything you need to know about what social businesses are, how they work and what you need to start your own — and start changing the world for the better.
---

### Jason Haber

Social entrepreneur Jason Haber was the cofounder of real estate agency Rubicon Property in New York, which he sold in 2013. He is also a frequent commentator on CNBC and Fox Business News.

