---
id: 5681bd45704a880007000069
slug: the-business-romantic-en
published_date: 2016-01-01T00:00:00.000+00:00
author: Tim Leberecht
title: The Business Romantic
subtitle: Give Everything, Quantify Nothing and Create Something Greater Than Yourself
main_color: E32D36
text_color: BF262E
---

# The Business Romantic

_Give Everything, Quantify Nothing and Create Something Greater Than Yourself_

**Tim Leberecht**

_The Business Romantic_ (2015) offers an alternative approach to doing business in which passion, not profit, matters most. These blinks guide you through the strategies that will bring authenticity and connection into your workplace.

---
### 1. What’s in it for me? Romance is not just for poets. 

What's the common image of the business person? It's either a ruthless, greedy backstabber like Gordon Gekko or an awkward, socially inept geek à la Mark Zuckerberg. Whichever image you think is more accurate, we can all agree that business is not a romantic occupation.

Well, maybe this needs to change. In order to attract the customer and employee of the twenty-first century, businesses need to adopt a more human, romantic approach. Of course, no one is saying that CEOs should shower their customers with candle-lit dinners or bouquets of roses, so what do we mean? These blinks will show you how romance works in a business sense.

In these blinks, you'll discover

  * why society is teaching us to reach out to people and to distrust them at the same time;

  * why one social network has grown by paying people in pizzas; and

  * why when you leave a business, you should get out of there fast!

### 2. Millennials seek a sense of meaning from their jobs that most employers fail to provide. 

We live in a strange world. In an age where likes are currency, connecting with others and sharing our lives seems to be our number one priority. Yet we're more distrustful than ever. 

More than half of the Millennial generation — those born between the early 1980s and the early 2000s — have shared photos of themselves online. This would seem to indicate a desire to reach out to other people, right? Why, then, do less than a fifth of Millennials say they trust others? This is a paradox that leaves us increasingly unhappy. How did we find ourselves here?

Although social media platforms promise to connect us with others, they only serve to amplify our anxieties about social and economic status. There's always going to be someone in your news feed who is making more money, traveling to more exciting places and, apparently, having more fun than you. 

Of course, we won't stand for this! Many of us work harder and harder, chasing pay rises and promotions to assure ourselves that we're just as successful as our Facebook friends. Unfortunately, this means we're drifting further and further away from what we really need: true connection with others. 

But if we're going to build authentic relationships with those around us, we don't need to delete our Facebook accounts, leave our jobs and go off the grid. We can actually continue, more or less, the same activities. All we need is a change in mindset _._

Why not be a _business romantic?_ Rather than working himself to the bone for financial reward, a business romantic looks for work that offers valuable experiences, and is happier because of this.

Research by the _Harvard Business Review_ reveals that employees who find meaning in their work are more likely to stay with their company, indicate higher job satisfaction and be more engaged. Unfortunately, business romantics are a rare breed.

According to a 2013 Gallup poll conducted in 140 countries, only 13 percent of employees worldwide are fully involved in and enthusiastic about their jobs. Some 63 percent are "not engaged" and "lack motivation." About 24 percent are "actively disengaged," meaning that they are unhappy and unproductive, and subsequently spread negativity to coworkers.

Companies would benefit enormously from turning their employees into business romantics. But how are they supposed to do that? Read on!

### 3. Human connection trumps mere productivity. 

What does your company care about more: your opportunities to have a chat with colleagues or getting your tasks done on time? Of course, the latter is the top priority. But should it be?

A 2013 study conducted by the London School of Economics claims that there is only one aspect of work that creates happiness similar to that experienced outside the workplace: casual interactions with coworkers. Why, then, don't our employers encourage socializing?

The answer is obvious enough. It is widely believed that too much chatting harms productivity. Because efficiency and productivity are the foundations of businesses, we're faced with the same message over and over: shut up and get back to work!

Where does this leave businesses? With 87 percent of the workforce experiencing negative feelings that range from "lack of motivation" to "active disengagement," that's where. If businesses want to engage their Millennial employees, they need to flip their approach.

Instead of policing workplace banter, businesses should let it thrive. Happy employees experience higher satisfaction. And nothing makes people happier than connecting with others.

Case in point: The author invited 15 people for a dinner called the "15 Toasts" during the World Economic Forum. His guests were leaders in the private sector, government and civil society, and they were all total strangers to each other. Not for long, though.

The author asked each guest to offer a toast that answered the question: What is a good life? The toasts began slowly but the guests soon rose to the occasion. Rather than forcing 15 strangers to discuss prearranged topics, this dinner gave them the opportunity to engage as human beings. One guest remarked that it was the first dinner he'd been to where he didn't know anyone, yet left feeling genuinely connected with every single guest.

### 4. The gift of giving can revitalize your workplace. 

There's nothing more deeply human than the act of giving. By giving a gift, we don't only make the recipient happy. We experience joy, too, feeling like a better person. It should come as no surprise that giving is a cornerstone of romantic business. 

Wharton Business School professor Adam Grant explored the power of generosity in the workplace in his book _Give and Take._ He asserts that altruism at the office works wonders for employee motivation. Grant suggests that by creating a workplace culture of giving, companies can expect an incredible improvement in collaboration, innovation, service excellence _and_ quality assurance. 

And the best way to generate an altruistic atmosphere? By offering employees the chance to give during one of their tasks. This a great chance for leaders to get creative. Chocolate manufacturer Anthon Berg created a pop-up store in Copenhagen where customers pay for their chocolate with the promise of a good deed for a loved one. 

Social platform and entertainment website Reddit gives us another great example of embedding altruism in company culture. 

Their campaign "Random Acts of Pizza" allows Reddit users to buy pizza for fellow users based on how funny, entertaining and creative their pizza requests are. This campaign centered on the simple act of giving without asking for anything in return, and it made working for and using Reddit even more fun.

### 5. Customers value products that challenge them. 

Today, we enjoy the luxuries of instant shipping, near real-time delivery and an abundance of services on demand. Life has never been so convenient. But is this what we really want? 

If so, then why do people camp out for three days in front of the Apple store?

Because most customers are actually more interested in experiences, challenges and receiving rewards that they feel they've earned. Dramatic moments and grueling challenges are ultimately what makes a buying experience unique. As a result, the perceived value of a product skyrockets.

Consider also why IKEA is so popular. Aside from the timeless design and great prices, customers love the fact that they can assemble the products themselves. A frustrating hour with an Allen key in one hand and a deceptively simple manual in the other make the finished piece of furniture all the more exciting. 

Of course, companies have to strike a balance. If too little effort is involved in acquiring a product, customers won't value it. But too much effort will only annoy them! If you can find the happy medium, you'll have happy customers that love to suffer just a little for your product.

### 6. A romantic business has mystique. 

Remember the saying, "what happens in Vegas, stays in Vegas?" These days, what happens in Vegas seems to end up on Vine.com. 

Our society loves to overshare, and many businesses decided to follow suit, thinking that openness and transparency would make them more appealing to their customers. 

However, honesty isn't the antidote for poor service. Twitter learned this the hard way during a six-hour Q&A session that users promptly filled with excruciating criticism, complaints and jibes. 

Honesty is always the best policy, but firms can do their PR team a favor by instead playing up their _mystique_. 

One master of mystique is McKinsey. _Fortune_ has labeled McKinsey both the most well-known and the most secretive consulting firm on Earth. So how have they crafted this image?

They don't share what they don't have to. Although they are happy to tell the world about their professionalism, their efficiency and their analytical rigor, they don't overshare the nuts and bolts.

Even clients who work with the firm are asked not to share what the company has done for them. All of this helps create a mysterious image that draws prospects in. If you want your company to connect with business romantics, be alluring! By keeping the secrets of success to yourself, you'll pique customers' interest.

### 7. Romantic businesses know how to conclude customer and employee experiences on a good note. 

Most businesses place great importance on the beginning of a customer relationship. But it's not just first impressions that are crucial. What really determines whether your customers return is the way transactions _end._

Companies that aren't able to control their first impression on customers know this well. Take the airline industry, for example. The beginning of a flight experience is very rarely positive. Airlines have to grin and bear it while delays, waiting lines and frustrating regulations pile up.

Because of this, they ensure the end of a flight is all the more pleasant. They thank us over the PA for flying with their airline, and flight attendants personally say goodbye to each passenger. The Dutch airline KLM even hands out miniature ceramic houses containing schnapps to charm their business-class passengers.

Happy endings aren't just for customers either — business relationships deserve them too. This is particularly important when things go awry, like they did for the author during a botched attempt at merging a design company and an engineering firm.

After the project failed, the author decided to return to his previous job. But he felt he owed it to the other companies to clean up the mess, which meant his farewell lasted for three whole months. Goodbyes became tedious, relationships petered out and his authority faded altogether. This wasn't the ending he had hoped for!

The best ends to partnerships are always clear and clean. When Groupon founder and CEO Andrew Mason resigned, he wrote a letter to his 11,000 staff members. It opened with a joke and closed with the words "I was fired today."

His letter explained to his employees that he had lost the vision that the company needed, but was sure that the new CEO would restore it. This was a graceful and definite exit, and one that the whole company appreciated. Both Mason and the company then moved on to great successes.

### 8. Final summary 

The key message in this book:

**If businesses want engaged employees and loyal customers, they'll need to make some changes. Whether it's through altruism, creative challenges, exclusivity or a personal touch, companies that create authentic experiences within the workplace and customer service will lead the new Millennial market.**

Actionable advice: 

**Open up your office!**

Bring your employees closer together through your office design. Swap closed doors for open-plan spaces, cold conference rooms for comfortable discussion spaces. Employees love nothing more than having a good chat with coworkers. This makes them happier, more engaged, and more productive!

**Suggested further reading:** ** _Misfit Economy_** **by Alexa Clay and Kyra Maya Phillips**

_Misfit Economy_ (2015) sings the praises of people who break society's rules. You'll learn the strategies of such freethinkers — often working on the periphery of the mainstream economy — who succeed amid tough circumstances. Instead of shunning such "misfits," you'll instead be inspired to adopt such strategies in your own life and career.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Tim Leberecht

Tim Leberecht is the chief marketing officer of NBBJ, a design and architecture firm that helps global players like Amazon, Boeing, Microsoft and Starbucks create meaningful brand experiences. His TED talk "Three Ways to (Usefully) Lose Control of Your Brand" has been viewed by a million people.

