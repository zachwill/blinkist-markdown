---
id: 5a096cd2b238e100065bcc65
slug: the-captain-class-en
published_date: 2017-11-13T00:00:00.000+00:00
author: Sam Walker
title: The Captain Class
subtitle: The Hidden Force That Creates the World's Greatest Teams
main_color: 9B761F
text_color: 806319
---

# The Captain Class

_The Hidden Force That Creates the World's Greatest Teams_

**Sam Walker**

_The Captain Class_ (2017) gets to the bottom of what it truly means to be the captain of a winning sports team. You might think all it takes to make a great team is to bring together the best possible players. This is just one of the many misperceptions people have about team sports. Walker points out that the best teams have captains who possess specific characteristics that enable them to lead their teams to victory.

---
### 1. What’s in it for me? Find out how to lead like a winning sports captain. 

What's the number-one secret to a winning sports team? Most people who try to answer this question will focus in on the quality of a team's best players or coach, or examine their strategies.

But in these blinks, you'll learn that the real secret to winning in team sports lies in something else — or rather some _one_ else — entirely: the team captain. In fact, when you look at what all the best sports teams throughout history have had in common, the pattern that emerges is that they have all had the same types of captains.

These blinks will show you what kind of captain this is, as well as the skill set a winning captain must have.

You'll also discover

  * whether a captain has to have superstar talent;

  * what one captain's broken cheekbone says about their role; and

  * how a synchronized war dance can help your chances of winning.

### 2. The best teams have great captains, but they’re usually not the superstars you have in mind. 

Every once in a while, a sports team comes along that's so good, it's practically invincible. Between 1956 and 1969, this was the case for the Boston Celtics basketball team.

Their domination during this period shows how important one key player can be to a team's success.

In 1956, the Boston Celtics had yet to win a championship, but that year they picked up a promising new player, Bill Russell — and they would go on to win 11 championships over the next 13 years.

But once Russell retired after the 1969 season, the team fell apart and wouldn't win a title for another five years.

We can see this same trend in many other sports and with several other teams, including the New York Yankees, and their star player, Yogi Berra. For the Australian football team, the Collingwood Magpies, their success coincided with the presence of their captain Syd Coventry.

In each of these teams, this key player is inevitably promoted to the role of team captain, but they're often not the kind of captain you might expect.

To begin with, these players are rarely the most talented athletes on the team. Instead, they're often what people would call an "average player," and their coach will usually be able to point to one or two crucial skills that they lack. They also tend to be quiet types who avoid the spotlight and interviews.

Generally speaking, these team captains are not the kind of players who offer leadership in the traditional sense. And they're not the ones who dramatically score points at the last minute to pull off a heroic, game-saving win.

So what is it that makes these captains so special that they can lead teams to glory, but that the same teams can't win without them? What elusive abilities do they hold? Let's find out in the blinks ahead.

### 3. Talent and a big budget will only take a team so far. 

Society loves to put talent on a pedestal and worship those who possess an amazing singing voice, a deft hand for visual art, tremendous athletic prowess or a gift for mathematics.

But when we take a closer look at team sports, we can see that talent isn't the decisive factor for winning trophies.

In many other areas of life, we can see a direct correlation between having a _talent cluster_ and getting amazing results. In 2010, a study at the University of Texas showed that the best teams for accomplishing intellectual tasks are the ones in which a majority of the members have above-average skills — in other words, a talent cluster.

But this isn't the case for team sports. In fact, the sports teams with the most talent are generally not the ones winning championship titles.

In 2000, the Spanish football team Real Madrid did everything it could to recruit the world's top talent, including star players Luis Figo, Zinedine Zidane, Ronaldo and David Beckham. But after a strong showing in their first seasons, the team went on to suffer three consecutive seasons without a trophy.

The truth is, you can't buy a winning team, no matter how many millions of dollars you spend.

Nevertheless, many people have tried, working under the misconception that with enough resources, a team is bound to be successful.

But it's easy to prove that this strategy is a waste of money. Many of the best teams in history reached their peak while being relatively poor.

The Collingwood Magpies are an Australian football team that had a famous winning streak in the 1920s. The team won four Grand Final titles despite being so broke that other teams kept buying up their best players.

So, for the teams that achieve those legendary winning streaks, it's not about talent or money.

> _"When it comes to freakish success, lavish spending seems to have little to do with it."_

### 4. A great team requires the right combination of coach and captain. 

The American football coach Vince Lombardi is often brought up when people discuss the greatest coaches of all time. Many credit his motivational skills as being instrumental in turning his team, the Green Bay Packers, into NFL champions and Super Bowl winners.

However, while brilliant coaches can be influential in a team's success, they're not necessarily a requirement.

If we turn our attention back to the Collingwood Magpies, the team won four championship titles between 1927 and 1939 with the help of Jock McHale, who had already won two championships while coaching other teams.

McHale was a highly innovative coach, so his influence can indeed be seen as a contributing factor to the team's success. In this case, he came to the team with unique training methods that favored fast-paced improvisation over fixed routines.

But there are plenty of examples of teams rising to greatness while being coached by people with little to no experience or influential methods. This was the case for the Hungarian football team, the Mighty Magyars, who ruled the footballing world in the first half of the 1950s despite being coached by the unremarkable Gustav Sebes. Another example is the amazing Australian field hockey team, the Kookaburras, which had a decidedly average coach in Ric Charlesworth.

There are also countless teams that continued a brilliant winning streak despite switching from one coach to another.

What history shows us is that even the best coach will still need a great captain if they hope to attain greatness.

While Jock McHale _was_ an incredible influence and motivator for the Collingwood Magpies, it wasn't until he brought Syd Coventry on as captain in 1927 that the team began to reach greatness.

McHale had a great "all for one" philosophy that he tried to impart on his team, but it didn't show up on the pitch until Coventry turned that attitude into action. He was completely selfless on the field and would just as soon assist someone else's goal before scoring one himself. In the end, it took the arrival of Captain Coventry for the Magpies to become legendary champions.

### 5. People will put in less effort in a team environment unless they have the right motivation. 

If you've watched any movie about a sports team, you know there's always a moment when one team is facing a crushing loss until one of their players makes a desperate but brilliant play that turns the tide.

These moments of desperation and courage in the face of adversity are exactly the kind of moments that can inspire a team to give it their all. This is important because athletes and players tend to give _less_ than their all when they're part of a team.

In 1913, the French professor, Maximilien Ringelmann, observed that people generally put in less effort when they're part of a team compared to the effort they'll expend when working solo. For instance, in a game of tug-of-war, participants use less force when part of a team than they would in an individual contest.

Professor Ringelmann referred to this phenomenon as _social loafing_.

In 1979, scientists at Ohio State University put Ringelmann's findings to the test when they added another layer to their experiment in the form of a coach.

In their experiments, the scientists looked at how loudly people would shout when they were alone versus in a pair. The results confirmed Ringelmann's theory — that is, until a third party was introduced to tell the individuals that their partner, or teammate, excelled at shouting. As a result, people began to shout just as loudly in pairs as they had done alone, because they were properly motivated.

We see this same principle in practice in sports, for example when a coach tells their team that they're expected to work as hard as the captain does.

But, of course, for this to be effective, you need a truly hard-working captain like, say, Carles Puyol.

As captain of the FC Barcelona football team of the 2000s, Puyol wasn't afraid to get dirty and throw himself at the ball to save a goal, even after a teammate had made a mistake. He was so willing to sacrifice his body for his team that he once broke his cheekbone trying to block a shot.

This kind of determination is a sign of a great captain — the kind of player who inspires a team to rise up and give everything they have.

### 6. A successful team captain isn’t necessarily a great role model. 

There are two activities in which society seems to think it's acceptable for people to hurt one another for victory: war and sports.

However, in both cases, the people involved will be heavily scrutinized over the methods they use.

Sports fans have a specific — and often misguided — set of expectations for how a team captain should behave. They generally prefer when their team's captain is a shining example of prim and proper behavior, the kind of player that can serve as a role model for every fan. And heaven forbid they fail to meet these expectations.

When David Beckham was captain of England's football team in the 2000s, he was subject to relentless criticism about his haircut, which was deemed to be unmasculine, as were the tears he shed after a tough loss.

Displaying the "appropriate" qualities can be more important to fans than a winning record.

Take Derek Jeter, for example, who became the captain of the New York Yankees in 2003. Calm, consistent and a solid family man, Jeter was always admired by his fans, even though his time as captain resulted in very few trophies.

What fans sometimes miss is the fact that players who are always on their best behavior don't necessarily make the best captains. Instead, great teams are led by players who aren't afraid to bend the rules or risk being unpopular.

During a 2015 Rugby World Cup match, the captain of New Zealand's All Blacks team was Richie McCaw, a player who gave everything he had to help his team win. During one match, McCaw found himself flat on the ground, while a player from the opposing team had the ball and an open field in front of him.

McCaw did the only thing he could and tripped up the player, drawing a penalty and many vicious boos from the crowd — but also preventing the opposing team from scoring a try.

Even so, it was clear to his team that McCaw was relentless in his drive to win, and his team won the match in the end, which was far more important than pleasing the crowd. That's the sign of a great captain.

### 7. Captains aren’t superstar players, they’re the indispensable supporting players. 

Superstar players are the kind that will get the ball as the clock is seconds away from hitting zero and score the game-winning goal or basket every time.

But most captains aren't this kind of ego-driven player and rely on different qualities to gain respect and lead their teams.

More often than not, a team's captain is hard at work at the back of the field doing the unglamorous tasks that are no less important — or difficult.

Richie McCaw of New Zealand's All Blacks played the position of flanker, a defensive position that takes a great physical toll, as it requires a lot of tackling and close contact.

It was the same for Carla Overbeck, the former captain of the US national soccer team. She hardly ever scored goals, but was always assisting her teammates' goals and making the right passes.

This also points to how defensive players can lead their team by assisting others and creating opportunities for their teammates to be the very best they can be.

Often, some of the best captains are on the sidelines, observing and finding the right time to jump in when they can be most effective.

In the 1990s, the captain of the French football team was Didier Deschamps, who described his role as captain as having very little to do with his own performance — it was all about helping others. In Deschamps's case, this meant finding ways to get the ball to Zinedine Zidane, the team's star player and top goal scorer.

In this team, Zidane relied on Deschamps just as much as Deschamps needed Zidane to win games. So even though Zidane was the star, he had much less of an impact without the support of his captain, who was dedicated to creating opportunities for him to shine.

This is how someone at the back of the field can be the leader, the one who everyone relies on for both inspiration and the opportunity to deliver the goods.

But, as we'll see in the next blink, the captain's duties extend to off the field as well.

### 8. Don’t expect captains to give the big speeches; they motivate by talking to teammates one-on-one. 

We've seen how a captain's dedicated performance can inspire his teammates. As such, you might think a team captain will also be the one in a team huddle giving impassioned, motivational speeches.

We've certainly seen this plenty of times in Hollywood movies. But in reality, most team captains will leave the speeches to the coach or someone else.

As captain of the French national handball team, Jerome Fernandez admitted to failing miserably at pep talks. And when this happens, it can end up hurting team morale more than helping it. This is also likely a reason why Carles Puyol, the captain of FC Barcelona, would _never_ address his teammates with a speech.

Even an extroverted player like Ferenc Puskas, the captain of the Hungarian football team during the 1950s, believed the speeches were someone else's responsibility.

But just because we associate speeches with leadership, it doesn't mean captains aren't using their words — they're just doing it on a one-to-one basis.

So yes, communication is indeed vital to the success of any team.

In 2005, MIT's Human Dynamics Laboratory studied different types of teams, including those working in hospitals and schools. Participants were recorded throughout the day to monitor their interactions and to make note of every minute detail about how they spoke and what their body language was saying.

The study found that those who fit the category of a "natural leader" would circulate from one team member to the next and engage in brief but focused one-on-one discussions.

We can see this play out in sports as well. In the 1998 FIFA World Cup, France was leading 2-0 against the defending champions Brazil. Zinedine Zidane had scored both goals, but at halftime he could barely stand up in the locker room.

So his captain, Deschamps, took Zidane's face in his hands and made his point clear: he couldn't stop fighting until the game was over.

Zidane used those words from his captain as motivation to get back on the field and, in the end, France won 3-0 to claim the country's first World Cup.

### 9. Captains can take advantage of human nature by sharing the right emotions and synchronizing their team. 

If you've played team sports, you may have experienced the thrill of everyone on your team locking in and feeling like your minds are all perfectly connected.

Scientists and psychologists have known that humans have a capacity for herd-like behavior and group mentality. But it's only been relatively recently that they've identified _mirror neurons_.

This discovery was made in 2004 by researchers at the University of Wisconsin. They confirmed that mirror neurons are specific brain cells that are activated when we recognize emotions in other people, and cause us to experience those same emotions.

The next time you experience contagious laughter or start crying when you see someone cry, you can blame your mirror neurons.

Even though we don't fully understand this aspect of human nature, team sports have always taken advantage of it.

When team captains try to motivate their players, they seek to evoke emotions of determination and excitement. The captain will first bring these emotions up in herself, such that her teammates will recognize it and start sensing that excitement in themselves.

There are a number of ways captains generate the emotions it takes to win games.

Bill Russell of the Boston Celtics would stride onto the court and give an icy stare at the opposing team with his arms crossed in front of his chest as if he were the king of basketball.

This wasn't just a way of psyching out the competition; Russell was channelling a dominant spirit, the very emotions he wanted his teammates to pick up on in order to win.

For New Zealand's All Blacks rugby team, they conjure up the right emotions by performing a traditional _haka_ war dance in front of an eager audience and a perplexed and sometimes intimidated opposing team.

The performance of the haka involves the team captain shouting out to his players, who respond by shouting back in unison while performing powerful poses, facial expressions and gestures.

While they didn't know it when they first brought this Māori tradition onto the rugby field, such exercises were syncing up the players' mirror neurons, which in turn caused them to play as a united, harmonious team.

### 10. Final summary 

The key message in this book:

**The impressive winning streaks of the world's best sports teams are not due to the oversized egos of their most talented superstars. Instead, those streaks are achieved by a winning combination of a savvy coach and a self-sacrificing captain who isn't afraid of risking a negative public persona or putting his body on the line for the sake of the team.**

Actionable advice:

**Enable your teammates to do their best work.**

If you want to become a team captain, fight the urge to stand in the limelight and receive all the attention. Being captain is about supporting your teammates and becoming an indispensable part of their success. This is how solid players become highly respected captains and lead their teams to victory.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Sports Gene_** **by David Epstein**

_The Sports Gene_ takes a look at the physiological traits that are beneficial in various sports, and at their hereditary background. It also examines why people in certain parts of the world have evolved in their particular way, and how this is beneficial in the realm of certain sports.

**This is a Blinkist staff pick**

_"In comparison to the famous 10,000-Hour rule, these blinks opened my eyes to some of the other ways people become great. Plus, I had a great time talking to Epstein about the book on the Blinkist podcast."_

– Ben S, Audio Lead at Blinkist
---

### Sam Walker

Sam Walker is an award-winning investigative reporter and deputy editor for the _Wall Street Journal_, as well as the founder of the newspaper's daily sports coverage. His other books include _Fantasyland,_ a fascinating journey into the heart of fantasy baseball competitions.

