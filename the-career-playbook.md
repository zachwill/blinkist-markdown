---
id: 55b7e9ec3235390007a70000
slug: the-career-playbook-en
published_date: 2015-07-30T00:00:00.000+00:00
author: James M. Citrin
title: The Career Playbook
subtitle: Essential Advice for Today's Aspiring Young Professional
main_color: 30B7EF
text_color: 1F7599
---

# The Career Playbook

_Essential Advice for Today's Aspiring Young Professional_

**James M. Citrin**

_The Career Playbook_ (2015) is based on interviews with top professionals as well as a survey of thousands of graduates and young professionals. It offers valuable advice for launching and building a strong career; by acquiring the right skills, building the right network and presenting yourself well, you'll be on your way in no time.

---
### 1. What’s in it for me? Learn how to play the cards of your career. 

You're just out of college and you're ready to make a start in the world of work. Work is your oyster, and you dream of a job that aligns with your passions and career choices — and that starts you off at a healthy salary to boot. And yet, if this is your dream, you're likely to be disappointed, at least at first.

Almost everyone is initially met with disappointment, including all those executive managers whose positions you want so badly.

In these blinks, you'll learn why. You'll learn that everyone's career path differs, and how you can make the smartest (but maybe not the most immediately rewarding) career moves as a young professional wishing to get ahead in your work life.

In these blinks, you'll discover

  * what John F. Kennedy has to do with your career path;

  * why you shouldn't say "you know" in a job interview; and

  * how to get a job that requires job experience that you don't have.

### 2. Pursuing your dreams by balancing the three points of the Career Triangle: job satisfaction, compensation and lifestyle. 

The Bermuda Triangle, an expanse in the Atlantic covering one million square miles, has enveloped wayward ships and planes for centuries. The _Career Triangle_ might seem less dangerous, but it can be just as challenging to navigate.

Why? Because in the early stages of your career, you have to struggle to balance your _job satisfaction, lifestyle_ and _compensation_.

It's very unlikely that you'll have the freedom to explore and discover yourself during the first few years of your career. You'll probably have to choose between making money and pursuing your own ideas, and the best-paying options probably won't be the most fulfilling.

So don't expect to maximize your income _and_ secure your dream job right away; in the beginning, you'll have to trade your _job satisfaction_ and _lifestyle_ for higher _compensation_.

When faced with this prospect, you might be tempted to pursue your passion and abandon everything else. However, that's not such a good career strategy.

In _Psychology Today_, Marty Nemko, a prominent career coach, explained the risks of chasing your dreams. Nemko writes that most people's passions center around travel, entertainment, fashion or political causes, like education or environmentalism — making jobs in those fields hypercompetitive.

That means employers in such fields usually pay below the market-compensation standard, because they have access to so many candidates who are willing to work either for free or for very little. If you dream of entering the fashion industry, for instance, you better plan on competing with droves of others for basic entry-level positions or internships. That means planning to be undercompensated.

### 3. A strong network helps you land a better job. 

Why do some people secure top positions in their organizations, even when they're not particularly skilled? Chances are, they've built and put to use a powerful _network_.

In his survey, over half of the author's respondents reported securing their jobs through _networking_ — by being recruited by someone they knew or getting a job recommendation from a colleague.

But when you're looking for a new job, don't just seek out information from your closest contacts; your most useful connections might not be the people you see everyday. Friends you fell out of touch with years ago are often more helpful than your current friends.

Adam Grant of the Wharton School of Business calls these connections _weak ties_. Your weak ties are often more beneficial because they move in different circles than you, so they provide access to a broader range of information.

So develop your connections by helping as many people as you can. When you help someone out, they'll want do the same for you in the future.

That's why John F. Kennedy once said, "Ask not what your country can do for you, ask what you can do for your country." The same holds true for your colleagues. If you recommend your friend for a good job offer, they'll be motivated to do the same for you in the future.

But be sure to seek out people who are willing to help others, otherwise your network won't do you much good. One way to do that is by focusing on _super-connectors_ — people who've provided you with more opportunities than usual.

Try making a list of your contacts and tracing how you met all of them. If the same name keeps popping up, that's definitely a person to whom you'll want to stay close.

### 4. Every career passes through three phases: the Aspiration Phase, the Promise Phase and the Momentum Phase. 

Consider the career paths of a doctor and a mechanical engineer. They're certainly different, but they both pass through the same three phases: the _Aspiration Phase_, the _Promise Phase_ and the _Momentum Phase_.

The Aspiration Phase starts in your college years and continues into your first few years in the workplace. This period is about discovery, introspection and getting to know yourself. Remember: your value in the career market is almost entirely determined by your potential.

In the Aspiration Phase, it's important to try out as many different tasks and jobs as possible. That's how you'll discover your strengths, weaknesses and interests and acquire your marketable skills. A mechanical engineer, for instance, might experiment with different internships and apprenticeships to figure out whether she's most interested in developing products or in helping the team as a project manager.

The Promise Phase begins with your first or second job. You'll continue to build your strengths in the Promise Phase, but you'll also start developing specific professional skills and making meaningful contributions to your organization.

So a mechanical engineer who's interested in development might specialize in working on the company's combustion engines. The Promise Phase is when you start living up to your initial potential.

The Momentum Phase runs through your early thirties to early or mid-forties. That's when, thanks to the skills and connections you've gained in the previous phases, you get better at overcoming tough obstacles. You'll become a much more valuable asset in your industry and be able to work across multiple sectors.

So a mechanical engineer who has developed her skills with combustion engines might try working with turbines in the aeronautics industry when she's in the Momentum Phase.

> _"Certainty exists only in hindsight, when you're looking back on the decisions and actions you took that eventually led to career success."_

### 5. Spend time seeking out meaningful interviews and present yourself in a straightforward manner. 

Once you know how you want to balance your Career Triangle, the next step is figuring out _how_.

The first step is to get yourself noticed in your field. A lot of people start by developing their LinkedIn profile — but how do you make yourself stand out?

There are several tips for managing your LinkedIn profile. First off, remember that the number of connections you have doesn't signify status. It's actually usually best only to add people you know personally.

So strive to make LinkedIn connections only with people you've had memorable conversations with. The only exceptions to this rule are people who know someone else you're certain you want to connect with. One study of LinkedIn found that over 40 percent of people who succeeded in making connections did so through a person whom they already knew.

So after you've connected with that hiring manager, aim to make the best impression you can by showcasing your qualifications and skills.

You also need to create a clear and straightforward resume. Keep it sharp and pleasing to the eye. Don't go overboard with flashy designs; artsy flourishes tend to distract the reader from the actual content.

Remember that HR only has a limited amount of time to look over resumes, so it's important to be as concise as possible when you outline your skills and job experience. Include two or three bullet points for each of the jobs you list. Be specific about your accomplishments and the responsibilities you had.

Describe things with precise numbers whenever possible. Numbers are clearer than lengthy sentences.

> _"A cover letter won't land you the job, but it can eliminate you from consideration."_

### 6. Build your credentials and uncover your hidden skills when you're looking for your first job. 

What's the first barrier you face when looking for your first job? For most, it's all those job postings that require prior experience. How are you supposed to get experience if you can't get a job?

This is called the _permission paradox,_ and there are a few ways to overcome it.

One way is to build your _credentials_. Do a program that earns you a professional certification or take an online course that teaches you a marketable skill. There are plenty of free online courses available through websites like Coursera and Khan Academy.

You can also learn valuable skills in your spare time. Many colleges offer six-week summer courses in business and finance, or in other fields, like healthcare or aviation.

Sometimes you don't even have to build your credentials! You might already have credentials you aren't aware of, because they're hidden in your past experiences.

That's why another method for overcoming the permission paradox is to make the most of your current experiences. James, a graduate with a BA in geography, did this when he wanted an entry-level position at a food company.

James didn't have professional experience in project management — one of the job requirements — but he realized he had a different kind of valuable experience. He'd planned a three-week trek across Eastern Europe for himself and his friends! He _did_ have the experience necessary, because he'd worked with a group of friends to _project manage_ their trip.

So James put this task in his cover letter, and described the actions he took to research their itinerary and find the lowest fares. He demonstrated that he _did_ have management experience — and he landed the job!

### 7. Present yourself well in interviews by preparing as much as you can. 

How do you feel about job interviews? If they make you anxious, you certainly aren't alone. Most of us feel uncomfortable with this part of the job-application process. But don't worry — there's a simple way to present yourself in interviews as a strong candidate.

An important strategy in interviews is to communicate in an open and equal way with your interviewer. Speak as if you're on common ground — not as if you're the interviewer's subordinate.

Some people are great at communicating with their friends, but freeze up in interviews. They make mistakes, like avoiding eye contact, tapping their feet or using too many vocal tics, like "you know" or "I mean."

You can overcome these bad habits by asking a friend to conduct a mock interview with you. Filming this interview, and then study the footage. Does your body language match your words? Are you fidgeting or looking down? The more you practice with friends, the more comfortable you'll become during interviews in real life.

Also, aim to tell a narrative story about yourself instead of just answering questions. Stories are much more memorable — and you want to be remembered!

A lot of people make the mistake of assuming that interviewees are supposed to listen quietly to each question and respond accordingly with a thorough reply. Most interviews are structured in a question and answer format, which can make the interview seem like a test.

But it's actually better to connect your answers together, presenting the interviewer with a three-dimensional view of who you are. Present yourself as a whole — tell your story in a meaningful way. After all, you're more than just a bullet list of attributes!

### 8. Strive to always make a good first impression. 

Imagine that you're meeting a friend's friend for the first time. You try to break the ice by making a risqué joke, but it doesn't go over well. It wouldn't be surprising if that person avoided you in the future; first impressions have a big impact!

So how can you make the best first impression? One important strategy is to always maintain a positive and enthusiastic attitude.

Most successful executives are nostalgic for those youthful days when they had the energy to work and play hard, and if you display the same attitude, they'll feel a deeper connection with you. Your colleagues and supervisors will also enjoy being around you more when you're in a good mood.

Don't stop looking for opportunities to learn, either. Whenever you take on a new task or role, use it to learn a new skill. You'll gain a reputation for your desire to grow and improve yourself, which will get you a lot of positive attention from your supervisors.

That first impression is also based largely on the way you communicate. Good communication skills are critical for advancing your career!

So avoid using verbal crutches like "um, like..." or "I mean." Those fillers can make you sound less confident or intelligent. Don't raise your voice at the end of your sentences, either. That makes you sound insecure about what you're saying.

Experts have found that even non-verbal communication has a big influence on your first impression. So watch your posture and facial expressions. Sit or stand upright and always maintain eye contact when speaking to others. Make that first impression a good one and it will take you a long way.

### 9. Progress through your career laterally – not just in a straight line. 

How do you think people become top CEOs? Most people assume that CEOs climb the career ladder one step at time, going from junior management to middle management to upper management, building their skills along the way. In reality, however, it's not that simple.

Most successful people get to the top with a bit of lateral maneuvering. They don't slowly climb a single ladder — they hop between multiple ladders, and each step isn't necessarily higher than the last!

So if you're working in junior management at HR, moving to a junior management position in marketing or finance might be more valuable than aiming for middle management in the same department.

Why? Because exploring different fields and departments gives you a broader range of skills and experience. And that's what makes you stand out when you're looking for a top executive job.

Jumping between ladders doesn't just mean exploring different departments, though. It also means working with a variety of organizations. You might think moving from a big firm to a start-up would be a downgrade, but such a move could actually provide you with a lot of new learning opportunities. You might earn less, but it might be better for your career in the long-term.

The progression of your career still has to make sense, however. You should still be able to explain it in a coherent narrative. If you can't, people won't be sure if the experience you've acquired is actually valuable.

So don't fall into the trap of staying on just one path. Explore different areas and build a range of skills; but make sure that the experiences you're acquiring are relevant to your overall career.

### 10. Final summary 

The key message in this book:

**You don't have to be a graduate of some Ivy League school to build a solid career for yourself. If you follow your core principles by making sound decisions, working hard and building meaningful relationships, you'll find your way to success. So stay dedicated, present your skills and experience clearly and always be sure to make a good first impression — both in and out of interviews.**

Actionable advice:

**Take your contacts seriously.**

If someone introduces you to a new contact, be sure to follow up immediately. Send thank you emails as soon as you finish any meetings. Don't be afraid of being too forward — you're not on a date! And even if an executive takes a few days to respond, they'll probably still be impressed if you respond immediately.

**Suggested** **further** **reading:** ** _The Defining Decade_** **by Meg Jay**

Drawing on a wealth of personal stories and fascinating facts, _The Defining Decade_ argues that, contrary to popular belief, 30 is _not_ the new 20. Author Meg Jay uses her vast experience as a professional psychologist to advise twentysomethings on such issues as choosing a partner, starting a family, picking a career, and generally making the most of one's 20s. Jay also argues that the years between 20 and 30 are the time to establish serious goals and, because the adult brain is at its most pliable then, to begin to take steps toward reaching them.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### James M. Citrin

James M. Citrin is an expert on leadership, governance and professional success. He's the leader of _Spencer Stuart's CEO Practice_, the world's premier executive search firm. He's also given roughly 5,000 executive interviews.

