---
id: 5984edabb238e100054c1cf8
slug: the-case-against-sugar-en
published_date: 2017-08-08T00:00:00.000+00:00
author: Gary Taubes
title: The Case Against Sugar
subtitle: None
main_color: E32D3C
text_color: BF2632
---

# The Case Against Sugar

_None_

**Gary Taubes**

_The Case Against Sugar_ (2016) offers a critical look at how the sugar industry has grown ever stronger despite medical data showing that it can be harmful to our health. Find out how this happened, and how critics have been silenced and ridiculed despite overwhelming evidence that this one ingredient can be linked to many of the most serious diseases in the Western world.

---
### 1. What’s in it for me? Find out why sugar may be the new tobacco. 

We all know that sugar is found in chocolate, soda and candy — but did you know that it's also in yogurt, ready-made meals, potato chips, marinades, sushi, ketchup and mayonnaise, among many other food products? In fact, sugar has gone from being a luxury item reserved for the monarchs and elites of the world to becoming a staple in most diets, whether we want it to be or not.

So how did we get to this point and what are the implications for our health?

Let's look at how sugar conquered the world and how it's come to influence so much of our lives. From its humble origins on Pacific islands to its current position at the heart of a shrewd and lucrative industry, sugar has come to influence our lives and health in a way most of us fail to perceive.

In these blinks, you'll find out

  * how a simple root changed the history of sugar;

  * why a calorie is not simply a calorie; and

  * how sugar is related to some of the most deadly diseases in the world.

### 2. There was a time when sugar wasn’t an additive in the majority of our processed foods. 

The history of sugar can be traced back thousands of years to the island of New Guinea and its native people, who were the first to plant sugarcane for cultivation.

For a long time, harvesting sugarcane was the only way to produce sugar, but this eventually became a costly burden. The sugarcane plant only grows in the tropics, and it was complicated and expensive to transport it to the rest of the world.

So, before other methods of producing sugar were developed, the cost and labor of sugar production made it an ingredient that only rich people could afford. It was such a luxurious status symbol that it was included, along with pearls and other treasures, in gifts that the King of Spain would habitually receive.

But it was only a matter of time before sugar was to become cheap and widely available, and this was primarily due to a plant now known as the sugar beet. Sugar beets can be grown just about anywhere, and once a process was developed to extract beet sugar, the ingredient became a whole lot easier to come by.

Then came the steam engine, which brought on the Industrial Revolution. As a result, a refinery in the 1920s could produce the same amount of sugar in a day that used to take a decade to produce in the 1820s.

It was this cheap, refined sugar that eventually made it possible for companies to produce the junk food found in supermarkets today.

In the early nineteenth century, people primarily used sugar to sweeten tea, coffee or other hot beverages. But with the new methods of refining sugar, sweet foods like candy, ice cream, chocolate bars and soft drinks could be mass-produced at a low cost. Plus, previously sugar-free foods, like bread, could now also be sweetened with sugar.

One of the most revolutionary sugary products was Coca-Cola, which was invented in 1885 by John Pemberton, who originally promoted the drink as a "brain tonic."

It wasn't until years later, when Asa Candler added more sugar and turned it into a soda, that it became the world's most popular soft drink.

### 3. There are different kinds of calories, and calories from sugar are among the worst for your health. 

Maybe you've heard the phrase, "a calorie is a calorie," and assumed that five calories from sugar are the same as five calories from an apple. But this couldn't be farther from the truth.

Like any science, nutritional science wasn't always as exact or sophisticated as it is now, and for a long time we've lived under two common misconceptions.

One is that all calories are equal, and the other is that our calorie intake is the primary cause of unhealthy weight gain.

For the sugar industry, this was great news. They could freely promote sugar by suggesting that three teaspoons of sugar had fewer calories than an apple. But modern-day nutritional science shows that not all calories are the same, regardless of what the sugar industry might say.

More accurate theories on nutrition emerged in the 1960s, when new technology allowed us to measure hormones in the blood stream. Scientists could now see that most of our hormones are hard at work extracting energy from our stored fat cells.

However, they noticed one hormone that actually performed the opposite function: insulin.

Our bodies produce insulin when our blood sugar, also referred to as blood glucose, level rises, and the body stores fat cells instead of turning them into energy. Things only go back to normal once our blood sugar and insulin drop to their usual levels.

So, what causes high blood sugar levels, rising insulin levels and an increase in fat storage? Eating meals that are high in carbohydrates — especially sugar.

Therefore, all calories are _not_ created equal.

Despite this evidence, the sugar industry has stayed on message and continues to push the low-calorie theory. And their persistence has been surprisingly effective, even convincing some scientists. A 2015 article in the _New York Times_ even showed that many scientists still consider a calorie surplus to be a main cause of obesity.

### 4. The sugar industry has been hard at work protecting its image and fighting competition. 

Has anyone ever told you to that you need to add more sugar to your diet? It sounds pretty unlikely, right?

But this is exactly what the sugar industry was promoting in 1928, when they created the Sugar Institute as part of their campaign to increase demand for sugar. They did this through an ad campaign that actually promoted sugar as a health food!

In their estimation, sugar was full of benefits: in summer, it refreshed the body; in winter, it provided a boost to your immune system; and in autumn, it was good for avoiding afternoon fatigue.

The 1950s was the period in which the obesity rate in the United States began to increase and different dieting trends began to emerge. This is also when the sugar industry turned to the "a calorie is a calorie" theory and suggested that other foods be cut from one's diet, not sugar.

But a different kind of campaign was launched in the 1960s, when the general public, as well as manufacturers of sweet products like diet sodas, began turning to artificial sweeteners as a healthier alternative to sugar.

There was no debating that artificial sugars like _saccharin_ and _cyclamate_ had fewer calories, so the sugar industry tried a different approach to this threat by trying to get the Food and Drug Administration (FDA) to ban them.

The FDA has a major influence on the food industry in the United States through its approval system of giving certain foods GRAS status, which stands for Generally Recognized As Safe.

In 1958, the Food and Drugs Act was amended to state that no additive could be deemed safe, "if it is found to induce cancer when ingested by man or animal."

So, between 1963 and 1969, the sugar industry honed in on this stipulation and spent over $4 million on studies to remove the GRAS status for cyclamate.

By the end of the campaign of testing artificial sweeteners on rats, they did succeed in getting cyclamate banned. It was considered to be "possibly carcinogenic" — though you would need to drink 550 cans of diet soda every day in order to equal the dosage levels given to the rats.

### 5. The sugar industry has spread lies about the dangers of other foods. 

You may have come across the popular assertion that saturated fat causes heart disease. However, the truth isn't as simple as this basic equation — which might even be entirely false.

So where do rumors like this come from?

In this case, the scientist Ancel Keys was the one who popularized the link between eating fatty foods and getting heart disease.

During the twentieth century, heart disease was on the rise and everybody was looking for a simple explanation, so Keys's theory was readily accepted.

But if we look closely, we can see that Keys had a conflict of interest: his research was sponsored by the sugar industry. Not only that, he was the inventor of _K-Rations_, the food packets used by the military, which were also full of sugar.

Needless to say, the sugar industry was eager to spread the news of the findings as additional evidence that sugar isn't bad for your health.

They were largely successful in their propaganda, and fat was seen as the number one enemy in the world of food throughout the 1970s. By the 80s, any scientist who so much as hinted that sugar was the real cause of heart disease ran the risk of being called out as a quack by agents of the sugar industry.

Yet the evidence was becoming ever clearer that sugar is the real culprit. During the twentieth century, heart disease steadily increased in Western nations, alongside obesity, diabetes and high blood pressure.

For some scientists, it was clear that this wasn't a coincidence, that these diseases were somehow related and that there was something linking them to the prosperous nations where this was happening. When they looked at the changes that had been made to typical diets and foods in these countries, the answer was clear: sugar.

There was also evidence to suggest that fat isn't so bad. Certain cultures, such as the Inuit people of Alaska and the Masai in Africa, have been eating a high-fat diet for generations and live long and healthy lives.

Diseases only began to show in these communities when sugar-rich foods were introduced.

### 6. Too much sugar leads to insulin resistance and metabolic syndrome. 

When it comes to linking a disease to sugar, perhaps the strongest association is with diabetes, specifically in terms of how sugar causes your body to develop a resistance to insulin.

As discussed, insulin is a hormone that is released when your blood sugar rises. When it's active, it stores fat — but it also reduces your blood sugar.

So, when you eat something sweet, the glucose in the food raises your blood sugar levels. To help your body keep your blood sugar in a healthy range, your pancreas will release insulin to either use the glucose as fuel or to store it as fat. And when blood sugar levels normalize, energy will again come from your stored fat.

When your body becomes insulin resistant, this means that the hormone can no longer store or use the glucose in your bloodstream, because the cells have become resistant. This can lead to a vicious cycle since, to lower your blood sugar levels, you need introduce more insulin into your body.

It may never be possible to prove with absolute certainty, but it is very likely that sugar is the cause of this resistance. In a controlled study at Stanford University, rats developed insulin resistance after being fed fructose, or fruit-based sugar.

Again, this would appear to prove that all calories are not the same, since 100 calories of sugar will provide different results than 100 calories of fat or protein, especially in terms of insulin release.

Another result of insulin resistance is _metabolic syndrome_, which is usually a precursor to diabetes or heart disease.

The first symptom of metabolic syndrome is an expanding waistline, but it is due to a combination of several conditions, such as obesity, high blood pressure and inflammation coming together. And all of these factors can be traced back to having too much sugar in your diet.

As we'll learn in the final blink, insulin resistance and metabolic syndrome are associated with just about every Western disease.

### 7. Sugar can be linked to a variety of diseases common in Western countries, including cancer. 

If you're still not convinced that sugar is bad for your health, let's look at what happened to the people living on the island of Tokelau, located off the coast of New Zealand. In 2014, this small community had the world's highest rate of diabetes, at 38 percent of its population.

But this problem was relatively new to the people of Tokelau. This alarming trend dates back to the 1960s, when New Zealand established a program that allowed them to emigrate to the mainland.

And here's where we can track the exact changes that happen to their diet — and their health.

In 1968, the diet of the Tokelau people consisted of coconut, pork, fish, chicken and breadfruit. Fat made up more than 50 percent of the calories and sugar only two percent, equal to about eight pounds of sugar per year. At this point, only three percent of the men and nine percent of the women were diabetic.

But then, by 1982, with access to the mainland, the consumption of sugar had risen to 55 pounds per year. Now, 11 percent of the men and one out of every five women who emigrated were diabetic. In addition, many were now obese and suffering from different Western diseases, which they had never had to deal with before.

This shows the deadly effects sugar can have, but there's one more example that reveals a substantial link to cancer.

This key finding was taken from a study of people who weren't obese or diabetic, but had metabolic syndrome and were experiencing insulin resistance. In the end, the research showed that patients who had increased levels of insulin in their blood ran a higher risk of developing cancer.

This data appears to have been backed up in 2005, when researchers in Scotland studied diabetic patients who were taking the drug _metformin_ to reduce insulin resistance and lower their insulin levels. The results showed that these patients had a markedly lower risk of getting cancer than those who took drugs that didn't reduce insulin levels.

With all this evidence pointing in one direction, it's probably best to keep those sugary foods to an absolute minimum.

### 8. Final summary 

The key message in this book:

**There's a strong case to be made that sugar is a principal underlying cause of many of the diseases currently plaguing Western nations. This is primarily due to the way sugar causes insulin resistance, which multiple studies have identified as a precursor to many other diseases. This isn't a new discovery either, as doctors have warned against the consumption of too much sugar for years. Nevertheless, the sugar industry is a powerful voice that has worked for decades to silence the truth.**

Actionable advice:

**Try to live without sugar.**

Most of us consume sugar every day and can hardly imagine living without it. The same is true for smokers: they can't imagine a life without cigarettes until they stop, and eventually the day comes when they can't imagine ever touching a cigarette again. The same could be true for sugar, but the first step is to realize how easy it is to live without it.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Why We Get Fat_** **by Gary Taubes**

_Why We Get Fat_ explains why certain types of carbohydrates are the main reason we get fat. The book not only shows why people gain weight, but why the topic is so controversial. It also talks about why some people get fat and others do not, the role genetic predispositions play in this process, and which foods we should all avoid.
---

### Gary Taubes

Gary Taubes is an award-winning journalist who covers science and health. His work has appeared in publications such as the _New York Times_, the _Atlantic_ and the _British Medical Journal_. He is also the author of _Why We Get Fat_ and _The Diet Delusion_.

