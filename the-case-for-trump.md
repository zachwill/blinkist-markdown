---
id: 5cb5eaa46cee070007d4e844
slug: the-case-for-trump-en
published_date: 2019-04-19T00:00:00.000+00:00
author: Victor Davis Hanson
title: The Case for Trump
subtitle: None
main_color: BD9C53
text_color: 806938
---

# The Case for Trump

_None_

**Victor Davis Hanson**

_The Case for Trump_ (2019) explains the story of how a businessman and reality TV star with no political experience managed to best his Republican rivals and Hillary Clinton to become president of the United States. And what's more, it shows how Trump, despite being constantly attacked in the media, is managing to implement his policies successfully.

---
### 1. What’s in it for me? Discover the meaning of Trumpism, and how it won the 2016 election. 

Unless you've been living under a rock over the past few years, you've probably heard that Donald J. Trump is now the president of the United States. His candidacy, nomination and election rocked the world, and his presidency continues to generate constant controversy.

But the question remains: How did he do it? Trump faced over sixteen qualified Republicans in the primaries, and went on to lock horns with arguably one of the most skilled political operatives of modern times, Hillary Clinton. And yet he won.

The answers behind Trump's victory are actually much easier to explain than one thinks. According to the author, at least, Trump managed to successfully tap into a growing frustration among America's white working-class majority. By combining simple issues with a media-savvy campaign strategy, Trump was able to awaken a whole demographic of Americans who had previously felt abandoned by the country's political elite.

In these blinks, you'll learn

  * how Trump managed to get a record-setting one billion dollars of free media coverage;

  * why Trump succeeded where John McCain and Mitt Romney had failed; and

  * how Trump's populist, nationalist agenda is being implemented.

### 2. Trump’s two signature campaign issues managed to appeal to a divided America. 

On June 16, 2015, Donald Trump announced to the nation that he was running for president of the United States. But at the time, the nation was anything but united. And it hadn't been for a long time.

Trump's campaign took place in a divided America. Coastal liberal elites who had been enriching themselves off the back of globalization bookended an increasingly disenfranchised American heartland. Incomes in places like San Francisco and New York were skyrocketing, but middle-class wages in red states had been stagnating for decades. While coastal big tech, media and financial institutions hauled in trillions from their 7.4 billion potential global clients, America's interior had lost out on jobs that had been shipped overseas.

But as Obama pushed the nation farther and farther to the left, Republican challengers like McCain and Romney were unable to counter it — they themselves were perceived as part of the elite, and were unable to capture the imagination of America's white working-class interior. The problem wasn't that red-state white working classes were voting for Democrats; they simply weren't turning up in sufficient numbers to vote for uninspiring establishment Republicans.

Enter Donald Trump. Unlike previous Republicans, he wasn't afraid to throw a wrench into things and disrupt the establishment. And by doing so, he was able to motivate a starkly divided and previously apathetic America to get out and vote.

He did so by relentlessly focusing on two major issues from the launch of his campaign all the way through to his ongoing presidency.

The first is, of course, the idea that America doesn't win anymore. This was particularly the case with the seemingly unending and infinitely expensive wars that his predecessors had gotten involved in. Trump told middle America that while he would talk loudly and carry a big stick, he would only use his stick if he could win. Sure, he would "bomb the shit" out of ISIS and stand up to North Korea to keep Americans safe. But he would rather spend money on Americans who were hurting at home than on nation-building in Iraq or Afghanistan.

Secondly, Trump promised to protect American jobs by recalibrating the entire international economic order. While both parties had in the past supported globalization and free trade deals, Trump claimed that in doing so, they'd humiliated the American working classes whose jobs had been shipped overseas.

But perhaps even worse was that both Democrats and Republicans had allowed floods of cheap labor over the southern border to steal jobs away from Americans in their own country.

The simple solution that would energize his emerging base? Building a wall.

### 3. The combination of Trump’s policies with his personality were key to him winning over America. 

Trump's relentless plugging of his two campaign issues was not, in itself, enough to win over swing-state voters; it was also the fact that it wasn't a career politician like Jeb Bush, but Donald Trump — the vulgar, divisive and promiscuous entrepreneur, who was promoting these issues that helped him win over the electorate.

If candidates like McCain or Romney had been a balm to soothe conservatives, Trump was the chemotherapy needed to cure America's progressive cancer. In other words, Trump may have been bad, but sometimes you need something bad to fight off something far worse. In this case, that something far worse was the Washington swamp of deep-state bureaucrats and progressive globalists that Trump promised to drain. And in doing so, he claimed, he would emancipate the American worker from the intractable webs of globalist servitude.

But Trump still needed a strategy to get his message out there. Luckily, coming from his TV fame as the mercurial boss who would fire incompetent people on his weekly reality show "The Apprentice," Trump knew his way around media strategy. He'd perfected the show's crassness over the years, with, at one point, 30 million Americans tuning in to watch him hurl insults, trash-talk and single-handedly make decisions based on his gut. If this worked to increase TV ratings, why couldn't it work to increase his standing in the polls?

And that is precisely what happened. The potential "firer-in-chief" began a campaign based on fiery rhetoric, insulting America's allies and proclaiming policies without recourse to experts. For an America sick and tired of Washington deadlock under Obama, this was something new, even if it was crude at times.

Unlike establishment politicians who spoke in teleprompted cadences like Obama, or the robotic Marco Rubio, Trump always spoke in the same way to everyone, and in the same way he'd spoken on TV — he was lowbrow, crass and vulgar, with a rather limited vocabulary. Words like "huge" and "tremendous" and "winning" became synonymous with his personality and policies.

All of this worked just as Trump intended. The resulting media attention meant the whole nation was repeatedly subjected to his two campaign promises. The more incendiary his speech, the more the media would report on him. It was a win-win scenario — news outlets generated more money, and Trump more coverage. Trump barely even needed to tap into his own millions: it's estimated that he received one billion dollars' worth of free coverage during the campaign — more than any other candidate in history.

### 4. Inept and outdated Republican and Democratic ideas helped amplify Trump’s unorthodox message. 

Even with his bombastic rhetoric and media-savvy strategy in hand, Trump didn't enter the presidential race in a vacuum. There were other candidates, both Republican and Democrat, who stood in his way. Why weren't they able to impede the forward march of Trumpism?

Well, for starters, take the sorry state of the Democratic party. Their slow drift to the left under Obama had left the white working class behind. More regulation and taxes had forced industries and companies to slow down hiring and caused wages to stagnate. Meanwhile, low interest rates meant that middle-class savings accounts hardly grew.

Along came 2016 and the entry of socialist Bernie Sanders into the race, who pushed Clinton's agenda even farther left than Obama's. And beyond economic issues, there was also the issue of identity politics. Instead of supporting her for her policies, Democratic voters had been primed over eight years of supporting America's first black president simply to vote for Clinton because she would be the first woman president.

Things weren't much better on the Republican side. A strange paradox had appeared in the Republican party — during Obama's presidency, it had managed to capture both houses of Congress and block a Democratic Supreme Court nominee. And yet the party still hadn't managed to stop Obama from nationalizing healthcare, raising taxes, and rewriting immigration law.

The situation with the party's presidential candidates wasn't much better. John McCain was a proponent of open borders, allowing in cheap labor, and was big on nation-building in places like the Middle East. As for Mitt Romney, he had set up a progressive, state-run healthcare system similar to Obamacare while governor of Massachusetts. All in all, the Republicans were failing because they were promoting policies that favored the very rich and very poor, all the while leaving America's floundering working- and middle-class majority to fend for itself.

Even though the 2016 Republican field was touted by the media as being one of the best groups of potential nominees in recent memory, policy-wise it was more of the same — unfettered free trade, military adventurism overseas and immigration reform that would allow in guest workers and grant amnesty to illegals.

Facing the choice of either progressive or progressive-lite, much of the electorate, tired of the status quo, saw Trump's entry into the race as a political outsider as offering them something altogether new.

### 5. Trump’s populism and Clinton’s corrupt insiderism created the conditions for his 2016 win. 

By the time the nominees for both parties had been selected, Trump had managed to simplify his message down to two main talking points: making America great again, and making sure the corrupt insider Clinton never got elected.

Polls showed that 56 percent of Americans believed that since 9/11, the "power and prestige" of the nation had declined. What's worse was that only ten percent believed the reverse. It was therefore no surprise that Trump's warnings of continued American decline resonated with a huge portion of the electorate. What's even less of a surprise was that people flocked to his promise to reverse this decline — and make America great again.

On the economic side, Trump complained of a rigged economy that benefited the few over the many, as did socialist Bernie Sanders. But, unlike Sanders, Trump didn't combine that with a pessimistic outlook based on fear-mongering over environmental collapse. Instead of calling for action against climate change, Trump called for more consumption and more domestic resource extraction. This tied in with his nationalist perspective — why should America rely on foreign energy when it had the resources to be self-sufficient?

As Trump continued to hammer home his message of making America great again, he also countered attacks from Clinton at every turn. For Trump, this was easy — Clinton made him out to be a sinner, and herself a saint. This strategy was ineffective against Trump. Everyone knew Trump was a sinner. But saints, on the other hand, aren't meant to sin — so when Trump made Clinton look like a sinner, she wore it badly.

Time and time again, Trump was able to deflect the sins he was accused of onto Clinton. When Clinton hammered him on his tax returns, he hit back with accusations of corruption and negligence over her destruction of 30,000 emails while Secretary of State. When she pointed the finger at Russia and its possible collusion with the Trump campaign, this also proved a win for Trump — it was, after all, Clinton, not Trump, who had infamously pushed the red "reset" button with Russian foreign minister Sergey Lavrov in 2009. And it was Clinton, not Trump, who had appeased an aggressive Russian foreign policy after Russia invaded Ukraine in 2014.

Trump couldn't have asked for a better opponent. After all, it was much easier to run against "Crooked Hillary Clinton," as Trump often called her, than Bernie Sanders — a politician whose hands were relatively clean.

### 6. Trump has been unfairly held to much higher standards than other modern presidents. 

On January 5, 2018, Michael Wolff's book _Fire and Fury_ was released. For the first few months of the year, mainstream media outlets could not get enough of its lurid stories of the Trump campaign and White House.

As the media reported story after story of the supposedly inept and dysfunctional White House, the economy was painting a different picture. By late 2018, the stock market was hitting record highs, GDP growth was at its highest point in a decade, and unemployment figures were at their lowest point in the twenty-first century. Additionally, America's place in the world was being successfully recalibrated — the Iran nuclear deal was no more, and America had recognized Jerusalem as Israel's capital.

But instead of reporting on Trump's successes, all the media cared about were the stories coming out of bestselling supposed "insider accounts." Compare that to the aftermath of Edward Klein's 2012 presidential gossip volume, "The Amateur," in which, for example, he claimed that Obama had bribed Reverend Jeremiah Wright, pastor of Obama's local church, to stop delivering anti-white sermons until after the 2008 election. One would think that would've generated national headlines, but it was all but ignored by the press.

The fact of the matter is that Trump has been subjected to the most biased and unfair media coverage of any modern president. This is the same media that still fawns over the Clinton years, and the same liberal elites who hold JFK and FDR in high regard.

FDR, president from 1933 to 1945, was able to get away with having his daughter arrange for him to have secret affairs without the media getting wind of it. But this has been conveniently forgotten by most, as has the infamous Clinton and the Lewinsky scandal.

Now, all of this doesn't mean that Trump is an angel. Quite the contrary. But the tenacity with which the mainstream media has attacked him is unrivalled in recent times. Eighty percent of evaluations of Trump coming from major liberal outlets such as the _New York Times_, CNN, and even the BBC were negative during his first hundred days, and three times as negative as Obama's coverage during the same honeymoon period in 2009.

It's no surprise, then, that as of April 2018, 77 percent of Americans polled by Monmouth University agreed that the mainstream media only offers "fake news."

### 7. Against all odds, President Trump has achieved a lot, both domestically and internationally. 

The very thought of Trump being remembered as a good president probably makes half of Americans laugh out loud. But once one goes beyond the media frenzy and looks at his accomplishments, the story begins to change.

It's undeniable that the first year of Trump's presidency was difficult. Relentless attacks from the mainstream media and a revolving door for White House staff mired the administration in drama, making the task of governing difficult.

Polling supported this view, with one poll in early 2017 showing that only 34 percent of those surveyed viewed Trump as honest, while 59 percent thought he didn't possess leadership skills.

But the drama of Trump's first year seems to have been worth it. He now has a close-knit team that he trusts, and the White House turnover rate has decreased substantially.

And by summer 2018, his legislative, economic and international success had pushed many of his approval polls into the 50 percent range.

Consider the domestic economy. In the year and a half leading up to summer 2018, GDP growth was faster than in any period since 2009, even hitting a whopping 4.1 percent in the second quarter of 2018. And unemployment indicators painted a similar picture, hitting 3.9 percent, a record not seen since the Clinton administration.

Internationally, things weren't much different. Trump put a check on Iran's nuclear program and growing military influence by ripping up Obama's nuclear deal. And even though Trump exited the Paris Climate Accord, America still managed to lower its carbon monoxide emissions by two percent in 2017 due to increased fracking. The EU, on the other hand, increased theirs by 1.6 percent in the same period!

Of course, a lot still has to be done. Trump's wall has yet to be built, and illegal immigration has not radically decreased. Guest workers are still taking American jobs, children of illegal immigrants are still not being deported, and Obamacare has only been partially dismantled.

But if the 2018 midterms are anything to go by, Trump still has a lot of time to enact the rest of his agenda. It's normal for sitting presidents to lose seats in Congress during midterms. But Republicans did comparatively well in 2018, losing fewer seats than during midterms at the same points under the presidencies of Clinton or Obama.

And as both of them went on to win reelection, it's altogether likely Trump isn't going anywhere for the next six years.

### 8. Final summary 

The key message in these blinks:

**If candidates like John McCain and Mitt Romney were an aspirin given to treat the symptoms of American decline, then Trump is the aggressive chemotherapy meant to cure the illness behind that decline. Trump harnessed, often in a vulgar and belligerent way, a nationalistic economic populism to take on — and beat — the establishment veteran Hillary Clinton. Since his victory, Trump's radical approach to American politics has shaken up the system and ushered the country into an age of Trumpism. But even in the face of constant attacks by the media, Trump has managed to steer America into an age of increasing economic prosperity.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Liars, Leakers, and Liberals_** **, by Judge Jeanine Pirro**

From Hanson's argument, we've seen how Trump managed to craft a winning strategy that propelled him into the nation's highest political office, despite heavy criticism and insults from a vast majority of mainstream media.

For more on the practices being used by the president's opposers, check out our blinks to _Liars, Leakers, and Liberals_. In it, Fox News host and conservative journalist Judge Jeanine Pirro unpacks what she calls the web of conspiracies and lies being woven by Trump's enemies in their attempts to put an end to his presidency.
---

### Victor Davis Hanson

Victor Davis Hanson is an American historian and author. He is professor emeritus at California State University, Fresno, and the Martin and Illie Anderson Senior Fellow in Classics and Military History at Stanford University's Hoover Institution. He is a recipient of the National Humanities Medal, and is the author of _The Second World Wars: How the First Global Conflict Was Fought and Won._

