---
id: 54b5b2833262620009140000
slug: the-challenger-sale-en
published_date: 2015-01-15T00:00:00.000+00:00
author: Matthew Dixon and Brent Adamson
title: The Challenger Sale
subtitle: Taking Control of the Customer Conversation
main_color: D7352B
text_color: BF2F26
---

# The Challenger Sale

_Taking Control of the Customer Conversation_

**Matthew Dixon and Brent Adamson**

Sales strategies have changed. Instead of shilling "one-size-fits-all" products, today's top sales reps excel by providing a customized solution to a unique problem. To do this, these sales reps follow the "challenger" selling model. In this book you'll learn what this model is and how it can revolutionize your sales organization.

---
### 1. What’s in it for me? Learn the most effective, highest performing approach to selling. 

"ABC: Always Be Closing" is a great line from the movie _Glengarry Glen Ross_, but no matter what Hollywood tells you, it's not actually the best approach to sales.

The cliche of the oil-tongued salesman with a pricey suit is seriously old school. Today's salespeople achieve success in a very different fashion.

In these blinks, you'll explore research that authors Matthew Dixon and Brent Adamson have unearthed to discover today's effective salesman: the challenger. This is a person who isn't afraid to take control, winning sales through _teaching_ and not through _persuading_.

It's a huge paradigm shift in the world of sales. You'll learn quickly why the challenger technique is so effective and how you or your sales staff can become challengers too.

In the following blinks, you'll discover

  * how a "challenger" salesperson embodies all the needed selling approaches;

  * how to guide a conversation toward a sale without actually pitching a product; and

  * why the most effective salesperson is disproportionately a "challenger" type.

### 2. Effective sellers involve the customer and personalize the product. 

When you think about sales, what comes to mind? Perhaps slick, fast-talking sales reps closing deals?

But why is it that we only ever think about the sales rep, and never about the customer?

Sales isn't a one-way conversation. In fact, the most effective sales techniques put the customer first.

_Solution selling_ is one such technique. It involves selling customizable products and services as a solution to a specific problem, rather than selling a "one-size-fits-all" product.

For instance, a car manufacturer wouldn't try to sell just _any_ car to a bakery; instead, a salesman would pitch a delivery truck customized to keep fresh bread warm. In other words, the car manufacturer would sell a _solution_ to the bakery's concerns of delivering stale bread.

So why is this approach so effective? Solution selling helps the seller differentiate himself from competitors as well as control prices.

Look at it this way: The car manufacturer doesn't have to worry that his competitors also sell a very similar kind of truck. Also, there's no need to compete by lowering prices. After all, price is only part of the solution on offer; there's also customization.

Solution selling does have its advantages. But it's also more challenging than other approaches, because it empowers customers to be more demanding, and also requires salespeople to find creative ways to ensure customer satisfaction.

After all, a solution seller's job is all about solving the customer's specific problem, not foisting a ready-made package upon them.

In other words, solution selling requires sales reps to do more than pitch. They have to work closely with the customer, finding out how her business works and researching every detail to offer a good solution.

### 3. When it comes to solution selling, innovative sales reps who are “challengers” close deals. 

Solution selling is an effective approach, as it is essentially a dynamic interaction between sales reps and customers.

And yet, it's important to realize that not all sales deals are conducive to this technique.

After conducting a study based on 90 companies around the world, the authors discovered that there are essentially five types of sales representatives.

First, there's the _hard worker_. These motivated overachievers put in twice the effort of everyone else on their team.

Second, there's the _relationship builder._ Essentially "nice guys," these salespeople are always there for their customers, their colleagues — basically, everybody. They strive for success through quality relationships.

Third, there's the _lone wolf._ This salesperson is one who never follows protocol, never writes a report, and won't or can't get on the team spirit bandwagon. In fact, you would probably fire him if he weren't so darned good at his job.

Fourth, the _reactive problem-solvers_. These employees are more customer-oriented than sales-oriented, but they're a lot less snuggly than the relationship builders.

And finally, the _challenger._ Such salespeople have a deep knowledge of the customer, and enjoy debating details and pushing the envelope with innovative ideas and insights.

Of these five types of salesperson, the challengers are undisputably the ones mostly likely to thrive in a solution-selling environment. Although you can find average-performing sales reps among all of the "types," almost 40 percent of the top sellers come from the challenger ranks.

What's more, this jumps to over 50 percent when you look only at solution selling. In other words, a challenger is the best kind of sales rep when it comes to offering customers a solution.

Let's take a closer look at what it is that makes challengers so special.

### 4. Challenger salespeople are teachers at the core; they shine light on solutions for the prospect. 

What makes a challenger so effective at solution sales? One major aspect of the challenger technique comes down to _teaching_.

And why is this quality so important? Let's first take a step back.

Many companies don't realize that the average customer can't differentiate between various products and brands. So when it comes to making a purchasing decision, sales are often the deciding factor.

For example, you might sincerely love pitching a car that uses one-percent less fuel than a competing product, but your customer might not appreciate that feature to same extent. However, your customer probably does care about the sales experience.

In fact, the authors realized this when they conducted a study of over 5,000 customers. Their research showed that 53 percent of customer loyalty depends on the sales experience. That's _huge_!

So since the sales experience is so important, what can you do to deliver a good one?

Many salespeople think that it comes down to knowing the customer's business very well. And although you'll definitely need to know your way around — especially if your goal is to deliver a solution — a salesperson's ability to teach is what _really_ makes the sales experience valuable for a customer.

Sure, showing your prospect you understand their problems will demonstrate your empathy, but not much else. Because ultimately, empathizing doesn't concretely help your customer.

But here's what does help them: when you know so much about their business that you can teach them something. Being able to deliver this experience is precisely what wins a challenger so many deals.

For instance, of course the baker would be pleased if you rattled of his entire product list and sympathized with his early wake-up time.

But here's what will _really_ help him: If you can explain why some breads can't survive a five-mile transport without falling apart, and how your product can help solve this problem!

> _"Over half of customer loyalty is a result not of what you sell, but how you sell."_

### 5. A challenger controls the conversation and structures it skillfully to guide a customer to a solution. 

So challengers succeed by _teaching_ a customer something about their business.

Does that sound tough? It's not, and really comes down to thorough preparation, as you need to construct the conversation in a certain way.

This is best achieved by mastering _commercial teaching_, which is all about teaching things that connect to your company's strengths. For instance, imagine a sales rep from an office furniture company meeting with someone whose company has just built a new office building.

A good sales conversation between them would follow six steps.

First, as a sales rep, you'd _build your credibility through a hypothesis about the customer's business._ You might point out that many of the new offices don't seem to be designed to encourage collaboration.

Next, you'd _reframe the problem or insight from a different perspective._ You'd point out that effective collaboration happens in small groups of three to four people. Yet these new meeting rooms were designed for eight people!

After this, you'd explain _why exactly the issue at hand is bigger than the customer may know._ You'd share information with the customer, case studies that prove large conference rooms kill innovation.

Then you d _emonstrate how the problem impacts the customer's business._ You'd point out how when employees can't collaborate, company innovation stalls.

Here is where, as a challenger, you separate yourself from the sales pack. Now you _offer something new to improve the client's current situation._ Perhaps you say, "You could divide one large conference room into two, smaller spaces."

And you have the solution, of course, at hand — yet you _only offer your solution at the very end of the conversation._ By now the prospect is convinced! They want to improve collaboration by splitting the conference rooms, but they don't know how; luckily, you're selling movable walls.

In other words, effective sales isn't just about pitching a product. It's about teaching and changing the perspective of your potential customer.

> _"Before they buy your solution, the customer has to buy the solution."_

### 6. It’s crucial to make your solution attractive to every individual involved in the decision. 

As we mentioned earlier, solution selling involves the customer a lot more than other techniques. Challengers excel at this; challengers know how to work _with_ the customer.

Of course, with multiple stakeholders and conflicting opinions, this can get complicated.

How complicated? Well, the authors' research found that decision makers who actually close deals (senior executives, for example) mostly care that other employees and team members support the deal. And team members, in turn, care about learning something from the sales rep.

That means you need to forget about convincing the big boss about your great product. Instead, take the time to convince her _team_ that they'll be better off if she buys the product.

To this end, it's important to _tailor_ your message to get support from different stakeholders. To clarify, _tailoring_ means matching your solution to the specific needs of the prospect, their industry and their company.

Because ultimately, you can't feed every single person the same pitch and expect them to get excited about your product. To get people excited, think about what they care about. What are their economic goals? What do they want to achieve?

For example, a CEO might care most about cost savings while a human resources department head is more concerned with employee satisfaction. And even if the CEO is ultimately signing the deal, you might still have to tailor your message to satisfy HR first.

So you don't want to waste time rambling about the versatility of movable walls when you're talking to an HR rep. Instead, teach him that collaboration happens in small groups of people and that using movable walls to divide the conference rooms will improve collaboration and innovation.

And then, tailor the whole message in a way that underlines the fact that collaboration reduces stress and makes employees happy.

> _"No one ever sold anything off a spreadsheet alone."_

### 7. Don’t shy away from the steering wheel; a challenger takes control of a sale from the very start. 

Challengers also excel at _taking control of the sale,_ from beginning to end.

This means that they're comfortable enough with what they're offering to stand firm and insist that the prospect sees the value of the solution.

This is important, as sales reps in general aren't comfortable taking control. In fact, most reps don't like talking about money or pushing the client to make a final decision.

Shyness isn't the only factor at play in this behavior. Actually, according to a study conducted by BayGroup International, 75 percent of sales reps feel that procurement departments (which buy outside goods and services) have more power in a bargaining situation.

But interestingly, the survey also found that procurement staffers thought sales reps were more influential!

Knowing this, perhaps it will be easier for you to take control of the sale from the start. Don't wait until you sit down at the final stage to discuss money matters; instead, take control from the very beginning.

For instance, you should always demand to see a certain amount of interest from the prospective customer — by meeting with senior stakeholders, for example.

This way you'll avoid finding yourself in a situation in which the prospect has already made up their mind to go with a competitor. In such cases, you'll be put in a meeting with junior executives, just because the company is curious about what else is on the market.

So to avoid wasting time and money researching the company and preparing a good teaching conversation, ask when a senior stakeholder will get involved. And if you don't get a clear answer, you might want to start looking for a new prospect instead.

And if this all seems like charm and magic to you, keep reading! We'll show you how to train even the shyest sales rep to make them a challenger, too.

### 8. Share knowledge and solutions with your entire sales team, so you all can win together. 

If you're a sales manager, you might be wondering just how you can turn your sales reps into real challengers.

Although some people pick up the challenger model instinctively, the ways to teach this model to individuals are also scalable throughout an organization. Here are three methods!

First, to help foster teaching conversations, make sure as much information as possible about different prospect industries and companies is available for your reps.

Tailoring can also be taught. It's easy to make a "cheat sheet," explaining objectives for different positions in specific industries. While young reps might not know instinctively the different ways to approach a human resources executive and an accounting rep, they can certainly learn.

And finally, you can teach the part of the challenger model that seems most closely connected with personal charm: taking control. While some sales reps can be passive, ready to give a customer anything she wants to just to get a deal signed, you can reverse this behavior.

For instance, you can guide reps to alter their sales channels and teach them to change course if a senior executive doesn't get involved in the conversation at a certain point.

Ultimately, your company is in the best position to teach a sales rep how to be a challenger. After all, the company over time has gathered useful information from dozens of individual sales. And since you have access to this treasure trove, you should share what works, and what doesn't.

For instance, how could a sales rep for a furniture company know how to approach a fashion boutique, for example, when all he's dealt with in the past has been technology stores?

As the manager, since you have the company's sales history at your fingertips, you can easily help your reps connect the dots and rethink what approach would work best in each situation.

In other words, if you already have the answers, make sure your reps do too!

### 9. If you’re ready to make your company a challenger shop, make sure your managers are on board. 

Here's the last key piece of advice to turning your sales team into a team of challengers: Managers are important.

In fact, you can't do _anything_ without your managers being on board.

As a business leader, you may make as many strategy decisions as you like, but at the end of the day, someone in your company has to execute them.

And you can't expect the CEO or head of sales to go to every single sales rep to coach them on challenger selling, and then observe each selling opportunity to make sure they follow the model.

Managers, however, _can_ and should do that. Thus you won't be able to execute the challenger sales model across your organization without first convincing your managers that it's a good idea.

To that end, if you don't convince your managers of the strategy, they could very likely undermine your efforts. No matter how much time you spend coaching sales reps, for instance, it won't do any good if a manager then turns around and says a rep is spending too much time researching a prospect's business model.

So, since you can't achieve anything without good managers, how can you find managers who will appreciate the challenger selling model and work toward it?

Well, look for individuals with certain fundamental qualities (like integrity and reliability) as well as a good understanding of sales.

It's also worth noting that these managers don't necessarily need years and years of challenger sales experience; they just have to be able to encourage challengers and not stand in their way.

And once you have the right managers, you can overhaul your company's sales strategy and start winning more customers!

### 10. Final summary 

The key message in this book:

**Selling today is all about offering a customized solution to address your customer's unique problem. The best way to do this is to learn valuable knowledge about your customer's industry, tailor it to each person involved in the buying decision and take control of the conversation from beginning to end. In other words, you need to be a challenger!**

Actionable advice:

**When you're preparing to have a teaching conversation, pay attention to the unique aspects of your product others don't fully appreciate.**

Don't pay attention to attractive features or services, or elements that make your solution a "good one," in a conventional sense. Instead, focus on a particular quality that you think is unique, but that others don't fully appreciate.

And as soon as you figure out what those qualities are, try to figure out why other people don't value them as much as you do. Ask yourself: Is there a way you can reframe these qualities for the customer to show her why they're so relevant and special?

**Suggested further reading:** ** _To Sell Is Human_** **by Daniel Pink**

_To Sell Is Human_ explains how selling has become an important part of almost every job, and equips the reader with tools and techniques to be more effective at persuading others.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Matthew Dixon and Brent Adamson

Matthew Dixon is the executive director at CEB, the world's leading member-based consulting company. Brent Adamson is the firm's managing director and chief storyteller.

