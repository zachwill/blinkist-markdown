---
id: 55c0f3e53439630007a30000
slug: the-change-masters-en
published_date: 2015-08-06T00:00:00.000+00:00
author: Rosabeth Moss Kanter
title: The Change Masters
subtitle: Innovation and Entrepreneurship in the American Corporation
main_color: 8F8B72
text_color: 73705B
---

# The Change Masters

_Innovation and Entrepreneurship in the American Corporation_

**Rosabeth Moss Kanter**

_The Change Masters_ (1983) is about Rosabeth Moss Kanter's findings from her extensive research on American corporations in the 1980s. She identifies the key factors that bring about change and innovation, and explains how you can structure your organization to adapt to change more effectively.

---
### 1. What’s in it for me? Discover what makes a company truly open to change. 

Resting on your laurels has never been more dangerous. In today's world, with rapidly changing markets and fierce global competition, only companies that can adapt to new realities and embrace change will survive.

But how do you lay the foundation for a truly innovative, adaptable company?

Rosabeth Moss Kanter has spent many years researching American Fortune 500 companies to determine the factors that contribute to a company's ability to change. As these blinks will show you, good communication, fluid job titles and cross-departmental collaborations are just some of the things that will make sure your company stays on the cutting edge of innovation.

In these blinks, you'll learn

  * why good communication is essential for change;

  * what loading guns has to do with innovation; and

  * why your project team needs to be protected from the outside world.

### 2. Collaboration and innovation are often stifled by success formulas in traditional organizations. 

Globalization has dramatically changed the rules of business in recent years. Traditionally, most industries were dominated by a few big corporations and markets were relatively stable. Today's businesses have to compete globally and survive in rapidly changing, volatile markets. Many organizations are failing to keep up. Why?

One reason is that traditional organizations are often held back by their _success formulas_.

When companies overcome their first set of challenges, they typically develop a success formula that outlines the way the organization should be run. It can address issues like production techniques or the company's structure. The point is that since the formula worked for the business in the past, it becomes the standard protocol. This can seriously impede innovation.

The author conducted research on a textile manufacturer that struggled with just this issue. The company had experienced problems with yarn breaking since it first went into operation, so the owners considered this to be a normal cost of the business and didn't try to fix it.

Then a new plant manager joined the company. He worked to improve employee communication and eventually met a worker who had an idea for modifying the production technique to reduce breakage. He'd had the idea for 32 years, but he never shared it until he was asked.

When the new manager asked the employee why he hadn't spoken up before, he said that the old manager simply hadn't been interested in changing the process. This shows how getting too comfortable in your routine can be detrimental.

Change-averse organizations that cling to the status quo and don't promote collaboration are called _segmentalist_. In the modern business world, segmentalism is the main factor that determines whether a company adapts and thrives, or stagnates and fails.

### 3. Segmentalist organizations stifle innovation by keeping their employees in strictly defined roles. 

The author identified several behaviors that hinder innovation and change as she conducted her research on modern-day businesses. She termed these behaviors _segmentalism_.

But what exactly is segmentalist behavior and why does it stifle innovation?

Well, segmentalist organizations make it difficult for people to solve problems in creative ways. Even if the employees have valuable ideas, they're held back by the company's structure. The employees are confined to their narrowly defined roles in the business, so they don't have the opportunity or motivation to share their ideas.

True innovation almost never surfaces within the bounds of one job or even one department. If you want to foster creativity, allow people to step outside of their usual roles and collaborate with others.

When segmentalist organizations keep their employees in strictly defined roles, they smother innovation before it even begins. These organizations tend to be unnecessarily hierarchical and draw clear lines between departments to keep employees in their place.

Sometimes segmentalist organizations will even encourage their departments to compete with one another, forcing their managers to hoard resources and guard their territory.

As an example, one manufacturing company that the author visited needed to train their line workers; a manager found that she could train them more quickly and cheaply if she had a computer. However, she couldn't obtain a computer without going through a lot of bureaucracy, and no other manager wanted to take time away from their usual job and approve the funds for it.

Meanwhile, mid-level managers from the IT department wouldn't help either. In fact, they even wanted to _prevent_ her from getting a computer, because they feared it would threaten their roles and responsibilities.

The manufacturing company in this example was highly segmentalist. If you want your organization to survive, be _integrative_ instead.

### 4. Integrative companies have complex organizational structures that foster collaboration. 

We've seen that an essential part of innovation is allowing people the freedom to step outside their usual roles. But why is this freedom so essential, and how do integrative organizations foster it?

Integrative companies use complex organizational structures that encourage collaboration.

Innovation actually requires quite a bit of complexity. If you want to foster innovation in your company, employees need access to more information and teams need to be more interconnected. People should have several ways of pooling together human and material resources.

True innovation arises when people approach problems from different angles. People with different mindsets and areas of expertise need to come together to be truly creative.

One way to achieve this is with a _matrix organization_. In a matrix organization, different departments and processes are all vertically integrated.

Product design, production and sales, for example, are departments that are usually managed separately, but they're all united by a common goal: creating products that sell well. So in a matrix organization, these departments are connected in terms of their management, responsibilities and rewards. In other words, they're forced to collaborate.

But company structure isn't the only key to fostering innovation. Employees also need broad, open-ended project assignments, which give them the space to decide for themselves how they will reach their goals. This sort of freedom also encourages creativity.

The author researched a medical company that understood this point well. One of their manufacturing managers had been tasked with designing a new kind of machine. Upper management gave him a rough budget, but few other constraints — he was allowed to design the device on his own.

He had the freedom to select his own team members and push forward his own ideas. He also got input from the manufacturing and design departments. Eventually, his design went into production and it sold very well.

### 5. Information, resources and support are crucial for innovation. 

Innovation requires a certain degree of complexity and freedom, but that's not all you need. There are three _basic commodities_ that are necessary for innovation: _information_, _resources_ and _support_.

Important changes can only come about with a free flow of information. Innovative ideas emerge when people approach problems from new perspectives, so it's vital that they have access to multiple sources of information.

The most innovative companies the author examined were those that maintained a _face-to-face information culture_.

Companies like Chipco and Wang Laboratories, for example, use _open communication systems_ across their entire organization, meaning that people at any level of the company hierarchy can suggest new ideas or offer criticism.

Integrative organizations make it easier for their employees to obtain important resources. 3M, for instance, has _innovation banks_ — funds for projects that employees can request without having to go through too much bureaucracy.

People also need to be supported — with endorsements or approval — when they pursue their projects. A good way to do this is to rotate your employees through different jobs, as it allows them to establish more personal relationships and can improve support structures among colleagues.

Within more entrepreneurial companies, it's normal for managers to make career moves that seem unusual. They might transfer from finance to manufacturing or HR, even if they don't have any formal training in these new areas.

This rotation of roles is useful because it forces people from different perspectives to address new kinds of problems, which, in turn, often results in innovation. It also allows employees to build a network that stretches through different sections of the organization, making it easier for them to call on their peers when they need additional information or resources.

Information, resources and support — _power_, in other words — are thus vital components of the creative process. Too much _power circulation_, however, can be counterproductive.

### 6. Too much power circulation is harmful. 

An organization needs some degree of power circulation to allow for creativity, but you _can_ have too much of a good thing. Without some limits on power circulation, employees might simply jump from project to project without staying on one long enough to finish it. It's important to balance power circulation with focus, but how?

An effective organization fosters the development of innovative ideas, then steers them in the right direction. Power circulation (through interconnectivity, complexity, freedom, diversity and so on) is necessary at first because it gives people the space to be creative. But later on, that same freedom can present problems.

Too much freedom makes it difficult to agree on which proposals should be implemented, how they should be executed and by whom. That's why a greater concentration of power (through formalization, clear procedures and allocated funding) can actually bring a project to life.

An organization certainly can't shift from power circulation to focused power at the drop of a hat; the best solution is to never fully disperse power, but to temporarily loosen it instead. This way, your employees enjoy autonomy, your organization won't fall victim to segmentalism and you'll still have an overarching structure for making strategic decisions.

The headquarters of General Electric Medical Systems, for instance, serves as the central entity that oversees all of the company's important projects. There's enough decentralized power to allow for ideas and projects to develop on their own, but the headquarters retains its control over large expenditures and determines the company's overall strategy. They also give additional support to especially important projects.

In the end, your organization needs to have enough decentralized power and creative complexity to foster creativity, but it also needs focused upper management that can drive its projects home.

> _"An organization has to constantly balance the circulation and the concentration of power."_

### 7. Before you start your project, gather information, plant your seeds and ask for support. 

Even if an organization has provided its employees with sufficient information, resources and support, it still needs to ensure that their innovative ideas come to life. How?

The first step toward implementing an idea is to collect enough information for a feasible and goal-oriented project definition.

The author found that most project ideas emerged when people gathered information and ideas from within their job's "neighborhood." Hewlett Packard calls this _management by walking around_. It's when managers leave their offices to collect information and spread their ideas — or _plant their seeds_ — so that others might help develop them.

This is also an important part of building the company's network. A network of supporters helps mobilize the resources that projects need.

Ultimately, you need people to _buy in_ or _sign on_ to your project by offering their time, money, information or influence. So the first step is usually to _clear the investment,_ which means getting some sort of approval from your boss to seek wider support elsewhere in the organization.

It's also a good idea to get some _cheerleaders_. This means making sure you have support from team members in lower-ranking positions than your own. Cheerleaders are important because executives are more likely to support your ideas when more people are backing you.

At General Electric, for example, cheerleading is called _loading the gun,_ and your peers are considered your ammunition _._ Another computer firm called it _tin-cupping,_ because you're asking people to unselfishly help you out.

After you've got enough resources and support to start your project, there's still work to be done: now it's time to focus on maintaining your momentum and driving the project home.

> _**"** Organizational genius is ten percent inspiration and 90 percent acquisition."_

### 8. Don't let your team members get distracted by external problems or noise. 

There's another important truth that the author uncovered in her research: projects often fall apart over time, even if they start with the right information, resources and support.

This is often caused by a lack of focus or external pressure from the rest of the organization. As a project manager, you need to look out for exactly these sorts of distractions.

Criticism from the rest of the company can hurt your team's confidence and focus.

When a project gathers momentum, it will usually get more attention from the rest of the organization. Other departments and people that aren't involved might start to interfere by voicing their criticism or getting involved in the project's bureaucracy. The project manager has to stay focused on PR and internal company politics, leaving the other details to the team.

There are a few ways to deal with unnecessary criticism. Sometimes you can just ignore it and wait it out. You can also brush it aside by reminding people of how important it is to stay focused on the company's overall goals or mission.

A Google project manager, for example, might defend their work by saying, "This project is directly connected to our highest mantra: don't be evil." It's harder to be critical when the project is presented as a reflection of company values.

Remember that it's your job to deal with criticism, not your team members' — their job is to focus on the project.

Tom West, a project team leader from Data General, understood this when he managed a project to design a new computer. He deliberately kept his team unaware of the criticism and interference coming from other parts of the company.

West and his managers had an unspoken agreement: "We won't pass on the garbage and the politics." They didn't want anything to distract their team.

### 9. Use team-building skills to maintain your project's momentum. 

When projects run for a long time, team members can start to lose motivation, especially if they have other responsibilities to attend to as well. If a project drags on, team members might even start to resent working on it. They may not put in their best effort, or might even try to kill the project internally. As the team leader, it's your job to prevent this.

Preserve your project's momentum by using team-building skills. Your team members won't stay motivated if they don't feel involved, or feel undervalued.

It's a good idea to have team members meet regularly. This also ensures that everyone stays up to date on the project's developments.

One manager at a computer chip company, for instance, noticed that his team was dragging their feet slightly. So he started holding regular meetings with the core members, sending status reports to all the stakeholders and giving frequent presentations about the project to the upper management. This reminded everyone of how important the project was, and the continuous flow of information kept everyone interested — they wanted to see what would happen next.

You can also prevent things from slipping into a boring routine by having your executive supporters (or bosses) come by, even if it's just to remind everyone of what they're working on. Bosses can also send everyone updates about the project.

Finally, be sure to share the project's rewards with your team members. Innovative projects tend to get attention from upper management and can serve as career boosts. So praise your team's work in meetings and mention it to your superiors — it might help your team members' careers and could also make them much more enthusiastic about the project.

### 10. Teamwork isn't always necessary and can even be counterproductive. 

Getting employees to actively participate in projects is a crucial attribute of innovative organizations. Unfortunately, participation and teamwork can't solve all corporate problems on their own.

It's certainly possible to foster too much teamwork and hold too many meetings. And doing so can make simple tasks very time consuming.

For example, the author studied a company that used the term _task force_ to define the company's policy on participation. The employees had to form task forces to address even the smallest of issues.

This meant the team members were subjected to so many meetings that the conference rooms were always booked and the offices seemed empty. Understandably, the employees came to hate the task forces, and everyone just wanted to be left to work on their own.

Sometimes unilateral decisions are more effective than group participation.

The author found that teams function best when they're tasked with staying ahead of change, whether that's examining issues in new ways, providing each other with new kinds of assistance or discussing new ideas.

Naturally, employee participation is not always appropriate.

If one person clearly has more expertise on a given topic than everyone else, and the others accept that expertise as valid, there's no need for additional deliberations when making a decision. So if there's only one biochemist and they say, "This substance is poisonous — we need something else," you don't need a team to evaluate their statement.

Participation is also unnecessary if the participants have nothing to contribute or aren't interested. When the project has very limited time or the employees are more productive when they're on their own, teamwork can actually be counterproductive.

It's not always easy to know when group participation will be helpful or harmful. Remember that teamwork is just another tool — you don't always need to use it.

> _"Masters of change are also masters of the use of participation!"_

### 11. A company can't change unless the employees truly believe that change is possible. 

Changing an entire organization is a change master's greatest skill. You have to promote participation and power circulation, as well as make resources available, but that's not all. The _corporate culture_, that is, your company's mindset, has to be ready for change too.

The vice president of General Electric once said that if your company isn't fully prepared for innovation, your ideas won't break into the market, even if they're great. Your organization has to be ready to adjust to the changes that innovation entails.

If the people in your company don't believe that change is possible, they won't be motivated to work toward it — if people don't trust that change can actually come about, they won't participate.

The architecture of change requires a solid foundation, and a history of change within a company is a key factor in making future changes possible. If a company's structure doesn't support the new project, its foundation has to be changed first — companies need a strong foundation in order to support innovation.

A plant manager at Honeywell, for example, was struggling to get his employees interested in quality circles, groups of workers responsible for reviewing and discussing quality standards in a department. He looked into the company history and found that the previous management had broken a lot of promises regarding change. They had even failed to fix the ventilation and reduce noise within the plant.

The manager's first step was make good on the old promises, which made the employees realize that change was actually possible within the company. He earned their trust and they took him seriously when he started implementing his own changes.

Earning your employees' trust is the first step toward getting your company ready for change.

### 12. The way a company tells its story affects its attitude toward change. 

The author uncovered an interesting fact about change-averse companies during her research: they interpret their own company history differently than companies that embrace change.

Change-averse companies tend to insist that they haven't changed much since day one. Organizations that embrace change, on the other hand, openly stress the changes they've undergone. Why?

Well, when companies emphasize the changes in their past, employees are more prepared for changes in the future. They see change in a positive light, as they'll understand that it's a necessary aspect of doing business. They'll also be more comfortable suggesting their own ideas on how to change things.

When certain parts of a company's history of change are distorted, this also affects the company culture.

Company history is usually airbrushed to an extent. For example, if a project is successful, top management might act like they supported it all along, even if it was controversial or faced a lot of opposition in the beginning.

In reality, there's almost always some disagreement about a project before it starts. Usually, it's only a small group of creative people that push a project forward until it can get enough support to take off.

It's harmful to alter these company stories because doing so promotes a false understanding of change. Moreover, it discourages the employees from working hard to push forth their ideas. They might give up when they run into their first obstacle because they'll assume great projects are always accepted right away.

That's why integrative companies that truly embrace change are always up-front about the changes in their past. Positive stories about change can inspire people to work toward more changes in the future.

> _"The organization's culture influences the stories that must be constructed for continuous change to happen."_

### 13. Final summary 

The key message in this book:

**Innovation requires decentralized power and more freedom for employees. Have your employees step outside their usual roles, while promoting openness and being honest about your company's history. Even if your employees have great ideas, they won't make a difference if your organization isn't structured to help them emerge.**

Actionable advice:

**An overseeing structure helps steer projects in the right direction.**

Never disperse power fully. The most effective strategy is to temporarily loosen it, granting people autonomy and freedom when they develop ideas, and preventing segmentalism from setting in. Be sure to preserve an overarching structure that can still make strategic decisions.

**Suggested** **further** **reading:** ** _Good to Great_** **by Jim Collins**

_Good to Great_ presents the findings of a five-year study by the author and his research team. The team identified public companies that had achieved enduring success after years of mediocre performance and isolated the factors which differentiated those companies from their lackluster competitors.

These factors have been distilled into key concepts regarding leadership, culture and strategic management.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Rosabeth Moss Kanter

Rosabeth Moss Kanter is a sociologist and Management Professor at Harvard Business School _,_ where she was the first woman to be awarded an endowment chair. Her main fields of research are leadership strategy, change management and innovation.

