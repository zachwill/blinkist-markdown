---
id: 5aa6ef5db238e10007af4491
slug: the-charge-en
published_date: 2018-03-15T00:00:00.000+00:00
author: Brendon Burchard
title: The Charge
subtitle: Activating the 10 Human Drives That Make You Feel Alive
main_color: 9A4120
text_color: 9A4120
---

# The Charge

_Activating the 10 Human Drives That Make You Feel Alive_

**Brendon Burchard**

_The Charge_ (2012) explains how to activate the human drives that lead to happiness. These blinks clearly lay out what you need to do to feel confident, grounded, energized and prepared for any challenge, providing helpful tips on how to do it.

---
### 1. What’s in it for me? Live a charged life by activating the ten human drives! 

Do you ever feel as though your life is living you? You wake up, and you have no say in what your day will look like — it's already all mapped out. You just eat, work, eat, work, eat, sleep, repeat. And on the weekend, you're too burned out to do much more than watch TV and catch up on sleep.

Well, it's time for a drastic change. Even if you're relatively content with your life, and only occasionally wonder what it would have been like to pursue your dreams full-throttle, it's time for a new outlook.

Instead of feeling like you live in a cage that's merely comfortable, start living a charged life — the only lifestyle that will bring you lasting happiness and long-term fulfillment.

Luckily, it's by no means impossible to get there. All you have to do is activate the ten human drives, which is exactly what these blinks will teach you how to do.

In these blinks, you'll also find out

  * the difference between a goal and a challenge;

  * how creative you are; and

  * how to be who you think you are.

### 2. There are three types of life you can lead: caged, comfortable or charged. 

What constitutes a fulfilled life in your eyes? Is it working a well-paying job, owning a house with a white picket fence and taking the kids to the beach on Sundays? Well, that's what society would typically have you believe — that happiness is the sum of a materialistic equation. But society's math is faulty.

Indeed, living a life prescribed by others is like living in a cage. And the bars of the cage are erected early on.

When we're young, our caregivers reward the development of certain attributes, such as self-discipline and ambitiousness, by accepting us and giving us affection. In adulthood, we continue to seek those same rewards by following similar rules. We do what others expect us to do, and work the kind of job that would make our parents proud.

We may be unhappy. We may feel limited and unfulfilled. But we still won't break out of the cage, because we fear losing what we've worked so hard to gain: acceptance.

Now, some of us don't feel utterly caged. We're _comfortable_, living what society calls the "good life": a nice house, a nice spouse and a nice paycheck at the end of each nice month.

We're thankful for all this, but, deep down, questions nag: Is this really what I want? Am I missing out on something better?

Like our caged comrades, we long for self-realization. But instead of pursuing it, we count our blessings and convince ourselves that we're content.

Then there are those who live the _charged life_. Let's simply call them chargers.

They feel unhindered by others' expectations, and they're unconcerned about the so-called "good life." Their world overflows with possibility, and they follow their own path through it, pursuing their own happiness.

If you've been living the comfortable or the caged life, it's time to learn how to be a charger.

> _"Chargers are optimistic about the future, and that optimism works like a magnet, drawing that desired future to them."_

### 3. Take control of your approach to life and work. 

Regardless of whether your life is caged, comfortable or charged, one thing is certain: you're seeking happiness. And it's a simple fact that people living charged lives are _happier_.

That's because they've activated the ten _human drives_.

Human drives aren't essential to survival, and you can experience temporary happiness without them, but they're something we all desire — and activating them is the only way to live a truly charged life and achieve sustainable happiness.

The first five drives are called _baseline drives_, and they underpin feelings of inner wellness, such as self-confidence and a sense of belonging. These drives are _control, competence, congruence, caring_ and _connection_.

Let's start at the beginning, with control.

Most events in life are simply beyond your control. You can't control the flow of traffic or what strangers will say to you or how hard the rain will fall. However, you can control how you react to these things, and this can make a huge difference.

If you live a caged life and something displeasing befalls you, you'll probably interpret that event as further proof of your own limitations. If you live a charged life, however, you'll regard all negative events as nothing more than information, not something that can ruin your day or dash your self-confidence.

The author's father exemplifies control perfectly. When diagnosed with leukemia, he maintained his positive attitude. He won over the hospital staff with his friendliness and his jokes, and he frequently told his family how proud of them he was. He stayed in control and didn't let cancer ruin the last weeks of his life.

Once you've gained personal control, you can begin establishing professional control at work. It starts with ownership — the sense that you _own_ certain projects, and that your contribution is palpable and appreciated.

Ownership can turn a job that you dread into the job of your dreams.

Unfortunately, most companies don't encourage ownership. They depend on big teams, and the tiny contributions of individual team members tend to go unappreciated.

If you don't currently feel responsible for a project at work, and there's no project you could join, consider spearheading one yourself. You'll be happier for it.

### 4. Competence is scarce these days, so you should work on yours. 

Did you ever have a teacher who seemed capable of explaining anything? What about a coworker who could deal with any work-related problem? Such people are as inspirational as they are rare, and that's because they embody the second baseline drive: _competence_.

Competence is a threefold ability. It means you can _understand_, _perform_ and, eventually, _master_ whatever you decide to undertake. If you know that you can do these three things, even the most daunting of challenges will no longer seem insurmountable.

Unfortunately, the modern workplace tends to put a damper on competence. In general, employees are encouraged to multitask rather than master particular skills, which essentially short-circuits the _confidence-competence-loop_. If you never feel competent, you'll never feel confident, and if you never feel confident, you'll never engage in projects that might lead to a feeling of competence.

But moments will always arise when competence is required, and if you lack the competence to face those moments, you'll begin to feel insecure. This, in turn, may lead to self-doubt, anxiety and disappointments.

So you've got to reactivate the confidence-competence-loop by setting yourself challenges.

There are three criteria for the challenges you set: they should be _real_ (that is, difficult to accomplish), _time-bound_ and _observable_.

So a basketball player might try to increase the average number of baskets he sinks by five (real) in the fourth quarter (time-bound) and have his coach watch him do it (observable).

Overcoming these challenges will build your competence, which will boost your confidence, which will make you eager to tackle the next challenge. The loop will be back in action.

You can start with a 60-day speed-learning challenge. Take two months to learn a new skill, like how to play your favorite song on the guitar, or how to operate a complicated tool at work. This will kickstart the loop for you.

### 5. Attain positive congruence between your self-image and your actions by setting standards for yourself. 

You get home after work, eat dinner with your partner, get in bed and, when you're about to turn off the light, your partner says, "You didn't even remember." It's your anniversary, and you've forgotten again. Or you're explaining to your crying son that you'll take him to the zoo _next_ week, because — oops — you scheduled an after-work meeting and totally forgot about your family outing.

If you're striving to be a better partner or a better parent, you'll need to activate the third baseline drive, _congruence_, which is what brings your actions in line with your self-image and the person you're aspiring to be.

There are three levels of congruence. Between the way you act and the way you see yourself, there can either be positive congruence, negative congruence or no congruence whatsoever.

If there's no congruence, then your actions don't reflect your self-image. Deep down, you believe you could be a better person, and you're constantly disappointing yourself with your actions.

If there's negative congruence, then you have a low opinion of yourself, and you act accordingly.

For instance, if you don't think you're capable of, say, learning Swedish, then you'll never even attempt to learn it, which will only bear out the dim view you have of your abilities. There's congruence between self-image and your lack of actions, but you remain dissatisfied because you never actually pursue your dreams.

If there's positive congruence, however, then you know both who you are and who you want to be — and you align your actions with this knowledge. You're comfortable with the choices you make, and you feel deeply content and at peace.

So how do you achieve positive congruence? Well, you've got to set standards for yourself and truly stick to them. This might sound hard; however, if your personal standards are both realistic and free of external influence, then you'll find it easy to commit to them because they come from you and you alone.

Listen to your inner voice, and write six words on a piece of paper: three about who you want to be and three about how you want to treat others. Keep this note in your purse or pin it beside your desk — anywhere that you'll see it often so that it'll stay on top of your mind.

Stick to your standards, and you'll begin to feel more content in all situations.

### 6. Care for yourself and be receptive to the care others give you. 

We all long to feel accepted, and we all crave attention and affection. And the only way to have these needs met is to receive the _care_ of another person.

Care is the fourth baseline drive, and it functions as both an emotional compass and anchor, guiding how we act around others as well as connecting us to them.

But before we can truly start giving care to others, let alone receiving it from them, we must learn how to care for ourselves, and the first step on the road to _self-care_ is listening to your emotions.

Turning a deaf ear to your feelings can upend your life in surprising ways. For instance, the author once coached a former football star who was having trouble managing the car dealership he owned. Throughout his life, the athlete had experienced letdown after emotional letdown, so he tended to keep people at arm's length. But his customers found this off-putting, and car sales suffered.

So the author encouraged him to start listening to his emotions by taking a moment, three times per day, to check in with his feelings. This not only helped him reestablish emotional equilibrium; it also gave business a boost.

Naturally, your bodily health is just as important as your emotional health. So be sure that you get seven hours of sleep per night, that you exercise three days per week and that vegetables make up one-third of your daily food intake. Caring for your body won't only improve your overall health; it'll sharpen your mind and boost your sense of well-being too.

Now that you've established good self-care habits, it's time to embrace the care of others.

If you tend to hold people at a distance, then this may be frightening. But remember — the risk of getting hurt is an inextricable part of gaining the care of others, and it's certainly a risk worth taking.

One way to start opening up to others is to write down some of your life challenges, be they emotional, professional or financial. Then, share this list with people in your life. Explain what your goals are, and listen to the advice these people have to offer. This will instantly create a caring dynamic.

### 7. Forge deep connections with others. 

Humans are social animals. We need people with whom we can share our thoughts and feelings, from our sadness about a recent fight with our parents to our excitement about an upcoming first date. But we can't confide our innermost feelings to just anyone; it has to be someone to whom we feel deeply connected.

If you want to activate the fifth baseline drive and forge _connections_, then you need to be crystal clear about what you expect a relationship to look like.

In your opinion, what exactly _is_ a close relationship? Come up with clear definitions for all your relationships — from your friends and coworkers to your parents and your partner.

Then, share each definition with the relevant person or people, and request that they share their expectations too.

For instance, the author once asked his mom what she expects from a deep mother-son connection. She requested that he call her every Sunday, to say hello and express his love — and this ritual, which he's been doing for the last 15 years, has brought him much closer to his mother.

Good relationships are predicated on good people, so in order to forge those deep connections, you've got to find the right folks. While you can't choose your family, you can certainly pick your pals. But before you start searching for a new bestie, you should categorize your current friends.

Make a list of all the friends you've had. Mention the reason for the friendship and what made each friendship good. Now put each friend into one of three categories: old friends, the ones who no longer play an active role in your life; maintenance friends, the ones you only enjoy seeing occasionally; and growth friends, the ones who make you feel great, make you laugh and always have your back.

You should see your growth friends at least once a month, and you should try to find about ten of them; they're the ones who'll bring your life happiness and charge.

### 8. Stay positive in the face of change and be clear about your goals. 

Now that you're familiar with the baseline drives, let's move on to the next five drives — the _forward drives_, which are about living into the future and fulfilling your dreams.

The first one is _change_.

Biologically speaking, we're used to change. In our first years of life, we transform from a helpless baby into a walking, talking being, and from there into a self-aware adult. As adults, we continue to change; the cells in our body are constantly being replaced by new ones, and our brain is constantly seeking new stimuli.

But even though change is normal, we still tend to find it scary, in large part because we fear loss. Or, to be more precise, we fear that we'll lose more than we stand to gain.

For instance, let's say you're offered a new position at work, but it requires a transfer to a different department. You might well decline, fearing that a transfer will strip you of the perks and power of your current position, and drive a wedge between you and your work friends.

Instead of focusing on fears of loss, however, you should consider the potential gains. In this case, you might find the new position more satisfying, you might meet new and interesting people and may make even more money.

We also fear change because of a lack of clarity.

Most people don't know what they want in life, usually for no other reason than not knowing what their options are. For instance, college graduates are often at a loss, not because they don't know which path to pick, but because they don't know which paths are available.

So, research your options. If you're a recent graduate, check out websites of companies you find interesting and learn about how the people you admire most got where they are today.

Then, create a list of the things you do and don't want in life. For example, if you love your leisure, don't pursue a career that'll require you to work late and on weekends.

### 9. Challenge yourself and don’t fear the judgment of others. 

Maybe you've pushed your physical limits and managed to run a marathon. Or maybe you've challenged your intellect and written an A+ midterm essay. All of us have faced difficult tasks, and most of us are familiar with the extremely satisfying feeling of rising to a challenge and overcoming it. Well, that's what the seventh drive — _challenge_ — is all about.

Before we get down to brass tacks, however, we've got to differentiate between challenges and goals.

A goal isn't necessarily a big deal; it can be something small, like taking the dog for a walk or doing the laundry.

A challenge, on the other hand, is difficult by definition. To overcome it, you'll have to put in everything you've got — skills and effort.

So, when setting yourself challenges, make sure that they're _really_ challenges, not just goals.

Try challenging yourself every month for the next year: draw 12 boxes on a piece of paper and write a challenge in each.

For example, do you usually use notes when you have to give a presentation? Go without them next time. Do you usually play tennis against similarly skilled opponents? Play against someone who's obviously better. Or perhaps you want to improve your social skills? Well, concentrate on listening for a month, or on being a team player.

Whenever a new month starts, start the next challenge.

But whatever the challenges you set for yourself, don't let the judgment of others prevent you from committing to them.

It's common to worry that your friends or coworkers won't support you in your challenge, that they'll secretly be hoping you fail. But, in truth, people are much more likely to root for you than against you.

For example, how many times do you think the average middle-aged person has experienced truly painful rejection? Well, the author has been all over the world and asked hundreds of middle-aged audiences this very question. The answer: seven times.

The number of times these audiences experienced encouragement and support from others? Almost a thousand times!

Clearly, fears of judgment are seriously unfounded.

### 10. Assess your creativity and work on improving it. 

Every child is creative. Whether it's painting with watercolors, singing songs or simply making sandcastles on the beach, creative endeavors are something children are innately drawn to. But when did you last build a sandcastle or burst into spontaneous song?

The eighth drive, _creative expression_, usually peters out when we hit adulthood, which is too bad because it comes with many benefits — both personal and professional.

Indeed, creativity is extremely important to one's sense of self. It not only makes it easier to form more meaningful connections, it also deepens one's understanding of oneself.

Creativity is an especially valuable asset in today's business world, which is driven by innovation and design. In his book, _The Rise of the Creative Class_, Richard Florida explains that, at the turn of the twentieth century, less than one-tenth of Americans were engaged in creative work.

By the turn of the twenty-first century, that number had risen to one-third. In order to compete, current companies need creative employees.

But before you can start improving your creativity, you've got to establish how well you already express it.

Here's a simple test that'll help you do that:

On a scale of zero (not at all) to ten (completely), how much do you agree with the following questions?

  1. Does the interior style of your home reflect your personality?

  2. Is your work representative of who you are?

  3. Do you feel heard in your relationship? Does your partner value your interests?

  4. Is your personality an important part of your friendships?

  5. Do your hobbies represent your interests?

  6. Are you contributing to the world?

These questions are meant to measure your creative expression in everyday life. If you scored less than 45, you should start working on this topic.

And the best way to do that is to start creating.

Creativity is hard work that requires discipline. Got an idea for a novel? Start writing. Think you could become an abstract painter? Start painting. And if people don't like what you produce, don't give up — double down instead. Every artist started somewhere, and mistakes are simply part of mastery. So get started, get feedback and don't let setbacks get you down.

### 11. Contribute to a meaningful cause by following your passion and interests. 

If you could choose, would you rather work at a shoe outlet or at an institute dedicated to cancer research? Well, if you're like most people, you want your work to have meaning. You want to feel that, by working there, you're giving back to the world. So you'd probably choose curing cancer over peddling kicks.

But activating the ninth drive, _contribution_, doesn't necessarily entail seeking employment at a laudable organization; it can simply mean doing something you enjoy.

If there's something you love doing, and you truly give it your all, then contribution is the inevitable outcome.

For instance, the author once met a bank manager who felt she'd never truly be able to contribute to the world. Everyone said she had to "give back" — but she loved her job too much to quit and take up a nobler cause.

But then the author discovered something. The bank manager's employees all thought she was the best boss; she took their problems seriously and was a source of inspiration. She may not have worked for an NGO, but by doing what she loved, she _did_ make a contribution.

You can also contribute to a cause you're passionate about. This contribution can take many forms — skills, time, money or other resources. What's important is that you feel that your contribution is making an impact and encouraging positive change.

For instance, let's say there are two organizations: one helps feed hungry children in Nigeria, but the only way you can contribute is by washing dishes in a backroom where you never see the children; the other helps kids in public school learn to sing, and you can contribute by teaching them directly.

Even if you prefer the former organization, you might want to consider dedicating time to the latter, since you'll be able to see the immediate impact of your presence.

> _"When we feel as though we're contributing to the world, we gain a profound sense of meaning and purpose."_

### 12. Improve consciousness by focusing on your thoughts and appreciating life’s wonders. 

What does it mean to be conscious? Is it to completely occupy the current moment, to experience our surroundings with our senses and to enjoy complete self-awareness? Well, yes — but that's _thought consciousness_, and it's only one side of the consciousness coin.

There's also _transcendent consciousness_, the feeling that we're surrounded by a greater something, an energy that connects all life and even holds together the universe itself. For arguably all of human history, we've sought both kinds of _consciousness_, the tenth and final drive.

To foster the first kind, try to be more conscious of the thoughts you have.

Most of our time is spent thinking about random and unimportant things. We're controlled by our impulses, and we follow threads of thought that lead nowhere. So, if you want to achieve "thought consciousness," you'll have to focus your mind.

You can, for example, try what the author did. He set an alarm at three-hour intervals, and every time it went off he asked himself: what shall I focus on right now?

This improved his productivity, reengaged his attention and put him back at the helm of his thoughts, which he'd allowed to drift away far too often.

You can work on the second kind of consciousness by appreciating the world's small wonders.

Modern life is such a maze of distractions that we rarely take the time to stop and appreciate everyday sights and objects. Some of us will go into a rage if our cell phone loses connectivity. But if you stop to think about it, it's no small miracle to begin with that this tiny device can connect our voice to the voice of a loved one, or function as a portal to the internet, which is a miracle of equal magnitude.

Take a moment to marvel at such things, and you'll feel more connected to the world. Just as we may feel renewed affection for our partner when we take a moment to register how kind or beautiful or smart he or she is, we feel closer to the universe and all life when we consciously notice how amazing it is.

Once you've reached this level of consciousness, it won't be hard to stay charged.

### 13. Final summary 

The key message in these blinks:

**Everyone can live a happy, fulfilled life. All you have to do is activate the ten human drives that constitute the foundation of happiness and fulfillment. Once you incorporate them all into your life, you'll feel more motivated, more socially connected and more confident than ever before.**

Actionable advice:

**Implement the 90-day getaway routine.**

In order to give your brain something new and exciting every now and then, you should commit to the 90-day getaway plan, which means breaking out of your daily life every 90 days. This can be on your own or with someone else. It's not about traveling as far as possible; it's about getting away from your usual thoughts and doing something new, relaxing and enjoyable. Go somewhere you haven't been before and come back feeling recharged.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _High Performance Habits_** **by Brendon Burchard**

_High Performance Habits_ (2017) explores the six habits that can turn an ordinary person into an extraordinarily productive one. Performance coach Brendon Burchard draws on the data and statistics from one of the largest studies of the world's most productive people ever conducted to explore their habits and find out what makes them tick.

_"Burchard's research into the habits of high performers across the globe unearths some fascinating and practical insights. For example, did you know that you'll be more motivated to do something if you think you're doing it for someone else's sake?"_ — Ben H, Head of Content at Blinkist
---

### Brendon Burchard

Brendon Burchard, founder of the High Performance Academy, is an American author and motivational trainer. His other books include _The Millionaire Messenger_ and _High Performance Habits_.

