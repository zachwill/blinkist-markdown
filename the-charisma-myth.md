---
id: 52cc0b8564326600110e0000
slug: the-charisma-myth-en
published_date: 2014-01-08T19:22:51.000+00:00
author: Olivia Fox Cabane
title: The Charisma Myth
subtitle: How Anyone Can Master the Art and Science of Personal Magnetism
main_color: 426E44
text_color: 426E44
---

# The Charisma Myth

_How Anyone Can Master the Art and Science of Personal Magnetism_

**Olivia Fox Cabane**

_The Charisma Myth_ (2013) defies the popular notion that charisma is inherited, arguing instead that everyone can cultivate their own charisma, and in doing so can have a more positive attitude, find more success, and handle obstacles more successfully.

Using wide-ranging examples of charismatic people, from state leaders to CEOs to employees, the book also outlines the different styles of charisma and how to practice demonstrating each, and offers some useful tools and exercises with which to improve their psychological well-being.

---
### 1. Charisma is good for you: it makes for success in all areas of your life. 

Charismatic people have a great impact on others; they're charming, persuasive and impressive — all without any apparent effort. They lead more successful lives, too. Compared with the average person, charismatic people are more attractive, higher earners and less stressed.

It's unsurprising, then, that charisma gives you an advantage in the business world. It gets people to like and trust you, and to follow your leadership. Whether you're applying for a new job or trying to get ahead in your current one, charisma can help you to excel.

Indeed, studies have revealed that the workplace performance of charismatic people is rated very highly by their co-workers. One researcher observed that followers of charismatic leaders were strongly committed to their leaders' mission, often exceeding their official duties and even making significant sacrifices in their personal lives.

But charisma is beneficial not just in business; it can also help in other professions and in education.

For example, charisma can help high school students perform brilliantly in college interviews, or win student elections. It also helps them to become socially confident, and thus more popular among their peers.

As for professionals, charismatic academics find it much easier to get their research published and funded, and get to teach the courses they want to. Similarly, charismatic physicians are very popular with their patients, who are more likely to follow their medical advice and less likely to sue, should anything go wrong.

We can see the benefits of charisma outside the professional arena, too. Stay-at-home mothers, for example, must charm and influence all kinds of people: their children, school teachers and other community members.

Charisma, then, is for everyone, not just leaders or business people. From stay-at-home moms to students to physicians, charisma can help people lead happier, healthier, more successful lives. And the good news is that _anyone_ can be charismatic. How? Keep reading to find out.

**Charisma is good for you: it makes for success in all areas of your life.**

### 2. Debunking the charisma myth: it's not inherited but a set of behaviors which anyone can learn. 

Despite the popular _charisma myth_, people aren't born with charisma — no matter how naturally magnetic some people appear.

If such people were inherently charismatic, then without fail they would _always_ be captivating. But no one is charismatic around the clock. Someone might be utterly captivating one day, and the next be totally boring. This is because charisma is not some kind of magical aura but the result of a person displaying certain behaviors.

These behaviors are nonverbal _–_ they are _body language_. In any given moment, if we appear charismatic, it's because our body language is expressing charisma. For that reason, our charisma levels fluctuate: a single person will sometimes be perceived as charismatic and other times not.

For example, consider the famously charismatic Marilyn Monroe, who was able to switch at will between her "Marilyn" and "Norma Jean" personas. On one occasion, "Norma Jean" managed to ride the subway without passengers noticing her. The moment she reemerged on the New York streets, she transformed herself into "Marilyn." How? Simply by changing her body language: adjusting her hairstyle and striking a pose. All eyes were immediately on her.

Why, then, is the charisma myth so popular? The reason is that we experience only the effect of a charismatic person. What we don't see is the time and effort such people put into learning charismatic behavior.

Many famously charismatic people worked hard to learn this behavior, until it eventually became instinctive. One such person was Steve Jobs, who, early on, appeared very awkward during presentations. Over the years, he managed to gradually increase his charisma, eventually becoming the Steve Jobs we admire: the man who kept audiences hanging on his every word.

As you can see, everyone can be charismatic, simply by learning, practicing and refining the appropriate behaviors. Next, we'll look at those behaviors in detail.

**The charisma myth: it's not inherited but a set of behaviors which anyone can learn.**

### 3. Charisma is, above all, body language – and this begins in your mind. 

At every moment, you're unintentionally sending signals that are perceived subconsciously by those around you — your body language.

Whether you like it or not, your body language reveals your mental state. Anxiety, for example, produces a _microexpression_ on your face, and observers will sense that something's wrong.

On the other hand, certain signals are perceived by others as charismatic, and fortunately you can develop a mental state which will produce these signals.

However, since we can't consciously control every tiny expression, we can't broadcast charismatic body language at will. For example, if you're genuinely happy, your smile reflects it: the outer corners of the mouth rise, and the inner corners of the eyebrows descend. But if you're not genuinely happy, you will smile with your mouth only; your eyes will not express happiness, and your smile will seem fake — a _social smile_. People have no difficulty in spotting the difference.

But if we can't directly control our body language, how can we develop charisma? The solution is to _indirectly_ control our body language by influencing the part of the mind that controls nonverbal signals: the subconscious.

Because our brains cannot differentiate imagination from reality, simply imagining a situation causes your brain to send your body the same instructions as it would if that situation were real. So, if you create an _internal_ charismatic state, your body language will display charisma authentically. Others will see you as charismatic as a result.

This process is similar to the well-known _placebo effect_. A placebo is a simulated medical procedure: people are led to believe they've received a medical intervention when, in fact, they haven't, and this can have a positive effect. Similarly, creating an internal charismatic state will cause you to behave charismatically.

Creating the right internal state is critical to expressing charisma through body language. And it also accounts for the next key element of charisma: _presence_.

**Charisma is, above all, body language — and this begins in your mind.**

### 4. Charisma requires presence – a skill that makes you stand out, but is hard to achieve. 

There are many ways to appear charismatic, but the main one is presence: being aware of what's happening, moment to moment. In social interactions, this means being with the other person completely, and listening to them intently.

If you're fully present with others, you'll appear charismatic and generate trust, rapport and loyalty. If you lack presence, the person you're interacting with will likely notice, and might suffer emotionally. As one person complained, interacting with someone who lacked presence made her feel resentful and inferior to "whatever was more important to her than our conversation."

Clearly, being present is essential for charisma. But it's far from easy.

This is because our brains are wired to pay full attention to potentially important stimuli, such as new sights, sounds or smells. This inclination, which was key to our ancestors' survival, means that we're easily distracted.

Our society magnifies this problem by offering never-ending distractions, and the constant stimulation we receive can lead to a state of _continuous partial attention_ in which we can't attend fully to any one thing. One study found that almost half the average person's time was spent mind-wandering — thinking about something other than what they were doing in that moment. In fact, the only activity that kept people fairly focused on the task at hand was love-making.

But if you _can_ manage to be present, it will distinguish you from the crowd, and people will feel special around you. Just a five-minute conversation with a stranger is enough to impress them and establish a bond. People will sense your full attention and feel that in this moment they are the most important thing in the world to you.

The ability to be present makes you so charismatic that you can establish an emotional connection with another person in just five minutes.

Next, you'll learn how to master two key ingredients of charisma: _power_ and _warmth_.

**Charisma requires presence — a skill that makes you stand out, but is hard to achieve.**

### 5. Charisma means being powerful and warm – being willing to use your power for others. 

Human survival has always depended on finding people who are both powerful and kind. In a dangerous situation, we need to determine which people might want to help us and have the power to do so.

In the past especially, finding such people has meant the difference between life and death: imagine having a friend who was physically strong enough to rescue you from a saber-toothed tiger, or having a king as your friend who could save you from execution.

We subconsciously view the _combination_ of power and warmth as extremely positive, so we look for signs of it in others and try to keep as friends those who possess it.

In fact, the power-warmth combination is a necessary condition of charisma. Someone powerful but not warm might be impressive but not necessarily charismatic. They might even appear arrogant or cold. And someone warm but not powerful might be likeable, but not charismatic. Rather, they could seem overeager and subservient.

Compare, for example, the two candidates in Great Britain's 1868 elections, William Gladstone and Benjamin Disraeli, both of whom projected power, intelligence and knowledge. One woman who dined with each of them, separately, commented on how Gladstone had impressed her with his power, but seemed to lack warmth, while Disraeli projected presence and warmth in addition to power, making her, as his dining companion, feel special and fascinating.

The woman later said, "After dining with Mr. Gladstone, I thought he was the cleverest person in England. But after dining with Mr. Disraeli, I thought _I_ was the cleverest person in England." No surprise, then, that Disraeli won the election.

Displaying both power and warmth is key to being perceived as charismatic. Turning this into practice will depend on your personal charisma style, as discussed in the next blink.

**Charisma means being powerful _and_ warm — being willing to use your power for others.**

### 6. There are several charisma styles; everyone has to find the right one for themselves. 

Not all people are charismatic in the same way, and different situations require different kinds of charisma.

There are four distinct charisma styles:

_Focus charisma_ lets people know you're fully present — a good example is Bill Clinton.

_Visionary charisma_ inspires people or gets them to believe in something, much like Steve Jobs did.

_Kindness charisma_ makes others feel seen and accepted — just look at the Dalai Lama.

And finally, _authority charisma_ makes others believe you have the power to change their lives, much like Bill Gates does.

How do you choose the right one? This depends on three aspects: your _personality,_ your _goals_ and the _situation._

When choosing a style, don't go against your natural personality, as this can be counterproductive. For example, in the 2004 US elections, John Kerry "dumbed down" his focused, intellectual charisma in an attempt to be more accessible. The strategy backfired, alienating those who liked his original personality, and making him appear disingenuous.

In terms of goals, consider how you want people to feel and respond to you. For example, if you're delivering bad news, kindness is the best choice — it makes the other person feel more comfortable about accepting the news. Using authority in this situation wouldn't work: you'd probably appear arrogant or cruel.

You can adapt styles to the situation by combining or alternating between them. This is a talent of highly charismatic people. If you decide to try out new styles, only do so in low-stake situations where there's little to lose. For example, try expressing kindness at a low-profile networking event, but don't try out visionary charisma for the first time during an important presentation.

There is no "one-size-fits-all" charisma style; you have to find the one that suits you best. But whichever style you choose, there's one clear, foolproof way to appear charismatic — as you'll find out in the next blink.

**There are several charisma styles; everyone has to find the right one for themselves.**

### 7. The most important part of being seen as charismatic is making the right first impression. 

We all tend to remember the "firsts" — the first time we met someone, for instance. This means first impressions are very important, especially as we only get one chance to make a great one.

To make a great first impression, you have to make others feel you're similar to them.

People feel most comfortable with those who behave and appear as they do, because they assume you have much in common: social background, education and values. The author learned this the hard way when she wore a black business suit to a presentation at a casually-attired firm and was explicitly told to lose the suit.

The same logic applies to your demeanor and language. For example, a financial analyst had difficulty relating to her boss, a blunt, military type who used battle metaphors in conversation. But when she began to adopt the same militaristic language, speaking for example of "loyal soldiers," their interactions quickly improved.

Once you've made a favorable first impression on someone, it will color the rest of the relationship. This is because, as economist John Kenneth Galbraith believes, we seek to reaffirm our first impressions rather than revise them.

And first impressions are often proven right: in one study, it was found that after looking at a single photograph, subjects could accurately judge most personality traits of the person depicted (for example, their extraversion, or conscientiousness).

So, begin that first meeting by complimenting the other person or asking open-ended questions, and stick to positive subjects. When it's time to end the meeting, try again to leave the other person with positive feelings.

Finally, don't underestimate the handshake — it requires trust, so it's an important step in intimacy. For instance, if a CEO is faced with two equally strong candidates for a job, the handshake may be the deciding factor.

Making a great first impression comes from paying attention to others. Now let's find out how to overcome some of the common obstacles in being charismatic.

**The most important part of being seen as charismatic is making the right first impression.**

### 8. Being charismatic means overcoming mental and physical discomfort. 

Feeling discomfort, whether physical or mental, can affect your performance and emotions, as well as others' perception of you.

Being hungry, for example, will probably cloud your thinking, and low blood sugar can impair your attention and emotional control. If that discomfort is visible in your body language, it can affect how charismatic you appear.

Consider Tom, a consultant whose choice of clothing for a meeting with a client almost lost him a four-million-dollar deal. Tom wore a black wool suit, but the day of the meeting was hot and Tom was sitting in direct sunlight. He spent the meeting fidgeting with his collar and squinting against the sun, behavior which, in the client's eyes, looked like anger, disapproval and insecurity. Naturally, the client began to doubt Tom's abilities.

Mental discomfort, such as anxiety or self-criticism, can have the same effect. Tom had spent the lead-up to his meeting reeling as he considered its possible outcomes. Unsurprisingly, he became anxious, and this showed in his body language. People generally don't handle uncertainty well: it makes us tense and stops us from being present — both of which harm our charisma.

So, here are three ways to counteract discomfort: _prevent_, _recognize_, and _remedy or explain_.

_Prevent_ means to ensure your comfort by planning ahead. In Tom's case, he could have chosen a venue more appropriate for a business meeting, and clothing that better suited the climate.

_Recognize_ relates to being present: the more present you are, the more aware of your body language you will be. If Tom were fully present, he would have recognized that his behavior looked suspicious.

And the more aware you are, the easier it is to _remedy or explain_ the problem. When the sun hit Tom's eyes, he could have paused to ask whether they could move seats, explaining that the sunlight was bothering him.

By identifying and overcoming the obstacles of discomfort, you take an important step towards being charismatic.

Another great way to deal with obstacles, especially emotional ones like self-criticism and shame, is to _dedramatize_ — as we will discuss next.

**Being charismatic means overcoming mental and physical discomfort.**

### 9. Dedramatize: what's happening isn't serious, and lots of others are going through it. 

When you're feeling internal discomfort or negativity — both obstacles to charisma — remember you're not alone in the experience and that what's happening isn't really serious.

This is dedramatizing, and it involves interpreting a negative emotion in a way that reduces its impact.

This is what Michael, a consultant, did when he felt sure a client was about to end their relationship. He convinced himself that nothing serious was happening by imagining that he was simply a physical being with certain chemicals flooding his system.

Another way to dedramatize is to imagine the negative emotion is being experienced by a community of people rather than just you alone. Doing this will relieve you, as you'll realize it's not wrong to think or feel as you do. Think about people who've already been through a similar experience, especially those you admire, and trust that they experienced the same thoughts and feelings as you.

Dedramatizing can also help with an emotion that is particularly damaging to charisma: shame. When you're ashamed, you don't appear charismatic, as your body language isn't projecting power, warmth or presence.

Put simply, shame is the intense, fear-based feeling that we're flawed and therefore don't deserve love. This feeling causes us to fear we'll be excluded by those important to us.

To display charisma, it's critical for us to remove the stigma of shame from our difficult emotions and experiences. Just dedramatize by remembering that shame is a normal part of human experience, and that everyone feels it occasionally.

Dedramatizing is a great way to prevent negative feelings from damaging your potential for charisma. In the final blink, you'll learn about two other techniques: _destigmatizing_ and _neutralizing_.

**Dedramatize: what's happening isn't serious, and lots of others are going through it.**

### 10. Destigmatize and neutralize: negative thoughts are nothing to feel bad about, and they’re not necessarily real. 

Overcoming internal discomfort can be achieved by destigmatizing and neutralizing negativity.

Experiencing internal discomfort and negativity is a natural part of human life — even for the most balanced among us. Consider that even devout Buddhist monk Thich Nhat Hanh was once so infuriated by someone that he felt like punching him. This shows we're _all_ capable of experiencing the entire spectrum of emotions.

Yet, we've become used to reading any physical or mental discomfort as an indication that something's wrong. We feel that negative thoughts and emotions shouldn't be happening, thus making them difficult to deal with.

The solution here is to destigmatize: remind yourself that such discomfort is normal, merely a by-product of our survival mechanism.

Another way to overcome discomfort is to neutralize your negativity by realizing that your thoughts are not necessarily accurate. For example, if we feel bad because someone has reacted negatively to us, we should consider that the reaction might have nothing to do with us — that person could be struggling to manage their own physical or mental discomfort.

The reason we're so affected by our negative thoughts is that we believe our minds perceive reality accurately. But in fact our minds do not give us a complete and accurate image of reality; instead, we perceive only the small amount of information that's most relevant and important to us.

Remember Tom, the consultant whose choice of clothing led to him spending much of an important meeting fidgeting with his collar and squinting against the sun? The client mistakenly interpreted Tom's body language as negative emotions, when in fact it was discomfort caused by the environment.

So the next time you make a mistake, feel negative or experience discomfort, just remember that it's a part of normal daily life. Remind yourself that your mind isn't always giving you accurate information about reality.

**Destigmatize and neutralize: negative thoughts are nothing to feel bad about, and they're not necessarily real.**

### 11. Final summary 

**Final Summary**

The key message in this book:

**Everyone can be charismatic; we just need to get into a suitable mindset so we display the right body language, and practice this until it becomes instinctive. There are various charisma styles to choose from, so make sure you select the style that best suits your personality and the situation. Also, don't worry if you experience mental and physical obstacles while developing charisma; even successful people experience these things. Simply apply the three steps to overcoming these obstacles: dedramatize, destigmatize and neutralize.**

Actionable advice from the book:

**Visualization**

Visualization involves imagining yourself physically performing an activity, successfully. This exercise can be done at home, at work or even in public, and it requires you to create a vivid mental image of a triumphant experience in order to experience your feelings at that time; e.g., a ski team could visualize themselves careening through the entire course, feeling their muscles tensing, and experiencing each bump and turn. 

**Rewriting reality**

When experiencing anxiety, try using the "rewriting reality" technique. For example, if you have an important presentation tomorrow and you're anxious about the outcome, take a moment to imagine and list a few alternative outcomes, ranging from the presentation going smoothly to it failing completely. Describe your way of speaking, what the audience looked like, their reaction to you, and so on. As the list grows, your anxiety should subside.

This exercise enables you to get into a mental state conducive to better performance. The technique can be very effective because of the brain's tendency to confuse imagination and reality.

**Suggested further reading: Fascinate by Sally Hogshead**

In _Fascinate,_ author Sally Hogshead helps us realize our potential for fascination. By explaining in vivid language exactly how fascination works and how you can trigger it in others, _Fascinate_ provides you, your company and your brand with the tools to fascinate. These "seven triggers of fascination" can help you to increase the odds of success, both in your personal life and in business.
---

### Olivia Fox Cabane

Olivia Fox Cabane is an expert in charisma and leadership. She is currently Director of Innovative Leadership for the Stanford StartX program, and has worked with the leaders of Fortune 500 companies, assisting them in becoming more persuasive, influential and inspiring. Olivia is often featured in media such as the _New York Times_, _Bloomberg_ and the _Wall Street Journal_. She also writes regularly for _Forbes_.

