---
id: 59f5de46b238e10006f6074f
slug: the-chimp-paradox-en
published_date: 2017-11-02T00:00:00.000+00:00
author: Prof Steve Peters
title: The Chimp Paradox
subtitle: The Mind Management Program for Confidence, Success and Happiness
main_color: C73828
text_color: C73828
---

# The Chimp Paradox

_The Mind Management Program for Confidence, Success and Happiness_

**Prof Steve Peters**

_The Chimp Paradox_ (2012) is about the complicated and crazy mess that is the human brain. These blinks explain why people can be calm, rational and composed one minute and irrational and irate the next. Learn how you can keep your cool when faced with triggering situations.

---
### 1. What’s in it for me? Outsmart your inner monkey. 

Movies like _Planet of the Apes_ ; expressions like "Monkey see, monkey do"; and songs like "Hey, Hey, We're The Monkees" are only a few of the many popular references to our not-so-distant relatives. But chimps, beyond reminding us of our evolutionary past and playing a role in popular culture, occupy a psychological place within us all.

This chimp-like part of our brain affects our decisions, emotions and how we interact with other people. Sometimes it's in conflict with the more rational and human side of our brain and makes us behave in ways we might otherwise try to avoid. Here, we will delve into the universe that is our brain and try to understand some of its more base reactions.

In these blinks, you'll learn

  * how to determine whether your chimp side is governing your actions;

  * what the computer part of your brain is; and

  * that we communicate in four major ways.

### 2. The human brain has two primary ways of thinking that easily come into conflict with one another. 

It doesn't take a neuroscientist to know that people don't always act rationally. In fact, even when people know what the most rational response _should_ be, we often end up doing something entirely different. But how come?

Because our brains are divided into two distinct parts. The first is the "human," or rational, part, which is located in the _frontal lobe_. This part of the brain thinks and acts based on facts. The second part, which lives in the limbic system, is known as the _inner chimp_. The functions of this section of the brain are more primitive and immediate. In other words, the chimp part of the brain acts based on feelings and emotions.

Naturally these two ways of processing information can easily come into conflict, and when they do, the chimp brain often prevails. After all, the limbic system, which is evolutionarily much older, works faster, sending stronger impulses to action. For instance, imagine that a man named John complains to his wife, Pauline, about an incident he experienced that morning; their neighbor's car was blocking their driveway and, since John was late for work, he had to ask the neighbor to move it.

Hearing this, Pauline asks why he's complaining; their neighbor had moved his car quickly and everything's fine again. John's human brain can hear the statement as a fact, understand that it's true and leave it at that. But if his chimp brain were to prevail in this situation, John might hear Pauline's comment as a criticism. And this might lead him to ask why she never supports him, or result in him defensively saying that he's _not_ making a big deal out of the experience.

Most people have, at some point, found themselves involved in such irrational fights. To avoid these uncomfortable clashes, it's key for the human brain to actively manage the chimp brain. In the next blink you'll learn precisely how.

> _"As the Chimp within us is more powerful, it is more likely that the Chimp will speak before the Human gets a chance to take control."_

### 3. Manage your inner chimp by giving it room to vent. 

So the human brain is comprised of two different parts and it's key to be able to manage them. Here's how:

First, in any given interaction, you should begin by determining which section of your brain is directing your behavior: Is it the human or the chimp? To figure this out, you can ask yourself simple questions that begin with, "Do I want…?" By answering them, you'll learn which part of the brain you're following.

For instance, you might ask yourself, "Do I want to behave this way?" or "Do I want to feel these feelings?" If you answer no to these questions, it's a sure sign that the chimp is in control.

To make it more concrete, let's say you're obsessively worrying about being late for a meeting. You could ask yourself, "Do I want to be worried about this?" If you say no, you can be sure that you've got an inner-chimp problem that needs to be managed.

Once you've determined this, you can reign in this emotional side of yourself by giving it a little exercise. We're not talking jumping rope or jogging. Rather, exercising your inner chimp is about giving it the freedom to vent.

For example, imagine an interaction that made you angry — say, someone bumping into you at the supermarket without so much as an "excuse me." The key here is to allow yourself to express anger in an entirely uncensored way in a safe environment, ranting and raving about the encounter for ten minutes and saying whatever pops into your head. By making space for this time you'll likely satisfy your inner chimp and the anger will fade into the background.

However, for this technique to be effective, a truly safe environment is absolutely necessary. Don't go ape on the person in the supermarket. Instead, find a place where you can be alone and say everything you wanted to yell at the person in the moment. Don't worry about how long it takes; just keep going until the emotions begin to subside.

That's how you manage your inner chimp. But, of course, it's not _that_ simple. There's a third player in this game, too — a part of your brain that you'll learn about in the next blink.

### 4. Automatic functioning is essential to human existence, but it has a destructive side. 

Now that you know about the human and chimp parts of your brain, it's time to learn about another part. It's called the _computer_ and it's responsible for automatic functions based on learned patterns.

This is essential since autopilot plays a major role in the lives of most humans. Such automatic behavior is based on patterns you've learned, beliefs you've absorbed and programs you've established.

For instance, when you make your morning coffee, chew your sandwich at lunch or brush your teeth before bed, you're conducting actions that you've done every day for years, if not decades. You do them practically without thought or conscious effort. Naturally, such an ability has its benefits. For instance, if you grew up in a loving family, you'll default to a pattern of assuming that people love and value you.

However, not all automation is useful.

There are also nefarious elements to the automatic functioning of the brain. These are called _goblins_. They're the destructive patterns you've formed based on lived experience and, just like their useful counterparts, they're stored in your computer.

As an example, imagine a child brings home a drawing she made at school. A well-intentioned dad might compliment the child's work, hug her and put the picture up on the fridge. While that all sounds well and good, it might have unintended side effects. For instance, the child might begin to think that she'll only be loved for her achievements, thereby creating a pattern of thinking along the lines of "I'm unlovable unless I perform well."

Clearly, such patterns are dangerous, and one should do all one can to discourage the formation of goblins.

The father might have done well to take a different tack. First, he could have put the drawing aside, hugged his daughter and told her that he loves her and is proud of her. From there, he could then look at the drawing and ask her if she wants to put it up on the fridge, neutralizing any connection between the achievement and his love.

That being said, this approach really only works for nipping goblins in the bud. So, next up, you'll learn how to get rid of the ones that already exist.

### 5. Get rid of your goblins by identifying them and replacing them with positive alternatives. 

So goblins are real and they can cause some serious trouble. To handle them, you'll need to get your computer into shape, and that means implanting positive patterns. Here's how to do it:

First, you need to identify which goblins actually reside within your computer. This initial step is essential since both the chimp and the human parts of your brain look to the computer when they don't know how to act. And if there's a goblin in the computer — say, a negative behavioral pattern — the result will be unsettling for both the emotional chimp and the rational human.

Unfortunately, goblins are often well hidden, resulting in all kinds of issues. For instance, imagine you're standing in line to buy a coffee when someone cuts in front of you. The human will want to politely let them know that you're in line, while the chimp will lunge to knock the person off their feet. But before either of these impulses can be acted on, they'll look to the computer.

Now, if there's a goblin in the computer saying that you're not as good as other people, both the chimp and the human will back down and you won't react at all. Such a goblin can prevent you from doing all kinds of things, and the first step toward dealing with it is identifying it.

Once you've figured out that there's a goblin in your computer, you can simply replace it with a positive pattern.

So if your goblin is telling you that you're worth less than others, try adopting a positive alternative — for instance, that you are just as good as the people around you.

From there, you should be attentive to your state of mind and work to notice when the negative goblin rears its head. Every time it does, just replace it with the positive view until it becomes absolutely automatic. If you can manage to build in this new autopilot, you'll eventually deal with difficult situations politely, without even giving it a second thought.

### 6. The chimp tends to forget that different people have different brains. 

Being able to hear where other people are coming from is essential to navigating the world, and if you want to do this well, you need to calm down your chimp. Once you do, you'll realize that different people have different brains. This might come as a surprise since people tend to assume that the brains of others work just like theirs. But this assumption inevitably leads to misunderstandings, which in turn anger the chimp.

When you quiet down the chimp by using the methods you've now learned, however, you'll be able to discern different perspectives on life. Just take an extreme example that the author encountered in his work. One of his clients was the father of an 18-year-old boy with some unusual traits caused by autism. For instance, he would use an entire bottle of shampoo every time he took a shower. Another issue was that, every night when his father returned from work, the son would greet him with an endless stream of questions until the father couldn't take it any more.

These behavioral idiosyncrasies clearly demonstrate how utterly different human minds can be. Obviously, most people aren't as particular as the son in this example, but, nonetheless, connecting with and understanding others can be very difficult. It often requires both patience and creativity. (The father eventually solved the issues with his son by putting portioned-out bottles of shampoo in the shower and setting a rule of no more than three questions per evening.)

So, to help you understand others, keep these three points in mind:

First, don't assume anything about other people. Just because someone appears distant or distracted doesn't necessarily imply that they're unfriendly. They might just be dealing with a personal problem you know nothing about.

Second, don't have unreasonable expectations of others. After all, people make mistakes and expecting them to be perfect all the time is a recipe for disappointment.

And, finally, it's essential to abandon all prejudice, both positive and negative. It's best to simply take people as they are and try to get to know them.

### 7. People communicate in four basic ways, and knowing how to get your point across without getting aggressive is key. 

You just learned how easy it is to misunderstand others. Now think of how frustrating it can be trying to communicate with different people. It's no secret that communication, or a lack thereof, can cause all kinds of problems, but the good news is that it's a skill you can practice.

But before we get there, let's take a look at the four basic ways that people communicate, all of which involve your chimp and human brains.

First, your human brain can communicate with another human brain. Second, your human brain can communicate with someone else's chimp brain. Third, your chimp can communicate with another person's human brain and, finally, your chimp can communicate with another chimp.

In this final scenario, the results tend to be pretty ugly and the resulting calamity is familiar to anyone who has seen a couple in an argument. To avoid such unpleasantness, let's take a look at how to set up ideal conditions from the get-go.

To get off on the right foot, it's important to deal with issues as they arise and to speak in an assertive but not aggressive way. People tend to talk about such problems with everyone _but_ the person they have an issue with. This is not a good way to go about things. It's better to deal with the person directly while keeping in mind that aggression usually makes matters worse.

After all, aggressive communication is emotional communication and it will necessarily trigger an emotional response. Assertiveness, on the other hand, is about explaining where you're coming from.

For instance, imagine that you arrive late to a dinner date with a friend, who then gets upset and begins yelling at you. Being assertive in such a situation would mean doing three things. First, telling the person what you don't want; second, explaining how the situation makes you feel; and, finally, saying what you do want. More concretely, you might tell the friend that you don't want to be shouted at, that it makes you feel intimidated and that you'd prefer if he spoke in a quieter voice.

### 8. Getting healthy is easy if you focus on solutions instead of problems. 

By now you've seen how the brain and its different parts affect human behavior. Now it's time to learn how the chimp and human brains can impact your health. It's true, the brain even affects your physical well-being, so it's easy to improve your health by bringing your attention to solutions rather than problems.

In fact, the dilemma of physical health is quintessential to the human-vs-chimp debate; the human wants to exercise and lose weight while the chimp just wants to plop down in front of the television with a huge tub of ice cream.

When faced with this contradiction, focusing on problems like being overweight will only strengthen your inner chimp, since the worse you feel about yourself, the more you'll seek out the quick comforts of junk food and laziness. So, instead of falling into this trap, just focus on what you want. In this case, that means getting some exercise and shedding a few pounds.

In other words, to get healthy, you've got to be both proactive and responsive. The former means having a plan and the latter means that, if your plan fails, you'll be able to regroup and patch things together.

For instance, imagine you want to get in shape. You get a membership to the local gym and schedule two workouts per week with a fit friend. Just like that, you've got a plan. But then, after two weeks, you find yourself missing your allotted gym time. Now you've got to be responsive.

So, instead of dwelling on your failures, try telling your workout buddy how much you enjoy your gym time and how much progress you're making. Such a simple comment will encourage your emotional chimp, whose only goal is to feel good and save face. Chances are, you'll happily turn up at the gym next time around to prove to your friend that you were serious.

### 9. Seek out happiness and celebrate your victories. 

Did you know that happiness is actually a choice? That doesn't mean that you get to be happy whenever you want, but, rather, that life has its ups and downs, and that you can actively work to have more good times and fewer bad ones.

So how does this work?

Well, it's all about adding good things to your life. Remember: there's nothing superficial about seeking out what you need to be happy. For instance, there are certain material things that you likely need to feel happy and relaxed — like food, a comfortable apartment and a shower. However, there are also emotional things you probably want, like the love of a partner or the respect of your colleagues and friends. No matter what you want, write all these wants down and consider how you'll bring the items on your list into your life.

But as you do, keep in mind that pushing to do too much can obscure all your great achievements along the way. In other words, the real issue with happiness is that the chimp part of your brain will never be satisfied; it will always seek out more, dangerously leading to achievement, but not happiness.

Just imagine an athlete whose ultimate goal is to win an Olympic medal. On her journey to the top she picks up a number of national titles, putting on brilliant displays of athleticism. However, despite these accomplishments, she never celebrates.

Chances are that, even if she does win that Olympic gold, she won't be able to enjoy it. Instead, she'll immediately move on to the next potential win.

Such a practice is a recipe for disaster and, to avoid it, you simply need to appreciate your achievements as they come, ensuring that you enjoy the happiness that you originally strove for.

Once you've nailed down this piece, you'll have a complete toolkit for outsmarting your chimp. Just remember to make healthy plans that counter your primal urges, let yourself vent in safe spaces, communicate with wisdom and love and celebrate all your successes along the way!

### 10. Final summary 

The key message in this book:

**Your brain is composed of distinct parts, each operating according to its own internal logic. Some are rational, thoughtful and objective, while others are highly emotional and impulsive or simply automatic. While such differences can naturally come into conflict, you can manage them through awareness and training.**

Actionable advice:

**When choosing a partner, take both his chimp and his human into account.**

Finding a romantic partner can be difficult, but you can get a better sense of the whole person if you consider both sides of their brain. For example, a person's human side might be loving, generous, fun and engaging while the chimp side is vehemently opposed to monogamy. Whatever the differences, it's important for you to identify the characteristics that might be deal breakers for you, since you probably won't be able to change your partner's basic traits.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Power of Habit_** **by Charles Duhigg**

_The Power of Habit_ (2012) explains how important a role habits play in our lives, from brushing our teeth to smoking to exercising, and how exactly those habits are formed. The research and anecdotes in _The Power of Habit_ provide easy tips for changing habits both individually as well as in organizations. The book spent over 60 weeks on the _New York Times_ bestseller list.

**This is a Blinkist staff pick**

_"Having tried and failed several times to pick up good habits like morning meditation, I found these blinks really helped me understand the root of the problem."_

– Ben H, Head of Editorial at Blinkist
---

### Prof Steve Peters

Professor Steve Peters is an acclaimed psychiatrist who specializes in improving the performance and functionality of people's brains. He works with a number of successful business people and athletes, such as the players of the FC Liverpool soccer team.

