---
id: 572b87f12c9d0b000358d97c
slug: the-china-study-en
published_date: 2016-05-09T00:00:00.000+00:00
author: T. Colin Campbell and Thomas M. Campbell
title: The China Study
subtitle: The Most Comprehensive Study of Nutrition Ever Conducted and the Startling Implications for Diet, Weight Loss and Long-Term Health
main_color: 49A274
text_color: 2E6649
---

# The China Study

_The Most Comprehensive Study of Nutrition Ever Conducted and the Startling Implications for Diet, Weight Loss and Long-Term Health_

**T. Colin Campbell and Thomas M. Campbell**

_The China Study_ (2005) is a controversial book that explores the connection between diet and disease, showing you how a diet high in animal-based proteins can lead to a host of health problems. Based on scientific data, these blinks explain why if you want to stay healthy, you should go vegan.

---
### 1. What’s in it for me? Challenge what you know about diets and improve your health. 

Are you afraid to eat an apple? While this question may sound ridiculous, plenty of people shun carbohydrate-rich foods like fruit because of the "bad rap" that carbs were given in recent years.

This attitude is misguided, as you'll soon learn in the following blinks. Eating a largely plant-based diet, rich in fruits and vegetables, is exactly what keeps your body healthy.

But how about dairy products and meat? Aren't these good for us, too?

Not really. These blinks tell the story of a groundbreaking study in China that revealed unequivocally that animal-based proteins harbor scary health risks. In short, you can get all the protein your body needs, feel great and even conquer cancer — if you just eat your fruit and veg.

In these blinks, you'll discover

  * how the molecular trigger for cancer is found in cheese;

  * why a single scientist's recommendation is to blame for our obsession with protein; and

  * how eating an apple a day might keep you from going blind.

### 2. Good nutrition is essential to staying healthy, as popping pills or vitamins won’t do the job alone. 

Medicine has come a long way, but people still get sick — and stay sick — all the time. Why is this the case?

To improve health overall, focusing too much on medical care is a mistake. While it's important to seek cures to the things that make us sick, finding a precise solution to some of the more complicated illnesses is challenging at best.

For instance, many events that lead to serious disease, such as _carcinogenesis –_ the process by which cancerous tumors are formed — modern medicine has yet to fully comprehend.

What's more, pushing for more healthcare system investment doesn't necessarily translate into better overall public health.

In fact, the health of Americans has steadily worsened, even though the proportion of the country's gross domestic product (GDP) spent on healthcare has risen by almost 300 percent in the past 40 years!

Since 1970, _more_ people are dying from cancer; and the rate of diabetes among 30- to 39-year-olds rose by 70 percent between 1990 and 1998.

What's worse, however, is that an overreliance on medicine to remain "healthy" can blind us to its drawbacks.

Consider that approximately 7 percent of hospital patients who take medication as instructed by a doctor suffer severe side-effects — and sometimes even die. In other words, the pharmaceutical industry's promise of a "magic pill" for health isn not only costly but also dangerous.

A better focus would be improving nutrition. Maintaining a good diet is a proven method of preventing disease. You literally "are what you eat," since every cell in your body is composed of the nutrients that go into it!

Nutrition thus is the logical cornerstone of good health, not pills or surgery. The best part is, compared to elaborate and expensive medical cures, it's easier to focus on a healthy diet to prevent disease.

Maintaining a healthy diet has already been shown effective in preventing illnesses such as early-stage heart disease and diabetes — and even cancer.

But first, it's important to understand what is healthy when it comes to nutrition.

> _"He who does not know food, how can he understand the diseases of man?"_ — Hippocrates

### 3. We don’t need to eat nearly as much protein as we think we do, and meat can even make us sick. 

Plenty of people worry about eating too much fat or carbs. At the same time, however, we also worry about eating too little protein. But the truth is that we need much less protein than we think we do.

While your body naturally needs some protein to build muscle cells and keep other vital tasks functioning, a little protein goes a long way. In fact, in the nineteenth century, German nutritionist Carl Voit determined that we need to eat no more than 48 grams of protein daily.

Ironically, Voit can also be blamed for the insistence that we need more protein in our diets, not less. Despite his own research supporting a low-protein diet, Voit nonetheless recommended eating 118 grams of protein per day, with the assumption that "too much of a good thing" wasn't bad.

However, when an individual lives in an environment full of toxins, a diet rich in animal proteins can be a catalyst toward developing cancer.

In the 1960s, researchers in India exposed two groups of rats to aflatoxin, a toxic mold that can cause liver cancer. After the rats were exposed to the mold, one group was fed a diet that contained 20 percent casein, an animal protein, while a second group was fed a diet containing just 5 percent casein.

Every rat in the first group developed liver cancer or suspect lesions. Yet the second group stayed healthy, even though each rat had been exposed to the same amount of toxin.

We can assume that other types of animal protein can have a comparable effect on not just rat health but human health, too. For example, out of a group of Filipino children accidentally exposed to aflatoxins, those who ate a high-protein diet were the most likely to develop liver cancer.

But not all protein is bad. Often when we hear the word "protein" we immediately think "meat." Yet there are plenty of plant-based sources of protein, such as beans, nuts and soy. Importantly, plant-based proteins don't carry the same ill-health effects that meat and other animal proteins do.

> _"More people die because of the way they eat than by tobacco use, accidents or any other lifestyle or environmental factor. "_

### 4. A seminal study in China helped researchers make the connection between diet and disease. 

In the early 1980s, a group of researchers from Cornell University and the University of Oxford partnered with the Chinese government to conduct one of the largest public health studies ever undertaken.

This groundbreaking research project would come to be known as the _China Study_.

The focus of the study was to examine how a person's environment and nutrition affected overall health. But why did researchers decide on China? As China is a genetically homogenous society — that is, in terms of genetics, Chinese people have more or less the same chance of developing any given illness — researchers knew genetic differences would not distort their results.

Illness rates in China, however, vary widely by region. Some parts of the country claim cancer rates that are 100 times higher than in others. And since genetic variation couldn't be to blame, environmental effects must play a role. Thus the choice of location was ideal in determining how environment, including diet, could make a person sick.

In China, regional diets range from those based primarily on plant-based protein to those with more animal-based protein. This offered researchers an opportunity to determine how each type of protein might correlate to the prevalence of particular diseases.

There's another reason why China was a good choice. In the 1970s, Chinese Premier Chou EnLai encouraged a series of cancer studies in the country, the results of which were mapped in a publication called the Cancer Atlas. With several decades worth of data as a basis, researchers were already well-primed for their own project.

The China Study thus collected and analyzed large data sets to determine how different diets might correlate to certain diseases. The researchers conducted experiments and visited some 60 different regions within China to collect food and urine samples, as well as hand out questionnaires.

Researchers then combined their results with Cancer Atlas data to see if there was indeed a connection between nutrition and disease, including certain cancers and heart disease.

### 5. Animal-based protein diets were found to be a greater driver of cancer than carcinogen exposure. 

So what exactly did the results of this grand research project reveal?

We all know that cancer is a serious disease. The China Study revealed the connection between diets high in animal-based proteins and cancer. Here's what they found.

When a normal cell turns into a cancerous cell, several changes happen. A _carcinogen_, or potential cancer-causing agent, can only make a normal cell cancerous if certain enzymes are present, for example. This is where diet comes into play.

A low-protein diet can decrease enzyme activity in the body. So even if a person is host to a lot of carcinogens, the body's cells are less likely to turn cancerous if the individual maintains a low-protein diet.

Full-blown tumors develop from precursors named _foci_, or groups of cells created by enzymes that are primed to become cancerous but have not yet made that final change. It turns out that the number and size of foci are primarily determined by the body's protein intake and not the presence of carcinogens.

For instance, rats who were fed a diet of 20 percent or more animal protein exhibited triple the growth of foci compared to rats that consumed a diet of just 5 percent animal protein.

Here's the most incredible statistic. Animals that were exposed to low doses of aflatoxin yet consumed high levels of animal proteins developed nine times as many tumors as did animals that were exposed to high amounts of aflatoxin but consumed a low-protein diet.

Such results suggest that we could stop the development of cancerous tumors simply by restricting our consumption of animal-based proteins!

The China Study also discovered that the consumption of plant proteins doesn't appear to drive cancer growth. In separate studies, rats exposed to aflatoxin that were also given wheat or soy protein to eat didn't exhibit growth in pre-cancerous foci, thus suggesting that plant proteins were a better diet choice.

> _"Like flipping a light switch on and off, we could control cancer promotion ... by changing levels of [milk] protein..."_

### 6. A plant-based or vegan diet is the smartest choice for a healthy body and mind. 

So from these groundbreaking studies, we've learned that diets based on animal proteins can promote cancer while diets based on plant proteins can prevent it. The logical conclusion from these findings is to leave cows alone and choose a plate of vegetables for dinner instead!

Eating vegan, or maintaining a diet that is exclusively plant-based, is a good choice both for reducing your risk of cancer and avoiding other health problems.

A vegan diet can help women reduce the risk of breast cancer, for example. Many animal products such as milk and butter contain a lot of fat. A high-fat diet can raise the level of estrogen in the body; an excess of this hormone is known to increase the risk of cancer in women.

The China Study found that rural Chinese women whose consumption of dietary fat averaged 6 percent of their total caloric intake had a lower risk of developing breast cancer than those with an intake of more than 24 percent. In China in particular, eating less fat is closely tied to consuming fewer animal-based proteins.

Plant-based diets are also healthier as they contain more fiber than diets with a lot of animal protein. The China Study found that a high-fiber diet correlated to lower instances of colon and rectal cancer, as well as lower levels of cholesterol in the blood.

Because many Chinese people eat mostly plant-based diets, the average Chinese person consumes a healthy 33 grams of fiber daily compared to an average North American's intake of just 11 grams.

Plant-based foods are also a good source of antioxidants, nutrients that help the body fight off damaging molecules known as _free radicals._ Free radicals can damage cells. For instance, they can cause macular degeneration, a condition that can cause gradual blindness.

Antioxidants neutralize free radicals before they can do any damage. Good sources of antioxidants are fruits and vegetables, as well as olive oil and cocoa.

### 7. Final summary 

The key message in this book:

**Adopting a vegan diet is one of the best and simplest actions you can take to improve your health. Eating plant-based foods can put you at a decreased risk of cancer and diabetes, while also relieving you of the stress of disease and high medical costs.**

Actionable advice:

**Try a vegan restaurant for lunch.**

Maybe you think "going vegan" is too costly, or perhaps you don't like eating vegetables. Challenge your prejudices by taking a trip and exploring three new vegan restaurants. Order a dish you've never had, to consider whether you might incorporate these new tastes and flavors into your diet. You might be pleasantly surprised at how enjoyable and delicious it is!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Blue Zones_** **by Dan Buettner**

_The Blue Zones_ (2012, first published in 2008) whisks you through the regions of the world with the highest concentrations of healthy centenarians. By examining how people in these regions live and interact, we gain insight into how to extend our own lifespans.
---

### T. Colin Campbell and Thomas M. Campbell

T. Colin Campbell is a professor Emeritus at Cornell University and best known as the author of _The China Study_. Born on a dairy farm, Campbell went on to study veterinary medicine; he holds a doctorate in nutritional science and biochemistry.

A certified family physician and T. Colin Campbell's son, Thomas M. Campbell II cofounded the Program for Nutrition in Medicine at the University of Rochester _._ He is both the program director there and at the nonprofit T. Colin Campbell Center for Nutrition Studies.

