---
id: 568b8f92de78c00007000058
slug: the-cio-paradox-en
published_date: 2016-01-07T00:00:00.000+00:00
author: Martha Heller
title: The CIO Paradox
subtitle: Battling The Contradictions of IT Leadership
main_color: 6BB1E2
text_color: 426E8C
---

# The CIO Paradox

_Battling The Contradictions of IT Leadership_

**Martha Heller**

In our current technological age, even low-tech businesses are more dependent on IT than most companies were 30 years ago. Yet this fact is still not reflected in how many organizations function or the relevance given to their Chief Information Officer, or CIO. _The CIO Paradox_ tackles some of the complex situations that the modern head of IT will encounter.

---
### 1. What’s in it for me? Get key insights into what makes a successful CIO. 

What sort of tools do you use in your work? You probably send emails, talk on the phone or hold video conferences. Maybe you download some reports on statistics, use a CRM system or promote your business on the company website. These are just a few of the things that the Chief Information Officer — the CIO — is responsible for. So how can all these different responsibilities be combined in one role?

It's not an easy task. IT is now an integral part of most businesses, which means that more and more businesses depend on the leadership of their CIO. As most companies know, IT can be the source of much frustration and farce. But there is another side to IT — the strategic side that tackles the problems of how to develop the business, improve its systems and create trust and continuity. In these blinks, we'll explore what can be done to bridge the gap between the two parts of the CIO role.

In these blinks, you'll discover

  * why the CIO of Boeing decided to bring in outside revenue to the business;

  * how outsourcing can stimulate innovation; and

  * why Kimberly-Clark's CIO started a video blog.

### 2. Focus on simplicity and instill an innovative mindset in your IT organization. 

The job of the Chief Information Officer is paradoxical. CIOs must be cost-efficient and stick to proven, safe solutions. At the same time, they must invest in innovation and take risks. So how do you balance these contradictory demands? 

One way to pursue both goals is to simplify the IT organization and make it easier for employees to understand.

IT is costly, complex and, for most companies, absolutely vital. To the average employee, however, it often seems not only high-cost but inconvenient and out of touch with the business itself. That's not how it has to be, though. Because IT at it's best is both cost-efficient and great at solving problems.

Geir Ramleth, the CIO of the global engineering giant Bechtel Group, found that employees often regard IT as the department responsible for setting up barriers that prevent work from getting done. So, when Ramleth joined Bechtel, he went about simplifying and streamlining the role his IT department played.

Ramleth took the 33 different IT helpdesks Bechtel had around the world and turned them into one operation with a universal ticketing system and single phone number, available 24/7. They distributed this number to employees around the world, and, from call one, they increased the number of problems solved from just 20 percent to over 65 percent. At the same time, Ramleth's strategy brought costs down by over 30 percent.

Another way to encourage innovation is to give your team the time and space they need to focus on innovating.

Tom Farrah, the CIO of Dr Pepper Snapple Group Inc., found an effective way to do this. Farrah outsourced the routine, day-to-day operations of his IT department so that his internal team could spend their time focusing on innovation. With the mundane tasks of handling helpdesk tickets and network support out of the way, his team could focus on more innovative tracks like mobility and business intelligence.

### 3. Don’t let operational issues hinder your strategic work. 

So, IT is just the department that answers the helpdesk phones, right? Well, such a limited view of IT, which exists even among company executives, can greatly diminish the important work that CIOs can accomplish.

A good CIO makes sure that the IT department isn't seen as merely serving a supportive function. Kim Hammonds, the CIO of Boeing, demonstrates how IT can influence company strategy and even bring in more revenue for a company. 

Once, during a conference with 25 other airline-industry CIOs, Hammonds seized an opportunity to promote Boeing's mobile security work. Other companies, it turned out, were struggling with their own mobile security, and wanted to buy Boeing's solution. Hammonds sold the solution, a move that generated new revenue for the company.

Hammonds also reinforced the importance of earning revenue by delegating 10 percent of her entire IT team to working on customer contracts. Separating the routine support duties from the strategic contract work allowed Hammonds' team to focus on more important issues.

Not all CIOs can influence company strategy as well as Hammonds, though. Despite being on the senior management team, CIOs usually lack the clout of other department heads. To strengthen their influence, CIOs should propose an IT development strategy right off the bat — even as early as the hiring interview!

This is what Ron Kifer did when he was hired as CIO for American equipment manufacturer Applied Materials. 

Kifer knew he was inheriting a dysfunctional IT environment. So, during his interview, Kifer explained his strategy for fixing the problem. This included four requirements that would ensure he would have the right amount of influence at the company.

First, he would report directly to the CEO. Second, the CIO would be on equal footing with the other executives on the senior management team. Third, all IT teams would be under his control. And, lastly, he would be free to hire a new IT leadership team.

By securing his autonomy within the company, Kifer was guaranteed a better chance of success.

### 4. CIOs of global companies need to understand local markets and the importance of communication. 

Running a truly global IT organization means taking into consideration multiple languages, cultures, time zones and currencies. This can make being a CIO extremely challenging. So how do you meet these demands?

First of all, a CIO should know which processes are suited for globalization and which should stay local. After all, consumer expectations differ from country to country. For example, in India it's not uncommon to wait in line for 45 minutes. For customers in Brazil, on the other hand, anything over a 20-second wait is enough to make them leave.

It's a common mistake to take a system that works in your country and attempt to use it in another country. John Dick, the former CIO of Western Union, faced this challenge when working as CIO of a company with thousands of local markets worldwide, and he stresses the importance of understanding how specific markets work and building systems accordingly.

For example, many companies would love to have a standard global payroll system. But when it comes to getting paid, the US system may not be compatible with, say, a Malaysian one. Therefore, it may be more effective to establish a local payroll processor in each country.

Communicating on a global scale is another challenge for CIOs. Sometimes you need to have a face-to-face meeting, but you simply can't be physically present.

This difficulty is also surmountable. You can use video, both for meetings and to spread your message.

Ramon Baez, the CIO of Kimberly-Clark, started a video blog as part of his global communications strategy. After Baez posted a video on the importance of 360-degree-feedback, he quickly found that even the most reluctant team members started reaching out to him and giving feedback. If you're not as tech-savvy as Baez, hire a communications specialist to help you produce videos for your organization.

And don't worry about repetition — saying something countless times doesn't mean it's reached every corner of the globe!

### 5. CIOs need to be able to work with and improve upon the technology they’re given. 

Have you ever been saddled with an ancient cell phone at work because it's the preferred device of the IT department? It might seem like a minor inconvenience, but these kinds of technology-based decisions can have a big impact on a CIO.

It's normal for employees to want to use the most up-to-date technology, and the CIO will be flooded with demands from every department: the CEO wants to use her iPad for conference calls; the sales team needs to access their customer database on the road; your administrative assistant will want to log into his corporate email account from his new iPhone. And, naturally, the more these requests are denied, the more IT-resenting employees there will be.

Pressure to keep up with the latest technology makes the CIO's life increasingly difficult, because CIOs are often forced to operate within the framework set by a company's technological decisions from 15 or more years ago. Sales and marketing executives, in contrast, inherit no such challenges.

So how can CIOs reconcile the software of today with the IT of yesteryear?

A paradox indeed, with no easy solution. But we can learn from Tom Murphy, the former CIO of the pharmaceutical giant AmerisourceBergen, who had to deal with hundreds of applications within a faulty, 30-year-old mainframe, and managed to convince the CEO to invest in an upgrade.

Murphy knew he had to persuade the CEO to invest in new software. To do this he created a visual presentation. Murphy polled the company leaders to determine which applications are considered the most important, and then created a color-coded chart that showed how over 50 percent of the most critical 80 applications failed at least once a month. Additionally, his data showed that, over time, these results would only get worse.

With the help of a visual aid, Murphy provided a clear argument and convinced the CEO that new technology was a worthy investment.

### 6. Clarify the role of IT by building relationships and simplifying communications. 

It's not uncommon for there to be confusion about the role IT plays in an organization. Even CEOs sometimes misinterpret the function of the IT department, because their background is typically in finance, sales or operations — not IT.

This misunderstanding between the CEO and CIO can lead to another paradox: you're an integral part of every aspect of the business, yet you're viewed as being a separate department altogether.

To resolve this paradox, it's crucial that the CIO builds relationships with the leaders of the other departments. The best way to create positive relationships is to ensure you come across as a colleague, not a technologist. This means paying attention to how you present yourself and the IT department in meetings, reports and conversation.

It's much easier for the CEO and other executives to understand the importance of IT when you cut the confusing techno-babble. Leslie Jones discovered this when she became CIO of Motorola Solutions, Inc. Her predecessor had filled the weekly reports to the executive committee with eight pages of technical details that only a CIO would understand.

Jones discontinued these reports and put together a simple, clear, one-page weekly report instead. For her efforts, Jones received a note from the CEO congratulating her on delivering the best IT report he'd ever read.

Leslie Jones also knows about the importance of communication between IT and the other company departments. When she came on board as CIO, she noticed that there was confusion about which person in the IT department should be contacted for certain issues.

So, Jones simplified things. She appointed one individual from IT to be the contact person for each specific department, no matter whether the issue related to tech support or business. By electing one IT person as the contact for the marketing department, another for sales, and so on, Jones made it easy for every department to communicate with IT.

### 7. CIOs should plan ahead and prepare a smooth transition for the future CIO. 

So you've improved the functionality of the IT department and created a better work environment. Your job as CIO is complete, right? Not so fast. To really build a lasting legacy you need to make sure your successor is right for the job.

This brings us to our final paradox: how do you groom a successor while keeping in mind that only a fraction of Fortune 500 companies adhere to their CIO's succession plan?

Let's look at why that is. Often, when a CIO leaves, the CEO will want to bring in a replacement from outside. This is because CEOs are typically dissatisfied with the state of IT; every internal IT manager is tainted by association.

And even if there is a strong internal candidate, that person has likely had too little contact thus far with the company executives to be taken seriously.

These reasons can stack the deck against a CIO's plans. Luckily, you can still groom a future CIO.

First of all, ensure the successor candidate has plenty of interaction with the rest of the business. According to Barbara Cooper, the former CIO of Toyota Motor North America, 40 percent of your time should be dedicated to preparing the rest of the business for a future CIO by ensuring adequate interaction.

Cooper did this by building a program for potential successors to establish real relationships with different departments and develop the skills they'll need as a CIO.

This program allows the CIO to select IT managers seen as potential successors to work on assignments with executives of other departments that can benefit from IT involvement. Barbara Cooper started this _executive rotational initiative_ for her IT department, thus ensuring that her top IT managers got exposure to other parts of the business. It was a success, and became a company-wide program for future executives.

### 8. The Final summary 

The key message in this book:

**Today, technology is what enables a business to be successful. Therefore, IT should be seen as a crucial part of any business strategy. Yet, CIOs are constantly struggling to be taken into consideration. To overcome this CIO paradox, CIOs should look beyond the IT department and focus on how they can influence the entire business and instill an innovative mindset in their team.**

Actionable advice:

**Give your organization a motto.**

A motto allows you to emphasize two or three attributes you would like your IT organization to be known for. A powerful mantra can be a great tool in bringing IT together with the business.

Take the example of Tom Conophy, CIO of Intercontinental Hotels Group, whose mantra, "our mission is to transform technology to be the enabler of brand differentiation," helped his team to create a new mindset focused on innovation. 

**Suggested further reading:** ** _The Five Dysfunctions of a Team_** **by Patrick M. Lencioni**

_The Five Dysfunctions of a Team_ presents the notion that teams are inherently dysfunctional, so deliberate steps must be taken to facilitate great teamwork. A knowledgeable team leader can do a great deal to make his or her team effective, and the book outlines practical tools for achieving this.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Martha Heller

Martha Heller is president of Heller Search Associates, a recruiting firm that focuses on senior-level IT leaders. She is a regular columnist for _CIO_ magazine and is also on the judging panel for the publication's prestigious CIO 100 Awards.

