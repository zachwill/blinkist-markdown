---
id: 5a92caa7b238e10006d08228
slug: the-clash-of-civilizations-and-the-remaking-of-world-order-en
published_date: 2018-02-28T00:00:00.000+00:00
author: Samuel P. Huntington
title: The Clash of Civilizations and the Remaking of World Order
subtitle: None
main_color: 125A3B
text_color: 125A3B
---

# The Clash of Civilizations and the Remaking of World Order

_None_

**Samuel P. Huntington**

Since the end of the Cold War, the global order has been in a state of constant flux. Huntington's incredibly influential _The Clash of Civilizations and the Remaking of World Order_ (1996) is both an analysis of the situation and a harbinger of things to come. The book dives deep into geopolitical tensions, and suggests what should be done to ensure the dominance of Western civilization.

---
### 1. What’s in it for me? Get to the heart of modern geopolitics. 

Have you ever read a book written years ago and been shocked by its prescience? Well, of all the books that foreshadowed the future, the controversial _The Clash of Civilizations and the Remaking of World Order_ is perhaps one of the most influential. It shaped political thinking across the entire spectrum, from the Grand Old Party right to the neoliberal left. In fact, it's amazing to see how ideas that filtered down to George W. Bush and Donald Trump are present here in nascent form.

What's even more incredible is that the book was written before 9/11 and yet still reads as though it were published yesterday.

Simply put, it's one of the books that formed our modern worldview, even if we haven't actually read it. A clear-sighted analysis, it accurately predicts the role of identity politics in our current climate. But it's also reductive, dismissing other cultures and presenting world politics as a zero-sum game where only the strong and imperialistic will win.

It's as much a part of the zeitgeist as an analysis of it, and, whether you agree or disagree with the author, reading it will make you that much wiser about the current state of global-order politics.

You'll also learn

  * which surprising factor led to the overthrow of the last Shah of Iran;

  * the difference between modernization and Westernization; and

  * how the Soviet Union made the West stay true to its beliefs.

### 2. The West has dominated all other world civilizations for the last 500 years. 

Civilizations have existed for millennia. Civilizations developed when people with common cultures and ways of life began living together, and particular customs, languages and worldviews emerged.

At the end of the twentieth century, the author reckoned there were eight civilizations in total: Western, Orthodox, Islamic, Buddhist, Hindu, African, Latin American, Chinese, and Japanese.

But it wasn't always this way. In fact, before 1500, Chinese and Islamic civilizations were more advanced militarily, economically and culturally than Christian Western Europe.

But the European Renaissance changed this balance. By 1500, the West had begun ascending to a position of global dominance due to technological progress, accelerating economic growth and increased social pluralism.

There are two specific reasons for this.

First, the West formed a _multipolar international system_. In other words, nations that were close to each other, such as Britain, France and Germany, though they were often fighting each other, cooperated politically and economically. This cooperation enabled these nations to grow closer and stronger, which, in turn, enabled Renaissance ideas to spread quickly. This resulted in a technological boom, and seemingly impossible feats — such as transatlantic ocean travel — soon became commonplace.

In contrast, the Andean and Mesoamerican civilizations behaved quite differently. They had almost no contact with each other until Europeans arrived. They lived huge distances apart and simply lacked the technology to make such journeys.

Second, after 1500, the West began conquering and colonizing. They had all the firepower and advanced navigation technologies they needed to expand their commercial empires. Before long, the influence of Western ideas, politics and religion could be felt the world over. And the West still dominates to this day.

### 3. The powerful West holds much sway over the cultures of other civilizations. 

When you think about it, the world seems like a small place. And that's because, thanks to economic globalization motivated by Western commercial interests, Western products, ideas and culture have infiltrated the four corners of the world.

This isn't without irony. One can easily imagine a scenario where a group of young men in the Middle East, sipping Coca-Cola and sporting American-designed clothing, plot a terrorist attack on the United States.

Such a scenario doesn't sound implausible because, as more Western products and ideas have poured into other nations, more anti-Western sentiment has fomented in those nations' hearts.

Non-Western leaders, for instance, often use the denouncement of perceived Western cultural imperialism as a rallying cry, telling their followers that they want to preserve their own native culture. This resentment is perfectly understandable, as is the fear that one's own culture is under threat. After all, American popular culture is overwhelmingly dominant. The facts confirm this: in 1993, 88 of the 100 most-attended films in the world were made in America.

Nonetheless, the author claims that Western cultural and economic dominance doesn't necessarily mean the world is becoming more "Western."

Post-Cold War world thinkers like historian Francis Fukuyama claim that, since the United States is the world's sole superpower, all other nations will model themselves after it so that they can trade effectively. The end result, on Fukuyama's view, will be a world replete with Western-style liberal democracies.

But there will be blowback. The West might see this advance toward a universal world culture as a good thing. But, for many non-Westerners, such universal globalism represents the destruction of their own cultures.

So, even as the non-Western world continues to modernize, the world as a whole actually becomes _more_ resistant to Western culture.

To understand why, it's essential to differentiate between two converging processes: _modernization_ and _Westernization._ Let's look at those now.

### 4. Increased modernization has resulted in power moving away from the West. 

If you want to understand why the push toward _modernization_ in non-Western countries has not led to increased _Westernization_, it's essential to understand these terms precisely. They are often confused.

Modernization is the process whereby developing countries reach Western levels of industrialization, urbanization and education. It's about economic and social development.

Westernization, on the other hand, is the process whereby Western cultural values of freedom, liberalism and secularism spread and create a universal global culture.

So the modernized West may lead the world in technological advancement, military strength and economic power. But it doesn't follow that other civilizations will culturally Westernize as a result.

In fact, paradoxically, because the West has pushed modernization so hard, other civilizations have advanced without needing to adopt Western values. China, South Korea and Japan are the prime examples. They have successfully modernized to Western standards, and this has made it easier for them to promote their own native cultural values while ignoring those of the West.

On top of this, successful non-Western modernizations can lead to power moving away from the West. What the author terms the "Islamic Resurgence" is an example of this.

Starting in the 1970s, Muslim-majority countries began to modernize. At the same time, Western-backed secular regimes in these countries began to falter, giving way to a resurgence of religious thought — which, of course, is diametrically opposed to the values of the secular West. And this all happened thanks to modernization-induced urbanization and burgeoning group consciousness. This group consciousness could be described as a shared set of social norms and ideas that help a group define itself.

The example par excellence is the 1979 Iranian Revolution. Shia Islamists overthrew the CIA-backed regime of the Iranian Shah. As a result, a new leader emerged, a Shia Muslim cleric named Ayatollah Khomeini who quickly became one of the West's most outspoken enemies in the Muslim world.

Urbanization played its part. In 1953, 17 percent of Iran was urban. By 1979, 47 percent of Iranians lived in cities. The American military simply could no longer prop the Shah up in the face of the will of his own modernized and urbanized people.

### 5. New groupings within civilizations are forming in the post-Cold War era. 

During the Cold War, knowing where you stood was simple. You were either pro-West or pro-Soviet. However, since the Soviet Union's dissolution, identity and identification have become much more complex.

Today, people are searching for new hooks on which to hang their identity, be it ethnicity, language, moral values or religion.

The world has realigned, and new groups and peoples have emerged with new identities. Consequently, civilizations themselves are grouping in different ways.

These groups are centered around _core states,_ such as Germany and France in Europe, or the United States in North America, even though they are technically all constituent parts of Western civilization. These states act as anchors for other, less powerful nearby states. They keep the peace and maintain the social order, either through military alliances, such as the US-led NATO, or political-economic cooperation, such as the German- and French-led European Union.

The problem, however, is that some civilizations don't have core states to form around. And this can be dangerous. The Islamic world, to take the obvious example, has no core state, which has been — and continues to be — a major stumbling block to its development and modernization.

The situation resulted from the disintegration of the Ottoman Empire during World War I. Previously, the Ottoman Empire had dominated most of the Middle East, and had functioned, for centuries, as Sunni Islam's core state. Until World War I, it had wielded the military, political and religious power necessary to keep the region in order.

After the Ottoman Empire fell, however, tribal and local alliances quickly replaced the loose loyalty that had once united all people in the region. This background informs the global Islamic resurgence of religious consciousness that is currently taking place.

The author is certain that the Islamic world's civilizational awareness and lack of core states — what he terms _consciousness without cohesion_ — will surely be a source of future problems and conflicts in the region.

### 6. Clashes between civilizations are inevitable. 

The author imagines two clear threats to Western global power: increasing Islamic global consciousness and Asian economic progress. No matter what ends up happening, the potential for conflict looms large.

For the author, the Islamic world is of particular concern, mainly due to its poor economic development and high population growth.

The Islamic world has high levels of unemployment and youth alienation. And the youth population is large; the number of people between the ages of 15 and 25 is growing particularly rapidly. In other words, there are plenty of recruits willing to fight for Islamist causes.

All that's needed is a spark of perceived Western cultural imperialism, and the tinderbox of violence will be ablaze.

The question is, why is this only starting now? There's a simple explanation: during the Cold War, the West and the Islamists were united by a "the enemy of my enemy is my friend" mentality. The Soviet Union was their common enemy, and they fought against it together, especially in Afghanistan and Yemen. Now that the Soviet Union is gone, clashes between the former allies are unavoidable.

It's different in Asia though. Quite different sorts of clashes will take place there.

Taiwan and Japan, with their rapidly growing economies, aren't the issue. What's troubling is China and its burgeoning military strength.

On top of that, there is Asia's growing sense of its own cultural relevance, especially in the face of American cultural imperialism. Resentment between Asian and Western nations will almost undoubtedly increase.

The author is especially worried that the demise of the "enemy of my enemy is my friend" mentality will have serious consequences in Asia. He imagines a world where Asian powers unite with Islamists against the West. This can already be seen in the emergent Iran-Pakistan-China axis.

To be sure, there are common grievances against the West, such as opposition to the proliferation of Western weapons and Western notions of human rights. It wouldn't take much for these grievances to reach a tipping point and turn into an economic or military conflict — perhaps no more than China's military encroaching on American interests in the Pacific.

### 7. The international liberal order of the United States is under threat and must adapt. 

There's a tendency for every great civilization to convince itself that it is the acme of human social development. This is known as the _mirage of immortality_.

Since they think they're already so powerful, such civilizations have trouble adapting to changing global orders. But that is precisely what the West will have to do to stay relevant because, as things stand, we're not trending toward a global empire based on Western principles like free markets and democracy.

The author sees _multiculturalism_ as the biggest threat in the West's own sphere, particularly in the United States. He's convinced it will lead to the collapse of American global dominance. Multiculturalism, he claims, is a direct threat to Western values of liberty, freedom and justice. In his view, it creates other sexual, racial and alternative cultural groupings and identities. It's the first step on the path to dissolution.

It's an inversion of the situation in the Islamic world. The West may soon have _cohesion_ in the form of a nation state, but no _consciousness_ — that is, the values that unite the people of a particular civilization.

To prevent internal collapse, the author believes that the West must stay committed to its roots. If the rest of the world is forming around religious and cultural identities, then the United States must loudly proclaim its Western values and _not_ divide society by promoting multiculturalism.

Consequently, the author holds that Americans should resist the advice of multiculturalists and avoid integration with Asian civilizations, despite the apparent common economic interests. Instead, we should double down on our commitments to our Western roots and forge firmer bonds with our Atlantic neighbors. It's only through developing economic and political integration with these nations that America will survive.

The author is unambiguous about it: the United States is under threat. If its international liberal order is to be maintained, multiculturalism and embracing Asia must be rejected.

### 8. Final summary 

The key message in this book:

**The West's international liberal order may be on the verge of collapse. Multiculturalism in the West threatens to tear apart any social cohesion. Meanwhile, anti-Western sentiment is on the rise in other civilizations. Consequently, global peace is at risk, and the Islamic world poses an especially large threat. To top it all off, rapidly modernizing Asian civilizations that refuse to Westernize will soon compete with the West for global dominance.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Political Order and Political Decay_** **by Francis Fukuyama**

_Political Order and Political Decay_ (2014) contrasts the history of democracy in America with its current condition to reveal the fundamental flaws of our modern democracy. From a declining middle class to selfish lobbyists and unadaptable institutions, these blinks explain just a few sources of political decay in the United States.
---

### Samuel P. Huntington

Samuel P. Huntington was a political scientist whose books played a highly influential role in forming modern American foreign policy. He spent over 50 years at Harvard University, where he was Director of the Center for International Affairs. He also wrote _Who Are We? The Challenges to America's National Identity_ and _Political Order in Changing Societies._

