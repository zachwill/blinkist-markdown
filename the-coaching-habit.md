---
id: 57a5a51ebed677000322c475
slug: the-coaching-habit-en
published_date: 2016-08-09T00:00:00.000+00:00
author: Michael Bungay Stanier
title: The Coaching Habit
subtitle: Say Less, Ask More & Change the Way You Lead Forever
main_color: 29B6CF
text_color: 1C7B8C
---

# The Coaching Habit

_Say Less, Ask More & Change the Way You Lead Forever_

**Michael Bungay Stanier**

_The Coaching Habit_ (2016) breaks down the elements of coaching and explains how to coach effectively. Contrary to what you might think, coaching isn't about giving advice but instead about guiding employees to find their way to success. These blinks show how you too can become a great coach.

---
### 1. What’s in it for me? Become a better coach and empower your team toward greatness. 

You've most likely worked with a coach before, whether a football coach in high school, a piano teacher from the neighborhood or a manager at work. If you were lucky, this person not only taught you the skills you needed to perform a particular task, but also empowered you to be the best you could be.

Such coaches are unfortunately rare in life. In fact, a majority of employees say that coaching hasn't helped them at work at all.

If you're a manager, how do you make sure that your coaching moments are effective? First, you need to realize that coaching is not about you — but about the employee.

These blinks offer a clear guide to mastering the right questions and habits to becoming a coach who listens well and guides employees to greatness.

In these blinks, you'll also learn

  * how to kickstart a coaching session;

  * how asking a "lazy" question will help productivity; and

  * why your brain is your biggest enemy in slaying old habits.

### 2. Effective coaching is about empowering a team and improving its long-term performance. 

Most managers have attended a coaching seminar or two. Unfortunately, only 23 percent of employees report that coaching sessions have had a positive effect on work performance.

How can this be the case? How can we make coaching better?

Let's start by looking at some common problems in the workplace that make it far too easy for team members and leaders to fall into unproductive work habits:

Your team refers all decisions, big and small, to you. They lose motivation, feel no agency, and you become the bottleneck of every project.

You're overwhelmed at work. You run from meeting to meeting, checking emails on the go. If an employee has an urgent question, they need to find you first.

Your team is dedicated to completing work tasks, but you're unsure which tasks are important and which make little difference.

As a leader, if you find yourself in one of these unproductive work dynamics, how do you make a change? The key is to develop a _coaching habit_.

Aim to coach your employees for ten minutes every day in an informal setting, rather than scheduling rigid weekly coaching sessions. Coaching should be a regular part of life in the office; you should always be in "coaching" mode.

A coaching habit helps you guide your employees toward self-sufficiency. It prevents you from being buried by work and reconnects you and your team to the work that matters most.

So focus on _development_, not performance. Performance is important, but you won't empower your team if you're constantly putting out small fires and forgetting the larger goals. Look for areas in which an employee can grow. Guide your team to becoming better and more effective at what they do as a group.

> _"Coaching for development is about turning the focus from the issue to the person dealing with the issue, the person who's managing the fire."_

### 3. Use three basic questions to initiate and maintain a constructive conversation with an employee. 

A lot of managers "coach" by feigning interest and nodding meaningfully as an employee talks. Such behavior isn't productive for either side. To coach effectively, begin with the _Kickstart_ question.

The Kickstart question is an essential tool in your coaching toolbox, and simply involves asking: "What's on your mind?" Moreover, if you find yourself in a 20-minute discussion about the weekend with an employee you're looking to coach, asking the Kickstart question will get you back on track.

After you've listened carefully to what your employee has to say, move to the _AWE_ question: "And what else?"

The AWE question prevents a conversation from becoming stuck on a single topic when it's clear there's more the employee wants to say, but perhaps can't find a way. Asking the AWE question is handy if you find yourself wanting to make a comment, too.

As a coach, remember that you have to _listen_ more than speak or offer advice. Your goal is to empower your employees, letting them come to their own conclusions. The worst thing for a leader is a group of employees who run to them every time a decision needs to be made.

In cases where the first two questions in your coaching talk aren't leading to a productive conversation, consider using the _Focus_ question: "What's the real challenge here for you?"

When an employee starts to lose his train of thought or if you're having a hard time following, that's a good time to pop the Focus question.

While an employee might want to vent about problems with a certain project, for instance, it won't solve anything. The Focus question helps you narrow down the problem so you can tackle it together. In short, it helps you determine which challenge you need to work on first.

The three coaching questions — Kickstart, AWE and Focus — are the foundation of your coaching practice.

Let's look at a further four questions which will launch you from basic coach to professional coach.

### 4. Identifying an employee’s needs and wants will help you become a more effective manager. 

It's not always easy to identify what you need to do at work every day. Similarly, how can you coach an employee if she doesn't even know why she's speaking with you?

An effective coach knows how to determine an employee's needs in any given situation. People are always driven by wants or needs and asking the _Foundation_ question can help when a conversation starts going in circles.

Use the Foundation question "What do you want?" to get to the heart of the matter.

Scientists say that people are driven by nine major wants and needs: affection, creation, recreation, freedom, identity, understanding, participation, protection and subsistence. The Foundation question helps you figure out which of these wants or needs is motivating your employee.

Does your employee want you to _understand_ that they need to get home early? Do they want to _participate_ in a project more? Do they need more _freedom_ to explore an idea?

Another important tool in ascertaining the needs and wants of an employee is the _Lazy_ question. Ask the question "How can I help you?" when an employee has nothing to offer but complaints about a situation.

The Lazy question sets up a positive coaching moment. Asking this question helps you check if an employee is asking you for something or just wants to let off steam. It also clarifies the issue by pushing your employee to get to the point. While it's a direct question, it also will help you earn your employee's respect.

When you ask the Lazy question, you show your employee that you want to know what _they_ want. Understanding an employee's wants will set you apart from many other managers who just don't care.

> _"You want your people to feel that working with you is a place of reward, not risk."_

### 5. Balance your schedule and make space for learning by posing the Strategic and Learning questions. 

Do you often jump at an opportunity without assessing the situation thoroughly? Here's a couple of tips and key questions to help you better gauge the risks and benefits of any opportunity.

In short, it's unwise to say "yes" to every single opportunity that comes your way. To ensure you give yourself time to think clearly, ask yourself the _Strategic_ question: "If you're saying 'yes' to this, what are you saying 'no' to?"

When you take on something new, you drop something else. Yet you shouldn't waste resources or neglect important projects. Use the Strategic question when you need to focus your energy on the projects that matter to you and the company.

Focus your decisions by identifying the projects, people or habits you'll have to change or abandon if you take on a new task. Consider that some tasks require you to hire new personnel or develop completely new organizational or working methods.

Whenever you say "yes" or "no" to a project, make sure you understand the reason _why_. Here's a tip: never respond to a request right away. Get as much information as you can before you make a commitment.

Ask yourself: What will this new opportunity demand of you? What's the deadline? How much time will it take? What's the reason for it? What else needs to be accomplished before it can be finished?

In addition to strategizing effectively, another important part of coaching is making room for employees to learn.

This isn't an easy task, as people don't automatically internalize new information or quickly build new habits. In general, people learn when they're able to _reflect_ on new information or processes. Reflection is what makes a lesson "click."

To guide your employees toward that "click" moment, use the _Learning_ question. At the end of every coaching session, ask your employee: "What was most useful for you?"

Asking the Learning question helps to guide your employee to reflect on the session so he can keep building his skills and learning daily.

### 6. A good coach doesn’t just know what to ask, they know how to ask it. 

This happens to every coach. You try to hold your tongue, but then, you accidentally blurt out a bit of advice. That's not coaching!

It's parenting, or worse: it's preaching. You now know that coaching is about questions, so how do you make sure your coaching practice _stays_ about questions?

A good coach knows _how_ to ask questions. But spouting a series of questions can easily make an employee feel as if he's being interviewed or even interrogated. As a coach, you don't want to make your employees feel uncomfortable.

So put an employee at ease by asking one question at a time. Don't beat around the bush, however. Skip the small talk and cut to your first question; it saves both of you time.

Ask "what" questions instead of "why" questions. Don't make the employee feel like she needs to be on the defensive with a question like, "Why is that on your mind?" Ask instead, "What's on your mind?"

It's also important to avoid asking rhetorical questions. Questions that start with, "Did you consider…?" or "Have you thought about…?" aren't genuine questions — they're _advice_ with a question mark at the end!

Make sure you _listen_ to your employees, too. Don't be afraid of silence. Silence tends to make people uncomfortable, but it's an asset in coaching sessions. When you're silent after you ask a question, you give your employee time to think about what she wants to say.

And when an employee gives you an answer, nod or summarize her thoughts to show that you've understood. Doing so shows that you care and encourages the employee to share more.

Finally, use every available channel to be a positive and effective coach. Perhaps your team members stay in touch via email or messaging software such as Slack. Remember that you're still wearing your coaching hat when you communicate through these tools. Every interaction you have with employees is a coaching opportunity.

### 7. Develop a long-term coaching habit so you can remain an expert coach for the rest of your life. 

Learning _what_ to do and _doing it_ are two different things. Let's go over some ways you can stick to the tips you've learned by making them a habit in your coaching practice.

It's not easy to change behavior. In fact, according to a Duke University study, 45 percent of a person's behavior stems from habit alone. And it's difficult to change a habit if you don't even think about it any more!

To develop new habits, then, you need to put _theory_ into _practice_.

In recent years, neuroscientists and behavioral economists have gained insight into how humans develop and maintain habits. There are five events that need to occur for a habit to form: _cause, trigger, mini-habit, training_ and an _action plan_.

Let's go through each event. A _cause_ is the reason you want to change current behavior, like wanting to avoid giving advice, as it's a hallmark of poor coaching. Once you know what you want to change, you identify your _trigger_ — the moments that encourage you to offer advice. When you know your trigger, you'll be prepared to deal with it.

Your _mini-habits_ are the seven coaching questions you've learned in these blinks. Practice them as often as possible. That's your _training_!

Finally, outline an _action plan_ to fall back on when you slip up. Everyone makes mistakes, so you just need to figure out how to get back on track.

Importantly, write down your action plan. Your plan could read, "The next time John stops me in the hallway with a question, I'll ask a coaching question instead of giving advice."

Once you put theory into practice, you'll develop a solid coaching habit that will stick with you for the rest of your life. Good luck — and happy coaching!

### 8. Final summary 

The key message in this book:

**A good coach doesn't just spout advice to a team. A good coach instead guides employees toward self-sufficiency in a positive, caring way. Use key coaching questions and truly** ** _listen_** **to your employees to figure out what they need and want. Empower them daily so they can lead themselves.**

Actionable advice:

**Create a coaching support group.**

Find other people who are looking to develop a coaching habit. Check in with each other often and share experiences and strategies. You'll all benefit when you work together, and your employees will, too!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Leadership Challenge_** **by James Kouzes and Barry Posner**

In _The_ _Leadership_ _Challenge_, James Kouzes and Barry Posner explain how anyone can become a better leader. Citing various examples from their 25 years of experience and extensive research, the authors present their theories on what makes a successful leader, and give practical advice on how to learn good leadership behavior.
---

### Michael Bungay Stanier

Michael Bungay Stanier is the author of a number of books, including the best-selling title, _Do More Great Work_. He was the first recipient of the Canadian Coach of the Year award.

