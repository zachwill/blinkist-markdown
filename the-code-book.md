---
id: 569e3616f16f3400070000b4
slug: the-code-book-en
published_date: 2016-01-21T00:00:00.000+00:00
author: Simon Singh
title: The Code Book
subtitle: The Science of Secrecy From Ancient Egypt to Quantum Cryptography
main_color: C29E66
text_color: 806843
---

# The Code Book

_The Science of Secrecy From Ancient Egypt to Quantum Cryptography_

**Simon Singh**

_The Code Book_ (1999) lays out the long and intriguing history of secret communication. These blinks will take you on a journey from Ancient Greece to the modern-day NSA, detailing innumerable stories of cunning, determination and deceit along the way.

---
### 1. What’s in it for me? Trace the evolution of secret messaging. 

When you were a kid, did you ever invent your own code to exchange top-secret messages with your best friends? There's something profoundly fascinating about secretive messaging, and humans have been at it for many centuries. 

Cryptography has usually been used by the mighty, but also by those seeking power themselves. It was instrumental in historical ruses and schemes, and the very strength of a code had the power to change the course of history; it could determine which of two conflicting nations would triumph in a war, or whether someone would live, die, become king or spend the rest of his life in prison. 

These blinks will lead you through some of these fateful events, right up to the twentieth century, when codebreakers changed the course of World War II. You'll find out about the increasing sophistication of cryptography and get to know how both cryptography and code breaking are enhanced by information technology. 

In these blinks, you'll also discover

  * ways to encrypt your private letters, keeping them safe from colleagues or nosy siblings;

  * how a weak code had a _very_ serious impact on Mary, Queen of Scots; and

  * why your government will do everything it can to keep you from using the next state-of the-art encryption method.

### 2. Secret codes developed early on in human history and evolved quickly. 

While secret codes might seem like a relatively modern phenomenon, the earliest known form of _cryptography_, that is, the practice of concealing the meaning of a message, actually dates back to the fifth century BC! It was at this time that Greece, faced with the constant threat of being conquered by Persia, realized that secure communication was essential. 

The result was cryptography, a field that simultaneously developed two distinct branches: _transposition_ and _substitution._

Transposition works by rearranging the letters of a word or sentence to produce a _cipher_, a secret method of writing. For instance, the _rail fence cipher_, a popular form of transposition, alternates the letters of a message in a zigzag pattern that moves between two consecutive rows. 

The other method, substitution, is a system wherein one letter stands for another. For instance, A=V, B=X and so on until every letter of the alphabet has a substitute pair, thereby forming a _cipher alphabet._ Since this process forms an alphabet that replaces the conventional one, it is referred to as a _monoalphabetic cipher_. 

For example, one of the simplest forms of substitution is called the _Caesar shift cipher_, so named because it was favored by Julius Caesar himself. It works by using the standard alphabet but shifting the letter it begins on by a set number of characters. So, if you shifted the alphabet three places then A=D, B=E, C=F and so on. 

However, simple Caesar shift ciphers only fooled dedicated adversaries for so long and eventually the _keyword cipher alphabet_ was formed, adding a twist to the monoalphabetic cipher. This cipher is similar to the Caesar shift except the alphabet starts with a keyword or phrase, at which point the conventional alphabet resumes but without the letters used in the keyword. 

For instance, if "Caesar" was the keyword, the alphabet would begin CAESRBDFGHIJK… Therefore A=C, B=A, C=E, D=S, and so on.

### 3. The death of Mary, Queen of Scots sparked cryptographic advances. 

So, ciphers emerged early on, but they were rapidly countered by _cryptanalysts_, experts who devise strategies to crack codes. And of all those in the field, none were more skilled than the Arab cryptanalysts who, in 750 AD, invented an extremely effective cipher-breaking tool: _frequency analysis,_ which is used to crack a monoalphabetic cipher. 

Every written language uses certain letters and words more often than others, and frequency analysis is designed to identify these characters in a _monoalphabetically_ encrypted message; knowing which letters are used the most is a major aid in deciphering a code. 

For example, in written English, the most commonly used letters are E, T, A, O, N, S and R. So, say you had an encrypted message that began "_piuub gkulvwpev!"_ By knowing which letters are most common in the language being used, a cryptanalyst can substitute them for the most common letters in the code — and can see that it now reads "_p_ ERR _b_ _gk_ R _l_ ST _p_ AS!" With these letters filled in, figuring out the rest is easy. 

So, as code breakers became more advanced, cryptographic methods had to improve. But between the second and fifteenth centuries, only small security improvements were made to ciphers, like the addition of _codes –_ a cryptographic tool that replaces whole words or phrases with other symbols. For instance, a _nomenclature_ is a cipher alphabet that uses codes. Unfortunately, this strategy isn't as secure as it might sound, and it took a royal beheading to prove that ciphers needed to be refined. 

On February 8th, 1587, Mary, Queen of Scots was executed after she was tried and found guilty of conspiring to kill her cousin, Queen Elizabeth. While Mary pled innocent, she had no idea that her correspondence, masked through a monoalphabetic nomenclature cipher, was easily being deciphered for Queen Elizabeth. 

At this point, it became clear that cryptanalysts were too advanced for current methods and new cryptographic strategies were necessary. After all, if royalty was falling victim to code breakers, who was safe?

### 4. In the sixteenth century, a new cipher emerged, one that was incorrectly believed to be unbreakable. 

Frequency analysis was challenging the security of the monoalphabetic cipher. In the sixteenth century, a Frenchman named Blaise de Vigenère developed a cryptographic technique that used 26 distinct cipher alphabets in a single message — in other words, a _polyalphabetic cipher_. 

Vigenère's cipher was first published in 1586 and called "Le Chiffre Indéchiffrable", or _the unbreakable cipher_. It works like this:

First you create what's called a Vigenère square and codeword. The square contains 26 rows, each containing a cipher alphabet shifted one place relative to the one above it. For instance, if the first row is BCDEF, the second row would be CDEFG and so on. 

The codeword is used to indicate which alphabets you are using. For example, with the codeword WHITE, you could build a cipher that uses five different alphabets. That's because the first letter would correspond to the 22nd cipher alphabet, which would begin with the letter W, the second letter to the seventh alphabet, which begins with the letter H, and so on. 

But while the Vigenère cipher is more secure, it isn't practical and certainly isn't unbreakable. It was too complex and time consuming to gain traction with the military, whose communications depend on agility and simplicity. The popular ciphers of the seventeenth century, like the one favored by Louis XIV, were simply enhanced monoalphabetic ciphers, using numbers and the substitution of syllables rather than letters.

However, as telegraph communication caught on in the eighteenth century, the Vigenère cipher did too. While any mailman could drop a letter in a box, a telegraph operator had to read a message to deliver it, which meant an obvious decrease in privacy. 

Then, in the nineteenth century, the British cryptanalyst Charles Babbage found that, even using multiple alphabets, there were still signs and repetitions in polyalphabetic ciphers that pointed to the length of the codeword in use and enabled deciphering. 

You've now learned how cryptography has played a role in history, but let's get back to basics and learn more about the connections between cryptography and language.

> _"At the end of the 19th century, cryptography was in disarray."_

### 5. Cryptanalysis played an essential role in deciphering ancient Egyptian and Greek languages. 

Did you know that the US military had the idea of employing Native American Navajos as radio operators during World War II? The logic was that their language could never be deciphered as there was no written record of it. But this interesting bit of history isn't the only time a little-known language joined forces with cryptography. 

The Rosetta Stone, unearthed in 1798, bore the same message in three different languages: Greek, Demotic and in hieroglyphics that had never been seen before. The English linguistic prodigy Thomas Young jumped at the opportunity to decode the hieroglyphs. 

Armed with the Greek translation as a guide, he went to work and solved part of the riddle by discovering that the _cartouches_ — encircled hieroglyphs in the text — actually represented the names "Ptolemy," an Egyptian ruler, and "Bernika," his wife. 

Based on this discovery, the French linguist Jean-François Champollion continued to work on the stone, finding the cartouches for Cleopatra and Alexander. These names alone were enough information for Champollion to decode the ancient hieroglyphics and publish his results in 1824. 

But if decoding long lost hieroglyphs seems like an awe-inspiring feat, the ancient language of _Linear B_ was even more challenging. 

In 1900, clay tablets dating to 1375 BC were found on the island of Crete, sparking debate about which language was represented on the oldest tablet. Cryptanalysts called it Linear B, and it remained a complete mystery until the 1940s, when the English architect Michael Ventris began linking symbols to important Greek locations. 

Ventris soon identified shipping hubs like Knossos and Tylissos, giving himself the clues he needed to decipher the language. Academics were baffled when Ventris announced that Linear B was in fact an ancient version of the Greek language, and his discovery went down in history as "The Everest of Greek Archaeology."

So, the connection between cryptography and linguistics is unmistakable, but the former's impact on world events can't be understated. With the outbreak of World War II, cryptography would once again determine the course of history.

### 6. Wartime led to significant advances in cryptography. 

The invention of radio and the increasing secrecy necessitated by the World Wars made finding secure methods of communication more important than ever. So, during World War I, the US military started working on a virtually unbreakable system called the _one-time pad cipher_. 

This cipher, considered the "holy grail of cryptography," is a variation on Vigenère's system. It uses two identical books, one held by the sender and one by the receiver. Each page of the book contains a unique, randomly generated 24-letter codeword. After the sender uses the random code to deliver a message, each party then destroys the page that was used, meaning each code is only used once. 

While it's mathematically proven that this system is indecipherable, it's also totally impractical. The military sends hundreds of messages a day and generating entirely random keywords isn't as easy as it might sound. 

In addition, having to constantly distribute new books also presents a problem. That being said, for encrypting correspondence between people with ample resources, say two world leaders, the one time pad cipher works fantastically well. 

So, a different system was necessary, and with the creation of the _Enigma_, cryptography became mechanized. 

In 1918, the German inventor Arthur Scherbius found a new way to make ciphers by constructing a mechanical device called the Enigma. It consisted of a keyboard, a scrambling unit composed of cipher discs and a display board. The user simply typed a letter and the configuration of cipher discs dictated which cipher letters appeared on the display.

While Scherbius struggled to sell his invention in the peace that followed World War I, in the years leading up to World War II, the German military's interest piqued and soon had 30,000 Enigma machines in use. The sheer scale of this distribution enabled a level of encryption that was unheard of at the time, and Enigma was deemed impenetrable.

> _"Gentlemen should not read each other's mail." - Henry Stimson, former US Secretary of State_

### 7. Cracking the Enigma code was a huge challenge that decided the course of World War II. 

By 1926, the British were keeping close tabs on German communications and began intercepting some odd ciphers. This was the work of Enigma and it was baffling the Allied cryptanalysts. But, ironically enough, a method that the Germans had devised to increase their security would eventually expose Enigma's weakness.

German communications relied on two keys to send their messages. All correspondence would use a daily key, but every message would start with a new key solely for decrypting that message. To prevent errors, the sender would repeat the message key twice — a simple three-letter phrase that gave instructions on how to set the scrambler discs. 

The Polish cryptanalyst and mathematician Marian Rejewski seized on this repetition by studying the three-letter message keys of every intercepted message. Within a year, he'd assembled a catalog of every possible scrambler setting the Enigma could generate — 105,456 configurations in total. 

So, message keys became fingerprints that revealed the day key and Enigma settings. 

However, if it weren't for Alan Turing and the cryptanalysis team at Bletchley Park, the war might still have dragged on. The Allies knew the Germans might recognize their folly of repeating a message key and Alan Turing was assigned to find another way to break the Enigma cipher. 

Turing, like Rejewski, went to work on old messages, identifying patterns. For instance, every morning the Germans would broadcast a weather report. Closely examining the reports uncovered the cipher word for "weather." 

But Turing's real genius was to mechanize Rejewski's cataloging process, thereby connecting Enigmas electronically until they gave the right combination to reveal the key. Turing and his team's work gave the Allies advance knowledge of bombing raids, and even details on the German forces the Allies would face at Normandy. It's widely accepted that their essential work led to a shorter war and fewer casualties. 

Enigma marked a new phase in the development of cryptography, but the field didn't end there. Next, we'll explore how modern cryptography developed, and where it's going.

### 8. The rise of personal computers created new cryptographic methods and forms of security. 

Enigma and its eventual deciphering made one thing clear: computing was the future of cryptography. As computers were made commercially available, new forms of secure communication emerged. The expansion of commercial computers into the businesses world in the 1960s necessitated a new form of security for financial transactions and trade negotiations. 

The result was IBM's _Lucifer_, a system that translates written messages into binary code, breaks it into 64 blocks and then scrambles it 16 times according to a given key. By 1976, Lucifer was approved by the US National Security Administration (NSA) as the _Data Encryption Standard_, or _DES_. 

But a better method for distributing keys was still missing. To this end, three cryptographers, Whitfield Diffie, Martin Hellman and Ralph Merkle, joined forces to find a way for people to securely exchange encrypted messages over huge distances. 

Up until that point, cryptography assumed that if someone sent an encrypted message, the recipient would need the sender's key to decipher it. So, unless people met in person, the key would need to be mailed, thereby making it prone to interception. 

However, this team came up with another option: the _Diffie-Hellman-Merkle key exchange_, which works as follows: 

Upon receiving an encrypted message, the recipient encrypts it again using his own key. Then, the twice-encrypted message is returned to the sender who removes his own encryption before sending it back. Now the only encryption is the recipient's own and he can easily decode it. 

But there's always room for improvement, and in 1977, three scientists at MIT created the _RSA cipher_, made even more secure through its use of extra safe keys based on the products of prime numbers. 

These keys are especially safe because there's no simple, general-purpose algorithm for determining a number's prime factors; it thus tends to be a highly laborious enterprise. For instance, while it's not a problem to do this math on small products like 21, whose prime factors are 3 and 7, higher numbers mean much more work.

### 9. The future of cryptography depends on advances in computers – and on political developments. 

Enigma changed the game of cryptography, and ever since its invention, the field has transformed from one based on language to one heavily influenced by mathematicians. In fact, physics may now hold the key to cryptography's future, because cryptography has finally realized its ultimate goal of outpacing cryptanalysis. 

How? Through the impenetrability of the DES and RSA ciphers. 

These days, even the NSA, who has regulated the complexity of DES keys, can't keep up with the sheer quantity of ciphered data and run the necessary computations to find prime factors. This means the only way these ciphers will ever be broken is through a technological and theoretical breakthrough. 

But that advancement in codebreaking may just lie in quantum computers. The most logical way to unlock modern ciphers would be to make multiple computations simultaneously. Doing so would enable a cryptanalyst to achieve what would otherwise take some 17 years in mere minutes. 

So how do quantum computers work?

Well, they run on _qubits_, the quantum equivalent of a standard computer's binary bit. But in a quantum computer, spinning particles stand in for the 1s and 0s of binary. Each qubit can operate independently of the others, which means that 250 qubits can run 1075 simultaneous computations. 

But cryptographers have recognized this potential and are already working to retain their advantage. In fact, quantum physics might also enable new ways of building extra secure ciphers and keys. 

For instance, physicists have already succeeded in sending _photons_, which are quantum particles of light, over huge distances using fiber optic cables. Furthermore, photons can be ordered in a way that creates perfectly random keys for the secure one-time pad cipher and are sensitive enough to rapidly show signs of an attempted third-party interception. 

Naturally, this technology could mean incredibly secure ciphers; in fact, they could be _so_ secure that governments will forbid the public and potential criminals from using them.

### 10. Final summary 

The key message in this book:

**For hundreds of years, militaries and governments around the world have used encrypted messages to win wars and hide their secrets; meanwhile, codebreakers have been honing their craft to decode messages more efficiently. But while cryptography has a long history, the modern era and computer technology have completely transformed the practices of both encrypting and deciphering messages.**

**Suggested further reading:** ** _A Mind for Numbers_** **by Barbara Oakley**

_A Mind for Numbers_ offers insight into the way our brains take in and process information. It outlines strategies that can help you learn more effectively, especially when it comes to math and science. Even if mathematical or scientific concepts don't come naturally to you, you can master them with the right kind of dedication and perseverance — and this book will teach you how.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Simon Singh

Simon Singh holds a PhD in physics from Cambridge University. He wrote the bestselling book _Fermat's Enigma_ and directed the award-winning documentary, _Fermat's Last Theorem_.

