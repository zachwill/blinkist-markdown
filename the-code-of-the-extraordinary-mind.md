---
id: 57d2824673d9bc0003c6819c
slug: the-code-of-the-extraordinary-mind-en
published_date: 2016-09-12T00:00:00.000+00:00
author: Vishen Lakhiani
title: The Code of the Extraordinary Mind
subtitle: 10 Unconventional Laws to Redefine Your Life and Succeed On Your Own Terms
main_color: 9F957A
text_color: 736C58
---

# The Code of the Extraordinary Mind

_10 Unconventional Laws to Redefine Your Life and Succeed On Your Own Terms_

**Vishen Lakhiani**

_The Code of the Extraordinary Mind_ (2016) unveils a method for overcoming the madness of everyday life, one that enables anyone to stand out from the pack and become an extraordinary individual. The author lays down ten laws that anyone can easily follow to experience a radical transformation and find meaning and happiness in each day.

---
### 1. What’s in it for me? Crack the code of your extraordinary mind. 

The human mind is simply remarkable. It's a vast source of cognition and reason that allows us to recall vivid memories, perceive what's going on around us and cast judgments on our own and other people's actions.

Even so, the mind is not necessarily programmed to maximize our happiness or contentedness in life. Rather, it can make us follow hopelessly old-fashioned rules and beliefs that are passed down from generation to generation. But what if you could remove these restrictions and start fresh?

These blinks provide a thorough guide on how to avoid the pitfalls of your own mind. They use a framework based on the ten laws of extraordinary living _,_ separated into four stages.

The first two rules focus on discovering how we are shaped by the world around us, while the next three help us understand how we can change our perception of the world. Laws six, seven and eight explain how we can create stronger mindsets, and the final two show us how we can employ these mindsets to change the world. Altogether, these laws take into account the complex code of the human mind and will guide you toward a more extraordinary life.

In these blinks, you'll discover

  * what bullshit rules are, and why you should question them;

  * why someone can have an embarrassing past of wearing thick, ugly glasses and still be attractive; and

  * what it means to be unfuckwithable.

### 2. Transcend the culturescape and question society’s bullshit rules. 

As we move through life, we're constantly told what we _should_ do or how we _should_ live. Our culture contains a number of outdated beliefs that dictate what kind of job and education we _should_ have and how we _should_ dress and look.

Taken together, these beliefs make up today's _culturescape_ — a set of rules and norms on how to love, eat, marry and make a living.

But if you want to be extraordinary, the first law for an extraordinary life is to _transcend_ your culturescape.

This is what the author realized he had to do. Until the age of 19, Vishen Lakhani lived in Malaysia, where it was the norm for families and teachers to push kids into becoming an engineer, a lawyer or a doctor. And since he was a smart student, he was told that these were the rules he needed to follow in order to succeed.

So, he went along with these expectations, giving up his hobbies of photography and performing arts to become a computer programmer. However, when he "succeeded" by landing a top job at Microsoft, he realized this wasn't the life he was meant to lead — and he quit.

To stand out and become extraordinary, Lakhiani realized he needed to transcend his culturescape and selectively choose which rules to follow and which ones to ignore.

This leads us to the second law for an extraordinary life: question the bullshit rules.

A bullshit rule, or, to use the author's terminology, a _Brule_, is an outdated rule from the culturescape that we blindly follow, even though it can hold us back from pursuing our dreams.

Take the _College Brule_, for example. Society tells us we need a college degree to guarantee success; but not only does a degree not guarantee us anything, it can actually hinder your success by weighing you down with massive student debts.

Steve Jobs questioned this Brule by dropping out of college and going on to become an extraordinary man. He didn't need a degree to be a radical innovator, invent the smartphone and revolutionize multiple industries.

> _"Outside the rules of physics and the rules of law, all other rules are open to questioning."_

### 3. Extraordinary minds practice consciousness engineering and create self-encouraging models of reality. 

What are the chances you still use a computer that runs Windows 95? Pretty small. You almost certainly care about your computer running efficiently, so you make sure to update the operating system every few years.

Well, this is the same kind of care that you should put into your mind as well.

Updating your mind's hardware and software is what the author calls _consciousness engineering_. It's the third law for an extraordinary life.

Just like your computer, your mind contains both hardware and software.

The hardware contains your model of reality. This is where you hold your beliefs about the world, whether they're about politics, economics, marriage or schooling.

Meanwhile, the software contains your systems for living. These are your daily habits and they determine how you utilize the hardware.

For example, say your hardware and model of reality hold that it's wrong to kill animals for food. In turn, your software and system of living would tell you to buy and eat as a vegan would.

Now, _consciousness engineering_ is a way of understanding that personal growth depends on your ability to carefully choose — and frequently update — your hardware and software.

We can update our hardware by following the fourth law for an extraordinary life: rewrite your models of reality.

Extraordinary people do this by embracing a mindset that helps them achieve their goals and create positive self-esteem.

The author didn't always feel good about himself. As a child, Lakhiani wore thick glasses and suffered from terrible acne, which led to low self-esteem due to his appearance.

But that all changed during college. At a party, Lakhiani found himself talking to the prettiest girl in the room, and she told him that he was one hot dude.

This experience allowed Lakhiani to update his hardware. He built a new and encouraging model of reality that told him he was now an attractive man, and his dating life took off.

In the next blink, we'll move on and look at how to update your software and your systems for living.

> _"As I've learned about the power of beliefs, I've chosen specific models of reality to help me stay healthier and younger."_

### 4. Extraordinary minds upgrade their systems for living and are able to bend reality. 

Naturally, when you revise your hardware's model of reality, it means your software, as well as your actions based on these beliefs, must also be revised.

This leads us to the fifth law for an extraordinary life: upgrade your systems for living with a three-step approach of _discovering, refreshing_ and _measuring_.

Extraordinary minds continuously optimize their systems for living in order to achieve their goals.

Think of it this way: your systems for living are like apps that are designed to reach goals in your life or solve specific problems. If one app doesn't work or becomes outdated, you either upgrade the app or replace it with a better one.

So the first step to upgrading is to _discover_. You can do this by reading books, talking to people and exploring the world around you for inspiration. Think of it like browsing the App Store.

Second, _refresh_ your systems regularly. For example, every year the author refreshes his system for exercise and fitness; one year he tried Christine Bullock's Total Transformation program, and the next he moved on to kettlebells.

Third, _measure_ how effective your systems are. The author measures the effectiveness of his updated fitness systems simply by keeping track of which notches he's using on his favorite belt.

With your updated software and systems of living, you can get started on re-coding your mindset. This involves the sixth and seventh laws for living an extraordinary life.

Let's start with law number six, the ability to _bend reality_.

Now, this isn't a magic trick; it's a new system for living that will dramatically increase your happiness.

Being able to bend reality means that you can hold onto your exciting goals for the future while making sure that your happiness is firmly rooted in the present.

The author realized the importance of having this frame of mind while building his own company. Rather than being happy or excited about his venture, Lakhiani was constantly stressed about whether or not he'd reach certain revenue goals.

So he bent his reality by focusing on the here and now. He decided to make fun and happiness a key part of his daily life. Soon thereafter, his company's revenues went through the roof.

> _"Keep the big goals — just don't tie your happiness to your goals. Be happy now."_

### 5. Extraordinary minds practice blissipline and create a vision for their future. 

Everyone knows the feeling of bliss that accompanies happiness — but chances are you also know how short-lived this feeling can be.

Luckily, with an extraordinary mind comes the ability to experience bliss for longer periods of time. But how can you achieve this?

You'll need to focus on the seventh law of extraordinary living by practicing _blissipline_, or the discipline of regular and prolonged bliss.

Blissipline starts with gratitude. It has been scientifically proven that people have more energy and experience less depression when they practice gratitude.

You can do this by embracing an idea from entrepreneurial coach Dan Sullivan, who suggests that you switch to your _reverse gap_.

What does he mean by this? Well, most of us are trained to see the gap between our present selves and who we want to be; this is called _the forward gap_. But the problem with focusing on the forward gap is that we're always keeping happiness out of our reach and in the future.

Instead, we should reverse the gap and look to our past to appreciate and be grateful for what we've achieved so far. This allows us to practice blissipline and experience gratitude on a daily basis.

With our blissipline in place, it's time to focus on the final three laws that will help you change the world. Let's start with the eighth law for an extraordinary life: craft a vision for your future that corresponds with your happiness.

Creating a future vision is all about setting goals. However, these goals can be dangerous if you confuse _means_ _goals_ with _end goals_.

Means goals are the ones society tells us we need to have in order to be happy, such as a college degree and a safe job. But means goals rarely make us happy.

End goals, on the other hand, are a great source of happiness. These are the goals that bring us joy in and of themselves, and not because they adhere to the culturescape. A great example of this could be the unrivalled joy of seeing your child succeed at school, dance or sports.

Think about what _you_ really want to do, whether it's a visit to the top of Mount Kilimanjaro or writing the next great novel.

> _"The big problem with life in many industrialized countries is that far too often, we're expected to choose a career before we can legally buy a beer."_

### 6. The final steps to becoming extraordinary are to be unfuckwithable and to embrace your quest. 

All extraordinary people have goals. But their motivation for reaching them comes from _within_ ; they don't seek validation from outside opinion, and their sense of self-worth and happiness are not attached to outside values.

This is the essence of the ninth law for an extraordinary life: be _unfuckwithable_.

This means realizing that you depend only on yourself and that nothing anyone says or does can stop you from achieving your goals.

Being unfuckwithable consists of two components:

The first is to have self-fueled goals that aren't dependent upon others.

Say you decide on a goal of being closer to your children; what happens when your children naturally desire to be more independent? The success of this goal depends on their cooperation.

A better goal is simply to try and be the best parent possible. Since this is a self-fueled goal, nobody can stop you from achieving it.

The second component is to realize that only _you_ have what it takes to be happy and successful. This means that you need to resist the temptation to blame outside circumstances for what happens in your life.

Don't spend the day in bed depressed because someone forgot your birthday; instead, learn to let it go. After all, you can't control the actions of others.

Finally, the tenth law for an extraordinary life is to _embrace your quest_.

Extraordinary people usually have a calling — a quest to create positive change and make a meaningful contribution to the world.

This means that extraordinary people don't view work as something that keeps them from pursuing their dreams; in fact, it's quite the opposite.

Richard Branson, a great entrepreneur and a perfect example of an extraordinary individual, was asked how he kept his work-life balance in place. He replied that work and life are the same thing — both are part of living.

Branson found work that was completely in line with his calling and created a life that he could embrace completely.

So, now that you know the ten laws of an extraordinary life, it's your turn to embrace it.

> _"Extraordinary minds are not content to merely be in the world. They have a calling, a pull to shift things."_

### 7. Final summary 

The key message in this book:

**Anyone can live an extraordinary life. You can get started on this path by following the ten laws of an extraordinary life and transforming your existence. A meaningful life of true happiness is possible for us all.**

Actionable advice:

**Practice daily gratitude.**

In the seventh law, you learned about switching to your reverse gap and practicing gratitude by looking backward and appreciating what you've achieved so far.

Here's an exercise to help: each day, take a few minutes to think about three to five things you're grateful for in your personal life, and three to five things you're grateful for in your work life. But be careful; a lot of people turn this into a mechanical list-making exercise. Instead, focus on your true feelings and consider what makes you happy, optimistic, comforted, confident or proud.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Seven Spiritual Laws of Success_** **by Deepak Chopra**

_The Seven Spiritual Laws of Success_ (2007) is about the forces in the universe that can lead you toward the perfect job, increased wealth and well-being, strong relationships and, most importantly, the ability to enjoy what you have this moment. Discover the seven universal laws of success and tap into the endless energy and abundance of the universe!
---

### Vishen Lakhiani

Vishen Lakhiani is a Malaysian-born entrepreneur. He's the founder and CEO of Mindvalley, a company designed to educate and empower people, helping them reach their full potential. Lakhiani is a frequent speaker on improving business mindsets and company culture, and is also the founder of the Awesomeness Fest, a personal development event for entrepreneurs and authors.

