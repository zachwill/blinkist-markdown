---
id: 54214b2437303300083e0000
slug: the-communist-manifesto-en
published_date: 2014-09-26T00:30:00.000+00:00
author: Karl Marx and Friedrich Engels
title: The Communist Manifesto
subtitle: None
main_color: CD2939
text_color: CD2939
---

# The Communist Manifesto

_None_

**Karl Marx and Friedrich Engels**

_The Communist Manifesto_ is the result of a meeting of international communists in London. It vividly portrays the first common position of political communism regarding the class struggle between the working class and the capitalist bourgeoisie.

---
### 1. What’s in it for me? Finally understand the arguments for communism. 

People around the world are dying of starvation as others toss perfectly good food into the trash; companies profit from remote civil wars in which child soldiers are forced to slaughter one another; factory owners cash in as mothers sew cheap clothes in their factories for a meager wage so the West can shop at competitive prices.

All of these harrowing images demonstrate the raw exploitation that can be found in our modern capitalist system of economic production.

This, of course, is nothing new. Nineteenth-century communists saw similar circumstances in the factories of industrializing Europe, and they were horrified by it. Indeed, the political philosophy of communism rose from the suffering of the working class.

The classic political work _The Communist Manifesto_ offers a great introduction into the considerations and plans of the early proletarian movement. By reading these blinks, you'll understand the basis of communist political thought, straight from the source.

In reading these blinks, you'll discover

  * why nobody has a right to hold private property;

  * why our lives aren't much different than they were during feudalism; and

  * what you can do to fight against bourgeois oppression.

> _"A spectre is haunting Europe — the spectre of Communism."_

### 2. The class that controls the economy controls society. 

Why is it that we never see poor people in Congress or wealthy landowners sweating in factories? The wealthy always seem to inhabit positions of power while the rest of us are at their mercy. But how did this come to be? The answer lies in economics.

Changes in the economy drive changes in society. Every change in social relationships is triggered by a change in the _mode of production_, i.e., the method by which the necessities of life (food, shelter, transportation, etc.) are created.

The mode of food production in the age of hunter-gatherer societies, for example, was such that humans could only provide for their immediate communities. As a result, there were hardly any class distinctions within a community; people were more or less equal.

However, with the advent of farming, a more efficient mode of food production emerged. Suddenly there was an abundance of food, enough for farmers to sell to other people.

This change in the mode of production caused a hierarchy to develop between those who controlled the food supply and those who had to work for them. And so the first class system was created: the class that held economic power also held the political power.

Historically, all societies have organized into complicated hierarchies of conflicting classes. The lines that separate these classes can be traced back to their degree of control over the modes of production. In essence, the class that controls a society's wealth controls that society by using its position to subjugate the other classes.

In the Roman world, for example, slaves were not allowed to hold property, which amplified their oppression.

Things were similar in the feudal societies of medieval Europe: poor serfs were tied to their land and indebted to wealthy landowners. In other words, these landowners actually _owned_ them and forced them to work the land.

The relationship between these oppressive and oppressed classes, i.e., the _class struggle_, is what has driven history.

> _"The history of all hitherto existing society is the history of class struggles."_

### 3. The bourgeoisie dominates the economy and therefore society. 

You don't have to visit a medieval cathedral or castle to comprehend the vast amount of power kings and the church once had. But now they have relatively little power, and the power they once had lies in the hands of the mighty bosses of industry. In other words, the latter are today's dominant class. So how did they take over?

Their ascent to power took place after the downfall of the feudal system.

In the feudal system, the means of production (mainly agricultural land) was owned by the crown, its nobles, i.e., the aristocracy, and the church. Because they owned the land, they maintained control over the other classes. The only way for peasants to access land in order to grow or earn enough to survive was to sell themselves into servitude.

However, eighteenth-century industrialization brought alternative modes of production to feudal servitude, giving farmers an alternative form of labor to turn to for survival: factory work.

Farmers left in droves from agriculture to industry, and the old social system vanished. Oppression, however, did not vanish with it: a new powerful class emerged — the _bourgeoisie_, or the owners of industrial capital.

The rise of the bourgeoisie was revolutionary: feudalism was replaced with naked self-interest and the invisible hand of the free market.

However, the fundamental social hierarchy remained the same: workers were selling their labor for a _wage_ that wasn't equal to the amount of _capital_ (or the total wealth) they were producing.

In concrete terms, when a worker in a factory creates a chair, his wage isn't equal to the price of the chair. Rather, he only receives a fraction of its worth. The remaining capital goes into the hands of the bourgeoisie, which in turn grows their power even more.

Meanwhile, the bourgeoisie are constantly searching for more capital and more markets to exploit, which has turned capitalism into a global phenomenon. Due to the growing power of the bourgeoisie, _capitalism_ has become the defining economic doctrine of our time.

> _"The weapons with which the bourgeoisie felled feudalism to the ground are now turned against the bourgeoisie itself."_

### 4. Although the working class is suppressed, it is becoming increasingly unified. 

Today, an oppressive king has been replaced with the bourgeoisie. With them in control, there's a new oppressed class as well: the working-class _proletariat_.

If there had ever been charm in work, it is certainly gone by now. You don't work and produce for yourself, but for a factory owner who will pay you only what you need to survive: not because he cares for you, but so you can continue working for him and help his factory grow.

Thus, you'll live as long as you find work, and die soon after as your labor is no longer sufficient in increasing the wealth of the bourgeoisie. In this way, workers themselves have become little more than utilitarian commodities.

As if that weren't bad enough already, with each increase in the _division of labor_, i.e., when the steps required to produce something become smaller, simpler and more specialized, workers become increasingly insignificant.

Rather than individuals producing an entire product as they would have in the past, they now suffer the monotony of cranking a single factory lever all day long for a miniscule wage. All aspects of humanity no longer matter under capitalism: age, sex, desires and aspirations all lose their validity; individuals are merely the appendages of a machine.

As a result of the sheer scale of industrial production, there are more workers today than ever before. Moreover, their means of communication have increased with their agglomeration in the cities, and differences among them have been erased through their reduction to commodity status.

Although workers are forced into direct competition against one another in terms of labor and wage, they've realized that only together can they challenge the system erected by the bourgeois class. The proletariat is a revolutionary force, and workers have unionized to stand together in order to create a more just society.

Since the birth of the proletarian class, workers have struggled against the bourgeoisie. Soon, capitalists will realize that they've given birth to their own gravediggers.

> _"The proletariat alone is a really revolutionary class."_

### 5. Communists and the proletariat have a common aim: to abolish private property and claim political power. 

Workers' rage is obvious, but the question remains: How can the proletariat translate its rage and frustration into a political movement that brings about fundamental change? The _communists_ have a plan.

Communists want to unite the workers for a common cause. While unions sprout up in all countries and industries, their networks are still far too small in comparison to the global power of the bourgeoisie, with their economic power and international connections.

So international communists have pledged to represent the interests of the proletariat as a _global class_. These communists come from working-class parties and organizations all over the world, and bring with them an understanding of the bigger picture — a global proletarian movement that transcends national boundaries.

And in order to enact their vision, communists want to abolish private property.

Many people find this confusing because they believe earning money and acquiring private property under capitalism is the only thing that motivates people to work. They think that abolishing private property would have fatal economic consequences, as nothing would ever be produced.

But the proletariat doesn't create any property for _itself_ through wage labor. Workers are paid the bare minimum to secure their meager existence, which is certainly not enough to build up capital for themselves.

The only ones who benefit from the property created by wage labor are the bourgeoisie, and it is precisely this property that serves as the basis for its domination over the proletariat. In essence, the proletariat creates the instruments for its own exploitation under capitalism.

Therefore, there is no reasonable basis for ownership of individual or private capital under proletarian rule: the creation of capital requires an entire society and should thus be held by everyone.

For this reason, the communists' plans are immediate and decisive: the united proletariat must topple the bourgeoisie and usurp political power in order to redistribute wealth to everyone across society.

> _"In your existing society, private property is already done away with for nine-tenths of the population."_

### 6. The ten demands of the International Communists. 

In order to usher in a fair and just communist society, ten conditions must be met:

  1. The exclusive rights to land create and perpetuate class distinctions. Therefore, all property in land and all rents of land shall be expropriated for public purposes.

  2. In order to ensure that wealth does not accumulate and concentrate at the top, a heavy progressive tax must be introduced.

  3. Inheritance concentrates wealth into the hands of people who are already wealthy and thus cements class distinctions. Therefore, all right of inheritance should be abolished.

  4. Emigrants abroad have no use for their property in their native country. Therefore, we shall confiscate that property, as well as the property of those who would undermine the rule of the proletariat.

  5. Credit institutions exist merely by virtue of their capital without making contributions to production. Therefore, the State will have an exclusive monopoly on credit by means of a national bank with State capital.

  6. Freedom of movement is a right every citizen should enjoy, and for which society is responsible. Thus, the centralization of transport will be put into the hands of the State.

  7. Much of our land remains underused. We should extend the instruments of production now owned by the State and use them to cultivate the wastelands and improve the soil.

  8. As it stands, the many work for the benefit of the few. Therefore, everyone should be required to work. In addition, industrial armies, especially for agriculture, should be established.

  9. Promote the gradual elimination of the distinctions between town and countryside by more equitably distributing the population.

  10. Our lives are determined in part by the quality of our childhood. Therefore, education will be free in public schools, and children's factories shall be abolished in their current form.

> _"We shall have an association, in which the free development of each is the condition for the free development of all."_

### 7. All common criticisms of communism are easily rejected. 

Obviously, the communists' radical plan for the transformation of society has met with strong opposition. But the criticisms are hypocritical and invalid. Here are some of the most common:

Communism has been accused of working against the family, but this isn't true. Because they demand that children be educated publicly instead of at home, communists are accused of degrading traditional family values and bonds.

But we can only scoff at this criticism. Families have _already_ been successfully ruined by capitalism! Mothers work 60 hours a week and children are enslaved in factories almost as soon as they can walk, reduced to little more than commodities.

Communism simply proposes that education be free, and free from the influence of the ruling class.

Others would have you believe that communism deprives workers of their nationality. However, workers have no country to begin with.

Workers aren't represented by the great values and history that nationalists attribute to their nations. Rather, they are represented by their labor and their position as oppressed subjects.

Communists can't take from the workers what they don't have. While communism will begin with political control on a national level, communists aim to further blur national borders. Workers of all countries have already become increasingly similar through standardization, international trade and a common cause, which will make this process easier.

Furthermore, some lament that communism will undermine religion. Indeed!

Communism works to replace the existing set of morals, as they were shaped by the oppressive power relations of our capitalist society. Traditionally, religion's primary purpose has been to support the ruling classes. In feudal society, it supported the king's right to rule; under capitalism, the church stands against socialist and communist movements.

The church's role in upholding class hierarchy simply means that religion must be tossed out with the rest of the old world.

The critiques that communism faces are weak. All they demonstrate is that many people have still not understood the causes and demands of the proletarian movement, which exemplifies the reason why this manifesto is so important.

> _"Working men of all countries, unite!"_

### 8. Final summary 

The key message in this book:

**The history of mankind is defined by a struggle between ruling classes and their subjects, which has peaked in the suppression of the working class by the capitalist bourgeoisie. The communists will unite the global proletariat and take political control by revolutionizing the modes of production.**

Actionable advice:

**Workers of all countries, unite!**

Only through international solidarity can we break the chains that hold us to the factories and to the capitalists who own them. The bourgeoisie have never shared our interests, and they will never willingly give up their domination over the proletariat. It is up to us, then, to unite against our oppressors through revolutionary action.

**Suggested** **further** **reading:** ** _Free to Choose_** **by Milton Friedman**

_Free to Choose_ explores the relationship between freedom and the choices an individual is allowed to make in regards to the economy. Friedman reveals to us that economic freedom is an essential part of liberty. He details the myriad ways in which government regulations and interventions chip away at our fundamental right to make decisions in our own self-interest.
---

### Karl Marx and Friedrich Engels

Karl Marx was a German philosopher who lived from 1818 to 1881. He is one of the main thinkers in communist political thought and established his own socialist tradition, _Marxism_, in his seminal work _Capital._

Friedrich Engels was a writer and social scientist who lived from 1820 to 1895. He was one of Marx's political companions and helped co-author several of his works.

