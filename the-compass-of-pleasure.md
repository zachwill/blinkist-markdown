---
id: 55d1de70688f670009000131
slug: the-compass-of-pleasure-en
published_date: 2015-08-21T00:00:00.000+00:00
author: David J. Linden
title: The Compass of Pleasure
subtitle: How Our Brains Make Fatty Foods, Orgasm, Exercise, Marijuana, Generosity, Vodka, Learning and Gambling Feel So Good
main_color: F28830
text_color: 99561F
---

# The Compass of Pleasure

_How Our Brains Make Fatty Foods, Orgasm, Exercise, Marijuana, Generosity, Vodka, Learning and Gambling Feel So Good_

**David J. Linden**

_The Compass of Pleasure_ (2011) explains what seemingly different experiences, from taking heroin to giving to charity, from overeating to orgasm, have in common: their impact on our brain's pleasure circuitry. These blinks reveal the way pleasurable experiences rewire our brains over time and explain the true nature of addiction.

---
### 1. What’s in it for me? Discover the neurological secrets behind pleasure. 

Have you ever wondered why some things are so pleasurable? And why some things are addictive, while others are not?

Neuroscience has been able to shed light on some of these mysteries. As these blinks will show, pleasure, whether it comes from digging into a juicy chocolate cake, or placing a daring bet at the poker table, arises in an intricate set of structures in the brain that all work together in sometimes surprising ways.

In these blinks you'll learn

  * why cigarettes are more addictive than heroin;

  * the real difference between sex and love; and

  * what running and cannabis have in common.

### 2. Pleasurable experiences activate the brain’s medial forebrain pleasure circuit. 

What's your preferred source of pleasure? Whether it's legal, illegal, something you can talk about at the dinner table or something taboo, pleasurable activities all share one thing in common. They activate the pleasure circuit in the medial forebrain. So although you might view an orgasm, eating a slice of chocolate cake or injecting heroin as rather different events, the science behind them is essentially the same. 

The human brain contains several interconnected structures that allow us to feel pleasure. One of these structures is the ventral tegmental area (VTA). When we experience something pleasurable, the neurons in the VTA release dopamine to the amygdala. This is the part of the brain which controls our emotions. 

Dopamine is also sent to the dorsal striatum, a structure that is responsible for learning habits. So when you eat a delicious slice of chocolate cake you'll both enjoy it and want to eat it again — and again. Our attempts to repeatedly experience certain kinds of pleasure lead to habits and addictions.

In this way, our medial forebrain pleasure circuit is a strong influence on our behavior. Scientists have examined this relationship through studies where the pleasure circuit is deliberately stimulated. 

One of these studies was highly controversial, and its findings have been contested to this day. It took place in the 1970s, at Tulane University, by Dr. Robert Galbraith Heath. He wanted to discover if a homosexual man could derive pleasure from heterosexual intercourse by electrically stimulating his pleasure circuit. 

Heath implanted electrodes into the subject's brain and claimed that later in the study, the subject had changed so much that he was able to ejaculate during intercourse with a woman in the lab. Heath also said that the subject even had a sexual relationship with a married woman after the experiment concluded.

Despite its limited scope, this study shows that direct electrical stimulation on the brain's pleasure circuitry can be highly influential to short-term behavior.

> _"There's a neural unity of virtue and vice — pleasure is our compass, no matter the path we take."_

### 3. Addiction is shaped by the way in which a drug gives pleasure – and by its availability. 

We now know that enjoyable activities activate our pleasure circuit. But not all activities trigger the circuit in the same way. Some things stimulate it more strongly than others, which makes the activity itself more addictive. 

Heroin has a powerful impact on our pleasure circuit, and thus carries a higher risk of addiction than weaker drugs like cannabis. Drugs such as LSD carry little to no risk of addiction because they don't activate the pleasure circuit at all. 

Still, neurology isn't all that matters. For example, a recent study in the United States revealed that 80 percent of those who try cigarettes become addicted, while only 35 percent of those who inject heroin continue using it. Why is that?

Of course, heroin is illegal, while cigarettes aren't, so it makes sense that more of us take up smoking than shooting up. A drug's availability, the attitudes of your peers toward it and the methods and rituals surrounding it are hugely influential factors in addiction. 

But it also comes down to the kinds of pleasure that heroin and cigarettes produce. An injection of heroin results in one big pleasure surge, while lots of puffs at many cigarettes give us lighter and more fleeting pleasure rushes. 

This means that acting and being rewarded for that action happens more frequently with smoking — thus addiction occurs more quickly. It's like training a dog — if you ask him to do a trick multiple times a day and reward him with immediate, frequent treats, he'll learn a trick more quickly than if it happens just once in 24 hours.

Addiction doesn't just alter our behavior and daily routines. It physically changes structures in the brain. This was demonstrated in a study where rats that had been administered a cocaine solution for 28 days exhibited far bushier extensions of nerve cells in their pleasure circuit than prior to the experiment.

> _"Pleasure is central to some — but not all — psychoactive drugs."_

### 4. Our pleasure circuits are activated by foods full of fat and sugar. 

Why is it that you can never forgo your daily afternoon sugar rush? You've guessed it. Because it activates your brain's pleasure circuit. Although our brains have a system to keep our weight in check, it can be compromised when the delights of fatty foods lead us to addiction. 

Let's take a closer look at our brain's weight-management system first. You might not believe it, but your body should automatically control its weight. The hypothalamus in our brain receives signals from our body that inform it of gains and losses. As we put on the pounds, the level of leptin, a hormone produced by fat cells, increases. Leptin works by activating neurons in the hypothalamus that suppress appetite and increase energy expenditure. 

This means that in normal situations we no longer feel hungry after eating. But in cases where a person suffers from obesity, they may be leptin-resistant. This means that although eating results in an increase of leptin, it fails to suppress a person's appetite. 

And, while leptin is working hard to keep our eating under control, other hormones are making its job tougher. As we learned in the first blink, pleasure causes the hormone dopamine to be released. 

Foods high in sugar and fat release more dopamine, which is why we enjoy chocolate and pizza so much. The urge to continue activating our pleasure circuits with such foods may be powerful even when we don't have a huge appetite. The subsequent rewiring of our brains is what leads us to reach for the third slice of chocolate cake, and see the numbers on our bathroom scales rise.

### 5. Falling in love and sex both activate the pleasure circuit, but in different ways. 

Sex and love are not the same thing, and that's not just a line you might hear from a cheating partner. There's a neurological explanation behind it too. 

Falling in love and sexual arousal both activate the pleasure circuit. But the way they activate it is quite different. Unlike in sexual arousal, feelings of love deactivate the judgment and social cognition centers of the brain. This is why when we fall in love with someone, we believe them to be better, smarter or more beautiful than everybody else. Something to consider next time you meet a friend's questionable new squeeze! 

Lucy Brown at Albert Einstein College of Medicine in New York took brain scans of couples who were viewing images of their loved one and then an platonic acquaintance. She found that certain brain areas were activated by images of the former but not the latter. 

Orgasm has another, altogether different impact on the pleasure circuit. It's far more than just a pleasure buzz. An orgasm is a multifaceted emotional and sensory experience. In fact, it can be experienced without pleasure, too. 

Studies reveal that electrode stimulation can induce orgasms that feature the standard physiological hallmarks of increased heart rate and muscle contractions, but do not activate the pleasure circuit. These kinds of orgasms may occur in rape victims, or during epileptic seizures. 

However, orgasm is normally an intensely pleasurable experience. This is because it produces a dopamine surge. Dutch scientist Gert Holstege's brain scans of heterosexual men and women orgasming proved this by showing significant activation of the pleasure circuit. Sex and love both produce pleasure — all the more reason to combine them!

> _"Orgasm occurs in the brain, not the crotch."_

### 6. Gambling is addictive because it also stimulates the pleasure circuit. 

Many of us recoil at the prospect of losing money. Nevertheless, there are those who love the thrill of poker and blackjack. Why? The pleasure circuit is at work. 

We've already heard about drug addiction in the second blink, and seen how fatty foods can be addictive. But can a recreational behavior like gambling be an addiction too? To find out, let's take a look at what an addiction really is. 

An activity or substance becomes addictive when it leads to persistent, compulsive repetition in the face of increasingly negative life consequences; increased dependence alongside increased tolerance; strong cravings in later stages that lead to a high incidence of relapse; the replacement of euphoric pleasure with desire; and dangerously pleasurable relapses after abstinence. 

So at the heart of all addictions is the activation and then alteration of the pleasure circuit. Gambling and other compulsive disorders like obsessive video-game playing meet the criteria above, and thus can be counted as addictions too. 

Both nature and nurture lead people to gamble. On the nature side, it turns out that our brains are hardwired to find certain kinds of uncertainty pleasurable. 

This is a characteristic we share with some primates. Wolfram Schultz of the University of Cambridge taught monkeys to associate a green light with a sweet syrupy reward and a red light with none. Then they learned that if a blue light flashed, there was a 50 percent chance they would get a treat two seconds later. The monkeys began to experience dopamine surges as they waited to find out if they would get their reward after each blue flash — the pleasure lay in the uncertainty, a feeling many human gamblers know well.

As for nurture, compulsive gambling often runs in families. Compulsive gambler Bill Lee describes in his memoir, _Born to Lose_, how his father dragged him round gambling dens as a "good luck charm" as a kid, and his grandfather even sold his family to another family to cover a gambling debt!

### 7. It’s not just vices that light up our pleasure circuit – healthy living and good behavior can too. 

We all know the long-term benefits of exercise, such as mental and cardiovascular improvement. But have you heard about the short-term positive impacts? One of these is runner's high.

Runner's high refers to the bliss that runners and other athletes enjoy even when nauseated from fatigue. This physical pain causes increased opioid release in the brain and a rise in endocannabinoids — the brain's natural cannabis-like molecules — in the bloodstream. 

Painful stimuli also release dopamine, which, as we know, gives us pleasure. This explains why we can actually experience pain and pleasure simultaneously, which can be the experience of women during childbirth and of sadomasochists during sex games. 

Giving to charity can also activate the pleasure circuit. This could be due to the pleasure of pure altruism, and to making an independent choice. It can also enhance social status. In fact, a recent paper by William Harbaugh at the University of Oregon found that taxation and charitable giving activated the pleasure center in the same way that receiving money did.

Knowing things simply for the sake of knowing can also activate the pleasure circuit. Ethan Bromberg-Martin and Okihide Hikosaka at the National Eye Institute in Bethesda, Maryland, showed that monkeys, like humans, when given the choice, opted to receive information about a future reward (such as its shape), even though it didn't boost their chances of receiving that reward.

> _"Ideas are like addictive drugs."_

### 8. Final summary 

The key message in this book:

**From heroin to compulsive gambling to gorging yourself on pizza, there are number of pleasurable activities that we run the risk of becoming addicted to, because our brains rewire themselves accordingly. Fortunately, there are other pleasurable experiences that don't do lasting damage to our lives, but activate our pleasure circuits in the same way, such as donating to charity.**

Actionable advice:

**Swap positive associations for the negative ones and beat that habit.**

Remember all those times you felt like dying during a frightening high, or those hangovers that were like being seasick at best? By associating these negative experiences with the addiction you're trying to beat instead of the happier times, it'll be that much easier to quit. Our brains are malleable, and that's what got you addicted. But it's also what can get you out of it!

**Suggested** **further** **reading:** ** _Meet Your Happy Chemicals_** **by Loretta Graziano Breuning**

_Meet Your Happy Chemicals_ (2012) provides a detailed introduction to the four chemicals responsible for our happiness: dopamine, serotonin, endorphin and oxytocin. The book explores the mechanics of what makes us happy and why, as well as why some bad things make us feel so good.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### David J. Linden

David J. Linden is a professor of neuroscience at John Hopkins University and editor-in-chief of the _Journal of Neurophysiology_. A popularizer of brain science, he is also the author of _The Accidental Mind: How Brain Evolution Has Given Us Love, Memory, Dreams and God_.

